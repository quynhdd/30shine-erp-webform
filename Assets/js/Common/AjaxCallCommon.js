﻿function AjaxPostApi(data, uri, async = false) {
    var result = null;
    $.ajax({
        type: "POST",
        url: uri,
        async: async,
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        //crossDomain: true,
        data: JSON.stringify(data),
        success: function (response, textStatus, xhr) {
            console.log(xhr);
            result = response;
        },
        error: function (jqXhr, statusText, errorMessager) {
            console.log(jqXhr);
            alert(jqXhr.statusText);
            return result = jqXhr.status;
        }
    });
    return result;
}

// Api Get
function AjaxGetApi(uri, async = false) {
    var result = null;
    $.ajax({
        type: "GET",
        url: uri,
        async: async,
        //data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        //crossDomain: true,
        success: function (response, textStatus, xhr) {
            console.log(xhr);
            result = response;
        },
        error: function (jqXhr, textStatus, errorMessager) {
            alert(jqXhr.statusText);
            return result = jqXhr.status;
        }
    });
    return result;
}

// Api PUT
function AjaxPutFromBodyApi(data, uri, async = false) {
    var result = null;
    $.ajax({
        type: "PUT",
        url: uri,
        async: async,
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        //crossDomain: true,
        success: function (response, statusText, xhr) {
            console.log(xhr);
            result = response;            
        },
        error: function (jqXhr, textStatus, errorMessager) {
            console.log(jqXhr.status);
            alert(jqXhr.statusText);
            return result = jqXhr.status;
        }
    });
    return result;
}

function AjaxDeleteFromBodyApi(uri, async = false) {
    var result = null;
    $.ajax({
        type: "DELETE",
        url: uri,
        async: async,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        //crossDomain: true,
        success: function (response, statusText, xhr) {
            console.log(xhr);
            result = response;            
        },
        error: function (jqXhr, textStatus, errorMessager) {
            console.log(jqXhr.status);
            alert(jqXhr.statusText);
            return result = jqXhr.status;
        }
    });
    return result;
}