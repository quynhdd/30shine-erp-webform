﻿
jQuery(document).ready(function () {
    $('.select').select2();

    $.datetimepicker.setLocale('vi');
    $('.datetime-picker').datetimepicker({
        dayOfWeekStart: 1,
        startDate: new Date(),
        format: 'd/m/Y',
        dateonly: true,
        showHour: false,
        showMinute: false,
        timepicker: false,
        scrollMonth: false,
        scrollInput: false,
        onChangeDateTime: function (dp, $input) { }
    });
});


/*
* Mở layer loading
*/
function startLoading() {
    $(".page-loading").fadeIn();
}

/*
* Đóng layer loading
*/
function finishLoading() {
    $(".page-loading").fadeOut();
}

//export excel
function exportExcel(fileName, sheetTitle, headers, body) {
    // Default font
    var excel = $JExcel.new("Calibri light 10 #333333");
    fileName = (fileName + "").trim();
    // excel.set is the main function to generate content:
    // We can use parameter notation excel.set(sheetValue,columnValue,rowValue,cellValue,styleValue)
    // Or object notation excel.set({sheet:sheetValue,column:columnValue,row:rowValue,value:cellValue,style:styleValue })
    // null or 0 are used as default values for undefined entries
    excel.set({ sheet: 0, value: sheetTitle });
    excel.addSheet("Sheet 2");
    // Borders are LEFT,RIGHT,TOP,BOTTOM. Check $JExcel.borderStyles for a list of valid border styles
    var evenRow = excel.addStyle({
        border: "thin,thin,thin,thin #333333"
    });
    // Style for odd ROWS
    // Background color, plain #RRGGBB, there is a helper $JExcel.rgbToHex(r,g,b)
    var oddRow = excel.addStyle({
        fill: "#ECECEC",
        border: "thin,thin,thin,thin #333333"
    });
    for (var i = 1; i < body.length; i++) excel.set({ row: i, style: i % 2 == 0 ? evenRow : oddRow });
    // Format for headers
    // Border for header
    var formatHeader = excel.addStyle({
        border: "thin,thin,thin,thin #333333",
        font: "Calibri 12 #0000AA B"
    });
    // Loop all the haders
    for (var i = 0; i < headers.length; i++) {
        excel.set(0, i, 0, headers[i], formatHeader);
        excel.set(0, i, undefined, "auto");
    }
    for (var i = 0; i < body.length; i++) {
        setDataRow(body[i], i + 1, excel);
    }
    if (fileName.substr(-5, 5) !== ".xlsx")
        fileName += ".xlsx";
    excel.generate(fileName);
}

// Loop all the haders
function setDataRow(data, rowIndex, excel) {
    for (var i = 0; i < data.length; i++) {
        excel.set(0, i, rowIndex, data[i]);
    }
}