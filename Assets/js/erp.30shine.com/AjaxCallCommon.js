﻿
//============================
// Get Query Strings
//============================ 
function getQueryStrings() {
    var assoc = {};
    var decode = function (s) { return decodeURIComponent(s.replace(/\+/g, " ")); };
    var queryString = location.search.substring(1);
    var keyValues = queryString.split('&');

    for (var i in keyValues) {
        var key = keyValues[i].split('=');
        if (key.length > 1) {
            assoc[decode(key[0])] = decode(key[1]);
        }
    }
    return assoc;
}
/*
* Mở layer loading
*/
function startLoading() {
    $(".page-loading").fadeIn();
}

/*
* Đóng layer loading
*/
function finishLoading() {
    $(".page-loading").fadeOut();
}
// Show thông báo cho người dùng.
//VD: showMsgSystem("Thông báo test"," bg-warning",15000);
function showMsgSystem(msg, status, duration) {
    var duration = duration || 5000;
    $(".msg-system").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);
    setTimeout(function () {
        $(".msg-system").fadeTo("slow", 0, function () {
            $(".msg-system").text("").css("opacity", 1);
            $(".msg-system").hide();

        });
    }, duration);
}
// Api Post
function AjaxPostApi(data, uri, postSuccessCallBack, async = false) {
    startLoading();
    $.ajax({
        type: "POST",
        url: uri,
        async: async,
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        crossDomain: true,
        data: JSON.stringify(data),
        success: function (response, textStatus, xhr) {
            if (typeof response.message !== 'undefined') {
                showMsgSystem(response.message, " bg-success", 15000);
            }
            else {
                alert("Thêm mới thành công!");
            }
            if (postSuccessCallBack) {
                postSuccessCallBack(xhr.statusText);
            }
        },
        error: function (jqXhr, textStatus, errorMessager) {
            finishLoading();
            showMsgSystem("Đã xảy ra lỗi", " bg-warning", 15000);
            return;
        }
    });
};
// Api Post
function AjaxPostApiV1(data, uri, async = false) {
    startLoading();
    var result = null;
    $.ajax({
        type: "POST",
        url: uri,
        async: async,
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        crossDomain: true,
        data: JSON.stringify(data),
        success: function (response, textStatus, xhr) {
            result = response.d;
            finishLoading();
        },
        error: function (jqXhr, textStatus, errorMessager) {
            finishLoading();
            showMsgSystem("Đã xảy ra lỗi", " bg-warning", 15000);
            return;
        }
    });
    return result;
};
// Api Get
function AjaxGetApiV1(uri, async = false) {
    var result = null;
    $.ajax({
        type: "GET",
        url: uri,
        async: async,
        //data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        //crossDomain: true,
        success: function (response, textStatus, xhr) {
            result = response;
        },
        error: function (jqXhr, textStatus, errorMessager) {
            showMsgSystem("Đã xảy ra lỗi", " bg-warning", 15000);
            //alert(jqXhr.statusText);
            return;
        }
    });
    return result;
};
// Api PUT
function AjaxPutFromBodyApi(data, uri, putSuccessCallBack, async = false) {
    startLoading();
    $.ajax({
        type: "PUT",
        url: uri,
        async: async,
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        crossDomain: true,
        success: function (response, textStatus, xhr) {
            if (typeof response.message !== 'undefined') {
                showMsgSystem(response.message, " bg-success", 15000);
            }
            else {
                alert("Cập nhật thành công!");
            }
            if (putSuccessCallBack) {
                putSuccessCallBack(xhr.statusText);
            }
        },
        error: function (jqXhr, textStatus, errorMessager) {
            showMsgSystem("Đã xảy ra lỗi", " bg-warning", 15000);
            //alert(jqXhr.statusText);
            return;
        }
    });
}
// Validate keypress
function ValidateKeypress(numcheck, e) {
    var keynum, keychar, numcheck;
    if (window.event) {
        keynum = e.keyCode;
    }
    else if (e.which) {
        keynum = e.which;
    }
    if (keynum === 8 || keynum === 127 || keynum === null || keynum === 9 || keynum === 0 || keynum === 13) return true;
    keychar = String.fromCharCode(keynum);
    var result = numcheck.test(keychar);
    return result;
}
//định dạng text box học phí 000.000
String.prototype.reverse = function () {
    return this.split("").reverse().join("");
}
function reformatText(input) {
    var x = input.value;
    x = x.replace(/,/g, "");
    x = x.reverse();
    x = x.replace(/.../g, function (e) {
        return e + ",";
    });
    x = x.reverse();
    x = x.replace(/^,/, "");
    input.value = x;
}