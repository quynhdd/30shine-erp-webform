﻿var validate = { vipcard: true };
var listIds = [];
var existStylist = false;
var recordIds = {};
//var requestRatingver1;
//var requestRatingver2;
jQuery(document).ready(function () {

    //getRatingver2();
    // Add active menu
    $("#glbService").addClass("active");
    $("#subMenu .li-pending-complete").addClass("active");

    // Mở box listing sản phẩm
    $(".show-product").bind("click", function () {
        $(this).parent().parent().find(".listing-product").show();
        $("select#ddlProductCategory").val($("select#ddlProductCategory option:first-child").val());
        showProductByCategory($("select#ddlProductCategory"));
    });

    $(".show-item").bind("click", function () {
        var item = $(this).attr("data-item");
        var classItem = ".popup-" + item + "-item";

        $(classItem).openEBPopup();
        ExcCheckboxItem();
        ExcQuantity();
        ExcCompletePopup();

        $('#EBPopup .listing-product').mCustomScrollbar({
            theme: "dark-2",
            scrollInertia: 100
        });

        $("#EBPopup .btn-esc").bind("click", function () { autoCloseEBPopup(0); });
    });

    // Btn Send
    $("#BtnSend").bind("click", function () {
        var InputVal = {
            Stylist: parseInt($("#Hairdresser").val().trim()) || 0,
            Skinner: parseInt($("#HairMassage").val().trim()) || 0,
            Checkout: parseInt($("#HDF_Checkout").val().trim()) || 0,
            CustomerName: $("#CustomerName").val().trim(),
            TotalMoney: parseInt($("#HDF_TotalMoney").val().trim()) || 0,
            hasServices: $("#HDF_ServiceIds").val().trim() != "" && $("#HDF_ServiceIds").val().trim() != "[]",
            hasProducts: $("#HDF_ProductIds").val() != "" && $("#HDF_ProductIds").val() != "[]",
            Comestic: parseInt($("#Cosmetic").val().trim()) || 0,
            RatingPoint: parseInt($("#HDF_Rating").val()) || 0,
            Description: $("#Description").val().trim()
        };
        try {
            // Validate : Mã khách hàng || Khách không cho thông tin
            if (InputVal.CustomerName.length < 1 && $("#InputCusNoInfor").is(":checked") == false) {
                throw new Error("Bạn chưa nhập mã khách hàng hoặc chưa tích chọn \"Khách không cho thông tin\".");
            }

            // Validate : Chọn dịch vụ khi đã nhập Stylist hoặc Skinner hoặc Checkout
            if (!InputVal.hasServices && !InputVal.Stylist && !InputVal.Skinner && !InputVal.Checkout) {
                if (InputVal.hasProducts && !InputVal.Comestic) {
                    throw new Error("Bạn chưa chọn Nhân viên bán mỹ phẩm");
                }

                if (!InputVal.hasProducts && InputVal.Comestic) {
                    throw new Error("Bạn chưa chọn mỹ phẩm");
                }

                if (!InputVal.Comestic && !InputVal.hasProducts) {
                    throw new Error("Bạn chưa chọn dịch vụ hoặc sản phẩm.");
                }
            }

            if (!InputVal.hasServices && (InputVal.Stylist || InputVal.Skinner || InputVal.Checkout)) {
                throw new Error("Bạn chưa chọn dịch vụ");
            }

            if ((InputVal.hasServices || InputVal.hasProducts) && (!InputVal.Stylist && !InputVal.Skinner && !InputVal.Comestic && !InputVal.Checkout)) {
                throw new Error("Bạn chưa chọn Nhân viên");
            }

            if (InputVal.hasServices && !InputVal.Checkout && checkSalon) {
                throw new Error("Bạn chưa chọn Checkout");
            }

            // Validate : Chọn dịch vụ nhưng chưa chọn Stylist hoặc skinner
            if (!InputVal.Stylist && !InputVal.Skinner && !InputVal.Comestic && !InputVal.Checkout) {
                throw new Error("Bạn chưa chọn Nhân viên.");
            }

            if (InputVal.Comestic) {
                if (!InputVal.hasProducts) {
                    throw new Error("Bạn chọn người bán nhưng chưa chọn sản phẩm.");
                }
            }

            if (!validate.vipcard) {
                throw new Error("Lỗi thẻ VIP.");
            }

            // Validate : Chọn dịch vụ hoặc sản phẩm
            if (!InputVal.TotalMoney) {
                //throw new Error("Bạn chưa chọn dịch vụ hoặc sản phẩm.");
            }
        }
        catch (ex) {
            showMsgSystem(ex.message, "warning");
            return;
        }


        var staffInputError = [];

        $('.staff-auto-selection').each(function (index, item) {
            var Type = item.dataset.staffType;
            var OrderCode = item.value.trim().length > 0 ? parseInt(item.value.trim()) : 0;
            var staff = _FN.StaffSuggestion.getStaff(Type, OrderCode);

            if (OrderCode > 0 && staff === null) {
                staffInputError.push(item);
            }
        });

        if (staffInputError.length > 0) {
            var txtStaffInputError = '';

            for (var i in staffInputError) {
                txtStaffInputError += "Thông tin ô nhập " + staffInputError[i].getAttribute('placeholder') + " chưa đúng, vui lòng kiểm tra lại!<br>";
            }

            // show EBPopup
            $("#contentStaffInputError").empty().html(txtStaffInputError);
            $(".popup-staff-input-error").openEBPopup();
            staffInputError[0].focus();

            return;
        }

        // Check cosmetic seller
        if (InputVal.hasProducts && !InputVal.Comestic) {
            $("#ValidateCosmetic").css({ "visibility": "visible" });
            return;
        }

        $("#ValidateCosmetic").css({ "visibility": "hidden" });
        var CustomerCode = $("#HDF_CustomerCode").val();
        var submit = function () {
            //Bind_UserImagesToHDF();

            var submitHandler = function () {
                //addLoading();
                //Bind_UserImagesToHDF();
                //Call api ghi nhận thông tin chiến dịch marketting.
                //CallApiMktCampaign();
                //closeAppRating();
                //In phieu, chỉ in phiếu khi checkout lần đầu.
                //if (pending == 1) {
                //    PrintBill();
                //}
                $("#BtnFakeSend").click();
            };
            var ret = $.event.trigger({
                type: "onBeforeCompleteBillService",
                submitHandler: submitHandler,
                _data: InputVal
            });

            if (typeof ret === "function") {
                submitHandler = ret;
            }

            submitHandler.call(null);
        };

        // Check duplicate bill
        $.ajax({
            type: "POST",
            url: "/GUI/FrontEnd/Service/Service_Add.aspx/CheckDuplicateBill",
            data: '{CustomerCode : "' + CustomerCode + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                var mission = JSON.parse(response.d);
                if (mission.success) {
                    removeLoading();
                    var products = "Sản phẩm : ";
                    var services = "Dịch vụ : ";
                    var confirm_content = "";
                    var bill = JSON.parse(mission.msg);
                    if (bill.ProductIds != "") {
                        var _Product = $.parseJSON(bill.ProductIds);
                        $.each(_Product, function (i, v) {
                            products += v.Name + " ( " + v.Price + " ), ";
                        });
                        products = products.replace(/(\,\s)+$/, '');
                    }
                    if (bill.ServiceIds != "") {
                        var _Service = $.parseJSON(bill.ServiceIds);
                        $.each(_Service, function (i, v) {
                            services += v.Name + " ( " + v.Price + " ), ";
                        });
                        services = services.replace(/(\,\s)+$/, '');
                    }
                    confirm_content += products + ", " + services + ", ";
                    confirm_content = confirm_content.replace(/(\,\s)+$/, '');

                    // show EBPopup
                    $(".confirm-yn").openEBPopup();
                    $("#EBPopup .confirm-yn-text").text("Khách hàng mã [" + CustomerCode + "] đã được lập hóa đơn trong hôm nay [ " + confirm_content + " ]. Bạn có chắc muốn tiếp tục ?");

                    $("#EBPopup .yn-yes").bind("click", function () {
                        $("#EBCloseBtn").click();
                        submit.call(null);

                    });
                    $("#EBPopup .yn-no").bind("click", function () {
                        autoCloseEBPopup(0);
                    });
                } else {
                    submit.call(null);
                }
            },
            beforeSend: function () {
                startLoading();
            },
            complete: function () {
                finishLoading();
            },
            failure: function (response) { alert(response.d); }
        });
    });

    // Init execute service, product quantity
    TotalMoney();
    getProductIds();
    getServiceIds();
    ExcQuantity();

    // Auto select staff
    AutoSelectStaff();

    /// Bind gói phụ trợ
    pushFreeService();

    /// Get Rating
    //requestRatingver1 = setInterval(getRatingver1, 1200);
    //requestRatingver2 = setInterval(getRatingver2, 3000);

    excThumbWidth();

    $(window).load(function () {
        $('.listing-img-upload .thumb-wp').each(function () {
            var THIS = $(this);

            $("<img>").attr("src", $(".thumb").attr("src")).load(function () {
                var ImgW = this.width;
                var ImgH = this.height;
                console.log(ImgW);
                if ((ImgW > ImgH)) {
                    (THIS).find('img').addClass('degree90');
                }
            });
        });
    });

    // Enter don't submit
    $(window).on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });
});

function excThumbWidth() {
    var ImgStandardWidth = 120,
        ImgStandardHeight = 120;
    var width = ImgStandardWidth,
        height, left, top;
    $(".thumb-wp .thumb").each(function () {
        height = ImgStandardWidth * $(this).height() / $(this).width();
        left = 0;
        top = (ImgStandardHeight - height) / 2;
        $(this).css({ "width": width, "height": height, "left": left, "top": top });
    });
}

//// Xử lý mã khách hàng => bind khách hàng
//function LoadCustomer(code) {
//    ajaxGetCustomer(code);
//}

// Xử lý khi chọn checkbox sản phẩm
function ExcCheckboxItem() {
    $("#EBPopup .td-product-checkbox input[type='checkbox']").unbind("click").bind("click", function () {
        var obj = $(this).parent().parent(),
            boxMoney = obj.find(".box-money"),
            price = obj.find(".td-product-price").data("price"),
            quantity = obj.find("input.product-quantity").val(),
            voucher = obj.find("input.product-voucher").val(),
            checked = $(this).is(":checked"),
            item = obj.attr("data-item"),
            promotion = 0;
        obj.find(".promotion-money input[type='checkbox']:checked").each(function () {
            var value = $(this).attr("data-value").trim();
            promotion += (value != "" ? parseInt(value) : 0);
        });

        // check value
        price = price.toString().trim() != "" ? parseInt(price) : 0;
        quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
        voucher = voucher.trim() != "" ? parseInt(voucher) : 0;


        if (checked) {
            boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100 - promotion)).show();
        } else {
            boxMoney.text("").hide();
        }

    });
}

// Số lượng | Quantity
function ExcQuantity() {
    $("input.product-quantity").bind("change", function () {
        var obj = $(this).parent().parent(),
            quantity = $(this).val(),
            price = obj.find(".td-product-price").data("price"),
            voucher = obj.find("input.product-voucher").val(),
            boxMoney = obj.find(".box-money"),
            promotion = 0;

        obj.find(".promotion-money input[type='checkbox']:checked").each(function () {
            var value = $(this).attr("data-value").trim();
            promotion += (value != "" ? parseInt(value) : 0);
        });

        // check value
        price = price.toString().trim() != "" ? parseInt(price) : 0;
        quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
        voucher = voucher.trim() != "" ? parseInt(voucher) : 0;

        boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100 - promotion)).show();
        TotalMoney();
        getProductIds();
        getServiceIds();
    });
    $("input.product-voucher").bind("change", function () {
        var obj = $(this).parent().parent().parent(),
            quantity = obj.find("input.product-quantity").val(),
            price = obj.find(".td-product-price").data("price"),
            voucher = obj.find("input.product-voucher").val(),
            boxMoney = obj.find(".box-money"),
            promotion = 0;

        obj.find(".promotion-money input[type='checkbox']:checked").each(function () {
            var value = $(this).attr("data-value").trim();
            promotion += (value != "" ? parseInt(value) : 0);
        });

        // check value
        price = price.toString().trim() != "" ? parseInt(price) : 0;
        quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
        voucher = voucher.trim() != "" ? parseInt(voucher) : 0;

        console.log(voucher);

        if (promotion == 0) {
            boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100)).show();
        } else {
            obj.find("input.product-voucher").val(0);
        }

        TotalMoney();
        getProductIds();
        getServiceIds();
    });
    $("input.item-promotion").bind("click", function () {
        var obj = $(this).parent().parent().parent().parent().parent(),
            quantity = obj.find("input.product-quantity").val(),
            price = obj.find(".td-product-price").data("price"),
            voucher = obj.find("input.product-voucher").val(),
            boxMoney = obj.find(".box-money"),
            promotion = $(this).attr("data-value"),
            _This = $(this),
            isChecked = $(this).is(":checked");
        obj.find(".promotion-money input[type='checkbox']:checked").prop("checked", false);
        if (isChecked) {
            $(this).prop("checked", true);
            promotion = promotion.trim() != "" ? parseInt(promotion) : 0;
            obj.find("input.product-voucher").val(0);
        } else {
            promotion = 0;
        }

        // check value
        price = price.toString().trim() != "" ? parseInt(price) : 0;
        quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
        voucher = voucher.trim() != "" ? parseInt(voucher) : 0;
        //promotion = promotion.trim() != "" ? parseInt(promotion) : 0;

        if (promotion > 0) {
            boxMoney.text(FormatPrice(quantity * price - promotion)).show();
            obj.find("input.product-voucher").css({ "text-decoration": "line-through" });
        } else {
            boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100)).show();
            obj.find("input.product-voucher").css({ "text-decoration": "none" });
        }
        TotalMoney();
        getProductIds();
        getServiceIds();
    });
}

// Xử lý click hoàn tất chọn item từ popup
function ExcCompletePopup() {

    $("#EBPopup .btn-complete").unbind("click").bind("click", function () {
        var item = $(this).attr("data-item"),
            tableItem = $("table.table-item-" + item + " tbody"),
            objTmp;

        $("#EBPopup .item-product-checkbox input[type='checkbox']:checked").each(function () {
            var Code = $(this).attr("data-code");
            if (!ExcCheckItemIsChose(Code, tableItem)) {
                objTmp = $(this).parent().parent().clone().find("td:first-child").remove().end();
                tableItem.append(objTmp);
            }
        });

        TotalMoney();
        getProductIds();
        getServiceIds();
        ExcQuantity();
        UpdateItemOrder(tableItem);
        autoCloseEBPopup(0);
    });
}

// Check item đã được chọn
function ExcCheckItemIsChose(Code, itemClass) {
    var result = false;
    $(itemClass).find(".td-product-code").each(function () {
        var _Code = $(this).text().trim();
        if (_Code == Code)
            result = true;
    });
    return result;
}

// Remove item đã được chọn
function RemoveItem(THIS, name, itemName) {
    existStylist = false;
    // Confirm
    $("table.table-item-service tbody tr").each(function () {
        recordIds = {};
        var THIS = $(this);
        var Id = THIS.find("td.td-product-code").attr("data-id");
        // check value
        Id = Id.toString().trim() != "" ? parseInt(Id) : 0;
        listIds.push(recordIds);
    });
    $(".confirm-yn").openEBPopup();
    if (listIds.length == 1) {
        $("#EBPopup .confirm-yn-text").text("Bạn có muốn xoá luôn cả stylist không?");
    }
    else {
        $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn hủy [ " + name + " ] ?");
    }
    listIds = [];
    $("#EBPopup .yn-yes").bind("click", function () {
        var Code = THIS.find(".td-product-code").attr("data-code");
        //Su dung cho ham CalCampaignService();
        var CodeV1 = THIS.find(".td-product-code")
        $("#quick-" + itemName).find("input[data-code='" + Code + "']").removeAttr("checked");
        THIS.remove();
        CalCampaignService(CodeV1);
        TotalMoney();
        getProductIds();
        getServiceIds();
        UpdateItemDisplay(itemName);
        autoCloseEBPopup(0);
    });
    $("#EBPopup .yn-no").bind("click", function () {
        existStylist = true;
        var Code = THIS.find(".td-product-code").attr("data-code");
        //Su dung cho ham CalCampaignService();
        var CodeV1 = THIS.find(".td-product-code")
        $("#quick-" + itemName).find("input[data-code='" + Code + "']").removeAttr("checked");
        THIS.remove();
        CalCampaignService(CodeV1);
        TotalMoney();
        getServiceIds();
        UpdateItemDisplay(itemName);
        autoCloseEBPopup(0);
    });
}

// Remove item (đã lưu trong phiếu pending) được chọn
function RemoveItemPending(THIS, itemName, itemId, itemType) {
    // Confirm
    $(".confirm-yn").openEBPopup();

    $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn hủy [ " + itemName + " ] ?");
    $("#EBPopup .yn-yes").bind("click", function () {

        THIS.remove();
        TotalMoney();
        getProductIds();
        getServiceIds();
        UpdateItemDisplay(itemType);
        autoCloseEBPopup(0);
        $.ajax({
            type: "POST",
            url: "/GUI/SystemService/Ajax/Del.aspx/Delele_Item_Pending",
            data: '{itemId : ' + itemId + ', BillId : ' + $("#HDF_BillId").val() + ', itemType : "' + itemType + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                var mission = JSON.parse(response.d);
                if (mission.success) {
                    delSuccess();
                    Row.remove();
                } else {
                    delFailed();
                }
            },
            failure: function (response) { alert(response.d); }
        });
    });
    $("#EBPopup .yn-no").bind("click", function () {
        autoCloseEBPopup(0);
    });
}

// Update table display
function UpdateItemDisplay(itemName) {
    var dom = $(".table-item-" + itemName);
    var len = dom.find("tbody tr").length;
    if (len == 0) {
        dom.parent().hide();
    } else {
        UpdateItemOrder(dom.find("tbody"));
    }
}

// Update order item
function UpdateItemOrder(dom) {
    var index = 1;
    dom.find("tr").each(function () {
        $(this).find("td.td-product-index").text(index);
        index++;
    });
}

function TotalMoney() {
    var Money = 0;
    $(".fe-service-table-add .box-money:visible").each(function () {
        Money += parseInt($(this).text().replace(/\./gm, ""));
    });
    $("#TotalMoney").val(FormatPrice(Money));
    $("#HDF_TotalMoney").val(Money);
}

//function showMsgSystem(msg, status) {
//    debugger;
//    $("#MsgSystem").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);
//    setTimeout(function () {
//        $("#MsgSystem").fadeTo("slow", 0, function () {
//            $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 1);
//        });
//    }, 5000);
//    $('html, body').animate({ scrollTop: 0 }, 'fast');

//}

function hideMsgSystem() {
    $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 0);
}

function getProductIds() {
    var Ids = [];
    var prd = {};
    $("table.table-item-product tbody tr").each(function () {
        prd = {};
        var THIS = $(this);
        var Id = THIS.find("td.td-product-code").attr("data-id"),
            Code = THIS.find("td.td-product-code").text().trim(),
            MapId = THIS.find("td.td-product-map").attr("data-mapid"),
            Name = THIS.find("td.td-product-name").text().trim(),
            Price = THIS.find(".td-product-price").attr("data-price"),
            Quantity = THIS.find("input.product-quantity").val(),
            VoucherPercent = THIS.find("input.product-voucher").val(),
            Promotion = 0;

        THIS.find(".promotion-money input[type='checkbox']:checked").each(function () {
            var value = $(this).attr("data-value").trim();
            Promotion += (value != "" ? parseInt(value) : 0);
        });

        // check value
        Id = Id.toString().trim() != "" ? parseInt(Id) : 0;
        Price = Price.toString().trim() != "" ? parseInt(Price) : 0;
        Quantity = Quantity.trim() != "" ? parseInt(Quantity) : 0;
        VoucherPercent = VoucherPercent.trim() != "" ? parseInt(VoucherPercent) : 0;

        prd.Id = Id;
        prd.Code = Code;
        prd.MapIdProduct = MapId;
        prd.Name = Name;
        prd.Price = Price;
        prd.Quantity = Quantity;
        prd.VoucherPercent = VoucherPercent;
        prd.Promotion = Promotion;

        Ids.push(prd);

    });
    Ids = JSON.stringify(Ids);
    $("#HDF_ProductIds").val(Ids);
}

function getServiceIds() {
    var Ids = [];
    var prd = {};
    $("table.table-item-service tbody tr").each(function () {
        prd = {};
        var THIS = $(this),
            obj = THIS.parent().parent();

        var Id = THIS.find("td.td-product-code").attr("data-id"),
            Code = THIS.find("td.td-product-code").text().trim(),
            Name = THIS.find("td.td-product-name").text().trim(),
            Price = THIS.find(".td-product-price").attr("data-price"),
            Quantity = THIS.find("input.product-quantity").val(),
            VoucherPercent = THIS.find("input.product-voucher").val();

        // check value
        Id = Id.toString().trim() != "" ? parseInt(Id) : 0;
        Price = Price.toString().trim() != "" ? parseInt(Price) : 0;
        Quantity = Quantity.trim() != "" ? parseInt(Quantity) : 0;
        VoucherPercent = VoucherPercent.trim() != "" ? parseInt(VoucherPercent) : 0;

        prd.Id = Id;
        prd.Code = Code;
        prd.Name = Name;
        prd.Price = Price;
        prd.Quantity = Quantity;
        prd.VoucherPercent = VoucherPercent;

        Ids.push(prd);

    });
    Ids = JSON.stringify(Ids);
    if (Ids == "[]") {
        if (!existStylist) {
            let $value = $("#InputStylist").val();
            if ($value != '') {
                document.getElementById("InputStylist").setAttribute('value', '');
                document.getElementById("Hairdresser").setAttribute('value', '');
                $('#FVStylist').html('');
            }
        }
    }
    $("#HDF_ServiceIds").val(Ids);
}

// Get Customer
function ajaxGetCustomer(CustomerCode) {
    $.ajax({
        type: "POST",
        url: "/GUI/FrontEnd/Service/Service_Add.aspx/GetCustomer",
        data: '{CustomerCode : "' + CustomerCode + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json", success: function (response) {
            var mission = JSON.parse(response.d);
            if (mission.success) {
                var customer = JSON.parse(mission.msg);
                customer = customer[0];
                $("#CustomerName").val(customer.Fullname);
                $("#CustomerName").attr("data-code", customer.Customer_Code);
                $("#HDF_CustomerCode").val(customer.Customer_Code);

            } else {
                var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                showMsgSystem(msg, "warning");
            }
        },
        failure: function (response) { alert(response.d); }
    });
}

function pushQuickData(This, typeData, cusType, disService) {
    var Code = This.attr("data-code");
    if (This.is(":checked")) {
        var Dom = $("#table-" + typeData).find("input[data-code='" + Code + "']").parent().parent().clone().find("td:first-child").remove().end(),
            quantity = Dom.find(".product-quantity").val(),
            price = Dom.find(".td-product-price").data("price"),
            voucher = Dom.find("input.product-voucher").val();
        if (cusType == "1" || cusType == "2") {
            Dom.find("input.product-voucher").val(disService);
            voucher = disService;
        }
        // check value            
        price = price.toString().trim() != "" ? parseInt(price) : 0;
        quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
        voucher = voucher.trim() != "" ? parseInt(voucher) : 0;

        Dom.find(".box-money").text(FormatPrice(quantity * price * (100 - voucher) / 100)).show();
        console.log(Dom);
        $("#table-item-" + typeData).append(Dom);
        $(".item-" + typeData).show();

    } else {
        $("#table-item-" + typeData).find(".td-product-code[data-code='" + Code + "']").parent().remove();
    }

    TotalMoney();
    getProductIds();
    getServiceIds();
    ExcQuantity();
    UpdateItemOrder($("#table-item-" + typeData).find("tbody"));
}

function pushFreeService(This, id) {
    var arr = [];
    var sv = {};
    $(".free-service input[type='checkbox']").each(function () {
        if ($(this).is(":checked")) {
            arr.push(parseInt($(this).attr('data-id')));
        }
    });
    $("#HDF_FreeService").val(JSON.stringify(arr));
}

//============================
// Suggestion Functions
//============================
//function Bind_Suggestion() {
//    $(".eb-suggestion").bind("keyup", function (e) {
//        if (e.keyCode == 40) {
//            UpDownListSuggest($(this));
//        } else {
//            Call_Suggestion($(this));
//        }
//    });
//    $(".eb-suggestion").bind("focus", function () {
//        Call_Suggestion($(this));
//    });
//    $(".eb-suggestion").bind("blur", function () {
//        //Exc_To_Reset_Suggestion($(this));
//    });
//    $(window).bind("click", function (e) {
//        if ((!e.target.className.match("eb-select") && e.target.parentElement.className.match("ul-listing-suggestion")) ||
//            (!e.target.className.match("eb-select") && !e.target.parentElement.className.match("ul-listing-suggestion"))) {
//            EBSelect_HideBox();
//        }
//    });
//}

//function UpDownListSuggest(This) {
//    var UlSgt = This.parent().find(".ul-listing-suggestion"),
//        index = 0,
//        LisLen = UlSgt.find(">li").length - 1,
//        Value;

//    This.blur();
//    UlSgt.find(">li.active").removeClass("active");
//    UlSgt.find(">li:eq(" + index + ")").addClass("active");

//    $(window).unbind("keydown").bind("keydown", function (e) {
//        if (e.keyCode == 40) {
//            if (index == LisLen) return false;
//            UlSgt.find(">li.active").removeClass("active");
//            UlSgt.find(">li:eq(" + (++index) + ")").addClass("active");
//            return false;
//        } else if (e.keyCode == 38) {
//            if (index == 0) return false;
//            UlSgt.find(">li.active").removeClass("active");
//            UlSgt.find(">li:eq(" + (--index) + ")").addClass("active");
//            return false;
//        } else if (e.keyCode == 13) {
//            // Bind data to HDF Field
//            var THIS = UlSgt.find(">li.active");
//            //var Value = THIS.text().trim();
//            var Value = THIS.attr("data-code");
//            var dataField = This.attr("data-field");

//            BindIdToHDF(THIS, Value, dataField, "#HDF_Suggestion_Code", "#HDF_Suggestion_Field", This);
//            EBSelect_HideBox();
//        }
//    });
//}

//function Exc_To_Reset_Suggestion(This) {
//    var value = This.val();
//    if (value == "") {
//        $(".eb-suggestion").each(function () {
//            var THIS = $(this);
//            var sgValue = THIS.val();
//            if (sgValue != "") {
//                BindIdToHDF(THIS, sgValue, THIS.attr("data-field"), "#HDF_Suggestion_Code", "#HDF_Suggestion_Field", THIS);
//                return false;
//            }
//        });
//    }
//}

//function Call_Suggestion(This) {
//    var text = This.val(),
//        field = This.attr("data-field");
//    Suggestion(This, text, field);
//}

//function Suggestion(This, text, field) {
//    var This = This;
//    var text = text || "";
//    var field = field || "";
//    var InputDomId;
//    var HDF_Sgt_Code = "#HDF_Suggestion_Code";
//    var HDF_Sgt_Field = "#HDF_Suggestion_Field";

//    if (text == "") return false;

//    switch (field) {
//        case "customer.name": InputDomId = "#CustomerName"; break;
//        case "customer.phone": InputDomId = "#CustomerPhone"; break;
//        case "customer.code": InputDomId = "#CustomerCode"; break;
//        case "bill.code": InputDomId = "#BillCode"; break;
//    }

//    $.ajax({
//        type: "POST",
//        url: "/GUI/SystemService/Ajax/Suggestion.aspx/Suggestion_Staff",
//        data: '{field : "' + field + '", text : "' + text + '"}',
//        contentType: "application/json; charset=utf-8",
//        dataType: "json", success: function (response) {
//            var mission = JSON.parse(response.d);
//            if (mission.success) {
//                var OBJ = JSON.parse(mission.msg);
//                if (OBJ.length > 0) {
//                    var lis = "";
//                    $.each(OBJ, function (i, v) {
//                        lis += "<li data-code='" + v.Customer_Code + "'" +
//                            "onclick=\"BindIdToHDF($(this),'" + v.Customer_Code + "','" + field + "','" + HDF_Sgt_Code +
//                            "','" + HDF_Sgt_Field + "','" + InputDomId + "')\">" +
//                            v.Value +
//                            "</li>";
//                    });
//                    This.parent().find("ul.ul-listing-suggestion").empty().append(lis).parent().show();
//                    This.parent().find(".eb-select-data").show();
//                } else {
//                    This.parent().find(".eb-select-data").hide().find("ul.ul-listing-suggestion").empty();
//                }
//            } else {
//                This.parent().find("ul.ul-listing-suggestion").empty();
//                var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
//                showMsgSystem(msg, "warning");
//            }
//        },
//        failure: function (response) { alert(response.d); }
//    });
//}

//function BindIdToHDF(THIS, Code, Field, HDF_Sgt_Code, HDF_Sgt_Field, Input_DomId) {
//    var text = THIS.text().trim();
//    $("input.eb-suggestion").val("");
//    $(HDF_Sgt_Code).val(Code);
//    $(HDF_Sgt_Field).val(Field);
//    $(Input_DomId).val(text);
//    $(Input_DomId).parent().find(".eb-select-data").hide();

//    // Auto post server
//    $("#BtnFakeUP").click();
//}

//function EBSelect_HideBox() {
//    $(".eb-select-data").hide();
//    $("ul.ul-listing-suggestion li.active").removeClass("active");
//}

//===================
// Auto select staff
//===================
function AutoSelectStaff() {
    $(".auto-select").bind("keyup", function (e) {
        var This = $(this);
        var StaffType = This.attr("data-field");
        var code = parseInt(This.val());
        if (!isNaN(code)) {
            $.ajax({
                type: "POST",
                url: "/GUI/SystemService/Ajax/Suggestion.aspx/Suggestion_OneStaff",
                //data: '{code : "' + code + '", StaffType : "' + StaffType + '"}',
                data: '{code : "' + code + '", SalonId : ' + $("#HDF_SalonId").val() + ', StaffType : "' + StaffType + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var mission = JSON.parse(response.d);
                    if (mission.success) {
                        var OBJ = JSON.parse(mission.msg);
                        if (OBJ.length > 0) {
                            var lis = "";
                            This.parent().find(".fake-value").text(This.val() + "- " + OBJ[0].Fullname);
                            $("#" + StaffType).val(OBJ[0].Id);
                        } else {
                            This.parent().find(".fake-value").text("");
                            $("#" + StaffType).val("");
                        }
                    } else {
                        This.parent().find("ul.ul-listing-suggestion").empty();
                        var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                        showMsgSystem(msg, "warning");
                    }
                },
                failure: function (response) { alert(response.d); }
            });
        } else {
            This.parent().find(".fake-value").text("");
            $("#" + StaffType).val("");
        }
    });
}
//function getRatingver1() {
//    $.ajax({
//        type: "POST",
//        url: "/GUI/SystemService/Webservice/wsRating.asmx/RequestRatingVer1",
//        data: "{BillId : " + $("#HDF_BillId").val() + "}",
//        contentType: "application/json; charset=utf-8",
//        dataType: "json", success: function (response) {
//            var mission = response.d;
//            if (mission.success) {
//                if (mission.data != "" && mission.data != "0") {
//                    $("#CkRating").prop('checked', true);
//                    $(".rating-icon-wrap1").text("Khách đã đánh giá");
//                    $(".rating-icon-wrap1").css("color", "red");
//                    $("#HDF_Rating").val(parseInt(mission.data));
//                }
//                else {
//                    $("#CkRating").prop('checked', false);
//                    $(".rating-icon-wrap1").text("Khách chưa đánh giá");
//                    $(".rating-icon-wrap1").css("color", "red");
//                    $("#HDF_Rating").val("");
//                }
//            }
//            else {
//                console.log("Get rating error.");
//            }
//            $("#not-pleasure-reason").hide();
//        },
//        failure: function (response) { alert(response.d); }
//    });
//}
//function getRatingver2() {
//    $.ajax({
//        type: "POST",
//        url: "/GUI/SystemService/Webservice/wsRating.asmx/RequestRatingVer2",
//        data: "{BillId : " + $("#HDF_BillId").val() + "}",
//        contentType: "application/json; charset=utf-8",
//        dataType: "json", success: function (response) {
//            var mission = response.d;
//            if (mission.success) {
//                $("#CkRating").prop('checked', true);
//                $(".rating-icon-wrap1").text("Khách đã đánh giá");
//                $(".rating-icon-wrap1").css("color", "red");
//                $("#HDF_Rating").val(parseInt(mission.data));
//            }
//            else {
//                $("#CkRating").prop('checked', false);
//                $(".rating-icon-wrap1").text("Khách chưa đánh giá");
//                $(".rating-icon-wrap1").css("color", "red");

//                $("#HDF_Rating").val("0");
//            }
//            $("#not-pleasure-reason").hide();
//        },
//        failure: function (response) { alert(response.d); }
//    });
//}

//function excRating(This, value) {
//    var salonId = parseInt($("#HDF_SalonId").val());
//    salonId = !isNaN(salonId) ? salonId : 0;
//    if (!isAccountAdmin) {
//        return;
//    }
//    else {
//        clearInterval(requestRatingver2);
//        $("#HDF_Rating").val(value);
//        This.parent().find(".rating-icon").removeClass("active");
//        This.addClass("active");
//    }
//}

//function bindRating(mark) {
//    switch (mark) {
//        case 1: $(".rating-icon").removeClass("active");
//            $(".rating-icon.icon-sad").addClass("active");
//            break;
//        case 2: $(".rating-icon").removeClass("active");
//            $(".rating-icon.icon-normal").addClass("active");
//            break;
//        case 3: $(".rating-icon").removeClass("active");
//            $(".rating-icon.icon-happy").addClass("active");
//            break;
//        default: break;
//    }
//}

////get rating number star
//function excRatingV2(This, starNumber) {
//    var salonId = parseInt($("#HDF_SalonId").val());
//    salonId = !isNaN(salonId) ? salonId : 0;
//    if (!isAccountAdmin) {
//        return;
//    }
//    else {
//        clearInterval(requestRatingver2);
//        $("#HDF_Rating").val(starNumber);
//        This.parent().find(".rating-icon").removeClass("active");
//        This.addClass("active");
//    }
//}

//get rating number star
//function bindRatingV2(starNumber) {
//    switch (starNumber) {
//        case 1:
//            radiobtn = document.getElementById("1star");
//            radiobtn.checked = true;
//            $(".rating-icon").css("color", "#ddd");
//            $("fieldset.rating").addClass("rating2");
//            break;
//        case 2:
//            radiobtn = document.getElementById("2star");
//            radiobtn.checked = true;
//            $(".rating-icon").css("color", "#ddd");
//            $("fieldset.rating").addClass("rating2");
//            break;
//        case 3:
//            radiobtn = document.getElementById("3star");
//            radiobtn.checked = true;
//            $(".rating-icon").css("color", "#ddd");
//            $("fieldset.rating").addClass("rating2");
//            break;
//        case 4:
//            radiobtn = document.getElementById("4star");
//            radiobtn.checked = true;
//            $(".rating-icon").css("color", "#ddd");
//            $("fieldset.rating").addClass("rating2");
//            break;
//        case 5:
//            radiobtn = document.getElementById("5star");
//            radiobtn.checked = true;
//            $(".rating-icon").css("color", "#ddd");
//            $("fieldset.rating").addClass("rating2");
//            break;
//        default: break;
//    }
//}

function showProductByCategory(This) {
    $("table#table-product tbody tr").each(function () {
        if ($(this).attr("data-cate") != This.val()) {
            $(this).hide();
        } else {
            $(this).show();
        }
    });
    if ($(".popup-product-item").isOpenEBPopup()) {
        $(".popup-product-item").alignment();
    }
}

//var templateVIPCard = 4;
// active input VIP card
//function activeVIPCard(This) {
//    var activeItem = $(".item-vipcard.active");
//    var thisValue = This.find("input[type='text']").val();
//    activeItem.find(".left i").removeClass("fa-check-square-o").addClass("fa-square-o");
//    activeItem.find("input[type=text]").val("").attr("disabled", "disabled");
//    activeItem.removeClass("active").removeClass("error");
//    This.addClass("active").find("input[type='text']").val(thisValue);
//    This.find(".left i").removeClass("fa-square-o").addClass("fa-check-square-o");
//    This.find("input[type='text']").removeAttr("disabled").focus();
//}

// validate input VIP card 
//function validateVIPCard(This, event) {
//    if (!checkTypingNumber(event)) {
//        This.parent().parent().addClass("error");
//        validate.vipcard = false;
//        event.preventDefault();
//    } else {
//        var cardValue = This.val();
//        if (!isAllowTyping(event) && cardValue.length >= templateVIPCard) {
//            This.parent().parent().addClass("error");
//            showMsgSystem("Lỗi. Thẻ VIP chỉ gồm tối đa 4 chữ số.", "warning");
//            event.preventDefault();
//        } else {
//            This.parent().parent().removeClass("error");
//            hideMsgSystem();
//            validate.vipcard = true;
//        }
//    }
//}

// validate VIP card value
//function validateVIPCardValue(This, event) {
//    var cardValue = This.val();
//    if (cardValue != '') {
//        if (cardValue.length > templateVIPCard) {
//            This.parent().parent().addClass("error");
//            validate.vipcard = false;
//            showMsgSystem("Lỗi. Thẻ VIP chỉ gồm tối đa 4 chữ số.", "warning");
//        } else {
//            if (!checkNumber(cardValue)) {
//                This.parent().parent().addClass("error");
//                validate.vipcard = false;
//            } else if (This.attr("id") == "VIPGive") {
//                checkIssetVIPCard(This, cardValue);
//            }
//        }
//    } else {
//        This.parent().parent().removeClass("error");
//        hideMsgSystem();
//        validate.vipcard = true;
//    }
//}

//function inputVIPcardBlur(This) {
//    var cardValue = This.val();
//    if (cardValue == "") {
//        This.parent().parent().removeClass("error");
//        hideMsgSystem();
//        validate.vipcard = true;
//    }
//}

//function checkIssetVIPCard(This, card) {
//    $.ajax({
//        type: "POST",
//        url: "/GUI/FrontEnd/Service/Service_Pending_Complete.aspx/issetVIPCard",
//        data: "{input : '" + card + "'}",
//        contentType: "application/json; charset=utf-8",
//        dataType: "json", success: function (response) {
//            var mission = JSON.parse(response.d);
//            if (mission.success) {
//                This.parent().parent().addClass("error");
//                validate.vipcard = false;
//                showMsgSystem("Lỗi. Mã thẻ VIP đã được tặng.", "warning");
//            } else {
//                This.parent().parent().removeClass("error");
//                hideMsgSystem();
//                validate.vipcard = true;
//            }
//        },
//        failure: function (response) { alert(response.d); }
//    });
//}

//Them san pham tu dropdown
function ClickAddProduct(This) {
    var index = 1;
    $('#tbodyProduct').find("tr").each(function () {
        $('#tbodyProduct tr').find("td.td-product-index").text(index);
        index++;
    });
    //thêm mapid
    var IdProduct = $("#ddlProduct").find(':selected').data('id');
    var Code = $("#ddlProduct").find(':selected').data('code');
    var MapId = $("#ddlProduct").find(':selected').data('mapid');
    var PriceProduct = $("#ddlProduct").find(':selected').data('price');
    var Cate = $("#ddlProduct").find(':selected').data('cate');
    var NameProduct = $("#ddlProduct").find(':selected').data('name');
    var DiscountCosmetic = $("#ddlProduct").find(":selected").data("discount");
    var Quantity = 1;
    var Price = (FormatPrice(Quantity * PriceProduct * (100 - DiscountCosmetic) / 100));

    if (IdProduct != 0) {
        //thêm td mapid
        $('#tbodyProduct').append('<tr data-cate="' + Cate + '"><td class="td-product-index"></td><td class="td-product-name">' + NameProduct + '</td><td class="td-product-code" data-id="' + IdProduct + '" data-code="' + Code + '">' + Code + '</td><td class="td-product-map" style="display:none;" data-mapid="' + MapId + '">' + MapId + '</td><td class="td-product-price" data-price="' + PriceProduct + '">' + PriceProduct + '</td><td class="td-product-quantity"><input type="text" class="product-quantity" value="' + Quantity + '"></td><td class="td-product-voucher" data-voucher="' + DiscountCosmetic + '"><div class="row"><input type="text" class="product-voucher voucher-cosmetic" value="' + DiscountCosmetic + '" style="margin: 0 auto; float: none; text-align: center; width: 50px;">%</div></td><td class="map-edit"><div class="box-money" style="display: block;">' + FormatPrice(Price) + '</div><div class="edit-wp"><a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),\'' + NameProduct + '\',\'product\')" href="javascript://" title="Xóa"></a></div></td></tr>');
        $(".item-product").show();
        TotalMoney();
        getProductIds();
        getServiceIds();
        ExcQuantity();
        UpdateItemOrder($("#table-item-product").find("tbody"));
        ResetDropdownProduct();
    }
    else {
        alert("Bạn chưa chọn sản phẩm !");
    }

}

// ResetDropdownProduct
//function ResetDropdownProduct() {
//    $("#ddlProduct").val(0);
//    $("#select2-ddlProduct-container").attr('title', 'Chọn sản phẩm ...');
//    $("#select2-ddlProduct-container").text("Chọn sản phẩm ...");
//}

// Them san pham khi them bang checkbox
function pushQuickproductData(This) {
    var index = 1;
    var DiscountCosmetic;
    var IdProduct = This.data('id');
    var Code = This.data('code');
    //Thêm mapid
    var MapId = This.data('mapid');
    var PriceProduct = This.data('price');
    var Cate = This.data('cate');
    var NameProduct = This.data('name');

    if (disCosmetic !== null && disCosmetic !== undefined && disCosmetic !== '' && parseInt(disCosmetic) > 0) {
        DiscountCosmetic = disCosmetic.trim() !== "" ? parseInt(disCosmetic) : 0;
    }
    else {
        DiscountCosmetic = This.data('discount') !== "" ? This.data('discount') : 0;
    }
    var Quantity = 1;
    var Price = (FormatPrice(Quantity * PriceProduct * (100 - DiscountCosmetic) / 100));

    if (This.is(":checked")) {
        //thêm td mapid
        $('#tbodyProduct').append('<tr data-cate="' + Cate + '"><td class="td-product-index"></td><td class="td-product-name">' + NameProduct + '</td><td class="td-product-code" data-id="' + IdProduct + '" data-code="' + Code + '">' + Code + '</td><td class="td-product-map" style="display:none;" data-mapid="' + MapId + '">' + MapId + '</td><td class="td-product-price" data-price="' + PriceProduct + '">' + PriceProduct + '</td><td class="td-product-quantity"><input type="text" class="product-quantity" value="' + Quantity + '"></td><td class="td-product-voucher" data-voucher="' + DiscountCosmetic + '"><div class="row"><input type="text" class="product-voucher voucher-cosmetic" value="' + DiscountCosmetic + '" style="margin: 0 auto; float: none; text-align: center; width: 50px;">%</div></td><td class="map-edit"><div class="box-money" style="display: block;">' + FormatPrice(Price) + '</div><div class="edit-wp"><a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),\'' + NameProduct + '\',\'product\')" href="javascript://" title="Xóa"></a></div></td></tr>');
        $(".item-product").show();

    } else {
        $("#table-item-product").find(".td-product-code[data-code='" + Code + "']").parent().remove();
    }
    TotalMoney();
    getProductIds();
    getServiceIds();
    ExcQuantity();
    UpdateItemOrder($("#table-item-product").find("tbody"));
}
