﻿var validate = { vipcard: true };
var listIds = [];
var existStylist = false;
var recordIds = {};
//var requestRatingver1;
var requestRatingver2;
function replaceSpecialChar(This) {
    return This.val(This.val().replace(/[^a-zA-Z0-9]/g, ''));
}

jQuery(document).ready(function () {
    $('.checkbox-membership').click(function () {
        $('.checkbox-membership').not(this).prop('checked', false);
        //tinh lai phi
        tr = $(this).parent().parent();
        calPriceMembership(tr.find(".member-quantity"));

        money = tr.find(".member-money");
        $('.member-money').not(money).text('');
    });

    $("#InputStylist").keypress(function () {
        replaceSpecialChar($("#InputStylist"));
    });
    $("#InputStylist").blur(function () {
        replaceSpecialChar($("#InputStylist"));
    });
    $("#InputSkinner").keypress(function () {
        replaceSpecialChar($("#InputSkinner"));
    });
    $("#InputSkinner").blur(function () {
        replaceSpecialChar($("#InputSkinner"));
    });
    $("#InputReception").keypress(function () {
        replaceSpecialChar($("#InputReception"));
    });
    $("#InputReception").blur(function () {
        replaceSpecialChar($("#InputReception"));
    });
    $("#InputCheckout").keypress(function () {
        replaceSpecialChar($("#InputCheckout"));
    });
    $("#InputCheckout").blur(function () {
        replaceSpecialChar($("#InputCheckout"));
    });
    $("#InputSecurity").keypress(function () {
        replaceSpecialChar($("#InputSecurity"));
    });
    $("#InputSecurity").blur(function () {
        replaceSpecialChar($("#InputSecurity"));
    });

    getRatingver2();
    // Add active menu
    $("#glbService").addClass("active");
    $("#subMenu .li-pending-complete").addClass("active");

    // Mở box listing sản phẩm
    $(".show-product").bind("click", function () {
        $(this).parent().parent().find(".listing-product").show();
        $("select#ddlProductCategory").val($("select#ddlProductCategory option:first-child").val());
        showProductByCategory($("select#ddlProductCategory"));
    });

    $(".show-item").bind("click", function () {
        var item = $(this).attr("data-item");
        var classItem = ".popup-" + item + "-item";

        $(classItem).openEBPopup();
        ExcCheckboxItem();
        ExcQuantity();
        ExcCompletePopup();

        $('#EBPopup .listing-product').mCustomScrollbar({
            theme: "dark-2",
            scrollInertia: 100
        });

        $("#EBPopup .btn-esc").bind("click", function () { autoCloseEBPopup(0); });
    });

    // Init execute service, product quantity
    TotalMoney();
    getProductIds();
    getServiceIds();
    ExcQuantity();

    // Auto select staff
    AutoSelectStaff();

    /// Bind gói phụ trợ
    pushFreeService();
    /// Get Rating
    requestRatingver2 = setInterval(getRatingver2, 3000);

    excThumbWidth();

    $(window).load(function () {
        $('.listing-img-upload .thumb-wp').each(function () {
            var THIS = $(this);

            $("<img>").attr("src", $(".thumb").attr("src")).load(function () {
                var ImgW = this.width;
                var ImgH = this.height;
                console.log(ImgW);
                if ((ImgW > ImgH)) {
                    (THIS).find('img').addClass('degree90');
                }
            });
        });
    });

    // Enter don't submit
    $(window).on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });
});

function excThumbWidth() {
    var ImgStandardWidth = 120,
        ImgStandardHeight = 120;
    var width = ImgStandardWidth,
        height, left, top;
    $(".thumb-wp .thumb").each(function () {
        height = ImgStandardWidth * $(this).height() / $(this).width();
        left = 0;
        top = (ImgStandardHeight - height) / 2;
        $(this).css({ "width": width, "height": height, "left": left, "top": top });
    });
}

// Xử lý mã khách hàng => bind khách hàng
function LoadCustomer(code) {
    ajaxGetCustomer(code);
}

// Xử lý khi chọn checkbox sản phẩm
function ExcCheckboxItem() {
    $("#EBPopup .td-product-checkbox input[type='checkbox']").unbind("click").bind("click", function () {
        var obj = $(this).parent().parent(),
            boxMoney = obj.find(".box-money"),
            price = obj.find(".td-product-price").data("price"),
            quantity = obj.find("input.product-quantity").val(),
            voucher = obj.find("input.product-voucher").val(),
            checked = $(this).is(":checked"),
            item = obj.attr("data-item"),
            promotion = 0;
        obj.find(".promotion-money input[type='checkbox']:checked").each(function () {
            var value = $(this).attr("data-value").trim();
            promotion += (value !== "" ? parseInt(value) : 0);
        });

        // check value
        price = price.toString().trim() !== "" ? parseInt(price) : 0;
        quantity = quantity.trim() !== "" ? parseInt(quantity) : 0;
        voucher = voucher.trim() !== "" ? parseInt(voucher) : 0;


        if (checked) {
            boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100 - promotion)).show();
        } else {
            boxMoney.text("").hide();
        }

    });
}

// Số lượng | Quantity
function ExcQuantity() {
    $("input.product-quantity").bind("change", function () {
        var obj = $(this).parent().parent(),
            quantity = $(this).val(),
            price = obj.find(".td-product-price").data("price"),
            voucher = obj.find("input.product-voucher").val(),
            boxMoney = obj.find(".box-money"),
            promotion = 0;

        obj.find(".promotion-money input[type='checkbox']:checked").each(function () {
            var value = $(this).attr("data-value").trim();
            promotion += value !== "" ? parseInt(value) : 0;
        });

        // check value
        price = price.toString().trim() !== "" ? parseInt(price) : 0;
        quantity = quantity.trim() !== "" ? parseInt(quantity) : 0;
        voucher = voucher.trim() !== "" ? parseInt(voucher) : 0;

        boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100 - promotion)).show();
        TotalMoney();
        getProductIds();
        getServiceIds();
    });

    $("input.product-voucher").bind("change", function () {
        var obj = $(this).parent().parent().parent(),
            quantity = obj.find("input.product-quantity").val(),
            //price = obj.find(".td-product-price").data("price"),
            price = obj.find(".td-product-price").text(),
            voucher = obj.find("input.product-voucher").val(),
            boxMoney = obj.find(".box-money"),
            promotion = 0;
        obj.find(".promotion-money input[type='checkbox']:checked").each(function () {
            var value = $(this).attr("data-value").trim();
            promotion += value !== "" ? parseInt(value) : 0;
        });

        // check value
        price = price.toString().trim() !== "" ? parseInt(price) : 0;
        quantity = quantity.trim() !== "" ? parseInt(quantity) : 0;
        voucher = voucher.trim() !== "" ? parseInt(voucher) : 0;
        if (voucher > 100) {
            voucher = 100;
        }

        console.log(voucher);

        if (promotion === 0) {
            boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100)).show();
        } else {
            obj.find("input.product-voucher").val(0);
        }

        TotalMoney();
        getProductIds();
        getServiceIds();
    });
    $("input.item-promotion").bind("click", function () {
        var obj = $(this).parent().parent().parent().parent().parent(),
            quantity = obj.find("input.product-quantity").val(),
            price = obj.find(".td-product-price").data("price"),
            voucher = obj.find("input.product-voucher").val(),
            boxMoney = obj.find(".box-money"),
            promotion = $(this).attr("data-value"),
            _This = $(this),
            isChecked = $(this).is(":checked");
        obj.find(".promotion-money input[type='checkbox']:checked").prop("checked", false);
        if (isChecked) {
            $(this).prop("checked", true);
            promotion = promotion.trim() !== "" ? parseInt(promotion) : 0;
            obj.find("input.product-voucher").val(0);
        } else {
            promotion = 0;
        }
        // check value
        price = price.toString().trim() !== "" ? parseInt(price) : 0;
        quantity = quantity.trim() !== "" ? parseInt(quantity) : 0;
        voucher = voucher.trim() !== "" ? parseInt(voucher) : 0;
        if (promotion > 0) {
            boxMoney.text(FormatPrice(quantity * price - promotion)).show();
            obj.find("input.product-voucher").css({ "text-decoration": "line-through" });
        } else {
            boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100)).show();
            obj.find("input.product-voucher").css({ "text-decoration": "none" });
        }
        TotalMoney();
        getProductIds();
        getServiceIds();
    });
}

// Xử lý click hoàn tất chọn item từ popup
function ExcCompletePopup() {
    $("#EBPopup .btn-complete").unbind("click").bind("click", function () {
        var item = $(this).attr("data-item"),
            tableItem = $("table.table-item-" + item + " tbody"),
            objTmp;

        $("#EBPopup .item-product-checkbox input[type='checkbox']:checked").each(function () {
            var Code = $(this).attr("data-code");
            if (!ExcCheckItemIsChose(Code, tableItem)) {
                objTmp = $(this).parent().parent().clone().find("td:first-child").remove().end();
                tableItem.append(objTmp);
            }
        });

        TotalMoney();
        getProductIds();
        getServiceIds();
        ExcQuantity();
        UpdateItemOrder(tableItem);
        autoCloseEBPopup(0);
    });
}

// Check item đã được chọn
function ExcCheckItemIsChose(Code, itemClass) {
    var result = false;
    $(itemClass).find(".td-product-code").each(function () {
        var _Code = $(this).text().trim();
        if (_Code === Code)
            result = true;
    });
    return result;
}

// Remove item đã được chọn
function RemoveItem(THIS, name, itemName) {
    if (itemName === 'service') {
        existStylist = false;
        // Confirm
        $("table.table-item-service tbody tr").each(function () {
            recordIds = {};
            var THIS = $(this);
            var Id = THIS.find("td.td-product-code").attr("data-id");
            // check value
            Id = Id.toString().trim() !== "" ? parseInt(Id) : 0;
            listIds.push(recordIds);
        });
        $(".confirm-yn").openEBPopup();
        if (listIds.length === 1) {
            $("#EBPopup .confirm-yn-text").text("Bạn có muốn xoá luôn cả stylist không?");
        }
        else {
            $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn hủy [ " + name + " ] ?");
        }
        $("#EBPopup .yn-yes").bind("click", function () {
            var Code = THIS.find(".td-product-code").text();
            //Su dung cho ham CalCampaignService();
            var CodeV1 = THIS.find(".td-product-code");
            $("#quick-" + itemName).find("input[data-code='" + Code + "']").removeAttr("checked");
            THIS.remove();
            CalCampaignService($("#quick-" + itemName).find("input[data-code='" + Code + "']"));
            TotalMoney();
            getProductIds();
            getServiceIds();
            UpdateItemDisplay(itemName);
            autoCloseEBPopup(0);
            listIds = [];
            $("#txtReceiveMoney").val("");
            $("#txtReturnMoney").val(0);
        });
        $("#EBPopup .yn-no").bind("click", function () {
            if (listIds.length === 1) {
                existStylist = true;
                var Code = THIS.find(".td-product-code").text();
                //Su dung cho ham CalCampaignService();
                var CodeV1 = THIS.find(".td-product-code");
                $("#quick-" + itemName).find("input[data-code='" + Code + "']").removeAttr("checked");
                THIS.remove();
                CalCampaignService($("#quick-" + itemName).find("input[data-code='" + Code + "']"));
                TotalMoney();
                getServiceIds();
                UpdateItemDisplay(itemName);
                autoCloseEBPopup(0);
                listIds = [];
            }
            else {
                autoCloseEBPopup(0);
            }
        });
    }
    else {
        var option = $("#tbodyMembershipProduct").find("td[data-code='" + THIS.find(".td-product-code").text() + "']");
        var category = option.data("cate");
        if (category == 98 && $("#Pending").val() == "1") {
            $("#NotRemoveproductModal").modal();
            return;
        }

        $(".confirm-yn").openEBPopup();
        $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn hủy [ " + name + " ] ?");
        $("#EBPopup .yn-yes").bind("click", function () {
            //cap nhat lai gia neu huy membership
            if (category == 98) {
                CancelPriceMembership();
            }
            //
            var Code = THIS.find(".td-product-code").attr("data-code");
            //Su dung cho ham CalCampaignService();
            var CodeV1 = THIS.find(".td-product-code");
            $("#quick-" + itemName).find("input[data-code='" + Code + "']").removeAttr("checked");
            THIS.remove();
            CalCampaignService(CodeV1);
            TotalMoney();
            getProductIds();
            getServiceIds();
            UpdateItemDisplay(itemName);
            autoCloseEBPopup(0);
        });
        $("#EBPopup .yn-no").bind("click", function () {
            autoCloseEBPopup(0);
        });
    }
}

// Remove item (đã lưu trong phiếu pending) được chọn
function RemoveItemPending(THIS, itemName, itemId, itemType) {
    // Confirm
    $(".confirm-yn").openEBPopup();

    $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn hủy [ " + itemName + " ] ?");
    $("#EBPopup .yn-yes").bind("click", function () {

        THIS.remove();
        TotalMoney();
        getProductIds();
        getServiceIds();
        UpdateItemDisplay(itemType);
        autoCloseEBPopup(0);
        $.ajax({
            type: "POST",
            url: "/GUI/SystemService/Ajax/Del.aspx/Delele_Item_Pending",
            data: '{itemId : ' + itemId + ', BillId : ' + $("#HDF_BillId").val() + ', itemType : "' + itemType + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                var mission = JSON.parse(response.d);
                if (mission.success) {
                    delSuccess();
                    Row.remove();
                } else {
                    delFailed();
                }
            },
            failure: function (response) { alert(response.d); }
        });
    });
    $("#EBPopup .yn-no").bind("click", function () {
        autoCloseEBPopup(0);
    });
}

// Update table display
function UpdateItemDisplay(itemName) {
    var dom = $(".table-item-" + itemName);
    var len = dom.find("tbody tr").length;
    if (len === 0) {
        dom.parent().hide();
    } else {
        UpdateItemOrder(dom.find("tbody"));
    }
}

// Update order item
function UpdateItemOrder(dom) {
    var index = 1;
    dom.find("tr").each(function () {
        $(this).find("td.td-product-index").text(index);
        index++;
    });
}

function TotalMoney() {
    var Money = 0;
    $(".fe-service-table-add .box-money:visible").each(function () {
        Money += parseInt($(this).text().replace(/\./gm, ""));
    });
    $("#TotalMoney").val(FormatPrice(Money));
    $("#HDF_TotalMoney").val(Money);
    //tinh tien tra truoc prepaid, giam tru deductions
    var totalMoneyPrepaid = 0;
    var totalMoneyDeductions = 0;

    $("table.table-item-service tbody tr").each(function () {
        //debugger
        var THIS = $(this).find(".td-product-price");

        var moneyPrepaid = THIS.attr("data-money-prepaid");
        moneyPrepaid = moneyPrepaid.toString().trim() !== "" ? parseInt(moneyPrepaid) : 0;
        totalMoneyPrepaid += moneyPrepaid;
        //
        totalMoneyDeductions += parseInt(THIS.attr("data-money-deductions").replace(/\./gm, ""));

    });
    //debugger;
    //
    $("#MoneyPrepaid").val(FormatPrice(totalMoneyPrepaid));
    $("#MoneyDeductions").val(FormatPrice(totalMoneyDeductions));
    //
    $("#HDF_TotalMoneyPrepaid").val(totalMoneyPrepaid);
    $("#HDF_TotalMoneyDeductions").val(totalMoneyDeductions);
    //
    var totalMoneyInBill = Money + totalMoneyPrepaid - totalMoneyDeductions;
    $("#MoneyInBill").val(FormatPrice(totalMoneyInBill));
    $("#HDF_TotalMoneyInBill").val(totalMoneyInBill);
}

function showMsgSystem(msg, status) {
    $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 0);
    $("#MsgSystem2").text("").attr("class", "msg-system").css("opacity", 0);
    $("#MsgSystem").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);
    $("#MsgSystem2").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);

    setTimeout(function () {
        $("#MsgSystem").fadeTo("slow", 0, function () {
            $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 1);
        });
    }, 20000);
    setTimeout(function () {
        $("#MsgSystem2").fadeTo("slow", 0, function () {
            $("#MsgSystem2").text("").attr("class", "msg-system").css("opacity", 1);
        });
    }, 20000);
}

function hideMsgSystem() {
    $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 0);
}

function getProductIds() {
    var Ids = [];
    var prd = {};
    $("table.table-item-product tbody tr").each(function () {
        prd = {};
        var THIS = $(this);
        var Id = THIS.find("td.td-product-code").attr("data-id"),
            Code = THIS.find("td.td-product-code").text().trim(),
            MapId = THIS.find("td.td-product-map").attr("data-mapid"),
            Name = THIS.find("td.td-product-name").text().trim(),
            Price = THIS.find(".td-product-price").attr("data-price"),
            Quantity = THIS.find("input.product-quantity").val(),
            VoucherPercent = THIS.find("input.product-voucher").val(),
            SellerId = THIS.find('#input-seller' + Id).val(),
            Promotion = 0;
        THIS.find(".promotion-money input[type='checkbox']:checked").each(function () {
            var value = $(this).attr("data-value").trim();
            Promotion += value !== "" ? parseInt(value) : 0;
        });

        // check value
        Id = Id.toString().trim() !== "" ? parseInt(Id) : 0;
        if (SellerId !== undefined) {
            SellerId = SellerId.toString().trim() !== "" ? parseInt(SellerId) : 0;
        }
        else {
            SellerId = 0;
        }
        Price = Price.toString().trim() !== "" ? parseInt(Price) : 0;
        // check Quantity
        if (CheckParameter(Quantity)) {
            Quantity = parseInt(Quantity);
        }
        else {
            Quantity = 0;
        }
        // check voucherpercent
        if (CheckParameter(VoucherPercent)) {
            VoucherPercent = parseInt(VoucherPercent);
        }
        else {
            VoucherPercent = 0;
        }
        prd.Id = Id;
        prd.SellerId = SellerId;
        prd.Code = Code;
        prd.MapIdProduct = MapId;
        prd.Name = Name;
        prd.Price = Price;
        prd.Quantity = Quantity;
        prd.VoucherPercent = VoucherPercent;
        prd.Promotion = Promotion;
        Ids.push(prd);

    });
    Ids = JSON.stringify(Ids);
    $("#HDF_ProductIds").val(Ids);
    $("#txtReceiveMoney").val("");
    $("#txtReturnMoney").val(0);
}
// add 20190522
let IdsService = [];
function getServiceIds() {
    var Ids = [];
    var prd = {};
    IdsService = [];
    $("table.table-item-service tbody tr").each(function () {

        prd = {};
        var THIS = $(this),
            obj = THIS.parent().parent();

        var Id = THIS.find("td.td-product-code").attr("data-id"),
            Code = THIS.find("td.td-product-code").text().trim(),
            Name = THIS.find("td.td-product-name").text().trim(),
            Price = THIS.find(".td-product-price").attr("data-price"),
            Quantity = THIS.find("input.product-quantity").val(),
            VoucherPercent = THIS.find("input.product-voucher").val();
        THIS.find(".promotion-money input[type='checkbox']:checked").each(function () {
            var value = $(this).attr("data-value").trim();
            Promotion += (value !== "" ? parseInt(value) : 0);
        });
        // check value
        Id = Id.toString().trim() !== "" ? parseInt(Id) : 0;
        Price = Price.toString().trim() !== "" ? parseInt(Price) : 0;
        // check quantity
        if (CheckParameter(Quantity)) {
            Quantity = parseInt(Quantity);
        }
        else {
            Quantity = 0;
        }
        // add 20190522
        if (Id === 69) {
            THIS.find("input.product-quantity").attr("disabled", "disabled");
            THIS.find("input.product-quantity").val(1);
        }
        if (Id === 105) {
            THIS.find("input.product-quantity").attr("disabled", "disabled");
            THIS.find("input.product-quantity").val(1);
        }
        // check voucherpercent
        if (CheckParameter(VoucherPercent)) {
            VoucherPercent = parseInt(VoucherPercent);
        }
        else {
            VoucherPercent = 0;
        }
        prd.Id = Id;
        prd.Code = Code;
        prd.Name = Name;
        prd.Price = Price;
        prd.Quantity = Quantity;
        prd.VoucherPercent = VoucherPercent;

        Ids.push(prd);
        IdsService.push(prd.Code);
    });
    Ids = JSON.stringify(Ids);
    if (Ids === "[]") {
        if (!existStylist) {
            let $value = $("#InputStylist").val();
            if ($value !== '') {
                document.getElementById("InputStylist").setAttribute('value', '');
                document.getElementById("Hairdresser").setAttribute('value', '');
                $('#FVStylist').html('');
            }
        }
    }
    $("#HDF_ServiceIds").val(Ids);
    $("#txtReceiveMoney").val("");
    $("#txtReturnMoney").val(0);
}

// Get Customer
function ajaxGetCustomer(CustomerCode) {
    $.ajax({
        type: "POST",
        url: "/GUI/FrontEnd/Service/Service_Add.aspx/GetCustomer",
        data: '{CustomerCode : "' + CustomerCode + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json", success: function (response) {
            var mission = JSON.parse(response.d);
            if (mission.success) {
                var customer = JSON.parse(mission.msg);
                customer = customer[0];
                $("#CustomerName").val(customer.Fullname);
                $("#CustomerName").attr("data-code", customer.Customer_Code);
                $("#HDF_CustomerCode").val(customer.Customer_Code);

            } else {
                var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                showMsgSystem(msg, "warning");
            }
        },
        failure: function (response) { alert(response.d); }
    });
}

function pushQuickData(This, typeData, cusType, disService) {

    var Code = This.attr("data-code");
    // add 20190522
    if (IdsService.indexOf("SP00088") !== -1) {
        if (Code === "SP00052") {
            var msg = "Mỗi bill sẽ chỉ được nhập 01 Dưỡng Protein 99k hoặc 01 Box Nhuộm, không nhập đồng thời cả 2!";
            showMsgSystem(msg, "warning");
            $("#quick-service").find("input[data-code='SP00052']").removeAttr("checked");
            return;
        }
    }
    if (IdsService.indexOf("SP00052") !== -1) {
        if (Code === "SP00088") {
            var box = "Mỗi bill sẽ chỉ được nhập 01 Dưỡng Protein 99k hoặc 01 Box Nhuộm, không nhập đồng thời cả 2!";
            showMsgSystem(box, "warning");
            $("#quick-service").find("input[data-code='SP00088']").removeAttr("checked");
            return;
        }

    }
    if (This.is(":checked")) {
        var td = $("#table-" + typeData).find("input[data-code='" + Code + "']").parent();
        var Dom = td.parent().clone().find("td:first-child").remove().end(),
            quantity = Dom.find(".product-quantity").val(),
            voucher = Dom.find("input.product-voucher").val();
        if (cusType == "1" || cusType == "2") {
            Dom.find("input.product-voucher").val(disService);
            voucher = disService;
        }
        var price = "0";
        //neu la membership
        if ($("#BarcodeStatus").val() == "bookingMember") {
            var option = $("#memberProduct").find("option[data-key='" + $("#MemberProductId").val() + '-1-' + Dom.find(".td-product-code").data("id") + "']");
            if (option != null) {
                price = option.data("price");
            }
            if (price == null) {
                price = Dom.find(".td-product-price").data("price");
            }
            price = (price == "" ? "0" : price);
            Dom.find(".td-product-price").text(price);
            Dom.find(".td-product-price").attr("data-price", price);
        } else {
            price = Dom.find(".td-product-price").data("price");
        }
        // check value     

        price = price.toString().trim() != "" ? parseInt(price) : 0;
        quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
        voucher = voucher.trim() != "" ? parseInt(voucher) : 0;
        //
        Dom.find(".td-product-quantity").html('<input class="product-quantity" maxlength="2" onkeypress="return ValidateKeypress(/\\d/,event);" type="text" value="' + quantity + '"> </input>');
        //
        Dom.find(".td-product-voucher").html('<div class="row"><input type="text" onblur="ValidatePercent($(this));" onkeypress="return ValidateKeypress(/\\d/,event);" maxlength="3" class="product-voucher voucher-services" value="' + voucher
            + '" style="margin: 0 auto; float: none; text-align: center; width: 50px; " />%</div >');
        Dom.find(".box-money").text(FormatPrice(quantity * price * (100 - voucher) / 100)).show();
        console.log(Dom);
        $("#table-item-" + typeData).append(Dom);
        $(".item-" + typeData).show();
        $("#txtReceiveMoney").val("");
        $("#txtReturnMoney").val(0);

    } else {
        $("#table-item-" + typeData).find(".td-product-code[data-code='" + Code + "']").parent().remove();
    }

    TotalMoney();
    getProductIds();
    getServiceIds();
    ExcQuantity();
    UpdateItemOrder($("#table-item-" + typeData).find("tbody"));
}

function pushFreeService(This, id) {
    var arr = [];
    var sv = {};
    $(".free-service input[type='checkbox']").each(function () {
        if ($(this).is(":checked")) {
            arr.push(parseInt($(this).attr('data-id')));
        }
    });
    $("#HDF_FreeService").val(JSON.stringify(arr));
}

//============================
// Suggestion Functions
//============================
function Bind_Suggestion() {
    $(".eb-suggestion").bind("keyup", function (e) {
        if (e.keyCode === 40) {
            UpDownListSuggest($(this));
        } else {
            Call_Suggestion($(this));
        }
    });
    $(".eb-suggestion").bind("focus", function () {
        Call_Suggestion($(this));
    });
    $(".eb-suggestion").bind("blur", function () {
        //Exc_To_Reset_Suggestion($(this));
    });
    $(window).bind("click", function (e) {
        if ((!e.target.className.match("eb-select") && e.target.parentElement.className.match("ul-listing-suggestion")) ||
            (!e.target.className.match("eb-select") && !e.target.parentElement.className.match("ul-listing-suggestion"))) {
            EBSelect_HideBox();
        }
    });
}

function UpDownListSuggest(This) {
    var UlSgt = This.parent().find(".ul-listing-suggestion"),
        index = 0,
        LisLen = UlSgt.find(">li").length - 1,
        Value;

    This.blur();
    UlSgt.find(">li.active").removeClass("active");
    UlSgt.find(">li:eq(" + index + ")").addClass("active");

    $(window).unbind("keydown").bind("keydown", function (e) {
        if (e.keyCode === 40) {
            if (index === LisLen) return false;
            UlSgt.find(">li.active").removeClass("active");
            UlSgt.find(">li:eq(" + (++index) + ")").addClass("active");
            return false;
        } else if (e.keyCode === 38) {
            if (index === 0) return false;
            UlSgt.find(">li.active").removeClass("active");
            UlSgt.find(">li:eq(" + (--index) + ")").addClass("active");
            return false;
        } else if (e.keyCode === 13) {
            // Bind data to HDF Field
            var THIS = UlSgt.find(">li.active");
            //var Value = THIS.text().trim();
            var Value = THIS.attr("data-code");
            var dataField = This.attr("data-field");

            BindIdToHDF(THIS, Value, dataField, "#HDF_Suggestion_Code", "#HDF_Suggestion_Field", This);
            EBSelect_HideBox();
        }
    });
}

function Exc_To_Reset_Suggestion(This) {
    var value = This.val();
    if (value === "") {
        $(".eb-suggestion").each(function () {
            var THIS = $(this);
            var sgValue = THIS.val();
            if (sgValue !== "") {
                BindIdToHDF(THIS, sgValue, THIS.attr("data-field"), "#HDF_Suggestion_Code", "#HDF_Suggestion_Field", THIS);
                return false;
            }
        });
    }
}

function Call_Suggestion(This) {
    var text = This.val(),
        field = This.attr("data-field");
    Suggestion(This, text, field);
}

function Suggestion(This, text, field) {
    var This = This;
    var text = text || "";
    var field = field || "";
    var InputDomId;
    var HDF_Sgt_Code = "#HDF_Suggestion_Code";
    var HDF_Sgt_Field = "#HDF_Suggestion_Field";

    if (text === "") return false;

    switch (field) {
        case "customer.name": InputDomId = "#CustomerName"; break;
        case "customer.phone": InputDomId = "#CustomerPhone"; break;
        case "customer.code": InputDomId = "#CustomerCode"; break;
        case "bill.code": InputDomId = "#BillCode"; break;
    }

    $.ajax({
        type: "POST",
        url: "/GUI/SystemService/Ajax/Suggestion.aspx/Suggestion_Staff",
        data: '{field : "' + field + '", text : "' + text + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json", success: function (response) {
            var mission = JSON.parse(response.d);
            if (mission.success) {
                var OBJ = JSON.parse(mission.msg);
                if (OBJ.length > 0) {
                    var lis = "";
                    $.each(OBJ, function (i, v) {
                        lis += "<li data-code='" + v.Customer_Code + "'" +
                            "onclick=\"BindIdToHDF($(this),'" + v.Customer_Code + "','" + field + "','" + HDF_Sgt_Code +
                            "','" + HDF_Sgt_Field + "','" + InputDomId + "')\">" +
                            v.Value +
                            "</li>";
                    });
                    This.parent().find("ul.ul-listing-suggestion").empty().append(lis).parent().show();
                    This.parent().find(".eb-select-data").show();
                } else {
                    This.parent().find(".eb-select-data").hide().find("ul.ul-listing-suggestion").empty();
                }
            } else {
                This.parent().find("ul.ul-listing-suggestion").empty();
                var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                showMsgSystem(msg, "warning");
            }
        },
        failure: function (response) { alert(response.d); }
    });
}

function BindIdToHDF(THIS, Code, Field, HDF_Sgt_Code, HDF_Sgt_Field, Input_DomId) {
    var text = THIS.text().trim();
    $("input.eb-suggestion").val("");
    $(HDF_Sgt_Code).val(Code);
    $(HDF_Sgt_Field).val(Field);
    $(Input_DomId).val(text);
    $(Input_DomId).parent().find(".eb-select-data").hide();

    // Auto post server
    $("#BtnFakeUP").click();
}

function EBSelect_HideBox() {
    $(".eb-select-data").hide();
    $("ul.ul-listing-suggestion li.active").removeClass("active");
}

//===================
// Auto select staff
//===================
function AutoSelectStaff() {
    $(".auto-select").bind("keyup", function (e) {
        var This = $(this);
        var StaffType = This.attr("data-field");
        var code = parseInt(This.val());
        if (!isNaN(code)) {
            $.ajax({
                type: "POST",
                url: "/GUI/SystemService/Ajax/Suggestion.aspx/Suggestion_OneStaff",
                //data: '{code : "' + code + '", StaffType : "' + StaffType + '"}',
                data: '{code : "' + code + '", SalonId : ' + $("#HDF_SalonId").val() + ', StaffType : "' + StaffType + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var mission = JSON.parse(response.d);
                    if (mission.success) {
                        var OBJ = JSON.parse(mission.msg);
                        if (OBJ.length > 0) {
                            var lis = "";
                            This.parent().find(".fake-value").text(This.val() + "- " + OBJ[0].Fullname);
                            $("#" + StaffType).val(OBJ[0].Id);
                        } else {
                            This.parent().find(".fake-value").text("");
                            $("#" + StaffType).val("");
                        }
                    } else {
                        This.parent().find("ul.ul-listing-suggestion").empty();
                        var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                        showMsgSystem(msg, "warning");
                    }
                },
                failure: function (response) { alert(response.d); }
            });
        } else {
            This.parent().find(".fake-value").text("");
            $("#" + StaffType).val("");
        }
    });
}

function getRatingver2() {
    $.ajax({
        type: "POST",
        url: "/GUI/SystemService/Webservice/wsRating.asmx/RequestRatingVer2",
        data: "{BillId : " + $("#HDF_BillId").val() + "}",
        contentType: "application/json; charset=utf-8",
        dataType: "json", success: function (response) {
            var mission = response.d;
            if (mission.success) {
                $("#CkRating").prop('checked', true);
                $(".rating-icon-wrap1").text("Khách đã đánh giá");
                $(".rating-icon-wrap1").css("color", "red");
                $("#HDF_Rating").val(parseInt(mission.data.StarNumber));
                $("#HDF_RatingIsComplete").val(mission.data.IsComplete === true ? 1 : 0);
            }
            else {
                $("#CkRating").prop('checked', false);
                $(".rating-icon-wrap1").text("Khách chưa đánh giá");
                $(".rating-icon-wrap1").css("color", "red");

                $("#HDF_Rating").val("0");
            }
            $("#not-pleasure-reason").hide();
        },
        failure: function (response) { alert(response.d); }
    });
}

function excRating(This, value) {
    var salonId = parseInt($("#HDF_SalonId").val());
    salonId = !isNaN(salonId) ? salonId : 0;
    if (!isAccountAdmin) {
        return;
    }
    else {
        clearInterval(requestRatingver2);
        $("#HDF_Rating").val(value);
        This.parent().find(".rating-icon").removeClass("active");
        This.addClass("active");
    }
}

function bindRating(mark) {
    switch (mark) {
        case 1: $(".rating-icon").removeClass("active");
            $(".rating-icon.icon-sad").addClass("active");
            break;
        case 2: $(".rating-icon").removeClass("active");
            $(".rating-icon.icon-normal").addClass("active");
            break;
        case 3: $(".rating-icon").removeClass("active");
            $(".rating-icon.icon-happy").addClass("active");
            break;
        default: break;
    }
}

//get rating number star
function excRatingV2(This, starNumber) {
    var salonId = parseInt($("#HDF_SalonId").val());
    salonId = !isNaN(salonId) ? salonId : 0;
    if (!isAccountAdmin) {
        return;
    }
    else {
        clearInterval(requestRatingver2);
        $("#HDF_Rating").val(starNumber);
        This.parent().find(".rating-icon").removeClass("active");
        This.addClass("active");
    }
}

//get rating number star
function bindRatingV2(starNumber) {
    switch (starNumber) {
        case 1:
            radiobtn = document.getElementById("1star");
            radiobtn.checked = true;
            $(".rating-icon").css("color", "#ddd");
            $("fieldset.rating").addClass("rating2");
            break;
        case 2:
            radiobtn = document.getElementById("2star");
            radiobtn.checked = true;
            $(".rating-icon").css("color", "#ddd");
            $("fieldset.rating").addClass("rating2");
            break;
        case 3:
            radiobtn = document.getElementById("3star");
            radiobtn.checked = true;
            $(".rating-icon").css("color", "#ddd");
            $("fieldset.rating").addClass("rating2");
            break;
        case 4:
            radiobtn = document.getElementById("4star");
            radiobtn.checked = true;
            $(".rating-icon").css("color", "#ddd");
            $("fieldset.rating").addClass("rating2");
            break;
        case 5:
            radiobtn = document.getElementById("5star");
            radiobtn.checked = true;
            $(".rating-icon").css("color", "#ddd");
            $("fieldset.rating").addClass("rating2");
            break;
        default: break;
    }
}

function showProductByCategory(This) {
    $("table#table-product tbody tr").each(function () {
        if ($(this).attr("data-cate") !== This.val()) {
            $(this).hide();
        } else {
            $(this).show();
        }
    });
    if ($(".popup-product-item").isOpenEBPopup()) {
        $(".popup-product-item").alignment();
    }
}

var templateVIPCard = 4;
// active input VIP card
function activeVIPCard(This) {
    var activeItem = $(".item-vipcard.active");
    var thisValue = This.find("input[type='text']").val();
    activeItem.find(".left i").removeClass("fa-check-square-o").addClass("fa-square-o");
    activeItem.find("input[type=text]").val("").attr("disabled", "disabled");
    activeItem.removeClass("active").removeClass("error");
    This.addClass("active").find("input[type='text']").val(thisValue);
    This.find(".left i").removeClass("fa-square-o").addClass("fa-check-square-o");
    This.find("input[type='text']").removeAttr("disabled").focus();
}

// validate input VIP card 
function validateVIPCard(This, event) {
    if (!checkTypingNumber(event)) {
        This.parent().parent().addClass("error");
        validate.vipcard = false;
        event.preventDefault();
    } else {
        var cardValue = This.val();
        if (!isAllowTyping(event) && cardValue.length >= templateVIPCard) {
            This.parent().parent().addClass("error");
            showMsgSystem("Lỗi. Thẻ VIP chỉ gồm tối đa 4 chữ số.", "warning");
            event.preventDefault();
        } else {
            This.parent().parent().removeClass("error");
            hideMsgSystem();
            validate.vipcard = true;
        }
    }
}

// validate VIP card value
function validateVIPCardValue(This, event) {
    var cardValue = This.val();
    if (cardValue !== '') {
        if (cardValue.length > templateVIPCard) {
            This.parent().parent().addClass("error");
            validate.vipcard = false;
            showMsgSystem("Lỗi. Thẻ VIP chỉ gồm tối đa 4 chữ số.", "warning");
        } else {
            if (!checkNumber(cardValue)) {
                This.parent().parent().addClass("error");
                validate.vipcard = false;
            } else if (This.attr("id") === "VIPGive") {
                checkIssetVIPCard(This, cardValue);
            }
        }
    } else {
        This.parent().parent().removeClass("error");
        hideMsgSystem();
        validate.vipcard = true;
    }
}

function inputVIPcardBlur(This) {
    var cardValue = This.val();
    if (cardValue === "") {
        This.parent().parent().removeClass("error");
        hideMsgSystem();
        validate.vipcard = true;
    }
}

function checkIssetVIPCard(This, card) {
    $.ajax({
        type: "POST",
        url: "/GUI/FrontEnd/Service/Service_Pending_Complete.aspx/issetVIPCard",
        data: "{input : '" + card + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json", success: function (response) {
            var mission = JSON.parse(response.d);
            if (mission.success) {
                This.parent().parent().addClass("error");
                validate.vipcard = false;
                showMsgSystem("Lỗi. Mã thẻ VIP đã được tặng.", "warning");
            } else {
                This.parent().parent().removeClass("error");
                hideMsgSystem();
                validate.vipcard = true;
            }
        },
        failure: function (response) { alert(response.d); }
    });
}

//Them san pham tu dropdown
function ClickAddProduct(This) {
    var index = 1;
    $('#tbodyProduct').find("tr").each(function () {
        $('#tbodyProduct tr').find("td.td-product-index").text(index);
        index++;
    });
    //thêm mapid
    var IdProduct = $("#ddlProduct").find(':selected').data('id');
    var Code = $("#ddlProduct").find(':selected').data('code');
    var MapId = $("#ddlProduct").find(':selected').data('mapid') !== undefined ? $("#ddlProduct").find(':selected').data('mapid') : "";
    var PriceProduct = $("#ddlProduct").find(':selected').data('price');
    var Cate = $("#ddlProduct").find(':selected').data('cate');
    var NameProduct = $("#ddlProduct").find(':selected').data('name');
    var DiscountCosmetic = 0;
    if (disCosmetic !== null && disCosmetic !== undefined && disCosmetic !== '' && parseInt(disCosmetic) > 0) {
        DiscountCosmetic = disCosmetic.trim() !== "" ? parseInt(disCosmetic) : 0;
    }
    else {
        if (This.data('discount') !== undefined) {
            DiscountCosmetic = This.data('discount') !== "" ? This.data('discount') : 0;
        }
    }
    var Quantity = 1;
    var price = 0;
    //neu la membership
    if ($("#BarcodeStatus").val() == "bookingMember") {
        var option = $("#memberProduct").find("option[data-key='" + $("#MemberProductId").val() + '-2-' + IdProduct + "']");
        if (option != null) {
            price = option.data("price");
        }
        if (price == null) {
            price = PriceProduct;
        }
    } else {
        price = PriceProduct;
    }

    var Price = (FormatPrice(Quantity * price * (100 - DiscountCosmetic) / 100));

    if (IdProduct !== 0) {
        //thêm td mapid
        var $body = $('#tbodyProduct');
        if (ExcCheckItemIsChose(Code, $body)) {
            showMsgSystem("Sản phẩm đã tồn tại trong danh sách!", "warning");
            UpdateItemOrder($("#table-item-product").find("tbody"));
            return;
        }
        $body.append(`<tr>
                                        <td class="td-product-index"></td>
                                        <td class="td-product-name">${NameProduct}</td>
                                        <td style="display: none !important;" class="td-product-code" data-code='${Code}' data-id='${IdProduct}'>${Code}</td>
                                        <!--thêm Mapid-->
                                        <td class="td-product-map" style="display:none;" data-mapid='${MapId}'>${MapId}</td>
                                        <td class="td-product-price" data-price='${price}'>${price}</td>
                                        <td class="td-product-quantity">
                                            <input class="product-quantity" maxlength="2" onkeypress="return ValidateKeypress(/\\d/,event);" type="text" value='${Quantity}' />
                                        </td>
                                        <td class="td-product-voucher" data-voucher='${DiscountCosmetic}'>
                                            <div class="row"><input type="text" onblur="ValidatePercent($(this));" onkeypress="return ValidateKeypress(/\\d/,event);" maxlength="3" class="product-voucher voucher-cosmetic" value='${DiscountCosmetic}' style="margin: 0 auto; float: none; text-align: center; width: 50px;">%</div>
                                        </td>
                                        <td class="td-product-seller">
                                            <input id="input-seller${IdProduct}" class="st-head ip-short" data-staff-type="Seller" data-field="Cosmetic" data-value="0" placeholder="Bán sản phẩm" style="width: 30% !important; text-align: center; border: 1px solid #ccc;" onkeyup="changeSeller($(this),${IdProduct})" />
                                            <input id="input-seller-name${IdProduct}" readonly class="st-head ip-short" data-staff-type="Seller" data-field="Cosmetic" data-value="0" style="width: 50% !important; text-align: left; padding-left: 10px; border: 1px solid #ccc; background-color: #e7e7e7" />
                                            </div>
                                        </td>
                                        <td class="map-edit">
                                            <div class="box-money" style="display: block;">${FormatPrice(Price)}</div>
                                            <div class="edit-wp"><a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),\'${NameProduct}\',\'product\')" href="javascript://" title="Xóa"></a></div>
                                        </td>
                                    </tr>`);
        $(".item-product").show();
        TotalMoney();
        getProductIds();
        getServiceIds();
        ExcQuantity();
        UpdateItemOrder($("#table-item-product").find("tbody"));
        ResetDropdownProduct();
        $("#txtReceiveMoney").val("");
        $("#txtReturnMoney").val(0);
    }
    else {
        showMsgSystem("Bạn chưa chọn sản phẩm!", "warning");
    }

}

// ResetDropdownProduct
function ResetDropdownProduct() {
    $("#ddlProduct").val(0);
    $("#select2-ddlProduct-container").attr('title', 'Chọn sản phẩm ...');
    $("#select2-ddlProduct-container").text("Chọn sản phẩm ...");
}

// Them san pham khi them bang checkbox
function pushQuickproductData(This) {
    var index = 1;
    var IdProduct = This.data('id');
    var Code = This.data('code');
    //Thêm mapid
    var MapId = This.data('mapid');
    var PriceProduct = This.data('price');
    var Cate = This.data('cate');
    var NameProduct = This.data('name');
    var price = 0;
    //neu la membership
    if ($("#BarcodeStatus").val() == "bookingMember") {
        var option = $("#memberProduct").find("option[data-key='" + $("#MemberProductId").val() + '-2-' + IdProduct + "']");
        if (option != null) {
            price = option.data("price");
        }
        if (price == null) {
            price = PriceProduct;
        }
    } else {
        price = PriceProduct;
    }

    var DiscountCosmetic = 0;

    if (disCosmetic !== null && disCosmetic !== undefined && disCosmetic !== '' && parseInt(disCosmetic) > 0) {
        DiscountCosmetic = disCosmetic.trim() !== "" ? parseInt(disCosmetic) : 0;
    }
    else {
        if (This.data('discount') !== undefined) {
            DiscountCosmetic = This.data('discount') !== "" ? This.data('discount') : 0;
        }
    }
    var Quantity = 1;

    var Price = (FormatPrice(Quantity * price * (100 - DiscountCosmetic) / 100));

    if (This.is(":checked")) {
        var $body = $('#tbodyProduct');
        if (ExcCheckItemIsChose(Code, $body)) {
            showMsgSystem("Sản phẩm đã tồn tại trong danh sách!", "warning");
            This.attr('checked', false);
            UpdateItemOrder($("#table-item-product").find("tbody"));
            return;
        }
        //thêm td mapid

        $body.append(`<tr>
                                        <td class="td-product-index"></td>
                                        <td class="td-product-name">${NameProduct}</td>
                                        <td style="display: none !important;" class="td-product-code" data-code='${Code}' data-id='${IdProduct}'>${Code}</td>
                                        <!--thêm Mapid-->
                                        <td class="td-product-map" style="display:none;" data-mapid='${MapId}'>${MapId}</td>
                                        <td class="td-product-price" data-price='${price}'>${price}</td>
                                        <td class="td-product-quantity">
                                            <input class="product-quantity" maxlength="2" onkeypress="return ValidateKeypress(/\\d/,event);" type="text" value='${Quantity}' />
                                        </td>
                                        <td class="td-product-voucher" data-voucher='${DiscountCosmetic}'>
                                            <div class="row"><input type="text" onblur="ValidatePercent($(this));" onkeypress="return ValidateKeypress(/\\d/,event);" maxlength="3" class="product-voucher voucher-cosmetic" value='${DiscountCosmetic}' style="margin: 0 auto; float: none; text-align: center; width: 50px;">%</div>
                                        </td>
                                        <td class="td-product-seller">
                                            <input id="input-seller${IdProduct}" class="st-head ip-short" data-staff-type="Seller" data-field="Cosmetic" data-value="0" placeholder="Bán sản phẩm" style="width: 30% !important; text-align: center; border: 1px solid #ccc;" onkeyup="changeSeller($(this),${IdProduct})" />
                                            <input id="input-seller-name${IdProduct}" readonly class="st-head ip-short" data-staff-type="Seller" data-field="Cosmetic" data-value="0" style="width: 50% !important; text-align: left; padding-left: 10px; border: 1px solid #ccc; background-color: #e7e7e7" />
                                            </div>
                                        </td>
                                        <td class="map-edit">
                                            <div class="box-money" style="display: block;">${FormatPrice(Price)}</div>
                                            <div class="edit-wp"><a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),\'${NameProduct}\',\'product\')" href="javascript://" title="Xóa"></a></div>
                                        </td>
                                    </tr>`);
        $(".item-product").show();
    } else {
        $("#table-item-product").find(".td-product-code[data-code='" + Code + "']").parent().remove();
    }
    TotalMoney();
    getProductIds();
    getServiceIds();
    ExcQuantity();
    UpdateItemOrder($("#table-item-product").find("tbody"));
    $("#txtReceiveMoney").val("");
    $("#txtReturnMoney").val(0);
}

//
function sendComplete(customerConfirmType) {
    var InputVal = {
        Stylist: parseInt($("#Hairdresser").val().trim()) || 0,
        Skinner: parseInt($("#HairMassage").val().trim()) || 0,
        Checkout: parseInt($("#HDF_Checkout").val().trim()) || 0,
        CustomerName: $("#CustomerName").val().trim(),
        TotalMoney: parseInt($("#HDF_TotalMoney").val().trim()) || 0,
        hasServices: $("#HDF_ServiceIds").val().trim() !== "" && $("#HDF_ServiceIds").val().trim() !== "[]",
        hasProducts: $("#HDF_ProductIds").val() !== "" && $("#HDF_ProductIds").val() !== "[]",
        RatingPoint: parseInt($("#HDF_Rating").val()) || 0,
        Description: $("#Description").val().trim(),
        ReceiveMoney: $("#txtReceiveMoney").val().replace(/\./gm, "") || 0
    };
    try {
        // Validate : Mã khách hàng || Khách không cho thông tin
        if (InputVal.CustomerName.length < 1 && $("#InputCusNoInfor").is(":checked") === false) {
            throw new Error("Bạn chưa nhập mã khách hàng hoặc chưa tích chọn \"Khách không cho thông tin\".");
        }

        if (InputVal.hasServices && !InputVal.Stylist && !InputVal.Skinner) {
            throw new Error("Nhấn f5 để chọn stylist ban đầu");
        }
        // Validate : Chọn dịch vụ khi đã nhập Stylist hoặc Skinner hoặc Checkout
        if (!InputVal.hasServices && !InputVal.Stylist && !InputVal.Skinner && !InputVal.Checkout) {
            if (!InputVal.hasProducts) {
                throw new Error("Bạn chưa chọn dịch vụ hoặc sản phẩm.");
            }
        }

        if (!InputVal.hasServices && (InputVal.Stylist || InputVal.Skinner || InputVal.Checkout)) {
            throw new Error("Bạn chưa chọn dịch vụ");
        }

        if (InputVal.hasServices && (!InputVal.Stylist && !InputVal.Skinner && !InputVal.Checkout)) {
            throw new Error("Bạn chưa chọn Nhân viên");
        }

        if (InputVal.hasServices && !InputVal.Checkout && checkSalon) {
            throw new Error("Bạn chưa chọn Checkout");
        }

        $("table.table-item-service tbody tr").each(function () {
            var THIS = $(this);
            var Id = THIS.find("td.td-product-code").attr("data-id"),
                Name = THIS.find("td.td-product-name").text().trim(),
                Quantity = THIS.find("input.product-quantity").val();
            if (Quantity === undefined || Quantity === null || Quantity === '') {
                throw new Error(`Sản phẩm : ${Name} chưa chọn số lượng!`);
            }
            else if (Quantity === '0') {
                throw new Error(`Số lượng sản phẩm ${Name} phải lớn hơn 0!`);
            }
        });

        $("table.table-item-product tbody tr").each(function () {
            var THIS = $(this);
            var Id = THIS.find("td.td-product-code").attr("data-id"),
                Name = THIS.find("td.td-product-name").text().trim(),
                Quantity = THIS.find("input.product-quantity").val(),
                SellerId = THIS.find('#input-seller' + Id).val(),
                SellerName = THIS.find('#input-seller-name' + Id).val();
            if (SellerId === undefined || SellerId === null || SellerId === '' || SellerId == '0') {
                throw new Error("Bạn chưa chọn nhân viên bán sản phẩm!");
            }
            else if (SellerName === undefined || SellerName === null || SellerName === '' || SellerName === '...') {
                throw new Error("Vui lòng kiểm tra lại nhân viên hoặc nhân viên chưa được chấm công!");
            }
            else if (Quantity === undefined || Quantity === null || Quantity === '') {
                throw new Error(`Sản phẩm : ${Name} chưa chọn số lượng!`);
            }
            else if (Quantity === '0') {
                throw new Error(`Số lượng sản phẩm ${Name} phải lớn hơn 0!`);
            }
        });

        if (!validate.vipcard) {
            throw new Error("Lỗi thẻ VIP.");
        }

        // Validate : Chọn dịch vụ hoặc sản phẩm
        if (!InputVal.TotalMoney) {
            //throw new Error("Bạn chưa chọn dịch vụ hoặc sản phẩm.");
        }
        // add 20190514
        if (pending === 1 && $("#PayByCard").is(":checked") === false) {
            // get total money in bill
            var totalMoney = $("#MoneyInBill").val().replace(/\./gm, "");
            if (totalMoney > 0) {
                totalMoney = totalMoney.trim() !== "" ? parseInt(totalMoney) : 0;
            }

            var receive = InputVal.ReceiveMoney;
            if (receive > 0 && receive !== "") {
                receive = receive.trim() !== "" ? parseInt(receive) : 0;
            }

            if (!InputVal.ReceiveMoney) {
                $("#txtReceiveMoney").css("border-color", "red");
                $("#txtReceiveMoney").focus();
                throw new Error("Bạn phải nhập số tiền nhận của khách hàng!");
            }
            if (isNaN(InputVal.ReceiveMoney)) {
                $("#txtReceiveMoney").css("border-color", "red");
                $("#txtReceiveMoney").focus();
                $("#txtReturnMoney").val("");
                throw new Error("Bạn vui lòng kiểm tra lại số tiền bạn vừa nhập!", "warning");
            }
            if (receive < totalMoney) {
                $("#txtReceiveMoney").css("border-color", "red");
                $("#txtReceiveMoney").focus();
                $("#txtReturnMoney").val("");
                throw new Error("Bạn vui lòng kiểm tra lại số tiền bạn vừa nhập!", "warning");
            }
        }
    }
    catch (ex) {
        showMsgSystem(ex.message, "warning");
        return;
    }


    var staffInputError = [];

    $('.staff-auto-selection').each(function (index, item) {
        var Type = item.dataset.staffType;
        var OrderCode = item.value.trim().length > 0 ? parseInt(item.value.trim()) : 0;
        var staff = _FN.StaffSuggestion.getStaff(Type, OrderCode);

        if (OrderCode > 0 && staff === null) {
            staffInputError.push(item);
        }
    });

    if (staffInputError.length > 0) {
        var txtStaffInputError = '';

        for (var i in staffInputError) {
            txtStaffInputError += "Thông tin ô nhập " + staffInputError[i].getAttribute('placeholder') + " chưa đúng, vui lòng kiểm tra lại!<br>";
        }

        // show EBPopup
        $("#contentStaffInputError").empty().html(txtStaffInputError);
        $(".popup-staff-input-error").openEBPopup();
        staffInputError[0].focus();

        return;
    }

    var CustomerCode = $("#HDF_CustomerCode").val();
    var submit = function () {
        //Bind_UserImagesToHDF();
        var submitHandler = function () {
            closeAppRating();
            //In phieu, chỉ in phiếu khi checkout lần đầu.
            if (pending === 1) {
                // Chỉ push noti cho khách lần đầu.
                //PushCustomer();
                PrintBill();
            }
            setTimeout(function () { $("#BtnFakeSend").click(); }, 1000);
        };
        var ret = $.event.trigger({
            type: "onBeforeCompleteBillService",
            submitHandler: submitHandler,
            _data: InputVal
        });

        if (typeof ret === "function") {
            submitHandler = ret;
        }

        submitHandler.call(null);
    };

    // Check duplicate bill
    var CheckDuplicateBill = function () {
        $.ajax({
            type: "POST",
            url: "/GUI/FrontEnd/Service/Service_Add.aspx/CheckDuplicateBill",
            data: '{CustomerCode : "' + CustomerCode + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                var mission = JSON.parse(response.d);
                if (mission.success) {
                    removeLoading();
                    var products = "Sản phẩm : ";
                    var services = "Dịch vụ : ";
                    var confirm_content = "";
                    var bill = JSON.parse(mission.msg);
                    if (bill.ProductIds !== "" && bill.ProductIds !== null) {
                        var _Product = $.parseJSON(bill.ProductIds);
                        $.each(_Product, function (i, v) {
                            products += v.Name + " ( " + v.Price + " ), ";
                        });
                        products = products.replace(/(\,\s)+$/, '');
                    }
                    if (bill.ServiceIds !== "") {
                        var _Service = $.parseJSON(bill.ServiceIds);
                        $.each(_Service, function (i, v) {
                            services += v.Name + " ( " + v.Price + " ), ";
                        });
                        services = services.replace(/(\,\s)+$/, '');
                    }
                    confirm_content += products + ", " + services + ", ";
                    confirm_content = confirm_content.replace(/(\,\s)+$/, '');

                    // show EBPopup
                    $(".confirm-yn").openEBPopup();
                    $("#EBPopup .confirm-yn-text").text("Khách hàng mã [" + CustomerCode + "] đã được lập hóa đơn trong hôm nay [ " + confirm_content + " ]. Bạn có chắc muốn tiếp tục ?");

                    $("#EBPopup .yn-yes").bind("click", function () {
                        $("#EBCloseBtn").click();
                        submit.call(null);

                    });
                    $("#EBPopup .yn-no").bind("click", function () {
                        autoCloseEBPopup(0);
                    });
                } else {
                    submit.call(null);
                }
            },
            beforeSend: function () {
                startLoading();
            },
            complete: function () {
                finishLoading();
            },
            failure: function (response) { alert(response.d); }
        });
    };

    var CheckCustomerConfirm = function () {
        $("#CustomerConfirm").val(customerConfirmType);
        //truong hop nhan Hoan tat
        if (customerConfirmType === 1) {
            //kiem tra KH da xac nhan bill chua            
            $.ajax({
                type: "GET",
                url: URL_API_CHECKOUT + "/api/bill-confirm?billId=" + $("#HDF_BillId").val(),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response) {
                        CheckDuplicateBill.call(null);

                    } else {
                        $("#CustomerNotConfirmModal").modal();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#CustomerNotConfirmModal").modal();
                }
            });
        } else {
            //nhan KH khong xac nhan, he thong loi
            // show EBPopup
            $(".confirm-yn").openEBPopup();
            $("#EBPopup .confirm-yn-text").text("Bill này sẽ được Phòng Quản lý chất lượng kiểm tra lại qua camera. Bạn có chắc muốn tiếp tục ?");

            $("#EBPopup .yn-yes").bind("click", function () {
                $("#EBCloseBtn").click();
                CheckDuplicateBill.call(null);
            });
            $("#EBPopup .yn-no").bind("click", function () {
                autoCloseEBPopup(0);
            });

        }
    }
    //
    if (pending === 1) {
        //kiem tra campaign voucher da bi het han chua
        if ($("#ListCampaignServices").val() != "" && $('#TextBoxVoucher').val() != '') {
            $.ajax({
                //async: false,
                type: "POST",
                url: domainApiBillService + '/api/validate-campaign/validateVoucher',
                data: JSON.stringify({ "customerPhone": $("#HDF_CustomerCode").val(), "bookDate": $('#BillCreatedDate').val(), "voucher": $('#TextBoxVoucher').val() }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.status) {
                        if (response.listService != null && response.listService.length > 0) {
                            //
                            //cancelCampaign();
                            ////
                            //campaignMktObj.campaign = response.campaign;
                            //campaignMktObj.listService = response.listService;
                            //campaignMktObj.bookingNote = response.bookingNote;
                            ////
                            //showMsg($("#MsgInputVoucher"), "Khách hàng được hưởng Voucher trong chương trình: " + campaignMktObj.campaign.label, "success");
                            ////Tinh toan uu dai cho khach hang.
                            //CalCampaignService(0, voucherCodeTmp);
                            CheckCustomerConfirm();
                        } else {
                            //showMsg($("#MsgInputVoucher"), "Mã Voucher đã không còn đủ điều kiện sử dụng!!!", "warning");
                            $("#contentCampaignNotCondition").text("Mã Voucher đã không còn đủ điều kiện sử dụng!!!");
                            $("#CampaignNotConditionModal").modal();
                            cancelCampaign();
                            campaignMktObj.listService = [];
                            $(".MKT-label").html('');
                        }
                    }
                },
                failure: function (response) {
                    alert(response);
                },
                timeout: function (response) {
                    alert(response);
                },
                error: function (response) {
                    alert(response);
                }
            });
        } else if ($("#ListCampaignServices").val() != "") {
            $.ajax({
                //async: false,
                type: "POST",
                url: domainApiBillService + '/api/validate-campaign/getCampaignCheckout',
                data: JSON.stringify({ "bookingId": parseInt($("#BookingId").val()), "customerId": customerID }),
                contentType: "application/json;charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.status) {
                        if (response.listService != null && response.listService.length > 0) {
                            if (campaignMktObj.campaign.id == response.campaign.id) {
                                //campaignMktObj.listService = response.listService;
                                //campaignMktObj.bookingNote = response.bookingNote;
                                ////Tinh toan uu dai cho khach hang.
                                //CalCampaignService(0);
                                CheckCustomerConfirm();
                            } else {
                                $("#contentCampaignNotCondition").text("Campaign đã không còn đủ điều kiện sử dụng!!!");
                                $("#CampaignNotConditionModal").modal();
                                cancelCampaign();
                                campaignMktObj.listService = [];
                                $(".MKT-label").html('');
                            }
                        } else {
                            $("#contentCampaignNotCondition").text("Campaign đã không còn đủ điều kiện sử dụng!!!");
                            $("#CampaignNotConditionModal").modal();
                            cancelCampaign();
                            campaignMktObj.listService = [];
                            $(".MKT-label").html('');
                        }
                    }
                }
            });
        } else {
            CheckCustomerConfirm();
        }

    } else {
        //truong hop sua bill
        CheckDuplicateBill.call(null);
    }

}

// add 20190514
function checkReceiveMoney(This) {
    // get total money in bill
    var totalMoney = $("#MoneyInBill").val().replace(/\./gm, "");

    // get receive money
    var receiveMoney = This.val().replace(/\./gm, "");
    receiveMoney = receiveMoney.trim() !== "" ? parseInt(receiveMoney) : 0;

    if (isNaN(receiveMoney)) {
        showMsgSystem("Bạn vui lòng kiểm tra lại số tiền bạn vừa nhập!", "warning");
        $("#txtReceiveMoney").css("border-color", "red");
        $("#txtReceiveMoney").focus();
        $("#txtReturnMoney").val("");
        return;
    }
    //
    var totalReturn = 0;
    // check
    if (receiveMoney >= totalMoney) {
        totalReturn = receiveMoney - totalMoney;
        //
        $("#txtReceiveMoney").css("border-color", "#50B347");
        $("#txtReceiveMoney").focus();
        $("#txtReceiveMoney").val(FormatPrice(receiveMoney));
        //
        $("#txtReturnMoney").val(FormatPrice(totalReturn));
        $("#txtReturnMoney").css("border-color", "red");
        $("#txtReturnMoney").focus();
        // bind data to HDF in bill
        $("#HDF_ReceiveMoney").val(FormatPrice(receiveMoney));
        $("#HDF_ReturnMoney").val(FormatPrice(totalReturn));
    }
    else {
        showMsgSystem("Bạn vui lòng kiểm tra lại số tiền bạn vừa nhập!", "warning");
        $("#txtReceiveMoney").css("border-color", "red");
        $("#txtReceiveMoney").focus();
        $("#txtReturnMoney").val("");
    }
}


/*
* Validate keypress
*/
function ValidateKeypress(numcheck, e) {
    var keynum, keychar, numcheck;
    if (window.event) {
        keynum = e.keyCode;
    }
    else if (e.which) {
        keynum = e.which;
    }
    if (keynum === 8 || keynum === 127 || keynum === null || keynum === 9 || keynum === 0 || keynum === 13) return true;
    keychar = String.fromCharCode(keynum);
    var result = numcheck.test(keychar);
    return result;
}

function ValidatePercent(This) {
    if (This.val() > 100) {
        //alert("Giảm giá không được nhập quá 100");
        This.val('100');
    }
}

function BuyMembership() {
    //neu la member roi thi ko cho phep mua tiep
    if ($('#MemberType').val() == "1" && $("#Pending").val() == 1) {
        showMsgMembership("Không áp dụng mua thẻ với khách hàng đã là member.", "warning");
        return;
    }
    var txtBarcode = $("#txtInputBarcode").val();
    if ($("#BarcodeStatus").val() == "") {
        //chua thuc hien quet barcode
        showMsgMembership("Chưa có thông tin mã vạch của khách hàng. Hãy thực hiện quét!!!", "warning");
        return;
    }
    if ($("#BarcodeStatus").val() == "notBookingNotMember" ||
        $("#BarcodeStatus").val() == "notBookingMember") {
        showMsgMembership("Không áp dụng với số điện thoại khác với booking.", "warning");
        return;
    }
    //validate thong tin 
    if (!validateProductMembership()) {
        return;
    }
    //bat lua chon neu co campaign
    if (campaignMktObj.listService.length > 0) {
        //
        $("#MembershipDisableCampaignModal").modal();
        return;
    }

    //set id cua product the membership
    $("#tbodyMembershipProduct").find("input[type='checkbox']:checked").each(function () {
        var tr = $(this).parent().parent();
        var td = tr.find(".member-info");
        var IdProduct = td.data('id');
        $("#MemberProductId").val(IdProduct);
    });

    //
    ApplyPriceMembership();
    //thuc hien add vao phan product
    pushProductMembership();
    //

    //thuc hien add the membership vao bill khi checkout, ko dung khi sua bill
    if ($("#Pending").val() == "1") {
        updateBillBuyMembership();
    } else {
        $("#MemberLabel").html("Áp dụng membership thành công.");
        //
        $("#membershipModal").modal("hide");
    }
}

function ApplyMembership() {

    var temp = $("#BarcodeStatus").val();
    var txtBarcode = $("#txtInputBarcode").val();
    if ($("#BarcodeStatus").val() == "") {
        //chua thuc hien quet barcode
        showMsgMembership("Chưa có thông tin mã vạch của khách hàng. Hãy thực hiện quét!!!", "warning");
    }
    //
    else if ($("#BarcodeStatus").val() == "bookingMember") {
        //bat lua chon neu co campaign
        if (campaignMktObj.listService.length > 0) {
            //
            $("#MembershipDisableCampaignModal").modal();
            return;
        }
        //apply
        ApplyPriceMembership();
        //khach hang la membership va dung ma barcode
        $("#MemberLabel").html("Áp dụng membership thành công.");
        //
        $("#membershipModal").modal("hide");
        //        
    } else {
        //kh ko phai la membership
        showMsgMembership("Khách hàng không phải là membership. Không áp dụng được tính năng thành viên!!!", "warning");
    }

}

function checkBarcode(barcode) {


    $('#txtInputBarcode').val(barcode);
    if ($('#HDF_CustomerId').val() == barcode.trim()) {
        //dung ma khach hang      
        if ($('#MemberType').val() == "1") {
            //neu la member -> cho phep
            $('#BarcodeStatus').val("bookingMember");
            if ($('#btnBuyMembership').is(":visible")) {
                showMsgMembership("Xác thực mã khách hàng thành công.", "success");
            } else {
                showMsgMembership("Khách hàng là membership.", "success");
            }
            $("#txtInputPhone").prop('disabled', true);
        } else {
            //ko la member 
            $('#BarcodeStatus').val("bookingNotMember");
            if ($('#btnBuyMembership').is(":visible")) {
                showMsgMembership("Xác thực mã khách hàng thành công.", "success");
                $("#txtInputPhone").prop('disabled', true);
            } else {
                //-> ko cho phep khi ap dung
                showMsgMembership("Khách hàng không phải là membership.", "warning");
            }

        }

    } else {
        //ko phai la so dat lich, check customer xem customerId nay co phai la membership ko
        $.ajax({
            type: "GET",
            url: URL_API_CHECKOUT + "/api/customer?customerId=" + barcode,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                //                
                if (response == "no content" || response == null) {
                    showMsgMembership("Không có khách hàng ứng với mã vạch này.", "warning");
                } else if (response.customerId != null) {
                    if (response.memberType == "0") {
                        //ko phai member, ko cho phep
                        $('#BarcodeStatus').val("notBookingNotMember");
                        showMsgMembership("Số thuê bao quét ra (" + response.phone + ") khác với số booking (" + $('#CustomerCode').val().replace('Số ĐT : ', "").trim() + ").", "warning");
                    } else {
                        //la member, cho phep
                        $('#BarcodeStatus').val("notBookingMember");
                        showMsgMembership("Số thuê bao quét ra (" + response.phone + ") khác với số booking (" + $('#CustomerCode').val().replace('Số ĐT : ', "").trim() + ").", "warning");
                    }
                } else {
                    showMsgMembership("Lỗi hệ thống. Hãy thử lại sau.", "warning");
                }
            },
            error: function () {
                $("#BarcodeStatus").val("");
                showMsgMembership("Lỗi hệ thống. Hãy thử lại sau.", "warning");
            },
            timeout: function () {
                $("#BarcodeStatus").val("");
                showMsgMembership("Lỗi hệ thống. Hãy thử lại sau.", "warning");
            }
        });
    }

}

function checkPhone(inputPhone) {

    var phone = $('#CustomerCode').val().replace('Số ĐT : ', "").trim();

    if (phone == inputPhone.trim()) {
        //dung so khach hang   
        if ($('#MemberType').val() == "1") {
            //neu la member -> cho phep
            $('#BarcodeStatus').val("bookingMember");
            if ($('#btnBuyMembership').is(":visible")) {
                showMsgMembership("Xác thực mã khách hàng thành công.", "success");
            } else {
                showMsgMembership("Khách hàng là membership.", "success");
            }
            $("#txtInputBarcode").prop('disabled', true);
        } else {
            //ko la member 
            $('#BarcodeStatus').val("bookingNotMember");
            if ($('#btnBuyMembership').is(":visible")) {
                showMsgMembership("Xác thực mã khách hàng thành công.", "success");
                $("#txtInputBarcode").prop('disabled', true);
            } else {
                //-> ko cho phep khi ap dung
                showMsgMembership("Khách hàng không phải là membership.", "warning");
            }
        }
    } else {
        $('#BarcodeStatus').val("notBookingNotMember");
        showMsgMembership("Số thuê bao quét ra (" + inputPhone + ") khác với số booking (" + phone + ").", "warning");
    }
}

function CheckInputBarcode() {
    //khi nhap xong o txt barcode, kiem tra co phai la membership ko
    if ($('#txtInputBarcode').val() != "") {
        checkBarcode($('#txtInputBarcode').val());
    }
}

function CheckInputPhone() {
    if ($('#txtInputPhone').val() != "") {
        checkPhone($('#txtInputPhone').val());
    }
}

function validateProductMembership() {
    var buyMembership = false;
    var totalQuantity = 0;
    $("#tbodyMembershipProduct").find("input[type='checkbox']:checked").each(function () {
        buyMembership = true;
        var tr = $(this).parent().parent();
        var Quantity = tr.find(".member-quantity").val();
        totalQuantity += Quantity;
    });

    if (!buyMembership) {
        showMsgMembership("Chưa chọn loại thẻ thành viên.", "warning");
        return false;
    }

    if (totalQuantity == 0) {
        showMsgMembership("Chưa nhập số lượng thẻ.", "warning");
        return false;
    }
    //da co the member roi => ko cho add nua
    var foundProduct = false;
    $("#tbodyMembershipProduct").find(".member-info").each(function () {
        buyMembership = true;
        var id = $(this).data("id");
        //                
        $("#tbodyProduct").find('td[data-id="' + id + '"]').each(function () {
            foundProduct = true;
        });
    });
    if (foundProduct) {
        showMsgMembership("Đã có thông tin mua thẻ membership trong Bill.", "warning");
        return false;
    }

    return true;
}

function pushProductMembership() {
    $("#tbodyMembershipProduct").find("input[type='checkbox']:checked").each(function () {

        var tr = $(this).parent().parent();
        var td = tr.find(".member-info");
        var IdProduct = td.data('id');
        var Code = td.data('code');
        var Cate = td.data('cate');
        var PriceProduct = tr.find(".member-price").text();
        var NameProduct = tr.find(".member-name").text();
        var Quantity = tr.find(".member-quantity").val();
        var Price = (FormatPrice(Quantity * PriceProduct));
        var foundProduct = false;
        $("#tbodyProduct").find('td[data-id="' + IdProduct + '"]').each(function () {
            foundProduct = true;
        });
        if (!foundProduct) {
            //thêm td mapid
            $('#tbodyProduct').append('<tr data-cate="' + Cate + '"><td class="td-product-index"></td><td class="td-product-name">' + NameProduct
                + '</td><td style="display: none !important;" class="td-product-code" data-id="' + IdProduct + '" data-code="' + Code + '">' + Code
                + '</td><td class="td-product-map" style="display:none;" data-mapid="' + '">'
                + '</td><td class="td-product-price" data-price="' + PriceProduct + '" >' + PriceProduct
                + '</td><td class="td-product-quantity"><input type="text" onkeypress="return ValidateKeypress(/\\d/,event);" maxlength="2" class="product-quantity" value="' + Quantity
                + '"></td><td class="td-product-voucher" data-voucher="' + 0
                + '"><div class="row"><input type="text" onblur="ValidatePercent($(this));" onkeypress="return ValidateKeypress(/\\d/,event);" maxlength="3" class="product-voucher voucher-cosmetic" value="' + 0
                + '" style="margin: 0 auto; float: none; text-align: center; width: 50px;">%</div></td>'
                + '<td class="td-product-seller">'
                + '<input id="input-seller' + IdProduct + '" class="st-head ip-short" data-staff-type="Seller" data-field="Cosmetic" data-value="0" placeholder="Bán sản phẩm" style="width: 30% !important; text-align: center; border: 1px solid #ccc;" onkeyup="changeSeller($(this),' + IdProduct + ')" />'
                + '<input id="input-seller-name' + IdProduct + '" readonly class="st-head ip-short" data-staff-type="Seller" data-field="Cosmetic" data-value="0" style="width: 50% !important; text-align: left; padding-left: 10px; border: 1px solid #ccc; background-color: #e7e7e7" />'
                + '</div>'
                + '</td>'
                + '<td class="map-edit"><div class="box-money" style="display: block;">' + FormatPrice(Price) + '</div>'
                + '<div class="edit-wp">'
                + ($("#Pending").val() == "0" ? '<a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),\''
                    + NameProduct + '\',\'product\')" href="javascript://" title="Xóa"></a>' : "")
                + '</div></td></tr>');
            $(".item-product").show();
        }
    });
    TotalMoney();
    getProductIds();
    getServiceIds();
    ExcQuantity();
    UpdateItemOrder($("#table-item-product").find("tbody"));
}

function ApplyPriceMembership() {
    $('#table-item-product').find("th").eq(4).text('Đơn giá cho member');
    $('#table-item-product').find("th").eq(4).css('color', 'red');

    $('#table-item-service').find("th").eq(3).text('Đơn giá cho member');
    $('#table-item-service').find("th").eq(3).css('color', 'red');
    //cap nhat gia cho service dang co
    $("#tbodyService").find("tr").each(function () {

        var tr = $("#table-service").find("input[data-code='" + $(this).find(".td-product-code").text() + "']").parent().parent();

        var price = "0";
        var td = tr.find(".td-product-price");
        var MemberProductId = $("#MemberProductId").val();
        //neu la membership
        var option = $("#memberProduct").find("option[data-key='" + $("#MemberProductId").val() + '-1-' + $(this).find(".td-product-code").data("id") + "']");
        if (option != null) {
            price = option.data("price");
        }
        if (price == null) {
            price = tr.find(".td-product-price").data("price");
        }
        //
        price = (price == "" ? "0" : price);
        $(this).find(".td-product-price").text(price);
        $(this).find(".td-product-price").attr("data-price", price);
        //
        $(this).find("input.product-voucher").val(0);
        //tinh lai tien
        var quantity = $(this).find(".product-quantity").val(),
            voucher = $(this).find("input.product-voucher").val();
        price = price.toString().trim() != "" ? parseInt(price) : 0;
        quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
        voucher = voucher.trim() != "" ? parseInt(voucher) : 0;
        var money = FormatPrice(quantity * price * (100 - voucher) / 100);
        $(this).find(".box-money").text(money).show();

    });
    //cap nhat gia cho product dang co
    $('#tbodyProduct').find("tr").each(function () {
        //var option = $("#ddlProduct").find("option[data-code='" + $(this).find(".td-product-code").text() + "']");
        var price = "0";
        //
        var option = $("#memberProduct").find("option[data-key='" + $("#MemberProductId").val() + '-2-' + $(this).find(".td-product-code").data("id") + "']");
        if (option != null) {
            price = option.data("price");
        }
        if (price == null) {
            price = $(this).find(".td-product-price").data("price");
        }
        price = (price == "" ? "0" : price);
        $(this).find(".td-product-price").text(price);
        $(this).find(".td-product-price").attr("data-price", price);
        //
        $(this).find("input.product-voucher").val(0);
        //tinh lai tien
        var quantity = $(this).find(".product-quantity").val(),
            voucher = $(this).find("input.product-voucher").val();
        price = price.toString().trim() != "" ? parseInt(price) : 0;
        quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
        voucher = voucher.trim() != "" ? parseInt(voucher) : 0;
        $(this).find(".box-money").text(FormatPrice(quantity * price * (100 - voucher) / 100)).show();
    });
    //
    TotalMoney();
    getProductIds();
    getServiceIds();
    ExcQuantity();
    UpdateItemOrder($("#table-item-product").find("tbody"));
}

function CancelPriceMembership() {
    $('#table-item-product').find("th").eq(4).text('Đơn giá');
    $('#table-item-product').find("th").eq(4).css('color', 'black');

    $('#table-item-service').find("th").eq(3).text('Đơn giá');
    $('#table-item-service').find("th").eq(3).css('color', 'black');
    //cap nhat gia cho service dang co ve gia ban dau
    $("#tbodyService").find("tr").each(function () {

        var tr = $("#table-service").find("input[data-code='" + $(this).find(".td-product-code").text() + "']").parent().parent();

        var price = tr.find(".td-product-price").data("price");
        //
        price = (price == "" ? "0" : price);
        $(this).find(".td-product-price").text(price);
        $(this).find(".td-product-price").attr("data-price", price);
        //
        //tinh lai tien
        var quantity = $(this).find(".product-quantity").val(),
            voucher = $(this).find("input.product-voucher").val();
        price = price.toString().trim() != "" ? parseInt(price) : 0;
        quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
        voucher = voucher.trim() != "" ? parseInt(voucher) : 0;
        var money = FormatPrice(quantity * price * (100 - voucher) / 100);
        $(this).find(".box-money").text(money).show();

    });
    //cap nhat gia cho product dang co
    $('#tbodyProduct').find("tr").each(function () {
        var option = $("#ddlProduct").find("option[data-code='" + $(this).find(".td-product-code").text() + "']");
        if (option != null) {
            var price = option.data("price");
            if (price != null) {
                price = (price == "" ? "0" : price);
                $(this).find(".td-product-price").text(price);
                $(this).find(".td-product-price").attr("data-price", price);
                //tinh lai tien
                var quantity = $(this).find(".product-quantity").val(),
                    voucher = $(this).find("input.product-voucher").val();
                price = price.toString().trim() != "" ? parseInt(price) : 0;
                quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
                voucher = voucher.trim() != "" ? parseInt(voucher) : 0;
                $(this).find(".box-money").text(FormatPrice(quantity * price * (100 - voucher) / 100)).show();
            }
        }
    });

}

function updateBillBuyMembership() {
    let dataClone = {
        billId: $("#HDF_BillId").val(),
        customerId: $("#HDF_CustomerId").val(),
        productIds: ($('#HDF_ProductIds').val() != "[]" ? $('#HDF_ProductIds').val() : "")
    }
    //alert(JSON.stringify(dataClone));
    $.ajax({
        type: "PUT",
        url: URL_API_CHECKOUT + "/api/checkout/UpdateBillBuyMembership",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(dataClone),
        dataType: "json",
        success: function (response) {
            //                
            if (response == "no content" || response == null) {
                showMsgMembership("Lỗi hệ thống. Thực hiện thử lại sau.", "warning");
            } else if (response == "Success") {
                $('#BarcodeStatus').val("bookingMember");
                //showMsg($("#MemberLabel"), "Áp dụng membership thành công.", "success");
                $("#MemberLabel").html("Áp dụng membership thành công.");
                //
                $("#membershipModal").modal("hide");
            } else {
                showMsgMembership("Lỗi hệ thống. Hãy thử lại sau.", "warning");
            }
        },
        error: function () {
            showMsgMembership("Lỗi hệ thống. Hãy thử lại sau.", "warning");
        },
        timeout: function () {
            showMsgMembership("Lỗi hệ thống. Hãy thử lại sau.", "warning");
        }
    });

}

function DisableCampaign() {
    campaignMktObj.listService = [];
    $(".MKT-label").html('');
    //
    if ($("#MemberType").val() == "1") {
        //ap dung
        ApplyMembership();
    } else {
        //mua the
        BuyMembership();
    }

    $("#MembershipDisableCampaignModal").modal("hide");
}

$(document).on("click", "#closeMembershipDisableCampaignModal", function () {
    $("#MembershipDisableCampaignModal").modal("hide");
});

function checkVoucher(This) {
    var voucherCodeTmp = This.val();
    if ('' === voucherCodeTmp) {
        cancelCampaign();
        campaignMktObj.listService = [];
        $(".MKT-label").html('');
        return;
    }

    // Lấy dữ liệu thông qua API
    $.ajax({
        //async: false,
        type: "POST",
        url: domainApiBillService + '/api/validate-campaign/validateVoucher',
        data: JSON.stringify({ "customerPhone": $("#HDF_CustomerCode").val(), "bookDate": $('#BillCreatedDate').val(), "voucher": voucherCodeTmp }),
        contentType: "application/json;charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (response.status) {
                if (response.listService != null && response.listService.length > 0) {
                    //
                    cancelCampaign();
                    //
                    campaignMktObj.campaign = response.campaign;
                    campaignMktObj.listService = response.listService;
                    campaignMktObj.bookingNote = response.bookingNote;
                    //
                    showMsg($("#MsgInputVoucher"), "Khách hàng được hưởng Voucher trong chương trình: " + campaignMktObj.campaign.label, "success");
                    //Tinh toan uu dai cho khach hang.
                    CalCampaignService(0, voucherCodeTmp);
                } else {
                    showMsg($("#MsgInputVoucher"), "Mã Voucher không đúng hoặc không đủ điều kiện sử dụng!!!", "warning");
                    cancelCampaign();
                    campaignMktObj.listService = [];
                    $(".MKT-label").html('');
                }
            }
        },
        failure: function (response) {
            alert(response);
        },
        timeout: function (response) {
            alert(response);
        },
        error: function (response) {
            alert(response);
        }
    });

}

function pressVoucher(e) {
    if (e.keyCode === 13) {
        checkVoucher($('#TextBoxVoucher'));
    }
}

function cancelCampaign() {
    $("#ListCampaignServices").val("");
    //xoa campaign tren tung ban ghi
    var listService = $("#table-item-service tbody tr");
    $.each(listService,
        function (u, v) {
            var tdServiceId = $(this).find(".td-product-code").data("id");
            var tdServiceVoucher = $(this).find("input.product-voucher");
            var DiscountPercenMax = 0;
            //
            $.each(campaignMktObj.listService,
                function (u1, v1) {
                    if (tdServiceId === v1.serviceId) {
                        if (DiscountPercenMax < v1.discountPercent) {
                            DiscountPercenMax = v1.discountPercent;
                        }
                    }
                });
            var tam = tdServiceVoucher.val();

            var Dom = $("#table-service")
                .find("input[data-code='" + $(this).find(".td-product-code").text() + "']").parent().parent()
                .clone().find("td:first-child").remove().end();
            //Lấy ServiceId
            var id = Dom.find(".td-product-code").attr("data-id");
            if (id === tdServiceId) {
                //set lai so tien can tra truoc, so tien duoc giam tru
                $(this).find(".td-product-price").attr("data-money-prepaid", "0");
                $(this).find(".td-product-price").attr("data-money-deductions", "0");
                $(this).find(".td-product-price").attr("data-campaign", "0");
                //
                var NumberVoucher = parseInt(tam) - parseInt(DiscountPercenMax);
                NumberVoucher = NumberVoucher < 0 ? 0 : (NumberVoucher > 100 ? 100 : NumberVoucher);
                tdServiceVoucher.val(NumberVoucher);
                tdServiceVoucher.change();

            }
        });
}

// check parameter
function CheckParameter(param) {
    if (param === undefined || param === null || param === '') {
        return false;
    }
    return true;
}

