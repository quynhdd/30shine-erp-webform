﻿var slot = 0,
    hour = 0,
    checkPermission;

jQuery(document).ready(function () {
    if (permissionSM === 'False') {
        $('#header').hide();
    }
    $('#HDF_WorkTimeId').val('');

    //checkbox for all
    $(".check-for-all").click(function () {
        $('.checkbox-checked').not(this).prop('checked', this.checked);
    });

    //hide popup notification
    $("#success-alert").hide();

    //========================
    $('#myModal').modal({
        backdrop: 'static',
        keyboard: false
    });

    $('#myModal').modal('toggle');
    //========================

    //============================
    //select 2
    $('.select').select2();
    //============================

    //============================
    //set width
    //============================
    $(window).bind('resize', function () {
        $("table#table-body").css("width", $("table#table-header").outerWidth(true));
        $("td.td-stt").css("width", $("th#th-stt").outerWidth(true));
        $("td.td-id").css("width", $("th#th-id").outerWidth(true));
        $("td.td-name").css("width", $("th#th-name").outerWidth(true));
        $("td.td-department").css("width", $("th#th-department").outerWidth(true));
        $("td.td-timekeeping").css("width", $("th#th-timekeeping").outerWidth(true));
    });
    //============================

    //============================
    //set width keeping
    //============================
    $(window).bind('resize', function () {
        $("table#table-body-timekeeping").css("width", $("table#table-header-timekeeping").outerWidth(true));
        $("td.td-hour-timekeeping").css("width", $("th#th-hour-timekeeping").outerWidth(true));
        $("td.td-stt-timekeeping").css("width", $("th#th-stt-timekeeping").outerWidth(true));
        $("td.td-id-timekeeping").css("width", $("th#th-id-timekeeping").outerWidth(true));
        $("td.td-name-timekeeping").css("width", $("th#th-name-timekeeping").outerWidth(true));
        $("td.td-department-timekeeping").css("width", $("th#th-department-timekeeping").outerWidth(true));
        $("td.td-work-hour").css("width", $("th#th-work-hour").outerWidth(true));
        $("td.td-work-date").css("width", $("th#th-work-date").outerWidth(true));
        $("td.td-flow-timekeeping").css("width", $("th#th-flow-timekeeping").outerWidth(true));
    });
    //============================

    //============================
    // Datepicker
    //============================
    $('.txtDateTime').datetimepicker({
        dayOfWeekStart: 1,
        lang: 'vi',
        startDate: '2014/10/10',
        format: 'd/m/Y',
        dateonly: true,
        showHour: false,
        showMinute: false,
        timepicker: false,
        onChangeDateTime: function (dp, $input) {
        }
    });
    //============================

    //============================
    //work time 
    //============================
    $.ajax({
        type: "GET",
        contentType: "application/json;charset:UTF-8",
        dataType: "JSON",
        url: url_timekeeping + "/api/work-time",
        success: function (list) {
            AppendWorkTime(list);
            AppendWorkTimePopup(list);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var responseText = jqXHR.responseJSON.message;
            finishLoading();
            NotificationPopup(`Lỗi: ${responseText}`, 'alert-danger', 2500);
        }
    });

    function AppendWorkTime(list) {
        let count = list.length,
            str = '';
        $('#work-time').html('');
        if (count > 0) {
            for (var i = 0; i < count; i++) {
                str += `<div class="col-md-2 btn do-work" onclick="SetWorktime(${list[i].id}, $(this));">
                            <span class="span-do-work">
                                <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                <b data-id="${list[i].id}" class="b-do-work">${list[i].name}</b>
                                <span class="time-do-work">${list[i].startTime} - ${list[i].endTime}</span>
                            </span>
                        </div>`;
            }
            $('#work-time').append(str);
        }
    }

    function AppendWorkTimePopup(list) {
        let count = list.length,
            str = '';
        $('#work-time-popup').html('');
        if (count > 0) {
            for (var i = 0; i < count; i++) {
                str += `<span class="div-work-time div-work-time${list[i].id} col-md-2" data-id="${list[i].id}">
                            <span class="span-work-time span-work-time${list[i].id}" data-id="${list[i].id}">${list[i].name}</span><br />
                            <span class="span-work-time span-work-time${list[i].id}" data-id="${list[i].id}">${list[i].startTime} - ${list[i].endTime}</span>
                        </span>`;
            }
            $('#work-time-popup').append(str);
        }
    }

    //==========================


    //==========================
    //get list don't timekeeping
    //==========================
    //==========================

    //==========================
    //get list timekeeping
    //==========================
    GetList();
    //==========================

    //============================
    //set width keeping
    //============================
    SetWidthTableTimekeeping();

    //============================
    //set width
    //============================
    SetWidthTableDoNotTimekeeping();
});


//============================
//set width
//============================
function SetWidthTableDoNotTimekeeping() {
    //setTimeout(function () {
    $("table#table-body").css("width", $("table#table-header").outerWidth(true));
    $("td.td-stt").css("width", $("th#th-stt").outerWidth(true));
    $("td.td-id").css("width", $("th#th-id").outerWidth(true));
    $("td.td-name").css("width", $("th#th-name").outerWidth(true));
    $("td.td-department").css("width", $("th#th-department").outerWidth(true));
    $("td.td-timekeeping").css("width", $("th#th-timekeeping").outerWidth(true));
    //}, 200);
}
//==========================

//============================
//set width keeping
//============================
function SetWidthTableTimekeeping() {
    //setTimeout(function () {
    $("table#table-body-timekeeping").css("width", $("table#table-header-timekeeping").outerWidth(true));
    $("td.td-hour-timekeeping").css("width", $("th#th-hour-timekeeping").outerWidth(true));
    $("td.td-stt-timekeeping").css("width", $("th#th-stt-timekeeping").outerWidth(true));
    $("td.td-id-timekeeping").css("width", $("th#th-id-timekeeping").outerWidth(true));
    $("td.td-name-timekeeping").css("width", $("th#th-name-timekeeping").outerWidth(true));
    $("td.td-department-timekeeping").css("width", $("th#th-department-timekeeping").outerWidth(true));
    $("td.td-work-hour").css("width", $("th#th-work-hour").outerWidth(true));
    $("td.td-flow-timekeeping").css("width", $("th#th-flow-timekeeping").outerWidth(true));
    //}, 200);
}


//==========================
//set work time
//==========================
function SetWorktime(Id, This) {
    This.parent().find(".active").removeClass("active");
    $(".fa-check-circle-o").css('display', 'none');
    //This.find(".b-do-work.active").removeClass("active");
    //This.find(".time-do-work.active").removeClass("active");
    This.addClass("active");
    This.find(".fa-check-circle-o").css('display', 'block');
    This.find(".b-do-work").addClass("active");
    This.find(".time-do-work").addClass("active");
    $("#HDF_WorkTimeId").val(Id);
}
//==========================

//==========================
//list staff don't timekeeping
//==========================
function GetList() {
    let error = false,
        $date = $('#txtWorkDate').val(),
        $salonId = $('#ddlSalonTimekeeping :selected').val(),
        $departmentId = $('#ddlDepartment :selected').val(),
        $date2 = $('#TxtDateTimeFrom').val(),
        $salonId2 = $('#ddlSalon :selected').val(),
        $departmentId2 = $('#ddlStaffType :selected').val();
    if ($date === undefined || $date === null || $date === '') {
        NotificationPopup('Vui lòng nhập ngày chấm công!', 'alert-warning', 2500);
        error = true;
    }
    else if ($salonId === undefined || $salonId === null || $salonId === '') {
        NotificationPopup(`Có lỗi xảy ra. Salon: ${$salonId}`, 'alert-warning', 2500);
        error = true;
    }
    else if ($departmentId === undefined || $departmentId === null || $departmentId === '') {
        NotificationPopup(`Có lỗi xảy ra. Bộ Phận: ${$departmentId}`, 'alert-warning', 2500);
        error = true;
    }
    if ($date2 === undefined || $date2 === null || $date2 === '') {
        NotificationPopup('Vui lòng nhập ngày chấm công!', 'alert-warning', 2500);
        error = true;
    }
    else if ($salonId2 === undefined || $salonId2 === null || $salonId2 === '') {
        NotificationPopup(`Có lỗi xảy ra. Salon: ${$salonId2}`, 'alert-warning', 2500);
        error = true;
    }
    else if ($departmentId2 === undefined || $departmentId2 === null || $departmentId2 === '') {
        NotificationPopup(`Có lỗi xảy ra. Bộ Phận: ${$departmentId2}`, 'alert-warning', 2500);
        error = true;
    }
    if (!error) {
        startLoading();
        $.ajax({
            type: "GET",
            contentType: "application/json;charset:UTF-8",
            dataType: "JSON",
            url: url_timekeeping + `/api/flowtimeKeeping/get-list-staff-not-timekeeping?salonId=${$salonId}&departmentId=${$departmentId}&workDate=${$date}`,
            success: function (list) {
                AppendListStaffDoNotTimekeeping(list);
                SetWidthTableDoNotTimekeeping();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var responseText = jqXHR.responseJSON.message;
                finishLoading();
                NotificationPopup(`Lỗi: ${responseText}`, 'alert-danger', 2500);
            }
        });
        $.ajax({
            type: "GET",
            contentType: "application/json;charset:UTF-8",
            dataType: "JSON",
            url: url_timekeeping + `/api/flowtimeKeeping/get-list-staff-timekeeping?salonId=${$salonId}&departmentId=${$departmentId}&workDate=${$date}`,
            success: function (list) {
                AppendListStaffTimekeeping(list);
                SetWidthTableTimekeeping();
                finishLoading();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var responseText = jqXHR.responseJSON.message;
                finishLoading();
                NotificationPopup(`Lỗi: ${responseText}`, 'alert-danger', 2500);
            }
        });
    }
}

//==========================

//==========================
//Append list staff do not timekeeping
//==========================
function AppendListStaffDoNotTimekeeping(list) {
    let str = '',
        index = 0,
        count = list.length,
        note = '';
    $('#body-content').html('');
    if (count > 0) {
        for (var i = 0; i < count; i++) {
            index++;
            note = '';
            if (list[i].noteStatusSalonCurrent !== undefined && list[i].noteStatusSalonCurrent !== null && list[i].noteStatusSalonCurrent !== '') {
                note = list[i].noteStatusSalonCurrent;
            }
            str += `<tr>
                        <td class="td-stt" style="height: 28px;">${index}</td>
                        <td class="td-salon-id" data-salon="${list[i].salonIdWorking}" style="display:none;height: 28px;">${list[i].salonId}</td>
                        <td class="td-id" style="height: 28px;" data-id="${list[i].id}">${list[i].id}</td>
                        <td class="td-name" style="height: 28px;" data-name="${list[i].fullName}">${list[i].fullName}  &nbsp; ${note}</td>
                        <td class="td-department-id" style="display:none;height: 28px;">${list[i].departmentId}</td>
                        <td class="td-department" style="height: 28px;">${list[i].departmentName}</td>
                        <td class="td-timekeeping" style="height: 28px;">
                            <div class="checkbox">
                                <label>
                                    <input class="checkbox-checked" type="checkbox" />
                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                </label>
                            </div>
                        </td>
                    </tr>`;
        }
        $('#body-content').append(str);
        SetPermissionTimekeeping();
    }
}
//==================================



//==========================
//Append list staff do not timekeeping
//==========================
function AppendListStaffTimekeeping(list) {
    let data = list;
    slot = 0;
    hour = 0;
    $('#body-content-timekeeping').html('');
    if (data !== undefined && data !== null) {
        if (data.ca01 !== undefined && data.ca01 !== null && data.ca01.length > 0) {
            ForListStaffTimekeeping(data.ca01, 'Ca 01', 'background: #f35304;');
        }
        if (data.ca1 !== undefined && data.ca1 !== null && data.ca1.length > 0) {
            ForListStaffTimekeeping(data.ca1, 'Ca 1', 'background: rgb(212, 210, 210, 0.3);');
        }
        if (data.ca2 !== undefined && data.ca2 !== null && data.ca2.length > 0) {
            ForListStaffTimekeeping(data.ca2, 'Ca 2', 'background: rgb(92, 184, 92, 0.3);');
        }
        if (data.ca21 !== undefined && data.ca21 !== null && data.ca21.length > 0) {
            ForListStaffTimekeeping(data.ca21, 'Ca 2.1', 'background: #049cf3;');
        }
        if (data.ca3 !== undefined && data.ca3 !== null && data.ca3.length > 0) {
            ForListStaffTimekeeping(data.ca3, 'Ca 3', 'background: rgb(252, 211, 68, 0.3);');
        }
        if (data.ca11 !== undefined && data.ca11 !== null && data.ca11.length > 0) {
            ForListStaffTimekeeping(data.ca11, 'Ca 1.1', 'background: rgb(113, 108, 95, 0.6);');
        }
        if (data.ca31 !== undefined && data.ca31 !== null && data.ca31.length > 0) {
            ForListStaffTimekeeping(data.ca31, 'Ca 3.1', 'background: rgb(188, 149, 11, 0.6);');
        }
    }
    SetPermissionTimekeeping();
}
//==========================

//=================================
//list staff time keeping
//=================================
function ForListStaffTimekeeping(data, WorkTime, Color) {
    let str = '',
        index = 0,
        note = '',
        status = '',
        arr = [];
    for (var i1 = 0; i1 < data.length; i1++) {
        index++;
        note = '';
        status = '';
        arr = [];
        if (data[i1].departmentId === 1) {
            if (data[i1].hourIds !== undefined && data[i1].hourIds !== null && data[i1].hourIds !== '') {
                arr = data[i1].hourIds.split(",");
                slot += arr.length;
                hour = Math.floor(slot / 2);
            }
        }
        if (data[i1].noteStatus !== undefined && data[i1].noteStatus !== null && data[i1].noteStatus !== '') {
            note = data[i1].noteStatus;
        }
        else if (data[i1].noteStatusSalonCurrent !== undefined && data[i1].noteStatusSalonCurrent !== null && data[i1].noteStatusSalonCurrent !== '') {
            note = data[i1].noteStatusSalonCurrent;
        }
        if (data[i1].staffStatus !== undefined && data[i1].staffStatus !== null && data[i1].staffStatus !== '') {
            if (note !== undefined && note !== null && note !== '') {
                status = ', ' + data[i1].staffStatus;
            }
            else {
                status = data[i1].staffStatus;
            }
        }
        if (i1 === 0 && index === 1) {
            str += `<tr>
                        <td class="td-hour-timekeeping" rowspan="${data.length}" style="vertical-align: middle !important;background:${data[i1].color};font-size:20px;">${WorkTime}</td>
                        <td class="td-stt-timekeeping" rowspan="1" style="${Color}">${index}</td>
                        <td class="td-id-timekeeping" rowspan="1" data-delete="0" style="${Color}">${data[i1].staffId}</td>
                        <td class="td-name-timekeeping" rowspan="1" data-name="${data[i1].staffName}" style="${Color}">${data[i1].staffName} &nbsp; ${note}${status}</td>
                        <td class="td-department-timekeeping" rowspan="1" style="${Color}">${data[i1].departmentName}</td>
                        <td class="td-work-hour" rowspan="1" style="${Color}">${data[i1].workHour}</td>
                        <td class="td-flow-timekeeping" rowspan="1" style="">
                            <div class="box-action">
                                <button type="button" class="btn btn-danger box-action-btn" onclick="DeleteTimekeeping($(this))">Huỷ chấm công</button>
                                <button type="button" class="btn btn-info box-action-btn" data-toggle="modal" data-backdrop="static" data-keyboard="false" onclick="ShowHourOver($(this))">Sửa chấm công</button>
                            </div>
                        </td>
                    </tr>`;
        }
        if (i1 > 0 && index > 1) {
            str += `<tr >
                        <td class="td-stt-timekeeping" rowspan="1" style="${Color}">${index}</td>
                        <td class="td-id-timekeeping" rowspan="1" data-delete="1" style="${Color}">${data[i1].staffId}</td>
                        <td class="td-name-timekeeping" rowspan="1" data-name="${data[i1].staffName}" style="${Color}">${data[i1].staffName} &nbsp; ${note}${status}</td>
                        <td class="td-department-timekeeping" rowspan="1" style="${Color}">${data[i1].departmentName}</td>
                        <td class="td-work-hour" rowspan="1" style="${Color}">${data[i1].workHour}</td>
                        <td class="td-flow-timekeeping" rowspan="1" style="">
                            <div class="box-action">
                                <button type="button" class="btn btn-danger box-action-btn" onclick="DeleteTimekeeping($(this))">Huỷ chấm công</button>
                                <button type="button" class="btn btn-info box-action-btn" data-toggle="modal" data-backdrop="static" data-keyboard="false" onclick="ShowHourOver($(this))">Sửa chấm công</button>
                            </div>
                        </td>
                    </tr>`;
        }
    }
    $('#span-slot').text(slot);
    $('#span-hour').text(hour);
    $('#body-content-timekeeping').append(str);
}
//=================================

//=================================
//noti 
//=================================
function NotificationPopup(content, classes, times) {
    $('#success-alert').removeAttr('class');
    $('#success-alert').addClass('alert');
    $('#success-alert').addClass(classes);
    $('#success-alert').text(content);
    $("#success-alert").fadeTo(times, times).slideUp(times, function () {
        $("#success-alert").slideUp(times);
    });
}
//==================================

//==================================
// click hour parttime
//==================================
function chooseHour(This) {
    if (This.hasClass("active")) {
        This.removeClass("active");
    }
    else if (This.hasClass("over-active")) {
        This.removeClass("over-active");
    }
    else {
        This.addClass("active");
    }
}
//==================================


//==================================
// edit hour overtime
//==================================
function ShowHourOver(This) {
    let error = false,
        Id = 0,
        $date = $('#TxtDateTimeFrom').val();
    if ($date === undefined || $date === null || $date === '') {
        NotificationPopup('Vui lòng nhập ngày chấm công!', 'alert-warning', 2500);
        error = true;
    }
    Id = This.parent().parent().parent().find('td.td-id-timekeeping').text().trim();
    if (Id === undefined || Id === null || Id === '') {
        NotificationPopup('Có lỗi xảy ra vui lòng liên hệ nhà phát triển!', 'alert-warning', 2500);
        error = true;
    }
    if (!error) {
        $.ajax({
            type: "GET",
            contentType: "application/json;charset:UTF-8",
            dataType: "JSON",
            url: url_timekeeping + `/api/flowtimeKeeping?staffId=${Id}&workDate=${$date}`,
            success: function (list) {
                AppendBookhourStaff(This, list, Id);
                $('#myModal').modal();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var responseText = jqXHR.responseJSON.message;
                finishLoading();
                NotificationPopup(`Lỗi: ${responseText}`, 'alert-danger', 2500);
            }
        });
    }
}
//==================================


//==================================
// Append list bookhour by staff
//==================================
function AppendBookhourStaff(This, list, Id) {
    let str = '',
        active = '',
        name = '',
        hour = list.hours;
    $('#list-book-hour').html('');
    if (hour !== undefined && hour !== null && hour.length > 0) {
        name = This.parent().parent().parent().find('td.td-name-timekeeping').text().trim();
        $('#span-staff-name').text('Nhân viên: ' + name);
        $('#span-staff-id').text(Id);
        $('#work-time-popup .div-work-time').removeClass('active');
        $('.span-work-time').removeClass('active');
        $('#work-time-popup .div-work-time' + list.workTimeId + '').addClass('active');
        $('.span-work-time' + list.workTimeId + '').addClass('active');
        for (var i = 0; i < hour.length; i++) {
            active = '';
            if (hour[i].iskeeping) {
                active = 'active';
                if (hour[i].isOverTime) {
                    active = 'over-active';
                }
            }
            str += `<a class='it_bookhour ${active}' data-hourid="${hour[i].hourId}" data-hourframe="${hour[i].hourFrame}" onclick="chooseHour($(this));">${hour[i].hour}</a>`;
        }
        $('#list-book-hour').append(str);
    }
    else {
        NotificationPopup(`Có lỗi xảy ra vui lòng liên hệ nhà phát triển`, 'alert-danger', 2500);
    }
}
//==================================

//==================================
//insert staff timekeeping
//==================================

function TransferMoveStaffOtherTimekeeping() {
    let error = false,
        $date = $('#TxtDateTimeFrom').val(),
        $salonId = $('#ddlSalon :selected').val(),
        $workTimeId = $("#HDF_WorkTimeId").val(),
        $userId = doUserId,
        $staffId = $('#ddlStaff :selected').val();
    SetPermissionTimekeeping();
    if (!checkPermission) {
        NotificationPopup('Bạn không có quyền thực hiện tác vụ này!', 'alert-danger', 2500);
        return;
    }
    else {
        startLoading();
        if ($date === undefined || $date === null || $date === '') {
            NotificationPopup('Vui lòng nhập ngày chấm công!', 'alert-warning', 2500);
            error = true;
            finishLoading();
        }
        else if ($salonId === undefined || $salonId === null || $salonId === '') {
            NotificationPopup(`Có lỗi xảy ra. Salon: ${$salonId}`, 'alert-warning', 2500);
            error = true;
            finishLoading();
        }
        else if ($workTimeId === undefined || $workTimeId === null || $workTimeId === '') {
            NotificationPopup('Vui lòng chọn ca làm việc!', 'alert-warning', 2500);
            error = true;
            finishLoading();
        }
        else if ($staffId === undefined || $staffId === null || $staffId === '') {
            NotificationPopup(`Có lỗi xảy ra. Nhân viên: ${$staffId}`, 'alert-warning', 2500);
            error = true;
            finishLoading();
        }
        if (!error) {
            var data = {
                salonId: parseInt($salonId),
                staffId: $staffId,
                workDate: $date,
                workTimeId: parseInt($workTimeId),
                doUserId: $userId
            };
            var dataSm = {
                salonId: parseInt($salonId),
                staffId: $staffId,
                workDate: $date,
                workTimeId: parseInt($workTimeId),
                doUserId: 0
            };
            var dataSalary = {
                salonId: parseInt($salonId),
                staffId: $staffId,
                workDate: $date,
                workHour: 0
            };
            CallAjaxInsertStaffTimekeeping(data, dataSm, dataSalary, 'other');
        }
        else {
            finishLoading();
        }
    }
}

function InsertStaffTimekeeping() {
    let error = false,
        $date = $('#txtWorkDate').val(),
        $salonId = $('#ddlSalonTimekeeping :selected').val(),
        $workTimeId = $("#HDF_WorkTimeId").val(),
        $userId = doUserId,
        Ids = '',
        Names = '';
    SetPermissionTimekeeping();
    if (!checkPermission) {
        NotificationPopup('Bạn không có quyền thực hiện tác vụ này!', 'alert-danger', 2500);
        return;
    }
    else {
        $('#body-content tr').find('input[type="checkbox"]:checked').each(function () {
            var This = $(this);
            let Id = This.parent().parent().parent().parent().find("td.td-id").attr("data-id");
            let Name = This.parent().parent().parent().parent().find("td.td-name").attr("data-name");
            let SalonId = This.parent().parent().parent().parent().find("td.td-salon-id").text().trim();
            let SalonIdWorking = This.parent().parent().parent().parent().find("td.td-salon-id").attr("data-salon");
            if (SalonId !== SalonIdWorking) {
                Names += Name + ', ';
            }
            Id = Id.toString().trim() !== "" ? parseInt(Id) : 0;
            Ids += Id + ',';
        });
        if ($date === undefined || $date === null || $date === '') {
            NotificationPopup('Vui lòng nhập ngày chấm công!', 'alert-warning', 2500);
            error = true;
            finishLoading();
        }
        else if ($salonId === undefined || $salonId === null || $salonId === '') {
            NotificationPopup(`Có lỗi xảy ra. Salon: ${$salonId}`, 'alert-warning', 2500);
            error = true;
            finishLoading();
        }
        else if ($workTimeId === undefined || $workTimeId === null || $workTimeId === '') {
            NotificationPopup('Vui lòng chọn ca làm việc!', 'alert-warning', 2500);
            error = true;
            finishLoading();
        }
        else if (Ids === undefined || Ids === null || Ids === '') {
            NotificationPopup('Vui lòng chọn nhân viên muốn chấm công!', 'alert-warning', 2500);
            error = true;
            finishLoading();
        }
        if (Names.endsWith(', ')) {
            Names = Names.slice(0, Names.length - 2);
        }
        if (!error && Names !== '') {
            $(".confirm-yn").openEBPopup();
            $("#EBPopup .confirm-yn-text").text(`Nhân viên ${Names} đang làm việc tại salon khác bạn có muốn tiếp tục?`);
            $("#EBPopup .yn-yes").bind("click", function () {
                if (!error) {
                    startLoading();
                    if (Ids.endsWith(',')) {
                        Ids = Ids.slice(0, Ids.length - 1);
                    }
                    var data = {
                        salonId: parseInt($salonId),
                        staffId: Ids,
                        workDate: $date,
                        workTimeId: parseInt($workTimeId),
                        doUserId: parseInt($userId)
                    };
                    var dataSm = {
                        salonId: parseInt($salonId),
                        staffId: Ids,
                        workDate: $date,
                        workTimeId: parseInt($workTimeId),
                        doUserId: 0
                    };
                    var dataSalary = {
                        salonId: parseInt($salonId),
                        staffId: Ids,
                        workDate: $date,
                        workHour: 0
                    };
                    CallAjaxInsertStaffTimekeeping(data, dataSm, dataSalary, 'main');
                    autoCloseEBPopup(0);
                }
            });
            $("#EBPopup .yn-no").bind("click", function () {
                $('.checkbox-checked').prop('checked', false);
                autoCloseEBPopup(0);
            });
        }
        else if (!error) {
            startLoading();
            if (Ids.endsWith(',')) {
                Ids = Ids.slice(0, Ids.length - 1);
            }
            var data = {
                salonId: parseInt($salonId),
                staffId: Ids,
                workDate: $date,
                workTimeId: parseInt($workTimeId),
                doUserId: parseInt($userId)
            };
            var dataSm = {
                salonId: parseInt($salonId),
                staffId: Ids,
                workDate: $date,
                workTimeId: parseInt($workTimeId),
                doUserId: 0
            };
            var dataSalary = {
                salonId: parseInt($salonId),
                staffId: Ids,
                workDate: $date,
                workHour: 0
            };
            CallAjaxInsertStaffTimekeeping(data, dataSm, dataSalary, 'main');
        }
    }
}


function CallAjaxInsertStaffTimekeeping(data, dataSm, dataSalary, salon) {
    $.ajax({
        type: "POST",
        data: JSON.stringify(data),
        contentType: "application/json;charset:UTF-8",
        dataType: "JSON",
        url: url_timekeeping + `/api/flowtimeKeeping/insert-time-keeping`,
        success: function (response) {
            finishLoading();
            if (response.status === 1) {
                $('.check-for-all').prop('checked', false);
                NotificationPopup('Hoàn tất thành công!', 'alert-success', 2500);
                CallAjaxInsertStaffSmTimekeeping(dataSm);
                CallAjaxInsertSalaryIncome(dataSalary);
            }
            else {
                $('.check-for-all').prop('checked', false);
                NotificationPopup('Hoàn tất thất bại!', 'alert-danger', 2500);
            }
            if (salon === 'main') {
                $('#span-do-work').click();
            }
            $('#ViewData').click();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            var responseText = jqXHR.responseJSON.message;
            finishLoading();
            NotificationPopup(`Lỗi: ${responseText}`, 'alert-danger', 2500);
        }
    });
}

function CallAjaxInsertStaffSmTimekeeping(data) {
    $.ajax({
        type: "POST",
        data: JSON.stringify(data),
        contentType: "application/json;charset:UTF-8",
        dataType: "JSON",
        url: url_timekeeping + `/api/sm-enrolltemp`,
        error: function (jqXHR, textStatus, errorThrown) {
            var responseText = jqXHR.responseJSON.message;
            NotificationPopup(`Lỗi table SM: ${responseText}`, 'alert-danger', 2500);
        }
    });
}

function CallAjaxInsertSalaryIncome(data) {
    $.ajax({
        type: "POST",
        data: JSON.stringify(data),
        contentType: "application/json;charset:UTF-8",
        dataType: "JSON",
        url: url_timekeeping + `/salary-income`,
        error: function (jqXHR, textStatus, errorThrown) {
            var responseText = jqXHR.responseJSON.message;
            NotificationPopup(`Lỗi lương: ${responseText}`, 'alert-danger', 2500);
        }
    });
}
//==================================

//==================================
//delete timekeeping
//==================================
function DeleteTimekeeping(This) {
    let error = false,
        $date = $('#TxtDateTimeFrom').val(),
        $salonId = $('#ddlSalon :selected').val(),
        $staffId = 0,
        $workHour = 0,
        id = This.parent().parent().parent().find('td.td-id-timekeeping').text().trim(),
        name = This.parent().parent().parent().find('td.td-name-timekeeping').attr("data-name"),
        hour = This.parent().parent().parent().find('td.td-work-hour').text().trim();
    SetPermissionTimekeeping();
    if (!checkPermission) {
        NotificationPopup('Bạn không có quyền thực hiện tác vụ này!', 'alert-danger', 2500);
        return;
    }
    else {
        if (id !== undefined && id !== null && id !== '') {
            $staffId = parseInt(id);
        }
        else if (hour !== undefined && hour !== null && hour !== '') {
            workHour = parseInt(hour);
        }
        else if ($date === undefined || $date === null || $date === '') {
            NotificationPopup('Vui lòng nhập ngày chấm công!', 'alert-warning', 2500);
            error = true;
        }
        else if ($salonId === undefined || $salonId === null || $salonId === '') {
            NotificationPopup(`Có lỗi xảy ra. Salon: ${$salonId}`, 'alert-warning', 2500);
            error = true;
        }
        if ($staffId === undefined || $staffId === null || $staffId === '') {
            NotificationPopup('Có lỗi xảy ra vui lòng liên hệ nhà phát triển!', 'alert-warning', 2500);
            error = true;
        }
        if ($workHour === undefined || $workHour === null || $workHour === '') {
            NotificationPopup('Có lỗi xảy ra vui lòng liên hệ nhà phát triển!', 'alert-warning', 2500);
            error = true;
        }
        if (!error) {
            var data = {
                salonId: parseInt($salonId),
                staffId: $staffId,
                workDate: $date,
                workHour: parseInt($workHour),
                isEnroll: true,
                workTimeId: 0
            };
            startLoading();
            $.ajax({
                type: "DELETE",
                data: JSON.stringify(data),
                contentType: "application/json;charset:UTF-8",
                dataType: "JSON",
                url: url_timekeeping + `/api/flowtimeKeeping`,
                success: function (data) {
                    finishLoading();
                    $('#span-do-work').click();
                    $('#ViewData').click();
                    if (data.status === 1) {
                        NotificationPopup('Hoàn tất thành công!', 'alert-success', 2500);
                        var dataSalary = {
                            salonId: parseInt($salonId),
                            staffId: parseInt($staffId),
                            workDate: $date,
                            workHour: 0
                        };
                        CallAjaxInsertSalaryIncome(dataSalary);
                    }
                    else {
                        NotificationPopup('Hoàn tất thất bại!', 'alert-danger', 2500);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    var responseText = jqXHR.responseJSON.message;
                    finishLoading();
                    NotificationPopup(`Lỗi: ${responseText}`, 'alert-danger', 2500);
                }
            });
        }
        else {
            finishLoading();
        }
    }
}

//==================================

//===============================
//Update Overtime
//===============================
function UpdateOvertime() {
    let error = false,
        $date = $('#TxtDateTimeFrom').val(),
        $salonId = $('#ddlSalon :selected').val(),
        $staffId = $('#span-staff-id').text(),
        $userId = doUserId,
        $HourIds = '',
        $HourFrame = '';
    SetPermissionTimekeeping();
    if (!checkPermission) {
        NotificationPopup('Bạn không có quyền thực hiện tác vụ này!', 'alert-danger', 2500);
        return;
    }
    else {
        $('#list-book-hour').find(".it_bookhour").each(function () {
            if ($(this).hasClass('active') || $(this).hasClass('over-active')) {
                var hour = parseInt($(this).attr('data-hourid'));
                $HourIds += hour + ',';
                var hourFrame = $(this).attr('data-hourframe');
                $HourFrame += hourFrame + ',';
            }
        });
        if ($HourIds === undefined && $HourIds === null && $HourIds === '') {
            NotificationPopup('Vui lòng chọn khung giờ chấm công!', 'alert-warning', 2500);
            error = true;
        }
        else if ($HourFrame === undefined && $HourFrame === null && $HourFrame === '') {
            NotificationPopup('Vui lòng chọn khung giờ chấm công!', 'alert-warning', 2500);
            error = true;
        }
        else if ($date === undefined || $date === null || $date === '') {
            NotificationPopup('Vui lòng nhập ngày chấm công!', 'alert-warning', 2500);
            error = true;
        }
        else if ($salonId === undefined || $salonId === null || $salonId === '') {
            NotificationPopup(`Có lỗi xảy ra. Salon: ${$salonId}`, 'alert-warning', 2500);
            error = true;
        }
        else if ($staffId === undefined || $staffId === null || $staffId === '') {
            NotificationPopup(`Có lỗi xảy ra. Nhân viên: ${$staffId}`, 'alert-warning', 2500);
            error = true;
        }
        if (!error) {
            startLoading();
            if ($HourIds.endsWith(',')) {
                $HourIds = $HourIds.slice(0, $HourIds.length - 1);
            }
            if ($HourFrame.endsWith(',')) {
                $HourFrame = $HourFrame.slice(0, $HourFrame.length - 1);
            }
            var data = {
                salonId: parseInt($salonId),
                staffId: parseInt($staffId),
                workDate: $date,
                hourIds: $HourIds,
                listHourFrame: $HourFrame,
                doUserId: parseInt($userId)
            };
            $.ajax({
                type: "PUT",
                data: JSON.stringify(data),
                contentType: "application/json;charset:UTF-8",
                dataType: "JSON",
                url: url_timekeeping + `/api/flow-time-keeping-part-time`,
                success: function (response) {
                    CloseModal();
                    finishLoading();
                    if (response.status === 1) {
                        NotificationPopup('Hoàn tất thành công!', 'alert-success', 2500);
                        var dataSalary = {
                            staffId: parseInt($staffId),
                            workDate: $date,
                            salonId: 0,
                            workHour: response.hour,
                            isEnroll: true,
                            workTimeId: 0
                        };
                        $.ajax({
                            type: "PUT",
                            data: JSON.stringify(dataSalary),
                            contentType: "application/json;charset:UTF-8",
                            dataType: "JSON",
                            url: url_timekeeping + `/salary-income/overtime-salary`,
                            error: function (jqXHR, textStatus, errorThrown) {
                                var responseText = jqXHR.responseJSON.message;
                                finishLoading();
                                NotificationPopup(`Lỗi lương: ${responseText}`, 'alert-danger', 2500);
                            }
                        });
                    }
                    else {
                        NotificationPopup('Hoàn tất thất bại!', 'alert-danger', 2500);
                    }
                    $('#ViewData').click();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    var responseText = jqXHR.responseJSON.message;
                    finishLoading();
                    NotificationPopup(`${responseText}`, 'alert-danger', 2500);
                }
            });
        }
    }
}
//===============================

//===============================
//Close modal
//===============================
function CloseModal() {
    $('#myModal').modal('toggle');
}
//===============================

function Cancel() {
    $('.checkbox-checked').prop('checked', false);
    $('.check-for-all').prop('checked', false);
}

function SetPermissionTimekeeping() {
    let $date = $('#txtWorkDate').val();
    $.ajax({
        type: "POST",
        contentType: "application/json;charset:UTF-8",
        dataType: "JSON",
        data: "{date: '" + $date + "'}",
        async: false,
        url: '/GUI/BackEnd/TimeKeeping/EnrollTimekeeping.aspx/CheckLimitTime',
        success: function (response) {
            if (!response.d) {
                $('.cr').css('background', '#ccc');
                $('.cr').css('background', '#ccc');
                $('.check-for-all').attr('disabled', true);
                $('.checkbox-checked').attr('disabled', true);
                $('.cr').addClass('cr-hover');
                $('.box-action-btn').attr('disabled', true);
                $('.box-action-btn').css('background', '#b3b3b3').css('border', '#b3b3b3');
                $('.box-action').addClass('box-action-btn-hover');
                Cancel();
            }
            else {
                $('.cr').css('background', '');
                $('.check-for-all').removeAttr('disabled');
                $('.cr').removeClass('cr-hover');
                $('.box-action').removeClass('box-action-btn-hover');
                $('.check-for-all').prop('checked', false);
            }
            checkPermission = response.d;
        }
    });
}

function OnchangeDatetime(This, Type) {
    let $date = This.val();
    if (Type === 'step1') {
        $('#txtWorkDate').val($date);
    }
    else if (Type === 'step2') {
        $('#TxtDateTimeFrom').val($date);
    }
}

function OnchangeSalon(Type) {
    if (Type === 'step1') {
        let $salon = $("#ddlSalon :selected").index(),
            $text = $("#ddlSalon :selected").text();
        $("#ddlSalonTimekeeping").get(0).selectedIndex = $salon;
        $('#select2-ddlSalonTimekeeping-container').text($text);
    }
    else if (Type === 'step2') {
        let $salon = $("#ddlSalonTimekeeping :selected").index(),
            $text = $("#ddlSalonTimekeeping :selected").text();
        $("#ddlSalon").get(0).selectedIndex = $salon;
        $('#select2-ddlSalon-container').text($text);
    }
}

function OnchangeDepartment(Type) {
    if (Type === 'step1') {
        let $staffType = $("#ddlStaffType :selected").index(),
            $text = $("#ddlStaffType :selected").text();
        $("#ddlDepartment").get(0).selectedIndex = $staffType;
        $('#select2-ddlDepartment-container').text($text);
    }
    else if (Type === 'step2') {
        let $department = $("#ddlDepartment :selected").index(),
            $text = $("#ddlDepartment :selected").text();
        $("#ddlStaffType").get(0).selectedIndex = $department;
        $('#select2-ddlStaffType-container').text($text);
    }
}