﻿////////////////////////////////////////
// SERVICE BILL PENDING
////////////////////////////////////////

jQuery(document).ready(function ()
{
	//============================
	// Set Active Menu
	//============================
	$("#glbService").addClass("active");

	//============================
	// Datepicker
	//============================
	$('.txtDateTime').datetimepicker({
		dayOfWeekStart: 1,
		lang: 'vi',
		startDate: '2014/10/10',
		format: 'd/m/Y',
		dateonly: true,
		showHour: false,
		showMinute: false,
		timepicker: false,
		onChangeDateTime: function (dp, $input) { }
	});

	//============================
	// Filter
	//============================
	$(".filter-item input[type='text']").bind("keydown", function (e)
	{
		if (e.keyCode == 13) {
			$("#ViewDataFilter").click();
		}
	});
	// Bind Suggestion
	Bind_Suggestion();

	//============================
	// Show System Message
	//============================ 
	var qs = getQueryStrings();
	showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);

	// click out of team color
	$('html').click(function ()
	{
		$(".team-color-edit-box").hide();
	});
	$('.pending-team-color').click(function (event)
	{
		event.stopPropagation();
	});

});

//============================
// Suggestion Functions
//============================
function Bind_Suggestion()
{
	$(".eb-suggestion").bind("keyup", function (e)
	{
		if (e.keyCode == 40) {
			UpDownListSuggest($(this));
		} else {
			Call_Suggestion($(this));
		}
	});
	$(".eb-suggestion").bind("focus", function ()
	{
		Call_Suggestion($(this));
	});
	$(".eb-suggestion").bind("blur", function ()
	{
		//Exc_To_Reset_Suggestion($(this));
	});
	$(window).bind("click", function (e)
	{
		if ((!e.target.className.match("eb-select") && e.target.parentElement.className.match("ul-listing-suggestion")) ||
			(!e.target.className.match("eb-select") && !e.target.parentElement.className.match("ul-listing-suggestion"))) {
			EBSelect_HideBox();
		}
	});
}

function UpDownListSuggest(This)
{
	var UlSgt = This.parent().find(".ul-listing-suggestion"),
		index = 0,
		LisLen = UlSgt.find(">li").length - 1,
		Value;

	This.blur();
	UlSgt.find(">li.active").removeClass("active");
	UlSgt.find(">li:eq(" + index + ")").addClass("active");

	$(window).unbind("keydown").bind("keydown", function (e)
	{
		if (e.keyCode == 40) {
			if (index == LisLen) return false;
			UlSgt.find(">li.active").removeClass("active");
			UlSgt.find(">li:eq(" + (++index) + ")").addClass("active");
			return false;
		} else if (e.keyCode == 38) {
			if (index == 0) return false;
			UlSgt.find(">li.active").removeClass("active");
			UlSgt.find(">li:eq(" + (--index) + ")").addClass("active");
			return false;
		} else if (e.keyCode == 13) {
			// Bind data to HDF Field
			var THIS = UlSgt.find(">li.active");
			//var Value = THIS.text().trim();
			var Value = THIS.attr("data-code");
			var dataField = This.attr("data-field");

			BindIdToHDF(THIS, Value, dataField, "#HDF_Suggestion_Code", "#HDF_Suggestion_Field", This);
			EBSelect_HideBox();
		}
	});
}

function Exc_To_Reset_Suggestion(This)
{
	var value = This.val();
	if (value == "") {
		$(".eb-suggestion").each(function ()
		{
			var THIS = $(this);
			var sgValue = THIS.val();
			if (sgValue != "") {
				BindIdToHDF(THIS, sgValue, THIS.attr("data-field"), "#HDF_Suggestion_Code", "#HDF_Suggestion_Field", THIS);
				return false;
			}
		});
	}
}

function Call_Suggestion(This)
{
	var text = This.val(),
		field = This.attr("data-field");
	Suggestion(This, text, field);
}

function Suggestion(This, text, field)
{
	var This = This;
	var text = text || "";
	var field = field || "";
	var InputDomId;
	var HDF_Sgt_Code = "#HDF_Suggestion_Code";
	var HDF_Sgt_Field = "#HDF_Suggestion_Field";

	if (text == "") return false;

	switch (field) {
		case "customer.name": InputDomId = "#CustomerName"; break;
		case "customer.phone": InputDomId = "#CustomerPhone"; break;
		case "customer.code": InputDomId = "#CustomerCode"; break;
		case "bill.code": InputDomId = "#BillCode"; break;
	}

	$.ajax({
		type: "POST",
		url: "/GUI/SystemService/Ajax/Suggestion.aspx/Suggestion_Customer",
		data: '{field : "' + field + '", text : "' + text + '"}',
		contentType: "application/json; charset=utf-8",
		dataType: "json", success: function (response)
		{
			var mission = JSON.parse(response.d);
			if (mission.success) {
				var OBJ = JSON.parse(mission.msg);
				if (OBJ.length > 0) {
					var lis = "";
					$.each(OBJ, function (i, v)
					{
						lis += "<li data-code='" + v.Customer_Code + "'" +
									 "onclick=\"BindIdToHDF($(this),'" + v.Customer_Code + "','" + field + "','" + HDF_Sgt_Code +
									 "','" + HDF_Sgt_Field + "','" + InputDomId + "')\">" +
									v.Value +
								"</li>";
					});
					This.parent().find("ul.ul-listing-suggestion").empty().append(lis).parent().show();
					This.parent().find(".eb-select-data").show();
				} else {
					This.parent().find(".eb-select-data").hide().find("ul.ul-listing-suggestion").empty();
				}
			} else {
				This.parent().find("ul.ul-listing-suggestion").empty();
				var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
				showMsgSystem(msg, "warning");
			}
		},
		failure: function (response) { alert(response.d); }
	});
}

function BindIdToHDF(THIS, Code, Field, HDF_Sgt_Code, HDF_Sgt_Field, Input_DomId)
{
	var text = THIS.text().trim();
	$("input.eb-suggestion").val("");
	$(HDF_Sgt_Code).val(Code);
	$(HDF_Sgt_Field).val(Field);
	$(Input_DomId).val(text);
	$(Input_DomId).parent().find(".eb-select-data").hide();

	// Auto post server
	$("#BtnFakeUP").click();
}

function EBSelect_HideBox()
{
	$(".eb-select-data").hide();
	$("ul.ul-listing-suggestion li.active").removeClass("active");
}

//============================
// Event delete
//============================
function del(This, code, name)
{
	var code = code || null,
		name = name || null,
		Row = This;
	if (!code) return false;

	// show EBPopup
	$(".confirm-yn").openEBPopup();
	$("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa hóa đơn mã [ " + name + " ] ?");

	$("#EBPopup .yn-yes").bind("click", function ()
	{
		$.ajax({
			type: "POST",
			url: "/GUI/SystemService/Ajax/Del.aspx/Delele_Bill",
			data: '{Id : "' + code + '"}',
			contentType: "application/json; charset=utf-8",
			dataType: "json", success: function (response)
			{
				var mission = JSON.parse(response.d);
				if (mission.success) {
					delSuccess();
					Row.remove();
				} else {
					delFailed();
				}
			},
			failure: function (response) { alert(response.d); }
		});
	});
	$("#EBPopup .yn-no").bind("click", function ()
	{
		autoCloseEBPopup(0);
	});
}

/// Update team color
function showBoxColorEdit(This)
{
	$(".pending-team-color.active").removeClass("active");
	if (This.hasClass("active")) {
		This.removeClass("active");
	} else {
		This.addClass("active");
	}
	$(".pending-team-color").each(function ()
	{
		if (!$(this).hasClass("active")) {
			$(this).parent().find(".team-color-edit-box").hide();
		}
	});
	This.parent().find(".team-color-edit-box").toggle();
}
function updateTeamColor(This, teamId, color, billId)
{
	This.parent().parent().find(".pending-team-color").css("cssText", "background:" + color + "!important");
	// update teamId
	$.ajax({
		type: "POST",
		url: "/GUI/SystemService/Ajax/Suggestion.aspx/Update_BillTeamId",
		data: '{billId : ' + billId + ', teamId : ' + teamId + '}',
		contentType: "application/json; charset=utf-8",
		dataType: "json", success: function (response)
		{
			var mission = JSON.parse(response.d);
			if (mission.success) {
				//
				$(".team-color-edit-box").hide();
			} else {
				delFailed();
			}
		},
		failure: function (response) { alert(response.d); }
	});
}

////////////////////////////////////////////////////////
// Đồng hồ thời gian chờ
////////////////////////////////////////////////////////
var totalSeconds = 0;
var minutesLabel;
var secondsLabel;

/**
* Hàm trigger hiển thị đồng hồ đếm thời gian chờ
*/
function triggerClock()
{
	clearAllInterval();

	$("table#tablePending tbody tr").each(function ()
	{
		var order = $(this).find("td.clock").attr("data-order");
		var createdTime = $(this).find("td.clock").attr("data-time");
		var inprocedureTime = $(this).find("td.clock").attr("data-inprocedure");
		var bookingTime = $(this).find("td.clock").attr("data-booking");

		//console.log(bookingTime);

		var createdTimeDate = getDateFromStringDatetime(createdTime);
		var bookingTimeDate = getDateFromStringDatetime(bookingTime);
		var now = Date.now();

		//console.log(createdTimeDate + " - " + bookingTimeDate + ' - ' + Date.now());

		if ($(this).hasClass("is-booking")) {
			if (bookingTimeDate != null && bookingTimeDate > createdTimeDate)
			{
				var totalSeconds = getTimeSecondFromDatetime(bookingTime);
				setInterval(function ()
				{
					++totalSeconds;
					$(".second-" + order).text(pad(totalSeconds % 60));
					$(".minute-" + order).text(pad(parseInt(totalSeconds / 60)));
				}, 1000);
			}
			else
			{
				var totalSeconds = getTimeSecondFromDatetime(createdTime);
				setInterval(function ()
				{
					++totalSeconds;
					$(".second-" + order).text(pad(totalSeconds % 60));
					$(".minute-" + order).text(pad(parseInt(totalSeconds / 60)));
				}, 1000);
			}
		}
		else {
			if (inprocedureTime != "") {
				var totalSeconds = TimeSecondDifferent(createdTime, inprocedureTime);
				$(".second-" + order).text(pad(totalSeconds % 60));
				$(".minute-" + order).text(pad(parseInt(totalSeconds / 60)));
			}
			else {
				//$(this).find("td.clock").empty();
				var totalSeconds = getTimeSecondFromDatetime(createdTime);
				setInterval(function ()
				{
					++totalSeconds;
					$(".second-" + order).text(pad(totalSeconds % 60));
					$(".minute-" + order).text(pad(parseInt(totalSeconds / 60)));
				}, 1000);
			}
		}
	});
}

/**
* Tính số giây từ 1 thời điểm trước đó đến hiện tại
*/
function getTimeSecondFromDatetime(datetime)
{
	//var createdTime = Date.parse(datetime).getTime() / 1000;
	var time1 = getDateFromStringDatetime(datetime);
	if (time1 != null) {
		return Math.floor(Date.now() / 1000) - time1.getTime()/1000 ;
	}
	else
	{
		return 0;
	}
}

/**
* Tính chênh lệch thời gian từ lúc khách bắt đầu được checkin đến lúc gội
*/
function TimeSecondDifferent(createdTime, inprocedureTime)
{
    try
    {
        var time1 = getDateFromStringDatetime(createdTime).getTime() / 1000;
        var time2 = getDateFromStringDatetime(inprocedureTime).getTime() / 1000;
        if (time1 != null && time2 != null) {
            return time2.getTime()/1000 - time1.getTime()/1000;
        }
        else
        {
            return 0;
        }
    }	
    catch(Ex)
    {
        return 0;
    }
}

/**
* Trả về kiểu datetime từ datetime string
*/
function getDateFromStringDatetime(stringDatetime)
{
	if (stringDatetime !== '')
	{
		//var time = '2/11/2017 12:22:15 PM';
		var datetime = new Date();
		var part = stringDatetime.split(/(\s)+/);

		var partDay = part[0].split(/(\/)+/);
		datetime.setDate(parseInt(partDay[2]));
		datetime.setMonth(parseInt(partDay[0]) - 1);
		datetime.setYear(parseInt(partDay[4]));

		var parts = stringDatetime.match(/(\d+):(\d+):(\d+) (AM|PM)/);
		if (parts) {
			var hours = parseInt(parts[1]),
				minutes = parseInt(parts[2]),
				seconds = parseInt(parts[3]),
				tt = parts[4];
			if (tt === 'PM' && hours < 12) hours += 12;
			datetime.setHours(hours, minutes, seconds, 0);
		}
		return datetime;
	}
	else
	{
		return null;
	}
}

/**
* Clear toàn bộ Inteval
*/
function clearAllInterval()
{
	var interval_id = window.setInterval("", 9999); // Get a reference to the last
	// interval +1
	for (var i = 1; i < interval_id; i++)
		window.clearInterval(i);
}

/**
* Định dạng thời gian theo "00:00"
*/
function pad(val)
{
	var valString = val + "";
	if (valString.length < 2) {
		return "0" + valString;
	}
	else {
		return valString;
	}
}

jQuery(document).ready(function ()
{
	// Xử lý mặc định khi Checkin/Checkout các salon đăng nhập vào hệ thống
	if ($("#ddlSalon").val() == "3" || $("#ddlSalon").val() == "5") {
		Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(triggerClock);
	}
	else {
		$(".item-inprocedure").hide();
		$(".item-timewait").hide();
		$(".item-skinner").hide();
	}

	// Đối với tài khoản root/admin
	$("#ddlSalon").bind("change", function ()
	{
		if ($("#ddlSalon").val() == "3" || $("#ddlSalon").val() == "5") {
			Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(triggerClock);
		}
		else {
			$(".item-inprocedure").hide();
			$(".item-timewait").hide();
			$(".item-skinner").hide();
		}
	});
});