﻿/*** Functions decrelation ***/

window._FN = {};
(function ($) {
    "use strict";

    var StaffSuggestion = function (list) {
        this.list = {};
        this.init(list);
    };

    // Prototype
    StaffSuggestion.prototype = {
        init: function (list) {
            this.addList(list);
            return this;
        },

        addList: function (list) {
            var list = typeof list === 'object' ? list : {};
            this.list = $.extend({}, this.list, list);
            
            return this;
        },

        getStaff: function (Type, OrderCode, AtLeast) {
            var OrderCode = OrderCode || 0;
            var AtLeast = AtLeast || false;
            
            if (typeof this.list[Type] === 'undefined') {
                return null;
            }

            for (var i in this.list[Type]) {
                var staff = this.list[Type][i];
                if ((!OrderCode && AtLeast) || parseInt(staff.OrderCode) === parseInt(OrderCode)) {
                    return staff;
                }
            }
            return null;
        },

        bind: function (staff, target) {
            var Target = $(target);
            var Faker = Target.parent().find(".fake-value");
            var Field = Target.data("field");

            if ( $.isEmptyObject(staff) !== true ) {
                Faker.text(Target.val() + "- " + staff.Fullname);
                $("#" + Field).val(staff.Id);
            }
            else {
                Faker.html(Target.val() + "- <span class=\"reset_element\" style=\"\">...</span>");
                $("#" + Field).val(0);
            }

            return this;
        },

        listen: function (TargetSelector, Event) {
            var Suggestion = this;
            var Event = Event || "";

            if (!Event || Event.trim().length < 1) {
                return this;
            }
            Event = /*"staffSuggestion."*/ Event.trim();

            $(window.document).off(Event).on(Event, TargetSelector, function (e) {
                var Target = $(e.target);
                var Value = Target.val().trim();
                var Type = Target.data("staff-type");
                
                (function (fn) {
                    if (fn._TimeOutSuggestion) {
                        clearTimeout(fn._TimeOutSuggestion);
                    }

                    if (!(Type && Value)) {
                        Target.parent().find(".fake-value").empty();
                    }

                    fn._TimeOutSuggestion = setTimeout(function () {
                        if (Type && Value) {
                            var staff = Suggestion.getStaff(Type, Value);
                            Suggestion.bind(staff, Target);
                        }
                        else {
                            $("#" + Target.data("field")).val(0);
                            Target.parent().find(".fake-value").empty();
                        }
                    }, 0);
                })(this);
                
            });

            return this;
        }
    };

    _FN.StaffSuggestion = new StaffSuggestion();
    $.fn.staffSuggestion = function (List, Event) {
        console.log(List);
        _FN.StaffSuggestion.addList(List).listen(this.selector, Event);
    };
})(jQuery);