﻿
// KHai bao domain api
var domainReportOperation = window.ReportOperation.domain;
var GetapiOperationReportToDay = window.ReportOperation.GetApiOperationReportToday;
var GetApiOperationReport = window.ReportOperation.GetApiOperationReport;
// script get, bind data
//var type = 2, salon = 2;
function viewDataByDate(This) {
    // time fillter
    var time = This.data('time');
    // uri khi lấy api data bang truy vấn
    var uriApi = "";
    var IdSalon = $("#Salon").val();
    var salonType = 0;
    if (IdSalon == 0) {
        salonType = 0;
    }
    else {
        salonType = 2;
    }
    // kieu fillter :
    //  type = 0 : lấy api data bang truy vấn logic
    //  type = 1 : lấy api data truy vấn statistictic nhiều ngày
    var type = This.data('type');
    $(".tag-date.active").removeClass("active");
    This.addClass("active");
    if (type === 0 || type === 1) {
        $("#TxtDateTimeTo").val(time);
        $("#TxtDateTimeFrom").val(time);
    };
    var CountDate = funcDatediff($("#TxtDateTimeFrom").val(), $("#TxtDateTimeTo").val()) + 1;      // cong them 1 ngay hien tai
    //console.log("CountDate", CountDate);
    if (CountDate < 1) {
        alert("Bạn vui lòng xem lại ngày chọn!");
        return;
    }
    if (type === 0) {
        uriApi = domainReportOperation + GetapiOperationReportToDay + "?";
    }
    else if (type === 2 || type === 1) {
        var FromDate = "";
        var ToDate = "";
        if ($("#TxtDateTimeFrom").val()) {
            FromDate = $("#TxtDateTimeFrom").val();
            if ($("#TxtDateTimeTo").val()) {
                ToDate = $("#TxtDateTimeTo").val()
            }
            else {
                $("#TxtDateTimeTo").val(FromDate);
                ToDate = FromDate;
            }
        }
        else {
            alert("Bạn chưa chọn ngày !");
        }
        uriApi = domainReportOperation + GetApiOperationReport + "?from=" + FromDate + "&to=" + ToDate + "&";
    }
    startLoading();
    $.ajax({
        type: "GET",
        url: uriApi + "type=" + salonType + "&Id=" + IdSalon,
        //async: false,
        //data: ,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (DataReport, textStatus, xhr) {
            $("#totalReport").css("display", "none");
            $("#table-detail").css("display", "none");

            if (xhr.status == 200) {
                dataProcessing(DataReport, CountDate);
                console.log("DataReport", DataReport);
            }
            else {
                alert(textStatus);
                finishLoading();
            }

        },
        error: function (objData, error) {
            $('#tbodyBCVH').html('');
            alert('Có lỗi xảy ra ! Vui lòng liên hệ với nhóm phát triển !');
            finishLoading();
        }
    });
};

function dataProcessing(DataReport, numberOfDay, ) {

    ResetValue();
    //var DataHeader = DataReport.headerServiceFollow;
    var DataToTal = DataReport.totalServiceFollow;
    var DataBodyReport = DataReport.dataBodyReport;
    console.log("DataBodyReport", DataBodyReport);
    // so salon co du lieu
    var CountSalon = DataReport.dataBodyReport.length;
    var dvtTrieu = 1000000; // đơn vị tính tiền triệu
    var dvtNghin = 1000; // đơn vị tính tiền nghìn
    var dvtTram = 100 // đơn vị tính %
    var Percent = "%";
    var Hour = 2;

    var strHTML = "";
    var str30Shine = "";

    var sumTotalIncome = 0 // Tong doanh thu
        , sumTotalServiceInCome = 0 // tong doanh thu dich vu
        , sumTotalProductIncome = 0 // tong doanh thu my pham
        , sumWorktimeStylist = 0// số Giờ làm việc
        , sumNumberOfTurns = 0 // so luong khach
        , sumNumberCustomerOld = 0 // so luong khach cu
        , sumNumberBillNotImg = 0 // so luong bill khong anh
        //, sumNumberBillKHL = 0 // so luong bill khong hai long
        , sumNumberBill1Star = 0 // so luong bill 1 sao
        , sumNumberBill2Star = 0 // so luong bill 2 sao
        , sumNumberBill3Star = 0 // so luong bill 3 sao
        , sumNumberBill4Star = 0 // so luong bill 4 sao
        , sumNumberBill5Star = 0 // so luong bill 5 sao
        , sumTotalStarNumber = 0 // tong so sao danhg gia
        , sumNumberErrorMonitoring = 0 // so luong loi giam sat
        , sumNumberStylistTimekeeping = 0 // so luong stylist cham cong
        , sumNumberSkinnerTimekeeping = 0 // so luong skinner cham cong
        , sumNumberStylist = 0 // so luong stylist chinh thuc
        , sumNumberCanleBooking = 0 // so luong huy book                   
        , sumOvertimeHoursStylist = 0 // Số giờ tăng ca của stylist
        , sumNumberSlotBooking = 0 // Tổng số slot được booking
        , sumNumberDeleteBooking = 0 // so luong xoa book online 
        , sumNumberBooking = 0 // Tổng số booking online
        , sumNumberBookingBefor = 0 // Tổng số bill đặt trước hoàn thành  bill service
        , sumNumberBillWaited = 0 // bill chờ lâu
        , sumTotalShineCombo = 0 // tong so luong billShinecombo
        , sumTotalKidCombo = 0 // tong so luong KidCombo
        , sumTotalProtein = 0 // tong so luong PROTEIN
        , sumTotalMask = 0 // tong so luong Tay da chet
        , sumTotalExFoliation = 0 // tong so luong dap mat na
        , sumTotalGroupUonDuoi = 0 // tong so luong group uon + duoi
        , sumTotalGroupColorCombo = 0; // tong so luong group Color ComBo ( Nhuộm )
    jQuery.each(DataBodyReport, function (key, item) {

        var KHL = item.numberBill1Star + item.numberBill2Star;
        var HL = item.numberBill3Star;
        var RHL = item.numberBill4Star + item.numberBill5Star;
        var totalRatting = item.numberBill1Star + item.numberBill2Star + item.numberBill3Star + item.numberBill4Star + item.numberBill5Star;
        sumTotalIncome = sumTotalIncome + item.totalIncome; // Tong doanh thu
        sumTotalServiceInCome = sumTotalServiceInCome + item.totalServiceInCome;// tong doanh thu dich vu
        sumTotalProductIncome = sumTotalProductIncome + item.totalProductIncome; // tong doanh thu my pham
        sumWorktimeStylist = sumWorktimeStylist + item.worktimeStylist;// số Giờ làm việc  
        sumNumberOfTurns = sumNumberOfTurns + item.numberOfTurns; // so luong khach
        sumNumberCustomerOld = sumNumberCustomerOld + item.numberCustomerOld;  // so luong khach cu
        sumNumberBillNotImg = sumNumberBillNotImg + item.numberBillNotImg; // so luong bill khong anh
        //sumNumberBillKHL = sumNumberBillKHL + item.numberBillKHL;  // so luong bill khong hai long
        sumNumberBill1Star = sumNumberBill1Star + item.numberBill1Star; // so luong bill 1 sao
        sumNumberBill2Star = sumNumberBill2Star + item.numberBill2Star; // so luong bill 2 sao
        sumNumberBill3Star = sumNumberBill3Star + item.numberBill3Star; // so luong bill 3 sao
        sumNumberBill4Star = sumNumberBill4Star + item.numberBill4Star; // so luong bill 4 sao
        sumNumberBill5Star = sumNumberBill5Star + item.numberBill5Star; // so luong bill 5 sao
        sumTotalStarNumber = sumTotalStarNumber + item.totalStarNumber; // tong so sao danhg gia
        sumNumberErrorMonitoring = sumNumberErrorMonitoring + item.numberErrorMonitoring; // so luong loi giam sat
        sumNumberStylistTimekeeping = sumNumberStylistTimekeeping + item.numberStylistTimekeeping; // so luong stylist cham cong
        sumNumberSkinnerTimekeeping = sumNumberSkinnerTimekeeping + item.numberSkinnerTimekeeping; // so luong skinner cham cong
        sumNumberStylist = sumNumberStylist + item.numberStylist; // so luong stylist chinh thuc
        sumNumberCanleBooking = sumNumberCanleBooking + item.numberCanleBooking; // so luong huy book   
        sumNumberDeleteBooking = sumNumberDeleteBooking + item.numberDeleteBooking  // so luong xoa book online 
        sumOvertimeHoursStylist = sumOvertimeHoursStylist + item.overtimeHoursStylist; // Số giờ tăng ca của stylist
        sumNumberSlotBooking = sumNumberSlotBooking + item.numberSlotBooking; // Tổng số slot được booking
        sumNumberBooking = sumNumberBooking + item.numberBooking; // Tổng số booking 
        sumNumberBookingBefor = sumNumberBookingBefor + item.numberBookingBefor;  // Tổng số bill đặt trước hoàn thành  bill service
        sumNumberBillWaited = sumNumberBillWaited + item.numberBillWaited;// bill chờ lâu
        sumTotalShineCombo = sumTotalShineCombo + item.totalShineCombo; // tong so luong billShinecombo
        sumTotalKidCombo = sumTotalKidCombo + item.totalKidCombo; // tong so luong KidCombo
        sumTotalProtein = sumTotalProtein + item.totalProtein; // tong so luong PROTEIN
        sumTotalMask = sumTotalMask + item.totalMask; // tong so luong Tay da chet
        sumTotalExFoliation = sumTotalExFoliation + item.totalExFoliation;// tong so luong dap mat na
        sumTotalGroupUonDuoi = sumTotalGroupUonDuoi + item.totalGroupUonDuoi; // tong so luong group uon + duoi
        sumTotalGroupColorCombo = sumTotalGroupColorCombo + item.totalGroupColorCombo; // tong so luong group Color ComBo ( Nhuộm )
        //console.log("test", ((item.totalServiceInCome / item.numberOfTurns).toFixed(2)));
        strHTML += ' <tr> ' +
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + item.salonName + ' </td >' +
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + formatPrice(item.numberOfTurns) + ' </td >' + // 1.Khach
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + formatPrice(item.totalShineCombo) + ' </td >' + //2.shine combo
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + formatPrice(item.totalKidCombo) + ' </td >' + // 3.kid combo
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + formatPrice(item.totalProtein) + ' </td >' + // 4.protein
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + formatPrice(item.totalGroupUonDuoi) + ' </td >' + // 5.uon
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + formatPrice(item.totalGroupColorCombo) + ' </td >' + // 6.nhuom
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + formatPrice(item.totalExFoliation) + ' </td >' + // 7.SkinShine
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + formatPrice(item.totalMask) + ' </td >' + // 8.Cool Shine
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + formatPrice((item.totalIncome / dvtNghin).toFixed(0)) + ' </td >' + // 9. Doanh thu
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + formatPrice((item.totalServiceInCome / dvtNghin).toFixed(0)) + ' </td >' + // 10. Dịch vụ
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + formatPrice((item.totalProductIncome / dvtNghin).toFixed(0)) + ' </td >' + // 11. Mỹ phẩm
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center; ' + ReturnWaring("DTDVOnKhach", ((item.totalServiceInCome / item.numberOfTurns).toFixed(2))).Style + ' " >' + formatPrice((item.totalServiceInCome / item.numberOfTurns).toFixed(2)) + ' </td >' + // 12.DT DV/ Khách
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center; ' + ReturnWaring("DTMPOnKhach", ((item.totalProductIncome / item.numberOfTurns).toFixed(2))).Style + '" >' + formatPrice((item.totalProductIncome / item.numberOfTurns).toFixed(2)) + ' </td >' + // 13.DT MP/Khách
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + formatPrice((item.numberStylistTimekeeping / numberOfDay).toFixed(2)) + ' </td >' + // 14.Stylist
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + formatPrice((item.numberSkinnerTimekeeping / numberOfDay).toFixed(2)) + ' </td >' + // 15.Skinner
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + formatPrice((item.worktimeStylist + item.overtimeHoursStylist) / Hour) + ' </td >' + // 16.Tổng giờ làm
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + (RHL / totalRatting * dvtTram).toFixed(2) + " - " + (HL / totalRatting * dvtTram).toFixed(2) + ' - ' + (KHL / totalRatting * dvtTram).toFixed(2) + ' </td >' + // 17. Chất lượng (%)
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + (item.numberCustomerOld / item.numberOfTurns * dvtTram).toFixed(2) + ' </td >' + // 18. % khách cũ
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + (item.numberBillWaited / item.numberOfTurns * dvtTram).toFixed(2) + ' </td >' + // 19. % khách chờ
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center; ' + ReturnWaring("NotImg", ((item.numberBillNotImg / item.numberOfTurns * dvtTram).toFixed(2))).Style + ' " >' + (item.numberBillNotImg / item.numberOfTurns * dvtTram).toFixed(2) + ' </td >' + // 20. % Không ảnh
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center; ' + ReturnWaring("bookingBefor", ((item.numberBookingBefor / item.numberOfTurns * dvtTram).toFixed(2))).Style + ' " >' + (item.numberBookingBefor / item.numberOfTurns * dvtTram).toFixed(2) + ' </td >' + // 21. % Đặt trước
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center; ' + ReturnWaring("CanleBooking", (((item.numberDeleteBooking + item.numberCanleBooking) / item.numberSlotBooking * dvtTram).toFixed(2))).Style + ' " >' + ((item.numberDeleteBooking + item.numberCanleBooking) / item.numberSlotBooking * dvtTram).toFixed(2) + ' </td >' + // 22. % Hủy lịch
            '</tr >';
    });

    var sumRHL = sumNumberBill4Star + sumNumberBill5Star;
    var sumHL = sumNumberBill3Star;
    var sumKHL = sumNumberBill1Star + sumNumberBill2Star;
    var sumTotalRatting = sumNumberBill1Star + sumNumberBill2Star + sumNumberBill3Star + sumNumberBill4Star + sumNumberBill5Star;
    str30Shine = ' <tr> ' +
        '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > 30Shine </td >' +
        '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + formatPrice(sumNumberOfTurns) + ' </td >' + // 1.Khach
        '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + formatPrice(sumTotalShineCombo) + ' </td >' + //2.shine combo
        '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + formatPrice(sumTotalKidCombo) + ' </td >' + // 3.kid combo
        '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + formatPrice(sumTotalProtein) + ' </td >' + // 4.protein
        '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + formatPrice(sumTotalGroupUonDuoi) + ' </td >' + // 5.uon
        '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + formatPrice(sumTotalGroupColorCombo) + ' </td >' + // 6.nhuom
        '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + formatPrice(sumTotalExFoliation) + ' </td >' + // 7.SkinShine
        '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + formatPrice(sumTotalMask) + ' </td >' + // 8.Cool Shine
        '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + formatPrice((sumTotalIncome / dvtNghin).toFixed(0)) + ' </td >' + // 9. Doanh thu
        '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + formatPrice((sumTotalServiceInCome / dvtNghin).toFixed(0)) + ' </td >' + // 10. Dịch vụ
        '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + formatPrice((sumTotalProductIncome / dvtNghin).toFixed(0)) + ' </td >' + // 11. Mỹ phẩm
        '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center; ' + ReturnWaring("DTDVOnKhach", ((sumTotalServiceInCome / sumNumberOfTurns).toFixed(2))).Style + ' " >' + formatPrice((sumTotalServiceInCome / sumNumberOfTurns).toFixed(2)) + ' </td >' + // 12.DT DV/ Khách
        '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;' + ReturnWaring("DTMPOnKhach", ((sumTotalProductIncome / sumNumberOfTurns).toFixed(2))).Style + '" >' + formatPrice((sumTotalProductIncome / sumNumberOfTurns).toFixed(2)) + ' </td >' + // 13.DT MP/Khách
        '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + formatPrice((sumNumberStylistTimekeeping / numberOfDay).toFixed(2)) + ' </td >' + // 14.Stylist
        '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + formatPrice((sumNumberSkinnerTimekeeping / numberOfDay).toFixed(2)) + ' </td >' + // 15.Skinner
        '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + formatPrice((sumWorktimeStylist + sumOvertimeHoursStylist) / Hour) + ' </td >' + // 16.Tổng giờ làm
        '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + (sumRHL / sumTotalRatting * dvtTram).toFixed(2) + " - " + (sumHL / sumTotalRatting * dvtTram).toFixed(2) + ' - ' + (sumKHL / sumTotalRatting * dvtTram).toFixed(2) + ' </td >' + // 17. Chất lượng (%)
        '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + (sumNumberCustomerOld / sumNumberOfTurns * dvtTram).toFixed(2) + ' </td >' + // 18. % khách cũ
        '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + (sumNumberBillWaited / sumNumberOfTurns * dvtTram).toFixed(2) + ' </td >' + // 19. % khách chờ
        '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center; ' + ReturnWaring("NotImg", ((sumNumberBillNotImg / sumNumberOfTurns * dvtTram).toFixed(2))).Style + ' " >' + (sumNumberBillNotImg / sumNumberOfTurns * dvtTram).toFixed(2) + ' </td >' + // 20. % Không ảnh
        '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center; ' + ReturnWaring("bookingBefor", ((sumNumberBookingBefor / sumNumberOfTurns * dvtTram).toFixed(2))).Style + ' " >' + (sumNumberBookingBefor / sumNumberOfTurns * dvtTram).toFixed(2) + ' </td >' + // 21. % Đặt trước
        '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center; ' + ReturnWaring("CanleBooking", (((sumNumberDeleteBooking + sumNumberCanleBooking) / sumNumberSlotBooking * dvtTram).toFixed(2))).Style + ' " >' + ((sumNumberDeleteBooking + sumNumberCanleBooking) / sumNumberSlotBooking * dvtTram).toFixed(2) + ' </td >' + // 22. % Hủy lịch
        '</tr >';

    $('#tbodyBCVH').append(str30Shine).append(strHTML);
    // xu ly lay du lieu cuoi
    //=========Doanh Thu ===============
    var HoursumWorktimeStylist = sumWorktimeStylist / Hour;
    //bind tong DOANH THU (1)
    var stringTotalIncome = formatPrice((sumTotalIncome / dvtTrieu).toFixed(2));
    $("#totalIncome").html(stringTotalIncome);
    //bind tong doanh thu dich vu (2)
    var stringTotalServiceInCome = formatPrice((sumTotalServiceInCome / dvtTrieu).toFixed(2)) + " triệu";
    $("#totalServiceIncome").html(stringTotalServiceInCome);
    //bind tong doanh thu my pham (3)
    var stringTotalProductIncome = formatPrice((sumTotalProductIncome / dvtTrieu).toFixed(2)) + " triệu";
    $("#totalProductIncome").html(stringTotalProductIncome);
    // bind DT/khách (4)
    var DTOnKhach = (sumTotalIncome / (sumNumberOfTurns * dvtNghin)).toFixed(2);
    //console.log("DTOnKhach", DTOnKhach);
    var stringtotalIncomePercentCus = formatPrice(DTOnKhach) + " k";
    $("#totalIncomePercentCus").html(stringtotalIncomePercentCus).addClass(ReturnWaring("DTOnKhach", DTOnKhach, "Nghin").class);
    $("#span-totalIncomePercentCus").addClass(ReturnWaring("DTOnKhach", DTOnKhach, "Nghin").class);
    //console.log("ReturnWaring", ReturnWaring("DTOnKhach", DTOnKhach, "Nghin").class)
    //bind DTDV/khách (5)
    var DTDVOnKhach = (sumTotalServiceInCome / (sumNumberOfTurns * dvtNghin)).toFixed(2);
    var stringtotalServiceIncomePercentCus = formatPrice(DTDVOnKhach) + " k";
    $("#totalServiceIncomePercentCus").html(stringtotalServiceIncomePercentCus).addClass(ReturnWaring("DTDVOnKhach", DTDVOnKhach, "Nghin").class);
    $("#span-totalServiceIncomePercentCus").addClass(ReturnWaring("DTDVOnKhach", DTDVOnKhach, "Nghin").class);
    //bind DTMP/khách (6)
    var DTMPOnKhach = (sumTotalProductIncome / (sumNumberOfTurns * dvtNghin)).toFixed(2)
    var stringtotalProductIncomePercentCus = formatPrice(DTMPOnKhach) + " k";
    $("#totalProductIncomePercentCus").html(stringtotalProductIncomePercentCus).addClass(ReturnWaring("DTMPOnKhach", DTMPOnKhach, "Nghin").class);
    $("#span-totalProductIncomePercentCus").addClass(ReturnWaring("DTMPOnKhach", DTMPOnKhach, "Nghin").class);

    //========= Khách ==============
    // so khách (7)
    var stringnumberOfTurns = formatPrice(sumNumberOfTurns);
    $("#numberOfTurns").html(stringnumberOfTurns);
    // Xu ly  service auto
    //jQuery.each(DataToTal, function (key, item) {
    //    $("#valueServiceAuto").append("<p class=\"p-value-body-right\">" + (item.totalServiceNumber / sumNumberOfTurns * dvtTram).toFixed(2) + Percent + "</p>");
    //});

    jQuery.each(DataToTal, function (key, item) {
        $("#ServiceAuto").append("<div class=\"row-content\"><p class= \"p-name-body-report \" >   <span class=\"" + ReturnWaringService(item.id, ((item.totalServiceNumber / sumNumberOfTurns * dvtTram).toFixed(2)), "Nghin").class + "\"> " + item.serviceName + ":</span> </p ><p class=\"p-value-body-right  " + ReturnWaringService(item.id, ((item.totalServiceNumber / sumNumberOfTurns * dvtTram).toFixed(2)), "Nghin").class + "\" id=\"totalServiceIncome\">" + (item.totalServiceNumber / sumNumberOfTurns * dvtTram).toFixed(2) + Percent + "</p></div>");
    });
    //========= Năng Suất ==========

    // nghìn/stylist /h (13)
    var stringproductivity = (sumTotalIncome / (HoursumWorktimeStylist + (sumOvertimeHoursStylist / Hour)) / dvtNghin).toFixed(2); // DT Dịch vụ (2) / ( số Giờ làm việc (28)+ Số giờ tăng ca của stylist (29) )
    $("#productivity").html(stringproductivity);
    // DT/St/day (14)
    var stringDTStday = (sumTotalIncome / sumNumberStylistTimekeeping / dvtTrieu).toFixed(2) + " triệu"; // tong  DT (1) / so luong stylist cham cong )
    $("#DT-St-day").html(stringDTStday);
    // DT/Sl/day (15) 
    var stringDTSlday = formatPrice((sumTotalIncome / (numberOfDay * CountSalon * dvtTrieu)).toFixed(2)) + " triệu"; // tong  DT (1) / ( khoang ngay chọn * Số salon )  ( đơn vụi triệu )
    $("#DT-Sl-day").html(stringDTSlday);
    // Khách/St/h (16)
    var stringKhachSth = (sumNumberOfTurns / (HoursumWorktimeStylist + (sumOvertimeHoursStylist / Hour))).toFixed(2); // So khach (7) / ( số Giờ làm việc (28) + Số giờ tăng ca của stylist (29));
    $("#Khach-St-h").html(stringKhachSth);
    // Khách/St/day (17) note lai
    var stringKhachStday = (sumNumberOfTurns / sumNumberStylistTimekeeping).toFixed(2); // So khach (7) / so luong stylist cham cong
    $("#Khach-St-day").html(stringKhachStday);
    // Khách/Sl/day (18)
    var stringKhachSlday = formatPrice((sumNumberOfTurns / (numberOfDay * CountSalon)).toFixed(2));  // so khach (7) / ((số ngày trong khoảng thời gian đc chọn) * (số salon đc chọn))
    $("#Khach-Sl-day").html(stringKhachSlday);

    //========== Chất lượng ==========
    // Điểm chất lượng trung bình  (19)
    var stringSao = (sumTotalStarNumber / sumNumberOfTurns).toFixed(2);  //(tổng số sao tất cả bill dịch vụ trong tất cả salon đc chọn, trong khoảng thời gian đc chọn) / so khách (7)
    $("#sao").html(stringSao);

    //Tỉ lệ khách cũ (20)
    var stringKhachCu = formatPrice(sumNumberCustomerOld) + " (" + (sumNumberCustomerOld / sumNumberOfTurns * dvtTram).toFixed(2) + Percent + " )"; // so luong khach cu / so khach (7)
    $("#khach-cu").html(stringKhachCu);

    // tỉ lệ khách chờ lâu (21)
    var PercentWaited = (sumNumberBillWaited / sumNumberOfTurns * dvtTram).toFixed(2);
    var stringChoLau = formatPrice(sumNumberBillWaited) + " ( " + PercentWaited + Percent + " )"; //  bill chờ lâu / so khach (7)
    $("#cho-lau").html(stringChoLau).addClass(ReturnWaring("Waited", PercentWaited).class);
    $("#span-cho-lau").addClass(ReturnWaring("Waited", PercentWaited).class);
    // khong anh (22)
    var PercentNotImg = (sumNumberBillNotImg / sumNumberOfTurns * dvtTram).toFixed(2);
    var stringKhongAnh = formatPrice(sumNumberBillNotImg) + " ( " + PercentNotImg + Percent + ")"; // so luong bill khong anh / so khach (7)
    $("#khong-anh").html(stringKhongAnh).addClass(ReturnWaring("NotImg", PercentNotImg).class);
    $("#span-khong-anh").addClass(ReturnWaring("NotImg", PercentNotImg).class);
    // 1-2* (23)
    var totalNumberBillKHL = sumNumberBill1Star + sumNumberBill2Star;
    var stringSao12 = formatPrice(totalNumberBillKHL) + " ( " + (totalNumberBillKHL / sumNumberOfTurns * dvtTram).toFixed(2) + Percent + " )"; // so luong bill khong hai long / so khach (7)
    $("#sao1-2").html(stringSao12);

    // Lỗi giám sát (24)
    var stringLoiGS = formatPrice(sumNumberErrorMonitoring) + " ( " + (sumNumberErrorMonitoring / sumNumberOfTurns * dvtTram).toFixed(2) + Percent + " )";  // so luong loi giam sat / so khach (7)
    $("#Loi-GS").html(stringLoiGS);

    //============ Nhân sự ===========

    // Stylist đi làm (25)
    var PercentStylistDiLam = (sumNumberStylistTimekeeping / sumNumberStylist * dvtTram).toFixed(2)
    var stringStylistDiLam = PercentStylistDiLam + Percent; //  so luong stylist cham cong   / so luong stylist chinh thuc
    $("#Stylist-di-lam").html(stringStylistDiLam).addClass(ReturnWaring("StylistTimekeeping", PercentStylistDiLam).class);
    $("#span-Stylist-di-lam").addClass(ReturnWaring("StylistTimekeeping", PercentStylistDiLam).class);

    // Stylist (26)
    var stringStylist = formatPrice((sumNumberStylistTimekeeping / numberOfDay).toFixed(2));
    $("#Stylist").html(stringStylist);

    // Skinner (27)
    var stringSkinner = formatPrice((sumNumberSkinnerTimekeeping / numberOfDay).toFixed(2));
    $("#Skinner").html(stringSkinner);

    // Giờ làm (28)
    var stringGioLam = formatPrice(HoursumWorktimeStylist) + " h";
    $("#GioLam").html(stringGioLam);

    // Tăng ca (29)
    var stringTangCa = formatPrice(sumOvertimeHoursStylist / Hour) + " h";
    $("#TangCa").html(stringTangCa);

    //========= Booking ===========
    // Book kín slot (30)
    var stringBookKinSlot = (sumNumberBooking / sumNumberSlotBooking * 100).toFixed(2) + Percent; // Tổng số booking  / Tổng số slot được booking
    $("#BookKinSlot").html(stringBookKinSlot);

    // slot (31)
    var stringSlot = formatPrice(sumNumberSlotBooking); // Tổng số slot được booking
    $("#Slot").html(stringSlot);

    // Hủy lịch (32)
    var PercentCanleBooking = ((sumNumberCanleBooking + sumNumberDeleteBooking) / sumNumberSlotBooking * dvtTram).toFixed(2);
    var stringHuyLich = PercentCanleBooking + Percent;
    $("#HuyLich").html(stringHuyLich).addClass(ReturnWaring("CanleBooking", PercentCanleBooking).class);
    $("#span-HuyLich").addClass(ReturnWaring("CanleBooking", PercentCanleBooking).class);

    // Đặt trước (33)
    var PercentBeforBooking = (sumNumberBookingBefor / sumNumberOfTurns * dvtTram).toFixed(3)
    var stringDatTruoc = PercentBeforBooking + Percent;
    $("#DatTruoc").html(stringDatTruoc).addClass(ReturnWaring("bookingBefor", PercentBeforBooking).class);
    $("#span-DatTruoc").addClass(ReturnWaring("bookingBefor", PercentBeforBooking).class);
    // hien thi html 
    $("#totalReport").css("display", "block");
    $('html,body').animate({
        scrollTop: $("#totalReport").offset().top
    },
        'slow');
    finishLoading();

};
// Xuat file excel
function ExportExcel(This) {
    // function export excel
    funcExportExcelHtml("#table-detail", "BCVH");
};

function ResetValue() {
    $('#tbodyBCVH').html('');
    $("#totalIncome").html("");
    $("#totalServiceIncome").html("");
    $("#totalProductIncome").html("");
    $("#totalIncomePercentCus").html("");
    $("#totalServiceIncomePercentCus").html("");
    $("#totalProductIncomePercentCus").html("");
    $("#nameServiceAuto").html("");
    $("#valueServiceAuto").html("");
    $("#DT-St-day").html("");
    $("#DT-Sl-day").html("");
    $("#Khach-St-h").html("");
    $("#Khach-St-day").html("");
    $("#Khach-Sl-day").html("");
    $("#sao").html("");
    $("#khach-cu").html("");
    $("#cho-lau").html("");
    $("#khonganh").html("");
    $("#sao1-2").html("");
    $("#Loi-GS").html("");
    $("#BookKinSlot").html("");
    $("#Slot").html("");
    $("#HuyLich").html("");
    $("#DatTruoc").html("");
    $("#ServiceAuto").html("");
    var Viewdetail = $("#viewDetail");
    Viewdetail.text("Xem chi tiết");
    Viewdetail.removeClass("active");
    $(".value-hight, .des-value, .p-name-body-report span, .p-value-body-right").removeClass("blue warning danger  ");

};

function ReturnWaring(IndexName, ValueIndex, DVTNghin) {
    try {
        var dvtDong = 1000;
        if (DVTNghin === "Nghin") {
            dvtDong = 1;
        }
        else {
            dvtDong = 1000;
        }

        var ValueRead = parseFloat(ValueIndex);

        var objWaring = new Object();
        switch (IndexName) {
            case "DTOnKhach": // .4
                if (ValueRead > (170 * dvtDong)) {
                    objWaring.class = "blue";
                    objWaring.Style = "background: #00ff00";
                }
                else if (ValueRead >= (165 * dvtDong) && ValueRead <= (170 * dvtDong)) {
                    objWaring.class = "warning";
                    objWaring.Style = "background: #ffff00";
                }
                else if (ValueRead < (165 * dvtDong)) {
                    objWaring.class = "danger";
                    objWaring.Style = "background: #db0000";
                }
                break;
            case "DTDVOnKhach": //.5
                if (ValueRead > (152 * dvtDong)) {
                    objWaring.class = "blue";
                    objWaring.Style = "background: #00ff00";
                }
                else if (ValueRead >= (148 * dvtDong) && ValueRead <= (152 * dvtDong)) {
                    objWaring.class = "warning";
                    objWaring.Style = "background: #ffff00";
                }
                else if (ValueRead < (148 * dvtDong)) {
                    objWaring.class = "danger";
                    objWaring.Style = "background: #db0000";
                }
                break;
            case "DTMPOnKhach": //.6
                if (ValueRead > (20 * dvtDong)) {
                    objWaring.class = "blue";
                    objWaring.Style = "background: #00ff00";
                }
                else if (ValueRead >= (17 * dvtDong) && ValueRead <= (20 * dvtDong)) {
                    objWaring.class = "warning";
                    objWaring.Style = "background: #ffff00";
                }
                else if (ValueRead < (17 * dvtDong)) {
                    objWaring.class = "danger";
                    objWaring.Style = "background: #db0000";
                }
                break;
            case "Waited": //.21
                if (ValueRead < 1.5) {
                    objWaring.class = "blue";
                    objWaring.Style = "background: #00ff00";
                }
                else if (ValueRead >= 1.5 && ValueRead <= 2) {
                    objWaring.class = "warning";
                    objWaring.Style = "background: #ffff00";
                }
                else if (ValueRead > 2) {
                    objWaring.class = "danger";
                    objWaring.Style = "background: #db0000";
                }
                break;
            case "NotImg": // .22
                if (ValueRead < 3) {
                    objWaring.class = "blue";
                    objWaring.Style = "background: #00ff00";
                }
                else if (ValueRead >= 3 && ValueRead <= 5) {
                    objWaring.class = "warning";
                    objWaring.Style = "background: #ffff00";
                }
                else if (ValueRead > 5) {
                    objWaring.class = "danger";
                    objWaring.Style = "background: #db0000";
                }
                break;
            case "StylistTimekeeping": //.25
                if (ValueRead > 90) {
                    objWaring.class = "blue";
                    objWaring.Style = "background: #00ff00";
                }
                else if (ValueRead >= 85 && ValueRead <= 90) {
                    objWaring.class = "warning";
                    objWaring.Style = "background: #ffff00";
                }
                else if (ValueRead < 85) {
                    objWaring.class = "danger";
                    objWaring.Style = "background: #db0000";
                }
                break;

            case "CanleBooking": // .32

                if (ValueRead < 15) {
                    objWaring.class = "blue";
                    objWaring.Style = "background: #00ff00";
                }
                else if (ValueRead >= 15 && ValueRead <= 20) {
                    objWaring.class = "warning";
                    objWaring.Style = "background: #ffff00";
                }
                else if (ValueRead > 20) {
                    objWaring.class = "danger";
                    objWaring.Style = "background: #db0000";
                }
                break;
            case "bookingBefor":  //.33
                console.log("bookingBefor", ValueRead);
                if (ValueRead > 70) {
                    objWaring.class = "blue";
                    objWaring.Style = "background: #00ff00";
                }
                else if (ValueRead >= 65 && ValueRead <= 70) {
                    objWaring.class = "warning";
                    objWaring.Style = "background: #ffff00";
                }
                else if (ValueRead < 65) {
                    objWaring.class = "danger";
                    objWaring.Style = "background: #db0000";
                }
                break;
        }
        return objWaring;

    } catch (e) {
        throw e;
    }

}

function ReturnWaringService(IdService, ValueIndex, DVTNghin) {
    var ValueRead = parseFloat(ValueIndex);
    var objWaring = new Object();
    var dvtDong = 1;
    if (DVTNghin = "Nghin") {
        dvtDong = 1000
    }
    switch (IdService) {
        case 14: // .9
            if (ValueRead > 5.5) {
                objWaring.class = "blue";
                objWaring.Style = "background: #00ff00";
            }
            else if (ValueRead >= 4.5 && ValueRead <= 5.5) {
                objWaring.class = "warning";
                objWaring.Style = "background: #ffff00";
            }
            else if (ValueRead < 4.5) {
                objWaring.class = "danger";
                objWaring.Style = "background: #db0000";
            }
            break;
        case 16: // .8
            if (ValueRead > 12) {
                objWaring.class = "blue";
                objWaring.Style = "background: #00ff00";
            }
            else if (ValueRead >= 10 && ValueRead <= 12) {
                objWaring.class = "warning";
                objWaring.Style = "background: #ffff00";
            }
            else if (ValueRead < 10) {
                objWaring.class = "danger";
                objWaring.Style = "background: #db0000";
            }
            break;
        case 69: // .10
            if (ValueRead > 7.5) {
                objWaring.class = "blue";
                objWaring.Style = "background: #00ff00";
            }
            else if (ValueRead >= 6 && ValueRead <= 7.5) {
                objWaring.class = "warning";
                objWaring.Style = "background: #ffff00";
            }
            else if (ValueRead < 6) {
                objWaring.class = "danger";
                objWaring.Style = "background: #db0000";
            }
            break;
        case 90: // .10
            if (ValueRead > 35) {
                objWaring.class = "blue";
                objWaring.Style = "background: #00ff00";
            }
            else if (ValueRead >= 30 && ValueRead <= 35) {
                objWaring.class = "warning";
                objWaring.Style = "background: #ffff00";
            }
            else if (ValueRead < 30) {
                objWaring.class = "danger";
                objWaring.Style = "background: #db0000";
            }
            break;
        case 91: // .10
            if (ValueRead > 35) {
                objWaring.class = "blue";
                objWaring.Style = "background: #00ff00";
            }
            else if (ValueRead >= 30 && ValueRead <= 35) {
                objWaring.class = "warning";
                objWaring.Style = "background: #ffff00";
            }
            else if (ValueRead < 30) {
                objWaring.class = "danger";
                objWaring.Style = "background: #db0000";
            }
            break;
        default:
            objWaring.class = "";
            objWaring.Style = "";
    }
    return objWaring;
}

