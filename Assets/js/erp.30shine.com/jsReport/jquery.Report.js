﻿// KHai bao domain api
var uri = window.apisReportBCKQKD.domain;

// Khai báo List dât BCKQKD
//var DataBCKQKD = {};
// click xem data BCKQKD version 4
function viewDataByDate(This) {
    // time fillter
    var time = This.data('time');
    // uri khi lấy api data bang truy vấn
    var uriApi = "";
    // kieu fillter :
    //  type = 0 : lấy api data bang truy vấn logic
    //  type = 1 : lấy api data truy vấn statistictic nhiều ngày
    var type = This.data('type');
    $(".tag-date.active").removeClass("active");
    This.addClass("active");
    if (type === 0 || type === 1) {
        $("#TxtDateTimeTo").val(time);
        $("#TxtDateTimeFrom").val(time);
    }

    if (type === 0) {
        uriApi = uri + window.apisReportBCKQKD.GetApiReportToday;
    }
    else if (type === 2 || type === 1) {
        var FromDate = "";
        var ToDate = "";
        if ($("#TxtDateTimeFrom").val()) {
            FromDate = $("#TxtDateTimeFrom").val();
            if ($("#TxtDateTimeTo").val()) {
                ToDate = $("#TxtDateTimeTo").val();
            }
            else {
                $("#TxtDateTimeTo").val(FromDate);
                ToDate = FromDate;
            }
        }
        else {
            alert("Bạn chưa chọn ngày !");
        }

        uriApi = uri + window.apisReportBCKQKD.GetApiReport + '?from=' + FromDate + '&to=' + ToDate;
    }
    startLoading();
    $.ajax({
        type: "GET",
        url: uriApi,
        //async: false,
        //data: ,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (Data) {
            appendDataBCKQKD(Data);
        },
        error: function (objData, error) {
            $('#tbodyBCKQKD').html('');
            alert('Có lỗi xảy ra ! Vui lòng liên hệ với nhóm phát triển !');
            finishLoading();
        }
    });
}
// apend content data BCKQKD version 4

function appendDataBCKQKD(Data) {
    console.log(Data);
    $('#tbodyBCKQKD').html('');
    // tien xep hang rank
    const priceCompare = 3000000;
    // comparative : so sanh thứ hạng : hang A = Tổng lợi nhuận sau thuế < comparative; hang B = 0 < Tổng lợi nhuận sau thuế < comparative ; hang c = comparative < 0
    // cong thuc comparative = so ngay * 3000000

    var comparative =
        //tinh so ngay xem du lieu
        (funcDatediff($("#TxtDateTimeFrom").val(), $("#TxtDateTimeTo").val())
            +
            // cong them 1 ngay hien tai
            1)
        *
        priceCompare;
    var strHTML = "";
    var strTotal = "";
    // gan lai data
    var ListData = Data;
    // dkhai bao bien dem tong so salon
    var CountData = ListData.length;
    // Khai bao cac bien Total
    var totalSalon = 'Toàn hệ thống'; // Salon total
    var sumTotalIncomeAfterTax = 0;  //B. Tổng lợi nhuận sau thuế 
    var sumTotalNumberOfTurns = 0;  //C. Tổng khách = TỔNG BILL
    var sumTotalSales = 0; //D. Tổng doanh thu 
    var sumTotalServiceProfit = 0; //E. Tổng doanh thu dịch vụ
    var sumTotalProductProfit = 0;    //F. Tổng doanh thu sản phẩm
    var sumTotalTransactionPerCus = 0; //G. Tổng doanh thu trên khách 
    var sumTotalServicePerCus = 0; //H. Tổng doanh thu dịch vụ trên khách 
    var sumTotalProductPerCus = 0;//I. Tổng doanh thu sản phẩm trên khách
    var sumOtherIncome = 0; //DQ. Thu nhap Khac
    var sumDirectFee = 0;  //J. Giá vốn trực tiếp
    var sumTotalStaffSalary = 0; //K. Giá vốn dịch vụ tiền lương dịch vụ = STYLIST + SKINNER + CHECKIN + CHECKOUT + BAOVE
    var sumSalaryServicePerServiceIncome = 0;  //L. Tỉ lệ lương giá vốn trên doanh thu dịch vụ = K/E
    var sumSecurityCheckOutSalary = 0; // add 201900402 Luong Bao Ve + Checkout
    var sumTotalProductCapital = 0; //M. Giá vốn dịch vụ tiền lương mỹ phẩm = 10% * F
    // Note lai
    var sumTotalProdutPrice = 0; // Gía vốn - MP bán
    var sumCompensation = 0; // Gía vốn bù trừ - MP bán
    // end note
    var sumTotalPayOffKCS = 0;  //O. Lương thưởng doanh số KCS = 5% * TỔNG BILL CÓ ẢNH
    var sumTotalDailyCostInventory = 0;//P. Giá vốn dịch vụ CCDC sử dụng - khăn
    var sumElectricityAndWaterBill = 0; //Q.Tiền điện nước
    var sumSalonFee = 0; //R. Chi phí salon
    var sumRentWithTax = 0; //S. Tiền thuê cửa hàng đã bao gồm chi phí thuế 
    var sumCapitalSpending = 0; //T. Phân bổ đầu tư khấu hao chi phí trả trước
    var sumMKTExpense = 0; //U. CP quảng cáo
    var sumTotalSMSExpenes = 0; //V. Chi phí SMS
    var sumShippingExpend = 0; //W. Chi phí vận chuyển TQ
    var sumInternetAndPhoneBill = 0; //X. Cước điện thoại và Internet
    var sumSocialInsuranceAndFixedCost = 0; //Y. Tiền bảo hiểm xã hội, KPCĐ
    var sumTax = 0; //Z. Thuế
    var sumIncomeTaxes = 0; //AA. Thuế thu nhập doanh nghiệp
    var sumSalonUnplannedSpending = 0; //AB. Chi phí vận hành phát sinh hàng ngày
    var sumManageFee = 0;  //AC. Chi phí quản lý
    var sumOfficeRentAndSeviceCost = 0; //AD. Chi phí thuê văn phòng thái hà
    var sumOfficeStaffSalary = 0; //AE. Lương cơ bản
    var sumSalesSalary = 0; //AF. Lương KPI/OKR
    var sumItSalary = 0; //AG. Chi phí lương IT
    var sumOfficeStaffSocialInsurance = 0;  //AH. Tiền bảo hiểm xã hội bộ phận chung
    var sumUnplannedSpending = 0; //AI. Chi phí phát sinh hằng ngày (vận hành chung)
    var objRank = {};
    // add 20190226
    var sumSMDistributionToday = 0;    // doanh thu shinemember phan bo trong ky ke toan 
    var sumSMDistribution = 0;    // doanh thu shinemember phan bo trong ky hom nay
    var sumProductShineMember = 0;    // doanh thu shinemember trong ngay
    var sumNetRevenue = 0;    // doanh thu thuan
    if (CountData > 0) {
        // tinh tong so ban row trong data
        for (var i = 0; i < CountData; i++) {
            objRank = funcReturnRank(parseInt(ListData[i].totalIncomeAfterTax), comparative); // gan objRank
            sumTotalIncomeAfterTax = sumTotalIncomeAfterTax + parseInt(ListData[i].totalIncomeAfterTax);  //B. Tổng lợi nhuận sau thuế 
            sumTotalNumberOfTurns = sumTotalNumberOfTurns + parseInt(ListData[i].numberOfTurns);  //C. Tổng khách = TỔNG BILL
            sumTotalSales = sumTotalSales + parseInt(ListData[i].totalSales); //D. Tổng doanh thu 
            sumTotalServiceProfit = sumTotalServiceProfit + parseInt(ListData[i].totalServiceProfit); //E. Tổng doanh thu dịch vụ
            sumTotalProductProfit = sumTotalProductProfit + parseInt(ListData[i].totalProductProfit);    //F. Tổng doanh thu sản phẩm
            sumTotalTransactionPerCus = sumTotalTransactionPerCus + parseFloat(ListData[i].totalTransactionPerCus); //G. Tổng doanh thu trên khách 
            sumTotalServicePerCus = sumTotalServicePerCus + parseFloat(ListData[i].totalServicePerCus); //H. Tổng doanh thu dịch vụ trên khách 
            sumTotalProductPerCus = sumTotalProductPerCus + parseFloat(ListData[i].totalProductPerCus);//I. Tổng doanh thu sản phẩm trên khách
            sumOtherIncome = sumOtherIncome + parseFloat(ListData[i].otherIncome);// DQ Tong thu nhap khac
            sumDirectFee = sumDirectFee + parseInt(ListData[i].directFee);  //J. Giá vốn trực tiếp
            sumTotalStaffSalary = sumTotalStaffSalary + parseInt(ListData[i].totalStaffSalary); //K. Giá vốn dịch vụ tiền lương dịch vụ = STYLIST + SKINNER + CHECKIN + CHECKOUT + BAOVE
            sumSalaryServicePerServiceIncome = sumSalaryServicePerServiceIncome + parseFloat(ListData[i].salaryServicePerServiceIncome);  //L. Tỉ lệ lương giá vốn trên doanh thu dịch vụ = K/E
            sumTotalProductCapital = sumTotalProductCapital + parseFloat(ListData[i].totalProductCapital); //M. Giá vốn dịch vụ tiền lương mỹ phẩm = 10% * F
            sumSecurityCheckOutSalary = sumSecurityCheckOutSalary + parseFloat(ListData[i].totalSecurityCheckOutSalary); // add 20190402 Luong BV + checkout
            // Note lai
            sumTotalProdutPrice = sumTotalProdutPrice + parseInt(ListData[i].totalProdutPrice); // Gía vốn - MP bán
            sumCompensation = sumCompensation + parseInt(ListData[i].compensation); // Gía vốn bù trừ - MP bán
            // end note
            sumTotalPayOffKCS = sumTotalPayOffKCS + parseFloat(ListData[i].totalPayOffKCS);  //O. Lương thưởng doanh số KCS = 5% * TỔNG BILL CÓ ẢNH
            sumTotalDailyCostInventory = sumTotalDailyCostInventory + parseInt(ListData[i].totalDailyCostInventory);//P. Giá vốn dịch vụ CCDC sử dụng - khăn
            sumElectricityAndWaterBill = sumElectricityAndWaterBill + parseInt(ListData[i].electricityAndWaterBill); //Q.Tiền điện nước
            sumSalonFee = sumSalonFee + parseInt(ListData[i].salonFee); //R. Chi phí salon
            sumRentWithTax = sumRentWithTax + parseInt(ListData[i].rentWithTax); //S. Tiền thuê cửa hàng đã bao gồm chi phí thuế 
            sumCapitalSpending = sumCapitalSpending + parseInt(ListData[i].capitalSpending); //T. Phân bổ đầu tư khấu hao chi phí trả trước
            sumMKTExpense = sumMKTExpense + parseInt(ListData[i].mktExpense); //U. CP quảng cáo
            sumTotalSMSExpenes = sumTotalSMSExpenes + parseInt(ListData[i].totalSMSExpenes); //V. Chi phí SMS
            sumShippingExpend = sumShippingExpend + parseInt(ListData[i].shippingExpend); //W. Chi phí vận chuyển TQ
            sumInternetAndPhoneBill = sumInternetAndPhoneBill + parseInt(ListData[i].internetAndPhoneBill); //X. Cước điện thoại và Internet
            sumSocialInsuranceAndFixedCost = sumSocialInsuranceAndFixedCost + parseInt(ListData[i].socialInsuranceAndFixedCost); //Y. Tiền bảo hiểm xã hội, KPCĐ
            sumTax = sumTax + parseInt(ListData[i].tax); //Z. Thuế
            sumIncomeTaxes = sumIncomeTaxes + parseInt(ListData[i].incomeTaxes); //AA. Thuế thu nhập doanh nghiệp
            sumSalonUnplannedSpending = sumSalonUnplannedSpending + parseInt(ListData[i].salonUnplannedSpending); //AB. Chi phí vận hành phát sinh hàng ngày
            sumManageFee = sumManageFee + parseInt(ListData[i].manageFee);  //AC. Chi phí quản lý
            sumOfficeRentAndSeviceCost = sumOfficeRentAndSeviceCost + parseInt(ListData[i].officeRentAndSeviceCost); //AD. Chi phí thuê văn phòng thái hà
            sumOfficeStaffSalary = sumOfficeStaffSalary + parseInt(ListData[i].officeStaffSalary); //AE. Lương cơ bản
            sumSalesSalary = sumSalesSalary + parseInt(ListData[i].salesSalary); //AF. Lương KPI/OKR
            sumItSalary = sumItSalary + parseInt(ListData[i].itSalary); //AG. Chi phí lương IT
            sumOfficeStaffSocialInsurance = sumOfficeStaffSocialInsurance + parseInt(ListData[i].officeStaffSocialInsurance);  //AH. Tiền bảo hiểm xã hội bộ phận chung
            sumUnplannedSpending = sumUnplannedSpending + parseInt(ListData[i].unplannedSpending); //AI. Chi phí phát sinh hằng ngày (vận hành chung)
            sumSMDistributionToday = sumSMDistributionToday + parseInt(ListData[i].smDistributionToday);// doanh thu SM phan bo trong ky ke toan
            sumSMDistribution = sumSMDistribution + parseInt(ListData[i].totalShineMemberDistribution);// doanh thu SM phan bo trong ky hom nay
            sumProductShineMember = sumProductShineMember + parseInt(ListData[i].totalProductShineMember);// tong doanh thu shinemeber
            sumNetRevenue = sumNetRevenue + parseInt(ListData[i].totalNetRevenue); // doanh thu thuan
            strHTML += ' <tr> ' +
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + ListData[i].salonName + ' </td >' + //A Salon total
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" >' + formatPrice(parseInt(ListData[i].totalIncomeAfterTax)) + '</td>' +  //B. Tổng lợi nhuận sau thuế
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center; background-color:' + objRank.colorRank + '" >' + objRank.rank + '</td>' +  // XẾP HẠNG SALON
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].numberOfTurns)) + '</td>' + //C. Tổng khách = TỔNG BILL
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].totalNetRevenue)) + '</td>' +// doanh thu Thuan
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].totalSales)) + '</td>' + //D. Tổng doanh thu 
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].totalServiceProfit)) + '</td>' + //E. Tổng doanh thu dịch vụ
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].totalProductProfit)) + '</td>' + //F. Tổng doanh thu sản phẩm Khong tinh Shinemember
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].totalProductShineMember)) + '</td>' + // Doanh thu SHinemember ban trong ngay
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].totalShineMemberDistribution)) + '</td>' + // Shinemember phan bo trong ky hom nay
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].smDistributionToday)) + '</td>' + // Shinemember phan bo trong ky ke toan
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseFloat(ListData[i].totalTransactionPerCus)) + '</td>' +  //G. Tổng doanh thu trên khách 
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseFloat(ListData[i].totalServicePerCus)) + '</td>' + //H. Tổng doanh thu dịch vụ trên khách
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseFloat(ListData[i].totalProductPerCus)) + '</td>' +  //I. Tổng doanh thu sản phẩm trên khách
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].otherIncome)) + '</td>' + //DQ. Thu nhap khac
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].directFee)) + '</td>' + //J. Giá vốn trực tiếp
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].totalStaffSalary)) + '</td>' + //K. Giá vốn dịch vụ tiền lương dịch vụ = STYLIST + SKINNER + CHECKIN + CHECKOUT + BAOVE
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + parseInt((parseFloat(ListData[i].salaryServicePerServiceIncome) * 100)) + '%</td>' +   //L. Tỉ lệ lương giá vốn trên doanh thu dịch vụ = K/E
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].totalProductCapital)) + '</td>' + //M. Giá vốn dịch vụ tiền lương mỹ phẩm = 10% * F
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].totalSecurityCheckOutSalary)) + '</td>' + // add 20190402 Luong Bv + Checkout
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].totalProdutPrice)) + '</td>' + //N Gía vốn - MP bán
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].compensation)) + '</td>' + //N Gía vốn bù trừ - MP bán
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].totalPayOffKCS)) + '</td>' +  //O. Lương thưởng doanh số KCS = 5% * TỔNG BILL CÓ ẢNH )
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].totalDailyCostInventory)) + '</td>' + //P. Giá vốn dịch vụ CCDC sử dụng - khăn )
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].salonFee)) + '</td>' + //R. Chi phí salon
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].rentWithTax)) + '</td>' + //S. Tiền thuê cửa hàng đã bao gồm chi phí thuế 
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].capitalSpending)) + '</td>' + //T. Phân bổ đầu tư khấu hao chi phí trả trước
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].mktExpense)) + '</td>' + //U. CP quảng cáo
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].totalSMSExpenes)) + '</td>' + //V. Chi phí SMS
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].shippingExpend)) + '</td>' + //W. Chi phí vận chuyển TQ
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].tax)) + '</td>' + '</td>' +  //Z. Thuế
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].incomeTaxes)) + '</td>' + //AA. Thuế thu nhập doanh nghiệp
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].electricityAndWaterBill)) + ' </td>' + //Q.Tiền điện nước
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].salonUnplannedSpending)) + '</td>' + //AB. Chi phí vận hành phát sinh hàng ngày
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].manageFee)) + '</td>' + //AC. Chi phí quản lý 
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].internetAndPhoneBill)) + '</td>' +  //X. Cước điện thoại và Internet
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].socialInsuranceAndFixedCost)) + '</td>' + //Y. Tiền bảo hiểm xã hội, KPCĐ
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].officeRentAndSeviceCost)) + '</td>' +   //AD. Chi phí thuê văn phòng thái hà
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].officeStaffSalary)) + '</td>' +  //AE. Lương cơ bản
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].salesSalary)) + '</td>' + //AF. Lương thuong khac
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].itSalary)) + '</td>' + //AG. Chi phí lương IT 
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].officeStaffSocialInsurance)) + '</td>' + //AH. Tiền bảo hiểm xã hội bộ phận chung
                '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(parseInt(ListData[i].unplannedSpending)) + '</td>' + //AI. Chi phí phát sinh hằng ngày (vận hành chung)
                '</tr >';
        }
        //console.log('strHTML', strHTML);
        // string html row SUMTotal
        strTotal = ' <tr> ' +
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + totalSalon + ' </td >' + // Salon total
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumTotalIncomeAfterTax) + '</td>' + //B. Tổng lợi nhuận sau thuế 
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > </td>' + // XẾP HẠNG SALON
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumTotalNumberOfTurns) + '</td>' + //C. Tổng khách = TỔNG BILL
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumNetRevenue) + '</td>' + // Doanh thu thuan
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumTotalSales) + '</td>' + //D. Tổng doanh thu 
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumTotalServiceProfit) + '</td>' + //E. Tổng doanh thu dịch vụ
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumTotalProductProfit) + '</td>' + //F. Tổng doanh thu sản phẩm khong tinh Shinemember
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumProductShineMember) + '</td>' + // Doanh thu SHinemember ban trong ngay
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumSMDistribution) + '</td>' + // Doanh thu Shinemember phan bo hom nay
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumSMDistributionToday) + '</td>' + // Doanh thu Shinmember phan bo trong ky ke toan
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + (formatPrice(((parseFloat(sumTotalSales)) / parseFloat(sumTotalNumberOfTurns)).toFixed(0))) + '</td>' +  //G. Tổng doanh thu trên khách 
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + (formatPrice(((parseFloat(sumTotalServiceProfit)) / sumTotalNumberOfTurns).toFixed(0))) + '</td>' + //H. Tổng doanh thu dịch vụ trên khách
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + (formatPrice(((parseFloat(sumTotalProductProfit)) / sumTotalNumberOfTurns).toFixed(0))) + '</td>' +  //I. Tổng doanh thu sản phẩm trên khách
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumOtherIncome) + '</td>' + //DQ. Thu nhap khac
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumDirectFee) + '</td>' + //J. Giá vốn trực tiếp
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumTotalStaffSalary) + '</td>' + //K. Giá vốn dịch vụ tiền lương dịch vụ = STYLIST + SKINNER + CHECKIN + CHECKOUT + BAOVE
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + parseInt((parseFloat(sumTotalStaffSalary) / sumTotalServiceProfit).toFixed(2) * 100) + '%</td>' +   //L. Tỉ lệ lương giá vốn trên doanh thu dịch vụ = K/E
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumTotalProductCapital) + '</td>' + //M. Giá vốn dịch vụ tiền lương mỹ phẩm = 10% * F
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumSecurityCheckOutSalary) + '</td>' +// add 20190402 Luong BV + Checkout
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumTotalProdutPrice) + '</td>' + // Gía vốn - MP bán
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumCompensation) + '</td>' + // Gía vốn bù trừ - MP bán
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumTotalPayOffKCS) + '</td>' +  //O. Lương thưởng doanh số KCS = 5% * TỔNG BILL CÓ ẢNH )
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumTotalDailyCostInventory) + '</td>' + //P. Giá vốn dịch vụ CCDC sử dụng - khăn )
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumSalonFee) + '</td>' + //R. Chi phí salon
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumRentWithTax) + '</td>' + //S. Tiền thuê cửa hàng đã bao gồm chi phí thuế 
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumCapitalSpending) + '</td>' + //T. Phân bổ đầu tư khấu hao chi phí trả trước
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumMKTExpense) + '</td>' + //U. CP quảng cáo
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumTotalSMSExpenes) + '</td>' + //V. Chi phí SMS
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumShippingExpend) + '</td>' + //W. Chi phí vận chuyển TQ
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumTax) + '</td>' +  //Z. Thuế
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumIncomeTaxes) + '</td>' + //AA. Thuế thu nhập doanh nghiệp
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumElectricityAndWaterBill) + ' </td>' + //Q.Tiền điện nước
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumSalonUnplannedSpending) + '</td>' + //AB. Chi phí vận hành phát sinh hàng ngày
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumManageFee) + '</td>' + //AC. Chi phí quản lý 
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumInternetAndPhoneBill) + '</td>' +  //X. Cước điện thoại và Internet
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumSocialInsuranceAndFixedCost) + '</td>' + //Y. Tiền bảo hiểm xã hội, KPCĐ
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumOfficeRentAndSeviceCost) + '</td>' +   //AD. Chi phí thuê văn phòng thái hà
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumOfficeStaffSalary) + '</td>' +  //AE. Lương cơ bản
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumSalesSalary) + '</td>' + //AF. Lương KPI/OKR
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumItSalary) + '</td>' + //AG. Chi phí lương IT 
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumOfficeStaffSocialInsurance) + '</td>' + //AH. Tiền bảo hiểm xã hội bộ phận chung
            '<td style="border: 1px solid black; border-collapse: collapse; padding: 5px;  text-align: center;" > ' + formatPrice(sumUnplannedSpending) + '</td>' + //AI. Chi phí phát sinh hằng ngày (vận hành chung)
            '</tr >';

        //console.log('strTotal', strTotal);
        $('#tbodyBCKQKD').append(strTotal).append(strHTML);
        $('html,body').animate({
            scrollTop: $("#UPTotal").offset().top
        },
            'slow');

    }
    else {
        $('#tbodyBCKQKD').html('');
    }
    finishLoading();
};
// Xuat file excel
function ExportExcel(This) {
    // function export excel
    funcExportExcelHtml("#table-wp-bckqkd", "BCKQKD");
};
// load khi load trang
$(document).ready(function () {
    $("tbody.tbody-report").css({ "height": "" + screen.height - 300 + "" });
});

// Return RANK
function funcReturnRank(money, comparative) {
    var objRank = {};
    if (parseInt(money) >= parseInt(comparative)) {
        objRank.rank = "A";
        objRank.colorRank = "#00ff00";
    }
    else if (parseInt(money) < parseInt(comparative) && parseInt(money) >= 0) {
        objRank.rank = "B";
        objRank.colorRank = "#ffff99";
    }
    else if (parseInt(money) < 0) {
        objRank.rank = "C";
        objRank.colorRank = "#ff0000";
    }
    return objRank;
};

