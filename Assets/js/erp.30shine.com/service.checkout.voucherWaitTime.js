﻿$(document).ready(function () {
    // Gọi hàm kiểm tra vourcher giảm giá 20% Shine Combo 
    checkVoucherWaitTime();

    // Kiểm tra khách có bị chờ lâu lần này không
    checkLongWaitTime();
});

var voucherClass = function () {
    this.ID = 0;
    this.isVoucher = false;
    this.VoucherPercent = 0;
    this.ServiceIDs = [];
}
var voucherObj = new voucherClass();
/*
* get voucherObj
*/
function getVoucher() {
    return voucherObj;
}
/*
* set voucherObj
*/
function setVoucher(voucher) {
    voucherObj.ID = voucher.ID;
    voucherObj.isVoucher = voucher.isVoucher;
    voucherObj.VoucherPercent = voucher.VoucherPercent;
    voucherObj.ServiceIDs = voucher.ServiceIDs;
}

/*
* get billID
*/
function getBillID()
{
    try {
        var billID = parseInt($("#HDF_BillId").val());
        return !isNaN(billID) ? billID : 0;
    }
    catch (ex)
    {
        console.log(ex);
        return 0;
    }
}

/*
* get customerName
*/
function getCustomerName() {
    try {
        return $("#CustomerName").val();
    }
    catch (ex) {
        console.log(ex);
        return "";
    }
}

/*
* Kiểm tra voucher giảm giá 20% Shine Combo
*/
function checkVoucherWaitTime() {
    $.ajax({
        type: "POST",
        url: "/GUI/FrontEnd/Service/ConfigCheck/Service_Pending_Complete_V2.aspx/checkVoucherWaitTime",
        data: '{CustomerId : ' + customerID + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            console.log(response);
            if (response.d.success) {
                var data = response.d.data;
                if (data.isVoucher) {
                    setVoucher(data);
                    bindWaitTimeVoucher();
                }
                else {
                    console.log("No data." + data.VoucherPercent);
                }
            }
            else {
                alert("Đã có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển.");
            }
        },
        failure: function (response) { alert(response.d); }
    });
}

/*
* Kiểm tra ở dịch vụ lần này, khách có phải chờ lâu hay không
*/
function checkLongWaitTime() {
    $.ajax({
        type: "POST",
        url: "/GUI/FrontEnd/Service/ConfigCheck/Service_Pending_Complete_V2.aspx/checkCustomerIsWaitLongTime",
        data: '{billID : ' + getBillID() + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            console.log(response);
            if (response.d.success) {
                var data = response.d.data;
                if (data) {
                    console.log('ads' + data);
                    var form = '<div class="field cusInfo">' +
                                    '<p class="pSpecCus" style="text-transform:uppercase">(*) Lễ tân phát voucher giảm giá dịch vụ Shine Combo cho khách. Voucher áp dụng cho lần sau.</p>' +
                                '</div>' +
                                '<div class="field cusInfo">' +
                                    '<p class="pNote">Lần này khách phải đợi quá lâu.</p>' +
                                '</div>' +
                                '<div class="field cusInfo">' +
                                    '<p class="pExam">VD: Anh '+getCustomerName()+' ơi, rất xin lỗi vì để anh phải chờ lâu hôm nay, chúng em xin tặng anh phiếu giảm giá gói Shine Combo cho lần dùng dịch vụ tiếp theo.</p>' +
                                '</div>';
                    bindModal("LƯU Ý PHÁT VOUCHER GIẢM GIÁ CHO KHÁCH LẦN SAU", form);
                }
            }
        },
        failure: function (response) { alert(response.d); }
    });
}


/*
* Hiển thị thông tin khách hàng được nhận Voucher giảm giá 20% trường hợp chờ lâu lần trước
*/
function bindWaitTimeVoucher() {
    var data = getVoucher();
    var form = '<div class="field cusInfo">' +
                    '<p class="pSpecCus">(*) KHÁCH ĐƯỢC GIẢM GIÁ ' + data.VoucherPercent + '% SHINE COMBO 100K</p>' +
                '</div>' +
                '<div class="field cusInfo">' +
                    '<p class="pNote">Lần trước khách phải đợi quá lâu.</p>' +
                '</div>' +
                '<div class="field cusInfo">' +
                    '<p class="pInfo">LỄ TÂN CHÚ Ý THÔNG BÁO GIẢM GIÁ SHINE COMBO 100K CHO KHÁCH (CHỈ ÁP DỤNG CHO LẦN NÀY)</p>' +
                '</div>' +
                '<div class="field cusInfo">' +
                    '<p class="pExam">VD: Lần trước anh phải chờ lâu nên hôm nay chúng em giảm giá ' + data.VoucherPercent + '% gói dịch vụ Shine Combo 100K cho anh ạ.</p>' +
                '</div>';
    $(form).insertAfter($("#CustomerCode"));

    bindModal("LƯU Ý GIẢM GIÁ "+data.VoucherPercent+"% CHO KHÁCH", form);

    // Set cột giảm giá
    $("table#table-item-service tbody tr").each(function () {
        var id = parseInt($(this).find("td.td-product-code").attr("data-id"));
        id = !isNaN(id) ? id : 0;
        if (data.ServiceIDs.indexOf(id) != -1) {
            var input = $(this).find("td.td-product-voucher input.voucher-services");
            input.val(data.VoucherPercent);
            updateServiceInput(input);
        }
    });
}

function bindModal(title, content)
{
    $("#modal-page").find(".modal-title").text(title);
    $("#modal-page").find(".modal-body").html(content);
    $("#modal-page").find(".btn").text("Đóng");
    $("#modal-page").modal('show');
}

function updateServiceInput(This) {
    var obj = This.parent().parent().parent(),
    quantity = obj.find("input.product-quantity").val(),
    price = obj.find(".td-product-price").data("price"),
    voucher = obj.find("input.product-voucher").val(),
    boxMoney = obj.find(".box-money"),
    promotion = 0;

    obj.find(".promotion-money input[type='checkbox']:checked").each(function () {
        var value = $(this).attr("data-value").trim();
        promotion += (value != "" ? parseInt(value) : 0);
    });

    // check value
    price = price.toString().trim() != "" ? parseInt(price) : 0;
    quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
    voucher = voucher.trim() != "" ? parseInt(voucher) : 0;
    if (promotion == 0) {
        boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100)).show();
    } else {
        obj.find("input.product-voucher").val(0);
    }
    TotalMoney();
    getProductIds();
    getServiceIds();
}

/*
* Thực hiện 1 số xử lý client trước khi post server ,kiểm tra gọi lên hàm updateVoucherWaitTimeIsUsed
* Kiểm tra sự kiện onclick="clientScriptBtnSend() trong <Asp:Panel>
*/
function clientScriptBtnSend() {
    addLoading();
    //updateVoucherWaitTimeIsUsed();
    //createVoucher();
}

// insert du lieu vao bang 
//function createVoucher() {
//    //var data = getVoucher();
//    $.ajax({
//        type: "POST",
//        url: "/GUI/FrontEnd/Service/ConfigCheck/Service_Pending_Complete_V2.aspx/createVoucher",
//        data: '{ billID : ' + billID + ' }',
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: function (response) {
//            console.log(response);

//        },
//        failure: function (response) { alert(response.d); }
//    });
//}
/*
* Cập nhật trạng thái khách đã được sử dụng Voucher giảm giá 20% trường hợp đợi lâu
*/

//function updateVoucherWaitTimeIsUsed() {
//    var data = getVoucher();
//    $.ajax({
//        type: "POST",
//        url: "/GUI/FrontEnd/Service/ConfigCheck/Service_Pending_Complete_V2.aspx/updateVoucherWaitTimeIsUsed",
//        data: '{Id : ' + data.ID + '}',
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: function (response) {
//            console.log(response);
//            if (response.d.success) {
//                Console.log(response.d.msg);
//            }
//            else {
//                //alert("Đã có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển.");
//            }
//        },
//        failure: function (response) { alert(response.d); }
//    });
//}