﻿/// <reference path="../parse_date.js" />
/// <reference path="../common.js" />
var PhoneItem = '';
const objCusCompareScore =
{
    levelScore2: 0
};
const objCusClassLevel =
{
    classLevel2: 'score-1',
    classNone: 'score-none'
};

// change phone
function changePhone(phone) {
    var phone_replace = '';
    if (phone.length == 11) {
        switch (phone.substring(0, 4)) {
            //mobifone
            case '0120':
                phone_replace = phone.replace(phone.substring(0, 4), '070');
                break;
            case '0121':
                phone_replace = phone.replace(phone.substring(0, 4), '079');
                break;
            case '0122':
                phone_replace = phone.replace(phone.substring(0, 4), '077');
                break;
            case '0126':
                phone_replace = phone.replace(phone.substring(0, 4), '076');
                break;
            case '0128':
                phone_replace = phone.replace(phone.substring(0, 4), '078');
                break;
            //vinaphone
            case '0123':
                phone_replace = phone.replace(phone.substring(0, 4), '083');
                break;
            case '0124':
                phone_replace = phone.replace(phone.substring(0, 4), '084');
                break;
            case '0125':
                phone_replace = phone.replace(phone.substring(0, 4), '085');
                break;
            case '0127':
                phone_replace = phone.replace(phone.substring(0, 4), '081');
                break;
            case '0129':
                phone_replace = phone.replace(phone.substring(0, 4), '082');
                break;
            //viettel
            case '0162':
                phone_replace = phone.replace(phone.substring(0, 4), '032');
                break;
            case '0163':
                phone_replace = phone.replace(phone.substring(0, 4), '033');
                break;
            case '0164':
                phone_replace = phone.replace(phone.substring(0, 4), '034');
                break;
            case '0165':
                phone_replace = phone.replace(phone.substring(0, 4), '035');
                break;
            case '0166':
                phone_replace = phone.replace(phone.substring(0, 4), '036');
                break;
            case '0167':
                phone_replace = phone.replace(phone.substring(0, 4), '037');
                break;
            case '0168':
                phone_replace = phone.replace(phone.substring(0, 4), '038');
                break;
            case '0169':
                phone_replace = phone.replace(phone.substring(0, 4), '039');
                break;
            //vietnammobile
            case '0186':
                phone_replace = phone.replace(phone.substring(0, 4), '056');
                break;
            case '0188':
                phone_replace = phone.replace(phone.substring(0, 4), '058');
                break;
            //gmobile
            case '0199':
                phone_replace = phone.replace(phone.substring(0, 4), '059');
                break;
            default:
                phone_replace = phone;
                break;
        };
        return phone_replace;
    }
    else
        return phone;

}
/*
* Lấy salonID từ dropdowlist
*/
function getSalonID() {
    try {
        return $("#ddlSalon").val();
    }
    catch (ex) {
        console.log(ex);
    }
}
// pdfmake
function buildTableBody(data, columns) {
    var body = [];

    body.push(columns);

    data.forEach(function (row) {
        var dataRow = [];

        columns.forEach(function (column) {
            dataRow.push(row[column].toString());
        });

        body.push(dataRow);
    });

    return body;
}

function table(data, columns) {
    return {
        table: {
            headerRows: 1,
            widths: [140, 20],
            body: buildTableBody(data, columns)
        }
    };
}
/*
* Lấy thống tin khách hàng từ API
*/
function callCustomerData(This) {
    debugger;
    var phone = This.val();
    var phoneLen = phone.length;
    if (phoneLen >= 10 && phoneLen <= 11) {
        // Lấy dữ liệu thông qua API
        startLoading();
        $.ajax({
            type: "POST",
            //url: "/GUI/Booking/OrderBooking.aspx/BindHourWhereStylistNewVersion3",
            url: "/checkin/customerbyphone-j5pFZRNvCvmAe2Lv",
            data: '{phone : "' + changePhone(phone) + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                if (response.d != null) {
                    console.log(response.d);
                    var end_date = null;
                    $("#EBPopup .customer-name").val(response.d.Fullname);
                    $("#EBPopup .customer-name").attr("data-cusscore", response.d.Score);
                    checkCustomerType(response.d.Id, '');
                    var today = new Date();//moment().format("MM/D/YYYY");
                    if (response.d.MemberEndTime == null) {
                        bill.IsSM = false;
                    }
                    else {
                        var end_date = ToJavaScriptDate(response.d.MemberEndTime);
                        //end_date = nowDate.format("ddd mmm dd yyyy HH:MM:ss");
                        //console.log(end_date.getMonth());
                        if (end_date < today) {
                            bill.IsSM = false;
                        }
                        else if (response.d.MemberType == null) {
                            bill.IsSM = false;
                        }
                        else if (response.d.MemberType == 0) {
                            bill.IsSM = false;
                        }
                        else {
                            bill.IsSM = true;
                        }
                    }
                    bill.snDay = (response.d.SN_day != null || response.d.SN_day != 0) ? response.d.SN_day : "";
                    bill.snMonth = (response.d.SN_month != null || response.d.SN_month != 0) ? response.d.SN_month : "";
                    bill.snYear = (response.d.SN_year != null || response.d.SN_year != 0) ? response.d.SN_year : "";
                    $('#EBPopup input.sn-day').val(bill.snDay == 0 ? "" : bill.snDay);
                    $('#EBPopup input.sn-month').val(bill.snMonth == 0 ? "" : bill.snMonth);
                    $('#EBPopup input.sn-year').val(bill.snYear == 0 ? "" : bill.snYear);

                    // console.log(end_date, today);
                }
                else {
                    $("#EBPopup .customer-name").val("");
                    $("#EBPopup .customer-name").attr("data-cusscore", 0);
                    $('.div_checkCustomer').addClass('hidden');
                    finishLoading();
                    //$("#EBPopup .customer-name").val("");
                }

            },
            failure: function (response) { alert(response.d); }
        });
    }
    else if (phoneLen > 0) {
        alert("Vui lòng nhập đúng số điện thoại.");
    }
}

/*
* Set giá trị salonID trong trường hợp chọn salon
*/
function setSalon(This) {
    bill.salonID = This.val();
}

function setTeam(This, teamID) {
    bill.teamID = teamID;
    $("#EBPopup .list-team .btn-team.active").removeClass("active");
    This.addClass("active");
}

/*
* Set giá trị nhân viên checkin
*/
function setCheckin(This) {
    //startLoading();
    $.ajax({
        type: "POST",
        url: "/ver2-beta-timeline/checkinbycode",
        data: '{orderCode : "' + This.val() + '", salonID : ' + bill.salonID + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json", success: function (response) {
            if (response.d != null) {
                bill.checkinID = response.d.Id;
                $("#EBPopup .checkin-name").val(response.d.Fullname);
            }
            else {
                bill.checkinID = 0;
                $("#EBPopup .checkin-name").val("");
            }
            //finishLoading();
        },
        failure: function (response) { alert(response.d); }
    });
}

/*
*Cập nhật tên khách hàng
*/
function getCustomerName() {
    var name = $('#EBPopupContent .customer-name').val();
    $.ajax({
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        url: '/ver2-beta-timeline/setcustomername',
        //url:'/checkin/customerbyname',
        data: "{'customerName':'" + name + "', bookingID : " + bill.bookingID + "}",
        success: function (response) {
            if (response.d.success) {
                bill.customerName = name;
                $("#" + bill.stylistID + "-" + bill.bookingID + "-bk").find(".customer-name").text(name);
                alert("Lưu tên khách hàng thành công!");
                autoCloseEBPopup();
            }
        },
        error: function () {
            alert("Có lỗi xảy ra! Vui lòng với nhà phát triển!");
        }
    });
}

/*
* Mở popup để in bill
*/
function openPrintBill(This, billID, bookingID, stylistID, stylistName, customerPhone, customerName, hourID, subHourID, hourFrame, isAutoStylist, isBookAtSalon, snDay, snMonth, snYear) {
    // Lấy lại giá trị customerName, customerPhone
    customerName = This.find(".customer-name").text().trim();
    if (CheckParam(This.find(".customer-phone span").attr('data-customer-code'))) {
        let phone = This.find(".customer-phone span").attr('data-customer-code').split("-")[1];
        if (CheckParam(phone)) {
            customerPhone = changePhone(phone);
            PhoneItem = customerPhone;
        }
    }
    bill.billID = billID;
    bill.stylistID = stylistID;
    bill.stylistName = stylistName;
    bill.hourID = hourID;
    bill.subHourID = subHourID;
    bill.hourFrame = hourFrame;
    console.log(bill);

    if (bookingID !== null) {
        bill.bookingID = bookingID;
    }
    bill.IsAutoStylist = isAutoStylist == 1 ? true : false;
    bill.IsBookAtSalon = isBookAtSalon == 1 ? true : false;

    $("#eventBillAdd").openEBPopup();
    $(".customer-phone").focus();
    if ($('.customer-phone').val() == '') {
        $('.div_checkCustomer').addClass('hidden');
    }
    if (snDay == 0) {
        snDay = "";
    }
    if (snMonth == 0) {
        snMonth = "";
    }
    if (snYear == 0) {
        snYear = "";
    }
    $('#EBPopup input.sn-day').val(snDay);
    $("#EBPopup input.sn-month").val(snMonth);
    $("#EBPopup input.sn-year").val(snYear);
    // Ẩn nút Đặt chỗ
    $("#EBPopup .btn-action-book").hide();
    // Mở list hóa chất
    $("#EBPopup .list-item-hc").show();
    // Ẩn list khách book Stylist
    $("#EBPopup .list-item-book-stylist").hide();

    $("#EBPopup .popup-hour-frame").text(hourFrame.substring(0, 5).toString());
    $("#EBPopup .popup-stylist span.sp-stylist-name").text(stylistName);
    if (customerName != "") {
        $("#EBPopup .customer-name").val(customerName);
    }
    if (customerPhone != "") {
        $("#EBPopup .customer-phone").val(ReplaceFirstPhone(changePhone(customerPhone), 4, 'X'));
    }

    if (isAutoStylist == 0) {
        $("#EBPopup .star-not-auto-stylist").addClass("active");
        $("#EBPopup .is-book-stylist").prop("checked", true);
    }

    if (isBookAtSalon == 1) {
        $("#EBPopup .customer-book-at-salon").addClass("active");
    }

    ms = $('#EBPopup .ms-suggest').magicSuggest({
        maxSelection: 10,
        data: listService,
        valueField: 'Id',
        displayField: 'Name'
    });
    $(ms).on('selectionchange', function (e, m) {
        bill.services = this.getSelection();
    });

    setDefaultService();

    if (This.parent().hasClass("is-makebill")) {
        //$("#EBPopup .btn-print-bill").hide();
        // Mở nút Cập nhật phiếu
        $("#EBPopup .btn-action-print").hide();
        $("#EBPopup .btn-action-print-update").show();
        startLoading();
        $.ajax({
            type: "POST",
            url: "/ver2-beta-timeline/getbillinfobyid",
            data: '{billTempID : ' + billID + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                var billTemp = response.d.data;
                console.log(billTemp);
                if (billTemp != null) {
                    // I. Mã KH, tên KH
                    if (billTemp.checkinID != null) {
                        $("#EBPopup .checkin-code").val(billTemp.checkinID);
                        $("#EBPopup .checkin-name").val(billTemp.checkinName);
                        checkCustomerType(billTemp.customerId, '');
                    }
                    // II. Set gói dịch vụ
                    if (billTemp.services != null) {
                        var arrService = [];
                        if (billTemp.services.length > 0) {
                            $.each(billTemp.services, function (i, v) {
                                arrService.push(v);
                            });
                        }
                        ms.setSelection(arrService);
                    }
                    // III. Hóa chất
                    if (billTemp.HCItem != null) {
                        var hcId;
                        var hcList = billTemp.HCItem.split(',');
                        if (hcList.length > 0) {
                            hcArr = [];
                            for (var i in hcList) {
                                if (hcList[i] != "") {
                                    hcId = parseInt(hcList[i]);
                                    if (!isNaN(hcId)) {
                                        hcArr.push(hcId);
                                    }
                                }
                            }

                            $("#EBPopup .list-item-hc input[type='checkbox']").each(function () {
                                var index = hcList.indexOf($(this).attr("data-value"));
                                if (index != -1) {
                                    $(this).prop("checked", true);
                                }
                            });
                        }
                    }
                }

                finishLoading();
            },
            failure: function (response) { alert(response.d); }
        });
    }
    else {
        checkCustomerType(0, PhoneItem);
        // Mở nút In phiếu
        $("#EBPopup .btn-action-print-update").hide();
        if (!isPrintBill) {
            $("#EBPopup .btn-action-print").hide();
            return;
        }
        $("#EBPopup .btn-action-print").show();
        //setDefaultService();	
    }
}

/*
* Mở popup để đặt lịch cho khách
*/
function openBooking(This, bookingID, stylistID, stylistName, customerPhone, customerName, hourID, SubHourId, hourFrame) {
    bill.stylistID = stylistID;
    bill.stylistName = stylistName;
    bill.hourID = hourID;
    bill.subHourID = SubHourId;
    bill.hourFrame = hourFrame;

    if (bookingID != null) {
        bill.bookingID = bookingID;
    }

    $("#eventBillAdd").openEBPopup();

    if ($('.customer-phone').val() == '') {
        $('.div_checkCustomer').addClass('hidden');
    }

    // Mở nút Đặt chỗ
    $("#EBPopup .btn-action-book").show();
    $(".customer-phone").focus();
    // Ẩn nút In phiếu
    $("#EBPopup .btn-action-print").hide();
    // Ẩn nút Cập nhật phiếu
    $("#EBPopup .btn-action-print-update").hide();
    // Ẩn team
    $("#EBPopup .team-wrap").hide();
    // Ẩn checkin
    $("#EBPopup .checkin-wrap").hide();
    // Ẩn dịch vụ
    $("#EBPopup .service-wrap").hide();
    // Ẩn list hóa chất
    $("#EBPopup .list-item-hc").hide();
    // Mở list khách book Stylist
    $("#EBPopup .list-item-book-stylist").show();

    $("#EBPopup .popup-hour-frame").text(hourFrame.substring(0, 5).toString());
    $("#EBPopup .popup-stylist span.sp-stylist-name").text(stylistName);
    if (customerName != "") {
        $("#EBPopup .customer-name").val(customerName);
    }
    if (customerPhone != "") {
        $("#EBPopup .customer-phone").val(ReplaceFirstPhone(changePhone(customerPhone), 4, 'X'));
    }

    ms = $('#EBPopup .ms-suggest').magicSuggest({
        maxSelection: 10,
        data: listService,
        valueField: 'Id',
        displayField: 'Name'
    });
    $(ms).on('selectionchange', function (e, m) {
        bill.services = this.getSelection();
    });

    setDefaultService();

    if (This.parent().hasClass("is-makebill")) {
        $("#EBPopup .btn-print-bill").hide();
    }
    else {
        //setDefaultService();	
    }
}

/*
* Set dịch vụ mặc định
*/
function setDefaultService() {
    if (listService.length > 0) {
        for (var index in listService) {
            if (listService[index].Id == defaultServiceID) {
                ms.setSelection([listService[index]]);
                break;
            }
        }
    }
}

/*
* Kiểm tra gọi điện thoại cho khách
*/
var interval = null;
function checkCallPhone(This, bookingID) {
    var isCall = false;
    if (This.find(">i").hasClass("is-call") && !$(This).parent().hasClass("is-makebill")) {
        order = 0;
        isCall = false;
        This.find(">i").removeClass("is-call");
        $(This).parent().find(".bill-created-time").css("display", "none");
        $(This).parent().find(".board-cell-content").find(".customer-name").find(".customer-text-wrap").find(".fa-warning").css("display", "none");

        $(This).parent().find(".board-cell-content").find(".customer-name").find('.customer-text-wrap').find('.fa-warning').addClass('hidden');
        if (interval != null) {
            clearInterval(interval);
            $(This).parent().find(".bill-created-time").attr('data-interval', '');
        }
        else {
            var a = $(This).parent().find(".bill-created-time").attr('data-interval');
            clearInterval(a);
        }
    }
    else if ($(This).parent().hasClass("is-makebill")) {
        if (This.find(">i").hasClass("is-call")) {
            isCall = false;
            This.find(">i").removeClass("is-call");
        }
        else {
            isCall = true;
            This.find(">i").addClass("is-call");
            This.parent().find(".order-checked").css('display', 'block');
        }
    }
    else {
        isCall = true;
        This.find(">i").addClass("is-call");
        This.parent().find(".order-checked").css('display', 'block');
        $(This).parent().find(".bill-created-time").css("display", "block");

        var seconds = 0;
        var minutes = 0;
        $(This).parent().find('.min').html(((minutes < 10 && minutes >= 0) ? "0" : "") + minutes);
        $(This).parent().find(".sec").html(((seconds < 10 && seconds >= 0) ? "0" : "") + seconds);
        interval = setInterval(function () {
            ++seconds;
            if (seconds > 59) {
                seconds = 0;
                minutes = minutes + 1;
                $(This).parent().find(".min").html(((minutes < 10) ? "0" : "") + minutes);
            }
            $(This).parent().find(".sec").html(((seconds < 10) ? "0" : "") + seconds);

            if ((minutes > 4) && (!$(This).parent().hasClass("is-book-at-salon"))) {
                $(This).parent().find(".board-cell-content").find(".customer-name").find('.customer-text-wrap').find('.fa-warning').removeClass('hidden');
            }
            else if ((minutes > 9) && ($(This).parent().hasClass("is-book-at-salon"))) {
                $(This).parent().find(".board-cell-content").find(".customer-name").find('.customer-text-wrap').find('.fa-warning').removeClass('hidden');
            }
            else if ((minutes > 4) && ($(This).parent().hasClass("is-makebill")) && ($(This).attr("data-procedure") == "")) {
                $(This).parent().find(".board-cell-content").find(".customer-name").find('.customer-text-wrap').find('.fa-warning').removeClass('hidden');
            }
            else {
                $(This).parent().find(".board-cell-content").find(".customer-name").find('.customer-text-wrap').find('.fa-warning').addClass('hidden');
            }
            $(This).parent().find(".min").attr('data-min', ((minutes < 10) ? "0" : "") + minutes);
            $(This).parent().find(".sec").attr('data-sec', ((seconds < 10) ? "0" : "") + seconds);
        }, 1000);
    }

    startLoading();
    $.ajax({
        type: "POST",
        url: "/ver2-beta-timeline/setcallphone",
        data: '{bookingID : ' + bookingID + ', isCall : ' + isCall + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json", success: function (response) {
            if (response.d != null) {
                var bookingRecord = JSON.parse(response.d.message);
                bill.checkinID = bookingRecord.Id;
                if (bookingRecord.Order != null) {
                    This.parent().find(".order-checked").text(bookingRecord.Order);
                }
                else {
                    This.parent().find(".order-checked").text("");
                }
                $("#EBPopup .checkin-name").val(response.d.Fullname);
            }
            else {
                bill.checkinID = 0;
                $("#EBPopup .checkin-name").val("");
            }

            //order_Checked_Salon();
            finishLoading();
        },
        failure: function (response) { alert(response.d); }
    });
}

/*
* Mở popup hủy lịch đặt
*/
function openCancelBook(This, bookingID, customerName, customerPhone) {
    $(".popup-cancel-book").openEBPopup();
    bookingCancel.cancelBookingID = bookingID;
    bookingCancel.customerName = customerName;
    bookingCancel.customerPhone = customerPhone;
    bookingCancel.itemBookingCancel = This;
}

/*
* Hủy lịch đặt
*/

function cancelBook() {
    bookingCancel.noteCancel = $("#EBPopup textarea.note-cancel").val();
    if (bookingCancel.cancelBookingID > 0) {
        if (bookingCancel.noteCancel.trim().length > 0) {
            startLoading();
            $.ajax({
                type: "POST",
                url: "/GUI/FrontEnd/Service/TimeLine_Beta_V2.aspx/cancelBook",
                data: '{bookingID : ' + bookingCancel.cancelBookingID + ', noteCancel : "' + bookingCancel.noteCancel + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    if (response.d != null && response.d.success) {
                        var _board = bookingCancel.itemBookingCancel.parent();
                        _board.removeClass("is-book");
                        _board.removeAttr("id");
                        _board.find(".board-cell-content").remove();
                        _board.find(".call-phone").remove();
                        _board.find(".cancel-book").remove();
                        _board.find(".stylist-not-auto").remove();
                        _board.find(".customer-hc").remove();
                        _board.prepend('<div class="board-cell-content"></div>');
                        alert("Hủy lịch thành công!");
                        location.href = location.href;
                        //autoCloseEBPopup();
                    }
                    else {
                        alert(response.d.message);
                    }
                    finishLoading();
                },
                failure: function (response) { alert(response.d); }
            });
        }
        else {
            alert("Vui lòng nhập lý do hủy lịch.");
        }
    }
    else {
        alert("Lỗi. Không xác định được khung giờ. Vui lòng liên hệ nhóm phát triển.")
    }
}

/*
* Update bill
*/

function updateBill() {
    bill.customerName = $("#EBPopup .customer-name").val();
    bill.customerPhone = changePhone(PhoneItem);
    console.log('updatebill - hcArr');
    console.log(hcArr);
    if (hcArr.length > 0) {
        var hcStr = '';
        for (var i = 0; i < hcArr.length; i++) {
            hcStr += hcArr[i] + ',';
        }
        bill.HCItem = hcStr.replace(/\,+$/, '');
    }

    var errorList = [];
    if (bill.customerName == "") {
        errorList.push("Tên Khách hàng không được để trống");
    }
    if (bill.customerPhone == "") {
        errorList.push("Số điện thoại không được để trống");
    }
    if (bill.services.length == 0) {
        errorList.push("Vui lòng chọn dịch vụ");
    }

    if (errorList.length > 0) {
        var errorStr = "";
        for (var index in errorList) {
            errorStr += "[" + errorList[index] + "] ";
        }
        alert("Vui lòng nhập đầy đủ thông tin " + errorStr);
    }
    else {
        bill.datedBook = $("#DatedBook").val().toString();
        startLoading();
        $.ajax({
            type: "POST",
            url: "/ver2-beta-timeline/updatebill",
            data: '{billClient : \'' + JSON.stringify(bill) + '\'}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                if (response.d != null && response.d.success == true) {
                    billResponse = response.d.data;
                    if (billResponse != null) {
                        var strCell = "<div class=\"board-cell-content\" onclick=\"openPrintBill($(this), " + billResponse.billID + ", " + billResponse.bookingID + ", " + billResponse.stylistID + ", '" + billResponse.stylistName + "', '" + changePhone(billResponse.customerPhone) + "', '" + billResponse.customerName + "', " + billResponse.hourID + ", " + billResponse.subHourID + ", '" + billResponse.hourFrame + "', " + (billResponse.IsAutoStylist ? 1 : 0) + ", " + (billResponse.IsBookAtSalon ? 1 : 0) + "," + billResponse.snDay + "," + billResponse.snMonth + "," + billResponse.snYear + ")\">" +
                            "<p class=\"customer-name\">" + billResponse.customerName + "</p>" +
                            "<p class=\"customer-phone\">" + (bill.IsSM == false || bill.IsSM == null ? "" : "SM - ") + "" + ReplaceFirstPhone(changePhone(billResponse.customerPhone), 4, 'X') + "</p>" +
                            "<p class=\"customer-phone\ hidden\">" +
                            "<span data-customer-code=\"" + billResponse.bookingID + "-" + changePhone(billResponse.customerPhone) + "-hidden-" + billResponse.billID + "\"></span>" +
                            "</p>" +
                            "</div>" +
                            "<div class=\"cancel-book\" title=\"Hủy lịch đặt\" onclick=\"openCancelBook($(this), " + billResponse.bookingID + ", '" + billResponse.customerName + "', '" + changePhone(billResponse.customerPhone) + "')\" style=\"display:none;\">" +
                            "<i class=\"fa fa-times\" aria-hidden=\"true\"></i>" +
                            "</div>" +
                            "<div class=\"call-phone\" title=\"Check gọi điện cho khách hàng\" onclick=\"checkCallPhone($(this), " + billResponse.bookingID + ")\">" +
                            "<i class=\"fa fa-check " + (billResponse.isCall ? "is-call" : "") + "\"" + "aria-hidden=\"true\"></i>" +
                            "</div>" +
                            //
                            //"<div class=\"isSalonNote\">" +
                            //"<button type=\"button\" class=\"btnSalonNote\" id=\"btn-" + bookingRecord.Id + "\" style='background:red' onclick=\"getNote(" + bookingRecord.Id + ")\"></button>" +
                            //"</div>" +
                            //
                            "<div class=\"board-cell-action\" data-id=\"" + billResponse.stylistID + "-" + billResponse.stylistName + "-" + billResponse.hourID + "-" + billResponse.hourFrame + "\">" +
                            "<div class=\"col-xs-6 cell-item-action cell-item-action-book\" onclick=\"openBooking($(this), " + billResponse.bookingID + ", " + billResponse.stylistID + ", '" + billResponse.stylistName + "', '', '', " + billResponse.hourID + ", '" + billResponse.hourFrame + "')\">Book lịch</div>" +
                            "</div>" +
                            '<div class="stylist-not-auto ' + (!(billResponse.IsAutoStylist) ? "active" : "") + '">B</div>' +
                            '<div class="customer-hc ' + (billResponse.HCItem != '' ? "active" : "") + '">H</div>';

                        // Nếu bill được lập từ cell đã booking
                        var cell = $("#" + billResponse.stylistID + "-" + billResponse.bookingID + "-bk");
                        if (cell.length > 0) {
                            var isMultipleItem = cell.parent().find(".board-cell-item").length > 1 ? true : false;
                            cell.empty().append($(strCell)).addClass("is-makebill");
                            if (isMultipleItem) {
                                cell.addClass("col-6");
                            }
                        }
                        else {
                            var cell = $("#" + billResponse.stylistID + "-" + billResponse.hourID);
                            var isMultipleItem = cell.find(".customer-name").length > 0 ? true : false;
                            strCell = "<div class=\"board-cell-item is-makebill is-enroll\" id=\"" + (billResponse.stylistID + "-" + billResponse.bookingID + "-bk") + "\">" + strCell + "</div>";
                            if (isMultipleItem) {
                                cell.append($(strCell));
                            }
                            else {
                                cell.empty().append($(strCell));
                            }
                        }

                        console.log(billResponse);
                        //location.href = location.pathname + "?msg_update_status=success&msg_update_message=Thêm%20thành%20công!&msg_print_billcode=" + billResponse.PDFBillCode;
                        //openPdfIframe("/Public/PDF/" + billResponse.PDFBillCode + ".pdf");
                        autoCloseEBPopup();

                        // Reset giá trị bill
                        bill.billID = 0;
                        bill.bookingID = 0;
                        bill.hourID = 0;
                        bill.subHourID = 0;
                        bill.hourFrame = '';
                        bill.stylistID = 0;
                        bill.stylistName = '';
                        bill.customerPhone = '';
                        bill.customerName = '';
                        bill.salonName = '';
                        bill.checkinID = 0;
                        bill.checkinName = '';
                        bill.services = [];
                        bill.teamID = 0;
                        bill.PDFBillCode = '';
                        bill.HCItem = '';
                        bill.IsAutoStylist = true;
                        bill.snDay = 0;
                        bill.snMonth = 0;
                        bill.snYear = 0;
                        PhoneItem = '';
                        // Reset 1 số biến giá trị
                        hcArr = [];
                    }
                    else {
                        alert("Đã có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển.");
                    }
                }
                else {
                    alert("Đã có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển.");
                }
                $('.div_checkCustomer').addClass('hidden');
                finishLoading();
            },
            failure: function (response) { alert(response.d); }
        });
    }
}

/*
* Insert booking
*/
function insertBooking() {
    PhoneItem = '';
    bill.customerName = $("#EBPopup .customer-name").val();
    bill.customerPhone = changePhone($("#EBPopup .customer-phone").val());
    PhoneItem = bill.customerPhone;
    bill.CustomerScore = $("#EBPopup .customer-name").attr("data-cusscore");
    // chan chuyen khac khung gio
    var countCusPhone = $(".customer-phone").length;
    var PhoneNew;
    var Index;
    for (var i = 0; i < countCusPhone; i++) {
        Index = $(".customer-phone").eq(i);
        PhoneNew = Index.text().trim();
        console.log("phonecheck", index);
        if (PhoneNew === bill.customerPhone) {
            //var asdfs = $(this).parent().parent().parent().attr("data-hourframe");
            var HourFrameOld = Index.parent().parent().parent().attr("data-hourframe");
            var IscheckOut = false;
            var IsMakebill = false;
            IscheckOut = Index.parent().parent().hasClass("is-checkout");
            IsMakebill = Index.parent().parent().hasClass("is-makebill");
            if (IscheckOut === true) {
                alert("Số điện thoại " + ReplaceFirstPhone(changePhone(bill.customerPhone), 4, 'X') + " đã CHECKOUT, bạn không thể cật nhật khung giờ ( đã CHECKOUT khung giờ " + HourFrameOld + " ).");
                $("body #EBCloseBtn").click();
                return;
            }
            if (HourFrameOld !== bill.hourFrame && IsMakebill === true) {
                alert("Số điện thoại " + ReplaceFirstPhone(changePhone(bill.customerPhone), 4, 'X') + " đã đặt lịch khung giờ " + HourFrameOld + ". Chỉ có thể chuyển trong cùng khung giờ khi đã IN BILL! Khung giờ hiện tại là :" + bill.hourFrame);
                $("body #EBCloseBtn").click();
                return;
            }
        }
    }
    // End chan chuyen khac khung gio
    var errorList = [];

    if (bill.customerName === "") {
        errorList.push("Tên Khách hàng không được để trống");
    }

    if (bill.customerPhone === "") {
        errorList.push("Số điện thoại không được để trống");
    }

    if (PhoneItem.length != 10 || PhoneItem.charAt(0) != 0) {
        errorList.push("Số điện thoại không đúng. Số điện thoại phải bằng 10 kí tự và bắt đầu bằng 0");
    }
    if (errorList.length > 0) {
        var errorStr = "";
        for (var index in errorList) {
            errorStr += "[" + errorList[index] + "] ";
        }
        alert("Vui lòng nhập đầy đủ thông tin " + errorStr);
    }


    else {
        //debugger;
        bill.datedBook = $("#DatedBook").val().toString();
        //if ($("#EBPopup input#check-birthday").prop('checked')) {
        //    bill.snDay = null;
        //    bill.snMonth = null;
        //    bill.snYear = null;
        //}
        //else {
        //if (!ValidBirthDay()) {
        //    return;
        //}
        //else {
        bill.snDay = $('#EBPopup input.sn-day').val() == "" ? null : $('#EBPopup input.sn-day').val();
        bill.snMonth = $("#EBPopup input.sn-month").val() == "" ? null : $("#EBPopup input.sn-month").val();
        bill.snYear = $("#EBPopup input.sn-year").val() == "" ? null : $("#EBPopup input.sn-year").val();
        //}
        //}
        startLoading();
        $.ajax({
            type: "POST",
            url: "/GUI/FrontEnd/Service/TimeLine_Beta_V2.aspx/checkBooking",
            data: '{billClient : \'' + JSON.stringify(bill) + '\'}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                console.log(response.d.message);
                if (response.d !== null && response.d.success === true) {

                    //console.log("bookingRecord", bookingRecord);
                    //console.log("bill", bill);
                    var bookingRecord = JSON.parse(response.d.message);
                    var customer_info = JSON.parse(response.d.dtShow);

                    if (customer_info.SN_day != 0 || customer_info.SN_month != 0 || customer_info.SN_year != 0) {
                        bill.snDay = customer_info.SN_day;
                        bill.snMonth = customer_info.SN_month;
                        bill.snYear = customer_info.SN_year;
                    }
                    else {
                        bill.snDay = 0;
                        bill.snMonth = 0;
                        bill.snYear = 0;
                    }
                    console.log(customer_info);
                    var cell = $("#" + bill.stylistID + "-" + bill.hourID + "-" + bill.subHourID);

                    var isMultipleItem = cell.find(".customer-name").length > 0 ? true : false;

                    if (bookingRecord.Id > 0) {
                        checkCustomerType(0, bill.customerPhone);
                        //SendBookingRequest(bookingRecord.Id, bookingRecord.CustomerPhone, bookingRecord.SalonId);
                        //CountingRequestBooking();
                        var strCell = "<div class=\"board-cell-item " + funScoreCus(bookingRecord.CustomerScore) + " is-enroll event is-book is-book-at-salon" + (isMultipleItem ? " col-6 " : "") + "\" id=\"" + (bill.stylistID + "-" + bookingRecord.Id) + "-bk\" data-cusscore=\"" + bookingRecord.CustomerScore + "\" draggable=\"true\" ondragstart=\"timeline_drag($(this), event);\">" +
                            "<div class=\"board-cell-content\" onclick=\"openPrintBill($(this), 0, " + bookingRecord.Id + ", " + bill.stylistID + ", '" + bill.stylistName + "', '" + bill.customerPhone + "', '" + bill.customerName + "', " + bill.hourID + ", " + bill.subHourID + ", '" + bill.hourFrame + "', " + (bookingRecord.IsAutoStylist ? 1 : 0) + ", " + (bookingRecord.IsBookAtSalon ? 1 : 0) + "," + bill.snDay + "," + bill.snMonth + "," + bill.snYear + ")\">" +
                            "<i class=\"triangle-top-left\"></i>" +
                            "<p class=\"customer-name\">" +
                            "<span class=\"customer-text-wrap\">" +
                            "<i class=\"fa fa-warning hidden\" aria-hidden=\"true\" style=\"margin-right: 5px; color: #F44336; font-size: 14px; display:none;\"></i>" +
                            bill.customerName +
                            "<i class=\"fa fa-star " + (customerType ? "" : " hidden ") + "\" aria-hidden=\"true\" style=\"margin-left: 5px; color: #fddd4a; font-size: 14px; display:inline-block;\"></i></p>" +
                            "<span class=\"show-checkout\"></span>" +
                            "</span>" +
                            "</p>" +
                            "<p class=\"customer-phone\">" + (bill.IsSM == true ? "SM - " : "") + "" + ReplaceFirstPhone(changePhone(bill.customerPhone), 4, 'X') + "</p>" +
                            "<p class=\"customer-phone\ hidden\">" +
                            "<span data-customer-code=\"" + bookingRecord.Id + "-" + bill.customerPhone + "-hidden-" + 0 + "\"></span>" +
                            "</p>" +
                            "<div class=\"bill-created-time\" style=\"display: none; color:#ffffff;\">" +
                            "<input type='hidden' id='hd-booking-id' value='" + bookingRecord.Id + "'>" +
                            "<label class=\"min\" data-min=\"0\"></label>:<label class=\"sec\" data-sec=\"0\"></label>" +
                            "</div>" +
                            "</div>" +
                            "<div class=\"cancel-book\" title=\"Hủy lịch đặt\" onclick=\"openCancelBook($(this), " + bookingRecord.Id + ", '" + bookingRecord.CustomerName + "', '" + changePhone(bookingRecord.CustomerPhone) + "')\">" +
                            "<i class=\"fa fa-times\" aria-hidden=\"true\"></i>" +
                            "</div>" +
                            "<div class=\"order-checked\" style=\"position: absolute; bottom: 0px; right: 22px; color:#ffffff; display: inline-block;\">" + (bookingRecord.Order != null ? bookingRecord.Order : "") + "</div>" +

                            "<div class=\"call-phone\" title=\"Check gọi điện cho khách hàng\" onclick=\"checkCallPhone($(this), " + bookingRecord.Id + ")\" data-time=\"\">" +
                            "<i class=\"fa fa-check\"" + "aria-hidden=\"true\"></i>" +
                            "</div>" +
                            "<div class=\"board-cell-action\" data-id=\"" + bill.stylistID + "-" + bill.stylistName + "-" + bill.hourID + "-" + bill.hourFrame + "\">" +
                            "<div class=\"col-xs-6 cell-item-action cell-item-action-printbill\" " + (isPrintBill ? "" : "style='display:none !important;'") + "  onclick=\"openPrintBill($(this), 0, " + bookingRecord.Id + ", " + bill.stylistID + ", '" + bill.stylistName + "', '" + bill.customerPhone + "', '" + bill.customerName + "', " + bill.hourID + ", " + bill.subHourID + ", '" + bill.hourFrame + "', " + (bookingRecord.IsAutoStylist ? 1 : 0) + ", " + (bookingRecord.IsBookAtSalon ? 1 : 0) + "," + bill.snDay + "," + bill.snMonth + "," + bill.snYear + ")\">In phiếu</div>" +
                            "<div class=\"col-xs-6 cell-item-action cell-item-action-book\" onclick=\"openBooking($(this), " + bookingRecord.Id + ", " + bill.stylistID + ", '" + bill.stylistName + "', '', '', " + bill.hourID + ", " + bill.subHourID + ", '" + bill.hourFrame + "')\">Book lịch</div>" +
                            "</div>" +
                            '<div class="stylist-not-auto ' + (!(bookingRecord.IsAutoStylist) ? "active" : "") + '">B</div>' +
                            '<div class="customer-hc ' + (bookingRecord.IsHCItem ? "active" : "") + '">H</div>' +
                            '</div>';

                        if (isMultipleItem) {
                            cell.append($(strCell));
                            cell.find(".board-cell-item").addClass("col-6");
                        }
                        else {
                            cell.empty().append($(strCell));
                        }
                    }

                    autoCloseEBPopup();

                    // Reset giá trị bill
                    bill.billID = 0;
                    bill.bookingID = 0;
                    bill.hourID = 0;
                    bill.subHourID = 0;
                    bill.hourFrame = '';
                    bill.stylistID = 0;
                    bill.stylistName = '';
                    bill.customerPhone = '';
                    bill.customerName = '';
                    bill.salonName = '';
                    bill.checkinID = 0;
                    bill.checkinName = '';
                    bill.services = [];
                    bill.teamID = 0;
                    bill.PDFBillCode = '';
                    bill.HCItem = '';
                    bill.IsAutoStylist = true;
                    bill.IsBookAtSalon = true;
                    bill.CustomerScore = 0;
                    bill.snDay = 0;
                    bill.snMonth = 0;
                    bill.snYear = 0;
                    PhoneItem = '';

                    // Reset 1 số biến giá trị
                    hcArr = [];
                    //window.location.reload();
                }

                else if (response.d.success === false && response.d.status === "update") {
                    if (confirm(response.d.message)) {
                        updateStylistBookTimeline();
                    } else {
                        console.log("Huỷ");
                    }
                } else {
                    alert("Đã có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển.");
                }
                $('.div_checkCustomer').addClass('hidden');
                finishLoading();
            },
            failure: function (response) { alert(response.d); }
        });



    }
}
function updateStylistBookTimeline() {
    bill.customerName = $("#EBPopup .customer-name").val();
    //SendBookingRequest($("#hd-booking-id").val(), bill.customerPhone, bill.salonID);
    //CountingRequestBooking();
    bill.customerPhone = changePhone(PhoneItem);

    var errorList = [];
    if (bill.customerName == "") {
        errorList.push("Tên Khách hàng");
    }
    if (bill.customerPhone == "") {
        errorList.push("Số điện thoại");
    }

    if (errorList.length > 0) {
        var errorStr = "";
        for (var index in errorList) {
            errorStr += "[" + errorList[index] + "] ";
        }
        alert("Vui lòng nhập đầy đủ thông tin " + errorStr);
    }
    else {
        bill.datedBook = $("#DatedBook").val().toString();
        startLoading();
        $.ajax({
            type: "POST",
            url: "/GUI/FrontEnd/Service/TimeLine_Beta_V2.aspx/updateStylistBookTimeline",
            data: '{billClient : \'' + JSON.stringify(bill) + '\'}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                //console.log(response);
                if (response.d != null && response.d.success == true) {
                    console.log(response.d);
                    location.reload();

                } else {
                    alert(response.d.message);
                }
                finishLoading();
            },
            failure: function (response) { alert(response.d); }
        });
    }
}

function NotdeleteBooking() {
    bill.customerName = $("#EBPopup .customer-name").val();
    bill.customerPhone = changePhone($("#EBPopup .customer-phone").val());

    var errorList = [];
    if (bill.customerName == "") {
        errorList.push("Tên Khách hàng");
    }
    if (bill.customerPhone == "") {
        errorList.push("Số điện thoại");
    }

    if (errorList.length > 0) {
        var errorStr = "";
        for (var index in errorList) {
            errorStr += "[" + errorList[index] + "] ";
        }
        alert("Vui lòng nhập đầy đủ thông tin " + errorStr);
    }
    else {
        bill.datedBook = $("#DatedBook").val().toString();
        startLoading();
        $.ajax({
            type: "POST",
            url: "/GUI/FrontEnd/Service/TimeLine_Beta_V2_2.aspx/insertBooking",
            data: '{billClient : \'' + JSON.stringify(bill) + '\', isDelete: "' + parseInt(0) + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                //console.log(response);
                if (response.d != null && response.d.success == true) {
                    var bookingRecord = JSON.parse(response.d.message);
                    var cell = $("#" + bill.stylistID + "-" + bill.hourID);
                    var isMultipleItem = cell.find(".customer-name").length > 0 ? true : false;
                    if (bookingRecord.Id > 0) {
                        var strCell = "<div class=\"board-cell-item event is-enroll is-book is-book-at-salon" + (isMultipleItem ? " col-6 " : "") + "\" id=\"" + (bill.stylistID + "-" + bookingRecord.Id) + "-bk\" draggable=\"true\" ondragstart=\"timeline_drag($(this), event);\">" +
                            "<div class=\"board-cell-content\" onclick=\"openPrintBill($(this), 0, " + bookingRecord.Id + ", " + bill.stylistID + ", '" + bill.stylistName + "', '" + bill.customerPhone + "', '" + bill.customerName + "', " + bill.hourID + ", " + bill.subHourID + ", '" + bill.hourFrame + "', " + (bookingRecord.IsAutoStylist ? 1 : 0) + ", " + (bookingRecord.IsBookAtSalon ? 1 : 0) + ")\">" +
                            "<p class=\"customer-name\">" + bill.customerName + "</p>" +
                            "<p class=\"customer-phone\">" + (bill.IsSM == false || bill.IsSM == null ? "" : "SM - ") + "" + changePhone(bill.customerPhone) + "</p>" +
                            "</div>" +
                            "<div class=\"cancel-book\" title=\"Hủy lịch đặt\" onclick=\"openCancelBook($(this), " + bookingRecord.Id + ", '" + bookingRecord.CustomerName + "', '" + changePhone(bookingRecord.CustomerPhone) + "')\">" +
                            "<i class=\"fa fa-times\" aria-hidden=\"true\"></i>" +
                            "</div>" +
                            "<div class=\"call-phone\" title=\"Check gọi điện cho khách hàng\" onclick=\"checkCallPhone($(this), " + bookingRecord.Id + ")\">" +
                            "<i class=\"fa fa-check\"" + "aria-hidden=\"true\"></i>" +
                            "</div>" +
                            ////
                            //"<div class=\"isSalonNote\">" +
                            //"<button type=\"button\" class=\"btnSalonNote\" id=\"btn-" + bookingRecord.Id + "\" style='background:red' onclick=\"getNote(" + bookingRecord.Id + ")\"></button>" +
                            //"</div>" +
                            ////
                            "<div class=\"board-cell-action\" data-id=\"" + bill.stylistID + "-" + bill.stylistName + "-" + bill.hourID + "-" + bill.hourFrame + "\">" +
                            "<div class=\"col-xs-6 cell-item-action cell-item-action-printbill\" " + (isPrintBill ? "" : "style='display:none !important;'") + " onclick=\"openPrintBill($(this), 0, " + bookingRecord.Id + ", " + bill.stylistID + ", '" + bill.stylistName + "', '" + bill.customerPhone + "', '" + bill.customerName + "', " + bill.hourID + ", " + bill.subHourID + ", '" + bill.hourFrame + "', " + (bookingRecord.IsAutoStylist ? 1 : 0) + ", " + (bookingRecord.IsBookAtSalon ? 1 : 0) + ")\">In phiếu</div>" +
                            "<div class=\"col-xs-6 cell-item-action cell-item-action-book\" onclick=\"openBooking($(this), " + bookingRecord.Id + ", " + bill.stylistID + ", '" + bill.stylistName + "', '', '', " + bill.hourID + ", '" + bill.hourFrame + "')\">Book lịch</div>" +
                            "</div>" +
                            '<div class="stylist-not-auto ' + (!(bookingRecord.IsAutoStylist) ? "active" : "") + '">B</div>' +
                            '<div class="customer-hc ' + (bookingRecord.IsHCItem ? "active" : "") + '">H</div>' +
                            '</div>';
                        if (isMultipleItem) {
                            cell.append($(strCell));
                            cell.find(".board-cell-item").addClass("col-6");
                        }
                        else {
                            cell.empty().append($(strCell));
                        }
                    }

                    autoCloseEBPopup();

                    // Reset giá trị bill
                    bill.billID = 0;
                    bill.bookingID = 0;
                    bill.hourID = 0;
                    bill.subHourID = 0;
                    bill.hourFrame = '';
                    bill.stylistID = 0;
                    bill.stylistName = '';
                    bill.customerPhone = '';
                    bill.customerName = '';
                    bill.salonName = '';
                    bill.checkinID = 0;
                    bill.checkinName = '';
                    bill.services = [];
                    bill.teamID = 0;
                    bill.PDFBillCode = '';
                    bill.HCItem = '';
                    bill.IsAutoStylist = true;
                    bill.IsBookAtSalon = true;
                    bill.CustomerScore = 0;
                    bill.snDay = 0;
                    bill.snMonth = 0;
                    bill.snYear = 0;
                    PhoneItem = '';
                    // Reset 1 số biến giá trị
                    hcArr = [];

                } else {
                    alert("Đã có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển.");
                }
                finishLoading();
            },
            failure: function (response) { alert(response.d); }
        });
    }
}

/*
* Tìm kiếm khách hàng từ bảng timeline
*/
function searchByPhone(evt, This) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    var isMatch = false;
    if (charCode == 13) {
        $("p.customer-phone").each(function () {
            if ($(this).text() == This.val()) {
                $(this).parent().click();
                isMatch = true;
            }
        });
        if (!isMatch) {
            alert("Không tìm thấy số điện thoại.");
        }
        return false;
    }

    return true;
}

/*
* Mở file pdf
*/
function openPdfIframe(src) {
    var PDF = document.getElementById("iframePrint");
    PDF.src = src;
    PDF.onload = function () {
        PDF.focus();
        PDF.contentWindow.print();
        PDF.contentWindow.close();
    }
}

(function () {

    var beforePrint = function () {
        console.log('Functionality to run before printing.');
    };

    var afterPrint = function () {
        console.log('Functionality to run after printing');
    };

    if (window.matchMedia) {
        var mediaQueryList = window.matchMedia('print');
        mediaQueryList.addListener(function (mql) {
            if (mql.matches) {
                beforePrint();
            } else {
                afterPrint();
            }
        });
    }

    window.onbeforeprint = beforePrint;
    window.onafterprint = afterPrint;

}());

window.onafterprint = function () {
    console.log("Printing completed...");
}

/*
* Hàm set thanh timeline
*/
var date = new Date();
var strDate = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
var ttStart = (new Date(strDate + " 08:30:00").getTime() / 1000);

function setTimeLine() {
    var date = new Date();
    var ttNow = Math.floor(date.getTime() / 1000);
    var ttCurrent = ttNow - ttStart;
    // (ttCurrent / 60 ) = so phut 
    // 280 : px cua 1 khung gio, 15 : so phu cua 1 khung gio
    // (280/15) = so Px cua 1 phut
    // boardLeftNow = so phut * so px của 1 phut
    var boardLeftNow = (ttCurrent / 60) * (280 / 15);
    $("div.board-book").animate({
        scrollLeft: boardLeftNow - 280
    }, 0);
}

/*
* Hàm check bill dùng hóa chất
*/
var hcArr = [];
function setHoaChat(This, itemValue) {
    console.log("test - hcArr");
    console.log(hcArr);
    console.log("test - itemValue");
    console.log(itemValue);
    var index = hcArr.indexOf(itemValue);
    if (This.prop("checked")) {
        if (index == -1) {
            hcArr.push(itemValue);
        }
    }
    else {
        hcArr.splice(index, 1);
    }
}

/*
* Hàm set khách chủ động book Stylist hay không
*/
function setBookStylist(This) {
    if (This.prop("checked")) {
        bill.IsAutoStylist = false;
    }
    else {
        bill.IsAutoStylist = true;
    }
    console.log('set auto : ' + bill);
    console.log('IsAutoStylist : ' + bill.IsAutoStylist);
}

function setBookSalon(This) {
    if (This.prop("checked")) {
        bill.IsBookAtSalon = true;
    }
    else {
        bill.IsBookAtSalon = false;
    }
    console.log('set auto : ' + bill);
    console.log('IsBookAtSalon : ' + bill.IsBookAtSalon);
}

//set cho bill đấy là book stylist hay không
function printBillSetAutoStylist(This) {
    var checked;

    if (This.is(":checked")) {
        checked = 1;
        This.prop('checked', true);
        bill.IsAutoStylist = false;

    } else {
        checked = 0;
        bill.IsAutoStylist = true;
    }
    $.ajax({
        url: "/GUI/FrontEnd/Service/TimeLine_Beta_V2.aspx/SetIsBookStylist",
        data: "{check:" + checked + ",bookingID:" + bill.bookingID + "}",
        type: "POST",
        dataType: "JSON",
        contentType: "application/json;charset:UTF-8",
        success: function (data) {
            //jsonData = JSON.parse(data.d);
            if (data.BookingTempId == 0) {
                return;
            }

            if (data.IsAutoStylist == true) {
                $("#stylist-not-auto-" + data.BookingTempId).hide();
                $(".star-not-auto-stylist").hide();
            } else {
                $("#stylist-not-auto-" + data.BookingTempId).show();
                $(".star-not-auto-stylist").show();
            }
        }
    });
}

/*
    * update booking (drag and drop book cell)
*/
function updateBooking_drop(customerPhone, bookingId, hourId, subHourID, stylistId, minutes, seconds) {
    $.ajax({
        type: "POST",
        url: "/ver2-beta-timeline/updateBooking",
        data: '{customerPhone :"' + customerPhone + '", bookingId:' + bookingId + ', stylistId: ' + stylistId + ', hourId: ' + hourId + ', subHourID : ' + subHourID + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            //console.log(response);
            if (response.d !== null && response.d.success === true) {
                var bookingRecord = JSON.parse(response.d.message);
                //console.log("bookingRecord", bookingRecord);
                //console.log("bill", bill);
                //SendBookingRequest(bookingRecord.bookingId, bookingRecord.CustomerPhone, bookingRecord.salonId);
                //CountingRequestBooking();
                //return;
                var minute = bookingRecord.HourFrame.Minutes;
                if (minute === 0) {
                    bookingRecord.HourFrame.Minutes = '00';
                }
                var hourFrame = bookingRecord.HourFrame.Hours + ':' + bookingRecord.HourFrame.Minutes + ':00';
                //console.log("hourFrame", hourFrame);
                var cell = $("#" + stylistId + "-" + hourId + "-" + subHourID);
                //console.log("call", cell);
                var isMultipleItem = cell.find(".customer-name").length > 0 ? true : false;
                var bookAtSalon = bookingRecord.IsBookAtSalon > 0 ? true : false;
                var callTime = (bookingRecord.IsCallTimeModified !== null && bookingRecord.IsCallTimeModified !== '') ? bookingRecord.IsCallTimeModified : bookingRecord.IsCallTime;
                var valueTime = "";
                if (callTime !== null) {
                    valueTime = ConvertJsonDateToStringDateTime(callTime);
                }
                if (bookingRecord.bookingId > 0) {
                    let display = '';
                    if ((bookingRecord.TextNote1 !== undefined && bookingRecord.TextNote1 !== null && bookingRecord.TextNote1 !== "") ||
                        (bookingRecord.TextNote2 !== undefined && bookingRecord.TextNote2 !== null && bookingRecord.TextNote2 !== "") ||
                        (bookingRecord.RatingNote !== undefined && bookingRecord.RatingNote !== null && bookingRecord.RatingNote !== "")
                    ) {
                        display = "<div class=\"isSalonNote\">" +
                            "<button type=\"button\" class=\"btnSalonNote\" id=\"btn-" + bookingRecord.bookingId + "\" style='background:red' onclick=\"getNote(" + bookingRecord.bookingId + ")\"></button>" +
                            "</div>";
                    }
                    var strCell = "<div class=\"board-cell-item " + funScoreCus(bookingRecord.CustomerScore) + " event " + (bookingRecord.billId > 0 ? "is-makebill" : "") + " is-enroll is-book " + (bookAtSalon ? " is-book-at-salon " : "") + (isMultipleItem ? " col-6 " : "") + "\" id=\"" + (stylistId + "-" + bookingRecord.bookingId) + "-bk\" data-cusscore=\"" + bookingRecord.CustomerScore + "\" draggable=\"true\" ondragstart=\"timeline_drag($(this), event);\">" +
                        "<div class=\"board-cell-content\" onclick=\"openPrintBill($(this), 0, " + bookingRecord.bookingId + ", " + stylistId + ", '" + bookingRecord.stylistName + "', '" + bookingRecord.CustomerPhone + "', '" + bookingRecord.CustomerName + "', " + bookingRecord.HourId + ", " + bookingRecord.SubHourId + ", '" + hourFrame + "', " + (bookingRecord.IsAutoStylist ? 1 : 0) + ", " + (bookingRecord.IsBookAtSalon ? 1 : 0) + "," + bookingRecord.snDay + "," + bookingRecord.snMonth + "," + bookingRecord.snYear + ")\">" +
                        "<i class=\"triangle-top-left\"></i>" +
                        "<p class=\"customer-name\">" +
                        "<span class=\"customer-text-wrap\">" +
                        "<i class=\"fa fa-warning hidden\" aria-hidden=\"true\" style=\"margin-right: 5px; color: #F44336; font-size: 14px; display:inline-block;\"></i>" +
                        bookingRecord.CustomerName +
                        "<i class=\"fa fa-star hidden\" aria-hidden=\"true\" style=\"margin-left: 5px; color: #fddd4a; font-size: 14px; display:inline-block;\"></i>" +
                        "<span class=\"show-checkout\"></span>" +
                        "</span>" +
                        "</p>" +

                        "<p class=\"customer-phone\">" + (bill.IsSM == false || bill.IsSM == null ? "" : "SM - ") + "" + ReplaceFirstPhone(changePhone(bookingRecord.CustomerPhone), 4, 'X') + "</p>" +
                        "<p class=\"customer-phone\ hidden\">" +
                        "<span data-customer-code=\"" + bookingRecord.bookingId + "-" + changePhone(bookingRecord.CustomerPhone) + "-hidden-" + 0 + "\"></span>" +
                        "</p>" +
                        "<div class=\"bill-created-time\" style=\"display: none; color:#ffffff;\">" +
                        "<label class=\"min\" data-min=\"" + minutes + "\"></label>:<label class=\"sec\" data-sec=\"" + seconds + "\"></label>" +
                        "</div>" +
                        "</div>" +
                        "<div class=\"cancel-book\" " + (bookingRecord.billId > 0 ? "style=\"display:none\"" : "") + "    title=\"Hủy lịch đặt\" onclick=\"openCancelBook($(this), " + bookingRecord.bookingId + ", '" + bookingRecord.CustomerName + "', '" + bookingRecord.CustomerPhone + "')\">" +
                        "<i class=\"fa fa-times\" aria-hidden=\"true\"></i>" +
                        "</div>" +
                        "<div class=\"order-checked\" style=\"position: absolute; bottom: 0px; right: 22px; color:#ffffff; display: inline-block;\">" + (bookingRecord.Order != null ? bookingRecord.Order : "") + "</div>" +
                        "<div class=\"call-phone\" title=\"Check gọi điện cho khách hàng\" onclick=\"checkCallPhone($(this), " + bookingRecord.bookingId + ")\" data-time=\"" + valueTime + "\">" +
                        "<i class=\"fa fa-check" + (bookingRecord.IsCall ? " is-call " : "") + "\"aria-hidden=\"true\"></i>" +
                        "</div>" +
                        display +
                        "<div class=\"board-cell-action\" data-id=\"" + stylistId + "-" + bookingRecord.stylistName + "-" + bookingRecord.HourId + "-" + "" + subHourID + "" + "-" + hourFrame + "\">" +
                        "<div class=\"col-xs-6 cell-item-action cell-item-action-printbill\" " + (isPrintBill ? "" : "style='display:none !important;'") + "  onclick=\"openPrintBill($(this), 0, " + bookingRecord.bookingId + ", " + stylistId + ", '" + bookingRecord.stylistName + "', '" + bookingRecord.CustomerPhone + "', '" + bookingRecord.CustomerName + "', " + bookingRecord.HourId + ", " + bookingRecord.SubHourId + ", '" + hourFrame + "', " + (bookingRecord.IsAutoStylist ? 1 : 0) + ", " + (bookingRecord.IsBookAtSalon ? 1 : 0) + ", " + (bookingRecord.IsBookAtSalon ? 1 : 0) + "," + bookingRecord.snDay + "," + bookingRecord.snMonth + "," + bookingRecord.snYear + ")\">In phiếu</div>" +
                        "<div class=\"col-xs-6 cell-item-action cell-item-action-book\" onclick=\"openBooking($(this), " + bookingRecord.bookingId + ", " + stylistId + ", '" + bookingRecord.stylistName + "', '', '', " + bookingRecord.HourId + ", " + bookingRecord.SubHourId + ", '" + hourFrame + "')\">Book lịch</div>" +
                        "</div>" +
                        '<div class="stylist-not-auto ' + (!(bookingRecord.IsAutoStylist) ? "active" : "") + '">B</div>' +
                        '<div class="customer-hc">H</div>' +
                        '</div>';
                    if (isMultipleItem) {
                        cell.append($(strCell));
                        cell.find(".board-cell-item").addClass("col-6");
                    }
                    else {
                        cell.empty().append($(strCell));
                    }
                }

                loadSetWaitingTime();
            }
            else {
                alert(response.d.messageNoti);
                window.location.reload();
            }
            finishLoading();
            window.location.reload();
        },
        failure: function (response) { alert(response.d); }
    });

}
var customerType;
/*
* Check khách đặc biệt hoặc khách VIP
*/
function checkCustomerType(customerId, customerPhone) {
    $.ajax({
        type: "POST",
        url: "/GUI/FrontEnd/Service/TimeLine_Beta_V2.aspx/checkCustomerType",
        data: '{customerId : ' + customerId + ', customerPhone: "' + customerPhone + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json", success: function (response) {
            if (response.d != null && response.d.success == true) {
                var customer = JSON.parse(response.d.message);
                //console.log(customer);
                $(".div_checkCustomer").removeClass('hidden');
                if (customer.CustomerTypeId == 1) {
                    $(".reason").removeClass('hidden');
                    $(".customerType").empty();
                    $(".customerType").append('<i class="fa fa-star" aria-hidden="true" style="margin-right: 10px; color: #fddd4a; font-size: 18px;"></i>' + customer.TypeName);
                    $(".reason").text("- " + customer.ReasonDiff);
                    console.log(customer.TypeName);
                    console.log(customer.ReasonDiff);
                }
                else if (customer.CustomerTypeId == 2) {
                    $(".customerType").empty();
                    $(".customerType").append('<i class="fa fa-star" aria-hidden="true" style="margin-right: 10px; color: #fddd4a; font-size: 18px;"></i>' + customer.TypeName);
                    $(".reason").addClass('hidden');
                    console.log(customer.TypeName);
                }
                else {
                    $('.div_checkCustomer').addClass('hidden');
                }
                customerType = customer.CustomerTypeId != null ? true : false;
            }
            else {
                $('.div_checkCustomer').addClass('hidden');
            }
            finishLoading();
        },
        failure: function (response) { alert(response.d.message); }
    });
}

/*
* Cac xu ly keo tha 
*/

function timeline_drag(THIS, event) {

    $(THIS).find('.board-cell-action').addClass('hidden');
    event.dataTransfer.setData("Text", $(THIS).attr('id'));

}
function timeline_dragleave(THIS, event) {
    if ($(THIS).children().hasClass("board-cell-item")) {
        $(THIS).find(".board-cell-content").css("background", "");
    }
}

function timeline_drop(THIS, event) {

    event.preventDefault();
    event.stopPropagation();
    if (!$(THIS).hasClass("is-checkout")) {
        if ($(THIS).children().hasClass("board-cell-item")) {
            $(THIS).find(".board-cell-content").css("background", "#ffe4b9");
        }
        if (event.type === 'drop') {
            if ($(THIS).children().hasClass("board-cell-item")) {
                $(THIS).find(".board-cell-content").css("background", "");
            }

            //  chan chuyen khac khung gio
            let hourOld = 0, subHourOld = 0, hourNew, subHourNew, x = 0, y = 0, IsMakebill = false;
            if ($(THIS).attr('id') !== "") {
                var dataIdOld = event.dataTransfer.getData('Text', $(THIS).attr('id'));
                hourOld = $('#' + dataIdOld).parent().attr('id').split("-")[1];
                subHourOld = $('#' + dataIdOld).parent().attr('id').split("-")[2];
            }
            x = parseInt(subHourOld) + 1;
            y = parseInt(subHourOld) - 1;
            hourNew = $(THIS).attr('id').split("-")[1];
            subHourNew = $(THIS).attr('id').split("-")[2];
            IsMakebill = $('#' + dataIdOld).hasClass("is-makebill");
            if (hourOld !== hourNew && subHourNew !== x && subHourNew !== y && IsMakebill === true) {
                alert("Chỉ có thể chuyển trong cùng khung giờ khi đã IN BILL !");
                return;
            }
            // End chan chuyen khac khung gio
            $(THIS).find('.board-cell-action').removeClass('hidden');
            var not_enroll = $(THIS).children().hasClass('not-enroll');
            var col_6 = $(THIS).children().hasClass('col-6');
            var is_makebill = $(THIS).children().hasClass('is-makebill');
            var is_checkout = $(THIS).children().hasClass('is-checkout');
            var is_book = $(THIS).children().hasClass('is-book');
            if (not_enroll) {
                alert("Stylist không chấm công khung h này.");
                return;
            }
            else if (col_6) {
                alert("Đã kín lịch, vui lòng chọn khung giờ khác.");
                return;
            }
            else if (is_checkout) {
                alert("");
            }
            else if (!col_6 && !not_enroll && (is_book || is_makebill)) {
                var data = event.dataTransfer.getData('Text', $(THIS).attr('id'));
                var test = $('#' + data).parent().html();
                var olderData = $('#' + data).attr("id");
                var test1 = $(THIS).html();
                var idOlder = $('#' + data).parent().attr("id");
                var cell_act_Id = $('#' + data).find('.board-cell-action').attr('data-id');
                console.log("cell_act_Id", cell_act_Id);
                console.log("cell_act_Id.split(" - ")[4]", cell_act_Id.split("-")[4]);
                var minutes = $('#' + data).find('.board-cell-content').find(".bill-created-time").find(".min").attr('data-min');
                var seconds = $('#' + data).find('.board-cell-content').find(".bill-created-time").find(".sec").attr('data-sec');
                if (!$('#' + data).hasClass("is-checkout")) {
                    startLoading();
                    if (confirm('Bạn có muốn đổi booking cho khách?')) {
                        var strCell = '<div class="board-cell-item is-enroll">' +
                            '<div class="board-cell-content"></div>' +
                            "<div class=\"board-cell-action\" data-id=\"" + cell_act_Id.split("-")[0] + "-" + cell_act_Id.split("-")[1] + "-" + cell_act_Id.split("-")[2] + "-" + cell_act_Id.split("-")[3] + "-" + cell_act_Id.split("-")[4] + "\">" +
                            "<div class=\"col-xs-6 cell-item-action cell-item-action-printbill\" " + (isPrintBill ? "" : "style='display:none !important;'") + "  onclick=\"openPrintBill($(this),0, 0, " + cell_act_Id.split("-")[0] + ", '" + cell_act_Id.split("-")[1] + "', '', '', " + cell_act_Id.split("-")[2] + ", '" + cell_act_Id.split("-")[3] + "','" + cell_act_Id.split("-")[4] + "', 1, 0)\">In phiếu</div>" +
                            "<div class=\"col-xs-6 cell-item-action cell-item-action-book\" onclick=\"openBooking($(this), 0, " + cell_act_Id.split("-")[0] + ", '" + cell_act_Id.split("-")[1] + "', '', '', " + cell_act_Id.split("-")[2] + ", '" + cell_act_Id.split("-")[3] + "','" + cell_act_Id.split("-")[3] + "')\">Book lịch</div>" +
                            '</div>' +
                            '</div>';
                        $('#' + data).parent().empty().append(strCell);

                        //customerPhone = $('#' + olderData).find('.customer-phone span').attr('data-customer-code').split("-")[1];
                        if (olderData != "") {
                            bookingId = olderData.split("-")[1];
                        }
                        if ($(THIS).attr('id') != "") {
                            stylist = $(THIS).attr('id').split("-")[0];
                            hourId = $(THIS).attr('id').split("-")[1];
                            subHourID = $(THIS).attr('id').split("-")[2];
                        }
                        updateBooking_drop(customerPhone, bookingId, hourId, subHourID, stylist, minutes, seconds);
                        //loadSetWaitingTime();
                    }
                    else {
                        console.log("Huỷ");
                        finishLoading();
                        return;
                    }
                }
                else {
                    alert("Khách đã in bill, không được chuyển khung h.");
                    return;
                }

            }
            else {
                var data = event.dataTransfer.getData('Text', $(THIS).attr('id'));
                var olderData = $('#' + data).attr("id");
                var idOlder = $('#' + data).parent().attr("id");
                var cell_act_Id = $('#' + data).find('.board-cell-action').attr('data-id');
                console.log("cell_act_Id", cell_act_Id);
                var minutes = $('#' + data).find('.board-cell-content').find(".bill-created-time").find(".min").attr('data-min');
                var seconds = $('#' + data).find('.board-cell-content').find(".bill-created-time").find(".sec").attr('data-sec');
                if (!$('#' + data).hasClass("is-checkout")) {
                    startLoading();
                    if (confirm('Bạn có muốn đổi booking cho khách?')) {
                        if ($('#' + data).parent().find('.board-cell-item').length > 1) {
                            $('#' + data).addClass('cls-del');
                            $('#' + idOlder).find('.cls-del').detach();
                            $('#' + idOlder).find(".board-cell-item").removeClass('col-6');
                        }
                        else {
                            var strCell = '<div class="board-cell-item is-enroll">' +
                                '<div class="board-cell-content"></div>' +
                                "<div class=\"board-cell-action\" data-id=\"" + cell_act_Id.split("-")[0] + "-" + cell_act_Id.split("-")[1] + "-" + cell_act_Id.split("-")[2] + "-" + cell_act_Id.split("-")[3] + "-" + cell_act_Id.split("-")[4] + "\">" +
                                "<div class=\"col-xs-6 cell-item-action cell-item-action-printbill\" " + (isPrintBill ? "" : "style='display:none !important;'") + "  onclick=\"openPrintBill($(this),0, 0, " + cell_act_Id.split("-")[0] + ", '" + cell_act_Id.split("-")[1] + "', '', '', " + cell_act_Id.split("-")[2] + ", '" + cell_act_Id.split("-")[3] + "','" + cell_act_Id.split("-")[4] + "', 1, 0)\">In phiếu</div>" +
                                "<div class=\"col-xs-6 cell-item-action cell-item-action-book\" onclick=\"openBooking($(this), 0, " + cell_act_Id.split("-")[0] + ", '" + cell_act_Id.split("-")[1] + "', '', '', " + cell_act_Id.split("-")[2] + ", '" + cell_act_Id.split("-")[3] + "','" + cell_act_Id.split("-")[4] + "')\">Book lịch</div>" +
                                '</div>' +
                                '</div>';
                            $('#' + data).parent().empty().append(strCell);
                        }

                        //customerPhone = $('#' + olderData).find('.customer-phone span').attr('data-customer-code').split("-")[1];
                        if (olderData != "") {
                            bookingId = olderData.split("-")[1];
                        }
                        if ($(THIS).attr('id') != "") {
                            stylist = $(THIS).attr('id').split("-")[0];
                            hourId = $(THIS).attr('id').split("-")[1];
                            subHourID = $(THIS).attr('id').split("-")[2];
                        }
                        updateBooking_drop(customerPhone, bookingId, hourId, subHourID, stylist, minutes, seconds);
                    }
                    else {
                        console.log("Huỷ");
                        finishLoading();
                        return;
                    }
                }
                else {
                    alert("Khách đã in bill, không được chuyển khung h.");
                    return;
                }

            }
        }
    }
}

/*
* convert gia tri mili giay sang PHUT - GIAY
*/
function convertHMS(value, isBook) {
    var sec_num = parseInt(value, 10); // đổi giá trị sang number (đơn vị giây)
    var minutes = parseInt(sec_num / 60);
    var seconds = sec_num % 60;
    if (isBook == true) {
        minutes = '-' + minutes;
    }
    return minutes + ':' + seconds;
}

/*
* set time khi load trang
*/
function intervalFn(minutes, seconds, i) {
    var min = parseInt(minutes);
    var test = setInterval(function () {
        seconds++;

        if (seconds > 59) {
            seconds = 0;
            min = min + 1;
            $('.call-phone').eq(i).parent().find(".board-cell-content").find('.bill-created-time').find('.min').html(((min < 10 && min >= 0) ? "0" : "") + min);
        }

        $('.call-phone').eq(i).parent().find(".board-cell-content").find('.bill-created-time').find(".sec").html(((seconds < 10 && seconds >= 0) ? "0" : "") + seconds);
        if ((min > 4) && (!$('.call-phone').eq(i).parent().hasClass("is-book-at-salon")) && (!$('.call-phone').eq(i).parent().hasClass("is-makebill"))) {
            $('.call-phone').eq(i).parent().find(".board-cell-content").find(".customer-name").find('.customer-text-wrap').find('.fa-warning').removeClass('hidden');
        }
        else if ((min > 9) && ($('.call-phone').eq(i).parent().hasClass("is-book-at-salon")) && (!$('.call-phone').eq(i).parent().hasClass("is-makebill"))) {
            $('.call-phone').eq(i).parent().find(".board-cell-content").find(".customer-name").find('.customer-text-wrap').find('.fa-warning').removeClass('hidden');
        }
        else if ((min > 4) && ($('.call-phone').eq(i).parent().hasClass("is-makebill")) && ($('.call-phone').eq(i).attr("data-procedure") == "")) {
            $('.call-phone').eq(i).parent().find(".board-cell-content").find(".customer-name").find('.customer-text-wrap').find('.fa-warning').removeClass('hidden');
        }
        else {
            $('.call-phone').eq(i).parent().find(".board-cell-content").find(".customer-name").find('.customer-text-wrap').find('.fa-warning').addClass('hidden');
        }
        $('.call-phone').eq(i).parent().find(".board-cell-content").find('.bill-created-time').find('.min').attr('data-min', ((min < 10 && min >= 0) ? "0" : "") + min);
        $('.call-phone').eq(i).parent().find(".board-cell-content").find('.bill-created-time').find(".sec").attr('data-sec', ((seconds < 10 && seconds >= 0) ? "0" : "") + seconds);
    }, 1000);
    $('.call-phone').eq(i).parent().find(".board-cell-content").find('.bill-created-time').attr('data-interval', test);


}

/* 
* Load Waiting time
*/

function loadSetWaitingTime() {
    // clear all setInterval
    for (var i = 1; i < 9999; i++) {
        window.clearInterval(i);
    }

    for (var i = 0; i < $('.call-phone').length; i++) {
        var isCall = $('.call-phone').eq(i).children().hasClass("is-call");
        var isMakeBill = $('.call-phone').eq(i).parent().hasClass("is-makebill");
        var isCheckOut = $('.call-phone').eq(i).parent().hasClass("is-checkout");
        var attrTime = $('.call-phone').eq(i).attr("data-time");
        var attrBillTime = $('.call-phone').eq(i).attr("data-createbill");
        var attrComplete = $('.call-phone').eq(i).attr("data-completetime");
        var currentTime = new Date().getTime() / 1000;
        var isBook = false;
        if (isCheckOut == false) {
            if ((isCall == true) || (isMakeBill == true) || (attrTime != "" && attrTime != undefined && attrBillTime == undefined && attrComplete == undefined)) {
                if (attrTime != '' && (attrBillTime == '' || attrBillTime == undefined)) {
                    isBook = false;
                    var timeSplit = '';
                    if (CheckParam($('.call-phone').eq(i).attr("data-time"))) {
                        timeSplit = $('.call-phone').eq(i).attr("data-time").split(/\-|\s/);
                    }
                    var time = new Date(timeSplit.slice(0, 3).reverse().join('/') + ' ' + timeSplit[3]).getTime() / 1000;
                    var ssMM = convertHMS(currentTime - time, isBook);
                    var seconds = ssMM.split(':')[1];
                    var minutes = ssMM.split(':')[0];
                    $('.call-phone').eq(i).parent().find(".board-cell-content").find('.bill-created-time').css('display', 'block');
                    $('.call-phone').eq(i).parent().find(".board-cell-content").find('.bill-created-time').find('.min').html(((minutes < 10 && minutes >= 0) ? "0" : "") + minutes);
                    $('.call-phone').eq(i).parent().find(".board-cell-content").find('.bill-created-time').find(".sec").html(((seconds < 10 && seconds >= 0) ? "0" : "") + seconds);
                    intervalFn(minutes, seconds, i);

                }
                else if (attrBillTime != '' && (attrComplete == '' || attrComplete == undefined)) {
                    isBook = false;
                    var billSplit = $('.call-phone').eq(i).attr("data-createbill").split(/\-|\s/);
                    var bill = new Date(billSplit.slice(0, 3).reverse().join('/') + ' ' + billSplit[3]).getTime() / 1000;
                    var ssMM = ssMM = convertHMS(currentTime - bill, isBook);

                    var seconds = ssMM.split(':')[1];
                    var minutes = ssMM.split(':')[0];
                    $('.call-phone').eq(i).parent().find(".board-cell-content").find('.bill-created-time').css('display', 'block');
                    $('.call-phone').eq(i).parent().find(".board-cell-content").find('.bill-created-time').find('.min').html(((minutes < 10 && minutes >= 0) ? "0" : "") + minutes);
                    $('.call-phone').eq(i).parent().find(".board-cell-content").find('.bill-created-time').find(".sec").html(((seconds < 10 && seconds >= 0) ? "0" : "") + seconds);
                    intervalFn(minutes, seconds, i);
                }
            }
            else if ((attrTime == "" || attrTime == undefined) && attrBillTime == undefined && attrComplete == undefined && (isMakeBill == true)) {
                isBook = false;
                var seconds = $('.call-phone').eq(i).parent().find(".board-cell-content").find('.bill-created-time').find(".sec").attr('data-sec');
                var minutes = $('.call-phone').eq(i).parent().find(".board-cell-content").find('.bill-created-time').find('.min').attr('data-min');
                $('.call-phone').eq(i).parent().find(".board-cell-content").find('.bill-created-time').css('display', 'block');
                $('.call-phone').eq(i).parent().find(".board-cell-content").find('.bill-created-time').find('.min').html(((minutes < 10 && minutes >= 0) ? "0" : "") + minutes);
                $('.call-phone').eq(i).parent().find(".board-cell-content").find('.bill-created-time').find(".sec").html(((seconds < 10 && seconds >= 0) ? "0" : "") + seconds);
                intervalFn(minutes, seconds, i);
            }
        }

    }
}

function openFrameBooking() {
    $(".frame-booking-pane").openEBPopup();

}

/*
* Lấy danh sách ước lượng thời gian cắt
*/
function getListEstimateTimecut() {
    try {
        $.ajax({
            type: "POST",
            url: "/ver2-beta-timeline/estimatetimecut/listing",
            data: '{salonID : ' + getSalonID() + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                if (response.d != null && response.d.success == true) {
                    var data = response.d.data;
                    if (data.length > 0) {
                        var trs = "";
                        $.each(data, function (i, v) {
                            trs += "<tr>" +
                                "<td>" + (i + 1) + "</td>" +
                                "<td>" + v.CustomerName + "</td>" +
                                "<td>" + v.CustomerPhone + "</td>" +
                                "<td>" + v.StylistName + "</td>" +
                                "<td>" + v.EstimateTimeCut + "</td>"
                            "</tr>";
                        });
                        $("#estimate-timecut-table tbody").empty().append($(trs));
                    }
                }
                else {
                    $('.div_checkCustomer').addClass('hidden');
                }
                finishLoading();
            },
            failure: function (response) { alert(response.d.message); }
        });
    }
    catch (ex) {
        console.log(ex);
    }
}
/*
* In bill
*/
function printBillTest() {
    var test = '{"status":null,"message":"Success!","success":true,"data":{"billI":1377863,"bookingID":1888145,"hourID":1498,"hourFrame":"15:30:00","stylistID":459,"stylistName":"ST14","customerPhone":"01686437373","customerName":"IT_TEST","salonID":24,"salonName":"","checkinID":0,"checkinName":"","services":[{"Id":53,"Code":"SP00036","Name":"Shine Combo 100k","Price":100000,"Quantity":1,"VoucherPercent":0}],"teamID":0,"PDFBillCode":"HD0907180014","HCItem":"","IsAutoStylist":true,"IsBookAtSalon":false,"TextNote1":"Khách hàng thân thiết - MIỄN PHÍ CẮT KHI ANH UỐN hoặc NHUỘM \\n \\n Ưu đãi dành riêng cho anh 250k/3 lần ShineCombo \\n \\n Ưu đãi riêng anh -30% Dưỡng Protein","customerId":0}}'
    var data = JSON.parse(test);
    console.log(billResponse);
    var billResponse = data.data;

}
//printbill
function printBill() {
    debugger;
    bill.customerName = $("#EBPopup .customer-name").val();
    if (CheckParam(PhoneItem)) {
        bill.customerPhone = changePhone(PhoneItem);
    }
    else {
        bill.customerPhone = changePhone($("#EBPopup .customer-phone").val());
    }
    bill.CustomerScore = $("#EBPopup .customer-name").attr("data-cusscore");
    if (hcArr.length > 0) {
        var hcStr = '';
        for (var i = 0; i < hcArr.length; i++) {
            hcStr += hcArr[i] + ',';
        }
        bill.HCItem = hcStr.replace(/\,+$/, '');
    }
    var errorList = [];
    if (bill.customerName === "") {
        errorList.push("Tên Khách hàng không được để trống");
    }

    if (bill.customerPhone === "") {
        errorList.push("Số điện thoại không được để trống");
    }

    if (bill.customerPhone.length != 10 || bill.customerPhone.charAt(0) != 0) {
        errorList.push("Số điện thoại không đúng. Số điện thoại phải bằng 10 kí tự và bắt đầu bằng số 0");
    }
    if (bill.services.length === 0) {
        errorList.push("Vui lòng chọn dịch vụ");
    }

    if (errorList.length > 0) {
        var errorStr = "";
        for (var index in errorList) {
            errorStr += "[" + errorList[index] + "] ";
        }
        alert(errorStr);
    }
    else {
        //if ($("#EBPopup input#check-birthday").prop('checked')) {
        //    bill.snDay = null;
        //    bill.snMonth = null;
        //    bill.snYear = null;
        //}
        //else {
        //if (!ValidBirthDay()) {
        //    return;
        //}
        //else {
        bill.snDay = $('#EBPopup input.sn-day').val() == "" ? null : $('#EBPopup input.sn-day').val();
        bill.snMonth = $("#EBPopup input.sn-month").val() == "" ? null : $("#EBPopup input.sn-month").val();
        bill.snYear = $("#EBPopup input.sn-year").val() == "" ? null : $("#EBPopup input.sn-year").val();
        //}
        //}
        bill.datedBook = $("#DatedBook").val().toString();
        startLoading();
        $.ajax({
            type: "POST",
            url: "/ver2-beta-timeline/printbill",
            data: '{billClient : \'' + JSON.stringify(bill) + '\'}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.d !== null && response.d.success === true) {
                    billResponse = response.d.data;
                    console.log(billResponse);
                    if (billResponse !== null) {
                        var strCell = "<div class=\"board-cell-content\" onclick=\"openPrintBill($(this), " + billResponse.billID + ", " + billResponse.bookingID + ", " + billResponse.stylistID + ", '" + billResponse.stylistName + "', '" + changePhone(billResponse.customerPhone) + "', '" + billResponse.customerName + "', " + billResponse.hourID + ", " + billResponse.subHourID + ", '" + billResponse.hourFrame + "', " + (billResponse.IsAutoStylist ? 1 : 0) + ", " + (billResponse.IsBookAtSalon ? 1 : 0) + "," + billResponse.snDay + "," + billResponse.snMonth + "," + billResponse.snYear + ")\">" +

                            "<p class=\"customer-name\">" + billResponse.customerName + "</p>" +
                            "<p class=\"customer-phone\">" + (bill.IsSM == false || bill.IsSM == null ? "" : "SM - ") + "" + ReplaceFirstPhone(changePhone(billResponse.customerPhone), 4, 'X') + "</p>" +
                            "<p class=\"customer-phone\ hidden\">" + changePhone(billResponse.customerPhone) + "</p>" +
                            //"<p class=\"customer-phone\">" + changePhone(billResponse.customerPhone) + "</p>" +
                            "</div>" +
                            "<div class=\"cancel-book\" title=\"Hủy lịch đặt\" onclick=\"openCancelBook($(this), " + billResponse.bookingID + ", '" + billResponse.customerName + "', '" + changePhone(billResponse.customerPhone) + "')\" style=\"display:none;\">" +
                            "<i class=\"fa fa-times\" aria-hidden=\"true\"></i>" +
                            "</div>" +
                            "<div class=\"call-phone\" data-time=\"" + billResponse.DatetimeCurrent + "\" title=\"Check gọi điện cho khách hàng\" onclick=\"checkCallPhone($(this), " + billResponse.bookingID + ")\">" + /*DatetimeCurrent*/
                            "<i class=\"fa fa-check " + (billResponse.isCall ? "is-call" : "") + "\"" + "aria-hidden=\"true\"></i>" +
                            "</div>" +
                            "<div class=\"board-cell-action\" data-id=\"" + billResponse.stylistID + "-" + billResponse.stylistName + "-" + billResponse.hourID + "-" + billResponse.subHourID + "-" + billResponse.hourFrame + "\">" +
                            "<div class=\"col-xs-6 cell-item-action cell-item-action-book\" onclick=\"openBooking($(this), " + billResponse.bookingID + ", " + billResponse.stylistID + ", '" + billResponse.stylistName + "', '', '', " + billResponse.hourID + ", " + billResponse.subHourID + ", '" + billResponse.hourFrame + "')\">Book lịch</div>" +
                            "</div>";

                        // Nếu bill được lập từ cell đã booking

                        var cell = $("#" + billResponse.stylistID + "-" + billResponse.bookingID + "-bk");
                        if (cell.length > 0) {
                            var isMultipleItem = cell.parent().find(".board-cell-item " + funScoreCus(billResponse.CustomerScore) + "").length > 1 ? true : false;
                            cell.empty().append($(strCell)).addClass("is-makebill");
                            if (isMultipleItem) {
                                cell.addClass("col-6");
                            }
                        }
                        else {
                            var cell = $("#" + billResponse.stylistID + "-" + billResponse.hourID + "-" + billResponse.subHourID);
                            var isMultipleItem = cell.find(".customer-name").length > 0 ? true : false;
                            strCell = "<div class=\"board-cell-item " + funScoreCus(billResponse.CustomerScore) + " event is-makebill is-enroll\" id=\"" + (billResponse.stylistID + "-" + billResponse.bookingID + "-bk") + "\" data-cusscore=\"" + billResponse.CustomerScore + "\" draggable=\"true\" ondragstart=\"timeline_drag($(this), event);\" >" + strCell + "</div>";
                            if (isMultipleItem) {
                                cell.append($(strCell));
                            }
                            else {
                                cell.empty().append($(strCell));
                            }
                        }
                        autoCloseEBPopup();
                        // Reset giá trị bill
                        bill.billID = 0;
                        bill.bookingID = 0;
                        bill.hourID = 0;
                        bill.subHourID = 0;
                        bill.hourFrame = '';
                        bill.stylistID = 0;
                        bill.stylistName = '';
                        bill.customerPhone = '';
                        bill.customerName = '';
                        bill.salonName = '';
                        bill.checkinID = 0;
                        bill.checkinName = '';
                        bill.services = [];
                        bill.teamID = 0;
                        bill.PDFBillCode = '';
                        bill.HCItem = '';
                        bill.IsAutoStylist = true;
                        bill.CustomerScore = 0;
                        PhoneItem = '';
                        bill.snDay = 0;
                        bill.snMonth = 0;
                        bill.snYear = 0;
                        // Reset 1 số biến giá trị
                        hcArr = [];
                        $('.div_checkCustomer').addClass('hidden');
                    }
                    else {
                        alert("Đã có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển.");
                    }
                }
                else {
                    alert(response.d.messageNoti);
                }
                finishLoading();

            },
            complete: function (response) {
                billResponse = response.responseJSON.d.data;
                // create array service
                var arrServices = [];
                for (var i = 0; i < billResponse.services.length; i++) {
                    arrServices.push({
                        'Dịch vụ': billResponse.services[i].Name,
                        'SL': ' '
                    })
                };
                arrServices.push({
                    'Dịch vụ': ' ',
                    'SL': ' '
                });
                arrServices.push({
                    'Dịch vụ': ' ',
                    'SL': ' '
                });
                arrServices.push({
                    'Dịch vụ': ' ',
                    'SL': ' '
                });
                //print bill with javascript
                let imageStar = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAMDAwMDAwQEBAQFBQUFBQcHBgYHBwsICQgJCAsRCwwLCwwLEQ8SDw4PEg8bFRMTFRsfGhkaHyYiIiYwLTA+PlQBAwMDAwMDBAQEBAUFBQUFBwcGBgcHCwgJCAkICxELDAsLDAsRDxIPDg8SDxsVExMVGx8aGRofJiIiJjAtMD4+VP/CABEIACsALAMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAJAAgEBwoFBv/aAAgBAQAAAABU4C6jWilTazlXYxJLJjNVWTPWxBebD46LInXl8v28dQpSrOEVCwaoqIi3/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/aAAgBAhAAAAAAf//EABQBAQAAAAAAAAAAAAAAAAAAAAD/2gAIAQMQAAAAAH//xAAyEAABBAECAwYEBQUAAAAAAAACAQMEBQYHEQAIEgkQEyExURRBQlIVIGFzkTJDRIKi/9oACAEBAAE/AO7m71snam6vT2q2xfWkx15YVajbqoBuMrs7JH9TP0L7eOUXWNzV3SGA5YyVfu6PausiJdzcVtN2ny/dD/rfvy/nY0Bw6yl1x3MuzlQ3TaeGBEJ4esPtcLoAuNTe0NorfFrqpw3HLiLPmw3Y8axmOtNLHV1NvFEG1c3Pu5atfpugOZTLRYLllV2UL4edBB3wlNQXqacFfvDih7RHRWw6W7KsyKqL3KM3ID+W3N+MM190azylS1qMxrUY8UmiGU6kN0TFEXZW3+guOZzlnvNCMgWZCR2bilg+qV87bdWCX/Gkexp8i+v8lZWWFzYxa6uiuy5kt0GY8dkFcccccXYREU81JeNLuz4w0cOhu6hS55X7+7r7EB9AajCXozvsXWY/MuMoxiizOhsaO8rmZ9dPZJqRGdHcSFfP5ehJ6iSeaLxqHT0+PZ7k9PTOPuV1dczIsRx8kVw2mHSAVJRRN1Xbv7OzAcHlUF7mxxfiMkiWZwGnHtlGKwrIH1sexn1bKXdeWjFJSWdo9t4UCE/Jc3+1kFNeJkt+fMflvF1Ovum64XuTi7qv8rxpxy0az6qVUi3x7GnigNMEbUmSYxQlKn0MK5t1kXGQ41kGJW0ipvayXWz2C2djSWiaNP12JOOzWyFG5+fUB/3mIE1of2jNtzusIUSyhSYUxkH40pp1h9o03FxtwSEhJPZU9eOVDRzTO+1bztq1xyLPaoJRfhrMkjeba2f6PMDLY/8AfhpttoBBsBAAFBERTZERE8kRONY9NsD1Ew20byehh2Sw4Eh6M64Ki8yYBuituhsY8dnxi1BBoczu2IQDYlPbhfEdREXgf19Hd//EABQRAQAAAAAAAAAAAAAAAAAAADD/2gAIAQIBAT8AH//EABQRAQAAAAAAAAAAAAAAAAAAADD/2gAIAQMBAT8AH//Z';
                let imageSystem = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAABGCAMAAABG8BK2AAABAlBMVEVHcExEREBEREBFRUFFRUFFRUFFRUFEREBEREBEREBEREBGRkEAAAAlJSMwMC0BAQEBAQEBAQEBAQEqKicBAQEDAwMaGhgPDw4CAgIBAQECAgELCwoBAQEFBQQMDAsPDw4UFBMODg0AAAAHBwcZGRgWFhUGBgYBAQEAAAAZGRcbGxkDAwMxMS4vLy0BAQELCwoEBAQgIB4YGBYBAQEyMi8mJiQmJiQdHRsLCwo5OTYFBQQMDAwEBAMKCgofHx0gIB4ODg0SEhEbGxkRERAjIyEYGBcZGRcqKigWFhQlJSMDAwMREQ8vLysuLiwAAAADAwMBAQECAgIDAwICAgECAgMGBgX0Mw3/AAAATnRSTlMAAwQHAQkFCwoCCAbPFxPn4vn1K+uYNU3wpstWtNjDZwmEuos9JG2u/Rxjwz0jkstzW4HdSk8Mb+IQeq2g1QF3qbqOtX6YpFdFMGGYZW5hflvKAAAHcUlEQVRYw42Y13bcuBJFN0gQBMlu5RysK1mOkpxkyzmGsSfcA0Ca+f9fuQ9ks9kKntuPXFi7qwqnUAEKh8+srypvM48raGrqhiLDm8pXxtusIDMALuuPGo8ryGvqnMJBkeOdLbOqrExpnScvqGuKhtLZ0lRllZUVg1+TTT7TePKCoqHGUThKY8uqKsk8jSevKRp8+9maEl7fPng8Gt3aWf24fgbYqrSmpPvPHN+Ad5TZlFLMUipTYnikICVFSfP3Xr4GWkrdUhz4hjKjpfRm5kOK87yTopSkEJW08v0B2KktjgKDb22xU4+GFOs8bnxvd+vo1mhhsWVp8fsZme0ppWPikc087npKU4AF8OPDl0eSYtD8U5iY7TOmtpQ3Ujy1bTkAJ0/mpRT0Duoa7/DGeio7jMuQYquS1pa6pnDUGWVVGaj3NqSkrTkmRw2tLf31FTm+wWeUlZlcXV33puAq42FuU0lamCNv7wd8VZXWeJynsb2zQwHUNVn9x6NPq/t7h2OApoCHUtD8WXfUQ08BgOsoOY8kJSlu7PzIsOQVa0sKGmVUZVWVFWbiESe3jz5vgx3KqKCu8YZdKajV4MJDKMg5WYpK93CmKm2Fn1COl5Skt2SO/uqKLujm5+nGaauaeK6DE7Ia1hSi9qC0xk9kzdyizhWinkEvoz4xMuqzD+X4xbcDSSkuboPPeCqF+2OMKcG1d8QTJYULhY2aiaQnlNLZSeT4fVVJSXs0NmdVUV+hxFDgPE3Nn1HhQkFLOQBQXRUjLoPDkc6lp+QNrChoDes8uJK8gGcKFwpRG2f/eb79x481A9QUrTCMMQDk5A3sKEQtYz3rivEzeLqcrhmHpBAVNL8iSXp1b89gO1uqnO2Pe+tz0FDDVyksUGUVI4X7d8BR0BTUOazNh6gQY5CCYpC0uzdReyscrWweQg1sxaRnULIuxS+4gu7RaGCro8Sgi6B0rqR7r7GlsSX8oxgUox5B0TCWgs5sZet5pRGG9gGzsD66aJ3qKSEpnZ6QVWWV8UaSopIOwBa8UYzPqGr2o+IyDXWbeftKlyjnSSFpZa6VNGuHhy9HSoqfcA63qLRABS+U9ISalrJ5iZJ03spIuwZT4hoA7iokvYCC21FapmGsoN+Aoibj3YCShpQY0z0a26UXPJX+PqXKOJb0Bg+PlY6wNOTcvZbSfdNLqkmSOu4p6hjL2VLQV3CsxnjqgMLOxYsZShhSYtCDtgzUFDm/S/EJJbyP6TEU7EdtlOAb/lR/09dR4mOYUPgwn/SWqmJHadcAHxXuP4CM10szlHOlWUrQQyapDlsxPoaGzaiFjJq7MWoZ4OHNlM7RjZyiq6T2leIOjeWr0inUfIzSA6hZjTOU8yuUoNt0FO7cP9c+wFaMWwDPFDbOoOLWDXEZBF1juiL7SFHPyWDlXJ8h51HUaQPY930MbqIEfWqTtOKLtFRVnmVJtykyDmJ8DPDh9N8p51qmocysM3/urFHANyWt0TC3qLRJDtV8nKFcTChx6OgtuvIIeDhbiRcbBvipoGdU0JzqXyghKeiQrnpZT8NYSb+B50mK8RgLjK71KPZXF5JC0i4MGjJu69UYb9iNcQE8tDfVptIsJQ4oQXcx00qasVziHc9jjKvUFsMn/R+UGP+ZY9DWZeAdvGtLg2mfjpsooaco6IAr7Qvb0i1wHnh5LeWip0zSX3fJ7WwTZFjfe03joWFNN9qShlcnPZ2melcDARpPRgErN8VllpKC3kxq4LRpnjSHGQfxsnavo8SgGI9+AjOtd0fJa27HayhBf1+GXwRJoydj8q6t6wXgqLEP++NxQBk4OqyB0tI6Vxsyah7EX1I0rF4pKeh33GVKUcPu5Qu6hpIGWfeeS21dTk7DZryZEq9SgsbMNmQ5FA37v4jLMOidGKU5ZhqyHIp8+Kj/ktK/I18ws21djcNzJ85Szq+V9MQjvb1sSzeWuXn9ijJ4vCS9/QET4dVtW+fadGcrDUz/FeXW3TtMKf0cWGAomakxNzwYknb3jwFMibk0NTkoDRzol5SkqJUvP2GqFzcz2YI3FkZpljKj6iRpc3sOJo/6FUo7ljG3GG+iJEmj/54Aw8nWXZpsDVVp+O9NtlxIr/bvAK7P6clk2w+axrZjGRxdKiYtJUkb746B55vL7XxsppPtZOj1phvLDD91VS+S7n9+AdxZfSUtt7bYfm8wM6tXpcVkH05T132mgSGjN2dw9tdIUlo8oxuyLq0UunDZCs8f0kVQ+ntCkXT6/QRY35GUFOIOtB7NUNxwEQC/DTrhqKSNt+sZHH7f6CvGX1RDSn2ZUoJhZ5p50v2Dh3PA9qLUU95ftmUyZfe2ZNSsJqWgEBW19e0BQMmypgJYGnMdpcwobb8IwB5LSefSwupPwFVVaR3jI3XZ+H4ZP6U0k+j2MmpH1Tzn6YK0sflXBrSLicwDd4/uS9r6xrQklR2lmFJMiStwFLYh+7E9BopuM9W+1PDgxfodYLCw6ubyBu86MU72WLgCAEPRLj+qytusoKkBgCKz3a6MrN+fDdZqBa7gf86ovl5qT37gAAAAAElFTkSuQmCC';
                let imageCustomer = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABzCAMAAABZ71dFAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAHsUExURUdwTAQEBAoKCgMDAwEBASgoKFVVVSwsLB8fHwAAAF9fXy0tLWZmZhMTEyUlJTExMSoqKisrK4SEhAQEBBISEjAwMFFRUS0tLQcHBwcHB9fX1xMTEwgICDk5OWVlZSgoKB8fHwUFBQ0NDXJycg4ODhAQEElJSQYGBiIiIigoKCgoKBAQEDk5ORgYGD4+Ph0dHUhISDU1NUdHRwMDAxcXFyQkJGpqaiEhIR8fH0RERDY2NhEREQoKCgEBAQoKCiwsLG1tbQoKChISEjs7OxQUFBcXFy8vL1FRUSsrK2BgYAcHB0RERAYGBlNTU0dHRzY2NgICAi0tLRgYGGFhYVZWVhgYGCwsLBUVFVVVVZaWlikpKSMjIxAQEC0tLTc3Nx0dHRYWFgsLCxMTE0lJSTIyMkBAQCEhIQ4ODklJSRkZGRcXFz4+Pjk5OTo6OhUVFRsbG8HBwRoaGklJSZCQkE9PTz09PT09PScnJ0VFRQUFBU1NTQ0NDRgYGFJSUnZ2diMjIwAAACEhIQcHBwICAh0dHX9/fx8fH2RkZCMjI0VFRVBQUH19fVFRUUhISJmZmVtbW2JiYmlpaW9vbwoKCgoKCi0tLSEhIcjIyDs7Ozw8PEVFRUBAQAAAAAEBAQYGBgQEBAICAhISEggICAwMDIg+tMwAAACcdFJOUwDm5+vmFAMHCaYVmgfq7Fu+jxSmpapTOfuwCB34GQ1FwdE2JtSQnZ1Jsg9adsx3tWAfJfHtlD3R7lA10kX93NQsfCpc/MQRbHpYxz/vHoWDoHeveKhpbM6zVib+hoUt8nS98o3HNaQ/quCclp294vUG/eo66m7qxWf2ffNSyzWLlFJN9XlKppihy150SrgcistypL/h6t45/qSJViHSwlgAAAQ0SURBVBgZ7cBlX1tnAAXwA4wkuAQdTodb0Qqlpe7u7u5uc1/nbuckT8IXHaWsJV3kPjf3vtmvf7zxRgbNGy7urg6HwwUFBXuK+lpC8F5gzbWhX+oiRsZIJtJ4o+rAEXirYXWJ4Txx3lT92iA8MzwUFZMwLNkIjxyLk6LE14mMDuTDA+dLmVZ8MbLWPkMapiZGNiFL7RGSYhoiNyEry6foxF5kYTpGB0ROwL1yik6Y0kq4VUaKzpyFS81L6VhsGu6U0TGxE+6co+iQ4e1FcOP9GEXnyuDGCtFGOdzYSSvbeuHCQ9rZDBcmaacf9hbFaOc67K2J0ob4M+ytMbQh3gjA2n5aim2HtS9pRYw2wForLUUbYOu7o7QUbYCt7TFaESMNsHUiRitifBiWmlfSWhEsXaM9UwM7k3ThA1gJ5dGFKlgJ5YnWFIaVUJ5oTWFYCeXRnsKwEhyjC9Wws4oufA87nbSnWtippb2bhbBTXErRhsRDsLWDop3IBGxVltKCRF6CvelGSnRI5Hq4UbiKokPie3BnGZ0zn8GdwG90rBxu3adj3XCrsoSGmUlcAvcG6VAL3Ku8TDEjsRXZ6KYjR5CVcorpiSxCdu7GaZiW4VglsrSWGX2DrI2TYioyZAWyV3yQYioSy+GFYBtTEMmDxfBEsI0SkzBiWxAeCbaJyU0G4ZnQJCkmksglQXgo1MMkvm2GtwZjfE58KXcjPBc81ijOEueMdhbCDxt2H5wRRXJmoH8DfNP0bMepU4NfTyNrAZyouXjgp6vjHaeXLoIDW+u7rhR1ftG3oqYJjoTuX1p/5nHc8IWvbiGj39dRnCUTHy0Z2teCDGpbZ5johwAyuPsHKS6wrqMJqTVtiVDiKxI10Iu0Wm6T4gIiYx1IZVOMFBOIZHQ8HyktbyXFRBJ5tBZJrY4yhU8+L0My+f2no0wh3o4kVhsmZ0RyW/3VR+eDweK3n7tVjPytg0Vb6kQZMRlpqh3/8SkzMXV5Jb8+uHPv3p0HpbnbIkxPnFqO1/xIz5mVQSQoHKWYkSTqOUrMSNyDBNfpi71Y4Bn9caYQrwzQJ8fx0lr6Q3zahH8toW92Yt5m+udmIV6opo8WY85IjD56iDll9NPjJsxqLqfooyeVAFpGKfpGbP0bQAX99XQcQFj0lQkDKKDPCgAUSPSRWACgin4SI1UAdtBP4thfACZKKfrozwnMekQ/NW7HnAuk6JcyzKsSJXpNIiN9eOlshL44WYMFhs9FSYoeMeKsWAVeM9Kzjt4Rp04eb0YS/asaRU9Ecju6kUp+d/U7dcxONG99RW0I6Y08qxhaOWNEa8bMbOnZWdMLZ0LDy54cqqrPrYtHjZhIYiJjIvGZd3b17Nu4tTcIeyOHl727Yn/1la4P65eW5Obk5Lw1LycnJ3fs8q6urgtFfR9/tPlwfgBAAO4E8Mb/0z9r5GHf069dZgAAAABJRU5ErkJggg==';
                let CusPhone = billResponse.customerPhone.toString().length;
                let firstNoPhone = '';
                let endNoPhone = '';
                let replacePhone = '';
                let shineMember = '';
                if (billResponse.isShineMember == 1) {
                    shineMember = 'CÓ';
                }
                else shineMember = 'KHÔNG';
                if (CusPhone == 10) {
                    firstNoPhone = billResponse.customerPhone.toString().substring(0, 6);
                    replacePhone = billResponse.customerPhone.toString().replace(firstNoPhone, 'xxxxxxx');
                }
                else if (CusPhone == 11) {
                    firstNoPhone = billResponse.customerPhone.toString().substring(0, 7);
                    replacePhone = billResponse.customerPhone.toString().replace(firstNoPhone, 'xxxxxxxx');
                }
                billResponse.customerPhone = replacePhone;
                var currentdate = new Date();
                var datetime = currentdate.getDate() + "/"
                    + (currentdate.getMonth() + 1) + "/"
                    + currentdate.getFullYear() + " "
                    + currentdate.getHours() + ":"
                    + currentdate.getMinutes();
                // Luu ý margin [0,0,0,0] trái, trên, phải, dưới
                var doc = {
                    content: [

                        {
                            text: parseInt(billResponse.PDFBillCode.substring(8)), fontSize: 37
                        },
                        { text: 'Ngày ' + datetime, fontSize: 10, italics: true, margin: [3, 3, 3, 3] },
                        {
                            columns: [
                                {
                                    height: 15,
                                    width: 20,
                                    text: 'KH: ',
                                    margin: [0, 2, 0, 3]
                                },
                                {
                                    height: 30,
                                    width: 160,
                                    text: billResponse.customerName.toUpperCase(),
                                    margin: [0, 0, 0, 3],
                                    bold: true,
                                    fontSize: 14
                                },
                            ],
                            columnGap: 3
                        },
                        {
                            columns: [
                                {
                                    width: 50,
                                    text: 'Số ĐT: ',
                                    margin: [0, 3, 0, 3],
                                },
                                {
                                    width: 100,
                                    text: changePhone(billResponse.customerPhone),
                                    margin: [0, 3, 0, 3],
                                    bold: true,
                                    fontSize: 12
                                },
                            ],
                            columnGap: 10
                        },
                        { text: '', fontSize: 3, margin: [0, 3, 0, 0] },
                        GetShineMember(billResponse.isShineMember, billResponse.memberEndTime),
                        {
                            columns: [
                                {
                                    width: 51,
                                    text: 'Giờ book:',
                                    margin: [0, 3, 0, 0]
                                },
                                {
                                    width: 110,
                                    text: billResponse.hourFrame,
                                    margin: [0, 3, 0, 0]
                                },
                            ],
                            columnGap: 10
                        },
                        {
                            columns: [
                                {
                                    width: 51,
                                    text: 'Giờ đến:',
                                    margin: [0, 3, 0, 0]
                                },
                                {
                                    width: 130,
                                    text: billResponse.timeCustomerCome,
                                    margin: [0, 3, 0, 0]
                                },
                            ],
                        },
                        { text: '', fontSize: 3, margin: [0, 5, 0, 0] },
                        GetNeedConsult(billResponse.NeedConsult),
                        { text: '', fontSize: 3, margin: [0, 5, 0, 0] },
                        getTextNoteTable(response.responseJSON.d.campaignInfo, imageSystem, 'CAMPAIGN CỦA KHÁCH HÀNG'),
                        { text: '', fontSize: 3, margin: [0, 5, 0, 0] },
                        getTextNoteTable(billResponse.noteSpecialCustomer, imageSystem, 'KHÁCH HÀNG ĐẶC BIỆT'),
                        { text: '', fontSize: 3, margin: [0, 5, 0, 0] },
                        getTextNoteTable(billResponse.TextNote1, imageSystem, 'NOTE CỦA HỆ THỐNG'),
                        { text: '', fontSize: 3, margin: [0, 5, 0, 0] },
                        getTextNoteTable(billResponse.TextNote2, imageCustomer, 'NOTE CỦA KHÁCH'),
                        {
                            columns: [
                                {
                                    width: 50,
                                    text: 'Skinner: ',
                                    margin: [0, 3, 0, 3]
                                },
                                {
                                    width: '100',
                                    text: ' ',
                                    margin: [0, 3, 0, 3]
                                },
                            ],
                            columnGap: 10
                        },
                        {
                            columns: [
                                {
                                    width: 50,
                                    text: 'Checkin: ',
                                    margin: [0, 3, 0, 3]
                                },
                                {
                                    width: '100',
                                    text: billResponse.checkinName,
                                    margin: [0, 3, 0, 3]
                                },
                            ],
                            // optional space between columns
                            columnGap: 10
                        },
                        {
                            columns: [
                                {
                                    width: 40,
                                    text: 'Stylist: ',
                                    margin: [0, 3, 0, 3]
                                },
                                {
                                    width: 120,
                                    text: billResponse.IsAutoStylist == false ? billResponse.stylistName + ' (B)' : billResponse.stylistName,
                                    margin: [0, 3, 0, 3]
                                },
                            ],
                            columnGap: 10
                        },
                        table(arrServices, ['Dịch vụ', 'SL']),
                        {
                            columns: [
                                {
                                    text: 'Đánh giá: ', width: 50, margin: [0, 10, 0, 3]
                                },
                                {
                                    image: imageStar,
                                    width: 14,
                                    height: 14,
                                    margin: [0, 11, 0, 0]
                                },
                                {
                                    image: imageStar,
                                    width: 14,
                                    height: 14,
                                    margin: [0, 11, 0, 1]
                                },
                                {
                                    image: imageStar,
                                    width: 14,
                                    height: 14,
                                    margin: [0, 11, 0, 1]
                                },
                                {
                                    image: imageStar,
                                    width: 14,
                                    height: 14,
                                    margin: [0, 11, 0, 1]
                                },
                                {
                                    image: imageStar,
                                    width: 14,
                                    height: 14,
                                    margin: [0, 11, 0, 1]
                                }
                            ],
                            columnGap: 10
                        },
                        {
                            columns: [
                                {
                                    text: '', width: 50, margin: [0, 0, 0, 3]
                                },
                                {
                                    text: '1',
                                    width: 14,
                                    height: 14,
                                    margin: [0, 0, 0, 2],
                                    alignment: 'center'
                                },
                                {
                                    text: '2',
                                    width: 14,
                                    height: 14,
                                    margin: [0, 0, 0, 3],
                                    alignment: 'center'
                                },
                                {
                                    text: '3',
                                    width: 14,
                                    height: 14,
                                    margin: [0, 0, 0, 3],
                                    alignment: 'center'
                                },
                                {
                                    text: '4',
                                    width: 14,
                                    height: 14,
                                    margin: [0, 0, 0, 3],
                                    alignment: 'center'
                                },
                                {
                                    text: '5',
                                    width: 14,
                                    height: 14,
                                    margin: [0, 0, 0, 3],
                                    alignment: 'center'
                                }
                            ],
                            columnGap: 10
                        },
                        {
                            text: ' \n\n\n\n .'
                        }
                    ],
                    pageMargins: [20, 20, 20, 20],
                    pageSize: 'letter',
                    pageOrientation: 'portrait'
                };
                pdfMake.createPdf(doc).print();

            },
            failure: function (response) { alert(response.d); }
        });
    }
}

// Hiện thông tin shine member
function GetShineMember(isShineMember, memberDate) {
    if (typeof isShineMember == 'undefined' || isShineMember == 0) {
        return {};
    }
    else {
        return {
            table: {
                headerRows: 1,
                widths: [168],
                body: [[{ text: 'Member: ' + memberDate }]]
            }
        }
    }
}

// Hiện thông tin khách cần tư vấn kỹ
function GetNeedConsult(needConsult) {
    if (typeof needConsult == 'undefined' || needConsult == '') {
        return {};
    }
    else {
        return {
            table: {
                headerRows: 1,
                widths: [168],
                body: [[{ text: needConsult }]]
            }
        }
    }
}
// Hiện thông tin chiến dịch. và thông tin khách khi book lịch textnote1; của hệ thống
function getTextNoteTable(textNote, image, caption) {
    if (textNote != null && typeof textNote != 'undefined' && textNote.trim() != "") {
        return {
            table: {
                headerRows: 1,
                widths: [10, 150],
                body: [
                    [{ image: image, width: 15, height: 15 }, { text: caption, margin: [3, 5, 0, 0], fontSize: 9, decoration: 'underline', }],
                    [{ text: textNote, colSpan: 2 }, {}]
                ]
            },
            layout: {
                hLineWidth: function (i, node) {
                    return (i === 0 || i === node.table.body.length) ? 1 : 0;
                },
                vLineWidth: function (i, node) {
                    return (i === 0 || i === node.table.widths.length) ? 1 : 0;
                }
            }
        }
    }
    else {
        return {};
    }
}

// bind class ScoreCustomer
function funScoreCus(CustomerScore) {
    var resultClass = CustomerScore > objCusCompareScore.levelScore2 ? objCusClassLevel.classNone : objCusClassLevel.classLevel2;
    return resultClass;
}

function ReplaceFirstPhone(phone, repeat, format) {
    if (permViewPhone === 0) {
        if (CheckParam(phone)) {
            return format.repeat(repeat).toString() +
                phone.slice(repeat, phone.length).toString();
        }
    }
    return phone;
}


function CheckParam(param) {
    if (param === undefined || param === null || param === '' || param === '0') {
        return false;
    }
    return true;
}
function ToJavaScriptDate(value) {
    var pattern = /Date\(([^)]+)\)/;
    var results = pattern.exec(value);
    var dt = new Date(parseFloat(results[1]));
    return dt;
    //return (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();
}
/*
* Lear date()
var date = new Date();
var strDate = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
strDate = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
var ttNow = Math.floor(date.getTime() / 1000);
var ttStart = Math.floor(new Date(strDate + " 08:00:00").getTime() / 1000);
var ttFinish = Math.floor(new Date(strDate + " 22:00:00").getTime() / 1000);
var ttTotal = ttFinish - ttStart;
var ttCurrent = ttFinish - ttNow;
var cellsWidth = 140 * 27;
console.log(ttFinish + " - " + ttNow);
var boardStyleLeft = ttCurrent * cellsWidth / ttTotal;
console.log(boardStyleLeft);
*/