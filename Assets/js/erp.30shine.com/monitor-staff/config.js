apis = {
    monitorStaffService: {
        //dev 
        //domainS3: "http://gitlab.30shine.com:8888",
        //domainS3: "https://api-upload-s3-30shine.herokuapp.com",

        //live
        //domain: "https://std-api-monitor-staff.30shine.com",
        domain: domainAPIMonitorStaff,
        //domain: "http://localhost:5000",
        //domainS3: "https://api-upload-s3.30shine.com",
        domainS3: domainAPIUploadS3,
        multiUpload: "/api/s3/multiUpload",
        staffMonitor: "/api/monitor-staff/staff/monitor",
        caregoryErrorHanle: "/api/monitor-staff/error-hanle",
        caregoryErrorRoot: "/api/monitor-staff/category-error/root",
        categoryError: "/api/monitor-staff/category-error/",
        staffError: "/api/monitor-staff/staff-error/",
        staffErrorV1: "/api/monitor-staff/staff-error-v1/",
        statisticBySalon: "/api/monitor-staff/staff-error/statistic-by-salon",
        statisticByStaff: "/api/monitor-staff/staff-error/statistic-by-staff",
        staffDepartment: "/api/monitor-staff/staff-department",
        staffDeaprtmentV1: "/api/monitor-staff/staff-department-V1",
        staff: "/api/monitor-staff/staff",
        salons: "/api/monitor-staff/salons",
        salonsArea: "/api/monitor-staff/salon-area",
        statisticAllSalon: "/api/monitor-staff/statistic-all-salon",
        statisticAllSalon_v1: "/api/monitor-staff/statistic-all-salon-v1",
        handle: "/api/monitor-staff/handle"
    }
};

consts = {
    login: "/dang-nhap.html",
    //ASM, SM, DSM
    departmentASMId: 17,
    departmentSMId: 9,
    departmentDSMId: 16,
    //GS?
    departmentAdmin: 7,
    departmentGSMId: 19,
    departmentGSId: 18,
    rootSocketServer: "https://api-upload-s3.30shine.com:8889",
    rootMonitorStaffUrl: domainAPIMonitorStaff
    //rootMonitorStaffUrl: "http:/localhost:5000"
};
