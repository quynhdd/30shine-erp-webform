﻿//close all dropdown list when click out site
$(document).mouseup(function (e) {
    let container = $(elms.resultSearch);

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        container.hide();
    }
});



function getDate(date, format) {
    let result = '';
    let day = "";
    let month = "";

    if (!format) {
        format = 'dd-MM-yyyy hh:mm:ss';
    }

    if (date.getMonth() + 1 < 10) {
        month = "0" + (date.getMonth() + 1);
    } else {
        month = (date.getMonth() + 1);
    }

    if (date.getDate() + 1 < 10) {
        day = "0" + date.getDate();
    } else {
        day = date.getDate();
    }

    switch (format) {
        case 'dd-MM-yyyy hh:mm:ss':
            result = day + "-" + month + "-" + date.getFullYear();
            break;
        case 'MM-dd-yyy hh:mm:ss':
            result = month + "-" + day + "-" + date.getFullYear();
            break;
    }
    return result;
}

function getUserById(id) {
    if (!id) {
        alert("mã nhân viên không đúng");
        return;
    }

    let result;
    let url = window.apis.monitorStaffService.domain + window.apis.monitorStaffService.staff + "/" + id;
    console.log(url);
    //ajax
    $.ajax({
        contentType: "application/json",
        type: "GET",
        url: url,
        dataType: "json",
        crossDomain: true,
        // headers: headers,
        //data: JSON.stringify(data),
        async: false,
        success: function (response, textStatus, xhr) {
            if (response) {
                result = response;
            } else {
                alert("Chưa xác định người dùng. Bạn hãy đăng nhập lại");
                $(body).remove();
            }
        },
        error: function (jqXhr, textStatus, errorMessager) {
            console.log(errorMessager + "\n" + jqXhr.responseJSON);
            alert("Có lỗi xảy ra. Bạn hãy thông báo với Admin");
        }
    });

    return result;
}

function waitDialogShow() {
    $("#waitDialog").show();
};

function waitDialogHide() {
    $("#waitDialog").hide();
};


function callAjax(jsonSend, urlReciver, methodSend, isAsync, completeCallback, errorCallBack, successCallBack) {
    // var data = jsonSend;
    $.ajax({
        url: urlReciver,
        method: methodSend,
        contentType: "application/json",
        data: jsonSend,
        async: isAsync || true,
        crossDomain: true,
        success: function (result, status, xhr) {
            if (successCallBack)
                successCallBack(result, status, xhr)
        },
        error: function (xhr, status, error) {
            if (errorCallBack)
                errorCallBack(xhr, status, error)
        },
        complete: function (xhr, status) {
            if (completeCallback)
                completeCallback(xhr, status)
        }
    });
}

function exportExcel(fileName, sheetTitle, headers, body) {
    var excel = $JExcel.new("Calibri light 10 #333333");			// Default font
    fileName = (fileName + "").trim();
    // excel.set is the main function to generate content:
    // 		We can use parameter notation excel.set(sheetValue,columnValue,rowValue,cellValue,styleValue)
    // 		Or object notation excel.set({sheet:sheetValue,column:columnValue,row:rowValue,value:cellValue,style:styleValue })
    // 		null or 0 are used as default values for undefined entries

    excel.set({ sheet: 0, value: sheetTitle });
    excel.addSheet("Sheet 2");

    var evenRow = excel.addStyle({ 																	// Style for even ROWS
        border: "thin,thin,thin,thin #333333"
    });													// Borders are LEFT,RIGHT,TOP,BOTTOM. Check $JExcel.borderStyles for a list of valid border styles

    var oddRow = excel.addStyle({ 																	// Style for odd ROWS
        fill: "#ECECEC", 																			// Background color, plain #RRGGBB, there is a helper $JExcel.rgbToHex(r,g,b)
        border: "thin,thin,thin,thin #333333"
    });

    for (var i = 1; i < body.length; i++) excel.set({ row: i, style: i % 2 == 0 ? evenRow : oddRow });

    var formatHeader = excel.addStyle({ 															// Format for headers
        border: "thin,thin,thin,thin #333333", 													// 		Border for header
        font: "Calibri 12 #0000AA B"
    });
    for (var i = 0; i < headers.length; i++) {																// Loop all the haders
        excel.set(0, i, 0, headers[i], formatHeader);
        excel.set(0, i, undefined, 15);
    }

    for (var i = 0; i < body.length; i++) {
        setDataRow(body[i], i + 1, excel);
    }


    if (fileName.substr(-5, 5) !== ".xlsx")
        fileName += ".xlsx"
    excel.generate(fileName);
}

function setDataRow(data, rowIndex, excel) {
    for (var i = 0; i < data.length; i++) {																// Loop all the haders
        excel.set(0, i, rowIndex, data[i]);
    }
}