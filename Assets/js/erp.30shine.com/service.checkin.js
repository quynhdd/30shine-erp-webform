﻿////////////////////////////////////////
// SERVICE ADD BILL
////////////////////////////////////////

var autoLoadBooking;
var validate = { service: false, team: false, checkin: false, phone: false };
var TYPE_DATE_FORMAT = "dd/MM/yyy";
//btn pending and book action=0 
//btn pending disable action=-1
//btn booking disable action=-2
var $action = $('#HDF_Action');
var $customerAdd = $('.customer-add');
var $fullName = $customerAdd.find('.full-name');
var $phone = $customerAdd.find('.phone');
var $btnBook = $('#BtnBook');


var $tableBooked = $('table.table-booked');
var $tableBooking = $('table.table-booking');

var $btnPending = $('#BtnSend');
var $btnFakePending = $('#BtnFakeSend');
var $btnFakeBook = $('#BtnFakeBook');
var $chooseItemColor = $('.wr-choose-team .item-color');
var $teamIdBooking = $('#HDF_TeamId_Booking');
var $teamColorSelect = $('#TeamColorSelect');

function setPending(This, id) {
    var salonId = $('#HDF_SalonId').val();
    $.ajax({
        type: 'POST',
        url: '/GUI/SystemService/Webservice/BookingService.asmx/SetBillPending',
        data: '{id:"' + id + '",salonId:"' + salonId + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var msg = JSON.parse(response.d);
            if (msg.success) {
                //location.reload(true);
                var billCode = msg.msg;
                window.location.replace("/dich-vu/them-phieu-v2.html?msg_update_status=success&msg_update_message=Th%C3%AAm%20bill%20th%C3%A0nh%20c%C3%B4ng!&msg_print_billcode=" +
                    billCode);
                This.css("color", "#000");

            } else {
                This.css("color", "#d0d0d0");
                //console.log(msg.msg);
                showMsgSystem("Đã đầy 4 bill team màu này Pending", "warning");
            }

        }, failure: function (response) { alert(response.d); }
    });
}

function setBookingToForm(This) {
    var id = $(This).attr("id");
    var team = $(This).find('.team').attr('data-color') + "!important";
    var time = $(This).find('.time').text();
    var $caption = $('.status-team');
    //console.log(id + "|" + team + "|" + time);
    var $colorChoose = $('#TeamColorSelect');
    var $timeChoose = $('.wr-choose-team .time');

    $action.val("-1");

    $teamIdBooking.val(id);
    $colorChoose.attr("style", "background:" + team);
    $timeChoose.text(time);

    $btnFakePending.prop('disabled', true);
    $btnPending.css('opacity', '0.6');

    $btnBook.css('opacity', '1');
    $btnFakeBook.prop('disabled', false);

    $caption.text("");
}

$chooseItemColor.bind('click', function () {
    $btnFakePending.prop('disabled', false);
    $btnPending.css('opacity', '1');

    $btnBook.css('opacity', '0.6');
    $btnBook.prop('disabled', true);
    $btnFakeBook.prop('disabled', true);

    $action.val("-2");
})

function getSocialThreadId() {
    $("#HDF_SocialThread").val($("input[name='socialThread']:checked").val());
}

// Xử lý mã khách hàng => bind khách hàng
function LoadCustomer(code) {
    ajaxGetCustomer(code);
}

// Xử lý khi chọn checkbox sản phẩm
function ExcCheckboxItem() {
    $("#EBPopup .td-product-checkbox input[type='checkbox']").unbind("click").bind("click", function () {
        var obj = $(this).parent().parent(),
            boxMoney = obj.find(".box-money"),
            price = obj.find(".td-product-price").data("price"),
            quantity = obj.find("input.product-quantity").val(),
            voucher = obj.find("input.product-voucher").val(),
            checked = $(this).is(":checked"),
            item = obj.attr("data-item"),
            promotion = 0;
        obj.find(".promotion-money input[type='checkbox']:checked").each(function () {
            var value = $(this).attr("data-value").trim();
            promotion += (value != "" ? parseInt(value) : 0);
        });

        // check value
        price = price.toString().trim() != "" ? parseInt(price) : 0;
        quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
        voucher = voucher.trim() != "" ? parseInt(voucher) : 0;


        if (checked) {
            boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100 - promotion)).show();
        } else {
            boxMoney.text("").hide();
        }

    });
}

// Số lượng | Quantity
function ExcQuantity() {
    $("input.product-quantity").bind("change", function () {
        var obj = $(this).parent().parent(),
            quantity = $(this).val(),
            price = obj.find(".td-product-price").data("price"),
            voucher = obj.find("input.product-voucher").val(),
            boxMoney = obj.find(".box-money"),
            promotion = 0;

        obj.find(".promotion-money input[type='checkbox']:checked").each(function () {
            var value = $(this).attr("data-value").trim();
            promotion += (value != "" ? parseInt(value) : 0);
        });

        // check value
        price = price.toString().trim() != "" ? parseInt(price) : 0;
        quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
        voucher = voucher.trim() != "" ? parseInt(voucher) : 0;

        boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100 - promotion)).show();
        TotalMoney();
        getProductIds();
        getServiceIds();
    });
    $("input.product-voucher").bind("change", function () {
        var obj = $(this).parent().parent().parent(),
            quantity = obj.find("input.product-quantity").val(),
            price = obj.find(".td-product-price").data("price"),
            voucher = obj.find("input.product-voucher").val(),
            boxMoney = obj.find(".box-money"),
            promotion = 0;

        obj.find(".promotion-money input[type='checkbox']:checked").each(function () {
            var value = $(this).attr("data-value").trim();
            promotion += (value != "" ? parseInt(value) : 0);
        });

        // check value
        price = price.toString().trim() != "" ? parseInt(price) : 0;
        quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
        voucher = voucher.trim() != "" ? parseInt(voucher) : 0;

        if (promotion == 0) {
            boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100)).show();
        } else {
            obj.find("input.product-voucher").val(0);
        }

        TotalMoney();
        getProductIds();
        getServiceIds();
    });
    $("input.item-promotion").bind("click", function () {
        var obj = $(this).parent().parent().parent().parent().parent(),
            quantity = obj.find("input.product-quantity").val(),
            price = obj.find(".td-product-price").data("price"),
            voucher = obj.find("input.product-voucher").val(),
            boxMoney = obj.find(".box-money"),
            promotion = $(this).attr("data-value"),
            _This = $(this),
            isChecked = $(this).is(":checked");
        obj.find(".promotion-money input[type='checkbox']:checked").prop("checked", false);
        if (isChecked) {
            $(this).prop("checked", true);
            promotion = promotion.trim() != "" ? parseInt(promotion) : 0;
            obj.find("input.product-voucher").val(0);
        } else {
            promotion = 0;
        }

        // check value
        price = price.toString().trim() != "" ? parseInt(price) : 0;
        quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
        voucher = voucher.trim() != "" ? parseInt(voucher) : 0;
        //promotion = promotion.trim() != "" ? parseInt(promotion) : 0;

        if (promotion > 0) {
            boxMoney.text(FormatPrice(quantity * price - promotion)).show();
            obj.find("input.product-voucher").css({ "text-decoration": "line-through" });
        } else {
            boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100)).show();
            obj.find("input.product-voucher").css({ "text-decoration": "none" });
        }
        TotalMoney();
        getProductIds();
        getServiceIds();
    });
}

// Xử lý click hoàn tất chọn item từ popup
function ExcCompletePopup() {
    $("#EBPopup .btn-complete").unbind("click").bind("click", function () {
        var item = $(this).attr("data-item"),
            tableItem = $("table.table-item-" + item + " tbody"),
            objTmp;

        $("#EBPopup .item-product-checkbox input[type='checkbox']:checked").each(function () {
            var Code = $(this).attr("data-code");
            if (!ExcCheckItemIsChose(Code, tableItem)) {
                objTmp = $(this).parent().parent().clone().find("td:first-child").remove().end();
                tableItem.append(objTmp);
            }
        });

        TotalMoney();
        getProductIds();
        getServiceIds();
        ExcQuantity();
        UpdateItemOrder(tableItem);
        autoCloseEBPopup(0);
    });
}

// Check item đã được chọn
function ExcCheckItemIsChose(Code, itemClass) {
    var result = false;
    $(itemClass).find(".td-product-code").each(function () {
        var _Code = $(this).text().trim();
        if (_Code == Code)
            result = true;
    });
    return result;
}

// Remove item đã được chọn
function RemoveItem(THIS, name, itemName) {
    // Confirm
    $(".confirm-yn").openEBPopup();

    $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn hủy [ " + name + " ] ?");
    $("#EBPopup .yn-yes").bind("click", function () {
        var Code = THIS.find(".td-product-code").attr("data-code");
        $("#quick-" + itemName).find("input[data-code='" + Code + "']").removeAttr("checked");
        THIS.remove();
        TotalMoney();
        getProductIds();
        getServiceIds();
        UpdateItemDisplay(itemName);
        autoCloseEBPopup(0);
    });
    $("#EBPopup .yn-no").bind("click", function () {
        autoCloseEBPopup(0);
    });
}

// Update table display
function UpdateItemDisplay(itemName) {
    var dom = $(".table-item-" + itemName);
    var len = dom.find("tbody tr").length;
    if (len == 0) {
        dom.parent().hide();
    } else {
        UpdateItemOrder(dom.find("tbody"));
    }
}

// Update order item
function UpdateItemOrder(dom) {
    var index = 1;
    dom.find("tr").each(function () {
        $(this).find("td.td-product-index").text(index);
        index++;
    });
}

function TotalMoney() {
    var Money = 0;
    $(".fe-service-table-add .box-money:visible").each(function () {
        Money += parseInt($(this).text().replace(/\./gm, ""));
    });
    $("#TotalMoney").val(FormatPrice(Money));
    $("#HDF_TotalMoney").val(Money);
}

function showMsgSystem(msg, status) {
    $("#MsgSystem").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);
    setTimeout(function () {
        $("#MsgSystem").fadeTo("slow", 0, function () {
            $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 1);
        });
    }, 5000);
}

function getProductIds() {
    var Ids = [];
    var prd = {};
    $("table.table-item-product tbody tr").each(function () {
        prd = {};
        var THIS = $(this);

        var Id = THIS.find("td.td-product-code").attr("data-id"),
            Code = THIS.find("td.td-product-code").text().trim(),
            Name = THIS.find("td.td-product-name").text().trim(),
            Price = THIS.find(".td-product-price").attr("data-price"),
            Quantity = THIS.find("input.product-quantity").val(),
            VoucherPercent = THIS.find("input.product-voucher").val(),
            Promotion = 0;

        THIS.find(".promotion-money input[type='checkbox']:checked").each(function () {
            var value = $(this).attr("data-value").trim();
            Promotion += (value != "" ? parseInt(value) : 0);
        });

        // check value
        Id = Id.toString().trim() != "" ? parseInt(Id) : 0;
        Price = Price.toString().trim() != "" ? parseInt(Price) : 0;
        Quantity = Quantity.trim() != "" ? parseInt(Quantity) : 0;
        VoucherPercent = VoucherPercent.trim() != "" ? parseInt(VoucherPercent) : 0;

        prd.Id = Id;
        prd.Code = Code;
        prd.Name = Name;
        prd.Price = Price;
        prd.Quantity = Quantity;
        prd.VoucherPercent = VoucherPercent;
        prd.Promotion = Promotion;

        Ids.push(prd);

    });
    Ids = JSON.stringify(Ids);
    $("#HDF_ProductIds").val(Ids);
}

function getServiceIds() {
    var Ids = [];
    var prd = {};
    $("table.table-item-service tbody tr").each(function () {
        prd = {};
        var THIS = $(this),
            obj = THIS.parent().parent();

        var Id = THIS.find("td.td-product-code").attr("data-id"),
            Code = THIS.find("td.td-product-code").text().trim(),
            Name = THIS.find("td.td-product-name").text().trim(),
            Price = THIS.find(".td-product-price").attr("data-price"),
            Quantity = THIS.find("input.product-quantity").val(),
            VoucherPercent = THIS.find("input.product-voucher").val();

        // check value
        Id = Id.toString().trim() != "" ? parseInt(Id) : 0;
        Price = Price.toString().trim() != "" ? parseInt(Price) : 0;
        Quantity = Quantity.trim() != "" ? parseInt(Quantity) : 0;
        VoucherPercent = VoucherPercent.trim() != "" ? parseInt(VoucherPercent) : 0;

        prd.Id = Id;
        prd.Code = Code;
        prd.Name = Name;
        prd.Price = Price;
        prd.Quantity = Quantity;
        prd.VoucherPercent = VoucherPercent;

        Ids.push(prd);

    });
    Ids = JSON.stringify(Ids);
    $("#HDF_ServiceIds").val(Ids);
}

// Get Customer
function ajaxGetCustomer(CustomerId) {
    if (CustomerId != undefined) {
        $.ajax({
            type: "POST",
            url: "/GUI/SystemService/Ajax/Suggestion.aspx/GetCustomer",
            data: '{CustomerId : "' + CustomerId + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                var mission = JSON.parse(response.d);
                if (mission.success) {
                    var customer = JSON.parse(mission.msg);
                    $("#CustomerName").val(customer.Fullname).attr("disabled", "disabled");
                    $("#CustomerName").attr("data-code", customer.Customer_Code);
                    $("#CustomerName").attr("data-id", customer.Id);
                    $("#HDF_CustomerCode").val(customer.Id);
                    $("#HDF_Suggestion_Code").val(customer.Id);
                    validatePhone(customer.Customer_Code);
                    $("#trFlowInfor").hide();
                } else {
                    $("#CustomerName").val("");
                    $("#CustomerName").attr("data-code", "");
                    $("#CustomerName").attr("data-id", "");
                    $("#HDF_CustomerCode").val("");
                    $("#HDF_Suggestion_Code").val("");
                    //var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                    //showMsgSystem(msg, "warning");
                }
            },
            failure: function (response) { alert(response.d); }
        });
    }
}

function pushQuickData(This, typeData) {
    var Code = This.attr("data-code");
    if (This.is(":checked")) {
        var Dom = $("#table-" + typeData).find("input[data-code='" + Code + "']").parent().parent().clone().find("td:first-child").remove().end(),
        quantity = Dom.find(".product-quantity").val(),
        price = Dom.find(".td-product-price").data("price"),
        voucher = Dom.find("input.product-voucher").val(),
        promotion = 0;

        Dom.find(".promotion-money input[type='checkbox']:checked").each(function () {
            var value = $(this).attr("data-value").trim();
            promotion += (value != "" ? parseInt(value) : 0);
        });

        // check value
        price = price.toString().trim() != "" ? parseInt(price) : 0;
        quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
        voucher = voucher.trim() != "" ? parseInt(voucher) : 0;

        Dom.find(".box-money").text(FormatPrice(quantity * price * (100 - voucher) / 100 - promotion)).show();
        $("#table-item-" + typeData).append(Dom);
        $(".item-" + typeData).show();
    } else {
        $("#table-item-" + typeData).find(".td-product-code[data-code='" + Code + "']").parent().remove();
    }

    TotalMoney();
    getProductIds();
    getServiceIds();
    ExcQuantity();
    UpdateItemOrder($("#table-item-" + typeData).find("tbody"));
}

function pushFreeService(This, id) {
    var arr = [];
    var sv = {};
    $(".free-service input[type='checkbox']").each(function () {
        if ($(this).is(":checked")) {
            arr.push(parseInt($(this).attr('data-id')));
        }
    });
    $("#HDF_FreeService").val(JSON.stringify(arr));
}

function excPromotion(This, money) {
    //promotion-money
    //if (This.is(":checked")) {
    //    $(".fe-service .promotion-money input[type='checkbox']").prop("checked", false);
    //} else {
    //    $(".fe-service .promotion-money input[type='checkbox']").prop("checked", false);
    //    This.prop("checked", true);
    //}

    //TotalMoney();
    //getProductIds();
    //getServiceIds();
    //ExcQuantity();
    //UpdateItemOrder($("#table-item-" + typeData).find("tbody"));
}

function quickAddCustomer(This) {
    var display = $("#TrIframeAddCustomer").css("display");
    if (display == "none") {
        $("#TrIframeAddCustomer").show();
    }
    $("#IframeAddCustomer").toggle(0, function () {
        if (display == "table-row") {
            $("#TrIframeAddCustomer").hide();
        }
    });
}

window.callback_QuickAddCustomer = function (id, code, name) {
    $("#CustomerCode").val(code);
    $("#CustomerName").val(name);
    $("#HDF_CustomerCode").val(code);
    $("#HDF_Suggestion_Code").val(code);
}

window.callback_HideIframeCustomer = function () {
    //$("#IframeAddCustomer").toggle(0, function () {
    //    $("#TrIframeAddCustomer").hide();
    //});
}

window.callback_Iframe_SetStyle = function (style) {
    $("body,html").append($(style));
}

/// Chọn team
function setTeamColor(This, idTeam, color) {
    $(".item-color.active").removeClass("active");
    This.addClass("active");
    $("#HDF_TeamId").val(idTeam);
    // bind color to .team-color-select
    $(".team-color-select").css({ "background": color });
    $('#BtnBook').prop('disabled', false);
    $('#BtnBook').css('opacity', '0.6');
    $('#TeamColorSelect').attr('data-color', idTeam);
    //checkPendingMax(idTeam);
}

function checkPendingMax(teamId) {
    var $btnPending = $('#BtnSend');
    var $btnFakePending = $('#BtnFakeSend');
    var $caption = $('.fe-service-table-add .tr-send .status-team');
    var $time = $('.wr-choose-team .time');
    var salonId = $('#HDF_SalonId').val();
    $.ajax({
        type: "POST",
        url: "/GUI/SystemService/Webservice/BookingService.asmx/CheckPendingMax",
        data: '{teamId : "' + teamId + '",salonId:"' + salonId + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json", success: function (response) {
            check = response.d;
            //console.log("ckeck " + check);
            if (check) {
                //console.log("bill max")
                $btnFakePending.prop('disabled', true);

                $btnPending.css('opacity', '0.6');
                $caption.text("Đã đầy 4 bill team màu này Pending");
                showMsgSystem("Đã đầy 4 bill team màu này Pending", "warning");
                $action.val("-1");
            } else {
                //console.log("team available")
                $btnFakePending.prop('disabled', false);
                $btnPending.css('opacity', '1');
                $caption.text("");
                $time.text("");
                $action.val("-2");
            }
        },
        failure: function (response) { alert(response.d); }
    });

}

/// Update team color
function showBoxColorEdit(This) {
    $(".pending-team-color.active").removeClass("active");
    if (This.hasClass("active")) {
        This.removeClass("active");
    } else {
        This.addClass("active");
    }
    $(".pending-team-color").each(function () {
        if (!$(this).hasClass("active")) {
            $(this).parent().find(".team-color-edit-box").hide();
        }
    });
    This.parent().find(".team-color-edit-box").toggle();
}
function updateTeamColor(This, teamId, color, billId) {
    This.parent().parent().find(".pending-team-color").css("cssText", "background:" + color + "!important");
    // update teamId
    $.ajax({
        type: "POST",
        url: "/GUI/SystemService/Ajax/Suggestion.aspx/Update_BillTeamId",
        data: '{billId : ' + billId + ', teamId : ' + teamId + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json", success: function (response) {
            var mission = JSON.parse(response.d);
            if (mission.success) {
                //
                $(".team-color-edit-box").hide();
            } else {
                delFailed();
            }
        },
        failure: function (response) { alert(response.d); }
    });
}

//====================
// Sync bill status
//====================
var tableBooking = $("table#TableBooking");
var tbodyBooking = $("table#TableBooking tbody");
function loadBillStatus() {
    // 1. Lấy danh sách bill Id
    // 2. Request lấy trạng thái của các bill này
    // 3. Từ kết quả response về, bind lại trạng thái vào các bill
    var ids = [];
    var id;
    tbodyBooking.find(">tr").each(function () {
        id = $(this).attr("id");
        ids.push(id);
    });
    $.ajax({
        type: "POST",
        url: "/GUI/SystemService/Webservice/BookingService.asmx/getBillStatus",
        data: '{ids : \'' + JSON.stringify(ids) + '\'}',
        contentType: "application/json; charset=utf-8",
        dataType: "json", success: function (response) {
            var mission = JSON.parse(response.d);
            var statusBlock;
            var tdStatusBlock;
            if (mission.success) {
                var listStatus = JSON.parse(mission.msg);
                //console.log(mission.msg);
                $.each(listStatus, function (i, v) {
                    tdStatusBlock = tbodyBooking.find(">tr[id='" + v.Id + "'] .td-bill-status");
                    if (v.StatusValue > 0) {
                        statusBlock = '<span class="status-value ' + v.StatusClass + '">' + v.StatusText + '</span><span class="status-time">' + v.Time + '</span>';
                        tdStatusBlock.empty().append($(statusBlock));
                        if (v.StatusValue >= 4) {
                            var tr = tdStatusBlock.parent();
                            setTimeout(function () {
                                tr.remove();
                            }, 1000);
                        }
                    }
                });
            } else {
                //delFailed();
            }
        },
        failure: function (response) { alert(response.d); }
    });
}

//====================
// Sync bill booking
//====================
function toggleListBooking(This) {
    $(".wp-booking").hide();
    This.parent().find(".wp-booking").toggle();
}

function closeListBooking(This) {
    This.parent().parent().hide();
}

function loadBillBookingBySalon(This) {
    $("#HDF_SalonId").val(This.val());
    clearInterval(autoLoadBooking);
    loadBillBooking();
    autoLoadBooking = setInterval(loadBillBooking, 10000);
}


// load danh sách khách đặt lịch
var startBookingID = 0;
var loadBillBooking = function () {
    var salonId = $("#HDF_SalonId").val();
    $.ajax({
        type: "POST",
        url: "/GUI/SystemService/Webservice/BookingService.asmx/loadBillFromBooking_V2",
        data: '{salonId : ' + salonId + ', startBookingID : ' + startBookingID + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json", success: function (response) {
            var mission = JSON.parse(response.d);
            if (mission.success) {
                var list = JSON.parse(mission.msg);
                var trClass = "", checked = "";
                var tr = '';
                if (list.length > 0) {
                    $.each(list, function (i, v) {
                        startBookingID = v.Id > startBookingID ? v.Id : startBookingID;

                        var trClass = "", checked = "";
                        if (v.IsMakeBill == true) {
                            trClass = "class='is-makebill'";
                            checked = "checked='checked'";
                        }

                        tr += "<tr " + trClass + " id='a'>" +
                                    "<td style='text-align:center;' data-id='" + v.Id + "'>" + (i + 1) + "</td>" +
                                    "<td class='c-name'>" + v.CustomerName + "</td>" +
                                    "<td class='c-phone'>" + v.CustomerPhone + "</td>" +
                                    "<td style='text-align:center;'>" + v.Hour + "</td>" +
                                    "<td class='c-stylist'>" + v.StylistName + "</td>" +
                                    "<td style='text-align:center;'><input type='checkbox' onclick=\"updateBookingStatus($(this), " + v.Id + ", '" + v.CustomerName + "', '" + v.CustomerPhone + "')\"" + checked + " /></td>" +
                                    "<td class='delete-booking'><i onclick='openDeleteBox($(this))' class='fa fa-trash-o' aria-hidden='true'></i>" +
                                        "<div class='note-box'>" +
                                            "<textarea placeholder='Ghi chú (Bắt buộc)' rows='3'></textarea>" +
                                            "<div class='col-xs-6'>" +
                                                "<div class='btn-action btn-complete' onclick='deleteBooking($(this)," + v.Id + ")'>OK</div>" +
                                            "</div>" +
                                            "<div class='col-xs-6'>" +
                                                "<div class='btn-action btn-close' onclick='closeDeleteBox($(this))'>Đóng</div>" +
                                            "</div>" +
                                        "</div>" +
                                    "</td>" +
                                "</tr>";
                    });
                }
                $("#tableBookingList tbody").append($(tr));
            } else {
                //delFailed();
            }
        },
        failure: function (response) { alert(response.d); }
    });
}


// Cập nhật trang thái đã lập bill cho khách đặt lịch
function updateBookingStatus(This, id, customerName, customerPhone) {
    var status;
    if (This.is(":checked")) {
        status = true;
        This.prop("checked", true);
        This.parent().parent().addClass("is-makebill");
        // bind phone, customer name to input 
        //bindDataToCustomerInput(This, customerName, customerPhone);
        $("#CustomerCode").val(customerPhone);
        $("#CustomerName").val(customerName);

        //$(".close-booking-box").click();
    } else {
        status = false;
        This.prop("checked", false);
        This.parent().parent().removeClass("is-makebill");
        // clear phone, customer name from input 
        clearDataFromCustomerInput();
    }
    // Cập nhật trạng thái IsMakebill - đã check và tạo bill từ lịch đặt
    $.ajax({
        type: "POST",
        url: "/GUI/SystemService/Webservice/BookingService.asmx/updateStatusForBooking",
        data: '{id : "' + id + '", status : "' + status + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json", success: function (response) {
            var mission = JSON.parse(response.d);
            if (mission.success) {
                //
            } else {
                //delFailed();
            }
        },
        failure: function (response) { alert(response.d); }
    });
}

function loadBillWaitingBySalon(This) {
    $("#HDF_SalonId").val(This.val());
    clearInterval(autoLoadWaiting);
    loadBillWaiting();
    autoLoadWaiting = setInterval(loadBillWaiting, 10000);
}

// load danh sách khách đợi tại salon
var startWatingID = 0;
var loadBillWaiting = function () {
    var salonId = $("#HDF_SalonId").val();
    $.ajax({
        type: "POST",
        url: "/GUI/FrontEnd/Service/Service_Add_V3.aspx/loadBillWaitAtSalon",
        data: '{salonId : ' + salonId + ', startID : ' + startWatingID + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json", success: function (response) {
            var mission = JSON.parse(response.d);
            if (mission.success) {
                var list = JSON.parse(mission.msg);
                var trClass = "", checked = "";
                var tr = '';
                if (list.length > 0) {
                    $.each(list, function (i, v) {
                        startWatingID = v.Id > startWatingID ? v.Id : startWatingID;

                        var trClass = "", checked = "";
                        if (v.IsPending == true) {
                            trClass = "class='is-makebill'";
                            checked = "checked='checked'";
                        }
                        tr += "<tr " + trClass + " id='a'>" +
                                    "<td style='text-align:center;'>" + v.Id + "</td>" +
                                    "<td class='c-name'>" + v.CustomerName + "</td>" +
                                    "<td class='c-phone'>" + v.CustomerPhone + "</td>" +
                                    "<td>" + (v.HourFrame != null ? v.HourFrame : "") + "</td>" +
                                    "<td>" + (v.StylistName != null ? v.StylistName : "") + "</td>" +
                                    "<td class='c-time'>" + convertDate(v.CreatedTime) + "</td>" +
                                    "<td style='text-align:center;'><input type='checkbox' onclick=\"updateBillWaitStatus($(this), " + v.Id + ", '" + v.CustomerName + "', '" + v.CustomerPhone + "')\"" + checked + " /></td>" +
                                "</tr>";
                    });
                }
                $("#tableBillWait tbody").append($(tr));
                reSortBooking();
            } else {
                //delFailed();
            }
        },
        failure: function (response) { alert(response.d); }
    });
}

// Cập nhật trạng thái đã chuyển sang pending của bill khách đợi tại salon
function updateBillWaitStatus(This, id, customerName, customerPhone) {
    var status;
    if (This.is(":checked")) {
        status = true;
        This.prop("checked", true);
        This.parent().parent().addClass("is-makebill");
        // bind phone, customer name to input 
        //bindDataToCustomerInput(This);
        //$(".close-booking-box").click();
        $("#CustomerCode").val(customerPhone);
        $("#CustomerName").val(customerName);
    } else {
        status = false;
        This.prop("checked", false);
        This.parent().parent().removeClass("is-makebill");
        // clear phone, customer name from input 
        clearDataFromCustomerInput();
    }
    // Cập nhật trạng thái IsMakebill - đã check và tạo bill từ lịch đặt
    $.ajax({
        type: "POST",
        url: "/GUI/FrontEnd/Service/Service_Add_V3.aspx/UpdateBillWaitAtSalon",
        data: '{id : "' + id + '", status : "' + status + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json", success: function (response) {
            var mission = JSON.parse(response.d);
            if (mission.success) {
                //
            } else {
                //delFailed();
            }
        },
        failure: function (response) { alert(response.d); }
    });
}

// Convert Datetime to dd/MM/yyyy HH:ii:ss
function convertDate(inputFormat) {
    function pad(s) { return (s < 10) ? '0' + s : s; }
    inputFormat = parseInt(inputFormat.replace('/Date(', '').replace(')/', ''));
    if (!isNaN(inputFormat)) {
        var d = new Date(inputFormat);
        return [pad(d.getHours()), pad(d.getMinutes())].join('h');
    } else {
        return '';
    }
}

// Convert Datetime to dd/MM/yyyy HH:ii:ss
function convertDate1(inputFormat) {
    function pad(s) { return (s < 10) ? '0' + s : s; }
    var d = new Date(inputFormat);
    return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join('/');
}

// Thêm khách đợi tại salon
function addCustomerWait(This) {
    addLoading();
    var customerPhone = $("#CustomerCode").val();
    var customerName = $("#CustomerName").val();
    var salonId = 0;
    if ($("#Salon").length) {
        salonId = $("#Salon").val();
    } else {
        salonId = $("#HDF_SalonId").val();
    }
    $.ajax({
        type: "POST",
        url: "/checkin/add-bill-wait-salon",
        data: '{CustomerPhone : "' + customerPhone + '", CustomerName : "' + customerName + '", SalonId : ' + salonId + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json", success: function (response) {
            var mission = JSON.parse(response.d);
            if (mission.success) {
                var billWait = JSON.parse(mission.msg);
                var trClass = "", checked = "";
                if (billWait.IsPending == true) {
                    trClass = "class='is-makebill'";
                    checked = "checked='checked'";
                }
                //var tr = "<tr " + trClass + ">" +
                //                "<td style='text-align:center;'>" + billWait.Id + "</td>" +
                //                //"<td style='text-align:center;'>"+v.Hour+"</td>" +
                //                "<td class='c-name'>" + billWait.CustomerName + "</td>" +
                //                //"<td class='c-stylist'>"+v.StylistName+"</td>" +
                //                "<td class='c-phone'>" + billWait.CustomerPhone + "</td>" +
                //                "<td class='c-time'>" + convertDate(billWait.CreatedTime) + "</td>" +
                //                "<td style='text-align:center;'><input type='checkbox' onclick='updateBillWaitStatus($(this), " + billWait.Id + ")' " + checked + " /></td>" +
                //            "</tr>";
                //$(".customer-waiting-list table tbody").append($(tr));
                clearDataFromCustomerInput();
                alert("Thêm khách chờ thành công.")
                removeLoading();
            } else {
                //delFailed();
                alert("Đã có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển!");
                removeLoading();
            }
        },
        failure: function (response) { alert(response.d); }
    });
}

/// Kiểm tra và bind dữ liệu khách hàng từ danh sách booking
function bindDataToCustomerInput(This, customerName, customerPhone) {
    var phone = This.parent().parent().find('.c-phone').text().replace(/\s+/, '');
    var name = capitalizeText(This.parent().parent().find('.c-name').text().trim());
    //console.log(phone + ' : ' + capitalizeText(name));
    // check load customer data
    $.ajax({
        type: "POST",
        url: "/GUI/SystemService/Ajax/Suggestion.aspx/GetCustomerByPhone",
        data: '{phone : "' + phone + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json", success: function (response) {
            var mission = JSON.parse(response.d);
            if (mission.success) {
                var customer = JSON.parse(mission.msg);
                $("#CustomerCode").val(customer.Phone);
                $("#CustomerName").val(customer.Fullname).attr("disabled", "disabled");
                $("#HDF_CustomerCode").val(customer.Id);
                $("#trFlowInfor").hide();
            } else {
                $("#CustomerCode").val(phone).removeAttr("disabled");
                $("#CustomerName").val(name).removeAttr("disabled");
                $("#HDF_CustomerCode").val(0);
                $("#trFlowInfor").show();
            }
        },
        failure: function (response) { alert(response.d); }
    });
}

/// clear data từ input khách hàng
function clearDataFromCustomerInput() {
    $("#CustomerCode").val("").removeAttr("disabled");
    $("#CustomerName").val("").removeAttr("disabled");
    $("#HDF_CustomerCode").val(0);
    $("#trFlowInfor").show();
}

/// kiểm tra và bind dữ liệu khách hàng bằng số điện thoại khi blur khỏi input số điện thoại
function checkBindCustomerDataByPhone(This) {
    //if (validatePhone(This)) {
    //    $.ajax({
    //        type: "POST",
    //        url: "/GUI/SystemService/Ajax/Suggestion.aspx/GetCustomerByPhone",
    //        data: '{phone : "' + This.val() + '"}',
    //        contentType: "application/json; charset=utf-8",
    //        dataType: "json", success: function (response) {
    //            var mission = JSON.parse(response.d);
    //            if (mission.success) {
    //                var customer = JSON.parse(mission.msg);
    //                if (customer.Phone != "") {
    //                    $("#CustomerCode").val(customer.Phone);
    //                    $("#CustomerName").val(customer.Fullname).attr("disabled", "disabled");
    //                    $("#HDF_CustomerCode").val(customer.Id);
    //                    $("#trFlowInfor").hide();
    //                }
    //            } else {
    //                //
    //            }
    //        },
    //        failure: function (response) { alert(response.d); }
    //    });
    //}
}

/// Validate phone
function validatePhone(This) {
    var phone, ret = false;
    if (typeof This == 'string') {
        phone = This.replace(/\s+/, "");
    } else {
        phone = This.val().replace(/\s+/, "");
    }
    if (phone == "") {
        hideValidatePhone();
        ret = true;
    } else if (!checkStringNumber(phone) || !checkPhoneLength(phone, [10, 11])) {
        showValidatePhone();
        ret = false;
    } else {
        hideValidatePhone();
        ret = true;
    }
    return ret;
}

function validatePhoneByString(phone) {
    var phone = This.val().replace(/\s+/, "");
    if (phone == "") {
        hideValidatePhone();
    } else if (!checkStringNumber(phone) || !checkPhoneLength(phone, [10, 11])) {
        showValidatePhone();
    } else {
        hideValidatePhone();
    }
}

function validateTypingNumber(event, This) {
    if (!checkTypingNumber(event) || (This.val().length >= 11 && !isAllowTyping(event))) {
        event.preventDefault();
        showValidatePhone();
    } else if (!isAllowTyping(event) && isFuncKeyCode(event)) {
        event.preventDefault();
    } else {
        hideValidatePhone();
    }
}

function showValidatePhone() {
    $(".validate-phone").show();
    validate.phone = false;
}

function hideValidatePhone() {
    $(".validate-phone").hide();
    validate.phone = true;
}
/// End Validate phone

//document.getElementById("copyButton").addEventListener("click", function() {
//    copyToClipboard(document.getElementById("copyTarget"));
//});

function copyContent(This) {
    $(".booking-listing table td").css({ "background": "none" });
    This.style.background = "#2771C3";
    $(".booking-listing table td").css({ "color": "#222222" });
    This.style.color = "#ffffff";
    // copy phone-customer_name to clip box
    copyToClipboard(This);
    // paste phone-customer_name to input
}

//============================
// Suggestion Functions
//============================
function Bind_Suggestion() {
    $(".eb-suggestion").bind("keyup", function (e) {
        if (e.keyCode == 40) {
            UpDownListSuggest($(this));
        } else {
            Call_Suggestion($(this));
        }
    });
    $(".eb-suggestion").bind("focus", function () {
        Call_Suggestion($(this));
    });
    $(".eb-suggestion").bind("blur", function () {
        //Exc_To_Reset_Suggestion($(this));
    });
    $(window).bind("click", function (e) {
        if ((!e.target.className.match("eb-select") && e.target.parentElement.className.match("ul-listing-suggestion")) ||
            (!e.target.className.match("eb-select") && !e.target.parentElement.className.match("ul-listing-suggestion"))) {
            //EBSelect_HideBox();
        }
    });
}

function UpDownListSuggest(This) {
    var UlSgt = This.parent().find(".ul-listing-suggestion"),
        index = 0,
        LisLen = UlSgt.find(">li").length - 1,
        Value;

    This.blur();
    UlSgt.find(">li.active").removeClass("active");
    UlSgt.find(">li:eq(" + index + ")").addClass("active");

    $(window).unbind("keydown").bind("keydown", function (e) {
        if (e.keyCode == 40) {
            if (index == LisLen) return false;
            UlSgt.find(">li.active").removeClass("active");
            UlSgt.find(">li:eq(" + (++index) + ")").addClass("active");
            return false;
        } else if (e.keyCode == 38) {
            if (index == 0) return false;
            UlSgt.find(">li.active").removeClass("active");
            UlSgt.find(">li:eq(" + (--index) + ")").addClass("active");
            return false;
        } else if (e.keyCode == 13) {
            // Bind data to HDF Field
            var THIS = UlSgt.find(">li.active");
            //var Value = THIS.text().trim();
            var Value = THIS.attr("data-id");
            var dataField = This.attr("data-field");

            BindIdToHDF(THIS, Value, dataField, "#HDF_Suggestion_Code", "#HDF_Suggestion_Field", This);
            EBSelect_HideBox();
        }
    });
}

function Exc_To_Reset_Suggestion(This) {
    var value = This.val();
    if (value == "") {
        $(".eb-suggestion").each(function () {
            var THIS = $(this);
            var sgValue = THIS.val();
            if (sgValue != "") {
                BindIdToHDF(THIS, sgValue, THIS.attr("data-field"), "#HDF_Suggestion_Code", "#HDF_Suggestion_Field", THIS);
                return false;
            }
        });
    }
}

function Call_Suggestion(This) {
    var text = This.val(),
        field = This.attr("data-field");
    Suggestion(This, text, field);
}

function Suggestion(This, text, field) {
    var This = This;
    var text = text || "";
    var field = field || "";
    var InputDomId;
    var HDF_Sgt_Code = "#HDF_Suggestion_Code";
    var HDF_Sgt_Field = "#HDF_Suggestion_Field";
    var Callback = This.attr("data-callback");

    if (text == "") return false;

    switch (field) {
        case "customer.name": InputDomId = "#CustomerName"; break;
        case "customer.phone": InputDomId = "#CustomerPhone"; break;
        case "customer.code": InputDomId = "#CustomerCode"; break;
        case "bill.code": InputDomId = "#BillCode"; break;
    }

    $.ajax({
        type: "POST",
        url: "/GUI/SystemService/Ajax/Suggestion.aspx/Suggestion_Customer",
        data: '{field : "' + field + '", text : "' + text + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json", success: function (response) {
            var mission = JSON.parse(response.d);
            if (mission.success) {
                var OBJ = JSON.parse(mission.msg);
                if (OBJ != null && OBJ.length > 0) {
                    var lis = "";
                    $.each(OBJ, function (i, v) {
                        lis += "<li data-code='" + v.Customer_Code + "' data-id='" + v.CustomerId + "'" +
                                     "onclick=\"BindIdToHDF($(this),'" + v.CustomerId + "','" + field + "','" + HDF_Sgt_Code +
                                     "','" + HDF_Sgt_Field + "','" + InputDomId + "')\" data-callback='" + Callback + "'>" +
                                    v.Value +
                                "</li>";
                    });
                    This.parent().find("ul.ul-listing-suggestion").empty().append(lis).parent().show();
                    This.parent().find(".eb-select-data").show();
                } else {
                    This.parent().find(".eb-select-data").hide().find("ul.ul-listing-suggestion").empty();
                }
            } else {
                This.parent().find("ul.ul-listing-suggestion").empty();
                var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                showMsgSystem(msg, "warning");
            }
        },
        failure: function (response) { alert(response.d); }
    });
}

function BindIdToHDF(THIS, Code, Field, HDF_Sgt_Code, HDF_Sgt_Field, Input_DomId) {
    var text = THIS.text().trim();
    $("input.eb-suggestion").val("");
    $(HDF_Sgt_Code).val(Code);
    $(HDF_Sgt_Field).val(Field);
    $(Input_DomId).val(text);
    $(Input_DomId).parent().find(".eb-select-data").hide();
    if (THIS.attr("data-callback") != "") {
        window[THIS.attr("data-callback")].call();
    }
    // Auto post server
    //$("#BtnFakeUP").click();
}

function EBSelect_HideBox() {
    $(".eb-select-data").hide();
    $("ul.ul-listing-suggestion li.active").removeClass("active");
}

function Callback_UpdateCustomerCode() {
    LoadCustomer($("#HDF_Suggestion_Code").val());
}

function CustomerInputOnchange(This) {
    //ajaxGetCustomer(This.attr("data-id"));
}

//===================
// Auto select staff
//===================
function AutoSelectStaff() {
    $(".auto-select").bind("keyup", function (e) {
        var This = $(this);
        var StaffType = This.attr("data-field");
        var code = parseInt(This.val());
        if (!isNaN(code)) {
            $.ajax({
                type: "POST",
                url: "/GUI/SystemService/Ajax/Suggestion.aspx/Suggestion_OneStaff",
                data: '{code : "' + code + '", SalonId : ' + $("#HDF_SalonId").val() + ', StaffType : "' + StaffType + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var mission = JSON.parse(response.d);
                    if (mission.success) {
                        var OBJ = JSON.parse(mission.msg);
                        if (OBJ.length > 0) {
                            var lis = "";
                            This.parent().find(".fake-value").text(This.val() + "- " + OBJ[0].Fullname);
                            $("#" + StaffType).val(OBJ[0].Id);
                        } else {
                            This.parent().find(".fake-value").text("");
                            $("#" + StaffType).val("");
                        }
                    } else {
                        This.parent().find("ul.ul-listing-suggestion").empty();
                        var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                        showMsgSystem(msg, "warning");
                    }
                },
                failure: function (response) { alert(response.d); }
            });
        } else {
            This.parent().find(".fake-value").text("");
            $("#" + StaffType).val("");
        }
    });
}

/*
* BẢN NÂNG CẤP THÁNG 3 / 2017
*/
function closeListBooking(This) {
    This.parent().hide();
}
function toggleListBooking(This) {
    $(".frame-booking").hide();
    This.parent().find(".frame-booking").toggle();
}

function openDeleteBox(This) {
    This.parent().find(".note-box").show();
}

function closeDeleteBox(This) {
    This.parent().parent().hide();
}

function deleteBooking(This, id) {
    var box = This.parent().parent();
    var note = box.find("textarea").val();
    if (note != "") {
        $.ajax({
            type: "POST",
            url: "/checkin/booking/delte",
            data: '{Id : ' + id + ', Note : "' + note + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                if (response.d != null) {
                    if (response.d.success) {
                        box.parent().parent().remove();
                        reSortBooking();
                    }
                    alert(response.d.msg);
                }
            },
            failure: function (response) { alert(response.d); }
        });
    }
    else {
        alert("Bạn chưa nhập ghi chú.");
    }
}

function reSortBooking() {
    $("table#tableBookingList tbody tr").each(function (i, v) {
        $(this).find("td:first-child").text((i + 1));
    });
}


function callCustomerData(This) {
    var phone = This.val();
    var phoneLen = phone.length;
    //if (phoneLen >= 10 && phoneLen <= 11) {
    // Lấy dữ liệu thông qua API
    $.ajax({
        type: "POST",
        //url: "/GUI/Booking/OrderBooking.aspx/BindHourWhereStylistNewVersion3",
        url: "/checkin/customerbyphone",
        data: '{phone : "' + phone + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json", success: function (response) {
            if (response.d != null) {
                $("#CustomerName").val(response.d.Fullname);
                $("#HDF_CustomerCode").val(response.d.Id);
            }
            else {
                $("#CustomerName").val("");
                $("#HDF_CustomerCode").val("");
            }
        },
        failure: function (response) { alert(response.d); }
    });
    //}
    //else {

    //}
}

function openFrameBooking() {
    $(".frame-booking-pane").openEBPopup();
}

/*
* Them dich vu vao list
*/
var itemService = function ()
{
    this.Id = 0;
    this.Code = '';
    this.Name = '';
    this.Price = '';
    this.Quantity = 0;
    this.VoucherPercent = 0;
}
var serviceList = [];
function addServiceToList(This, id, code, name, price, quantity, voucherPercent)
{
    var item = new itemService();
    item.Id = id;
    item.Code = code;
    item.Name = name;
    item.Price = price;
    item.Quantity = quantity;
    item.VoucherPercent = voucherPercent;

    var index = getServiceIndex(item);

    if (This.prop("checked")) {
        if (index == -1) {
            serviceList.push(item);
        }
    }
    else
    {
        if (index != -1)
        {
            serviceList.splice(index, 1);
        }
    }
    $("#HDF_ServiceIds").val(JSON.stringify(serviceList));
    //console.log(serviceList);
}

function getServiceIndex(item)
{
    for (var i = 0; i < serviceList.length; i++)
    {
        if (item.Id == serviceList[i].Id)
        {
            return i;
        }
    }
    return -1;
}

jQuery(document).ready(function ($) {
    $action.val("0");
    // Add active menu
    $("#glbService").addClass("active");

    // Mở box listing sản phẩm
    $(".show-product").bind("click", function () {
        $(this).parent().parent().find(".listing-product").show();
    });

    $(".show-item").bind("click", function () {
        var item = $(this).attr("data-item");
        var classItem = ".popup-" + item + "-item";

        $(classItem).openEBPopup();
        ExcCheckboxItem();
        ExcQuantity();
        ExcCompletePopup();

        $('#EBPopup .listing-product').mCustomScrollbar({
            theme: "dark-2",
            scrollInertia: 100
        });

        $("#EBPopup .btn-esc").bind("click", function () { autoCloseEBPopup(0); });
    });

    // Btn Send
    $("#BtnSend").bind("click", function () {
        // var statusTeam = $('.status-team').text();
        //console.log($("#CustomerCode").val());
        //if ($("#CustomerCode").val().replace(/\s+/, "") != "") {                
        //    showValidatePhone();
        //} else 
        if (validate.team == false && $teamColorSelect.attr('data-color') == "") {
            // show EBPopup
            $(".confirm-yn").openEBPopup();
            $("#EBPopup .confirm-yn-text").text("Bạn chưa chọn đội. Bạn có chắc chắn muốn tiếp tục ?");

            $("#EBPopup .yn-yes").bind("click", function () {
                validate.team = true;
                $("#EBPopup").empty();
                $("#BtnSend").click();
            });

            $("#EBPopup .yn-no").bind("click", function () {
                autoCloseEBPopup(0);
            });
        }
        else if (validate.service == false && ($("#HDF_ServiceIds").val() == "" || $("#HDF_ServiceIds").val() == "[]")) {
            //console.log(" service ");
            // show EBPopup
            $(".confirm-yn").openEBPopup();
            $("#EBPopup .confirm-yn-text").text("Bạn chưa chọn dịch vụ. Bạn có chắc chắn muốn tiếp tục ?");

            $("#EBPopup .yn-yes").bind("click", function () {
                validate.service = true;
                $("#EBPopup").empty();
                $("#BtnSend").click();
            });

            $("#EBPopup .yn-no").bind("click", function () {
                autoCloseEBPopup(0);
            });
        }
        else if (validate.checkin == false && ($("#Reception").val() == "")) {
            //console.log(" service ");
            // show EBPopup
            $(".confirm-yn").openEBPopup();
            $("#EBPopup .confirm-yn-text").text("Bạn chưa chọn lễ tân check-in. Bạn có chắc chắn muốn tiếp tục ?");

            $("#EBPopup .yn-yes").bind("click", function () {
                validate.checkin = true;
                $("#EBPopup").empty();
                $("#BtnSend").click();
            });

            $("#EBPopup .yn-no").bind("click", function () {
                autoCloseEBPopup(0);
            });
        } else {
            autoCloseEBPopup(0);
            // Check cosmetic seller
            if (($("#HDF_ProductIds").val() != "" && $("#HDF_ProductIds").val() != "[]") && $("#Cosmetic").val() == 0) {
                $("#ValidateCosmetic").css({ "visibility": "visible" });
            } else {
                $("#ValidateCosmetic").css({ "visibility": "hidden" });
                var CustomerId = parseInt($("#HDF_CustomerCode").val());
                // Add loading và post form
                addLoading();
                $("#BtnFakeSend").click();
            }
        }
    });

    // Btn Booking
    $("#BtnBook").bind("click", function () {

        if ($action.val() != "-2") {

            // console.log("here");
            if ($("#CustomerName").val() == "" && $("#InputCusNoInfor").is(":checked") == false) {
                showMsgSystem("Bạn chưa nhập mã khách hàng hoặc chưa tích chọn \"Khách không cho thông tin\".", "warning");
                //} else if ($("#HDF_TotalMoney").val() == "") {
                //    showMsgSystem("Bạn chưa chọn dịch vụ hoặc sản phẩm.", "warning");
            } else {
                var teamColor = $('#TeamColorSelect').attr('style');

                if (teamColor != null && teamColor != "")
                    $("#BtnFakeBook").click();
                else
                    showMsgSystem("Bạn không thể đặt lịch cho khách khi chưa chọn Team ", "warning");
            }
        }
    });

    /// Focus vào ô nhập số điện thoại
    $("#CustomerCode").focus();

    //============================
    // Show System Message
    //============================ 
    var qs = getQueryStrings();
    showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);

    // Enter don't submit
    $(window).on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

    /// click out of team color
    $('html').click(function () {
        $(".team-color-edit-box").hide();
    });
    $('.pending-team-color').click(function (event) {
        event.stopPropagation();
    });

    /// click out of eb-suggestion-listing
    $('html').click(function () {
        $(".eb-suggestion-listing").hide();
    });
    $('.eb-suggestion-listing').click(function (event) {
        event.stopPropagation();
    });

    // Sync bill
    //var requestLoadBillStatus = setInterval(loadBillStatus, 3000);

    // get SocialThreadId
    getSocialThreadId();

    // load bill booking (danh sách khách đặt lịch)
    loadBillBooking();
    autoLoadBooking = setInterval(loadBillBooking, 2000);
    // load bill waiting (danh sách khách đợi tại salon)
    loadBillWaiting();
    autoLoadWaiting = setInterval(loadBillWaiting, 10000);

    /// click out of wp-booking
    $('html').click(function () {
        $(".frame-booking").hide();
    });
    $('.open-booking-list, .frame-booking').click(function (event) {
        event.stopPropagation();
    });

    // Bind staff suggestion
    Bind_Suggestion();
    AutoSelectStaff();

    // Mặc định dịch vụ
    $("#defaultService").click();
});