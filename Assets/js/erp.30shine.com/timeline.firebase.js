﻿
//* TODO(DEVELOPER): Update Firebase initialization code:
//1. Go to the Firebase console: https://console.firebase.google.com/
//2. Choose a Firebase project you've created
//3. Click "Add Firebase to your web app"
//4. Replace the following initialization code with the code from the Firebase console:

// Retrieve Firebase Messaging object.
const messaging = firebase.messaging();
// [END get_messaging_object]

// IDs of divs that display Instance ID token UI or request permission UI.
const tokenDivId = 'token_div';
const permissionDivId = 'permission_div';

// [START refresh_token]
// Callback fired if Instance ID token is updated.
messaging.onTokenRefresh(function () {
    messaging.getToken()
	.then(function (refreshedToken) {
	    console.log('Token refreshed.');
	    // Indicate that the new Instance ID token has not yet been sent to the
	    // app server.
	    setTokenSentToServer(false);
	    // Send Instance ID token to app server.
	    sendTokenToServer(refreshedToken);
	    // [START_EXCLUDE]
	    // Display new Instance ID token and clear UI of all previous messages.
	    resetUI();
	    // [END_EXCLUDE]
	})
	.catch(function (err) {
	    console.log('Unable to retrieve refreshed token ', err);
	    showToken('Unable to retrieve refreshed token ', err);
	});
});
// [END refresh_token]

// [START receive_message]
// Handle incoming messages. Called when:
// - a message is received while the app has focus
// - the user clicks on an app notification created by a sevice worker
//   `messaging.setBackgroundMessageHandler` handler.
messaging.onMessage(function (payload) {
    console.log("Message received. ", payload);
    //console.log(JSON.parse(payload.data.contents));
    rebindBookCell(JSON.parse(payload.data.contents));
    // [START_EXCLUDE]
    // Update the UI to include the received message.
    appendMessage(payload);
    // [END_EXCLUDE]
});
// [END receive_message]

function resetUI() {
    clearMessages();
    showToken('loading...');
    // [START get_token]
    // Get Instance ID token. Initially this makes a network call, once retrieved
    // subsequent calls to getToken will return from cache.
    messaging.getToken()
	.then(function (currentToken) {
	    if (currentToken) {
	        sendTokenToServer(currentToken);
	        updateUIForPushEnabled(currentToken);
	        insertIDToken(currentToken);
	        console.log(currentToken);
	    } else {
	        // Show permission request.
	        console.log('No Instance ID token available. Request permission to generate one.');
	        requestPermission();
	        // Show permission UI.
	        updateUIForPushPermissionRequired();
	        setTokenSentToServer(false);
	    }
	})
	.catch(function (err) {
	    console.log('An error occurred while retrieving token. ', err);
	    showToken('Error retrieving Instance ID token. ', err);
	    setTokenSentToServer(false);
	});
}
// [END get_token]

function showToken(currentToken) {
    return
    // Show token in console and UI.
    var tokenElement = document.querySelector('#token');
    tokenElement.textContent = currentToken;
}

// Send the Instance ID token your application server, so that it can:
// - send messages back to this app
// - subscribe/unsubscribe the token from topics
function sendTokenToServer(currentToken) {
    if (!isTokenSentToServer()) {
        console.log('Sending token to server...');
        // TODO(developer): Send the current token to your server.
        setTokenSentToServer(true);
    } else {
        console.log('Token already sent to server so won\'t send it again ' +
			'unless it changes');
    }

}

function isTokenSentToServer() {
    if (window.localStorage.getItem('sentToServer') == 1) {
        return true;
    }
    return false;
}

function setTokenSentToServer(sent) {
    window.localStorage.setItem('sentToServer', sent ? 1 : 0);
}

function showHideDiv(divId, show) {
    return;
    const div = document.querySelector('#' + divId);
    if (show) {
        div.style = "display: visible";
    } else {
        div.style = "display: none";
    }
}

function requestPermission() {
    console.log('Requesting permission...');
    // [START request_permission]
    messaging.requestPermission()
	.then(function () {
	    console.log('Notification permission granted.');
	    // TODO(developer): Retrieve an Instance ID token for use with FCM.
	    // [START_EXCLUDE]
	    // In many cases once an app has been granted notification permission, it
	    // should update its UI reflecting this.
	    resetUI();
	    // [END_EXCLUDE]
	})
	.catch(function (err) {
	    console.log('Unable to get permission to notify.', err);
	});
    // [END request_permission]
}

function deleteToken() {
    // Delete Instance ID token.
    // [START delete_token]
    messaging.getToken()
	.then(function (currentToken) {
	    messaging.deleteToken(currentToken)
		.then(function () {
		    console.log('Token deleted.');
		    setTokenSentToServer(false);
		    // [START_EXCLUDE]
		    // Once token is deleted update UI.
		    resetUI();
		    // [END_EXCLUDE]
		})
		.catch(function (err) {
		    console.log('Unable to delete token. ', err);
		});
	    // [END delete_token]
	})
	.catch(function (err) {
	    console.log('Error retrieving Instance ID token. ', err);
	    showToken('Error retrieving Instance ID token. ', err);
	});

}

// Add a message to the messages element.
function appendMessage(payload) {
    return;
    const messagesElement = document.querySelector('#messages');
    const dataHeaderELement = document.createElement('h5');
    const dataElement = document.createElement('pre');
    dataElement.style = 'overflow-x:hidden;'
    dataHeaderELement.textContent = 'Received message:';
    dataElement.textContent = JSON.stringify(payload, null, 2);
    messagesElement.appendChild(dataHeaderELement);
    messagesElement.appendChild(dataElement);
}

// Clear the messages element of all children.
function clearMessages() {
    return;
    const messagesElement = document.querySelector('#messages');
    while (messagesElement.hasChildNodes()) {
        messagesElement.removeChild(messagesElement.lastChild);
    }
}

function updateUIForPushEnabled(currentToken) {
    showHideDiv(tokenDivId, true);
    showHideDiv(permissionDivId, false);
    showToken(currentToken);
}

function updateUIForPushPermissionRequired() {
    showHideDiv(tokenDivId, false);
    showHideDiv(permissionDivId, true);
}

resetUI();

/****************************************************************
* HÀM XỬ LÝ TỪ ERP 30SHINE
****************************************************************/
function insertIDToken(currentToken) {
    $.ajax({
        type: "POST",
        url: "/realtime/updatetoken",
        data: '{currentToken : \'' + currentToken + '\'}',
        contentType: "application/json; charset=utf-8",
        dataType: "json", success: function (response) {
            console.log(response.d.success);
        },
        failure: function (response) { alert(response.d); }
    });
}

function rebindBookCell(dataBill) {
    // vao day
    //console.log(dataBill);
    //console.log($("#" + dataBill.stylistID + "-" + dataBill.hourID));
    var bookCell = $("#" + dataBill.stylistID + "-" + dataBill.hourID);

    //var cell = '<div class="board-cell-content" onclick="openPrintBill($(this), ' + dataBill.bookingID + ', ' + dataBill.stylistID + ', \'' + dataBill.stylistName + '\', \'' + dataBill.customerPhone + '\', \'' + dataBill.customerName + '\', ' + dataBill.hourID + ', \'' + dataBill.hourFrame + '\')">' +
	//				'<p class="customer-name">' + dataBill.customerName + '</p>' +
	//				'<p class="customer-phone">' + dataBill.customerPhone + '</p>' +
	//			'</div> ' +
	//			'<i class="fa fa-check-square aria-hidden="true"></i>';

    var strCell = "<div class=\"board-cell-content\" onclick=\"openPrintBill($(this), 0, " + dataBill.stylistID + ", '" + dataBill.stylistName + "', '" + dataBill.customerPhone + "', '" + dataBill.customerName + "', " + dataBill.hourID + ", '" + dataBill.hourFrame + "')\">" +
                                    "<p class=\"customer-name\">" + dataBill.customerName + "</p>" +
                                    "<p class=\"customer-phone\">" + dataBill.customerPhone + "</p>" +
                                "</div>" +
                                "<div class=\"call-phone\" title=\"Check gọi điện cho khách hàng\" onclick=\"checkCallPhone($(this), " + dataBill.bookingID + ")\">" +
                                    "<i class=\"fa fa-check\" aria-hidden=\"true\" ></i>" +
                                "</div>" +
                                "<div class=\"cancel-book\" title=\"Hủy lịch đặt\" onclick=\"openCancelBook($(this), " + dataBill.bookingID + ", '" + dataBill.CustomerName + "', '" + dataBill.CustomerPhone + "')\">" +
                                    "<i class=\"fa fa-times\" aria-hidden=\"true\" ></i>" +
                                "</div>";

    bookCell.empty().append($(strCell)).addClass("is-book");
    //onclick="openPrintBill($(this), <%=v2.BookingID %>, <%=v.StylistId %>, '<%=v.StylistName %>', '<%=v2.CustomerPhone %>', '<%=v2.CustomerName %>', <%=v2.HourId %>, '<%=v2.HourFrame %>')"

}