﻿
/*
* Lấy thống tin khách hàng từ API
*/
function callCustomerData(This) {
    var phone = This.val();
    var phoneLen = phone.length;
    if (phoneLen >= 10 && phoneLen <= 11) {
        // Lấy dữ liệu thông qua API
        startLoading();
        $.ajax({
            type: "POST",
            //url: "/GUI/Booking/OrderBooking.aspx/BindHourWhereStylistNewVersion3",
            url: "/checkin/customerbyphone-j5pFZRNvCvmAe2Lv",
            data: '{phone : "' + phone + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                if (response.d != null) {
                    //console.log(response.d);
                    $("#EBPopup .customer-name").val(response.d.Fullname);
                }
                else {
                    //$("#EBPopup .customer-name").val("");
                }
                finishLoading();
            },
            failure: function (response) { alert(response.d); }
        });
    }
    else if (phoneLen > 0) {
        alert("Vui lòng nhập đúng số điện thoại.")
    }
}

/*
* Set giá trị salonID trong trường hợp chọn salon
*/
function setSalon(This) {
    bill.salonID = This.val();
}

function setTeam(This, teamID) {
    bill.teamID = teamID;
    $("#EBPopup .list-team .btn-team.active").removeClass("active");
    This.addClass("active");
}

/*
* Set giá trị nhân viên checkin
*/
function setCheckin(This) {
    //startLoading();
    $.ajax({
        type: "POST",
        url: "/beta-timeline/checkinbycode",
        data: '{orderCode : "' + This.val() + '", salonID : ' + bill.salonID + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json", success: function (response) {
            if (response.d != null) {
                bill.checkinID = response.d.Id;
                $("#EBPopup .checkin-name").val(response.d.Fullname);
            }
            else {
                bill.checkinID = 0;
                $("#EBPopup .checkin-name").val("");
            }
            //finishLoading();
        },
        failure: function (response) { alert(response.d); }
    });
}

/*
* Mở popup để in bill
*/
function openPrintBill(This, billID, bookingID, stylistID, stylistName, customerPhone, customerName, hourID, hourFrame, isAutoStylist, IsBookAtSalon) {
    bill.billID = billID;
    bill.stylistID = stylistID;
    bill.stylistName = stylistName;
    bill.hourID = hourID;
    bill.hourFrame = hourFrame;
    if (bookingID != null) {
        bill.bookingID = bookingID;
    }

    $("#eventBillAdd").openEBPopup();
    $(".customer-phone").focus();

    // Ẩn nút Đặt chỗ
    $("#EBPopup .btn-action-book").hide();
    // Mở list hóa chất
    $("#EBPopup .list-item-hc").show();
    // Ẩn list khách book Stylist
    $("#EBPopup .list-item-book-stylist").hide();

    $("#EBPopup .popup-hour-frame").text(hourFrame.substring(0, 5));
    $("#EBPopup .popup-stylist span.sp-stylist-name").text(stylistName);
    if (customerName != "") {
        $("#EBPopup .customer-name").val(customerName);
    }
    if (customerPhone != "") {
        $("#EBPopup .customer-phone").val(customerPhone);
    }

    if (isAutoStylist == 0) {
        $("#EBPopup .star-not-auto-stylist").addClass("active");
    }

    if (IsBookAtSalon == 1) {
        $("#EBPopup .customer-book-at-salon").addClass("active");
    }

    ms = $('#EBPopup .ms-suggest').magicSuggest({
        maxSelection: 10,
        data: listService,
        valueField: 'Id',
        displayField: 'Name'
    });
    $(ms).on('selectionchange', function (e, m) {
        bill.services = this.getSelection();
    });

    setDefaultService();

    if (This.parent().hasClass("is-makebill")) {
        //$("#EBPopup .btn-print-bill").hide();
        // Mở nút Cập nhật phiếu
        $("#EBPopup .btn-action-print").hide();
        $("#EBPopup .btn-action-print-update").show();
        startLoading();
        $.ajax({
            type: "POST",
            url: "/beta-timeline/getbillinfobyid",
            data: '{billTempID : ' + billID + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                var billTemp = response.d.data;
                if (billTemp != null) {
                    // I. Mã KH, tên KH
                    if (billTemp.checkinID != null) {
                        $("#EBPopup .checkin-code").val(billTemp.checkinID);
                        $("#EBPopup .checkin-name").val(billTemp.checkinName);
                    }
                    // II. Set gói dịch vụ
                    if (billTemp.services != null) {
                        var arrService = [];
                        if (billTemp.services.length > 0) {
                            $.each(billTemp.services, function (i, v) {
                                arrService.push(v);
                            });
                        }
                        ms.setSelection(arrService);
                    }
                    // III. Hóa chất
                    if (billTemp.HCItem != null) {
                        var hcId;
                        var hcList = billTemp.HCItem.split(',');
                        if (hcList.length > 0) {
                            hcArr = [];
                            for (var i in hcList) {
                                if (hcList[i] != "") {
                                    hcId = parseInt(hcList[i]);
                                    if (!isNaN(hcId)) {
                                        hcArr.push(hcId);
                                    }
                                }
                            }

                            $("#EBPopup .list-item-hc input[type='checkbox']").each(function () {
                                var index = hcList.indexOf($(this).attr("data-value"));
                                if (index != -1) {
                                    $(this).prop("checked", true);
                                }
                            });
                        }
                    }
                }

                finishLoading();
            },
            failure: function (response) { alert(response.d); }
        });
    }
    else {
        // Mở nút In phiếu
        $("#EBPopup .btn-action-print-update").hide();
        $("#EBPopup .btn-action-print").show();
        //setDefaultService();	
    }
}

/*
* Mở popup để đặt lịch cho khách
*/
function openBooking(This, bookingID, stylistID, stylistName, customerPhone, customerName, hourID, hourFrame) {    
    bill.stylistID = stylistID;
    bill.stylistName = stylistName;
    bill.hourID = hourID;
    bill.hourFrame = hourFrame;
    if (bookingID != null) {
        bill.bookingID = bookingID;
    }

    $("#eventBillAdd").openEBPopup();

    // Mở nút Đặt chỗ
    $("#EBPopup .btn-action-book").show();
    $(".customer-phone").focus();
    // Ẩn nút In phiếu
    $("#EBPopup .btn-action-print").hide();
    // Ẩn nút Cập nhật phiếu
    $("#EBPopup .btn-action-print-update").hide();
    // Ẩn team
    $("#EBPopup .team-wrap").hide();
    // Ẩn checkin
    $("#EBPopup .checkin-wrap").hide();
    // Ẩn dịch vụ
    $("#EBPopup .service-wrap").hide();
    // Ẩn list hóa chất
    $("#EBPopup .list-item-hc").hide();
    // Mở list khách book Stylist
    $("#EBPopup .list-item-book-stylist").show();

    $("#EBPopup .popup-hour-frame").text(hourFrame.substring(0, 5));
    $("#EBPopup .popup-stylist span.sp-stylist-name").text(stylistName);
    if (customerName != "") {
        $("#EBPopup .customer-name").val(customerName);
    }
    if (customerPhone != "") {
        $("#EBPopup .customer-phone").val(customerPhone);
    }

    ms = $('#EBPopup .ms-suggest').magicSuggest({
        maxSelection: 10,
        data: listService,
        valueField: 'Id',
        displayField: 'Name'
    });
    $(ms).on('selectionchange', function (e, m) {
        bill.services = this.getSelection();
    });

    setDefaultService();

    if (This.parent().hasClass("is-makebill")) {
        $("#EBPopup .btn-print-bill").hide();
    }
    else {
        //setDefaultService();	
    }
}

/*
* Set dịch vụ mặc định
*/
function setDefaultService() {
    if (listService.length > 0) {
        for (var index in listService) {
            if (listService[index].Id == defaultServiceID) {
                ms.setSelection([listService[index]]);
                break;
            }
        }
    }
}

/*
* Kiểm tra gọi điện thoại cho khách
*/
function checkCallPhone(This, bookingID) {
    var isCall = false;
    if (This.find(">i").hasClass("is-call")) {
        isCall = false;
        This.find(">i").removeClass("is-call");
    }
    else {
        isCall = true;
        This.find(">i").addClass("is-call");
    }

    startLoading();
    $.ajax({
        type: "POST",
        url: "/beta-timeline/setcallphone",
        data: '{bookingID : ' + bookingID + ', isCall : ' + isCall + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json", success: function (response) {
            if (response.d != null) {
                bill.checkinID = response.d.Id;
                $("#EBPopup .checkin-name").val(response.d.Fullname);
            }
            else {
                bill.checkinID = 0;
                $("#EBPopup .checkin-name").val("");
            }
            finishLoading();
        },
        failure: function (response) { alert(response.d); }
    });
}

/*
* Mở popup hủy lịch đặt
*/
function openCancelBook(This, bookingID, customerName, customerPhone) {
    $(".popup-cancel-book").openEBPopup();
    bookingCancel.cancelBookingID = bookingID;
    bookingCancel.customerName = customerName;
    bookingCancel.customerPhone = customerPhone;
    bookingCancel.itemBookingCancel = This;
}

/*
* Hủy lịch đặt
*/
function cancelBook() {
    bookingCancel.noteCancel = $("#EBPopup textarea.note-cancel").val();
    if (bookingCancel.cancelBookingID > 0) {
        if (bookingCancel.noteCancel.trim().length > 0) {
            startLoading();
            $.ajax({
                type: "POST",
                url: "/GUI/FrontEnd/Service/TimeLine_Beta.aspx/cancelBook",
                data: '{bookingID : ' + bookingCancel.cancelBookingID + ', noteCancel : "' + bookingCancel.noteCancel + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    if (response.d != null && response.d.success) {
                        var _board = bookingCancel.itemBookingCancel.parent();
                        _board.removeClass("is-book");
                        _board.removeAttr("id");
                        _board.find(".board-cell-content").remove();
                        _board.find(".call-phone").remove();
                        _board.find(".cancel-book").remove();
                        _board.find(".stylist-not-auto").remove();
                        _board.find(".customer-hc").remove();
                        _board.prepend('<div class="board-cell-content"></div>');
                        alert("Hủy lịch thành công!");
                        location.href = location.href;
                        //autoCloseEBPopup();
                    }
                    else {
                        alert(response.d.message);
                    }
                    finishLoading();
                },
                failure: function (response) { alert(response.d); }
            });
        }
        else {
            alert("Vui lòng nhập lý do hủy lịch.");
        }
    }
    else {
        alert("Lỗi. Không xác định được khung giờ. Vui lòng liên hệ nhóm phát triển.")
    }
}

/*
* In bill
*/
function printBill() {
    bill.customerName = $("#EBPopup .customer-name").val();
    bill.customerPhone = $("#EBPopup .customer-phone").val();
    if (hcArr.length > 0) {
        var hcStr = '';
        for (var i = 0; i < hcArr.length; i++) {
            hcStr += hcArr[i] + ',';
        }
        bill.HCItem = hcStr.replace(/\,+$/, '');
    }

    var errorList = [];
    if (bill.customerName == "") {
        errorList.push("Tên Khách hàng");
    }
    if (bill.customerPhone == "") {
        errorList.push("Số điện thoại");
    }
    if (bill.services.length == 0) {
        errorList.push("Dịch vụ");
    }

    if (errorList.length > 0) {
        var errorStr = "";
        for (var index in errorList) {
            errorStr += "[" + errorList[index] + "] ";
        }
        alert("Vui lòng nhập đầy đủ thông tin " + errorStr);
    }
    else {
        startLoading();
        $.ajax({
            type: "POST",
            url: "/beta-timeline/printbill",
            data: '{billClient : \'' + JSON.stringify(bill) + '\'}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                if (response.d != null && response.d.success == true) {
                    billResponse = response.d.data;

                    if (billResponse != null) {
                        var strCell = "<div class=\"board-cell-content\" onclick=\"openPrintBill($(this), " + billResponse.billID + ", " + billResponse.bookingID + ", " + billResponse.stylistID + ", '" + billResponse.stylistName + "', '" + billResponse.customerPhone + "', '" + billResponse.customerName + "', " + billResponse.hourID + ", '" + billResponse.hourFrame + "', " + (billResponse.IsAutoStylist ? 1 : 0) + ", " + (billResponse.IsBookAtSalon ? 1 : 0) + ")\">" +
                                        "<p class=\"customer-name\">" + billResponse.customerName + "</p>" +
                                        "<p class=\"customer-phone\">" + billResponse.customerPhone + "</p>" +
                                    "</div>" +
                                    "<div class=\"cancel-book\" title=\"Hủy lịch đặt\" onclick=\"openCancelBook($(this), " + billResponse.bookingID + ", '" + billResponse.customerName + "', '" + billResponse.customerPhone + "')\" style=\"display:none;\">" +
                                        "<i class=\"fa fa-times\" aria-hidden=\"true\"></i>" +
                                    "</div>" +
                                    "<div class=\"call-phone\" title=\"Check gọi điện cho khách hàng\" onclick=\"checkCallPhone($(this), " + billResponse.bookingID + ")\">" +
                                        "<i class=\"fa fa-check " + (billResponse.isCall ? "is-call" : "") + "\"" + "aria-hidden=\"true\"></i>" +
                                    "</div>" +
                                    "<div class=\"board-cell-action\">" +
                                        "<div class=\"col-xs-6 cell-item-action cell-item-action-book\" onclick=\"openBooking($(this), " + billResponse.bookingID + ", " + billResponse.stylistID + ", '" + billResponse.stylistName + "', '', '', " + billResponse.hourID + ", '" + billResponse.hourFrame + "')\">Book lịch</div>" +
                                    "</div>";

                        // Nếu bill được lập từ cell đã booking
                        var cell = $("#" + billResponse.stylistID + "-" + billResponse.bookingID + "-bk");
                        if (cell.length > 0) {
                            var isMultipleItem = cell.parent().find(".board-cell-item").length > 1 ? true : false;
                            cell.empty().append($(strCell)).addClass("is-makebill");
                            if (isMultipleItem) {
                                cell.addClass("col-6");
                            }
                        }
                        else {
                            var cell = $("#" + billResponse.stylistID + "-" + billResponse.hourID);
                            var isMultipleItem = cell.find(".customer-name").length > 0 ? true : false;
                            strCell = "<div class=\"board-cell-item is-makebill is-enroll\" id=\"" + (billResponse.stylistID + "-" + billResponse.bookingID + "-bk") + "\">" + strCell + "</div>";
                            if (isMultipleItem) {
                                cell.append($(strCell));
                            }
                            else {
                                cell.empty().append($(strCell));
                            }
                        }

                        //console.log(billResponse);
                        location.href = location.pathname + "?msg_update_status=success&msg_update_message=Thêm%20thành%20công!&msg_print_billcode=" + billResponse.PDFBillCode;
                        //openPdfIframe("/Public/PDF/" + billResponse.PDFBillCode + ".pdf");
                        autoCloseEBPopup();

                        // Reset giá trị bill
                        bill.billID = 0;
                        bill.bookingID = 0;
                        bill.hourID = 0;
                        bill.hourFrame = '';
                        bill.stylistID = 0;
                        bill.stylistName = '';
                        bill.customerPhone = '';
                        bill.customerName = '';
                        bill.salonName = '';
                        bill.checkinID = 0;
                        bill.checkinName = '';
                        bill.services = [];
                        bill.teamID = 0;
                        bill.PDFBillCode = '';
                        bill.HCItem = '';
                        bill.IsAutoStylist = true;

                        // Reset 1 số biến giá trị
                        hcArr = [];
                    }
                    else {
                        alert("Đã có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển.");
                    }
                }
                else {
                    alert("Đã có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển.");
                }
                finishLoading();
            },
            failure: function (response) { alert(response.d); }
        });
    }
}

/*
* Update bill
*/
function updateBill() {
    bill.customerName = $("#EBPopup .customer-name").val();
    bill.customerPhone = $("#EBPopup .customer-phone").val();
    console.log('updatebill - hcArr');
    console.log(hcArr);
    if (hcArr.length > 0) {
        var hcStr = '';
        for (var i = 0; i < hcArr.length; i++) {
            hcStr += hcArr[i] + ',';
        }
        bill.HCItem = hcStr.replace(/\,+$/, '');
    }

    var errorList = [];
    if (bill.customerName == "") {
        errorList.push("Tên Khách hàng");
    }
    if (bill.customerPhone == "") {
        errorList.push("Số điện thoại");
    }
    if (bill.services.length == 0) {
        errorList.push("Dịch vụ");
    }

    if (errorList.length > 0) {
        var errorStr = "";
        for (var index in errorList) {
            errorStr += "[" + errorList[index] + "] ";
        }
        alert("Vui lòng nhập đầy đủ thông tin " + errorStr);
    }
    else {
        startLoading();
        $.ajax({
            type: "POST",
            url: "/beta-timeline/updatebill",
            data: '{billClient : \'' + JSON.stringify(bill) + '\'}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                if (response.d != null && response.d.success == true) {
                    billResponse = response.d.data;
                    if (billResponse != null) {
                        var strCell = "<div class=\"board-cell-content\" onclick=\"openPrintBill($(this), " + billResponse.billID + ", " + billResponse.bookingID + ", " + billResponse.stylistID + ", '" + billResponse.stylistName + "', '" + billResponse.customerPhone + "', '" + billResponse.customerName + "', " + billResponse.hourID + ", '" + billResponse.hourFrame + "', " + (billResponse.IsAutoStylist ? 1 : 0) + ", " + (billResponse.IsBookAtSalon ? 1 : 0) + ")\">" +
                                        "<p class=\"customer-name\">" + billResponse.customerName + "</p>" +
                                        "<p class=\"customer-phone\">" + billResponse.customerPhone + "</p>" +
                                    "</div>" +
                                    "<div class=\"cancel-book\" title=\"Hủy lịch đặt\" onclick=\"openCancelBook($(this), " + billResponse.bookingID + ", '" + billResponse.customerName + "', '" + billResponse.customerPhone + "')\" style=\"display:none;\">" +
                                        "<i class=\"fa fa-times\" aria-hidden=\"true\"></i>" +
                                    "</div>" +
                                    "<div class=\"call-phone\" title=\"Check gọi điện cho khách hàng\" onclick=\"checkCallPhone($(this), " + billResponse.bookingID + ")\">" +
                                        "<i class=\"fa fa-check " + (billResponse.isCall ? "is-call" : "") + "\"" + "aria-hidden=\"true\"></i>" +
                                    "</div>" +
                                    "<div class=\"board-cell-action\">" +
                                        "<div class=\"col-xs-6 cell-item-action cell-item-action-book\" onclick=\"openBooking($(this), " + billResponse.bookingID + ", " + billResponse.stylistID + ", '" + billResponse.stylistName + "', '', '', " + billResponse.hourID + ", '" + billResponse.hourFrame + "')\">Book lịch</div>" +
                                    "</div>" +
                                    '<div class="stylist-not-auto ' + (!(billResponse.IsAutoStylist) ? "active" : "") + '">B</div>' +
                                    '<div class="customer-hc ' + (billResponse.HCItem != '' ? "active" : "") + '">H</div>';

                        // Nếu bill được lập từ cell đã booking
                        var cell = $("#" + billResponse.stylistID + "-" + billResponse.bookingID + "-bk");
                        if (cell.length > 0) {
                            var isMultipleItem = cell.parent().find(".board-cell-item").length > 1 ? true : false;
                            cell.empty().append($(strCell)).addClass("is-makebill");
                            if (isMultipleItem) {
                                cell.addClass("col-6");
                            }
                        }
                        else {
                            var cell = $("#" + billResponse.stylistID + "-" + billResponse.hourID);
                            var isMultipleItem = cell.find(".customer-name").length > 0 ? true : false;
                            strCell = "<div class=\"board-cell-item is-makebill is-enroll\" id=\"" + (billResponse.stylistID + "-" + billResponse.bookingID + "-bk") + "\">" + strCell + "</div>";
                            if (isMultipleItem) {
                                cell.append($(strCell));
                            }
                            else {
                                cell.empty().append($(strCell));
                            }
                        }

                        console.log(billResponse);
                        //location.href = location.pathname + "?msg_update_status=success&msg_update_message=Thêm%20thành%20công!&msg_print_billcode=" + billResponse.PDFBillCode;
                        //openPdfIframe("/Public/PDF/" + billResponse.PDFBillCode + ".pdf");
                        autoCloseEBPopup();

                        // Reset giá trị bill
                        bill.billID = 0;
                        bill.bookingID = 0;
                        bill.hourID = 0;
                        bill.hourFrame = '';
                        bill.stylistID = 0;
                        bill.stylistName = '';
                        bill.customerPhone = '';
                        bill.customerName = '';
                        bill.salonName = '';
                        bill.checkinID = 0;
                        bill.checkinName = '';
                        bill.services = [];
                        bill.teamID = 0;
                        bill.PDFBillCode = '';
                        bill.HCItem = '';
                        bill.IsAutoStylist = true;

                        // Reset 1 số biến giá trị
                        hcArr = [];
                    }
                    else {
                        alert("Đã có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển.");
                    }
                }
                else {
                    alert("Đã có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển.");
                }
                finishLoading();
            },
            failure: function (response) { alert(response.d); }
        });
    }
}

/*
* Insert booking
*/
function insertBooking() {
    bill.customerName = $("#EBPopup .customer-name").val();
    bill.customerPhone = $("#EBPopup .customer-phone").val();

    var errorList = [];
    if (bill.customerName == "") {
        errorList.push("Tên Khách hàng");
    }
    if (bill.customerPhone == "") {
        errorList.push("Số điện thoại");
    }

    if (errorList.length > 0) {
        var errorStr = "";
        for (var index in errorList) {
            errorStr += "[" + errorList[index] + "] ";
        }
        alert("Vui lòng nhập đầy đủ thông tin " + errorStr);
    }
    else {
        startLoading();
        $.ajax({
            type: "POST",
            url: "/GUI/FrontEnd/Service/TimeLine_Beta.aspx/checkBooking",
            data: '{billClient : \'' + JSON.stringify(bill) + '\', isDelete: "' + parseInt(0) + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                //console.log(response);
                if (response.d != null && response.d.success == true) {
                    var bookingRecord = JSON.parse(response.d.message);
                    var cell = $("#" + bill.stylistID + "-" + bill.hourID);

                    var isMultipleItem = cell.find(".customer-name").length > 0 ? true : false;
                    if (bookingRecord.Id > 0) {
                        var strCell = "<div class=\"board-cell-item is-enroll event is-book is-book-at-salon" + (isMultipleItem ? "col-6" : "") + "\" id=\"" + (bill.stylistID + "-" + bookingRecord.Id) + "-bk\" draggable='true'>" +
                                            "<div class=\"board-cell-content\" onclick=\"openPrintBill($(this), 0, " + bookingRecord.Id + ", " + bill.stylistID + ", '" + bill.stylistName + "', '" + bill.customerPhone + "', '" + bill.customerName + "', " + bill.hourID + ", '" + bill.hourFrame + "', " + (bookingRecord.IsAutoStylist ? 1 : 0) + ", " + (bookingRecord.IsBookAtSalon ? 1 : 0) + ")\">" +
                                                "<p class=\"customer-name\">" + bill.customerName + "</p>" +
                                                "<p class=\"customer-phone\">" + bill.customerPhone + "</p>" +
                                            "</div>" +
                                            "<div class=\"cancel-book\" title=\"Hủy lịch đặt\" onclick=\"openCancelBook($(this), " + bookingRecord.Id + ", '" + bookingRecord.CustomerName + "', '" + bookingRecord.CustomerPhone + "')\">" +
                                                "<i class=\"fa fa-times\" aria-hidden=\"true\"></i>" +
                                            "</div>" +
                                            "<div class=\"call-phone\" title=\"Check gọi điện cho khách hàng\" onclick=\"checkCallPhone($(this), " + bookingRecord.Id + ")\">" +
                                                "<i class=\"fa fa-check\"" + "aria-hidden=\"true\"></i>" +
                                            "</div>" +
                                            "<div class=\"board-cell-action\">" +
                                                "<div class=\"col-xs-6 cell-item-action cell-item-action-printbill\" onclick=\"openPrintBill($(this), 0, " + bookingRecord.Id + ", " + bill.stylistID + ", '" + bill.stylistName + "', '" + bill.customerPhone + "', '" + bill.customerName + "', " + bill.hourID + ", '" + bill.hourFrame + "', " + (bookingRecord.IsAutoStylist ? 1 : 0) + ", " + (bookingRecord.IsBookAtSalon ? 1 : 0) + ")\">In phiếu</div>" +
                                                "<div class=\"col-xs-6 cell-item-action cell-item-action-book\" onclick=\"openBooking($(this), " + bookingRecord.Id + ", " + bill.stylistID + ", '" + bill.stylistName + "', '', '', " + bill.hourID + ", '" + bill.hourFrame + "')\">Book lịch</div>" +
                                             "</div>" +
                                             '<div class="stylist-not-auto ' + (!(bookingRecord.IsAutoStylist) ? "active" : "") + '">B</div>' +
                                                '<div class="customer-hc ' + (bookingRecord.IsHCItem ? "active" : "") + '">H</div>' +
                                            '</div>';
                        if (isMultipleItem) {
                            cell.append($(strCell));
                            cell.find(".board-cell-item").addClass("col-6");
                        }
                        else {
                            cell.empty().append($(strCell));
                        }
                    }

                    autoCloseEBPopup();

                    // Reset giá trị bill
                    bill.billID = 0;
                    bill.bookingID = 0;
                    bill.hourID = 0;
                    bill.hourFrame = '';
                    bill.stylistID = 0;
                    bill.stylistName = '';
                    bill.customerPhone = '';
                    bill.customerName = '';
                    bill.salonName = '';
                    bill.checkinID = 0;
                    bill.checkinName = '';
                    bill.services = [];
                    bill.teamID = 0;
                    bill.PDFBillCode = '';
                    bill.HCItem = '';
                    bill.IsAutoStylist = true;
                    bill.IsBookAtSalon = true;


                    // Reset 1 số biến giá trị
                    hcArr = [];
                }
                else if (response.d.success == false && response.d.status == "delete") {
                    if (confirm('Bạn có chắc chắn muốn xoá booking đã có không?')) {
                        deleteBooking();
                    } else {
                        //NotdeleteBooking();
                        console.log("Huỷ");
                    }
                } else {
                    alert("Đã có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển.");
                }
                finishLoading();
            },
            failure: function (response) { alert(response.d); }
        });
    }
}

function deleteBooking() {
    bill.customerName = $("#EBPopup .customer-name").val();
    bill.customerPhone = $("#EBPopup .customer-phone").val();

    var errorList = [];
    if (bill.customerName == "") {
        errorList.push("Tên Khách hàng");
    }
    if (bill.customerPhone == "") {
        errorList.push("Số điện thoại");
    }

    if (errorList.length > 0) {
        var errorStr = "";
        for (var index in errorList) {
            errorStr += "[" + errorList[index] + "] ";
        }
        alert("Vui lòng nhập đầy đủ thông tin " + errorStr);
    }
    else {
        startLoading();
        $.ajax({
            type: "POST",
            url: "/GUI/FrontEnd/Service/TimeLine_Beta.aspx/insertBooking",
            data: '{billClient : \'' + JSON.stringify(bill) + '\', isDelete: "' + parseInt(1) + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                //console.log(response);
                if (response.d != null && response.d.success == true) {
                    var bookingRecord = JSON.parse(response.d.message);
                    var cell = $("#" + bill.stylistID + "-" + bill.hourID);

                    var isMultipleItem = cell.find(".customer-name").length > 0 ? true : false;
                    if (bookingRecord.Id > 0) {
                        var strCell = "<div class=\"board-cell-item is-enroll is-book is-book-at-salon" + (isMultipleItem ? "col-6" : "") + "\" id=\"" + (bill.stylistID + "-" + bookingRecord.Id) + "-bk\">" +
                                            "<div class=\"board-cell-content\" onclick=\"openPrintBill($(this), 0, " + bookingRecord.Id + ", " + bill.stylistID + ", '" + bill.stylistName + "', '" + bill.customerPhone + "', '" + bill.customerName + "', " + bill.hourID + ", '" + bill.hourFrame + "', " + (bookingRecord.IsAutoStylist ? 1 : 0) + ", " + (bookingRecord.IsBookAtSalon ? 1 : 0) + ")\">" +
                                                "<p class=\"customer-name\">" + bill.customerName + "</p>" +
                                                "<p class=\"customer-phone\">" + bill.customerPhone + "</p>" +
                                            "</div>" +
                                            "<div class=\"cancel-book\" title=\"Hủy lịch đặt\" onclick=\"openCancelBook($(this), " + bookingRecord.Id + ", '" + bookingRecord.CustomerName + "', '" + bookingRecord.CustomerPhone + "')\">" +
                                                "<i class=\"fa fa-times\" aria-hidden=\"true\"></i>" +
                                            "</div>" +
                                            "<div class=\"call-phone\" title=\"Check gọi điện cho khách hàng\" onclick=\"checkCallPhone($(this), " + bookingRecord.Id + ")\">" +
                                                "<i class=\"fa fa-check\"" + "aria-hidden=\"true\"></i>" +
                                            "</div>" +
                                            "<div class=\"board-cell-action\">" +
                                                "<div class=\"col-xs-6 cell-item-action cell-item-action-printbill\" onclick=\"openPrintBill($(this), 0, " + bookingRecord.Id + ", " + bill.stylistID + ", '" + bill.stylistName + "', '" + bill.customerPhone + "', '" + bill.customerName + "', " + bill.hourID + ", '" + bill.hourFrame + "', " + (bookingRecord.IsAutoStylist ? 1 : 0) + ", " + (bookingRecord.IsBookAtSalon ? 1 : 0) + ")\">In phiếu</div>" +
                                                "<div class=\"col-xs-6 cell-item-action cell-item-action-book\" onclick=\"openBooking($(this), " + bookingRecord.Id + ", " + bill.stylistID + ", '" + bill.stylistName + "', '', '', " + bill.hourID + ", '" + bill.hourFrame + "')\">Book lịch</div>" +
                                             "</div>" +
                                             '<div class="stylist-not-auto ' + (!(bookingRecord.IsAutoStylist) ? "active" : "") + '">B</div>' +
                                                '<div class="customer-hc ' + (bookingRecord.IsHCItem ? "active" : "") + '">H</div>' +
                                            '</div>';
                        if (isMultipleItem) {
                            cell.append($(strCell));
                            cell.find(".board-cell-item").addClass("col-6");
                        }
                        else {
                            cell.empty().append($(strCell));
                        }
                    }

                    autoCloseEBPopup();

                    // Reset giá trị bill
                    bill.billID = 0;
                    bill.bookingID = 0;
                    bill.hourID = 0;
                    bill.hourFrame = '';
                    bill.stylistID = 0;
                    bill.stylistName = '';
                    bill.customerPhone = '';
                    bill.customerName = '';
                    bill.salonName = '';
                    bill.checkinID = 0;
                    bill.checkinName = '';
                    bill.services = [];
                    bill.teamID = 0;
                    bill.PDFBillCode = '';
                    bill.HCItem = '';
                    bill.IsAutoStylist = true;
                    bill.IsBookAtSalon = true;


                    // Reset 1 số biến giá trị
                    hcArr = [];
                    location.href = location.href;

                } else {
                    alert("Đã có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển.");
                }
                finishLoading();
            },
            failure: function (response) { alert(response.d); }
        });
    }
}

function NotdeleteBooking() {
    bill.customerName = $("#EBPopup .customer-name").val();
    bill.customerPhone = $("#EBPopup .customer-phone").val();

    var errorList = [];
    if (bill.customerName == "") {
        errorList.push("Tên Khách hàng");
    }
    if (bill.customerPhone == "") {
        errorList.push("Số điện thoại");
    }

    if (errorList.length > 0) {
        var errorStr = "";
        for (var index in errorList) {
            errorStr += "[" + errorList[index] + "] ";
        }
        alert("Vui lòng nhập đầy đủ thông tin " + errorStr);
    }
    else {
        startLoading();
        $.ajax({
            type: "POST",
            url: "/GUI/FrontEnd/Service/TimeLine_Beta.aspx/insertBooking",
            data: '{billClient : \'' + JSON.stringify(bill) + '\', isDelete: "' + parseInt(0) + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                //console.log(response);
                if (response.d != null && response.d.success == true) {
                    var bookingRecord = JSON.parse(response.d.message);
                    var cell = $("#" + bill.stylistID + "-" + bill.hourID);
                    var isMultipleItem = cell.find(".customer-name").length > 0 ? true : false;
                    if (bookingRecord.Id > 0) {
                        var strCell = "<div class=\"board-cell-item is-enroll is-book is-book-at-salon" + (isMultipleItem ? "col-6" : "") + "\" id=\"" + (bill.stylistID + "-" + bookingRecord.Id) + "-bk\">" +
                                            "<div class=\"board-cell-content\" onclick=\"openPrintBill($(this), 0, " + bookingRecord.Id + ", " + bill.stylistID + ", '" + bill.stylistName + "', '" + bill.customerPhone + "', '" + bill.customerName + "', " + bill.hourID + ", '" + bill.hourFrame + "', " + (bookingRecord.IsAutoStylist ? 1 : 0) + ", " + (bookingRecord.IsBookAtSalon ? 1 : 0) + ")\">" +
                                                "<p class=\"customer-name\">" + bill.customerName + "</p>" +
                                                "<p class=\"customer-phone\">" + bill.customerPhone + "</p>" +
                                            "</div>" +
                                            "<div class=\"cancel-book\" title=\"Hủy lịch đặt\" onclick=\"openCancelBook($(this), " + bookingRecord.Id + ", '" + bookingRecord.CustomerName + "', '" + bookingRecord.CustomerPhone + "')\">" +
                                                "<i class=\"fa fa-times\" aria-hidden=\"true\"></i>" +
                                            "</div>" +
                                            "<div class=\"call-phone\" title=\"Check gọi điện cho khách hàng\" onclick=\"checkCallPhone($(this), " + bookingRecord.Id + ")\">" +
                                                "<i class=\"fa fa-check\"" + "aria-hidden=\"true\"></i>" +
                                            "</div>" +
                                            "<div class=\"board-cell-action\">" +
                                                "<div class=\"col-xs-6 cell-item-action cell-item-action-printbill\" onclick=\"openPrintBill($(this), 0, " + bookingRecord.Id + ", " + bill.stylistID + ", '" + bill.stylistName + "', '" + bill.customerPhone + "', '" + bill.customerName + "', " + bill.hourID + ", '" + bill.hourFrame + "', " + (bookingRecord.IsAutoStylist ? 1 : 0) + ", " + (bookingRecord.IsBookAtSalon ? 1 : 0) + ")\">In phiếu</div>" +
                                                "<div class=\"col-xs-6 cell-item-action cell-item-action-book\" onclick=\"openBooking($(this), " + bookingRecord.Id + ", " + bill.stylistID + ", '" + bill.stylistName + "', '', '', " + bill.hourID + ", '" + bill.hourFrame + "')\">Book lịch</div>" +
                                             "</div>" +
                                             '<div class="stylist-not-auto ' + (!(bookingRecord.IsAutoStylist) ? "active" : "") + '">B</div>' +
                                                '<div class="customer-hc ' + (bookingRecord.IsHCItem ? "active" : "") + '">H</div>' +
                                            '</div>';
                        if (isMultipleItem) {
                            cell.append($(strCell));
                            cell.find(".board-cell-item").addClass("col-6");
                        }
                        else {
                            cell.empty().append($(strCell));
                        }
                    }

                    autoCloseEBPopup();

                    // Reset giá trị bill
                    bill.billID = 0;
                    bill.bookingID = 0;
                    bill.hourID = 0;
                    bill.hourFrame = '';
                    bill.stylistID = 0;
                    bill.stylistName = '';
                    bill.customerPhone = '';
                    bill.customerName = '';
                    bill.salonName = '';
                    bill.checkinID = 0;
                    bill.checkinName = '';
                    bill.services = [];
                    bill.teamID = 0;
                    bill.PDFBillCode = '';
                    bill.HCItem = '';
                    bill.IsAutoStylist = true;
                    bill.IsBookAtSalon = true;

                    // Reset 1 số biến giá trị
                    hcArr = [];

                } else {
                    alert("Đã có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển.");
                }
                finishLoading();
            },
            failure: function (response) { alert(response.d); }
        });
    }
}

/*
* Tìm kiếm khách hàng từ bảng timeline
*/
function searchByPhone(evt, This) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    // if(charCode != 13)
    // {
    // if (charCode > 31 && (charCode < 48 || charCode > 57)) {
    // return false;
    // }
    // }
    var isMatch = false;
    if (charCode == 13) {
        $("p.customer-phone").each(function () {
            if ($(this).text() == This.val()) {
                $(this).parent().click();
                isMatch = true;
            }
        });
        if (!isMatch) {
            alert("Không tìm thấy số điện thoại.");
        }
        return false;
    }

    return true;
}

/*
* Mở file pdf
*/
function openPdfIframe(src) {
    var PDF = document.getElementById("iframePrint");
    PDF.src = src;
    PDF.onload = function () {
        PDF.focus();
        PDF.contentWindow.print();
        PDF.contentWindow.close();
    }
}

(function () {

    var beforePrint = function () {
        console.log('Functionality to run before printing.');
    };

    var afterPrint = function () {
        console.log('Functionality to run after printing');
    };

    if (window.matchMedia) {
        var mediaQueryList = window.matchMedia('print');
        mediaQueryList.addListener(function (mql) {
            if (mql.matches) {
                beforePrint();
            } else {
                afterPrint();
            }
        });
    }

    window.onbeforeprint = beforePrint;
    window.onafterprint = afterPrint;

}());

window.onafterprint = function () {
    console.log("Printing completed...");
}

/*
* Hàm set thanh timeline
*/
var timeLineLeftDefault = 486 - 160 - 25;
var cellsWidth = 278 * 52;
var date = new Date();
var strDate = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
var ttStart = (new Date(strDate + " 08:00:00").getTime() / 1000);
var ttFinish = (new Date(strDate + " 21:00:00").getTime() / 1000);
var ttTotal = Math.floor((ttFinish - ttStart) * 2);
var boardLeft = 0;
function setTimeLine() {
    var date = new Date();
    var ttNow = Math.floor(date.getTime() / 1000);
    var ttCurrent = ttNow - ttStart;
    var boardLeftNow = ttCurrent * cellsWidth / ttTotal;

    var leftPos = $('div.board-book').scrollLeft();
    //console.log(leftPos);
    $("div.board-book").animate({
        scrollLeft: boardLeftNow - timeLineLeftDefault
    }, 0);
}

/*
* Hàm check bill dùng hóa chất
*/
var hcArr = [];
function setHoaChat(This, itemValue) {
    console.log("test - hcArr");
    console.log(hcArr);
    console.log("test - itemValue");
    console.log(itemValue);
    var index = hcArr.indexOf(itemValue);
    if (This.prop("checked")) {
        if (index == -1) {
            hcArr.push(itemValue);
        }
    }
    else {
        hcArr.splice(index, 1);
    }
}

/*
* Hàm set khách chủ động book Stylist hay không
*/
function setBookStylist(This) {
    if (This.prop("checked")) {
        bill.IsAutoStylist = false;
    }
    else {
        bill.IsAutoStylist = true;
    }
    console.log('set auto : ' + bill);
    console.log('IsAutoStylist : ' + bill.IsAutoStylist);
}

function setBookSalon(This) {
    if (This.prop("checked")) {
        bill.IsBookAtSalon = true;
    }
    else {
        bill.IsBookAtSalon = false;
    }
    console.log('set auto : ' + bill);
    console.log('IsBookAtSalon : ' + bill.IsBookAtSalon);
}

/*
    * update booking (drag and drop book cell)
*/
function updateBooking_drop(customerPhone, bookingId, hourId, stylistId, olderHourdId) {
        $.ajax({
            type: "POST",
            url: "/GUI/FrontEnd/Service/TimeLine_Beta.aspx/updateBooking",
            data: '{customerPhone :"' + customerPhone + '", bookingId:' + bookingId + ', stylistId: ' + stylistId + ', hourId: ' + hourId + ', olderHourdId: ' + olderHourdId + '}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                finishLoading();
                window.location.reload();
            },
            failure: function (response) { alert(response.d); }
        });
   
}

function updateBooking_drop(customerPhone, bookingId, hourId, stylistId, olderHourdId) {
    $.ajax({
        type: "POST",
        url: "/GUI/FrontEnd/Service/TimeLine_Beta.aspx/updateBooking",
        data: '{customerPhone :"' + customerPhone + '", bookingId:' + bookingId + ', stylistId: ' + stylistId + ', hourId: ' + hourId + ', olderHourdId: ' + olderHourdId + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            finishLoading();
            window.location.reload();
        },
        failure: function (response) { alert(response.d); }
    });
   
}

/*
* Lear date()


var date = new Date();
var strDate = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();

strDate = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
var ttNow = Math.floor(date.getTime() / 1000);
var ttStart = Math.floor(new Date(strDate + " 08:00:00").getTime() / 1000);
var ttFinish = Math.floor(new Date(strDate + " 22:00:00").getTime() / 1000);
var ttTotal = ttFinish - ttStart;
var ttCurrent = ttFinish - ttNow;
var cellsWidth = 140 * 27;
console.log(ttFinish + " - " + ttNow);

var boardStyleLeft = ttCurrent * cellsWidth / ttTotal;

console.log(boardStyleLeft);
*/