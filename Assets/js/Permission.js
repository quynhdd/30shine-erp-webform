﻿var permisionAction;
var ActionId;
var jsonListPermAction;

// Get list Salon
function GetListPermission() {
    $.ajax({
        url: "/GUI/BackEnd/Permission/Edit_Roles_V4.aspx/GetListPermAction",
        type: "POST",
        contentType: "application/JSON; charset:UTF-8",
        dataType: "json",
        success: function (data) {
            jsonListPermAction = JSON.parse(data.d);
            for (var i = 0; i < jsonListPermAction.length; i++) {
                var option = "<option value='" + jsonListPermAction[i].Id + "'>" + jsonListPermAction[i].Name + "</option>";

                $("#ddlPermissionAction").append(option);
            }
            $("#ddlPermissionAction").multiselect({
                includeSelectAllOption: true
            })
        },
    });
}

function GetListMapApi(pageId, permissionId) {
    $.ajax({
        url: "/GUI/BackEnd/Permission/Edit_Roles_V4.aspx/GetListMapApi",
        type: "POST",
        contentType: "application/JSON; charset:UTF-8",
        dataType: "json",
        data: '{pageId:' + pageId + ', permissionId:' + permissionId+'}',
        success: function (data) {
            var jsonListMapApi = JSON.parse(data.d);
            //for (var i = 0; i < jsonListPermAction.length; i++) {
            //    var option = "<option value='" + jsonListPermAction[i].Id + "'>" + jsonListPermAction[i].Name + "</option>";

            //    $("#ddlPermissionAction").append(option);
            //}
            //$("#ddlPermissionAction").multiselect({
            //    includeSelectAllOption: true
            //})
            debugger
            $("#table-map-detail tbody").html("");

            if (jsonListMapApi != null && jsonListMapApi.length > 0) {
                //

                //
                var index = 1;
                $.each(jsonListMapApi, function (i, v) {
                    //
                    var tr = '<tr>' +
                        '<td>' + index++ + '</td>' +
                        '<td class="td-product-code maSPcol" data-id="' + v.Id + '">' + v.Action + '</td>' +
                        '<td class="td-bar-code">' + v.ApiPath + '</td>' +
                        '<td class="td-product-name">' + v.ApiMethod + '</td>' +
                        '<td>' +
                        '<a class="elm del-btn" onclick="delMapApi($(this).parent().parent(),' + v.Id + ',\'' + v.ApiPath + '-' + v.ApiMethod + '\')" href = "javascript://" title = "Xóa" ></a >' +
                        '</td>' +
                        '</tr>';
                    $("#table-map-detail tbody").append($(tr));
                    debugger
                });
            }
        },
    });
}


function bindMenuApi(menuId, name, link, permissionId) {
    $("#IDPage").val(menuId);
    $("#NamePage").val(name);
    $("#LinkPage").val(link);
    $("#Hidden_PermissionId").val(permissionId);
}

function bindMenu(menuId, name, link) {
    $("#ID").val(menuId);
    $("#Name").val(name);
    $("#Link").val(link);
    //$.ajax({
    //    url: "/GUI/BackEnd/Permission/Edit_Roles_V4.aspx/getNameById",
    //    type: "POST",
    //    contentType: "application/JSON; charset:UTF-8",
    //    dataType: "json",
    //    data: '{id:' + menuId + '}',
    //    success: function (data) {
    //        var jsonData = JSON.parse(data.d);
    //        if (jsonData != null) {
    //            $("#ID").val(jsonData.Id);
    //            $("#Name").val(jsonData.Name);
    //            $("#Link").val(jsonData.Link);
    //        }
    //    }
    //});
}

//get list id salon
function getListIdPermission() {
    var Ids = [];
    var prd = {};
    $(".multiselect-native-select input:checked").each(function () {
        prd = {};
        var actionId = $(this).val();
        if (actionId != null && actionId > 0) {
            actionId = actionId.toString().trim() != "" ? parseInt(actionId) : 0;
            prd.actionId = actionId;
            Ids.push(prd);
        }
    });

    ActionId = Ids;
}

///get salonId bind to dropdowlistcheckbox
function bindPermissonAction(menuId, permissionId) {
    $.ajax({
        url: "/GUI/BackEnd/Permission/Edit_Roles_V4.aspx/GetListActionId",
        type: "POST",
        contentType: "application/JSON; charset:UTF-8",
        dataType: "json",
        data: '{menuId:' + menuId + ', permissionId: ' + permissionId + '}',
        success: function (data) {
            var jsonData = JSON.parse(data.d);
            var count = 0;
            var str = "";
            for (var i = 0; i < jsonData.length; i++) {
                $(".multiselect-native-select").find("input[value='" + jsonData[i].ActionId + "']").prop("checked", true);
                str += "" + jsonData[i].Name + "" + ", ";
                count++;
            }
            if (count < 4) {
                $(".multiselect-selected-text").html(str);

            }
            else {
                $(".multiselect-selected-text").html(count + " selected");
            }

        }
    });
}



//Update
function update() {
    getListIdPermission();
    var menuId = $("#ID").val();
    menuId = menuId.toString().trim() != "" ? parseInt(menuId) : 0;
    if (menuId >= 0) {
        var data = JSON.stringify({ menuId: menuId, Action: ActionId });
        $.ajax({
            url: "/GUI/BackEnd/Permission/Edit_Roles_V4.aspx/Update",
            data: data,
            type: "POST",
            contentType: "application/JSON; charset:UTF-8",
            dataType: "JSON",
            success: function (data) {
                if (data.d.success == true) {
                    alert("Cập nhật thành công!");
                    removeLoading();
                }
            },
            error: function (xhr) {
                console.log(xhr)
                alert('Có lỗi xảy ra! Xin vui lòng liên hệ với nhóm phát triển!');
            }
        });
    }
}

function addApi() {
    debugger
    //
    var menuIdTmp = $("#IDPage").val();
    menuIdTmp = menuIdTmp.toString().trim() != "" ? parseInt(menuIdTmp) : 0;
    var actionIdTmp = $("#actionList").val();
    actionIdTmp = actionIdTmp.toString().trim() != "" ? parseInt(actionIdTmp) : 0;
    var apiIdTmp = $("#apiList").val();
    apiIdTmp = apiIdTmp.toString().trim() != "" ? parseInt(apiIdTmp) : 0;
    var permissionIdTmp = $("#Hidden_PermissionId").val();
    permissionIdTmp = permissionIdTmp.toString().trim() != "" ? parseInt(permissionIdTmp) : 0;
    //
    if (actionIdTmp == null || actionIdTmp == 0) {
        alert("Chưa chọn action!");
        return;
    }
    if (apiIdTmp == null || apiIdTmp == 0) {
        alert("Chưa chọn api!");
        return;
    }

    if (menuId >= 0) {
        var data = JSON.stringify({ menuId: menuIdTmp, actionId: actionIdTmp, apiId: apiIdTmp, permissionId: permissionIdTmp });
        $.ajax({
            url: "/GUI/BackEnd/Permission/Edit_Roles_V4.aspx/AddApi",
            data: data,
            type: "POST",
            contentType: "application/JSON; charset:UTF-8",
            dataType: "JSON",
            success: function (data) {
                if (data.d.success == true) {
                    alert("Thêm mới thành công!");
                    removeLoading();
                    GetListMapApi(menuId, permissionIdTmp);
                } else if (data.d.message == 'duplicate') {
                    alert("Dữ liệu đã có trên hệ thống!");
                    removeLoading();
                }
            },
            error: function (xhr) {
                console.log(xhr)
                alert('Có lỗi xảy ra! Xin vui lòng liên hệ với nhóm phát triển!');
            }
        });
    }
}

function delMapApi(This, id, apiName) {
    debugger
    var tmp = confirm("Bạn có chắc chắn muốn xóa " + apiName + " ?");

    if (tmp) {
        var data = JSON.stringify({ mapId: id });
        $.ajax({
            url: "/GUI/BackEnd/Permission/Edit_Roles_V4.aspx/DeleteApi",
            data: data,
            type: "POST",
            contentType: "application/JSON; charset:UTF-8",
            dataType: "JSON",
            success: function (data) {
                if (data.d.success == true) {
                    alert("Xóa thành công!");
                    This.remove();
                    removeLoading();
                }
            },
            error: function (xhr) {
                console.log(xhr)
                alert('Có lỗi xảy ra! Xin vui lòng liên hệ với nhóm phát triển!');
            }
        });
    }
    //
    //var test = {
    //    //func khi chon co
    //    funcYes: function (instance, toast) {
    //        debugger
    //        var data = JSON.stringify({ mapId: id});
    //        $.ajax({
    //            url: "/GUI/BackEnd/Permission/Edit_Roles_V4.aspx/DeleteApi",
    //            data: data,
    //            type: "POST",
    //            contentType: "application/JSON; charset:UTF-8",
    //            dataType: "JSON",
    //            success: function (data) {
    //                if (data.d.success == true) {
    //                    alert("Xóa thành công!");
    //                    This.remove();
    //                    removeLoading();
    //                }
    //            },
    //            error: function (xhr) {
    //                console.log(xhr)
    //                alert('Có lỗi xảy ra! Xin vui lòng liên hệ với nhóm phát triển!');
    //            }
    //        });
    //        return;
    //    },
    //    //func khi chon khong
    //    funcNo: function (instance, toast) {
    //        return;
    //    }
    //};
    //ShowConfirm("confirm", 100000, "Xác nhận", "Bạn có chắc chắn muốn xóa " + apiName+" ?", "Có", "Không", test);
}


