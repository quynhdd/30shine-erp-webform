﻿//=================================================================
// Library Functions
//=================================================================

//============================================
// Execute Paging Update Panel UPTableListing
//============================================
function excPaging(page) {
    $("#HDF_Page").val(page);
    $("#BtnFakeUP").click();
    addLoading();
}
function addLoading() {
    var loadingDom = "<div class='loading-frame'></div>";
    $("html, body").append($(loadingDom));
}

function removeLoading() {
    if ($(".loading-frame").length > 0) {
        $(".loading-frame").remove();
    }
}

function SelectDropdownlist() {
    $('.select').select2();
}

function excExcel(type) {
    var type = type || 'Export';
    $("#HDF_ExcelType").val(type);
    $("#BtnFakeExcel").click();
}

function EntryExportExcel(param) {
    $("#HDF_ExcelType").val(param);
    $("#HDF_EXCELPage").val(1);
    $("#HDF_EXCELSegment").val(1000000000);
    $("#BtnFakeExcel").click();
}

function EntryExportExcelForMisa() {
    $("#BtnFakeExcelForMisa").click();
}

function CallBackExportExcel(url) {
    window.open(url, "_blank");
}

function ShowUlOptSegment(THIS) {
    var UlOptSegment = THIS.parent().find(".ul-opt-segment"),
        OPTSegment = $("#OPTSegment"),
        HDF_OPTSegment = $("#HDF_OPTSegment");
    UlOptSegment.toggle();
    UlOptSegment.find(">li").bind("click", function () {
        var segment = $(this).attr("data-value");
        var _segment = segment == 1000000 ? "Tất cả" : segment;
        HDF_OPTSegment.val(segment);
        OPTSegment.text(_segment);
        UlOptSegment.hide();
        excPaging(1);
    });
}

//============================
// Price Format
//============================   

function FormatPrice(price) {
    var set = 3,
        symbol = ".",
        tmp = "",
        price = price.toString().trim().replace(/\./g, '');
    if (price != "" && price.match(/[^0-9]+/) == null) {
        var loop = price.length;
        for (var i = 1; i <= loop; i++) {
            tmp = price[loop - i] + tmp;
            if (i < loop && i % set == 0) {
                tmp = symbol + tmp;
            }
        }
    } else {
        tmp = price;
    }
    return tmp;
}

//============================
// Price Number
//============================   

function FormatNumber(Number) {
    var set = 3,
        symbol = ",",
        tmp = "",
        Number = Number.toString().trim().replace(/\./g, '');
    if (Number != "" && Number.match(/[^0-9]+/) == null) {
        var loop = Number.length;
        for (var i = 1; i <= loop; i++) {
            tmp = Number[loop - i] + tmp;
            if (i < loop && i % set == 0) {
                tmp = symbol + tmp;
            }
        }
    } else {
        tmp = Number;
    }
    return tmp;
}
// Function callback after update panel UPTableListing
function UP_FormatPrice(domClassId) {
    $(domClassId).each(function () {
        $(this).text(FormatPrice($(this).text()));
    });
}

//============================
// Get Query Strings
//============================ 
function getQueryStrings() {
    var assoc = {};
    var decode = function (s) { return decodeURIComponent(s.replace(/\+/g, " ")); };
    var queryString = location.search.substring(1);
    var keyValues = queryString.split('&');

    for (var i in keyValues) {
        var key = keyValues[i].split('=');
        if (key.length > 1) {
            assoc[decode(key[0])] = decode(key[1]);
        }
    }

    return assoc;
}

//============================
// Show Message System
//============================ 
function showMsgSystem(msg, status, duration) {
    var duration = duration || 5000;
    $("#MsgSystem").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);
    setTimeout(function () {
        $("#MsgSystem").fadeTo("slow", 0, function () {
            $("#MsgSystem").text("").attr("class", "login-msg").css("opacity", 1);
        });
    }, duration);
}

//=========================================
// Set Active table.table-listing
//=========================================
function SetActiveTableListing() {
    $("table.table-listing tr").bind("click", function () {
        $("table.table-listing tr.active").removeClass("active");
        $(this).addClass("active");
    });
}

//=========================================
// Set Active Sub Menu
//=========================================
function SetActiveSubMenu() {
    var Pathname = window.location.pathname;
    $("ul#subMenu li").removeClass("active");
    $("ul#subMenu li a").each(function () {
        var href = $(this).attr("href");
        if (href == Pathname) {
            $(this).parent().addClass("active");
        }
    });
}

//=========================================
// Fix thumbnail upload images plugin
//=========================================
function excThumbWidth(ImgStandardWidth, ImgStandardHeight) {
    var width = ImgStandardWidth,
        height, left, top;
    $(".thumb-wp .thumb").each(function () {
        height = ImgStandardWidth * $(this).height() / $(this).width();
        left = 0;
        top = (ImgStandardHeight - height) / 2;
        $(this).css({ "width": width, "height": height, "left": left });
    });
}

//=========================================
// Capitalize text
//=========================================
function capitalizeText(text) {
    //split the value of this input by the spaces
    var split = text.split(' ');

    //iterate through each of the "words" and capitalize them
    for (var i = 0, len = split.length; i < len; i++) {
        split[i] = split[i].charAt(0).toUpperCase() + split[i].slice(1);
    }

    //re-join the string and set the value of the element
    text = split.join(' ');
    return text;
}

//=========================================
// Copy text to clipboard
//=========================================
function copyToClipboard(elem) {
    // create hidden text element, if it doesn't already exist
    var targetId = "_hiddenCopyText_";
    var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
    var origSelectionStart, origSelectionEnd;
    if (isInput) {
        // can just use the original source element for the selection and copy
        target = elem;
        origSelectionStart = elem.selectionStart;
        origSelectionEnd = elem.selectionEnd;
    } else {
        // must use a temporary form element for the selection and copy
        target = document.getElementById(targetId);
        if (!target) {
            var target = document.createElement("textarea");
            target.style.position = "absolute";
            target.style.left = "-9999px";
            target.style.top = "0";
            target.id = targetId;
            document.body.appendChild(target);
        }
        target.textContent = elem.textContent;
    }
    // select the content
    var currentFocus = document.activeElement;
    target.focus();
    target.setSelectionRange(0, target.value.length);

    // copy the selection
    var succeed;
    try {
        succeed = document.execCommand("copy");
    } catch (e) {
        succeed = false;
    }
    // restore original focus
    if (currentFocus && typeof currentFocus.focus === "function") {
        currentFocus.focus();
    }

    if (isInput) {
        // restore prior selection
        elem.setSelectionRange(origSelectionStart, origSelectionEnd);
    } else {
        // clear temporary content
        target.textContent = "";
    }
    return succeed;
}

//=========================================
// Select text
//=========================================
function SelectText(element) {
    //var text = document.getElementById(element);
    if ($.browser.msie) {
        var range = document.body.createTextRange();
        range.moveToElementText(element);
        range.select();
    } else if ($.browser.mozilla || $.browser.opera) {
        var selection = window.getSelection();
        var range = document.createRange();
        range.selectNodeContents(element);
        selection.removeAllRanges();
        selection.addRange(range);
    } else if ($.browser.safari) {
        var selection = window.getSelection();
        selection.setBaseAndExtent(element, 0, element, 1);
    }
}

//=========================================
// onkeydown number : 0 -> 9
//=========================================
var funcKeycode = [
    8,  // Backspace
    27, // Esc
    112, // F1
    113, // F2
    114, // F3
    115, // F4
    116, // F5
    117, // F6
    118, // F7
    119, // F8
    120, // F9
    121, // F10
    122, // F11
    123, // F12
    145, // Scroll Lock
    19,  // Pause Break
    45,  // Insert
    46,  // Delete
    36,  // Home
    35,  // End
    33,  // Page Up
    34,  // Page Down
    38,  // Arrow Up
    40,  // Arrow Down
    37,  // Arrow Left
    39,  // Arrow Right
    17,  // Control
    18,  // Alt
    16,  // Shift
    91,  // Window
    93,  // Page (= right click)
    20,  // Caps Lock
    9,   // Tab
    144,  // Num Lock
    //96,   // Số 0 bên ô bàn phìm số (sử dụng numlock)
    //97,   // 1
    //98,   // 2
    //99,   // 3
    //100,  // 4
    //101,  // 5
    //102,  // 6
    //103,  // 7
    //104,  // 8
    //105   // 9
];
var typingAllowFuncKeycode = [
    8,  // Backspace
    27, // Esc    
    46,  // Delete
    45,  // Insert
    37,  // Arrow Left
    39,  // Arrow Right
    38,  // Arrow Up
    40,  // Arrow Down
    20,  // Caps Lock
    9,   // Tab
    144  // Num Lock
];

function checkTypingNumber(e) {
    if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || isFuncKeyCode(e)) {
        return true;
    } else {
        return false;
    }
}

function isAllowTyping(e) {
    if (typingAllowFuncKeycode.indexOf(e.keyCode) != -1) {
        return true;
    } else {
        return false;
    }
}

function isFuncKeyCode(e) {
    if (funcKeycode.indexOf(e.keyCode) != -1) {
        return true;
    } else {
        return false;
    }
}

function checkStringNumber(string) {
    string = string.replace(/\s+/, string);
    var ascciCode;
    var isNumber = true;
    if (string != "") {
        if (string[0] != "0") {
            isNumber = false;
            return isNumber;
        }
        for (var i = 0; i < string.length; i++) {
            ascciCode = string.charCodeAt(i);
            if (ascciCode < 48 || ascciCode > 57) {
                isNumber = false;
            }
        }
    } else {
        isNumber = false;
    }
    return isNumber;
}

function checkNumber(string) {
    string = string.replace(/\s+/, string);
    var ascciCode;
    var isNumber = true;
    if (string != "") {
        for (var i = 0; i < string.length; i++) {
            ascciCode = string.charCodeAt(i);
            if (ascciCode < 48 || ascciCode > 57) {
                isNumber = false;
            }
        }
    } else {
        isNumber = false;
    }
    return isNumber;
}

function checkPhoneLength(string, arrLen) {
    string = string.replace(/\s+/, string);
    if (string != "" && arrLen.length > 0) {
        if (arrLen.indexOf(string.length) == -1) {
            return false;
        }
        else {
            return true;
        }
    } else {
        return false;
    }
}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function ConvertDateToTypeMMddyyyy(input) {
    if (typeof input.getMonth === "function") {
        var dd = input.getDate();
        if (dd <= 9)
            dd = '0' + dd;
        var MM = input.getMonth() + 1;
        if (MM <= 9)
            MM = '0' + MM;
        var yyyy = input.getFullYear();
        var newdate = MM + '-' + dd + '-' + yyyy;
        return newdate;
    }
    else {
        var datearray = input.split("/");

        var newdate = datearray[1] + '-' + datearray[0] + '-' + datearray[2];
        return newdate;
    }
}

function ConvertJsonDateToStringFormat(jsonDate) {
    //var newDate = dateFormat(jsonDate, "dd/MM/yyyy");
    var date = new Date(parseInt(jsonDate.substr(6)));
    var dd = date.getDate();
    if (dd <= 9)
        dd = '0' + dd.toString();
    var MM = date.getMonth() + 1; // Tháng trong js bắt đầu từ 0->11
    if (MM <= 9)
        MM = '0' + MM.toString();
    var yyyy = date.getFullYear();

    return dd + '/' + MM + '/' + yyyy;
}

function ConvertJsonDateToStringDateTime(jsonDate) {
    //var newDate = dateFormat(jsonDate, "dd/MM/yyyy");
    var date = new Date(parseInt(jsonDate.substr(6)));
    var dd = date.getDate();
    if (dd <= 9)
        dd = '0' + dd.toString();
    var MM = date.getMonth() + 1; // Tháng trong js bắt đầu từ 0->11
    if (MM <= 9)
        MM = '0' + MM.toString();
    var yyyy = date.getFullYear();
    var hh = ((date.getHours() < 10 && date.getHours() >= 0) ? "0" : "") + date.getHours();
    var mm = ((date.getMinutes() < 10 && date.getMinutes() >= 0) ? "0" : "") + date.getMinutes();
    var ss = ((date.getSeconds() < 10 && date.getSeconds() >= 0) ? "0" : "") + date.getSeconds();
    return dd + '-' + MM + '-' + yyyy + ' ' + hh + ':' + mm + ':' + ss;
}


function ConvertDateToTypeDateTimeDefault(input) {
    if (typeof input.getMonth === "function") {
        var dd = input.getDate();
        if (dd <= 9)
            dd = '0' + dd;
        var MM = input.getMonth() + 1;
        if (MM <= 9)
            MM = '0' + MM;
        var yyyy = input.getFullYear();
        var hh = input.getHours();
        var mm = input.getMinutes();
        var newdate = MM + '/' + dd + '/' + yyyy + ' ' + hh + ':' + mm;
        return newdate;
    }
}

function ConvertJsonDateToStringMMddyyyy(jsonDate) {
    //var newDate = dateFormat(jsonDate, "dd/MM/yyyy");
    var date = new Date(parseInt(jsonDate.substr(6)));
    var dd = date.getDate();
    if (dd <= 9)
        dd = '0' + dd.toString();
    var MM = date.getMonth() + 1; // Tháng trong js bắt đầu từ 0->11
    if (MM <= 9)
        MM = '0' + MM.toString();
    var yyyy = date.getFullYear();
    return MM + '/' + dd + '/' + yyyy;
}

function ConvertJsonTime(jsonDate) {
    //var newDate = dateFormat(jsonDate, "dd/MM/yyyy");
    var date = new Date(parseInt(jsonDate.substr(6)));
    //var dd = date.getDate();
    //if (dd <= 9)
    //    dd = '0' + dd.toString();
    //var MM = date.getMonth() + 1; // Tháng trong js bắt đầu từ 0->11
    //if (MM <= 9)
    //    MM = '0' + MM.toString();
    //var yyyy = date.getFullYear();
    var hh = date.getHours();
    var mm = date.getMinutes();
    var ss = date.getSeconds();
    return hh + ':' + mm + ':' + ss;
}

/*
* Mở layer loading
*/
function startLoading() {
    $(".page-loading").fadeIn();
}

/*
* Đóng layer loading
*/
function finishLoading() {
    $(".page-loading").fadeOut();
}

/*
*  Succcess Messenger alert
*/
function displayMessageSuccess(message) {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-center",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "2000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    Command: toastr["success"](message);
}
/*
*  Error Messenger alert
*/
function displayMessageError(message) {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-center",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    Command: toastr["error"](message);
}

/*
*  Warning Messenger alert
*/
function displayMessageWarning(message) {
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-center",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    Command: toastr["warning"](message);
}

/*
* Disable, enable scroll
*/
// left: 37, up: 38, right: 39, down: 40,
// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
var keys = { 37: 1, 38: 1, 39: 1, 40: 1 };

function preventDefault(e) {
    e = e || window.event;
    if (e.preventDefault)
        e.preventDefault();
    e.returnValue = false;
}

function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}

function disableScroll() {
    if (window.addEventListener) // older FF
        window.addEventListener('DOMMouseScroll', preventDefault, false);
    window.onwheel = preventDefault; // modern standard
    window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
    window.ontouchmove = preventDefault; // mobile
    document.onkeydown = preventDefaultForScrollKeys;
}

function enableScroll() {
    if (window.removeEventListener)
        window.removeEventListener('DOMMouseScroll', preventDefault, false);
    window.onmousewheel = document.onmousewheel = null;
    window.onwheel = null;
    window.ontouchmove = null;
    document.onkeydown = null;
}

/*
* Toggle full screen
*/
function toggleFullScreen() {
    if (!document.fullscreenElement) {
        document.documentElement.requestFullscreen();
    } else {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        }
    }
}
// datediff format dd/mm/yyyy
function funcDatediff(startDate, endDate) {
    try {
        // test
        var msMinute = 60 * 1000;
        var msDay = 60 * 60 * 24 * 1000;
        var statarr = startDate.split('/');
        var endarr = endDate.split('/');
        var dateStart = new Date(statarr[1] + '/' + statarr[0] + '/' + statarr[2]).getTime();
        var dateEnd = new Date(endarr[1] + '/' + endarr[0] + '/' + endarr[2]).getTime();
        var dateDiff = parseInt(dateEnd - dateStart) / msDay;
        return dateDiff;
        // end test
    } catch (e) {
        alert("Lỗi : " + e.message);
    }


};
// export file exel content html
function funcExportExcelHtml(idDiv, nameFile) {
    let file = new Blob([$(idDiv).html()], { type: "application/vnd.ms-excel" });

    let url = URL.createObjectURL(file);

    let a = $("<a />", {
        href: url,
        download: "" + nameFile + ".xls"
    })
        .appendTo("body")
        .get(0)
        .click();
}
// Format price
function formatPrice(price) {
    var pricef = price.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    return pricef;
};

//upload only img 

function singleUploadImg(data, uri) {
    $.ajax({
        type: "POST",
        url: uri,
        //async: false,
        data: JSON.stringify(data),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        crossDomain: true,
        data: JSON.stringify(data),
        success: function (response, textStatus, xhr) {
            if (response) {
                return response;
            }
        },
        error: function (jqXhr, textStatus, errorMessager) {
            console.log(errorMessager + "\n" + jqXhr.responseJSON);
            alert("upload images lên S3 bị lỗi. Bạn hãy thông báo với Admin");
            return;
        }
    });
};
//================================================================
// Common JQuery Execute
//================================================================
jQuery(document).ready(function () {
    //============ 
    // Set active for table.table-listing tr when click
    //============
    SetActiveTableListing();

    //=========================================
    // Set Active Sub Menu
    //=========================================
    SetActiveSubMenu();
});
// function show message with type 1: info, 2: success, 3: warning, 4: error
function ShowMessage(_title, msg, type, timeout = 3000) {
    iziToast.settings({
        timeout: timeout
    });

    switch (type) {
        case 1:
            iziToast.info({
                title: _title,
                message: msg
            });
            break;
        case 2:
            iziToast.success({
                title: _title,
                message: msg
            });
            break;
        case 3:
            iziToast.warning({
                title: _title,
                message: msg
            });
            break;
        case 4:
            iziToast.error({
                title: _title,
                message: msg
            });
            break;
        default:
            break;
    }
}
//=========================================
// function show confirm yes/no
function ShowConfirm(id, timeout, _title, message, yesButton, noButton, obj) {
    iziToast.question({
        timeout: timeout,
        close: false,
        overlay: true,
        displayMode: 'once',
        id: id,
        titleSize: '18px',
        zindex: 999,
        title: _title,
        message: message,
        messageSize: '20px',
        position: 'center',
        buttons: [
            ['<button wfd-id="229" style="font-size : 20px;"><b  style="font-size : 20px;">' + yesButton + '</b></button>', function (instance, toast) {

                instance.hide({ transitionOut: 'fadeOut' }, toast, 'yes');
                obj.funcYes.call(obj, instance, toast);

            }, true],
            ['<button wfd-id="228" style="font-size : 20px;">' + noButton + '</button>', function (instance, toast) {

                instance.hide({ transitionOut: 'fadeOut' }, toast, 'no');
                obj.funcNo.call(obj, instance, toast);
            }],
        ]
        //,
        //onClosing: function (instance, toast, closedBy) {            
        //    funcOnClosing.call(instance, toast, closedBy);
        //},
        //onClosed: function (instance, toast, closedBy) {
        //    console.info('Closing | closedBy: ' + closedBy);
        //    funcOnClosed.call(instance, toast, closedBy);
        //}
    });

};
//=========================================

//export excel
function exportExcel(fileName, sheetTitle, headers, body) {
    // Default font
    var excel = $JExcel.new("Calibri light 10 #333333");
    fileName = (fileName + "").trim();
    // excel.set is the main function to generate content:
    // We can use parameter notation excel.set(sheetValue,columnValue,rowValue,cellValue,styleValue)
    // Or object notation excel.set({sheet:sheetValue,column:columnValue,row:rowValue,value:cellValue,style:styleValue })
    // null or 0 are used as default values for undefined entries
    excel.set({ sheet: 0, value: sheetTitle });
    excel.addSheet("Sheet 2");
    // Borders are LEFT,RIGHT,TOP,BOTTOM. Check $JExcel.borderStyles for a list of valid border styles
    var evenRow = excel.addStyle({
        border: "thin,thin,thin,thin #333333"
    });
    // Style for odd ROWS
    // Background color, plain #RRGGBB, there is a helper $JExcel.rgbToHex(r,g,b)
    var oddRow = excel.addStyle({
        fill: "#ECECEC",
        border: "thin,thin,thin,thin #333333"
    });
    for (var i = 1; i < body.length; i++) excel.set({ row: i, style: i % 2 == 0 ? evenRow : oddRow });
    // Format for headers
    // Border for header
    var formatHeader = excel.addStyle({
        border: "thin,thin,thin,thin #333333",
        font: "Calibri 12 #0000AA B"
    });
    // Loop all the haders
    for (var i = 0; i < headers.length; i++) {
        excel.set(0, i, 0, headers[i], formatHeader);
        excel.set(0, i, undefined, "auto");
    }
    for (var i = 0; i < body.length; i++) {
        setDataRow(body[i], i + 1, excel);
    }
    if (fileName.substr(-5, 5) !== ".xlsx")
        fileName += ".xlsx"
    excel.generate(fileName);
}

// Loop all the haders
function setDataRow(data, rowIndex, excel) {
    for (var i = 0; i < data.length; i++) {
        excel.set(0, i, rowIndex, data[i]);
    }
}

