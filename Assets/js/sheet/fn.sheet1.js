﻿+(function ($) {
    "use strict";
    if (typeof XLSX === 'undefined' && typeof require !== 'undefined') XLSX = require('xlsx');

    var _ExcelHelper = function () {
        this.options = {};
        this.DataFromExcel = [];
        this.workBookSample = {};
        this.reader = new FileReader();
        this.init();
    }

    _ExcelHelper.prototype = {
        init: function (options) {
            var options = typeof options === 'object' ? options : {};
            this.options = options;

            document.getElementById(this.options.selector);

            return this;
        },

        ToWorkSheetData: function (data, dataStruct, ignoredColumns) {
            var ws_data = [];
            var ws_header = [];
            var regex = new RegExp('^\\d+$');

            $.each(data, function (index, item) {
                var record = [];
                var cols = dataStruct;

                try {
                    for (var i in cols) {
                        if (typeof item[i] != "undefined") {
                            cols[i] = item[i];
                        }
                    }

                    for (var i in cols) {
                        if ($.inArray(i, ignoredColumns) !== -1) {
                            continue;
                        }
                        if (index == 0) {
                            ws_header.push(i);
                        }
                        if (regex.test(cols[i])) {
                            cols[i] = parseInt(cols[i]);
                        }
                        record.push(cols[i]);
                    }
                    ws_data.push(record);
                }
                catch (ex) { }
            });
            ws_data.unshift(ws_header);
            return ws_data;
        },

        HandleDrop: function (event) {
            event.stopPropagation(); event.preventDefault();
            var _this = this;
            var File;
            var data = event.dataTransfer;
            if (data.items) {
                File = data.items[0].getAsFile();
            } else {
                File = data.files[0];
            }

            // Collect data from excel
            _this.CollectData(File);

            if (typeof _this.options.onHandleDrop === 'function') {
                _this.options.onHandleDrop.call(event, File);
            }
        },

        HandleInput: function (event) {
            var File = e.target.files[0];

            if (typeof this.options.onHandleInput === 'function') {
                this.options.onHandleInput.call(event, File);
            }
        },

        CollectData: function (File) {
            var _this = this;
            _this.DataFromExcel = [];

            try {
                this.ValidateFile(File);
            }
            catch (Ex) {
                alert(Ex.message);
                return;
            }

            var xlsxFlag = false; /*Flag for checking whether excel is .xls format or .xlsx format*/
            if (File.name.toLowerCase().indexOf(".xlsx") > 0) {
                xlsxFlag = true;
            }

            _this.reader.onload = function (e) {
                var data = e.target.result;
                if (xlsxFlag) {
                    var workbook = XLSX.read(data, { type: 'binary' });
                }
                else {
                    data = new Uint8Array(data)
                    var workbook = XLS.read(data, { type: 'array' });
                }

                _this.workBookSample = Object.assign({}, workbook);
                _this.workBookSample.SheetNames = [];
                _this.workBookSample.Sheets = {};
                _this.workBookSample.Strings = [];
                _this.workBookSample.WorkBook = {};

                var sheetNames = workbook.SheetNames;
                var cnt = 0;
                sheetNames.forEach(function (y) { /*Iterate through all sheets*/
                    try {
                        /*Convert the cell value to Json*/
                        if (xlsxFlag) {
                            var objExcel = XLSX.utils.sheet_to_json(workbook.Sheets[y]);
                            for (var i in objExcel) {
                                _this.DataFromExcel.push(objExcel[i]);
                            }
                        }
                        else {
                            var objExcel = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[y]);
                            for (var i in objExcel) {
                                _this.DataFromExcel.push(objExcel[i]);
                            }
                        }
                        if (_this.DataFromExcel.length > 0 && cnt == 0) {
                            cnt++;
                        }
                    }
                    catch (ex) { }
                });

                if (typeof _this.options.onDataCollected === 'function') {
                    _this.options.onDataCollected.call(null, File, _this.DataFromExcel);
                }
            }

            if (xlsxFlag) {
                _this.reader.readAsBinaryString(File);
            }
            else {
                _this.reader.readAsArrayBuffer(File);
            }
        },

        ValidateFile: function (File) {
            var regex = /^.*?(.xlsx|.xls)$/;
            if (typeof File == "undefined" || !regex.test(File.name.toLowerCase())) {
                $('#FileName').empty().text('');
                $('#UpdateStaffBtn').prop('disabled', true);
                throw new Error("Vui lòng chọn File Excel trước khi cập nhật!");
            }

            /*Checks whether the browser supports HTML5*/
            if (typeof (FileReader) == "undefined") {
                throw new Error("Trình duyệt không hỗ trợ upload file, \n Vui lòng liên hệ nhóm phát triển!");
            }
        },

        SaveToArrayBuffer: function (s) {
            if (typeof ArrayBuffer !== 'undefined') {
                var buf = new ArrayBuffer(s.length);
                var view = new Uint8Array(buf);
                for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
                return buf;
            } else {
                var buf = new Array(s.length);
                for (var i = 0; i != s.length; ++i) buf[i] = s.charCodeAt(i) & 0xFF;
                return buf;
            }
        },

        ExportToExcel: function (data, type, fn) {
            var wb = XLSX.utils.book_new();
            var ws = XLSX.utils.aoa_to_sheet(data, { /*cellDates: true*/ });

            if (typeof fn === 'function') {
                try {
                    fn.call(this, wb, ws);
                }
                catch (ex) { }
            }

            XLSX.utils.book_append_sheet(wb, ws, "Sheet");


            var wbout = XLSX.write(wb, { bookType: type, bookSST: true, type: 'binary' });
            var fname = this.options.DownloadFileName || 'WorkBook-' + this.UniqueId(8) + '.' + type;

            try {
                saveAs(new Blob([this.SaveToArrayBuffer(wbout)], { type: "application/octet-stream" }), fname);
            } catch (e) { if (typeof console != 'undefined') console.log(e, wbout); }
            return wbout;
        },

        UniqueId: function (length) {
            var d = new Date().getTime().toString(36).toUpperCase();
            var n = d.toString();
            var uniqueId = '';
            var length = parseInt(length > 0 ? length : 32);
            var p = 0;
            var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';

            for (var i = length; i > 0; --i) {
                uniqueId += ((i & 1) && n.charAt(p) ? n.charAt(p) : chars[Math.floor(Math.random() * chars.length)]);
                if (i & 1) p++;
            };

            return uniqueId;
        },

        Instance: function () {
            return new _ExcelHelper();
        },

        Listen: function (selector) {
            var _this = this,
                _selector = $(selector),
                inputFile;

            if (selector.tagName !== 'INPUT') {
                inputFile = _selector.find('input[type="file"]');
            }
            else {
                inputFile = _selector;
            }

            document.addEventListener("drop", function (event) {
                event.preventDefault();
                alert(1)
            }, false);

            inputFile.unbind('change').bind('change', function (e) {
                var File = e.target.files[0];
                // Collect data from excel
                _this.CollectData(File);
            });

            _selector.unbind('click').bind('click', function (e) {
                if (e.target !== inputFile.get(0)) {
                    //if (validatebooking()) {
                        inputFile.trigger('click');
                    //}
                }
            });

            document.getElementById('dropZoneExcel').addEventListener("drop", function (event) {
                _this.HandleDrop(event);
            }, false);

            document.getElementById('dropZoneExcel').addEventListener("dragover", function (event) {
                event.preventDefault();
            }, false);
            //if (validatebooking()) {
            if (typeof _this.options.ajax.trigger !== 'undefined') {
                var button = _this.options.ajax.trigger.substr(0, _this.options.ajax.trigger.lastIndexOf("."));
                var _event = _this.options.ajax.trigger.substr(_this.options.ajax.trigger.lastIndexOf(".") + 1);
                //if (validatebooking()) {
                    $(button).bind(_event, function (e) {
                        e.preventDefault();
                        //var CampaignID = query_ex('CampaignID', _this.options.ajax.url);
                        var CampaignID = $('#ddlCampaign :selected').val();
                        if (validatebooking()) {
                            $.ajax({
                                type: "POST",
                                url: _this.options.ajax.url,
                                data: JSON.stringify({ "CampaignID": CampaignID, "DataFromExcel": _this.DataFromExcel }),
                                // data: JSON.stringify({ "CampaignID": 1, "DataFromExcel": [_this.DataFromExcel] }),
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                beforeSend: function () {
                                    $(".page-loading").fadeIn(200);
                                },
                                complete: function () {
                                    $(".page-loading").fadeOut(200);
                                    // Disable submit button
                                    UpdateBtn.prop('disabled', true);
                                    // Reset input file
                                    inputFile.val('');
                                    $('#FileName').empty();
                                },
                                success: function (response, textStatus, jqXHR) {
                                    _this.options.ajax.success.call(_this, response, textStatus, jqXHR);
                                },
                                failure: function (response) {
                                    $(".page-loading").fadeOut(200);
                                    alert("Có lỗi xảy ra trong quá trình upload file\n Vui lòng thử lại hoặc liên hệ Nhóm phát triển!");
                                }
                            });
                        }
                    });
                //}
            
                }
            //}
        },

        ToCellName: function (r, c) {
            var cellName,
                character = "ABCDEFGHIJKLMNOPQRSTWXYZ"[c],
                number = parseInt(r) + 1;
            if (typeof character !== 'undefined') {
                cellName = character + number;
            }
            return cellName;
        }
    }

    window.ExcelHelper = new _ExcelHelper();

    $.fn.proccessExcel = function (options) {
        window.ExcelHelper.Instance().init(options).Listen(this.selector);
    };
})(jQuery);
function validatebooking() {
    var CampaignID = $('#ddlCampaign :selected').val();
    if (CampaignID == "0") {
        alert("Vui lòng chọn chiến dịch!");
        return false;
    }
    return true;
}

//function GetURLParameter(sParam, url) {
//    var sPageURL = url.split('?')[1];
//    var sURLVariables = sPageURL.split('&');
//    for (var i = 0; i < sURLVariables.length; i++) {
//        var sParameterName = sURLVariables[i].split('=');
//        if (sParameterName[0] == sParam) {
//            return sParameterName[1];
//            alert(sParameterName[1]);
//        }
//    }
//};
