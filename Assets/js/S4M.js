﻿
function fillDistrict(_province, DistrictId) {
    $.ajax({
        type: "POST",
        contentType: "application/json;charset:UTF-8",
        datatype: "JSON",
        url: "/GUI/BackEnd/S4M_Student/Student_Add.aspx/fillDistrict",
        data: '{id:' + _province + '}',
        success: function (response) {
            var jsonData = JSON.parse(response.d);
            var listItems = "";
            for (var i = 0; i < jsonData.length; i++) {
                listItems += "<option value='" + jsonData[i].ID + "'>" + jsonData[i].TenQuanHuyen + "</option>";
            }
            $("#District").html(listItems);
            $("#District").val(DistrictId);
        }
    });
}

// Click khong nhan ho so
function AddCancel(This) {
    //debugger;
    var kt = true;
    var error = 'Vui lòng nhập đầy đủ thông tin!';
    var name = $('#Name').val();
    var phone = $('#Phone').val();
    var note = $('#Note').val();
    var address = $('#Address').val();
    var city = $('#City :selected').val();
    var district = $('#District :selected').val();
    var level = $("input[name='levelConcern']:checked").val();
    if (name == "") {
        kt = false;
        error += " [Họ tên], ";
        $("#Name").css("border-color", "red");
    }
    if (phone == "") {
        kt = false;
        error += "[Số điện thoại], ";
        $("#Phone").css("border-color", "red");
    }
    if (level == undefined) {
        kt = false;
        error += "[Chọn mức độ quan tâm], ";
        $("#Phone").css("border-color", "red");
    }
    if (note == "") {
        kt = false;
        error += "[Ghi chú] ";
        $("#Note").css("border-color", "red");
    }
    if (!kt) {
        $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
            $("#success-alert").slideUp(2000);
        });
        $("#msg-alert").text(error);
    }
    else {
        $.ajax({
            type: "POST",
            contentType: "application/json;charset:UTF-8",
            datatype: "JSON",
            data: '{Name:"' + name + '",Phone:"' + phone + '", City:' + city + ', District:' + district + ', Address:"' + address + '", radiobttList:' + level + ', Note:"' + note + '" }',
            url: "/GUI/BackEnd/S4M_Student/Student_Add.aspx/AddOrUpdateCancel",
            success: function (response) {

                var mission = JSON.parse(response.d);
                if (mission.success) {
                    $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                        $("#success-alert").slideUp(2000);
                    });
                    $("#msg-alert").text("Hồ sơ học viên đã được lưu trữ!");
                    setTimeout(function () {
                        window.location.href = "/admin/s4m/hoc-vien/danh-sach.html";
                    }, 1500);
                }
                else {
                    $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                        $("#success-alert").slideUp(2000);
                    });
                    $("#msg-alert").text("Có lỗi xảy ra!Vui lòng liên hệ với nhóm phát triển!");
                }
            }
        });
    }
    return kt;
}

//Click hoàn tất nhập học
function AddOrUpdate(This) {
    getImage();
    var hocphi1;
    var kt = true;
    var error = 'Vui lòng kiểm tra lại thông tin!';
    var name = $('#Name').val();
    var phone = $('#Phone').val();
    var email = $('#Email').val();
    var hocphi = $('#HocPhi').val();
    hocphi1 = $('#hocphi2').val(); if (hocphi1 == "") { hocphi1 = "0"; hocphi1 = parseInt(hocphi1); }
    var cmtnumber = $('#CMT').val();
    var goihoc = $('#ddlGoiHoc :selected').val();
    var khoahoc = $('#ddlKhoahoc :selected').val();
    var cmtimage1 = $('#HDF_MainImgCMT1').val();
    var cmtimage2 = $('#HDF_MainImgCMT2').val();
    var note = $('#Note').val();
    var address = $('#Address').val();
    var city = $('#City :selected').val();
    var district = $('#District :selected').val();
    var level = $("input[name='levelConcern']:checked").val();
    var salonID = $('#Salon :selected').val();
    var hocphiThem1 = $('#HocphiThem1').val(); if (hocphiThem1 == "") { hocphiThem1 = "0"; hocphiThem1 = parseInt(hocphiThem1); }
    if (name == "") {
        kt = false;
        error += " [Họ tên], ";
        $("#Name").css("border-color", "red");
    }
    if (phone == "") {
        kt = false;
        error += "[Số điện thoại], ";
        $("#Phone").css("border-color", "red");
    }
    if (level == undefined) {
        kt = false;
        error += "[Chọn mức độ quan tâm], ";
        $("#cls_ConcernId").css("border-color", "red");
    }
    if (email == "") {
        kt = false;
        error += "[ Tài khoản đăng nhập], ";
        $("#Email").css("border-color", "red");
    }
    if (cmtnumber == "") {
        kt = false;
        error += "[Số chứng minh thư], ";
        $("#CMT").css("border-color", "red");
    }

    if (hocphi == "") {
        kt = false;
        error += "[Nhập mức học phí của gói học], ";
        $("#HocPhi").css("border-color", "red");
    }
    if (goihoc == "0") {
        kt = false;
        error += "[Chọn gói học!], ";
        $("#ddlGoiHoc").css("border-color", "red");
    }
    if (khoahoc == "0") {
        kt = false;
        error += "[Chọn gói học!], ";
        $("#ddlKhoahoc").css("border-color", "red");
    }
    if (salonID == undefined) {
        kt = false;
        error += "[Chọn Salon!], ";
        $("#Salon").css("border-color", "red");
    }
    //phúc sửa hoc "hocphi1 == NaN"

    if (!kt) {
        $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
            $("#success-alert").slideUp(2000);
        });
        $("#msg-alert").text(error);
    }


    else {
        // hocphi1 = $("#hocphi2").val();
        if (Id == 0 && checkIssetAccount() == false) {
            kt = false;
            $("#success-alert").fadeTo(2000, 800).slideUp(800, function () {
                $("#success-alert").slideUp(800);
            });
            $("#msg-alert").text("Tài khoản này đã tồn tại. Vui lòng nhập tài khoản khác!");
            $("#Email").css("border-color", "pink");
            $("#Email").focus();
        }
        else {
            $.ajax({
                type: "POST",
                contentType: "application/json;charset:UTF-8",
                datatype: "JSON",
                data: '{Id:' + Id + ',Name:"' + name + '",NumberCMT:"' + cmtnumber + '",Phone:"' + phone + '", City:' + city + ', District:' + district + ', Address:"' + address + '", radiobttList:' + level + ', Note:"' + note + '", HocPhi:"' + hocphi + '",HocPhi1:"' + hocphi1 + '",Khoahoc:' + khoahoc + ',Goihoc:' + goihoc + ',ImgCMT1:"' + cmtimage1 + '",ImgCMT2:"' + cmtimage2 + '", Email:"' + email + '",salonID:' + salonID + ', hocphiThem1:' + hocphiThem1 + ' }',
                url: "/GUI/BackEnd/S4M_Student/Student_Add.aspx/AddOrUpdate",
                success: function (response) {
                    var mission = JSON.parse(response.d);
                    if (mission.success) {
                        $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                            $("#success-alert").slideUp(2000);
                        });
                        $("#msg-alert").text("Cập nhật thành công!");
                        setTimeout(function () {
                            window.location.href = "/admin/s4m/hoc-vien/bao-cao-diem-hoc-vien.html";
                        }, 1500);
                    }
                    else {
                        $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                            $("#success-alert").slideUp(2000);
                        });
                        $("#msg-alert").text("Có lỗi xảy ra!Vui lòng liên hệ với nhóm phát triển!");
                    }
                }
            });
        }

    }
}

// Validate keypress
function ValidateKeypress(numcheck, e) {
    var keynum, keychar, numcheck;
    if (window.event) {
        keynum = e.keyCode;
    }
    else if (e.which) {
        keynum = e.which;
    }
    if (keynum == 8 || keynum == 127 || keynum == null || keynum == 9 || keynum == 0 || keynum == 13) return true;
    keychar = String.fromCharCode(keynum);
    var result = numcheck.test(keychar);
    return result;
}

//định dạng text box học phí 000.000
String.prototype.reverse = function () {
    return this.split("").reverse().join("");
}
function reformatText(input) {
    var x = input.value;
    x = x.replace(/,/g, "");
    x = x.reverse();
    x = x.replace(/.../g, function (e) {
        return e + ",";
    });
    x = x.reverse();
    x = x.replace(/^,/, "");
    input.value = x;
}

//check định dạng số điện thoại
function checkPhone(phone) {
    phone = String(phone);
    var filter = /^[0-9-+]+$/;
    var x = phone.substring(0, 1);

    if (phone.length < 10 || phone.length > 11 || x != '0' || filter.test(phone) == false)
        return false;
    else
        return true;
}

function kt() {
    var phone = ('"' + $("#Phone").val() + '"').replace(/[^\d]+/g, '');
    if (checkPhone(phone) == false) {
        $("#success-alert").fadeTo(2000, 800).slideUp(800, function () {
            $("#success-alert").slideUp(800);
        });
        $("#msg-alert").text("Vui lòng kiểm tra lại định dạng số điện thoại!");
        $(this).css("border-color", "pink");
        return false;
        $(this).focus();
    }
}

//check chỉ cho nhập chữ
function fnCheckNameOnKeypress(THIS) {
    var sValue = "";
    if ($(THIS).val() == undefined) {
        sValue = THIS;
    }
    else {
        sValue = $(THIS).val();
    }
    var rgx = /[a-zàáạảãâầấậẩẫăằắặẳẵđèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹ\s]/i;
    for (var i = 0; i < sValue.length; i++) {
        if (rgx.test(sValue[i])) {
        }
        else {
            if ($(THIS).val() != undefined) {
                $(THIS).val(sValue.substr(0, sValue.length - 1));
                $("#success-alert").fadeTo(2000, 800).slideUp(800, function () {
                    $("#success-alert").slideUp(800);
                });
                $("#msg-alert").text("Chỉ cho phép nhập chữ!");
                $(this).css("border-color", "pink");
                $(this).focus();
            }
            return false;
        }
    }
    return true;
}

