﻿
/**********************************************************
// EB Popup
**********************************************************/

jQuery.fn.extend({
    initEBPopup: function () {
        var ebDom = '<div class="eb-popup-wp" id="EBPopupWP">' +
                        '<div class="eb-popup-float" id="EBPopupFloat">' +
                            '<div class="eb-popup" id="EBPopup"></div>' +
                            '<div class="closeBtn" id="EBCloseBtn">' +
                                '<div class="child-wp">' +
                                    '<div class="text">Nhấn Esc để đóng</div>' +
                                    '<div class="triangle"></div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>' +
                    '<div class="msg-success" id="MsgSuccess"><div class="row"></div></div>' +
                    '<div class="confirm-yn" id="EBConfirmYN">' +
                        '<div class="confirm-yn-content">' +
                            '<div class="row">' +
                                '<p class="confirm-yn-text">Bạn có chắc chắn muốn xóa [ Abc ] ?</p>' +
                            '</div>' +
                            '<div class="row">' +
                                '<div class="yn-wp">' +
                                    '<div class="yn-elm yn-yes">Có</div>' +
                                    '<div class="yn-elm yn-no">Không</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                    '</div>';
        var ExistEBDom = document.getElementById("EBPopupWP");
        if (ExistEBDom == null) {
            $('body').append($(ebDom));
        }
    },

    openEBPopup: function () {
        this.initEBPopup();
        this.declareVariable();
        this.showEBPopup();
    },

    declareVariable: function () {
        this.EBPopupWP = $("#EBPopupWP"),
        this.EBPopupFloat = $("#EBPopupFloat"),
        this.EBPopup = $("#EBPopup"),
        this.EBCloseBtn = $("#EBCloseBtn");
    },

    showEBPopup: function () {
        var clone = $(this.selector).clone();
        clone.attr("id", "EBPopupContent").show();
        this.EBPopup.append(clone);
        this.EBPopupFloat.width(clone.width());
        this.EBPopupWP.show();
        this.EBPopupFloat.css({ "margin-top": (window.innerHeight - this.EBPopup.height()) / 2 });

        // trigger close function
        this.closeEBPopup();
        // trigger alignment function
        this.alignment();
    },

    closeEBPopup: function () {
        var THIS = this;
        THIS.EBCloseBtn.unbind("click").bind("click", function () {
            THIS.clearEBPopup();
        });
        $(window).unbind("keydown").bind("keydown", function (e) {
            if (e.keyCode == 27) {
                THIS.clearEBPopup();
            }
        });
    },

    clearEBPopup: function () {
        this.EBPopupWP.hide();
        this.EBPopup.empty();
        this.EBPopupFloat.css({ "width": "auto", "margin-top": 0 });
    },

    alignment: function () {
        this.declareVariable();
        this.EBPopupFloat.css({ "margin-top": (window.innerHeight - this.EBPopup.height()) / 2 });
        var This = this;
        $(window).unbind("resize").bind("resize", function () {
            This.EBPopupFloat.css({ "margin-top": (window.innerHeight - This.EBPopup.height()) / 2 });
        });
    },

    isOpenEBPopup: function () {
        if ($("#EBPopupContent") != undefined && $("#EBPopupContent").hasClass(this.selector.replace(/\./g, ''))) {
            return true;
        } else {
            return false;
        }
    }
});

//======================================
// Library Functions
//======================================

function delSuccess() {
    var msg = "Dữ liệu đã được xóa thành công!";
    $("#EBCloseBtn").click();
    $("#MsgSuccess").find(".row").text(msg).end().openEBPopup();
    autoCloseEBPopup(2000);
}

function delFailed() {
    var msg = "Có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển!";
    $("#EBCloseBtn").click();
    $("#MsgSuccess").find(".row").text(msg).end().openEBPopup();
    autoCloseEBPopup(2000);
}

function autoCloseEBPopup(duration) {
    setTimeout(function () {
        $("#EBCloseBtn").click();
    }, duration);
}

