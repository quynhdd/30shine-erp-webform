﻿var Salon;
var staffID;
var jsonListSalon;
// Get list Salon
function GetListSalon() {
    $.ajax({
        async: false,
        url: "/GUI/BackEnd/Permission/PermissionSalonArea_Add.aspx/GetListSalon",
        type: "POST",
        contentType: "application/JSON; charset:UTF-8",
        dataType: "json",
        success: function (data) {
            jsonListSalon = JSON.parse(data.d);
            for (var i = 0; i < jsonListSalon.length; i++) {
                var option = "<option value='" + jsonListSalon[i].Id + "'>" + jsonListSalon[i].Name + "</option>";

                $("#ddlSalon").append(option);
            }
            $("#ddlSalon").multiselect({
                includeSelectAllOption: true
            })
        },
    });
}

//get list id salon
function getListIdSalon() {
    var Ids = [];
    var prd = {};
    $(".multiselect-native-select input:checked").each(function () {
        prd = {};
        var salonId = $(this).val();
        if (salonId != null && salonId > 0) {
            salonId = salonId.toString().trim() != "" ? parseInt(salonId) : 0;
            prd.salonId = salonId;
            Ids.push(prd);
        }
    });

    Salon = Ids;
}

///get salonId bind to dropdowlistcheckbox
function bindOBJSalon(staffId) {
    $.ajax({
        async: false,
        url: "/GUI/BackEnd/Permission/PermissionSalonAreaListing.aspx/GetListSalonId",
        type: "POST",
        contentType: "application/JSON; charset:UTF-8",
        dataType: "json",
        data: '{staffId:' + staffId + '}',
        success: function (data) {
            var jsonData = JSON.parse(data.d);
            var count = 0;
            var str = "";
            for (var i = 0; i < jsonData.length; i++) {
                $(".multiselect-native-select").find("input[value='" + jsonData[i].SalonId + "']").prop("checked", true);
                str += "" + jsonData[i].Name + "" + ", ";
                count++;
            }
            if (count < 4) {
                $(".multiselect-selected-text").html(str);

            }
            else {
                $(".multiselect-selected-text").html(count + " selected");
            }

        }
    });
}

// reset input
function resetInput() {
    $("#ID").val("");
    $("#Name").val("");
}
// Load tên nv
function GetListStaffASM() {
    var id = $("#ID").val();
    $.ajax({
        url: "/GUI/BackEnd/Permission/PermissionSalonArea_Add.aspx/GetStaff",
        data: '{id:' + id + '}',
        type: "POST",
        contentType: "application/JSON; charset:UTF-8",
        dataType: "JSON",
        success: function (data) {
            var jsonData = JSON.parse(data.d);
            //console.log(jsonData);
            if (jsonData != null) {
                $("#Name").val(jsonData.Fullname);
            }
            else {
                $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                    $("#success-alert").slideUp(2000);
                });
                $("#msg-alert").text("Nhân viên này không thuộc quản lý vùng ASM! Vui lòng kiểm tra lại!");
                resetInput();
            }
        }
    });
}

// Add PermissionSalonArea
function add() {
    getListIdSalon();
    var kt = true;
    var error = 'Vui lòng kiểm tra lại thông tin!';
    var staffId = $("#ID").val();
    if (staffId < 0 || staffId == "") {
        kt = false;
        error += "[Bạn chưa nhập ID nhân viên], ";
        $('#ID').css("border-color", "red");
    }
    if (Salon == "" || Salon == "[]") {
        kt = false;
        error += " [Bạn chưa chọn Salon!]";
    }
    if (!kt) {
        $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
            $("#success-alert").slideUp(2000);
        });
        $("#msg-alert").text(error);
    }

    else {
        addLoading();
        staffId = staffId.toString().trim() != "" ? parseInt(staffId) : 0;
        var data = JSON.stringify({ staffId: staffId, Salon: Salon });
        $.ajax({
            url: "/GUI/BackEnd/Permission/PermissionSalonArea_Add.aspx/Add",
            data: data,
            type: "POST",
            contentType: "application/JSON; charset:UTF-8",
            dataType: "JSON",
            success: function (data) {
                if (data.d.success == true) {
                    $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                        $("#success-alert").slideUp(2000);
                    });
                    $("#msg-alert").text("Thêm mới thành công!");
                    resetInput();
                    setTimeout(function () {
                        window.location.href = "/admin/phan-quyen/danh-sach-phan-vung-salon.html";
                    }, 300);
                    removeLoading();
                }
            },
            error: function (xhr) {
                console.log(xhr)
                alert('Có lỗi xảy ra! Xin vui lòng liên hệ với nhóm phát triển!');
            }
        });

    }
}

//Update
function update() {
    getListIdSalon();
    var staffId = $("#ID").val();
    staffId = staffId.toString().trim() != "" ? parseInt(staffId) : 0;
    if (staffID > 0 && Salon.length > 0) {
        var data = JSON.stringify({ staffId: staffId, Salon: Salon });
        $.ajax({
            url: "/GUI/BackEnd/Permission/PermissionSalonArea_Add.aspx/Update",
            data: data,
            type: "POST",
            contentType: "application/JSON; charset:UTF-8",
            dataType: "JSON",
            success: function (data) {
                if (data.d.success == true) {
                    alert("Cập nhật thành công!");
                    removeLoading();
                }
            },
            error: function (xhr) {
                console.log(xhr)
                alert('Có lỗi xảy ra! Xin vui lòng liên hệ với nhóm phát triển!');
            }
        });
    }
    else {
        alert("Bạn phải chọn Salon!");
    }
}

///validate chỉ được phép nhập số
function ValidateKeypress(numcheck, e) {
    var keynum, keychar, numcheck;
    if (window.event) {
        keynum = e.keyCode;
    }
    else if (e.which) {
        keynum = e.which;
    }
    if (keynum == 8 || keynum == 127 || keynum == null || keynum == 9 || keynum == 0 || keynum == 13) return true;
    keychar = String.fromCharCode(keynum);
    var result = numcheck.test(keychar);
    return result;
}