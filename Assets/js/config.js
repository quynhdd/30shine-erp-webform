﻿
var apiUploadImg = {
    //up anh len s3
    //domainS3: "https://api-upload-s3.30shine.com",
    domainS3: domainAPIUploadS3,
    singleUpload: "/api/s3/singleUpload",
    multiUpload: "/api/s3/multiUpload"
};
var apisReportBCKQKD = {
    // domain deploy apiReport 
    domain: domainAPIReport,
    // api Post data inport file excel
    PostApiSalonDailyCost: "/api/report/salon-daily-cost",
    // api BCKQKD 1 fromdate - todate
    GetApiReport: "/api/report",
    //api BCKQKD ToDay query logic
    GetApiReportToday: "/api/report/today",
    // chay lai bckqkd
    PostRerunBCKQKD: "/api/report/rerun",
    // import lại BCKQKD
    PostDailyCostDayPast: "/api/report/salon-daily-cost-day-past"
};

// Báo cáo nộp doanh số kế toán
var ReportAccounting = {
    domain: domainAPIReport,
    // Bao cao nop so
    accountingReport: "/api/accounting-report",
    // bao cao thue
    accountingReportTax: "/api/accounting-report/accounting-tax"
};

var ReportOperation = {
    domain: domainAPIReport,
    // api bao cao ngay hom nay
    GetApiOperationReportToday: "/api/operation-report/today",
    // api ba cao nhieu ngay
    GetApiOperationReport: "/api/operation-report",
    PostApiOperationReportToday: "POST /api/operation-report/static-expense",
    PostApiOperationReportRerun: "/api/operation-report/static-expense-rerun"
};
