﻿function checkAppointmentTime(salonId, addTime) {
    console.log("check appointment time "+ salonId + "|" + addTime);
    $.ajax({
        type: 'POST',
        url: '/GUI/SystemService/Webservice/BookingService.asmx/CheckAppointmentTime',
        data: '{salonId:"' + salonId + '",addTime:"' + addTime + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var mission = JSON.parse(response.d);
            if (mission.success) {
                var obj = JSON.parse(mission.msg);
                var id = obj.Id;
                var stylist = "";

                if (obj.HairdresserName != null && obj.HairdresserName != "")
                    stylist = ", đặt Stylist " + obj.HairdresserName;

                var customer = obj.CustomerName;
                console.log(obj);
                alertify.confirm("Thông báo lịch hẹn", "Hẹn khách hàng " + customer + stylist + " trong 15 phút nữa.",
                                    function () {
                                        setAppointmentPending(id);
                                        alertify.success('In phiếu');
                                    },
                                    function () {
                                        cancelAppointment(id, salonId);
                                    }).set('labels', { ok: 'In phiếu', cancel: 'Hủy bỏ' });;
            }
            else {
                //alertify.error(mission.msg);
                //console.log("present not have appointment");
            }

        }, failure: function (response) { alert(response.d); }
    });
}

function setAppointmentPending(id) {
    var salonId = $('#HDF_SalonId').val();
    $.ajax({
        type: 'POST',
        url: '/GUI/SystemService/Webservice/BookingService.asmx/SetAppointmentPending',
        data: '{id:"' + id + '",salonId:"' + salonId + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var msg = JSON.parse(response.d);
            if (msg.success) {
                //location.reload(true);
                var billCode = msg.msg;
                window.location.replace("/dich-vu/dat-lich.html?msg_update_status=success&msg_update_message=Th%C3%AAm%20bill%20th%C3%A0nh%20c%C3%B4ng!&msg_print_billcode=" +
                    billCode);
                // This.css("color", "#000");

            } else {
                // This.css("color", "#d0d0d0");
                console.log(msg.msg);
               // showMsgSystem("Đã đầy 4 bill team màu này Pending", "warning");
            }

        }, failure: function (response) { alert(response.d); }
    });
}

function cancelAppointment(id, salonId) {
    console.log("cancel appointment " + id);
    $.ajax({
        type: 'POST',
        url: '/GUI/SystemService/Webservice/BookingService.asmx/SetAppointmentCancel',
        data: '{id:"' + id + '",salonId:"' + salonId + '"}',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (response) {
            var mission = JSON.parse(response.d);
            if (mission.success) {
                alertify.success(mission.msg);
            }
            else
                alertify.error(mission.msg);

        }, failure: function (response) { alert(response.d); }
    });
}