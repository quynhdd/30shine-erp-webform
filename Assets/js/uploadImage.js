﻿//upload image to s3 amazon
function uploadImageStaffToS3(data) {
    var result = '';
    var url = window.apis.monitorStaffService.domainS3 + window.apis.monitorStaffService.multiUpload;
    $.ajax({
        contentType: "application/json",
        type: "POST",
        url: url,
        dataType: "json",
        async: false,
        crossDomain: true,
        data: JSON.stringify(data),
        success: function (response, textStatus, xhr) {
            if (response) {
                result = response;
            }
        },
        error: function (jqXhr, textStatus, errorMessager) {
            console.log(errorMessager + "\n" + jqXhr.responseJSON);
            alert("upload images lên S3 bị lỗi. Bạn hãy thông báo với Admin");
        }
    });

    console.log(result);
    return result;
}
/// show image avatar staff
function uploadAvatar() {
    if (window.File && window.FileList && window.FileReader) {

        $('#files').on("change", function (event) {
            var files = event.target.files; //FileList object
            var output = document.getElementById("result");
            for (var i = 0; i < files.length; i++) {
                var file = files[i];

                console.log(file);
                if (file.type.match('image.*')) {
                    if (this.files[0].size > 2097152) {
                    }

                    var picReader = new FileReader();
                    picReader.addEventListener("load", function (event) {
                        var picFile = event.target;
                        var sReult = "result";
                        var imageData = picFile.result;
                        var div = document.createElement("div");
                        div.innerHTML = "<div class='div-isdelete'><img class='thumbnail' src='" + imageData + "'" +
                            "title='preview image' class='thumb img-delete' style=' height: 120px; width:120px ;'/><span class='btn btn-default span-isdelete' onclick='deleteThumbnail($(this), \"" + sReult + "\" )'>Xóa Avatar</span></div>";
                        console.log(div);
                        output.insertBefore(div, null);
                    });
                    //Read the image
                    $('#clear, #result').show();
                    picReader.readAsDataURL(file);

                } else {
                    alert("You can only upload image file.");
                    $(this).val("");
                }
            }

        });
    } else {
        console.log("Your browser does not support File API");
    }
}

//show image CMT mặt trước
function uploadCMT1() {
    if (window.File && window.FileList && window.FileReader) {

        $('#files1').on("change", function (event) {
            var files = event.target.files; //FileList object
            var output = document.getElementById("result1");
            for (var i = 0; i < files.length; i++) {
                var file = files[i];

                console.log(file);
                if (file.type.match('image.*')) {
                    if (this.files[0].size > 2097152) {
                    }

                    var picReader = new FileReader();
                    picReader.addEventListener("load", function (event) {
                        var picFile = event.target;
                        var sReult = "result1";
                        var imageData = picFile.result;
                        var div = document.createElement("div");
                        div.innerHTML = "<div class='div-isdelete'><img class='thumbnail' src='" + imageData + "'" +
                            "title='preview image' class='thumb1 img-delete' style=' height: 120px; width:120px ;'/><span class='btn btn-default span-isdelete' onclick='deleteThumbnail1($(this), \"" + sReult + "\" )'>Xóa CMT</span></div>";
                        console.log(div);
                        output.insertBefore(div, null);
                    });
                    //Read the image
                    $('#clear, #result1').show();
                    picReader.readAsDataURL(file);

                } else {
                    alert("You can only upload image file.");
                    $(this).val("");
                }
            }

        });
    } else {
        console.log("Your browser does not support File API");
    }
}

//show image CMT mặt sau
function uploadCMT2() {
    if (window.File && window.FileList && window.FileReader) {

        $('#files2').on("change", function (event) {
            var files = event.target.files; //FileList object
            var output = document.getElementById("result2");
            for (var i = 0; i < files.length; i++) {
                var file = files[i];

                console.log(file);
                if (file.type.match('image.*')) {
                    if (this.files[0].size > 2097152) {
                    }

                    var picReader = new FileReader();
                    picReader.addEventListener("load", function (event) {
                        var picFile = event.target;
                        var sReult = "result2";
                        var imageData = picFile.result;
                        var div = document.createElement("div");
                        div.innerHTML = "<div class='div-isdelete'><img class='thumbnail' src='" + imageData + "'" +
                            "title='preview image' class='thumb2 img-delete' style=' height: 120px; width:120px ;'/><span class='btn btn-default span-isdelete' onclick='deleteThumbnail2($(this), \"" + sReult + "\" )'>Xóa CMT</span></div>";
                        console.log(div);
                        output.insertBefore(div, null);
                    });
                    //Read the image
                    $('#clear, #result2').show();
                    picReader.readAsDataURL(file);

                } else {
                    alert("You can only upload image file.");
                    $(this).val("");
                }
            }

        });
    } else {
        console.log("Your browser does not support File API");
    }
}

//delete image avatar
function deleteThumbnail(This, StoreImgField) {
    var imgs = "";
    var lst = This.parent().parent();
    This.parent().remove();
    lst.find("img.thumb").each(function () {
        imgs += $(this).attr("src") + ",";
    });
    $("#" + StoreImgField).val(imgs);
    $('#result').hide();
    $('#files').val("");
    $(this).hide();
}

//delete image cmt1
function deleteThumbnail1(This, StoreImgField) {
    var imgs = "";
    var lst = This.parent().parent();
    This.parent().remove();
    lst.find("img.thumb1").each(function () {
        imgs += $(this).attr("src") + ",";
    });
    $("#" + StoreImgField).val(imgs);
    $('#result1').hide();
    $('#files1').val("");
    $(this).hide();
}

//delete image cmt2
function deleteThumbnail2(This, StoreImgField) {
    var imgs = "";
    var lst = This.parent().parent();
    This.parent().remove();
    lst.find("img.thumb2").each(function () {
        imgs += $(this).attr("src") + ",";
    });
    $("#" + StoreImgField).val(imgs);
    $('#result2').hide();
    $('#files2').val("");
    $(this).hide();
}