﻿
function ClickAddCombo(This) {
    var index = 1;
    $('#tbodyProduct').find("tr").each(function () {
        $('#tbodyProduct tr').find("td.td-product-index").text(index);
        index++;
    });
    var IdProduct = $("#ddlProduct").find(':selected').data('id');
    var Code = $("#ddlProduct").find(':selected').data('code');
    var PriceProduct = $("#ddlProduct").find(':selected').data('cost');
    var Cate = $("#ddlProduct").find(':selected').data('cate');
    var NameProduct = $("#ddlProduct").find(':selected').data('name');
    var Quantity = 1;
    if (IdProduct != 0) {
        $('#tbodyProduct').append(
            '<tr data-cate="' + Cate + '"><td class="td-product-index"></td><td class="td-product-name">' + NameProduct + '</td><td class="td-product-code" data-id="' + IdProduct + '" data-code="' + Code + '">' + Code + '</td><td class="td-product-price" data-cost="' + PriceProduct + '">' + PriceProduct + '</td><td class="td-product-quantity"><input type="text" class="product-quantity" value="' + Quantity + '"></td><td class="map-edit"><div class="edit-wp"><a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),\'' + NameProduct + '\',\'product\')" href="javascript://" title="Xóa"></a></div></td></tr>');
        $(".item-product").show();
        TotalMoney();
        UpdateItemOrder($("#table-item-product").find("tbody"));
        getProductIds();
        getQuantity();
        ResetDropdownProduct();
    }
    else {
        alert("Bạn chưa chọn sản phẩm !");
    }
}
//TotalMoney

function TotalMoney() {
    var Money = 0;
    $("table.table-item-product tbody tr").each(function () {
        var money = $(this).find("td.td-product-price").attr("data-cost");
        Money += parseFloat(money);
    });
    console.log(Money);
    $("#TotalMoney").val(FormatPrice(Money));
    $("#HDF_TotalMoney").val(Money);
}
// ResetDropdownProduct
function ResetDropdownProduct() {
    $("#ddlProduct").val(0);
    $("#select2-ddlProduct-container").attr('title', 'Chọn sản phẩm ...');
    $("#select2-ddlProduct-container").text("Chọn sản phẩm ...");
}
//Update product
function UpdateItemDisplay(itemName) {
    var dom = $(".table-item-" + itemName);
    var len = dom.find("tbody tr").length;
    if (len == 0) {
        dom.parent().hide();
    } else {
        UpdateItemOrder(dom.find("tbody"));
    }
}
// Update STT
function UpdateItemOrder(dom) {
    var index = 1;
    dom.find("tr").each(function () {
        $(this).find("td.td-product-index").text(index);
        index++;
    });
}
//RemoveItem
function RemoveItem(THIS, name, itemName) {
    var Code = THIS.find(".td-product-code").attr("data-code");
    $("#quick-" + itemName).find("input[data-code='" + Code + "']").removeAttr("checked");
    THIS.remove();
    UpdateItemDisplay(itemName);
    TotalMoney();
    getProductIds();
    getQuantity();
}
//GetIDProduct
function getProductIds() {
    var Ids = '';
    $("table.table-item-product tbody tr").each(function () {
        var Id = $(this).find("td.td-product-code").attr("data-id"),
        Id = Id.toString().trim() != "" ? parseInt(Id) : 0;
        if (Ids != '')
            Ids += ',';
        Ids += Id;
    });
    $("#HDF_ProductIds").val(Ids);
}
function getQuantity() {
    var Quantity = '';
    $("table.table-item-product tbody tr").each(function () {
        debugger;
        var quantity = $(this).find("td.td-product-quantity input:text").attr("value"),
            quantity = parseInt(quantity);
        if (Quantity != '')
            Quantity += ',';
        Quantity += quantity;
    });
    $("#HDF_Quantity").val(Quantity);
}
function ValidateKeypress(numcheck, e) {
    var keynum, keychar, numcheck;
    if (window.event) {
        keynum = e.keyCode;
    }
    else if (e.which) {
        keynum = e.which;
    }
    if (keynum == 8 || keynum == 127 || keynum == null || keynum == 9 || keynum == 0 || keynum == 13) return true;
    keychar = String.fromCharCode(keynum);
    var result = numcheck.test(keychar);
    return result;
}