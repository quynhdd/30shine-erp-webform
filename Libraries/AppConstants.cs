﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Libraries
{
    public static class AppConstants
    {
        public static string URL_API_LIVESITE = ConfigurationManager.AppSettings["URL_API_LIVESITE"];/*"https://api.30shine.com"*/
        public static string URL_API_FINANCIAL = ConfigurationManager.AppSettings["URL_API_FINANCIAL"]; /*"http://localhost:63088"*/
        public static string URL_API_STAFF = ConfigurationManager.AppSettings["URL_API_STAFF"];/* "https://apistaff.30shine.com"*/
        public static string URL_API_STAFF_WEBSERVICE = ConfigurationManager.AppSettings["URL_API_STAFF_WEBSERVICE"]; /*"https://api.30shine.com"*///"";
        public static string URL_API_CHECKIN = ConfigurationManager.AppSettings["URL_API_CHECKIN"]; /*"https://apicheckin.30shine.com"*///"";
        public static string URL_API_BOOKING_BETA = ConfigurationManager.AppSettings["URL_API_BOOKING_BETA"]; /*"https://apibookingbeta.30shine.com"*///"";
        public static string URL_API_BILL_SERVICE = ConfigurationManager.AppSettings["URL_API_BILL_SERVICE"];/* "https://apibill.30shine.com"*///""; 
        public static string URL_API_INVENTORY = ConfigurationManager.AppSettings["URL_API_INVENTORY"]; /*"https://api-inventory.30shine.com"*///""; 
        public static string URL_API_PUSH_NOTICE = ConfigurationManager.AppSettings["URL_API_PUSH_NOTICE"]; /*"https://api-push-notic.30shine.com"*/
        public static string URL_API_DOMAINS3 = ConfigurationManager.AppSettings["URL_API_DOMAINS3"]; /*"https://api-upload-s3.30shine.com"*///"";
        public static string URL_API_CHECKOUT = ConfigurationManager.AppSettings["URL_API_CHECKOUT"]; /*"https://apicheckout.30shine.com"*///"";
        public static string URL_API_NOTIFICATION = ConfigurationManager.AppSettings["URL_API_NOTIFICATION"]; /*"https://apinotification.30shine.com"*/
        public static string URL_API_TIMEKEEPING = ConfigurationManager.AppSettings["URL_API_TIMEKEEPING"]; /*"https://apinotification.30shine.com"*/
        public static string URL_API_REPORTS = ConfigurationManager.AppSettings["URL_API_REPORTS"];
        public static string URL_API_BOOKING_REQUEST = ConfigurationManager.AppSettings["URL_API_BOOKING_REQUEST"];
        public static string URL_API_MANAGER_APP = ConfigurationManager.AppSettings["URL_API_MANAGER_APP"];
        public static string GROUP_SLACK = ConfigurationManager.AppSettings["GROUP_SLACK"]; /*"CBHB99UGM"*/
        //public const int ENROLL_LIMIT_DAY_BEFORE = -3;
        public static string URL_API_MONITORSTAFF = ConfigurationManager.AppSettings["URL_API_MONITORSTAFF"]; /*"CBHB99UGM"*/

        public class OrderStatus
        {
            //
            public const int NOT_ORDER = 1;
            public const int ORDERED = 2;
            public const int ORDER_REJECT = 3;
            public const int WAIT_IMPORT = 4;
            public const int IMPORTED = 5;
        }

        public class InventoryType
        {
            //
            public const int AREA = 1;
            public const int SALON = 2;
            public const int STAFF = 3;
            public const int SUPPLIER = 4;
        }

        // This list of default attributes follows the OpenID Connect specification.
        // Source: https://docs.aws.amazon.com/cognito/latest/developerguide/user-pool-settings-attributes.html
        // Some default attributes might be required when registering a user depending on the user pool configuration.

        public const string Address = "address";
        public const string BirthDate = "birthdate";
        public const string Email = "email";
        public const string EmailVerified = "email_verified";
        public const string FamilyName = "family_name";
        public const string Gender = "gender";
        public const string GivenName = "given_name";
        public const string Locale = "locale";
        public const string MiddleName = "middle_name";
        public const string Name = "name";
        public const string NickName = "nickname";
        public const string PhoneNumber = "phone_number";
        public const string PhoneNumberVerified = "phone_number_verified";
        public const string Picture = "picture";
        public const string PreferredUsername = "preferred_username";
        public const string Profile = "profile";
        public const string ZoneInfo = "zoneinfo";
        public const string UpdatedAt = "updated_at";
        public const string Website = "website";

        //password default
        public const string PASSWORD_DEFAULT = "abc@123";

        //custom exception
        public const string INTERNAL_ERROR_EXCEPTION = "InternalErrorException";
        public const string INVALID_PARAMETER_EXCEPTION = "InvalidParameterException";
        public const string NOT_AUTHORIEZED_EXCEPTION = "Incorrect username or password.";
        public const string PASSWORD_RESET_REQUIRED_EXCEPTION = "PasswordResetRequiredException";
        public const string RESOURCE_NOT_FOUND_EXCEPTION = "ResourceNotFoundException";
        public const string TOO_MANY_REQUESTS_EXCEPTION = "TooManyRequestsException";
        public const string USER_NOT_CONFIRMED_EXCEPTION = "UserNotConfirmedException";
        public const string USER_NOT_FOUND_EXCEPTION = "User does not exist.";
        public const string PASSWORD_ATTEMPTS_EXCEEDED = "Password attempts exceeded";
        public const string PASSWORD_INCORRECT = "Invalid access token.";
        public const string USER_DISABLED = "User is disabled";
        //end exception

        public static string POOL_ID = ConfigurationManager.AppSettings["POOL_ID"];
        public static string APP_CLIENT_ID = ConfigurationManager.AppSettings["APP_CLIENT_ID"];
        public static string KEY = ConfigurationManager.AppSettings["KEY"];

        public static string ACCESS_KEY = ConfigurationManager.AppSettings["ACCESS_KEY"];
        public static string SECRET_KEY = ConfigurationManager.AppSettings["SECRET_KEY"];
    }
}