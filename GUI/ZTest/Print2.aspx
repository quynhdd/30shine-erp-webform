﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Print2.aspx.cs" Inherits="_30shine.GUI.ZTest.Print2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../../Assets/js/print/jspdf.js"></script>
    <script src="../../Assets/js/print/from_html.js"></script>
    <script src="../../Assets/js/print/split_text_to_size.js"></script>
    <script src="../../Assets/js/print/standard_fonts_metrics.js"></script>
    <script src="../../Assets/js/print/cell.js"></script>
    <script src="../../Assets/js/print/addimage.js"></script>
    <script src="../../Assets/js/print/canvas.js"></script>
    <script src="../../Assets/js/print/context2d.js"></script>
    <script src="../../Assets/js/print/png_support.js"></script>
    <script src="../../Assets/js/print/autoprint.js"></script>
    <script src="../../Assets/js/print/zlib.js"></script>
    <script src="../../Assets/js/print/png.js"></script>
    <script src="../../Assets/js/jquery.v1.11.1.js"></script>
</head>
<body>
    
    <p class="ignorePDF">don't print this to pdf</p>
    <%--<img class="ignorePDF" src="http://solution.30shine.net/Assets/images/logo.png" style="width: 100px;" />--%>
    <div runat="server" id="htmlContent">
      
    </div>
 
</body>
</html>
