﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ScrollPage.aspx.cs" Inherits="_30shine.GUI.ZTest.ScrollPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/Assets/css/bootstrap/bootstrap.min.css" rel="stylesheet" />
    <script src="/Assets/js/jquery.v1.11.1.js"></script>
    <script src="/Assets/js/bootstrap/bootstrap.min.js"></script>
    <script src="/Assets/js/jquery-ui.js"></script>
</head> 
<body>
    <style>
        .wrap-scroll-page { width: 100%; float: left;}
        .wrap-scroll-page .scroll-block { width: 100%; float: left; position: relative;}
        .wrap-scroll-page .scroll-block { width: 100%; float: left;}

        .wrap-scroll-page .bl01 .scroll-block-ct { height: 620px; background: url(/Assets/images/slide/slide.01.jpg) center no-repeat; z-index: 101;}
        .wrap-scroll-page .bl02 .scroll-block-ct { height: 500px; background: url(/Assets/images/slide/slide.02.jpg) center no-repeat; z-index: 102;}
        .wrap-scroll-page .bl03 .scroll-block-ct { height: 600px; background: url(/Assets/images/slide/slide.03.jpg) center no-repeat; z-index: 103;}
        .wrap-scroll-page .bl04 .scroll-block-ct { height: 580px; background: url(/Assets/images/slide/slide.04.jpg) center no-repeat; z-index: 104;}
        .wrap-scroll-page .bl05 .scroll-block-ct { height: 550px; background: url(/Assets/images/slide/slide.05.jpg) center no-repeat; z-index: 105;}
        .wrap-scroll-page .bl06 .scroll-block-ct { height: 605px; background: url(/Assets/images/slide/slide.06.jpg) center no-repeat; z-index: 106;}

        .wrap-scroll-page .bl01{ height: 620px; z-index: 101;}
        .wrap-scroll-page .bl02{ height: 500px; z-index: 102;}
        .wrap-scroll-page .bl03{ height: 600px; z-index: 103;}
        .wrap-scroll-page .bl04{ height: 580px; z-index: 104;}
        .wrap-scroll-page .bl05{ height: 550px; z-index: 105;}
        .wrap-scroll-page .bl06{ height: 600px; z-index: 106;}

        .footer {width: 100%; height: 505px; float: left; background: url(/Assets/images/slide/slide.07.jpg) center no-repeat; z-index: 106;}

    </style>
    <form id="form1" runat="server">
    <div class="wrap-scroll-page" id="WrapScrollPage">
        <div class="scroll-block bl01" data-item="1">
            <div class="scroll-block-ct">
                <%-- content here --%>
            </div>
        </div>
        <div class="scroll-block bl02" data-item="2">
            <div class="scroll-block-ct"></div>
        </div>
        <div class="scroll-block bl03" data-item="3">
            <div class="scroll-block-ct"></div>
        </div>
        <div class="scroll-block bl04" data-item="4">
            <div class="scroll-block-ct"></div>
        </div>
        <div class="scroll-block bl05" data-item="5">
            <div class="scroll-block-ct"></div>
        </div>
        <div class="scroll-block bl06" data-item="6">
            <div class="scroll-block-ct"></div>
        </div>
    </div>

    <div class="footer"></div>
        
    <script type="text/javascript">
        var activeItem, totalItem;
        var iScrollPos = 0,
            valueBound = 333,
            LSToffset = [],
            status = false,
            marginTop = 0;
        // left: 37, up: 38, right: 39, down: 40,
        // spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36
        var keys = { 37: 1, 38: 1, 39: 1, 40: 1 };        var CoundChildNodes = function (DomElm) {
            var count = 0;
            for (var i = 0; i < DomElm.childNodes.length; i++) {
                var node = DomElm.childNodes[i];
                if (node.nodeType == Node.ELEMENT_NODE) {
                    count++;
                }
            }
            return count;
        }

        var preventDefault = function (e) {
            e = e || window.event;
            if (e.preventDefault)
                e.preventDefault();
            e.returnValue = false;
        }

        var preventDefaultForScrollKeys = function (e) {
            if (keys[e.keyCode]) {
                preventDefault(e);
                return false;
            }
        }

        var disableScroll = function () {
            if (window.addEventListener) // older FF
                window.addEventListener('DOMMouseScroll', preventDefault, false);
            window.onwheel = preventDefault; // modern standard
            window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
            window.ontouchmove = preventDefault; // mobile
            document.onkeydown = preventDefaultForScrollKeys;

        }

        var enableScroll = function () {
            if (window.removeEventListener)
                window.removeEventListener('DOMMouseScroll', preventDefault, false);
            window.onmousewheel = document.onmousewheel = null;
            window.onwheel = null;
            window.ontouchmove = null;
            document.onkeydown = null;            
        }
                        function ScrollToTop(selector, callBack, duration, change, easing) {
            selector = $(selector) || $(window);
            duration = duration || 700;
            change = change || 0;
            easing = easing || 'easeInOutCirc';
            var top = selector.offset().top;
            $('body,html').animate({ scrollTop: top + change }, duration, easing, function () {
                callBack();
            });
        }        var detectDevice = function () {
            var device = { tablet: false, mobile: false, desktop: false };
            if (navigator.userAgent.match(/Tablet|iPad/)) {
                device.tablet = true;
            } else if (navigator.userAgent.match(/Mobile/)) {
                device.mobile = true;
            } else {
                device.desktop = true;
            }
            return device;
        }    
        jQuery(document).ready(function ($) {
            if (!detectDevice().mobile) {
                totalItem = CoundChildNodes(document.getElementById("WrapScrollPage"));
                $(".wrap-scroll-page .scroll-block").each(function () {
                    LSToffset.push($(this).offset().top);
                });

                $(window).unbind("scroll").bind("scroll", function (e) {                    
                    var iCurScrollPos = $(this).scrollTop();
                    var pageYOffset = window.pageYOffset;
                    if (iCurScrollPos > iScrollPos) {
                        //disableScroll();                        
                        for (var i = 0; i < LSToffset.length; i++) {                            
                            var item = i + 1;
                            var _This = $(".wrap-scroll-page .scroll-block[data-item=" + item + "]");
                            var _Prev = $(".wrap-scroll-page .scroll-block[data-item=" + (parseInt(item) - 1) + "]");
                            var boundY = LSToffset[i] - pageYOffset - marginTop;
                            
                            // First element case
                            if (item == 1 && -valueBound < boundY && boundY < 0) {
                                activeItem = 1;
                                iScrollPos = 0;
                            }
                            if ((0 < boundY && boundY < valueBound)) {
                                //Scrolling Down
                                if (item != activeItem) {
                                    disableScroll();
                                    _This.animate({ marginTop: -boundY + "px" }, 1000, "easeInOutCubic", function () {
                                        enableScroll();
                                        marginTop += boundY;
                                    });
                                    _Prev.animate({
                                        paddingTop: _Prev.find(">div").height() / 2 + "px"
                                    }, 2200, "easeInOutCubic", function () { });
                                    activeItem = item;
                                    status = true;
                                    break;
                                }
                            } else {
                                //enableScroll();
                            }
                        }
                    } else {
                        //Scrolling Up
                        $(".wrap-scroll-page .scroll-block").css({ "padding-top": "0px", "margin-top": "0px" });
                        marginTop = 0;
                    }
                    iScrollPos = iCurScrollPos;
                });
            }
        });

    </script>
    </form>
</body>
</html>
