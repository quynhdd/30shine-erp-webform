﻿using _30shine.Helpers.Http;
using _30shine.MODEL.CustomClass;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.ZTest
{
    public partial class TestAsyncTask : System.Web.UI.Page
    {
        public const string PATH_ASYNC_TEST_LOG = @"D:\30ShineSystemLog\test_async_task.txt";

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                var request = new Request();
                /// Tinh cho stylist

                Task taskStatisBillNew = Task.Run(async () =>
                {
                    await request.RunPostAsync(
                        //"http://erp.30shine.net/GUI/ZTest/TestAsyncTask.aspx/Task1",
                        "http://erp.30shine.net/GUI/SystemService/Webservice/wsBillService.asmx/Task1",
                        new
                        {
                            data = new Data_ThongkeVattu
                            {
                                BillId = 1,
                                StylistId = 2,
                                SkinnerId = 3,
                                CompletedTime = DateTime.Now
                            }
                        }
                    );
                });
                taskStatisBillNew.Wait(100);
            }
            catch (Exception)
            {

            }
        }

        [WebMethod]
        public static void Task1()
        {
            try
            {
                var task = Task.Run(async delegate
                {
                    await Task.Delay(10000);                    
                    return DateTime.Now;
                });
                //task.Wait();
                Library.Function.WriteToFile("---------------------------------------------", @"D:\test_async_task.txt");
                Library.Function.WriteToFile("task call : " + DateTime.Now, PATH_ASYNC_TEST_LOG);
                Library.Function.WriteToFile("task finish : " + task.Result, PATH_ASYNC_TEST_LOG);
            }
            catch (Exception ex)
            {
                Library.Function.WriteToFile(ex.Message, PATH_ASYNC_TEST_LOG);
            }
        }
    }
}