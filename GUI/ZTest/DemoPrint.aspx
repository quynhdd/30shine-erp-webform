﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DemoPrint.aspx.cs" Inherits="_30Shine.GUI.DemoPrint" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="/Assets/js/jquery.v1.11.1.js"></script>
    <title>Print asp.net gridview data using javascript</title>
    <script type="text/javascript">
        function printGrid() {
            var gridData = document.getElementById('<%= grdResultDetails.ClientID %>');
            var windowUrl = 'about:blank';
            var _Data = $('#demo');
            //set print document name for gridview
            var uniqueName = new Date();
            var windowName = 'Print_' + uniqueName.getTime();

            var prtWindow = window.open(windowUrl, windowName,
            'left=100,top=100,right=100,bottom=100,width=700,height=500');
            prtWindow.document.write('<html><head></head>');
            prtWindow.document.write('<body style="background:none !important;">');
            //prtWindow.document.write(gridData.outerHTML);
            prtWindow.document.write(_Data.html());
            
            prtWindow.document.write('</body></html>');
            prtWindow.document.close();
            prtWindow.focus();
            prtWindow.print();
            prtWindow.close();
        }
    </script>

    <script type="text/javascript">
        function PrintHoaDon() {
            var windowUrl = 'about:blank';
            var _Data = $('.content-print');
            //set print document name for gridview
            var uniqueName = new Date();
            var windowName = 'Print_' + uniqueName.getTime();

            var prtWindow = window.open(windowUrl, windowName,
            'left=100,top=100,right=100,bottom=100');
            //prtWindow.document.write('<style>.content-print { width: 226.771654px;}.content-print table { width: 100%; border: 1px solid #AAA; table-layout: fixed; border-collapse: collapse; margin: 10px 0;}.content-print table th, .content-print table td { border: 1px solid #DDD; text-align: left; padding: 5px; -webkit-box-sizing: border-box; box-sizing: border-box; text-align: center;}.des { font-size: 16px; padding-bottom: 5px;}.des strong { padding-right: 5px;}.des span { padding-right: 5px; text-decoration: underline;}.total { font-size: 20px; font-weight: bold;}.total1 { font-size: 16px; font-weight: bold;}.button { text-align: center; margin-top: 15px;}</style>');
            prtWindow.document.write('<html><head></head>');
            prtWindow.document.write('<body style="background:none !important;text-align: center">');
            //prtWindow.document.write(gridData.outerHTML);
            prtWindow.document.write(_Data.html());

            prtWindow.document.write('</body></html>');
            prtWindow.document.close();
            prtWindow.focus();
            prtWindow.print();
            prtWindow.close();
        }


        function DemoPrint() {
            var win = window.open('', 'printwindow');
            win.document.write('<html><head><link rel="stylesheet" type="text/css" href="css/demoprint.css"></head><body>');
            win.document.write($(".content-print").html());
            win.document.write('</body></html>');
            win.print();
            win.close();
        }


        function printWithCss() {
            //Works with Chome, Firefox, IE, Safari
            //Get the HTML of div
            var title = document.title;
            var divElements = $('#content-print').html();
            var printWindow = window.open("", "", "");
            //open the window
            printWindow.document.open();
            //write the html to the new window, link to css file
            printWindow.document.write('<html><head><link rel="stylesheet" type="text/css" href="/Css/demoprint.css"></head><body>');
            printWindow.document.write(divElements);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.focus();
            //The Timeout is ONLY to make Safari work, but it still works with FF, IE & Chrome.
            setTimeout(function () {
                printWindow.print();
                printWindow.close();
            }, 100);
        }
    </script>
    

</head>
<body>
    <form id="form1" runat="server">
    <div>

    <table id="table-print">
        <tr>
            <td><h4>Print asp.net gridview data in asp.net c#, vb.net</h4></td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnPrint" runat="server" Text="Print From Client-side"
                OnClientClick="printGrid()" />&nbsp;&nbsp;
                <asp:Button ID="btnPrintFromCodeBehind" runat="server"
                Text="Print From Code-behind" OnClick="btnPrintFromCodeBehind_Click" />
            </td>
        </tr>
        <tr><td>&nbsp;&nbsp;</td></tr>
        <tr id="demo" style="width: 226.771654px; display:none;">
            <td>
                <div class="des"><span>Khách hàng:</span> Lê Thành Công</div>
                <div class="des"><span>Mã KH:</span> ABCD</div>

                <asp:GridView Width="226.771654" ID="grdResultDetails" runat="server" AutoGenerateColumns="false">
                   <HeaderStyle Font-Bold="true" BackColor="#ff6600" BorderColor="#222"
                    ForeColor="White" Height="30" />
                   <Columns>
                     <asp:BoundField DataField="DV" HeaderText="Dịch vụ"
                     ItemStyle-HorizontalAlign="Center" />
                     <asp:BoundField DataField="SL" HeaderText="Số lượng"
                     ItemStyle-HorizontalAlign="Center" />
                    </Columns>
                </asp:GridView>
                <div class="des"><span>Stylist:</span> Nguyễn Văn A</div>
            <div class="des"><span>Skinner:</span> Nguyễn Văn B</div>
            <div style="width: 226.771654px;" class="des total1"><strong>Thành tiền:</strong> <span style="float: right; font-weight: bold; font-size: 16px;">100.000đ</span></div>

                <asp:GridView Width="226.771654" ID="grv2" runat="server" AutoGenerateColumns="false">
                   <HeaderStyle Font-Bold="true" BackColor="#ff6600" BorderColor="#222"
                    ForeColor="White" Height="30" />
                   <Columns>
                     <asp:BoundField DataField="SP" HeaderText="Sản phẩm" 
                     ItemStyle-HorizontalAlign="Center" />
                     <asp:BoundField DataField="SL" HeaderText="Số lượng" 
                     ItemStyle-HorizontalAlign="Center" />
                    </Columns>
                </asp:GridView>
                <div class="des"><span>Seller:</span> Nguyễn Văn C</div>
            <div class="des total1" style="width: 226.771654px;"><strong>Thành tiền:</strong> <span style="float: right; font-weight: bold; font-size: 16px;">200.000đ</span></div>
            <div style="width: 226.771654px;" class="des total"><strong>Tổng tiền:</strong> <span style="float: right; font-weight: bold; font-size: 20px;">300.000đ<span></span></div>
            </td>
        </tr>
    </table>
    </div>
    </form>
</body>
</html>
