﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Globalization;
using _30shine.Helpers;
using System.Text;
using System.Security.Cryptography;

namespace _30shine.GUI.ZTest
{
    public partial class Mess : System.Web.UI.Page
    {
        private UIHelpers _UIHelper = new UIHelpers();
        protected void Page_Load(object sender, EventArgs e)
        {
            //var date = DateTime.Now.ToString("dd/MM/yyyy");
            //log(date);

            // dbcn
            //dbcn2();

            // format price
            //CultureInfo elGR = CultureInfo.CreateSpecificCulture("el-GR");
            //var str = String.Format(elGR, "{0:0,0}", 100000);
            //Response.Write(str);

            //Response.Write("<br/>" + GetUniqueKey() + "<br/>");

            var title = "Thử cái tiêu đề xem thế nào";
            var slug = _UIHelper.GenerateSlug(title);
            Response.Write(slug);

            int dayofweek = (int)DateTime.Now.DayOfWeek;
            Response.Write("<br>day of week " + dayofweek + "</br>");

            using (MD5 md5Hash = MD5.Create())
            {
                // check in kham thien
                var Password = "30shinecheckinkt@";
                var PasswordGen = GenPassword(md5Hash, Password);
                log("30shine check in kham thien - " + PasswordGen);
                // check out kham thien
                Password = "30shinecheckoutkt@";
                PasswordGen = GenPassword(md5Hash, Password);
                log("30shine check out kham thien - " + PasswordGen);
                // check in tran dai nghia
                Password = "30shinecheckintdn@";
                PasswordGen = GenPassword(md5Hash, Password);
                log("30shine check in tran dai nghia - " + PasswordGen);
                // check out tran dai nghia
                Password = "30shinecheckouttdn@";
                PasswordGen = GenPassword(md5Hash, Password);
                log("30shine check out tran dai nghia - " + PasswordGen);
                // check in doi can
                Password = "30shinecheckindc@";
                PasswordGen = GenPassword(md5Hash, Password);
                log("30shine check in doi can - " + PasswordGen);
                // check out doi can
                Password = "30shinecheckoutdc@";
                PasswordGen = GenPassword(md5Hash, Password);
                log("30shine check out doi can - " + PasswordGen);
            }
        }

        /// <summary>
        /// Generate unique key
        /// </summary>
        /// <returns></returns>
        public string GetUniqueKey()
        {
            int maxSize = 20;
            int minSize = 20;
            char[] chars = new char[62];
            string a;
            a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            chars = a.ToCharArray();
            int size = maxSize;
            byte[] data = new byte[1];
            System.Security.Cryptography.RNGCryptoServiceProvider crypto = new System.Security.Cryptography.RNGCryptoServiceProvider();
            crypto.GetNonZeroBytes(data);
            size = maxSize;
            data = new byte[size];
            crypto.GetNonZeroBytes(data);
            System.Text.StringBuilder result = new System.Text.StringBuilder(size);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length - minSize)]);
            }
            return result.ToString();
        }

        public void dbcn()
        {
            using (var db = new Solution_30shineEntities())
            {
                var Bills = db.BillServices.Where(w => w.IsDelete != 1)
                                            .Join(db.Customers,
                                                    a => a.CustomerCode,
                                                    b => b.Customer_Code,
                                                    (a, b) => new
                                                    {
                                                        Id = a.Id,
                                                        code = a.CustomerCode,
                                                        name = b.Fullname,
                                                        money = a.TotalMoney,
                                                        CityId = b.CityId
                                                    })
                                            .Join(db.TinhThanhs,
                                                    c => c.CityId,
                                                    d => d.ID,
                                                    (c, d) => new
                                                    {
                                                        Id = c.Id,
                                                        code = c.code,
                                                        name = c.name,
                                                        money = c.money,
                                                        CityName = d.TenTinhThanh
                                                    })
                                            .OrderBy(o => o.Id).Take(1000)
                                            .ToList();
                var Bills2 = db.BillServices.Where(w => w.IsDelete != 1).OrderBy(o => o.Id).Take(1000).ToList();

                var Bills3 = db.BillServices.Where(w => w.IsDelete != 1)
                                            .GroupBy(
                                                g => g.CustomerCode,
                                                g => g.TotalMoney,
                                                (key, group) => new
                                                {
                                                    CustomerCode = key,
                                                    money = group.Sum()
                                                }
                                            );
                var Bills4 = db.BillServices.Where(w => w.IsDelete != 1)
                                            .Join(db.Customers,
                                                    a => a.CustomerCode,
                                                    b => b.Customer_Code,
                                                    (a, b) => new
                                                    {
                                                        Id = a.Id,
                                                        code = a.CustomerCode,
                                                        name = b.Fullname,
                                                        money = a.TotalMoney,
                                                        CityId = b.CityId
                                                    })
                                            .Join(db.TinhThanhs,
                                                    c => c.CityId,
                                                    d => d.ID,
                                                    (c, d) => new
                                                    {
                                                        Id = c.Id,
                                                        code = c.code,
                                                        name = c.name,
                                                        money = c.money,
                                                        CityName = d.TenTinhThanh
                                                    })
                                            .OrderBy(o => o.code).Take(1000)
                                            .ToList()
                                            ;
                var Bills5 = Bills4.GroupBy(g => g.code)
                                                  .Select(s => new
                                                  {
                                                      code = s.Key,
                                                      money = s.Sum(sum => sum.money),
                                                      name = s.First().name,
                                                      city = s.First().CityName
                                                  });

                foreach (var v in Bills5)
                {
                    log(v.name + " : " + v.code + " : " + v.money.ToString() + " : " + v.city);
                }
                log("==========================");

            }
        }

        public void dbcn2()
        {
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                string TxtTimeFrom = "01/09/2015";
                string TxtTimeTo = "01/10/2015";
                DateTime _FromDate = Convert.ToDateTime(TxtTimeFrom, culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                DateTime _ToDate = Convert.ToDateTime(TxtTimeTo, culture).AddDays(1);

                var LST0 = db.Staffs.Where(w => w.Type == 2)
                                   .OrderBy(o => o.Id).ToList();
                var LST = LST0.Join(db.BillServices,
                                        a => a.Id,
                                        b => b.Staff_HairMassage_Id,
                                        (a, b) => new
                                        {
                                            a.Id,
                                            a.Fullname,
                                            a.Type,
                                            a.IdShowroom,
                                            BillIsDelete = b.IsDelete,
                                            BillId = b.Id,
                                            BillCreatedDate = b.CreatedDate,
                                            b.ServiceIds
                                        }
                                   ).Where(w => w.BillCreatedDate > _FromDate && w.BillCreatedDate < _ToDate && w.BillIsDelete != 1).ToList();

                var LST2 = LST.Join(db.FlowServices,
                                       a => a.BillId,
                                       b => b.BillId,
                                       (a, b) => new
                                       {
                                           a.Id,
                                           a.Fullname,
                                           a.Type,
                                           a.IdShowroom,
                                           a.BillId,
                                           b.ServiceId,
                                           FlowServiceIsDelete = b.IsDelete
                                       }
                                   )
                                   .GroupBy(g => g.Id)
                                   .Select(s => new
                                   {
                                       Id = s.First().Id,
                                       Fullname = s.First().Fullname,
                                       Type = s.First().Type,
                                       IdShowroom = s.First().IdShowroom,
                                       ServiceIds = s.Select(ss => new { ss.ServiceId }).ToList()
                                   })
                                   .ToList();
                
                if (LST2.Count > 0)
                {
                    var totalservice = 0;
                    foreach (var v in LST2)
                    {
                        var lstServiceStatistic = new List<ServiceStatistic>();
                        var service = new ServiceStatistic();
                        var _ServiceTmp = new ServiceStatistic();
                        int index = -1;
                        if (v.ServiceIds.Count() > 0)
                        {
                            foreach (var v2 in v.ServiceIds)
                            {                                
                                service = new ServiceStatistic();
                                index = lstServiceStatistic.FindIndex(fi => fi.ServiceId == v2.ServiceId);
                                if (index == -1)
                                {
                                    service.ServiceId = Convert.ToInt32(v2.ServiceId);
                                    service.ServiceTimes = 1;
                                    lstServiceStatistic.Add(service);
                                }
                                else
                                {
                                    _ServiceTmp = new ServiceStatistic();
                                    service = lstServiceStatistic.Find(f => f.ServiceId == v2.ServiceId);
                                    _ServiceTmp.ServiceId = service.ServiceId;
                                    _ServiceTmp.ServiceTimes = service.ServiceTimes + 1;
                                    lstServiceStatistic[index] = _ServiceTmp;
                                }
                            }
                        }

                        totalservice += v.ServiceIds.Count();

                        log(v.Id + " : " + v.Fullname + " : " + v.ServiceIds.Count());
                        if (lstServiceStatistic.Count > 0)
                        {
                            foreach (var v3 in lstServiceStatistic)
                            {
                                log("Dịch vụ : " + v3.ServiceId + " : " + v3.ServiceTimes);
                            }
                        }
                    }
                    log("total service : " + totalservice);
                }

                if (LST0.Count > 0)
                {
                    log("LST0================== " + LST0.Count.ToString());
                    foreach (var v in LST0)
                    {
                        log(v.Id + " : " + v.Fullname);
                    }
                }

                if (LST.Count > 0)
                {
                    log("LST================== " + LST.Count.ToString());
                }
            }
        }

        public void log(string strlog)
        {
            Response.Write(strlog + "<br>");
        }

        static string GenPassword(MD5 md5Hash, string input)
        {
            var scrPush = "uaefsfkoiu&@(^%=";
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input + scrPush));
            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }

    }

    public struct Staff_TimeKeeping
    {
        public int Id { get; set; }
        public string Fullname { get; set; }
        public int Type { get; set; }
        public int IdShowroom { get; set; }
    }

    public struct ServiceStatistic
    {
        public int ServiceId { get; set; }
        public int ServiceName { get; set; }
        public int ServiceTimes { get; set; }
    }
}