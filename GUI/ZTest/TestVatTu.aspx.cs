﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.ZTest
{
    public partial class TestVatTu : System.Web.UI.Page
    {
        Solution_30shineEntities db = new Solution_30shineEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindListData(5, 3, 99);
                BindHead(5);
            }
            //try
            //{
            //    var times = 5;
            //    var listQuantity = this.InitListQuantity(times);
            //    var itemQuantity = new ItemQuantity();
            //    var listStaff = new List<ItemStaff>();
            //    var itemStaff = new ItemStaff();

            //    var db = new Solution_30shineEntities();
            //    var listData = db.Statistics_XuatVatTu.Where(w => w.IsDelete == false)
            //        .OrderByDescending(o => o.Date_XuatVatTu)
            //        .OrderByDescending(o => o.CreateTime).ToList();
            //    var index = -1;
            //    if (listData.Count > 0)
            //    {
            //        foreach (var v in listData)
            //        {
            //            index = listStaff.FindIndex(w => w.StaffId == v.Staff_ID);

            //            if (index == -1)
            //            {
            //                itemStaff = new ItemStaff();
            //                itemStaff.StaffId = v.Staff_ID.Value;
            //                itemStaff.StaffName = "v.StaffName-" + v.Staff_ID;
            //                itemStaff.ListQuantity = this.InsertListQuantity(listQuantity, v.TotalBill_XuatVatTu.Value);
            //                listStaff.Add(itemStaff);
            //            }
            //            else
            //            {
            //                itemStaff = listStaff[index];
            //                itemStaff.ListQuantity = this.InsertListQuantity(itemStaff.ListQuantity, v.TotalBill_XuatVatTu.Value);
            //                listStaff[index] = itemStaff;
            //            }
            //        }
            //    }
            //    var a = listStaff;
            //}
            //catch (Exception)
            //{
            //    throw;
            //}
        }

        protected void BindListData(int Number, int Salon_ID, int Staff_ID)
        {
            try
            {
                int CountListStaff = 0;
                List<Staff> LstStaff = new List<Staff>();
                List<Statistics_XuatVatTu> Lst_XuatVatTu = new List<Statistics_XuatVatTu>();
                List<Item> result = new List<Item>();
                if (Staff_ID > 0)
                {
                    LstStaff = db.Staffs.Where(a => a.IsDelete == 0 && a.Id == Staff_ID).ToList();
                }
                else
                {
                    LstStaff = db.Staffs.Where(a => a.IsDelete == 0 && a.SalonId == Salon_ID).ToList();
                }
                CountListStaff = LstStaff.Count;
                if (CountListStaff > 0)
                {
                    for (int i = 0; i < CountListStaff; i++)
                    {
                        var staff = LstStaff[i];
                        Lst_XuatVatTu = db.Statistics_XuatVatTu.Where(a => a.IsDelete == false && a.Staff_ID == staff.Id).OrderByDescending(a => a.Date_XuatVatTu).OrderByDescending(a => a.CreateTime).Take(Number).ToList();
                        if (Lst_XuatVatTu.Count < Number)
                        {
                            int div = Number - Lst_XuatVatTu.Count;
                            List<Statistics_XuatVatTu> range = new List<Statistics_XuatVatTu>(div);
                            Lst_XuatVatTu.AddRange(range);
                        }
                        else
                        {
                            //
                        }
                        var item = new Item(staff.Fullname, Lst_XuatVatTu);
                        result.Add(item);
                    }
                    //bind data result;
                    var Data = result;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        protected void BindHead(int Number)
        {
            try
            {
                List<ItemNumber> LstItemNumber = new List<ItemNumber>();
                ItemNumber ItemNumber = new ItemNumber();
                for (int i = 0; i < Number; i++)
                {
                    ItemNumber = new ItemNumber();
                    ItemNumber.NameNumber = "Số lần  " + (Number - i) + "";
                    LstItemNumber.Add(ItemNumber);
                }
                var ItemNumber0 = new ItemNumber();
                ItemNumber0.NameNumber = "Số lần";
                LstItemNumber.Insert(0, ItemNumber0);
                var resut = LstItemNumber;

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        private List<ItemQuantity> InitListQuantity(int elementQuantity)
        {
            try
            {
                var listItemQuantity = new List<ItemQuantity>();
                var itemQuantity = new ItemQuantity();
                for (int i = 1; i <= elementQuantity; i++)
                {
                    itemQuantity = new ItemQuantity();
                    itemQuantity.Order = i;
                    itemQuantity.Quantity = 0;
                    itemQuantity.IsSet = false;
                    listItemQuantity.Add(itemQuantity);
                }
                return listItemQuantity;
            }
            catch (Exception)
            {

                throw;
            }
        }

        private List<ItemQuantity> InsertListQuantity(List<ItemQuantity> listQuantity, int quantity)
        {
            try
            {
                if (listQuantity.Count > 0)
                {
                    var loop = 0;
                    var temp = listQuantity;
                    foreach (var v in temp)
                    {
                        if (!v.IsSet)
                        {
                            listQuantity[loop].Quantity = quantity;
                            listQuantity[loop].IsSet = true;
                            break;
                        }
                        loop++;
                    }
                }
                return listQuantity;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public class ItemQuantity
        {
            public int Order { get; set; }
            public int Quantity { get; set; }
            public bool IsSet { get; set; }
        }

        public class ItemStaff
        {
            public int StaffId { get; set; }
            public string StaffName { get; set; }
            public List<ItemQuantity> ListQuantity { get; set; }
        }

        public class Item
        {
            public string StaffName { get; set; }
            public List<Statistics_XuatVatTu> list { get; set; }
            public Item(string staffName, List<Statistics_XuatVatTu> list)
            {
                this.StaffName = staffName;
                this.list = list;
            }
        }
        public class ItemNumber
        {
            public string NameNumber { get; set; }
        }

    }
}