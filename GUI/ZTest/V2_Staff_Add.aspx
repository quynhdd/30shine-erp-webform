﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="V2_Staff_Add.aspx.cs" Inherits="_30shine.GUI.ZTest.V2_Staff_Add" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="StaffAdd" ContentPlaceHolderID="CtMain" runat="server">
<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

<style>
    .staff-servey-mark span { height: 28px !important; line-height: 28px !important; }
    .staff-servey-mark td input[type="radio"] { top: 4px !important; }
</style>

<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">
                <li>Quản lý Nhân sự &nbsp;&#187; </li>
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add">
    <%-- Add --%>                
    <div class="wp960 content-wp">
        <!-- System Message -->
        <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
        <!-- END System Message -->

        <div class="table-wp">
            <table class="table-add">
                <tbody>
                    <tr class="title-head">
                        <td><strong>Thông tin nhân viên</strong></td>
                        <td></td>
                    </tr>
                    <tr id="TrSalon" runat="server">
                        <td class="col-xs-2 left"><span>Salon</span></td>
                        <td class="col-xs-9 right"><span class="field-wp">
                            <asp:DropDownList ID="ddlSalon" runat="server" ClientIDMode="Static" Enabled="false">
                            </asp:DropDownList>
                  </span></td>
                    </tr>
                    <tr>
                        <td class="col-xs-2 left"><span>Phân quyền</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:DropDownList ID="PermissionList" runat="server" Enabled="false" ClientIDMode="Static"></asp:DropDownList>
                               
                            </span>
                        </td>
                    </tr>

                    <tr>
                        <td class="col-xs-2 left"><span>Bộ phận</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:DropDownList ID="TypeStaff" Enabled ="false" runat="server" ClientIDMode="Static"></asp:DropDownList>
                               
                            </span>
                        </td>
                    </tr>

                    <tr>
                        <td class="col-xs-2 left"><span>Họ và tên</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="FullName" runat="server" ClientIDMode="Static"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="FullNameValidate" ControlToValidate="FullName" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập họ tên!"></asp:RequiredFieldValidator>
                            </span>
                                            
                        </td>
                    </tr>

                    <tr>
                        <td class="col-xs-2 left"><span>Số CMND</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="StaffID" runat="server" ClientIDMode="Static"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="Phone" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập số điện thoại!"></asp:RequiredFieldValidator>--%>
                            </span>
                        </td>
                    </tr>

                    <tr class="tr-field-ahalf">
                        <td class="col-xs-2 left"><span>Ngày bắt đầu</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="DateJoin" CssClass="txtDateTime" runat="server" ClientIDMode="Static"></asp:TextBox>
                            </span>
                        </td>
                    </tr>

                    <tr>
                        <td class="col-xs-2 left"><span>Số điện thoại</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="Phone" runat="server" ClientIDMode="Static"></asp:TextBox>
                            </span>
                        </td>
                    </tr>

                    <tr>
                        <td class="col-xs-2 left"><span>Email (Tên đăng nhập)</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="Email" runat="server" ClientIDMode="Static" placeholder="example@gmail.com"></asp:TextBox>
                            </span>
                        </td>
                    </tr>

                    <tr class="tr-birthday">
                        <td class="col-xs-2 left"><span>Sinh nhật</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="Day" runat="server" ClientIDMode="Static" placeholder="Ngày"></asp:TextBox>
                                <asp:TextBox ID="Month" runat="server" ClientIDMode="Static" placeholder="Tháng"></asp:TextBox>
                                <asp:TextBox ID="Year" runat="server" ClientIDMode="Static" placeholder="Năm"></asp:TextBox>
                            </span>
                        </td>
                    </tr>

                    <tr>
                        <td class="col-xs-2 left"><span>Giới tính</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:DropDownList ID="Gender" runat="server" ClientIDMode="Static"></asp:DropDownList>
                            </span>
                        </td>
                    </tr>

                            <tr>
                                <td class="col-xs-2 left"><span>Địa chỉ</span></td>
                                <td class="col-xs-9 right">
                         
<%--                                            <div class="city">
                                                <asp:DropDownList ID="ddlCity" runat="server" CssClass="select" Enabled ="false" ClientIDMode="Static">
                                                </asp:DropDownList>
                                            </div>--%>
                     
                                    <asp:TextBox ID="Address" runat="server" placeholder="Địa chỉ" ClientIDMode="Static"></asp:TextBox>
                                </td>
                            </tr>

                            <tr class="tr-upload">
                                <td class="col-xs-3 left"><span>Ảnh avatar</span></td>
                                <td class="col-xs-7 right">
                                    <div class="wrap btn-upload-wp HDF_MainImg" style="margin-bottom: 5px;">
                                        <div id="Btn_UploadImg" class="btn-upload-img" onclick="popupImageIframe('HDF_MainImg')">Chọn ảnh đăng</div>
                                        <asp:FileUpload ID="UploadFile" ClientIDMode="Static" style="display:none;"
                                            CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                        <div class="wrap listing-img-upload">
                                            <% if(HasImages){ %>   
                                            <% for (var i = 0; i < ListImagesUrl.Count; i++)
                                                { %>
                                                <div class="thumb-wp">
                                                    <img class="thumb" alt="" title="" src="<%=ListImagesUrl[i] %>" 
                                                        data-img="<%=ListImagesUrl[i] %>" />
                                                    <span class="delete-thumb" onclick="deleteThumbnail($(this), 'HDF_MainImg')"></span>
                                                </div>
                                            <% } %>
                                            <% } %>
                                        </div>
                                    </div>                         
                                </td>
                            </tr>
                              <tr class="tr-field-ahalf">
                                    <td class="col-xs-2 left"><span>Nguồn lao động</span></td>
                                    <td class="col-xs-9 right">
                                        <span class="field-wp" id="everything">
                                            <asp:DropDownList runat="server" CssClass="form-control select" Width="100%" ID="ddlNguonLaoDong" Enabled="false" ClientIDMode="Static"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="rfvType" runat="server" ControlToValidate="ddlNguonLaoDong"
                                                InitialValue="0" ErrorMessage="Vui lòng chọn nguồn lao động!!!"></asp:RequiredFieldValidator>
                                        </span>
                                    </td>
                                </tr>
                    <tr class="tr-send">
                        <td class="col-xs-2 left"></td>
                        <td class="col-xs-9 right no-border">
                            <span class="field-wp">
                                <asp:Button ID="Send" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="AddStaff"></asp:Button>
                                <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>
                            </span>
                        </td>
                    </tr>
                    <asp:HiddenField runat="server" ID="UrlKeyField"  ClientIDMode="Static"/>
                    <asp:HiddenField runat="server" ID="ImgCoverField"  ClientIDMode="Static"/>
                    <asp:HiddenField ID="HDF_MainImg" runat="server" ClientIDMode="Static"/>
                </tbody>
            </table>
        </div>
    </div>
    <%-- end Add --%>
</div>

<script>
    jQuery(document).ready(function () {
        $("#glbAdminContent").addClass("active");
        $("#glbAdminStaff").addClass("active");

        //============================
        // Datepicker
        //============================
        $('.txtDateTime').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'vi',
            startDate: '2014/10/10',
            format: 'd/m/Y',
            dateonly: true,
            showHour: false,
            showMinute: false,
            timepicker: false,
            onChangeDateTime: function (dp, $input) {
                //$input.val($input.val().split(' ')[0]);
            },
            scrollMonth: false,
            scrollTime: false,
            scrollInput: false
        });
    });
</script>

<!-- Popup plugin image -->
<script type="text/javascript">
    function popupImageIframe(StoreImgField) {
        var Width = 960;
        var Height = 560;
        var ImgList = [];
        var iframe = $("<div class='iframe-image-upload' style='width:" + Width + "px; height:" + Height + "px; float:left; display:none;'><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + StoreImgField + "&callback=getThumbFromIframe&imgWidth=800&imgHeight=600&multi_img=false&Folder=NhanVien' style='width:100%; height: 100%;'></iframe></div");

        $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
    }

    window.getThumbFromIframe = function (Imgs, StoreImgField) {
        var imgs = "";
        var thumb = "";
        if (Imgs.length > 0) {
            thumb = '<div class="thumb-wp">' +
                        '<img class="thumb" alt="" title="" src="' + Imgs[0] + '"' +
                            'data-img="" />' +
                        '<span class="delete-thumb" onclick="deleteThumbnail($(this), \'' + StoreImgField + '\')"></span>' +
                    '</div>';
            $("." + StoreImgField).find(".listing-img-upload").empty().append(thumb);
            excThumbWidth(120, 120);
            $("#" + StoreImgField).val(Imgs[0]);
        }
        autoCloseEBPopup();
    }

    function deleteThumbnail(This, StoreImgField) {
        var imgs = "";
        var lst = This.parent().parent();
        This.parent().remove();
        lst.find("img.thumb").each(function () {
            imgs += $(this).attr("src") + ",";
        });
        $("#" + StoreImgField).val(imgs);
    }
</script>
<!-- Popup plugin image -->

</asp:Panel>
</asp:Content>


