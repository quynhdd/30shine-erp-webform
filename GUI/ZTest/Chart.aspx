﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Chart.aspx.cs" Inherits="_30shine.GUI.ZTest.Chart" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/Public/Plugins/chartist/dist/chartist.min.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <script src="/Public/Plugins/chartist/dist/chartist.min.js"></script>
        <div style="width: 500px; margin: 20px auto;">
            <div class="ct-chart ct-golden-section" id="chart1"></div>
            <div class="ct-chart ct-golden-section" id="chart2"></div>
            <div class="ct-chart ct-golden-section" id="chart-stack"></div>
        </div>        


        <script>
            // Initialize a Line chart in the container with the ID chart1
            new Chartist.Line('#chart1', {
                labels: [1, 2, 3, 4, 5],
                series: [[100, 120, 180, 200, 221]]
            }).on("draw", function (data) {
                //
            });

            // Initialize a Line chart in the container with the ID chart2
            new Chartist.Bar('#chart2', {
                labels: ["57 <br/> Số 7 Cát Linh", "346 Khâm Thiên", "82 Trần Đại Nghĩa", "82 Trần Đại Nghĩa", "82 Trần Đại Nghĩa", "82 Trần Đại Nghĩa", "82 Trần Đại Nghĩa"],
                series: [[57, 121, 63, 77, 190, 88, 65]]
            }).on("draw", function (data) {
                var barHorizontalCenter, barVerticalCenter, label, value;
                if (data.type === 'bar') {
                    data.element.attr({
                        style: 'stroke-width: 30px'
                    });
                    barHorizontalCenter = data.x1 + (data.element.width() * .5);
                    barVerticalCenter = data.y1 + (data.element.height() * -1) - 10;
                    value = data.element.attr('ct:value');
                    if (value !== '0') {
                        label = new Chartist.Svg('text');
                        label.text(euro(value));
                        label.addClass("ct-barlabel");
                        label.attr({
                            x: barHorizontalCenter,
                            y: barVerticalCenter,
                            'text-anchor': 'middle'
                        });
                        return data.group.append(label);
                    }
                }
            });

            new Chartist.Bar('#chart-stack', {
                labels: ['Q1', 'Q2', 'Q3', 'Q4'],
                series: [
                  [800000, 1200000, 1400000, 1300000],
                  [200000, 400000, 500000, 300000],
                  [100000, 200000, 400000, 600000]
                ]
            }, {
                stackBars: true,
                axisY: {
                    labelInterpolationFnc: function (value) {
                        return (value / 1000) + 'k';
                    }
                }
            }).on('draw', function (data) {
                var barHorizontalCenter, barVerticalCenter, label, value;
                if (data.type === 'bar') {
                    data.element.attr({
                        style: 'stroke-width: 30px'
                    });
                    barHorizontalCenter = data.x1 + (data.element.width() * .5);
                    barVerticalCenter = data.y1 + (data.element.height() * -1) - 10;
                    value = data.element.attr('ct:value');
                    if (value !== '0') {
                        label = new Chartist.Svg('text');
                        label.text(euro(value));
                        label.addClass("ct-barlabel");
                        label.attr({
                            x: barHorizontalCenter,
                            y: barVerticalCenter,
                            'text-anchor': 'middle'
                        });
                        return data.group.append(label);
                    }
                }
            });
            var euro = function (x) {
                return '€' + x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
            };
        </script>
    </form>
</body>
</html>