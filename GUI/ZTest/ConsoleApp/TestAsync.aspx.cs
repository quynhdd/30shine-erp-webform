﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.ZTest.ConsoleApp
{
    public partial class TestAsync : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Write("Run...<br>");
            RegisterAsyncTask(new PageAsyncTask(AsyncPrint));
            //AsyncPrint();
            Response.Write("Lala...");
            Response.Redirect("https://google.com");            
        }

        public async Task AsyncPrint()
        {
            for (var i = 0; i < 100; i++)
            {
                SingletonA.Instance.PrintBillId();
            }
        }
    }
}