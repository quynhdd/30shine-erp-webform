﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace _30shine.GUI.ZTest.ConsoleApp
{
    public sealed class SingletonA
    {
        public string PATH_ASYNC_CHECKOUT_BILL_LOG = @"D:\30ShineSystemLog\TestLog\" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + "_async_checkout_bill_statistic.txt";
        public string PATH_ASYNC_CHECKOUT_BILL_LOG_EXCEPTION = @"D:\30ShineSystemLog\TestLog\" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + "_async_checkout_bill_statistic_exception.txt";

        private static volatile SingletonA instance;
        private static object syncRoot = new Object();
        private Solution_30shineEntities db;

        private SingletonA() {
            db = new Solution_30shineEntities();
        }

        public static SingletonA Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new SingletonA();
                    }
                }

                return instance;
            }
        }

        public async Task PrintBillId()
        {
            try
            {
                Library.Function.WriteToFile("Test async " + DateTime.Now + " succeed: ", PATH_ASYNC_CHECKOUT_BILL_LOG);

                var rd = new Random();
                int id = rd.Next(1,1000000);
                HttpContext.Current.Response.Write("Start " + DateTime.Now + " : " + id + "<br/>");
                var bill = db.BillServices.Where(w=>w.Id == id).FirstOrDefault();                
                if (bill != null)
                {
                    HttpContext.Current.Response.Write("Print " + DateTime.Now + " : " + id + "<br/>");
                }
                else
                {
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.StackTrace);
            }
        }
    }
}