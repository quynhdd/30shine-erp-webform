﻿using PdfFileWriter;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Printing;

namespace _30shine.GUI.ZTest
{
    public partial class Print2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
        }

        public PdfDocument Document;
        private Font DefaultFont;
        private Int32 PageNo;

        ////////////////////////////////////////////////////////////////////
        // Create charting examples PDF document
        ////////////////////////////////////////////////////////////////////

        public void Test( Boolean Debug, String FileName )
        {
            // Step 1: Create empty document
            // Arguments: page width: 8.5”, page height: 11”, Unit of measure: inches
            // Return value: PdfDocument main class
            Document = new PdfDocument(PaperType.Letter, false, UnitOfMeasure.Inch, FileName);

            // Debug property
            // By default it is set to false. Use it for debugging only.
            // If this flag is set, PDF objects will not be compressed, font and images will be replaced
            // by text place holder. You can view the file with a text editor but you cannot open it with PDF reader.
            Document.Debug = Debug;

            // create default font for printing
            DefaultFont = new Font("Arial", 10.0F, FontStyle.Regular);

            // start page number
            PageNo = 1;

            // create PrintPdfDocument
            PdfImageControl ImageControl = new PdfImageControl();
            ImageControl.Resolution = 300.0;
            ImageControl.SaveAs = SaveImageAs.BWImage;
            PdfPrintDocument Print = new PdfPrintDocument(Document, ImageControl);

            // the method that will print one page at a time to PrintDocument
            Print.PrintPage += PrintPage;

            // set margins 
            Print.SetMargins(1.0, 1.0, 1.0, 1.0);

            // crop the page image result to reduce PDF file size
            Print.PageCropRect = new RectangleF(0.95F, 0.95F, 6.6F, 9.1F);

            // initiate the printing process (calling the PrintPage method)
            // after the document is printed, add each page an an image to PDF file.
            Print.AddPagesToPdfDocument();

            // dispose of the PrintDocument object
            Print.Dispose();

            // create the PDF file
            Document.CreateFile();

            // start default PDF reader and display the file
            Process Proc = new Process();
            Proc.StartInfo = new ProcessStartInfo(FileName);
            Proc.Start();

            // exit
            return;
        }

        ////////////////////////////////////////////////////////////////////
        // Print each page of the document to PrintDocument class
        // You can use standard PrintDocument.PrintPage(...) method.
        // NOTE: The graphics origin is top left and Y axis is pointing down.
        // In other words this is not PdfContents printing.
        ////////////////////////////////////////////////////////////////////

        public void PrintPage(object sender, PrintPageEventArgs e)
        {
            // graphics object short cut
            Graphics G = e.Graphics;

            // Set everything to high quality
            G.SmoothingMode = SmoothingMode.HighQuality;
            G.InterpolationMode = InterpolationMode.HighQualityBicubic;
            G.PixelOffsetMode = PixelOffsetMode.HighQuality;
            G.CompositingQuality = CompositingQuality.HighQuality;

            // print area within margins
            Rectangle PrintArea = e.MarginBounds;

            // draw rectangle around print area
            G.DrawRectangle(Pens.Black, PrintArea);

            // line height
            Int32 LineHeight = DefaultFont.Height + 8;
            Rectangle TextRect = new Rectangle(PrintArea.X + 4, PrintArea.Y + 4, PrintArea.Width - 8, LineHeight);

            // display page bounds
            String text = String.Format("Page Bounds: Left {0}, Top {1}, Right {2}, Bottom {3}", e.PageBounds.Left, e.PageBounds.Top, e.PageBounds.Right, e.PageBounds.Bottom);
            G.DrawString(text, DefaultFont, Brushes.Black, TextRect);
            TextRect.Y += LineHeight;

            // display print area
            text = String.Format("Page Margins: Left {0}, Top {1}, Right {2}, Bottom {3}", PrintArea.Left, PrintArea.Top, PrintArea.Right, PrintArea.Bottom);
            G.DrawString(text, DefaultFont, Brushes.Black, TextRect);
            TextRect.Y += LineHeight;

            // print some lines
            for (Int32 LineNo = 1; ; LineNo++)
            {
                text = String.Format("Page {0}, Line {1}", PageNo, LineNo + "Họ và tên Châu Du Dân Nguyễn Kiều Oanh");
                G.DrawString(text, DefaultFont, Brushes.Black, TextRect);
                TextRect.Y += LineHeight;
                if (TextRect.Bottom > PrintArea.Bottom) break;
            }

            // move on to next page
            PageNo++;
            e.HasMorePages = PageNo <= 1;
            return;
        }
    }
}