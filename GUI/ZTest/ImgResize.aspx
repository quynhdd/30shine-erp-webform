﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ImgResize.aspx.cs" Inherits="_30shine.GUI.ZTest.ImgResize" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="/Assets/js/jquery.v1.11.1.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="wp">
            <div class="thumb-wp">
                <img src="/Assets/images/lock.png" />
            </div>
        </div>        
    </div>
      
    </form>

<style>
    .thumb-wp { width: 200px; height: 500px; float: left; background: green;}
    .wp { width: 500px; margin: 100px auto;}
</style>

<script type="text/ecmascript">
    jQuery(document).ready(function ($) {
        $(window).load(function () {
            var StdWidth = $(".thumb-wp").width();
            var StdHeight = $(".thumb-wp").height();
            var ImgWidth = $(".thumb-wp img").width();
            var ImgHeight = $(".thumb-wp img").height();
            var left = 0;
            var top = 0;

            console.log(StdWidth + " | " + StdHeight + " | " + ImgWidth + " | " + ImgHeight);
            var Width = StdWidth;
            var Height = Width * ImgHeight / ImgWidth;
            if (Height < StdHeight) {
                Height = StdHeight;
                Width = Height * ImgWidth / ImgHeight;
                left = (StdWidth - Width) / 2;
            }
            else {
                top = (StdHeight - Height) / 2;
            }
            console.log(Width + " | " + Height);

            $(".thumb-wp img").css({
                "width": Width,
                "height": Height,
                "margin-top": top,
                "margin-left" : left
            });
        });
    });
</script>

</body>
</html>
