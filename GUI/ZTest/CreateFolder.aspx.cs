﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.ZTest
{
    public partial class CreateFolder : System.Web.UI.Page
    {
        private string ImageFolder = "/Public/Resource/AnhKhachHang/";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                try
                {
                    string timeFrom = textFrom.Text;
                    string timeTo = textTo.Text;
                    int salonId = Convert.ToInt32(textSalonId.Text);

                    createFolder_AnhKhachHang(timeFrom, timeTo, salonId);
                }
                catch (Exception ex)
                {
                    textMessage.Text = ex.Message;
                }
            }            
        }

        public void createFolder_AnhKhachHang(string timeFrom, string timeTo, int salonId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var salonFolderName = "TranDaiNghia_7_3_2017";
                var list = db.Store_AnhKhachHang_BySalonId(timeFrom, timeTo, salonId).ToList();
                if (list.Count > 0)
                {
                    foreach (var v in list)
                    {
                        string dirFullPath = HttpContext.Current.Server.MapPath("~" + ImageFolder + "/" + salonFolderName + "/" + v.CustomerName + "_" + v.CustomerCode);
                        createDir(dirFullPath);
                    }
                }
            }
        }

        public void createDir(string pathDir)
        {
            // If the directory doesn't exist, create it.
            if (!Directory.Exists(pathDir))
            {
                Directory.CreateDirectory(pathDir);
            }
        }
    }
}