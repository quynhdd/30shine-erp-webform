﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using TheArtOfDev.HtmlRenderer.WinForms;
using TheArtOfDev.HtmlRenderer.Core;
using System.Text.RegularExpressions;
using System.Configuration;

namespace _30shine.GUI.ZTest
{
    public partial class Print : System.Web.UI.Page
    {
        private Font printFont;
        private string stringToPrint = "";
        Font Font = new Font("Arial", 13, GraphicsUnit.Pixel);
        private int x = 1;
        private int y = 2;
        protected string ImgUrlTestPrint;

        protected void Page_Load(object sender, EventArgs e)
        {
            //string printerName = "";
            //for (int i = 0; i < PrinterSettings.InstalledPrinters.Count; i++)
            //{
            //    printerName = PrinterSettings.InstalledPrinters[i];
            //    Response.Write(printerName + "<br/>");
            //}
            ImgUrlTestPrint = ConfigurationManager.AppSettings["img_url_testprint"];
        }

        protected void GenPDF(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter tw = new StringWriter(sb);
            HtmlTextWriter hw = new HtmlTextWriter(tw);
            PrintBill.RenderControl(hw);
            var htmlContent = sb.ToString();

            int integer;
            int h = int.TryParse(HDF_PrintBillHeight.Value, out integer) ? integer : 0;            
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            htmlToPdf.PageWidth = 65;
            htmlToPdf.PageHeight = pixelToMM(h);
            htmlToPdf.Margins.Left = 0;
            htmlToPdf.Margins.Right = 0;
            htmlToPdf.Margins.Top = 0;
            htmlToPdf.Margins.Bottom = 0;
            var pdfBytes = htmlToPdf.GeneratePdf("<html><head><meta charset='utf-8' /></head><body>" + htmlContent + "</body></html>");

            string filename = HttpContext.Current.Server.MapPath("~/Public/PDF/hello.pdf");
            File.WriteAllBytes(filename, pdfBytes);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "openPdfIframe('/Public/PDF/hello.pdf');", true);
            //Response.ContentType = "application/pdf";
            //Response.ContentEncoding = System.Text.Encoding.UTF8;
            //Response.AddHeader("Content-Disposition", "Inline; filename=TEST.pdf");
            //Response.BinaryWrite(pdfBytes);
            //Response.Flush();
            //Response.End();            
        }

        private int pixelToMM(float pixel) 
        {
            return (int)(pixel * 0.2645833333333);
        }

        protected void SaveBillImg()
        {
            StringBuilder sb = new StringBuilder();
            StringWriter tw = new StringWriter(sb);
            HtmlTextWriter hw = new HtmlTextWriter(tw);
            PrintBill.RenderControl(hw);
            var html = sb.ToString();
            string str_image = "/Assets/images/htmlrendertoimage_" + Guid.NewGuid().ToString() + ".jpg";
            string pathToSave = HttpContext.Current.Server.MapPath("~" + str_image);

            string height = PrintBill.Style["height"];
            height = HDF_PrintBillHeight.Value;
            Regex regex = new Regex("[^0-9]*");
            height = regex.Replace(height, "");
            int h = height != "" ? Convert.ToInt32(height) : 200;

            System.Drawing.Image img = TheArtOfDev.HtmlRenderer.WinForms.HtmlRender.RenderToImage(html, new Size(180, h));
            img.Save(pathToSave);
        }

        protected void BtnClick_SaveBillImg(object sender, EventArgs e)
        {
            SaveBillImg();
        }

        protected void BtnClick_Print(object sender, EventArgs e)
        {
            Printing();
        }

        private void Printing()
        {
            PrintDocument pd = new PrintDocument();

            pd.PrinterSettings.PrinterName = "RP58 Printer";
            pd.OriginAtMargins = true;
            PaperSize pageSize = new PaperSize();
            //pageSize.RawKind = 512; //this is number of created custom size 563x1251
            //pageSize.Height = 50;
            Margins margins = new Margins(3, 0, 0, 0);
            pd.DefaultPageSettings.Margins = margins;
            pd.DefaultPageSettings.Landscape = false;
            pd.PrintPage += new PrintPageEventHandler(pd_PrintPage);
            pd.Print();
        }

        private void pd_PrintPage(object sender, PrintPageEventArgs e)
        {     
            // Chiều ngang khổ in : 189px
            List<KeyValuePair<string, string>> items = new List<KeyValuePair<string, string>>();

            // Print logo
            //System.Drawing.Image img = System.Drawing.Image.FromFile(@"C:\logo.jpg");
            //var imgRec = new Rectangle(new Point((e.MarginBounds.Width - 100)/2 , y), new Size(100, 60));
            //e.Graphics.DrawImage(img, imgRec);
            x = 1;
            y += 65;

            // Render top
            items = new List<KeyValuePair<string, string>>();
            items.Add(new KeyValuePair<string, string>("Ngày " + String.Format("{0:dd/MM/yyyy}", DateTime.Now), ""));
            Render_Table(e, Brushes.Black, Pens.White, items, 184, 0, 22);

            // Render Khách hàng
            items = new List<KeyValuePair<string, string>>();
            items.Add(new KeyValuePair<string, string>("KH", "Châu Kiệt Luân"));
            items.Add(new KeyValuePair<string, string>("Mã KH", "KT1000000"));
            Render_Table(e, Brushes.Black, Pens.White, items, 70, 114, 22);

            // Render service table
            items = new List<KeyValuePair<string, string>>();
            items.Add(new KeyValuePair<string, string>("Dịch vụ", "SL"));
            items.Add(new KeyValuePair<string, string>("Shine Master", "1"));
            items.Add(new KeyValuePair<string, string>("Gội đầu Massage", "1"));
            //items.Add(new KeyValuePair<string, string>("Gội đầu MassageGội đầu Massage Gội đầu Massage", "1"));
            Render_Table(e, Brushes.Black, Pens.Black, items, 154, 30, 28);

            // Render stylist, skinner
            items = new List<KeyValuePair<string, string>>();
            items.Add(new KeyValuePair<string, string>("Stylist", "Lã Mạnh Phúc"));
            items.Add(new KeyValuePair<string, string>("Skinner", ""));
            items.Add(new KeyValuePair<string, string>("Thành tiền", "170.000 VNĐ"));
            Render_Table(e, Brushes.Black, Pens.White, items, 70, 114, 22);

            // Render product table
            items = new List<KeyValuePair<string, string>>();
            items.Add(new KeyValuePair<string, string>("Sản phẩm", "SL"));
            items.Add(new KeyValuePair<string, string>(" ", ""));
            items.Add(new KeyValuePair<string, string>(" ", ""));
            items.Add(new KeyValuePair<string, string>(" ", ""));
            Render_Table(e, Brushes.Black, Pens.Black, items, 154, 30, 28);

            // Render seller, total money
            items = new List<KeyValuePair<string, string>>();
            items.Add(new KeyValuePair<string, string>("Seller", ""));
            items.Add(new KeyValuePair<string, string>("Thành tiền", ""));
            items.Add(new KeyValuePair<string, string>("Tổng số", "170.000 VNĐ"));
            Render_Table(e, Brushes.Black, Pens.White, items, 70, 114, 22);
                        
        }

        /// <summary>
        /// Drawing table
        /// </summary>
        /// <param name="e">PrintPageEventArgs e</param>
        /// <param name="items">List<KeyValuePair<string, string>> items</param>
        /// <param name="keyWidth">td width for key</param>
        /// <param name="valueWidth">td width for value</param>
        /// <param name="height">td height</param>
        public void Render_Table(PrintPageEventArgs e, Brush brush, Pen pen, List<KeyValuePair<string, string>> items, int keyWidth, int valueWidth, int height)
        {
            if (items.Count > 0)
            {
                // Construct 2 new StringFormat objects
                StringFormat format1 = new StringFormat(StringFormatFlags.NoClip);
                StringFormat format2 = new StringFormat(format1);

                // Set the LineAlignment and Alignment properties for
                // both StringFormat objects to different values.
                format1.LineAlignment = StringAlignment.Center;
                format1.Alignment = StringAlignment.Near;
                format2.LineAlignment = StringAlignment.Center;
                format2.Alignment = StringAlignment.Far;

                Rectangle rec1 = new Rectangle();
                Rectangle rec2 = new Rectangle();
                Size size1 = new Size();
                Size size2 = new Size();
                int numline = 1;

                if (items.Count > 0)
                {
                    // Remap data
                    var Items = new List<PrintRec>();
                    var Item = new PrintRec();
                    foreach (var v in items)
                    {
                        Item = new PrintRec();
                        Item.Key = v.Key;
                        Item.Value = v.Value;
                        numline = Divide_Line(e, Font, v.Key, keyWidth);
                        Item.Height = numline > 1 ? (int)Math.Ceiling((double)height * numline * 0.7) : height * numline;
                        Items.Add(Item);
                    }

                    // Draw content
                    foreach (var v in Items)
                    {
                        size1 = new Size(keyWidth, v.Height);
                        size2 = new Size(valueWidth, v.Height);
                        rec1 = new Rectangle(new Point(x, y), size1);
                        rec2 = new Rectangle(new Point(x + keyWidth, y), size2);

                        e.Graphics.DrawRectangle(pen, rec1);
                        e.Graphics.DrawRectangle(pen, rec2);
                        e.Graphics.DrawString(v.Key, Font,
                            brush, (RectangleF)rec1, format1);
                        e.Graphics.DrawString(v.Value, Font,
                            brush, (RectangleF)rec2, format2);

                        x = 1;
                        y += v.Height;
                    }
                    y++;
                }
            }
        }

        /// <summary>
        /// Divide lines width long string
        /// </summary>
        /// <param name="e"></param>
        /// <param name="font"></param>
        /// <param name="str"></param>
        /// <param name="boundWidth"></param>
        /// <returns></returns>
        public int Divide_Line(PrintPageEventArgs e, Font font, string str, int boundWidth)
        {
            int numberLine = 1;
            if ((int)e.Graphics.MeasureString("a", font).Width > 0)
            {
                int charsWidth = (int)e.Graphics.MeasureString(str, font).Width;
                numberLine = (int)Math.Ceiling((double)charsWidth / boundWidth);
            }
            return numberLine;
        }

        public void htmlRender(PrintPageEventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter tw = new StringWriter(sb);
            HtmlTextWriter hw = new HtmlTextWriter(tw);
            PrintBill.RenderControl(hw);
            var html = sb.ToString();
            //string height = PrintBill.Style["height"];
            string height = HDF_PrintBillHeight.Value;
            Regex regex = new Regex("[^0-9]*");
            height = regex.Replace(height, "");
            int h = height != "" ? Convert.ToInt32(height) : 200;

            PointF point = new PointF(0, 0);
            HtmlRender.AddFontFamily(new FontFamily("Arial"));

            System.Drawing.Image img = HtmlRender.RenderToImage(html, new Size(180, h));
            e.Graphics.DrawImage(img, 0, 0);
        }

        public void drawString(PrintPageEventArgs e)
        {
            string s = String.Format("{0}", "Ngày 2/11/2015") + "\n";
            s += "\n";
            s += String.Format("{0,-10}{1,18}", "Khách hàng", "Châu Kiệt Luân") + "\n";
            s += String.Format("{0,-10}{1,18}", "Mã KH", "KT100000") + "\n";

            s += "\n";
            s += String.Format("{0,-25}{1,3}", "DỊCH VỤ", "SL") + "\n";
            s += String.Format("{0,-25}{1,3}", "Shine Master", "1") + "\n";
            s += String.Format("{0,-25}{1,3}", "Gội đầu Massage", "1") + "\n";
            s += String.Format("{0,-10}{1,18}", "Stylist", "Lã Mạnh Phúc") + "\n";
            s += String.Format("{0,-10}{1,18}", "Skinner", "") + "\n";
            s += String.Format("{0,-10}{1,18:N0}", "Thành tiền", "170.000 VNĐ") + "\n";

            s += "\n";
            s += String.Format("{0,-25}{1,3}", "SẢN PHẨM", "SL") + "\n";
            s += String.Format("{0,-10}{1,18}", "", "") + "\n";
            s += String.Format("{0,-10}{1,18}", "", "") + "\n";
            s += String.Format("{0,-10}{1,18}", "", "") + "\n";

            s += String.Format("{0,-10}{1,18}", "Seller", "") + "\n";
            s += String.Format("{0,-10}{1,18}", "Thành tiền", "") + "\n";
            s += String.Format("{0,-10}{1,18}", "Tổng cộng", "") + "\n";

            e.Graphics.DrawString(s, Font, Brushes.Black,
                e.MarginBounds, StringFormat.GenericTypographic);

            // Sets the value of charactersOnPage to the number of characters 
            // of stringToPrint that will fit within the bounds of the page.
            //e.Graphics.MeasureString(stringToPrint, Font,
            //    e.MarginBounds.Size, StringFormat.GenericTypographic,
            //    out charactersOnPage, out linesPerPage);

            // Draws the string within the bounds of the page
            //e.Graphics.DrawString(stringToPrint, Font, Brushes.Black,
            //    e.MarginBounds, StringFormat.GenericTypographic);

            // Remove the portion of the string that has been printed.
            //stringToPrint = stringToPrint.Substring(charactersOnPage);

            // Check to see if more pages are to be printed.
            //e.HasMorePages = (stringToPrint.Length > 0);
        }
        
    }

    public struct PrintRec
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public int Height { get; set; }
    }
}