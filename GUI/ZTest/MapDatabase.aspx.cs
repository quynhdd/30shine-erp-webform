﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Linq.Expressions;
using LinqKit;
using System.Globalization;
using ExportToExcel;
using System.Web.Script.Serialization;

namespace _30shine.GUI.ZTest
{
    public partial class MapDatabase : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AddLog();

        }

        private void AddLog()
        {

            using (var db = new Solution_30shineEntities())
            {
                var type = "staff";
                var sql = @"select * from Staff";
                var list = db.Database.SqlQuery<Staff>(sql).ToList();
                //for created date
                foreach (Staff obj0 in list)
                {
                    var obj = new Log();
                    //if (obj0.ModifiedDate != null)
                    //    obj0.SkillLevel = 3;
                    var serializer = new JavaScriptSerializer();
                    var content = serializer.Serialize(obj0);
                    obj.OBJId = obj0.Id;
                    obj.Type = type;
                    if (obj0.ModifiedDate == null)
                        obj.LogTime = obj0.CreatedDate;
                    else
                        obj.LogTime = obj0.ModifiedDate;
                    obj.Content = content;
                    //add bang tay
                    obj.Status = 0;
                    db.Logs.Add(obj);
                    db.SaveChanges();
                }

                //for date modify



            }

        }
    }
}