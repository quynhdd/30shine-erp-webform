﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CropImage.aspx.cs" Inherits="_30shine.GUI.ZTest.CropImage" %>


<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Image Crop v3</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0" />
	<link rel="icon" href="data:;base64,iVBORw0KGgo=">
	<link href='http://fonts.googleapis.com/css?family=Bitter|Cantora+One|PT+Serif:400,700' rel='stylesheet' type='text/css'>

    <link href="../../Assets/css/bootstrap/bootstrap.min.css" rel="stylesheet" />
    <link href="../../Assets/css/style.css" rel="stylesheet" />
    <script src="../../Assets/js/bootstrap/bootstrap.js"></script>

	<link rel="stylesheet" type="text/css" href="/Assets/css/plugin.crop/example.css">
	<link rel="stylesheet" type="text/css" href="/Assets/css/plugin.crop/crop.css">
    <script src="/Assets/js/jquery.v1.11.1.js"></script>
	<script src="/Assets/js/plugin.crop/syntaxhighlight.js"></script>
	<script src="/Assets/js/plugin.crop/crop.js"></script>
</head>

<body>

	<div class="example">

		<!-- cropper container element -->
		<div class="default"></div>

	</div>

	<ul class="cmd">
		<li onclick="foo.import();">Import</li>
		<li onclick="foo.zoom('min');">Min</li>
		<li onclick="foo.zoom('max');">Max</li>
		<li onclick="foo.flip();">Flip</li>
		<li onclick="foo.rotate();">Rotate</li>
		<li class="maskbg">Mask</li>
		<li><a id="unqiueID" onclick="foo.download(300, 300, 'test', 'png', this.id);" href="#">Download</a></li>
		<li onclick="console.log(foo.crop(300,300,'png'))" data-tooltip="Prints object to console">Crop</li>
		<li onclick="console.log(foo.original())" data-tooltip="Prints object to console">Original</li>
	</ul>

	<script>

	    //  create a new instance
	    // --------------------------------------------------------------------------

	    // you may do multiple instances of the cropper on a single page
	    // just be sure to give each a unique name
	    var foo = new CROP();

	    foo.init({

	        // element to load the cropper into
	        container: '.default',

	        // image to load, accepts base64 string
	        image: '/Assets/images/test.jpg',

	        // aspect ratio
	        width: 300,
	        height: 300,

	        // prevent image from leaking outside of container. boolean
	        mask: false,

	        // input[range] attributes
	        zoom: {

	            // slider step change
	            steps: 0.01,

	            // minimum and maximum zoom
	            min: 1,
	            max: 5

	        },

	        // optional preview. remove this object if you wish to hide it
	        //preview: {

	        // element to load the preview into
	        //	container: '.pre',

	        // aspect ratio
	        //	ratio: 0.5,

	        //},


	    });


	    //  toggle mask
	    // --------------------------------------------------------------------------

	    $('body').on("click", 'li.maskbg', function () {

	        $('.example').toggleClass('maskbg');
	        $('.default').attr('data-mask', $('.default').attr('data-mask') === 'true' ? 'false' : 'true');
	        $('li.maskbg').html($('li.maskbg').html() == 'Mask' ? 'Unmask' : 'Mask');

	    });


	</script>

</body>
</html>
