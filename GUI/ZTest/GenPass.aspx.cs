﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.ZTest
{
    public partial class GenPass : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            using (MD5 md5Hash = MD5.Create())
            {
                if (IsPostBack)
                {
                    string PasswordGen = GenPassword(md5Hash, textPass.Text);
                    textPassGen.Text = PasswordGen;
                }                
            }
        }

        static string GenPassword(MD5 md5Hash, string input)
        {
            var scrPush = "uaefsfkoiu&@(^%=";
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input + scrPush));
            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }
    }
}