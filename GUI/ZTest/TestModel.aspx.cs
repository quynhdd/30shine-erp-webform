﻿using _30shine.MODEL.BO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.ZTest
{
    public partial class TestModel : System.Web.UI.Page
    {
        private BookingModel bookingModel;
        public TestModel()
        {
            this.bookingModel = new BookingModel();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            // Lấy danh sách booking online
            var listBooking = this.bookingModel.GetListBooking(2, DateTime.Now.Date, DateTime.Now.Date);
        }
    }
}