﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Yogesh.ExcelXml;
using ExportToExcel;
using Excel;

namespace _30shine.GUI.ZTest
{
    public partial class ExportExcel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Example_Import();
            //Example_Export();
        }

        public void Example_Import()
        {
            var Path = Server.MapPath("~") + "Public/Excel/" + "test_28_07_2015_06_28_13.xlsx";
            ImportExcel(Path);
        }

        public void Example_Export()
        {
            var _ExcelHeadRow = new List<string>();
            _ExcelHeadRow.Add("STT");
            _ExcelHeadRow.Add("Field1");
            _ExcelHeadRow.Add("Field2");

            var LST = new List<List<string>>();
            var v = new List<string>();
            v.Add("1");
            v.Add("2");
            v.Add("3");
            LST.Add(v);

            // export
            var ExcelStorePath = Server.MapPath("~") + "Public/Excel/";
            var FileName = "test_" + string.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
            ExportXcel(_ExcelHeadRow, LST, ExcelStorePath + FileName);
        }

        protected void ExportXcel(List<string> _ExcelHeadRow, List<List<string>> List, string Path)
        {
            try
            {
                CreateExcelFile.ExcelHeadRow = _ExcelHeadRow;
                CreateExcelFile.CreateExcel(List, Path);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi. Không tạo được file excel.\r\nException: " + ex.Message);
            }
        }

        protected void ImportExcel(string Path)
        {
            var tblDom = "<table><tbody>";
            foreach (var worksheet in Workbook.Worksheets(@Path))
                foreach (var row in worksheet.Rows)
                {
                    tblDom += "<tr>";
                    foreach (var cell in row.Cells)
                    {                        
                        if (cell != null)
                        {                            
                            tblDom += "<td>" + cell.Text + "</td>";
                        }                        
                    }
                    tblDom += "<tr>";
                }
            tblDom += "</tbody></table>";
            Response.Write(tblDom);
        }
    }

    public struct TestStruct
    {
        public string key1 { get; set; }
        public string key2 { get; set; }
        public string key3 { get; set; }
        public string key4 { get; set; }
        public string key5 { get; set; }
        public string key6 { get; set; }
        public string key7 { get; set; }
        public string key8 { get; set; }
        public string key9 { get; set; }
        public string key10 { get; set; }
        public string key11 { get; set; }
        public string key12 { get; set; }
        public string key13 { get; set; }
        public string key14 { get; set; }
        public string key15 { get; set; }
        public string key16 { get; set; }
        public string key17 { get; set; }
        public string key18 { get; set; }
        public string key19 { get; set; }
        public string key20 { get; set; }
        public string key21 { get; set; }
        public string key22 { get; set; }
        public string key23 { get; set; }
        public string key24 { get; set; }
        public string key25 { get; set; }
    }

    public class ListString : List<string> { }
    public class ListStringLST : List<ListString> { }

}