﻿using _30shine.Helpers;
using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.ZTest
{
    public partial class V2_Staff_Add : System.Web.UI.Page
    {
        public UIHelpers _UIHelpers = new UIHelpers();
        protected int _Code;
        private bool Perm_Access = false;
        private bool Perm_AllPermission = false;

        public bool HasImages = false;
        public List<string> ListImagesUrl = new List<string>();
        public string[] ListImagesName;
        private Solution_30shineEntities db = new Solution_30shineEntities();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                //Bind_SkillLevel();
                Bind_TypeStaff();
                Bind_Gender();
                Bind_Salon();
                //FillTinhThanh();
                //FillQuanHuyen();
                Bind_Nguon();
                Bind_Permission();
            }
        }

        private void Bind_Nguon()
        {
            _Code = Convert.ToInt32(Request.QueryString["Code"]);
            using (var db = new Solution_30shineEntities())
            {
                var lst = (from tn in db.TuyenDung_Nguon join tu in db.TuyenDung_UngVien on tn.Id equals tu.NguonTuyenDungId where tu.Id == _Code select tn).FirstOrDefault();
                if (lst != null)
                {
                    ddlNguonLaoDong.Items.Add(new ListItem(lst.Name, lst.Id.ToString()));
                }
                ddlNguonLaoDong.DataBind();
            }
        }


       

        private void Bind_Permission()
        {
            PermissionList.DataTextField = "Permission";
            PermissionList.DataValueField = "Value";
            var key = 0;

            var item = new ListItem("Nhân viên", "0");
            PermissionList.Items.Insert(key++, item);
            if (Perm_Access)
            {
                item = new ListItem("Lễ tân", "1");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Check-in", "7");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Check-out", "8");
                PermissionList.Items.Insert(key++, item);
                // Hide Lễ tân
                PermissionList.Items.FindByValue("1").Enabled = false;
            }
            if (Perm_AllPermission)
            {
                item = new ListItem("Trưởng cửa hàng", "2");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Kế toán", "3");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Mode1", "4");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Admin", "5");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Khảo sát", "6");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Online store", "9");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Quản lý kho", "10");
                PermissionList.Items.Insert(key++, item);
            }
            PermissionList.SelectedIndex = 0;
        }
        private void Bind_Salon()
        {
            _Code = Convert.ToInt32(Request.QueryString["Code"]);
            using (var db = new Solution_30shineEntities())
            {
                var salonID = db.TuyenDung_UngVien.Where(w => w.Id == _Code).FirstOrDefault();
                FullName.Text = salonID.FullName;
                Phone.Text = salonID.Phone;
                StaffID.Text = salonID.CMT;
                var sl = Convert.ToInt32(salonID.salonId);
                var _salonID = db.Tbl_Salon.Where(s => s.Id == sl).FirstOrDefault();
                if (_salonID != null)
                {
                    ddlSalon.Items.Add(new ListItem(_salonID.Name, _salonID.Id.ToString()));
                }
                ddlSalon.DataBind(); 
            }
        }

        // Fill tỉnh thành
        //protected void FillTinhThanh()
        //{
        //    _Code = Convert.ToInt32(Request.QueryString["Code"]);
        //    using (var db = new Solution_30shineEntities())
        //    {
        //        var lst = (from t in db.TinhThanhs join tu in db.TuyenDung_UngVien on t.ID equals tu.TinhThanhId where tu.Id == _Code select t).FirstOrDefault();

        //        if (lst != null)
        //        {
        //            ddlCity.Items.Add(new ListItem(lst.TenTinhThanh, lst.ID.ToString()));

        //        }
        //        ddlCity.DataBind();
        //    }
        //}

        private void Bind_Gender()
        {
            Gender.DataTextField = "TypeStaff";
            Gender.DataValueField = "Id";
            ListItem item = new ListItem("Chọn giới tính", "0");
            Gender.Items.Insert(0, item);
            item = new ListItem("Nam", "1");
            Gender.Items.Insert(1, item);
            item = new ListItem("Nữ", "2");
            Gender.Items.Insert(2, item);
            item = new ListItem("Khác", "3");
            Gender.Items.Insert(3, item);
        }
        public void Bind_TypeStaff()
        {
            _Code = Convert.ToInt32(Request.QueryString["Code"]);
            using (var db = new Solution_30shineEntities())
            {
                var LST = (from st in db.Staff_Type join tu in db.TuyenDung_UngVien on st.Id equals tu.DepartmentId where tu.Id == _Code select st).FirstOrDefault();
                if (LST != null)
                {
                    TypeStaff.Items.Add(new ListItem(LST.Name, LST.Id.ToString()));
                }
                TypeStaff.DataBind();
            }
        }




        /// <summary>
        /// Check Email
        /// </summary>
        /// Nguyễn Mạnh Dũng
        /// 11/08/2017
        /// <returns></returns>
        public bool CheckIsExists_Emails(Staff objEntity)
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
            var result = db.Staffs.Where(q => q.Email.Equals(objEntity.Email)).Count();
            if (result > 0)
                return true;
            else
                return false;
        }

        public bool CheckIsExists_Phone(Staff objEntity)
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
            var result = db.Staffs.Where(q => q.Phone.Equals(objEntity.Phone)).Count();
            if (result > 0)
                return true;
            else
                return false;
        }


        /// <summary>
        /// Thêm Mới
        /// </summary>
        /// Nguyễn Mạnh Dũng
        /// 11/08/2017
        /// <returns></returns>
        protected void AddStaff(object sender, EventArgs e)
        {

            using (var db = new Solution_30shineEntities())
            {
                var obj = new _30shine.MODEL.ENTITY.EDMX.Staff();
                int integer;
                bool _boolean;
                DateTime datetime;
        
                obj.Fullname = FullName.Text;
                obj.StaffID = StaffID.Text;
                obj.DateJoin = DateTime.TryParse(DateJoin.Text, out datetime) ? datetime : Convert.ToDateTime(null);
                obj.Phone = Phone.Text;
                obj.Email = Email.Text;
                obj.Avatar = HDF_MainImg.Value;
                if (Day.Text.Length > 0)
                {
                    obj.SN_day = int.TryParse(Day.Text, out integer) ? integer : 0;
                }

                if (Month.Text.Length > 0)
                {
                    obj.SN_month = int.TryParse(Month.Text, out integer) ? integer : 0;
                }
                if (Year.Text.Length > 0)
                {
                    obj.SN_year = int.TryParse(Year.Text, out integer) ? integer : 0;
                }
                if (ddlNguonLaoDong.SelectedValue == "2" && (TypeStaff.SelectedValue == "1" || TypeStaff.SelectedValue == "2"))
                {

                    obj.SkillLevel = 3;
                }
                else if (ddlNguonLaoDong.SelectedValue == "1" && (TypeStaff.SelectedValue == "1" || TypeStaff.SelectedValue == "2"))
                {
                    obj.SkillLevel = 4;
       
                }
                else
                {
                    obj.SkillLevel = 0;
                }
                obj.Address = Address.Text;
                obj.CreatedDate = DateTime.Now;
                //obj.CityId = string.IsNullOrEmpty(ddlCity.SelectedValue) ? DBNull.Value : Convert.ToInt32(ddlCity.SelectedValue);
                //obj.DistrictId = Convert.ToInt32(District.SelectedValue);
                obj.Gender = Convert.ToByte(Gender.SelectedValue);
                obj.IsDelete = 0;
                obj.Active = 1;
                obj.Type = Convert.ToInt32(TypeStaff.SelectedValue);
                obj.isAccountLogin = 0;
                obj.SalaryByPerson = false;
                obj.RequireEnroll = true;
                int Error = 0;
                int ErrorPerm = 0;
                if (!Perm_AllPermission)
                {
                    obj.SalonId = Convert.ToInt32(Session["SalonId"]);
                }
                else
                {
                    obj.SalonId = Convert.ToInt32(ddlSalon.SelectedValue);
                }
                string[] PermLST = new string[] { "staff", "reception", "salonmanager", "accountant", "mode1", "admin", "servey", "checkin", "checkout", "onlinestore", "inventory_manager", "inventory_manager_2" };
                int index = PermissionList.SelectedValue != "" ? Convert.ToInt32(PermissionList.SelectedValue) : 0;
                if (index > PermLST.Length || (!Perm_AllPermission && index > 1))
                {
                    Error++;
                    ErrorPerm++;
                }
                else
                {
                    obj.Permission = PermLST[index];
                }

                if (ErrorPerm > 0)
                {
                    var msg = "Giá trị phân quyền không đúng.";
                    var status = "msg-system warning";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + msg + "','" + status + "')", true);
                }

                // Validate
                // Check trùng số điện thoại
                if (obj.Phone != "")
                {
                    var ExistPhone = db.Staffs.Count(w => w.Phone == obj.Phone);
                    if (ExistPhone > 0)
                    {
                        var msg = "Số điện thoại đã tồn tại. Bạn vui lòng nhập số điện thoại khác.";
                        var status = "msg-system warning";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + msg + "','" + status + "')", true);
                        Error++;
                    }
                }

                // Check trùng email
                if (obj.Email != "")
                {
                    var ExistEmail = db.Staffs.Count(w => w.Email == obj.Email);
                    if (ExistEmail > 0)
                    {
                        var msg = "Email đã tồn tại. Bạn vui lòng nhập email khác.";
                        var status = "msg-system warning";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + msg + "','" + status + "')", true);
                        Error++;
                    }
                }

                if (Error == 0)
                {
                    db.Staffs.Add(obj);
                    db.SaveChanges();
                 
                    // Update OrderCode
                    db.Database.ExecuteSqlCommand("update Staff set OrderCode = " + obj.Id + " where Id = " + obj.Id);
                    // Log skill level10
                    if (TypeStaff.SelectedValue == "4")
                    {
                        SalaryLib.initSalaryByStaffId(obj.Id, obj.CreatedDate.Value.Date, Convert.ToInt32(obj.SalonId), db);

                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                        UIHelpers.Redirect("/admin/nhan-vien/" + obj.Id + ".html", MsgParam);
                    }
                    else
                    {
                        StaffLib.AddLogSkillLevel(obj.Id, Convert.ToInt32(obj.SkillLevel.Value), db);
                        // Init flow salary
                        SalaryLib.initSalaryByStaffId(obj.Id, obj.CreatedDate.Value.Date, Convert.ToInt32(obj.SalonId), db);

                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                        UIHelpers.Redirect("/admin/nhan-vien/" + obj.Id + ".html", MsgParam);
                    }
                    
                }
            }
        }

    } 
}