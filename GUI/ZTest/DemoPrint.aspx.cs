﻿using System;
using System.Web.UI;

namespace _30Shine.GUI
{
    public partial class DemoPrint : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadGridData();
            }
        }

        private void LoadGridData()
        {
            try
            {
                System.Data.DataTable dtSubData = new System.Data.DataTable();

                //Creating grid columns
                dtSubData.Columns.Add("DV");
                dtSubData.Columns.Add("SL");

                //Adding row deails
                dtSubData.Rows.Add("Shine Master", "1");
                dtSubData.Rows.Add("Shine Combo", "2");

                //Binding details to gridview
                grdResultDetails.DataSource = dtSubData;
                grdResultDetails.DataBind();

                System.Data.DataTable dtSubData2 = new System.Data.DataTable();

                //Creating grid columns
                dtSubData2.Columns.Add("SP");
                dtSubData2.Columns.Add("SL");

                //Adding row deails
                dtSubData2.Rows.Add("Sáp Oasis", "1");
                dtSubData2.Rows.Add("Sáp Colonna", "2");

                //Binding details to gridview
                grv2.DataSource = dtSubData2;
                grv2.DataBind();
            }
            catch { }
        }

        protected void btnPrintFromCodeBehind_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "printGrid", "printGrid();", true);
            }
            catch { }
        }
    }
}