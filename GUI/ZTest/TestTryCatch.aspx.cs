﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.ZTest
{
    public partial class TestTryCatch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string fullname = textFullname.Value;
            string fullname2 = textFullnameControl.Text;            

            try
            {
                this.checkUsername(fullname);
            }
            catch (Exception ex)
            {
                TextMessage.Text = ex.Message;
            }            
        }

        private void checkUsername(string username)
        {
            if (String.IsNullOrEmpty(username))
            {
                throw new Exception("Can not be empty!");
            }

            try
            {
                this.checkUsernameLength(username);
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        private void checkUsernameLength(string username)
        {
            if (username.Length < 5)
            {
                throw new Exception("Chuoi phai > 5 ky tu!");
            }
            XemHoTen.Text = username;
            TextMessage.Text = "Success";
        }
    }
}