﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Print.aspx.cs" Inherits="_30shine.GUI.ZTest.Print" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>jQuery Print Area</title>
<meta name="description" content="jQuery Print Area" />
<meta name="keywords" content="jQuery Print Area" />
<meta http-equiv="imagetoolbar" content="no" />
<link href="/Assets/css/font.css" rel="stylesheet" />
<%--<link href="/Assets/css/print/core.css" rel="stylesheet" media="screen" type="text/css" />
<link href="/Assets/css/print/core.css" rel="stylesheet" media="print" type="text/css" />--%>
<script src="/Assets/js/jquery.v1.11.1.js"></script>
</head>

<body style="background: white;">

    <form runat="server" id="FormPrint">

        <div class="btn-print-wrap">
<%--            <asp:Panel runat="server" ID="BtnPrint" ClientIDMode="Static" onclick="printBill()" CssClass="btn-print">Print</asp:Panel>
            <asp:Panel runat="server" ID="BtnSaveBillImg" ClientIDMode="Static" onclick="setHeightPrintBill()" CssClass="btn-print">Save bill as image</asp:Panel>--%>
            <asp:Button runat="server" ID="Fake_BtnPrint" ClientIDMode="Static" Text="Print" OnClick="GenPDF" OnClientClick="setHeightPrintBill()"
                style="padding: 5px 10px; cursor: pointer; margin-right: 10px;" />
            <asp:Button runat="server" ID="Fake_BtnSaveBillImg" ClientIDMode="Static" Text="Save bill as image" 
                 style="padding: 5px 10px; cursor: pointer;" OnClick="BtnClick_SaveBillImg" OnClientClick="setHeightPrintBill()"/>
            <asp:HiddenField runat="server" ID="HDF_PrintBillHeight" ClientIDMode="Static" />
        </div>        

        <!-- In hóa đơn -->
        <asp:Panel id="PrintBill" runat="server" clientidmode="Static" style="float: left;">
            <div class="print-wp" style="width: 225px; padding-left: 15px;">
                <div class="logo" style="width: 100%; text-align: center; padding: 0; margin-bottom: 5px;">
                    <img class="prt-logo" src="/Assets/images/logo.jpg" style="width: 100px;" />
                </div>
                <div style="width: 100%; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 2px;">Ngày : 28/10/2015 - 15h30</div>
                <div style="width: 100%; font-family: Arial, sans-serif;font-size: 16px;">Code : KT121115001</div>
                <table style="width: 100%;margin-top: 3px;">
                    <tbody>
                        <tr>
                            <td style="font-family: Arial, sans-serif; font-size: 16px; min-width: 30px;">KH</td>
                            <td style="font-family: Arial, sans-serif; font-size: 16px;">
                                : <b style="font-family: Arial, sans-serif; font-size: 16px; text-transform: uppercase; ">Châu Kiệt Luân</b>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-family: Arial, sans-serif;font-size: 16px; min-width: 30px;">Mã KH</td>
                            <td style="font-family: Arial, sans-serif;font-size: 16px;">: KT100000</td>
                        </tr>
                        <tr>
                            <td style="font-family: Arial, sans-serif;font-size: 16px; min-width: 30px;">Số DV</td>
                            <td style="font-family: Arial, sans-serif;font-size: 16px;">: 10</td>
                        </tr>
                    </tbody>            
                </table>

                <%--<div style="width: 100%; float: left; margin-top: 5px; margin-bottom: 10px;">
                    <img src="<%=ImgUrlTestPrint %>" style="width: 100%; float: left;" />
                </div>--%>

                <table style="border-collapse: collapse;width: 100%;margin-top: 5px;">
                    <thead>
                        <tr>
                            <th style="font-family: Arial, sans-serif;font-size: 16px; border: 1px solid #000;width: 80%; padding: 5px; font-weight:normal;">Dịch vụ</th>
                            <th style="font-family: Arial, sans-serif;font-size: 16px; border: 1px solid #000;width: 20%; padding: 5px; font-weight: normal;">SL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="font-family: Arial, sans-serif;font-size: 16px; border: 1px solid #000;width: 80%; padding: 5px;">Shine Master</td>
                            <td style="font-family: Arial, sans-serif;font-size: 16px; border: 1px solid #000;width: 20%; padding: 5px; text-align: right;">1</td>
                        </tr>
                        <tr>
                            <td style="font-family: Arial, sans-serif;font-size: 16px; border: 1px solid #000;width: 80%; padding: 5px;">Gội đầu Massage</td>
                            <td style="font-family: Arial, sans-serif;font-size: 16px; border: 1px solid #000;width: 20%; padding: 5px; text-align: right;">1</td>
                        </tr>
                    </tbody>            
                </table>

                <div style='width: 50%; float: left; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 0px; margin-top: 5px;' >
                    <div style='width: 12px; height: 12px; background: url(http://ql.30shine.com/Assets/images/checkbox3_12x12.png); margin-top: 2px; margin-right: 7px; float:left;'></div>Cạo mặt
                </div>
                <div style='width: 50%; float: left; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 0px; margin-top: 5px;' >
                    <div style='width: 12px; height: 12px; background: url(http://ql.30shine.com/Assets/images/checkbox3_12x12.png); margin-top: 2px; margin-right: 7px; float:left;'></div>Cắt móng
                </div>

                <table style="width: 100%;">    
                    <tbody>                
                        <tr>
                            <td style="font-family: Arial, sans-serif;font-size: 16px; padding: 5px 5px 0px 5px;">Stylist&nbsp;&nbsp;&nbsp;:</td>
                            <td style="font-family: Arial, sans-serif;font-size: 16px; padding: 5px 5px 0px 5px; text-align: right;">
                                <b style="font-family: Arial, sans-serif;font-size: 16px; font-weight: normal;">Lã Mạnh Phúc</b>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-family: Arial, sans-serif;font-size: 16px; padding: 2px 5px 0px 5px;">Skinner&nbsp;:</td>
                            <td style="font-family: Arial, sans-serif;font-size: 16px; padding: 2px 5px 0px 5px; text-align: right;"></td>
                        </tr>
                    </tbody>
                </table>

                <%--<div style="width: 50%; float: left; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 2px;">
                    <div style="width: 12px; height: 12px; border: 1px solid #000; margin-top: 2px; margin-right: 7px; float:left;"></div> Cạo Mặt
                </div>
                <div style="width: 50%; float: left; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 2px;">
                    <div style="width: 12px; height: 12px; border: 1px solid #000; margin-top: 2px; margin-right: 7px; float:left;"></div> Trị gầu
                </div>
                <div style="width: 50%; float: left; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 2px;">
                    <div style="width: 12px; height: 12px; border: 1px solid #000; margin-top: 2px; margin-right: 7px; float: left;"></div> Dưỡng tóc
                </div>

                <div style="width: 50%; float: left; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 2px;">
                    <div style="width: 12px; height: 12px; border: 1px solid #000; margin-top: 2px; margin-right: 7px; float: left;"></div> Ngăn rụng tóc
                </div>--%>

                <%--<div style="width: 100%; float: left; padding: 5px 0 10px 0">
                    <div style="width: 15%; height: 20px; float: left; background: url(http://ql.30shine.com/Assets/images/star_20.png) center no-repeat; position: relative; top: -2px; text-align: right;">:</div>
                    <div style="width: 15%; height: 20px; float: left; text-align: right;">1</div>
                    <div style="width: 15%; height: 20px; float: left; text-align: right;">2</div>
                    <div style="width: 15%; height: 20px; float: left; text-align: right;">3</div>
                    <div style="width: 15%; height: 20px; float: left; text-align: right;">4</div>
                    <div style="width: 15%; height: 20px; float: left; text-align: right;">5</div>
                </div>--%>

                <div style="width: 100%; float: left; margin-top: 5px; margin-bottom: 5px;">
                    <div style="width: 37%; float: left; font-family: Arial, sans-serif; padding-left: 3%;">
                        Đánh giá :
                    </div>
                    <div style="width: 60%; float: left;">
                        <div style="width: 100%; float: left; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 2px;">
                            <div style="width: 12px; height: 12px; border: 1px solid #000; margin-top: 2px; margin-right: 7px; float:left;"></div> Không hài lòng
                        </div>
                        <div style="width: 100%; float: left; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 2px;">
                            <div style="width: 12px; height: 12px; border: 1px solid #000; margin-top: 2px; margin-right: 7px; float:left;"></div> Hài lòng
                        </div>
                        <div style="width: 100%; float: left; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 2px;">
                            <div style="width: 12px; height: 12px; border: 1px solid #000; margin-top: 2px; margin-right: 7px; float:left;"></div> Rất hài lòng
                        </div>
                    </div>
                </div>

                <table style="border-collapse: collapse;width: 100%;margin-top: 5px;">
                    <thead>
                        <tr>
                            <th style="font-family: Arial, sans-serif;font-size: 16px; border: 1px solid #000;width: 80%; padding: 5px; font-weight: normal;">Sản phẩm</th>
                            <th style="font-family: Arial, sans-serif;font-size: 16px; border: 1px solid #000;width: 20%; padding: 5px; font-weight: normal;">SL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="font-family: Arial, sans-serif;font-size: 16px; border: 1px solid #000;width: 80%; height: 28px;"></td>
                            <td style="font-family: Arial, sans-serif;font-size: 16px; border: 1px solid #000;width: 20%; height: 28px;"></td>
                        </tr>
                        <tr>
                            <td style="font-family: Arial, sans-serif;font-size: 16px; border: 1px solid #000;width: 80%; height: 28px;"></td>
                            <td style="font-family: Arial, sans-serif;font-size: 16px; border: 1px solid #000;width: 20%; height: 28px;"></td>
                        </tr>
                        <tr>
                            <td style="font-family: Arial, sans-serif;font-size: 16px; border: 1px solid #000;width: 80%; height: 28px;"></td>
                            <td style="font-family: Arial, sans-serif;font-size: 16px; border: 1px solid #000;width: 20%; height: 28px;"></td>
                        </tr>
                    </tbody>            
                </table>

                <table style="font-family: Arial, sans-serif;font-size: 16px; width: 100%; margin-top: 5px;">
                    <tbody>
                        <tr>
                            <td style="font-family: Arial, sans-serif;font-size: 16px;">Seller :</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td style="font-family: Arial, sans-serif;font-size: 16px;">Tổng số :</td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>

                <table style="border-collapse: collapse;width: 100%;margin-top: 5px;">
                    <thead>
                        <tr>
                            <th style="font-family: Arial, sans-serif;font-size: 16px; border: 1px solid #000;width: 80%; padding: 5px; font-weight: normal;">Lịch sử gần nhất</th>
                            <th style="font-family: Arial, sans-serif;font-size: 16px; border: 1px solid #000;width: 20%; padding: 5px; font-weight: normal;">SL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="font-family: Arial, sans-serif;font-size: 14px; border: 1px solid #000;width: 80%; height: 28px; font-style: italic; border-bottom: none;">
                                Ngày 10/11/2015
                            </td>
                            <td style="font-family: Arial, sans-serif;font-size: 16px; border: 1px solid #000;width: 20%; height: 28px;"></td>
                        </tr>
                        <tr>
                            <td style="font-family: Arial, sans-serif;font-size: 16px; border: 1px solid #000; border-top:none; width: 80%; height: 28px;">
                                DV : Shine Star
                            </td>
                            <td style="font-family: Arial, sans-serif;font-size: 16px; border: 1px solid #000;width: 20%; height: 28px;">1</td>
                        </tr>

                        <tr>
                            <td style="font-family: Arial, sans-serif;font-size: 14px; border: 1px solid #000;width: 80%; height: 28px; font-style: italic; border-bottom: none;">
                                Ngày 09/09/2015
                            </td>
                            <td style="font-family: Arial, sans-serif;font-size: 16px; border: 1px solid #000;width: 20%; height: 28px;"></td>
                        </tr>
                        <tr>
                            <td style="font-family: Arial, sans-serif;font-size: 16px; border: 1px solid #000; border-top: none; width: 80%; height: 28px;">
                                SP : Sáp Colonna
                            </td>
                            <td style="font-family: Arial, sans-serif;font-size: 16px; border: 1px solid #000;width: 20%; height: 28px;">1</td>
                        </tr>
                    </tbody>                              
                </table>

                <table style="width: 100%;">    
                    <tbody>                
                        <tr>
                            <td style="font-family: Arial, sans-serif;font-size: 16px; padding: 5px 5px 0px 5px;">Stylist :</td>
                            <td style="font-family: Arial, sans-serif;font-size: 16px; padding: 5px 5px 0px 5px; text-align: right;">
                                <b style="font-family: Arial, sans-serif;font-size: 16px; font-weight: normal;">Nguyễn Như Trung</b>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </asp:Panel>
        <style type="text/css">
            .btn-print-wrap { width: 100%; float: left;}
            .btn-print { width: 100px; margin: 0 auto; padding: 5px 10px; background: #d0d0d0; color: #ffffff; cursor: pointer; margin-bottom: 10px;}
        </style>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                $("#PrintBill").height($("#PrintBill").find(".print-wp").height() + 10);
            });
            function setHeightPrintBill() {
                $("#PrintBill").height($("#PrintBill").find(".print-wp").height() + 10);
                $("#HDF_PrintBillHeight").val($("#PrintBill").height());
            }

            function openPdfIframe(src) {
                var PDF = document.getElementById("iframePrint");
                PDF.src = src;
                PDF.onload = function () {
                    PDF.focus();
                    PDF.contentWindow.print();
                    PDF.contentWindow.close();
                }                
            }
        </script>
        <!-- In hóa đơn -->

        <iframe id="iframePrint" style="display:none;"></iframe>


    </form>

</body>

</html>

