﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Data.Entity.Migrations;

namespace _30shine.GUI.FrontEnd.WorkFlow
{
    public partial class WorkFlow_Search : System.Web.UI.Page
    {
        private string CustomerCode;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                if (ExistCustomer())
                {
                    //MsgParam.Add(new KeyValuePair<string, string>("Code", CustomerCode));
                    Session["Customer_Code_Redirect"] = CustomerCode;
                    UIHelpers.Redirect("/khach-hang.html");
                }
                else
                {
                    MsgParam.Add(new KeyValuePair<string, string>("msg_login_status", "warning"));
                    MsgParam.Add(new KeyValuePair<string, string>("msg_login_message", "Mã khách hàng hoặc số điện thoại không tồn tại."));
                    UIHelpers.Redirect("/", MsgParam);
                }
            }
        }

        private bool ExistCustomer()
        {
            using (var db = new Solution_30shineEntities())
            {
                CustomerCode = code.Value.Trim();
                var OBJ = db.Customers.FirstOrDefault(w => w.Customer_Code == CustomerCode || w.Phone == CustomerCode);
                if (OBJ != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
}