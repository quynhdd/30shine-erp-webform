﻿<%@ Page Language="C#" EnableViewState="false" AutoEventWireup="true" CodeBehind="WorkFlow_Customer_Detail.aspx.cs" Inherits="_30shine.GUI.FrontEnd.WorkFlow.WorkFlow_Customer_Detail" MasterPageFile="~/TemplateMaster/WorkFlow.Master" %>

<asp:Content ID="MainContent" ContentPlaceHolderID="Main" runat="server">

<script src="/Assets/js/dropzone.js"></script>
<link href="/Assets/css/dropzone.css" rel="stylesheet" />

<div class="wp work-flow">
    <div class="container">        
        <div class="row search-form">
            <div class="form-group">                
                <div class="col-xs-8 col-sm-6 search-elm">
                    <input type="text" class="form-control" id="Code" placeholder="Nhập mã KH hoặc Số ĐT" runat="server" ClientIDMode="Static" autocomplete="off"/>
                </div>
                <div class="col-xs-4 col-sm-2 search-elm">
                    <div class="btn btn-default" id="ViewDetail">Xem thông tin</div>
                </div>            
            </div>
        </div>
        <div class="row">
            <% if (Customer_Img_First != ""){ %>
            <div class="col-xs-12 top-img"><%=Customer_Img_First %></div>
            <% } %>
            <div class="col-xs-12 top-title">
                <h1>
                    <i class="fa fa-chevron-right"></i><i class="fa fa-chevron-right" style="margin-left: -5px; margin-right: 1px;"></i>
                    Thông tin khách hàng
                </h1>
            </div>
            <% if(_Customer != null){ %>
            <div class="col-xs-8 col-sm-6 customer-infor">
                <div class="row elm">
                    <div class="col-xs-5">Họ tên</div>
                    <div class="col-xs-7"><%=_Customer.Fullname %></div>
                </div>
                <div class="row elm">
                    <div class="col-xs-5">Tuổi</div>
                    <div class="col-xs-7"><%=_Customer.Age %></div>
                </div>
                <div class="row elm elm-last">
                    <div class="col-xs-5">Địa chỉ</div>
                    <div class="col-xs-7">
                        <div class="row">
                            <div class="col-xs-12"><%=_Customer.CityName %>, <%=_Customer.DistrictName %></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6"></div>
                            <div class="col-xs-6"></div>
                        </div>
                    </div>
                </div>
            </div>
            <% }else{ %>
            <div class="col-xs-8 col-sm-6 customer-infor">
                <div class="row">
                    <div class="col-xs-12" style="margin-top: 5px;">Khách hàng mới</div>
                    <div class="col-xs-12">
                        <input type="text" class="form-control" id="NewCustomer" style="margin-top: 10px;"
                            placeholder="Nhập mã KH hoặc Số ĐT"/>
                    </div>
                </div>
            </div>
            <% } %>
            <div class="col-xs-4 col-sm-6 upload-image">
                <div class="upload-image-box" id="BtnUploadImg">
                    Chọn ảnh upload
                    <%--<asp:FileUpload ID="UploadFile" ClientIDMode="Static" 
                                    CssClass="input-file-upload" runat="server" AllowMultiple="true" />--%>
                </div>    
                <div class="listing-img-upload"></div>            
            </div>
            <div class="col-xs-12">
                <div id="dZUpload" class="dropzone dz-work-flow">
                    <div class="dz-default dz-message wrap listing-img-upload">
                        Drop image here. 
                    </div>
                </div>
            </div>
        </div>

        <% if(_Customer != null){ %>
        <div class="row history">
            <div class="col-xs-12 top-title">
                <h1>
                    <i class="fa fa-chevron-right"></i><i class="fa fa-chevron-right" style="margin-left: -5px; margin-right: 1px;"></i>
                    Lịch sử khách hàng
                </h1>
            </div>

            <%-- Timeline Block --%>
            <asp:Repeater ID="Rpt_History" runat="server">
                <ItemTemplate>
                    <div class="col-xs-12">
                        <strong class="time<%# Container.ItemIndex == 0 ? " time-first" : "" %>"><i class="fa fa-clock-o"></i>
                            Thời gian : <%# string.Format("{0:dd/MM/yyyy}",Eval("CreatedDate")) %>
                        </strong>
                    </div>
                    <div class="col-xs-12 col-sm-6 customer-infor">
                        <div class="row elm">
                            <div class="col-xs-5">Dịch vụ</div>
                            <div class="col-xs-7">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <%# Eval("ServiceNames") %>
                                        <%# Eval("ServiceNames") == null ? "- <i>Không có dịch vụ nào</i>" : "" %>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                        <div class="row elm">
                            <div class="col-xs-5">Sản phẩm</div>
                            <div class="col-xs-7">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <%# Eval("ProductNames") %>
                                        <%# Eval("ProductNames") == null ? "- <i>Không có sản phẩm nào</i>" : "" %>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 upload-image">
                        <%--<div class="upload-image-box">No Image</div>--%>
                        <div class="wrap listing-img-upload">
                            <%# Eval("ListImg") %>
                            <%# Eval("ListImg") == null ? "<div class='thumb-wp'><img class='thumb' src='/Assets/Images/no.photo.png' /></div>" : "" %>
                            <%--<div class="thumb-wp">

                                <img class="thumb" alt="" title="" src="http://ql.30shine.com/Public/Media/Upload/Images/User.Hair/1ea86e7b-8a19-4660-8ac1-e3596b4f7ab8.jpg" style="width: 120px; left: 0px;" />
                                <span class="delete-thumb" onclick="deleteThum($(this), '618d7abb-11f6-48e0-8cae-65b48d02d3df.jpg', true)"></span>
                            </div>--%>
                            <%--<% if(HasImages){ %>   
                            <% for (var i = 0; i < ListImagesName.Length; i++ )
                                { %>
                                <div class="thumb-wp">
                                    <img class="thumb" alt="" title="" src="<%=ListImagesUrl[i] %>" />
                                    <span class="delete-thumb" onclick="deleteThum($(this), '<%= ListImagesName[i] %>', true)"></span>
                                </div>
                            <% } %>
                            <% } %>--%>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>            
            <%-- END Timeline Block --%>            
        </div> 
        <% } %>       
    </div>
</div>
<asp:HiddenField ID="HDF_CustomerCode" runat="server" ClientIDMode="Static" />

<script type="text/javascript">
    jQuery(document).ready(function () {
        //==============================
        // Upload Images
        //==============================
        $("#UploadFile").unbind("change").bind("change", function () {
            //readImage(this,$(this));
            //Plugin_UploadImages(this, $(this));
        });
        excThumbWidth();

        //==============================
        // Upload Images - Dropzone
        //==============================
        Dropzone.autoDiscover = false;
        var Code, InitDropZone = false;
        $("#BtnUploadImg").bind("click", function () {
            if (document.getElementById("NewCustomer") != null) {
                Code = $("#NewCustomer").val();
            } else {
                Code = $("#HDF_CustomerCode").val();
            }
            if (!InitDropZone) {
                $("#dZUpload").dropzone({
                    url: "/GUI/BackEnd/UploadImage/DropZoneUploader.ashx?Code=" + Code,
                    maxFiles: 100,
                    addRemoveLinks: true,
                    success: function (file, response) {
                        $("#dZUpload").show();
                        var imgName = response;
                        file.previewElement.classList.add("dz-success");
                    },
                    error: function (file, response) {
                        file.previewElement.classList.add("dz-error");
                    }
                });
                InitDropZone = true;
            }
            $("#dZUpload").click();
        });

        // View Customer Detail
        $("#ViewDetail").bind("click", function () {
            var code = $("#Code").val();
            window.location.assign("/workflow/khach-hang?code=" + code);
        });

        $("#Code").bind("keydown", function (e) {
            if (e.keyCode == 13) {
                $("#ViewDetail").click();
                return false;
            }
        });

    });

    //=================================================
    // Xử lý ảnh
    //=================================================
    //draw image while upload
    var ListImgName = [],
        ListImgSrc = [],
        LIstImgDelete = [],
        ImgStandardWidth = 120,
        ImgStandardHeight = 120;

    function readImage(THIS, This) {
        var LoopName = 0;
        if (THIS.files.length > 0) {
            for (var i = 0; i < THIS.files.length; i++) {
                var FR = new FileReader();
                FR.onload = function (e) {
                    var img = new Image();
                    img.src = e.target.result;
                    img.onload = function () {
                        ListImgSrc.push(img.src);
                        var width = ImgStandardWidth;
                        var height = ImgStandardWidth * img.height / img.width;
                        var left = 0;
                        var top = (ImgStandardHeight - height) / 2;
                        var ThumbWp = "<div class='thumb-wp'>" +
                                        "<img class='thumb' style='width:" + width + "px;height:" +
                                        height + "px;left:" + left + "px;top:" + top + "px;' " + "src='" + img.src + "' />" +
                                      "<span class=\"delete-thumb\" onclick=\"deleteThum($(this), '" + img.src + "')\"></span></div>";
                        This.parent().parent().find(".listing-img-upload").append($(ThumbWp));

                    };
                    console.log(e.target.result);
                };
                FR.readAsDataURL(THIS.files[i]);
            }
            THIS.parentNode.setAttribute("class", "upload-image-box active");
        }
    }

    function Plugin_UploadImages(THIS, This) {

    }

    function Bind_UserImagesToHDF() {
        var jsonImages = JSON.stringify(ListImgSrc);
        var jsonImagesDel = JSON.stringify(LIstImgDelete);
        $("#HDF_UserImages").val(jsonImages);
        $("#HDF_UserImagesDelete").val(jsonImagesDel);
    }

    function deleteThum(THIS, data, oldImg) {
        oldImg = oldImg || false;
        if (!oldImg) {
            var index = ListImgSrc.indexOf(data);
            if (index != -1) {
                ListImgSrc.splice(index, 1);
            }
        } else {
            LIstImgDelete.push(data);
        }
        THIS.parent().remove();
    }

    function excThumbWidth() {
        var ImgStandardWidth = 120,
            ImgStandardHeight = 120;
        var width = ImgStandardWidth,
            height, left, top;
        $(".thumb-wp .thumb").each(function () {
            height = ImgStandardWidth * $(this).height() / $(this).width();
            left = 0;
            top = (ImgStandardHeight - height) / 2;
            $(this).css({ "width": width, "height": height, "left": left, "top": top });
        });
    }

    function AjaxUploadImage_BillService(ImageName) {
        $.ajax({
            type: "POST",
            url: "/GUI/SystemService/Ajax/UploadImage.aspx/Add_User_Hair",
            data: '{ImageName : "' + ImageName + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                var mission = JSON.parse(response.d);
                if (mission.success) {
                    +
                        ListImgName.push(mission.msg);
                } else {
                    var msg = "Đã có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển";
                    showMsgSystem(msg, "warning");
                }
            },
            failure: function (response) { alert(response.d); }
        });
    }
</script>

</asp:Content>
