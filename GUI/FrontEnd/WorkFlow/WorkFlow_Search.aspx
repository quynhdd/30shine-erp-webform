﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WorkFlow_Search.aspx.cs" Inherits="_30shine.GUI.FrontEnd.WorkFlow.WorkFlow_Search" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

	<title>Search Customer</title>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>

	<script type="text/javascript" src="/Assets/js/jquery.v1.11.1.js"></script>
    <script type="text/javascript" src="/Assets/js/ebstore/ebpopup.js"></script>
    <script type="text/javascript" src="/Assets/js/common.js"></script>

	<script type="text/javascript" src="/Assets/js/bootstrap/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="/Assets/css/bootstrap/bootstrap.min.css"/>

	<link rel="stylesheet" type="text/css" href="/Assets/css/font.css"/>
	<link rel="stylesheet" type="text/css" href="/Assets/css/login.css"/>
	<link rel="stylesheet" type="text/css" href="/Assets/css/font-awesome.css"/>

    <style type="text/css">
        .login-wp { margin-top: 14%;}
        .login-wp .txt-login { margin-bottom: 10px;}
        .login-wp .txt-login .login-msg { top: 100px;}
    </style>
</head>

<body >
<form runat="server" id="login" enctype="multipart/form-data">
    <div class="container login-wp">
	    <div class="row">
		    <div class="col-sm-2 col-md-3 col-sub"></div>
		    <div class="col-xs-12 col-sm-8 col-md-6 col-main">
			    <div class="row">
				    <div class="col-xs-12 txt-login">
					    <div class="login-symbol"><%--<i class="fa fa-user"></i>--%><i class="fa fa-search"></i></div>
					    <div class="login-dasboard-txt">Nhập mã khách hàng</div>
					    <div class="login-msg success" id="MsgSystem"></div>
				    </div>
				    <div class="col-md-9 col-sm-9 col-xs-12 input-field username-field">
					    <div class="login-icon user-icon"></div>
					    <input type="text" id="code" placeholder="Mã khách hàng hoặc số điện thoại" spellcheck="false" 
                            class="active" runat="server" autocomplete="off"/>
				    </div>
				    <div class="col-md-3 col-sm-3 col-xs-12 submit-field">
					    <div class="row btn-submit-wp" style="margin-top: 20px;">
						    <div class="col-sm-6 col-md-8 col-sub-02"></div>
                            <div class="col-xs-12 col-sm-6 col-md-4 btn-submit" id="BtnSubmit">Xem thông tin</div>
					    </div>
				    </div>
			    </div>
		    </div>
		    <div class="col-sm-2 col-md-3 col-sub"></div>
	    </div>
    </div>
</form>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        // auto focus on input[name="username"]
        $("input[name='username']").focus();

        // add class input active
        $(".input-field input").bind({
            focus: function () {
                $(this).addClass("active");
            },
            blur: function () {
                $(this).removeClass("active");
            }
        });

        // add class checkbox active
        var _isActive;
        $("#checkBoxRemember").bind({
            click: function () {
                _isActive = $(this).hasClass("active") ? true : false;
                if (_isActive) {
                    $(this).removeClass("active");
                } else {
                    $(this).addClass("active");
                }
            },
            focus: function () {
                $(this).bind("keypress", function () {
                    _isActive = $(this).hasClass("active") ? true : false;
                    if (_isActive) {
                        $(this).removeClass("active");
                    } else {
                        $(this).addClass("active");
                    }
                });
            }
        });

        //============================
        // Show Message
        //============================ 
        var qs = getQueryStrings();
        showMsgSystem(qs["msg_login_message"], qs["msg_login_status"]);

        // Submit
        $("#BtnSubmit").bind("click", function () {
            var code = $("#code").val();
            window.location.assign("/workflow/khach-hang?code=" + code);
        });

        $("#code").bind("keydown", function (e) {
            if (e.keyCode == 13) {
                $("#BtnSubmit").click();
                return false;
            }
        });
    });

</script>
</body>
</html>

