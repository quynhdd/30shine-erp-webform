﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;

namespace _30shine.GUI.FrontEnd.WorkFlow
{
    public partial class WorkFlow_Customer_Detail : System.Web.UI.Page
    {
        protected _30shine.MODEL.ENTITY.EDMX.Customer _Customer = new _30shine.MODEL.ENTITY.EDMX.Customer();
        protected string Customer_Img_First = "";
        protected string _Code;
        protected void Page_Load(object sender, EventArgs e)
        {
            GetCustomerDetail();          
        }

        public void GetCustomerDetail()
        {
            using (var db = new Solution_30shineEntities())
            {                
                _Code = Request.QueryString["code"];
                HDF_CustomerCode.Value = _Code;

                _Customer = db.Customers.FirstOrDefault(w => w.Customer_Code == _Code || w.Phone == _Code);                
                if (_Customer != null)
                {
                    var History = db.BillServices.Where(w => w.IsDelete != 1 && w.CustomerCode == _Code)
                                                    .OrderByDescending(o=>o.Id).ToList();
                    var _Count = History.Count;
                    if (_Count > 0)
                    {
                        var First = 1;
                        for (var i = 0; i < _Count; i++)
                        {
                            if (History[i].Images != null && History[i].Images != "")
                            {
                                var Temp1 = History[i].Images.Split(',');
                                var Temp2 = "";
                                foreach (var v in Temp1)
                                {
                                    var Img = "";
                                    Match match = Regex.Match(v, "Public/Media/Upload");
                                    if (!match.Success)
                                    {
                                        Img = "<img class='thumb' alt='' title='' src='" +
                                                    "http://" + "solution.30shine.net" + "/Public/Media/Upload/Images/User.Hair/" + v +
                                                "' />";
                                    }
                                    else
                                    {
                                        Img = "<img class='thumb' alt='' title='' src='" +
                                                    "http://" + "solution.30shine.net" + v +
                                                "' />";
                                    }
                                    Temp2 += "<div class='thumb-wp'>" + Img + "</div>";
                                    if (First == 1)
                                    {
                                        Customer_Img_First = Img;
                                    }
                                    First++;
                                }
                                History[i].ListImg = Temp2;
                            }
                            
                            // Get product name and service name
                            JavaScriptSerializer serializer = new JavaScriptSerializer();
                            if (History[i].ProductIds != null)
                            {
                                var ProductListThis = serializer.Deserialize<List<ProductBasic>>(History[i].ProductIds);
                                var _ProductName = "";
                                if (ProductListThis != null)
                                {
                                    foreach (var v2 in ProductListThis)
                                    {
                                        _ProductName += "- " + v2.Name + "<br/>";
                                    }
                                    History[i].ProductNames = _ProductName;
                                }
                            }

                            if (History[i].ServiceIds != null)
                            {
                                var ServiceListThis = serializer.Deserialize<List<ProductBasic>>(History[i].ServiceIds);
                                var _ServiceName = "";
                                if (ServiceListThis != null)
                                {
                                    foreach (var v2 in ServiceListThis)
                                    {
                                        _ServiceName += "- " + v2.Name + "<br/>";
                                    }
                                    History[i].ServiceNames = _ServiceName;
                                }
                            }
                        }
                        Rpt_History.DataSource = History;
                        Rpt_History.DataBind();
                    }
                }
            }            
        }

        public string generateID()
        {
            return Guid.NewGuid().ToString("N");
        }
    }
}