﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Linq.Expressions;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.FrontEnd.Tracking
{
    public partial class Tracking_Cus_Special_Requirements : System.Web.UI.Page
    {
        private string PageID = "TRACKING";
        protected Paging PAGING = new Paging();
        private bool Perm_Access = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected string DayI = "";
        protected string DayII = "";
        protected string DayIII = "";
        CultureInfo culture = new CultureInfo("vi-VN");

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ShowSalon", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            SetPermission();
            if (!IsPostBack)
            {
                txtStartDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now));
                GetDateBook();
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_Access);
                //Bind_Paging();
                BindData();
            }
        }
        protected void GetDateBook()
        {
            DateTime day = DateTime.Now;
            DateTime a = DateTime.Today;
            DayI = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now));
            DayII = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now).AddDays(+1));
            DayIII = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now).AddDays(+2));
            int dayI = ((int)day.DayOfWeek);
            int dayII = ((int)day.AddDays(+1).DayOfWeek);
            int dayIII = ((int)day.AddDays(+2).DayOfWeek);

        }
        protected void BindData()
        {
            using (var db = new Solution_30shineEntities())
            {
                DateTime _StartDate = Convert.ToDateTime(txtStartDate.Text, culture);
                int _SalonId = Convert.ToInt32(ddlSalon.SelectedValue !=null ? ddlSalon.SelectedValue : "0");
                var lstData = db.Tracking_Cus_Special_Requirements_Store(_StartDate, _SalonId).ToList(); ;
                RptBooking.DataSource = lstData;
                RptBooking.DataBind();
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            //Bind_Paging();
            BindData();
            RemoveLoading();
        }

        [WebMethod]
        public static object UpdateSalonNoteTracking( int _Tracking_ID, string SalonNoteTracking)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var obj = db.TrackingWeb_DataV2.FirstOrDefault(a => a.ID == _Tracking_ID);
                    if(obj != null)
                    {
                        obj.SalonNote = SalonNoteTracking;
                        if(obj.TimeSalonNote == null)
                        {
                            obj.TimeSalonNote = DateTime.Now;
                        }
                        db.SaveChanges();
                    }
                    return obj;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        //protected void Bind_Paging()
        //{
        //    // init Paging value            
        //    PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
        //    PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
        //    PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
        //    PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
        //    PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
        //    PAGING._Paging = PAGING.Make_Paging();

        //    RptPaging.DataSource = PAGING._Paging.ListPage;
        //    RptPaging.DataBind();
        //}
        //protected int Get_TotalPage()
        //{
        //    using (var db = new Solution_30shineEntities())
        //    {
        //        DateTime _StartDate = Convert.ToDateTime(txtStartDate.Text, culture);
        //        int _SalonId = Convert.ToInt32(ddlSalon.SelectedValue);
        //        var lstData = db.Tracking_Cus_Special_Requirements_Store(_StartDate, _SalonId).ToList(); ;
        //        var Count = lstData.Count();
        //        int TotalRow = Count - PAGING._TopNewsNum;
        //        int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
        //        return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        //    }
        //}

        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
        private void CheckPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "checkin", "salonmanager", "checkout" };
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            using (var db = new Solution_30shineEntities())
            {
                //Code.Enabled = false;
                //Code.Text = GenCode(db.Services.Count());
            }
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

     
    }
}