﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq.Expressions;
using LinqKit;
using _30shine.Helpers;
using _30shine.MODEL.ENTITY.EDMX;

namespace _30Shine.GUI.NhanVien
{
    public partial class NhanVien : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        protected Paging PAGING2 = new Paging();
        protected Staff OBJ = new Staff();
        private Expression<Func<BillService, bool>> WhereBill = PredicateBuilder.True<BillService>();
        private int Code;
        protected int TotalService = 0;
        protected int TotalProduct = 0;

        private bool Perm_Access = false;
        private bool Perm_Edit = false;
        private bool Perm_AllPermission = false;
        private bool Perm_HidePermission = false;
        private bool Perm_ActionMenu = false;
        private bool Perm_TypeStaff = false;
        string[] LSTPerm = new string[] { "root", "admin", "salonmanager", "reception", "staff" };

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckPermission();

            GenWhere();
            Bind_OBJ();
        }

        /// <summary>
        /// Bind data for service and product
        /// </summary>
        private void GenWhere()
        {
            Code = Int32.TryParse(Request.QueryString["Code"], out Code) ? Code : 0;
            // Time condition
            CultureInfo culture = new CultureInfo("vi-VN");
            DateTime _FromDate = new DateTime();
            DateTime _ToDate = new DateTime();
            if (HDF_Time.Value == "")
            {
                if (TxtDateTimeFrom.Text != "")
                {
                    _FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                    if (TxtDateTimeTo.Text != "")
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                    }
                    else
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(1);
                    }
                    WhereBill = WhereBill.And(w => w.CreatedDate > _FromDate && w.CreatedDate < _ToDate);
                }
            }            
            else
            {
                switch (HDF_Time.Value)
                {
                    case "lastday": _FromDate = DateTime.Today.AddDays(-1);
                        _ToDate = DateTime.Today;
                        WhereBill = WhereBill.And(w => w.CreatedDate >= _FromDate && w.CreatedDate < _ToDate);
                        break;
                    case "today": _FromDate = DateTime.Today;
                        _ToDate = DateTime.Today.AddDays(1);
                        WhereBill = WhereBill.And(w => w.CreatedDate >= _FromDate && w.CreatedDate < _ToDate);
                        break;
                    case "lastmon" : var now = DateTime.Now;
                        var lastmon = now.AddMonths(-1);
                        _FromDate = new DateTime(lastmon.Year, lastmon.Month, 1);
                        _ToDate = _FromDate.AddMonths(1);
                        WhereBill = WhereBill.And(w => w.CreatedDate >= _FromDate && w.CreatedDate < _ToDate);
                        break;
                    case "thismon": _FromDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
                        _ToDate = DateTime.Now.AddDays(1);
                        WhereBill = WhereBill.And(w => w.CreatedDate >= _FromDate && w.CreatedDate < _ToDate);
                        break;

                    default: _FromDate = Convert.ToDateTime("1/8/2015", culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                        WhereBill = WhereBill.And(w => w.CreatedDate > _FromDate);
                        break;
                }                
            }

            WhereBill = WhereBill.And(w => w.Staff_Hairdresser_Id == Code || w.Staff_HairMassage_Id == Code || w.SellerId == Code);
            WhereBill = WhereBill.And(w => w.IsDelete != 1);
        }

        private bool Bind_OBJ()
        {
            var ExitsOBJ = true;
            using (var db = new Solution_30shineEntities())
            {
                OBJ = db.Staffs.FirstOrDefault(w => w.Id == Code);
                if (OBJ != null)
                {
                    //
                }
                else
                {
                    ExitsOBJ = false;
                    var msg = "Lỗi! Nhân viên không tồn tại.";
                    var status = "msg-system warning";
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + msg + "','" + status + "');showDom('#StaffInfoWrap', true);", true);
                }
                return ExitsOBJ;
            }
        }

        public void Bind_Data_Service()
        {
            using (var db = new Solution_30shineEntities())
            {
                var Bills = db.BillServices.AsExpandable().Where(WhereBill).OrderBy(w => w.CreatedDate).ToList();
                // Print Service
                var Services = Bills.Join(db.FlowServices,
                                       a => a.Id,
                                       b => b.BillId,
                                       (a, b) => new
                                       {
                                           a.Id,
                                           b.CreatedDate,
                                           b.ServiceId
                                       }
                                   )
                                   .Join(db.Services,
                                       a => a.ServiceId,
                                       b => b.Id,
                                       (a, b) => new { 
                                           a.Id,
                                           a.CreatedDate,
                                           a.ServiceId,
                                           b.Name
                                       }
                                   )
                                   .GroupBy(g => g.ServiceId)
                                   .Select(s => new
                                   {
                                       Id = s.First().Id,
                                       Name = s.First().Name,
                                       CreatedDate = s.First().CreatedDate,
                                       Times = s.Count()
                                   }).ToList();
                if (Services.Count > 0)
                {
                    foreach (var v in Services)
                    {
                        TotalService += v.Times;
                    }
                }

                Rpt_DataService.DataSource = Services;
                Rpt_DataService.DataBind();
            }
        }

        public void Bind_Data_Product()
        {
            using (var db = new Solution_30shineEntities())
            {
                var Bills = db.BillServices.AsExpandable().Where(WhereBill).OrderBy(w => w.CreatedDate).ToList();
                // Print Service
                var Products = Bills.Join(db.FlowProducts,
                                       a => a.Id,
                                       b => b.BillId,
                                       (a, b) => new
                                       {
                                           a.Id,
                                           b.CreatedDate,
                                           b.ProductId
                                       }
                                   )
                                   .Join(db.Products,
                                       a => a.ProductId,
                                       b => b.Id,
                                       (a, b) => new
                                       {
                                           a.Id,
                                           a.CreatedDate,
                                           a.ProductId,
                                           b.Name
                                       }
                                   )
                                   .GroupBy(g => g.ProductId)
                                   .Select(s => new
                                   {
                                       Id = s.First().Id,
                                       Name = s.First().Name,
                                       CreatedDate = s.First().CreatedDate,
                                       Times = s.Count()
                                   }).ToList();
                if (Products.Count > 0)
                {
                    foreach (var v in Products)
                    {
                        TotalProduct += v.Times;
                    }
                }

                Rpt_DataProduct.DataSource = Products;
                Rpt_DataProduct.DataBind();
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Data_Product();
            Bind_Data_Service();

            //ScriptManager.RegisterStartupScript(this, GetType(), "Script", "SetActiveTableListing();ReplaceZezoValue();", true);
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["User_Id"] == null)
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/profile/login.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "mode1", "salonmanager", "reception", "staff" };
                var Permission = Session["User_Permission"].ToString().Trim();
                var Code = Request.QueryString["Code"] != "" ? Convert.ToInt32(Request.QueryString["Code"]) : 0;
                int UserId = Convert.ToInt32(Session["User_Id"]);
                var UserSalonId = Convert.ToInt32(Session["SalonId"]);

                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }

                using (var db = new Solution_30shineEntities())
                {
                    var obj = db.Staffs.FirstOrDefault(w => w.Id == Code);
                    if (obj != null && Array.IndexOf(Allow, obj.Permission) != -1)
                    {
                        /// Check quyền truy cập
                        /// Đối với quyền từ salonmanager trở lên, quyền cao hơn có thể truy cập xem quyền thấp hơn
                        /// Đối với quyền reception, staff, không được truy cập xem các nhân viên khác
                        if (Array.IndexOf(Allow, Session["User_Permission"]) <= Array.IndexOf(Allow, "salonmanager"))
                        {
                            if (Array.IndexOf(Allow, obj.Permission) <= Array.IndexOf(Allow, Session["User_Permission"]))
                            {
                                if (obj.Id != UserId)
                                {
                                    Perm_Access = false;
                                }
                            }
                            // Set quyền edit đối với nhân viên có quyền thấp hơn
                            if (Array.IndexOf(Allow, obj.Permission) >= Array.IndexOf(Allow, Session["User_Permission"]))
                            {
                                Perm_Edit = true;
                            }
                            // Từ chối truy cập xem nhân viên ở salon khác đối với quyền salonmanager
                            if (Array.IndexOf(Allow, Session["User_Permission"]) >= Array.IndexOf(Allow, "salonmanager") && obj.SalonId != UserSalonId)
                            {
                                Perm_Access = false;
                            }
                        }
                        else
                        {
                            if (obj.Id != UserId)
                            {
                                Perm_Access = false;
                            }
                        }
                    }
                }
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

            if (!Perm_Edit)
            {
                var Inputs = UIHelpers.GetMarkedControls(this.Controls, "perm-input-field");
                if (Inputs.Count() > 0)
                {
                    foreach (var v in Inputs)
                    {
                        ((TextBox)(v)).Enabled = false;
                    }
                }

                var DropDownLists = UIHelpers.GetMarkedControls(this.Controls, "perm-ddl-field");
                if (DropDownLists.Count() > 0)
                {
                    foreach (var v in DropDownLists)
                    {
                        ((DropDownList)(v)).Enabled = false;
                    }
                }

                var Buttons = UIHelpers.GetMarkedControls(this.Controls, "perm-btn-field");
                if (Buttons.Count() > 0)
                {
                    foreach (var v in Buttons)
                    {
                        ((Button)(v)).Visible = false;
                    }
                }
            }
        }

    }

}