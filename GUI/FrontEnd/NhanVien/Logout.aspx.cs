﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.Helpers;
using _30shine.MODEL.ENTITY.EDMX;

namespace _30Shine.GUI.Security
{
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckNotLogin();
            UnsetSessionUser();

            var MsgParam = new List<KeyValuePair<string, string>>();
            MsgParam.Add(new KeyValuePair<string, string>("msg_login_status", "success"));
            MsgParam.Add(new KeyValuePair<string, string>("msg_login_message", "Đăng xuất thành công!"));
            UIHelpers.Redirect("/profile/login.html", MsgParam);
        }

        private void UnsetSessionUser()
        {
            Session["User_Id"] = null;
            Session["User_Code"] = null;
            Session["User_Name"] = null;
            Session["User_Email"] = null;
            Session["User_SN_day"] = null;
            Session["User_SN_month"] = null;
            Session["User_SN_year"] = null;
        }

        private void CheckNotLogin()
        {
            if (Session["User_Id"] == null)
            {
                Response.Redirect("/profile/login.html");
            }
        }
    }
}