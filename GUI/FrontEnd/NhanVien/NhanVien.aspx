﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/30Shine_NhanVien.Master" AutoEventWireup="true" CodeBehind="NhanVien.aspx.cs" Inherits="_30Shine.GUI.NhanVien.NhanVien" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cplMeta" runat="server">
    <title>Thông tin nhân viên</title>   
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cplContent" runat="server">
<form id="FormStaff" runat="server" class="wp">
<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

    <div class="info-nhanvien">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nhanvien">
                    <div class="top-title">THÔNG TIN NHÂN VIÊN</div>
                    <%--<div class="avatar">
                        <img src="/images/nhanvien/member.jpg?v=1" alt="" />
                    </div>--%>
                    <div class="f-name">Họ và tên: <%=OBJ.Fullname %></div>
                    <div class="position">Bộ phận: <%=OBJ.StaffTypeName %> (Bậc <%=OBJ.SkillLevel %>)</div>
                    <div class="store">Salon: <%=OBJ.SalonName %></div>
                    <div class="detail"><a id="view-more" href="javascript://">(Xem chi tiết)</a></div>
                    <div class="more">
                        <div class="des">Ngày bắt đầu làm việc: <%=OBJ.DateJoin %></div>
                        <div class="des">Giới tính: <%= OBJ.Gender == 1 ? "Nam" : ( OBJ.Gender == 2 ? "Nữ" : "Khác") %> </div>
                        <div class="des">Ngày sinh: <%=OBJ.SN_day %>/<%=OBJ.SN_month %>/<%=OBJ.SN_year %></div>
                        <div class="des">Số CMND: <%=OBJ.StaffID %></div>
                        <div class="des">Số điện thoại: <%=OBJ.Phone %> </div>
                        <div class="des">Email (Tên đăng nhập): <%=OBJ.Email %></div>
                        <div class="des">Địa chỉ: <%=OBJ.DistrictName %>, <%=OBJ.CityName %></div>
                    </div>

                    <div class="statistic-wp">
                        <div class="statistic-line statistic-service">
                            <div class="statistic-title">
                                <p>Dịch vụ</p>
                            </div>
                            <div class="line">
                                <div class="lst-bound">
                                    <div class="bound bound-200"><span class="bound-value"></span></div>
                                    <div class="bound bound-400"><span class="bound-value"></span></div>
                                    <div class="bound bound-600"><span class="bound-value"></span></div>
                                    <div class="bound bound-700"><span class="bound-value"></span></div>
                                </div>  
                                <div class="percent-wp">
                                    <div class="percent" data-total="470"><span class="total-value">470</span></div>
                                </div>                                                              
                            </div>                            
                        </div>
                        <div class="statistic-line statistic-product">
                            <div class="statistic-title">
                                <p>Mỹ phẩm</p>
                            </div>
                            <div class="line">                                
                                <div class="lst-bound">
                                    <div class="bound bound-200"><span class="bound-value"></span></div>
                                    <div class="bound bound-400"><span class="bound-value"></span></div>
                                    <div class="bound bound-600"><span class="bound-value"></span></div>
                                    <div class="bound bound-700"><span class="bound-value"></span></div>
                                </div>     
                                <div class="percent-wp">                           
                                    <div class="percent" data-total="65"><span class="total-value">65</span></div>
                                </div>
                            </div>                            
                        </div>
                        <script>
                            jQuery(document).ready(function ($) {
                                var bound200 = 200,
                                    bound400 = 400,
                                    bound600 = 600,
                                    bound700 = 700;
                                var boundMax = 700;
                                var arrBound = [200, 400, 600, 700];
                                var lineWidth = $(".line").width();
                                var len = arrBound.length - 1;
                                for (var i = 0; i <= len; i++) {
                                    $(".bound-" + arrBound[i]).find(".bound-value").text(arrBound[i]).end()
                                        .animate({ left: arrBound[i] / boundMax * lineWidth });
                                }
                                $(".bound-" + arrBound[len]).find(".bound-value").css({ "left": -$(".bound-" + arrBound[len]).find(".bound-value").width() - 2 });
                                $(".bound-value").fadeIn();

                                setTimeout(function () {
                                    $(".line .percent").each(function () {
                                        var total = $(this).attr("data-total");
                                        $(this).show().animate({ left: -(lineWidth - total / boundMax * lineWidth) });
                                    });
                                }, 200);
                            });
                        </script>
                    </div>

                    <div class="view-button">
                        <input type="button" value="Hôm qua" onclick="viewByDate('lastday')" />
                        <input type="button" value="Hôm nay" onclick="viewByDate('today')" />
                        &nbsp&nbsp&nbsp&nbsp
                        <input type="button" value="Tháng trước" onclick="viewByDate('lastmon')" />
                        <input type="button" value="Tháng này" onclick="viewByDate('thismon')"/>
                    </div>
                    <div class="from-to">
                        <asp:TextBox runat="server" ID="TxtDateTimeFrom" ClientIDMode="Static" CssClass="txtDateTime"
                            placeholder="Từ ngày..." style="width: 232px;" autocomplete="off"></asp:TextBox>
                        <img src="/Assets/images/nhanvien/todate.png?v=1" alt="" />
                        <asp:TextBox runat="server" ID="TxtDateTimeTo" ClientIDMode="Static" CssClass="txtDateTime"
                            placeholder="Từ ngày..." style="width: 232px;" autocomplete="off"></asp:TextBox>
                    </div>
                    <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
                    <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                        <ContentTemplate>
                            <div class="dichvu">
                                <table>
                                    <tr>
                                        <th>STT</th>
                                        <th>Gói dịch vụ</th>
                                        <th>Số lượng</th>
                                    </tr>
                                    <tr class="total">
                                        <td></td>
                                        <td>Tổng cộng</td>
                                        <td><%=TotalService %></td>
                                    </tr>
                                    <asp:Repeater runat="server" ID="Rpt_DataService">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 %></td>
                                                <td><%# Eval("Name") %></td>
                                                <td><%# Eval("Times") %></td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>                                    
                                </table>
                            </div>

                            <div class="dichvu">
                                <table>
                                    <tr>
                                        <th>STT</th>
                                        <th>Sản phẩm</th>
                                        <th>Số lượng</th>
                                    </tr>
                                    <tr class="total">
                                        <td></td>
                                        <td>Tổng cộng</td>
                                        <td><%=TotalProduct %></td>
                                    </tr>
                                    <asp:Repeater runat="server" ID="Rpt_DataProduct">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 %></td>
                                                <td><%# Eval("Name") %></td>
                                                <td><%# Eval("Times") %></td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="BtnFakeViewdata" EventName="Click"  />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <!-- Hidden Field -->
    <asp:Button ID="BtnFakeViewdata" runat="server" ClientIDMode="Static" OnClick="_BtnClick" style="display:none;" />
    <asp:HiddenField ID="HDF_Time" runat="server" ClientIDMode="Static" />
    <!--/ Hidden Field -->

    <link href="/Assets/css/jquery.datetimepicker.css" rel="stylesheet" />
    <script src="/Assets/js/jquery.datetimepicker.js"></script>
    <script>
        jQuery(document).ready(function () {
            var _CurrentText = $('#view-more').text();
            $('#view-more').bind('click', function () {
                $('.more').slideToggle();
                if (_CurrentText == '(Xem chi tiết)')
                    $('#view-more').text('(Rút gọn)');
                else
                    $('#view-more').text('(Xem chi tiết)');
            });

            //============================
            // Datepicker
            //============================
            $('.txtDateTime').datetimepicker({
                dayOfWeekStart: 1,
                lang: 'vi',
                startDate: '2014/10/10',
                format: 'd/m/Y',
                dateonly: true,
                showHour: false,
                showMinute: false,
                timepicker: false,
                onChangeDateTime: function (dp, $input) { }
            });

            var flagDate = false;
            $("#TxtDateTimeTo").bind("change", function () {
                if ($(this).val() == "") {
                    $(this).addClass("warning");
                } else {
                    $(this).removeClass("warning");
                    if ($("#TxtDateTimeFrom").val() == "") {
                        $("#TxtDateTimeFrom").addClass("warning");
                        flagDate = true;
                    } else {
                        $("#HDF_Time").val("");
                        $("#BtnFakeViewdata").click();
                    }
                }                
            });

            $("#TxtDateTimeFrom").bind("change", function () {
                if (flagDate) {
                    if ($(this).val() == "") {
                        $(this).addClass("warning");
                    } else {
                        $(this).removeClass("warning");
                        if ($("#TxtDateTimeTo").val() == "") {
                            $("#TxtDateTimeTo").addClass("warning");
                        } else {
                            $("#HDF_Time").val("");
                            $("#BtnFakeViewdata").click();
                        }
                    }
                }
            });
        });

        function viewByDate(time) {
            $("#HDF_Time").val(time);
            console.log("here");
            $("#BtnFakeViewdata").click();
        }
    </script>

</asp:Panel>
</form>
</asp:Content>
