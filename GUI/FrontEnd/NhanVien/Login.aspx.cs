﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Text;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using _30shine.MODEL.ENTITY.EDMX;

namespace _30Shine.GUI.Security
{
    public partial class Login : System.Web.UI.Page
    {
        public UIHelpers _UIHelper = new UIHelpers();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CheckIsLogin();
            }            
        }

        protected void Btn_Login(object sender, EventArgs e)
        {
            var Email = username.Value;
            var Password = password.Value;
            string PasswordGen;

            using (MD5 md5Hash = MD5.Create())
            {
                PasswordGen = GenPassword(md5Hash, Password);
            }
            using (var db = new Solution_30shineEntities())
            {
                var Root = "root";
                var RootPass = "30shine@#";
                if (Email == Root && Password == RootPass)
                {
                    Session["User_Id"] = 0;
                    Session["User_Code"] = "ROOT";
                    Session["User_Name"] = "Root";
                    Session["User_Email"] = "root@mail.com";
                    Session["User_SN_day"] = "";
                    Session["User_SN_month"] = "";
                    Session["User_SN_year"] = "";
                    Session["User_Permission"] = 1;

                    UIHelpers.Redirect("/");
                }
                else 
                {
                    var User = db.Staffs.FirstOrDefault(w => w.Email == Email && w.Password == PasswordGen);
                    if (User != null)
                    {
                        Session["User_Id"] = User.Id;
                        Session["User_Code"] = User.Code;
                        Session["User_Name"] = User.Fullname;
                        Session["User_Email"] = User.Email;
                        Session["User_SN_day"] = User.SN_day;
                        Session["User_SN_month"] = User.SN_month;
                        Session["User_SN_year"] = User.SN_year;
                        Session["User_Permission"] = User.Permission;

                        UIHelpers.Redirect("/profile/" + User.Id.ToString() + ".html");
                    }
                    else
                    {
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_login_status", "warning"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_login_message", "Email hoặc mật khẩu không đúng."));
                        UIHelpers.Redirect("/profile/login.html", MsgParam);
                    }   
                }
            }
        }

        static string GenPassword(MD5 md5Hash, string input)
        {
            var scrPush = "uaefsfkoiu&@(^%=";
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input + scrPush));
            StringBuilder sBuilder = new StringBuilder();
            
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }

        static bool VerifyPassword(MD5 md5Hash, string input, string hash)
        {
            string hashOfInput = GenPassword(md5Hash, input);
            StringComparer comparer = StringComparer.OrdinalIgnoreCase;

            if (0 == comparer.Compare(hashOfInput, hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void CheckIsLogin()
        {
            if (Session["User_Id"] != null)
            {
                Response.Redirect("/profile/" + Session["User_Id"].ToString() + ".html");
            }
        }
    }
}