﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="_30Shine.GUI.Security.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>

	<title>Login</title>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=320, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>

    <link rel="Shortcut Icon" href="/Admin/Public/Assets/images/favicon.png" type="image/x-icon" />

	<script type="text/javascript" src="/Assets/js/jquery.v1.11.1.js"></script>
    <script type="text/javascript" src="/Assets/js/ebstore/ebpopup.js"></script>
    <script type="text/javascript" src="/Assets/js/common.js"></script>

	<script type="text/javascript" src="/Assets/js/bootstrap/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="/Assets/css/bootstrap/bootstrap.min.css"/>

	<link rel="stylesheet" type="text/css" href="/Assets/css/font.css"/>
	<link rel="stylesheet" type="text/css" href="/Assets/css/login.css"/>
	<link rel="stylesheet" type="text/css" href="/Assets/css/font-awesome.css"/>

</head>

<body >
<form runat="server" id="login" enctype="multipart/form-data">
    <div class="container login-wp">
	    <div class="row">
		    <div class="col-sm-3 col-md-4 col-sub"></div>
		    <div class="col-xs-12 col-sm-6 col-md-4 col-main">
			    <div class="row">
				    <div class="col-xs-12 txt-login">
					    <div class="login-symbol">
                            <%--<i class="fa fa-unlock-alt"></i>--%>
                            <img alt="logo" title="logo" src="/Assets/images/logo_30shine_yellow.png" />
					    </div>
					    <%--<div class="login-dasboard-txt">Dashboard Login</div>--%>
					    <div class="login-msg success" id="MsgSystem">
						    <%--Email hoặc mật khẩu không đúng.--%>
						    <!-- Đăng nhập thành công...... -->
					    </div>
				    </div>
				    <div class="col-xs-12 input-field username-field">
					    <div class="login-icon user-icon"></div>
					    <input type="text" id="username" placeholder="Email" spellcheck="false" class="active" runat="server" />
				    </div>
				    <div class="col-xs-12 input-field password-field">
					    <div class="login-icon password-icon"></div>
					    <input type="password" id="password" placeholder="Password" runat="server" />
				    </div>
				    <div class="col-xs-12 checkbox-wp">
					    <a href="javascript://" class="check-box active" id="checkBoxRemember"></a>
					    <div class="txt">Ghi nhớ đăng nhập</div>
				    </div>
				    <div class="col-xs-12">
					    <div class="row btn-submit-wp">
						    <div class="col-sm-6 col-md-8 col-sub-02"></div>
                            <asp:Button ID="submit" CssClass="col-xs-12 col-sm-6 col-md-4 btn-submit" runat="server" Text="Đăng nhập" OnClick="Btn_Login" />
					    </div>
				    </div>
			    </div>
		    </div>
		    <div class="col-sm-3 col-md-4 col-sub"></div>
	    </div>
    </div>
</form>

<style type="text/css">
    .login-wp .login-icon { color: #edbf07;}
    .login-wp .btn-submit { color: #edbf07;}
    .login-wp .btn-submit:hover { color: #edbf07;}
    .login-symbol { border: none; height: auto; width: auto;}
    .login-wp .txt-login { margin-bottom: 12px;}
    .login-wp { margin-top: 12%;}
    .login-wp .txt-login .login-msg { top: 150px;}    
</style>
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        // auto focus on input[name="username"]
        $("input[name='username']").focus();

        // add class input active
        $(".input-field input").bind({
            focus: function () {
                $(this).addClass("active");
            },
            blur: function () {
                $(this).removeClass("active");
            }
        });

        // add class checkbox active
        var _isActive;
        $("#checkBoxRemember").bind({
            click: function () {
                _isActive = $(this).hasClass("active") ? true : false;
                if (_isActive) {
                    $(this).removeClass("active");
                } else {
                    $(this).addClass("active");
                }
            },
            focus: function () {
                $(this).bind("keypress", function () {
                    _isActive = $(this).hasClass("active") ? true : false;
                    if (_isActive) {
                        $(this).removeClass("active");
                    } else {
                        $(this).addClass("active");
                    }
                });
            }
        });

        //============================
        // Show Message
        //============================ 
        var qs = getQueryStrings();
        showMsgSystem(qs["msg_login_message"], qs["msg_login_status"], 'login-msg');

    });

</script>

</body>
</html>

