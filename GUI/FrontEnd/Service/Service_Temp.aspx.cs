﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.IO;
using System.Web.Helpers;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Globalization;

namespace _30shine.GUI.UIService
{
    public partial class Service_Temp : System.Web.UI.Page
    {
        private bool Perm_ShowSalon = false;
        private bool Perm_Access = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckPermission();

            if (!IsPostBack)
            {
                Bind_Staff_Hairdresser();
                Bind_Staff_HairMassage();
                Bind_Staff_Cosmetic();
                Bind_RptProduct();
                Bind_RptService();
                Bin_Mark();
            }
        }

        protected void AddService(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                //// Insert to BillService
                var obj = new BillService();
                obj.CustomerCode = HDF_CustomerCode.Value;
                obj.Staff_Hairdresser_Id = Convert.ToInt32(Hairdresser.SelectedValue);
                obj.Staff_HairMassage_Id = Convert.ToInt32(HairMassage.SelectedValue);
                obj.ProductIds = HDF_ProductIds.Value != "[]" ? HDF_ProductIds.Value : "";
                obj.ServiceIds = HDF_ServiceIds.Value != "[]" ? HDF_ServiceIds.Value : "";
                obj.TotalMoney = Convert.ToInt32(HDF_TotalMoney.Value);
                obj.Note = NoteText.Text;
                obj.Mark = Convert.ToInt32(Mark.SelectedValue);
                obj.IsDelete = 0;
                obj.SalonId = Convert.ToInt32(Session["SalonId"]);
                obj.CreatedDate = DateTime.Now;
                obj.CustomerNoInfo = InputCusNoInfor.Checked;

                if (obj.ProductIds != "")
                    obj.SellerId = Convert.ToInt32(Cosmetic.SelectedValue);
                else
                    obj.SellerId = 0;

                var DataAdd = HDF_UserImages.Value;
                var serializer = new JavaScriptSerializer();
                var ListImgAdd = serializer.Deserialize<List<string>>(DataAdd);

                // Add images
                if (ListImgAdd != null)
                {
                    obj.Images = obj.Images == null ? "" : obj.Images + ",";
                    foreach (var v in ListImgAdd)
                    {
                        obj.Images += v + ",";
                    }
                    obj.Images = obj.Images.TrimStart(',').TrimEnd(',');
                }

                db.BillServices.Add(obj);
                var exc = db.SaveChanges();
                var Error = exc > 0 ? 0 : 1;

                // Insert to FlowProduct
                if (Error == 0)
                {
                    var objProduct = new FlowProduct();
                    //var str = "[{\"Id\":\"19\",\"Price\":340000,\"Quantity\":\"1\"}]";
                    serializer = new JavaScriptSerializer();
                    var ProductList = serializer.Deserialize<List<ProductBasic>>(obj.ProductIds);
                    if (ProductList != null)
                    {
                        foreach (var v in ProductList)
                        {
                            if (Error == 0)
                            {
                                objProduct.BillId = obj.Id;
                                objProduct.ProductId = v.Id;
                                objProduct.Price = v.Price;
                                objProduct.Quantity = v.Quantity;
                                objProduct.VoucherPercent = Convert.ToInt32(v.VoucherPercent);
                                objProduct.CreatedDate = DateTime.Now;
                                objProduct.IsDelete = 0;
                                objProduct.SalonId = Convert.ToInt32(Session["SalonId"]);
                                objProduct.SellerId = Convert.ToInt32(Cosmetic.SelectedValue);

                                db.FlowProducts.Add(objProduct);
                                exc = db.SaveChanges();
                                Error = exc > 0 ? Error : ++Error;
                            }
                        }
                    }
                }

                // Insert to FlowService
                if (Error == 0)
                {
                    var objService = new FlowService();
                    //var str = "[{\"Id\":\"19\",\"Price\":340000,\"Quantity\":\"1\"}]";
                    serializer = new JavaScriptSerializer();
                    var ServiceList = serializer.Deserialize<List<ProductBasic>>(obj.ServiceIds);
                    if (ServiceList != null)
                    {
                        foreach (var v in ServiceList)
                        {
                            if (Error == 0)
                            {
                                objService.BillId = obj.Id;
                                objService.ServiceId = v.Id;
                                objService.Price = v.Price;
                                objService.Quantity = v.Quantity;
                                objService.VoucherPercent = Convert.ToInt32(v.VoucherPercent);
                                objService.CreatedDate = DateTime.Now;
                                objService.IsDelete = 0;
                                objService.SalonId = Convert.ToInt32(Session["SalonId"]);
                                objService.SellerId = Convert.ToInt32(Cosmetic.SelectedValue);

                                db.FlowServices.Add(objService);
                                exc = db.SaveChanges();
                                Error = exc > 0 ? Error : ++Error;
                            }
                        }
                    }
                }

                if (Error == 0)
                {
                    var MsgParam = new List<KeyValuePair<string, string>>();
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                    UIHelpers.Redirect("/dich-vu/" + obj.Id + ".html", MsgParam);
                }
                else
                {
                    MsgSystem.Text = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                    MsgSystem.CssClass = "msg-system warning";
                }

            }
        }

        public void Bind_Salon()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Tbl_Salon.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Order).ToList();
                var Key = 0;
                var Count = LST.Count;
                Salon.DataTextField = "Salon";
                Salon.DataValueField = "Id";
                ListItem item = new ListItem("Chọn salon", "0");
                Salon.Items.Insert(Key, item);
                Key++;

                if (Count > 0)
                {
                    foreach (var v in LST)
                    {
                        item = new ListItem(v.Name, v.Id.ToString());
                        Salon.Items.Insert(Key, item);
                        Key++;
                    }
                }
            }
        }

        private void Bind_RptProduct()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Products = db.Products.OrderByDescending(o => o.Id).ToList();

                Rpt_Product.DataSource = _Products;
                Rpt_Product.DataBind();
            }
        }

        private void Bind_RptService()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Service = db.Services.OrderByDescending(o => o.Id).ToList();

                Rpt_Service.DataSource = _Service;
                Rpt_Service.DataBind();
            }
        }

        // Nhân viên cắt tóc
        private void Bind_Staff_Hairdresser()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Hairdresser = db.Staffs.Where(w => w.Type == 1).ToList();
                var Key = 0;

                Hairdresser.DataTextField = "Hairdresser";
                Hairdresser.DataValueField = "Id";
                ListItem item = new ListItem("Chọn nhân viên cắt tóc", "0");
                Hairdresser.Items.Insert(Key, item);

                foreach (var v in _Hairdresser)
                {
                    Key++;
                    item = new ListItem(v.Fullname, v.Id.ToString());
                    item.Attributes.Add("data-code", v.Code);
                    Hairdresser.Items.Insert(Key, item);
                }
            }
        }

        // Nhân viên gội đầu
        private void Bind_Staff_HairMassage()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _HairMassage = db.Staffs.Where(w => w.Type == 2).ToList();
                var Key = 0;

                HairMassage.DataTextField = "HairMassage";
                HairMassage.DataValueField = "Id";
                ListItem item = new ListItem("Chọn nhân viên gội đầu", "0");
                HairMassage.Items.Insert(Key, item);

                foreach (var v in _HairMassage)
                {
                    Key++;
                    item = new ListItem(v.Fullname, v.Id.ToString());
                    item.Attributes.Add("data-code", v.Code);
                    HairMassage.Items.Insert(Key, item);
                }
            }
        }

        // Nhân viên bán mỹ phẩm
        private void Bind_Staff_Cosmetic()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Cosmetic = db.Staffs.Where(w => w.IsDelete != 1 && (w.Permission == null || w.Permission != "admin")).OrderBy(o => o.Fullname).ToList();
                var Key = 0;

                Cosmetic.DataTextField = "Cosmetic";
                Cosmetic.DataValueField = "Id";
                ListItem item = new ListItem("Chọn nhân viên bán mỹ phẩm", "0");
                Cosmetic.Items.Insert(Key, item);

                foreach (var v in _Cosmetic)
                {
                    Key++;
                    item = new ListItem(v.Fullname, v.Id.ToString());
                    item.Attributes.Add("data-code", v.Code);
                    Cosmetic.Items.Insert(Key, item);
                }
            }
        }

        private void Bin_Mark()
        {
            Mark.DataTextField = "Mark";
            Mark.DataValueField = "Id";
            ListItem item = new ListItem("Chọn điểm đánh giá", "0");
            Mark.Items.Insert(0, item);
            item = new ListItem("1", "1");
            Mark.Items.Insert(1, item);
            item = new ListItem("2", "2");
            Mark.Items.Insert(2, item);
            item = new ListItem("3", "3");
            Mark.Items.Insert(3, item);
            item = new ListItem("4", "4");
            Mark.Items.Insert(4, item);
            item = new ListItem("5", "5");
            Mark.Items.Insert(5, item);
        }

        protected void btnPrintFromCodeBehind_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, typeof(Page), "printGrid", "printGrid('.print-wp');", true);
            }
            catch { }
        }

        [WebMethod]
        public static string GetCustomer(string CustomerCode)
        {
            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var _Customer = db.Customers.Where(w => w.Customer_Code == CustomerCode).ToList();

                if (_Customer.Count > 0)
                {
                    _Msg.success = true;
                    _Msg.msg = serializer.Serialize(_Customer);
                }
                else
                {
                    _Msg.success = false;
                }
            }
            return serializer.Serialize(_Msg);
        }

        [WebMethod]
        public static string CheckDuplicateBill(string CustomerCode)
        {
            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var today = Convert.ToDateTime(DateTime.Now.ToString("d/M/yyyy"), culture);
                var todayL = today.AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                var todayR = today.AddDays(1);
                var _Bill = db.BillServices.FirstOrDefault(w => w.CreatedDate > todayL && w.CreatedDate < todayR && w.CustomerCode == CustomerCode);

                if (_Bill != null)
                {
                    _Msg.success = true;
                    _Msg.msg = serializer.Serialize(_Bill);
                }
                else
                {
                    _Msg.success = false;
                }
            }
            return serializer.Serialize(_Msg);
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["User_Id"] == null)
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "salonmanager", "reception" };
                string[] AllowSalon = new string[] { "root", "admin" };
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }
                if (Array.IndexOf(AllowSalon, Permission) != -1)
                {
                    Perm_ShowSalon = true;
                }
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
            else if (Perm_ShowSalon)
            {
                TrSalon.Visible = true;
                Bind_Salon();
            }
        }
    }

}