﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Service_Process.aspx.cs" Inherits="_30shine.GUI.FrontEnd.Service.Service_Process" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../../Assets/css/bootstrap/bootstrap.css" rel="stylesheet" />
    <link href="../../../Assets/css/style.css" rel="stylesheet" />
    <link href="../../../Assets/css/font.css" rel="stylesheet" />
    <link href="../../../Assets/css/semantic.css" rel="stylesheet" />

    <script src="../../../Assets/js/jquery.v1.11.1.js"></script>
    <%--<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>--%>
    <script src="../../../Assets/js/semantic.js"></script>
    <script src="../../../Assets/js/bootstrap/bootstrap.min.js"></script>

    <!-- Include the plugin's CSS and JS: -->
    <script src="../../../Assets/css/bootstrap/bootstrap-multiselect.js"></script>
    <link rel="stylesheet" href="/Assets/css/bootstrap/bootstrap-multiselect.css" type="text/css" />
    <link href="../../../Assets/css/font-awesome.css" rel="stylesheet" />

    <style>
        body { background: #000000; }
        .service-process { width: 1024px; height: 580px; background: #fff; padding: 12px; overflow: auto; max-width: 100%; }
            .service-process table { width: 100%; float: left; }
                .service-process table tr { border: 1px solid #d0d0d0; }
            .service-process .team { border: 1px solid #d0d0d0; width: 65px; height: 31px; float: left; }
            .service-process .contents { float: left; width: 100%; border: 1px solid #d0d0d0; padding: 5px; }
                .service-process .contents * { float: left; }
            .service-process .customer.name { font-size: 18px; padding: 0px; margin: 0px; font-family: Roboto Condensed Bold; }
            .service-process .contents .name { padding: 5px; margin: 0px; }
        .no-padding { padding: 0px !important; }
        .service-process table thead .steps .step { border: 1px dotted #000000; background: #d0d0d0; padding: 5px; text-align: center; }
        .service-process table thead .steps .step { border: 1px dotted #000000; background: #d0d0d0; padding: 5px; text-align: center; }
        .service-process table tbody .item.bill { width: 297px; float: left; }
        .service-process table thead tr td { padding: 0px; }
        .service-process table tbody tr td { padding: 5px; min-width: 308px; }
        /*.service-process table tbody .item.bill .customer, .service-process table tbody .item.bill .services, .service-process table tbody .item.bill .next-step { width: 33.33%; }*/
        .service-process table tbody .item.bill .services { margin-left: 5px; }
        .relative { position: relative; }
        .btn { padding: 5px; background: #d0d0d0; border-radius: 3px; font-family: Roboto Condensed Bold; }
        .btn-dotted { margin: 0; cursor: pointer; padding: 5px; background: #fff; border-radius: 3px; font-family: Roboto Condensed Bold; border: 1px dotted #d0d0d0; min-width: 50px; margin-right: 5px !important; }
            .btn-dotted:hover { background: #808080; color: #fff; }
        .btn:hover { background: #50b347; color: #fff; }
        .right { float: right !important; }
        .left { float: left !important; }
        .relative { position: relative; }
        .absolute { position: absolute; }
        .top { top: 0px; left: 0; z-index: 9999; height: 100%; }

        .multiselect-container > li, .btn .caret { float: none !important; }
        .btn .caret { margin-left: 5px; }
        .multiselect-container > li > a { width: 100%; }


        .service-process .popup { background: rgba(0, 0, 0, 0) linear-gradient(to bottom, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.9) 100%) repeat scroll 0 0; }
            .service-process .popup .row { width: 50%; margin: 0 22.22%; float: left; /*min-height: 30%;*/ background: #fff; padding: 20px; border: 4px; }
                .service-process .popup .row .submit { /*width: 100%;*/ text-align: center; }
                    .service-process .popup .row .submit .btn { width: 120px; }
                    .service-process .popup .row .submit.cancel:hover p { background: #808080; }

        .group.fields .field { width: 100%; float: left; }

        .service-process .checkbox-select, .service-process .radio-select { max-height: 300px; overflow-y: scroll; border: 4px; }

        .ui.checkbox label { float: left; }
        .ui.checkbox p { float: left; margin-left: 12px; }
        .log-out { float: left; width: 100%; height: 43px; cursor: pointer; border: 1px solid #d0d0d0; text-align: center; }
            .log-out:hover { background: #d0d0d0; }
                .log-out:hover i { color: #ff0000; }
            .log-out i { font-size: 40px; }

        /* popup big */
        .popup-wrap { width: 100%; height: 100%; float: left; position: absolute; top: 0; left: 0; background: rgba(0,0,0,0.8); z-index: 1000; display: none; }
        .popup-content { width: 300px; margin: 0 auto; margin-top: 200px; background: #fff; }
        .popup-content-wp { width: 100%; float: left; background: #fff; }
        .popup-content ul.ul-popup-content { width: 100%; float: left; background: #ffffff; padding: 0 2%; }
            .popup-content ul.ul-popup-content li { padding: 10px 15px; border-top: 1px solid #ddd; }
                .popup-content ul.ul-popup-content li label { font-size: 20px; margin-left: 5px; font-family: Roboto Condensed Bold; }
                .popup-content ul.ul-popup-content li input { padding: 0; width: 18px; height: 17px; position: relative; top: 2px; }
        p.popup-content-head { width: 92%; padding: 5px 4%; font-family: Roboto Condensed Regular; font-size: 18px; margin-bottom: 0px; }
        .confirm-button { padding: 10px 5px; float: none; color: #ffffff; font-family: Roboto Condensed Bold; font-size: 20px; cursor: pointer; float: left; width: 100%; }
        .confirm-yes { padding-left: 0; padding-right: 5px; }
        .confirm-no { padding-left: 5px; padding-right: 0; }
        .confirm-yes .confirm-button { background: #50b347; }
        .confirm-no .confirm-button { background: #ff0000; }
        .popup-content i.btn-close { position: absolute; top: -6px; right: 0; font-size: 24px; font-size: 40px; color: red; cursor: pointer;}

        .fullscreen { float: left; font-size: 33px; position: absolute; right: -20px; top: 6px; z-index: 1000; cursor: pointer; }
    </style>

</head>

<body>
    <form id="Form_Process" runat="server">
        <div class="wrap service-process relative" id="ServiceProcess">
            <div class="container main">
                <div class="row">
                    <div class="col-xs-12 col-md-12 col-sm-12">
                        <div class="col-xs-1 col-md-1 col-sm-1 no-padding">
                            <div class="contents">
                                <%--<p class="name">Team</p>--%>
                                <div class="team" style="background: <%=Color%>" data-type="change-team" data-option="radio"></div>
                            </div>
                        </div>
                        <div class="col-xs-4 col-md-4 col-sm-4 no-padding">
                            <div class="contents skinners">
                                <div class="list">
                                    <%--<p class="btn-dotted btn-popup skinner name" data-id="" data-type='del-skinner' data-option="del">Lan Anh</p>--%>
                                </div>
                                <p class="btn btn-popup add-skinner right" data-action='input-data' data-type="add-skinner" data-option="checkbox">+Skinner</p>
                            </div>
                        </div>
                        <div class="col-xs-3 col-md-3 col-sm-3 no-padding">
                            <div class="contents stylists">
                                <div class="list">
                                    <%--<p class="btn-dotted btn-popup stylist name" data-id="" data-type="del-stylist" data-option="del">Thắng</p>--%>
                                    <%--<p class="btn-dotted btn-popup stylist name" data-id="" data-type="del-stylist" data-option="del">Vinh</p>--%>
                                </div>
                                <p class="btn btn-popup add-skinner right" data-action='input-data' data-type="add-stylist" data-option="checkbox">+Stylist</p>
                            </div>
                        </div>
                        <div class="col-xs-3 col-md-3 col-sm-3 no-padding">
                            <div class="contents times">
                                <p class="name">Ca:</p>
                                <p class="name">8-17h.</p>
                                <p class="name">Giờ nghỉ:</p>
                                <p class="name">11h30-12h</p>
                            </div>
                        </div>
                        <div class="col-xs-1 col-md-1 col-sm-1 no-padding">
                            <p data-type="log-out" data-option="log-out" onclick="popup($(this))" class="log-out"><i class="fa fa-power-off" aria-hidden="true"></i></p>
                        </div>
                        <div class="fullscreen" data-fullscreen="false"><i class="fa fa-arrows-alt" aria-hidden="true"></i></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12 col-md-12 col-sm-12">
                        <table class="table" id="TableTeamProcess">
                            <thead>

                                <tr class="steps">
                                    <td>
                                        <p class="step step-0">Chờ/Hẹn</p>
                                    </td>
                                    <td>
                                        <p class="step step-1">Gội</p>
                                    </td>
                                    <td>
                                        <p class="step step-2">Cắt</p>
                                    </td>

                                </tr>

                            </thead>
                            <tbody>
                                <% foreach (var v in billsPending)
                                    {%>
                                <tr>
                                    <% if (v.Status == 0)
                                        { %>
                                    <td class="col-xs-4 td-1">
                                        <div class="item bill">
                                            <div class="contents">
                                                <p class="customer name"><%= v.CustomerName %></p>
                                                <div class="services right">
                                                    <%--<p class="btn-dotted btn-popup name" data-type="choose-services" data-option="checkbox">SC, SM, U</p>--%>
                                                </div>
                                            </div>
                                            <div class="contents staffs">
                                                <div class="names">
                                                    <% if (v.Staff_HairMassage_Id > 0)
                                                        { %>
                                                    <p class="btn-dotted btn-popup skinner" data-type="change-skinner" data-option="radio">
                                                        <%= v.skinnerName %>
                                                    </p>
                                                    <% } %>
                                                    <% if (v.Staff_Hairdresser_Id > 0)
                                                        { %>
                                                    <p class="btn-dotted btn-popup stylist right" data-type="change-stylist" data-option="radio">
                                                        <%= v.stylistName %>
                                                    </p>
                                                    <% } %>
                                                </div>

                                                <div class="next-step right" onclick="nextStep($(this), <%=v.Status %>,<%=v.Id %>)">

                                                    <p class="btn right">Gội >></p>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="col-xs-4 td-2"></td>
                                    <td class="col-xs-4 td-3"></td>
                                    <% }
                                        else if (v.Status == 1)
                                        { %>
                                    <td class="col-xs-4 td-1"></td>
                                    <td class="col-xs-4 td-2">
                                        <div class="item bill">
                                            <div class="contents">
                                                <p class="customer name"><%= v.CustomerName %></p>
                                                <div class="services right">
                                                    <%--<p class="btn-dotted btn-popup name" data-type="choose-services" data-option="checkbox">SC, SM, U</p>--%>
                                                </div>

                                            </div>
                                            <div class="contents staffs">
                                                <div class="names">
                                                    <% if (v.Staff_HairMassage_Id > 0)
                                                        { %>
                                                    <p class="btn-dotted btn-popup skinner" data-type="change-skinner" data-option="radio">
                                                        <%= v.skinnerName %>
                                                    </p>
                                                    <% } %>
                                                    <% if (v.Staff_Hairdresser_Id > 0)
                                                        { %>
                                                    <p class="btn-dotted btn-popup stylist right" data-type="change-stylist" data-option="radio">
                                                        <%= v.stylistName %>
                                                    </p>
                                                    <% } %>
                                                </div>

                                                <div class="next-step right complete-button" onclick="completeStep($(this), <%=v.Status %>,<%=v.Id %>)">
                                                    <p class="btn right">Xong >></p>

                                                </div>
                                                <div class="next-step right next-button" onclick="nextStep($(this), <%=v.Status %>,<%=v.Id %>)" style="display: none;">
                                                    <p class="btn right">Cắt >></p>

                                                </div>
                                            </div>
                                        </div>
                                        <td class="col-xs-4 td-3"></td>
                                        <%}
                                            else if (v.Status == 2)
                                            { %>
                                        <td class="col-xs-4 td-1"></td>
                                        <td class="col-xs-4 td-2"></td>
                                        <td class="col-xs-4 td-3">
                                            <div class="item bill">
                                                <div class="contents">
                                                    <p class="customer name"><%= v.CustomerName %></p>
                                                    <div class="services right">
                                                        <%--<p class="btn-dotted btn-popup name" data-type="choose-services" data-option="checkbox">SC, SM, U</p>--%>
                                                    </div>

                                                    <div class="contents staffs">
                                                        <div class="names">
                                                            <% if (v.Staff_HairMassage_Id > 0){ %>
                                                            <p class="btn-dotted btn-popup skinner" data-type="change-skinner" data-option="radio">
                                                                <%= v.skinnerName %>
                                                            </p>
                                                            <% } %>
                                                            <% if (v.Staff_Hairdresser_Id > 0){ %>
                                                            <p class="btn-dotted btn-popup stylist right" data-type="change-stylist" data-option="radio">
                                                                <%= v.stylistName %>
                                                            </p>
                                                            <% } %>
                                                        </div>
                                                        <div class="next-step right" onclick="nextStep($(this), <%=v.Status %>, <%=v.Id %>)">
                                                            <p class="btn right">Hoàn thành >></p>
                                                        </div>
                                                    </div>
                                                </div>
                                        </td>
                                        <% } %>
                                </tr>
                                <% } %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="container popup absolute top" style="display: none;">

                <div class="row">
                    <div class="checkbox-select" data-type="">
                        <div class="list">
                        </div>
                    </div>
                    <div class="radio-select">
                        <div class="ui form">
                            <div class="grouped fields" data-type="">
                            </div>
                        </div>
                    </div>

                    <div class="submit confirm right">
                        <p class="btn" data-action=""></p>
                    </div>

                    <div class="submit cancel left">
                        <p class="btn" data-action="cancel">Hủy bỏ</p>
                    </div>

                </div>
            </div>

            <!-- popup big wrap -->
            <div class="popup-wrap">
                <div class="popup-content">
                    <div class="popup-content-wp">
                        <div class="content-wp" style="position: relative;">
                            <i class="fa fa-times btn-close" aria-hidden="true"></i>
                            <ul class="ul-popup-content"></ul>                            
                        </div>
                    </div>
                </div>
            </div>
            <!--// popup big wrap -->
        </div>

        <asp:HiddenField ID="HDF_Action" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="HDF_Salon_Id" ClientIDMode="Static" Value="3" runat="server" />
        <asp:HiddenField ID="HDF_Team_Id" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="HDF_Data_Type" ClientIDMode="Static" Value="" runat="server" />
        <asp:HiddenField ID="HDF_Skinner" ClientIDMode="Static" Value="" runat="server" />
        <asp:HiddenField ID="HDF_Stylist" ClientIDMode="Static" Value="" runat="server" />
        <asp:HiddenField ID="HDF_Id" ClientIDMode="Static" Value="" runat="server" />

    </form>

    <script>

        var $popup = $('.service-process .popup');
        var $main = $('.service-process .main');
        var $btnPopup = $('.service-process .btn-popup');
        var $btnSubmit = $('.submit')
        var $btnCancel = $popup.find('.cancel .btn');
        var $btnConfirm = $popup.find('.confirm .btn');
        var $checkboxSelect = $popup.find('.checkbox-select');
        var $radioSelect = $popup.find('.radio-select');
        var $salonId = $('#HDF_Salon_Id');
        var $teamId = $('#HDF_Team_Id');
        var $table = $('.service-process table')


        $(document).ready(function () {
            $('.multil-select').multiselect();
            $checkboxSelect.find('.checkbox').checkbox();
            $radioSelect.find('.checkbox').checkbox();

            //bindPendingByTeam();

            // Sync bill
            var requestLoadBill = setInterval(loadBillToTeam, 1000);

            loadLocalStore();
        });

        function logOut() {
            console.log("clear all local store");
            localStorage.clear();
            //go to logout to clear all cookies
            window.location.href = "/dang-xuat.html";
        }

        $(window).on('mouseover', (function () {
            window.onbeforeunload = null;
        }));
        $(window).on('mouseout', (function () {
            window.onbeforeunload = ConfirmLeave;
        }));

        function ConfirmLeave() {
            return "";
        }
        var prevKey = "";
        $(document).keydown(function (e) {
            if (e.key == "F5") {
                window.onbeforeunload = ConfirmLeave;
            }
            else if (e.key.toUpperCase() == "W" && prevKey == "CONTROL") {
                window.onbeforeunload = ConfirmLeave;
            }
            else if (e.key.toUpperCase() == "R" && prevKey == "CONTROL") {
                window.onbeforeunload = ConfirmLeave;
            }
            else if (e.key.toUpperCase() == "F4" && (prevKey == "ALT" || prevKey == "CONTROL")) {
                window.onbeforeunload = ConfirmLeave;
            }
            prevKey = e.key.toUpperCase();
        });

        function loadLocalStore() {
            var skinners = JSON.parse(localStorage.getItem("skinners"));
            var stylists = JSON.parse(localStorage.getItem("stylists"));
            console.log(skinners);

            bindSkinners(skinners);
            bindStylists(stylists);

        }



        $btnPopup.bind('click', function () {
            //cases
            //change-team : change team color
            //add-skinner: add a skinner
            //add-stylist: add a stylist
            //choose-services: choose many services
            //change skinner: change this skinner
            //change stylist: change this stylist
            popup($(this));

        })
        function popup(This) {
            //typeAction: input-data or confirm
            var dataType = $(This).attr('data-type');
            var dataOption = $(This).attr('data-option');
            //var typeAction = $(This).attr('data-action');
            //if (dataType2 == null || dataType2 == "")

            $('#HDF_Data_Type').val(dataType);
            // else dataType = dataType2;

            optionSelect(dataOption);
            //console.log(dataOption + "|" + typeAction);
            switch (dataType) {
                case 'change-team':
                    bindTeam();
                    $popup.show();
                    break;
                case 'add-skinner':
                    bindStaff(2, 'checkbox');
                    $popup.show();
                    break;
                case 'add-stylist':
                    bindStaff(1, 'checkbox')
                    $popup.show();
                    break;
                case 'choose-services':
                    bindService()
                    $popup.show();
                    break;
                case 'change-skinner':
                    var list = JSON.parse(localStorage.getItem("skinners"));
                    // var stylists = JSON.parse(localStorage.getItem("stylists"));
                    bindChooseStaff(list, 'radio');
                    $popup.show();
                    break;
                case 'change-stylist':
                    //var skinners = JSON.parse(localStorage.getItem("skinners"));
                    var list = JSON.parse(localStorage.getItem("stylists"));
                    bindChooseStaff(list, 'radio');
                    $popup.show();
                    break;
                case 'del-stylist':
                    var id = This.attr('data-id');
                    $('#HDF_Id').val(id);
                    $popup.show();
                    break;

                case 'del-skinner':
                    var id = This.attr('data-id');
                    $('#HDF_Id').val(id);
                    $popup.show();
                    break;

                case 'log-out':
                    $popup.show();
                    break;

            }




        }

        $btnSubmit.bind('click', function () {
            var action = $(this).find('.btn').attr('data-action');
            var dataType = $('#HDF_Data_Type').val();
            //var dataType = $(this).attr('data-type');
            //console.log(dataType);
            if (action != 'cancel') {
                //do action
                switch (dataType) {
                    case 'change-team':
                        break;
                    case 'add-skinner':

                        var list = getItemsChecked();
                        // console.log(list);
                        bindSkinners(list);
                        // Store
                        var jsonList = JSON.stringify(list);
                        localStorage.setItem("skinners", jsonList);
                        // Retrieve
                        window.location.reload();
                        break;
                    case 'add-stylist':

                        var list = getItemsChecked();
                        bindStylists(list);
                        var jsonList = JSON.stringify(list);
                        localStorage.setItem("stylists", jsonList);
                        window.location.reload();
                        break;
                    case 'choose-services':

                        break;
                    case 'change-skinner':

                        break;
                    case 'change-stylist':

                        break;
                    case 'del-stylist':

                        var id = $('#HDF_Id').val();
                        var stylists = JSON.parse(localStorage.getItem("stylists"));
                        var list = updateList(stylists, id);

                        var jsonList = JSON.stringify(list);
                        localStorage.setItem("stylists", jsonList);
                        window.location.reload();
                        break;
                    case 'del-skinner':
                        var id = $('#HDF_Id').val();
                        var skinners = JSON.parse(localStorage.getItem("skinners"));
                        console.log(id);

                        var list = updateList(skinners, id);

                        var jsonList = JSON.stringify(list);
                        localStorage.setItem("skinners", jsonList);
                        window.location.reload();
                        break;

                    case 'log-out':
                        logOut();
                        break;
                }

            }

            $popup.hide();
        })



        function updateList(list, Id) {
            //    ------- delete item -----------
            for (var i = 0; i < list.length; i++) {
                if (list[i].Id == Id) {
                    list.splice(i, 1);
                    i--;
                    break;
                }
            }
            console.log(list);
            return list;
        }

        function bindSkinners(list) {
            var $list = $('.service-process .contents.skinners');
            var item = "";
            $(list).each(function (i, v) {
                item = "<p class='btn-dotted btn-popup skinner name'  onclick='popup($(this))' data-id='" + v.Id + "' data-order='" + v.OrderCode + "' data-type='del-skinner' data-option='del-skinner'>" + v.FullName + "</p>";
                $list.append(item);
            });
        }

        function bindStylists(list) {
            var $list = $('.service-process .contents.stylists');
            var item = "";
            $(list).each(function (i, v) {
                item = "<p class='btn-dotted btn-popup stylist name'  onclick='popup($(this))' data-id='" + v.Id + "' data-order='" + v.OrderCode + "' data-type='del-stylist' data-option='del-stylist'>" + v.FullName + "</p>";
                $list.append(item);
            });
        }

        function getItemsChecked() {
            var list = [];
            var $listItems = $('.service-process .checkbox-select .item');
            $($listItems).each(function (i, v) {
                var checked = $(v).find('div.child').hasClass('checked');
                if (checked) {
                    var obj = new Object();
                    var fullName = $(v).find('div.child input').attr('name');
                    var id = $(v).find('div.child input').attr('data-id');
                    var names = fullName.trim().split(" ");
                    obj.FullName = names[names.length - 1];
                    obj.Id = id;
                    obj.OrderCode = $(v).find('div.child input').attr('data-order');
                    list.push(obj);

                }
            })
            return list;
        }



        function bindTeam() {
            console.log('set team ');
        }


        function bindPendingByTeam() {
            var salonId = $salonId.val();
            var teamId = $teamId.val();
            $.ajax({
                type: 'POST',
                url: '/GUI/SystemService/Webservice/BookingService.asmx/GetBillPendingByTeam',
                data: '{salonId:"' + salonId + '",teamId:"' + teamId + '"}',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (response) {
                    var list = JSON.parse(response.d);

                    var $tds = [];
                    var listIndex = [1, 4, 7, 10];
                    $(listIndex).each(function (i, v) {
                        var position = getPositionTable(v);
                        $tds.push(position);
                    })

                    //console.log($tds);
                    $(list).each(function (i, v) {
                        var stylist = v.HairMassageName || "stylist";
                        var skinner = v.HairdresserName || "Skinner";
                        var bill = "<div class='item bill'><div class='contents'>" +
                                                "<p class='customer name'>" + v.CustomerName + "</p><div class='services right'>" +
                                                    "<p class='btn-dotted btn-popup name' data-action='input-data' data-type='choose-services' data-option='checkbox'  onclick='popup($(this))'>SC, SM, U</p>" +
                                                "</div></div><div class='contents staffs'><div class='names'>" +
                                                    "<p class='btn-dotted btn-popup skinner' data-action='input-data' data-type='change-skinner' data-option='radio' onclick='popup($(this))'>" + skinner + "</p>" +

                                                    "<p class='btn-dotted btn-popup stylist right' data-action='input-data' data-type='change-stylist' data-option='radio'  onclick='popup($(this))'>" + stylist + "</p></div>" +
                                                "<div class='next-step right' data-step='0' onclick=\"nextStep($(this))\">" +
                                                    "<p class='btn right'>Gội &gt;&gt;</p></div></div> </div>";

                        $tds[i].empty().append(bill);
                    })

                }, failure: function (response) { alert(response.d); }
            });
        }

        function nextStep01(This) {
            var step = $(This).attr('data-step');
            var $tr = This.parent().parent().parent().parent();

            console.log(step + "|" + $tr);
            //console.log($tr);

            switch (step) {
                case '0':
                    $(This).attr('data-step', '1');
                    This.find('.btn').text("Cắt >>");
                    var bill = This.parent().parent();
                    $tr.find('td.td-2').empty().append(bill);
                    break;
                case '1':
                    //step goi
                    //next step: cat

                    $(This).attr('data-step', '2');
                    This.find('.btn').text("Hoàn thành >>");
                    var bill = This.parent().parent();
                    $tr.find('td.td-3').empty().append(bill);

                    break;
                case '2':
                    //step goi
                    //next step: hoan thanh
                    $($tr).remove();
                    break;

            }
        }

        function bindStaff(staffType, typeOption) {
            //staffType 2 or 1 : skinner or stylist
            //typeOption: radio or checkbox
            var salonId = $salonId.val();
            var teamId = $teamId.val();

            //console.log(salonId + "|" + teamId + "|" + staffType + "|" + typeOption);

            var listChecked;
            console.log(staffType);
            if (staffType == 2)
                listChecked = JSON.parse(localStorage.getItem("skinners"));
            else if (staffType == 1)
                listChecked = JSON.parse(localStorage.getItem("stylists"));

            console.log(listChecked);
            $.ajax({
                type: 'POST',
                url: '/GUI/SystemService/Webservice/BookingService.asmx/GetStaff',
                data: '{salonId:"' + salonId + '",teamId:"' + teamId + '",staffType:"' + staffType + '"}',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                success: function (response) {
                    var list = JSON.parse(response.d);
                    if (typeOption == 'checkbox') {
                        bindCheckbox(list, listChecked);
                    }
                    else if (typeOption == 'radio') {
                        bindRadio(list, listChecked);
                    }


                    $checkboxSelect.find('.checkbox').checkbox();
                    $radioSelect.find('.checkbox').checkbox();

                }, failure: function (response) { alert(response.d); }
            });
        }

        function bindChooseStaff(list, typeOption) {
            //
            // console.log(list);
            if (typeOption == 'checkbox')
                bindCheckbox(list, listChecked);
            else if (typeOption == 'radio')
                bindRadio(list, listChecked);

            $checkboxSelect.find('.checkbox').checkbox();
            $radioSelect.find('.checkbox').checkbox();
        }

        function bindCheckbox(list, listChecked) {
            var items = "";
            //  console.log(listChecked);
            $(list).each(function (i, v) {
                //console.log(i);
                var checked = "";
                if (listChecked != null)
                    for (j = 0; j < listChecked.length; ++j) {
                        var idC = listChecked[j].Id;
                        if (idC == v.Id)
                            checked = "checked";
                    }

                items += "<div class='item'>" +
                "<div class='ui child checkbox " + checked + "'>" +
                    "<input type='checkbox' data-id='" + v.Id + "' name='" + v.FullName + "' data-order='" + v.OrderCode + "' class='hidden'>" +
                    "<label>" + v.FullName + "</label>" +
                    "<p class='order-code'>" + v.OrderCode + "</p>" +
                "</div>" +
            "</div>";
            })

            var $listCheckBox = $('.service-process .checkbox-select');
            $checkboxSelect.empty().append(items);
        }

        function bindRadio(list, staffType) {

            var items = "";
            $(list).each(function (i, v) {
                items += "<div class='field'>" +
                                    "<div class='ui radio checkbox'>" +
                                        "<input type='radio' data-id='" + v.Id + "' name='" + v.FullName + "' data-order='" + v.OrderCode + "' tabindex='0' class='hidden'>" +
                                        "<label>" + v.FullName + "</label>" +
                                          "<p class='order-code'>" + v.OrderCode + "</p>" +
                                    "</div>" +
                                "</div>";
            })
            var $listRadio = $('.service-process .radio-select .grouped.fields');
            $listRadio.empty().append(items);

        }

        function bindService() {
            //   optionSelect('checkbox');
        }

        function optionSelect(typeSelect) {
            //typeSelect: checkbox or radio

            switch (typeSelect) {
                case 'checkbox':
                    $btnConfirm.text("OK");
                    $checkboxSelect.show();
                    $radioSelect.hide();
                    $btnConfirm.attr('data-action', 'checkbox');
                    break;
                case 'radio':
                    $btnConfirm.text("OK");
                    $checkboxSelect.hide();
                    $radioSelect.show();
                    $btnConfirm.attr('data-action', 'radio');
                    break;
                case 'del-stylist':
                    $btnConfirm.text("Xóa");
                    $radioSelect.hide();
                    $checkboxSelect.hide();
                    $btnConfirm.attr('data-action', 'del');
                    break;
                case 'del-skinner':
                    $btnConfirm.text("Xóa");
                    $radioSelect.hide();
                    $checkboxSelect.hide();
                    $btnConfirm.attr('data-action', 'del');
                    break;
                case 'log-out':
                    $btnConfirm.text("Đăng xuất");
                    $radioSelect.hide();
                    $checkboxSelect.hide();
                    $btnConfirm.attr('data-action', 'log-out');
                    break;

            }
        }

        function getPositionTable(index) {
            //tr is col 
            //td is row
            //1-12 position
            var $tbody = $('.service-process table tbody');

            var tr1 = '.tr-1';
            var tr2 = '.tr-2';
            var tr3 = '.tr-3';
            var tr4 = '.tr-4';

            var td1 = ' .td-1';
            var td2 = ' .td-2';
            var td3 = ' .td-3';

            var position;
            switch (index) {
                case 1:
                    position = $tbody.find(tr1 + td1);
                    break;
                case 2:
                    position = $tbody.find(tr1 + td2);
                    break;
                case 3:
                    position = $tbody.find(tr1 + td3);
                case 4:
                    position = $tbody.find(tr2 + td1);
                    break;
                case 5:
                    position = $tbody.find(tr2 + td2);
                    break;
                case 6:
                    position = $tbody.find(tr2 + td3);
                    break;
                case 7:
                    position = $tbody.find(tr3 + td1);
                    break;
                case 8:
                    position = $tbody.find(tr3 + td2);
                    break;
                case 9:
                    position = $tbody.find(tr3 + td3);
                    break;
                case 10:
                    position = $tbody.find(tr4 + td1);
                    break;
                case 11:
                    position = $tbody.find(tr4 + td2);
                    break;
                case 12:
                    position = $tbody.find(tr4 + td3);
                    break;

            }
            //console.log(position);
            return position;
        }

        //=====================
        // Sync bill
        //=====================
        function loadBillToTeam() {
            var teamId = $("#HDF_Team_Id").val();
            var salonId = $("#HDF_Salon_Id").val();
            //return;
            $.ajax({
                type: "POST",
                url: "/GUI/FrontEnd/Service/Service_Process.aspx/loadBillToTeam",
                data: "{salonId : " + salonId + ", teamId : " + teamId + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var mission = JSON.parse(response.d);
                    if (mission.success) {
                        var bills = JSON.parse(mission.msg);
                        $.each(bills, function (i, v) {
                            var skinner = v.skinnerId > 0 ? '<p class="btn-dotted btn-popup skinner" data-type="change-skinner" data-option="radio">' + v.skinnerName + '</p>' : '';
                            var stylist = v.stylistId > 0 ? '<p class="btn-dotted btn-popup stylist right" data-type="change-stylist" data-option="radio">' + v.stylistName + '</p>' : '';
                            var tr = '<tr class="tr-1">' +
                                        '<td class="td-1">' +
                                            '<div class="item bill">' +
                                                '<div class="contents">' +
                                                    '<p class="customer name">' + v.CustomerName + '</p>' +
                                                    '<div class="services right">' +
                                                        //'<p class="btn-dotted btn-popup name" data-type="choose-services" data-option="checkbox">SC, SM, U</p>' +
                                                    '</div>' +
                                                '</div>' +
                                                '<div class="contents staffs">' +
                                                    '<div class="names">' +
                                                        skinner +
                                                        stylist +
                                                    '</div>' +
                                                    '<div class="next-step right" onclick="nextStep($(this),' + v.Status + ',' + v.Id + ')">' +
                                                        '<p class="btn right">Gội >></p>' +
                                                    '</div>' +
                                                '</div>' +
                                            '</div>' +
                                        '</td><td class="td-2"></td><td class="td-3"></td>' +
                                    '</tr>';
                            $("table#TableTeamProcess").append($(tr));
                        });
                    } else {
                        console.log("No result.");
                    }
                },
                failure: function (response) { alert(response.d); }
            });
        }

        var staffId, blockActive, billStatus;
        function nextStep(This, status, bId) {
            blockActive = This.parent().parent();
            billStatus = status;
            var list;
            var li = '';
            switch (status) {
                case 0: list = JSON.parse(localStorage.getItem("skinners"));
                    if (list.length > 0) {
                        li += '<li><p class="popup-content-head">Chọn skinner</p></li>';
                        $.each(list, function (i, v) {
                            li += '<li onclick="getStaff($(this),' + v.Id + ',0,' + bId + ')">' +
                                        '<input type="radio" />' +
                                        '<label>' + v.FullName + '</label>' +
                                    '</li>';
                        });
                        $("ul.ul-popup-content").empty().append($(li));
                        $(".popup-wrap").show();
                    }
                    break;
                case 1: list = JSON.parse(localStorage.getItem("stylists"));
                    if (list.length > 0) {
                        li += '<li><p class="popup-content-head">Chọn stylist</p></li>';
                        $.each(list, function (i, v) {
                            li += '<li onclick="getStaff($(this),0,' + v.Id + ',' + bId + ')">' +
                                        '<input type="radio" />' +
                                        '<label>' + v.FullName + '</label>' +
                                    '</li>';
                        });
                        $("ul.ul-popup-content").empty().append($(li));
                        $(".popup-wrap").show();
                    }
                    break;
                case 2: li = '<li><p class="popup-content-head">Hoàn thành dịch vụ ?</p></li>' +
                             '<li style="text-align:center; width: 100%; float: left;">' +
                                '<div class="col-xs-6 confirm-yes"><span class="confirm-button" onclick="completeBill($(this), ' + bId + ')">Xác nhận</span></div>' +
                                '<div class="col-xs-6 confirm-no"><span class="confirm-button" onclick="closePopupBig()">Đóng</span></div>' +
                                '</li>';
                    $("ul.ul-popup-content").empty().append($(li));
                    $(".popup-wrap").show();
                default: break;
            }

            /// Close popup
            $('.popup-wrap, .btn-close').click(function () {
                $(".popup-wrap").hide();
            });

            $('.popup-content-wp').bind('click', function (event) {
                event.stopPropagation();
            });
        }

        function completeStep(This, status, bId) {
            $.ajax({
                type: "POST",
                url: "/GUI/FrontEnd/Service/Service_Process.aspx/updateCompleteStep",
                data: "{bId : " + bId + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var mission = JSON.parse(response.d);
                    if (mission.success) {
                        var bills = JSON.parse(mission.msg);
                        //blockActive.parent().next().append(blockActive);
                        This.hide();
                        This.parent().find(".next-button").show();
                    } else {
                        console.log("No result.");
                    }
                },
                failure: function (response) { alert(response.d); }
            });
        }

        function completeBill(This, bId) {
            $.ajax({
                type: "POST",
                url: "/GUI/FrontEnd/Service/Service_Process.aspx/updateCompleteStep",
                data: "{bId : " + bId + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var mission = JSON.parse(response.d);
                    if (mission.success) {
                        var bills = JSON.parse(mission.msg);
                        blockActive.parent().parent().remove();
                        $(".popup-wrap").hide();
                    } else {
                        console.log("No result.");
                    }
                },
                failure: function (response) { alert(response.d); }
            });
        }

        function getStaff(This, skinnerId, stylistId, bId) {

            $.ajax({
                type: "POST",
                url: "/GUI/FrontEnd/Service/Service_Process.aspx/updateBillStep",
                data: "{bId : " + bId + ", skinnerId : " + skinnerId + ", stylistId : " + stylistId + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var mission = JSON.parse(response.d);
                    if (mission.success) {
                        var bill = JSON.parse(mission.msg);
                        var skinner = bill.skinnerId > 0 ? '<p class="btn-dotted btn-popup skinner" data-type="change-skinner" data-option="radio">' + bill.skinnerName + '</p>' : '';
                        var stylist = bill.stylistId > 0 ? '<p class="btn-dotted btn-popup stylist right" data-type="change-stylist" data-option="radio">' + bill.stylistName + '</p>' : '';
                        var buttonNext = '';
                        switch (bill.Status) {
                            case 1: buttonNext = '<div class="next-step right complete-button" onclick="completeStep($(this),' + bill.Status + "," + bill.Id + ')">' +
                                                    '<p class="btn right">Xong >></p>' +
                                                '</div>' +
                                                '<div class="next-step right next-button" onclick="nextStep($(this),' + bill.Status + "," + bill.Id + ')" style="display:none;">' +
                                                    '<p class="btn right">Cắt >></p>' +
                                                '</div>'; break;
                            case 2: buttonNext = '<div class="next-step right" onclick="nextStep($(this),' + bill.Status + "," + bill.Id + ')">' +
                                                    '<p class="btn right">Hoàn thành >></p>' +
                                                '</div>'; break;
                            default: break;
                        }
                        var block = '<div class="item bill">' +
                                        '<div class="contents">' +
                                            '<p class="customer name">' + bill.CustomerName + '</p>' +
                                            '<div class="services right">' +
                                                //'<p class="btn-dotted btn-popup name" data-type="choose-services" data-option="checkbox">SC, SM, U</p>' +
                                            '</div>' +
                                        '</div>' +
                                        '<div class="contents staffs">' +
                                            '<div class="names">' +
                                                skinner +
                                                stylist +
                                            '</div>' +
                                            buttonNext +
                                        '</div>' +
                                    '</div>';
                        blockActive.parent().next().append(block);
                        blockActive.parent().empty();
                        $(".popup-wrap").hide();
                    } else {
                        console.log("No result.");
                    }
                },
                failure: function (response) { alert(response.d); }
            });
        }

        function closePopupBig() {
            $(".popup-wrap").hide();
        }

        jQuery(document).ready(function ($) {
            // Fullscreen
            var DomFullScreenInBtn = $(".fullscreen"),
                DomFullScreenOutBtn = $(".exit-fullscreen"),
                dom = document.getElementById("ServiceProcess"),
                isFullScreen = false;
            var fullScreenIn = function () {
                if (dom.requestFullscreen) {
                    dom.requestFullscreen();
                } else if (dom.msRequestFullscreen) {
                    dom.msRequestFullscreen();
                } else if (dom.mozRequestFullScreen) {
                    dom.mozRequestFullScreen();
                } else if (dom.webkitRequestFullscreen) {
                    dom.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
                }
            }
            var fullScreenOut = function () {
                if (document.exitFullscreen) {
                    document.exitFullscreen();
                } else if (document.msExitFullscreen) {
                    document.msExitFullscreen();
                } else if (document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else if (document.webkitExitFullscreen) {
                    document.webkitExitFullscreen();
                }
            }
            var toggleFullScreen = function () {
                // Full screen
                if (!isFullScreen) {  // current working methods   
                    if (dom.requestFullscreen) {
                        dom.requestFullscreen();
                    } else if (dom.msRequestFullscreen) {
                        dom.msRequestFullscreen();
                    } else if (dom.mozRequestFullScreen) {
                        dom.mozRequestFullScreen();
                    } else if (dom.webkitRequestFullscreen) {
                        dom.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
                    }
                    isFullScreen = true;
                } else {
                    if (document.exitFullscreen) {
                        document.exitFullscreen();
                    } else if (document.msExitFullscreen) {
                        document.msExitFullscreen();
                    } else if (document.mozCancelFullScreen) {
                        document.mozCancelFullScreen();
                    } else if (document.webkitExitFullscreen) {
                        document.webkitExitFullscreen();
                    }
                    isFullScreen = false;
                }

            }

            DomFullScreenInBtn.bind("click", function () {
                var status = $(this).attr("data-fullscreen");
                if (status == "false") {
                    $(".body-wrap-background").show();
                    $(".exit-fullscreen").show();
                    fullScreenIn();
                    $(".fullscreen").attr("data-fullscreen", "true");
                } else if (status == "true") {
                    $(".body-wrap-background").hide();
                    $(".exit-fullscreen").hide();
                    fullScreenOut();
                    $(".fullscreen").attr("data-fullscreen", "false");
                }

                $(document).bind("keyup", function (e) {
                    if (e.keyCode == 27) {
                        $(".body-wrap-background").hide();
                        $(".exit-fullscreen").hide();
                    }
                });
            });
            DomFullScreenOutBtn.bind("click", function () {
                var status = $(".fullscreen").attr("data-fullscreen");
                if (status == "true") {
                    $(".body-wrap-background").hide();
                    $(".exit-fullscreen").hide();
                    fullScreenOut();
                    $(".fullscreen").attr("data-fullscreen", "false");
                }
            });
            
        });
    </script>

    <div class="body-wrap-background"></div>
    <style>
        .body-wrap-background { width: 100%; height: 100%; float: left; position: fixed; top: 0; left: 0; background: rgba(0,0,0,0.7); display: none; z-index: 100000; }
    </style>
</body>
</html>
