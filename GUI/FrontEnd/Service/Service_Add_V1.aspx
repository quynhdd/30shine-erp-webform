﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Service_Add_V1.aspx.cs" Inherits="_30shine.GUI.UIService.Service_Add_V1" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ServiceAdd" ContentPlaceHolderID="CtMain" runat="server">
<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet"/>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>

<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">
                <li class="li-listing active"><a href="/dich-vu/danh-sach.html">Danh sách</a></li>
                <% if(SalonId == 2){ %>
                    <li class="li-pending active"><a href="/dich-vu/pending.html"><div class="pending-1"></div>Pending</a></li>
                    <li class="li-add"><a href="/dich-vu/them-phieu.html">Thêm mới</a></li>
                <% }else{ %>
                    <li class="li-add"><a href="/dich-vu/them-moi.html">Thêm mới</a></li>
                 <li class="li-add active"><a href="/dich-vu/dat-lich.html">Đặt lịch</a></li>
                <% } %>
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add admin-product-add fe-service">    
    <%-- Add --%>                
    <div class="wp960 content-wp">
        <!-- System Message -->
        <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
        <!-- END System Message -->

        <div class="table-wp">
            <table class="table-add admin-product-table-add fe-service-table-add">
                <tbody>
                    <tr class="title-head">
                        <td><strong>Thông tin dịch vụ</strong></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr class="tr-margin" style="height: 20px;"></tr>

                    <tr class="tr-field-ahalf">
                        <td class="col-xs-2 left"><span>Khách hàng</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="CustomerName" runat="server" ClientIDMode="Static" ReadOnly="true" style="width:35%;"></asp:TextBox>
                                <asp:TextBox ID="CustomerCode" CssClass="mgl" placeholder="Nhập mã khách hàng ở đây" runat="server" 
                                    ClientIDMode="Static" onchange="LoadCustomer(this.value)" style="width: 25%;"></asp:TextBox>
                                <span class="checkbox cus-no-infor" style="width: auto;">
                                    <label>
                                        <input type="checkbox" id="InputCusNoInfor" runat="server" ClientIDMode="Static" style="top: 10px;"/> Khách không cho thông tin
                                    </label>
                                </span>
                                <%--<asp:RequiredFieldValidator ID="ValidateCustomerName" ControlToValidate="CustomerName" runat="server" 
                                    CssClass="fb-cover-error" Text="Bạn chưa nhập mã khách hàng!" ClientIDMode="Static"></asp:RequiredFieldValidator>--%>
                            </span>                            
                        </td>
                    </tr>

                    <tr class="tr-field-ahalf tr-product">
                        <td class="col-xs-2 left"><span>Dịch vụ</span></td>
                        <td class="col-xs-9 right">
                            <%--<span class="field-wp">
                                <asp:DropDownList ID="ServiceName" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <asp:RequiredFieldValidator InitialValue="0" 
                                    ID="rfvDDL_Product" Display="Dynamic" 
                                    ControlToValidate="ServiceName"
                                    runat="server"  Text="Bạn chưa chọn dịch vụ!" 
                                    ErrorMessage="Please Select the Product"
                                    ForeColor="Red"
                                    CssClass="fb-cover-error select-service-error">
                                </asp:RequiredFieldValidator>
                            </span>--%>
                            <div class="row">
                                <div class="show-product show-item" data-item="service"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Thêm dịch vụ</div>
                            </div>                            
                            <div class="listing-product item-service">
                                <table class="table table-listing-product table-item-service">
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Tên dịch vụ</th>
                                            <th>Mã dịch vụ</th>
                                            <th>Đơn giá</th>
                                            <th>Số lượng</th>
                                            <th>Giảm giá</th>
                                            <th>Thành tiền</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>  
                        </td>
                    </tr>

                    <tr class="tr-field-ahalf tr-field-third">
                        <td class="col-xs-2 left"><span>Nhân viên</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:DropDownList ID="Hairdresser" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <asp:DropDownList ID="HairMassage" CssClass="mgl" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <asp:DropDownList ID="Cosmetic" CssClass="mgl" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <span id="ValidateCosmetic" class="fb-cover-error" style="visibility:hidden; top: -28px; width: 255px;">Bạn chưa chọn nhân viên bán mỹ phẩm!</span>
                            </span>
                                            
                        </td>
                    </tr>

                    <tr class="tr-field-ahalf" style="display:none;">
                        <td class="col-xs-2 left"><span>Điểm đánh giá</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:DropDownList ID="Mark" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="ValidateMark" ControlToValidate="Mark" runat="server" CssClass="fb-cover-error" Text="Bạn chưa đánh giá!"></asp:RequiredFieldValidator>
                            </span>
                                            
                        </td>
                    </tr>

                    <tr class="tr-field-ahalf tr-product">
                        <td class="col-xs-2 left"><span>Sản phẩm</span></td>
                        <td class="col-xs-9 right">
                            <div class="row">
                                <div class="show-product show-item"  data-item="product"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Thêm sản phẩm</div>
                            </div>                            
                            <div class="listing-product item-product">
                                <table class="table table-listing-product table-item-product">
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Tên sản phẩm</th>
                                            <th>Mã sản phẩm</th>
                                            <th>Đơn giá</th>
                                            <th>Số lượng</th>
                                            <th>Giảm giá</th>
                                            <th>Thành tiền</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>                                        
                        </td>
                    </tr>

                    <tr class="tr-description tr-field-ahalf">
                        <td class="col-xs-2 left"><span>Tổng số tiền</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="TotalMoney" runat="server" ClientIDMode="Static" Text="0" ReadOnly="true"></asp:TextBox>
                                <span class="unit-money">VNĐ</span>
                            </span>
                        </td>
                    </tr>

                    <tr class="tr-upload" style="display:none;">
                        <td class="col-xs-2 left"><span>Đăng ảnh</span></td>
                        <td class="col-xs-9 right">
                            <div class="wrap btn-upload-wp">
                                <div id="Btn_UploadImg" class="btn-upload-img" onclick="OpenIframeImage()">Chọn ảnh đăng</div>
                                <div class="wrap listing-img-upload"></div>
                            </div>                         
                        </td>
                    </tr>

                    <tr class="tr-description">
                        <td class="col-xs-2 left"><span>Ghi chú</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox TextMode="MultiLine" Rows="5" ID="NoteText" runat="server" ClientIDMode="Static"></asp:TextBox>
                            </span>
                        </td>
                    </tr>

                    <tr class="tr-field-ahalf" runat="server" id="TrSalon" visible="false">
                        <td class="col-xs-2 left"><span>Salon</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:DropDownList ID="Salon" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <asp:RequiredFieldValidator InitialValue="0" 
                                    ID="ValidateSalon" Display="Dynamic" 
                                    ControlToValidate="Salon"
                                    runat="server"  Text="Bạn chưa chọn Salon!" 
                                    ErrorMessage="Vui lòng chọn Salon!"
                                    ForeColor="Red"
                                    CssClass="fb-cover-error">
                                </asp:RequiredFieldValidator>
                            </span>
                        </td>
                    </tr>
                    
                    <tr class="tr-send">
                        <td class="col-xs-2 left"></td>
                        <td class="col-xs-9 right no-border">
                            <span class="field-wp">
                                <asp:Panel ID="BtnSend" CssClass="btn-send" runat="server" ClientIDMode="Static">Hoàn tất</asp:Panel>
                                <asp:Button ID="BtnFakeSend" runat="server" Text="Hoàn tất" 
                                    ClientIDMode="Static" OnClick="AddService" style="display:none;"></asp:Button>
                                <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>
                            </span>
                        </td>
                    </tr>
                    <asp:HiddenField runat="server" ID="UrlKeyField"  ClientIDMode="Static"/>
                    <asp:HiddenField runat="server" ID="ImgCoverField"  ClientIDMode="Static"/>
                </tbody>
            </table>

            <!-- input hidden -->
            <asp:HiddenField ID="HDF_CustomerCode" runat="server" ClientIDMode="Static"/>
            <asp:HiddenField ID="HDF_ProductIds" runat="server" ClientIDMode="Static"/>
            <asp:HiddenField ID="HDF_ServiceIds" runat="server" ClientIDMode="Static"/>
            <asp:HiddenField ID="HDF_TotalMoney" runat="server" ClientIDMode="Static"/>
            <asp:HiddenField ID="HDF_UserImages" runat="server" ClientIDMode="Static"/>
            <asp:HiddenField ID="HDF_UserImagesDelete" runat="server" ClientIDMode="Static"/>
            <!-- END input hidden -->
        </div>
    </div>
    <%-- end Add --%>
</div>

<!-- Danh mục sản phẩm -->
<div class="popup-product-wp popup-product-item">
    <div class="wp popup-product-head">
        <strong>Danh mục sản phẩm</strong>
    </div>
    <div class="wp popup-product-content" >
        <div class="wp popup-product-guide">
            <div class="left">Hướng dẫn:</div>
            <div class="right">
                <p>- Click vào ô check box để chọn sản phẩm</p>
                <p>- Click vào ô số lượng để thay đổi số lượng sản phẩm</p>
            </div>
        </div>
        <div class="wp listing-product item-product">
            <table class="table">
                <thead>
                    <tr>
                        <th><input type="checkbox" /></th>
                        <th>STT</th>
                        <th>Tên sản phẩm</th>
                        <th>Mã sản phẩm</th>
                        <th>Đơn giá</th>
                        <th class="th-product-quantity">Số lượng</th>
                        <th>Giảm giá</th>
                        <th>Thành tiền</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="Rpt_Product" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td class="td-product-checkbox item-product-checkbox">
                                    <input type="checkbox" value="<%# Eval("Id") %>"
                                        data-code="<%# Eval("Code") %>" data-name="<%# Eval("Name") %>"/>
                                </td>
                                <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                <td class="td-product-name"><%# Eval("Name") %></td>
                                <td class="td-product-code" data-id="<%# Eval("Id") %>"><%# Eval("Code") %></td>
                                <td class="td-product-price" data-price="<%# Eval("Price") %>"><%# Eval("Price") %></td>
                                <td class="td-product-quantity">
                                    <input type="text" class="product-quantity" value="1" />                                
                                </td>
                                <td class="td-product-voucher" data-voucher="<%# Eval("VoucherPercent") %>"><%# Eval("VoucherPercent") %> (%)</td>
                                <td class="map-edit">
                                    <div class="box-money"></div>
                                    <div class="edit-wp">
                                        <a class="elm del-btn" onclick="RemoveItem(this.parentNode.parentNode.parentNode,'<%# Eval("Name") %>','product')"
                                             href="javascript://" title="Xóa"></a>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>                                        
                </tbody>
            </table>
        </div> 
        <div class="wp btn-wp">
            <div class="popup-product-btn btn-complete" data-item="product">Hoàn tất</div>
            <div class="popup-product-btn btn-esc">Thoát</div>
        </div>
    </div>    
</div>
<!-- END Danh mục sản phẩm -->

<!-- Danh mục dịch vụ -->
<div class="popup-product-wp popup-service-item">
    <div class="wp popup-product-head">
        <strong>Danh mục dịch vụ</strong>
    </div>
    <div class="wp popup-product-content" >
        <div class="wp popup-product-guide">
            <div class="left">Hướng dẫn:</div>
            <div class="right">
                <p>- Click vào ô check box để chọn dịch vụ</p>
                <p>- Click vào ô số lượng để thay đổi số lượng dịch vụ</p>
            </div>
        </div>
        <div class="wp listing-product item-product">
            <table class="table">
                <thead>
                    <tr>
                        <th><input type="checkbox" /></th>
                        <th>STT</th>
                        <th>Tên dịch vụ</th>
                        <th>Mã dịch vụ</th>
                        <th>Đơn giá</th>
                        <th class="th-product-quantity">Số lượng</th>
                        <th>Giảm giá</th>
                        <th>Thành tiền</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="Rpt_Service" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td class="td-product-checkbox item-product-checkbox">
                                    <input type="checkbox" value="<%# Eval("Id") %>"
                                        data-code="<%# Eval("Code") %>" data-name="<%# Eval("Name") %>"/>
                                </td>
                                <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                <td class="td-product-name"><%# Eval("Name") %></td>
                                <td class="td-product-code" data-id="<%# Eval("Id") %>"><%# Eval("Code") %></td>
                                <td class="td-product-price" data-price="<%# Eval("Price") %>"><%# Eval("Price") %></td>
                                <td class="td-product-quantity">
                                    <input type="text" class="product-quantity" value="1" />                                
                                </td>
                                <td class="td-product-voucher" data-voucher="<%# Eval("VoucherPercent") %>"><%# Eval("VoucherPercent") %> (%)</td>
                                <td class="map-edit">
                                    <div class="box-money"></div>
                                    <div class="edit-wp">
                                        <a class="elm del-btn" onclick="RemoveItem(this.parentNode.parentNode.parentNode,'<%# Eval("Name") %>','service')"
                                            href="javascript://" title="Xóa"></a>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>                                        
                </tbody>
            </table>
        </div> 
        <div class="wp btn-wp">
            <div class="popup-product-btn btn-complete" data-item="service">Hoàn tất</div>
            <div class="popup-product-btn btn-esc">Thoát</div>
        </div>
    </div>    
</div>
<!-- END Danh mục dịch vụ -->

<!-- Popup Images -->
<link href="/Assets/css/plugin.upload.images/pui.style.css" rel="stylesheet" />
<div class="plugin-image">
    <div class="plugin-image-head be-report">
        <!-- Filter -->
        <div class="row">
            <div class="filter-item">
                <asp:TextBox CssClass="txtDateTime st-head TxtDateTimeFrom" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                    ClientIDMode="Static" runat="server"></asp:TextBox>
            </div>
            <strong class="st-head" style="margin-left: 10px;">
                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
            </strong>
            <div class="filter-item">
                <asp:TextBox CssClass="txtDateTime st-head TxtDateTimeTo" ID="TxtDateTimeTo" placeholder="Đến ngày"
                    ClientIDMode="Static" runat="server"></asp:TextBox>
            </div>
            <div class="filter-item">
                <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion SgCustomerCode" data-field="customer.code" data-value="0"
                    AutoCompleteType="Disabled" ID="SgCustomerCode" ClientIDMode="Static" placeholder="Mã Khách hàng" runat="server" onblur="BindIdToHDF($(this), $(this).val(), 'customer.code', '#HDF_Suggestion_Code', '#HDF_Suggestion_Field', '#SgCustomerCode', true)"></asp:TextBox>
                <div class="listing-staff-wp eb-select-data ">
                    <ul class="ul-listing-staff ul-listing-suggestion" id="UlCustomerCode"></ul>
                </div>
            </div>
            <div class="filter-item">
                <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                    <ul class="ul-listing-staff ul-listing-suggestion" id="UlCustomerName">
                    </ul>
                </div>
            </div>
            
            <div class="filter-item">
                <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion SgCustomerPhone" data-value="0" data-field="customer.phone"
                        AutoCompleteType="Disabled" ID="SgCustomerPhone" ClientIDMode="Static" placeholder="Số điện thoại" runat="server" onblur="BindIdToHDF($(this), $(this).val(), 'customer.phone', '#HDF_Suggestion_Code', '#HDF_Suggestion_Field', '#SgCustomerPhone', true)"></asp:TextBox>
                <div class="listing-staff-wp eb-select-data">
                    <ul class="ul-listing-staff ul-listing-suggestion" id="UlCustomerPhone"></ul>
                </div>
            </div>
            <a href="javascript://" class="st-head btn-viewdata plugin-image-view" onclick="Plugin_Images_Load()">Xem ảnh</a>
            <%--<a href="javascript://" class="st-head btn-viewdata plugin-image-upload" onclick="Plugin_Images_Upload('<%=_CustomerCode %>')">Upload</a>--%>
            <a href="javascript://" class="st-head btn-viewdata plugin-image-push" onclick="Plugin_Images_Push()">Chèn ảnh</a>
        </div>
        <!-- End Filter -->
    </div>
    <div class="plugin-image-content">
        <div class="listing-img-upload">                  
            <asp:Repeater ID="Rpt_Images" runat="server">
                <ItemTemplate>
                    <div class="thumb-wp" onclick="Plugin_Images_Chose(event, $(this))">
                        <img class="thumb" alt="" title="" src="<%#Eval("Value") %>" 
                            data-img="<%#Eval("Value") %>" />
                        <span class="delete-thumb" onclick="deleteThum($(this), '<%#Eval("Value") %>', false)"></span>
                        <div class="thumb-cover"></div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</div>
<div id="dZUpload" class="dropzone dz-work-flow">
    <div class="dz-default dz-message wrap listing-img-upload">
        Drop image here. 
    </div>
</div>
<!-- Hidden Field-->
<asp:HiddenField ID="HDF_Suggestion_Code" ClientIDMode="Static" runat="server" />
<asp:HiddenField ID="HDF_Suggestion_Field" ClientIDMode="Static" runat="server" />
<!-- END Hidden Field-->

<!--/ Popup Images -->

<!-- Suggestion -->
<script src="/assets/js/jquery.mCustomScrollbar.js"></script>
<link href="/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
<script type="text/javascript">
    var Imgs = [],
        ImgsDelete = [],
        DbImgs = [],
        InitDropZone = false;

    jQuery(document).ready(function ($) {
        //
    });

    function Plugin_Images_Chose(event, This) {
        if (This.hasClass("active")) {
            This.removeClass("active");
        } else {
            This.addClass("active");
        }
    }

    function Plugin_Images_Push() {
        $('#EBPopup .listing-img-upload .thumb-wp.active').each(function () {
            var dataImg = $(this).find("img.thumb").attr("data-img");
            if (Imgs.indexOf(dataImg) == -1 && DbImgs.indexOf(dataImg) == -1) {
                $(".fe-service-table-add .listing-img-upload").append($(this).removeClass("active"));
            }
        });
        $(".fe-service-table-add .listing-img-upload .thumb-wp").each(function () {
            var dataImg = $(this).find("img.thumb").attr("data-img");
            if (Imgs.indexOf(dataImg) == -1 && DbImgs.indexOf(dataImg) == -1) {
                Imgs.push(dataImg);
            }
        });
        autoCloseEBPopup();
    }

    function deleteThum(THIS, data, oldImg) {
        oldImg = oldImg || false;
        if (!oldImg) {
            var index = Imgs.indexOf(data);
            if (index != -1) {
                Imgs.splice(index, 1);
            }
        } else {
            ImgsDelete.push(data);
        }
        THIS.parent().remove();
    }

    function Bind_UserImagesToHDF() {
        var jsonImages = JSON.stringify(Imgs);
        var jsonImagesDel = JSON.stringify(ImgsDelete);
        $("#HDF_UserImages").val(jsonImages);
        $("#HDF_UserImagesDelete").val(jsonImagesDel);
    }

    function OpenIframeImage() {

        $(".plugin-image").openEBPopup();
        $(".plugin-image-view").click();
        // Bind Suggestion
        Bind_Suggestion();

        $('#EBPopup .plugin-image-content').mCustomScrollbar({
            theme: "dark-2",
            scrollInertia: 100
        });

        //============================
        // Datepicker
        //============================
        $('.txtDateTime').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'vi',
            startDate: '2014/10/10',
            format: 'd/m/Y',
            dateonly: true,
            showHour: false,
            showMinute: false,
            timepicker: false,
            onChangeDateTime: function (dp, $input) { }
        });

        // execute thumbnail size
        //excThumbWidth();

    }

    function Plugin_Images_Load() {
        var TimeFrom = $("#EBPopup .TxtDateTimeFrom").val() != undefined ? $("#EBPopup .TxtDateTimeFrom").val() : "",
            TimeTo = $("#EBPopup .TxtDateTimeTo").val() != undefined ? $("#EBPopup .TxtDateTimeTo").val() : "",
            SgCode = $("#HDF_Suggestion_Code").val(),
            SgField = $("#HDF_Suggestion_Field").val();
        $.ajax({
            type: "POST",
            url: "/GUI/FrontEnd/Service/Service_Detail.aspx/Ajax_LoadImages",
            data: '{TimeFrom : "' + TimeFrom + '", TimeTo : "' + TimeTo + '", SgCode : "' + SgCode + '", SgField : "' + SgField + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                var mission = JSON.parse(response.d);
                if (mission.success) {
                    var OBJ = JSON.parse(mission.msg);
                    var Thumbs = "";
                    if (OBJ.length > 0) {
                        $.each(OBJ, function (i, v) {
                            Thumbs += '<div class="thumb-wp" onclick="Plugin_Images_Chose(event, $(this))">' +
                                        '<img class="thumb" alt="" title="" src="' + v.Value + '" data-img="' + v.Value + '"/>' +
                                        '<span class="delete-thumb" onclick="deleteThum($(this), ' + "'" + v.Value + "'" + ', false)"></span>' +
                                        '<div class="thumb-cover"></div>' +
                                    '</div>';
                        });
                    } else {
                        //
                    }
                    $("#EBPopup .listing-img-upload").empty().append($(Thumbs));
                    // execute thumbnail size
                    excThumbWidth();
                } else {
                    //
                }
            },
            failure: function (response) { alert(response.d); }
        });
    }

    //============================
    // Suggestion Functions
    //============================
    function Bind_Suggestion() {
        $(".eb-suggestion").bind("keyup", function (e) {
            if (e.keyCode == 40) {
                UpDownListSuggest($(this));
            } else {
                Call_Suggestion($(this));
            }
        });
        $(".eb-suggestion").bind("focus", function () {
            Call_Suggestion($(this));
        });
        $(".eb-suggestion").bind("blur", function () {
            //Exc_To_Reset_Suggestion($(this));
        });
        $(window).unbind("click").bind("click", function (e) {
            if ((!e.target.className.match("eb-select") && e.target.parentElement.className.match("ul-listing-suggestion")) ||
                (!e.target.className.match("eb-select") && !e.target.parentElement.className.match("ul-listing-suggestion"))) {
                EBSelect_HideBox();
            }
        });
    }

    function UpDownListSuggest(This) {
        var UlSgt = This.parent().find(".ul-listing-suggestion"),
            index = 0,
            LisLen = UlSgt.find(">li").length - 1,
            Value;

        This.blur();
        UlSgt.find(">li.active").removeClass("active");
        UlSgt.find(">li:eq(" + index + ")").addClass("active");

        $(window).unbind("keydown").bind("keydown", function (e) {
            if (e.keyCode == 40) {
                if (index == LisLen) return false;
                UlSgt.find(">li.active").removeClass("active");
                UlSgt.find(">li:eq(" + (++index) + ")").addClass("active");
                return false;
            } else if (e.keyCode == 38) {
                if (index == 0) return false;
                UlSgt.find(">li.active").removeClass("active");
                UlSgt.find(">li:eq(" + (--index) + ")").addClass("active");
                return false;
            } else if (e.keyCode == 13) {
                // Bind data to HDF Field
                var THIS = UlSgt.find(">li.active");
                var Value = THIS.text().trim();
                //var Value = THIS.attr("data-code");
                var dataField = This.attr("data-field");

                BindIdToHDF(THIS, Value, dataField, "#HDF_Suggestion_Code", "#HDF_Suggestion_Field", This);
                EBSelect_HideBox();
            }
        });
    }

    function Exc_To_Reset_Suggestion(This) {
        var value = This.val();
        if (value == "") {
            $(".eb-suggestion").each(function () {
                var THIS = $(this);
                var sgValue = THIS.val();
                if (sgValue != "") {
                    BindIdToHDF(THIS, sgValue, THIS.attr("data-field"), "#HDF_Suggestion_Code", "#HDF_Suggestion_Field", THIS);
                    return false;
                }
            });
        }
    }

    function Call_Suggestion(This) {
        var text = This.val(),
            field = This.attr("data-field");
        Suggestion(This, text, field);
    }

    function Suggestion(This, text, field) {
        var This = This;
        var text = text || "";
        var field = field || "";
        var InputDomId;
        var HDF_Sgt_Code = "#HDF_Suggestion_Code";
        var HDF_Sgt_Field = "#HDF_Suggestion_Field";

        if (text == "") return false;

        switch (field) {
            //case "customer.name": InputDomId = "#CustomerName"; break;
            case "customer.phone": InputDomId = "#EBPopup .SgCustomerPhone"; break;
            case "customer.code": InputDomId = "#EBPopup .SgCustomerCode"; break;
                //case "bill.code": InputDomId = "#BillCode"; break;
        }

        $.ajax({
            type: "POST",
            url: "/GUI/SystemService/Ajax/Suggestion.aspx/Suggestion_Customer",
            data: '{field : "' + field + '", text : "' + text + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                var mission = JSON.parse(response.d);
                if (mission.success) {
                    var OBJ = JSON.parse(mission.msg);
                    if (OBJ.length > 0) {
                        var lis = "";
                        $.each(OBJ, function (i, v) {
                            lis += "<li data-code='" + v.Customer_Code + "'" +
                                         "onclick=\"BindIdToHDF($(this),'" + v.Value + "','" + field + "','" + HDF_Sgt_Code +
                                         "','" + HDF_Sgt_Field + "','" + InputDomId + "')\">" +
                                        v.Value +
                                    "</li>";
                        });
                        This.parent().find("ul.ul-listing-suggestion").empty().append(lis).parent().show();
                        This.parent().find(".eb-select-data").show();
                    } else {
                        This.parent().find(".eb-select-data").hide().find("ul.ul-listing-suggestion").empty();
                    }
                } else {
                    This.parent().find("ul.ul-listing-suggestion").empty();
                    var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                    showMsgSystem(msg, "warning");
                }
            },
            failure: function (response) { alert(response.d); }
        });
    }

    function BindIdToHDF(THIS, Code, Field, HDF_Sgt_Code, HDF_Sgt_Field, Input_DomId, NoSgt) {
        var NoSgt = NoSgt || false;
        var text = THIS.text().trim();
        //$("input.eb-suggestion").val("");
        $(HDF_Sgt_Code).val(Code);
        $(HDF_Sgt_Field).val(Field);
        if (NoSgt) {
            $(Input_DomId).val(Code);
        } else {
            $(Input_DomId).val(text);
        }
        $(Input_DomId).parent().find(".eb-select-data").hide();
        // Auto post server
        //$("#BtnFakeUP").click();
    }

    function EBSelect_HideBox() {
        $(".eb-select-data").hide();
        $("ul.ul-listing-suggestion li.active").removeClass("active");
    }

    //=================================================
    // Xử lý ảnh
    //=================================================
    //draw image while upload
    var ListImgName = [],
        ListImgSrc = [],
        LIstImgDelete = [],
        ImgStandardWidth = 120,
        ImgStandardHeight = 120;

    function readImage(THIS) {
        var LoopName = 0;
        if (THIS.files.length > 0) {
            for (var i = 0; i < THIS.files.length; i++) {
                var FR = new FileReader();
                FR.onload = function (e) {
                    var img = new Image();
                    img.src = e.target.result;
                    img.onload = function () {
                        ListImgSrc.push(img.src);
                        var width = ImgStandardWidth;
                        var height = ImgStandardWidth * img.height / img.width;
                        var left = 0;
                        var top = (ImgStandardHeight - height) / 2;
                        var ThumbWp = "<div class='thumb-wp'>" +
                                        "<img class='thumb' style='width:" + width + "px;height:" +
                                        height + "px;left:" + left + "px;top:" + top + "px;' " + "src='" + img.src + "' />" +
                                      "<span class=\"delete-thumb\" onclick=\"deleteThum($(this), '" + img.src + "')\"></span></div>";
                        $(".listing-img-upload").append($(ThumbWp));

                    };
                };
                FR.readAsDataURL(THIS.files[i]);
            }
        }
    }

    function Bind_UserImagesToHDF1() {
        var jsonImages = JSON.stringify(ListImgSrc);
        var jsonImagesDel = JSON.stringify(LIstImgDelete);
        $("#HDF_UserImages").val(jsonImages);
        $("#HDF_UserImagesDelete").val(jsonImagesDel);
    }

    function deleteThum1(THIS, data, oldImg) {
        oldImg = oldImg || false;
        if (!oldImg) {
            var index = ListImgSrc.indexOf(data);
            if (index != -1) {
                ListImgSrc.splice(index, 1);
            }
        } else {
            LIstImgDelete.push(data);
        }
        THIS.parent().remove();
    }

    function excThumbWidth() {
        var ImgStandardWidth = 120,
            ImgStandardHeight = 120;
        var width = ImgStandardWidth,
            height, left, top;
        $(".thumb-wp .thumb").each(function () {
            height = ImgStandardWidth * $(this).height() / $(this).width();
            left = 0;
            top = (ImgStandardHeight - height) / 2;
            $(this).css({ "width": width, "height": height, "left": left, "top": top });
        });
    }

    function AjaxUploadImage_BillService(ImageName) {
        $.ajax({
            type: "POST",
            url: "/GUI/SystemService/Ajax/UploadImage.aspx/Add_User_Hair",
            data: '{ImageName : "' + ImageName + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                var mission = JSON.parse(response.d);
                if (mission.success) {
                    +
                        ListImgName.push(mission.msg);
                } else {
                    var msg = "Đã có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển";
                    showMsgSystem(msg, "warning");
                }
            },
            failure: function (response) { alert(response.d); }
        });
    }
</script>
<!--/ Suggestion -->    

<script src="/assets/js/jquery.mCustomScrollbar.js"></script>
<link href="/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
<script type="text/ecmascript">
    jQuery(document).ready(function () {
        // Add active menu
        $("#glbService").addClass("active");

        // Mở box listing sản phẩm
        $(".show-product").bind("click", function () {
            $(this).parent().parent().find(".listing-product").show();
        });

        $(".show-item").bind("click", function () {
            var item = $(this).attr("data-item");
            var classItem = ".popup-" + item + "-item";

            $(classItem).openEBPopup();
            ExcCheckboxItem();
            ExcQuantity();
            ExcCompletePopup();

            $('#EBPopup .listing-product').mCustomScrollbar({
                theme: "dark-2",
                scrollInertia: 100
            });

            $("#EBPopup .btn-esc").bind("click", function () { autoCloseEBPopup(0); });
        });
        
        // Btn Send
        $("#BtnSend").bind("click", function () {
            if ($("#CustomerName").val() == "" && $("#InputCusNoInfor").is(":checked") == false) {
                showMsgSystem("Bạn chưa nhập mã khách hàng hoặc chưa tích chọn \"Khách không cho thông tin\".", "warning");
            } else if ($("#HDF_TotalMoney").val() == "") {
                showMsgSystem("Bạn chưa chọn dịch vụ hoặc sản phẩm.", "warning");
            } else {
                // Check cosmetic seller
                if (($("#HDF_ProductIds").val() != "" && $("#HDF_ProductIds").val() != "[]") && $("#Cosmetic").val() == 0) {
                    $("#ValidateCosmetic").css({ "visibility": "visible" });
                } else {
                    $("#ValidateCosmetic").css({ "visibility": "hidden" });
                    var CustomerCode = $("#HDF_CustomerCode").val();
                    // Check duplicate bill
                    $.ajax({
                        type: "POST",
                        url: "/GUI/FrontEnd/Service/Service_Add.aspx/CheckDuplicateBill",
                        data: '{CustomerCode : "' + CustomerCode + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            var mission = JSON.parse(response.d);
                            if (mission.success) {
                                var products = "Sản phẩm : ";
                                var services = "Dịch vụ : ";
                                var confirm_content = "";
                                var bill = JSON.parse(mission.msg);
                                if (bill.ProductIds != "") {
                                    var _Product = $.parseJSON(bill.ProductIds);
                                    $.each(_Product, function (i, v) {
                                        products += v.Name + " ( " + v.Price + " ), ";
                                    });
                                    products = products.replace(/(\,\s)+$/, '');
                                }
                                if (bill.ServiceIds != "") {
                                    var _Service = $.parseJSON(bill.ServiceIds);
                                    $.each(_Service, function (i, v) {
                                        services += v.Name + " ( " + v.Price + " ), ";
                                    });
                                    services = services.replace(/(\,\s)+$/, '');
                                }
                                confirm_content += products + ", " + services + ", ";
                                confirm_content = confirm_content.replace(/(\,\s)+$/, '');

                                // show EBPopup
                                $(".confirm-yn").openEBPopup();
                                $("#EBPopup .confirm-yn-text").text("Khách hàng mã [" + CustomerCode + "] đã được lập hóa đơn trong hôm nay [ " + confirm_content + " ]. Bạn có chắc muốn thêm hóa đơn mới ?");

                                $("#EBPopup .yn-yes").bind("click", function () {
                                    Bind_UserImagesToHDF();
                                    $("#BtnFakeSend").click();
                                });
                                $("#EBPopup .yn-no").bind("click", function () {
                                    autoCloseEBPopup(0);
                                });
                            } else {
                                Bind_UserImagesToHDF();
                                $("#BtnFakeSend").click();
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    });
                    //Bind_UserImagesToHDF();
                    //$("#BtnFakeSend").click();
                }
            }
                        
        });
    });

    // Xử lý mã khách hàng => bind khách hàng
    function LoadCustomer(code) {
        ajaxGetCustomer(code);
    }

    // Xử lý khi chọn checkbox sản phẩm
    function ExcCheckboxItem() {
        $("#EBPopup .td-product-checkbox input[type='checkbox']").unbind("click").bind("click", function () {
            var obj = $(this).parent().parent(),
                boxMoney = obj.find(".box-money"),
                price = obj.find(".td-product-price").data("price"),
                quantity = obj.find("input.product-quantity").val(),
                voucher = obj.find(".td-product-voucher").data("voucher"),
                checked = $(this).is(":checked"),
                item = obj.attr("data-item");
            

            if (checked) {
                boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100)).show();
            } else {
                boxMoney.text("").hide();
            }
            
        });
    }

    // Số lượng | Quantity
    function ExcQuantity() {
        $("input.product-quantity").bind("change", function () {
            var obj = $(this).parent().parent(),
                quantity = $(this).val(),
                price = obj.find(".td-product-price").data("price"),
                voucher = obj.find(".td-product-voucher").data("voucher"),
                boxMoney = obj.find(".box-money");

            boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100)).show();
            TotalMoney();
            getProductIds();
            getServiceIds();
        });
    }

    // Xử lý click hoàn tất chọn item từ popup
    function ExcCompletePopup() {
        $("#EBPopup .btn-complete").unbind("click").bind("click", function () {
            var item = $(this).attr("data-item"),
                tableItem = $("table.table-item-" + item + " tbody"),
                objTmp;

            $("#EBPopup .item-product-checkbox input[type='checkbox']:checked").each(function () {
                var Code = $(this).attr("data-code");
                if (!ExcCheckItemIsChose(Code,tableItem)) {
                    objTmp = $(this).parent().parent().clone().find("td:first-child").remove().end();
                    tableItem.append(objTmp);
                }                
            });

            TotalMoney();
            getProductIds();
            getServiceIds();
            ExcQuantity();
            UpdateItemOrder(tableItem);
            autoCloseEBPopup(0);
        });
    }

    // Check item đã được chọn
    function ExcCheckItemIsChose(Code, itemClass) {
        var result = false;
        $(itemClass).find(".td-product-code").each(function () {
            var _Code = $(this).text().trim();
            if (_Code == Code)
                result = true;            
        });
        return result;
    }

    // Remove item đã được chọn
    function RemoveItem(THIS, name, itemName) {
        // Confirm
        $(".confirm-yn").openEBPopup();

        $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn hủy [ " + name + " ] ?");
        $("#EBPopup .yn-yes").bind("click", function () {
            THIS.remove();
            TotalMoney();
            getProductIds();
            getServiceIds();
            UpdateItemDisplay(itemName);
            autoCloseEBPopup(0);
        });
        $("#EBPopup .yn-no").bind("click", function () {
            autoCloseEBPopup(0);
        });        
    }

    // Update table display
    function UpdateItemDisplay(itemName) {
        var dom = $(".table-item-" + itemName);
        var len = dom.find("tbody tr").length;
        if (len == 0) {
            dom.parent().hide();
        } else {
            UpdateItemOrder(dom.find("tbody"));
        }        
    }

    // Update order item
    function UpdateItemOrder(dom) {
        var index = 1;
        dom.find("tr").each(function () {
            $(this).find("td.td-product-index").text(index);
            index++;
        });
    }

    function TotalMoney() {
        var Money = 0;
        $(".fe-service-table-add .box-money:visible").each(function () {
            Money += parseInt($(this).text().replace(/\./gm, ""));
        });
        $("#TotalMoney").val(FormatPrice(Money));
        $("#HDF_TotalMoney").val(Money);
    }

    function showMsgSystem(msg, status) {
        $("#MsgSystem").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);
        setTimeout(function () {
            $("#MsgSystem").fadeTo("slow", 0, function () {
                $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 1);
            });
        }, 5000);
    }

    function getProductIds() {
        var Ids = [];
        var prd = {};
        $("table.table-item-product tbody tr").each(function () {
            prd = {};
            var THIS = $(this);

            prd.Id = THIS.find("td.td-product-code").attr("data-id");
            prd.Code = THIS.find("td.td-product-code").text().trim();
            prd.Name = THIS.find("td.td-product-name").text().trim();
            prd.Price = THIS.find(".td-product-price").attr("data-price");
            prd.Quantity = THIS.find("input.product-quantity").val();
            prd.VoucherPercent = THIS.find(".td-product-voucher").attr("data-voucher");

            Ids.push(prd);

        });
        Ids = JSON.stringify(Ids);
        $("#HDF_ProductIds").val(Ids);
    }

    function getServiceIds() {
        var Ids = [];
        var prd = {};
        $("table.table-item-service tbody tr").each(function () {
            prd = {};
            var THIS = $(this),
                obj = THIS.parent().parent();

            prd.Id = THIS.find("td.td-product-code").attr("data-id");
            prd.Code = THIS.find("td.td-product-code").text().trim();
            prd.Name = THIS.find("td.td-product-name").text().trim();
            prd.Price = THIS.find(".td-product-price").attr("data-price");
            prd.Quantity = THIS.find("input.product-quantity").val();
            prd.VoucherPercent = THIS.find(".td-product-voucher").attr("data-voucher");

            Ids.push(prd);

        });
        Ids = JSON.stringify(Ids);
        $("#HDF_ServiceIds").val(Ids);
    }
    
    // Get Customer
    function ajaxGetCustomer(CustomerCode) {
        $.ajax({
            type: "POST",
            url: "/GUI/FrontEnd/Service/Service_Add.aspx/GetCustomer",
            data: '{CustomerCode : "' + CustomerCode + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                var mission = JSON.parse(response.d);
                if (mission.success) {
                    var customer = JSON.parse(mission.msg);
                    customer = customer[0];
                    $("#CustomerName").val(customer.Fullname);
                    $("#CustomerName").attr("data-code", customer.Customer_Code);
                    $("#HDF_CustomerCode").val(customer.Customer_Code);

                } else {
                    var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                    showMsgSystem(msg, "warning");
                }
            },
            failure: function (response) { alert(response.d); }
        });
    }

</script>

</asp:Panel>
</asp:Content>

