﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.IO;
using System.Drawing;
using System.Globalization;
using System.Drawing.Printing;
using LinqKit;
using System.Data.Entity.Migrations;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.UIService
{
    public partial class Service_Add : System.Web.UI.Page
    {
        private string PageID = "";
        private bool Perm_ShowSalon = false;
        private bool Perm_Access = false;

        Font Font = new Font("Arial", 13, GraphicsUnit.Pixel);
        private int x = 1;
        private int y = 2;

        private List<BillService> CustomerHistoryService = new List<BillService>();
        private List<BillService> CustomerHistoryProduct = new List<BillService>();
        private List<ProductBasic> ProductList = new List<ProductBasic>();
        private List<ProductBasic> ServiceList = new List<ProductBasic>();
        protected BillService billBooking = new BillService();
        private string _CustomerCode = "";
        private string _CustomerName = "";
        private string Stylist = "";
        private string Skinner = "";
        private string Seller = "";
        private string BillCode = "";
        private string PDFname = "";
        private int FP_CustomerId = 0;
        private string FP_token = "";
        private string FP_template = "";
        private int SalonId;
        private int times = 0;
        /// <summary>
        /// check permission
        /// </summary>
        protected void checkPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //string perName = Session["User_Permission"].ToString();
                //string url = Request.Url.AbsolutePath.ToString();
                //Solution_30shineEntities db = new Solution_30shineEntities();
                //var pem = (from m in db.Tbl_Permission_Map
                //           join mn in db.Tbl_Permission_Menu on m.mID equals mn.mID
                //           where m.pName == perName && mn.Url_Main.EndsWith(url) || mn.Url_Rewrite.EndsWith(url)
                //           select new { m.aID }
                //           ).FirstOrDefault();
                //if (pem != null)
                //{
                //    var pemArray = pem.aID.Split(',');
                //    if (Array.IndexOf(pemArray, "1") > -1)
                //    {
                //        Perm_Access = true;
                //        if (Array.IndexOf(pemArray, "6") > -1)
                //        {
                //            Perm_ShowSalon = true;
                //        }

                //    }
                //    else
                //    {
                //        ContentWrap.Visible = false;
                //        NotAllowAccess.Visible = true;
                //    }

                //}

                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        //private void ExecuteByPermission()
        //{
        //    if (!Perm_Access)
        //    {
        //        ContentWrap.Visible = false;
        //        NotAllowAccess.Visible = true;
        //    }
        //}
        protected void Page_Load(object sender, EventArgs e)
        {
            checkPermission();

            CheckIsBooking();
            if (!IsPostBack)
            {
                if (Session["SalonId"] != null)
                {
                    HDF_SalonId.Value = Session["SalonId"].ToString();
                }
                else
                {
                    HDF_SalonId.Value = "0";
                }                
                Bind_TeamWork();
            }
        }

        private void CheckIsBooking()
        {
            /// Check booking
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                int billId = int.TryParse(Request.QueryString["b"], out integer) ? integer : 0;
                if (billId > 0)
                {
                    billBooking = db.BillServices.FirstOrDefault(w => w.Id == billId);
                }
            }
        }

        protected void AddService(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                //// Insert to BillService
                var obj = new BillService();
                int integer;

                BillCode = GenBillCode("KT", 4);
                PDFname = BillCode + "_" + UIHelpers.GetUniqueKey(6,6);
                obj.BillCode = BillCode;

                if (TrSalon.Visible == true)
                {
                    obj.SalonId = Convert.ToInt32(Salon.SelectedValue);
                }
                else
                {
                    obj.SalonId = Convert.ToInt32(Session["SalonId"]);
                }

                obj.CustomerNoInfo = InputCusNoInfor.Checked;
                if (obj.CustomerNoInfo == true)
                {
                    /// Insert khách hàng anonymous
                    var cCode = UIHelpers.GetUniqueKey(8,8);
                    var newCus = new Customer();
                    newCus.Fullname = cCode;
                    newCus.Customer_Code = cCode;
                    newCus.SalonId = obj.SalonId;
                    newCus.IsDelete = 0;
                    newCus.CreatedDate = DateTime.Now;                
                    db.Customers.AddOrUpdate(newCus);
                    db.SaveChanges();

                    /// Add CustomerCode cho bill
                    obj.CustomerCode = cCode;
                    _CustomerCode = newCus.Customer_Code;
                    _CustomerName = newCus.Fullname;
                }
                else
                {
                    _CustomerCode = HDF_CustomerCode.Value;
                    var customer = db.Customers.FirstOrDefault(w => w.Customer_Code == _CustomerCode && w.IsDelete != 1);
                    if (customer != null)
                    {
                        _CustomerName = customer.Fullname;
                    }
                    obj.CustomerCode = _CustomerCode;   
                }                
                obj.ProductIds = "";
                obj.ServiceIds = "";
                obj.TotalMoney = 0;
                obj.IsDelete = 0;
                if (billBooking != null && billBooking.CreatedDate != null)
                {
                    obj.CreatedDate = billBooking.CreatedDate.Value.Add(TimeSpan.Parse("00:40:00"));
                    obj.IsBooking = true;
                    billBooking.IsBooked = true;
                    db.BillServices.AddOrUpdate(billBooking);
                    db.SaveChanges();
                }
                else
                {
                    obj.CreatedDate = DateTime.Now;
                }                
                obj.Pending = 1;
                obj.PDFBillCode = PDFname;
                obj.TeamId = int.TryParse(HDF_TeamId.Value, out integer) ? integer : 0;

                db.BillServices.Add(obj);
                var exc = db.SaveChanges();
                var Error = exc > 0 ? 0 : 1;

                if (Error == 0)
                {
                    // In phiếu
                    CustomerHistoryService = db.BillServices.Where(w => w.CustomerCode == _CustomerCode && w.Pending != 1 && w.ServiceIds != "" && w.IsDelete != 1)
                                                            .OrderByDescending(o => o.CreatedDate).Take(1).ToList();
                    CustomerHistoryProduct = db.BillServices.Where(w => w.CustomerCode == _CustomerCode && w.Pending != 1 && w.ProductIds != "" && w.IsDelete != 1)
                                                            .OrderByDescending(o => o.CreatedDate).Take(1).ToList();
                    if (CustomerHistoryService.Count > 0)
                    {
                        for (var i = 0; i < CustomerHistoryService.Count; i++ )
                        {
                            int id = Convert.ToInt32(CustomerHistoryService[i].Staff_Hairdresser_Id);
                            var stylistName = db.Staffs.FirstOrDefault(w=>w.Id == id);
                            if (stylistName != null)
                            {
                                CustomerHistoryService[i].HairdresserName = stylistName.Fullname;
                            }
                        }
                    }
                    // Generate pdf content to print                    
                    GenPDF();
                }
                else
                {
                    MsgSystem.Text = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                    MsgSystem.CssClass = "msg-system warning";
                }
                
            }
        }

        public void Bind_Salon()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Tbl_Salon.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Order).ToList();
                var Key = 0;
                var Count = LST.Count;
                Salon.DataTextField = "Salon";
                Salon.DataValueField = "Id";
                ListItem item = new ListItem("Chọn salon", "0");
                Salon.Items.Insert(Key, item);
                Key++;

                if (Count > 0)
                {
                    foreach (var v in LST)
                    {
                        item = new ListItem(v.Name, v.Id.ToString());
                        Salon.Items.Insert(Key, item);
                        Key++;
                    }
                }
            }
        }

        private void Bind_RptProduct()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Products = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1).OrderByDescending(o => o.Id).ToList();

                Rpt_Product.DataSource = _Products;
                Rpt_Product.DataBind();
            }
        }

        private void Bind_RptService()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Service = db.Services.Where(w=>w.IsDelete != 1 && w.Publish == 1 && (w.IsFreeService != 1 || w.IsFreeService == null)).OrderByDescending(o => o.Id).ToList();

                Rpt_Service.DataSource = _Service;
                Rpt_Service.DataBind();
            }
        }

        private void Bind_TeamWork()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.TeamServices.Where(w=>w.IsDelete != 1 && w.Publish == true && w.Status == 1).ToList();
                if (billBooking != null && billBooking.TeamId > 0 && lst.Count > 0)
                {
                    for (var i = 0; i < lst.Count; i++)
                    {
                        if (lst[i].Id != billBooking.TeamId)
                        {
                            lst.RemoveAt(i--);
                        }
                    }
                    HDF_TeamId.Value = billBooking.TeamId.ToString();
                    TeamColorSelect.Visible = false;
                }
                Rpt_TeamWork.DataSource = lst;
                Rpt_TeamWork.DataBind();
            }
        }

        private void GenPDF()
        {
            string htmlContent = GenHtmlContent();
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            htmlToPdf.PageWidth = 65;
            htmlToPdf.PageHeight = 1000;
            htmlToPdf.Margins.Left = 0;
            htmlToPdf.Margins.Right = 0;
            htmlToPdf.Margins.Top = 0;
            htmlToPdf.Margins.Bottom = 0;
            var pdfBytes = htmlToPdf.GeneratePdf(htmlContent);

            string filename = "/Public/PDF/" + PDFname + ".pdf";
            string filepath = HttpContext.Current.Server.MapPath("~" + filename);
            File.WriteAllBytes(filepath, pdfBytes);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Show print", "openPdfIframe('" + filename + "');", true);
            // Thông báo thành công
            var MsgParam = new List<KeyValuePair<string, string>>();
            MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
            MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Thêm bill thành công!"));
            MsgParam.Add(new KeyValuePair<string, string>("msg_print_billcode", PDFname));
            UIHelpers.Redirect("/dich-vu/them-phieu.html", MsgParam);

            //Response.ContentType = "application/pdf";
            //Response.ContentEncoding = System.Text.Encoding.UTF8;
            //Response.AddHeader("Content-Disposition", "Inline; filename=TEST.pdf");
            //Response.BinaryWrite(pdfBytes);
            //Response.Flush();
            //Response.End();
        }

        private string GenHtmlContent()
        {
            // gen service
            string service = "<table style='border-collapse: collapse;width: 100%;margin-top: 5px;'>" +
                                "<thead>" +
                                    "<tr>" +
                                        "<th style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 80%; padding: 5px; font-weight:normal;'>Dịch vụ</th>" +
                                        "<th style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 20%; padding: 5px; font-weight: normal;'>SL</th>" +
                                    "</tr>" +
                                "</thead>" +
                                "<tbody>";
            if (ServiceList != null && ServiceList.Count > 0)
            {
                var totalMoney = 0;
                foreach (var v in ServiceList)
                {
                    totalMoney += v.Price * v.Quantity;
                    service += "<tr>" +
                                    "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 80%; padding: 5px;'>" + v.Name + "</td>" +
                                    "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 20%; padding: 5px; text-align: right;'>" + v.Quantity.ToString() + "</td>" +
                                "</tr>";
                }
                // Render stylist, skinner
                //CultureInfo elGR = CultureInfo.CreateSpecificCulture("el-GR");
                //items.Add(new KeyValuePair<string, string>("Thành tiền", String.Format(elGR, "{0:0,0}", totalMoney) + " VNĐ"));
            }
            else
            {
                for (var i = 0; i < 2; i++)
                {
                    service += "<tr>" +
                                    "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 80%; height: 28px;'></td>" +
                                    "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 20%; height: 28px;'></td>" +
                                "</tr>";
                }
            }
            service += "</tbody></table>";

            // gen product
            string product = "<table style='border-collapse: collapse;width: 100%;margin-top: 5px;'>" +
                                "<thead>" +
                                    "<tr>" +
                                        "<th style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 80%; padding: 5px; font-weight:normal;'>Sản phẩm</th>" +
                                        "<th style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 20%; padding: 5px; font-weight: normal;'>SL</th>" +
                                    "</tr>" +
                                "</thead>" +
                                "<tbody>";
            if (ProductList != null && ProductList.Count > 0)
            {
                var totalMoney = 0;
                foreach (var v in ProductList)
                {
                    totalMoney += v.Price * v.Quantity;
                    product += "<tr>" +
                                    "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 80%; padding: 5px;'>" + v.Name + "</td>" +
                                    "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 20%; padding: 5px; text-align: right;'>" + v.Quantity.ToString() + "</td>" +
                                "</tr>";
                }
                // Render stylist, skinner
                //CultureInfo elGR = CultureInfo.CreateSpecificCulture("el-GR");
                //items.Add(new KeyValuePair<string, string>("Thành tiền", String.Format(elGR, "{0:0,0}", totalMoney) + " VNĐ"));
            }
            else
            {
                for (var i = 0; i < 3; i++)
                {
                    product += "<tr>" +
                                    "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 80%; height: 28px;'></td>" +
                                    "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 20%; height: 28px;'></td>" +
                                "</tr>";
                }
            }
            product += "</tbody></table>";

            // gen history latest
            string history = "";
            using (var db = new Solution_30shineEntities())
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var loop = 0;
                if (CustomerHistoryService.Count > 0 || CustomerHistoryProduct.Count > 0)
                {
                    history += "<table style='border-collapse: collapse;width: 100%;margin-top: 5px;'>" +
                                "<thead>" +
                                    "<tr>" +
                                        "<th style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 80%; padding: 5px; font-weight: normal;'>Lịch sử gần nhất</th>" +
                                        "<th style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 20%; padding: 5px; font-weight: normal;'>SL</th>" +
                                    "</tr>" +
                                "</thead>" +
                                "<tbody>";
                    if (CustomerHistoryService.Count > 0)
                    {
                        history += "<tr>" +
                                        "<td style='font-family: Arial, sans-serif;font-size: 14px; border: 1px solid #000;width: 80%; height: 28px; font-style: italic; border-bottom: none;'>Ngày " + String.Format("{0:dd/MM/yyyy}", CustomerHistoryService[0].CreatedDate) + "</td>" +
                                        "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 20%; height: 28px;'></td>" +
                                    "</tr>";
                        if (CustomerHistoryService[0].ServiceIds != null)
                        {
                            var ServiceListThis = serializer.Deserialize<List<ProductBasic>>(CustomerHistoryService[0].ServiceIds);
                            if (ServiceListThis != null)
                            {
                                foreach (var v2 in ServiceListThis)
                                {
                                    if (loop == 0)
                                    {
                                        history += "<tr>" +
                                            "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000; border-top:none; width: 80%; height: 28px;'>" +
                                                "DV : " + v2.Name +
                                            "</td>" +
                                            "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 20%; height: 28px;'>" + v2.Quantity.ToString() + "</td>" +
                                        "</tr>";
                                    }
                                    else
                                    {
                                        history += "<tr>" +
                                            "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000; border-top:none; width: 80%; height: 28px;'>" +
                                                "&nbsp;&nbsp;&nbsp;&nbsp;" + v2.Name +
                                            "</td>" +
                                            "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 20%; height: 28px;'>" + v2.Quantity.ToString() + "</td>" +
                                        "</tr>";
                                    }
                                    loop++;
                                }
                            }
                        }
                        loop = 0;
                    }
                    if (CustomerHistoryProduct.Count > 0)
                    {
                        history += "<tr>" +
                                        "<td style='font-family: Arial, sans-serif;font-size: 14px; border: 1px solid #000;width: 80%; height: 28px; font-style: italic; border-bottom: none;'>Ngày " + String.Format("{0:dd/MM/yyyy}", CustomerHistoryProduct[0].CreatedDate) + "</td>" +
                                        "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 20%; height: 28px;'></td>" +
                                    "</tr>";
                        if (CustomerHistoryProduct[0].ProductIds != null)
                        {
                            var ProductListThis = serializer.Deserialize<List<ProductBasic>>(CustomerHistoryProduct[0].ProductIds);
                            if (ProductListThis != null)
                            {
                                foreach (var v2 in ProductListThis)
                                {
                                    if (loop == 0)
                                    {
                                        history += "<tr>" +
                                            "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000; border-top:none; width: 80%; height: 28px;'>" +
                                                "SP : " + v2.Name +
                                            "</td>" +
                                            "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 20%; height: 28px;'>" + v2.Quantity.ToString() + "</td>" +
                                        "</tr>";
                                    }
                                    else
                                    {
                                        history += "<tr>" +
                                            "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000; border-top:none; width: 80%; height: 28px;'>" +
                                               "&nbsp;&nbsp;&nbsp;&nbsp;" + v2.Name +
                                            "</td>" +
                                            "<td style='font-family: Arial, sans-serif;font-size: 15px; border: 1px solid #000;width: 20%; height: 28px;'>" + v2.Quantity.ToString() + "</td>" +
                                        "</tr>";
                                    }
                                    loop++;
                                }
                            }
                        }
                        loop = 0;
                    }
                    history += "</tbody></table>";

                    if (CustomerHistoryService.Count > 0)
                    {
                        history += "<table style='width: 100%;'>" +
                                        "<tbody>" +
                                            "<tr>" +
                                                "<td style='font-family: Arial, sans-serif;font-size: 15px; padding: 5px 5px 0px 0px;'>Stylist </td>" +
                                                "<td style='font-family: Arial, sans-serif;font-size: 15px; padding: 5px 5px 0px 0px; text-align: right;'>" +
                                                    "<b style='font-family: Arial, sans-serif;font-size: 15px; font-weight: normal;'>" +
                                                        CustomerHistoryService[0].HairdresserName +
                                                    "</b>" +
                                                "</td>" +
                                            "</tr>" +
                                        "</tbody>" +
                                    "</table>";
                    }

                }

                // Số lần sử dụng dịch vụ
                times = db.BillServices.Count(w => (w.CustomerCode == _CustomerCode && w.CustomerCode != "") && (w.ServiceIds != null && w.ServiceIds != "") && w.IsDelete != 1);
            }

            // gen .print-wp style by printer
            string style = "";
            switch(SalonId)
            {
                case 1: style += "width: 225px; padding-left: 15px;";
                    break;
                case 2: style += "width: 225px; padding-left: 15px;";
                    break;
                case 3: style += "width: 225px; padding-left: 15px;";
                    break;
                case 4: style += "width: 225px; padding-left: 15px;";
                    break;
                default: style += "width: 215px;";
                    break;
            }

            // gen htmlContent
            string htmlContent = "<html><head><meta charset='utf-8' /></head><body>";
            htmlContent += "<div class='print-wp' style='" + style + "'>";
            htmlContent += "<div class='logo' style='width: 100%; text-align: center; padding: 0; margin-bottom: 5px;'>" +
                                "<img class='prt-logo' src='http://ql.30shine.com/Assets/images/logo.jpg' style='width: 100px;' />" +
                            "</div>" +
                            "<div style='width: 100%; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 2px;'>Ngày  " + String.Format("{0:dd/MM/yyyy}", DateTime.Now) + " - " + String.Format("{0:HH}", DateTime.Now) + "h" + String.Format("{0:mm}", DateTime.Now) + "</div>" +
                /// bill code
                            "<div style='width: 100%; font-family: Arial, sans-serif;font-size: 15px;'>Code  <b style='font-size: 16px;'>" + UIHelpers.getBillOrder(BillCode, 4) + "</b></div>" +
                /// customer
                            "<table style='width: 100%;margin-top: 3px;'>" +
                                "<tbody>" +
                                    "<tr>" +
                                        "<td style='font-family: Arial, sans-serif; font-size: 15px; min-width: 30px;'>KH</td>" +
                                        "<td style='font-family: Arial, sans-serif; font-size: 15px;'>" +
                                            " <b style='font-family: Arial, sans-serif; font-size: 15px; text-transform: uppercase; '>" + _CustomerName.ToUpper() + "</b>" +
                                        "</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td style='font-family: Arial, sans-serif;font-size: 15px; min-width: 30px;'>Mã KH</td>" +
                                        "<td style='font-family: Arial, sans-serif;font-size: 15px;'> " + _CustomerCode + "</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td style='font-family: Arial, sans-serif;font-size: 16px; min-width: 30px;'>Số DV</td>" +
                                        "<td style='font-family: Arial, sans-serif;font-size: 16px;'> " + times + "</td>" +
                                    "</tr>" +
                                "</tbody>" +
                            "</table>" +
                /// service
                            service +
                /// stylist
                            "<table style='width: 100%;'>" +
                                "<tbody>" +
                                    "<tr>" +
                                        "<td style='font-family: Arial, sans-serif;font-size: 15px; padding: 5px 5px 0px 0px;'>Stylist&nbsp;&nbsp;&nbsp;</td>" +
                                        "<td style='font-family: Arial, sans-serif;font-size: 15px; padding: 5px 5px 0px 0px; text-align: right;'>" +
                                            "<b style='font-family: Arial, sans-serif;font-size: 15px; font-weight: normal;'>" + Stylist + "</b>" +
                                        "</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td style='font-family: Arial, sans-serif;font-size: 15px; padding: 2px 5px 0px 0px;'>Skinner&nbsp;</td>" +
                                        "<td style='font-family: Arial, sans-serif;font-size: 15px; padding: 2px 5px 0px 0px; text-align: right;'>" + Skinner + "</td>" +
                                    "</tr>" +
                                "</tbody>" +
                            "</table>" +
                            // mini item
                            //"<div style='width: 50%; float: left; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 2px;'>" +
                            //    "<div style='width: 12px; height: 12px; border: 1px solid #000; margin-top: 2px; margin-right: 7px; float:left;'></div> Cạo Mặt" +
                            //"</div>" +
                            /// Phụ trợ : Trị gầu, Dưỡng tóc, Ngăn rụng tóc
                            //"<div style='width: 50%; float: left; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 2px;'>" +
                            //    "<div style='width: 12px; height: 12px; border: 1px solid #000; margin-top: 2px; margin-right: 7px; float:left;'></div> Trị gầu" +
                            //"</div>" +
                            //"<div style='width: 50%; float: left; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 2px;'>" +
                            //    "<div style='width: 12px; height: 12px; border: 1px solid #000; margin-top: 2px; margin-right: 7px; float:left;'></div> Dưỡng tóc" +
                            //"</div>" +
                            //"<div style='width: 50%; float: left; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 2px;'>" +
                            //    "<div style='width: 12px; height: 12px; border: 1px solid #000; margin-top: 2px; margin-right: 7px; float:left;'></div> Ngăn rụng tóc" +
                            //"</div>" +
                            //"<div style='width: 100%; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 2px;'>" +
                            //    "<div style='width: 12px; height: 12px; border: 1px solid #000; margin-top: 2px; margin-right: 7px; float:left;'></div> Lấy Ráy Tai" +
                            //"</div>" +
                            //"<div style='width: 100%; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 2px;'>" +
                            //    "<div style='width: 12px; height: 12px; border: 1px solid #000; margin-top: 2px; margin-right: 7px; float: left;'></div> Cắt Móng Tay" +
                            //"</div>" +
                            /// Sao đánh giá
//                            @"<div style='width: 100%; float: left; padding: 5px 0 10px 0;'>
//                                <div style='width: 15%; height: 20px; float: left; background: url(http://ql.30shine.com/Assets/images/star_20.png) center no-repeat; position: relative; top: -2px; text-align: right;'>:</div>
//                                <div style='width: 15%; height: 20px; float: left; text-align: right;'>1</div>
//                                <div style='width: 15%; height: 20px; float: left; text-align: right;'>2</div>
//                                <div style='width: 15%; height: 20px; float: left; text-align: right;'>3</div>
//                                <div style='width: 15%; height: 20px; float: left; text-align: right;'>4</div>
//                                <div style='width: 15%; height: 20px; float: left; text-align: right;'>5</div>
//                            </div>" +
                            @"<div style='width: 100%; float: left; margin-top: 5px; margin-bottom: 5px;'>
                                <div style='width: 37%; float: left; font-family: Arial, sans-serif; padding-left: 3%;'>
                                    Đánh giá :
                                </div>
                                <div style='width: 60%; float: left;'>
                                    <div style='width: 100%; float: left; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 2px;'>
                                        <div style='width: 12px; height: 12px; border: 1px solid #000; margin-top: 2px; margin-right: 7px; float:left;'></div> Tuyệt vời
                                    </div>
                                    <div style='width: 100%; float: left; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 2px;'>
                                        <div style='width: 12px; height: 12px; border: 1px solid #000; margin-top: 2px; margin-right: 7px; float:left;'></div> Hài lòng
                                    </div>
                                    <div style='width: 100%; float: left; font-family: Arial, sans-serif;font-size: 15px; font-style: italic; margin-bottom: 2px;'>
                                        <div style='width: 12px; height: 12px; border: 1px solid #000; margin-top: 2px; margin-right: 7px; float:left;'></div> Chưa hài lòng
                                    </div>
                                </div>
                            </div>" +
                /// product
                            product +
                /// seller, total money
                            "<table style='font-family: Arial, sans-serif;font-size: 15px; width: 100%; margin-top: 5px;'>" +
                                "<tbody>" +
                                    "<tr>" +
                                        "<td style='font-family: Arial, sans-serif;font-size: 15px;'>Người bán </td>" +
                                        "<td>"+ Seller +"</td>" +
                                    "</tr>" +
                                    "<tr>" +
                                        "<td style='font-family: Arial, sans-serif;font-size: 15px;'>Tổng số </td>" +
                                        "<td></td>" +
                                    "</tr>" +
                                "</tbody>" +
                            "</table>" +
                /// history latest
                            history;

            htmlContent += "</body></html>";
            return htmlContent;
        }
        
        /// <summary>
        /// Print
        /// </summary>
        private void Printing()
        {
            PrintDocument pd = new PrintDocument();

            pd.PrinterSettings.PrinterName = "RP58 Printer";
            pd.OriginAtMargins = true;
            PaperSize pageSize = new PaperSize();
            //pageSize.RawKind = 512; //this is number of created custom size 563x1251
            //pageSize.Height = 50;
            Margins margins = new Margins(3, 0, 0, 0);
            pd.DefaultPageSettings.Margins = margins;
            pd.DefaultPageSettings.Landscape = false;
            pd.PrintPage += new PrintPageEventHandler(pd_PrintPage);
            pd.Print();
        }

        private void pd_PrintPage(object sender, PrintPageEventArgs e)
        {
            // Chiều ngang khổ in : 189px
            List<KeyValuePair<string, string>> items = new List<KeyValuePair<string, string>>();            

            // Print logo
            System.Drawing.Image img = System.Drawing.Image.FromFile(@"C:\logo.jpg");
            var imgRec = new Rectangle(new Point((e.MarginBounds.Width - 100) / 2, y), new Size(100, 60));
            e.Graphics.DrawImage(img, imgRec);
            x = 1;
            y += 65;

            // Render top
            items = new List<KeyValuePair<string, string>>();
            items.Add(new KeyValuePair<string, string>("Ngày  : " + String.Format("{0:dd/MM/yyyy}", DateTime.Now), ""));
            items.Add(new KeyValuePair<string, string>("Code  : " + BillCode, ""));
            Render_Table(e, Brushes.Black, Pens.White, items, 184, 0, 18);

            // Render Khách hàng
            items = new List<KeyValuePair<string, string>>();
            items.Add(new KeyValuePair<string, string>("KH", _CustomerName.ToUpper()));
            items.Add(new KeyValuePair<string, string>("Mã KH", _CustomerCode));
            Render_Table(e, Brushes.Black, Pens.White, items, 70, 114, 22);

            // Render service table
            if (ServiceList != null && ServiceList.Count > 0)
            {
                var totalMoney = 0;
                items = new List<KeyValuePair<string, string>>();
                items.Add(new KeyValuePair<string, string>("Dịch vụ", "SL"));
                foreach (var v in ServiceList)
                {
                    totalMoney += v.Price * v.Quantity;
                    items.Add(new KeyValuePair<string, string>(v.Name, v.Quantity.ToString()));
                }
                //items.Add(new KeyValuePair<string, string>("Gội đầu MassageGội đầu Massage Gội đầu Massage", "1"));
                Render_Table(e, Brushes.Black, Pens.Black, items, 154, 30, 24);

                // Render stylist, skinner
                CultureInfo elGR = CultureInfo.CreateSpecificCulture("el-GR");
                items = new List<KeyValuePair<string, string>>();
                items.Add(new KeyValuePair<string, string>("Stylist", Stylist));
                items.Add(new KeyValuePair<string, string>("Skinner", ""));
                //items.Add(new KeyValuePair<string, string>("Thành tiền", String.Format(elGR, "{0:0,0}", totalMoney) + " VNĐ"));
                Render_Table(e, Brushes.Black, Pens.White, items, 50, 134, 22);
            }
            else
            {
                items = new List<KeyValuePair<string, string>>();
                items.Add(new KeyValuePair<string, string>("Dịch vụ", "SL"));
                items.Add(new KeyValuePair<string, string>(" ", ""));
                items.Add(new KeyValuePair<string, string>(" ", ""));
                Render_Table(e, Brushes.Black, Pens.Black, items, 154, 30, 24);
            }

            // Render product table            
            items = new List<KeyValuePair<string, string>>();
            items.Add(new KeyValuePair<string, string>("Sản phẩm", "SL"));
            items.Add(new KeyValuePair<string, string>(" ", ""));
            items.Add(new KeyValuePair<string, string>(" ", ""));
            items.Add(new KeyValuePair<string, string>(" ", ""));
            Render_Table(e, Brushes.Black, Pens.Black, items, 154, 30, 24);

            // Render seller, total money
            items = new List<KeyValuePair<string, string>>();
            items.Add(new KeyValuePair<string, string>("Seller", ""));                        
            items.Add(new KeyValuePair<string, string>("Tổng số", ""));
            Render_Table(e, Brushes.Black, Pens.White, items, 70, 114, 22);

            // Render customer history
            using (var db = new Solution_30shineEntities())
            {
                items = new List<KeyValuePair<string, string>>();
                items.Add(new KeyValuePair<string, string>("Lịch sử gần nhất", "SL"));
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var loop = 0;
                if (CustomerHistoryService.Count > 0)
                {
                    items.Add(new KeyValuePair<string, string>("Ngày " + String.Format("{0:dd/MM/yyyy}", CustomerHistoryService[0].CreatedDate), ""));                    
                    if (CustomerHistoryService[0].ServiceIds != null)
                    {
                        var ServiceListThis = serializer.Deserialize<List<ProductBasic>>(CustomerHistoryService[0].ServiceIds);
                        if (ServiceListThis != null)
                        {
                            foreach (var v2 in ServiceListThis)
                            {
                                if (loop == 0)
                                {
                                    items.Add(new KeyValuePair<string, string>("DV : " + v2.Name, v2.Quantity.ToString()));
                                }
                                else
                                {
                                    items.Add(new KeyValuePair<string, string>("DV : " + v2.Name, v2.Quantity.ToString()));
                                }
                                loop++;
                            }
                        }
                    }
                    loop = 0;
                }
                if(CustomerHistoryProduct.Count > 0)
                {
                    items.Add(new KeyValuePair<string, string>("Ngày " + String.Format("{0:dd/MM/yyyy}", CustomerHistoryProduct[0].CreatedDate), "")); 
                    if (CustomerHistoryProduct[0].ProductIds != null)
                    {
                        var ProductListThis = serializer.Deserialize<List<ProductBasic>>(CustomerHistoryProduct[0].ProductIds);
                        if (ProductListThis != null)
                        {
                            foreach (var v2 in ProductListThis)
                            {
                                if (loop == 0)
                                {
                                    items.Add(new KeyValuePair<string, string>("SP : " + v2.Name, v2.Quantity.ToString()));
                                }
                                else
                                {
                                    items.Add(new KeyValuePair<string, string>("SP : " + v2.Name, v2.Quantity.ToString()));
                                }
                                loop++;
                            }
                        }
                    }
                    loop = 0;
                    Render_Table(e, Brushes.Black, Pens.Black, items, 154, 30, 24);

                    // stylist, skinner
                    items = new List<KeyValuePair<string, string>>();
                    items.Add(new KeyValuePair<string, string>("Stylist", CustomerHistoryService[0].HairdresserName));
                    Render_Table(e, Brushes.Black, Pens.White, items, 50, 134, 20);
                }

            }
        }

        /// <summary>
        /// Drawing table
        /// </summary>
        /// <param name="e">PrintPageEventArgs e</param>
        /// <param name="items">List<KeyValuePair<string, string>> items</param>
        /// <param name="keyWidth">td width for key</param>
        /// <param name="valueWidth">td width for value</param>
        /// <param name="height">td height</param>
        public void Render_Table(PrintPageEventArgs e, Brush brush, Pen pen, List<KeyValuePair<string, string>> items, int keyWidth, int valueWidth, int height)
        {
            if (items.Count > 0)
            {
                // Construct 2 new StringFormat objects
                StringFormat format1 = new StringFormat(StringFormatFlags.NoClip);
                StringFormat format2 = new StringFormat(format1);

                // Set the LineAlignment and Alignment properties for
                // both StringFormat objects to different values.
                format1.LineAlignment = StringAlignment.Center;
                format1.Alignment = StringAlignment.Near;
                format2.LineAlignment = StringAlignment.Center;
                format2.Alignment = StringAlignment.Far;

                Rectangle rec1 = new Rectangle();
                Rectangle rec2 = new Rectangle();
                Size size1 = new Size();
                Size size2 = new Size();
                int numline = 1;

                if (items.Count > 0)
                {
                    // Remap data
                    var Items = new List<PrintRec>();
                    var Item = new PrintRec();
                    foreach (var v in items)
                    {
                        Item = new PrintRec();
                        Item.Key = v.Key;
                        Item.Value = v.Value;
                        numline = Divide_Line(e, Font, v.Key, keyWidth > valueWidth ? keyWidth : valueWidth);
                        Item.Height = numline > 1 ? (int)Math.Ceiling((double)height * numline * 0.7) : height * numline;
                        Items.Add(Item);
                    }

                    // Draw content
                    foreach (var v in Items)
                    {
                        size1 = new Size(keyWidth, v.Height);
                        size2 = new Size(valueWidth, v.Height);
                        rec1 = new Rectangle(new Point(x, y), size1);
                        rec2 = new Rectangle(new Point(x + keyWidth, y), size2);

                        e.Graphics.DrawRectangle(pen, rec1);
                        e.Graphics.DrawRectangle(pen, rec2);
                        e.Graphics.DrawString(v.Key, Font,
                            brush, (RectangleF)rec1, format1);
                        e.Graphics.DrawString(v.Value, Font,
                            brush, (RectangleF)rec2, format2);

                        x = 1;
                        y += v.Height;
                    }
                    y++;
                }
            }
        }

        /// <summary>
        /// Divide lines width long string
        /// </summary>
        /// <param name="e"></param>
        /// <param name="font"></param>
        /// <param name="str"></param>
        /// <param name="boundWidth"></param>
        /// <returns></returns>
        public int Divide_Line(PrintPageEventArgs e, Font font, string str, int boundWidth)
        {
            int numberLine = 1;
            if ((int)e.Graphics.MeasureString("a", font).Width > 0)
            {
                int charsWidth = (int)e.Graphics.MeasureString(str, font).Width;
                numberLine = (int)Math.Ceiling((double)charsWidth / boundWidth);
            }
            return numberLine;
        }

        /// <summary>
        /// Generate bill code
        /// </summary>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public string GenBillCode(string prefix, int template = 4)
        {
            using(var db = new Solution_30shineEntities())
            {
                string code = "";
                string temp = "";
                int integer;
                SalonId = Session["SalonId"] != null ? (int.TryParse(Session["SalonId"].ToString(), out integer) ? integer : 0) : 0;
                var Where = PredicateBuilder.True<BillService>();
                DateTime _FromDate = DateTime.Today;
                DateTime _ToDate = DateTime.Today.AddDays(1);
                Where = Where.And(w => w.IsDelete != 1 && w.CreatedDate >= _FromDate && w.CreatedDate < _ToDate);
                if (SalonId > 0)
                {
                    Where = Where.And(w=>w.SalonId == SalonId);
                }
                var LastBill = db.BillServices.AsExpandable().Where(Where).OrderByDescending(w=>w.Id).Take(1).ToList();
                if (LastBill.Count > 0)
                {
                    if (LastBill[0].BillCode != null && LastBill[0].BillCode != "")
                        temp = (int.TryParse(LastBill[0].BillCode.Substring(LastBill[0].BillCode.Length - template, template), out integer) ? ++integer : 0).ToString();
                    else
                        temp = "1";
                }
                else
                {
                    temp = "1";
                }

                int len = temp.Length;
                for (var i = 0; i < template; i++)
                {
                    if (len > 0)
                    {
                        code = temp[--len].ToString() + code;
                    }
                    else
                    {
                        code = "0" + code;
                    }
                }

                return prefix + String.Format("{0:ddMMyy}", DateTime.Now) + code;
            }            
        }

        [WebMethod]
        public static string GetCustomer(string CustomerCode)
        {
            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var _Customer = db.Customers.FirstOrDefault(w => (w.Customer_Code == CustomerCode || w.Phone == CustomerCode) && w.IsDelete != 1);

                if (_Customer != null)
                {                    
                    _Msg.success = true;
                    _Msg.msg = serializer.Serialize(_Customer);
                }
                else 
                {
                    _Msg.success = false;   
                }
            }
            return serializer.Serialize(_Msg);
        }

        [WebMethod]
        public static string CheckDuplicateBill(string CustomerCode)
        {
            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var today = Convert.ToDateTime(DateTime.Now.ToString("d/M/yyyy"), culture);
                var todayL = today.AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                var todayR = today.AddDays(1);
                var _Bill = db.BillServices.FirstOrDefault(w => w.CreatedDate > todayL && w.CreatedDate < todayR && w.CustomerCode == CustomerCode && w.IsDelete != 1 && w.Pending != 1);

                if (_Bill != null)
                {
                    _Msg.success = true;
                    _Msg.msg = serializer.Serialize(_Bill);
                }
                else
                {
                    _Msg.success = false;
                }
            }
            return serializer.Serialize(_Msg);
        }

        /// <summary>
        /// Check permission
        /// </summary>
        //private void CheckPermission()
        //{
        //    if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
        //    {
        //        var MsgParam = new List<KeyValuePair<string, string>>();
        //        UIHelpers.Redirect("/dang-nhap.html", MsgParam);
        //    }
        //    else
        //    {
        //        string[] Allow = new string[] { "root", "admin", "salonmanager", "reception", "checkin", "checkout" };
        //        string[] AllowSalon = new string[] { "root", "admin"};
        //        var Permission = Session["User_Permission"].ToString().Trim();
        //        if (Array.IndexOf(Allow, Permission) != -1)
        //        {
        //            Perm_Access = true;
        //        }
        //        if (Array.IndexOf(AllowSalon, Permission) != -1)
        //        {
        //            Perm_ShowSalon = true;
        //        }
        //    }

        // Call execute function
        //ExecuteByPermission();
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
            else if (Perm_ShowSalon)
            {
                TrSalon.Visible = true;
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ShowSalon);
            }
        }
    }

       

    public struct PrintRec
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public int Height { get; set; }
    }

}