﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Web.Helpers;
using System.Text.RegularExpressions;
using System.Globalization;
using LinqKit;
using System.Text;
using System.Data.Entity.Migrations;
using System.IO;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.FrontEnd.Service
{
    public partial class Service_Checkin : System.Web.UI.Page
    {
        private bool Perm_ShowSalon = false;
        private bool Perm_Access = false;

        private List<BillService> CustomerHistoryService = new List<BillService>();
        private List<BillService> CustomerHistoryProduct = new List<BillService>();
        private List<ProductBasic> ProductList = new List<ProductBasic>();
        private List<ProductBasic> ServiceList = new List<ProductBasic>();
        protected BillService billBooking = new BillService();
        private string _CustomerCode = "";
        private string _CustomerName = "";
        private string BillCode = "";
        private string PDFname = "";
        private int SalonId;
        protected int _salonId;
        protected Tbl_Salon SalonRecord = new Tbl_Salon();

        //list booking
        protected Paging PAGING = new Paging();
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;

        protected List<TeamService> lstTeamService = new List<TeamService>();
        protected bool HasService = false;
        /// <summary>
        /// check permission
        /// </summary>
        protected void checkPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //string perName = Session["User_Permission"].ToString();
                //string url = Request.Url.AbsolutePath.ToString();
                //Solution_30shineEntities db = new Solution_30shineEntities();
                //var pem = (from m in db.Tbl_Permission_Map
                //           join mn in db.Tbl_Permission_Menu on m.mID equals mn.mID
                //           where m.pName == perName && mn.Url_Main.EndsWith(url) || mn.Url_Rewrite.EndsWith(url)
                //           select new { m.aID }
                //           ).FirstOrDefault();
                //if (pem != null)
                //{
                //    var pemArray = pem.aID.Split(',');
                //    if (Array.IndexOf(pemArray, "1") > -1)
                //    {
                //        Perm_Access = true;
                //        if (Array.IndexOf(pemArray, "2") > -1)
                //        {
                //            Perm_ViewAllData = true;
                //        }
                //        if (Array.IndexOf(pemArray, "6") > -1)
                //        {
                //            Perm_ShowSalon = true;
                //        }
                //        if (Array.IndexOf(pemArray, "4") > -1)
                //        {
                //            Perm_Edit = true;
                //        }
                //        if (Array.IndexOf(pemArray, "5") > -1)
                //        {
                //            Perm_Delete = true;
                //        }
                //    }
                //    else
                //    {
                //        ContentWrap.Visible = false;
                //        NotAllowAccess.Visible = true;
                //    }

                //}
                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();

            }
        }
        //private void ExecuteByPermission()
        //{
        //    if (!Perm_Access)
        //    {
        //        ContentWrap.Visible = false;
        //        NotAllowAccess.Visible = true;
        //    }
        //}
        protected void Page_Load(object sender, EventArgs e)
        {
            checkPermission();

            if (!IsPostBack)
            {
                if (Session["SalonId"] != null)
                {
                    HDF_SalonId.Value = Session["SalonId"].ToString();
                    _salonId = Convert.ToInt32(Session["SalonId"]);
                }
                else
                {
                    HDF_SalonId.Value = "0";
                    _salonId = 0;
                }

                using (var db = new Solution_30shineEntities())
                {
                    SalonRecord = db.Tbl_Salon.FirstOrDefault(w => w.Id == _salonId);
                }

                Library.Function.bindSalon(new List<DropDownList>() { ddlSalon1, ddlSalon2 }, Perm_ViewAllData);
                Bind_TeamWork();
                //bindSocialThread();                
                //Bind_RptServiceFeatured();
                //Bind_RptService();
                //Bind_RptFreeService();
                // Mặc định chọn dịch vụ Shine Combo 100K
                HDF_ServiceIds.Value = "[{\"Id\":53,\"Code\":\"SP00036\",\"Name\":\"Shine Combo 100k\",\"Price\":100000,\"Quantity\":1,\"VoucherPercent\":0}]";
            }
        }

        /// <summary>
        /// Thêm mới bill dịch vụ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddService(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                //// Insert to BillService
                var obj = new BillService();
                int integer;
                int customerId = int.TryParse(HDF_CustomerCode.Value, out integer) ? integer : 0;
                string customerName = CustomerName.Text.Trim();
                string customerPhone = Regex.Replace(CustomerCode.Text, @"\s+", "");
                var customer = db.Customers.FirstOrDefault(w => w.Phone == customerPhone && w.Phone != "" && w.IsDelete != 1);

                // cập nhật checkbox x2 điểm dịch vụ
                obj.IsX2 = isX2.Checked;

                if (TrSalon.Visible == true)
                {
                    obj.SalonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
                }
                else
                {
                    obj.SalonId = int.TryParse(Session["SalonId"].ToString(), out integer) ? integer : 0;
                }

                BillCode = GenBillCode(Convert.ToInt32(obj.SalonId), "HD", 4);
                PDFname = BillCode + "_" + UIHelpers.GetUniqueKey(6, 6);
                obj.BillCode = BillCode;

                if (customer == null)
                {
                    /// Insert khách hàng mới
                    var newCus = new _30shine.MODEL.ENTITY.EDMX.Customer();
                    if (customerPhone == "")
                    {
                        customerPhone = UIHelpers.GetUniqueKey(8, 8);
                        newCus.isNoInfor = true;
                    }
                    if (customerName == "")
                    {
                        customerName = customerPhone;
                    }
                    newCus.Phone = customerPhone;
                    newCus.Customer_Code = customerPhone;
                    newCus.Fullname = customerName;
                    newCus.SalonId = obj.SalonId;
                    newCus.Info_Flow = int.TryParse(HDF_SocialThread.Value, out integer) ? integer : 0;
                    newCus.IsDelete = 0;
                    newCus.CreatedDate = DateTime.Now;
                    db.Customers.AddOrUpdate(newCus);
                    db.SaveChanges();

                    /// Add CustomerCode cho bill
                    obj.CustomerCode = newCus.Phone;
                    obj.CustomerId = newCus.Id;
                    _CustomerCode = newCus.Phone;
                    _CustomerName = newCus.Fullname;
                }
                else
                {
                    _CustomerName = customer.Fullname;
                    obj.CustomerId = customer.Id;
                    obj.CustomerCode = customer.Phone;
                }
                obj.ProductIds = "";
                obj.ServiceIds = HDF_ServiceIds.Value != "[]" ? HDF_ServiceIds.Value : "";
                obj.TotalMoney = 0;
                obj.IsDelete = 0;
                obj.Pending = 1;
                obj.PDFBillCode = PDFname;
                obj.TeamId = int.TryParse(HDF_TeamId.Value, out integer) ? integer : 0;
                if (Reception.Value != "")
                {
                    obj.ReceptionId = int.TryParse(Reception.Value, out integer) ? integer : 0;
                }

                // Kiểm tra nếu là khách booking
                DateTime today = DateTime.Now.Date;
                var bookingRecord = db.Bookings.Where(w => w.DatedBook == today && w.IsDelete != 1 && w.CustomerPhone == customerPhone && w.HourId > 0).Select(s => new { s.Id }).FirstOrDefault();
                if (bookingRecord != null)
                {
                    obj.IsBooking = true;
                    obj.BookingId = bookingRecord.Id;
                }
                else
                {
                    obj.IsBooking = false;
                }
                obj.CreatedDate = DateTime.Now;

                db.BillServices.Add(obj);
                var exc = db.SaveChanges();
                var Error = exc > 0 ? 0 : 1;

                if (Error == 0)
                {
                    // insert service to flow
                    var objService = new FlowService();
                    //var str = "[{\"Id\":\"19\",\"Price\":340000,\"Quantity\":\"1\"}]";
                    var serializer = new JavaScriptSerializer();
                    ServiceList = serializer.Deserialize<List<ProductBasic>>(obj.ServiceIds);
                    if (ServiceList != null)
                    {
                        foreach (var v in ServiceList)
                        {
                            objService = new FlowService();
                            objService.BillId = obj.Id;
                            objService.ServiceId = v.Id;
                            objService.Price = v.Price;
                            objService.Quantity = v.Quantity;
                            objService.VoucherPercent = Convert.ToInt32(v.VoucherPercent);
                            var coefficientRating = db.Services.FirstOrDefault(w => w.Id == v.Id);
                            if (coefficientRating != null && coefficientRating.CoefficientRating != null)
                            {
                                objService.CoefficientRating = coefficientRating.CoefficientRating;
                            }
                            else
                            {
                                objService.CoefficientRating = 0;
                            }
                            objService.CreatedDate = DateTime.Now;
                            objService.IsDelete = 0;
                            if (Perm_ShowSalon)
                            {
                                objService.SalonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
                            }
                            else
                            {
                                objService.SalonId = int.TryParse(HDF_SalonId.Value, out integer) ? integer : 0;
                            }
                            objService.SellerId = int.TryParse(Cosmetic.Value, out integer) ? integer : 0;

                            db.FlowServices.Add(objService);
                            Error += db.SaveChanges() > 0 ? 0 : 1;
                        }
                    }

                    if (Error == 0)
                    {
                        // Phụ trợ như : Cạo mặt, Lấy ráy tai...
                        var objPromotion = new FlowPromotion();
                        string promotion = (HDF_FreeService.Value != "[]" || HDF_FreeService.Value != "") ? HDF_FreeService.Value : "";
                        var prms = serializer.Deserialize<List<int>>(promotion);
                        if (prms != null)
                        {
                            foreach (var v in prms)
                            {
                                if (Error == 0)
                                {
                                    objPromotion = new FlowPromotion();
                                    objPromotion.PromotionId = v;
                                    objPromotion.BillId = obj.Id;
                                    objPromotion.Group = 1;
                                    objPromotion.Quantity = 1;
                                    objPromotion.CreatedDate = obj.CreatedDate;
                                    objPromotion.IsDelete = 0;
                                    objPromotion.SalonId = Convert.ToInt32(Session["SalonId"]);

                                    db.FlowPromotions.Add(objPromotion);
                                    exc = db.SaveChanges();
                                    Error = exc > 0 ? Error : ++Error;
                                }
                            }
                        }
                    }

                    if (Error == 0)
                    {
                        // print bill
                        PrintBillPDF.GenPDF(false, obj);
                        // Thông báo thành công
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Thêm thành công!"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_print_billcode", PDFname));
                        UIHelpers.Redirect("/dich-vu/checkin.html", MsgParam);
                    }
                    else
                    {
                        MsgSystem.Text = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                        MsgSystem.CssClass = "msg-system warning";
                    }
                }
                else
                {
                    MsgSystem.Text = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                    MsgSystem.CssClass = "msg-system warning";
                }

            }
        }

        /// <summary>
        /// Bind danh sách salon
        /// </summary>
        public void Bind_Salon()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Tbl_Salon.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Order).ToList();
                var Key = 0;
                var Count = LST.Count;
                Salon.DataTextField = "Salon";
                Salon.DataValueField = "Id";
                ListItem item = new ListItem("Chọn salon", "0");
                Salon.Items.Insert(Key, item);
                Key++;

                if (Count > 0)
                {
                    foreach (var v in LST)
                    {
                        item = new ListItem(v.Name, v.Id.ToString());
                        Salon.Items.Insert(Key, item);
                        Key++;
                    }
                }
            }
        }

        /// <summary>
        /// Bind danh sách sản phẩm
        /// </summary>
        //private void Bind_RptProduct()
        //{
        //    using (var db = new Solution_30shineEntities())
        //    {
        //        var _Products = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1).OrderByDescending(o => o.Id).ToList();

        //        Rpt_Product.DataSource = _Products;
        //        Rpt_Product.DataBind();
        //    }
        //}

        /// <summary>
        /// Bind danh sách dịch vụ
        /// </summary>
        //private void Bind_RptService()
        //{
        //    using (var db = new Solution_30shineEntities())
        //    {
        //        var _Service = db.Services.Where(w => w.IsDelete != 1 && w.Publish == 1 && (w.IsFreeService != 1 || w.IsFreeService == null)).OrderByDescending(o => o.Id).ToList();

        //        Rpt_Service.DataSource = _Service;
        //        Rpt_Service.DataBind();
        //    }
        //}

        /// <summary>
        /// Bind danh sách team
        /// </summary>
        private void Bind_TeamWork()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.TeamServices.Where(w => w.IsDelete != 1 && w.Publish == true && w.Status == 1).ToList();
                if (billBooking != null && billBooking.TeamId > 0 && lst.Count > 0)
                {
                    for (var i = 0; i < lst.Count; i++)
                    {
                        if (lst[i].Id != billBooking.TeamId)
                        {
                            lst.RemoveAt(i--);
                        }
                    }
                    HDF_TeamId.Value = billBooking.TeamId.ToString();
                    TeamColorSelect.Visible = false;
                }
                lstTeamService = lst;
                Rpt_TeamWork.DataSource = lst;
                Rpt_TeamWork.DataBind();
            }
        }

        /// <summary>
        /// Bind dịch vụ nổi bật
        /// </summary>
        private void Bind_RptServiceFeatured()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Services.Where(w => w.IsDelete != 1 && w.Publish == 1 && w.Status == 1).OrderByDescending(o => o.Id).ToList();

                Rpt_ServiceFeatured.DataSource = lst;
                Rpt_ServiceFeatured.DataBind();
            }
        }

        /// <summary>
        /// Bind các gói dịch vụ phụ trợ như : cắt móng tay, cạo mặt
        /// </summary>
        private void Bind_RptFreeService()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Service = db.Services.Where(w => w.IsDelete != 1 && w.Publish == 1 && w.IsFreeService == 1).OrderBy(o => o.Id).ToList();
                Rpt_FreeService.DataSource = _Service;
                Rpt_FreeService.DataBind();
            }
        }

        /// <summary>
        /// Generate bill code
        /// </summary>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public string GenBillCode(int salonId, string prefix, int template = 4)
        {
            using (var db = new Solution_30shineEntities())
            {
                string code = "";
                string temp = "";
                int integer;
                var Where = PredicateBuilder.True<BillService>();
                DateTime _FromDate = DateTime.Today;
                DateTime _ToDate = DateTime.Today.AddDays(1);
                Where = Where.And(w => w.IsDelete != 1 && w.CreatedDate >= _FromDate && w.CreatedDate < _ToDate);
                if (salonId > 0)
                {
                    Where = Where.And(w => w.SalonId == salonId);
                }
                var LastBill = db.BillServices.AsExpandable().Where(Where).OrderByDescending(w => w.Id).Take(1).ToList();
                if (LastBill.Count > 0)
                {
                    if (LastBill[0].BillCode != null && LastBill[0].BillCode != "")
                        temp = (int.TryParse(LastBill[0].BillCode.Substring(LastBill[0].BillCode.Length - template, template), out integer) ? ++integer : 0).ToString();
                    else
                        temp = "1";
                }
                else
                {
                    temp = "1";
                }

                int len = temp.Length;
                for (var i = 0; i < template; i++)
                {
                    if (len > 0)
                    {
                        code = temp[--len].ToString() + code;
                    }
                    else
                    {
                        code = "0" + code;
                    }
                }

                return prefix + String.Format("{0:ddMMyy}", DateTime.Now) + code;
            }
        }

        /// <summary>
        /// Lấy dữ liệu khách hàng theo mã KH
        /// </summary>
        /// <param name="CustomerCode"></param>
        /// <returns></returns>
        [WebMethod]
        public static string GetCustomer(string CustomerCode)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var _Customer = db.Customers.FirstOrDefault(w => (w.Customer_Code == CustomerCode || w.Phone == CustomerCode) && w.IsDelete != 1);

                if (_Customer != null)
                {
                    _Msg.success = true;
                    _Msg.msg = serializer.Serialize(_Customer);
                }
                else
                {
                    _Msg.success = false;
                }
            }
            return serializer.Serialize(_Msg);
        }

        /// <summary>
        /// Check khách hàng đã từng lập bill trong ngày, tránh nhập nhầm lần 2
        /// </summary>
        /// <param name="CustomerCode"></param>
        /// <returns></returns>
        [WebMethod]
        public static string CheckDuplicateBill(string CustomerCode)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var today = Convert.ToDateTime(DateTime.Now.ToString("d/M/yyyy"), culture);
                var todayL = today.AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                var todayR = today.AddDays(1);
                var _Bill = db.BillServices.FirstOrDefault(w => w.CreatedDate > todayL && w.CreatedDate < todayR && w.CustomerCode == CustomerCode && w.IsDelete != 1 && w.Pending != 1);

                if (_Bill != null)
                {
                    _Msg.success = true;
                    _Msg.msg = serializer.Serialize(_Bill);
                }
                else
                {
                    _Msg.success = false;
                }
            }
            return serializer.Serialize(_Msg);
        }

        /// <summary>
        /// Thêm khách hàng vào danh sách đợi tại salon
        /// 1. Kiểm tra tạo mới tài khoản nếu là khách mới
        /// 2. Cập nhật vào danh sách đợi tại salon
        /// </summary>
        /// <param name="CustomerPhone"></param>
        /// <param name="CustomerName"></param>
        /// <returns></returns>
        [WebMethod]
        public static string AddBillWaitAtSalon(string CustomerPhone, string CustomerName, int SalonId)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                // Kiểm tra đã tồn tại bản ghi cho khách hàng hay chưa
                CustomerPhone = CustomerPhone.Trim();
                DateTime now = DateTime.Now.Date;
                var billWait = db.Bill_WaitAtSalon.FirstOrDefault(w => w.CustomerPhone == CustomerPhone && w.SalonId == SalonId && w.IsDelete != true && w.CreatedTime >= now);
                if (billWait == null)
                {
                    var error = 0;
                    billWait = new Bill_WaitAtSalon();
                    if (CustomerPhone != "" && CustomerName != "" && SalonId > 0)
                    {
                        //1. Kiểm tra tạo mới tài khoản nếu là khách mới
                        var customer = db.Customers.FirstOrDefault(w => w.Phone == CustomerPhone && w.IsDelete != 1);
                        if (customer == null)
                        {
                            customer = new _30shine.MODEL.ENTITY.EDMX.Customer();
                            customer.Phone = CustomerPhone;
                            customer.Customer_Code = CustomerPhone;
                            customer.Fullname = CustomerName;
                            customer.SalonId = SalonId;
                            customer.IsDelete = 0;
                            customer.Publish = 1;
                            customer.CreatedDate = DateTime.Now;
                            db.Customers.AddOrUpdate(customer);
                            db.SaveChanges();
                        }
                        //2. Cập nhật khách vào danh sách đợi tại salon                    
                        billWait.CustomerPhone = CustomerPhone;
                        billWait.CustomerName = CustomerName;
                        billWait.SalonId = SalonId;
                        billWait.IsDelete = false;
                        billWait.IsPending = false;
                        billWait.CreatedTime = DateTime.Now;
                        db.Bill_WaitAtSalon.Add(billWait);
                        error += db.SaveChanges() > 0 ? 0 : 1;
                    }
                    else
                    {
                        error += 1;
                    }

                    if (error == 0)
                    {
                        _Msg.success = true;
                        _Msg.msg = serializer.Serialize(billWait);
                    }
                    else
                    {
                        _Msg.success = false;
                    }
                }
                else
                {
                    _Msg.success = false;
                }

            }
            return serializer.Serialize(_Msg);
        }

        /// <summary>
        /// Cập nhật trạng thái chuyển sang pending đối với khách chờ
        /// </summary>
        /// <param name="billWaitId"></param>
        /// <param name="isPending"></param>
        /// <returns></returns>
        [WebMethod]
        public static string UpdateBillWaitAtSalon(int id, bool status)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var bill = db.Bill_WaitAtSalon.FirstOrDefault(w => w.Id == id);
                if (bill != null)
                {
                    bill.IsPending = status;
                    bill.ModifiedTime = DateTime.Now;
                    db.Bill_WaitAtSalon.AddOrUpdate(bill);
                    db.SaveChanges();
                    _Msg.success = true;
                    _Msg.msg = "Cập nhật thành công.";
                }
            }
            return serializer.Serialize(_Msg);
        }

        [WebMethod]
        public static string loadBillWaitAtSalon(int salonId, int startID)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var Msg = new Msg();
            using (var db = new Solution_30shineEntities())
            {
                try
                {
                    var today = DateTime.Now.Date;
                    var tomorrow = today.AddDays(1);
                    var list = db.Store_HoaDon_Checkin_KhachCho(String.Format("{0:yyyy/MM/dd}", today), String.Format("{0:yyyy/MM/dd}", tomorrow), salonId).Where(w => w.Id > startID).ToList();
                    Msg.success = true;
                    Msg.msg = serializer.Serialize(list);
                }
                catch (Exception ex)
                {
                    Msg.success = false;
                    Msg.msg = ex.Message;
                }
            }

            return serializer.Serialize(Msg);
        }

        /// <summary>
        /// Lấy thông tin khách hàng theo số điện thoại
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        [WebMethod]
        public static object getCustomerByPhone(string phone)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                return db.Customers.FirstOrDefault(w => w.Phone == phone);
            }
        }

        /// <summary>
        /// Xóa bản ghi booking
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="Note"></param>
        /// <returns></returns>
        [WebMethod]
        public static object DeleteBooking(int Id, string Note)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var Msg = new Msg();
                if (Note != null && Note != "")
                {
                    var record = db.Bookings.FirstOrDefault(w => w.Id == Id);
                    if (record != null)
                    {
                        record.NoteDelete = Note;
                        //record.IsDelete = 1;
                        db.Bookings.AddOrUpdate(record);
                        db.SaveChanges();

                        Msg.success = true;
                        Msg.msg = "Xóa bản ghi thành công.";
                    }
                }
                else
                {
                    Msg.success = false;
                    Msg.msg = "Lỗi. Chưa nhập ghi chú.";
                }

                return Msg;
            }
        }

        /// <summary>
        /// Check permission
        /// </summary>
        //private void CheckPermission()
        //{
        //    if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
        //    {
        //        var MsgParam = new List<KeyValuePair<string, string>>();
        //        UIHelpers.Redirect("/dang-nhap.html", MsgParam);
        //    }
        //    else
        //    {
        //        string[] Allow = new string[] { "root", "admin", "salonmanager", "reception", "checkin", "checkout" };
        //        string[] AllowSalon = new string[] { "root", "admin" };
        //        var Permission = Session["User_Permission"].ToString().Trim();
        //        if (Array.IndexOf(Allow, Permission) != -1)
        //        {
        //            Perm_Access = true;
        //        }
        //        if (Array.IndexOf(AllowSalon, Permission) != -1)
        //        {
        //            Perm_ShowSalon = true;
        //            Perm_ViewAllData = true;
        //        }

        //    }

        //    // Call execute function
        //    ExecuteByPermission();
        //}

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
            else if (Perm_ShowSalon)
            {
                TrSalon.Visible = true;
                Library.Function.bindSalon(new List<DropDownList>() { Salon }, Perm_ShowSalon);
            }
        }


        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((System.Web.UI.WebControls.Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((System.Web.UI.WebControls.Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }

        [WebMethod]
        public static CustomerType checkCustomerType(string phone)
        {
            // Verify host from request
            if(_30shine.Helpers.SecurityLib.VerifyHostFromRequest())
            {
                return new CustomerType();
            }

            var db = new Solution_30shineEntities();
            var query = @"select B.* from SpecialCustomer A  left join CustomerType B on A.CustomerTypeId = B.Id left join Customer C on C.Id = A.CustomerId where A.IsDelete = 0 and  C.Phone like N'%" + phone + "%'";
            return db.Database.SqlQuery<CustomerType>(query).SingleOrDefault();
        }

        [WebMethod]
        public static store_getCutErrorBySpecCustomer_Result GetSpecialCustomer(string phone)
        {
            // Verify host from request
            if( _30shine.Helpers.SecurityLib.VerifyHostFromRequest())
            {
                return new store_getCutErrorBySpecCustomer_Result();
            }

            var db = new Solution_30shineEntities();
            return db.store_getCutErrorBySpecCustomer(phone).SingleOrDefault();
        }

        [WebMethod]
        public static store_Customer_getHisHairByVIPCustomer_Result GetVipCustomer(string phone)
        {
            if (Array.IndexOf(new string[] {
                                                "30shine.com", "30shine.net",
                                                "apibooking.30shine.com", "apibooking.30shine.net",
                                                "api.30shine.com", "api.30shine.net",
                                                "erp.30shine.com", "erp.30shine.net", "dev.erp.30shine.com", "staging.erp.30shine.com",
                                                "ql.30shine.com", "solution.30shine.net"}, HttpContext.Current.Request.UrlReferrer.Host) == -1)
            {
                return new store_Customer_getHisHairByVIPCustomer_Result();
            }
            else
            {
                var db = new Solution_30shineEntities();
                return db.store_Customer_getHisHairByVIPCustomer(phone).SingleOrDefault();
            }
        }

        public struct PrintRec
        {
            public string Key { get; set; }
            public string Value { get; set; }
            public int Height { get; set; }
        }

        public class BillService2 : BillService
        {
            public int billOrder { get; set; }
            public DateTime completeTime { get; set; }
            public string TeamColor { get; set; }
        }


    }
}
