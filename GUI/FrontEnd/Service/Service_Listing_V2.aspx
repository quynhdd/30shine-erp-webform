﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Service_Listing_V2.aspx.cs" Inherits="_30shine.GUI.UIService.Service_Listing_V2" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<%@ Register Src="~/GUI/BackEnd/Report/ReportMenu.ascx" TagName="ReportMenu" TagPrefix="uc1" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">

    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <style>
            table.table-total-report-1 {
                border-collapse: collapse;
            }

                table.table-total-report-1,
                table.table-total-report-1 th,
                table.table-total-report-1 td {
                    border: 1px solid black;
                }

                    table.table-total-report-1 td {
                        padding: 5px;
                        text-align: right;
                    }

                        table.table-total-report-1 td:first-child {
                            text-align: left;
                        }

            .billx2 tr,
            .billx2 td {
                background: yellow !important;
            }

            .is-booking {
                background: #bbff99;
            }

            .is-normal {
                background: #ff8080;
            }

            .is-paybycard {
                background: yellow;
            }

            .is-overtime {
                background: red;
            }

            ._p2 {
                float: left;
            }

            .p-note {
                font-family: Roboto Condensed Regular !important;
                margin-left: 7px;
                margin-right: 15px;
                padding-bottom: 5px;
            }

                .p-note span {
                    width: 10px;
                    height: 10px;
                    display: block;
                    float: left;
                    position: relative;
                    top: 5px;
                    margin-right: 5px;
                    border: 1px solid #000000;
                }

            .customer-listing .table-func-panel {
                margin-top: -25px;
                top: 20px;
            }

            .customer-listing .tag-wp {
                padding-left: 20px;
                margin-top: 8px;
            }


            #table-product-hover {
                position: relative;
                display: inline-block;
                border-bottom: 1px dotted black;
            }

                #table-product-hover #table-product {
                    visibility: hidden;
                    width: 400px;
                    /*background-color: #555;*/
                    /*color: #fff;*/
                    color: yellow;
                    text-align: center;
                    border-radius: 6px;
                    padding: 5px 0;
                    position: absolute;
                    z-index: 1;
                    bottom: 125%;
                    left: -244%;
                    margin-left: -150px;
                    opacity: 0;
                    /*transition: opacity 1s;*/
                }

                    #table-product-hover #table-product::after {
                        /*Content:"" hiện mũi tên nhỏ dưới text*/
                        /*content: "";*/
                        /*position: absolute;*/
                        background-color: none;
                        top: 100%;
                        left: 50%;
                        margin-left: -5px;
                        border-width: 5px;
                        border-style: solid;
                        border-color: #555 transparent transparent transparent;
                    }

                #table-product-hover:hover #table-product {
                    visibility: visible;
                    opacity: 1;
                }

            #ghi-chu-hover {
                position: relative;
                display: inline-block;
                border-bottom: 1px dotted black;
            }

                #ghi-chu-hover #ghi-chu {
                    visibility: hidden;
                    width: 278px;
                    height: auto;
                    background-color: #efefef;
                    color: black;
                    text-align: center;
                    /*border-radius: 6px;*/
                    border: 1px;
                    border-color: #bababa;
                    border-style: solid;
                    padding: 2px 2px 2px 2px;
                    position: absolute;
                    z-index: 1;
                    bottom: 120%;
                    left: -190px;
                    margin-left: -60px;
                    opacity: 0;
                    /*transition: opacity 1s;*/
                }

                    #ghi-chu-hover #ghi-chu::after {
                        position: absolute;
                        top: 100%;
                        left: 50%;
                        margin-left: -5px;
                        border-width: 5px;
                        border-style: solid;
                        border-color: #555 transparent transparent transparent;
                    }

                #ghi-chu-hover:hover #ghi-chu {
                    visibility: visible;
                    opacity: 1;
                }

            #note-bill-12p-hover {
                position: relative;
                display: inline-block;
                border-bottom: 1px dotted black;
            }

                #note-bill-12p-hover #note-bill-12p {
                    visibility: hidden;
                    width: 278px;
                    height: auto;
                    background-color: #efefef;
                    color: black;
                    text-align: center;
                    border: 1px;
                    border-color: #bababa;
                    border-style: solid;
                    padding: 2px 2px 2px 2px;
                    position: absolute;
                    z-index: 1;
                    bottom: 120%;
                    left: -190px;
                    margin-left: -60px;
                    opacity: 0;
                }

                    #note-bill-12p-hover #note-bill-12p::after {
                        position: absolute;
                        top: 100%;
                        left: 50%;
                        margin-left: -5px;
                        border-width: 5px;
                        border-style: solid;
                        border-color: #555 transparent transparent transparent;
                    }

                #note-bill-12p-hover:hover #note-bill-12p {
                    visibility: visible;
                    opacity: 1;
                }
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Dịch vụ &nbsp;&#187; </li>
                        <li class="li-listing active"><a href="/dich-vu/danh-sach.html">Danh sách</a></li>
                        <%--<% if(SalonId == 2){ %>--%>
                        <li class="li-pending"><a href="/dich-vu/pending.html">
                            <div class="pending-1"></div>
                            Pending</a></li>

                        <li class="li-add"><a href="/dich-vu/them-phieu-v3.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report" onload="abc()">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- System Message -->
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <!-- END System Message -->
                <!-- Filter -->
                <div class="row">
                    <div class="filter-item" style="margin-left: 0;">
                        <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="datepicker-wp">
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                                ClientIDMode="Static" runat="server"></asp:TextBox>

                            <strong class="st-head" style="margin-left: 10px; margin-right: 10px;">
                                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                            </strong>
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeTo" placeholder="Đến ngày"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                        </div>
                        <% if (Perm_ViewAllDate)
                            { %>
                        <br />
                        <% }
                            else
                            { %>
                        <style>
                            .datepicker-wp {
                                display: none;
                            }

                            .customer-listing .tag-wp {
                                padding-left: 0;
                                padding-top: 5px;
                            }
                        </style>
                        <% } %>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="Salon" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px;"></asp:DropDownList>
                    </div>
                    <div class="tag-wp">
                        <p class="tag tag-date" onclick="viewDataByDate($(this), '<%=Day1 %>')">Hôm kia <span class="txt-date"><%=Day1 %></span></p>
                        <p class="tag tag-date" onclick="viewDataByDate($(this), '<%=Day2 %>')">Hôm qua<span class="txt-date"><%=Day2 %></span></p>
                        <p class="tag tag-date tag-date-today" onclick="viewDataByDate($(this), '<%=Day3 %>')">Hôm nay<span class="txt-date"><%=Day3 %></span></p>
                    </div>

                    <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" Text="Xem dữ liệu"
                        ClientIDMode="Static" onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <%--<a href="/dich-vu/danh-sach.html" class="st-head btn-viewdata">Reset Filter</a>--%>
                    <div class="export-wp drop-down-list-wp">
                        <asp:Panel ID="fff" CssClass="st-head btn-viewdata"
                            ClientIDMode="Static" runat="server" onclick="EntryExportExcel('Export')">
                            Xuất File Excel
                        </asp:Panel>
                        <%--OnClick="Exc_ExportExcel" --%>
                        <ul class="ul-drop-down-list" style="display: none;">
                            <li onclick="EntryExportExcel('all')">Tất cả dữ liệu</li>
                            <li onclick="EntryExportExcel('current page')">Trang hiện tại</li>
                        </ul>
                    </div>
                    <div class="export-wp drop-down-list-wp">
                        <%if (Perm_ViewAllData)
                            {%>
                        <asp:Panel ID="Panel1" CssClass="st-head btn-viewdata"
                            ClientIDMode="Static" runat="server" onclick="EntryExportExcelForMisa('Export')">
                            Xuất All Data
                        </asp:Panel>
                        <%} %>
                    </div>
                </div>
                <!-- End Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <div class="row mgt">
                    <%--<strong class="st-head"><i class="fa fa-file-text"></i>Chi tiết các hóa đơn</strong>--%>
                </div>
                <asp:Timer ID="Timer1" runat="server" OnTick="_BtnClick" Interval="1000" Enabled="False"></asp:Timer>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="row">
                            <table class="table-total-report-1" style="float: left;">
                                <tbody>
                                    <tr>
                                        <td>Tổng số bill</td>
                                        <td>
                                            <span style="font-family: Roboto Condensed Bold; padding-left: 20px;"><%=totalReport.all_bills > 0 ? String.Format("{0:#,###}",totalReport.all_bills).Replace(',','.') : "0" %></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Bill dịch vụ</td>
                                        <td><span style="font-family: Roboto Condensed Bold; padding-left: 20px;"><%=totalReport.service_bills > 0 ?String.Format("{0:#,###}",totalReport.service_bills).Replace(',','.') : "0" %></td>
                                    </tr>
                                    <tr>
                                        <td>Bill mỹ phẩm (bill chỉ có mỹ phẩm)</td>
                                        <td><span style="font-family: Roboto Condensed Bold; padding-left: 20px;"><%=totalReport.product_bills > 0 ? String.Format("{0:#,###}",totalReport.product_bills).Replace(',','.') : "0" %></span></td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table-total-report-1" style="float: left; margin-left: 30px;">
                                <tbody>
                                    <tr>
                                        <td>Tổng doanh thu (VNĐ)</td>
                                        <td>
                                            <span style="font-family: Roboto Condensed Bold; padding-left: 20px;"><%= totalReport.all_money > 0 ? String.Format("{0:#,###}", totalReport.service_money + totalReport.product_money).Replace(',','.') : "0" %></span>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Doanh thu dịch vụ (VNĐ)</td>
                                        <td><span style="font-family: Roboto Condensed Bold; padding-left: 20px;"><%=totalReport.service_money > 0 ? String.Format("{0:#,###}",totalReport.service_money).Replace(',','.') : "0" %></span></td>
                                    </tr>
                                    <tr>
                                        <td>Doanh thu mỹ phẩm (VNĐ)</td>
                                        <td><span style="font-family: Roboto Condensed Bold; padding-left: 20px;"><%=totalReport.product_money > 0 ? String.Format("{0:#,###}",totalReport.product_money).Replace(',','.') : "0" %></span></td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table-total-report-1" style="float: left; margin-left: 30px;">
                                <tbody>
                                    <tr>
                                        <td>Số bill có ảnh</td>
                                        <td><span style="font-family: Roboto Condensed Bold; padding-left: 20px;"><%=totalReport.bill_has_images > 0 ? String.Format("{0:#,###}",totalReport.bill_has_images).Replace(',','.') : "0" %></span></td>
                                    </tr>
                                    <tr>
                                        <td>Số bill thanh toán qua thẻ</td>
                                        <td><span style="font-family: Roboto Condensed Bold; padding-left: 20px;"><%=totalReport.paybycard_bill > 0 ? String.Format("{0:#,###}",totalReport.paybycard_bill).Replace(',','.') : "0" %></span></td>
                                    </tr>
                                    <tr>
                                        <td>Số tiền thanh toán qua thẻ</td>
                                        <td><span style="font-family: Roboto Condensed Bold; padding-left: 20px;"><%=totalReport.paybycard_money > 0 ? String.Format("{0:#,###}",totalReport.paybycard_money).Replace(',','.') : "0" %></span></td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table-total-report-1" style="float: left; margin-left: 30px;">
                                <tbody>
                                    <tr>
                                        <td>Tổng giảm trừ (VNĐ)</td>
                                        <td><span style="font-family: Roboto Condensed Bold; padding-left: 20px;">0</span></td>
                                    </tr>
                                    <tr>
                                        <td>Tổng trả hàng (VNĐ)</td>
                                        <td><span style="font-family: Roboto Condensed Bold; padding-left: 20px;"><%=totalReport.total_product_return > 0 ? String.Format("{0:#,###}",totalReport.total_product_return).Replace(',','.') : "0" %></span></td>
                                    </tr>
                                    <tr>
                                        <td style="color: red">Tổng phải nộp (VNĐ)</td>
                                        <td><span style="font-family: Roboto Condensed Bold; padding-left: 20px; color: red"><%=String.Format("{0:#,###}",(totalReport.all_money-totalReport.paybycard_money-totalReport.total_product_return)).Replace(',','.') %></span></td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table-total-report-1" style="float: left; margin-left: 30px;">
                                <tbody>
                                    <tr>
                                        <td>Bill chờ dưới 15 phút</td>
                                        <td><span style="font-family: Roboto Condensed Bold; padding-left: 20px;"><%=totalWaitTime.BillUnder15 > 0 ? String.Format("{0:#,###}",totalWaitTime.BillUnder15).Replace(',','.') : "0" %></span></td>
                                    </tr>
                                    <tr>
                                        <td>Bill chờ quá 15 phút</td>
                                        <td><span style="font-family: Roboto Condensed Bold; padding-left: 20px;"><%=totalWaitTime.BillOver15 > 0 ? String.Format("{0:#,###}",totalWaitTime.BillOver15).Replace(',','.') : "0" %></span></td>
                                    </tr>
                                    <tr>
                                        <td>Bill không có DL thời gian chờ</td>
                                        <td><span style="font-family: Roboto Condensed Bold; padding-left: 20px;"><%= totalWaitTime.BillNotTime > 0 ? String.Format("{0:#,###}", totalWaitTime.BillNotTime).Replace(',','.') : "0" %></span></td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table-total-report-1" style="float: left; margin-left: 30px;">
                                <tbody>
                                    <tr>
                                        <td>Số lượt booking</td>
                                        <td><span style="font-family: Roboto Condensed Bold; padding-left: 20px;"><%=totalReport.booking_times > 0 ? String.Format("{0:#,###}",totalReport.booking_times).Replace(',','.') : "0" %></span></td>
                                    </tr>
                                    <tr>
                                        <td>Số khách book đã đến</td>
                                        <td><span style="font-family: Roboto Condensed Bold; padding-left: 20px;"><%=totalReport.booking_comes_times > 0 ? String.Format("{0:#,###}",totalReport.booking_comes_times).Replace(',','.') : "0" %></span></td>
                                    </tr>
                                    <tr>
                                        <td>Số khách đến salon chờ</td>
                                        <td><span style="font-family: Roboto Condensed Bold; padding-left: 20px;"><%=totalReport.wait_times > 0 ? String.Format("{0:#,###}",totalReport.wait_times).Replace(',','.') : "0" %></span></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Row Table Filter -->
                <div class="table-func-panel">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="200">200</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <p class="_p2">Ghi chú : </p>
                        <p class="_p2 p-note"><span class="is-booking"></span>Khách book lịch</p>
                        <p class="_p2 p-note"><span class="is-normal"></span>Khách ngoài</p>
                        <p class="_p2 p-note"><span class="is-paybycard"></span>Thanh toán quay thẻ</p>
                        <p class="_p2 p-note"><span class="is-overtime"></span>Tăng ca</p>
                        <div class="row table-wp">
                            <table class="table-add table-listing report-sales-listing">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Checkin</th>
                                        <th>Tgian chờ</th>
                                        <th>Checkout</th>
                                        <th>Tgian<br />
                                            hoàn thành<br />
                                            (phút)</th>
                                        <th>Đánh giá</th>
                                        <th>Tên khách hàng</th>
                                        <th>SĐT</th>
                                        <th class="has-sub">
                                            <div class="title">Dịch vụ</div>
                                            <table class="sub-table">
                                                <tbody>
                                                    <tr class="first">
                                                        <td class="col-xs-4">Tên</td>
                                                        <td class="col-xs-4">SL</td>
                                                        <td class="col-xs-4 no-bdr">D.Thu</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </th>
                                        <th class="has-sub">Sản phẩm
                                        </th>
                                        <th>Stylist</th>
                                        <th>Skinner</th>
                                        <th>NV Check-in</th>
                                        <th>NV Check-out</th>
                                        <th style="display: none">NV Security</th>
                                        <%-- <th>Bán mỹ phẩm</th>--%>
                                        <th>Đăng ảnh</th>
                                        <th style="padding-right: 27px; padding-left: 5px;">Tổng D.Thu</th>
                                        <th>Note bill 12p</th>
                                        <th>Ghi chú</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="RptBill" runat="server" OnItemDataBound="Repeater_ItemDataBound">
                                        <ItemTemplate>
                                            <tr>
                                                <td <%# Eval("IsX2") != null && Convert.ToBoolean(Eval("IsX2")) ? "class='is-overtime'" : "" %>><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td>
                                                    <a href="/dich-vu/<%# Eval("BillId") %>.html" target="_blank">
                                                        <%# Eval("CreatedDate") != null ? ( string.Format("{0:HH}",Eval("CreatedDate")) + "h" + string.Format("{0:mm}",Eval("CreatedDate")) ) : string.Format("{0:dd/MM/yyyy}",Eval("CreatedDate"))
                                                        %>
                                                    </a>
                                                </td>
                                                <td <%# Eval("IsBooking") != null && Convert.ToBoolean(Eval("IsBooking")) == true ? "class='is-booking'" : "class='is-normal'" %>>
                                                    <%# Eval("WaitTimeInProcedure") %>
                                                </td>
                                                <td>
                                                    <a href="/dich-vu/<%# Eval("BillId") %>.html" target="_blank">
                                                        <%# Eval("CompleteBillTime") != null ? ( string.Format("{0:HH}",Eval("CompleteBillTime")) + "h" + string.Format("{0:mm}",Eval("CompleteBillTime")) ) : string.Format("{0:dd/MM/yyyy}",Eval("CompleteBillTime"))
                                                        %>
                                                    </a>
                                                </td>
                                                <td><%# Eval("TotalCompleteTime") %></td>
                                                <td><%#Eval("RelationId") != null ? "Chưa đánh giá" : (Eval("Star") == null ? "Chưa đánh giá" : Eval("Star").ToString()) %></td>
                                                <td>
                                                    <a href="/khach-hang/<%# Eval("CustomerId") %>.html" target="_blank">
                                                        <%# Eval("CustomerName") %>
                                                    </a>
                                                </td>
                                                <td>
                                                    <%if (Perm_ViewPhone)
                                                        { %>
                                                    <a href="/khach-hang/<%# Eval("CustomerId") %>.html" target="_blank">
                                                        <%# Eval("CustomerPhone") != null ? Eval("CustomerPhone").ToString() : "" %>
                                                    </a>
                                                    <%} %>
                                                    <%else
                                                        { %>
                                                    <a href="/khach-hang/<%# Eval("CustomerId") %>.html" target="_blank">
                                                        <%# Library.Format.ReplaceFirstPhone(Eval("CustomerPhone") != null ? Eval("CustomerPhone").ToString() : "") %>
                                                    </a>
                                                    <%} %>
                                                </td>
                                                <td class="has-sub has-sub-service">
                                                    <table class="sub-table">
                                                        <tbody>
                                                            <%# Eval("ListServices") %>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td>
                                                    <div id="table-product-hover" class="has-sub has-sub-product">
                                                        <%# Eval("ProductIds")!=null && Eval("ProductIds").ToString() != "" ?"Chi tiết":""%>
                                                        <table id="table-product">
                                                            <tbody>
                                                                <tr style="background: #000;" class="first">
                                                                    <td style='width: 60%; font-family: Roboto Condensed Bold;'>Tên</td>
                                                                    <td style='width: 20%; font-family: Roboto Condensed Bold;'>Số lượng</td>
                                                                    <td style="font-family: Roboto Condensed Bold;">DT</td>
                                                                    <td style="font-family: Roboto Condensed Bold;">NV Bán </td>
                                                                </tr>
                                                                <%# Eval("ListProducts") %>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                                <td><%# Eval("StylistName") %></td>
                                                <td><%# Eval("SkinnerName") %></td>
                                                <td><%# Eval("CheckinName") %></td>
                                                <td><%# Eval("CheckoutName") %></td>
                                                <td style="display: none"><%# Eval("SecurityName") %></td>
                                                <%--<td><%# Eval("SellerName") %></td>--%>
                                                <td><%# Eval("Images") != null && Eval("Images") != "" ? 4 : 0 %></td>
                                                <td class="map-edit<%# Eval("IsPayByCard") != null && Convert.ToBoolean(Eval("IsPayByCard")) == true ? " is-paybycard" : "" %>" style="padding-right: 27px; padding-left: 5px;">
                                                    <div class="be-report-price" style="padding-left: 7px;"><%# Eval("TotalMoney") %></div>
                                                    <div class="edit-wp">
                                                        <asp:Panel CssClass="edit-action-wp action-edit" runat="server" ID="EAedit" Visible="false">
                                                            <a class="elm edit-btn" href="/dich-vu/<%# Eval("BillId") %>.html" title="Sửa" target="_blank"></a>
                                                        </asp:Panel>
                                                        <asp:Panel CssClass="edit-action-wp action-delete" runat="server" ID="EAdelete" Visible="false">
                                                            <a class="elm del-btn" onclick="del(this.parentNode.parentNode.parentNode.parentNode,'<%# Eval("BillId") %>', '<%# Eval("CustomerPhone") %>')" href="javascript://" title="Xóa"></a>
                                                        </asp:Panel>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div id="note-bill-12p-hover" class="has-sub has-sub-product">
                                                        <%# ((string)Eval("CustomerCode1")!="" && (string)Eval("CustomerCode1")!=null)?"Chi tiết":"" %>
                                                        <span id="note-bill-12p"><%# Eval("CustomerCode1") %></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div id="ghi-chu-hover" class="has-sub has-sub-product">
                                                        <%# (string)Eval("Note")!=""?"Chi tiết":"" %>
                                                        <span id="ghi-chu"><%# Eval("Note") %></span>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                        <asp:AsyncPostBackTrigger ControlID="Timer1" EventName="Tick" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="UPExcel" runat="server" ClientIDMode="Static">
                    <ContentTemplate></ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeExcel" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:Button ID="BtnFakeExcel" ClientIDMode="Static" runat="server" OnClick="Exc_ExportExcel" Style="display: none;" />
                <asp:Button ID="BtnFakeExcelForMisa" ClientIDMode="Static" runat="server" OnClick="Exc_ExportAddData" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="IsPaging" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_ExcelType" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELPage" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELSegment" />
            </div>
            <%-- END Listing --%>
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                // Add active menu
                //NoteByBillId();
                $("#glbService").addClass("active");

                // Price format
                UP_FormatPrice('.be-report-price');

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        //$input.val($input.val().split(' ')[0]);
                    }
                });

                // View data today
                //$(".tag-date-today").click();

                // test
                $(window).focus(function () {
                    //console.log("here");
                });
            });

            function SubTableFixHeight() {
                $("tbody table.sub-table").each(function () {
                    $(this).height($(this).parent().height() + 1);
                });
            }

            function viewDataByDate(This, time) {
                $(".tag-date.active").removeClass("active");
                This.addClass("active");
                $("#TxtDateTimeTo").val("");
                $("#TxtDateTimeFrom").val(time);
                $("#ViewData").click();
            }

            //============================
            // Event delete
            //============================
            function del(This, code, name) {
                var code = code || null,
                    name = name || null,
                    Row = This;
                if (!code) return false;

                // show EBPopup
                $(".confirm-yn").openEBPopup();
                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

                $("#EBPopup .yn-yes").bind("click", function () {
                    addLoading();
                    $.ajax({
                        type: "POST",
                        url: "/GUI/SystemService/Ajax/Del.aspx/Delele_Bill",
                        data: '{Id : "' + code + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            removeLoading();
                            var mission = JSON.parse(response.d);
                            if (mission.success) {
                                delSuccess();
                                Row.remove();
                            } else {
                                delFailed();
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    });
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
            }
        </script>

    </asp:Panel>
</asp:Content>
