﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Service_Book_Pending_V3.aspx.cs" Inherits="_30shine.GUI.FrontEnd.Service.Service_Book_Pending_V3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <link href="/Assets/css/bootstrap/bootstrap-toggle.min.css" rel="stylesheet" />
        <script src="/Assets/js/bootstrap/bootstrap-toggle.min.js"></script>
        <script src="/Assets/js/check-notify.js"></script>

        <style>
            .no-padding { padding: 0px !important; }
            .pending-content-wrap { width: auto; float: left; }
            .table-add { width: auto !important; float: left; }
            .wr-choose-team { width: 60px; margin-right: 25px; float: left; }
            .wr-choose-team .team-color-select { width: 60px; float: left; }
            .wr-choose-team .time { float: left; width: 100%; line-height: 25px; }

            .status-team { line-height: 5px; color: red; height: 5px; }
            .booking-pending { padding: 12px; float: left; width: 100%; }

            .no-padding { padding: 0px !important; }
            .no-margin { margin: 0px !important; }

            input { background: none; border: 1px solid #d0d0d0; }
            .caption { padding-bottom: 10px; float: left; text-align: left; }
            .btn { padding: 5px; background: #d0d0d0; cursor: pointer; }
            .table { padding: 5px; }
            .table thead tr { background: #d0d0d0; }
            .table tr td { padding: 5px; }
            table .team { margin: 0px; }
            table .team::before { width: 100%; height: 1px; background: #000000; }
            table.table td { border: 1px solid #bababa; }
            .table-booking thead tr { border-top: 1px solid #bababa; }
            .table-booking thead tr td { font-family: Roboto Condensed Bold; }
            .team { /*background: #ff0000;*/ padding: 5px; margin: 5px; float: left; width: 50px; height: 20px; cursor: pointer; text-align: center; }
            .team:hover { border: 1px solid #d0d0d0; }
            .action { padding: 12px; }
            .item-color { margin-right: 0px; width: 45px; margin-right: 5px; }

            table.table-booked .pending { cursor: pointer; }
            table.table-booked .pending:hover { color: #50b347; }
            table.table-booked .a-team-color { padding: 0px; margin: 0; }
            table.table-booked .pending-team-color { height: 20px; }

            .customer-add .table-add tr.tr-send .btn-send { min-width: 80px; width: auto !important; padding: 0 10px; }
            /*.fe-service table.fe-service-table-add .iframe-add-customer iframe { min-height: 305px !important; }*/
            .customer-add .table-add .no-border { padding-top: 12px !important; }
            .fe-service table.fe-service-table-add .checkbox.cus-no-infor { margin-left: 0; }
            .customer-listing table.table-listing tbody tr .team-color-edit-box { left: auto; right: -285px; top: -40px; }

            .wrap-booking .head { float: left; width: 100%; padding-bottom: 20px; }
            .wrap-booking .head .title { float: left; font-family: Roboto Condensed Bold; margin-right: 20px; }
            .wrap-booking .head .btn { float: left; }
            .wrap-booking .head .btn i { margin-right: 5px; }
            .wrap-booking .team { background: red; padding: 0px; margin: 0px; }
            .wrap-booking .team.add-staff { float: right; background: #e7e7e7; border: 1px solid #bababa; border-bottom: none; font-size: 12px; line-height: 18px; opacity: 0.6; }
            .wrap-booking .team.add-staff:hover { opacity: 1; }
            .wrap-booking .staff-name { }
            .wrap-booking table { width: 100%; }
            .wrap-booking table tr { width: 100%; }
            .wrap-booking table tr td { padding: 3px; border: 1px solid #bababa; margin-left: 10px; }
            .wrap-booking { float: left; border-bottom: 1px dotted #bababa; margin-bottom: 12px; }
            .wrap-booking .field-wp { margin-left: 10px; float: left; }
            .wrap-booking .field-wp .wr-choose-team { float: left; width: auto; }
            .wrap-booking .wrap-list-added { display: table; }
            .wrap-booking .item-added { padding-bottom: 10px; display: inline-block; float: none; }
            .wrap-booking .item-color { text-align: center; }
            .wrap-booking .item-color i { line-height: 30px; color: #000; display: none; }
            .wrap-booking .item-color:hover i { display: block; }
            .wrap-booking .item-color.isadded { opacity: 0.4; }
            .wrap-booking .item-color.isadded i { display: none; }
            .wrap-booking .tag-staff { float: left; padding: 0 10px; font-family: Roboto Condensed Bold; font-size: 13px; border: 1px solid #d0d0d0; border-bottom: none; margin-left: 5px; height: 20px; cursor: pointer; position: relative; }
            .wrap-booking .tag-staff i { position: absolute; top: 3px; left: 39%; display: none; }
            .wrap-booking .tag-staff:hover { border: 1px solid #2e2e2e; color: #d0d0d0; }
            .wrap-booking .tag-staff:hover i { display: block; }
            .wrap-booking .item-added .team i { display: none; line-height: 22px; }
            .wrap-booking .item-added .team:hover i { display: block; }
            .wrap-booking .item-added .input-staff { width: 50px; padding: 0 5px; }

            .add-staff-box { position: absolute; top: 21px; right: 15px; float: left; background: #ffffff; padding: 4px; min-width: 290px; display: none; -webkit-box-shadow: 2px 2px 40px 1px rgba(102, 102, 102, 1); -moz-box-shadow: 2px 2px 40px 1px rgba(102, 102, 102, 1); box-shadow: 2px 2px 40px 1px rgba(102, 102, 102, 1); }
            .add-staff-box input { width: 160px; float: left; height: 26px; line-height: 26px; padding: 0 5px; position: relative; z-index: 2000; font-size: 13px; }
            .add-staff-box .action-box { height: 26px; line-height: 26px; float: left; padding: 0 10px; margin-left: 5px; cursor: pointer; font-size: 13px; font-family: Roboto Condensed Bold; color: #252525; -webkit-border-radius: 3px; -moz-border-radius: 3px; border-radius: 3px; -webkit-transition: 0.2s all ease; -moz-transition: 0.2s all ease; -o-transition: 0.2s all ease; -ms-transition: 0.2s all ease; transition: 0.2s all ease; }
            .add-staff-box .action-box.complete-box { background: #50b347; }
            .add-staff-box .action-box.close-box { background: #ff0000; }
            .add-staff-box .action-box:hover { color: #ffffff; -webkit-transition: 0.2s all ease; -moz-transition: 0.2s all ease; -o-transition: 0.2s all ease; -ms-transition: 0.2s all ease; transition: 0.2s all ease; }
            .add-staff-box .add-staff-box-wrap { float: left; position: relative; }
            .add-staff-box .input-fake-value { float: left; height: 26px; width: 160px; line-height: 26px; padding: 0 6px; position: absolute; top: 0px; left: 0px; z-index: 1000; font-size: 13px; }
            ul.ul-sub-menu li { position: relative; }
            .wp-booking { width: 450px; height: 400px; padding: 10px; position: absolute; top: 36px; left: 0; z-index: 10000; background: #ffffff; border: 1px solid #d0d0d0; float: left; -webkit-box-shadow: 2px 2px 40px 1px rgba(102, 102, 102, 1); -moz-box-shadow: 2px 2px 40px 1px rgba(102, 102, 102, 1); box-shadow: 2px 2px 40px 1px rgba(102, 102, 102, 1); display: none; }
            .wp-booking table { width: 100%; border-collapse: collapse; }
            .wp-booking table tr.is-makebill { background: #e7e7e7; }
            .wp-booking table tr.is-makebill td { text-decoration: line-through; }
            .wp-booking table,
            .wp-booking table th,
            .wp-booking table td { border: 1px solid black; padding: 3px 10px !important; }
            .wp-booking table th { font-weight: normal; font-family: Roboto Condensed Bold; text-align: center; }
            .wp-booking .booking-listing { width: 100%; float: left; }
            .wp-booking .close-booking-box { width: 52px; height: 26px; position: absolute; top: 0px; right: -1px; background: rgba(102, 102, 102, 1); text-align: center; color: white; line-height: 26px; font-size: 13px; font-style: italic; text-decoration: underline; cursor: pointer; }
            .wp-booking .wp-booking-content { width: 100%; height: 100%; float: left; overflow-x: hidden; overflow-y: scroll; }
        </style>
        <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Dịch vụ &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/dich-vu/danh-sach.html">Danh sách</a></li>
                        <li class="li-pending"><a href="/dich-vu/pending.html">
                            <div class="pending-1"></div>
                            Pending</a></li>
                        <li class="li-add"><a href="/dich-vu/them-phieu-v2.html">Thêm mới</a></li>
                        <%--<% if (_salonId == 0 || _salonId == 3)
                            { %>--%>
                        <li class="li-listing">
                            <a href="javascript:void(0);" onclick="toggleListBooking()" class="open-booking-list">Khách đặt lịch</a>
                            <div class="wp-booking">
                                <div class="wp-booking-content">
                                    <p style="font-family: Roboto Condensed Bold; text-transform: uppercase; padding-bottom: 5px; float: left;">Chọn salon</p>
                                    <asp:DropDownList ID="ddlSalon" runat="server" Style="width: 245px; margin-left: 10px; margin-bottom: 10px;" onchange="loadBillBookingBySalon($(this))">
                                    </asp:DropDownList>
                                    <div class="close-booking-box" onclick="toggleListBooking()">X Đóng</div>
                                    <div class="booking-listing">
                                        <table>
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Khung giờ hẹn</th>
                                                    <th>Khách hàng</th>
                                                    <th>Số ĐT</th>
                                                    <th>Đã đến</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <%--<% } %>--%>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add admin-product-add fe-service no-padding no-margin">
            <%-- Add --%>
            <div class="content-wp no-padding">

                <!-- END System Message -->
                <div class="container">
                    <!-- System Message -->
                    <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                    <div class="col-xs-12 col-lg-12 com-md-12 col-sm-12" style="padding-left: 0; padding-right: 0;">
                        <%--form input customer booking or pending--%>
                        <div class="col-xs-6 col-ld-6 col-md-6 col-sm-6">
                            <div class="table-wp">
                                <table class="table-add admin-product-table-add fe-service-table-add">
                                    <tbody>
                                        <%--   <tr class="title-head">
                                            <td><strong>Thông tin dịch vụ</strong></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr class="tr-margin" style="height: 20px;"></tr>--%>

                                        <tr class="tr-field-ahalf">
                                            <td class="col-xs-1 left"><span>KH</span></td>
                                            <td class="col-xs-11 right">
                                                <div class="field-wp">
                                                    <asp:TextBox ID="CustomerName" runat="server" ClientIDMode="Static" ReadOnly="true" Style="width: 190px; font-family: Roboto Condensed Bold; font-size: 15px;"></asp:TextBox>
                                                    <%--<asp:TextBox ID="CustomerCode" CssClass="mgl" placeholder="Nhập mã KH hoặc SĐT" runat="server" 
                                    ClientIDMode="Static" onchange="LoadCustomer(this.value)" style="width: 30%;"></asp:TextBox>--%>

                                                    <div class="filter-item" style="margin-left: 10px; z-index: 3000; width: 190px;">
                                                        <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion" data-field="customer.code" data-value="0"
                                                            data-callback="Callback_UpdateCustomerCode" AutoCompleteType="Disabled" ID="CustomerCode" ClientIDMode="Static"
                                                            onchange="CustomerInputOnchange($(this))" placeholder="Nhập SĐT" runat="server" Style="width: 25%;"></asp:TextBox>
                                                        <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                                                            <ul class="ul-listing-staff ul-listing-suggestion" id="Ul4"></ul>
                                                        </div>
                                                        <div class="fake-value"></div>
                                                    </div>

                                                    <div class="checkbox cus-no-infor" style="padding-top: 4px;">
                                                        <label class="lbl-cus-no-infor">
                                                            <input type="checkbox" id="InputCusNoInfor" runat="server" clientidmode="Static" />
                                                            Không cho thông tin
                                                        </label>
                                                    </div>
                                                    <div class="addnew-customer" onclick="quickAddCustomer($(this))" style="width: 82px; display: none;"><i class="fa fa-plus-circle"></i>&nbsp;Thêm mới</div>
                                                    <%--<div class="addnew-customer" onclick="quickAddCustomer($(this))" style="width: 110px;"><i class="fa fa-plus-circle"></i>&nbsp;Đồng bộ vân tay</div>--%>
                                                    <%--<asp:RequiredFieldValidator ID="ValidateCustomerName" ControlToValidate="CustomerName" runat="server" 
                                    CssClass="fb-cover-error" Text="Bạn chưa nhập mã khách hàng!" ClientIDMode="Static"></asp:RequiredFieldValidator>--%>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr class="tr-field-ahalf" id="TrIframeAddCustomer">
                                            <td class="col-xs-1 left"><span></span></td>
                                            <td class="col-xs-11 right">
                                                <div class="field-wp">
                                                    <div class="iframe-add-customer" id="IframeAddCustomer">
                                                        <iframe src="/khach-hang/them-moi-iframe.html"></iframe>
                                                    </div>
                                                    <%--<asp:RequiredFieldValidator ID="ValidateCustomerName" ControlToValidate="CustomerName" runat="server" 
                                    CssClass="fb-cover-error" Text="Bạn chưa nhập mã khách hàng!" ClientIDMode="Static"></asp:RequiredFieldValidator>--%>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr class="tr-field-ahalf" runat="server" id="TrSalon" visible="false">
                                            <td class="col-xs-1 left"><span>Salon</span></td>
                                            <td class="col-xs-11 right">
                                                <span class="field-wp">
                                                    <asp:DropDownList ID="Salon" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator InitialValue="0"
                                                        ID="ValidateSalon" Display="Dynamic"
                                                        ControlToValidate="Salon"
                                                        runat="server" Text="Bạn chưa chọn Salon!"
                                                        ErrorMessage="Vui lòng chọn Salon!"
                                                        ForeColor="Red"
                                                        CssClass="fb-cover-error">
                                                    </asp:RequiredFieldValidator>
                                                </span>
                                            </td>
                                        </tr>

                                        <tr class="tr-field-ahalf tr-product">
                                            <td class="col-xs-1 left"><span style="line-height: 18px;">Dịch vụ</span></td>
                                            <td class="col-xs-11 right">
                                                <%--<span class="field-wp">
                                                    <asp:DropDownList ID="ServiceName" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator InitialValue="0" 
                                                        ID="rfvDDL_Product" Display="Dynamic" 
                                                        ControlToValidate="ServiceName"
                                                        runat="server"  Text="Bạn chưa chọn dịch vụ!" 
                                                        ErrorMessage="Please Select the Product"
                                                        ForeColor="Red"
                                                        CssClass="fb-cover-error select-service-error">
                                                    </asp:RequiredFieldValidator>
                                                </span>--%>
                                                <div class="row" id="quick-service">
                                                    <asp:Repeater runat="server" ID="Rpt_ServiceFeatured">
                                                        <ItemTemplate>
                                                            <div class="checkbox cus-no-infor">
                                                                <label class="lbl-cus-no-infor">
                                                                    <input type="checkbox" data-code="<%# Eval("Code") %>" onclick="pushQuickData($(this), 'service')" />
                                                                    <%# Eval("Name") %>
                                                                </label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:Repeater>

                                                    <%--<div class="show-product show-item" data-item="service"><i class="fa fa-plus-circle"></i>Thêm dịch vụ</div>--%>
                                                </div>
                                                <div class="listing-product item-service" id="ListingServiceWp" runat="server" clientidmode="Static">
                                                    <table class="table table-listing-product table-item-service" id="table-item-service">
                                                        <thead>
                                                            <tr>
                                                                <th>STT</th>
                                                                <th>Tên dịch vụ</th>
                                                                <th>Mã dịch vụ</th>
                                                                <th style="display: none;">Đơn giá</th>
                                                                <th style="display: none;">Số lượng</th>
                                                                <th style="display: none;">Giảm giá</th>
                                                                <th style="display: none;">Thành tiền</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <asp:Repeater ID="Rpt_Service_Bill" runat="server">
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                                                        <td class="td-product-name"><%# Eval("Name") %></td>
                                                                        <td class="td-product-code" data-id="<%# Eval("Id")%>"><%# Eval("Code") %></td>
                                                                        <td class="td-product-price" data-price="<%# Eval("Price") %>"><%# Eval("Price") %></td>
                                                                        <td class="td-product-quantity">
                                                                            <input type="text" class="product-quantity" value="<%# Eval("Quantity") %>" />
                                                                        </td>
                                                                        <td class="td-product-voucher" data-voucher="<%# Eval("VoucherPercent") %>">
                                                                            <div class="row">
                                                                                <input type="text" class="product-voucher" value="<%# Eval("VoucherPercent") %>" style="margin: 0 auto; float: none; text-align: center; width: 50px;" />
                                                                            </div>
                                                                        </td>
                                                                        <td class="map-edit">
                                                                            <div class="box-money" style="display: block;">
                                                                                <%# Convert.ToInt32(Eval("Quantity")) * Convert.ToInt32(Eval("Price")) * 
                                                            (100-Convert.ToInt32(Eval("VoucherPercent")))/100 %>
                                                                            </div>
                                                                            <div class="edit-wp">
                                                                                <a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),'<%# Eval("Name") %>',<%# Eval("Id") %>,'service')"
                                                                                    href="javascript://" title="Xóa"></a>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </tbody>
                                                    </table>
                                                </div>

                                                <div class="row free-service">
                                                    <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                                        <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">Phụ trợ : </label>
                                                    </div>
                                                    <asp:Repeater runat="server" ID="Rpt_FreeService">
                                                        <ItemTemplate>
                                                            <div class="checkbox cus-no-infor" style="padding-bottom: 4px;">
                                                                <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                                    <input type="checkbox" onclick="pushFreeService($(this),<%# Eval("Id")%>)" data-id="<%# Eval("Id") %>"
                                                                        <%# Eval("Status").ToString() == "1" ? "checked='checked'" : "" %> />
                                                                    <%# Eval("Name") %>
                                                                </label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </div>

                                            </td>
                                        </tr>

                                        <tr class="tr-send">
                                            <td class="col-xs-1 left"><span>Team</span></td>
                                            <td class="col-xs-11 right">
                                                <div class="field-wp ">
                                                    <p class="status-team"></p>
                                                    <div class="wr-choose-team">
                                                        <div class="team-color-select" runat="server" id="TeamColorSelect" clientidmode="Static" data-color="">
                                                        </div>
                                                        <p class="time"></p>
                                                    </div>
                                                    <asp:Repeater runat="server" ID="Rpt_TeamWork">
                                                        <ItemTemplate>
                                                            <div class="item-color" style="background: <%# Eval("Color")%>;" onclick="setTeamColor($(this),<%# Eval("Id") %>,'<%# Eval("Color") %>')"></div>
                                                        </ItemTemplate>
                                                    </asp:Repeater>

                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="col-xs-1 left"><span style="line-height: 18px;">Lễ tân</span></td>
                                            <td class="col-xs-11 right">
                                                <div class="filter-item" style="width: 180px;">
                                                    <asp:TextBox CssClass="st-head ip-short auto-select" data-field="Reception" data-value="0"
                                                        AutoCompleteType="Disabled" ID="InputReception" ClientIDMode="Static" placeholder="Nhập mã số" runat="server"></asp:TextBox>
                                                    <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                                                        <ul class="ul-listing-staff ul-listing-suggestion" id="Ul1"></ul>
                                                    </div>
                                                    <div class="fake-value" id="FVReception" runat="server" clientidmode="Static"></div>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr class="tr-send">
                                            <td class="col-xs-1 left"></td>
                                            <td class="col-xs-11 right no-border">
                                                <span class="field-wp">
                                                    <asp:Panel ID="BtnSend" CssClass="btn-send" runat="server" ClientIDMode="Static">In phiếu</asp:Panel>

                                                    <asp:Button ID="BtnFakeSend" runat="server" Text="Hoàn tất"
                                                        ClientIDMode="Static" OnClick="AddService" Style="display: none;"></asp:Button>

                                                    <asp:Panel ID="BtnBook" CssClass="btn-send btn-book" runat="server" ClientIDMode="Static" Style="margin-left: 12px; display: none;">Đặt lịch</asp:Panel>

                                                    <%--<asp:Button ID="BtnFakeBook" runat="server" Text="Hoàn tất"
                                                        ClientIDMode="Static" OnClick="AddServiceBooking" Style="display: none;"></asp:Button>--%>
                                                </span>

                                            </td>
                                        </tr>

                                        <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                                        <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
                                    </tbody>
                                </table>

                                <!-- input hidden -->
                                <asp:HiddenField ID="HDF_CustomerCode" runat="server" ClientIDMode="Static" />
                                <asp:HiddenField ID="HDF_ProductIds" runat="server" ClientIDMode="Static" />
                                <asp:HiddenField ID="HDF_ServiceIds" runat="server" ClientIDMode="Static" />
                                <asp:HiddenField ID="HDF_TotalMoney" runat="server" ClientIDMode="Static" />
                                <asp:HiddenField ID="HDF_UserImages" runat="server" ClientIDMode="Static" />
                                <asp:HiddenField ID="HDF_UserImagesDelete" runat="server" ClientIDMode="Static" />
                                <asp:HiddenField ID="Hairdresser" runat="server" ClientIDMode="Static" />
                                <asp:HiddenField ID="HairMassage" runat="server" ClientIDMode="Static" />
                                <asp:HiddenField ID="Cosmetic" runat="server" ClientIDMode="Static" />
                                <asp:HiddenField ID="HDF_SalonId" runat="server" ClientIDMode="Static" />
                                <asp:HiddenField ID="HDF_Suggestion_Code" ClientIDMode="Static" runat="server" />
                                <asp:HiddenField ID="HDF_Suggestion_Field" ClientIDMode="Static" runat="server" />
                                <asp:HiddenField ID="HDF_FreeService" ClientIDMode="Static" runat="server" />
                                <asp:HiddenField ID="HDF_TeamId" ClientIDMode="Static" runat="server" />
                                <asp:HiddenField ID="HDF_TeamId_Booking" ClientIDMode="Static" runat="server" />
                                <asp:HiddenField ID="Reception" runat="server" ClientIDMode="Static" />
                                <!-- END input hidden -->
                            </div>
                        </div>
                        <%--form listing booking--%>
                        <div class="col-xs-6 col-ld-6 col-md-6 col-sm-6">

                            <div class="wrap-booking floor-2">
                                <div class="head">
                                    <p class="title">TẦNG 2</p>
                                    <%--   <div class="btn btn-add-team"><span><i class="fa fa-plus" aria-hidden="true"></i>Thêm Team</span></div>--%>
                                    <div class="field-wp ">
                                        <div class="wr-choose-team" data-status="0">
                                            <% if (lstTeamService.Count > 0)
                                                {
                                                    foreach (var v in lstTeamService)
                                                    { %>
                                            <div class="item-color" style="background: <%= v.Color %>;" onclick="addTeamToFloor(<%=v.Id %>,'<%=v.Color %>',2)" data-team-id="<%=v.Id %>">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </div>
                                            <% } %>
                                            <% } %>
                                            <%--<div class="item-color" style="background: #FEFE00;" onclick="addTeamToFloor(1,'#FEFE00',2)" data-team-id="1">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </div>
                                            <div class="item-color" style="background: #2771C3;" onclick="addTeamToFloor(2,'#2771C3',2)" data-team-id="2">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </div>
                                            <div class="item-color" style="background: #F40000;" onclick="addTeamToFloor(3,'#F40000',2)" data-team-id="3">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </div>
                                            <div class="item-color" style="background: #36B04B;" onclick="addTeamToFloor(4,'#36B04B',2)" data-team-id="4">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </div>
                                            <div class="item-color" style="background: #7DFF5C;" onclick="addTeamToFloor(5,'#7DFF5C',2)" data-team-id="5">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </div>
                                            <div class="item-color" style="background: #57FFFF;" onclick="addTeamToFloor(6,'#57FFFF',2)" data-team-id="6">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </div>
                                            <div class="item-color" style="background: #6D31A2;" onclick="addTeamToFloor(7,'#6D31A2',2)" data-team-id="7">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </div>
                                            <div class="item-color" style="background: #D9D9D9;" onclick="addTeamToFloor(8,'#D9D9D9',2)" data-team-id="8">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </div>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-ld-12 col-md-12 col-sm-12 wrap-list-added"></div>
                            </div>

                            <div class="wrap-booking floor-3">
                                <div class="head">
                                    <p class="title">TẦNG 3</p>
                                    <%--   <div class="btn btn-add-team"><span><i class="fa fa-plus" aria-hidden="true"></i>Thêm Team</span></div>--%>
                                    <div class="field-wp ">
                                        <div class="wr-choose-team" data-status="0">
                                            <% if (lstTeamService.Count > 0)
                                                {
                                                    foreach (var v in lstTeamService)
                                                    { %>
                                            <div class="item-color" style="background: <%= v.Color %>;" onclick="addTeamToFloor(<%=v.Id %>,'<%=v.Color %>',3)" data-team-id="<%=v.Id %>">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </div>
                                            <% } %>
                                            <% } %>
                                            <%--<div class="item-color" style="background: #FEFE00;" onclick="addTeamToFloor(1,'#FEFE00',3)" data-team-id="1">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </div>
                                            <div class="item-color" style="background: #2771C3;" onclick="addTeamToFloor(2,'#2771C3',3)" data-team-id="2">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </div>
                                            <div class="item-color" style="background: #F40000;" onclick="addTeamToFloor(3,'#F40000',3)" data-team-id="3">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </div>
                                            <div class="item-color" style="background: #36B04B;" onclick="addTeamToFloor(4,'#36B04B',3)" data-team-id="4">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </div>
                                            <div class="item-color" style="background: #7DFF5C;" onclick="addTeamToFloor(5,'#7DFF5C',3)" data-team-id="5">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </div>
                                            <div class="item-color" style="background: #57FFFF;" onclick="addTeamToFloor(6,'#57FFFF',3)" data-team-id="6">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </div>
                                            <div class="item-color" style="background: #6D31A2;" onclick="addTeamToFloor(7,'#6D31A2',3)" data-team-id="7">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </div>
                                            <div class="item-color" style="background: #D9D9D9;" onclick="addTeamToFloor(8,'#D9D9D9',3)" data-team-id="8">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </div>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-ld-12 col-md-12 col-sm-12 wrap-list-added"></div>
                            </div>

                            <div class="wrap-booking floor-4">
                                <div class="head">
                                    <p class="title">TẦNG 4</p>
                                    <%--   <div class="btn btn-add-team"><span><i class="fa fa-plus" aria-hidden="true"></i>Thêm Team</span></div>--%>
                                    <div class="field-wp ">
                                        <div class="wr-choose-team" data-status="0">
                                            <% if (lstTeamService.Count > 0)
                                                {
                                                    foreach (var v in lstTeamService)
                                                    { %>
                                            <div class="item-color" style="background: <%= v.Color %>;" onclick="addTeamToFloor(<%=v.Id %>,'<%=v.Color %>',4)" data-team-id="<%=v.Id %>">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </div>
                                            <% } %>
                                            <% } %>
                                            <%--<div class="item-color" style="background: #FEFE00;" onclick="addTeamToFloor(1,'#FEFE00',4)" data-team-id="1">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </div>
                                            <div class="item-color" style="background: #2771C3;" onclick="addTeamToFloor(2,'#2771C3',4)" data-team-id="2">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </div>
                                            <div class="item-color" style="background: #F40000;" onclick="addTeamToFloor(3,'#F40000',4)" data-team-id="3">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </div>
                                            <div class="item-color" style="background: #36B04B;" onclick="addTeamToFloor(4,'#36B04B',4)" data-team-id="4">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </div>
                                            <div class="item-color" style="background: #7DFF5C;" onclick="addTeamToFloor(5,'#7DFF5C',4)" data-team-id="5">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </div>
                                            <div class="item-color" style="background: #57FFFF;" onclick="addTeamToFloor(6,'#57FFFF',4)" data-team-id="6">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </div>
                                            <div class="item-color" style="background: #6D31A2;" onclick="addTeamToFloor(7,'#6D31A2',4)" data-team-id="7">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </div>
                                            <div class="item-color" style="background: #D9D9D9;" onclick="addTeamToFloor(8,'#D9D9D9',4)" data-team-id="8">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                            </div>--%>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-ld-12 col-md-12 col-sm-12 wrap-list-added"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <%-- end Add --%>
            </div>

            <!-- Danh mục sản phẩm -->
            <div class="popup-product-wp popup-product-item">
                <div class="wp popup-product-head">
                    <strong>Danh mục sản phẩm</strong>
                </div>
                <div class="wp popup-product-content">
                    <div class="wp popup-product-guide">
                        <div class="left">Hướng dẫn:</div>
                        <div class="right">
                            <p>- Click vào ô check box để chọn sản phẩm</p>
                            <p>- Click vào ô số lượng để thay đổi số lượng sản phẩm</p>
                        </div>
                    </div>
                    <div class="wp listing-product item-product">
                        <table class="table" id="table-product">
                            <thead>
                                <tr>
                                    <th>
                                        <input type="checkbox" /></th>
                                    <th>STT</th>
                                    <th>Tên sản phẩm</th>
                                    <th>Mã sản phẩm</th>
                                    <th>Đơn giá</th>
                                    <th class="th-product-quantity">Số lượng</th>
                                    <th>Giảm giá (%)</th>
                                    <th>Thành tiền</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="Rpt_Product" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td class="td-product-checkbox item-product-checkbox">
                                                <input type="checkbox" value="<%# Eval("Id") %>"
                                                    data-code="<%# Eval("Code") %>" data-name="<%# Eval("Name") %>" />
                                            </td>
                                            <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                            <td class="td-product-name"><%# Eval("Name") %></td>
                                            <td class="td-product-code" data-id="<%# Eval("Id") %>" data-code="<%# Eval("Code") %>"><%# Eval("Code") %></td>
                                            <td class="td-product-price" data-price="  <%# Eval("Price") %>"><%# Eval("Price") %></td>
                                            <td class="td-product-quantity">
                                                <input type="text" class="product-quantity" value="1" />
                                            </td>
                                            <td class="td-product-voucher" data-voucher="<%# Eval("VoucherPercent") %>">
                                                <div class="row">
                                                    <input type="text" class="product-voucher" value="<%# Eval("VoucherPercent") %>" style="margin: 0 auto; float: none; text-align: center; width: 50px;" />
                                                    %
                                                </div>
                                                <div class="row promotion-money">
                                                    <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                                        <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                            <input type="checkbox" class="item-promotion" onclick="excPromotion($(this), 35000)" data-value="35000" data-id="1" style="margin-left: 0;" />
                                                            - 35.000 VNĐ
                                                        </label>

                                                    </div>
                                                    <br />
                                                    <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                                        <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                            <input type="checkbox" class="item-promotion" onclick="excPromotion($(this), 50000)" data-id="2" data-value="50000" style="margin-left: 0;" />
                                                            - 50.000 VNĐ
                                                        </label>

                                                    </div>
                                                </div>
                                            </td>
                                            <td class="map-edit">
                                                <div class="box-money"></div>
                                                <div class="edit-wp">
                                                    <a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),'<%# Eval("Name") %>','product')"
                                                        href="javascript://" title="Xóa"></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </div>
                    <div class="wp btn-wp">
                        <div class="popup-product-btn btn-complete" data-item="product">Hoàn tất</div>
                        <div class="popup-product-btn btn-esc">Thoát</div>
                    </div>
                </div>
            </div>
            <!-- END Danh mục sản phẩm -->

            <!-- Danh mục dịch vụ -->
            <div class="popup-product-wp popup-service-item">
                <div class="wp popup-product-head">
                    <strong>Danh mục dịch vụ</strong>
                </div>
                <div class="wp popup-product-content">
                    <div class="wp popup-product-guide">
                        <div class="left">Hướng dẫn:</div>
                        <div class="right">
                            <p>- Click vào ô check box để chọn dịch vụ</p>
                            <p>- Click vào ô số lượng để thay đổi số lượng dịch vụ</p>
                        </div>
                    </div>
                    <div class="wp listing-product item-product">
                        <table class="table" id="table-service">
                            <thead>
                                <tr>
                                    <th>
                                        <input type="checkbox" /></th>
                                    <th>STT</th>
                                    <th>Tên dịch vụ</th>
                                    <th>Mã dịch vụ</th>
                                    <th style="display: none;">Đơn giá</th>
                                    <th style="display: none;" class="th-product-quantity">Số lượng</th>
                                    <th style="display: none;">Giảm giá (%)</th>
                                    <th style="display: none;">Thành tiền</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="Rpt_Service" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td class="td-product-checkbox item-product-checkbox">
                                                <input type="checkbox" value="<%# Eval("Id") %>"
                                                    data-code="<%# Eval("Code") %>" data-name="<%# Eval("Name") %>" />
                                            </td>
                                            <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                            <td class="td-product-name"><%# Eval("Name") %></td>
                                            <td class="td-product-code" data-id="<%# Eval("Id") %>" data-code="<%# Eval("Code") %>"><%# Eval("Code") %></td>
                                            <td style="display: none!important;" class="td-product-price" data-price="   <%# Eval("Price") %>"><%# Eval("Price") %></td>
                                            <td style="display: none;" class="td-product-quantity">
                                                <input type="text" class="product-quantity" value="1" />
                                            </td>
                                            <td style="display: none!important;" class="td-product-voucher" data-voucher="<%# Eval("VoucherPercent") %>">
                                                <div class="row">
                                                    <input type="text" class="product-voucher" value="<%# Eval("VoucherPercent") %>" style="margin: 0 auto; float: none; text-align: center; width: 50px" />
                                                    %
                                                </div>
                                                <%--<div class="row">
                                        <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                            <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                <input type="checkbox" onclick="excPromotion($(this), -35)" data-id="<%# Eval("Id") %>" style="margin-left: 0;" /> - 35.000 VNĐ </label>

                                        </div><br />
                                        <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                            <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                <input type="checkbox" onclick="excPromotion($(this), -50)" data-id="<%# Eval("Id") %>" style="margin-left: 0;" /> - 50.000 VNĐ </label>

                                        </div>
                                    </div>--%>
                                            </td>
                                            <td class="map-edit" style="display: none;">
                                                <div class="box-money"></div>
                                                <div class="edit-wp">
                                                    <a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),'<%# Eval("Name") %>','service')"
                                                        href="javascript://" title="Xóa"></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </div>
                    <div class="wp btn-wp">
                        <div class="popup-product-btn btn-complete" data-item="service">Hoàn tất</div>
                        <div class="popup-product-btn btn-esc">Thoát</div>
                    </div>
                </div>
            </div>
            <!-- END Danh mục dịch vụ -->
        </div>

        <script src="/assets/js/jquery.mCustomScrollbar.js"></script>
        <link href="/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />


        <script>
            var TYPE_DATE_FORMAT = "dd/MM/yyy";
            //btn pending and book action=0 
            //btn pending disable action=-1
            //btn booking disable action=-2
            var $action = $('#HDF_Action');
            var $customerAdd = $('.customer-add');
            var $fullName = $customerAdd.find('.full-name');
            var $phone = $customerAdd.find('.phone');
            var $btnBook = $('#BtnBook');


            var $tableBooked = $('table.table-booked');
            var $tableBooking = $('table.table-booking');

            var $btnPending = $('#BtnSend');
            var $btnFakePending = $('#BtnFakeSend');
            var $btnFakeBook = $('#BtnFakeBook');
            var $chooseItemColor = $('.wr-choose-team .item-color');
            var $teamIdBooking = $('#HDF_TeamId_Booking');
            var $teamColorSelect = $('#TeamColorSelect');


            jQuery(document).ready(function () {
                var salonId = $('#HDF_SalonId').val();
                updateLSstruct();
                bindTeamToFloor();               
            })

            setInterval(function () {
                bindTeamToFloor();
            }, 100000);
           

            //-----------------SET FLOOR FOR TEAM-------------------------------

            function updateLSstruct(){
                var listTeam = [];
                var lsitems = localStorage.getItem("team-floor");
                if (lsitems != 'undefined' && lsitems != null) {
                    var list = JSON.parse(lsitems);
                    if (list.length > 0) {
                        $(list).each(function (i, v) {
                            var obj = JSON.parse(v);
                            if(obj.Staffs == null || obj.Staffs == 'undefined'){
                                obj.Staffs = [];
                            }
                            listTeam.push(JSON.stringify(obj));
                        });
                    }
                }
                localStorage.setItem("team-floor", JSON.stringify(listTeam));
            }

            function bindTeamToFloor() {
                var $floor2 = $('.wrap-booking.floor-2 .wrap-list-added');
                var $floor3 = $('.wrap-booking.floor-3 .wrap-list-added');
                var $floor4 = $('.wrap-booking.floor-4 .wrap-list-added');
                var $allFloor = $('.wrap-booking .wrap-list-added');
                var $chooseTeam = $('.wrap-booking .wr-choose-team');
                var $itemColor = $('.wrap-booking .wr-choose-team .item-color');
                $floor2.empty();
                $floor3.empty();
                $floor4.empty();
                var lsitems = localStorage.getItem("team-floor");
                if (lsitems != 'undefined' && lsitems != null) {
                    var list = JSON.parse(lsitems);
                    if (list.length > 0) {
                        $(list).each(function (i, v) {
                            var obj = JSON.parse(v);
                            var teamId = obj.TeamId;
                            var color = obj.TeamColor;
                            var floor = obj.Floor;
                            var salonId = obj.SalonId;

                            var listPending = getBillPendingOfTeam(salonId, teamId);

                            var trs = "";
                            $(listPending).each(function (i, v) {
                                trs += "<tr><td>" + v.Order + "</td><td>" + v.ServiceName + "</td><td>" + v.CreatedTime + "</td></tr>";
                            });
                            var staffs = "";
                            if(obj.Staffs.length > 0){
                                $.each(obj.Staffs, function(i, v){
                                    staffs += '<div class="tag-staff" onclick="removeStaffFromTeam($(this), '+v.id+', '+teamId+')"><i class="fa fa-times" aria-hidden="true"></i>'+v.name+'</div>';
                                });
                            }

                            var itemAdd = "<div class='col-xs-6 col-lg-6 item-added' data-team-color='" + color + "' data-team-id='" + teamId + "' data-floor='" + floor + "' data-salon='" + salonId + "'><input style='display:none' type='text' class='input-staff' placeholder='nhân viên'><div onclick='removeItemAdded($(this))' class='team' style='background:" + color + "'><i class='fa fa-times' aria-hidden='true'></i></div><div class='team add-staff' onclick='openAddStaffBox($(this))'>Thêm NV</div><div class='add-staff-box'><div class='add-staff-box-wrap'><input type='text' autocomplete='off' placeholder='Nhập mã số NV' onkeyup='getStaffByOrderCode($(this))' /><div class='input-fake-value' data-id='' data-name=''></div><div class='action-box complete-box' onclick='addStaffToTeam($(this))'>Hoàn tất</div><div class='action-box close-box' onclick='closeAddStaffBox($(this))'>Đóng</div></div></div><div class='staff-name'>"+staffs+"</div><table><thead><tr><td>STT</td><td>Dịch vụ</td><td>Check-in</td></tr></thead><tbody>" + trs + "</tbody></table></div>";
                        
                            var hasTeam = false;
                            $allFloor.find(".item-added").each(function(){
                                if($(this).attr("data-team-id") == teamId){
                                    hasTeam = true;
                                }
                            });
                            if(!hasTeam){
                                switch (floor) {
                                    case 2:
                                        $floor2.append(itemAdd);                            
                                        break;
                                    case 3:
                                        $floor3.append(itemAdd);                            
                                        break;
                                    case 4:
                                        $floor4.append(itemAdd);                            
                                        break;
                                }   
                            }
                        });
                    }
                    updateListTeamColorStatus();
                }
            }

            function addTeamToFloor(teamId, color, floor) {
                var $floor2 = $('.wrap-booking.floor-2 .wrap-list-added');
                var $floor3 = $('.wrap-booking.floor-3 .wrap-list-added');
                var $floor4 = $('.wrap-booking.floor-4 .wrap-list-added');
                var $allFloor = $('.wrap-booking .wrap-list-added');
                var $chooseTeam = $('.wrap-booking .wr-choose-team');
                var $itemColor = $('.wrap-booking .wr-choose-team .item-color');

                var salonId = $('#HDF_SalonId').val();

                var listPending = getBillPendingOfTeam(salonId, teamId);

                var trs = "";
                $(listPending).each(function (i, v) {
                    trs += "<tr><td>" + v.Order+ "</td><td>" + v.ServiceName + "</td><td>" + v.CreatedTime + "</td></tr>";
                })

                var staffs = "";
                var itemAdd = "<div class='col-xs-6 col-lg-6 item-added' data-team-color='" + color + "' data-team-id='" + teamId + "' data-floor='" + floor + "' data-salon='" + salonId + "'><div onclick='removeItemAdded($(this))' class='team' style='background:" + color + "'><i class='fa fa-times' aria-hidden='true'></i></div><div class='team add-staff' onclick='openAddStaffBox($(this))'>Thêm NV</div><div class='add-staff-box'><div class='add-staff-box-wrap'><input type='text' autocomplete='off' placeholder='Nhập mã số NV' onkeyup='getStaffByOrderCode($(this))' /><div class='input-fake-value' data-id='' data-name=''></div><div class='action-box complete-box' onclick='addStaffToTeam($(this))'>Hoàn tất</div><div class='action-box close-box' onclick='closeAddStaffBox($(this))'>Đóng</div></div></div><div class='staff-name'>"+staffs+"</div><table><thead><tr><td>STT</td><td>Dịch vụ</td><td>Check-in</td></tr></thead><tbody>" + trs + "</tbody></table></div>";

                var hasTeam = false;
                $allFloor.find(".item-added").each(function(){
                    if($(this).attr("data-team-id") == teamId){
                        hasTeam = true;
                    }
                });
                if(!hasTeam){
                    switch (floor) {
                        case 2:
                            $floor2.append(itemAdd);                            
                            break;
                        case 3:
                            $floor3.append(itemAdd);                            
                            break;
                        case 4:
                            $floor4.append(itemAdd);                            
                            break;
                    }   
                    var obj = new Object();
                    obj.TeamId = teamId;
                    obj.Floor = floor;
                    obj.SalonId = salonId;
                    obj.TeamColor = color;
                    obj.Staffs = [];
                    var jsonObj = JSON.stringify(obj);

                    var lsitems = localStorage.getItem("team-floor");
                    if (lsitems != 'undefined' && lsitems != null) {
                        var list0 = JSON.parse(localStorage.getItem("team-floor"));
                        var list = updateList(list0, jsonObj);
                        var jsonList = JSON.stringify(list);
                        localStorage.setItem("team-floor", jsonList);
                    } else {
                        var list = [];
                        list.push(jsonObj);
                        var jsonList = JSON.stringify(list);
                        localStorage.setItem("team-floor", jsonList);
                    }

                    updateListTeamColorStatus();
                }
            }

            function removeItemAdded(This) {
                var salonId = This.parent().attr("data-salon");
                var teamId = This.parent().attr("data-team-id");
                var floor = This.parent().attr("data-floor");
                var color = This.parent().attr("data-team-color");

                var obj = new Object();
                obj.TeamId = teamId;
                obj.Floor = floor;
                obj.SalonId = salonId;
                obj.TeamColor = color;

                var jsonObj = JSON.stringify(obj);

                var list0 = JSON.parse(localStorage.getItem("team-floor"));
                var list = removeItemInList(list0, jsonObj);
                var jsonList = JSON.stringify(list);
                localStorage.setItem("team-floor", jsonList);
                This.parent().remove();

                updateListTeamColorStatus();
            }

            function removeItemInList(list, jsonObj) {
                var objNew = JSON.parse(jsonObj);
                if (list.length > 0) {
                    for (var i = 0; i < list.length; i++) {
                        var obj = JSON.parse(list[i]);
                        if (obj.TeamId == objNew.TeamId && obj.Floor == objNew.Floor && obj.SalonId == objNew.SalonId) {
                            list.splice(i, 1);
                        }
                    }
                }
                return list;
            }

            function updateList_syduy(list, jsonObj) {
                var objNew = JSON.parse(jsonObj);
                var check = false;
                if (list.length > 0) {
                    for (var i = 0; i < list.length; i++) {
                        var obj = JSON.parse(list[i]);
                        if (obj.TeamId == objNew.TeamId && obj.Floor == objNew.Floor && obj.SalonId == objNew.SalonId) {
                            list.splice(i, 1);
                            i--;
                            check = true;
                            break;
                        }
                    }
                } else {
                    var list = [];
                    list.push(jsonObj);
                    var jsonList = JSON.stringify(list);
                    localStorage.setItem("team-floor", jsonList);
                }
                if (check == false) {
                    list.push(jsonObj);
                }
                return list;
            }

            function updateList(list, jsonObj){
                var objNew = JSON.parse(jsonObj);
                if(!isTeamAdded(objNew.TeamId, list)){
                    list.push(jsonObj);
                }
                return list;
            }

            function updateListTeamColorStatus(){
                var $itemColor = $('.wrap-booking .wr-choose-team .item-color');
                var listTeamAdded = JSON.parse(localStorage.getItem("team-floor"));
                $itemColor.each(function(){
                    if(isTeamAdded($(this).data("team-id"), listTeamAdded)){
                        $(this).addClass("isadded");
                    }
                    else {
                        $(this).removeClass("isadded");   
                    }
                });
            }

            function isTeamAdded(teamId, list){
                var isAdded = false;
                if(list != null && list.length > 0){
                    for (var i = 0; i < list.length; i++) {
                        var obj = JSON.parse(list[i]);
                        if (obj.TeamId == teamId) {
                            isAdded = true;
                            break;
                        }
                    }
                }
                return isAdded;
            }

            function getStaffByOrderCode(This){
                var code = parseInt(This.val());
                var inputFake = This.parent().find(".input-fake-value");
                if (!isNaN(code)) {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/SystemService/Ajax/Suggestion.aspx/Suggestion_OneStaff",
                        data: '{code : "' + code + '", StaffType : ""}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            var mission = JSON.parse(response.d);
                            if (mission.success) {
                                var OBJ = JSON.parse(mission.msg);
                                if (OBJ.length > 0) {
                                    inputFake.text(code + " - " + OBJ[0].Fullname);
                                    inputFake.data("id", OBJ[0].Id);
                                    inputFake.data("name", getMainName(OBJ[0].Fullname.trim()));
                                } else {
                                    inputFake.text("");
                                    inputFake.data("id", "");
                                    inputFake.data("name", "");
                                }
                            } else {
                                inputFake.text("");
                                inputFake.data("id", "");
                                inputFake.data("name", "");
                            }
                            //console.log(inputFake.data("id") + " - " + inputFake.data("name"));
                        },
                        failure: function (response) { alert(response.d); }
                    });
                } else {
                    inputFake.text("");
                    inputFake.data("id", "");
                    inputFake.data("name", "");
                }                
            }

            function getMainName(fullname){
                var list = fullname.split(' ');
                if(list.length > 0){
                    return list[list.length - 1];
                }else{
                    return fullname;
                }
            }

            function addStaffToTeam(This){
                var id = This.parent().find(".input-fake-value").data("id");
                var name = This.parent().find(".input-fake-value").data("name");
                var teamId = This.parent().parent().parent().data("team-id");
                var isUpdate = false;
                var listTeam = [];
                var lsitems = localStorage.getItem("team-floor");

                if(!isNaN(parseInt(id)) && name != "" && lsitems != 'undefined' && lsitems != null){
                    var listTeamAdded = JSON.parse(lsitems);
                    $.each(listTeamAdded, function(i, v){
                        var obj = JSON.parse(v);
                        if (isAddedStaff(id, obj.Staffs) == -1 && obj.TeamId == teamId) {
                            obj.Staffs.push({id : id, name : name}); 
                            // add staff to list view
                            var staffTag = '<div class="tag-staff" onclick="removeStaffFromTeam($(this), '+id+', '+teamId+')"><i class="fa fa-times" aria-hidden="true"></i>'+name+'</div>';
                            This.parent().parent().parent().find(".staff-name").append($(staffTag));
                        }
                        listTeam.push(JSON.stringify(obj));
                    });
                }
                localStorage.setItem("team-floor", JSON.stringify(listTeam));
            }

            function removeStaffFromTeam(This, id, teamId){
                //console.log('remove staff');
                var listTeam = [];
                var lsitems = localStorage.getItem("team-floor");
                if(lsitems != 'undefined' && lsitems != null){
                    var listTeamAdded = JSON.parse(lsitems);
                    $.each(listTeamAdded, function(i, v){
                        var obj = JSON.parse(v);
                        if (obj.TeamId == teamId) {
                            var index = isAddedStaff(id, obj.Staffs);
                            if(index != -1){
                                obj.Staffs.splice(index, 1);
                                This.remove();
                            }
                        }
                        listTeam.push(JSON.stringify(obj));
                    });
                }    
                localStorage.setItem("team-floor", JSON.stringify(listTeam));
            }

            function openAddStaffBox(This){
                This.parent().find(".add-staff-box").show();
            }

            function closeAddStaffBox(This){
                var inputFakeValue = This.parent().find(".input-fake-value");
                inputFakeValue.data("id", "");
                inputFakeValue.data("name", "");
                This.parent().parent().hide();
            }

            function isAddedStaff(staffId, list){
                // struct : [{id : id, name : name}]
                var isAdded = -1;
                if(list != null){
                    for(var i = 0; i<list.length; i++){
                        if(staffId == list[i].id){
                            isAdded = i;
                            break;
                        }
                    }
                }                                                                                        
                return isAdded;
            }

            function getBillPendingOfTeam(salonId, teamId) {
                var list;
                $.ajax({
                    type: 'POST',
                    url: '/GUI/SystemService/Webservice/BookingService.asmx/GetBillPendingOfTeam',
                    data: '{salonId:"' + salonId + '",teamId:"' + teamId + '"}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    async: false,
                    success: function (response) {
                        list = JSON.parse(response.d);
                    }, failure: function (response) { alert(response.d); }
                });
                return list;
            }


            $('.wrap-booking input.input-staff').keypress(function (e) {
                //console.log("here")
                var key = e.which;
                if(key == 13)  // the enter key code
                {
                    //$('input[name = butAssignProd]').click();
                    //return false;  

                    //console.log("enter" +$(this));
                }
            });   


            //---------------- / SET FLOOR FOR TEAM------------------


            function setPending(This, id) {
                var salonId = $('#HDF_SalonId').val();
                $.ajax({
                    type: 'POST',
                    url: '/GUI/SystemService/Webservice/BookingService.asmx/SetBillPending',
                    data: '{id:"' + id + '",salonId:"' + salonId + '"}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var msg = JSON.parse(response.d);
                        if (msg.success) {
                            //location.reload(true);
                            var billCode = msg.msg;
                            window.location.replace("/dich-vu/them-phieu-v2.html?msg_update_status=success&msg_update_message=Th%C3%AAm%20bill%20th%C3%A0nh%20c%C3%B4ng!&msg_print_billcode=" +
                                billCode);
                            This.css("color", "#000");

                        } else {
                            This.css("color", "#d0d0d0");
                            //console.log(msg.msg);
                            showMsgSystem("Đã đầy 4 bill team màu này Pending", "warning");
                        }

                    }, failure: function (response) { alert(response.d); }
                });
            }

            function setBookingToForm(This) {
                var id = $(This).attr("id");
                var team = $(This).find('.team').attr('data-color') + "!important";
                var time = $(This).find('.time').text();
                var $caption = $('.status-team');
                //console.log(id + "|" + team + "|" + time);
                var $colorChoose = $('#TeamColorSelect');
                var $timeChoose = $('.wr-choose-team .time');

                $action.val("-1");

                $teamIdBooking.val(id);
                $colorChoose.attr("style", "background:" + team);
                $timeChoose.text(time);

                $btnFakePending.prop('disabled', true);
                $btnPending.css('opacity', '0.6');

                $btnBook.css('opacity', '1');
                $btnFakeBook.prop('disabled', false);

                $caption.text("");
            }

            $chooseItemColor.bind('click', function () {
                $btnFakePending.prop('disabled', false);
                $btnPending.css('opacity', '1');

                $btnBook.css('opacity', '0.6');
                $btnBook.prop('disabled', true);
                $btnFakeBook.prop('disabled', true);

                $action.val("-2");
            })

            var validate = {service : false, team : false, checkin : false};
            jQuery(document).ready(function () {
                $action.val("0");
                // Add active menu
                $("#glbService").addClass("active");

                // Mở box listing sản phẩm
                $(".show-product").bind("click", function () {
                    $(this).parent().parent().find(".listing-product").show();
                });

                $(".show-item").bind("click", function () {
                    var item = $(this).attr("data-item");
                    var classItem = ".popup-" + item + "-item";

                    $(classItem).openEBPopup();
                    ExcCheckboxItem();
                    ExcQuantity();
                    ExcCompletePopup();

                    $('#EBPopup .listing-product').mCustomScrollbar({
                        theme: "dark-2",
                        scrollInertia: 100
                    });

                    $("#EBPopup .btn-esc").bind("click", function () { autoCloseEBPopup(0); });
                });

                // Btn Send
                $("#BtnSend").bind("click", function () {
                    //console.log("here");
                    // var statusTeam = $('.status-team').text();
                    if ($action.val() != "-1") {

                        if ($("#CustomerName").val() == "" && $("#InputCusNoInfor").is(":checked") == false) {
                            showMsgSystem("Bạn chưa nhập mã khách hàng hoặc chưa tích chọn \"Khách không cho thông tin\".", "warning");
                        } else if ( validate.team == false && $teamColorSelect.attr('data-color') == "") {
                            //console.log("team");
                            // show EBPopup
                            $(".confirm-yn").openEBPopup();
                            $("#EBPopup .confirm-yn-text").text("Bạn chưa chọn đội. Bạn có chắc chắn muốn tiếp tục ?");

                            $("#EBPopup .yn-yes").bind("click", function () {
                                validate.team = true;
                                $("#EBPopup").empty();
                                $("#BtnSend").click();
                            });

                            $("#EBPopup .yn-no").bind("click", function () {
                                autoCloseEBPopup(0);
                            });
                        }
                        else if (validate.service == false && ($("#HDF_ServiceIds").val() == "" || $("#HDF_ServiceIds").val() == "[]")){
                            //console.log(" service ");
                            // show EBPopup
                            $(".confirm-yn").openEBPopup();
                            $("#EBPopup .confirm-yn-text").text("Bạn chưa chọn dịch vụ. Bạn có chắc chắn muốn tiếp tục ?");

                            $("#EBPopup .yn-yes").bind("click", function () {
                                validate.service = true;
                                $("#EBPopup").empty();
                                $("#BtnSend").click();
                            });

                            $("#EBPopup .yn-no").bind("click", function () {
                                autoCloseEBPopup(0);
                            });
                        }
                        else if (validate.checkin == false && ($("#Reception").val() == "")){
                            //console.log(" service ");
                            // show EBPopup
                            $(".confirm-yn").openEBPopup();
                            $("#EBPopup .confirm-yn-text").text("Bạn chưa chọn lễ tân check-in. Bạn có chắc chắn muốn tiếp tục ?");

                            $("#EBPopup .yn-yes").bind("click", function () {
                                validate.checkin = true;
                                $("#EBPopup").empty();
                                $("#BtnSend").click();
                            });

                            $("#EBPopup .yn-no").bind("click", function () {
                                autoCloseEBPopup(0);
                            });
                        } else {
                            autoCloseEBPopup(0);
                            //console.log("go");
                            // Check cosmetic seller
                            if (($("#HDF_ProductIds").val() != "" && $("#HDF_ProductIds").val() != "[]") && $("#Cosmetic").val() == 0) {
                                $("#ValidateCosmetic").css({ "visibility": "visible" });
                            } else {
                                //console.log("check duplicate");
                                $("#ValidateCosmetic").css({ "visibility": "hidden" });
                                var CustomerCode = $("#HDF_CustomerCode").val();
                                // Check duplicate bill                            
                                $.ajax({
                                    type: "POST",
                                    url: "/GUI/FrontEnd/Service/Service_Add.aspx/CheckDuplicateBill",
                                    data: '{CustomerCode : "' + CustomerCode + '"}',
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json", success: function (response) {
                                        var mission = JSON.parse(response.d);
                                        if (mission.success) {
                                            var products = "Sản phẩm : ";
                                            var services = "Dịch vụ : ";
                                            var confirm_content = "";
                                            var bill = JSON.parse(mission.msg);
                                            if (bill.ProductIds != "") {
                                                var _Product = $.parseJSON(bill.ProductIds);
                                                $.each(_Product, function (i, v) {
                                                    products += v.Name + " ( " + v.Price + " ), ";
                                                });
                                                products = products.replace(/(\,\s)+$/, '');
                                            }
                                            if (bill.ServiceIds != "") {
                                                var _Service = $.parseJSON(bill.ServiceIds);
                                                $.each(_Service, function (i, v) {
                                                    services += v.Name + " ( " + v.Price + " ), ";
                                                });
                                                services = services.replace(/(\,\s)+$/, '');
                                            }
                                            confirm_content += products + ", " + services + ", ";
                                            confirm_content = confirm_content.replace(/(\,\s)+$/, '');

                                            // show EBPopup
                                            $(".confirm-yn").openEBPopup();
                                            $("#EBPopup .confirm-yn-text").text("Khách hàng mã [" + CustomerCode + "] đã được lập hóa đơn trong hôm nay [ " + confirm_content + " ]. Bạn có chắc muốn thêm hóa đơn mới ?");

                                            $("#EBPopup .yn-yes").bind("click", function () {
                                                //Bind_UserImagesToHDF();
                                                addLoading();
                                                //console.log("BtnFakeSend click 1");
                                                $("#BtnFakeSend").click();
                                            });

                                            $("#EBPopup .yn-no").bind("click", function () {
                                                autoCloseEBPopup(0);
                                            });
                                        } else {
                                            //Bind_UserImagesToHDF();
                                            addLoading();
                                            //console.log("BtnFakeSend click 2");
                                            $("#BtnFakeSend").click();
                                        }
                                    },
                                    failure: function (response) { alert(response.d); }
                                });
                                //Bind_UserImagesToHDF();
                                //$("#BtnFakeSend").click();
                            }
                        }
                    }
                });

                // Btn Booking
                $("#BtnBook").bind("click", function () {

                    if ($action.val() != "-2") {

                        // console.log("here");
                        if ($("#CustomerName").val() == "" && $("#InputCusNoInfor").is(":checked") == false) {
                            showMsgSystem("Bạn chưa nhập mã khách hàng hoặc chưa tích chọn \"Khách không cho thông tin\".", "warning");
                            //} else if ($("#HDF_TotalMoney").val() == "") {
                            //    showMsgSystem("Bạn chưa chọn dịch vụ hoặc sản phẩm.", "warning");
                        } else {
                            var teamColor = $('#TeamColorSelect').attr('style');

                            if (teamColor != null && teamColor != "")
                                $("#BtnFakeBook").click();
                            else
                                showMsgSystem("Bạn không thể đặt lịch cho khách khi chưa chọn Team ", "warning");
                        }
                    }
                });

                // Bind staff suggestion
                Bind_Suggestion();
                AutoSelectStaff();

                /// Mở luôn iframe thêm mới khách hàng khi vào trang
                quickAddCustomer();
                $("#CustomerCode").focus();

                //============================
                // Show System Message
                //============================ 
                var qs = getQueryStrings();
                showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);

                // Enter don't submit
                $(window).on('keyup keypress', function (e) {
                    var keyCode = e.keyCode || e.which;
                    if (keyCode === 13) {
                        e.preventDefault();
                        return false;
                    }
                });

                /// click out of team color
                $('html').click(function () {
                    $(".team-color-edit-box").hide();
                });
                $('.pending-team-color').click(function (event) {
                    event.stopPropagation();
                });

                // Sync bill
                var requestLoadBillStatus = setInterval(loadBillStatus, 3000);

            });

            // Xử lý mã khách hàng => bind khách hàng
            function LoadCustomer(code) {
                ajaxGetCustomer(code);
            }

            // Xử lý khi chọn checkbox sản phẩm
            function ExcCheckboxItem() {
                $("#EBPopup .td-product-checkbox input[type='checkbox']").unbind("click").bind("click", function () {
                    var obj = $(this).parent().parent(),
                        boxMoney = obj.find(".box-money"),
                        price = obj.find(".td-product-price").data("price"),
                        quantity = obj.find("input.product-quantity").val(),
                        voucher = obj.find("input.product-voucher").val(),
                        checked = $(this).is(":checked"),
                        item = obj.attr("data-item"),
                        promotion = 0;
                    obj.find(".promotion-money input[type='checkbox']:checked").each(function () {
                        var value = $(this).attr("data-value").trim();
                        promotion += (value != "" ? parseInt(value) : 0);
                    });

                    // check value
                    price = price.toString().trim() != "" ? parseInt(price) : 0;
                    quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
                    voucher = voucher.trim() != "" ? parseInt(voucher) : 0;


                    if (checked) {
                        boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100 - promotion)).show();
                    } else {
                        boxMoney.text("").hide();
                    }

                });
            }

            // Số lượng | Quantity
            function ExcQuantity() {
                $("input.product-quantity").bind("change", function () {
                    var obj = $(this).parent().parent(),
                        quantity = $(this).val(),
                        price = obj.find(".td-product-price").data("price"),
                        voucher = obj.find("input.product-voucher").val(),
                        boxMoney = obj.find(".box-money"),
                        promotion = 0;

                    obj.find(".promotion-money input[type='checkbox']:checked").each(function () {
                        var value = $(this).attr("data-value").trim();
                        promotion += (value != "" ? parseInt(value) : 0);
                    });

                    // check value
                    price = price.toString().trim() != "" ? parseInt(price) : 0;
                    quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
                    voucher = voucher.trim() != "" ? parseInt(voucher) : 0;

                    boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100 - promotion)).show();
                    TotalMoney();
                    getProductIds();
                    getServiceIds();
                });
                $("input.product-voucher").bind("change", function () {
                    var obj = $(this).parent().parent().parent(),
                        quantity = obj.find("input.product-quantity").val(),
                        price = obj.find(".td-product-price").data("price"),
                        voucher = obj.find("input.product-voucher").val(),
                        boxMoney = obj.find(".box-money"),
                        promotion = 0;

                    obj.find(".promotion-money input[type='checkbox']:checked").each(function () {
                        var value = $(this).attr("data-value").trim();
                        promotion += (value != "" ? parseInt(value) : 0);
                    });

                    // check value
                    price = price.toString().trim() != "" ? parseInt(price) : 0;
                    quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
                    voucher = voucher.trim() != "" ? parseInt(voucher) : 0;

                    if (promotion == 0) {
                        boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100)).show();
                    } else {
                        obj.find("input.product-voucher").val(0);
                    }

                    TotalMoney();
                    getProductIds();
                    getServiceIds();
                });
                $("input.item-promotion").bind("click", function () {
                    var obj = $(this).parent().parent().parent().parent().parent(),
                        quantity = obj.find("input.product-quantity").val(),
                        price = obj.find(".td-product-price").data("price"),
                        voucher = obj.find("input.product-voucher").val(),
                        boxMoney = obj.find(".box-money"),
                        promotion = $(this).attr("data-value"),
                        _This = $(this),
                        isChecked = $(this).is(":checked");
                    obj.find(".promotion-money input[type='checkbox']:checked").prop("checked", false);
                    if (isChecked) {
                        $(this).prop("checked", true);
                        promotion = promotion.trim() != "" ? parseInt(promotion) : 0;
                        obj.find("input.product-voucher").val(0);
                    } else {
                        promotion = 0;
                    }

                    // check value
                    price = price.toString().trim() != "" ? parseInt(price) : 0;
                    quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
                    voucher = voucher.trim() != "" ? parseInt(voucher) : 0;
                    //promotion = promotion.trim() != "" ? parseInt(promotion) : 0;

                    if (promotion > 0) {
                        boxMoney.text(FormatPrice(quantity * price - promotion)).show();
                        obj.find("input.product-voucher").css({ "text-decoration": "line-through" });
                    } else {
                        boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100)).show();
                        obj.find("input.product-voucher").css({ "text-decoration": "none" });
                    }
                    TotalMoney();
                    getProductIds();
                    getServiceIds();
                });
            }

            // Xử lý click hoàn tất chọn item từ popup
            function ExcCompletePopup() {
                $("#EBPopup .btn-complete").unbind("click").bind("click", function () {
                    var item = $(this).attr("data-item"),
                        tableItem = $("table.table-item-" + item + " tbody"),
                        objTmp;

                    $("#EBPopup .item-product-checkbox input[type='checkbox']:checked").each(function () {
                        var Code = $(this).attr("data-code");
                        if (!ExcCheckItemIsChose(Code, tableItem)) {
                            objTmp = $(this).parent().parent().clone().find("td:first-child").remove().end();
                            tableItem.append(objTmp);
                        }
                    });

                    TotalMoney();
                    getProductIds();
                    getServiceIds();
                    ExcQuantity();
                    UpdateItemOrder(tableItem);
                    autoCloseEBPopup(0);
                });
            }

            // Check item đã được chọn
            function ExcCheckItemIsChose(Code, itemClass) {
                var result = false;
                $(itemClass).find(".td-product-code").each(function () {
                    var _Code = $(this).text().trim();
                    if (_Code == Code)
                        result = true;
                });
                return result;
            }

            // Remove item đã được chọn
            function RemoveItem(THIS, name, itemName) {
                // Confirm
                $(".confirm-yn").openEBPopup();

                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn hủy [ " + name + " ] ?");
                $("#EBPopup .yn-yes").bind("click", function () {
                    var Code = THIS.find(".td-product-code").attr("data-code");
                    $("#quick-" + itemName).find("input[data-code='" + Code + "']").removeAttr("checked");
                    THIS.remove();
                    TotalMoney();
                    getProductIds();
                    getServiceIds();
                    UpdateItemDisplay(itemName);
                    autoCloseEBPopup(0);
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
            }

            // Update table display
            function UpdateItemDisplay(itemName) {
                var dom = $(".table-item-" + itemName);
                var len = dom.find("tbody tr").length;
                if (len == 0) {
                    dom.parent().hide();
                } else {
                    UpdateItemOrder(dom.find("tbody"));
                }
            }

            // Update order item
            function UpdateItemOrder(dom) {
                var index = 1;
                dom.find("tr").each(function () {
                    $(this).find("td.td-product-index").text(index);
                    index++;
                });
            }

            function TotalMoney() {
                var Money = 0;
                $(".fe-service-table-add .box-money:visible").each(function () {
                    Money += parseInt($(this).text().replace(/\./gm, ""));
                });
                $("#TotalMoney").val(FormatPrice(Money));
                $("#HDF_TotalMoney").val(Money);
            }

            function showMsgSystem(msg, status) {
                $("#MsgSystem").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);
                setTimeout(function () {
                    $("#MsgSystem").fadeTo("slow", 0, function () {
                        $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 1);
                    });
                }, 5000);
            }

            function getProductIds() {
                var Ids = [];
                var prd = {};
                $("table.table-item-product tbody tr").each(function () {
                    prd = {};
                    var THIS = $(this);

                    var Id = THIS.find("td.td-product-code").attr("data-id"),
                        Code = THIS.find("td.td-product-code").text().trim(),
                        Name = THIS.find("td.td-product-name").text().trim(),
                        Price = THIS.find(".td-product-price").attr("data-price"),
                        Quantity = THIS.find("input.product-quantity").val(),
                        VoucherPercent = THIS.find("input.product-voucher").val(),
                        Promotion = 0;

                    THIS.find(".promotion-money input[type='checkbox']:checked").each(function () {
                        var value = $(this).attr("data-value").trim();
                        Promotion += (value != "" ? parseInt(value) : 0);
                    });

                    // check value
                    Id = Id.toString().trim() != "" ? parseInt(Id) : 0;
                    Price = Price.toString().trim() != "" ? parseInt(Price) : 0;
                    Quantity = Quantity.trim() != "" ? parseInt(Quantity) : 0;
                    VoucherPercent = VoucherPercent.trim() != "" ? parseInt(VoucherPercent) : 0;

                    prd.Id = Id;
                    prd.Code = Code;
                    prd.Name = Name;
                    prd.Price = Price;
                    prd.Quantity = Quantity;
                    prd.VoucherPercent = VoucherPercent;
                    prd.Promotion = Promotion;

                    Ids.push(prd);

                });
                Ids = JSON.stringify(Ids);
                $("#HDF_ProductIds").val(Ids);
            }

            function getServiceIds() {
                var Ids = [];
                var prd = {};
                $("table.table-item-service tbody tr").each(function () {
                    prd = {};
                    var THIS = $(this),
                        obj = THIS.parent().parent();

                    var Id = THIS.find("td.td-product-code").attr("data-id"),
                        Code = THIS.find("td.td-product-code").text().trim(),
                        Name = THIS.find("td.td-product-name").text().trim(),
                        Price = THIS.find(".td-product-price").attr("data-price"),
                        Quantity = THIS.find("input.product-quantity").val(),
                        VoucherPercent = THIS.find("input.product-voucher").val();

                    // check value
                    Id = Id.toString().trim() != "" ? parseInt(Id) : 0;
                    Price = Price.toString().trim() != "" ? parseInt(Price) : 0;
                    Quantity = Quantity.trim() != "" ? parseInt(Quantity) : 0;
                    VoucherPercent = VoucherPercent.trim() != "" ? parseInt(VoucherPercent) : 0;

                    prd.Id = Id;
                    prd.Code = Code;
                    prd.Name = Name;
                    prd.Price = Price;
                    prd.Quantity = Quantity;
                    prd.VoucherPercent = VoucherPercent;

                    Ids.push(prd);

                });
                Ids = JSON.stringify(Ids);
                $("#HDF_ServiceIds").val(Ids);
            }

            // Get Customer
            function ajaxGetCustomer(CustomerCode) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/Service/Service_Add.aspx/GetCustomer",
                    data: '{CustomerCode : "' + CustomerCode + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            var customer = JSON.parse(mission.msg);
                            $("#CustomerName").val(customer.Fullname);
                            $("#CustomerName").attr("data-code", customer.Customer_Code);
                            $("#HDF_CustomerCode").val(customer.Customer_Code);
                            $("#HDF_Suggestion_Code").val(customer.Customer_Code);
                        } else {
                            $("#CustomerName").val("");
                            $("#CustomerName").attr("data-code", "");
                            $("#HDF_CustomerCode").val("");
                            $("#HDF_Suggestion_Code").val("");
                            //var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                            //showMsgSystem(msg, "warning");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function pushQuickData(This, typeData) {
                var Code = This.attr("data-code");
                if (This.is(":checked")) {
                    var Dom = $("#table-" + typeData).find("input[data-code='" + Code + "']").parent().parent().clone().find("td:first-child").remove().end(),
                    quantity = Dom.find(".product-quantity").val(),
                    price = Dom.find(".td-product-price").data("price"),
                    voucher = Dom.find("input.product-voucher").val(),
                    promotion = 0;

                    Dom.find(".promotion-money input[type='checkbox']:checked").each(function () {
                        var value = $(this).attr("data-value").trim();
                        promotion += (value != "" ? parseInt(value) : 0);
                    });

                    // check value
                    price = price.toString().trim() != "" ? parseInt(price) : 0;
                    quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
                    voucher = voucher.trim() != "" ? parseInt(voucher) : 0;

                    Dom.find(".box-money").text(FormatPrice(quantity * price * (100 - voucher) / 100 - promotion)).show();
                    $("#table-item-" + typeData).append(Dom);
                    $(".item-" + typeData).show();
                } else {
                    $("#table-item-" + typeData).find(".td-product-code[data-code='" + Code + "']").parent().remove();
                }

                TotalMoney();
                getProductIds();
                getServiceIds();
                ExcQuantity();
                UpdateItemOrder($("#table-item-" + typeData).find("tbody"));
            }

            function pushFreeService(This, id) {
                var arr = [];
                var sv = {};
                $(".free-service input[type='checkbox']").each(function () {
                    if ($(this).is(":checked")) {
                        arr.push(parseInt($(this).attr('data-id')));
                    }
                });
                $("#HDF_FreeService").val(JSON.stringify(arr));
            }

            function excPromotion(This, money) {
                //promotion-money
                //if (This.is(":checked")) {
                //    $(".fe-service .promotion-money input[type='checkbox']").prop("checked", false);
                //} else {
                //    $(".fe-service .promotion-money input[type='checkbox']").prop("checked", false);
                //    This.prop("checked", true);
                //}

                //TotalMoney();
                //getProductIds();
                //getServiceIds();
                //ExcQuantity();
                //UpdateItemOrder($("#table-item-" + typeData).find("tbody"));
            }

            //============================
            // Suggestion Functions
            //============================
            function Bind_Suggestion() {
                $(".eb-suggestion").bind("keyup", function (e) {
                    if (e.keyCode == 40) {
                        UpDownListSuggest($(this));
                    } else {
                        Call_Suggestion($(this));
                    }
                });
                $(".eb-suggestion").bind("focus", function () {
                    Call_Suggestion($(this));
                });
                $(".eb-suggestion").bind("blur", function () {
                    //Exc_To_Reset_Suggestion($(this));
                });
                $(window).bind("click", function (e) {
                    if ((!e.target.className.match("eb-select") && e.target.parentElement.className.match("ul-listing-suggestion")) ||
                        (!e.target.className.match("eb-select") && !e.target.parentElement.className.match("ul-listing-suggestion"))) {
                        //EBSelect_HideBox();
                    }
                });
            }

            function UpDownListSuggest(This) {
                var UlSgt = This.parent().find(".ul-listing-suggestion"),
                    index = 0,
                    LisLen = UlSgt.find(">li").length - 1,
                    Value;

                This.blur();
                UlSgt.find(">li.active").removeClass("active");
                UlSgt.find(">li:eq(" + index + ")").addClass("active");

                $(window).unbind("keydown").bind("keydown", function (e) {
                    if (e.keyCode == 40) {
                        if (index == LisLen) return false;
                        UlSgt.find(">li.active").removeClass("active");
                        UlSgt.find(">li:eq(" + (++index) + ")").addClass("active");
                        return false;
                    } else if (e.keyCode == 38) {
                        if (index == 0) return false;
                        UlSgt.find(">li.active").removeClass("active");
                        UlSgt.find(">li:eq(" + (--index) + ")").addClass("active");
                        return false;
                    } else if (e.keyCode == 13) {
                        // Bind data to HDF Field
                        var THIS = UlSgt.find(">li.active");
                        //var Value = THIS.text().trim();
                        var Value = THIS.attr("data-code");
                        var dataField = This.attr("data-field");

                        BindIdToHDF(THIS, Value, dataField, "#HDF_Suggestion_Code", "#HDF_Suggestion_Field", This);
                        EBSelect_HideBox();
                    }
                });
            }

            function Exc_To_Reset_Suggestion(This) {
                var value = This.val();
                if (value == "") {
                    $(".eb-suggestion").each(function () {
                        var THIS = $(this);
                        var sgValue = THIS.val();
                        if (sgValue != "") {
                            BindIdToHDF(THIS, sgValue, THIS.attr("data-field"), "#HDF_Suggestion_Code", "#HDF_Suggestion_Field", THIS);
                            return false;
                        }
                    });
                }
            }

            function Call_Suggestion(This) {
                var text = This.val(),
                    field = This.attr("data-field");
                Suggestion(This, text, field);
            }

            function Suggestion(This, text, field) {
                var This = This;
                var text = text || "";
                var field = field || "";
                var InputDomId;
                var HDF_Sgt_Code = "#HDF_Suggestion_Code";
                var HDF_Sgt_Field = "#HDF_Suggestion_Field";
                var Callback = This.attr("data-callback");

                if (text == "") return false;

                switch (field) {
                    case "customer.name": InputDomId = "#CustomerName"; break;
                    case "customer.phone": InputDomId = "#CustomerPhone"; break;
                    case "customer.code": InputDomId = "#CustomerCode"; break;
                    case "bill.code": InputDomId = "#BillCode"; break;
                }

                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Ajax/Suggestion.aspx/Suggestion_Customer",
                    data: '{field : "' + field + '", text : "' + text + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            var OBJ = JSON.parse(mission.msg);
                            if (OBJ != null && OBJ.length > 0) {
                                var lis = "";
                                $.each(OBJ, function (i, v) {
                                    lis += "<li data-code='" + v.Customer_Code + "'" +
                                                 "onclick=\"BindIdToHDF($(this),'" + v.Customer_Code + "','" + field + "','" + HDF_Sgt_Code +
                                                 "','" + HDF_Sgt_Field + "','" + InputDomId + "')\" data-callback='" + Callback + "'>" +
                                                v.Value +
                                            "</li>";
                                });
                                This.parent().find("ul.ul-listing-suggestion").empty().append(lis).parent().show();
                                This.parent().find(".eb-select-data").show();
                            } else {
                                This.parent().find(".eb-select-data").hide().find("ul.ul-listing-suggestion").empty();
                            }
                        } else {
                            This.parent().find("ul.ul-listing-suggestion").empty();
                            var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                            showMsgSystem(msg, "warning");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function BindIdToHDF(THIS, Code, Field, HDF_Sgt_Code, HDF_Sgt_Field, Input_DomId) {
                var text = THIS.text().trim();
                $("input.eb-suggestion").val("");
                $(HDF_Sgt_Code).val(Code);
                $(HDF_Sgt_Field).val(Field);
                $(Input_DomId).val(text);
                $(Input_DomId).parent().find(".eb-select-data").hide();
                if (THIS.attr("data-callback") != "") {
                    window[THIS.attr("data-callback")].call();
                }
                // Auto post server
                //$("#BtnFakeUP").click();
            }

            function EBSelect_HideBox() {
                $(".eb-select-data").hide();
                $("ul.ul-listing-suggestion li.active").removeClass("active");
            }

            function Callback_UpdateCustomerCode() {
                LoadCustomer($("#HDF_Suggestion_Code").val());
            }

            function CustomerInputOnchange(This) {
                ajaxGetCustomer(This.val());
            }

            //===================
            // Auto select staff
            //===================
            function AutoSelectStaff() {
                $(".auto-select").bind("keyup", function (e) {
                    var This = $(this);
                    var StaffType = This.attr("data-field");
                    var code = parseInt(This.val());
                    if (!isNaN(code)) {
                        $.ajax({
                            type: "POST",
                            url: "/GUI/SystemService/Ajax/Suggestion.aspx/Suggestion_OneStaff",
                            data: '{code : "' + code + '", SalonId : ' + $("#HDF_SalonId").val() + ', StaffType : "' + StaffType + '"}',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json", success: function (response) {
                                var mission = JSON.parse(response.d);
                                if (mission.success) {
                                    var OBJ = JSON.parse(mission.msg);
                                    if (OBJ.length > 0) {
                                        var lis = "";
                                        This.parent().find(".fake-value").text(This.val() + "- " + OBJ[0].Fullname);
                                        $("#" + StaffType).val(OBJ[0].Id);
                                    } else {
                                        This.parent().find(".fake-value").text("");
                                        $("#" + StaffType).val("");
                                    }
                                } else {
                                    This.parent().find("ul.ul-listing-suggestion").empty();
                                    var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                                    showMsgSystem(msg, "warning");
                                }
                            },
                            failure: function (response) { alert(response.d); }
                        });
                    } else {
                        This.parent().find(".fake-value").text("");
                        $("#" + StaffType).val("");
                    }
                });
            }

            function quickAddCustomer(This) {
                var display = $("#TrIframeAddCustomer").css("display");
                if (display == "none") {
                    $("#TrIframeAddCustomer").show();
                }
                $("#IframeAddCustomer").toggle(0, function () {
                    if (display == "table-row") {
                        $("#TrIframeAddCustomer").hide();
                    }
                });
            }

            window.callback_QuickAddCustomer = function (id, code, name) {
                $("#CustomerCode").val(code);
                $("#CustomerName").val(name);
                $("#HDF_CustomerCode").val(code);
                $("#HDF_Suggestion_Code").val(code);
            }

            window.callback_HideIframeCustomer = function () {
                //$("#IframeAddCustomer").toggle(0, function () {
                //    $("#TrIframeAddCustomer").hide();
                //});
            }

            window.callback_Iframe_SetStyle = function (style) {
                $("body,html").append($(style));
            }

            /// Chọn team
            function setTeamColor(This, idTeam, color) {
                $(".item-color.active").removeClass("active");
                This.addClass("active");
                $("#HDF_TeamId").val(idTeam);
                // bind color to .team-color-select
                $(".team-color-select").css({ "background": color });
                $('#BtnBook').prop('disabled', false);
                $('#BtnBook').css('opacity', '0.6');
                $('#TeamColorSelect').attr('data-color', idTeam);
                //checkPendingMax(idTeam);
            }

            function checkPendingMax(teamId) {
                var $btnPending = $('#BtnSend');
                var $btnFakePending = $('#BtnFakeSend');
                var $caption = $('.fe-service-table-add .tr-send .status-team');
                var $time = $('.wr-choose-team .time');
                var salonId = $('#HDF_SalonId').val();
                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Webservice/BookingService.asmx/CheckPendingMax",
                    data: '{teamId : "' + teamId + '",salonId:"' + salonId + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        check = response.d;
                        //console.log("ckeck " + check);
                        if (check) {
                            //console.log("bill max")
                            $btnFakePending.prop('disabled', true);

                            $btnPending.css('opacity', '0.6');
                            $caption.text("Đã đầy 4 bill team màu này Pending");
                            showMsgSystem("Đã đầy 4 bill team màu này Pending", "warning");
                            $action.val("-1");
                        } else {
                            //console.log("team available")
                            $btnFakePending.prop('disabled', false);
                            $btnPending.css('opacity', '1');
                            $caption.text("");
                            $time.text("");
                            $action.val("-2");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });

            }

            /// Update team color
            function showBoxColorEdit(This) {
                $(".pending-team-color.active").removeClass("active");
                if (This.hasClass("active")) {
                    This.removeClass("active");
                } else {
                    This.addClass("active");
                }
                $(".pending-team-color").each(function () {
                    if (!$(this).hasClass("active")) {
                        $(this).parent().find(".team-color-edit-box").hide();
                    }
                });
                This.parent().find(".team-color-edit-box").toggle();
            }
            function updateTeamColor(This, teamId, color, billId) {
                This.parent().parent().find(".pending-team-color").css("cssText", "background:" + color + "!important");
                // update teamId
                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Ajax/Suggestion.aspx/Update_BillTeamId",
                    data: '{billId : ' + billId + ', teamId : ' + teamId + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            //
                            $(".team-color-edit-box").hide();
                        } else {
                            delFailed();
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            //====================
            // Sync bill status
            //====================
            var tableBooking = $("table#TableBooking");
            var tbodyBooking = $("table#TableBooking tbody");
            function loadBillStatus() {
                // 1. Lấy danh sách bill Id
                // 2. Request lấy trạng thái của các bill này
                // 3. Từ kết quả response về, bind lại trạng thái vào các bill
                var ids = [];
                var id;
                tbodyBooking.find(">tr").each(function () {
                    id = $(this).attr("id");
                    ids.push(id);
                });
                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Webservice/BookingService.asmx/getBillStatus",
                    data: '{ids : \'' + JSON.stringify(ids) + '\'}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        var statusBlock;
                        var tdStatusBlock;
                        if (mission.success) {
                            var listStatus = JSON.parse(mission.msg);
                            //console.log(mission.msg);
                            $.each(listStatus, function (i, v) {
                                tdStatusBlock = tbodyBooking.find(">tr[id='" + v.Id + "'] .td-bill-status");
                                if (v.StatusValue > 0) {
                                    statusBlock = '<span class="status-value ' + v.StatusClass + '">' + v.StatusText + '</span><span class="status-time">' + v.Time + '</span>';
                                    tdStatusBlock.empty().append($(statusBlock));
                                    if (v.StatusValue >= 4) {
                                        var tr = tdStatusBlock.parent();
                                        setTimeout(function () {
                                            tr.remove();
                                        }, 1000);
                                    }
                                }
                            });
                        } else {
                            //delFailed();
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            //====================
            // Sync bill booking
            //====================
            var loadBillBooking = function(){
                var salonId = $("#HDF_SalonId").val();
                console.log(salonId);
                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Webservice/BookingService.asmx/loadBillFromBooking",
                    data: '{salonId : "'+salonId+'"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            var list = JSON.parse(mission.msg);
                            var trClass="", checked = "";
                            var table = "<table><thead><tr><th>ID</th><th>Khung giờ hẹn</th><th>Khách hàng</th><th>Số ĐT</th><th>Đã đến</th></tr></thead>";
                            if(list.length > 0){
                                $.each(list, function(i, v){
                                    var trClass="", checked = "";
                                    if(v.IsMakeBill == true){
                                        trClass = "class='is-makebill'";
                                        checked = "checked='checked'";
                                    }
                                    table += "<tr "+trClass+" id='a'>" +
                                                "<td style='text-align:center;'>"+v.Id+"</td>" +
                                                "<td style='text-align:center;'>"+v.Hour+"</td>" +
                                                "<td onclick='copyContent(this)'>"+v.CustomerName+"</td>" +
                                                "<td onclick='copyContent(this)'>"+v.CustomerPhone+"</td>" +
                                                "<td style='text-align:center;'><input type='checkbox' onclick='updateBookingStatus($(this), "+v.Id+")' "+checked+" /></td>" +
                                            "</tr>";
                                });                                
                            }
                            table += "</table>";
                            $(".booking-listing").empty().append($(table));
                        } else {
                            //delFailed();
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function updateBookingStatus(This, id){
                var status;
                if(This.is(":checked")){
                    status = true;
                    This.prop("checked", true);
                    This.parent().parent().addClass("is-makebill");                    
                }else{
                    status = false;
                    This.prop("checked", false);
                    This.parent().parent().removeClass("is-makebill");
                }
                // Cập nhật trạng thái IsMakebill - đã check và tạo bill từ lịch đặt
                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Webservice/BookingService.asmx/updateStatusForBooking",
                    data: '{id : "'+id+'", status : "'+status+'"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            //
                        } else {
                            //delFailed();
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function toggleListBooking(){
                $(".wp-booking").toggle();
            }

            function loadBillBookingBySalon(This){
                $("#HDF_SalonId").val(This.val());
                clearInterval(autoLoadBooking);
                loadBillBooking();
                autoLoadBooking = setInterval(loadBillBooking, 10000);                
            }

            //document.getElementById("copyButton").addEventListener("click", function() {
            //    copyToClipboard(document.getElementById("copyTarget"));
            //});

            function copyContent(This){
                $(".booking-listing table td").css({"background" : "none"});
                This.style.background = "#2771C3";
                copyToClipboard(This);
            }

            function copyToClipboard(elem) {
                // create hidden text element, if it doesn't already exist
                var targetId = "_hiddenCopyText_";
                var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
                var origSelectionStart, origSelectionEnd;
                if (isInput) {
                    // can just use the original source element for the selection and copy
                    target = elem;
                    origSelectionStart = elem.selectionStart;
                    origSelectionEnd = elem.selectionEnd;
                } else {
                    // must use a temporary form element for the selection and copy
                    target = document.getElementById(targetId);
                    if (!target) {
                        var target = document.createElement("textarea");
                        target.style.position = "absolute";
                        target.style.left = "-9999px";
                        target.style.top = "0";
                        target.id = targetId;
                        document.body.appendChild(target);
                    }
                    target.textContent = elem.textContent;
                }
                // select the content
                var currentFocus = document.activeElement;
                target.focus();
                target.setSelectionRange(0, target.value.length);
    
                // copy the selection
                var succeed;
                try {
                    succeed = document.execCommand("copy");
                } catch(e) {
                    succeed = false;
                }
                // restore original focus
                if (currentFocus && typeof currentFocus.focus === "function") {
                    currentFocus.focus();
                }
    
                if (isInput) {
                    // restore prior selection
                    elem.setSelectionRange(origSelectionStart, origSelectionEnd);
                } else {
                    // clear temporary content
                    target.textContent = "";
                }
                return succeed;
            }

            function SelectText(element) {
                //var text = document.getElementById(element);
                if ($.browser.msie) {
                    var range = document.body.createTextRange();
                    range.moveToElementText(element);
                    range.select();
                } else if ($.browser.mozilla || $.browser.opera) {
                    var selection = window.getSelection();
                    var range = document.createRange();
                    range.selectNodeContents(element);
                    selection.removeAllRanges();
                    selection.addRange(range);
                } else if ($.browser.safari) {
                    var selection = window.getSelection();
                    selection.setBaseAndExtent(element, 0, element, 1);
                }
            }

            var autoLoadBooking;
            jQuery(document).ready(function($){
                loadBillBooking();
                autoLoadBooking = setInterval(loadBillBooking, 10000);
                /// click out of wp-booking
                $('html').click(function () {
                    $(".wp-booking").hide();
                });
                $('.open-booking-list, .wp-booking').click(function (event) {
                    event.stopPropagation();
                });
            });

        </script>

        <!-- In hóa đơn -->
        <iframe id="iframePrint" style="display: none;"></iframe>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                var qs = getQueryStrings();
                if (qs["msg_print_billcode"] != undefined) {
                    openPdfIframe("/Public/PDF/" + qs["msg_print_billcode"] + ".pdf");
                }
                showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);
            });
            function openPdfIframe(src) {
                var PDF = document.getElementById("iframePrint");
                PDF.src = src;
                PDF.onload = function () {
                    PDF.focus();
                    PDF.contentWindow.print();
                    PDF.contentWindow.close();
                }
            }
            //kiem tra thong bao cuoc hen 
            //var salonId = $('#HDF_SalonId').val();
            //setInterval(function () {
            //    var addTime = 15;
            //    checkAppointmentTime(salonId, addTime);
            //}, 500);
        </script>
        <!--/ In hóa đơn -->
    </asp:Panel>
</asp:Content>
