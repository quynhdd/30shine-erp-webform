﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;

namespace _30shine.GUI.UIService
{
    public partial class Service_Edit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind_Staff_Hairdresser();
                Bind_Staff_HairMassage();
                Bind_Service();
                Bind_RptProduct();                
            }

        }

        protected void AddService(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var obj = new BillService();
                obj.CustomerCode = HDF_CustomerCode.Value;
                obj.ServiceId = Convert.ToInt32(ServiceName.SelectedValue);
                obj.Staff_Hairdresser_Id = Convert.ToInt32(Hairdresser.SelectedValue);
                obj.Staff_HairMassage_Id = Convert.ToInt32(HairMassage.SelectedValue);
                obj.ProductIds = HDF_ProductIds.Value;
                obj.TotalMoney = Convert.ToInt32(TotalMoney.Text);
                obj.CreatedDate = DateTime.Now;

                // Validate
                // Check trùng mã dịch vụ
                //var check = db.BillServices.Where(w => w.Customer_Code == obj.Customer_Code).ToList();
                var Error = false;
                /*if (check.Count > 0)
                {
                    MsgSystem.Text = "Mã khách hàng đã tồn tại. Bạn vui lòng nhập mã khách hàng khác.";
                    MsgSystem.CssClass = "msg-system warning";
                    Error = true;

                }*/

                if (!Error)
                {
                    db.BillServices.Add(obj);
                    db.SaveChanges();

                    MsgSystem.Text = "Cập nhật thành công!";
                    MsgSystem.CssClass = "msg-system success";
                }
            }
        }

        private void Bind_RptProduct()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Products = db.Products.OrderByDescending(o => o.Id).ToList();

                Rpt_Product.DataSource = _Products;
                Rpt_Product.DataBind();
            }
        }

        private void Bind_Service()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Services = db.Services.OrderBy(o => o.Price).ToList();
                var Key = 0;

                ServiceName.DataTextField = "ServiceName";
                ServiceName.DataValueField = "Id";
                ListItem item = new ListItem("Chọn dịch vụ", "0");
                ServiceName.Items.Insert(Key, item);

                foreach (var v in _Services)
                {
                    Key++;
                    item = new ListItem(v.Name, v.Id.ToString());
                    item.Attributes.Add("data-price", v.Price.ToString());
                    item.Attributes.Add("data-code", v.Code.ToString());
                    ServiceName.Items.Insert(Key, item);
                }

            }
        }

        private void Bind_Staff_Hairdresser()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Hairdresser = db.Staffs.Where(w => w.Type == 1).ToList();
                var Key = 0;

                Hairdresser.DataTextField = "Hairdresser";
                Hairdresser.DataValueField = "Id";
                ListItem item = new ListItem("Chọn nhân viên cắt tóc", "0");
                Hairdresser.Items.Insert(Key, item);

                foreach (var v in _Hairdresser)
                {
                    Key++;
                    item = new ListItem(v.Fullname, v.Id.ToString());
                    item.Attributes.Add("data-code", v.Code);
                    Hairdresser.Items.Insert(Key, item);
                }
            }
        }

        private void Bind_Staff_HairMassage()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _HairMassage = db.Staffs.Where(w => w.Type == 2).ToList();
                var Key = 0;

                HairMassage.DataTextField = "HairMassage";
                HairMassage.DataValueField = "Id";
                ListItem item = new ListItem("Chọn nhân viên gội đầu", "0");
                HairMassage.Items.Insert(Key, item);

                foreach (var v in _HairMassage)
                {
                    Key++;
                    item = new ListItem(v.Fullname, v.Id.ToString());
                    item.Attributes.Add("data-code", v.Code);
                    HairMassage.Items.Insert(Key, item);
                }
            }
        }

        [WebMethod]
        public static string GetCustomer(string CustomerCode)
        {
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var _Customer = db.Customers.Where(w => w.Customer_Code == CustomerCode).ToList();

                if (_Customer.Count > 0)
                {
                    Msg.success = true;                    
                    Msg.msg = serializer.Serialize(_Customer);
                }
                else 
                {
                    Msg.success = false;   
                }
            }
            return serializer.Serialize(Msg);
        }
    }

}