﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Data.Entity.Migrations;
using System.Web.Services;
using _30shine.MODEL.BO;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.FrontEnd.Service
{
    public partial class ServiceBadRatingList : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected int SalonId;
        protected Tbl_Salon Salon;
        protected List<TeamService> lstTeamService = new List<TeamService>();
        protected Solution_30shineEntities db;
        private static ServiceBadRatingList _Instance;

        public ServiceBadRatingList()
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            CheckPermission();
            /// khởi tạo store lương cho nhân viên hôm nay
            //SalaryLib.initSalaryToday(DateTime.Today);
            // Bind team work
            //Bind_TeamWork();

            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList>() { ddlSalon }, Perm_ViewAllData);
                bindData();
                RemoveLoading();
            }
        }

        private void bindData()
        {
            using (var db = new Solution_30shineEntities())
            {
                SalonId = Convert.ToInt32(ddlSalon.SelectedValue);
                // Lay data tu Store_Service_Rating
                var data = ServiceRatingModel.Instance.GetBadRatingBySalonId(SalonId);
                Bind_Paging(data.Count);
                var dataSegment = data.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                RptBillPending.DataSource = dataSegment;
                RptBillPending.DataBind();

                // Salon instance
                Salon = db.Tbl_Salon.FirstOrDefault(w => w.Id == SalonId);
            }
        }

        private void Bind_TeamWork()
        {
            using (var db = new Solution_30shineEntities())
            {
                lstTeamService = db.TeamServices.Where(w => w.IsDelete != 1 && w.Publish == true && w.Status == 1).ToList();
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            bindData();
            RemoveLoading();
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value       
            PAGING._Segment = 50;
            //PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //string[] Allow = new string[] { "root", "admin", "salonmanager", "reception", "checkin", "checkout" };
                //string[] AllowDelete = new string[] { "root", "admin" };
                //string[] AllowViewAllData = new string[] { "root", "admin" };
                //var Permission = Session["User_Permission"].ToString().Trim();
                //if (Array.IndexOf(Allow, Permission) != -1)
                //{
                //    Perm_Access = true;

                //}
                //if (Array.IndexOf(AllowDelete, Permission) != -1)
                //{
                //    Perm_Delete = true;
                //}
                //Perm_Edit = true;

                //if (Array.IndexOf(AllowViewAllData, Permission) != -1)
                //{
                //    Perm_ViewAllData = true;
                //}
                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }

            // Call execute function
            //ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            /**
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
            **/
        }

        /**
         * Get ServiceRatingModel Instance
         * @return ServiceRatingModel
         **/
        public static ServiceBadRatingList Instance
        {
            get
            {
                if (ServiceBadRatingList._Instance == null)
                {
                    ServiceBadRatingList._Instance = new ServiceBadRatingList();
                }
                return ServiceBadRatingList._Instance;
            }
        }
    }
}