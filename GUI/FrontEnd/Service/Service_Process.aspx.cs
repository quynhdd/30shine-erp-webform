﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Web.Helpers;
using System.Text.RegularExpressions;
using System.Globalization;
using LinqKit;
using System.Text;
using System.Data.Entity.Migrations;

namespace _30shine.GUI.FrontEnd.Service
{
    public partial class Service_Process : System.Web.UI.Page
    {
        protected string Color = "";
        protected List<Bill> billsPending = new List<Bill>();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.Cookies["TeamAccount"] != null)
                {
                    var TeamAccount = Request.Cookies["TeamAccount"].Value;
                    bindPendingByTeam(TeamAccount);
                }else
                    Response.Redirect("/dang-nhap.html", false);
            }            
        }

        private void bindPendingByTeam(string TeamAccount)
        {
            using (var db = new Solution_30shineEntities())
            {
                var salonId = 3;
                //var teamId = 1;
                var Team = db.TeamServices.FirstOrDefault(w=>w.Account==TeamAccount);
                if (Team != null)
                {
                    var teamId = Team.Id;
                    Color = Team.Color;
                    var sql = @"select a.*, c.Fullname as CustomerName, s1.Fullname as stylistName, s2.Fullname as skinnerName
                            from BillService as a
                            left join Customer as c
                            on a.CustomerCode = c.Customer_Code
                            left join Staff as s1
                            on a.Staff_Hairdresser_Id = s1.Id
                            left join Staff as s2
                            on a.Staff_HairMassage_Id = s2.Id
                            where a.IsDelete != 1 and a.Pending = 1
                            and a.SalonId = " + salonId + " and a.TeamId = " + teamId + @"
                            and a.Status >= 0 and a.CreatedDate >= '" + String.Format("{0:yyyy/MM/dd}", DateTime.Today) + "'" +
                            " order by Id asc";
                    billsPending = db.Database.SqlQuery<Bill>(sql).ToList();
                    if (billsPending.Count > 0)
                    {
                        for (var i = 0; i < billsPending.Count; i++)
                        {
                            billsPending[i].CustomerName = getName(billsPending[i].CustomerName);
                            billsPending[i].stylistName = getName(billsPending[i].stylistName);
                            billsPending[i].skinnerName = getName(billsPending[i].skinnerName);
                        }
                    }
                    HDF_Team_Id.Value = teamId.ToString();
                }
            }
        }

        private static string getName(string fullname)
        {
            if (fullname != null)
            {
                string[] names = fullname.Trim().Split(' ');
                if (names.Length > 0)
                {
                    return names[names.Length - 1];
                }
                else
                {
                    return fullname;
                }
            }
            else
            {
                return "";
            }        
        }

        [WebMethod(EnableSession = true)]
        public static string loadBillToTeam(int salonId, int teamId)
        {
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var sql = @"select a.Id, a.ServiceIds, a.CustomerCode as CustomerCode, Coalesce(a.Staff_Hairdresser_Id, 0) as stylistId, Coalesce(a.Staff_HairMassage_Id, 0) as skinnerId,
                            a.CreatedDate, Coalesce(a.SalonId, 0) as SalonId, Coalesce(a.TeamId, 0) as TeamId, c.Fullname as CustomerName, s1.Fullname as stylistName, s2.Fullname as skinnerName
                            from BillService as a
                            left join Customer as c
                            on a.CustomerCode = c.Customer_Code
                            left join Staff as s1
                            on a.Staff_Hairdresser_Id = s1.Id
                            left join Staff as s2
                            on a.Staff_HairMassage_Id = s2.Id
                            where a.IsDelete != 1 and a.Pending = 1
                            and a.SalonId = " + salonId+" and a.TeamId = "+ teamId +@"
                            and a.Status is null and a.CreatedDate >= '"+String.Format("{0:yyyy/MM/dd}", DateTime.Today)+"'";
                var bills = db.Database.SqlQuery<bill>(sql).ToList();
                var loop = 0;
                if (bills.Count > 0)
                {
                    foreach (var v in bills)
                    {
                        var obj = db.BillServices.FirstOrDefault(w => w.Id == v.Id);
                        obj.Status = 0;
                        db.BillServices.AddOrUpdate(obj);
                        db.SaveChanges();

                        bills[loop].CustomerName = getName(bills[loop].CustomerName);
                        bills[loop].skinnerName = getName(bills[loop].skinnerName);
                        bills[loop].stylistName = getName(bills[loop].stylistName);
                        loop++;
                    }
                    Msg.msg = serializer.Serialize(bills);
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        [WebMethod(EnableSession = true)]
        public static string updateBillStep(int bId, int skinnerId, int stylistId)
        {
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var objBill = db.BillServices.FirstOrDefault(w=>w.Id == bId);
                if (objBill != null)
                {
                    switch (objBill.Status)
                    {
                        case 0: objBill.ShampooTime0 = DateTime.Now; break;
                        case 1: objBill.HairCutTime0 = DateTime.Now; break;
                        default: break;
                    }
                    if (skinnerId > 0)
                    {
                        objBill.Staff_HairMassage_Id = skinnerId;
                    }
                    if (stylistId > 0)
                    {
                        objBill.Staff_Hairdresser_Id = stylistId;
                    }
                    objBill.Status = Convert.ToByte(objBill.Status + 1); 
                    db.BillServices.AddOrUpdate(objBill);
                    db.SaveChanges();

                    var sql = @"select a.Id, a.ServiceIds, a.CustomerCode as CustomerCode, Coalesce(a.Staff_Hairdresser_Id, 0) as stylistId, Coalesce(a.Staff_HairMassage_Id, 0) as skinnerId, a.Status,
                            a.CreatedDate, Coalesce(a.SalonId, 0) as SalonId, Coalesce(a.TeamId, 0) as TeamId, c.Fullname as CustomerName, s1.Fullname as stylistName, s2.Fullname as skinnerName
                            from BillService as a
                            left join Customer as c
                            on a.CustomerCode = c.Customer_Code
                            left join Staff as s1
                            on a.Staff_Hairdresser_Id = s1.Id
                            left join Staff as s2
                            on a.Staff_HairMassage_Id = s2.Id
                            where a.Id = " + bId;
                    var retBill = db.Database.SqlQuery<bill>(sql).FirstOrDefault();
                    retBill.CustomerName = getName(retBill.CustomerName);
                    retBill.skinnerName = getName(retBill.skinnerName);
                    retBill.stylistName = getName(retBill.stylistName);

                    Msg.msg = serializer.Serialize(retBill);
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        [WebMethod(EnableSession = true)]
        public static string updateCompleteStep(int bId)
        {
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var objBill = db.BillServices.FirstOrDefault(w => w.Id == bId);
                if (objBill != null)
                {
                    switch (objBill.Status)
                    {
                        case 1: objBill.ShampooTime1 = DateTime.Now; break;
                        case 2: objBill.HairCutTime1 = DateTime.Now; objBill.Status = Convert.ToByte(objBill.Status + 1); break;
                        default: break;
                    }
                    db.BillServices.AddOrUpdate(objBill);
                    db.SaveChanges();

                    //Msg.msg = serializer.Serialize(objBill);
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }
    }

    public class bill
    {
        public int Id { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public int skinnerId { get; set; }
        public string skinnerName { get; set; }
        public int stylistId { get; set; }
        public string stylistName { get; set; }
        public int TeamId { get; set; }
        public int SalonId { get; set; }
        public string ServiceIds { get; set; }
        public byte Status { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class Bill : BillService
    {
        public string CustomerName { get; set; }
        public string skinnerName { get; set; }
        public string stylistName { get; set; }
    }
}