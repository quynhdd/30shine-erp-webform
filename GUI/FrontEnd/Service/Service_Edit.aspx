﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Service_Edit.aspx.cs" Inherits="_30shine.GUI.UIService.Service_Edit" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ServiceAdd" ContentPlaceHolderID="CtMain" runat="server">

<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">
                <li class="li-listing"><a href="/dich-vu/danh-sach.html">Danh sách</a></li>
                <li class="li-add active"><a href="/dich-vu/them-moi.html">Thêm mới</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add admin-product-add fe-service">
    <%-- Add --%>                
    <div class="wp960 content-wp">
        <!-- System Message -->
        <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
        <!-- END System Message -->

        <div class="table-wp">
            <table class="table-add admin-product-table-add fe-service-table-add">
                <tbody>
                    <tr class="title-head">
                        <td><strong>Thông tin dịch vụ</strong></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr class="tr-margin" style="height: 20px;"></tr>

                    <tr class="tr-field-ahalf">
                        <td class="col-xs-3 left"><span>Khách hàng</span></td>
                        <td class="col-xs-7 right">
                            <span class="field-wp">
                                <asp:TextBox ID="CustomerName" runat="server" ClientIDMode="Static" ReadOnly="true"></asp:TextBox>
                                <asp:TextBox ID="CustomerCode" CssClass="mgl" placeholder="Nhập mã khách hàng ở đây" runat="server" ClientIDMode="Static"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="ValidateCustomerName" ControlToValidate="CustomerName" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập mã khách hàng!"></asp:RequiredFieldValidator>
                            </span>
                                            
                        </td>
                        <td class="col-xs-2"></td>
                    </tr>

                    <tr class="tr-field-ahalf">
                        <td class="col-xs-3 left"><span>Dịch vụ</span></td>
                        <td class="col-xs-7 right">
                            <span class="field-wp">
                                <asp:DropDownList ID="ServiceName" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <asp:RequiredFieldValidator InitialValue="0" 
                                    ID="rfvDDL_Product" Display="Dynamic" 
                                    ControlToValidate="ServiceName"
                                    runat="server"  Text="Bạn chưa chọn dịch vụ!" 
                                    ErrorMessage="Please Select the Product"
                                    ForeColor="Red"
                                    CssClass="fb-cover-error select-service-error">
                                </asp:RequiredFieldValidator>
                                <%--<asp:RequiredFieldValidator ID="ValidateServiceName" ControlToValidate="ServiceName" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập tên dịch vụ!"></asp:RequiredFieldValidator>--%>
                            </span>
                                            
                        </td>
                        <td class="col-xs-2"><span class="box-money box-money-service"></span></td>
                    </tr>

                    <tr class="tr-field-ahalf">
                        <td class="col-xs-3 left"><span>Nhân viên</span></td>
                        <td class="col-xs-7 right">
                            <span class="field-wp">
                                <asp:DropDownList ID="Hairdresser" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <asp:DropDownList ID="HairMassage" CssClass="mgl" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="ServiceName" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập tên dịch vụ!"></asp:RequiredFieldValidator>
                            </span>
                                            
                        </td>
                        <td class="col-xs-2"><span class=""></span></td>
                    </tr>

                    <tr class="tr-field-ahalf tr-product">
                        <td class="col-xs-3 left"><span>Sản phẩm</span></td>
                        <td class="col-xs-7 right">
                            <div class="row">
                                <div class="show-product" id="BtnShowProduct">Thêm sản phẩm</div>
                            </div>                            
                            <div class="listing-product" id="ListingProductWp">
                                <table class="table table-listing-product">
                                    <thead>
                                        <tr>
                                            <th><input type="checkbox" /></th>
                                            <th>STT</th>
                                            <th>Tên sản phẩm</th>
                                            <th>Mã sản phẩm</th>
                                            <th>Giá</th>
                                            <th>Số lượng</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="Rpt_Product" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td class="td-product-checkbox"><input type="checkbox" value="<%# Eval("Id") %>" /></td>
                                                    <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                                    <td class="td-product-name"><%# Eval("Name") %></td>
                                                    <td class="td-product-code"><%# Eval("Code") %></td>
                                                    <td class="td-product-price" data-price="<%# Eval("Price") %>"><%# Eval("Price") %></td>
                                                    <td class="td-product-quantity">
                                                        <input type="text" class="product-quantity" value="1" />
                                                        <div class="box-money"></div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>                                        
                                    </tbody>
                                </table>
                            </div>                                           
                        </td>
                        <td class="col-xs-2"></td>
                    </tr>

                    <tr class="tr-description tr-field-ahalf">
                        <td class="col-xs-3 left"><span>Tổng số tiền</span></td>
                        <td class="col-xs-7 right">
                            <span class="field-wp">
                                <asp:TextBox ID="TotalMoney" runat="server" ClientIDMode="Static" Text="0" ReadOnly="true"></asp:TextBox>
                                <span class="unit-money">VNĐ</span>
                            </span>
                        </td>
                        <td class="col-xs-2"></td>
                    </tr>

                    <tr class="tr-description">
                        <td class="col-xs-3 left"><span>Ghi chú</span></td>
                        <td class="col-xs-7 right">
                            <span class="field-wp">
                                <asp:TextBox TextMode="MultiLine" Rows="5" ID="Description" runat="server" ClientIDMode="Static"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-xs-2"></td>
                    </tr>
                    
                    <tr class="tr-send">
                        <td class="col-xs-3 left"></td>
                        <td class="col-xs-9 right no-border">
                            <span class="field-wp">
                                <asp:Button ID="Send" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="AddService"></asp:Button>
                                <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>
                            </span>
                        </td>
                    </tr>
                    <asp:HiddenField runat="server" ID="UrlKeyField"  ClientIDMode="Static"/>
                    <asp:HiddenField runat="server" ID="ImgCoverField"  ClientIDMode="Static"/>
                </tbody>
            </table>

            <!-- input hidden -->
            <asp:HiddenField ID="HDF_CustomerCode" runat="server" ClientIDMode="Static"/>
            <asp:HiddenField ID="HDF_ProductIds" runat="server" ClientIDMode="Static"/>
            <asp:HiddenField ID="HDF_TotalMoney" runat="server" ClientIDMode="Static"/>
            <!-- END input hidden -->
        </div>
    </div>
    <%-- end Add --%>
</div>

<script>
    jQuery(document).ready(function () {
        // Add active menu
        $("#glbService").addClass("active");

        // Xử lý mã khách hàng => bind khách hàng
        $("#CustomerCode").bind("change", function () {
            var CustomerCode = $(this).val();
            ajaxGetCustomer(CustomerCode);
        });

        // Mở box listing sản phẩm
        $("#BtnShowProduct").bind("click", function () {
            $("#ListingProductWp").show();
        });

        // Tính giá | Price
        $("select#ServiceName").bind("change", function () {
            var price = $(this).find("option:selected").data("price");
            if (price != undefined) {
                $(".box-money-service").text(price).show();
            } else {
                $(".box-money-service").text("").hide();
            }
            TotalMoney();

        });

        $(".td-product-checkbox input[type='checkbox']").unbind("click").bind("click", function () {
            var obj = $(this).parent().parent(),
                boxMoney = obj.find(".box-money"),
                price = obj.find(".td-product-price").data("price"),
                quantity = obj.find("input.product-quantity").val(),
                checked = $(this).is(":checked");

            if (checked) {
                boxMoney.text(quantity * price).show();
            } else {
                boxMoney.text("").hide();
            }
            TotalMoney();
            getProductIds();
        });

        // Số lượng | Quantity
        $("input.product-quantity").bind("change", function () {
            var obj = $(this).parent().parent(),
                quantity = $(this).val(),
                price = obj.find(".td-product-price").data("price"),
                boxMoney = obj.find(".box-money");

            boxMoney.text(quantity * price);
            TotalMoney();
        });

        function TotalMoney() {
            var Money = 0;
            $(".box-money:visible").each(function () {
                Money += parseInt($(this).text());
            });
            $("#TotalMoney").val(Money);
        }

        function ajaxGetCustomer(CustomerCode) {
            $.ajax({
                type: "POST",
                url: "/GUI/Service/Service_Add.aspx/GetCustomer",
                //data: '{_HoVaTen: "' + _HoVaTen + '", _SoDienThoai: "' + _SoDienThoai + '", _TinhThanh: "' + _TinhThanh + '"}',
                data: '{CustomerCode : "' + CustomerCode + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var mission = JSON.parse(response.d);
                    if (mission.success) {
                        var customer = JSON.parse(mission.msg);
                        customer = customer[0];
                        $("#CustomerName").val(customer.Fullname);
                        $("#CustomerName").attr("data-code", customer.Customer_Code);
                        $("#HDF_CustomerCode").val(customer.Customer_Code);

                    } else {
                        var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                        showMsgSystem(msg, "warning");
                    }
                },
                failure: function (response) { alert(response.d); }
            });
        }

        function showMsgSystem(msg, status) {
            $("#MsgSystem").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);
            setTimeout(function () {
                $("#MsgSystem").fadeTo("slow", 0, function () {
                    $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 1);
                });
            }, 5000);
        }

        function getProductIds() {
            var Ids = [];
            var prd = {};
            $(".td-product-checkbox input[type='checkbox']:checked").each(function () {
                prd = {};
                var THIS = $(this),
                    obj = THIS.parent().parent();

                prd.Id = THIS.val(),
                prd.Price = obj.find(".td-product-price").data("price"),
                prd.Quantity = obj.find("input.product-quantity").val();

                Ids.push(prd);

            });
            Ids = JSON.stringify(Ids);
            $("#HDF_ProductIds").val(Ids);
        }

    });
</script>

</asp:Content>

