﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Linq.Expressions;
using Project.RealtimeFirebase;
using System.Threading.Tasks;
using System.Data.Linq.SqlClient;
using System.Data.SqlClient;
using Project.SystemService.SendSMS;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using _30shine.Helpers.Http;
using Libraries;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Data.Objects;
using System.Net.Http;

namespace _30shine.GUI.FrontEnd.UIService
{
    public partial class TimeLine_Beta_V2 : System.Web.UI.Page
    {
        protected List<string> listHour = new List<string>();
        protected List<cls_timeline_stylist> dataTimeline = new List<cls_timeline_stylist>();
        protected List<cls_EstimateTimeData> estimateTime = new List<cls_EstimateTimeData>();
        public Solution_30shineEntities db;
        private bool Perm_Access = false;
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ShowSalon = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewPhone = false;
        protected List<Tbl_Salon> listSalon = new List<Tbl_Salon>();
        protected List<TeamService> listTeam = new List<TeamService>();
        protected string listService = "";
        protected int salonID = 0;
        public static int integer;
        public DateTime dateBook;
        private JavaScriptSerializer serializer = new JavaScriptSerializer();
        public static MessageNotification messageNotification;
        public static TimeLine_Beta_V2 instance;
        public static Library.SendEmail sendMail;
        public static CultureInfo culture = new CultureInfo("vi-VN");
        protected static int timeConfig = 0;
        protected string api = "";
        protected bool printBill = false;
        /// <summary>
        /// Constructor
        /// </summary>
        public TimeLine_Beta_V2()
        {
            db = new Solution_30shineEntities();
            messageNotification = new MessageNotification();
            sendMail = new Library.SendEmail();
        }

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                Perm_ViewPhone = permissionModel.CheckPermisionByAction("Perm_ViewPhone", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {

            //test();
            SetPermission();
            //api = ConfigurationManager.AppSettings["URL_API_BOOKING_REQUEST"].ToString();
            if (!IsPostBack)
            {

                timeConfig = Convert.ToInt32(ConfigurationManager.AppSettings["time_config"]);
                DatedBook.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now));
                dateBook = Convert.ToDateTime(DatedBook.Text, culture);
                Library.Function.bindSalon_NotHoiQuan_Timeline(new List<DropDownList> { ddlSalon }, Perm_ViewAllData, dateBook);
                salonID = GetSalonId(Convert.ToInt32(Session["User_Id"]), dateBook);
                ddlSalon.SelectedValue = salonID.ToString();
                listService = getListService();
                // Begin timing.
                if (Convert.ToDateTime(DatedBook.Text, culture) == DateTime.Now.Date)
                {
                    printBill = true;
                }
                dataTimeline = GetByDataTimeline(Convert.ToDateTime(DatedBook.Text, culture));
                listSalon = getListSalon();
                //listTeam = getTeam();

            }
            //datedBook = Convert.ToDateTime(DatedBook.Text, culture);
        }

        /// <summary>
        /// Get instance
        /// </summary>
        /// <returns></returns>
        public static TimeLine_Beta_V2 getInstance()
        {
            if (TimeLine_Beta_V2.instance is TimeLine_Beta_V2)
            {
                return TimeLine_Beta_V2.instance;
            }
            else
            {
                return new TimeLine_Beta_V2();
            }
        }

        /// <summary>
        /// Reload data by update panel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void updatePanelBindData(object sender, EventArgs e)
        {
            var datedBook = Convert.ToDateTime(DatedBook.Text, culture);
            dateBook = Convert.ToDateTime(DatedBook.Text, culture);
            try
            {
                timeConfig = Convert.ToInt32(ConfigurationManager.AppSettings["time_config"]);

                if (datedBook == DateTime.Now.Date)
                {
                    printBill = true;
                }
                if (datedBook.Date < DateTime.Now.Date)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "show_msg", "alert('Ngày chọn không được nhỏ hơn ngày hiện tại');", true);
                    return;
                }
                if (datedBook.Date > DateTime.Now.AddDays(timeConfig).Date)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "show_msg", "alert('Ngày chọn không được lớn hơn ngày hiện tại + " + timeConfig + "');", true);
                    return;
                }
                salonID = Convert.ToInt32(ddlSalon.SelectedValue);
                setSalonID(salonID);
                //Estimate_Time(salonID);

            }
            catch { }
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "add_class", "addClass();", true);
            dataTimeline = GetByDataTimeline(datedBook);
            listSalon = getListSalon();
            listService = getListService();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "set bill.SalonID", "bill.salonID = " + salonID + ";removeLoading();", true);

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "","removeLoading();", true);
        }

        /// <summary>
        /// Lấy dữ liệu dịch vụ
        /// </summary>
        /// <returns></returns>
        private string getListService()
        {
            return serializer.Serialize(db.Services.Where(w => w.IsDelete == 0 && w.Publish == 1).OrderBy(o => o.Order)
                .Select(s => new { s.Id, s.Code, s.Name, s.Price, Quantity = 1, s.VoucherPercent }).ToList());
        }

        /// <summary>
        /// Lấy giá trị salonID theo account đăng nhập
        /// </summary>
        /// <returns></returns>
        private int getSalonID()
        {
            int salonID = 0;
            if (Session["SalonId"] != null)
            {
                salonID = int.TryParse(Session["SalonId"].ToString(), out integer) ? integer : 0;
            }
            return salonID;
        }

        /// <summary>
        /// Set salonID
        /// </summary>
        /// <param name="salonID"></param>
        private void setSalonID(int salonID)
        {
            Session["SalonId"] = salonID;
        }

        /// <summary>
        /// Lấy danh sách salon
        /// </summary>
        /// <returns></returns>
        private List<Tbl_Salon> getListSalon()
        {
            var salons = new List<Tbl_Salon>();
            if (Perm_ViewAllData)
            {
                salons = db.Tbl_Salon.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Order).ToList();
            }
            else
            {
                if (Session["SalonId"] != null)
                {
                    int salonID = Convert.ToInt32(Session["SalonId"]);
                    salons = db.Tbl_Salon.Where(w => w.IsDelete != 1 && w.Publish == true).ToList();
                }
            }
            return salons;
        }

        /// <summary>
        /// Lấy danh sách dịch vụ theo bill
        /// </summary>
        /// <param name="billID"></param>
        /// <returns></returns>
        private static List<cls_timeline_service> getServiceByBillID(int billID)
        {
            using (var db = new Solution_30shineEntities())
            {
                return db.Database.SqlQuery<cls_timeline_service>(
                            @"select sv.Id, sv.Name, sv.Code, fs.Price, fs.Quantity, fs.VoucherPercent
	                        from SM_BillTemp as bill
	                        left join SM_BillTemp_FlowService as fs on fs.BillId = bill.Id and fs.IsDelete != 1
	                        left join [Service] as sv on sv.Id = fs.ServiceId
	                        where bill.Id = " + billID).ToList();
            }
        }

        /// <summary>
        /// Lấy danh sách team
        /// </summary>
        /// <returns></returns>
        private List<TeamService> getTeam()
        {
            return db.TeamServices.Where(w => w.IsDelete != 1 && w.Publish == true).ToList();
        }

        /// <summary>
        /// Lấy thông tin nhân viên theo OrderCode
        /// </summary>
        /// <param name="orderCode"></param>
        /// <returns></returns>
        [WebMethod]
        public static object getStaffByCode(string orderCode, int salonID)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                if (salonID == 0)
                {
                    return db.Staffs.Where(w => w.OrderCode == orderCode && w.IsDelete != 1 && w.Active == 1 && w.Type == 5).Select(s => new { s.Id, s.Fullname, s.Phone, s.OrderCode }).FirstOrDefault();
                }
                else
                {
                    //var keeping = db.FlowTimeKeepings.Where()
                    var keeping = (from a in db.FlowTimeKeepings
                                   join b in db.Staffs on a.StaffId equals b.Id
                                   where a.SalonId == salonID && a.IsDelete == 0 && b.Active == 1 && b.OrderCode == orderCode
                                   && b.Type == 5
                                   select new
                                   {
                                       Id = b.Id,
                                       Fullname = b.Fullname,
                                       Phone = b.Phone,
                                       OrderCode = b.OrderCode

                                   }).FirstOrDefault();
                    return keeping;
                    //return db.Staffs.Where(w => w.OrderCode == orderCode && w.SalonId == salonID && w.IsDelete != 1 && w.Active == 1 && w.Type == 5).Select(s => new { s.Id, s.Fullname, s.Phone, s.OrderCode }).FirstOrDefault();
                }
            }
        }

        /// <summary>
        /// Lấy dữ liệu billTemp theo ID
        /// </summary>
        /// <param name="billTempID"></param>
        /// <returns></returns>
        [WebMethod]
        public static object getBillInfoByID(int billTempID)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var message = new Library.Class.cls_message();
                var billInfo = new cls_timeline_bill();
                var billTemp = db.SM_BillTemp.FirstOrDefault(w => w.Id == billTempID);
                if (billTemp != null)
                {
                    billInfo.billID = billTempID;
                    billInfo.bookingID = billTemp.BookingId != null ? billTemp.BookingId.Value : 0;
                    billInfo.PDFBillCode = billTemp.PDFBillCode;
                    billInfo.HCItem = billTemp.HCItem;

                    var checkin = db.Staffs.FirstOrDefault(w => w.Id == billTemp.ReceptionId);
                    if (checkin != null)
                    {
                        billInfo.checkinID = int.TryParse(checkin.OrderCode, out integer) ? integer : 0;
                        billInfo.checkinName = checkin.Fullname;
                    }
                    var customer = db.Customers.FirstOrDefault(w => w.Id == billTemp.CustomerId);
                    if (customer != null)
                    {
                        billInfo.customerName = customer.Fullname;
                        billInfo.customerPhone = ChangePhone(customer.Phone);
                        billInfo.customerId = customer.Id;
                    }
                    var salon = db.Tbl_Salon.FirstOrDefault(w => w.Id == billTemp.SalonId);
                    if (salon != null)
                    {
                        billInfo.salonID = salon.Id;
                        billInfo.salonName = salon.Name;
                    }
                    var stylist = db.Staffs.FirstOrDefault(w => w.Id == billTemp.Staff_Hairdresser_Id);
                    if (stylist != null)
                    {
                        billInfo.stylistID = stylist.Id;
                        billInfo.stylistName = stylist.Fullname;
                    }
                    //billInfo.services = getServiceByBillID(billTemp.Id);

                    var booking = db.SM_BookingTemp.FirstOrDefault(w => w.Id == billTemp.BookingId);
                    if (booking != null)
                    {
                        billInfo.bookingID = booking.Id;
                        var hourFrame = db.BookHours.FirstOrDefault(w => w.Id == booking.HourId);
                        if (hourFrame != null)
                        {
                            billInfo.hourID = hourFrame.Id;
                            billInfo.hourFrame = hourFrame.Hour.Replace('h', ':');
                        }
                        billInfo.IsAutoStylist = booking.IsAutoStylist != null ? booking.IsAutoStylist.Value : true;
                    }

                    message.success = true;
                    message.data = billInfo;
                    message.message = "Success!";
                }
                else
                {
                    message.success = true;
                    message.message = "Lỗi. Bản ghi không tồn tại!";
                }

                return message;
            }
        }

        /// <summary>
        /// Set trạng thái gọi điện cho khách hàng
        /// </summary>
        /// <param name="orderCode"></param>
        /// <returns></returns>
        [WebMethod]
        public static object setCallPhone(int bookingID, bool isCall)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var DatedBook = String.Format("{0:yyyy/MM/dd}", DateTime.Today);
                var message = new Library.Class.cls_message();
                var serializer = new JavaScriptSerializer();
                var error = 0;
                var bookingRecord = db.SM_BookingTemp.FirstOrDefault(w => w.Id == bookingID);
                if (bookingRecord != null)
                {
                    string sql = @"select MAX([Order]) from SM_BookingTemp
                                where IsDelete = 0 and DatedBook = '" + DatedBook + "' and SalonId =" + bookingRecord.SalonId + " and IsCall = 1 and SubHour =" + bookingRecord.SubHour;
                    int? order = db.Database.SqlQuery<int?>(sql).SingleOrDefault();
                    if (order != null && isCall == true)
                    {
                        bookingRecord.Order = order + 1;
                    }
                    else if (order == null && isCall == true)
                    {
                        bookingRecord.Order = 1;
                    }
                    else if (isCall == false)
                    {
                        bookingRecord.Order = null;
                    }

                    if (bookingRecord.IsCall == null)
                    {
                        bookingRecord.IsCallTime = DateTime.Now;
                    }
                    else
                    {
                        bookingRecord.IsCallTimeModified = DateTime.Now;
                    }
                    bookingRecord.IsCall = isCall;



                    db.SM_BookingTemp.AddOrUpdate(bookingRecord);
                    error += db.SaveChanges() > 0 ? 0 : 1;

                    if (error == 0)
                    {
                        message.success = true;
                        message.message = serializer.Serialize(bookingRecord);
                    }
                    else
                    {
                        message.success = true;
                        message.message = "Cập nhật thất bại. Vui lòng liên hệ nhóm phát triển!";
                    }
                }
                else
                {
                    message.success = true;
                    message.message = "Lỗi. Bản ghi không tồn tại!";
                }

                return message;
            }
        }

        /// <summary>
        /// Hủy đặt lịch
        /// </summary>
        /// <param name="bookingID"></param>
        /// <returns></returns>
        [WebMethod]
        public static object cancelBook(int bookingID, string noteCancel)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var message = new Library.Class.cls_message();
                var error = 0;
                var bookingTempRecord = db.SM_BookingTemp.FirstOrDefault(w => w.Id == bookingID);
                if (bookingTempRecord != null)
                {
                    if (noteCancel != "")
                    {
                        var bookingTemp = db.Bookings.FirstOrDefault(w => w.BookingTempId == bookingTempRecord.Id && w.IsDelete == 0);
                        var objBill = db.BillServices.FirstOrDefault(a => a.BookingId == bookingTemp.Id && a.IsDelete == 0);
                        if (objBill != null)
                        {
                            message.success = false;
                            message.message = "Cập nhật thất bại. Lịch đặt đã có In BILL bạn không thể xóa! ( Quản lý có quyền xóa trong Pending )";
                            return message;
                        }
                        bookingTempRecord.IsDelete = 1;
                        bookingTempRecord.NoteDelete = noteCancel;
                        bookingTempRecord.ModifiedDate = DateTime.Now;
                        db.SM_BookingTemp.AddOrUpdate(bookingTempRecord);
                        error += db.SaveChanges() > 0 ? 0 : 1;

                        // Cập nhật bảng Booking

                        if (bookingTemp != null)
                        {

                            bookingTemp.IsDelete = 1;
                            bookingTemp.NoteDelete = noteCancel;
                            bookingTemp.ModifiedDate = DateTime.Now;
                            db.Bookings.AddOrUpdate(bookingTemp);
                            error += db.SaveChanges() > 0 ? 0 : 1;
                        }
                        if (error == 0)
                        {
                            message.success = true;
                            message.message = "Cập nhật thành công!";
                        }
                        else
                        {
                            message.success = true;
                            message.message = "Cập nhật thất bại. Vui lòng liên hệ nhóm phát triển!";
                        }
                    }
                    else
                    {
                        message.success = true;
                        message.message = "Lỗi. Vui lòng nhập lý do hủy lịch đặt!";
                    }
                }
                else
                {
                    message.success = true;
                    message.message = "Lỗi. Bản ghi không tồn tại!";
                }

                return message;
            }
        }

        /// <summary>
        /// Tạo bill dịch vụ
        /// </summary>
        /// <param name="billClient"></param>
        /// <returns></returns>
        [WebMethod]
        public static object insertBill(string billClient)
        {

            // AV
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var message = new Library.Class.cls_message();
                var serializer = new JavaScriptSerializer();
                try
                {
                    cls_timeline_bill bill = serializer.Deserialize<cls_timeline_bill>(billClient);
                    var datedBook = Convert.ToDateTime(bill.datedBook, culture);
                    if (bill != null)
                    {
                        // Kiểm tra tạo bản ghi Booking cho khách
                        var bookingTemp = new SM_BookingTemp();
                        var booking = new Booking();
                        #region process Customer
                        Customer customer = db.Customers.FirstOrDefault(w => w.Phone == bill.customerPhone);
                        if (customer == null)
                        {
                            customer = new Customer();
                            customer.Fullname = bill.customerName;
                            customer.Phone = bill.customerPhone;
                            customer.SalonId = bill.salonID;
                            customer.CreatedDate = DateTime.Now;
                            customer.IsDelete = 0;
                            if ((bill.snDay != null && bill.snMonth != null && bill.snYear != null) || (bill.snDay != "" && bill.snMonth != "" && bill.snYear != ""))
                            {
                                customer.SN_day = Convert.ToInt32(bill.snDay);
                                customer.SN_month = Convert.ToInt32(bill.snMonth);
                                customer.SN_year = Convert.ToInt32(bill.snYear);
                            }

                            customer.TotalBillservice = 0;
                            db.Customers.Add(customer);
                            db.SaveChanges();
                        }
                        else
                        {
                            if (customer.Fullname == null || (customer.Fullname != null && customer.Fullname == ""))
                            {
                                customer.Fullname = bill.customerName;
                            }
                            if ((bill.snDay != null && bill.snMonth != null && bill.snYear != null) || (bill.snDay != "" && bill.snMonth != "" && bill.snYear != ""))
                            {
                                customer.SN_day = Convert.ToInt32(bill.snDay);
                                customer.SN_month = Convert.ToInt32(bill.snMonth);
                                customer.SN_year = Convert.ToInt32(bill.snYear);
                                customer.ModifiedDate = DateTime.Now;
                            }
                            db.Customers.AddOrUpdate(customer);
                            db.SaveChanges();
                        }
                        #endregion

                        #region process booking
                        if (bill.bookingID <= 0)
                        {
                            bookingTemp = new SM_BookingTemp();
                            bookingTemp.CustomerName = bill.customerName;
                            bookingTemp.CustomerPhone = bill.customerPhone;
                            bookingTemp.HourId = bill.hourID;
                            bookingTemp.SubHour = bill.subHourID;
                            bookingTemp.IsMakeBill = true;
                            bookingTemp.SalonId = bill.salonID;
                            bookingTemp.StylistId = bill.stylistID;
                            bookingTemp.DatedBook = DateTime.Now.Date;
                            bookingTemp.IsDelete = 0;
                            bookingTemp.CreatedDate = DateTime.Now;
                            bookingTemp.IsBookViaBill = true;
                            bookingTemp.IsBookAtSalon = true;
                            bookingTemp.IsBookOnline = false;
                            bookingTemp.IsCall = true;
                            bookingTemp.IsCallTime = DateTime.Now;
                            bookingTemp.CustomerId = customer.Id;
                            bookingTemp.IsCall = true;
                            bookingTemp.IsCallTime = DateTime.Now;
                            bookingTemp.IsMakeBill = true;
                            //bookingTemp.Order = 
                            if (bill != null)
                            {
                                // Trường hợp ghi nhận tại salon khách có book Stylist
                                bookingTemp.IsAutoStylist = bill.IsAutoStylist;
                                bookingTemp.IsBookStylist = !bill.IsAutoStylist;
                            }
                            else
                            {
                                bookingTemp.IsAutoStylist = false;
                                bookingTemp.IsBookStylist = true;
                            }
                            if (datedBook >= DateTime.Now.Date && datedBook <= DateTime.Now.AddDays(timeConfig).Date)
                            {
                                db.SM_BookingTemp.AddOrUpdate(bookingTemp);
                                db.SaveChanges();
                            }
                            else
                            {
                                message.success = false;
                                message.message = "Ngày chọn không được lớn hơn ngày hiện tại + " + timeConfig;
                                return message;
                            }
                            // Insert vào bảng Booking                            
                            booking.CustomerName = bill.customerName.Trim();
                            booking.CustomerPhone = bill.customerPhone.Trim();
                            booking.HourId = bill.hourID;
                            booking.SubHour = bill.subHourID;
                            booking.IsMakeBill = true;
                            booking.SalonId = bill.salonID;
                            booking.StylistId = bill.stylistID;
                            booking.DatedBook = DateTime.Now.Date;
                            booking.IsDelete = 0;
                            booking.CreatedDate = DateTime.Now;
                            booking.IsBookAtSalon = true;
                            booking.IsBookOnline = false;
                            booking.CustomerId = customer.Id;
                            if (bill != null)
                            {
                                // Trường hợp ghi nhận tại salon khách có book Stylist
                                booking.IsAutoStylist = bill.IsAutoStylist;
                                booking.IsBookStylist = !bill.IsAutoStylist;
                            }
                            else
                            {
                                booking.IsAutoStylist = false;
                                booking.IsBookStylist = true;
                            }
                            booking.BookingTempId = bookingTemp.Id;
                            if (datedBook >= DateTime.Now.Date && datedBook <= DateTime.Now.AddDays(timeConfig).Date)
                            {
                                db.Bookings.AddOrUpdate(booking);
                                db.SaveChanges();
                            }
                            else
                            {
                                message.success = false;
                                message.message = "Ngày chọn không được lớn hơn ngày hiện tại + " + timeConfig;
                                return message;
                            }
                            bill.bookingID = bookingTemp.Id;
                        }
                        else
                        {
                            bookingTemp = db.SM_BookingTemp.FirstOrDefault(w => w.Id == bill.bookingID);
                            bookingTemp.IsMakeBill = true;
                            bookingTemp.IsCall = true;
                            if (bookingTemp.IsCallTime == null)
                            {
                                bookingTemp.IsCallTime = DateTime.Now;
                            }
                            else
                            {
                                bookingTemp.IsCallTimeModified = DateTime.Now;
                            }
                            booking = db.Bookings.FirstOrDefault(w => w.BookingTempId == bill.bookingID);
                        }
                        #endregion

                        // II. Insert bản ghi BillService
                        var billService = new BillService();
                        // check booking
                        var bookingCheck = db.Bookings.FirstOrDefault(w => w.Id == booking.Id && w.IsDelete == 0);
                        if (bookingCheck == null)
                        {
                            message.success = false;
                            message.status = "error";
                            message.messageNoti = "Khách đã hủy đặt lịch hoặc có lỗi xảy ra";
                            return message;
                        }
                        var billServiceCheck = db.BillServices.FirstOrDefault(a => a.IsDelete == 0 && a.BookingId == booking.Id);
                        if (billServiceCheck != null)
                        {
                            message.success = false;
                            message.status = "error";
                            message.messageNoti = "Lịch đặt đã được Inbill!";
                            return message;
                        }
                        billService.CustomerId = customer.Id;
                        billService.CustomerCode = bill.customerPhone;
                        billService.SalonId = bill.salonID;// billTemp.SalonId;
                        billService.ServiceIds = serializer.Serialize(bill.services);// billTemp.ServiceIds;
                        billService.ReceptionId = bill.checkinID;// billTemp.ReceptionId;
                        billService.TeamId = bill.teamID;// billTemp.TeamId;
                        billService.HCItem = bill.HCItem.TrimEnd(',');
                        if (bill.stylistID > 0)
                        {
                            billService.Staff_Hairdresser_Id = bill.stylistID;// billTemp.Staff_Hairdresser_Id;
                        }
                        //Dũng
                        billService.CustomerUsedNumber = customer.TotalBillservice;
                        if (booking != null)
                        {
                            billService.BookingId = booking.Id;
                        }
                        billService.BillCode = GenBillCode(Convert.ToInt32(bill.salonID), "HD", 4);// billTemp.BillCode;
                        billService.PDFBillCode = billService.BillCode + "_" + UIHelpers.GetUniqueKey(6, 6);// billTemp.PDFBillCode;
                        billService.CreatedDate = DateTime.Now;
                        billService.IsDelete = 0;
                        billService.Pending = 1;
                        db.BillServices.Add(billService);
                        db.SaveChanges();
                        // Tạo bản ghi ratting mặc định
                        var objRatingdetail = new RatingDetail();
                        objRatingdetail.BillId = billService.Id;
                        objRatingdetail.CreatedTime = DateTime.Now;
                        objRatingdetail.isAuto = true;
                        objRatingdetail.Isdelete = false;
                        objRatingdetail.Version = 2;
                        objRatingdetail.StarNumber = 5;
                        objRatingdetail.OrderNumber = 0;
                        db.RatingDetails.Add(objRatingdetail);
                        // Tạo bản ghi lưu ảnh uôn mặc định
                        var objImageData = new ImageData();
                        objImageData.CreatedTime = DateTime.Now;
                        objImageData.IsDelete = false;
                        objImageData.OBJId = billService.Id;
                        objImageData.Slugkey = "image_bill_curling";
                        objImageData.IsAuto = true;
                        db.ImageDatas.Add(objImageData);
                        db.SaveChanges();
                        bill.billID = billService.Id;
                        bill.IsAutoStylist = booking.IsAutoStylist != null ? booking.IsAutoStylist.Value : false;
                        var objCheckin = db.Staffs.FirstOrDefault(s => s.Id == billService.ReceptionId);
                        var objcustomer = db.Customers.FirstOrDefault(w => w.Id == billService.CustomerId);
                        if (objcustomer != null)
                        {
                            if (objcustomer.MemberType != 0 && objcustomer.MemberEndTime > DateTime.Now)
                            {
                                bill.isShineMember = 1;
                                bill.memberEndTime = string.Format("{0:dd/MM/yyyy HH:mm:ss}", objcustomer.MemberEndTime);
                            }
                            else bill.isShineMember = 0;
                        }
                        if (objCheckin != null)
                        {
                            bill.checkinName = objCheckin.Fullname;
                        }

                        bill.PDFBillCode = billService.BillCode;
                        bill.TextNote1 = booking == null ? "" : (booking.TextNote1 == null ? "" : booking.TextNote1);
                        bill.TextNote2 = booking == null ? "" : (booking.TextNote2 == null ? "" : booking.TextNote2);
                        bill.NeedConsult = customer == null ? "" : (customer.TotalBillservice == null || customer.TotalBillservice <= 1 ? "KHÁCH MỚI CẦN TƯ VẤN KỸ" : "");
                        bill.CustomerScore = bookingTemp.CustomerScore;
                        bill.DatetimeCurrent = String.Format("{0:dd-MM-yyyy HH:mm:ss}", bookingTemp.IsCallTime);
                        bill.timeCustomerCome = bookingTemp.IsCallTime == null ? "" : string.Format("{0:dd/MM/yyyy HH:mm:ss}", bookingTemp.IsCallTime);

                        // get ra Note special customer
                        var dataNoteSpecialCustomer = (from a in db.SpecialCustomers
                                                       join b in db.SpecialCusDetails on a.Id equals b.SpecialCusId
                                                       where a.CustomerId == booking.CustomerId
                                                       && a.IsDelete == false
                                                       select new
                                                       {
                                                           noteSpecialCustomer = b.ReasonDiff
                                                       }).FirstOrDefault();
                        if (dataNoteSpecialCustomer != null)
                        {
                            bill.noteSpecialCustomer = dataNoteSpecialCustomer.noteSpecialCustomer;
                        }

                        message.success = true;
                        message.messageNoti = "Success!";
                        message.data = bill;
                        //lay thong tin campaign     
                        if (booking != null && booking.CampaignId > 0)
                        {
                            //
                            var campaign = db.MktCampaigns.FirstOrDefault(e => e.Id == booking.CampaignId);
                            message.campaignInfo = campaign.Description;
                        }
                    }
                    else
                    {
                        message.success = false;
                        message.status = "error";
                        message.messageNoti = "Lỗi. Dữ liệu truyền lên không đúng.";
                        return message;
                    }
                }
                catch (Exception ex)
                {
                    message.success = false;
                    message.message = ex.Message;
                    return message;
                }

                return message;
            }
        }

        /// <summary>
        /// Cập nhật bill dịch vụ
        /// </summary>
        /// <param name="billClient"></param>
        /// <returns></returns>
        [WebMethod]
        public static object updateBill(string billClient)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var message = new Library.Class.cls_message();
                var error = 0;
                var serializer = new JavaScriptSerializer();
                try
                {
                    cls_timeline_bill bill = serializer.Deserialize<cls_timeline_bill>(billClient);
                    var booking = new Booking();
                    if (bill != null)
                    {
                        // Tạm thời chỉ cập nhật hóa chất
                        // I. Cập nhật bảng SM_BillTemp
                        var billTemp = db.SM_BillTemp.FirstOrDefault(w => w.Id == bill.billID);
                        if (billTemp != null)
                        {
                            billTemp.HCItem = bill.HCItem.TrimEnd(',');
                            billTemp.ModifiedDate = DateTime.Now;
                            db.SM_BillTemp.AddOrUpdate(billTemp);
                            error += db.SaveChanges() > 0 ? 0 : 1;

                            // II. Cập nhật bảng BillService
                            var billService = db.BillServices.FirstOrDefault(w => w.Id == billTemp.BillServiceID);
                            if (billService != null)
                            {
                                billService.HCItem = bill.HCItem.TrimEnd(',');
                                billService.ModifiedDate = DateTime.Now;
                                db.BillServices.AddOrUpdate(billService);
                                error += db.SaveChanges() > 0 ? 0 : 1;
                            }
                            else
                            {
                                message.success = false;
                                message.status = "error";
                                message.message = "Lỗi. Bản ghi bill service không tồn tại.";
                            }

                            bill.billID = billTemp.Id;
                            bill.PDFBillCode = billTemp.PDFBillCode;
                        }
                        else
                        {
                            message.success = false;
                            message.status = "error";
                            message.message = "Lỗi. Bản ghi bill temp không tồn tại.";
                        }

                        if (error == 0)
                        {
                            message.success = true;
                            message.message = "Sussess!";
                            message.data = bill;

                        }
                        else
                        {
                            message.success = false;
                            message.message = "Failed!";
                        }
                    }
                    else
                    {
                        message.success = false;
                        message.status = "error";
                        message.message = "Lỗi. Dữ liệu truyền lên không đúng.";
                    }
                }
                catch (Exception ex)
                {
                    message.success = false;
                    message.message = ex.Message;
                    return message;
                }

                return message;
            }
        }

        [WebMethod]
        public static object checkBooking(string billClient)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var message = new Library.Class.cls_message();
                var serializer = new JavaScriptSerializer();
                try
                {
                    var bill = serializer.Deserialize<cls_timeline_bill>(billClient);
                    if (bill != null)
                    {
                        var datedBook = Convert.ToDateTime(bill.datedBook, culture);
                        var bookingTemp = new SM_BookingTemp();
                        #region process Customer
                        var customer = db.Customers.FirstOrDefault(w => w.Phone == bill.customerPhone && w.IsDelete == 0);
                        if (customer == null)
                        {
                            customer = new Customer();
                            customer.Fullname = bill.customerName;
                            customer.Phone = bill.customerPhone;
                            customer.SalonId = bill.salonID;
                            customer.CreatedDate = DateTime.Now;
                            customer.IsDelete = 0;
                            if ((bill.snDay != null && bill.snMonth != null && bill.snYear != null) || (bill.snDay != "" && bill.snMonth != "" && bill.snYear != ""))
                            {
                                customer.SN_day = Convert.ToInt32(bill.snDay);
                                customer.SN_month = Convert.ToInt32(bill.snMonth);
                                customer.SN_year = Convert.ToInt32(bill.snYear);
                            }

                            db.Customers.AddOrUpdate(customer);
                            db.SaveChanges();
                        }
                        else
                        {
                            //Update ten khach hang khi ban ghi khach hang da ton tai nhung khong co ten trang bang Customer
                            if (customer.Fullname == null)
                            {
                                customer.Fullname = bill.customerName.Trim();
                            }
                            else if (customer.Fullname.Trim().Length == 0)
                            {
                                customer.Fullname = bill.customerName.Trim();
                            }
                            if ((bill.snDay != null && bill.snMonth != null && bill.snYear != null) || (bill.snDay != "" && bill.snMonth != "" && bill.snYear != ""))
                            {
                                customer.SN_day = Convert.ToInt32(bill.snDay);
                                customer.SN_month = Convert.ToInt32(bill.snMonth);
                                customer.SN_year = Convert.ToInt32(bill.snYear);
                                customer.ModifiedDate = DateTime.Now;
                            }
                            db.Customers.AddOrUpdate(customer);
                            db.SaveChanges();
                        }
                        #endregion
                        #region processs bookingTemp     
                        var countBookTemp = db.SM_BookingTemp.Where(p => p.CustomerPhone.Trim() == bill.customerPhone.Trim() && p.DatedBook >= EntityFunctions.TruncateTime(DateTime.Now) && p.IsDelete == 0);
                        if (countBookTemp.Count() > 0 && countBookTemp.OrderByDescending(a => a.Id).FirstOrDefault().IsMakeBill != true)
                        {

                            message.success = false;
                            message.status = "update";
                            message.message = "Khách đã đặt lịch  . Bạn chắc chắn muốn đổi lịch cho khách không?";
                            return message;
                        }
                        else if (countBookTemp.Count() > 0 && countBookTemp.OrderByDescending(a => a.Id).FirstOrDefault().IsMakeBill == true && datedBook == countBookTemp.OrderByDescending(a => a.Id).FirstOrDefault().DatedBook)
                        {

                            message.success = false;
                            message.status = "update";
                            message.message = "Khách đã đặt lịch  . Bạn chắc chắn muốn đổi lịch cho khách không?";
                            return message;
                        }
                        else
                        {
                            bookingTemp.CustomerName = bill.customerName.Trim();
                            bookingTemp.CustomerPhone = bill.customerPhone.Trim();
                            bookingTemp.HourId = bill.hourID;
                            bookingTemp.SubHour = bill.subHourID;
                            bookingTemp.SalonId = bill.salonID;
                            bookingTemp.StylistId = bill.stylistID;
                            bookingTemp.DatedBook = datedBook;
                            bookingTemp.IsDelete = 0;
                            bookingTemp.CreatedDate = DateTime.Now;
                            bookingTemp.IsBookAtSalon = bill.IsBookAtSalon;
                            bookingTemp.IsBookOnline = !bill.IsBookAtSalon;
                            bookingTemp.CustomerScore = customer.Score;
                            bookingTemp.CustomerId = customer.Id;
                            if (bill.IsAutoStylist == true)
                            {
                                // Trường hợp ghi nhận tại salon khách có book Stylist
                                bookingTemp.IsAutoStylist = bill.IsAutoStylist;
                                bookingTemp.IsBookStylist = !bill.IsAutoStylist;
                            }
                            else
                            {
                                bookingTemp.IsAutoStylist = false;
                                bookingTemp.IsBookStylist = true;
                            }
                            if (datedBook >= DateTime.Now.Date && datedBook <= DateTime.Now.AddDays(timeConfig).Date)
                            {
                                db.SM_BookingTemp.Add(bookingTemp);
                                db.SaveChanges();
                            }

                        }
                        #endregion
                        #region process booking
                        var countBook = db.Bookings.Where(p => p.CustomerPhone.Trim() == bill.customerPhone.Trim() && p.DatedBook == datedBook && p.HourId == bill.hourID && p.IsDelete != 1);
                        if (countBook.Count() > 0)
                        {
                            message.success = false;
                            message.status = "update";
                            message.message = "Lỗi. Lỗi không đồng nhất dữ liệu , bạn vui lòng báo cho nhà phát triển";
                            return message;
                        }
                        else
                        {
                            var booking = new Booking();
                            booking.CustomerName = bill.customerName.Trim();
                            booking.CustomerPhone = bill.customerPhone.Trim();
                            booking.HourId = bill.hourID;
                            booking.SubHour = bill.subHourID;
                            booking.SalonId = bill.salonID;
                            booking.StylistId = bill.stylistID;
                            booking.DatedBook = datedBook;
                            booking.IsDelete = 0;
                            booking.CreatedDate = DateTime.Now;
                            booking.IsBookAtSalon = bill.IsBookAtSalon;
                            booking.IsBookOnline = !bill.IsBookAtSalon;
                            booking.CustomerScore = customer.Score;
                            booking.CustomerId = customer.Id;
                            try
                            {
                                // Trường hợp ghi nhận tại salon khách có book Stylist
                                booking.IsAutoStylist = bill.IsAutoStylist;
                                booking.IsBookStylist = !bill.IsAutoStylist;
                            }
                            catch
                            {
                                booking.IsAutoStylist = false;
                                booking.IsBookStylist = true;
                            }
                            booking.BookingTempId = bookingTemp.Id;
                            if (datedBook >= DateTime.Now.Date && datedBook <= DateTime.Now.AddDays(timeConfig).Date)
                            {
                                db.Bookings.Add(booking);
                                db.SaveChanges();
                            }
                            message.success = true;
                            message.message = serializer.Serialize(bookingTemp);
                            message.dtShow = serializer.Serialize(customer);
                            return message;
                        }
                        #endregion
                    }

                }

                catch (Exception ex)
                {
                    message.success = false;
                    message.message = ex.Message;
                    return message;
                }
                return message;
            }
        }

        /// <summary>
        /// Update lại thông tin cho khách (drag and drop cell timeline)
        /// </summary>
        /// <param name="customerPhone"></param>
        /// <param name="bookingId"></param>
        /// <param name="stylistId"></param>
        /// <param name="hourId"></param>
        /// <returns></returns>
        [WebMethod]
        public static object updateBooking(string customerPhone, int bookingId, int stylistId, int hourId, int subHourID)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var message = new Library.Class.cls_message();
                var serializer = new JavaScriptSerializer();
                try
                {
                    var Respone = new cls_updatebookingDrop();
                    var datebook = DateTime.Now.Date;
                    var booking = db.Bookings.AsNoTracking().FirstOrDefault(p => p.BookingTempId == bookingId && p.IsDelete == 0);
                    if (booking != null)
                    {
                        var customer = db.Customers.FirstOrDefault(w => w.Id == booking.CustomerId);
                        // update bill
                        var objBill = db.BillServices.FirstOrDefault(a => a.BookingId == booking.Id && a.IsDelete == 0);

                        if (objBill != null)
                        {
                            var pending = objBill.Pending;
                            if (pending == 1)
                            {
                                var objHourFrameOld = db.BookHour_Sub.FirstOrDefault(a => a.IsDelete == false && a.Publish == true && a.SubHourId == booking.SubHour);
                                var objHourFrameNew = db.BookHour_Sub.FirstOrDefault(a => a.IsDelete == false && a.Publish == true && a.SubHourId == subHourID);
                                if (objHourFrameOld == null && objHourFrameNew == null)
                                {
                                    message.success = false;
                                    message.status = "error";
                                    message.messageNoti = "Lỗi. Dữ liệu truyền lên không đúng.";
                                    return message;
                                }

                                var mainHour = objHourFrameOld.SubHourId;
                                var subHour = objHourFrameNew.SubHourId;
                                int x = 0, y = 0;
                                x = mainHour + 1;
                                y = mainHour - 1;
                                if (subHour != x && subHour != y && objHourFrameNew.HourId != objHourFrameOld.HourId)
                                {
                                    message.success = false;
                                    message.status = "error";
                                    message.messageNoti = "Chỉ có thể chuyển trong cùng khung giờ khi đã IN BILL !";
                                    return message;
                                }
                                objBill.Staff_Hairdresser_Id = stylistId;
                                db.BillServices.AddOrUpdate(objBill);
                            }
                            else if (pending == 0)
                            {
                                message.success = true;
                                message.status = "update";
                                message.messageNoti = "Khung giờ đặt lịch đã được thanh toán, bạn không được thay đổi";
                                return message;
                            }

                        }

                        //update tbl Booking
                        booking.HourId = hourId;
                        booking.SubHour = subHourID;
                        booking.StylistId = stylistId;
                        booking.ModifiedDate = DateTime.Now;
                        db.Bookings.AddOrUpdate(booking);
                        db.SaveChanges();

                        var bookingTemp = db.SM_BookingTemp.AsNoTracking().FirstOrDefault(p => p.Id == bookingId && p.IsDelete == 0);
                        if (bookingTemp != null)
                        {
                            //update tbl SM_BookingTemp
                            bookingTemp.Order = bookingTemp.Order;
                            bookingTemp.HourId = hourId;
                            bookingTemp.SubHour = subHourID;
                            bookingTemp.StylistId = stylistId;
                            bookingTemp.ModifiedDate = DateTime.Now;

                            db.SM_BookingTemp.AddOrUpdate(bookingTemp);
                            db.SaveChanges();
                            //var result = db.SaveChanges();
                        }

                        // respone 
                        var objHour = db.BookHour_Sub.AsNoTracking().FirstOrDefault(a => a.SubHourId == bookingTemp.SubHour && a.IsDelete == false && a.Publish == true).HourFrame;
                        var objStaff = db.Staffs.AsNoTracking().FirstOrDefault(a => a.Id == bookingTemp.StylistId).Fullname;

                        Respone = new cls_updatebookingDrop
                        {
                            billId = objBill == null ? 0 : objBill.Id,
                            bookingId = bookingTemp.Id,
                            CustomerName = bookingTemp.CustomerName,
                            CustomerPhone = bookingTemp.CustomerPhone,
                            HourId = bookingTemp.HourId,
                            IsAutoStylist = bookingTemp.IsAutoStylist,
                            IsBookAtSalon = bookingTemp.IsBookAtSalon,
                            IsCall = bookingTemp.IsCall,
                            IsCallTime = bookingTemp.IsCallTime,
                            IsCallTimeModified = bookingTemp.IsCallTimeModified,
                            Order = bookingTemp.Order,
                            StylistId = bookingTemp.StylistId,
                            HourFrame = objHour,
                            stylistName = objStaff,
                            CustomerScore = bookingTemp.CustomerScore,
                            SubHourId = bookingTemp.SubHour,
                            TextNote1 = bookingTemp.TextNote1 ?? "",
                            TextNote2 = bookingTemp.TextNote2 ?? "",
                            salonId = (int)bookingTemp.SalonId,
                            snDay = customer.SN_day,
                            snMonth = customer.SN_month,
                            snYear = customer.SN_year,
                            //RatingNote = GetRatingNote(booking.Id)


                        };
                        message.success = true;
                        message.status = "update";
                        message.message = serializer.Serialize(Respone);
                        return message;
                    }
                    else
                    {
                        message.success = false;
                        message.messageNoti = " Lỗi không tìm thấy bản ghi! Vui lòng liên hệ với nhà phát triển.  ";
                        return message;
                    }
                }

                catch (Exception ex)
                {
                    message.success = false;
                    message.message = ex.Message;
                    message.messageNoti = "Đã có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển!";
                    return message;
                }
            }
        }

        /// <summary>
        /// Hủy bản ghi [BillService] trường hợp update kéo thả trên timeline
        /// Chỉ áp dụng hủy cho các bản ghi đang pending (chưa checkout)
        /// Đảm bảo không ảnh hưởng đến doanh số/doanh thu đã ghi nhận
        /// </summary>
        /// <param name="booking"></param>
        public void updateBooking_DeleteBill(Booking booking)
        {
            try
            {
                var bill = this.db.BillServices.FirstOrDefault(w => w.BookingId == booking.Id);
                if (bill != null && bill.Pending == 1)
                {
                    // Delete in [BillService]
                    bill.IsDelete = 1;
                    bill.ModifiedDate = DateTime.Now;
                    db.BillServices.AddOrUpdate(bill);
                    db.SaveChanges();

                    // Delete in [SM_BillTemp]
                    var billTemp = this.db.SM_BillTemp.FirstOrDefault(w => w.BillServiceID == bill.Id);
                    if (billTemp != null)
                    {
                        billTemp.IsDelete = 1;
                        billTemp.ModifiedDate = DateTime.Now;
                        db.SM_BillTemp.AddOrUpdate(billTemp);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Lấy số thứ tự của khách
        /// Áp dụng với các khách được check là đã đến
        /// </summary>
        /// <param name="bookingTemp"></param>
        /// <param name="hourID"></param>
        /// <returns></returns>
        public int? getOrderBooking(SM_BookingTemp bookingTemp, int hourID, int subHourID)
        {
            try
            {
                int? orderRet = null;
                string sql = @"select MAX([Order]) from SM_BookingTemp
                                    where IsDelete = 0 and DatedBook = '" + Convert.ToDateTime(DatedBook.Text, culture) + "'and SalonId =" + bookingTemp.SalonId + " and IsCall = 1 and HourId =" + hourID + "and SubHour = " + subHourID + "";
                int? order = this.db.Database.SqlQuery<int?>(sql).FirstOrDefault();
                if (order != null && bookingTemp.IsCall == true)
                {
                    if (bookingTemp.Order == order)
                    {
                        orderRet = order;
                    }
                    else if (bookingTemp.SubHour != subHourID)
                    {
                        orderRet = order + 1;
                    }
                }
                else if (order == null && bookingTemp.IsCall == true)
                {
                    orderRet = 1;
                }
                else if (bookingTemp.IsCall == false)
                {
                    orderRet = null;
                }

                return orderRet;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// updateBookTimeline
        /// </summary>
        /// <param name="billClient"></param>
        /// <returns></returns>
        [WebMethod]
        public static object updateStylistBookTimeline(string billClient)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var message = new Library.Class.cls_message();
                var serializer = new JavaScriptSerializer();
                try
                {
                    var bill = serializer.Deserialize<cls_timeline_bill>(billClient);
                    var datedBook = Convert.ToDateTime(bill.datedBook, culture);
                    var bookingTemp = new SM_BookingTemp();
                    var booking = new Booking();
                    var objBillservice = new BillService();
                    if (bill != null)
                    {
                        // process booking 
                        bookingTemp = db.SM_BookingTemp.AsNoTracking().OrderByDescending(a => a.Id).FirstOrDefault(a => a.CustomerPhone == bill.customerPhone && a.IsDelete == 0 && a.DatedBook >= EntityFunctions.TruncateTime(DateTime.Now));
                        // check  bookingTemp
                        if (bookingTemp == null)
                        {
                            message.success = false;
                            message.status = "error";
                            message.message = "Lỗi. Lịch đặt không tồn tại  ";
                            return message;
                        }
                        // check  booking
                        booking = db.Bookings.AsNoTracking().FirstOrDefault(a => a.BookingTempId == bookingTemp.Id && a.IsDelete == 0);
                        if (booking == null)
                        {
                            message.success = false;
                            message.status = "error";
                            message.message = "Lỗi. Dữ liệu không đồng nhất, Xin vui lòng báo nhóm phát triển";
                            return message;
                        }
                        // check  billservice
                        // && EntityFunctions.TruncateTime(a.CreatedDate) == EntityFunctions.TruncateTime(datedBook)
                        objBillservice = db.BillServices.AsNoTracking().FirstOrDefault(a => a.IsDelete == 0 && a.BookingId == booking.Id);
                        // process update Billservice
                        if (objBillservice != null)
                        {
                            var pending = objBillservice.Pending;
                            if (pending == 0)
                            {
                                message.success = false;
                                message.status = "error";
                                message.message = "Bill đã được checkOut bạn không thể cập nhật lại lịch đặt";
                                return message;
                            }
                            if (objBillservice.SalonId != bill.salonID)
                            {
                                var NameSalon = db.Tbl_Salon.FirstOrDefault(a => a.Id == objBillservice.SalonId).Name;
                                message.success = false;
                                message.status = "error";
                                message.message = "Số điện thoại đã IN BILL ở SALON " + NameSalon + "! Bạn có thể dùng chức năng in Bill trực tiếp.";
                                return message;
                            }
                            var objHourFrameOld = db.BookHour_Sub.FirstOrDefault(a => a.IsDelete == false && a.Publish == true && a.SubHourId == bookingTemp.SubHour).HourFrame;
                            var objHourFrameNew = db.BookHour_Sub.FirstOrDefault(a => a.IsDelete == false && a.Publish == true && a.SubHourId == bill.subHourID).HourFrame;
                            if (objHourFrameOld == null && objHourFrameNew == null)
                            {
                                message.success = false;
                                message.status = "error";
                                message.message = "Lỗi. Dữ liệu truyền lên không đúng.";
                                return message;
                            }
                            else if (objHourFrameOld != objHourFrameNew && datedBook == booking.DatedBook)
                            {
                                message.success = false;
                                message.status = "error";
                                message.message = "Chỉ có thể chuyển trong cùng khung giờ khi đã IN BILL !";
                                return message;
                            }

                            objBillservice.Staff_Hairdresser_Id = bill.stylistID;
                            objBillservice.ModifiedDate = DateTime.Now;
                            db.BillServices.AddOrUpdate(objBillservice);
                        }
                        // process update bookingTemp
                        bookingTemp.StylistId = bill.stylistID;
                        bookingTemp.ModifiedDate = DateTime.Now;
                        bookingTemp.DatedBook = datedBook;
                        if (bookingTemp.SalonId != bill.salonID)
                        {
                            bookingTemp.IsCall = null;
                            bookingTemp.IsCallTime = null;
                            bookingTemp.IsCallTimeModified = null;
                            bookingTemp.Order = null;
                        }
                        bookingTemp.SalonId = bill.salonID;
                        bookingTemp.HourId = bill.hourID;
                        bookingTemp.SubHour = bill.subHourID;
                        db.SM_BookingTemp.AddOrUpdate(bookingTemp);

                        // process update booking
                        booking.StylistId = bill.stylistID;
                        booking.DatedBook = datedBook;
                        booking.ModifiedDate = DateTime.Now;
                        booking.SalonId = bill.salonID;
                        booking.HourId = bill.hourID;
                        booking.SubHour = bill.subHourID;
                        db.Bookings.AddOrUpdate(booking);

                        int result = 0;
                        result = db.SaveChanges();
                        if (result == 0)
                        {
                            message.success = false;
                            message.status = "error";
                            message.message = "Lỗi. khi lưu dữ liệu";
                        }

                        message.success = true;
                        message.status = "update";
                        message.message = serializer.Serialize(booking);
                        return message;
                    }

                    else
                    {
                        message.success = false;
                        message.status = "error";
                        message.message = "Lỗi. Dữ liệu truyền lên không đúng.";
                        return message;
                    }
                }
                catch (Exception ex)
                {
                    message.success = false;
                    message.message = ex.Message;
                    return message;
                }
            }
        }

        /// <summary>
        /// Đặt lịch trực tiếp cho khách
        /// </summary>
        /// <param name="billClient"></param>
        /// <returns></returns>
        [WebMethod]
        public static object insertBooking(string billClient, int isDelete)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var message = new Library.Class.cls_message();
                var serializer = new JavaScriptSerializer();
                try
                {
                    var bill = serializer.Deserialize<cls_timeline_bill>(billClient);
                    var datedBook = Convert.ToDateTime(bill.datedBook, culture);
                    var bookingTemp = new SM_BookingTemp();
                    if (bill != null)
                    {
                        var countBookTemp = db.SM_BookingTemp.Where(p => p.CustomerPhone.Trim() == bill.customerPhone.Trim() && p.DatedBook == datedBook && p.IsDelete != 1).ToList();
                        if (countBookTemp.Count() > 0)
                        {
                            if (isDelete == 1)
                            {
                                foreach (var item in countBookTemp)
                                {
                                    var deleteBook = db.SM_BookingTemp.Single(p => p.Id == item.Id);
                                    deleteBook.IsDelete = 1;
                                    //deleteBook.NoteDelete = noteCancel;
                                    deleteBook.ModifiedDate = DateTime.Now;
                                }
                                db.SaveChanges();
                            }
                            //var bookingTemp = new SM_BookingTemp();
                            bookingTemp.CustomerName = bill.customerName.Trim();
                            bookingTemp.CustomerPhone = bill.customerPhone.Trim();
                            bookingTemp.HourId = bill.hourID;
                            bookingTemp.SubHour = bill.subHourID;
                            bookingTemp.SalonId = bill.salonID;
                            bookingTemp.StylistId = bill.stylistID;
                            bookingTemp.DatedBook = datedBook;
                            bookingTemp.IsDelete = 0;
                            bookingTemp.CreatedDate = DateTime.Now;
                            bookingTemp.IsBookAtSalon = true;
                            bookingTemp.IsBookOnline = false;

                            try
                            {
                                // Trường hợp ghi nhận tại salon khách có book Stylist
                                bookingTemp.IsAutoStylist = bill.IsAutoStylist;
                                bookingTemp.IsBookStylist = !bill.IsAutoStylist;
                            }
                            catch
                            {
                                bookingTemp.IsAutoStylist = false;
                                bookingTemp.IsBookStylist = true;
                            }
                            if (datedBook >= DateTime.Now.Date && datedBook <= DateTime.Now.AddDays(timeConfig).Date)
                            {
                                db.SM_BookingTemp.AddOrUpdate(bookingTemp);
                                db.SaveChanges();
                            }
                            message.success = true;
                            message.message = serializer.Serialize(bookingTemp);

                        }


                        var countBook = db.Bookings.Where(p => p.CustomerPhone.Trim() == bill.customerPhone.Trim() && p.DatedBook == datedBook && p.IsDelete != 1).ToList();
                        if (countBook.Count() > 0)
                        {
                            if (isDelete == 1)
                            {
                                foreach (var item in countBook)
                                {
                                    var deleteBook = db.Bookings.Single(p => p.Id == item.Id);
                                    deleteBook.IsDelete = 1;
                                    // deleteBook.NoteDelete = noteCancel;
                                    deleteBook.ModifiedDate = DateTime.Now;
                                }
                                db.SaveChanges();
                            }
                            // Insert vào bảng Booking
                            var booking = new Booking();
                            booking.CustomerName = bill.customerName.Trim();
                            booking.CustomerPhone = bill.customerPhone.Trim();
                            booking.HourId = bill.hourID;
                            booking.SubHour = bill.subHourID;
                            booking.SalonId = bill.salonID;
                            booking.StylistId = bill.stylistID;
                            booking.DatedBook = datedBook;
                            booking.IsDelete = 0;
                            booking.CreatedDate = DateTime.Now;
                            booking.BookingTempId = bookingTemp.Id;
                            booking.IsBookAtSalon = true;
                            booking.IsBookOnline = false;

                            try
                            {
                                // Trường hợp ghi nhận tại salon khách có book Stylist
                                booking.IsAutoStylist = bill.IsAutoStylist;
                                booking.IsBookStylist = !bill.IsAutoStylist;
                            }
                            catch
                            {
                                booking.IsAutoStylist = false;
                                booking.IsBookStylist = true;
                            }
                            if (datedBook >= DateTime.Now.Date && datedBook <= DateTime.Now.AddDays(timeConfig).Date)
                            {
                                booking.BookingTempId = bookingTemp.Id;
                                db.Bookings.AddOrUpdate(booking);
                            }

                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        message.success = false;
                        message.status = "error";
                        message.message = "Lỗi. Dữ liệu truyền lên không đúng.";
                    }
                }
                catch (Exception ex)
                {
                    message.success = false;
                    message.message = ex.Message;
                    return message;
                }

                return message;
            }
        }

        /// <summary>
        /// Insert TokenID
        /// </summary>
        /// <param name="currentToken"></param>
        /// <returns></returns>
        [WebMethod]
        public static object updateTokenID(string currentToken)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var error = 0;
                var message = new Library.Class.cls_message();
                var record = db.Realtime_Firebase_TokenID.FirstOrDefault(w => w.CurrentToken == currentToken);
                if (record == null)
                {
                    record = new Realtime_Firebase_TokenID();
                    record.CurrentToken = currentToken;
                    record.CreatedTime = DateTime.Now;
                    record.IsDelete = false;
                    record.Active = true;

                    db.Realtime_Firebase_TokenID.AddOrUpdate(record);
                    error += db.SaveChanges() > 0 ? 0 : 1;
                    if (error == 0)
                    {
                        message.success = true;
                        message.message = "Success!";
                    }
                    else
                    {
                        message.success = false;
                        message.message = "Failed!";
                    }
                }
                else
                {
                    message.success = true;
                    message.message = "Success! Is set TokenID";
                }

                return message;
            }
        }

        /// <summary>
        /// Insert bản ghi chi tiết dịch vụ
        /// </summary>
        /// <param name="db"></param>
        /// <param name="billID"></param>
        /// <param name="listService"></param>
        public static void insertBillFlow(Solution_30shineEntities db, int salonID, int billID, int billServiceID, List<cls_timeline_service> listService)
        {
            if (listService.Count > 0)
            {
                var billTempFlow = new SM_BillTemp_FlowService();
                var billFlowService = new FlowService();
                var services = db.Services.Where(w => w.IsDelete != 1 && w.Publish == 1).ToList();
                var service = new MODEL.ENTITY.EDMX.Service();
                foreach (var v in listService)
                {
                    service = services.FirstOrDefault(w => w.Id == v.Id);
                    // I. SM_BillTemp_FlowService
                    billTempFlow = new SM_BillTemp_FlowService();
                    billTempFlow.SalonId = salonID;
                    billTempFlow.BillId = billID;
                    billTempFlow.ServiceId = v.Id;
                    billTempFlow.Quantity = 1;
                    billTempFlow.IsDelete = 0;
                    billTempFlow.CreatedDate = DateTime.Now;
                    if (service != null)
                    {
                        billTempFlow.Price = service.Price;
                        billTempFlow.VoucherPercent = service.VoucherPercent;
                        billTempFlow.CoefficientRating = service.CoefficientRating;
                    }

                    db.SM_BillTemp_FlowService.AddOrUpdate(billTempFlow);
                    db.SaveChanges();

                    // II. Flowservice
                    billFlowService = new FlowService();
                    billFlowService.SalonId = salonID;
                    billFlowService.BillId = billServiceID;
                    billFlowService.ServiceId = v.Id;
                    billFlowService.Quantity = 1;
                    billFlowService.IsDelete = 0;
                    billFlowService.CreatedDate = DateTime.Now;
                    if (service != null)
                    {
                        billFlowService.Price = service.Price;
                        billFlowService.VoucherPercent = service.VoucherPercent;
                        billFlowService.CoefficientRating = service.CoefficientRating;
                    }

                    db.FlowServices.AddOrUpdate(billFlowService);
                    db.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Generate bill code
        /// </summary>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public static string GenBillCode(int salonId, string prefix, int template = 4)
        {
            using (var db = new Solution_30shineEntities())
            {
                string code = "";
                string temp = "";
                int integer;
                var Where = PredicateBuilder.True<BillService>();
                DateTime _FromDate = DateTime.Today;
                DateTime _ToDate = DateTime.Today.AddDays(1);
                Where = Where.And(w => w.IsDelete != 1 && w.CreatedDate >= _FromDate && w.CreatedDate < _ToDate);
                if (salonId > 0)
                {
                    Where = Where.And(w => w.SalonId == salonId);
                }
                var LastBill = db.BillServices.AsExpandable().Where(Where).OrderByDescending(w => w.Id).Take(1).ToList();
                if (LastBill.Count > 0)
                {
                    if (LastBill[0].BillCode != null && LastBill[0].BillCode != "")
                        temp = (int.TryParse(LastBill[0].BillCode.Substring(LastBill[0].BillCode.Length - template, template), out integer) ? ++integer : 0).ToString();
                    else
                        temp = "1";
                }
                else
                {
                    temp = "1";
                }

                int len = temp.Length;
                for (var i = 0; i < template; i++)
                {
                    if (len > 0)
                    {
                        code = temp[--len].ToString() + code;
                    }
                    else
                    {
                        code = "0" + code;
                    }
                }

                return prefix + String.Format("{0:ddMMyy}", DateTime.Now) + code;
            }
        }

        /// <summary>
        /// convert phone number
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        private static string ChangePhone(string phone)
        {
            var phone_Replace = "";
            if (phone.Length == 11)
            {
                switch (phone.Substring(0, 4))
                {
                    //mobifone
                    case "0120":
                        phone_Replace = phone.Replace(phone.Substring(0, 4), "070");
                        break;
                    case "0121":
                        phone_Replace = phone.Replace(phone.Substring(0, 4), "079");
                        break;
                    case "0122":
                        phone_Replace = phone.Replace(phone.Substring(0, 4), "077");
                        break;
                    case "0126":
                        phone_Replace = phone.Replace(phone.Substring(0, 4), "076");
                        break;
                    case "0128":
                        phone_Replace = phone.Replace(phone.Substring(0, 4), "078");
                        break;
                    //vinaphone
                    case "0123":
                        phone_Replace = phone.Replace(phone.Substring(0, 4), "083");
                        break;
                    case "0124":
                        phone_Replace = phone.Replace(phone.Substring(0, 4), "084");
                        break;
                    case "0125":
                        phone_Replace = phone.Replace(phone.Substring(0, 4), "085");
                        break;
                    case "0127":
                        phone_Replace = phone.Replace(phone.Substring(0, 4), "081");
                        break;
                    case "0129":
                        phone_Replace = phone.Replace(phone.Substring(0, 4), "082");
                        break;
                    //viettel
                    case "0162":
                        phone_Replace = phone.Replace(phone.Substring(0, 4), "032");
                        break;
                    case "0163":
                        phone_Replace = phone.Replace(phone.Substring(0, 4), "033");
                        break;
                    case "0164":
                        phone_Replace = phone.Replace(phone.Substring(0, 4), "034");
                        break;
                    case "0165":
                        phone_Replace = phone.Replace(phone.Substring(0, 4), "035");
                        break;
                    case "0166":
                        phone_Replace = phone.Replace(phone.Substring(0, 4), "036");
                        break;
                    case "0167":
                        phone_Replace = phone.Replace(phone.Substring(0, 4), "037");
                        break;
                    case "0168":
                        phone_Replace = phone.Replace(phone.Substring(0, 4), "038");
                        break;
                    case "0169":
                        phone_Replace = phone.Replace(phone.Substring(0, 4), "039");
                        break;
                    //vietnammobile
                    case "0186":
                        phone_Replace = phone.Replace(phone.Substring(0, 4), "056");
                        break;
                    case "0188":
                        phone_Replace = phone.Replace(phone.Substring(0, 4), "058");
                        break;
                    //gmobile
                    case "0199":
                        phone_Replace = phone.Replace(phone.Substring(0, 4), "059");
                        break;
                    default:
                        phone_Replace = phone;
                        break;
                }
                return phone_Replace;
            }
            else return phone;
        }

        /// <summary>
        /// Lấy dữ liệu Stylist
        /// </summary>
        /// <returns></returns>
        private List<cls_timeline_stylist> GetByDataTimeline(DateTime Date)
        {
            var data = new List<cls_timeline_stylist>();

            if (salonID > 0)
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var WorkDate = String.Format("{0:yyyy/MM/dd}", Date);
                var fromDate = WorkDate;
                var toDate = String.Format("{0:yyyy/MM/dd}", Date.AddDays(+1));
                string SQLInforStaff = @"declare
	                                @SalonId int ,
	                                @WorkDate datetime
	                                set @SalonId = " + salonID + @"
	                                set @WorkDate = '" + WorkDate + @"';
 
                                   WITH a AS (
                                    SELECT StaffId,GroupLevelId,Score,Fullname,TeamId,IsEnroll,isAccountLogin,
		                                CONVERT( int,LTRIM(RTRIM(m.n.value('.[1]','int')))) AS HourId
	                                FROM
	                                (
		                                SELECT Staff.Fullname,Staff.TeamId, Staff.Score, FlowTimeKeeping.IsEnroll,/*CAST(ISNULL(Staff.AdditionStaff,0) as int) as*/ cast(isnull(Staff.isAccountLogin, 0) as int) as isAccountLogin, Staff.GroupLevelId, FlowTimeKeeping.StaffId, FlowTimeKeeping.WorkTimeId,CAST('<XMLRoot><RowData>' + REPLACE(FlowTimeKeeping.HourIds,',','</RowData><RowData>') + '</RowData></XMLRoot>' AS XML) AS x
		                                FROM FlowTimeKeeping 
		                                inner join Staff on FlowTimeKeeping.StaffId = Staff.Id
		                                where FlowTimeKeeping.IsDelete = 0 and FlowTimeKeeping.SalonId = @SalonId and FlowTimeKeeping.WorkDate = @WorkDate and IsEnroll = 1 and Staff.[Type] =1 
	                                )t
	                                CROSS APPLY x.nodes('/XMLRoot/RowData')m(n) 
									), 
									b AS (
									SELECT Id AS StaffId,GroupLevelId,Score,Fullname,TeamId,CAST(0 AS BIT) AS IsEnroll,isAccountLogin, 0 AS HourId FROM dbo.Staff WHERE SalonId = @SalonId AND IsDelete = 0 AND Active = 1 AND isAccountLogin = 1 AND Type = 1
									)
									SELECT * FROM  a
									--UNION ALL
									--SELECT * FROM b
                                    ";
                string SQLBookBill = @"declare
	                                @SalonId int,
	                                @fromDate datetime,
	                                @toDate datetime
	                                set @SalonId = " + salonID + @"
	                                set @fromDate = '" + fromDate + @"'
	                                set @toDate = '" + toDate + @"'
                                    select booking.Id, booking.CustomerName , booking.CustomerPhone,cm.MemberType, cm.MemberEndTime, cm.SN_day, cm.SN_month, cm.SN_year, booking.HourId, booking.StylistId, booking.IsCall, booking.IsCallTime,booking.IsCallTimeModified , booking.IsBookViaBill, booking.IsBookAtSalon ,booking.IsBookStylist,booking.IsBookOnline, booking.[Order], booking.CustomerScore, booking.TextNote1,booking.TextNote2, booking.SubHour,booking.IsMakeBill, booking.SalonNote, booking.IsAutoStylist,
                                    billService.Id as BillId, 
                                    billService.InProcedureTime, billService.InProcedureTimeModifed, billService.CompleteBillTime, billService.CreatedDate as BillCreatedDate, billService.HCItem,billService.Pending
									,
									CAST( (CASE	
									WHEN c.CustomerTypeId = 2 THEN 2
									WHEN c1.CustomerTypeId  = 1 THEN 1
									ELSE 0
									END ) AS int) AS CustomerTypeId
                                    from SM_BookingTemp as booking
                                    inner join Booking as bookingRead on booking.Id = bookingRead.BookingTempId
                                    left join BillService as billService on billService.BookingId = bookingRead.Id  and billService.CreatedDate > @fromDate and billService.CreatedDate < @toDate and billService.SalonId = @SalonId and billService.IsDelete = 0
									left join SpecialCustomer as c on c.CustomerId = booking.CustomerId and c.IsDelete = 0 and c.CustomerTypeId = 2
									left join SpecialCustomer as c1 on c1.CustomerId = booking.CustomerId and c.IsDelete = 0 and c.CustomerTypeId =1
									left join SpecialCusDetail as sd on c1.Id = sd.SpecialCusId and sd.IsDelete = 0 and sd.QuantityInvited > 0 
                                    left join Customer cm on cm.Id = booking.CustomerId
                                    where 
                                    booking.IsDelete = 0
                                    and booking.DatedBook = @fromDate and booking.SalonId = @SalonId  ";

                var listBookHour = db.BookHours.Where(w => w.SalonId == salonID && w.IsDelete == 0).ToList();
                //var virtualStylist = db.Staffs.Where(w => w.SalonId == salonID && w.Active == 1 && w.AdditionStaff == true && w.IsDelete == 0 && w.Type == 1).ToList();
                //var listVirtual = new List<cls_infor_Staff>();
                //foreach (var item in listBookHour)
                //{
                //    //List<cls_infor_Staff> ls = new List<cls_infor_Staff>();
                //    foreach (var v in virtualStylist)
                //    {
                //        cls_infor_Staff obj = new cls_infor_Staff();
                //        obj.StaffId = v.Id;
                //        obj.HourId = item.Id;
                //        obj.GroupLevelId = v.GroupLevelId;
                //        obj.Fullname = v.Fullname;
                //        obj.isAccountLogin = Convert.ToInt32(v.AdditionStaff);
                //        obj.IsEnroll = false;
                //        obj.Score = v.Score;
                //        obj.TeamId = v.TeamId;
                //        listVirtual.Add(obj);
                //    }
                //}

                //var dbsdf = db.TEST222
                // lay danh sach cham cong , khung gio lam viec
                var ListFlowTimeKeepingStore = db.Database.SqlQuery<cls_infor_Staff>(SQLInforStaff);//.Union(listVirtual);
                // lay danh sach booking va bill
                var ListBookBill = db.Database.SqlQuery<cls_Booking_Bill>(SQLBookBill).ToList();

                //var test = ListBookBill.Where(a => a.CustomerPhone == "01698572033").ToList();
                // lay danh sach khung gio
                var ListSubHour = db.Database.SqlQuery<BookHour_Sub>("select * from BookHour_Sub where IsDelete = 0 and Publish = 1 and SalonId = " + salonID + "").ToList();
                var ListFlowTimeKeeping = (from a in ListFlowTimeKeepingStore
                                           join b in ListSubHour on a.HourId equals b.HourId into g
                                           from b in g.DefaultIfEmpty()
                                           select new cls_infor_Staff
                                           {
                                               Fullname = a.Fullname,
                                               GroupLevelId = a.GroupLevelId,
                                               HourId = a.HourId,
                                               IsEnroll = a.IsEnroll,
                                               Score = a.Score,
                                               StaffId = a.StaffId,
                                               SubHourId = b == null ? 0 : b.SubHourId,
                                               TeamId = a.TeamId,
                                               isAccountLogin = a.isAccountLogin
                                           }).ToList();
                // return HouFrame header timeline
                listHour = ListSubHour.OrderBy(a => a.HourFrame).Select(a => Convert.ToString(a.HourFrame)).ToList();
                var ListStylis = ListFlowTimeKeeping.GroupBy(a => new
                {
                    a.StaffId
                }).Select(group => group.First()).ToList();
                IEnumerable<int?> StaffIds = ListStylis.Select(a => a.StaffId).Cast<int?>();

                // lay sanh sach Staff da book nhung khong cham cong
                var ListStylistNotEnroll = ListBookBill.Where(a => !StaffIds.Contains(a.StylistId)).Select(a => a.StylistId).ToList();

                if (ListStylistNotEnroll.Count > 0)
                {
                    var StrStaffIds = string.Join(", ", ListStylistNotEnroll);
                    string strSTaff = @"select Id as StaffId, GroupLevelId, Score,Fullname,TeamId from Staff where Id in (" + StrStaffIds + ") ";
                    var ListStylistNotEnrollInfor = db.Database.SqlQuery<cls_infor_Staff>(strSTaff).Select(a => new cls_infor_Staff
                    {
                        StaffId = a.StaffId,
                        GroupLevelId = a.GroupLevelId,
                        Score = a.Score,
                        Fullname = a.Fullname,
                        TeamId = a.TeamId,
                        IsEnroll = false,
                        isAccountLogin = a.isAccountLogin
                    }).ToList();
                    ListStylis.AddRange(ListStylistNotEnrollInfor);
                }
                var itemDataTemp = new cls_timeline_stylist();
                if (ListFlowTimeKeeping != null)
                {
                    data = ListStylis.Select(w => new cls_timeline_stylist
                    {
                        StylistId = w.StaffId,
                        StylistName = w.Fullname,
                        BillTotal = ListBookBill.Count(a => a.Pending == 0 && a.StylistId == w.StaffId),
                        Score = w.Score,
                        GroupLevelId = w.GroupLevelId ?? 0,
                        TeamId = w.TeamId ?? 0,
                        ListHour = (from a in ListSubHour
                                    select new cls_timeline_stylist_hour
                                    {
                                        HourId = a.HourId,
                                        HourFrame = Convert.ToString(a.HourFrame),
                                        IsServiced = resultEnroll(ListFlowTimeKeeping, w.StaffId, a.SubHourId, w.TeamId),
                                        SubHourId = a.SubHourId,
                                        HourData = ReturBookingBill(ListBookBill, w.StaffId, a.SubHourId),
                                        isVirtual = resultEnroll(ListFlowTimeKeeping, w.StaffId, a.SubHourId, w.TeamId, true)
                                    }).OrderBy(a => a.HourFrame).ToList(),
                        isAccountLogin = w.isAccountLogin,
                    }).ToList();
                }
            }

            return data.OrderBy(w => w.TeamId).ThenBy(w => w.StylistId).ThenBy(w => w.GroupLevelId).ToList();
        }

        /// <summary>
        /// Check khách đặc biệt - khách VIP
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        [WebMethod]
        public static object checkCustomerType(int customerId, string customerPhone)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var message = new Library.Class.cls_message();
                var serializer = new JavaScriptSerializer();
                try
                {
                    var vipCustomer = db.Store_SpecialCustomer_CheckVIPCustomer(customerId, customerPhone).FirstOrDefault();
                    if (vipCustomer != null)
                    {
                        message.success = true;
                        message.message = serializer.Serialize(vipCustomer);
                    }
                    else
                    {
                        var sql = $@"select top 1 b.CustomerId, b.CustomerTypeId, d.TypeName, a.ReasonDiff, c.Fullname as CustomerName, c.Phone
                                     from SpecialCusDetail a
                                     left join SpecialCustomer b on b.Id = a.SpecialCusId
                                     left join CustomerType d on d.Id = b.CustomerTypeId
                                     left join Customer c on c.Id = b.CustomerId
                                     where a.IsDelete = 0 AND b.IsDelete = 0 
                                     and b.CustomerTypeId = 1
                                     and a.QuantityInvited > 0
                                     and b.CustomerId = {customerId}
                                     order by a.Id ASC";
                        var specialCustomer = db.Database.SqlQuery<CheckCustomerSpecial>(sql).FirstOrDefault();
                        if (specialCustomer != null)
                        {
                            message.success = true;
                            message.message = serializer.Serialize(specialCustomer);
                        }
                        else
                        {
                            message.success = false;
                            message.message = "";
                        }
                    }
                }
                catch (Exception ex)
                {
                    message.success = false;
                    message.message = ex.Message;
                    return message;
                }
                return message;
            }
        }

        /// <summary>
        /// Update tên khách hàng [CustomerName] trong [dbo].[Booking], [dbo].[SM_BookingTemp]
        /// </summary>
        /// <param name="customerName"></param>
        /// <param name="bookingID"></param>
        /// <returns></returns>
        [WebMethod]
        public static object setCustomerName(string customerName, int bookingID)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var message = new Library.Class.cls_message();
                try
                {

                    var bookingTmp = db.SM_BookingTemp.FirstOrDefault(f => f.Id == bookingID);
                    if (bookingTmp != null)
                    {
                        bookingTmp.CustomerName = customerName;
                        db.SM_BookingTemp.AddOrUpdate(bookingTmp);
                        var booking = db.Bookings.FirstOrDefault(f => f.BookingTempId == bookingTmp.Id);
                        if (booking != null)
                        {
                            booking.CustomerName = customerName;
                            db.Bookings.AddOrUpdate(booking);
                        }
                        else
                        {
                            throw new Exception("Error!!!");
                        }
                        db.SaveChanges();

                        try
                        {
                            //Update ten khach hang khi ban ghi khach hang da ton tai nhung khong co ten trang bang Customer
                            var objCus = db.Customers.Where(a => a.IsDelete == 0 && a.Phone == bookingTmp.CustomerPhone).OrderByDescending(a => a.Id).FirstOrDefault();
                            if (objCus != null)
                            {
                                if (objCus.Fullname == null)
                                {
                                    objCus.Fullname = customerName;
                                }
                                else if (objCus.Fullname.Trim().Length == 0)
                                {
                                    objCus.Fullname = customerName;
                                }
                                db.SaveChanges();
                            }
                        }
                        catch { }

                        message.success = true;
                    }
                    else
                    {
                        throw new Exception("Error!!!");
                    }

                }
                catch (Exception ex)
                {
                    message.success = false;
                }
                return message;
            }
        }

        /// <summary>
        /// Estimate time cut
        /// </summary>
        [WebMethod]
        public static object EstimateTime(int c)
        {
            try
            {
                var db = new Solution_30shineEntities();

                var data = db.Store_Estimate_TimeCut_V2(c).ToList();
                List<cls_EstimateTimeData> listCls = new List<cls_EstimateTimeData>();
                foreach (var item in data)
                {
                    cls_EstimateTimeData cls = new cls_EstimateTimeData();
                    cls.StylistName = item.StylistName;
                    cls.Avg_TimeCut = item.AVG_TIME_CUT != null ? item.AVG_TIME_CUT.Value : 0;
                    cls.Chuagoi = item.CHUAGOI != null ? item.CHUAGOI.Value : 0;
                    cls.Dagoi = item.DAGOI_SL != null ? item.DAGOI_SL.Value : 0;
                    cls.Danggoi_sl = item.DANGGOI_SL != null ? item.DANGGOI_SL.Value : 0;
                    cls.Danggoi_tg = item.DANGGOI_TIME != null ? item.DANGGOI_TIME.Value : 0;
                    cls.Khach_Cho = item.KHACH_CHO != null ? item.KHACH_CHO.Value : 0;
                    cls.Khach_Dang_Cat = item.KHACH_DANG_PHUCVU != null ? item.KHACH_DANG_PHUCVU.Value : 0;
                    cls.EstimateTime = item.ESTIMATETIMECUT != null ? item.ESTIMATETIMECUT.Value : 0;
                    cls.TimeEstimate = cls.EstimateTime + (cls.Avg_TimeCut * cls.Khach_Cho) + cls.Chuagoi * 12 + cls.Danggoi_tg;
                    cls.IsSulphite = item.IsSulphite != null && item.IsSulphite == 1 ? true : false;
                    listCls.Add(cls);
                }
                listCls = listCls.OrderBy(o => o.Khach_Cho).ThenBy(o => o.Khach_Dang_Cat).ThenBy(o => o.TimeEstimate).ToList();
                return new JavaScriptSerializer().Serialize(listCls);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }

        /// <summary>
        /// Estimate time massage
        /// </summary>
        [WebMethod]
        public static object EstimateTimeMassage(int c)
        {
            try
            {
                var db = new Solution_30shineEntities();

                var data = db.Store_Estimate_TimeMassage(c).ToList();
                List<cls_EstimateTimeMassage> listCls = new List<cls_EstimateTimeMassage>();
                foreach (var item in data)
                {
                    cls_EstimateTimeMassage cls = new cls_EstimateTimeMassage();
                    cls.danggoiTg = (item.DANGGOI_TIME_CONLAI != null ? item.DANGGOI_TIME_CONLAI.Value : 0) + (item.CHUAGOI != null ? item.CHUAGOI.Value : 0) * 12;
                    cls.chuagoiSl = item.CHUAGOI != null ? item.CHUAGOI.Value : 0;
                    cls.danggoiSl = item.DANGGOI_SL != null ? item.DANGGOI_SL.Value : 0;
                    cls.skinnerName = item.SkinnerName;
                    cls.hoachat = item.HOACHAT != null ? item.HOACHAT.Value : 0;
                    listCls.Add(cls);
                }
                listCls = listCls.OrderBy(o => o.chuagoiSl).ThenBy(o => o.danggoiTg).ThenBy(o => o.danggoiSl).ToList();
                return new JavaScriptSerializer().Serialize(listCls);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }

        /// <summary>
        /// Lấy dữ liệu từ SM_BookingTemp theo BookingID
        /// </summary>
        /// <param name="BookingId"></param>
        /// <returns></returns>
        [WebMethod]
        public static string GetNote(int BookingId)
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
            var _data = new RatingNote();
            var booking = db.SM_BookingTemp.FirstOrDefault(b => b.Id == BookingId);
            var customer = db.Customers.FirstOrDefault(w => w.Id == booking.CustomerId.Value);
            //if (customer != null && customer.LastBillId != null)
            //{
            //    var rateNote = new HttpClient().GetAsync(Libraries.AppConstants.URL_API_BOOKING_REQUEST + "/api-booking/customer/customer-rating?customerId=" + booking.CustomerId + "&lastBillid=" + customer.LastBillId).Result;
            //    if (rateNote.IsSuccessStatusCode)
            //    {
            //        var data = rateNote.Content.ReadAsStringAsync().Result;
            //        if (data != null)
            //        {
            //            _data = new JavaScriptSerializer().Deserialize<RatingNote>(data);
            //        }
            //    }
            //}

            var note = new ClassNote
            {
                CustomerPhone = customer.Phone,
                TextNote1 = booking.TextNote1,
                TextNote2 = booking.TextNote2,
                SalonNote = booking.SalonNote,
                // RatingNote = _data.ratingNote ?? ""
            };

            return new JavaScriptSerializer().Serialize(note);
        }

        /// <summary>
        /// Lưu dữ liệu vào Booking và SM_BookingTemp
        /// </summary>
        /// <param name="BookingId"></param>
        /// <param name="SalonNote"></param>
        /// <returns></returns>
        [WebMethod]
        public static object InsertNote(int BookingId, string SalonNote)
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
            var smBooking = db.SM_BookingTemp.FirstOrDefault(b => b.Id == BookingId);
            var booking = db.Bookings.FirstOrDefault(bk => bk.BookingTempId == BookingId);
            var tracking =
            (from tk in db.TrackingWeb_DataV2
             join b in db.Bookings on tk.Booking_ID equals b.Id
             join sm in db.SM_BookingTemp on b.BookingTempId equals sm.Id
             where sm.Id == BookingId
             select tk).FirstOrDefault();
            if (tracking != null)
            {
                tracking.SalonNote = SalonNote;
                tracking.TimeSalonNote = DateTime.Now;
                db.TrackingWeb_DataV2.AddOrUpdate(tracking);
            }

            smBooking.SalonNote = SalonNote;
            smBooking.TimeSalonNote = DateTime.Now;
            booking.SalonNote = SalonNote;
            booking.TimeSalonNote = DateTime.Now;

            db.SM_BookingTemp.AddOrUpdate(smBooking);
            db.Bookings.AddOrUpdate(booking);
            var r = db.SaveChanges();
            if (r > 0)
            {
                return "<span style='color:green'>Thêm ghi chú thành công</span>";
            }
            else
            {
                return "<span style='color:red'>Có lỗi xảy ra. Vui lòng liên hệ bộ phận phát triển</span>";
            }

        }

        /// <summary>
        /// set isbookstylist
        /// </summary>
        /// <param name="bookingID"></param>
        /// <param name="check"></param>
        /// <returns></returns>
        [WebMethod]
        public static object SetIsBookStylist(int bookingID, int check)
        {
            try
            {
                Solution_30shineEntities db = new Solution_30shineEntities();
                var smbooking = db.SM_BookingTemp.FirstOrDefault(w => w.Id == bookingID);
                if (check == 1)
                {
                    smbooking.IsAutoStylist = false;
                    smbooking.IsBookStylist = true;
                }
                else
                {
                    smbooking.IsAutoStylist = true;
                    smbooking.IsBookStylist = false;
                }
                db.SM_BookingTemp.AddOrUpdate(smbooking);
                db.SaveChanges();

                var booking = db.Bookings.FirstOrDefault(w => w.BookingTempId == bookingID);
                if (check == 1)
                {
                    booking.IsAutoStylist = false;
                    booking.IsBookStylist = true;
                }
                else
                {
                    booking.IsAutoStylist = true;
                    booking.IsBookStylist = false;
                }
                db.Bookings.AddOrUpdate(booking);
                db.SaveChanges();

                return new { IsAutoStylist = booking.IsAutoStylist, BookingTempId = booking.BookingTempId };
            }
            catch
            {
                return new { IsAutoStylist = false, BookingTempId = 0 };
            }
        }

        [WebMethod]
        public static object CountSpecialRequire(int salonId)
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
            var count = db.Tracking_Cus_Special_Requirements_Store(DateTime.Now.Date, salonId).Where(s => s.SalonNote == null || s.SalonNote == "").Count();
            return new JavaScriptSerializer().Serialize(count);
        }

        private bool resultEnroll(List<cls_infor_Staff> ListFlowTimeKeeping, int StylistId, int SubHourId, int? TeamId, bool isShowOpen = false)
        {
            try
            {
                var kq = false;
                var objTimeKeeping = ListFlowTimeKeeping.FirstOrDefault(a => a.StaffId == StylistId && a.SubHourId == SubHourId);
                if (objTimeKeeping != null)
                {
                    //if (objTimeKeeping.isAccountLogin == 0)
                    //{
                    if (objTimeKeeping.IsEnroll == true)
                    {
                        kq = ConvertEnroll(ListFlowTimeKeeping, TeamId, SubHourId, StylistId);
                    }
                    //}
                    //else
                    //{

                    //    kq = ConvertEnrollVirtual(ListFlowTimeKeeping, TeamId, SubHourId, StylistId, isShowOpen);
                    //}

                }
                else
                {

                    kq = false;
                }


                return kq;
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        private bool ConvertEnroll(List<cls_infor_Staff> ListFlowTimeKeeping, int? TeamId, int SubHourId, int? StylistId)
        {
            bool kq = false;
            var date = Convert.ToInt32(Convert.ToDateTime(DatedBook.Text, culture).Day);
            int MinStylistId = ListFlowTimeKeeping.Where(a => a.TeamId == TeamId /*&& a.SubHourId == SubHourId*/ && a.IsEnroll == true).Min(m => m.StaffId);
            // Kiem tra Stylist  phuc phuc khach khung gio chan le
            if (date % 2 == 0)
            {
                if (MinStylistId == StylistId && (SubHourId % 2) == 0)
                {
                    kq = true;
                }
                else if (MinStylistId == StylistId && (SubHourId % 2) == 1)
                {
                    kq = false;
                }
                else if (MinStylistId != StylistId && (SubHourId % 2) == 0)
                {
                    kq = false;
                }
                else if (MinStylistId != StylistId && (SubHourId % 2) == 1)
                {
                    kq = true;
                }
            }
            else
            {
                if (MinStylistId == StylistId && (SubHourId % 2) == 0)
                {
                    kq = false;
                }
                else if (MinStylistId == StylistId && (SubHourId % 2) == 1)
                {
                    kq = true;
                }
                else if (MinStylistId != StylistId && (SubHourId % 2) == 0)
                {
                    kq = true;
                }
                else if (MinStylistId != StylistId && (SubHourId % 2) == 1)
                {
                    kq = false;
                }
            }
            return kq;
        }

        private bool ConvertEnrollVirtual(List<cls_infor_Staff> ListFlowTimeKeeping, int? TeamId, int SubHourId, int? StylistId, bool isShowOpen = false)
        {
            var objTimeKeeping = ListFlowTimeKeeping.FirstOrDefault(a => a.StaffId == StylistId && a.SubHourId == SubHourId);
            if (objTimeKeeping.IsEnroll == false && isShowOpen == false)
            {
                return false;
            }

            bool kq = false;
            var date = Convert.ToInt32(Convert.ToDateTime(DatedBook.Text, culture).Day);
            var IsTimekeeping = ListFlowTimeKeeping.Where(a => a.TeamId == TeamId && a.SubHourId == SubHourId && a.IsEnroll == true && a.isAccountLogin == 1 && a.StaffId == StylistId).FirstOrDefault();
            //if (isShowOpen)
            //{
            var timekeeping = ListFlowTimeKeeping.Where(a => a.TeamId == TeamId && a.SubHourId == SubHourId && a.IsEnroll == false && a.isAccountLogin == 1).ToList();
            //}
            int MinStylistId = 0;
            if (timekeeping != null && timekeeping.Any())
            {
                MinStylistId = timekeeping.Min(m => m.StaffId);
            }
            else
            {
                return false;
            }

            // Kiem tra Stylist  phuc phuc khach khung gio chan le
            if (date % 2 == 0)
            {
                if (MinStylistId == StylistId && (SubHourId % 2) == 0)
                {
                    if (IsTimekeeping != null && !isShowOpen)
                    {
                        kq = true;
                    }
                    else if (isShowOpen)
                    {
                        kq = true;
                    }
                }
                else if (MinStylistId == StylistId && (SubHourId % 2) == 1)
                {
                    kq = false;
                }
                else if (MinStylistId != StylistId && (SubHourId % 2) == 0)
                {
                    kq = false;
                }
                else if (MinStylistId != StylistId && (SubHourId % 2) == 1)
                {
                    if (IsTimekeeping != null && !isShowOpen)
                    {
                        kq = true;
                    }
                    else if (isShowOpen)
                    {
                        kq = true;
                    }
                }
            }
            else
            {
                if (MinStylistId == StylistId && (SubHourId % 2) == 0)
                {
                    kq = false;
                }
                else if (MinStylistId == StylistId && (SubHourId % 2) == 1)
                {
                    if (IsTimekeeping != null && !isShowOpen)
                    {
                        kq = true;
                    }
                    else if (isShowOpen)
                    {
                        kq = true;
                    }
                }
                else if (MinStylistId != StylistId && (SubHourId % 2) == 0)
                {
                    if (IsTimekeeping != null && !isShowOpen)
                    {
                        kq = true;
                    }
                    else if (isShowOpen)
                    {
                        kq = true;
                    }
                }
                else if (MinStylistId != StylistId && (SubHourId % 2) == 1)
                {
                    kq = false;
                }
            }
            return kq;
        }

        private List<cls_timeline_stylist_hour_data> ReturBookingBill(List<cls_Booking_Bill> ListBookBill, int Stylist, int SubHourId)
        {
            try
            {
                var BookBillOfStylist = ListBookBill.Where(a => a.SubHour == SubHourId && a.StylistId == Stylist)
                    .Select(a => new cls_timeline_stylist_hour_data
                    {
                        CustomerName = a.CustomerName,
                        CustomerPhone = a.CustomerPhone,
                        BookingID = a.Id,
                        IsCall = a.IsCall,
                        IsCallTime = a.IsCallTime,
                        IsCallTimeModified = a.IsCallTimeModified,
                        BillID = a.BillId,
                        InProcedureTime = a.InProcedureTime,
                        InProcedureTimeModifed = a.InProcedureTimeModifed,
                        CompleteBillTime = a.CompleteBillTime,
                        BillCreatedDate = a.BillCreatedDate,
                        IsHCItem = (a.HCItem != null && a.HCItem != "") ? true : false,
                        Order = a.Order,
                        SalonNote = a.SalonNote,
                        TextNote1 = a.TextNote1,
                        TextNote2 = a.TextNote2,
                        IsAutoStylist = a.IsAutoStylist ?? true,
                        IsBookAtSalon = a.IsBookAtSalon ?? false,
                        CustomerScore = a.CustomerScore,
                        CustomerType = a.CustomerTypeId,
                        IsSM = (a.MemberType == null || a.MemberType == 0 || a.MemberEndTime < DateTime.Now) ? false : true,
                        snDay = a.SN_day,
                        snMonth = a.SN_month,
                        snYear = a.SN_year,
                        //RatingNote = GetRatingNote(a.Id)
                    }).ToList();
                return BookBillOfStylist;

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [WebMethod]
        public static string GetProvince()
        {

            try
            {
                var db = new Solution_30shineEntities();
                var province = (from a in db.TinhThanhs
                                select new
                                {
                                    ID = a.ID,
                                    TenTinhThanh = a.TenTinhThanh
                                }).ToList();
                return new JavaScriptSerializer().Serialize(province);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        [WebMethod]
        public static string GetCustomer(int provinceId, string customerPhone)
        {
            CultureInfo cultureInfo = new CultureInfo("vi-VN");
            var _date = DateTime.Now.AddDays(-2).Date;

            var db = new Solution_30shineEntities();

            var data = (from b in db.Bookings.AsNoTracking()
                            //join c in db.Customers.AsNoTracking() on b.CustomerId equals c.Id
                        join sl in db.Tbl_Salon.AsNoTracking() on b.SalonId equals sl.Id
                        join sh in db.BookHour_Sub.AsNoTracking() on b.SubHour equals sh.SubHourId
                        where sl.CityId == provinceId && b.DatedBook >= _date
                        && b.IsBookOnline == true && b.CustomerPhone.Contains(customerPhone)
                        select new
                        {
                            SalonId = b.SalonId,
                            SalonName = sl.ShortName,
                            CustomerName = b.CustomerName,
                            CustomerPhone = b.CustomerPhone,
                            DateBook = b.DatedBook,
                            BookHour = sh.Hour,
                            IsDelete = (b.IsDelete == 1) ? "Có" : "Không"
                        }
                        ).ToList();

            List<CustomerInfo> lsInfo = new List<CustomerInfo>();
            foreach (var item in data)
            {
                CustomerInfo info = new CustomerInfo();
                info.SalonId = (int)item.SalonId;
                info.SalonName = item.SalonName;
                info.CustomerName = item.CustomerName;
                info.CustomerPhone = Helpers.UIHelpers.StringReplace(item.CustomerPhone, customerPhone);
                info.DateBook = (DateTime)item.DateBook;
                info.BookHour = item.BookHour;
                info.IsDelete = item.IsDelete;
                lsInfo.Add(info);
            }
            if (data == null)
            {
                return new JavaScriptSerializer().Serialize(null);
            }
            return new JavaScriptSerializer().Serialize(lsInfo);
        }

        [WebMethod]
        public static string GetStatusHandle()
        {
            var db = new Solution_30shineEntities();
            var data = db.Tbl_Config.Where(w => w.Key == "booking_request").ToList();
            return new JavaScriptSerializer().Serialize(data);
        }

        public static string GetRatingNote(int bookingId)
        {
            try
            {
                var db = new Solution_30shineEntities();
                var booking = db.Bookings.FirstOrDefault(a => a.Id == bookingId);
                var customer = db.Customers.FirstOrDefault(a => a.Id == booking.CustomerId);
                var rateNote = new HttpClient().GetAsync(Libraries.AppConstants.URL_API_BOOKING_REQUEST + "/api-booking/customer/customer-rating?customerId=" + customer.Id + "&lastBillid=" + customer.LastBillId).Result;
                var _data = new RatingNote();
                _data.ratingNote = null;
                if (rateNote.IsSuccessStatusCode)
                {
                    var data = rateNote.Content.ReadAsStringAsync().Result;
                    if (data != null)
                    {
                        _data = new JavaScriptSerializer().Deserialize<RatingNote>(data);
                    }
                }
                return _data.ratingNote;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private int GetSalonId(int staffId, DateTime workDate)
        {
            var sl = 0;
            var staffKeeping = db.FlowTimeKeepings.Where(a => a.StaffId == staffId && a.WorkDate == EntityFunctions.TruncateTime(workDate) && a.IsDelete == 0 && a.IsEnroll == true).FirstOrDefault();
            if (staffKeeping != null)
            {
                sl = staffKeeping.SalonId;
            }
            else
            {
                sl = Convert.ToInt32(Session["SalonId"]);
            }
            return sl;
        }

        public partial class CustomerInfo
        {
            public int SalonId { get; set; }
            public string SalonName { get; set; }
            public string CustomerPhone { get; set; }

            public string CustomerName { get; set; }
            public DateTime DateBook { get; set; }
            public string BookHour { get; set; }
            public string IsDelete { get; set; }
        }

        public partial class cls_updatebookingDrop
        {
            public int salonId { get; set; }
            public int billId { get; set; }
            public int bookingId { get; set; }
            public Nullable<bool> IsAutoStylist { get; set; }
            public Nullable<bool> IsBookAtSalon { get; set; }
            public string CustomerName { get; set; }
            public string CustomerPhone { get; set; }
            public Nullable<int> HourId { get; set; }
            public Nullable<int> StylistId { get; set; }
            public Nullable<int> Order { get; set; }
            public Nullable<bool> IsCall { get; set; }
            public Nullable<System.DateTime> IsCallTime { get; set; }
            public Nullable<System.DateTime> IsCallTimeModified { get; set; }
            public string stylistName { get; set; }
            public Nullable<System.TimeSpan> HourFrame { get; set; }
            public double CustomerScore { get; set; }
            public Nullable<int> SubHourId { get; set; }
            public string TextNote1 { get; set; }
            public string TextNote2 { get; set; }
            public int? snDay { get; set; }
            public int? snMonth { get; set; }
            public int? snYear { get; set; }
            public string RatingNote { get; set; }
        }

        /// <summary>
        /// Class dữ liệu tính toán thời gian có thể phục
        /// vụ tiếp của stylist
        /// </summary>   
        public class cls_EstimateTimeData
        {
            public int StylistId { get; set; }
            public string StylistName { get; set; }
            public string CustomerName { get; set; }
            public int EstimateTime { get; set; }
            public int Chuagoi { get; set; }
            public int Dagoi { get; set; }
            public int Danggoi_sl { get; set; }
            public int Danggoi_tg { get; set; }
            public int Avg_TimeCut { get; set; }
            public bool IsSulphite { get; set; }
            public int Khach_Cho { get; set; }
            public int Khach_Dang_Cat { get; set; }
            public int TimeEstimate { get; set; }
        }

        public class cls_stylistHour
        {
            public int Id { get; set; }
            public Nullable<int> EnrollId { get; set; }
            public Nullable<int> HourId { get; set; }
            public Nullable<System.DateTime> CreatedTime { get; set; }
            public Nullable<System.DateTime> ModifiedTime { get; set; }
            public Nullable<bool> IsDelete { get; set; }
            public Nullable<System.Guid> Uid { get; set; }
            public Nullable<byte> MigrateStatus { get; set; }
            public string HourFrame { get; set; }
            public Nullable<int> SalonId { get; set; }
            public string SalonName { get; set; }
            public Nullable<int> StylistId { get; set; }
            public string StylistName { get; set; }
            public Nullable<int> GroupLevelId { get; set; }
            public int SubHourId { get; set; }
            public Nullable<int> TeamId { get; set; }
            public Nullable<int> IsEnroll { get; set; }
        }

        public class cls_EstimateTimeMassage
        {
            public int skinnerId { get; set; }
            public string skinnerName { get; set; }
            public int danggoiSl { get; set; }
            public int danggoiTg { get; set; }
            public int chuagoiSl { get; set; }
            public int hoachat { get; set; }
        }
        /// <summary>
        /// Class dữ liệu stylist, bao gồm toàn bộ khung giờ
        /// </summary>

        public class cls_timeline_stylist
        {
            public int StylistId { get; set; }
            public string StylistName { get; set; }
            public int GroupLevelId { get; set; }
            public int TeamId { get; set; }
            public List<cls_timeline_stylist_hour> ListHour { get; set; }
            public bool IsAutoStylist { get; set; }
            public int BillTotal { get; set; }
            public bool IsEnroll { get; set; }
            public double Score { get; set; }
            public int isAccountLogin { get; set; }
        }

        /// <summary>
        /// Class cấu trúc dữ liệu khung giờ
        /// </summary>
        public class cls_timeline_stylist_hour
        {
            public int HourId { get; set; }
            public int SubHourId { get; set; }
            public string HourFrame { get; set; }
            public bool IsServiced { get; set; }
            public List<cls_timeline_stylist_hour_data> HourData { get; set; }
            public bool isVirtual { get; set; }
            public int isAccountLogin { get; set; }
        }

        /// <summary>
        /// Cấu trúc dữ liệu booking|bill trong 1 khung giờ/1 stylist
        /// </summary>
        public class cls_timeline_stylist_hour_data
        {
            public string CustomerPhone { get; set; }
            public string CustomerName { get; set; }
            public int? BookingID { get; set; }
            public bool? IsCall { get; set; }
            public DateTime? IsCallTime { get; set; }
            public DateTime? IsCallTimeModified { get; set; }
            public int? BillID { get; set; }
            public DateTime? BillCreatedDate { get; set; }
            public DateTime? InProcedureTime { get; set; }
            public DateTime? InProcedureTimeModifed { get; set; }
            public DateTime? CompleteBillTime { get; set; }
            public int? Mark { get; set; }
            public bool IsAutoStylist { get; set; }
            public bool IsBookAtSalon { get; set; }
            public bool IsHCItem { get; set; }
            public int? CustomerType { get; set; }
            public int? Order { get; set; }
            public string SalonNote { get; set; }
            public string TextNote1 { get; set; }
            public string TextNote2 { get; set; }
            public double CustomerScore { get; set; }
            public bool? IsSM { get; set; }
            public int? snDay { get; set; }
            public int? snMonth { get; set; }
            public int? snYear { get; set; }
            public string RatingNote { get; set; }
        }

        /// <summary>
        /// Cấu trúc dữ liệu dịch vụ
        /// </summary>
        public class cls_timeline_service
        {
            public int Id { get; set; }
            public string Code { get; set; }
            public string Name { get; set; }
            public int Price { get; set; }
            public int Quantity { get; set; }
            public double VoucherPercent { get; set; }
        }

        /// <summary>
        /// Cấu trúc dữ liệu bill post lên từ client
        /// </summary>
        public class cls_timeline_bill
        {
            public string timeCustomerCome { get; set; }
            public string NeedConsult { get; set; }
            public string TextNote1 { get; set; }
            public string TextNote2 { get; set; }
            public int billID { get; set; }
            public int bookingID { get; set; }
            public int stylistID { get; set; }
            public string stylistName { get; set; }
            public int salonID { get; set; }
            public string salonName { get; set; }
            public int teamID { get; set; }
            public int checkinID { get; set; }
            public string checkinName { get; set; }
            public string customerName { get; set; }
            public string customerPhone { get; set; }
            public int isShineMember { get; set; }
            public string memberEndTime { get; set; }
            public int customerId { get; set; }
            public List<cls_timeline_service> services { get; set; }
            public int hourID { get; set; }
            public int subHourID { get; set; }
            public string hourFrame { get; set; }
            public string PDFBillCode { get; set; }
            public string HCItem { get; set; }
            public bool IsAutoStylist { get; set; }
            public bool IsBookAtSalon { get; set; }
            public double CustomerScore { get; set; }
            public string DatetimeCurrent { get; set; }
            public string datedBook { get; set; }
            public string snDay { get; set; }
            public string snMonth { get; set; }
            public string snYear { get; set; }
            public string noteSpecialCustomer { get; set; }
        }

        public class cls_infor_Staff
        {
            public int StaffId { get; set; }
            public int? GroupLevelId { get; set; }
            public double Score { get; set; }
            public string Fullname { get; set; }
            public bool? IsEnroll { get; set; }
            public int? HourId { get; set; }
            public int SubHourId { get; set; }
            public Nullable<int> TeamId { get; set; }
            public int isAccountLogin { get; set; }
        }

        public class cls_Booking_Bill
        {
            public int Id { get; set; }
            public string CustomerName { get; set; }
            public string CustomerPhone { get; set; }
            public Nullable<int> HourId { get; set; }
            public Nullable<int> StylistId { get; set; }
            public Nullable<bool> IsCall { get; set; }
            public Nullable<System.DateTime> IsCallTime { get; set; }
            public Nullable<System.DateTime> IsCallTimeModified { get; set; }
            public Nullable<bool> IsBookViaBill { get; set; }
            public Nullable<bool> IsBookAtSalon { get; set; }
            public Nullable<bool> IsBookStylist { get; set; }
            public Nullable<bool> IsBookOnline { get; set; }
            public Nullable<int> Order { get; set; }
            public double CustomerScore { get; set; }
            public string TextNote1 { get; set; }
            public string TextNote2 { get; set; }
            public Nullable<int> SubHour { get; set; }
            public Nullable<bool> IsMakeBill { get; set; }
            public string SalonNote { get; set; }
            public Nullable<int> BillId { get; set; }
            public Nullable<System.DateTime> InProcedureTime { get; set; }
            public Nullable<System.DateTime> InProcedureTimeModifed { get; set; }
            public Nullable<System.DateTime> CompleteBillTime { get; set; }
            public Nullable<System.DateTime> BillCreatedDate { get; set; }
            public string HCItem { get; set; }
            public Nullable<byte> Pending { get; set; }
            public Nullable<int> CustomerTypeId { get; set; }
            public Nullable<bool> IsAutoStylist { get; set; }
            public Nullable<int> MemberType { get; set; }
            public Nullable<DateTime> MemberEndTime { get; set; }
            public Nullable<int> SN_day { get; set; }
            public Nullable<int> SN_month { get; set; }
            public Nullable<int> SN_year { get; set; }

        }

        public class cls_CusSpecial
        {
            public string Phone { get; set; }
            public int CustomerTypeId { get; set; }
        }

        public class CheckCustomerSpecial
        {
            public int? CustomerId { get; set; }
            public int? CustomerTypeId { get; set; }
            public string TypeName { get; set; }
            public string ReasonDiff { get; set; }
            public string CustomerName { get; set; }
            public string Phone { get; set; }
        }

        public class ClassNote
        {
            public string CustomerPhone { get; set; }
            public string TextNote2 { get; set; }
            public string TextNote1 { get; set; }
            public string SalonNote { get; set; }
            public string RatingNote { get; set; }
        }

        public class RatingNote
        {
            public string ratingNote { get; set; }
        }
    }
}