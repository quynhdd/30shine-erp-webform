﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Service_Checkin.aspx.cs" Inherits="_30shine.GUI.FrontEnd.Service.Service_Checkin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>30Shine - Checkin</title>
    <script src="/assets/js/jquery.mCustomScrollbar.js"></script>
    <link href="/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
    <script src="/Assets/js/erp.30shine.com/service.checkin.js?v=5"></script>
    <script src="/Assets/js/bootstrap/bootstrap-toggle.min.js"></script>
    <script src="/Assets/js/check-notify.js"></script>
    <link href="/Assets/css/checkin.css" rel="stylesheet" />
    <link href="/Assets/css/bootstrap/bootstrap-toggle.min.css" rel="stylesheet" />    
    <style type="text/css">
        .tbl-Special {
            border: 1px solid black;
            padding: 5px 10px !important;
        }

        .btn-Add {
            background: #ddd !important;
            text-align: center !important;
            cursor: pointer !important;
            border-radius: 2px !important;
            float: left !important;
            width: 100% !important;
        }

        .equal {
            display: flex;
            flex-wrap: wrap;
        }
        .button-booking {background: #000000!important; }
    .button-booking:hover {color: #ffe400!important; }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Dịch vụ &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/dich-vu/danh-sach.html">Danh sách</a></li>
                        <li class="li-pending"><a href="/dich-vu/pending.html">
                            <div class="pending-1"></div>
                            Pending</a></li>
                        <li class="li-add"><a href="/dich-vu/them-phieu-v3.html">Thêm mới</a></li>
                        <li class="li-listing">
                            <a href="javascript:void(0);" onclick="openFrameBooking()" class="open-booking-list">Đặt lịch cho khách</a>
                            <div class="frame-booking-pane">
                                <%--<iframe src="https://30shine.com/erp-booking" class="iframe-booking"></iframe>--%>
                                <iframe src="http://30shine.com/erp-booking" class="iframe-booking"></iframe>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add admin-product-add fe-service no-padding no-margin">
            <%-- Add --%>
            <div class="content-wp no-padding">

                <!-- END System Message -->
                <div class="container">
                    <!-- System Message -->
                    <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                    <div class="col-xs-12 col-lg-12 com-md-12 col-sm-12" style="padding-left: 0; padding-right: 0;">
                        <%--form input customer booking or pending--%>
                        <div class="col-xs-3 col-sm-3" style="padding-left: 0;">
                            <div class="table-wp">
                                <table class="table-add admin-product-table-add fe-service-table-add" id="tblcheckin">
                                    <tbody>
                                        <%--   <tr class="title-head">
                                            <td><strong>Thông tin dịch vụ</strong></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr class="tr-margin" style="height: 20px;"></tr>--%>

                                        <tr class="tr-field-ahalf tr-send">
                                            <td class="col-xs-1 left"><span>KH</span></td>
                                            <td class="col-xs-11 right">
                                                <div class="field-wp">
                                                    <div class="filter-item" style="width: 100%; z-index: 3000; padding-right: 0;">
                                                        <%--<asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion" data-field="customer.code" data-value="0"
                                                            data-callback="Callback_UpdateCustomerCode" AutoCompleteType="Disabled" ID="CustomerCode" ClientIDMode="Static" onchange="CustomerInputOnchange($(this))" onfocus="CustomerInputOnchange($(this))" onblur="checkBindCustomerDataByPhone($(this));" onkeydown="validateTypingNumber(event, $(this))" placeholder="Số điện thoại" runat="server" Style="width: 25%;"></asp:TextBox>--%>
                                                        <asp:TextBox CssClass="st-head ip-short form-control" data-field="customer.code" data-value="0" data-callback="Callback_UpdateCustomerCode" AutoCompleteType="Disabled" ID="CustomerCode" ClientIDMode="Static" placeholder="Số điện thoại" runat="server" Style="width: 25%;"
                                                            onblur="callCustomerData($(this))" onkeypress="return isNumber(event)" onchange="customerPhone_Change();">
                                                            </asp:TextBox>
                                                        <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                                                            <ul class="ul-listing-staff ul-listing-suggestion" id="Ul4"></ul>
                                                        </div>
                                                        <div class="fake-value"></div>
                                                    </div>
                                                    <asp:TextBox ID="CustomerName" CssClass="form-control" runat="server" ClientIDMode="Static" placeholder="Tên Khách hàng" Style="width: 100%; font-family: Roboto Condensed Bold; font-size: 15px; margin-top: 10px;"></asp:TextBox>

                                                </div>
                                                <div class="field-wp">
                                                    <p class="validate-phone">(*) Số điện thoại chỉ gồm 10 hoặc 11 chữ số từ 0-9. Bắt đầu bằng số 0.</p>
                                                </div>
                                                <div class="field cusInfo hidden" style="width: 100%; float: left; padding: 5px 10px; border: 1px solid #ddd; margin-top: 10px;">
                                                    <p style="padding-top: 3px; float: left; width: 100%; text-transform: uppercase; color: red;" class="pCusType"></p>
                                                    <p class="pCutDate"></p>
                                                    <p class="pNote"></p>
                                                    <p class="pSalon"  style="padding-top: 3px; float: left; width: 100%; text-transform: uppercase; color: red;"> </p>
                                                    <p class="pExam"></p>
                                                    <p class="pStaff" style="display:none;"> </p>
                                                </div>
                                                <asp:Panel ID="Panel1" onclick="addCustomerWait($(this))" CssClass="btn-send" runat="server" ClientIDMode="Static" Style="height: 34px; line-height: 34px; margin-top: 10px;">
                                                        Thêm khách chờ
                                                    </asp:Panel>

                                            </td>
                                        </tr>
                                        <tr class="tr-field-ahalf" runat="server" id="TrSalon" visible="false">
                                            <td class="col-xs-1 left"><span>Salon</span></td>
                                            <td class="col-xs-11 right">
                                                <span class="field-wp">
                                                    <asp:DropDownList CssClass="form-control select"  ID="Salon" runat="server" ClientIDMode="Static" Style="width: 45%; max-width: 250px;"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator InitialValue="0"
                                                        ID="ValidateSalon" Display="Dynamic"
                                                        ControlToValidate="Salon"
                                                        runat="server" Text="Bạn chưa chọn Salon!"
                                                        ErrorMessage="Vui lòng chọn Salon!"
                                                        ForeColor="Red"
                                                        CssClass="fb-cover-error">
                                                    </asp:RequiredFieldValidator>
                                                </span>
                                            </td>
                                        </tr>

                                        <tr class="tr-field-ahalf tr-product">
                                            <td class="col-xs-1 left"><span style="line-height: 18px;">Dịch vụ</span></td>
                                            <td class="col-xs-11 right">
                                                <div class="row" id="quick-service">
                                                    <div class="checkbox cus-no-infor">
                                                        <label class="lbl-cus-no-infor">
                                                            <input type="checkbox" data-code="SP00036" onclick="addServiceToList($(this),53,'SP00036','Shine Combo 100k', 100000, 1, 0)" id="defaultService" style="margin-left: -15px;" />
                                                            Shine Combo 100k
                                                        </label>
                                                        <label class="lbl-cus-no-infor">
                                                            <input type="checkbox" data-code="<%# Eval("Code") %>" onclick="addServiceToList($(this),59,'SP00042','Gội, vuốt sáp miễn phí', 0, 1, 0)" style="margin-left: 10px;" />
                                                            Gội, vuốt sáp miễn phí
                                                        </label>
                                                    </div>
                                                    <asp:Repeater runat="server" ID="Rpt_ServiceFeatured">
                                                        <ItemTemplate>
                                                            <div class="checkbox cus-no-infor">
                                                                <label class="lbl-cus-no-infor">
                                                                    <input type="checkbox" data-code="<%# Eval("Code") %>" onclick="addServiceToList($(this),59,'SP00042','Gội, vuốt sáp miễn phí', 0, 1, 0)" />
                                                                    <%# Eval("Name") %>
                                                                </label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </div>
                                                <div class="listing-product item-service" id="ListingServiceWp" runat="server" clientidmode="Static">
                                                    <table class="table table-listing-product table-item-service" id="table-item-service">
                                                        <thead>
                                                            <tr>
                                                                <th>STT</th>
                                                                <th>Tên dịch vụ</th>
                                                                <th>Mã dịch vụ</th>
                                                                <th style="display: none;">Đơn giá</th>
                                                                <th style="display: none;">Số lượng</th>
                                                                <th style="display: none;">Giảm giá</th>
                                                                <th style="display: none;">Thành tiền</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <asp:Repeater ID="Rpt_Service_Bill" runat="server">
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                                                        <td class="td-product-name"><%# Eval("Name") %></td>
                                                                        <td class="td-product-code" data-id="<%# Eval("Id")%>"><%# Eval("Code") %></td>
                                                                        <td class="td-product-price" data-price="<%# Eval("Price") %>"><%# Eval("Price") %></td>
                                                                        <td class="td-product-quantity">
                                                                            <input type="text" class="product-quantity form-control" value="<%# Eval("Quantity") %>" />
                                                                        </td>
                                                                        <td class="td-product-voucher" data-voucher="<%# Eval("VoucherPercent") %>">
                                                                            <div class="row">
                                                                                <input type="text" class="product-voucher form-control" value="<%# Eval("VoucherPercent") %>" style="margin: 0 auto; float: none; text-align: center; width: 50px;" />
                                                                            </div>
                                                                        </td>
                                                                        <td class="map-edit">
                                                                            <div class="box-money" style="display: block;">
                                                                                <%# Convert.ToInt32(Eval("Quantity")) * Convert.ToInt32(Eval("Price")) * 
                                                            (100-Convert.ToInt32(Eval("VoucherPercent")))/100 %>
                                                                            </div>
                                                                            <div class="edit-wp">
                                                                                <a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),'<%# Eval("Name") %>',<%# Eval("Id") %>,'service')"
                                                                                    href="javascript://" title="Xóa"></a>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </tbody>
                                                    </table>
                                                </div>

                                                <div class="row free-service" style="display: none;">
                                                    <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                                        <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">Phụ trợ : </label>
                                                    </div>
                                                    <asp:Repeater runat="server" ID="Rpt_FreeService">
                                                        <ItemTemplate>
                                                            <div class="checkbox cus-no-infor" style="padding-bottom: 4px;">
                                                                <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                                    <input type="checkbox" onclick="pushFreeService($(this),<%# Eval("Id")%>)" data-id="<%# Eval("Id") %>"
                                                                        <%# Eval("Status").ToString() == "1" ? "checked='checked'" : "" %> />
                                                                    <%# Eval("Name") %>
                                                                </label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </div>

                                            </td>
                                        </tr>

                                        <tr class="tr-send">
                                            <td class="col-xs-1 left"><span>Team</span></td>
                                            <td class="col-xs-11 right">
                                                <div class="field-wp ">
                                                    <%--<p class="status-team"></p>--%>
                                                    <div class="wr-choose-team">
                                                        <div class="team-color-select" runat="server" id="TeamColorSelect" clientidmode="Static" data-color="">
                                                        </div>
                                                        <p class="time"></p>
                                                    </div>
                                                    <div class="item-color-wrap">
                                                        <asp:Repeater runat="server" ID="Rpt_TeamWork">
                                                            <ItemTemplate>
                                                                <div class="item-color" style="background: <%# Eval("Color")%>;" onclick="setTeamColor($(this),<%# Eval("Id") %>,'<%# Eval("Color") %>')"></div>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td class="col-xs-1 left"><span style="line-height: 18px;">Lễ tân</span></td>
                                            <td class="col-xs-11 right">
                                                <div class="filter-item" style="width: 100%; padding-right: 0;">
                                                    <asp:TextBox CssClass="st-head ip-short auto-select form-control" data-field="Reception" data-value="0"
                                                        AutoCompleteType="Disabled" ID="InputReception" ClientIDMode="Static" placeholder="Nhập mã số" runat="server"></asp:TextBox>
                                                    <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                                                        <ul class="ul-listing-staff ul-listing-suggestion" id="Ul1"></ul>
                                                    </div>
                                                    <div class="fake-value" id="FVReception" runat="server" clientidmode="Static"></div>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr style="display: none;">
                                            <td class="col-xs-1 left"><span style="line-height: 18px;"></span></td>
                                            <td class="col-xs-11 right">
                                                <div class="row free-service">
                                                    <div class="checkbox cus-no-infor" style="padding-bottom: 4px;">
                                                        <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                            <asp:CheckBox runat="server" ID="isX2" ClientIDMode="Static" />
                                                            Tăng ca
                                                        </label>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr class="tr-send">
                                            <td class="col-xs-1 left"></td>
                                            <td class="col-xs-11 right no-border">
                                                <span class="field-wp">
                                                    <asp:Panel ID="BtnSend" CssClass="btn-send btn-print-bill" runat="server" ClientIDMode="Static">In phiếu</asp:Panel>

                                                    <asp:Button ID="BtnFakeSend" runat="server" Text="Hoàn tất"
                                                        ClientIDMode="Static" OnClick="AddService" Style="display: none;"></asp:Button>

                                                    <asp:Panel ID="BtnBook" CssClass="btn-send btn-book" runat="server" ClientIDMode="Static" Style="margin-left: 12px; display: none;">Đặt lịch</asp:Panel>

                                                    <%--<asp:Button ID="BtnFakeBook" runat="server" Text="Hoàn tất"
                                                        ClientIDMode="Static" OnClick="AddServiceBooking" Style="display: none;"></asp:Button>--%>
                                                </span>

                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="col-xs-5 booking-content" style="padding-left: 0; <%= (SalonRecord != null && SalonRecord.IsSalonHoiQuan != null && SalonRecord.IsSalonHoiQuan == true) ? "display:none;" : "" %>">
                            <!-- Khách book lịch -->
                            <div class="wp-booking customer-booking">
                                <div class="wp-booking-content">
                                    <div style="display: none">
                                        <p style="font-family: Roboto Condensed Bold; text-transform: uppercase; padding-bottom: 5px; float: left;">Chọn salon</p>
                                        <asp:DropDownList ID="ddlSalon1" CssClass="form-control select" runat="server" Style="width: 245px; margin-left: 10px; margin-bottom: 10px;">
                                        </asp:DropDownList>
                                    </div>
                                    <p class="text-head">Booking</p>
                                    <div class="booking-listing customer-booking-list">
                                        <table class="table-booking-list" id="tableBookingList">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Khách hàng</th>
                                                    <th>Số ĐT</th>
                                                    <th>Khung giờ hẹn</th>
                                                    <th>Stylist</th>
                                                    <th>Đã đến</th>
                                                    <th>Xóa</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!--/ Khách book lịch -->
                        </div>
                        <div class="col-xs-4 booking-content" style="padding-left: 0; padding-right: 0;">
                            <!-- Khách chờ tại salon -->
                            <div class="wp-booking customer-waiting">
                                <div class="wp-booking-content">
                                    <div style="display: none">
                                        <p style="font-family: Roboto Condensed Bold; text-transform: uppercase; padding-bottom: 5px; float: left;">Chọn salon</p>
                                        <asp:DropDownList ID="ddlSalon2" CssClass="form-control select" runat="server" Style="width: 245px; margin-left: 10px; margin-bottom: 10px;" onchange="loadBillWaitBySalon($(this))">
                                        </asp:DropDownList>
                                    </div>
                                    <p class="text-head">Khách chờ tại salon</p>
                                    <div class="booking-listing customer-waiting-list">
                                        <table class="table-booking-list" id="tableBillWait">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Khách hàng</th>
                                                    <th>Số ĐT</th>
                                                    <th>Giờ book</th>
                                                    <th>Stylist</th>
                                                    <th>Giờ vào</th>
                                                    <th>Đã vào cắt</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!--/ Khách chờ tại salon -->
                        </div>
                    </div>
                </div>
                <%-- end Add --%>
            </div>
        </div>

        <!-- input hidden -->
        <asp:HiddenField ID="HDF_CustomerCode" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="HDF_ProductIds" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="HDF_ServiceIds" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="HDF_TotalMoney" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="HDF_UserImages" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="HDF_UserImagesDelete" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="Hairdresser" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="HairMassage" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="Cosmetic" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="HDF_SalonId" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="HDF_Suggestion_Code" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="HDF_Suggestion_Field" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="HDF_FreeService" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="HDF_TeamId" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="HDF_TeamId_Booking" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="Reception" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="HDF_SocialThread" runat="server" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
        <!-- END input hidden -->
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <!-- In hóa đơn -->
        <iframe id="iframePrint" style="display: none;"></iframe>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                var qs = getQueryStrings();
                if (qs["msg_print_billcode"] != undefined) {
                    openPdfIframe("/Public/PDF/" + qs["msg_print_billcode"] + ".pdf");
                }
                showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);
            });

            function openPdfIframe(src) {
                var PDF = document.getElementById("iframePrint");
                PDF.src = src;
                PDF.onload = function () {
                    PDF.focus();
                    PDF.contentWindow.print();
                    PDF.contentWindow.close();
                }
            }
            //sự kiện change của textbox nhập số đt của khách hàng
            function customerPhone_Change() {
                addLoading();
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/Service/Service_Checkin.aspx/checkCustomerType",
                    data: '{phone : "' + $("#CustomerCode").val() + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.d != null) {
                            if(response.d.Id == 1){
                                GetSpecialCustomer();
                            }
                            else if(response.d.Id == 2){
                                $(".pCusType").text(response.d.TypeName);
                                GetVIPCustomer();
                            }
                        }
                        else{
                            $(".cusInfo").addClass("hidden");
                        }
                        removeLoading();
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            //load thông tin khách đặc biệt
            function GetSpecialCustomer(){                
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/Service/Service_Checkin.aspx/GetSpecialCustomer",
                    data: '{phone : "' + $("#CustomerCode").val() + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.d != null) {
                            var lydo= "";
                            $(".cusInfo").removeClass("hidden");
                            $(".pCusType").text(response.d.TypeName);
                            $(".pNote").text('Lý do: ' + response.d.ReasonDiff);
                            $(".pCutDate").text('Ngày cắt lỗi: ' +ConvertJsonDateToStringFormat(response.d.CutErrorDate));
                            //if(response.d.Reason == "Khác"){
                            //    $(".pNote").removeClass('hidden');
                               
                            //}
                            //else{
                            //    $(".pNote").removeClass('hidden');
                            //    $(".pNote").text('Lý do: ' + response.d.Reason);
                            //}
                           
                            $(".pStaff").text('Nhân viên: '+ response.d.StaffName);
                            $(".pSalon").text("LỄ TÂN CHÚ Ý THÔNG BÁO FREE CHO KHÁCH");
                            $(".pExam").text("VD: Lần trước anh chưa hài lòng về dịch vụ bên em nên hôm nay chúng em mời anh sử dụng dịch vụ miễn phí ạ.")
                        }
                        else{
                            $(".cusInfo").addClass("hidden");
                        }
                        removeLoading();
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            //load thông tin khách VIP
            function GetVIPCustomer(){
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/Service/Service_Checkin.aspx/GetVipCustomer",
                    data: '{phone : "' + $("#CustomerCode").val() + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.d != null) {
                            $(".cusInfo").removeClass("hidden");
                            $(".pNote").addClass('hidden');
                            $(".pCusType").text(response.d.TypeName);
                            $(".pCutDate").addClass('hidden');
                            $(".pStaff").addClass('hidden');
                            $(".pSalon").addClass('hidden');
                            $(".pExam").addClass('hidden');
                        }
                        else{
                            $(".cusInfo").addClass("hidden");
                        }
                        removeLoading();
                    },
                    failure: function (response) { alert(response.d); }
                });
            }
        </script>
        <!--/ In hóa đơn -->
    </asp:Panel>
</asp:Content>
