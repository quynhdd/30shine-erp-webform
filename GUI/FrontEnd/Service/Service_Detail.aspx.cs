﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.IO;
using System.Web.Helpers;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Data.Entity.Migrations;
using System.Globalization;
using LinqKit;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.UIService
{
    public partial class Service_Detail : System.Web.UI.Page
    {
        private string PageID = "DV_INF0";
        private cls_bill OBJ;
        protected bool HasService = false;
        protected bool HasProduct = false;
        protected bool HasImages = false;
        protected List<string> ListImagesUrl = new List<string>();
        protected string[] ListImagesName;
        protected string _CustomerCode = "";
        protected bool CusNoInfor = false;
        protected int SalonId;
        protected bool IsPromotion = false;
        protected int _Code;

        private bool Perm_Access = false;
        private bool Perm_Edit = false;
        protected bool Perm_ViewPhone = false;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_ViewPhone = permissionModel.CheckPermisionByAction("Perm_ViewPhone", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();

            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
            if (Perm_Edit)
            {
                AEdit.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            int integer;
            SalonId = int.TryParse(Session["SalonId"].ToString(), out integer) ? integer : 0;
            _Code = int.TryParse(Request.QueryString["Code"], out integer) ? integer : 0;

            if (!IsPostBack)
            {
                if (Bind_Customer())
                {
                    Bind_RptProduct();
                    Bind_RptService();
                    Bind_ServicePromotion();
                    //Bind_Image_Default();
                }
                else
                {
                    MsgSystem.Text = "Không tồn tại hóa đơn!";
                    MsgSystem.CssClass = "msg-system warning";
                }
            }
        }

        public static string ChangeFileName(string fileName, string chain)
        {
            string temp = "";
            string type = "";
            int chainPos = fileName.IndexOf(chain);
            if (chainPos == -1)
            {
                for (var i = fileName.Length - 1; i >= 0; i--)
                {
                    if (fileName[i] == '.')
                    {
                        temp = fileName.Substring(0, i) + chain + ConvertToUnixTimestamp(DateTime.Now).ToString();
                        break;
                    }
                    else
                    {
                        type = fileName[i] + type;
                    }
                }
            }
            else
            {
                for (var i = fileName.Length - 1; i >= 0; i--)
                {
                    if (fileName[i] != '.')
                    {
                        type = fileName[i] + type;
                    }
                    else
                    {
                        break;
                    }
                }
                temp = fileName.Substring(0, chainPos) + chain + ConvertToUnixTimestamp(DateTime.Now).ToString();
            }

            return temp + "." + type;
        }

        public static double ConvertToUnixTimestamp(DateTime date)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            TimeSpan diff = date - origin;
            return Math.Floor(diff.TotalSeconds);
        }

        private bool Bind_Customer()
        {
            using (var db = new Solution_30shineEntities())
            {
                var ExistOBJ = false;
                var MatchInt = Regex.Match(Request.QueryString["Code"], @"[^0-9]+");
                if (!MatchInt.Success)
                {
                    var sql = @"select bill.*, customer.Fullname as ctmName, stylist.Fullname as stylistName, 
	                                    skinner.Fullname as skinnerName, checkin.Fullname as checkinName,checkout.Fullname as checkoutName, seller.Fullname as sellerName
                                    from BillService as bill
                                    left join Customer as customer
                                    on bill.CustomerId = customer.Id
                                    left join Staff as stylist
                                    on bill.Staff_Hairdresser_Id = stylist.Id
                                    left join Staff as skinner
                                    on bill.Staff_HairMassage_Id = skinner.Id
                                    left join Staff as checkin
                                    on bill.ReceptionId = checkin.Id
									left join Staff as checkout
									on bill.CheckoutId = checkout.Id
									left join Staff as seller
                                    on bill.SellerId = seller.Id
                                    where bill.Id = " + _Code;
                    OBJ = db.Database.SqlQuery<cls_bill>(sql).FirstOrDefault();
                    if (OBJ != null)
                    {
                        CustomerName.Text = OBJ.ctmName;
                        CustomerCode.Text = Perm_ViewPhone == true ? "Số ĐT : " + OBJ.CustomerCode : "Số ĐT : " + Library.Format.ReplaceFirstPhone(OBJ.CustomerCode);
                        Hairdresser.Text = OBJ.stylistName;
                        HairMassage.Text = OBJ.skinnerName;
                        Checkin.Text = OBJ.checkinName;
                        CheckOut.Text = OBJ.checkoutName;
                        TotalMoney.Text = OBJ.TotalMoney.ToString();
                        Description.Text = OBJ.Note;
                        HDF_CustomerCode.Value = OBJ.CustomerCode;
                        if (OBJ.VIPCard == true)
                        {
                            customerVIP.Checked = true;
                        }
                        if (OBJ.ServiceError == true)
                        {
                            ServiceError.Checked = true;
                        }
                        else
                        {
                            ServiceError.Checked = false;
                        }

                        if (OBJ.Images != "" && OBJ.Images != null)
                        {
                            ListImagesName = OBJ.Images.Split(',');
                            var Len = ListImagesName.Length;
                            if (Len > 0)
                            {
                                var loop = 0;
                                foreach (var v in ListImagesName)
                                {
                                    var match = Regex.Match(v, @"api.30shine.com");
                                    if (!match.Success)
                                    {
                                        //ListImagesName[loop] = "https://30shine.com/" + v;
                                    }
                                    loop++;
                                }
                                HasImages = true;
                            }
                        }

                        _CustomerCode = OBJ.CustomerCode;
                        CusNoInfor = Convert.ToBoolean(OBJ.CustomerNoInfo);
                        ExistOBJ = true;
                    }
                }
                else
                {
                    MsgSystem.Text = "Không tồn tại hóa đơn!";
                    MsgSystem.CssClass = "msg-system warning";
                }

                return ExistOBJ;
            }
        }

        /// <summary>
        /// Bind danh sách dịch vụ trong bill
        /// </summary>
        private void Bind_RptService()
        {
            using (var db = new Solution_30shineEntities())
            {
                //End code cũ
                //khi checkout
                if (OBJ.CompleteBillTime == null && OBJ.Pending == 1)
                {
                    var _Services = db.Store_CheckOut_BindService(OBJ.Id).ToList();
                    if (_Services.Count > 0)
                    {
                        ListingServiceWp.Style.Add("display", "block");
                        HasService = true;
                    }
                    Rpt_Service.DataSource = _Services;
                    Rpt_Service.DataBind();
                }
                //khi sửa bill
                else
                {
                    List<ProductBasic> lstService = new List<ProductBasic>();
                    var serializer = new JavaScriptSerializer();
                    if (OBJ.ServiceIds != "")
                    {
                        lstService = serializer.Deserialize<List<ProductBasic>>(OBJ.ServiceIds).ToList();
                        if (lstService.Count > 0)
                        {
                            ListingServiceWp.Style.Add("display", "block");
                            HasService = true;
                        }
                    }
                    Rpt_Service.DataSource = lstService;
                    Rpt_Service.DataBind();
                }
            }
        }

        private void Bind_RptProduct()
        {
            using (var db = new Solution_30shineEntities())
            {
                List<ProductBill> lstProduct = new List<ProductBill>();
                var serializer = new JavaScriptSerializer();
                var staff = new List<Staff>();
                var product = new List<FlowProduct>();
                if (!String.IsNullOrEmpty(OBJ.ProductIds))
                {
                    var list = serializer.Deserialize<List<ProductBill>>(OBJ.ProductIds).ToList();
                    var listIds = list.Select(w => w.Id);
                    if (list.Count > 0)
                    {
                        product = db.FlowProducts.Where(a => a.BillId == OBJ.Id && listIds.Contains(a.ProductId ?? 0)).ToList();
                        var productIds = product.Where(w => w.SellerId > 0).Select(w => w.SellerId);
                        if (productIds.Count() > 0)
                        {
                            staff = db.Staffs.Where(w => productIds.Contains(w.Id)).ToList();
                        }
                    }
                    //var listSeller = list.Where(w2 => w2.SellerId > 0).Select(s => s.SellerId).ToList();
                    //if (listSeller.Count > 0)
                    //{
                    //    staff = db.Staffs.Where(w => listSeller.Contains(w.Id)).ToList();
                    //}
                    foreach (var v in list)
                    {
                        var item = new ProductBill();
                        item.Id = v.Id;
                        item.Name = v.Name;
                        //item.SellerId = v.SellerId;
                        //item.SellerName = staff.FirstOrDefault(w => w.Id == v.SellerId) != null ? staff.FirstOrDefault(w => w.Id == v.SellerId).Fullname : "";
                        item.SellerId = product.FirstOrDefault(w => w.ProductId == v.Id) != null ? product.FirstOrDefault(w => w.ProductId == v.Id).SellerId ?? 0 : 0;
                        item.SellerName = staff.FirstOrDefault(w => w.Id == item.SellerId) != null ? staff.FirstOrDefault(w => w.Id == item.SellerId).Fullname : "";
                        item.CheckCombo = v.CheckCombo;
                        item.Code = v.Code;
                        item.Cost = v.Cost;
                        item.IsCheckVatTu = v.IsCheckVatTu;
                        item.MapIdProduct = v.MapIdProduct;
                        item.Order = v.Order;
                        item.Price = v.Price;
                        item.Promotion = v.Promotion;
                        item.Quantity = v.Quantity;
                        item.VoucherPercent = v.VoucherPercent ?? 0;
                        lstProduct.Add(item);
                    }
                }
                if (lstProduct.Count > 0)
                {
                    ListingProductWp.Style.Add("display", "block");
                    //HasProduct = true;
                }
                Rpt_Product.DataSource = lstProduct;
                Rpt_Product.DataBind();
            }
        }

        private void Bind_ServicePromotion()
        {
            using (var db = new Solution_30shineEntities())
            {
                var svp = db.FlowPromotions.Where(w => w.BillId == OBJ.Id && w.IsDelete != 1 && w.Group == 1)
                            .Join(db.Services,
                                a => a.PromotionId,
                                b => b.Id,
                                (a, b) => new
                                {
                                    b.Id,
                                    b.Name
                                }
                            ).ToList();
                if (svp.Count > 0)
                {
                    IsPromotion = true;
                }
                Rpt_FreeService.DataSource = svp;
                Rpt_FreeService.DataBind();
            }
        }

        protected void Update(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var OBJ = db.BillServices.FirstOrDefault(w => w.Id == _Code);

                if (OBJ != null)
                {
                    OBJ.ServiceError = ServiceError.Checked;
                    OBJ.ModifiedDate = DateTime.Now;

                    db.BillServices.AddOrUpdate(OBJ);
                    var exc = db.SaveChanges();

                    if (exc > 0)
                    {
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                        UIHelpers.Redirect("/dich-vu/" + OBJ.Id + ".html", MsgParam);
                        //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Execute Image Width", "excThumbWidth();", true);
                    }
                    else
                    {
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "warning"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thất bại. Vui lòng liên hệ nhóm phát triển"));
                        UIHelpers.Redirect("/dich-vu/" + OBJ.Id + ".html", MsgParam);
                    }
                }
            }
        }

        protected void AddImages(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Code = Convert.ToInt32(Request.QueryString["Code"]);
                var OBJ = db.BillServices.FirstOrDefault(w => w.Id == _Code);

                var DataAdd = HDF_UserImages.Value;
                var DataDel = HDF_UserImagesDelete.Value;
                var serializer = new JavaScriptSerializer();
                var ListImgAdd = serializer.Deserialize<List<string>>(DataAdd);
                var ListImgDel = serializer.Deserialize<List<string>>(DataDel);

                // Delete images
                if (ListImgDel.Count > 0)
                {
                    foreach (var v in ListImgDel)
                    {
                        if (v != "")
                            OBJ.Images = OBJ.Images.Replace(v, "");
                    }
                    var regex = new Regex("[\\,]+");
                    OBJ.Images = regex.Replace(OBJ.Images, ",");
                    OBJ.Images.TrimStart(',').TrimEnd(',');
                }

                if (OBJ.Images != "" && OBJ.Images != null)
                {
                    ListImagesName = OBJ.Images.Split(',');
                    var Len = ListImagesName.Length;
                    if (Len > 0) HasImages = true;
                }

                OBJ.ModifiedDate = DateTime.Now;

                db.BillServices.AddOrUpdate(OBJ);
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    var MsgParam = new List<KeyValuePair<string, string>>();
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                    UIHelpers.Redirect("/dich-vu/" + OBJ.Id + ".html", MsgParam);
                    //ScriptManager.RegisterStartupScript(Page, this.GetType(), "Execute Image Width", "excThumbWidth();", true);
                }
                else
                {
                    var MsgParam = new List<KeyValuePair<string, string>>();
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "warning"));
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thất bại. Vui lòng liên hệ nhóm phát triển"));
                    UIHelpers.Redirect("/dich-vu/" + OBJ.Id + ".html", MsgParam);
                }
            }
        }

        [WebMethod]
        public static string GetCustomer(string CustomerCode)
        {
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var _Customer = db.Customers.Where(w => w.Customer_Code == CustomerCode).ToList();

                if (_Customer.Count > 0)
                {
                    Msg.success = true;
                    Msg.msg = serializer.Serialize(_Customer);
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        [WebMethod]
        public static string Ajax_LoadImages(string TimeFrom, string TimeTo, string SgCode, string SgField)
        {
            var Msg = new Msg();
            var serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var _Images = new List<Tbl_Media>();
                var Where = PredicateBuilder.True<Tbl_Media>();
                var LoadDefault = true;

                if (TimeFrom != "")
                {
                    DateTime _FromDate = Convert.ToDateTime(TimeFrom, culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                    DateTime _ToDate;
                    if (TimeTo != "")
                    {
                        _ToDate = Convert.ToDateTime(TimeTo, culture).AddDays(1);
                    }
                    else
                    {
                        _ToDate = Convert.ToDateTime(TimeFrom, culture).AddDays(1);
                    }
                    Where = Where.And(w => w.CreatedDate > _FromDate && w.CreatedDate < _ToDate);
                    LoadDefault = false;
                }

                switch (SgField)
                {
                    case "customer.code":
                        Where = Where.And(w => w.Key.Contains(SgCode));
                        LoadDefault = false;
                        break;
                    case "customer.name":
                        Where = Where.And(w => w.Key.Contains(SgCode));
                        LoadDefault = false;
                        break;
                    case "customer.phone":
                        Where = Where.And(w => w.Key.Contains(SgCode));
                        LoadDefault = false;
                        break;
                    case "bill.code": break;
                }

                Where = Where.And(p => p.IsDelete != 1);

                if (LoadDefault)
                {
                    _Images = db.Tbl_Media.AsExpandable().Where(Where).OrderByDescending(o => o.Id).Take(35).ToList();
                }
                else
                {
                    _Images = db.Tbl_Media.AsExpandable().Where(Where).OrderByDescending(o => o.Id).ToList();
                }

                Msg.success = true;
                Msg.msg = serializer.Serialize(_Images);

                return serializer.Serialize(Msg);
            }
        }


        public class cls_bill : BillService
        {
            public string ctmName { get; set; }
            public string stylistName { get; set; }
            public string skinnerName { get; set; }
            public string checkinName { get; set; }
            public string checkoutName { get; set; }
            public string sellerName { get; set; }
        }
        public class ProductBill
        {
            public int Id { get; set; }
            /// <summary>
            /// Mã sản phẩm
            /// </summary>
            public string Code { get; set; }
            /// <summary>
            /// Tên sản phẩm
            /// </summary>
            public string Name { get; set; }
            /// <summary>
            /// Giá gốc
            /// </summary>
            public int Cost { get; set; }
            /// <summary>
            /// Giá bán
            /// </summary>
            public int Price { get; set; }
            /// <summary>
            /// Số lượng
            /// </summary>
            public int Quantity { get; set; }
            /// <summary>
            /// % khuyến mãi
            /// </summary>
            public int? VoucherPercent { get; set; }
            /// <summary>
            /// Tiền khuyến mãi
            /// </summary>
            public int Promotion { get; set; }
            public string MapIdProduct { get; set; }
            public bool CheckCombo { get; set; }
            public int? Order { get; set; }

            /// <summary>
            /// Check mức dùng vật tư
            /// </summary>
            public bool IsCheckVatTu { get; set; }
            public int SellerId { get; set; }
            public string SellerName { get; set; }
        }
    }

}