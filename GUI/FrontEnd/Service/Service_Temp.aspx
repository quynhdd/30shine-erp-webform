﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Service_Temp.aspx.cs" Inherits="_30shine.GUI.UIService.Service_Temp" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ServiceAdd" ContentPlaceHolderID="CtMain" runat="server">
<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet"/>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>

<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">
                <li class="li-listing"><a href="/dich-vu/danh-sach.html">Danh sách</a></li>
                <li class="li-add active"><a href="/dich-vu/them-moi.html">Thêm mới</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add admin-product-add fe-service">    
    <%-- Add --%>                
    <div class="wp960 content-wp">
        <!-- System Message -->
        <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
        <!-- END System Message -->

        <div class="table-wp">
            <table class="table-add admin-product-table-add fe-service-table-add">
                <tbody>
                    <tr class="title-head">
                        <td><strong>Thông tin dịch vụ</strong></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr class="tr-margin" style="height: 20px;"></tr>

                    <tr class="tr-field-ahalf">
                        <td class="col-xs-2 left"><span>Khách hàng</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="CustomerName" runat="server" ClientIDMode="Static" ReadOnly="true" style="width:35%;"></asp:TextBox>
                                <asp:TextBox ID="CustomerCode" CssClass="mgl" placeholder="Nhập mã khách hàng ở đây" runat="server" 
                                    ClientIDMode="Static" onchange="LoadCustomer(this.value)" style="width: 35%;"></asp:TextBox>
                                <span class="checkbox cus-no-infor">
                                    <label>
                                        <input type="checkbox" id="InputCusNoInfor" runat="server" ClientIDMode="Static"/> Khách không cho thông tin
                                    </label>
                                </span>
                                <%--<asp:RequiredFieldValidator ID="ValidateCustomerName" ControlToValidate="CustomerName" runat="server" 
                                    CssClass="fb-cover-error" Text="Bạn chưa nhập mã khách hàng!" ClientIDMode="Static"></asp:RequiredFieldValidator>--%>
                            </span>                            
                        </td>
                    </tr>

                    <tr class="tr-field-ahalf tr-product">
                        <td class="col-xs-2 left"><span>Dịch vụ</span></td>
                        <td class="col-xs-9 right">
                            <%--<span class="field-wp">
                                <asp:DropDownList ID="ServiceName" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <asp:RequiredFieldValidator InitialValue="0" 
                                    ID="rfvDDL_Product" Display="Dynamic" 
                                    ControlToValidate="ServiceName"
                                    runat="server"  Text="Bạn chưa chọn dịch vụ!" 
                                    ErrorMessage="Please Select the Product"
                                    ForeColor="Red"
                                    CssClass="fb-cover-error select-service-error">
                                </asp:RequiredFieldValidator>
                            </span>--%>
                            <div class="row">
                                <div class="show-product show-item" data-item="service">Thêm dịch vụ</div>
                            </div>                            
                            <div class="listing-product item-service">
                                <table class="table table-listing-product table-item-service">
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Tên dịch vụ</th>
                                            <th>Mã dịch vụ</th>
                                            <th>Đơn giá</th>
                                            <th>Số lượng</th>
                                            <th>Giảm giá</th>
                                            <th>Thành tiền</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>  
                        </td>
                    </tr>

                    <tr class="tr-field-ahalf tr-field-third">
                        <td class="col-xs-2 left"><span>Nhân viên</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:DropDownList ID="Hairdresser" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <asp:DropDownList ID="HairMassage" CssClass="mgl" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <asp:DropDownList ID="Cosmetic" CssClass="mgl" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <span id="ValidateCosmetic" class="fb-cover-error" style="visibility:hidden; top: -28px; width: 255px;">Bạn chưa chọn nhân viên bán mỹ phẩm!</span>
                            </span>
                                            
                        </td>
                    </tr>

                    <tr class="tr-field-ahalf" style="display:none;">
                        <td class="col-xs-2 left"><span>Điểm đánh giá</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:DropDownList ID="Mark" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="ValidateMark" ControlToValidate="Mark" runat="server" CssClass="fb-cover-error" Text="Bạn chưa đánh giá!"></asp:RequiredFieldValidator>
                            </span>
                                            
                        </td>
                    </tr>

                    <tr class="tr-field-ahalf tr-product">
                        <td class="col-xs-2 left"><span>Sản phẩm</span></td>
                        <td class="col-xs-9 right">
                            <div class="row">
                                <div class="show-product show-item"  data-item="product">Thêm sản phẩm</div>
                            </div>                            
                            <div class="listing-product item-product">
                                <table class="table table-listing-product table-item-product">
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Tên sản phẩm</th>
                                            <th>Mã sản phẩm</th>
                                            <th>Đơn giá</th>
                                            <th>Số lượng</th>
                                            <th>Giảm giá</th>
                                            <th>Thành tiền</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>                                        
                        </td>
                    </tr>

                    <tr class="tr-description tr-field-ahalf">
                        <td class="col-xs-2 left"><span>Tổng số tiền</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="TotalMoney" runat="server" ClientIDMode="Static" Text="0" ReadOnly="true"></asp:TextBox>
                                <span class="unit-money">VNĐ</span>
                            </span>
                        </td>
                    </tr>

                    <tr class="tr-upload" style="display:none;">
                        <td class="col-xs-2 left"><span>Đăng ảnh</span></td>
                        <td class="col-xs-9 right">
                            <div class="wrap btn-upload-wp">
                                <div id="Btn_UploadImg" class="btn-upload-img" onclick="OpenIframeImage()">Chọn ảnh đăng</div>
                                <div class="wrap listing-img-upload"></div>
                            </div>                         
                        </td>
                    </tr>

                    <tr class="tr-description">
                        <td class="col-xs-2 left"><span>Ghi chú</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox TextMode="MultiLine" Rows="5" ID="NoteText" runat="server" ClientIDMode="Static"></asp:TextBox>
                            </span>
                        </td>
                    </tr>

                    <tr class="tr-field-ahalf" runat="server" id="TrSalon" visible="false">
                        <td class="col-xs-2 left"><span>Salon</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:DropDownList ID="Salon" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <asp:RequiredFieldValidator InitialValue="0" 
                                    ID="ValidateSalon" Display="Dynamic" 
                                    ControlToValidate="Salon"
                                    runat="server"  Text="Bạn chưa chọn Salon!" 
                                    ErrorMessage="Vui lòng chọn Salon!"
                                    ForeColor="Red"
                                    CssClass="fb-cover-error">
                                </asp:RequiredFieldValidator>
                            </span>
                        </td>
                    </tr>
                    
                    <tr class="tr-send">
                        <td class="col-xs-2 left"></td>
                        <td class="col-xs-9 right no-border">
                            <span class="field-wp">
                                <asp:Panel ID="BtnSend" CssClass="btn-send" runat="server" ClientIDMode="Static">Hoàn tất</asp:Panel>
                                <asp:Button ID="BtnPrintFromCodeBehind" CssClass="btn-send" runat="server" ClientIDMode="Static"
                                    OnClick="btnPrintFromCodeBehind_Click" Text="In hóa đơn" style="margin-left: 15px;"/>
                                <asp:Button ID="BtnFakeSend" runat="server" Text="Hoàn tất" 
                                    ClientIDMode="Static" OnClick="AddService" style="display:none;"></asp:Button>                                
                                <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>                                
                            </span>
                        </td>
                    </tr>
                    <asp:HiddenField runat="server" ID="UrlKeyField"  ClientIDMode="Static"/>
                    <asp:HiddenField runat="server" ID="ImgCoverField"  ClientIDMode="Static"/>
                </tbody>
            </table>

            <!-- input hidden -->
            <asp:HiddenField ID="HDF_CustomerCode" runat="server" ClientIDMode="Static"/>
            <asp:HiddenField ID="HDF_ProductIds" runat="server" ClientIDMode="Static"/>
            <asp:HiddenField ID="HDF_ServiceIds" runat="server" ClientIDMode="Static"/>
            <asp:HiddenField ID="HDF_TotalMoney" runat="server" ClientIDMode="Static"/>
            <asp:HiddenField ID="HDF_UserImages" runat="server" ClientIDMode="Static"/>
            <asp:HiddenField ID="HDF_UserImagesDelete" runat="server" ClientIDMode="Static"/>
            <!-- END input hidden -->
        </div>
    </div>
    <%-- end Add --%>
</div>

<!-- Danh mục sản phẩm -->
<div class="popup-product-wp popup-product-item">
    <div class="wp popup-product-head">
        <strong>Danh mục sản phẩm</strong>
    </div>
    <div class="wp popup-product-content" >
        <div class="wp popup-product-guide">
            <div class="left">Hướng dẫn:</div>
            <div class="right">
                <p>- Click vào ô check box để chọn sản phẩm</p>
                <p>- Click vào ô số lượng để thay đổi số lượng sản phẩm</p>
            </div>
        </div>
        <div class="wp listing-product item-product">
            <table class="table">
                <thead>
                    <tr>
                        <th><input type="checkbox" /></th>
                        <th>STT</th>
                        <th>Tên sản phẩm</th>
                        <th>Mã sản phẩm</th>
                        <th>Đơn giá</th>
                        <th class="th-product-quantity">Số lượng</th>
                        <th>Giảm giá</th>
                        <th>Thành tiền</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="Rpt_Product" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td class="td-product-checkbox item-product-checkbox">
                                    <input type="checkbox" value="<%# Eval("Id") %>"
                                        data-code="<%# Eval("Code") %>" data-name="<%# Eval("Name") %>"/>
                                </td>
                                <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                <td class="td-product-name"><%# Eval("Name") %></td>
                                <td class="td-product-code" data-id="<%# Eval("Id") %>"><%# Eval("Code") %></td>
                                <td class="td-product-price" data-price="<%# Eval("Price") %>"><%# Eval("Price") %></td>
                                <td class="td-product-quantity">
                                    <input type="text" class="product-quantity" value="1" />                                
                                </td>
                                <td class="td-product-voucher" data-voucher="<%# Eval("VoucherPercent") %>"><%# Eval("VoucherPercent") %> (%)</td>
                                <td class="map-edit">
                                    <div class="box-money"></div>
                                    <div class="edit-wp">
                                        <a class="elm del-btn" onclick="RemoveItem(this.parentNode.parentNode.parentNode,'<%# Eval("Name") %>','product')"
                                             href="javascript://" title="Xóa"></a>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>                                        
                </tbody>
            </table>
        </div> 
        <div class="wp btn-wp">
            <div class="popup-product-btn btn-complete" data-item="product">Hoàn tất</div>
            <div class="popup-product-btn btn-esc">Thoát</div>
        </div>
    </div>    
</div>
<!-- END Danh mục sản phẩm -->

<!-- Danh mục dịch vụ -->
<div class="popup-product-wp popup-service-item">
    <div class="wp popup-product-head">
        <strong>Danh mục dịch vụ</strong>
    </div>
    <div class="wp popup-product-content" >
        <div class="wp popup-product-guide">
            <div class="left">Hướng dẫn:</div>
            <div class="right">
                <p>- Click vào ô check box để chọn dịch vụ</p>
                <p>- Click vào ô số lượng để thay đổi số lượng dịch vụ</p>
            </div>
        </div>
        <div class="wp listing-product item-product">
            <table class="table">
                <thead>
                    <tr>
                        <th><input type="checkbox" /></th>
                        <th>STT</th>
                        <th>Tên dịch vụ</th>
                        <th>Mã dịch vụ</th>
                        <th>Đơn giá</th>
                        <th class="th-product-quantity">Số lượng</th>
                        <th>Giảm giá</th>
                        <th>Thành tiền</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="Rpt_Service" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td class="td-product-checkbox item-product-checkbox">
                                    <input type="checkbox" value="<%# Eval("Id") %>"
                                        data-code="<%# Eval("Code") %>" data-name="<%# Eval("Name") %>"/>
                                </td>
                                <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                <td class="td-product-name"><%# Eval("Name") %></td>
                                <td class="td-product-code" data-id="<%# Eval("Id") %>"><%# Eval("Code") %></td>
                                <td class="td-product-price" data-price="<%# Eval("Price") %>"><%# Eval("Price") %></td>
                                <td class="td-product-quantity">
                                    <input type="text" class="product-quantity" value="1" />                                
                                </td>
                                <td class="td-product-voucher" data-voucher="<%# Eval("VoucherPercent") %>"><%# Eval("VoucherPercent") %> (%)</td>
                                <td class="map-edit">
                                    <div class="box-money"></div>
                                    <div class="edit-wp">
                                        <a class="elm del-btn" onclick="RemoveItem(this.parentNode.parentNode.parentNode,'<%# Eval("Name") %>','service')"
                                            href="javascript://" title="Xóa"></a>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>                                        
                </tbody>
            </table>
        </div> 
        <div class="wp btn-wp">
            <div class="popup-product-btn btn-complete" data-item="service">Hoàn tất</div>
            <div class="popup-product-btn btn-esc">Thoát</div>
        </div>
    </div>    
</div>
<!-- END Danh mục dịch vụ --> 

<!-- In hóa đơn -->
<div id="PrintBill">
    <div class="print-wp" style="width: 219.212598425px; padding: 5px 5px 10px 5px;">
        <div class="logo" style="width: 100%; text-align: center; padding: 0 24%; margin-bottom: 5px;">
            <img class="prt-logo" src="/Assets/images/logo.jpg" style="max-width: 100%;" />
        </div>
        <div style="width: 100%; font-family: Arial, sans-serif; font-size: 14px; font-style: italic;">Ngày 28/10/2015</div>
        <table style="width: 100%;margin-top: 5px;">
            <tbody>
                <tr>
                    <td style="font-family: Arial, sans-serif; font-size: 14px;">Khách hàng</td>
                    <td style="font-family: Arial, sans-serif; font-size: 14px;">
                        : <b style="font-family: Arial, sans-serif; font-size: 14px;">Châu Kiệt Luân</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-family: Arial, sans-serif; font-size: 14px;">Mã KH</td>
                    <td style="font-family: Arial, sans-serif; font-size: 14px;">: KT100000</td>
                </tr>
            </tbody>            
        </table>

        <table style="border-collapse: collapse;width: 100%;margin-top: 5px;">
            <thead>
                <tr>
                    <th style="font-family: Arial, sans-serif; font-size: 14px; border: 1px solid #000;width: 80%; padding: 5px;">Dịch vụ</th>
                    <th style="font-family: Arial, sans-serif; font-size: 14px; border: 1px solid #000;width: 20%; padding: 5px;">SL</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="font-family: Arial, sans-serif; font-size: 14px; border: 1px solid #000;width: 80%; padding: 5px;">Shine Master</td>
                    <td style="font-family: Arial, sans-serif; font-size: 14px; border: 1px solid #000;width: 20%; padding: 5px; text-align: right;">1</td>
                </tr>
                <tr>
                    <td style="font-family: Arial, sans-serif; font-size: 14px; border: 1px solid #000;width: 80%; padding: 5px;">Gội đầu Massage</td>
                    <td style="font-family: Arial, sans-serif; font-size: 14px; border: 1px solid #000;width: 20%; padding: 5px; text-align: right;">1</td>
                </tr>
            </tbody>            
        </table>

        <table style="width: 100%;">
            <tbody>                
                <tr>
                    <td style="font-family: Arial, sans-serif; font-size: 14px; padding: 5px 5px 0px 5px;">Stylist :</td>
                    <td style="font-family: Arial, sans-serif; font-size: 14px; padding: 5px 5px 0px 5px; text-align: right;">
                        <b style="font-family: Arial, sans-serif; font-size: 14px;">Lã Mạnh Phúc</b>
                    </td>
                </tr>
                <tr>
                    <td style="font-family: Arial, sans-serif; font-size: 14px; padding: 2px 5px 0px 5px;">Skinner :</td>
                    <td style="font-family: Arial, sans-serif; font-size: 14px; padding: 2px 5px 0px 5px; text-align: right;"></td>
                </tr>
                <tr>
                    <td style="font-family: Arial, sans-serif; font-size: 14px; padding: 2px 5px 5px 5px;">Thành tiền :</td>
                    <td style="font-family: Arial, sans-serif; font-size: 14px; padding: 2px 5px 5px 5px; text-align: right;">170.000 VNĐ</td>
                </tr>
            </tbody>
        </table>

        <table style="border-collapse: collapse;width: 100%;">
            <thead>
                <tr>
                    <th style="font-family: Arial, sans-serif; font-size: 14px; border: 1px solid #000;width: 80%; padding: 5px;">Sản phẩm</th>
                    <th style="font-family: Arial, sans-serif; font-size: 14px; border: 1px solid #000;width: 20%; padding: 5px;">SL</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="border: 1px solid #000;width: 80%; padding: 5px; height: 29px;"></td>
                    <td style="border: 1px solid #000;width: 80%; padding: 5px; height: 29px;"></td>
                </tr>
                <tr>
                    <td style="border: 1px solid #000;width: 80%; padding: 5px; height: 29px;"></td>
                    <td style="border: 1px solid #000;width: 80%; padding: 5px; height: 29px;"></td>
                </tr>
                <tr>
                    <td style="border: 1px solid #000;width: 80%; padding: 5px; height: 29px;"></td>
                    <td style="border: 1px solid #000;width: 80%; padding: 5px; height: 29px;"></td>
                </tr>
            </tbody>            
        </table>

        <table style="font-family: Arial, sans-serif; font-size: 14px; width: 100%; margin-top: 5px;">
            <tbody>
                <tr>
                    <td style="font-family: Arial, sans-serif; font-size: 14px;">Seller :</td>
                    <td></td>
                </tr>
                <tr>
                    <td style="font-family: Arial, sans-serif; font-size: 14px;">Thành tiền :</td>
                    <td></td>
                </tr>
                <tr>
                    <td style="font-family: Arial, sans-serif; font-size: 14px;">Tổng số :</td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<!-- In hóa đơn -->

<script src="/assets/js/jquery.mCustomScrollbar.js"></script>
<link href="/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
<script type="text/javascript">
    jQuery(document).ready(function () {
        // Add active menu
        $("#glbService").addClass("active");

        // Mở box listing sản phẩm
        $(".show-product").bind("click", function () {
            $(this).parent().parent().find(".listing-product").show();
        });

        $(".show-item").bind("click", function () {
            var item = $(this).attr("data-item");
            var classItem = ".popup-" + item + "-item";

            $(classItem).openEBPopup();
            ExcCheckboxItem();
            ExcQuantity();
            ExcCompletePopup();

            $('#EBPopup .listing-product').mCustomScrollbar({
                theme: "dark-2",
                scrollInertia: 100
            });

            $("#EBPopup .btn-esc").bind("click", function () { autoCloseEBPopup(0); });
        });

        // Btn Send
        $("#BtnSend").bind("click", function () {
            if ($("#CustomerName").val() == "" && $("#InputCusNoInfor").is(":checked") == false) {
                showMsgSystem("Bạn chưa nhập mã khách hàng hoặc chưa tích chọn \"Khách không cho thông tin\".", "warning");
            } else if ($("#HDF_TotalMoney").val() == "") {
                showMsgSystem("Bạn chưa chọn dịch vụ hoặc sản phẩm.", "warning");
            } else {
                // Check cosmetic seller
                if (($("#HDF_ProductIds").val() != "" && $("#HDF_ProductIds").val() != "[]") && $("#Cosmetic").val() == 0) {
                    $("#ValidateCosmetic").css({ "visibility": "visible" });
                } else {
                    $("#ValidateCosmetic").css({ "visibility": "hidden" });
                    var CustomerCode = $("#HDF_CustomerCode").val();
                    // Check duplicate bill
                    $.ajax({
                        type: "POST",
                        url: "/GUI/FrontEnd/Service/Service_Add.aspx/CheckDuplicateBill",
                        data: '{CustomerCode : "' + CustomerCode + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            var mission = JSON.parse(response.d);
                            if (mission.success) {
                                var products = "Sản phẩm : ";
                                var services = "Dịch vụ : ";
                                var confirm_content = "";
                                var bill = JSON.parse(mission.msg);
                                if (bill.ProductIds != "") {
                                    var _Product = $.parseJSON(bill.ProductIds);
                                    $.each(_Product, function (i, v) {
                                        products += v.Name + " ( " + v.Price + " ), ";
                                    });
                                    products = products.replace(/(\,\s)+$/, '');
                                }
                                if (bill.ServiceIds != "") {
                                    var _Service = $.parseJSON(bill.ServiceIds);
                                    $.each(_Service, function (i, v) {
                                        services += v.Name + " ( " + v.Price + " ), ";
                                    });
                                    services = services.replace(/(\,\s)+$/, '');
                                }
                                confirm_content += products + ", " + services + ", ";
                                confirm_content = confirm_content.replace(/(\,\s)+$/, '');

                                // show EBPopup
                                $(".confirm-yn").openEBPopup();
                                $("#EBPopup .confirm-yn-text").text("Khách hàng mã [" + CustomerCode + "] đã được lập hóa đơn trong hôm nay [ " + confirm_content + " ]. Bạn có chắc muốn thêm hóa đơn mới ?");

                                $("#EBPopup .yn-yes").bind("click", function () {
                                    Bind_UserImagesToHDF();
                                    $("#BtnFakeSend").click();
                                });
                                $("#EBPopup .yn-no").bind("click", function () {
                                    autoCloseEBPopup(0);
                                });
                            } else {
                                Bind_UserImagesToHDF();
                                $("#BtnFakeSend").click();
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    });
                    //Bind_UserImagesToHDF();
                    //$("#BtnFakeSend").click();
                }
            }

        });
    });

    // Xử lý mã khách hàng => bind khách hàng
    function LoadCustomer(code) {
        ajaxGetCustomer(code);
    }

    // Xử lý khi chọn checkbox sản phẩm
    function ExcCheckboxItem() {
        $("#EBPopup .td-product-checkbox input[type='checkbox']").unbind("click").bind("click", function () {
            var obj = $(this).parent().parent(),
                boxMoney = obj.find(".box-money"),
                price = obj.find(".td-product-price").data("price"),
                quantity = obj.find("input.product-quantity").val(),
                voucher = obj.find(".td-product-voucher").data("voucher"),
                checked = $(this).is(":checked"),
                item = obj.attr("data-item");


            if (checked) {
                boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100)).show();
            } else {
                boxMoney.text("").hide();
            }

        });
    }

    // Số lượng | Quantity
    function ExcQuantity() {
        $("input.product-quantity").bind("change", function () {
            var obj = $(this).parent().parent(),
                quantity = $(this).val(),
                price = obj.find(".td-product-price").data("price"),
                voucher = obj.find(".td-product-voucher").data("voucher"),
                boxMoney = obj.find(".box-money");

            boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100)).show();
            TotalMoney();
            getProductIds();
            getServiceIds();
        });
    }

    // Xử lý click hoàn tất chọn item từ popup
    function ExcCompletePopup() {
        $("#EBPopup .btn-complete").unbind("click").bind("click", function () {
            var item = $(this).attr("data-item"),
                tableItem = $("table.table-item-" + item + " tbody"),
                objTmp;

            $("#EBPopup .item-product-checkbox input[type='checkbox']:checked").each(function () {
                var Code = $(this).attr("data-code");
                if (!ExcCheckItemIsChose(Code, tableItem)) {
                    objTmp = $(this).parent().parent().clone().find("td:first-child").remove().end();
                    tableItem.append(objTmp);
                }
            });

            TotalMoney();
            getProductIds();
            getServiceIds();
            ExcQuantity();
            UpdateItemOrder(tableItem);
            autoCloseEBPopup(0);
        });
    }

    // Check item đã được chọn
    function ExcCheckItemIsChose(Code, itemClass) {
        var result = false;
        $(itemClass).find(".td-product-code").each(function () {
            var _Code = $(this).text().trim();
            if (_Code == Code)
                result = true;
        });
        return result;
    }

    // Remove item đã được chọn
    function RemoveItem(THIS, name, itemName) {
        // Confirm
        $(".confirm-yn").openEBPopup();

        $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn hủy [ " + name + " ] ?");
        $("#EBPopup .yn-yes").bind("click", function () {
            THIS.remove();
            TotalMoney();
            getProductIds();
            getServiceIds();
            UpdateItemDisplay(itemName);
            autoCloseEBPopup(0);
        });
        $("#EBPopup .yn-no").bind("click", function () {
            autoCloseEBPopup(0);
        });
    }

    // Update table display
    function UpdateItemDisplay(itemName) {
        var dom = $(".table-item-" + itemName);
        var len = dom.find("tbody tr").length;
        if (len == 0) {
            dom.parent().hide();
        } else {
            UpdateItemOrder(dom.find("tbody"));
        }
    }

    // Update order item
    function UpdateItemOrder(dom) {
        var index = 1;
        dom.find("tr").each(function () {
            $(this).find("td.td-product-index").text(index);
            index++;
        });
    }

    function TotalMoney() {
        var Money = 0;
        $(".fe-service-table-add .box-money:visible").each(function () {
            Money += parseInt($(this).text().replace(/\./gm, ""));
        });
        $("#TotalMoney").val(FormatPrice(Money));
        $("#HDF_TotalMoney").val(Money);
    }

    function showMsgSystem(msg, status) {
        $("#MsgSystem").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);
        setTimeout(function () {
            $("#MsgSystem").fadeTo("slow", 0, function () {
                $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 1);
            });
        }, 5000);
    }

    function getProductIds() {
        var Ids = [];
        var prd = {};
        $("table.table-item-product tbody tr").each(function () {
            prd = {};
            var THIS = $(this);

            prd.Id = THIS.find("td.td-product-code").attr("data-id");
            prd.Code = THIS.find("td.td-product-code").text().trim();
            prd.Name = THIS.find("td.td-product-name").text().trim();
            prd.Price = THIS.find(".td-product-price").attr("data-price");
            prd.Quantity = THIS.find("input.product-quantity").val();
            prd.VoucherPercent = THIS.find(".td-product-voucher").attr("data-voucher");

            Ids.push(prd);

        });
        Ids = JSON.stringify(Ids);
        $("#HDF_ProductIds").val(Ids);
    }

    function getServiceIds() {
        var Ids = [];
        var prd = {};
        $("table.table-item-service tbody tr").each(function () {
            prd = {};
            var THIS = $(this),
                obj = THIS.parent().parent();

            prd.Id = THIS.find("td.td-product-code").attr("data-id");
            prd.Code = THIS.find("td.td-product-code").text().trim();
            prd.Name = THIS.find("td.td-product-name").text().trim();
            prd.Price = THIS.find(".td-product-price").attr("data-price");
            prd.Quantity = THIS.find("input.product-quantity").val();
            prd.VoucherPercent = THIS.find(".td-product-voucher").attr("data-voucher");

            Ids.push(prd);

        });
        Ids = JSON.stringify(Ids);
        $("#HDF_ServiceIds").val(Ids);
    }

    // Get Customer
    function ajaxGetCustomer(CustomerCode) {
        $.ajax({
            type: "POST",
            url: "/GUI/FrontEnd/Service/Service_Add.aspx/GetCustomer",
            data: '{CustomerCode : "' + CustomerCode + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                var mission = JSON.parse(response.d);
                if (mission.success) {
                    var customer = JSON.parse(mission.msg);
                    customer = customer[0];
                    $("#CustomerName").val(customer.Fullname);
                    $("#CustomerName").attr("data-code", customer.Customer_Code);
                    $("#HDF_CustomerCode").val(customer.Customer_Code);

                } else {
                    var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                    showMsgSystem(msg, "warning");
                }
            },
            failure: function (response) { alert(response.d); }
        });
    }

    // Print Bill
    function printGrid(selector) {
        var windowUrl = '';
        var _Data = $(selector);
        //set print document name for gridview
        var uniqueName = new Date();
        var windowName = 'Print_' + uniqueName.getTime();

        var prtWindow = window.open(windowUrl, windowName,
        'left=100,top=100,right=100,bottom=100,width=700,height=500');
        prtWindow.document.write('<html><head></head>');
        prtWindow.document.write('<body style="background:none !important;">');
        prtWindow.document.write(_Data.html());

        prtWindow.document.write('</body></html>');
        prtWindow.document.close();
        prtWindow.focus();
        prtWindow.print();
        prtWindow.close();
    }

</script>

</asp:Panel>
</asp:Content>

