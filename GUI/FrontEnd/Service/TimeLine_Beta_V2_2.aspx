﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="TimeLine_Beta_V2_2.aspx.cs" Inherits="_30shine.GUI.FrontEnd.UIService.TimeLine_Beta_V2_2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Timeline</title>
    <script src="https://www.gstatic.com/firebasejs/3.7.3/firebase.js"></script>
    <script>
        // Initialize Firebase
        var config = {
            apiKey: "AIzaSyAna6aEaHXkOJZ8NTChXOlJtvM7QkfH0-s",
            authDomain: "erp-30shine.firebaseapp.com",
            databaseURL: "https://erp-30shine.firebaseio.com",
            storageBucket: "erp-30shine.appspot.com",
            messagingSenderId: "80706579109"
        };
        firebase.initializeApp(config);
    </script>
    <script src="/Assets/js/erp.30shine.com/timeline.firebase.js?v=5"></script>

    <script src="/assets/js/jquery.mCustomScrollbar.js"></script>
    <link href="/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
    <%--<link href="/Assets/css/timeline.css?v=1" rel="stylesheet" />  --%>
    <script src="/Assets/js/erp.30shine.com/timeline_beta_V2.js?v=3498758"></script>
    <script src="/Assets/libs/magicsuggest-master/magicsuggest.js"></script>

    <link href="/Assets/libs/magicsuggest-master/magicsuggest.css" rel="stylesheet" />
    <link href="/Assets/css/erp.30shine.com/timeline_beta.css?v=27" rel="stylesheet" />
    <link href="/Assets/css/checkin.css" rel="stylesheet" />
    <style type="text/css">
        .board-data .board-cell-item.is-book.is-book-at-salon.col-6 {
            background: #397ad3;
        }

        .event {
            cursor: move;
        }
        .min, .sec {
            color: #ffffff;
        }
        /*
        * Update timeline - estimate timecut
        */
        .timeline-board.etc-active { width: 75%; overflow-x: hidden; }
        .timeline-estimate-timecut { width: 25%; float: left; background: #ffffff; display:none; }
        .timeline-estimate-timecut.etc-active { display:block; }
        .timeline-estimate-timecut .estimate-top { width: 100%; float: left; line-height: 50px; font-size: 18px; font-family: SFT Regular; text-align: center; background: #3a4760; color: #ffffff; }
        .timeline-estimate-timecut .estimate-content { width: 100%; float: left; }
        .timeline-estimate-timecut .estimate-content table { width: 100%; float: left; }
        .timeline-estimate-timecut .estimate-content table, 
        .timeline-estimate-timecut .estimate-content table th, 
        .timeline-estimate-timecut .estimate-content table td {
            border: 1px solid black;
        }
        .timeline-estimate-timecut .estimate-content table thead{ background: #ddd; }
        .timeline-estimate-timecut .estimate-content table th { font-weight: normal; font-family: Roboto Condensed Bold; text-align: center; padding: 5px; }
        .timeline-estimate-timecut .estimate-content table td { line-height: 20px; padding: 5px; }
        .timeline-estimate-timecut .estimate-content table td:first-child { text-align: center; }
        .timeline-estimate-timecut .estimate-content table td:last-child { text-align: center; font-size: 18px; font-family: Roboto Condensed Bold; }
    </style>
    <script>    
        var billClass = function ()
        {
            this.billID = 0;
            this.bookingID = 0; // ID SM_BookingTemp
            this.hourID = 0;
            this.hourFrame = '';
            this.stylistID = 0;
            this.stylistName = '';
            this.customerPhone = '';
            this.customerName = '';
            this.salonID = 0;
            this.salonName = '';
            this.checkinID = 0;
            this.checkinName = '';            
            this.services = [];
            this.teamID = 0;                        
            this.PDFBillCode = '';
            this.HCItem = '';
            this.IsAutoStylist = true;
            this.IsBookAtSalon = true;
        }

        var bill = new billClass();
        bill.salonID = <%=salonID%>;

        var cancelBookingClass = function()
        {
            this.customerPhone = '';
            this.customerCode = '';
            this.cancelBookingID = 0;
            this.noteCancel = '';
            this.itemBookingCancel = null;
        }
        var bookingCancel = new cancelBookingClass();

        var ms;
        var listService = <%=listService%>;
        var defaultServiceID = 53;

        /* params drag and drop cell timeline */
        var customerPhone =  "";
        var stylist = 0;
        var hourId = 0;
        var olderHourdId = 0;
        var bookingId = 0;
        var billIdCurrent= 0;
         

        jQuery(document).ready(function () {
            
            //$("#eventBillAdd").openEBPopup();
            setTimeLine();
            var autoSetTimeLine = setInterval(setTimeLine, 10000);
            window.onload = function (){
                alert("dfsdf");

                if($('.event').hasClass('is-makebill')){
                    $('.is-checkout').removeClass('event');
                    $('.is-checkout').attr('draggable', 'false');
                }
                if($('.event').hasClass('is-checkout')){
                    $(This).parent().find(".customer-text-wrap").find(".fa-warning").css("display", "none");
                }
                loadSetWaitingTime();
            }
            if($('.event').hasClass('is-makebill')){
                $('.is-checkout').removeClass('event');
                $('.is-checkout').attr('draggable', 'false');
            }
            if($('.event').hasClass('is-checkout')){
                $('.is-checkout').find(".board-cell-content").find(".customer-name").find(".customer-text-wrap").find(".fa-warning").css("display", "none");
            }
            loadSetWaitingTime();
        });
        
    </script>
    <script src="/Assets/js/erp.30shine.com/timeline_beta_V2.js?v=90"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="timeline-wp" style="padding-left: 30px;">
            <div class="timeline-head">
                <h2 class="title-head">Timeline</h2>
                <div class="note-head" <%= Perm_ViewAllData == true ? "" : "style='display:none;'"%>>
                    <asp:DropDownList runat="server" CssClass="form-control select" ID="ddlSalon" AutoPostBack="true" style="margin-top: 4px; width: 160px;" ClientIDMode="Static" 
                        OnSelectedIndexChanged="updatePanelBindData" onchange="addLoading()"></asp:DropDownList>
                </div>
                <div class="note-head" style="padding-left: 5px;">
                    <a href="javascript:void(0);" onclick="openFrameBooking()" class="open-booking-list note-head-item">
                        <span class="fa fa-calendar" style="padding-left:0;"></span>
                        Đặt lịch cho khách</a>
                    <div class="frame-booking-pane">
                        <%--<iframe src="https://30shine.com/erp-booking" class="iframe-booking"></iframe>--%>
                        <iframe src="https://30shine.com/erp-booking" class="iframe-booking"></iframe>
                    </div>
                </div>
                <div class="note-head">
                    <div class="note-head-item">
                        <span class="item-color item-color-book"></span><span class="item-text">Khách book</span>
                    </div>
                    <div class="note-head-item">
                        <span class="item-color item-color-bill"></span><span class="item-text">Đã in bill</span>
                    </div>
                    <div class="note-head-item item-color-customer-wait">
                        <i class="fa fa-check is-call" aria-hidden="true"></i><span class="item-text">Khách đang chờ</span>
                    </div>
                    <div class="note-head-item">
                        <span class="item-color item-color-BookAtSalon"></span><span class="item-text">Khách book tại salon</span>
                    </div>
                    <div class="note-head-item">
                        <i class="fa fa-star" aria-hidden="true" style="color: #fddd4a; font-size: 18px;"></i><span class="item-text">&nbsp; Khách đặc biệt</span>
                    </div>
                    <div class="note-head-item">
                        <i class="fa fa-warning" aria-hidden="true" style="color: #F44336; font-size: 18px;"></i><span class="item-text">&nbsp; Khách đang chờ lâu</span>
                        
                    </div>
                    <a target="_blank" href="/tracking/danh-sach/yeu-cau-dac-biet-cua-khach.html" class="a_btn_tracking">DS KH yêu cầu đặc biệt</a>
                </div>
                <div class="search-frame hidden">
                    <div class="btn-search"></div>
                    <div class="input-frame" style="">
                        <input type="text" placeholder="Nhập số điện thoại khách hàng" autofocus="autofocus" onkeypress="searchByPhone(event, $(this))" />
                    </div>
                </div>
            </div>

            <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
            <asp:UpdatePanel runat="server" ID="UP01">
                <ContentTemplate>
                    <!-- Timeline Board -->
                    <div class="timeline-board <%= Array.IndexOf(new int[] { 4,5 }, salonID) != -1 ? "etc-active" : "" %>">
                        <div class="vertical-bar">
                            <div class="vertical-bar-relative">
                                <div class="circle"></div>
                            </div>
                        </div>
                        <div class="board-data board-stylist">
                            <div class="board-cell board-icon" id="board-hour-icon"></div>

                            <% if (dataTimeline.Count > 0)
                                { %>
                            <% int loop = 1; int teamId = 0;
                                foreach (var v in dataTimeline)
                                { %>
                            <div class="board-row <%= (loop++)%2 == 0 ? "highlight" : "" %>" <%--style="<%= (teamId != 0 && teamId != v.TeamId) ? "border-top: 1px solid #000000;" : "" %>"--%>>
                                <div class="board-cell" data-id="<%=v.StylistId %>">
                                    <div class="board-cell-content"><%=v.StylistName %></div>
                                    <div style="position: absolute; z-index: 999; right: 5px; top: -8px; font-family: Roboto Condensed Bold; color: #42cabf;">
                                        <%=v.BillTotal %>
                                    </div>
                                    <div style="position: absolute; z-index: 999; left: -17px; top: 5px; font-family: Roboto Condensed Bold; color: #475866;">
                                        <%=v.TeamId %>
                                    </div>
                                </div>
                            </div>
                            <% teamId = v.TeamId;
                                } %>

                            <% } %>
                        </div>
                        <div class="board-data-wrap">
                            <div class="board-data board-book">
                                <div class="board-book-wrap" id="timeline">
                                    <div class="board-row board-hour">
                                        <% if (listHour.Count > 0)
                                            { %>
                                        <% foreach (var v in listHour)
                                            { %>
                                        <div class="board-cell"><%=v.Substring(0,5) %></div>
                                        <%} %>
                                        <% foreach (var v in listHour)
                                            { %>
                                        <div class="board-cell"></div>
                                        <%} %>
                                        <%} %>
                                    </div>

                                    <% if (dataTimeline.Count > 0)
                                        { %>
                                    <% int loop = 1; int teamId = 0;
                                        foreach (var v in dataTimeline)
                                        {   %>
                                    <% if (v.ListHour.Count > 0)
                                        { %>
                                    <div class="board-row board-book-stylist <%--<%= (loop++)%2 == 0 ? "highlight" : "" %>--%>" <%--style="<%= (teamId != 0 && teamId != v.TeamId) ? "border-top: 1px solid #000000!important;" : "" %>"--%>>
                                        <% foreach (var v2 in v.ListHour)
                                            { %>
                                        <% if (v2.HourData.Count > 0)
                                            { %>
                                        <div class="board-cell" id="<%=v.StylistId %>-<%=v2.HourId %>" ondragleave="timeline_dragleave($(this), event);" ondragenter="timeline_drop($(this), event);" ondragover="timeline_drop($(this), event);" ondrop="timeline_drop($(this), event);">
                                            <% foreach (var v3 in v2.HourData)
                                                { %>
                                            <div class="board-cell-item event <%= v3.BillID != null ? "is-makebill" :"is-book" %> <%=v3.IsBookAtSalon ? "is-book-at-salon" : "" %> <%=v2.HourData.Count > 1 ? " col-6" : "" %> <%= v2.IsServiced ? "is-enroll" : "not-enroll" %> <%= v3.CompleteBillTime != null ? "is-checkout" : "" %>" id="<%=v.StylistId %>-<%=v3.BookingID %>-bk" draggable="true" ondragstart="timeline_drag($(this), event);">
                                                <div class="board-cell-content" onclick="openPrintBill($(this), <%=v3.BillID != null ? v3.BillID : 0 %>, <%=v3.BookingID %>, <%=v.StylistId %>, '<%=v.StylistName %>', '<%=v3.CustomerPhone %>', '<%=v3.CustomerName %>', <%=v2.HourId %>, '<%=v2.HourFrame %>', <%=v3.IsAutoStylist ? 1 : 0 %>, <%=v3.IsBookAtSalon ? 1 : 0 %>)">
                                                    <p class="customer-name">
                                                        <span class="customer-text-wrap">
                                                            <i class="fa fa-warning hidden" aria-hidden="true" style="margin-right: 5px; color: #F44336; font-size: 14px; display:inline-block;"></i>
                                                            <%= v3.CustomerName %>
                                                            <i class="fa fa-star <%= v3.CustomerType == null ? "hidden" : "" %>" aria-hidden="true" style="margin-left: 5px; color: #fddd4a; font-size: 14px; display:inline-block;"></i>
                                                            <span class="show-checkout"></span>
                                                        </span>
                                                    </p>
                                                    <p class="customer-phone">
                                                        <%=v3.CustomerPhone %>
                                                    </p>
                                                    <%--<p class="bill-created-time"></p>--%>
                                                    <div class="bill-created-time" style="display: none; color:#ffffff;">
                                                         <label class="min" data-min="00">00</label>:<label class="sec" data-sec="00">00</label>
                                                        <%--<%= v3.BillCreatedDate != null ? String.Format("{0:HH}",v3.BillCreatedDate) + ":" + String.Format("{0:mm}",v3.BillCreatedDate)  : ""%>--%>
                                                    </div>
                                                </div>
                                                <div class="order-checked" style="position: absolute; bottom: 0px; right: 22px; color:#ffffff; display: inline-block;""><%= v3.Order%></div>
                                                <div class="call-phone" title="Khách chờ tại salon" onclick="checkCallPhone($(this), <%=v3.BookingID %>)" 
                                                    data-time="<%= v3.IsCallTimeModified != null ? String.Format("{0:dd-MM-yyyy HH:mm:ss}", v3.IsCallTimeModified) : String.Format("{0:dd-MM-yyyy HH:mm:ss}", v3.IsCallTime) %>" 
                                                    data-createbill="<%= String.Format("{0:dd-MM-yyyy HH:mm:ss}", v3.BillCreatedDate) %>"
                                                    data-procedure="<%= v3.InProcedureTimeModifed != null ? String.Format("{0:dd-MM-yyyy HH:mm:ss}", v3.InProcedureTimeModifed) : String.Format("{0:dd-MM-yyyy HH:mm:ss}", v3.InProcedureTime)  %>"
                                                    data-completetime="<%= v3.CompleteBillTime != null ? String.Format("{0:dd-MM-yyyy HH:mm:ss}", v3.CompleteBillTime) : "" %>">
                                            
                                                    <i class="fa fa-check <%= v3.IsCall != null && v3.IsCall.Value ? "is-call" : "" %>" aria-hidden="true"></i>
                                                    <%--<span class="text-bill-checkout">1</span>--%>
                                                </div>

                                                <% if (v3.BillID == null)
                                                    { %>
                                                <div class="cancel-book" title="Hủy lịch đặt" onclick="openCancelBook($(this), <%=v3.BookingID %>, '<%= v3.CustomerName %>', '<%=v3.CustomerPhone %>')">
                                                    <i class="fa fa-times" aria-hidden="true"></i>
                                                </div>
                                                <% } %>
                                                <div class="board-cell-action" data-id="<%=v.StylistId %>-<%=v.StylistName %>-<%=v2.HourId %>-<%=v2.HourFrame %>">
                                                    <div class="col-xs-6 cell-item-action cell-item-action-printbill" onclick="openPrintBill($(this), 0, <%=v3.BookingID %>, <%=v.StylistId %>, '<%=v.StylistName %>', '<%=v3.CustomerPhone %>', '<%=v3.CustomerName %>', <%=v2.HourId %>, '<%=v2.HourFrame %>', <%=v3.IsAutoStylist ? 1 : 0 %>, <%=v3.IsBookAtSalon ? 1 : 0 %>)">In phiếu</div>
                                                    <div class="col-xs-6 cell-item-action cell-item-action-book" onclick="openBooking($(this), 0, <%=v.StylistId %>, '<%=v.StylistName %>', '', '', <%=v2.HourId %>, '<%=v2.HourFrame %>')">Book lịch</div>
                                                </div>
                                                <div class="stylist-not-auto <%= !(v3.IsAutoStylist) ? "active" : "" %>">B</div>
                                                <div class="customer-hc <%= (v3.IsHCItem) ? "active" : "" %>">H</div>
                                            </div>
                                            <%} %>
                                        </div>
                                        <%--<div class="board-cell"><div class="board-cell-content">asdf</div></div>--%>
                                        <%}
                                            else
                                            { %>
                                        <div class="board-cell" id="<%=v.StylistId %>-<%=v2.HourId %>" ondragleave="timeline_dragleave($(this), event);" ondragenter="timeline_drop($(this), event);" ondragover="timeline_drop($(this), event);" ondrop="timeline_drop($(this), event);">
                                            <div class="board-cell-item <%= v2.IsServiced ? "is-enroll" : "not-enroll" %>">
                                                <div class="board-cell-content"></div>
                                                <div class="board-cell-action" data-id="<%=v.StylistId %>-<%=v.StylistName %>-<%=v2.HourId %>-<%=v2.HourFrame %>">
                                                    <div class="col-xs-6 cell-item-action cell-item-action-printbill" onclick="openPrintBill($(this),0, 0, <%=v.StylistId %>, '<%=v.StylistName %>', '', '', <%=v2.HourId %>, '<%=v2.HourFrame %>', 1, 0)">In phiếu</div>
                                                    <div class="col-xs-6 cell-item-action cell-item-action-book" onclick="openBooking($(this), 0, <%=v.StylistId %>, '<%=v.StylistName %>', '', '', <%=v2.HourId %>, '<%=v2.HourFrame %>')">Book lịch</div>
                                                </div>
                                            </div>
                                        </div>
                                        <% } %>
                                        <%} %>
                                    </div>
                                    <%teamId = v.TeamId;
                                        } %>
                                    <%} %>
                                    <% } %>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/ Timeline Board -->
                    <!-- Estimate Timecut -->
                    <div class="timeline-estimate-timecut <%= Array.IndexOf(new int[] { 4,5 }, salonID) != -1 ? "etc-active" : "" %>">
                        <div class="estimate-top">Ước lượng thời gian cắt</div>
                        <div class="estimate-content">
                            <table id="estimate-timecut-table">
                                <thead>
                                    <th>STT</th>
                                    <th>KH</th>
                                    <th>SĐT</th>
                                    <th>Stylist</th>
                                    <th>Còn lại (phút)</th>
                                </thead>
                                <tbody>
                                    <%--<tr>
                                        <td>1</td>
                                        <td>Vọng</td>
                                        <td>0868606683</td>
                                        <td>Mạnh Phúc</td>
                                        <td>30</td>
                                    </tr>--%>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--/ Estimate Timecut -->
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlSalon" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>
        </div>

        <!-- Popup thêm mới team -->
        <div class="event-bill-add" id="eventBillAdd">
            <%--<strong class="bill-add-head">Lập bill checkin</strong>--%>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 popup-head">
                        <p class="popup-hour-frame">08:00</p>
                        <p class="popup-stylist"><i class="fa fa-star-o star-not-auto-stylist" aria-hidden="true"></i>Stylist <span style="position: relative; top: -2px;">&nbsp;|&nbsp;</span> <span class="sp-stylist-name"></span></p>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group form-group-lg">
                            <div class="col-xs-12">
                                <label class="label-customer-name"><span>Khách hàng</span> <span class="customer-book-at-salon">( <span style="color: red; float: none;">*</span> Book tại salon )</span></label>
                                <input id="customerPhone" class="form-control customer-phone" type="text" placeholder="Số điện thoại" onchange="callCustomerData($(this))" onkeypress="return isNumber(event)" />
                                <input id="customerName" class="form-control customer-name" type="text" placeholder="Tên Khách hàng" style="margin-top: 10px;" />
                                <button id="addName" style="top:10px;float:right;margin-top:-34px;height:34px;background-color:#565656;border-color:#565656;" onclick="getCustomerName()" class="btn btn-success">Lưu</button>
                            </div>
                        </div>
                        <div class="form-group form-group-lg" <%= !Perm_ViewAllData ? "style='display:none;'" : "" %>>
                            <div class="col-xs-12">
                                <label>Salon</label>
                                <select class="form-control ddl-salon select" onchange="setSalon($(this))">
                                    <option value="1">Chọn salon</option>
                                    <% if (listSalon.Count > 0)
                                        { %>
                                    <% foreach (var v in listSalon)
                                        { %>
                                    <option value="<%=v.Id %>"><%=v.Name %></option>
                                    <%} %>
                                    <%} %>
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-group-lg service-wrap">
                            <div class="col-xs-12 suggestion-wrap">
                                <label>Dịch vụ</label>
                                <input class="form-control ms-suggest" type="text" placeholder="Search theo tên dịch vụ" />
                            </div>
                        </div>
                        <div class="form-group form-group-lg checkin-wrap">
                            <div class="col-xs-12 suggestion-wrap">
                                <label>Nhân viên checkin</label>
                                <div class="row">
                                    <input class="form-control checkin-code" type="text" placeholder="STT" style="width: 25%; float: left;" onkeypress="return isNumber(event)" onkeyup="setCheckin($(this))" />
                                    <input class="form-control checkin-name" type="text" placeholder="Nhân viên checkin" disabled="disabled" style="width: 75%; float: left;" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <% if (listTeam.Count > 0)
                            { %>
                        <div class="form-group form-group-lg team-wrap" style="display: none;">
                            <div class="col-xs-12 suggestion-wrap" style="padding-bottom: 5px;">
                                <label>Chọn team</label>
                            </div>
                            <div class="col-xs-12 list-team">
                                <% foreach (var v in listTeam)
                                    { %>
                                <div class="btn btn-success btn-team" style="background: <%=v.Color%>" onclick="setTeam($(this),<%=v.Id %>)">
                                    <i class="fa fa-check-square" aria-hidden="true"></i>
                                </div>
                                <%} %>
                            </div>
                        </div>
                        <div class="form-group form-group-lg hidden div_checkCustomer" style="margin-top: 10px; border-radius: 10px; background: #eeeeee; padding: 10px;">
                            <p class="customerType" style="text-transform:uppercase; font-family:'Roboto Condensed Bold'; font-size: 16px; text-align: center;"></p>
                            <p class="reason" style="font-family:'Roboto Condensed Bold'; font-size: 14px;"></p>
                        </div>
                        <div class="row list-item-hc">
                            <div class="col-xs-3">
                                <div class="checkbox" style="float: left;">
                                    <label>
                                        <input type="checkbox" data-value="1" onclick="setHoaChat($(this), 1)" />
                                        Tẩy
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="checkbox" style="float: left;">
                                    <label>
                                        <input type="checkbox" data-value="2" onclick="setHoaChat($(this), 2)" />
                                        Uốn
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="checkbox" style="float: left;">
                                    <label>
                                        <input type="checkbox" data-value="3" onclick="setHoaChat($(this), 3)" />
                                        Nhuộm
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row list-item-book-stylist">
                            <div class="col-xs-12">
                                <div class="checkbox" style="float: left; margin-top: 0px; margin-bottom: 2px;">
                                    <label>
                                        <input type="checkbox" onclick="setBookStylist($(this))" />
                                        Khách book Stylist
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="checkbox" style="float: left; margin-top: 0px; margin-bottom: 2px; display:none;">
                                    <label>
                                        <input type="checkbox" checked="checked" onclick="setBookSalon($(this))" />
                                        Khách book tại salon
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-group-lg">
                            <div class="col-xs-12">
                                <div class="btn-print-bill btn-action-print" onclick="printBill()">In phiếu</div>
                                <div class="btn-print-bill btn-action-print-update" onclick="updateBill()">Cập nhật phiếu</div>
                                <div class="btn-print-bill btn-action-book" onclick="insertBooking()">Đặt chỗ</div>
                            </div>
                        </div>
                    </div>
                    <% } %>
                </div>
            </div>
        </div>
        <!--/ Popup thêm mới team -->

        <!-- Popup hủy lịch đặt -->
        <div class="popup-cancel-book">
            <div class="popup-cancel-book-content">
                <p class="popup-title">Bạn có chắc chắn muốn hủy lịch đặt? Vui lòng nhập lý do:</p>
                <div class="note-wrap">
                    <textarea class="note-cancel" placeholder="Nhập lý do hủy lịch đặt" rows="3"></textarea>
                </div>
                <div class="button-action-wp">
                    <div class="button-action">
                        <div class="popup-btn btn-ok" onclick="cancelBook()">Xác nhận</div>
                        <div class="popup-btn btn-close" onclick="autoCloseEBPopup()">Đóng</div>
                    </div>
                </div>
            </div>
        </div>
        <!--// Popup hủy lịch đặt -->

        <!-- Page loading -->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
        <!--/ Page loading -->
        <div class="html_current hidden"></div>
        <!-- Iframe phục vụ in hóa đơn -->
        <iframe id="iframePrint" style="display: none;"></iframe>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                var qs = getQueryStrings();
                if (qs["msg_print_billcode"] != undefined) {
                    openPdfIframe("/Public/PDF/" + qs["msg_print_billcode"] + ".pdf");
                }
                showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);
            });
            function openPdfIframe(src) {
                var PDF = document.getElementById("iframePrint");
                PDF.src = src;
                PDF.onload = function () {
                    PDF.focus();
                    PDF.contentWindow.print();
                    PDF.contentWindow.close();
                    window.history.pushState({}, 'timeline', '/ver2-beta-timeline');
                }                
            }
        </script>
        <!--/ Iframe phục vụ in hóa đơn -->
    </asp:Panel>
    <style>
        .a_btn_tracking
        {
            font-family: Roboto Condensed Bold;
            float: left;
            margin-left: 25px;
            background: red;
            color: #FFF;
            padding: 7px 10px;
            margin-top: -4px;
        }

        .a_btn_tracking:hover
        {
            color: #FFF;
            text-decoration: underline !important;
        }
    </style>
</asp:Content>
