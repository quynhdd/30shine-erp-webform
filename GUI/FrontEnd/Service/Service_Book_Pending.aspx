﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Service_Book_Pending.aspx.cs" Inherits="_30shine.GUI.FrontEnd.Service.Service_Book_Pending" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <link href="/Assets/css/bootstrap/bootstrap-toggle.min.css" rel="stylesheet" />
        <script src="/Assets/js/bootstrap/bootstrap-toggle.min.js"></script>
        <script src="/Assets/js/check-notify.js"></script>

        <style>
            .no-padding { padding: 0px !important; }
            .pending-content-wrap { width: auto; float: left; }
            .table-add { width: auto !important; float: left; }
            .wr-choose-team { width: 60px; margin-right: 25px; float: left; }
                .wr-choose-team .team-color-select { width: 60px; float: left; }
                .wr-choose-team .time { float: left; width: 100%; line-height: 25px; }

            .status-team { line-height: 20px; color: red; height: 20px; }

            .booking-pending { padding: 12px; float: left; width: 100%; }
            .no-padding { padding: 0px !important; }
            .no-margin { margin: 0px !important; }
            input { background: none; border: 1px solid #d0d0d0; }
            .caption { padding-bottom: 10px; float: left; text-align: left; }
            .btn { padding: 5px; background: #d0d0d0; cursor: pointer; }
            .table { padding: 5px; }
                .table thead tr { background: #d0d0d0; }
                .table tr td { padding: 5px; }
            table .team { margin: 0px; }
                table .team::before { width: 100%; height: 1px; background: #000000; }

            table.table td { border: 1px solid #bababa; }
            .table-booking thead tr { border-top: 1px solid #bababa; }
                .table-booking thead tr td { font-family: Roboto Condensed Bold; }

            .team { /*background: #ff0000;*/ padding: 5px; margin: 5px; float: left; width: 50px; height: 20px; cursor: pointer; text-align: center; }
                .team:hover { border: 1px solid #d0d0d0; }
            .action { padding: 12px; }
            .item-color { margin-right: 0px; width: 50px; margin-right: 5px; }


            table.table-booked .pending { cursor: pointer; }
                table.table-booked .pending:hover { color: #50b347; }
            table.table-booked .a-team-color { padding: 0px; margin: 0; }
            table.table-booked .pending-team-color { height: 20px; }

            .customer-add .table-add tr.tr-send .btn-send { min-width: 80px; width: auto !important; padding: 0 10px; }

            /*.fe-service table.fe-service-table-add .iframe-add-customer iframe { min-height: 305px !important; }*/
            .customer-add .table-add .no-border { padding-top: 12px !important; }
            .fe-service table.fe-service-table-add .checkbox.cus-no-infor { margin-left: 0; }
            .customer-listing table.table-listing tbody tr .team-color-edit-box { left: auto; right: -285px; top: -40px; }
        </style>
        <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Dịch vụ &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/dich-vu/danh-sach.html">Danh sách</a></li>
                        <li class="li-pending"><a href="/dich-vu/pending.html">
                            <div class="pending-1"></div>
                            Pending</a></li>
                        <li class="li-add active"><a href="/dich-vu/them-phieu-v2.html">Thêm mới</a></li>
                        <li class="li-add active"><a href="/dich-vu/dat-lich.html">Đặt lịch</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add admin-product-add fe-service no-padding no-margin">
            <%-- Add --%>
            <div class="content-wp no-padding">

                <!-- END System Message -->
                <div class="container">
                    <!-- System Message -->
                    <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                    <div class="col-xs-12 col-lg-12 com-md-12 col-sm-12" style="padding-left: 0; padding-right: 0;">
                        <%--form input customer booking or pending--%>
                        <div class="col-xs-6 col-ld-6 col-md-6 col-sm-6">
                            <div class="table-wp">
                                <table class="table-add admin-product-table-add fe-service-table-add">
                                    <tbody>
                                        <%--   <tr class="title-head">
                                            <td><strong>Thông tin dịch vụ</strong></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr class="tr-margin" style="height: 20px;"></tr>--%>

                                        <tr class="tr-field-ahalf">
                                            <td class="col-xs-1 left"><span>KH</span></td>
                                            <td class="col-xs-11 right">
                                                <div class="field-wp">
                                                    <asp:TextBox ID="CustomerName" runat="server" ClientIDMode="Static" ReadOnly="true" Style="width: 190px; font-family: Roboto Condensed Bold; font-size: 15px;"></asp:TextBox>
                                                    <%--<asp:TextBox ID="CustomerCode" CssClass="mgl" placeholder="Nhập mã KH hoặc SĐT" runat="server" 
                                    ClientIDMode="Static" onchange="LoadCustomer(this.value)" style="width: 30%;"></asp:TextBox>--%>

                                                    <div class="filter-item" style="margin-left: 10px; z-index: 3000; width: 190px;">
                                                        <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion" data-field="customer.code" data-value="0"
                                                            data-callback="Callback_UpdateCustomerCode" AutoCompleteType="Disabled" ID="CustomerCode" ClientIDMode="Static"
                                                            onchange="CustomerInputOnchange($(this))" placeholder="Nhập mã KH hoặc SĐT" runat="server" Style="width: 25%;"></asp:TextBox>
                                                        <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                                                            <ul class="ul-listing-staff ul-listing-suggestion" id="Ul4"></ul>
                                                        </div>
                                                        <div class="fake-value"></div>
                                                    </div>

                                                    <div class="checkbox cus-no-infor" style="padding-top: 4px;">
                                                        <label class="lbl-cus-no-infor">
                                                            <input type="checkbox" id="InputCusNoInfor" runat="server" clientidmode="Static" />
                                                            Không cho thông tin
                                                        </label>
                                                    </div>
                                                    <div class="addnew-customer" onclick="quickAddCustomer($(this))" style="width: 82px; display: none;"><i class="fa fa-plus-circle"></i>&nbsp;Thêm mới</div>
                                                    <%--<div class="addnew-customer" onclick="quickAddCustomer($(this))" style="width: 110px;"><i class="fa fa-plus-circle"></i>&nbsp;Đồng bộ vân tay</div>--%>
                                                    <%--<asp:RequiredFieldValidator ID="ValidateCustomerName" ControlToValidate="CustomerName" runat="server" 
                                    CssClass="fb-cover-error" Text="Bạn chưa nhập mã khách hàng!" ClientIDMode="Static"></asp:RequiredFieldValidator>--%>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr class="tr-field-ahalf" id="TrIframeAddCustomer">
                                            <td class="col-xs-1 left"><span></span></td>
                                            <td class="col-xs-11 right">
                                                <div class="field-wp">
                                                    <div class="iframe-add-customer" id="IframeAddCustomer">
                                                        <iframe src="/khach-hang/them-moi-iframe.html"></iframe>
                                                    </div>
                                                    <%--<asp:RequiredFieldValidator ID="ValidateCustomerName" ControlToValidate="CustomerName" runat="server" 
                                    CssClass="fb-cover-error" Text="Bạn chưa nhập mã khách hàng!" ClientIDMode="Static"></asp:RequiredFieldValidator>--%>
                                                </div>
                                            </td>
                                        </tr>

                                        <tr class="tr-field-ahalf" runat="server" id="TrSalon" visible="false">
                                            <td class="col-xs-1 left"><span>Salon</span></td>
                                            <td class="col-xs-11 right">
                                                <span class="field-wp">
                                                    <asp:DropDownList ID="Salon" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator InitialValue="0"
                                                        ID="ValidateSalon" Display="Dynamic"
                                                        ControlToValidate="Salon"
                                                        runat="server" Text="Bạn chưa chọn Salon!"
                                                        ErrorMessage="Vui lòng chọn Salon!"
                                                        ForeColor="Red"
                                                        CssClass="fb-cover-error">
                                                    </asp:RequiredFieldValidator>
                                                </span>
                                            </td>
                                        </tr>

                                        <tr class="tr-send">
                                            <td class="col-xs-1 left"><span>Team</span></td>
                                            <td class="col-xs-11 right">
                                                <div class="field-wp ">
                                                    <p class="status-team"></p>
                                                    <div class="wr-choose-team">
                                                        <div class="team-color-select" runat="server" id="TeamColorSelect" clientidmode="Static" data-color="">
                                                        </div>
                                                        <p class="time"></p>
                                                    </div>
                                                    <asp:Repeater runat="server" ID="Rpt_TeamWork">
                                                        <ItemTemplate>
                                                            <div class="item-color" style="background: <%# Eval("Color")%>;" onclick="setTeamColor($(this),<%# Eval("Id") %>,'<%# Eval("Color") %>')"></div>
                                                        </ItemTemplate>
                                                    </asp:Repeater>

                                                </div>
                                            </td>
                                        </tr>

                                        <tr class="tr-send">
                                            <td class="col-xs-1 left"></td>
                                            <td class="col-xs-11 right no-border">
                                                <span class="field-wp">
                                                    <asp:Panel ID="BtnSend" CssClass="btn-send" runat="server" ClientIDMode="Static">In phiếu</asp:Panel>

                                                    <asp:Button ID="BtnFakeSend" runat="server" Text="Hoàn tất"
                                                        ClientIDMode="Static" OnClick="AddService" Style="display: none;"></asp:Button>

                                                    <asp:Panel ID="BtnBook" CssClass="btn-send btn-book" runat="server" ClientIDMode="Static" Style="margin-left: 12px;">Đặt lịch</asp:Panel>

                                                    <asp:Button ID="BtnFakeBook" runat="server" Text="Hoàn tất"
                                                        ClientIDMode="Static" OnClick="AddServiceBooking" Style="display: none;"></asp:Button>
                                                </span>

                                            </td>
                                        </tr>

                                        <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                                        <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
                                    </tbody>
                                </table>

                                <!-- input hidden -->
                                <asp:HiddenField ID="HDF_CustomerCode" runat="server" ClientIDMode="Static" />
                                <asp:HiddenField ID="HDF_ProductIds" runat="server" ClientIDMode="Static" />
                                <asp:HiddenField ID="HDF_ServiceIds" runat="server" ClientIDMode="Static" />
                                <asp:HiddenField ID="HDF_TotalMoney" runat="server" ClientIDMode="Static" />
                                <asp:HiddenField ID="HDF_UserImages" runat="server" ClientIDMode="Static" />
                                <asp:HiddenField ID="HDF_UserImagesDelete" runat="server" ClientIDMode="Static" />
                                <asp:HiddenField ID="Hairdresser" runat="server" ClientIDMode="Static" />
                                <asp:HiddenField ID="HairMassage" runat="server" ClientIDMode="Static" />
                                <asp:HiddenField ID="Cosmetic" runat="server" ClientIDMode="Static" />
                                <asp:HiddenField ID="HDF_SalonId" runat="server" ClientIDMode="Static" />
                                <asp:HiddenField ID="HDF_Suggestion_Code" ClientIDMode="Static" runat="server" />
                                <asp:HiddenField ID="HDF_Suggestion_Field" ClientIDMode="Static" runat="server" />
                                <asp:HiddenField ID="HDF_FreeService" ClientIDMode="Static" runat="server" />
                                <asp:HiddenField ID="HDF_TeamId" ClientIDMode="Static" runat="server" />
                                <asp:HiddenField ID="HDF_TeamId_Booking" ClientIDMode="Static" runat="server" />
                                <!-- END input hidden -->
                            </div>
                        </div>
                        <%--form listing booking--%>
                        <div class="col-xs-2 col-ld-2 col-md-2 col-sm-2">

                            <div class="wrap-table pending-content-wrap">
                                <asp:Label ID="Label2" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                                <p class="_p1">Chọn lịch hẹn</p>
                                <table class="table table-col-2 table-booking" id="TableBooking">
                                    <thead>
                                        <tr>
                                            <td>Team</td>
                                            <td>Giờ hẹn</td>
                                            <td>Trạng thái</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="RptBillServicePending" runat="server">
                                            <ItemTemplate>

                                                <tr id="<%#Eval("Id")%>" onclick="setBookingToForm($(this))">
                                                    <td>
                                                        <div class="team" data-color='<%#Eval("TeamColor")%>' data-book='<%#Eval("IsBooked")%>' style='background: <%#Eval("TeamColor") %>!important; padding: 5px;'>
                                                            <p style='<%#Convert.ToBoolean(Eval("IsBooked"))==true?"height: 5px;width: 136%; float: left;margin-left: -16%; margin-top: 6%;background: #000;": ""%>'></p>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <p class="time"><%# string.Format("{0:HH}",Eval("completeTime")) %>h<%# string.Format("{0:mm}",Eval("completeTime")) %></p>
                                                    </td>
                                                    <td class="td-bill-status">
                                                        <%# Convert.ToByte(Eval("Status")) == 1 ? "<span class=\"status-value status-step1\">Gội</span><span class=\"status-time\">Từ "+String.Format("{0:HH}", Eval("ShampooTime0")) + "h" + String.Format("{0:mm}", Eval("ShampooTime0")) +"</span>" : "" %>
                                                        <%# Convert.ToByte(Eval("Status")) == 2 ? "<span class=\"status-value status-step2\">Cắt</span><span class=\"status-time\">Từ "+String.Format("{0:HH}", Eval("HairCutTime0")) + "h" + String.Format("{0:mm}", Eval("HairCutTime0")) +"</span>" : "" %>
                                                        <%# Convert.ToByte(Eval("Status")) == 3 ? "<span class=\"status-value status-complete\">Hoàn thành</span><span class=\"status-time\">Lúc "+String.Format("{0:HH}", Eval("HairCutTime1")) + "h" + String.Format("{0:mm}", Eval("HairCutTime1")) +"</span>" : "" %>
                                                        <%--<span class="status-value status-complete">Gội</span><span class="status-time">Từ 15h33</span>--%>
                                                    </td>
                                                </tr>

                                            </ItemTemplate>
                                        </asp:Repeater>

                                        <%--   <tr>
                                            <td>
                                                <div class="team" style="background: #ff0000; padding: 5px;"></div>
                                            </td>
                                            <td>
                                                <p class="time">12:00</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="team">
                                                    <p class="number">4</p>
                                                </div>
                                            </td>
                                            <td>
                                                <p class="time">12:00</p>
                                            </td>
                                        </tr>--%>
                                    </tbody>
                                </table>
                            </div>

                        </div>

                        <%--form listing booked--%>
                        <div class="col-xs-4 col-ld-4 col-md-4 col-sm-4">
                            <div class="customer-listing be-report no-padding" style="float: left">
                                <%-- Listing --%>
                                <div class="content-wp no-padding">
                                    <!-- Filter -->
                                    <div class="row" style="display: none">
                                        <strong class="st-head"><i class="fa fa-filter"></i>Lọc kết quả</strong>
                                        <div class="filter-item">
                                            <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion" data-field="customer.code" data-value="0"
                                                AutoCompleteType="Disabled" ID="TextBox1" ClientIDMode="Static" placeholder="Mã Khách hàng" runat="server"></asp:TextBox>
                                            <div class="listing-staff-wp eb-select-data ">
                                                <ul class="ul-listing-staff ul-listing-suggestion" id="UlCustomerCode"></ul>
                                            </div>
                                        </div>
                                        <div class="filter-item">
                                            <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion" data-field="customer.name" data-value="0"
                                                AutoCompleteType="Disabled" ID="TextBox2" ClientIDMode="Static" placeholder="Nhập tên Khách hàng" runat="server"></asp:TextBox>
                                            <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                                                <ul class="ul-listing-staff ul-listing-suggestion" id="UlCustomerName">
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="filter-item">
                                            <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion" data-value="0" data-field="customer.phone"
                                                AutoCompleteType="Disabled" ID="CustomerPhone" ClientIDMode="Static" placeholder="Số điện thoại" runat="server"></asp:TextBox>
                                            <div class="listing-staff-wp eb-select-data">
                                                <ul class="ul-listing-staff ul-listing-suggestion" id="UlCustomerPhone"></ul>
                                            </div>
                                        </div>

                                        <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                                            onclick="excPaging(1)" runat="server">
                                            Xem dữ liệu
                                        </asp:Panel>
                                        <a href="/dich-vu/pending.html" class="st-head btn-viewdata">Reset Filter</a>
                                    </div>

                                    <!-- End Filter -->
                                    <div class="pending-content-wrap">
                                        <asp:Label ID="Label1" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                                        <p class="_p1">Khách đã hẹn</p>

                                        <%--   <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>--%>
                                        <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                                            <ContentTemplate>
                                                <div class="table-wp">
                                                    <table class="table-add table-listing table-booked">
                                                        <thead>
                                                            <tr>
                                                                <th>STT</th>
                                                                <th>Team</th>
                                                                <th>Giờ hẹn</th>
                                                                <%--<th>Dự kiến xong</th>--%>
                                                                <%--<th>Tiến hành booking</th>--%>
                                                                <%--<th>Khách booking</th>--%>
                                                                <%--<th>Mã Khách Hàng</th>--%>
                                                                <th>Khách hàng</th>
                                                                <th>Điện thoại</th>
                                                                <th>In phiếu</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <asp:Repeater ID="RptBillService" runat="server" OnItemDataBound="Repeater_ItemDataBound">
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td><a href="/dich-vu/pending/<%# Eval("Id") %>.html" style="font-size: 15px; font-family: Roboto Condensed Bold;"><%# Eval("billOrder") %></a></td>
                                                                        <td>
                                                                            <a href="javascript:void(0);" class="a-team-color">
                                                                                <div class="pending-team-color" style="background: <%# Eval("TeamColor") %>!important" onclick="showBoxColorEdit($(this))"></div>
                                                                                <div class="team-color-edit-box">
                                                                                    <%foreach (var v in lstTeamService)
                                                                                        { %>
                                                                                    <div class="color-box-edit" style="background: <%= v.Color %>!important" onclick="updateTeamColor($(this),<%= v.Id %>,'<%=v.Color %>', <%# Eval("Id") %>)"></div>
                                                                                    <% } %>
                                                                                </div>
                                                                            </a>
                                                                        </td>
                                                                        <td>
                                                                            <a href="/dich-vu/pending/<%# Eval("Id") %>.html">
                                                                                <%# string.Format("{0:HH}",Eval("CreatedDate")) %>h<%# string.Format("{0:mm}",Eval("CreatedDate")) %>
                                                                            </a>
                                                                        </td>

                                                                        <td>
                                                                            <a href="/dich-vu/pending/<%# Eval("Id") %>.html">
                                                                                <%# Eval("CustomerName") %>
                                                                            </a>
                                                                        </td>
                                                                        <td><a href="/dich-vu/pending/<%# Eval("Id") %>.html"><%# Eval("CustomerPhone") %></a></td>

                                                                        <td>

                                                                            <p class="pending" onclick="setPending($(this),<%# Eval("Id")%>)">In phiếu</p>
                                                                        </td>

                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </tbody>
                                                    </table>
                                                </div>

                                                <!-- Paging -->
                                                <div class="site-paging-wp">
                                                    <% if (PAGING.TotalPage > 1)
                                                        { %>
                                                    <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                                        <% if (PAGING._Paging.Prev != 0)
                                                            { %>
                                                        <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                                        <% } %>
                                                        <asp:Repeater ID="RptPaging" runat="server">
                                                            <ItemTemplate>
                                                                <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                                                    <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                                                    <%# Eval("PageNum") %>
                                                                </a>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                        <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                                            { %>
                                                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                                        <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                                        <% } %>
                                                    </asp:Panel>
                                                    <% } %>
                                                </div>
                                                <!-- End Paging -->
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                                            </Triggers>
                                        </asp:UpdatePanel>


                                        <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                                        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                                        <!-- Hidden Field-->
                                        <asp:HiddenField ID="HDF_Action" ClientIDMode="Static" runat="server" />

                                        <asp:HiddenField ID="HDF_Bill_Id" ClientIDMode="Static" runat="server" />
                                        <!-- END Hidden Field-->
                                    </div>
                                    <%-- END Listing --%>
                                </div>
                            </div>
                        </div>
                    </div>

                    <%-- end Add --%>
                </div>

                <!-- Danh mục sản phẩm -->
                <div class="popup-product-wp popup-product-item">
                    <div class="wp popup-product-head">
                        <strong>Danh mục sản phẩm</strong>
                    </div>
                    <div class="wp popup-product-content">
                        <div class="wp popup-product-guide">
                            <div class="left">Hướng dẫn:</div>
                            <div class="right">
                                <p>- Click vào ô check box để chọn sản phẩm</p>
                                <p>- Click vào ô số lượng để thay đổi số lượng sản phẩm</p>
                            </div>
                        </div>
                        <div class="wp listing-product item-product">
                            <table class="table" id="table-product">
                                <thead>
                                    <tr>
                                        <th>
                                            <input type="checkbox" /></th>
                                        <th>STT</th>
                                        <th>Tên sản phẩm</th>
                                        <th>Mã sản phẩm</th>
                                        <th>Đơn giá</th>
                                        <th class="th-product-quantity">Số lượng</th>
                                        <th>Giảm giá (%)</th>
                                        <th>Thành tiền</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="Rpt_Product" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td class="td-product-checkbox item-product-checkbox">
                                                    <input type="checkbox" value="<%# Eval("Id") %>"
                                                        data-code="<%# Eval("Code") %>" data-name="<%# Eval("Name") %>" />
                                                </td>
                                                <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                                <td class="td-product-name"><%# Eval("Name") %></td>
                                                <td class="td-product-code" data-id="<%# Eval("Id") %>" data-code="<%# Eval("Code") %>"><%# Eval("Code") %></td>
                                                <td class="td-product-price" data-price="  <%# Eval("Price") %>"><%# Eval("Price") %></td>
                                                <td class="td-product-quantity">
                                                    <input type="text" class="product-quantity" value="1" />
                                                </td>
                                                <td class="td-product-voucher" data-voucher="<%# Eval("VoucherPercent") %>">
                                                    <div class="row">
                                                        <input type="text" class="product-voucher" value="<%# Eval("VoucherPercent") %>" style="margin: 0 auto; float: none; text-align: center; width: 50px;" />
                                                        %
                                                    </div>
                                                    <div class="row promotion-money">
                                                        <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                                            <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                                <input type="checkbox" class="item-promotion" onclick="excPromotion($(this), 35000)" data-value="35000" data-id="1" style="margin-left: 0;" />
                                                                - 35.000 VNĐ
                                                            </label>

                                                        </div>
                                                        <br />
                                                        <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                                            <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                                <input type="checkbox" class="item-promotion" onclick="excPromotion($(this), 50000)" data-id="2" data-value="50000" style="margin-left: 0;" />
                                                                - 50.000 VNĐ
                                                            </label>

                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="map-edit">
                                                    <div class="box-money"></div>
                                                    <div class="edit-wp">
                                                        <a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),'<%# Eval("Name") %>','product')"
                                                            href="javascript://" title="Xóa"></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <div class="wp btn-wp">
                            <div class="popup-product-btn btn-complete" data-item="product">Hoàn tất</div>
                            <div class="popup-product-btn btn-esc">Thoát</div>
                        </div>
                    </div>
                </div>
                <!-- END Danh mục sản phẩm -->

                <!-- Danh mục dịch vụ -->
                <div class="popup-product-wp popup-service-item">
                    <div class="wp popup-product-head">
                        <strong>Danh mục dịch vụ</strong>
                    </div>
                    <div class="wp popup-product-content">
                        <div class="wp popup-product-guide">
                            <div class="left">Hướng dẫn:</div>
                            <div class="right">
                                <p>- Click vào ô check box để chọn dịch vụ</p>
                                <p>- Click vào ô số lượng để thay đổi số lượng dịch vụ</p>
                            </div>
                        </div>
                        <div class="wp listing-product item-product">
                            <table class="table" id="table-service">
                                <thead>
                                    <tr>
                                        <th>
                                            <input type="checkbox" /></th>
                                        <th>STT</th>
                                        <th>Tên dịch vụ</th>
                                        <th>Mã dịch vụ</th>
                                        <th>Đơn giá</th>
                                        <th class="th-product-quantity">Số lượng</th>
                                        <th>Giảm giá (%)</th>
                                        <th>Thành tiền</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="Rpt_Service" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td class="td-product-checkbox item-product-checkbox">
                                                    <input type="checkbox" value="<%# Eval("Id") %>"
                                                        data-code="<%# Eval("Code") %>" data-name="<%# Eval("Name") %>" />
                                                </td>
                                                <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                                <td class="td-product-name"><%# Eval("Name") %></td>
                                                <td class="td-product-code" data-id="<%# Eval("Id") %>" data-code="<%# Eval("Code") %>"><%# Eval("Code") %></td>
                                                <td class="td-product-price" data-price="   <%# Eval("Price") %>"><%# Eval("Price") %></td>
                                                <td class="td-product-quantity">
                                                    <input type="text" class="product-quantity" value="1" />
                                                </td>
                                                <td class="td-product-voucher" data-voucher="<%# Eval("VoucherPercent") %>">
                                                    <div class="row">
                                                        <input type="text" class="product-voucher" value="<%# Eval("VoucherPercent") %>" style="margin: 0 auto; float: none; text-align: center; width: 50px" />
                                                        %
                                                    </div>
                                                    <%--<div class="row">
                                        <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                            <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                <input type="checkbox" onclick="excPromotion($(this), -35)" data-id="<%# Eval("Id") %>" style="margin-left: 0;" /> - 35.000 VNĐ </label>

                                        </div><br />
                                        <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                            <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                <input type="checkbox" onclick="excPromotion($(this), -50)" data-id="<%# Eval("Id") %>" style="margin-left: 0;" /> - 50.000 VNĐ </label>

                                        </div>
                                    </div>--%>
                                                </td>
                                                <td class="map-edit">
                                                    <div class="box-money"></div>
                                                    <div class="edit-wp">
                                                        <a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),'<%# Eval("Name") %>','service')"
                                                            href="javascript://" title="Xóa"></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <div class="wp btn-wp">
                            <div class="popup-product-btn btn-complete" data-item="service">Hoàn tất</div>
                            <div class="popup-product-btn btn-esc">Thoát</div>
                        </div>
                    </div>
                </div>
                <!-- END Danh mục dịch vụ -->
            </div>
        </div>
        <script src="/assets/js/jquery.mCustomScrollbar.js"></script>
        <link href="/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />


        <script>
            var TYPE_DATE_FORMAT = "dd/MM/yyy";
            //btn pending and book action=0 
            //btn pending disable action=-1
            //btn booking disable action=-2
            var $action = $('#HDF_Action');
            var $customerAdd = $('.customer-add');
            var $fullName = $customerAdd.find('.full-name');
            var $phone = $customerAdd.find('.phone');
            var $btnBook = $('#BtnBook');


            var $tableBooked = $('table.table-booked');
            var $tableBooking = $('table.table-booking');

            var $btnPending = $('#BtnSend');
            var $btnFakePending = $('#BtnFakeSend');
            var $btnFakeBook = $('#BtnFakeBook');
            var $chooseItemColor = $('.wr-choose-team .item-color');
            var $teamIdBooking = $('#HDF_TeamId_Booking');
            var $teamColorSelect = $('#TeamColorSelect');


            jQuery(document).ready(function () {
                var salonId = $('#HDF_SalonId').val();

                setInterval(function () {
                    var addTime = 1;
                    checkAppointmentTime(salonId, addTime);
                }, 500);
            })

            function bindPending() {

                $.ajax({
                    type: 'POST',
                    url: '/GUI/SystemService/Webservice/BookingService.asmx/GetBillPending',
                    data: '{}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var list = JSON.parse(response.d);
                        console.log(list);
                        var trs = "";
                        $(list).each(function (i, v) {
                            //console.log(v);
                            var style = "";
                            if (v.TeamColor == null)
                                style = "style='background-color:none; border:1px solid #d0d0d0;'";
                            else
                                style = "style='background-color:" + v.TeamColor + "'";

                            trs += "<tr>" +
                                    "<td><div class='team' " + style + "></div></td>" +
                                    "<td><p class='time'>" + v.CreatedTime + "</p></td>"
                            "<td><p>Book</p></td>" +
                            "</tr>";
                        })

                        $tableBooking.find('tbody').empty().append(trs);

                    }, failure: function (response) { alert(response.d); }
                });
            }

            function setPending(This, id) {
                var salonId = $('#HDF_SalonId').val();
                $.ajax({
                    type: 'POST',
                    url: '/GUI/SystemService/Webservice/BookingService.asmx/SetBillPending',
                    data: '{id:"' + id + '",salonId:"' + salonId + '"}',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var msg = JSON.parse(response.d);
                        if (msg.success) {
                            //location.reload(true);
                            var billCode = msg.msg;
                            window.location.replace("/dich-vu/them-phieu-v2.html?msg_update_status=success&msg_update_message=Th%C3%AAm%20bill%20th%C3%A0nh%20c%C3%B4ng!&msg_print_billcode=" +
                                billCode);
                            This.css("color", "#000");

                        } else {
                            This.css("color", "#d0d0d0");
                            console.log(msg.msg);
                            showMsgSystem("Đã đầy 4 bill team màu này Pending", "warning");
                        }

                    }, failure: function (response) { alert(response.d); }
                });
            }

            function setBookingToForm(This) {
                var id = $(This).attr("id");
                var team = $(This).find('.team').attr('data-color') + "!important";
                var time = $(This).find('.time').text();
                var $caption = $('.status-team');
                //console.log(id + "|" + team + "|" + time);
                var $colorChoose = $('#TeamColorSelect');
                var $timeChoose = $('.wr-choose-team .time');

                $action.val("-1");

                $teamIdBooking.val(id);
                $colorChoose.attr("style", "background:" + team);
                $timeChoose.text(time);

                $btnFakePending.prop('disabled', true);
                $btnPending.css('opacity', '0.6');

                $btnBook.css('opacity', '1');
                $btnFakeBook.prop('disabled', false);

                $caption.text("");
            }

            $chooseItemColor.bind('click', function () {
                $btnFakePending.prop('disabled', false);
                $btnPending.css('opacity', '1');

                $btnBook.css('opacity', '0.6');
                $btnBook.prop('disabled', true);
                $btnFakeBook.prop('disabled', true);

                $action.val("-2");
            })

            jQuery(document).ready(function () {
                $action.val("0");
                // Add active menu
                $("#glbService").addClass("active");

                // Mở box listing sản phẩm
                $(".show-product").bind("click", function () {
                    $(this).parent().parent().find(".listing-product").show();
                });

                $(".show-item").bind("click", function () {
                    var item = $(this).attr("data-item");
                    var classItem = ".popup-" + item + "-item";

                    $(classItem).openEBPopup();
                    ExcCheckboxItem();
                    ExcQuantity();
                    ExcCompletePopup();

                    $('#EBPopup .listing-product').mCustomScrollbar({
                        theme: "dark-2",
                        scrollInertia: 100
                    });

                    $("#EBPopup .btn-esc").bind("click", function () { autoCloseEBPopup(0); });
                });

                // Btn Send
                $("#BtnSend").bind("click", function () {
                    // var statusTeam = $('.status-team').text();
                    if ($action.val() != "-1") {

                        if ($("#CustomerName").val() == "" && $("#InputCusNoInfor").is(":checked") == false) {
                            showMsgSystem("Bạn chưa nhập mã khách hàng hoặc chưa tích chọn \"Khách không cho thông tin\".", "warning");


                        } else if ($teamColorSelect.attr('data-color') == "") {
                            showMsgSystem("Bạn hãy chọn thẻ màu cho đội", "warning");
                        }

                        else {
                            // Check cosmetic seller
                            if (($("#HDF_ProductIds").val() != "" && $("#HDF_ProductIds").val() != "[]") && $("#Cosmetic").val() == 0) {
                                $("#ValidateCosmetic").css({ "visibility": "visible" });
                            } else {
                                $("#ValidateCosmetic").css({ "visibility": "hidden" });
                                var CustomerCode = $("#HDF_CustomerCode").val();
                                // Check duplicate bill                            
                                $.ajax({
                                    type: "POST",
                                    url: "/GUI/FrontEnd/Service/Service_Add.aspx/CheckDuplicateBill",
                                    data: '{CustomerCode : "' + CustomerCode + '"}',
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json", success: function (response) {
                                        var mission = JSON.parse(response.d);
                                        if (mission.success) {
                                            var products = "Sản phẩm : ";
                                            var services = "Dịch vụ : ";
                                            var confirm_content = "";
                                            var bill = JSON.parse(mission.msg);
                                            if (bill.ProductIds != "") {
                                                var _Product = $.parseJSON(bill.ProductIds);
                                                $.each(_Product, function (i, v) {
                                                    products += v.Name + " ( " + v.Price + " ), ";
                                                });
                                                products = products.replace(/(\,\s)+$/, '');
                                            }
                                            if (bill.ServiceIds != "") {
                                                var _Service = $.parseJSON(bill.ServiceIds);
                                                $.each(_Service, function (i, v) {
                                                    services += v.Name + " ( " + v.Price + " ), ";
                                                });
                                                services = services.replace(/(\,\s)+$/, '');
                                            }
                                            confirm_content += products + ", " + services + ", ";
                                            confirm_content = confirm_content.replace(/(\,\s)+$/, '');

                                            // show EBPopup
                                            $(".confirm-yn").openEBPopup();
                                            $("#EBPopup .confirm-yn-text").text("Khách hàng mã [" + CustomerCode + "] đã được lập hóa đơn trong hôm nay [ " + confirm_content + " ]. Bạn có chắc muốn thêm hóa đơn mới ?");

                                            $("#EBPopup .yn-yes").bind("click", function () {
                                                //Bind_UserImagesToHDF();
                                                addLoading();
                                                $("#BtnFakeSend").click();
                                            });
                                            $("#EBPopup .yn-no").bind("click", function () {
                                                autoCloseEBPopup(0);
                                            });
                                        } else {
                                            //Bind_UserImagesToHDF();
                                            addLoading();
                                            $("#BtnFakeSend").click();
                                        }
                                    },
                                    failure: function (response) { alert(response.d); }
                                });
                                //Bind_UserImagesToHDF();
                                //$("#BtnFakeSend").click();
                            }
                        }
                    }
                });



                // Btn Booking
                $("#BtnBook").bind("click", function () {

                    if ($action.val() != "-2") {

                        // console.log("here");
                        if ($("#CustomerName").val() == "" && $("#InputCusNoInfor").is(":checked") == false) {
                            showMsgSystem("Bạn chưa nhập mã khách hàng hoặc chưa tích chọn \"Khách không cho thông tin\".", "warning");
                            //} else if ($("#HDF_TotalMoney").val() == "") {
                            //    showMsgSystem("Bạn chưa chọn dịch vụ hoặc sản phẩm.", "warning");
                        } else {
                            var teamColor = $('#TeamColorSelect').attr('style');

                            if (teamColor != null && teamColor != "")
                                $("#BtnFakeBook").click();
                            else
                                showMsgSystem("Bạn không thể đặt lịch cho khách khi chưa chọn Team ", "warning");
                        }

                    }
                });


                // Bind staff suggestion
                Bind_Suggestion();
                AutoSelectStaff();

                /// Mở luôn iframe thêm mới khách hàng khi vào trang
                quickAddCustomer();
                $("#CustomerCode").focus();

                //============================
                // Show System Message
                //============================ 
                var qs = getQueryStrings();
                showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);

                // Enter don't submit
                $(window).on('keyup keypress', function (e) {
                    var keyCode = e.keyCode || e.which;
                    if (keyCode === 13) {
                        e.preventDefault();
                        return false;
                    }
                });

                /// click out of team color
                $('html').click(function () {
                    $(".team-color-edit-box").hide();
                });
                $('.pending-team-color').click(function (event) {
                    event.stopPropagation();
                });

                // Sync bill
                var requestLoadBillStatus = setInterval(loadBillStatus, 3000);

            });

            // Xử lý mã khách hàng => bind khách hàng
            function LoadCustomer(code) {
                ajaxGetCustomer(code);
            }

            // Xử lý khi chọn checkbox sản phẩm
            function ExcCheckboxItem() {
                $("#EBPopup .td-product-checkbox input[type='checkbox']").unbind("click").bind("click", function () {
                    var obj = $(this).parent().parent(),
                        boxMoney = obj.find(".box-money"),
                        price = obj.find(".td-product-price").data("price"),
                        quantity = obj.find("input.product-quantity").val(),
                        voucher = obj.find("input.product-voucher").val(),
                        checked = $(this).is(":checked"),
                        item = obj.attr("data-item"),
                        promotion = 0;
                    obj.find(".promotion-money input[type='checkbox']:checked").each(function () {
                        var value = $(this).attr("data-value").trim();
                        promotion += (value != "" ? parseInt(value) : 0);
                    });

                    // check value
                    price = price.toString().trim() != "" ? parseInt(price) : 0;
                    quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
                    voucher = voucher.trim() != "" ? parseInt(voucher) : 0;


                    if (checked) {
                        boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100 - promotion)).show();
                    } else {
                        boxMoney.text("").hide();
                    }

                });
            }

            // Số lượng | Quantity
            function ExcQuantity() {
                $("input.product-quantity").bind("change", function () {
                    var obj = $(this).parent().parent(),
                        quantity = $(this).val(),
                        price = obj.find(".td-product-price").data("price"),
                        voucher = obj.find("input.product-voucher").val(),
                        boxMoney = obj.find(".box-money"),
                        promotion = 0;

                    obj.find(".promotion-money input[type='checkbox']:checked").each(function () {
                        var value = $(this).attr("data-value").trim();
                        promotion += (value != "" ? parseInt(value) : 0);
                    });

                    // check value
                    price = price.toString().trim() != "" ? parseInt(price) : 0;
                    quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
                    voucher = voucher.trim() != "" ? parseInt(voucher) : 0;

                    boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100 - promotion)).show();
                    TotalMoney();
                    getProductIds();
                    getServiceIds();
                });
                $("input.product-voucher").bind("change", function () {
                    var obj = $(this).parent().parent().parent(),
                        quantity = obj.find("input.product-quantity").val(),
                        price = obj.find(".td-product-price").data("price"),
                        voucher = obj.find("input.product-voucher").val(),
                        boxMoney = obj.find(".box-money"),
                        promotion = 0;

                    obj.find(".promotion-money input[type='checkbox']:checked").each(function () {
                        var value = $(this).attr("data-value").trim();
                        promotion += (value != "" ? parseInt(value) : 0);
                    });

                    // check value
                    price = price.toString().trim() != "" ? parseInt(price) : 0;
                    quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
                    voucher = voucher.trim() != "" ? parseInt(voucher) : 0;

                    if (promotion == 0) {
                        boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100)).show();
                    } else {
                        obj.find("input.product-voucher").val(0);
                    }

                    TotalMoney();
                    getProductIds();
                    getServiceIds();
                });
                $("input.item-promotion").bind("click", function () {
                    var obj = $(this).parent().parent().parent().parent().parent(),
                        quantity = obj.find("input.product-quantity").val(),
                        price = obj.find(".td-product-price").data("price"),
                        voucher = obj.find("input.product-voucher").val(),
                        boxMoney = obj.find(".box-money"),
                        promotion = $(this).attr("data-value"),
                        _This = $(this),
                        isChecked = $(this).is(":checked");
                    obj.find(".promotion-money input[type='checkbox']:checked").prop("checked", false);
                    if (isChecked) {
                        $(this).prop("checked", true);
                        promotion = promotion.trim() != "" ? parseInt(promotion) : 0;
                        obj.find("input.product-voucher").val(0);
                    } else {
                        promotion = 0;
                    }

                    // check value
                    price = price.toString().trim() != "" ? parseInt(price) : 0;
                    quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
                    voucher = voucher.trim() != "" ? parseInt(voucher) : 0;
                    //promotion = promotion.trim() != "" ? parseInt(promotion) : 0;

                    if (promotion > 0) {
                        boxMoney.text(FormatPrice(quantity * price - promotion)).show();
                        obj.find("input.product-voucher").css({ "text-decoration": "line-through" });
                    } else {
                        boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100)).show();
                        obj.find("input.product-voucher").css({ "text-decoration": "none" });
                    }
                    TotalMoney();
                    getProductIds();
                    getServiceIds();
                });
            }

            // Xử lý click hoàn tất chọn item từ popup
            function ExcCompletePopup() {
                $("#EBPopup .btn-complete").unbind("click").bind("click", function () {
                    var item = $(this).attr("data-item"),
                        tableItem = $("table.table-item-" + item + " tbody"),
                        objTmp;

                    $("#EBPopup .item-product-checkbox input[type='checkbox']:checked").each(function () {
                        var Code = $(this).attr("data-code");
                        if (!ExcCheckItemIsChose(Code, tableItem)) {
                            objTmp = $(this).parent().parent().clone().find("td:first-child").remove().end();
                            tableItem.append(objTmp);
                        }
                    });

                    TotalMoney();
                    getProductIds();
                    getServiceIds();
                    ExcQuantity();
                    UpdateItemOrder(tableItem);
                    autoCloseEBPopup(0);
                });
            }

            // Check item đã được chọn
            function ExcCheckItemIsChose(Code, itemClass) {
                var result = false;
                $(itemClass).find(".td-product-code").each(function () {
                    var _Code = $(this).text().trim();
                    if (_Code == Code)
                        result = true;
                });
                return result;
            }

            // Remove item đã được chọn
            function RemoveItem(THIS, name, itemName) {
                // Confirm
                $(".confirm-yn").openEBPopup();

                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn hủy [ " + name + " ] ?");
                $("#EBPopup .yn-yes").bind("click", function () {
                    var Code = THIS.find(".td-product-code").attr("data-code");
                    $("#quick-" + itemName).find("input[data-code='" + Code + "']").removeAttr("checked");
                    THIS.remove();
                    TotalMoney();
                    getProductIds();
                    getServiceIds();
                    UpdateItemDisplay(itemName);
                    autoCloseEBPopup(0);
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
            }

            // Update table display
            function UpdateItemDisplay(itemName) {
                var dom = $(".table-item-" + itemName);
                var len = dom.find("tbody tr").length;
                if (len == 0) {
                    dom.parent().hide();
                } else {
                    UpdateItemOrder(dom.find("tbody"));
                }
            }

            // Update order item
            function UpdateItemOrder(dom) {
                var index = 1;
                dom.find("tr").each(function () {
                    $(this).find("td.td-product-index").text(index);
                    index++;
                });
            }

            function TotalMoney() {
                var Money = 0;
                $(".fe-service-table-add .box-money:visible").each(function () {
                    Money += parseInt($(this).text().replace(/\./gm, ""));
                });
                $("#TotalMoney").val(FormatPrice(Money));
                $("#HDF_TotalMoney").val(Money);
            }

            function showMsgSystem(msg, status) {
                $("#MsgSystem").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);
                setTimeout(function () {
                    $("#MsgSystem").fadeTo("slow", 0, function () {
                        $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 1);
                    });
                }, 5000);
            }

            function getProductIds() {
                var Ids = [];
                var prd = {};
                $("table.table-item-product tbody tr").each(function () {
                    prd = {};
                    var THIS = $(this);

                    var Id = THIS.find("td.td-product-code").attr("data-id"),
                        Code = THIS.find("td.td-product-code").text().trim(),
                        Name = THIS.find("td.td-product-name").text().trim(),
                        Price = THIS.find(".td-product-price").attr("data-price"),
                        Quantity = THIS.find("input.product-quantity").val(),
                        VoucherPercent = THIS.find("input.product-voucher").val(),
                        Promotion = 0;

                    THIS.find(".promotion-money input[type='checkbox']:checked").each(function () {
                        var value = $(this).attr("data-value").trim();
                        Promotion += (value != "" ? parseInt(value) : 0);
                    });

                    // check value
                    Id = Id.toString().trim() != "" ? parseInt(Id) : 0;
                    Price = Price.toString().trim() != "" ? parseInt(Price) : 0;
                    Quantity = Quantity.trim() != "" ? parseInt(Quantity) : 0;
                    VoucherPercent = VoucherPercent.trim() != "" ? parseInt(VoucherPercent) : 0;

                    prd.Id = Id;
                    prd.Code = Code;
                    prd.Name = Name;
                    prd.Price = Price;
                    prd.Quantity = Quantity;
                    prd.VoucherPercent = VoucherPercent;
                    prd.Promotion = Promotion;

                    Ids.push(prd);

                });
                Ids = JSON.stringify(Ids);
                $("#HDF_ProductIds").val(Ids);
            }

            function getServiceIds() {
                var Ids = [];
                var prd = {};
                $("table.table-item-service tbody tr").each(function () {
                    prd = {};
                    var THIS = $(this),
                        obj = THIS.parent().parent();

                    var Id = THIS.find("td.td-product-code").attr("data-id"),
                        Code = THIS.find("td.td-product-code").text().trim(),
                        Name = THIS.find("td.td-product-name").text().trim(),
                        Price = THIS.find(".td-product-price").attr("data-price"),
                        Quantity = THIS.find("input.product-quantity").val(),
                        VoucherPercent = THIS.find("input.product-voucher").val();

                    // check value
                    Id = Id.toString().trim() != "" ? parseInt(Id) : 0;
                    Price = Price.toString().trim() != "" ? parseInt(Price) : 0;
                    Quantity = Quantity.trim() != "" ? parseInt(Quantity) : 0;
                    VoucherPercent = VoucherPercent.trim() != "" ? parseInt(VoucherPercent) : 0;

                    prd.Id = Id;
                    prd.Code = Code;
                    prd.Name = Name;
                    prd.Price = Price;
                    prd.Quantity = Quantity;
                    prd.VoucherPercent = VoucherPercent;

                    Ids.push(prd);

                });
                Ids = JSON.stringify(Ids);
                $("#HDF_ServiceIds").val(Ids);
            }

            // Get Customer
            function ajaxGetCustomer(CustomerCode) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/Service/Service_Add.aspx/GetCustomer",
                    data: '{CustomerCode : "' + CustomerCode + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            var customer = JSON.parse(mission.msg);
                            $("#CustomerName").val(customer.Fullname);
                            $("#CustomerName").attr("data-code", customer.Customer_Code);
                            $("#HDF_CustomerCode").val(customer.Customer_Code);
                            $("#HDF_Suggestion_Code").val(customer.Customer_Code);
                        } else {
                            $("#CustomerName").val("");
                            $("#CustomerName").attr("data-code", "");
                            $("#HDF_CustomerCode").val("");
                            $("#HDF_Suggestion_Code").val("");
                            //var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                            //showMsgSystem(msg, "warning");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function pushQuickData(This, typeData) {
                var Code = This.attr("data-code");
                if (This.is(":checked")) {
                    var Dom = $("#table-" + typeData).find("input[data-code='" + Code + "']").parent().parent().clone().find("td:first-child").remove().end(),
                    quantity = Dom.find(".product-quantity").val(),
                    price = Dom.find(".td-product-price").data("price"),
                    voucher = Dom.find("input.product-voucher").val(),
                    promotion = 0;

                    Dom.find(".promotion-money input[type='checkbox']:checked").each(function () {
                        var value = $(this).attr("data-value").trim();
                        promotion += (value != "" ? parseInt(value) : 0);
                    });

                    // check value
                    price = price.toString().trim() != "" ? parseInt(price) : 0;
                    quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
                    voucher = voucher.trim() != "" ? parseInt(voucher) : 0;

                    Dom.find(".box-money").text(FormatPrice(quantity * price * (100 - voucher) / 100 - promotion)).show();
                    $("#table-item-" + typeData).append(Dom);
                    $(".item-" + typeData).show();
                } else {
                    $("#table-item-" + typeData).find(".td-product-code[data-code='" + Code + "']").parent().remove();
                }

                TotalMoney();
                getProductIds();
                getServiceIds();
                ExcQuantity();
                UpdateItemOrder($("#table-item-" + typeData).find("tbody"));
            }

            function pushFreeService(This, id) {
                var arr = [];
                var sv = {};
                $(".free-service input[type='checkbox']").each(function () {
                    if ($(this).is(":checked")) {
                        arr.push(parseInt($(this).attr('data-id')));
                    }
                });
                $("#HDF_FreeService").val(JSON.stringify(arr));
            }

            function excPromotion(This, money) {
                //promotion-money
                //if (This.is(":checked")) {
                //    $(".fe-service .promotion-money input[type='checkbox']").prop("checked", false);
                //} else {
                //    $(".fe-service .promotion-money input[type='checkbox']").prop("checked", false);
                //    This.prop("checked", true);
                //}

                //TotalMoney();
                //getProductIds();
                //getServiceIds();
                //ExcQuantity();
                //UpdateItemOrder($("#table-item-" + typeData).find("tbody"));
            }

            //============================
            // Suggestion Functions
            //============================
            function Bind_Suggestion() {
                $(".eb-suggestion").bind("keyup", function (e) {
                    if (e.keyCode == 40) {
                        UpDownListSuggest($(this));
                    } else {
                        Call_Suggestion($(this));
                    }
                });
                $(".eb-suggestion").bind("focus", function () {
                    Call_Suggestion($(this));
                });
                $(".eb-suggestion").bind("blur", function () {
                    //Exc_To_Reset_Suggestion($(this));
                });
                $(window).bind("click", function (e) {
                    if ((!e.target.className.match("eb-select") && e.target.parentElement.className.match("ul-listing-suggestion")) ||
                        (!e.target.className.match("eb-select") && !e.target.parentElement.className.match("ul-listing-suggestion"))) {
                        EBSelect_HideBox();
                    }
                });
            }

            function UpDownListSuggest(This) {
                var UlSgt = This.parent().find(".ul-listing-suggestion"),
                    index = 0,
                    LisLen = UlSgt.find(">li").length - 1,
                    Value;

                This.blur();
                UlSgt.find(">li.active").removeClass("active");
                UlSgt.find(">li:eq(" + index + ")").addClass("active");

                $(window).unbind("keydown").bind("keydown", function (e) {
                    if (e.keyCode == 40) {
                        if (index == LisLen) return false;
                        UlSgt.find(">li.active").removeClass("active");
                        UlSgt.find(">li:eq(" + (++index) + ")").addClass("active");
                        return false;
                    } else if (e.keyCode == 38) {
                        if (index == 0) return false;
                        UlSgt.find(">li.active").removeClass("active");
                        UlSgt.find(">li:eq(" + (--index) + ")").addClass("active");
                        return false;
                    } else if (e.keyCode == 13) {
                        // Bind data to HDF Field
                        var THIS = UlSgt.find(">li.active");
                        //var Value = THIS.text().trim();
                        var Value = THIS.attr("data-code");
                        var dataField = This.attr("data-field");

                        BindIdToHDF(THIS, Value, dataField, "#HDF_Suggestion_Code", "#HDF_Suggestion_Field", This);
                        EBSelect_HideBox();
                    }
                });
            }

            function Exc_To_Reset_Suggestion(This) {
                var value = This.val();
                if (value == "") {
                    $(".eb-suggestion").each(function () {
                        var THIS = $(this);
                        var sgValue = THIS.val();
                        if (sgValue != "") {
                            BindIdToHDF(THIS, sgValue, THIS.attr("data-field"), "#HDF_Suggestion_Code", "#HDF_Suggestion_Field", THIS);
                            return false;
                        }
                    });
                }
            }

            function Call_Suggestion(This) {
                var text = This.val(),
                    field = This.attr("data-field");
                Suggestion(This, text, field);
            }

            function Suggestion(This, text, field) {
                var This = This;
                var text = text || "";
                var field = field || "";
                var InputDomId;
                var HDF_Sgt_Code = "#HDF_Suggestion_Code";
                var HDF_Sgt_Field = "#HDF_Suggestion_Field";
                var Callback = This.attr("data-callback");

                if (text == "") return false;

                switch (field) {
                    case "customer.name": InputDomId = "#CustomerName"; break;
                    case "customer.phone": InputDomId = "#CustomerPhone"; break;
                    case "customer.code": InputDomId = "#CustomerCode"; break;
                    case "bill.code": InputDomId = "#BillCode"; break;
                }

                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Ajax/Suggestion.aspx/Suggestion_Customer",
                    data: '{field : "' + field + '", text : "' + text + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            var OBJ = JSON.parse(mission.msg);
                            if (OBJ.length > 0) {
                                var lis = "";
                                $.each(OBJ, function (i, v) {
                                    lis += "<li data-code='" + v.Customer_Code + "'" +
                                                 "onclick=\"BindIdToHDF($(this),'" + v.Customer_Code + "','" + field + "','" + HDF_Sgt_Code +
                                                 "','" + HDF_Sgt_Field + "','" + InputDomId + "')\" data-callback='" + Callback + "'>" +
                                                v.Value +
                                            "</li>";
                                });
                                This.parent().find("ul.ul-listing-suggestion").empty().append(lis).parent().show();
                                This.parent().find(".eb-select-data").show();
                            } else {
                                This.parent().find(".eb-select-data").hide().find("ul.ul-listing-suggestion").empty();
                            }
                        } else {
                            This.parent().find("ul.ul-listing-suggestion").empty();
                            var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                            showMsgSystem(msg, "warning");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function BindIdToHDF(THIS, Code, Field, HDF_Sgt_Code, HDF_Sgt_Field, Input_DomId) {
                var text = THIS.text().trim();
                $("input.eb-suggestion").val("");
                $(HDF_Sgt_Code).val(Code);
                $(HDF_Sgt_Field).val(Field);
                $(Input_DomId).val(text);
                $(Input_DomId).parent().find(".eb-select-data").hide();
                if (THIS.attr("data-callback") != "") {
                    window[THIS.attr("data-callback")].call();
                }
                // Auto post server
                $("#BtnFakeUP").click();
            }

            function EBSelect_HideBox() {
                $(".eb-select-data").hide();
                $("ul.ul-listing-suggestion li.active").removeClass("active");
            }

            function Callback_UpdateCustomerCode() {
                LoadCustomer($("#HDF_Suggestion_Code").val());
            }

            function CustomerInputOnchange(This) {
                ajaxGetCustomer(This.val());
            }

            //===================
            // Auto select staff
            //===================
            function AutoSelectStaff() {
                $(".auto-select").bind("keyup", function (e) {
                    var This = $(this);
                    var StaffType = This.attr("data-field");
                    var code = parseInt(This.val());
                    if (!isNaN(code)) {
                        $.ajax({
                            type: "POST",
                            url: "/GUI/SystemService/Ajax/Suggestion.aspx/Suggestion_OneStaff",
                            data: '{code : "' + code + '", SalonId : ' + $("#HDF_SalonId").val() + ', StaffType : "' + StaffType + '"}',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json", success: function (response) {
                                var mission = JSON.parse(response.d);
                                if (mission.success) {
                                    var OBJ = JSON.parse(mission.msg);
                                    if (OBJ.length > 0) {
                                        var lis = "";
                                        This.parent().find(".fake-value").text(This.val() + "- " + OBJ[0].Fullname);
                                        $("#" + StaffType).val(OBJ[0].Id);
                                    } else {
                                        This.parent().find(".fake-value").text("");
                                        $("#" + StaffType).val("");
                                    }
                                } else {
                                    This.parent().find("ul.ul-listing-suggestion").empty();
                                    var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                                    showMsgSystem(msg, "warning");
                                }
                            },
                            failure: function (response) { alert(response.d); }
                        });
                    } else {
                        This.parent().find(".fake-value").text("");
                        $("#" + StaffType).val("");
                    }
                });
            }

            function quickAddCustomer(This) {
                var display = $("#TrIframeAddCustomer").css("display");
                if (display == "none") {
                    $("#TrIframeAddCustomer").show();
                }
                $("#IframeAddCustomer").toggle(0, function () {
                    if (display == "table-row") {
                        $("#TrIframeAddCustomer").hide();
                    }
                });
            }

            window.callback_QuickAddCustomer = function (id, code, name) {
                $("#CustomerCode").val(code);
                $("#CustomerName").val(name);
                $("#HDF_CustomerCode").val(code);
                $("#HDF_Suggestion_Code").val(code);
            }

            window.callback_HideIframeCustomer = function () {
                //$("#IframeAddCustomer").toggle(0, function () {
                //    $("#TrIframeAddCustomer").hide();
                //});
            }

            window.callback_Iframe_SetStyle = function (style) {
                $("body,html").append($(style));
            }

            /// Chọn team
            function setTeamColor(This, idTeam, color) {
                $(".item-color.active").removeClass("active");
                This.addClass("active");
                $("#HDF_TeamId").val(idTeam);
                // bind color to .team-color-select
                $(".team-color-select").css({ "background": color });
                $('#BtnBook').prop('disabled', false);
                $('#BtnBook').css('opacity', '0.6');
                $('#TeamColorSelect').attr('data-color', idTeam);
                checkPendingMax(idTeam);

            }

            function checkPendingMax(teamId) {
                var $btnPending = $('#BtnSend');
                var $btnFakePending = $('#BtnFakeSend');
                var $caption = $('.fe-service-table-add .tr-send .status-team');
                var $time = $('.wr-choose-team .time');
                var salonId = $('#HDF_SalonId').val();
                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Webservice/BookingService.asmx/CheckPendingMax",
                    data: '{teamId : "' + teamId + '",salonId:"' + salonId + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        check = response.d;
                        //console.log("ckeck " + check);
                        if (check) {
                            console.log("bill max")
                            $btnFakePending.prop('disabled', true);

                            $btnPending.css('opacity', '0.6');
                            $caption.text("Đã đầy 4 bill team màu này Pending");
                            showMsgSystem("Đã đầy 4 bill team màu này Pending", "warning");
                            $action.val("-1");
                        } else {
                            console.log("team available")
                            $btnFakePending.prop('disabled', false);
                            $btnPending.css('opacity', '1');
                            $caption.text("");
                            $time.text("");
                            $action.val("-2");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });

            }

            /// Update team color
            function showBoxColorEdit(This) {
                $(".pending-team-color.active").removeClass("active");
                if (This.hasClass("active")) {
                    This.removeClass("active");
                } else {
                    This.addClass("active");
                }
                $(".pending-team-color").each(function () {
                    if (!$(this).hasClass("active")) {
                        $(this).parent().find(".team-color-edit-box").hide();
                    }
                });
                This.parent().find(".team-color-edit-box").toggle();
            }
            function updateTeamColor(This, teamId, color, billId) {
                This.parent().parent().find(".pending-team-color").css("cssText", "background:" + color + "!important");
                // update teamId
                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Ajax/Suggestion.aspx/Update_BillTeamId",
                    data: '{billId : ' + billId + ', teamId : ' + teamId + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            //
                            $(".team-color-edit-box").hide();
                        } else {
                            delFailed();
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            //====================
            // Sync bill status
            //====================
            var tableBooking = $("table#TableBooking");
            var tbodyBooking = $("table#TableBooking tbody");
            function loadBillStatus() {
                // 1. Lấy danh sách bill Id
                // 2. Request lấy trạng thái của các bill này
                // 3. Từ kết quả response về, bind lại trạng thái vào các bill
                var ids = [];
                var id;
                tbodyBooking.find(">tr").each(function () {
                    id = $(this).attr("id");
                    ids.push(id);
                });
                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Webservice/BookingService.asmx/getBillStatus",
                    data: '{ids : \'' + JSON.stringify(ids) + '\'}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        var statusBlock;
                        var tdStatusBlock;
                        if (mission.success) {
                            var listStatus = JSON.parse(mission.msg);
                            console.log(mission.msg);
                            $.each(listStatus, function (i, v) {
                                tdStatusBlock = tbodyBooking.find(">tr[id='" + v.Id + "'] .td-bill-status");
                                if (v.StatusValue > 0) {
                                    statusBlock = '<span class="status-value ' + v.StatusClass + '">' + v.StatusText + '</span><span class="status-time">' + v.Time + '</span>';
                                    tdStatusBlock.empty().append($(statusBlock));
                                    if (v.StatusValue >= 4) {
                                        var tr = tdStatusBlock.parent();
                                        setTimeout(function () {
                                            tr.remove();
                                        }, 1000);
                                    }
                                }
                            });
                        } else {
                            //delFailed();
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

        </script>

        <!-- In hóa đơn -->
        <iframe id="iframePrint" style="display: none;"></iframe>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                var qs = getQueryStrings();
                if (qs["msg_print_billcode"] != undefined) {
                    openPdfIframe("/Public/PDF/" + qs["msg_print_billcode"] + ".pdf");
                }
                showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);
            });
            function openPdfIframe(src) {
                var PDF = document.getElementById("iframePrint");
                PDF.src = src;
                PDF.onload = function () {
                    PDF.focus();
                    PDF.contentWindow.print();
                    PDF.contentWindow.close();
                }
            }
            //kiem tra thong bao cuoc hen 
            var salonId = $('#HDF_SalonId').val();
            setInterval(function () {
                var addTime = 15;
                checkAppointmentTime(salonId, addTime);
            }, 500);
        </script>
        <!--/ In hóa đơn -->
    </asp:Panel>
</asp:Content>
