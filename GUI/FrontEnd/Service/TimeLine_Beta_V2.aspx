﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="TimeLine_Beta_V2.aspx.cs" Inherits="_30shine.GUI.FrontEnd.UIService.TimeLine_Beta_V2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Timeline</title>
    <script src="/assets/js/jquery.mCustomScrollbar.js"></script>
    <link href="/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
    <script src="/Assets/libs/magicsuggest-master/magicsuggest.js"></script>
    <link href="/Assets/libs/magicsuggest-master/magicsuggest.css" rel="stylesheet" />
    <link href="/Assets/css/erp.30shine.com/timeline_beta.css?v=27" rel="stylesheet" />
    <link href="/Assets/css/checkin.css" rel="stylesheet" />
    <script src="/Assets/js/erp.30shine.com/timeline_beta_V2.js?v=3213231232"></script>
    <script src="/Assets/js/parse_date.js"></script>
    <style type="text/css">
        #search-booking, #support-booking {
            z-index: 3050;
        }

        .board-data .board-cell-item.is-book.is-makebill {
            background: #475866;
        }

        .board-data.board-stylist {
            z-index: 1000 !important;
        }

        .board-cell-item .stylist-not-auto {
            z-index: 99;
            top: 0;
            left: 4px;
        }

        .board-data.board-stylist .board-cell.score-3 {
            background-color: #741b47;
        }

        .board-data.board-stylist .board-cell.score-2 {
            background-color: #BECA3F;
        }

        .board-data.board-stylist .board-cell.score-1 {
            background-color: #f56c3a;
        }


        .triangle-top-left {
            display: block;
            width: 0;
            height: 0;
            border-style: solid;
            border-width: 30px 30px 0 0;
            position: absolute;
            z-index: 98;
            top: 0;
            left: 0;
        }

        .score-4 .triangle-top-left {
        }

        .score-3 .triangle-top-left {
            border-color: #741b47 transparent transparent transparent;
        }

        .score-2 .triangle-top-left {
            border-color: #BECA3F transparent transparent transparent;
        }

        .score-1 .triangle-top-left {
            border-color: #f56c3a transparent transparent transparent;
        }

        .score-none .triangle-top-left {
            display: none !important;
        }

        .board-data .board-cell-item.is-checkout i.triangle-top-left {
            display: block;
        }

        #tableNote tr, #tableNote tr td {
            border: none;
        }

            #tableNote tr td {
                padding: 10px 0px;
            }

        .popup-btn.btn-insert {
            background: #50b347;
        }

        .popup-btn.btn-close {
            background: red;
            margin-left: 10px;
        }

        .popup-btn {
            float: left;
            padding: 7px 10px;
            color: #ffffff;
            width: 80px;
            cursor: pointer;
        }

        .isSalonNote {
            width: 20px;
            height: 20px;
            position: absolute;
            left: 1px;
            bottom: 1px;
        }

        .btnSalonNote {
            border-radius: 50%;
            border: none;
            height: 18px;
            width: 18px;
            margin-left: 1px;
            /*background:red*/
        }

        .board-data .board-cell-item.is-book.is-book-at-salon.col-6 {
            background: #397ad3;
        }

        .board-data .board-cell-item.is-book.is-makebill.is-book-at-salon.col-6 {
            background: #475866;
        }

        .board-data-wrap {
            width: 85%;
            float: left;
            background: #dadada;
        }

        .board-data.board-stylist {
            width: 15%;
        }

        .showWaittingTime {
            width: 25%;
            float: left;
            padding-left: 5px;
        }

            .showWaittingTime .estimate-time-head {
                width: 100%;
                float: left;
                text-align: center;
                height: 38px;
                line-height: 38px;
                background: #475866;
                color: #ffffff;
                font-weight: normal;
                font-family: Roboto Condensed Bold;
                font-size: 16px;
            }

            .showWaittingTime .table-estimate-time-cut thead th {
                text-align: center;
                font-weight: normal;
                font-family: Roboto Condensed Bold;
                line-height: 18px !important;
                padding-top: 2px !important;
                padding-bottom: 2px !important;
            }

            .showWaittingTime .table-estimate-time-cut td:not(:first-child) {
                text-align: center;
            }

            .showWaittingTime .table-estimate-time-cut td {
                padding-top: 3px !important;
                padding-bottom: 3px !important;
            }

        .timeline-wp.etc-active .timeline-board {
        }

        .timeline-wp.etc-active .showWaittingTime {
            display: block;
        }

        .note-head .note-head-item .item-text {
            font-size: 13px;
        }

        .note-head-item {
            padding-left: 10px !important;
        }

        .showWaittingTime .note-head .note-head-item .item-text {
            line-height: 30px;
        }

        .estimate-box-issulphite td, .estimate-box-issulphite-color {
            color: red !important;
            font-family: Roboto Condensed Bold !important;
        }

        .estimate-box-wait td, .estimate-box-wait-color {
            background: #708090 !important;
        }

        .estimate-box-free td, .estimate-box-free-color {
            background: lightgreen !important;
        }

        .estimate-box-not-active td, .estimate-box-not-active-color {
            background: orangered !important;
        }

        .showWaittingTime .note-head {
            width: 100% !important;
            padding: 0px !important;
            margin: 0px !important;
        }

            .showWaittingTime .note-head .item-text {
                font-family: Roboto Condensed Bold !important;
            }

        .showWaittingTime .note-head-item {
            width: 50%;
            padding: 0px !important;
            margin: 0px !important;
        }

            .showWaittingTime .note-head-item .item-color {
                width: 20px;
                height: 20px;
                border: 1px solid #ddd;
                float: left;
                display: inline-block;
                font-family: Roboto Condensed Bold !important;
                text-align: center;
            }

        /*.a_btn_tracking {
            font-family: Roboto Condensed Bold;
            float: left;
            margin-left: 25px;
            background: #053c2c;
            color: #FFF;
            padding: 7px 10px;
            margin-top: -4px;
            font-size: 13px;
        }*/

        .a_btn_tracking:hover {
            color: #FFF;
            text-decoration: underline !important;
        }

        .a_btn_tracking:active {
            color: #FFF;
            text-decoration: underline !important;
        }

        .event {
            cursor: move;
        }

        .min, .sec {
            color: #ffffff;
        }

        .loadingTable {
            background: #fff;
            opacity: 0.5;
            top: 0;
            left: 0;
            position: fixed;
            width: 100%;
            height: 100%;
            z-index: 888888;
        }

        #count {
            padding: 3px 10px;
            margin-left: 10px;
        }

        .search-booking, .support-booking, .a_btn_tracking {
            font-family: Roboto Condensed Bold;
            float: left;
            height: 40px;
            margin-bottom: 15px;
            width: 100%;
            background: #a75712;
            color: #FFF;
            font-size: 13px;
            border: 4px double;
        }

        .button-area {
            width: 135px;
            position: fixed;
            right: 20px;
            bottom: 0px;
            z-index: 3050;
        }

            .button-area i.fa {
                color: #fff;
                margin-right: 5px;
            }

        .btn-search-booking {
            background: #306e86;
            color: white;
            padding-left: 10px;
            padding-right: 10px;
            font-size: 15px;
        }

            .btn-search-booking:hover, .btn-search-booking:active {
                background: #1b3945;
                color: white;
                padding-left: 10px;
                padding-right: 10px;
                font-size: 15px
            }

        .form-search.form-group {
            height: 40px;
            font-size: 16px;
        }

        #DatedBook {
            height: 32px !important;
        }

        .count-request {
            background: #fff;
            font-size: 12px;
            padding: 4px 8px;
            border-radius: 12px;
            font-weight: 600;
            margin-right: 5px;
        }

        .show-day h5 {
            width: 100%;
            text-align: center;
            font-size: 20px;
            padding: 17px;
            background: #fff;
            color: red;
            font-weight: 600;
            -webkit-animation: my 5000ms infinite;
            -moz-animation: my 5000ms infinite;
            -o-animation: my 5000ms infinite;
            animation: my 5000ms infinite;
        }

        @-webkit-keyframes my {
            0% {
                color: red;
            }

            50% {
                color: #fff;
            }

            100% {
                color: #c66262;
            }
        }

        @-moz-keyframes my {
            0% {
                color: red;
            }

            50% {
                color: #fff;
            }

            100% {
                color: #c66262;
            }
        }

        @-o-keyframes my {
            0% {
                color: red;
            }

            50% {
                color: #fff;
            }

            100% {
                color: #c66262;
            }
        }

        @keyframes my {
            0% {
                color: red;
            }

            50% {
                color: #fff;
            }

            100% {
                color: #c66262;
            }
        }

        .btn-open-bk {
            width: 30% !important;
            color: #ffffff !important;
            background: #4a4949;
            margin: 0px auto;
            margin-top: 7px;
        }

        .not-enroll.virtual-stylist {
            opacity: unset !important;
            background: #e5e5e5;
        }

        .birthday {
            width: 60px;
            float: left;
            display: block;
            margin-right: 15px;
        }
        #_year{
            margin-right:0px !important;
            width:75px;
        }
    </style>
    <script>    
        var isPrintBill = <%=printBill.ToString().ToLower()%>;
        var billClass = function () {
            this.billID = 0;
            this.bookingID = 0; // ID SM_BookingTemp
            this.hourID = 0;
            this.subHourID = 0;
            this.hourFrame = '';
            this.stylistID = 0;
            this.stylistName = '';
            this.customerPhone = '';
            this.customerName = '';
            this.salonID = 0;
            this.salonName = '';
            this.checkinID = 0;
            this.checkinName = '';
            this.services = [];
            this.teamID = 0;
            this.PDFBillCode = '';
            this.HCItem = '';
            this.IsAutoStylist = true;
            this.IsBookAtSalon = true;
            this.CustomerScore = 0;
            this.datedBook = '';
            this.IsSM = false;
            this.snDay = null;
            this.snMonth = null;
            this.snYear = null;
        }

        var bill = new billClass();
        bill.salonID = <%=salonID%>;

        var cancelBookingClass = function () {
            this.customerPhone = '';
            this.customerCode = '';
            this.cancelBookingID = 0;
            this.noteCancel = '';
            this.itemBookingCancel = null;
        }
        var bookingCancel = new cancelBookingClass();

        var ms;
        var listService = <%=listService%>;
        var defaultServiceID = 53;

        /* params drag and drop cell timeline */
        var customerPhone = "";
        var stylist = 0;
        var hourId = 0;
        var subHourID = 0;
        var olderHourdId = 0;
        var bookingId = 0;
        var billIdCurrent = 0;


        jQuery(document).ready(function () {

            //$("#eventBillAdd").openEBPopup();
            setTimeLine();
            var autoSetTimeLine = setInterval(setTimeLine, 10000);
            window.onload = function () {
                //setTimeLine();
                if ($('.event').hasClass('is-makebill')) {
                    $('.is-checkout').removeClass('event');
                    $('.is-checkout').attr('draggable', 'false');
                }
                if ($('.event').hasClass('is-checkout')) {
                    $(This).parent().find(".customer-text-wrap").find(".fa-warning").css("display", "none");
                }
                loadSetWaitingTime();
            }
            if ($('.event').hasClass('is-makebill')) {
                $('.is-checkout').removeClass('event');
                $('.is-checkout').attr('draggable', 'false');
            }
            if ($('.event').hasClass('is-checkout')) {
                $('.is-checkout').find(".board-cell-content").find(".customer-name").find(".customer-text-wrap").find(".fa-warning").css("display", "none");
            }
            loadSetWaitingTime();

        });
    </script>
    <%--<script src="/Assets/js/erp.30shine.com/timeline_beta_V2.js?v=23847"></script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="timeline-wp <%= Array.IndexOf(new int[] { 4,5 }, salonID) != -1 ? "etc-active" : "" %>">
            <div class="timeline-head">
                <h2 class="title-head">Timeline</h2>
                
                <div class="note-head" <%--<%= Perm_ShowSalon == true ? "" : "style='display:none;'"%>--%>>
                    <asp:DropDownList runat="server" CssClass="form-control select" ID="ddlSalon" Style="margin-top: 4px; width: 160px;" ClientIDMode="Static">
                         
                    </asp:DropDownList>
                </div>
                <div class="note-head" style="padding-left: 5px;">
                    <asp:TextBox ID="DatedBook" runat="server" ClientIDMode="Static" CssClass="form-control txtDateTime" AutoCompleteType="Disabled"></asp:TextBox>
                    
                </div>
                <div class="note-head" style="padding-left:5px">
                    <asp:Button ID="btnBindData" ClientIDMode="Static" CssClass="form-control" OnClick="updatePanelBindData" OnClientClick="ValidDateBook();FetchDataToTable();" runat="server" Text="Xem dữ liệu" Style="background:black;color:#fff"/>
                </div>
                <div class="note-head">
                    <div class="note-head-item">
                        <span class="item-color item-color-book"></span><span class="item-text">Khách book</span>
                    </div>
                    <div class="note-head-item">
                        <span class="item-color item-color-bill"></span><span class="item-text">Đã in bill</span>
                    </div>
                    <div class="note-head-item item-color-customer-wait">
                        <i class="fa fa-check is-call" aria-hidden="true"></i><span class="item-text">Khách đang chờ</span>
                    </div>
                    <div class="note-head-item">
                        <span class="item-color item-color-BookAtSalon"></span><span class="item-text">Khách book tại salon</span>
                    </div>
                    <div class="note-head-item">
                        <i class="fa fa-star" aria-hidden="true" style="color: #fddd4a; font-size: 18px;"></i><span class="item-text">&nbsp; Khách đặc biệt</span>
                    </div>
                    <div class="note-head-item">
                        <i class="fa fa-warning" aria-hidden="true" style="color: #F44336; font-size: 18px;"></i><span class="item-text">&nbsp; Khách đang chờ lâu</span>
                    </div>
                    <div class="button-area">
                        <button type="button" class="form-control search-booking" onclick="GetProvice();ShowSearchBooking()"><i class="fa fa-search"></i>Tra cứu lịch đặt</button>
                        <%--<button type="button" class="form-control support-booking" style="background: forestgreen;" onclick="ShowSupportBooking()"><span class="count-request">0</span>Hỗ trợ đặt lịch</button>--%>
                        <a target="_blank" href="/tracking/danh-sach/yeu-cau-dac-biet-cua-khach.html" style="background: #053c2c;" class="form-control a_btn_tracking">KH yêu cầu ĐB<span id="count" class="label label-primary">0</span></a>
                    </div>
                </div>
                
                <div class="search-frame hidden">
                    <div class="btn-search"></div>
                    <div class="input-frame" style="">
                        <input type="text" class="form-control" placeholder="Nhập số điện thoại khách hàng" autofocus="autofocus" onkeypress="searchByPhone(event, $(this))" />
                    </div>
                </div>
            </div>
            <div class="show-day" <%=dateBook.ToString("dd-MM-yyyy") == DateTime.Now.ToString("dd-MM-yyyy") ? "style='display:none;'" : "" %>><h5>TIMELINE ĐANG Ở NGÀY <%=dateBook.ToString("dd-MM-yyyy")%></h5></div>
            <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
            <asp:UpdatePanel runat="server" ID="UP01">
                <ContentTemplate>
                    <div class="timeline-board">
                        <div class="vertical-bar">
                            <div class="vertical-bar-relative">
                                <div class="circle"></div>
                            </div>
                        </div>
                        <div class="board-data board-stylist">
                            <div class="board-cell board-icon" id="board-hour-icon"></div>

                            <% if (dataTimeline.Count > 0)
                                { %>
                            <% int loop = 1; int teamId = 0;
                                foreach (var v in dataTimeline)
                                { %>
                            <div class="board-row <%= (loop++)%2 == 0 ? "highlight" : "" %>" <%--style="<%= (teamId != 0 && teamId != v.TeamId) ? "border-top: 1px solid #000000;" : "" %>"--%>>
                                <div class="board-cell <%=v.Score > 2 ? "score-1" : "" %>" data-scrore="<%=v.Score %>" data-id="<%=v.StylistId %>">
                                    <div class="board-cell-content"><%=v.StylistName %></div>
                                    <div style="position: absolute; z-index: 999; right: 5px; top: -8px; font-family: Roboto Condensed Bold; color: #42cabf;">
                                        <%=v.BillTotal %>
                                    </div>
                                    <div style="position: absolute; z-index: 999; left: 4px; top: -10px; font-family: Roboto Condensed Bold; color: #475866;">
                                        <%=v.TeamId %>
                                    </div>
                                </div>
                            </div>
                            <% teamId = v.TeamId;
                                } %>

                            <% } %>
                        </div>
                        <div class="board-data-wrap">
                            <div class="board-data board-book">
                                <div class="board-book-wrap" id="timeline">
                                    <div class="board-row board-hour">
                                        <% if (listHour.Count > 0)
                                            { %>
                                        <% foreach (var v in listHour)
                                            { %>
                                        <div class="board-cell"><%=v.Substring(0,5) %></div>
                                        <%} %>
                                        <% foreach (var v in listHour)
                                            { %>
                                        <div class="board-cell"></div>
                                        <%} %>
                                        <%} %>
                                    </div>

                                    <% if (dataTimeline.Count > 0)
                                        { %>
                                    <% int loop = 1; int teamId = 0;
                                        foreach (var v in dataTimeline)
                                        {   %>
                                    <% if (v.ListHour.Count > 0)
                                        { %>
                                    <div class="board-row board-book-stylist <%--<%= (loop++)%2 == 0 ? "highlight" : "" %>--%>" <%--style="<%= (teamId != 0 && teamId != v.TeamId) ? "border-top: 1px solid #000000!important;" : "" %>"--%>>
                                        <% foreach (var v2 in v.ListHour)
                                            { %>
                                        <% if (v2.HourData.Count > 0)
                                            { %>
                                        <div class="board-cell" id="<%=v.StylistId %>-<%=v2.HourId %>-<%=v2.SubHourId %>" data-hourframe="<%=v2.HourFrame %>" ondragleave="timeline_dragleave($(this), event);" ondragenter="timeline_drop($(this), event);" ondragover="timeline_drop($(this), event);" ondrop="timeline_drop($(this), event); ">
                                            <% foreach (var v3 in v2.HourData)
                                                { %>
                                            <div data-isSM="<%=v3.IsSM.ToString().ToLower() %>"" class="board-cell-item <%=v3.CustomerScore > 0 ? "score-none" : "score-1"  %> event  <%= v3.BillID != null ? "is-makebill" : "is-book" %> <%=v3.IsBookAtSalon == true ? "is-book-at-salon" : "" %> <%=v2.HourData.Count > 1 ? " col-6" : "" %> <%= v2.IsServiced ? "is-enroll" : "not-enroll" %> <%= v3.CompleteBillTime != null ? "is-checkout" : "" %>" id="<%=v.StylistId %>-<%=v3.BookingID %>-bk" data-cusscore="<%=v3.CustomerScore %>" draggable="true" ondragstart="timeline_drag($(this), event);">
                                                <div class="board-cell-content" onclick="openPrintBill($(this), <%=v3.BillID != null ? v3.BillID : 0 %>, <%=v3.BookingID %>, <%=v.StylistId %>, '<%=v.StylistName %>', '<%=v3.CustomerPhone %>', '<%=v3.CustomerName %>', <%=v2.HourId %>,'<%=v2.SubHourId %>', '<%=v2.HourFrame %>', <%=v3.IsAutoStylist ? 1 : 0 %>, <%=v3.IsBookAtSalon ? 1 : 0 %>,<%=v3.snDay == null ? 0 : v3.snDay %>,<%=v3.snMonth == null ? 0 : v3.snMonth%>,<%= v3.snYear == null ? 0 : v3.snYear %>)">
                                                    <i class="triangle-top-left"></i>
                                                    <p class="customer-name">
                                                        <span class="customer-text-wrap">
                                                            <i class="fa fa-warning hidden" aria-hidden="true" style="margin-right: 5px; color: #F44336; font-size: 14px; display: inline-block;"></i>
                                                            <%= v3.CustomerName %>
                                                            <i class="fa fa-star <%= v3.CustomerType == 0 ? "hidden" : "" %>" aria-hidden="true" style="margin-left: 5px; color: #fddd4a; font-size: 14px; display: inline-block;"></i>
                                                            <span class="show-checkout"></span>
                                                        </span>
                                                    </p>
                                                    <%if (Perm_ViewPhone)
                                                        { %>
                                                    <p class="customer-phone-replace" data-id="<%=v3.IsSM %>""><%=v3.IsSM == false ? "" : "SM - " %>
                                                        <%=v3.CustomerPhone %>
                                                    </p>
                                                    <%}  %>
                                                    <%else
                                                        { %>
                                                    <p class="customer-phone-replace">
                                                        <%=!v3.IsSM == false ? "" : "SM - " %><%=Library.Format.ReplaceFirstPhone(v3.CustomerPhone) %>
                                                    </p>
                                                    <%}  %>
                                                    <p class="customer-phone hidden">
                                                        <span data-customer-code="<%=v3.BookingID %>-<%=v3.CustomerPhone %>-hidden-<%=v3.BillID %>"></span>
                                                    </p>
                                                    <%--<p class="bill-created-time"></p>--%>
                                                    <div class="bill-created-time" style="display: none; color: #ffffff;">
                                                        <label class="min" data-min="00">00</label>:<label class="sec" data-sec="00">00</label>
                                                        <%--<%= v3.BillCreatedDate != null ? String.Format("{0:HH}",v3.BillCreatedDate) + ":" + String.Format("{0:mm}",v3.BillCreatedDate)  : ""%>--%>
                                                    </div>
                                                </div>
                                                <div class="order-checked" style="position: absolute; bottom: 0px; right: 22px; color: #ffffff; display: inline-block;"><%= v3.Order%></div>
                                                <div class="call-phone" title="Khách chờ tại salon" onclick="checkCallPhone($(this), <%=v3.BookingID %>)"
                                                    data-time="<%= v3.IsCallTimeModified != null ? String.Format("{0:dd-MM-yyyy HH:mm:ss}", v3.IsCallTimeModified) : String.Format("{0:dd-MM-yyyy HH:mm:ss}", v3.IsCallTime) %>"
                                                    data-createbill="<%= String.Format("{0:dd-MM-yyyy HH:mm:ss}", v3.BillCreatedDate) %>"
                                                    data-procedure="<%= v3.InProcedureTimeModifed != null ? String.Format("{0:dd-MM-yyyy HH:mm:ss}", v3.InProcedureTimeModifed) : String.Format("{0:dd-MM-yyyy HH:mm:ss}", v3.InProcedureTime)  %>"
                                                    data-completetime="<%= v3.CompleteBillTime != null ? String.Format("{0:dd-MM-yyyy HH:mm:ss}", v3.CompleteBillTime) : "" %>">

                                                    <i class="fa fa-check <%= v3.IsCall != null && v3.IsCall.Value ? "is-call" : "" %>" aria-hidden="true"></i>
                                                    <%--<span class="text-bill-checkout">1</span>--%>
                                                </div>

                                                <div class="isSalonNote" <% if ((v3.TextNote1 == null || v3.TextNote1 == "") && (v3.TextNote2 == null || v3.TextNote2 == "") && (v3.RatingNote == null || v3.RatingNote == ""))
                                                    { %>
                                                    style="display: none"
                                                    <% } %>>

                                                    <button type="button" class="btnSalonNote" id="btn-<%= v3.BookingID %>"
                                                        <%
                                                        if (!(v3.SalonNote == null || v3.SalonNote == ""))
                                                        {
                                                            %>
                                                        style='background: #42f44e'
                                                        <%
                                                        }
                                                        else
                                                        {
                                                            %>
                                                        style='background: red'
                                                        <%
                                                        }
                                                            %>
                                                        onclick="getNote(<%= v3.BookingID %>)">
                                                    </button>

                                                </div>

                                                <% if (v3.BillID == null)
                                                    { %>
                                                <div class="cancel-book" title="Hủy lịch đặt" onclick="openCancelBook($(this), <%=v3.BookingID %>, '<%= v3.CustomerName %>', '<%=v3.CustomerPhone %>')">
                                                    <i class="fa fa-times" aria-hidden="true"></i>
                                                </div>
                                                <% } %>
                                                <div class="board-cell-action" data-id="<%=v.StylistId %>-<%=v.StylistName %>-<%=v2.HourId %>-<%=v2.SubHourId %>-<%=v2.HourFrame %>">
                                                    <div class="col-xs-6 cell-item-action cell-item-action-printbill" <%=(!printBill ? "style='display:none !important;'" : "") %>  onclick="openPrintBill($(this), 0, <%=v3.BookingID %>, <%=v.StylistId %>, '<%=v.StylistName %>', '<%=v3.CustomerPhone %>', '<%=v3.CustomerName %>', <%=v2.HourId %>,'<%=v2.SubHourId %>' ,'<%=v2.HourFrame %>', <%=v3.IsAutoStylist ? 1 : 0 %>, <%=v3.IsBookAtSalon ? 1 : 0 %>)">In phiếu</div>
                                                    <div class="col-xs-6 cell-item-action cell-item-action-book" onclick="openBooking($(this), 0, <%=v.StylistId %>, '<%=v.StylistName %>', '', '', <%=v2.HourId %>, '<%=v2.SubHourId %>', '<%=v2.HourFrame %>')">Book lịch</div>
                                                </div>
                                                <div id="stylist-not-auto-<%=v3.BookingID %>" class="stylist-not-auto <%= !(v3.IsAutoStylist) ? "active" : "" %>">B</div>
                                                <div class="customer-hc <%= (v3.IsHCItem) ? "active" : "" %>">H</div>
                                            </div>
                                            <%} %>
                                        </div>
                                        <%--<div class="board-cell"><div class="board-cell-content">asdf</div></div>--%>
                                        <%}
                                            else
                                            { %>
                                        <div class="board-cell " id="<%=v.StylistId %>-<%=v2.HourId %>-<%=v2.SubHourId %>" data-hourframe="<%=v2.HourFrame %>" ondragleave="timeline_dragleave($(this), event);" ondragenter="timeline_drop($(this), event);" ondragover="timeline_drop($(this), event);" ondrop="timeline_drop($(this), event);">
                                          
                                               <div class="board-cell-item <%=v2.IsServiced? "is-enroll" : "not-enroll" %> <%=v.isAccountLogin == 1 ? "virtual-stylist" : "" %>" data-virtual ="<%=v.isAccountLogin %>"  data-stylist="<%=v.StylistId %>" data-hourid="<%=v2.HourId %>">
                                                    <%--<%=v.isAccountLogin == 1 && v2.isVirtual && !v2.IsServiced ? "<button type='button' class='form-control btn-open-bk' onclick='Open_Booking($(this))'>Open</button>" : "" %>--%>
                                                    <div class="board-cell-content"></div>
                                                    <div class="board-cell-action" data-id="<%=v.StylistId %>-<%=v.StylistName %>-<%=v2.HourId %>-<%=v2.SubHourId %>-<%=v2.HourFrame %>">
                                                        <div class="col-xs-6 cell-item-action cell-item-action-printbill" <%=(!printBill ? "style='display:none !important;'" : "") %> onclick="openPrintBill($(this),0, 0, <%=v.StylistId %>, '<%=v.StylistName %>', '', '', <%=v2.HourId %>,'<%=v2.SubHourId %>', '<%=v2.HourFrame %>', 1, 0)">In phiếu</div>
                                                        <div class="col-xs-6 cell-item-action cell-item-action-book" onclick="openBooking($(this), 0, <%=v.StylistId %>, '<%=v.StylistName %>', '', '', <%=v2.HourId %> ,'<%=v2.SubHourId %>' , '<%=v2.HourFrame %>')">Book lịch</div>
                                                    </div>
                                                </div>
                                        </div>
                                        <% } %>
                                        <%} %>
                                    </div>
                                    <%teamId = v.TeamId;
                                        } %>
                                    <%} %>
                                    <% } %>
                                </div>
                            </div>
                        </div>
                        <div class="showWaittingTime" style="display: none;">
                            <div class="loadingTable" style="display: none">Vui lòng đợi trong giây lát...</div>
                            <div class="row">
                                <div class="note-head">
                                    <%-- <div class="note-head-item">
                                    <span class="item-color estimate-box-wait-color" title="Stylist có khách chờ bằng 0"></span>
                                    <span class="item-text">Khách chờ 0</span>
                                </div>--%>
                                    <div class="note-head-item">
                                        <span class="item-color estimate-box-issulphite-color" title="Stylist,Skinner có khách đang xử lý hóa chất">C</span>
                                        <span class="item-text">KH sử dụng hóa chất</span>
                                    </div>
                                    <div class="note-head-item">
                                        <span class="item-color estimate-box-free-color" title="Stylist,Skinner đang không có khách"></span>
                                        <span class="item-text">Stylist/Skinner Rảnh</span>
                                    </div>
                                    <%-- <div class="note-head-item">
                                    <span class="item-color estimate-box-not-active-color" title="Stylist không áng time cắt"></span>
                                    <span class="item-text">Stylist không áng</span>
                                </div>--%>
                                </div>
                                <strong class="estimate-time-head">ƯỚC LƯỢNG THỜI GIAN GỘI
                                </strong>
                                <table class="table table-listing-product table-bordered table-estimate-time-cut" id='tableEstimateTimeMassage'>
                                    <thead>
                                        <tr>
                                            <%--<th>Khách hàng</th>--%>
                                            <th style="line-height: 50px !important">Skinner</th>
                                            <th style="width: 60px;">TG gội còn lại (phút)</th>
                                            <th style="width: 60px;">SL Đang gội</th>
                                            <th style="width: 60px;">SL Chưa gội</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <%--<div class="row">
                                <strong class="estimate-time-head">ƯỚC LƯỢNG THỜI GIAN CẮT & GỘI
                                </strong>
                                <table class="table table-listing-product table-bordered table-estimate-time-cut" id='tableEstimate'>
                                    <thead>
                                    <tr>                                       
                                        <th style="line-height: 50px !important">Stylist</th>
                                        <th style="width: 60px;">TG còn lại (phút)</th>
                                        <th style="width: 60px;">SL đang cắt</th>
                                        <th style="width: 60px;">SL chưa cắt</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>--%>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlSalon" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>
        </div>

        <!-- Popup thêm mới team -->
        <div class="event-bill-add" id="eventBillAdd">
            <%--<strong class="bill-add-head">Lập bill checkin</strong>--%>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 popup-head">
                        <p class="popup-hour-frame">08:00</p>
                        <p class="popup-stylist"><i class="fa fa-star-o star-not-auto-stylist" aria-hidden="true"></i>Stylist <span style="position: relative; top: -2px;">&nbsp;|&nbsp;</span> <span class="sp-stylist-name"></span></p>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group form-group-lg">
                            <div class="col-xs-12">
                                <label class="label-customer-name"><span>Khách hàng</span> <span class="customer-book-at-salon">( <span style="color: red; float: none;">*</span> Book tại salon )</span></label>
                                <input id="customerPhone" class="form-control customer-phone" type="text" placeholder="Số điện thoại" onchange="callCustomerData($(this))" onkeypress="return isNumber(event)" />
                                <input id="customerName" class="form-control customer-name" type="text" placeholder="Tên Khách hàng" style="margin-top: 10px;" />
                                <button id="addName" style="top: 10px; float: right; margin-top: -34px; height: 34px; background-color: #565656; border-color: #565656;" onclick="getCustomerName()" class="btn btn-success">Lưu</button>
                                <%--<asp:TextBox CssClass="form-control txtDateTime" ID="BirthDay" ClientIDMode="Static" runat="server" placeholder="NGÀY SINH" AutoCompleteType="Disabled"></asp:TextBox>--%>
                                
                  
                            </div>
                            <div class="col-xs-12">
                                <label class="label-customer-name" style="margin-top:10px;"><span>Ngày sinh</span></label>
                            </div>
                            <div class="col-xs-12">
                                
                                <input id="sn-day" class ="form-control birthday sn-day" type="text" placeholder="NGÀY" autocomplete="off" onkeypress="return isNumber(event)"/>
                                <input id="sn-month" class ="form-control birthday sn-month" type="text" placeholder="THÁNG" autocomplete="off" onkeypress="return isNumber(event)"/>
                                <input id="sn-year" class ="form-control birthday sn-year" type="text" placeholder="NĂM" autocomplete="off" onkeypress="return isNumber(event)"/>
                            </div>
                            <%--<div class="col-xs-12">
                                <div class="checkbox" style="float: left; margin-top: 4px; margin-bottom: 0px">
                                <label>
                                        <input type="checkbox" id="check-birthday" onclick="setBookSalon($(this))" />
                                        Khách không cho ngày sinh
                                    </label>
                                </div>
                            </div>--%>
                        </div>
                   <%--     <div class="form-group form-group-lg" <%= !Perm_ViewAllData ? "style='display:none;'" : "" %>>
                            <div class="col-xs-12">
                                <label>Salon</label>
                                <select class="form-control ddl-salon" onchange="setSalon($(this))">
                                    <option value="1">Chọn salon</option>
                                    <% if (listSalon.Count > 0)
                                        { %>
                                    <% foreach (var v in listSalon)
                                        { %>
                                    <option value="<%=v.Id %>"><%=v.Name %></option>
                                    <%} %>
                                    <%} %>
                                </select>
                            </div>
                        </div>--%>
                        <div class="form-group form-group-lg service-wrap">
                            <div class="col-xs-12 suggestion-wrap">
                                <label>Dịch vụ</label>
                                <input class="form-control ms-suggest" type="text" placeholder="Search theo tên dịch vụ" />
                            </div>
                        </div>
                        <div class="form-group form-group-lg checkin-wrap">
                            <div class="col-xs-12 suggestion-wrap">
                                <label>Nhân viên checkin</label>
                                <div class="row">
                                    <input class="form-control checkin-code" type="text" placeholder="STT" style="width: 25%; float: left;" onkeypress="return isNumber(event)" onkeyup="setCheckin($(this))" />
                                    <input class="form-control checkin-name" type="text" placeholder="Nhân viên checkin" disabled="disabled" style="width: 75%; float: left;" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <%-- <% if (listTeam.Count > 0)
                            { %>
                        <div class="form-group form-group-lg team-wrap" style="display: none;">
                            <div class="col-xs-12 suggestion-wrap" style="padding-bottom: 5px;">
                                <label>Chọn team</label>
                            </div>
                            <div class="col-xs-12 list-team">
                                <% foreach (var v in listTeam)
                                    { %>
                                <div class="btn btn-success btn-team" style="background: <%=v.Color%>" onclick="setTeam($(this),<%=v.Id %>)">
                                    <i class="fa fa-check-square" aria-hidden="true"></i>
                                </div>
                                <%} %>
                            </div>
                        </div>--%>
                        <div class="form-group form-group-lg hidden div_checkCustomer" style="margin-top: 10px; border-radius: 10px; background: #eeeeee; padding: 10px;">
                            <p class="customerType" style="text-transform: uppercase; font-family: 'Roboto Condensed Bold'; font-size: 16px; text-align: center;"></p>
                            <p class="reason" style="font-family: 'Roboto Condensed Bold'; font-size: 14px;"></p>
                        </div>
                        <div class="row list-item-hc">
                            <input type="hidden" id="HDFBillID" />
                            <div class="col-xs-9">
                                <label>
                                    <input type="checkbox" class="is-book-stylist" style="float: left; margin-top: 4px; margin-right: 10px;" id="setStylist" onclick="printBillSetAutoStylist($(this))" />Khách book Stylist</label>
                            </div>
                        </div>
                        <div class="row list-item-hc">
                            <div class="col-xs-3">
                                <div class="checkbox" style="float: left;">
                                    <label>
                                        <input type="checkbox" data-value="1" onclick="setHoaChat($(this), 1)" />
                                        Tẩy
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="checkbox" style="float: left;">
                                    <label>
                                        <input type="checkbox" data-value="2" onclick="setHoaChat($(this), 2)" />
                                        Uốn
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="checkbox" style="float: left;">
                                    <label>
                                        <input type="checkbox" data-value="3" onclick="setHoaChat($(this), 3)" />
                                        Nhuộm
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row list-item-book-stylist">
                            <div class="col-xs-12">
                                <div class="checkbox" style="float: left; margin-top: 0px; margin-bottom: 2px;">
                                    <label>
                                        <input type="checkbox" onclick="setBookStylist($(this))" />
                                        Khách book Stylist
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="checkbox" style="float: left; margin-top: 0px; margin-bottom: 2px; display: none;">
                                    <label>
                                        <input type="checkbox" checked="checked" onclick="setBookSalon($(this))" />
                                        Khách book tại salon
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-group-lg">
                            <div class="col-xs-12">
                                <div class="btn-print-bill btn-action-print" <%=!printBill ? "style='display:none !important;'" : "" %> onclick="printBill()">In phiếu</div>
                                <div class="btn-print-bill btn-action-print-update" onclick="updateBill()">Cập nhật phiếu</div>
                                <div class="btn-print-bill btn-action-book" onclick="insertBooking()">Đặt chỗ</div>
                                 
                            </div>
                            
                        </div>
                    </div>
                    <%--<% } %>--%>
                </div>
            </div>
        </div>
        <!--/ Popup thêm mới team -->
        <!-- Popup hiển thị ghi chú -->
        <div class="event-bill-add popupNote">

            <div class="container">
                <div class="row">
                    <div class="col-xs-12 popup-head">
                        <p class="popup-hour-frame" style="font-size: 16px; font-weight: 800">Ghi chú của KH và Salon</p>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group form-group-lg">
                            <div class="col-xs-12">
                                <input type="hidden" id="HDBookingID" />
                                <input type="hidden" id="HDSalonNote" />
                                <table class="table table-condensed" id="tableNote">
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="button-action-wp">
                            <div class="button-action">
                                <div class="showResult" style="display: none"></div>
                                <div class="popup-btn btn-insert" onclick="InsertNote()">Xác nhận</div>
                                <div class="popup-btn btn-close" onclick="autoCloseEBPopup()">Đóng</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Popup hiển thị ghi chú -->
        <!-- Popup hủy lịch đặt -->
        <div class="popup-cancel-book">
            <div class="popup-cancel-book-content">
                <p class="popup-title">Bạn có chắc chắn muốn hủy lịch đặt? Vui lòng nhập lý do:</p>
                <div class="note-wrap">
                    <textarea class="note-cancel" placeholder="Nhập lý do hủy lịch đặt" rows="3"></textarea>
                </div>
                <div class="button-action-wp">
                    <div class="button-action">
                        <div class="popup-btn btn-ok" onclick="cancelBook()">Xác nhận</div>
                        <div class="popup-btn btn-close" onclick="autoCloseEBPopup()">Đóng</div>
                    </div>
                </div>
            </div>
        </div>
        <!--// Popup hủy lịch đặt -->
        <%-- popup tìm lịch đặt --%>
        <!-- Modal -->
        <div id="search-booking" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title" style="font-size: 25px; font-weight: bold; text-align: center">Tra cứu lịch đặt</h5>
                    </div>
                    <div class="modal-body">

                        <div class="row form-search">
                            <div class="form-group" style="width: 100%">
                                <label for="select">Tỉnh thành</label>
                                <select class="form-control sl-province select" style="width: 100%">
                                    <option value='0' selected='selected'>Chọn tỉnh thành</option>
                                </select>

                            </div>
                            <div class="form-group" style="width: 100%">
                                <label for="search-phone">Số điện thoại</label>
                                <input type="text" class="form-control search-phone" placeholder="Nhập số điện thoại tìm kiếm (ít nhất 4 kí tự)" onkeypress="return isNumber(event)" style="width: 100%" />
                            </div>
                            <button type="button" class="btn btn-default btn-search-booking" onclick="GetSearchInfo()">Tìm kiếm</button>



                        </div>
                        <div class="row search-info" style="margin-top: 20px; border-top: 1px solid #ccc; padding-top: 20px">
                            <table class="table table-booking-list table-info-cus">
                                <thead>
                                    <tr>
                                        <th style="width: 15%">Salon</th>
                                        <th style="width: 30%">Tên KH</th>
                                        <th style="width: 15%">Số ĐT</th>
                                        <th style="width: 15%">Ngày Book</th>
                                        <th style="width: 15%">Giờ Book</th>
                                        <th style="width: 10%">Hủy?</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <%--// popup tìm lịch đặt --%>

        <%-- popup hỗ trợ đặt lịch --%>
        <div id="support-booking" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h5 class="modal-title" style="font-size: 25px; font-weight: bold; text-align: center">Hỗ trợ đặt lịch</h5>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="hd-salon-id" value="<%=salonID %>" />
                        <div class="row support-info" style="margin-top: 20px; padding-top: 20px">
                            <table class="table table-booking-list table-support-info">
                                <thead>
                                    <tr>
                                        <th>Tên KH</th>
                                        <th>SĐT</th>
                                        <th>Thời gian YC</th>
                                        <th>Ghi chú KH</th>
                                        <th>Trạng thái Booking</th>
                                        <th>Giờ book - Ngày book</th>
                                        <th>Ghi chú sau gọi</th>
                                        <th>Trạng thái xử lý</th>

                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <%--// popup hỗ trợ đặt lịch --%>
        <!-- Page loading -->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
        <!--/ Page loading -->
        <div class="html_current hidden"></div>
        <!-- Iframe phục vụ in hóa đơn -->
        <iframe id="iframePrint" style="display:none;"></iframe>
        <link href="/Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="/Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .table-support-info th, .table-support-info td {
                text-align: center !important;
            }

            .note-head.select2-container {
                width: 150px !important;
                z-index: 9999;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
                z-index: 9999;
            }

            .table-info-cus {
                width: 100%;
                background-color: #f3f3f3;
            }

                .table-info-cus tbody {
                    height: 300px;
                    overflow-y: auto;
                    width: 100%;
                }

                .table-info-cus thead, .table-info-cus tbody, .table-info-cus tr, .table-info-cus td, .table-info-cus th {
                    display: block;
                }

                    .table-info-cus tbody td {
                        float: left;
                        /*width:20%;*/
                    }

                    .table-info-cus thead tr th {
                        float: left;
                        background-color: #f39c12;
                        border-color: #e67e22;
                        /*width:20%;*/
                    }

            #support-booking .modal-lg {
                width: 80% !important;
            }

            .select2-container--open {
                z-index: 4000;
            }
        </style>
        <script type="text/javascript">
            //let api = "<%=api%>";
            let time_keeping_api = "<%=Libraries.AppConstants.URL_API_TIMEKEEPING%>";
            // search booking function
            function ShowSearchBooking() {

                //ClearContentModal();
                $("#search-booking").modal();

            }
            function ClearContentModal() {
                $(".table-info-cus tbody").empty();
                $(".search-phone").val('');
                $(".sl-province").val(0);
                $(".sl-province").trigger('change');
            }

            // support booking function
            function ShowSupportBooking() {
                //ClearContentModal();
                $("#support-booking").modal();
                //GetCustomerRequest();
            }

            var permViewPhone = 0;
            jQuery(document).ready(function () {
                permViewPhone = <%=Perm_ViewPhone == true ? 1 : 0%>;
                var qs = getQueryStrings();
                if (qs["msg_print_billcode"] != undefined) {
                    openPdfIframe("/Public/PDF/" + qs["msg_print_billcode"] + ".pdf");
                }
                showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);
            });
            function openPdfIframe(src) {
                var PDF = document.getElementById("iframePrint");
                PDF.src = src;
                PDF.onload = function () {
                    PDF.focus();
                    PDF.contentWindow.print();
                    PDF.contentWindow.close();
                    window.history.pushState({}, 'timeline', '/ver2-beta-timeline');
                }
            }

            function GetProvice() {
                $.ajax({
                    url: "/GUI/FrontEnd/Service/TimeLine_Beta_v2.aspx/GetProvince",
                    type: "POST",
                    contentType: "application/json;charset:utf-8",
                    dataType: "json",
                    success: function (response) {
                        var data = JSON.parse(response.d);
                        $(".sl-province").empty();
                        var option = "<option value='0' selected='selected'>Chọn tỉnh thành</option>";
                        for (var i = 0; i < data.length; i++) {
                            option += "<option value='" + data[i].ID + "'>" + data[i].TenTinhThanh + "</option>";
                        }
                        $(".sl-province").append(option);
                    }
                })
            }

            function GetSearchInfo() {
                var provinceId = $(".sl-province").val();
                //var date = $(".sl-time-search").val();
                var phone = $(".search-phone").val();

                if (phone == null || phone == '' || phone.length < 4) {
                    ShowMessage("Thông báo", "Vui lòng nhập ít nhất 4 kí tự", 4);
                    $(".search-phone").focus();
                    $(".search-phone").css("border-color", "pink");
                    return;
                }
                if (provinceId == 0) {
                    ShowMessage("Thông báo", "Vui lòng chọn tỉnh thành tìm kiếm", 4);
                    return;
                }

                $.ajax({
                    url: "/GUI/FrontEnd/Service/TimeLine_Beta_v2.aspx/GetCustomer",
                    type: "POST",
                    data: "{provinceId:" + provinceId + ",customerPhone:'" + phone + "'}",
                    contentType: "application/json;charset:utf-8",
                    dataType: "json",
                    success: function (response) {
                        var data = JSON.parse(response.d);
                        var tr = "";
                        $(".table-info-cus tbody").empty();
                        //$(".table-info-cus").DataTable().clear();
                        if (data == null || data.length == 0) {
                            ShowMessage("Thông báo", "Không có dữ liệu", 4);
                            return;
                        }
                        //console.log(data);



                        for (var i = 0; i < data.length; i++) {
                            tr += "<tr>";
                            tr += "<td style='width:15%'>" + data[i].SalonName + "</td>";
                            tr += "<td style='width:30%'>" + data[i].CustomerName + "</td>";
                            tr += "<td style='width:15%'>" + data[i].CustomerPhone + "</td>";
                            tr += "<td style='width:15%'>" + ToJavaScriptDate(data[i].DateBook) + "</td>";
                            tr += "<td style='width:15%'>" + data[i].BookHour + "</td>";
                            tr += "<td style='width:10%'>" + data[i].IsDelete + "</td>";
                            tr += "</tr>";
                        }
                        $(".table-info-cus tbody").append(tr);


                    }
                })
            }
        </script>
        <!--/ Iframe phục vụ in hóa đơn -->

        <script>
            //Lấy ghi chú của khách hàng và salon theo BookingID
            function getNote(BookingId) {
                $("#HDBookingID").val(BookingId);
                $.ajax({
                    url: "/GUI/FrontEnd/Service/TimeLine_Beta_V2.aspx/GetNote",
                    data: "{BookingId:" + BookingId + "}",
                    type: "POST",
                    contentType: "application/json;charset:UTF-8",
                    datatype: "JSON",
                    success: function (data) {
                        var jsData = JSON.parse(data.d);
                        console.log(jsData);
                        var row = "<tr>";
                        row += "<td style='width:20%'>Số điện thoại KH:</td><td>" + jsData.CustomerPhone + "</td>";
                        row += "</tr>";
                        if (jsData.RatingNote != "") {
                            row += "<tr>";
                            row += "<td style='width:20%'>Phản hồi từ App: </td><td>" + jsData.RatingNote + "</td>";
                            row += "</tr>";
                        }
                        if (jsData.TextNote1 != null && jsData.TextNote2 != null) {
                            row += "<tr>";
                            row += "<td style='width:20%'>Ghi chú của KH: </td><td>" + jsData.TextNote1 + ". " + jsData.TextNote2 + "</td>";
                            row += "</tr>";
                        } else if (jsData.TextNote1 != null && jsData.TextNote2 == null) {
                            row += "<tr>";
                            row += "<td style='width:20%'>Ghi chú của KH: </td><td>" + jsData.TextNote1 + "</td>";
                            row += "</tr>";
                        } else if (jsData.TextNote1 == null && jsData.TextNote2 != null) {
                            row += "<tr>";
                            row += "<td style='width:20%'>Ghi chú của KH: </td><td>" + jsData.TextNote2 + "</td>";
                            row += "</tr>";
                        } else {
                        }
                        if (jsData.SalonNote != null) {
                            $(".btn-insert").hide();
                            row += "<tr>";
                            row += "<td style='width:20%'>Ghi chú của Salon: </td><td>" + jsData.SalonNote + "</td>";
                            row += "</tr>";
                        } else {
                            $(".btn-insert").show();
                            row += "<tr>";
                            row += "<td style='line-height:50px;width:20%'>Ghi chú của Salon: </td><td><textarea id='txtSalonNote' row='3' onchange='getSalonNote($(this))' class='form-control' placeholder='Nhập ghi chú của Salon'></textarea></td>";
                            row += "</tr>";
                        }
                        $("#tableNote").empty().append(row);
                    },
                    complete: function () {
                        $(".popupNote").openEBPopup();
                    }
                });
            }
            function getSalonNote(This) {
                $("#HDSalonNote").val(This.val());
            }
            //Lưu ghi chú vào DB
            function InsertNote() {
                var bk = $("#HDBookingID").val();
                var salonNote = $("#HDSalonNote").val();
                //console.log();
                if (salonNote == "" || salonNote == null) {
                    $(".showResult").show();
                    $(".showResult").empty().append("<span style='color:red'>SalonNote không được để trống</span>");
                } else {
                    $(".showResult").hide();
                    $.ajax({
                        url: "/GUI/FrontEnd/Service/TimeLine_Beta_V2.aspx/InsertNote",
                        data: "{BookingId:" + bk + ",SalonNote:'" + salonNote + "'}",
                        type: "POST",
                        dataType: "JSON",
                        contentType: "application/json;charset:UTF-8",
                        success: function (data) {
                            $(".showResult").empty().append(data.d);
                            if (data.d == "<span style='color:green'>Thêm ghi chú thành công</span>") {
                                autoCloseEBPopup();
                                $("#btn-" + bk + "").css("background", "#42f44e");
                            }
                        }
                    });
                }
            }

            //lấy dữ liệu áng thời gian cắt
            //function GetDataBySalon(SalonId) {
            //    $.ajax({
            //        url: "/GUI/FrontEnd/Service/TimeLine_Beta_V2.aspx/EstimateTime",
            //        data: "{c:" + SalonId + "}",
            //        type: "POST",
            //        dataType: "JSON",
            //        contentType: "application/json;charset:UTF-8",
            //        success: function (data) {
            //            var jsData = JSON.parse(data.d);
            //            var content = "";
            //            if (jsData.length > 0) {
            //                for (var i = 0; i < jsData.length; i++) {
            //                    content += "<tr class='"+
            //                        (jsData[i].IsSulphite == true ? 'estimate-box-issulphite' : '') +
            //                        //((jsData[i].Khach_Cho == '0' && jsData[i].Avg_TimeCut != '0') ? ' estimate-box-wait' : '') +
            //                        ((jsData[i].EstimateTime == '0' && jsData[i].Avg_TimeCut == '0') ? 'estimate-box-free' : '') +
            //                        //((jsData[i].EstimateTime == '0' && jsData[i].Avg_TimeCut != '0') ? 'estimate-box-not-active' : '') +
            //                        "'>";
            //                    //content += "<td>" + jsData[i].CustomerName + "</td>";
            //                    content += "<td>" + jsData[i].StylistName + "</td>";
            //                    content += "<td>" + jsData[i].TimeEstimate + "</td>";
            //                    content += "<td>" + jsData[i].Khach_Dang_Cat + "</td>";
            //                    content += "<td>" + jsData[i].Khach_Cho + "</td>";     
            //                    content += "</tr>";
            //                }
            //                $("#tableEstimate").find("tbody").empty().append(content);
            //            }
            //            else {
            //                content += "<tr colspan='4'>Không có dữ liệu</tr>";
            //            }
            //        }
            //    })
            //}
            //lấy dữ liệu áng thời gian gội
            function EstimateTimeMassage(SalonId) {
                $.ajax({
                    url: "/GUI/FrontEnd/Service/TimeLine_Beta_V2.aspx/EstimateTimeMassage",
                    data: "{c:" + SalonId + "}",
                    type: "POST",
                    dataType: "JSON",
                    contentType: "application/json;charset:UTF-8",
                    success: function (data) {
                        var jsData = JSON.parse(data.d);
                        var content = "";
                        if (jsData.length > 0) {
                            for (var i = 0; i < jsData.length; i++) {
                                content += "<tr class='" +
                                    (jsData[i].hoachat == 1 ? 'estimate-box-issulphite' : '') +
                                    //((jsData[i].Khach_Cho == '0' && jsData[i].Avg_TimeCut != '0') ? ' estimate-box-wait' : '') +
                                    ((jsData[i].danggoiTg == '0' && jsData[i].danggoiSl == '0' && jsData[i].chuagoiSl == '0') ? 'estimate-box-free' : '') +
                                    //((jsData[i].EstimateTime == '0' && jsData[i].Avg_TimeCut != '0') ? 'estimate-box-not-active' : '') +
                                    "'>";
                                //content += "<td>" + jsData[i].CustomerName + "</td>";
                                content += "<td>" + jsData[i].skinnerName + "</td>";
                                content += "<td>" + jsData[i].danggoiTg + "</td>";
                                content += "<td>" + jsData[i].danggoiSl + "</td>";
                                content += "<td>" + jsData[i].chuagoiSl + "</td>";
                                content += "</tr>";
                            }
                            $("#tableEstimateTimeMassage").find("tbody").empty().append(content);
                        }
                        else {
                            content += "<tr colspan='4'>Không có dữ liệu</tr>";
                        }
                    }
                })
            }
            //show dữ liệu ra table
            function FetchDataToTable() {
                var SalonID = $("#ddlSalon").val();
                var sID;
                if (SalonID != 0) {
                    sID = SalonID;
                    //CountingRequestBooking();
                    //window.setInterval(function () {
                    //    console.log("here 1 " + SalonID);
                    //    //GetDataBySalon(sID);
                    //    //EstimateTimeMassage(sID);
                    //},
                    //    60000);
                }
                else {
                    sID = $("#ddlSalon").val();
                    //window.setInterval(function () {
                    //    console.log("here 2 " + SalonID);
                    //    //GetDataBySalon(sID);
                    //    //EstimateTimeMassage(sID);
                    //},
                    //    60000);
                }
            }

            //request mỗi 60s yêu cầu đặc biệt của KH
            function GetCountSpecialRequire() {
                //window.setInterval(function () {
                //    var salonId = $("#ddlSalon").val();
                //    if (salonId > 0) {
                //        CountSpecialRequire(salonId);
                //    }
                //},
                //    60000);
            }
            //đếm số yêu cầu đặc biệt của KH
            function CountSpecialRequire(SalonID) {
                $.ajax({
                    url: "/GUI/FrontEnd/Service/TimeLine_Beta_V2.aspx/CountSpecialRequire",
                    data: "{salonId:" + SalonID + "}",
                    type: "POST",
                    dataType: "JSON",
                    contentType: "application/json;charset:UTF-8",
                    success: function (data) {
                        if (JSON.parse(data.d) != 0) {
                            $("#count").css("background", "red");
                            $("#count").empty().append(JSON.parse(data.d));
                        } else {
                            $("#count").empty().text("0");
                        }
                    },
                });
            }

            //load dữ liệu sau khi pageload
            $(document).ready(function () {
                var today = moment().format();
                console.log(today);

                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    defaultDate:new Date(),
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    minDate: '-1970/01/2',
                    //maxDate:'+1970/01/2',
                    onChangeDateTime: function (dp, $input) { }
                });
                // Mặc định load dữ liệu estimate time cut khi vào page
                var SalonID = $("#ddlSalon").val();
                //GetDataBySalon(SalonID);
                //EstimateTimeMassage(SalonID);
                // Load dữ liệu khách có yêu cầu đặc biệt
                //CountSpecialRequire(SalonID);
                // Auto load data estimate-time-cut
                FetchDataToTable();
                //GetCountSpecialRequire();
                //============================
                // Datepicker
                //============================

            });
            function ValidDateBook() {
                if ($("#DatedBook").val() == "" || $("#DatedBook").val() == null) {
                    ShowMessage("Lỗi", "Vui lòng chọn ngày tháng", 4);
                    return;
                }
                else {
                    addLoading();
                }
            }

            function ValidBirthDay() {
                var date = new Date();
                var this_year = date.getFullYear();
                console.log(this_year);
                var showMsg = true;
                var msg = "";
                var day = $('#EBPopup input.sn-day').val();
                var month = $("#EBPopup input.sn-month").val();
                var year = $("#EBPopup input.sn-year").val();

                //console.log(day, month, year);
                if (parseInt(day) === 0 || parseInt(day) > 31 || day === "") {
                    showMsg = false;
                    msg += "Ngày không hợp lệ. ";
                    $('#EBPopup input.sn-day').css("border-color","pink");
                }
                if (parseInt(month) === 0 || parseInt(month) > 12 || month === "") {
                    showMsg = false;
                    msg += "Tháng không hợp lệ. ";
                   $('#EBPopup input.sn-month').css("border-color","pink");
                }
                if (year.length != 4 || parseInt(year) < 1940 || parseInt(year) >= this_year || year === "") {
                    showMsg = false;
                    msg += "Năm không hợp lệ. ";
                   $('#EBPopup input.sn-year').css("border-color","pink");
                }
                if (day === "" && month === "" && year === "") {
                    showMsg = false;
                    msg = "Vui lòng điền đầy đủ thông tin. Nếu khách hàng không cho ngày sinh vui lòng click vào checkbox bên dưới";
                    $('#EBPopup input.sn-day').css("border-color", "pink");
                    $('#EBPopup input.sn-month').css("border-color", "pink");
                    $('#EBPopup input.sn-year').css("border-color","pink");
                }
                if (!showMsg) {
                    ShowMessage("Thông báo", msg, 4);
                    return showMsg;
                }
                else {
                    return showMsg;
                }
                
            }

            //function GetCustomerRequest() {
            //    var salonId = $("#ddlSalon").val();
            //    $.ajax({
            //        url: api + "/api-booking/fifteen-minutes/booking-request?salonId=" + salonId,
            //        type: "GET",
            //        dataType: "json",
            //        contentType: "application/json;charset:utf-8",
            //        success: function (data) {
            //            //console.log(data);
            //            $(".table-support-info tbody").empty();
            //            var tr = "";
            //            for (var i = 0; i < data.length; i++) {
            //                tr += '<tr>';
            //                tr += '<td style="line-height:30px;">' + data[i].customerName + '</td>';
            //                tr += '<td style="line-height:30px;">' + data[i].customerPhone + '</td>';
            //                tr += '<td style="line-height:30px;">' + data[i].requestTime + '</td>';
            //                tr += '<td style="line-height:30px;">' + data[i].customerRequest + '</td>';
            //                tr += '<td ><input type="checkbox" disabled="disabled" style="height: 25px !important;margin-top: 3px !important" ' + (data[i].isBooked == true ? 'checked="checked"' : '') + ' class="status-booking" /></td>';
            //                tr += '<td style="line-height:30px;">' + (data[i].dateTimeBook == null ? '' : data[i].dateTimeBook) + '</td>';
            //                tr += '<td><textarea class="form-control note-handle" rows="1" placeholder="Ghi chú....">' + (data[i].salonNote == null || data[i].salonNote == '' ? '' : data[i].salonNote) + '</textarea></td>';
            //                tr += '<td>';
            //                tr += GetBookingHandle(data[i].requestStatusId, data[i].id);
            //                tr += '</td>';

            //                tr += '</tr>';
            //            }
            //            $(".table-support-info tbody").append(tr);
            //        },
            //        error: function (xhr, ajaxOptions, thrownError) {
            //            if (xhr.status == 404) {
            //                $(".table-support-info tbody").empty();
            //            }
            //        }
            //    })
            //}

            function GetBookingHandle(statusId, requestId) {
                var select = '<select class="form-control status-handle" data-id=' + requestId + ' onchange="UpdateRequest($(this))">';
                $.ajax({
                    url: "/GUI/FrontEnd/Service/TimeLine_Beta_V2.aspx/GetStatusHandle",
                    type: "POST",
                    dataType: "json",
                    async: false,
                    contentType: "application/json;charset:utf-8",
                    success: function (data) {
                        json = JSON.parse(data.d);
                        select += '<option value="0" ' + (statusId == 0 ? 'selected="selected"' : '') + '>Chọn trạng thái</option>';
                        for (var i = 0; i < json.length; i++) {
                            select += '<option value="' + json[i].Value + '" ' + (json[i].Value == statusId ? 'selected="selected"' : '') + '>' + json[i].Label + '</option>';
                        }
                    },

                });
                select += '</select>';
                return select;
            }

            //function CountingRequestBooking() {
            //    var salonId = $("#ddlSalon").val();
            //    $.ajax({
            //        url: api + "/api-booking/fifteen-minutes/booking-request/count?salonId=" + salonId,
            //        type: "GET",
            //        dataType: "json",
            //        contentType: "application/json;charset:utf-8",
            //        success: function (data) {

            //            if (data.bookingRequest > 0) {
            //                //$(".button-area .fa-bell").css("color", "yellow");
            //                $(".button-area .count-request").css("color", "#f50000");
            //                $(".button-area .count-request").empty().html(data.bookingRequest);
            //            }
            //            else {
            //                //$(".button-area .fa-bell").css("color", "#fff");
            //                $(".button-area .count-request").empty().html(0);
            //            }

            //        },
            //        error: function (xhr, ajaxOptions, thrownError) {
            //            if (xhr.status == 404) {
            //                $(".button-area .count-request").empty().html(0);
            //            }
            //        }
            //    })
            //}

            //function SendBookingRequest(bookingId, customerPhone, salonId) {
            //    var data = JSON.stringify({ bookingTempId: bookingId });
            //    if (CheckBookingRequest(customerPhone, salonId)) {
            //        $.ajax({
            //            url: api + "/api-booking/fifteen-minutes/booking-request/booking",
            //            type: "PUT",
            //            dataType: "json",
            //            contentType: "application/json;charset:utf-8",
            //            data: data,
            //            success: function (response) {
            //                console.log(response);
            //            }
            //        })
            //    }
            //    else return;
            //}

            //function CheckBookingRequest(customerPhone, salonId) {
            //    var result = false;
            //    $.ajax({
            //        url: api + "/api-booking/fifteen-minutes/booking-request/validate?customerPhone=" + customerPhone + "&salonId=" + salonId,
            //        type: "GET",
            //        dataType: "json",
            //        async: false,
            //        contentType: "application/json;charset:utf-8",
            //        success: function (response) {
            //            result = response;
            //        }
            //    })
            //    return result;
            //}

            //function UpdateRequest(This) {
            //    var requestId = This.attr("data-id");
            //    var statusId = This.val();
            //    var note = This.closest("tr").find(".note-handle").val();
            //    $.ajax({
            //        url: api + "/api-booking/fifteen-minutes/booking-request/status/" + requestId,
            //        type: "PUT",
            //        dataType: "json",
            //        data: "{requestStatus:" + statusId + ",salonNote:'" + note + "'}",
            //        contentType: "application/json;charset:utf-8",
            //        success: function (response) {
            //            if (response != null) {
            //                ShowMessage("Thông báo", "Cập nhật thành công", 1);
            //            }
            //        }
            //    })
            //}

            function ValidPhoneNumber() {
                var phone = $("#customerPhone").val();
                var valid = false;
                if (checkNumber(phone)) {
                    valid = true;
                }
                if (checkPhoneLength(phone, 10)) {
                    valid = true;
                }
                if (!valid) {
                    ShowMessage("Thông báo", "Số điện thoại không chính xác. Vui lòng nhập lại", 4);
                    return;
                }
            }
            function ConfirmBooking(phoneNumber, datetime) {
                var c = confirm("Bạn có chắc chắn book lịch cho số ĐT: " + phoneNumber + " vào ngày: " + datetime + " không?");
                var cf = false;
                if (c) {
                    cf = true;
                }

                return cf;
            }

            function addClass() {
                var ct = "<div class='open-booking'><button type='button' class='form-control btn-open-bk' onclick='OpenBooking($(this))'>Mở khung</div>";
                $(".not-enroll.virtual-stylist").append(ct);
            }

            function Open_Booking(This) {
                var date_book = $("#DatedBook").val();
                var day = date_book.substring(0, 2);
                var month = date_book.substring(3, 5);
                var year = date_book.substring(6, 10);
                var dt_book = year + "/" + month + "/" + day;
                var _date = moment(dt_book).format("YYYY/MM/DD");
                console.log(_date);
                var input = new Object();

                input.salonId = bill.salonID;
                input.staffId = parseInt(This.parent().attr("data-stylist"));
                input.workDate = _date;
                input.hourId = parseInt(This.parent().attr("data-hourid"));
                input.doUserId = <%=Convert.ToInt32(Session["User_Id"])%>;


                console.log(input);

                $.ajax({
                    url: time_keeping_api + "api/flowtimeKeeping/insert-time-keeping-open-slot-timeline",
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json;charset:utf-8",
                    data: JSON.stringify(input),
                    success: function (response) {
                        if (response.status === 1) {
                            This.parent().removeClass();
                            This.parent().addClass("board-cell-item is-enroll virtual-stylist");
                            This.css("display", "none");
                            ShowMessage("Thành công", "Mở khung thành công", 1);
                            //location.reload();
                        }
                        else {
                            ShowMessage("Lỗi", "Có lỗi xảy ra. Vui lòng thử lại", 4);
                            return;
                        }
                    }
                })

            }
        </script>
    </asp:Panel>
</asp:Content>

