﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Service_Detail.aspx.cs" Inherits="_30shine.GUI.UIService.Service_Detail" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ServiceAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <link href="/Assets/css/plugin.upload.images/pui.style.css" rel="stylesheet" />
        <%--<script src="/Assets/js/dropzone.js"></script>
<link href="/Assets/css/dropzone.css" rel="stylesheet" />--%>
        <style>
            .customer-add .table-add td.map-edit .edit-wp {
                display: block;
            }

            .customer-vip {
                width: 30%;
                float: left;
                margin-left: 2%;
                font-family: Roboto Condensed Bold;
                font-weight: normal;
                color: #50b347;
                cursor: pointer;
            }

                .customer-vip input[type='checkbox'] {
                    width: auto !important;
                    position: relative;
                    top: 2px;
                    margin-right: 2px;
                }

            .degree90 {
                -webkit-transform: rotate(90deg);
                -moz-transform: rotate(90deg);
                -o-transform: rotate(90deg);
                -ms-transform: rotate(90deg);
                transform: rotate(90deg);
                height: 118px !important;
                width: 118px !important;
            }
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Dịch vụ &nbsp;&#187; </li>
                        <li class="li-listing active"><a href="/dich-vu/danh-sach.html">Danh sách</a></li>
                        <%--<% if(SalonId == 2){ %>--%>
                        <li class="li-pending active"><a href="/dich-vu/pending.html">
                            <div class="pending-1"></div>
                            Pending</a></li>
                        <li class="li-add"><a href="/dich-vu/them-phieu-v3.html">Thêm mới</a></li>
                        <%--<% }else{ %>
                    <li class="li-add"><a href="/dich-vu/them-moi.html">Thêm mới</a></li>
                <% } %>--%>
                        <li class="li-add active"><a href="/dich-vu/dat-lich.html">Đặt lịch</a></li>
                        <li class="li-edit active"><a href="/dich-vu/danh-sach.html">Chi tiết</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add admin-product-add fe-service" onload="MsgHandler()">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <!-- System Message -->
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <!-- END System Message -->

                <div class="table-wp">
                    <table class="table-add admin-product-table-add fe-service-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Thông tin dịch vụ</strong></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr class="tr-margin" style="height: 20px;"></tr>

                            <tr class="tr-field-ahalf">
                                <td class="col-xs-2 left"><span>Khách hàng</span></td>
                                <td class="col-xs-7 right">
                                    <% if (!CusNoInfor)
                                        { %>
                                    <span class="field-wp">
                                        <asp:TextBox ID="CustomerName" runat="server" ClientIDMode="Static" ReadOnly="true" Style="width: 30%;"></asp:TextBox>
                                        <asp:TextBox ID="CustomerCode" ReadOnly="true" CssClass="mgl" placeholder="Nhập mã khách hàng ở đây"
                                            runat="server" ClientIDMode="Static" Style="width: 30%;"></asp:TextBox>
                                        <label class="customer-vip">
                                            <input type="checkbox" runat="server" id="customerVIP" disabled="disabled" />
                                            Tặng thẻ VIP
                                        </label>
                                    </span>
                                    <% }
                                        else
                                        { %>
                                    <span class="field-wp">
                                        <span class="checkbox cus-no-infor">
                                            <label>
                                                <input type="checkbox" checked="checked" disabled="disabled" id="InputCusNoInfor" runat="server" clientidmode="Static" />
                                                Khách không cho thông tin
                                            </label>
                                        </span>
                                    </span>
                                    <% } %>  
                                </td>
                                <td class="col-xs-2"></td>
                            </tr>

                            <tr class="tr-field-ahalf tr-product">
                                <td class="col-xs-2 left"><span>Dịch vụ</span></td>
                                <td class="col-xs-7 right">
                                    <div class="row">
                                        <% if (HasService)
                                            { %>
                                        <%--<div class="show-product">Danh sách dịch vụ</div>--%>
                                        <% }
                                            else
                                            { %>
                                        <div class="no-item" style="font-style: italic;">Không có dịch vụ nào</div>
                                        <% } %>
                                    </div>
                                    <div class="listing-product" id="ListingServiceWp" runat="server" clientidmode="Static" style="margin-top: 0;">
                                        <table class="table table-listing-product" style="margin-bottom: 0!important;">
                                            <thead>
                                                <tr>
                                                    <th>STT</th>
                                                    <th>Tên dịch vụ</th>
                                                    <th>Mã dịch vụ</th>
                                                    <th>Đơn giá</th>
                                                    <th>Số lượng</th>
                                                    <th>Giảm giá</th>
                                                    <th>Thành tiền</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="Rpt_Service" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                                            <td class="td-product-name"><%# Eval("Name") %></td>
                                                            <td class="td-product-code"><%# Eval("Code") %></td>
                                                            <td class="td-product-price" data-price="<%# Eval("Price") %>"><%# Eval("Price") %></td>
                                                            <td class="td-product-quantity">
                                                                <input type="text" class="product-quantity" value="<%# Eval("Quantity") %>" disabled="disabled" />
                                                            </td>
                                                            <td class="td-product-voucher" data-voucher="<%# Eval("VoucherPercent") %>">
                                                                <div class="row">
                                                                    <%# Eval("VoucherPercent").ToString() != "0" ? Eval("VoucherPercent") + "(%)" : "" %>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="box-money" style="display: block;">
                                                                    <%# Convert.ToInt32(Eval("Quantity")) * Convert.ToInt32(Eval("Price")) * 
                                                            (100-Convert.ToInt32(Eval("VoucherPercent")))/100 %>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                    </div>

                                    <% if (IsPromotion == true)
                                        { %>
                                    <div class="row free-service">
                                        <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                            <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">Phụ trợ : </label>
                                        </div>
                                        <asp:Repeater runat="server" ID="Rpt_FreeService">
                                            <ItemTemplate>
                                                <div class="checkbox cus-no-infor" style="padding-bottom: 4px;">
                                                    <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                        <input type="checkbox" checked="checked" disabled="disabled" />
                                                        <%# Eval("Name") %>
                                                    </label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                    <% } %>                      
                                </td>
                                <td class="col-xs-2"></td>
                            </tr>

                            <tr class="tr-field-third">
                                <td class="col-xs-2 left"><span>Nhân viên</span></td>
                                <td class="col-xs-7 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="Hairdresser" ReadOnly="true" runat="server" ClientIDMode="Static" Style="width: 24%;"></asp:TextBox>
                                        <asp:TextBox ID="HairMassage" ReadOnly="true" CssClass="mgl" runat="server" ClientIDMode="Static" Style="width: 24%;"></asp:TextBox>
                                        <asp:TextBox ID="Checkin" ReadOnly="true" CssClass="mgl" runat="server" ClientIDMode="Static" Style="width: 24%;"></asp:TextBox>
                                        <asp:TextBox ID="CheckOut" ReadOnly="true" CssClass="mgl" runat="server" ClientIDMode="Static" Style="width: 25%;"></asp:TextBox>
                                    </span>

                                </td>
                                <td class="col-xs-2"><span class=""></span></td>
                            </tr>

                            <tr class="tr-field-ahalf tr-product">
                                <td class="col-xs-2 left"><span>Sản phẩm</span></td>
                                <td class="col-xs-7 right">
                                 <%--   <div class="row">
                                        <% if (HasProduct)
                                            { %>
                                        <% }
                                            else
                                            { %>
                                        <div class="no-item" style="font-style: italic;">Không có sản phẩm nào</div>
                                        <% } %>
                                    </div>--%>
                                    <div class="listing-product" id="ListingProductWp" runat="server" clientidmode="Static" style="margin-top: 0;">
                                        <table class="table table-listing-product" style="margin-bottom: 0!important;">
                                            <thead>
                                                <tr>
                                                    <th>STT</th>
                                                    <th>Tên sản phẩm</th>
                                                    <th style="display: none;">Mã sản phẩm</th>
                                                    <th style="display: none;"></th>
                                                    <th>Đơn giá</th>
                                                    <th>Số lượng</th>
                                                    <th>Giảm giá</th>
                                                    <th>Nhân viên bán</th>
                                                    <th>Thành tiền</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="Rpt_Product" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                                            <td class="td-product-name"><%#Eval("Name") %></td>
                                                            <td style="display: none !important;" class="td-product-code" data-code='<%#Eval("Code") %>' data-id='<%#Eval("Id") %>'><%#Eval("Code") %></td>
                                                            <!--thêm Mapid-->
                                                            <td class="td-product-map" style="display: none;" data-mapid='<%#Eval("MapIdProduct") %>'><%#Eval("MapIdProduct") %></td>
                                                            <td class="td-product-price" data-price='<%#Eval("Price") %>'><%#Eval("Price") %></td>
                                                            <td class="td-product-quantity">
                                                                <input class="product-quantity" maxlength="3" onkeypress="return ValidateKeypress(/\d/,event);" type="text" value='<%#Eval("Quantity") %>' />
                                                            </td>
                                                            <td class="td-product-voucher" data-voucher="0">
                                                                <div class="row">
                                                                    <input class="product-voucher voucher-cosmetic" onblur="ValidatePercent($(this));" onkeypress="return ValidateKeypress(/\d/,event);" maxlength="10" style="margin: 0 auto; float: none; text-align: center; width: 50px;" type="text" value='<%#Eval("VoucherPercent") %>'>%
                                                                </div>
                                                            </td>
                                                            <td class="td-product-seller">
                                                                <input id="input-seller<%#Eval("Id") %>" class="st-head ip-short" value="<%#Eval("SellerId") %>" disabled="disabled" maxlength="8" data-staff-type="Seller" data-field="Cosmetic" data-value="0" placeholder="Bán mỹ phẩm" style="width: 30% !important; text-align: center; border: 1px solid #ccc;" onkeyup="changeSeller($(this),<%#Eval("Id") %>)" />
                                                                <input id="input-seller-name<%#Eval("Id") %>" value="<%#Eval("SellerName") %>" class="st-head ip-short" data-staff-type="Seller" data-field="Cosmetic" data-value="0" style="width: 50% !important; text-align: left; padding-left: 10px; border: 1px solid #ccc; background-color: #e7e7e7" readonly />
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div class="box-money" style="display: block;">
                                                                    <%# Eval("Promotion").ToString() == "0" ? Convert.ToInt32(Eval("Quantity")) * Convert.ToInt32(Eval("Price")) * (100-Convert.ToInt32(Eval("VoucherPercent")))/100 : Convert.ToInt32(Eval("Quantity")) * Convert.ToInt32(Eval("Price")) - Convert.ToInt32(Eval("Promotion")) %>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                                <td class="col-xs-2"></td>
                            </tr>

                            <tr class="tr-description tr-field-ahalf">
                                <td class="col-xs-2 left"><span>Tổng số tiền</span></td>
                                <td class="col-xs-7 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="TotalMoney" runat="server" ClientIDMode="Static" Text="0" ReadOnly="true"></asp:TextBox>
                                        <span class="unit-money">VNĐ</span>
                                    </span>
                                </td>
                                <td class="col-xs-2"></td>
                            </tr>

                            <%--<tr class="tr-upload">
                        <td class="col-xs-2 left"><span>Ảnh</span></td>
                        <td class="col-xs-7 right">
                            <div class="wrap btn-upload-wp">
                                <div id="Btn_UploadImg" class="btn-upload-img" onclick="OpenIframeImage()">Chọn ảnh đăng</div>
                                <asp:FileUpload ID="UploadFile" ClientIDMode="Static" style="display:none;"
                                    CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                <div class="wrap listing-img-upload">
                                    <% if(HasImages){ %>   
                                    <% for (var i = 0; i < ListImagesName.Length; i++ )
                                       { %>
                                        <div class="thumb-wp" ondblclick ="Exc_cropImage($(this), '<%=ListImagesUrl[i] %>', true)">
                                            <img class="thumb" alt="" title="" src="<%=ListImagesUrl[i] %>" 
                                                data-img="<%=ListImagesUrl[i] %>" />
                                            <span class="delete-thumb" onclick="deleteThum($(this), '<%= ListImagesName[i] %>', true)"></span>
                                        </div>
                                    <% } %>
                                    <% } %>
                                </div>
                            </div>                         
                        </td>
                    </tr>--%>

                            <tr class="tr-description">
                                <td class="col-xs-2 left"><span>Ghi chú</span></td>
                                <td class="col-xs-7 right">
                                    <span class="field-wp">
                                        <asp:TextBox TextMode="MultiLine" Rows="5" ID="Description" runat="server" ClientIDMode="Static" ReadOnly="true"></asp:TextBox>
                                    </span>
                                </td>
                                <td class="col-xs-2"></td>
                            </tr>

                            <tr class="tr-product-category">
                                <td class="col-xs-2 left"><span style="line-height: 22px;">Khách quay lại vì không hài lòng</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:CheckBox ID="ServiceError" Checked="false" runat="server" />
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-upload">
                                <td class="col-xs-2 left"><span>Ảnh</span></td>
                                <td class="col-xs-7 right">
                                    <div class="wrap btn-upload-wp">
                                        <div id="Btn_UploadImg" class="btn-upload-img" onclick="/*OpenIframeImage()*/" style="display: none;">Chọn ảnh đăng</div>
                                        <asp:FileUpload ID="UploadFile" ClientIDMode="Static" Style="display: none;"
                                            CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                        <div class="wrap listing-img-upload">
                                            <% if (HasImages)
                                                { %>
                                            <% for (var i = 0; i < ListImagesName.Length; i++)
                                                { %>
                                            <div class="thumb-wp" ondblclick="Exc_cropImage($(this), '<%=ListImagesName[i] %>', true)">
                                                <img class="thumb" alt="" title="" src="<%=ListImagesName[i] %>"
                                                    data-img="<%=ListImagesName[i] %>" />
                                                <span class="delete-thumb" onclick="deleteThum($(this), '<%= ListImagesName[i] %>', true)" style="display: none;"></span>
                                            </div>
                                            <% } %>
                                            <% } %>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr class="tr-send">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <asp:Panel ID="BtnSend" CssClass="btn-send" runat="server" ClientIDMode="Static">Hoàn tất</asp:Panel>
                                        <asp:Button ID="BtnFakeSend" runat="server" Text="Hoàn tất"
                                            ClientIDMode="Static" OnClick="Update" Style="display: none;"></asp:Button>
                                        <asp:Panel ID="AEdit" runat="server" ClientIDMode="Static" Visible="false" Style="float: left;">
                                            <a class="btn-send" href="/dich-vu/checkout-v1/<%=_Code %>.html" style="margin-left: 10px;">Sửa</a>
                                        </asp:Panel>
                                    </span>
                                </td>
                            </tr>

                            <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
                        </tbody>
                    </table>

                    <!-- input hidden -->
                    <asp:HiddenField ID="HDF_CustomerCode" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_ProductIds" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_TotalMoney" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_UserImages" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_UserImagesDelete" runat="server" ClientIDMode="Static" />
                    <!-- END input hidden -->
                </div>
            </div>
            <%-- end Add --%>
        </div>

        <!-- Popup Images -->
        <%--<div class="plugin-image">
    <div class="plugin-image-head be-report">
        
        <div class="row">
            <div class="filter-item">
                <asp:TextBox CssClass="txtDateTime st-head TxtDateTimeFrom" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                    ClientIDMode="Static" runat="server"></asp:TextBox>
            </div>
            <strong class="st-head" style="margin-left: 10px;">
                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
            </strong>
            <div class="filter-item">
                <asp:TextBox CssClass="txtDateTime st-head TxtDateTimeTo" ID="TxtDateTimeTo" placeholder="Đến ngày"
                    ClientIDMode="Static" runat="server"></asp:TextBox>
            </div>
            <div class="filter-item">
                <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion SgCustomerCode" data-field="customer.code" data-value="0"
                    AutoCompleteType="Disabled" ID="SgCustomerCode" ClientIDMode="Static" placeholder="Mã Khách hàng" runat="server" onblur="BindIdToHDF($(this), $(this).val(), 'customer.code', '#HDF_Suggestion_Code', '#HDF_Suggestion_Field', '#SgCustomerCode', true)"></asp:TextBox>
                <div class="listing-staff-wp eb-select-data ">
                    <ul class="ul-listing-staff ul-listing-suggestion" id="UlCustomerCode"></ul>
                </div>
            </div>
            <div class="filter-item">
                <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                    <ul class="ul-listing-staff ul-listing-suggestion" id="UlCustomerName">
                    </ul>
                </div>
            </div>
            
            <div class="filter-item">
                <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion SgCustomerPhone" data-value="0" data-field="customer.phone"
                        AutoCompleteType="Disabled" ID="SgCustomerPhone" ClientIDMode="Static" placeholder="Số điện thoại" runat="server" onblur="BindIdToHDF($(this), $(this).val(), 'customer.phone', '#HDF_Suggestion_Code', '#HDF_Suggestion_Field', '#SgCustomerPhone', true)"></asp:TextBox>
                <div class="listing-staff-wp eb-select-data">
                    <ul class="ul-listing-staff ul-listing-suggestion" id="UlCustomerPhone"></ul>
                </div>
            </div>
            <a href="javascript://" class="st-head btn-viewdata plugin-image-view" onclick="Plugin_Images_Load()">Xem ảnh</a>
            <a href="javascript://" class="st-head btn-viewdata plugin-image-upload" onclick="Plugin_Images_Upload('<%=_CustomerCode %>')">Upload</a>
            <a href="javascript://" class="st-head btn-viewdata plugin-image-push" onclick="Plugin_Images_Push()">Chèn ảnh</a>
        </div>
        
    </div>
    <div class="plugin-image-content">
        <div class="listing-img-upload">                  
            <asp:Repeater ID="Rpt_Images" runat="server">
                <ItemTemplate>
                    <div class="thumb-wp" onclick="Plugin_Images_Chose(event, $(this))">
                        <img class="thumb" alt="" title="" src="<%#Eval("Value") %>" 
                            data-img="<%#Eval("Value") %>" />
                        <span class="delete-thumb" onclick="deleteThum($(this), '<%#Eval("Value") %>', false)"></span>
                        <div class="thumb-cover"></div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</div>--%>
        <%--<div id="dZUpload" class="dropzone dz-work-flow">
    <div class="dz-default dz-message wrap listing-img-upload">
        Drop image here. 
    </div>
</div>--%>
        <!-- Hidden Field-->
        <asp:HiddenField ID="HDF_Suggestion_Code" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="HDF_Suggestion_Field" ClientIDMode="Static" runat="server" />
        <!-- END Hidden Field-->

        <!-- END Popup Images -->

        <!-- Suggestion -->
        <script src="/assets/js/jquery.mCustomScrollbar.js"></script>
        <link href="/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <script type="text/javascript">
            var Imgs = [],
                ImgsDelete = [],
                DbImgs = [],
                InitDropZone = false;

            jQuery(document).ready(function ($) {
                // execute thumbnail size
                excThumbWidth();

                //$(".fe-service-table-add .listing-img-upload .thumb-wp").each(function () {
                //    var dataImg = $(this).find("img.thumb").attr("data-img");
                //    DbImgs.push(dataImg);
                //});

                //if (!InitDropZone) {
                //    $("#dZUpload").dropzone({
                //        url: "/GUI/BackEnd/UploadImage/DropZoneUploader.ashx?Code=" + "abc",
                //        maxFiles: 100,
                //        addRemoveLinks: true,
                //        success: function (file, response) {
                //            $("#dZUpload").show();
                //            var imgName = response;
                //            file.previewElement.classList.add("dz-success");
                //        },
                //        error: function (file, response) {
                //            file.previewElement.classList.add("dz-error");
                //        }
                //    });
                //    InitDropZone = true;
                //}

                $(window).load(function () {
                    $('.listing-img-upload .thumb-wp').each(function () {
                        var THIS = $(this);

                        $("<img>").attr("src", $(".thumb").attr("src")).load(function () {
                            var ImgW = this.width;
                            var ImgH = this.height;
                            console.log(ImgW);
                            if ((ImgW > ImgH)) {
                                (THIS).find('img').addClass('degree90');
                            }
                        });
                    });
                });
            });

            function Plugin_Images_Upload(Code) {

                //$("#dZUpload").click();
            }

            function Plugin_Images_Chose(event, This) {
                if (This.hasClass("active")) {
                    This.removeClass("active");
                } else {
                    This.addClass("active");
                }
            }

            function Plugin_Images_Push() {
                $('#EBPopup .listing-img-upload .thumb-wp.active').each(function () {
                    var dataImg = $(this).find("img.thumb").attr("data-img");
                    if (Imgs.indexOf(dataImg) == -1 && DbImgs.indexOf(dataImg) == -1) {
                        var _this = $(this).removeClass("active").removeAttr("onclick").attr("ondblclick", "Exc_cropImage($(this), '" + dataImg + "', false)");
                        $(".fe-service-table-add .listing-img-upload").append($(this).removeClass("active"));
                    }
                });
                $(".fe-service-table-add .listing-img-upload .thumb-wp").each(function () {
                    var dataImg = $(this).find("img.thumb").attr("data-img");
                    if (Imgs.indexOf(dataImg) == -1 && DbImgs.indexOf(dataImg) == -1) {
                        Imgs.push(dataImg);
                    }
                });
                autoCloseEBPopup();
            }

            function deleteThum(THIS, data, oldImg, removeParent) {
                oldImg = typeof oldImg !== 'undefined' ? oldImg : false;
                removeParent = typeof removeParent !== 'undefined' ? removeParent : true;
                if (!oldImg) {
                    var index = Imgs.indexOf(data);
                    if (index != -1) {
                        Imgs.splice(index, 1);
                    }
                } else {
                    ImgsDelete.push(data);
                }
                if (removeParent)
                    THIS.parent().remove();
            }

            function Bind_UserImagesToHDF() {
                var jsonImages = JSON.stringify(Imgs);
                var jsonImagesDel = JSON.stringify(ImgsDelete);
                $("#HDF_UserImages").val(jsonImages);
                $("#HDF_UserImagesDelete").val(jsonImagesDel);
            }

            function OpenIframeImage() {

                $(".plugin-image").openEBPopup();
                $(".plugin-image-view").click();
                // Bind Suggestion
                Bind_Suggestion();

                $('#EBPopup .plugin-image-content').mCustomScrollbar({
                    theme: "dark-2",
                    scrollInertia: 100
                });

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });

                // execute thumbnail size
                //excThumbWidth();

            }

            function Plugin_Images_Load() {
                var TimeFrom = $("#EBPopup .TxtDateTimeFrom").val() != undefined ? $("#EBPopup .TxtDateTimeFrom").val() : "",
                    TimeTo = $("#EBPopup .TxtDateTimeTo").val() != undefined ? $("#EBPopup .TxtDateTimeTo").val() : "",
                    SgCode = $("#HDF_Suggestion_Code").val(),
                    SgField = $("#HDF_Suggestion_Field").val();
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/Service/Service_Detail.aspx/Ajax_LoadImages",
                    data: '{TimeFrom : "' + TimeFrom + '", TimeTo : "' + TimeTo + '", SgCode : "' + SgCode + '", SgField : "' + SgField + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            var OBJ = JSON.parse(mission.msg);
                            var Thumbs = "";
                            if (OBJ.length > 0) {
                                $.each(OBJ, function (i, v) {
                                    Thumbs += '<div class="thumb-wp" onclick="Plugin_Images_Chose(event, $(this))">' +
                                        '<img class="thumb" alt="" title="" src="' + v.Value + '" data-img="' + v.Value + '"/>' +
                                        '<span class="delete-thumb" onclick="deleteThum($(this), ' + "'" + v.Value + "'" + ', false)"></span>' +
                                        '<div class="thumb-cover"></div>' +
                                        '</div>';
                                });
                            } else {
                                //
                            }
                            $("#EBPopup .listing-img-upload").empty().append($(Thumbs));
                            // execute thumbnail size
                            excThumbWidth();
                        } else {
                            //
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            //============================
            // Suggestion Functions
            //============================
            function Bind_Suggestion() {
                $(".eb-suggestion").bind("keyup", function (e) {
                    if (e.keyCode == 40) {
                        UpDownListSuggest($(this));
                    } else {
                        Call_Suggestion($(this));
                    }
                });
                $(".eb-suggestion").bind("focus", function () {
                    Call_Suggestion($(this));
                });
                $(".eb-suggestion").bind("blur", function () {
                    //Exc_To_Reset_Suggestion($(this));
                });
                $(window).unbind("click").bind("click", function (e) {
                    if ((!e.target.className.match("eb-select") && e.target.parentElement.className.match("ul-listing-suggestion")) ||
                        (!e.target.className.match("eb-select") && !e.target.parentElement.className.match("ul-listing-suggestion"))) {
                        EBSelect_HideBox();
                    }
                });
            }

            function UpDownListSuggest(This) {
                var UlSgt = This.parent().find(".ul-listing-suggestion"),
                    index = 0,
                    LisLen = UlSgt.find(">li").length - 1,
                    Value;

                This.blur();
                UlSgt.find(">li.active").removeClass("active");
                UlSgt.find(">li:eq(" + index + ")").addClass("active");

                $(window).unbind("keydown").bind("keydown", function (e) {
                    if (e.keyCode == 40) {
                        if (index == LisLen) return false;
                        UlSgt.find(">li.active").removeClass("active");
                        UlSgt.find(">li:eq(" + (++index) + ")").addClass("active");
                        return false;
                    } else if (e.keyCode == 38) {
                        if (index == 0) return false;
                        UlSgt.find(">li.active").removeClass("active");
                        UlSgt.find(">li:eq(" + (--index) + ")").addClass("active");
                        return false;
                    } else if (e.keyCode == 13) {
                        // Bind data to HDF Field
                        var THIS = UlSgt.find(">li.active");
                        var Value = THIS.text().trim();
                        //var Value = THIS.attr("data-code");
                        var dataField = This.attr("data-field");

                        BindIdToHDF(THIS, Value, dataField, "#HDF_Suggestion_Code", "#HDF_Suggestion_Field", This);
                        EBSelect_HideBox();
                    }
                });
            }

            function Exc_To_Reset_Suggestion(This) {
                var value = This.val();
                if (value == "") {
                    $(".eb-suggestion").each(function () {
                        var THIS = $(this);
                        var sgValue = THIS.val();
                        if (sgValue != "") {
                            BindIdToHDF(THIS, sgValue, THIS.attr("data-field"), "#HDF_Suggestion_Code", "#HDF_Suggestion_Field", THIS);
                            return false;
                        }
                    });
                }
            }

            function Call_Suggestion(This) {
                var text = This.val(),
                    field = This.attr("data-field");
                Suggestion(This, text, field);
            }

            function Suggestion(This, text, field) {
                var This = This;
                var text = text || "";
                var field = field || "";
                var InputDomId;
                var HDF_Sgt_Code = "#HDF_Suggestion_Code";
                var HDF_Sgt_Field = "#HDF_Suggestion_Field";

                if (text == "") return false;

                switch (field) {
                    //case "customer.name": InputDomId = "#CustomerName"; break;
                    case "customer.phone": InputDomId = "#EBPopup .SgCustomerPhone"; break;
                    case "customer.code": InputDomId = "#EBPopup .SgCustomerCode"; break;
                    //case "bill.code": InputDomId = "#BillCode"; break;
                }

                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Ajax/Suggestion.aspx/Suggestion_Customer",
                    data: '{field : "' + field + '", text : "' + text + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            var OBJ = JSON.parse(mission.msg);
                            if (OBJ.length > 0) {
                                var lis = "";
                                $.each(OBJ, function (i, v) {
                                    lis += "<li data-code='" + v.Customer_Code + "'" +
                                        "onclick=\"BindIdToHDF($(this),'" + v.Value + "','" + field + "','" + HDF_Sgt_Code +
                                        "','" + HDF_Sgt_Field + "','" + InputDomId + "')\">" +
                                        v.Value +
                                        "</li>";
                                });
                                This.parent().find("ul.ul-listing-suggestion").empty().append(lis).parent().show();
                                This.parent().find(".eb-select-data").show();
                            } else {
                                This.parent().find(".eb-select-data").hide().find("ul.ul-listing-suggestion").empty();
                            }
                        } else {
                            This.parent().find("ul.ul-listing-suggestion").empty();
                            var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                            showMsgSystem(msg, "warning");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function BindIdToHDF(THIS, Code, Field, HDF_Sgt_Code, HDF_Sgt_Field, Input_DomId, NoSgt) {
                var NoSgt = NoSgt || false;
                var text = THIS.text().trim();
                //$("input.eb-suggestion").val("");
                $(HDF_Sgt_Code).val(Code);
                $(HDF_Sgt_Field).val(Field);
                if (NoSgt) {
                    $(Input_DomId).val(Code);
                } else {
                    $(Input_DomId).val(text);
                }
                $(Input_DomId).parent().find(".eb-select-data").hide();
                // Auto post server
                //$("#BtnFakeUP").click();
            }

            function EBSelect_HideBox() {
                $(".eb-select-data").hide();
                $("ul.ul-listing-suggestion li.active").removeClass("active");
            }
        </script>
        <!-- END Suggestion -->

        <script type="text/javascript">
            jQuery(document).ready(function () {
                // Add active menu
                $("#glbService").addClass("active");
                $(".li-edit").addClass("active");

                // Price Format
                $("#BoxMoneyPrice").text(FormatPrice($("#BoxMoneyPrice").text())).show();
                $("#TotalMoney").val(FormatPrice($("#TotalMoney").val()));

                $(".box-money").each(function () {
                    $(this).text(FormatPrice($(this).text().trim()));
                });

                $(".td-product-price").each(function () {
                    $(this).text(FormatPrice($(this).text().trim()));
                });

                //============================
                // Show System Message
                //============================ 
                var qs = getQueryStrings();
                showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);

                //==============================
                // Upload Images
                //==============================
                $("#UploadFile").unbind("change").bind("change", function () {
                    readImage(this);
                });

                // Btn Send
                $("#BtnSend").bind("click", function () {
                    Bind_UserImagesToHDF();
                    $("#BtnFakeSend").click();
                });


                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });

            });

            function showMsgSystem(msg, status) {
                $("#MsgSystem").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);
                setTimeout(function () {
                    $("#MsgSystem").fadeTo("slow", 0, function () {
                        $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 1);
                    });
                }, 5000);
            }

            //=================================================
            // Xử lý ảnh
            //=================================================
            //draw image while upload
            var ListImgName = [],
                ListImgSrc = [],
                LIstImgDelete = [],
                ImgStandardWidth = 120,
                ImgStandardHeight = 120;

            function readImage(THIS) {
                var LoopName = 0;
                if (THIS.files.length > 0) {
                    for (var i = 0; i < THIS.files.length; i++) {
                        var FR = new FileReader();
                        FR.onload = function (e) {
                            var img = new Image();
                            img.src = e.target.result;
                            img.onload = function () {
                                ListImgSrc.push(img.src);
                                var width = ImgStandardWidth;
                                var height = ImgStandardWidth * img.height / img.width;
                                var left = 0;
                                var top = (ImgStandardHeight - height) / 2;
                                var ThumbWp = "<div class='thumb-wp'>" +
                                    "<img class='thumb' style='width:" + width + "px;height:" +
                                    height + "px;left:" + left + "px;top:" + top + "px;' " + "src='" + img.src + "' />" +
                                    "<span class=\"delete-thumb\" onclick=\"deleteThum($(this), '" + img.src + "')\"></span></div>";
                                $(".listing-img-upload").append($(ThumbWp));

                            };
                        };
                        FR.readAsDataURL(THIS.files[i]);
                    }
                }
            }

            function Bind_UserImagesToHDF1() {
                var jsonImages = JSON.stringify(ListImgSrc);
                var jsonImagesDel = JSON.stringify(LIstImgDelete);
                $("#HDF_UserImages").val(jsonImages);
                $("#HDF_UserImagesDelete").val(jsonImagesDel);
            }

            function deleteThum1(THIS, data, oldImg) {
                oldImg = oldImg || false;
                if (!oldImg) {
                    var index = ListImgSrc.indexOf(data);
                    if (index != -1) {
                        ListImgSrc.splice(index, 1);
                    }
                } else {
                    LIstImgDelete.push(data);
                }
                THIS.parent().remove();
            }

            function excThumbWidth() {
                var ImgStandardWidth = 120,
                    ImgStandardHeight = 120;
                var width = ImgStandardWidth,
                    height, left, top;
                $(".thumb-wp .thumb").each(function () {
                    height = ImgStandardWidth * $(this).height() / $(this).width();
                    left = 0;
                    top = (ImgStandardHeight - height) / 2;
                    $(this).css({ "width": width, "height": height, "left": left, "top": top });
                });
            }

            function AjaxUploadImage_BillService(ImageName) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Ajax/UploadImage.aspx/Add_User_Hair",
                    data: '{ImageName : "' + ImageName + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            +
                                ListImgName.push(mission.msg);
                        } else {
                            var msg = "Đã có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển";
                            showMsgSystem(msg, "warning");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }
        </script>

        <!-- Crop Image -->
        <link rel="stylesheet" type="text/css" href="/Assets/css/plugin.crop/example.css" />
        <link rel="stylesheet" type="text/css" href="/Assets/css/plugin.crop/crop.css" />
        <script src="/Assets/js/plugin.crop/syntaxhighlight.js"></script>
        <script src="/Assets/js/plugin.crop/crop.js"></script>

        <div class="plugin-crop-image" style="display: none; width: 800px;">
            <div class="example">
                <!-- cropper container element -->
                <div class="default"></div>
            </div>

            <ul class="cmd">
                <%--<li onclick="foo.import();">Import</li>--%>
                <%--<li onclick="foo.zoom('min');">Min</li>
		    <li onclick="foo.zoom('max');">Max</li>--%>
                <%--<li onclick="foo.flip();">Flip</li>--%>
                <li onclick="foo.rotate();"><i class="fa fa-share"></i>&nbsp;&nbsp;Xoay</li>
                <%--<li class="maskbg">Mask</li>--%>
                <%--<li><a id="unqiueID" onclick="foo.download(300, 300, 'test', 'png', this.id);" href="#">Download</a></li>--%>
                <li onclick="cropImage()" data-tooltip="Click để cắt ảnh và lưu lên server">
                    <i class="fa fa-crop"></i>&nbsp;&nbsp;Cắt ảnh và lưu
                </li>
                <%--<li onclick="saveOriginal()" data-tooltip="Click để lưu ảnh gốc lên server">
                <i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Lưu ảnh gốc
		    </li>--%>
            </ul>
        </div>

        <script type="text/javascript">
            // Declare global variable
            var foo, _This, _ImgPath, _OldImg;

            function Exc_cropImage(This, ImgPath, OldImg) {
                $(".plugin-crop-image").openEBPopup();
                initCrop(This, ImgPath, OldImg);
            }

            function initCrop(This, ImgPath, OldImg) {
                var _script = "<script id='ScriptCropImage'>" +
                    "foo = new CROP();" +
                    "foo.init({" +
                    "container: '#EBPopup .default'," +
                    "image: '" + ImgPath + "'," +
                    "width: 480," +
                    "height: 480," +
                    "mask: false," +
                    "zoom: {" +
                    "steps: 0.01," +
                    "min: 1," +
                    "max: 5" +
                    "}" +
                    "});" +
                    "$('body').on('click', '#EBPopup li.maskbg', function () {" +
                    "$('#EBPopup .example').toggleClass('maskbg');" +
                    "$('#EBPopup .default').attr('data-mask', $('#EBPopup .default').attr('data-mask') === 'true' ? 'false' : 'true');" +
                    "$('#EBPopup li.maskbg').html($('#EBPopup li.maskbg').html() == 'Mask' ? 'Unmask' : 'Mask');" +
                    "});" +
                    "<\/script>";

                $("#ScriptCropImage").remove();
                $('body').append($(_script));
                var mgTop = Math.abs($(window).height() - $("#EBPopup .plugin-crop-image").height()) / 2;
                $("#EBPopupFloat").css({ "margin-top": mgTop + "px" });

                _ImgPath = ImgPath;
                _This = This;
                _OldImg = OldImg;
            }

            function cropImage() {
                var imgCrop = foo.crop(480, 480, 'png');
                var CustomerCode = $("#HDF_CustomerCode").val();
                $.ajax({
                    type: 'POST',
                    url: "/GUI/SystemService/Ajax/UploadImage.aspx/Crop_User_Image",
                    data: '{ "ImageBase64" : "' + imgCrop.string + '", "ImgPath" : "' + _ImgPath + '", "Code" : "' + CustomerCode + '" }',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (response) {
                        var Rsp = $.parseJSON(response.d);
                        if (Rsp.success) {
                            _This.find("img.thumb").attr("src", Rsp.msg).attr("data-img", Rsp.msg).parent().attr("ondblclick", "Exc_cropImage($(this), '" + Rsp.msg + "', false)");
                            _This.find(".delete-thumb").attr("onclick", "deleteThum($(this), '" + Rsp.msg + "', true)");

                            deleteThum(_This.find(".delete-thumb"), _ImgPath, _OldImg, false);
                            Plugin_Images_Push();
                            Bind_UserImagesToHDF();
                        }
                    }
                });
            }

            function saveOriginal() {
                console.log(foo.original());
                var ImgOriginal = foo.original();
                $.ajax({
                    type: 'POST',
                    url: "/GUI/SystemService/Ajax/UploadImage.aspx/Add_User_Hair",
                    data: '{ "ImageBase64" : "' + ImgOriginal.string + '" }',
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    success: function (msg) {
                        //alert('Image saved successfully !');
                        var Rsp = $.parseJSON(msg);
                        //if (Rsp.success) {
                        //    _This.find("img.thumb").attr("src", Rsp.msg).attr("data-img", Rsp.msg);
                        //}
                    }
                });
            }
        </script>
        <!--/ Crop Image -->

    </asp:Panel>
</asp:Content>
