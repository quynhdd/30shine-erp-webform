﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Service_Pending_Complete.aspx.cs" Inherits="_30shine.GUI.UIService.Service_Pending_Complete" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>30Shine - Checkout</title>
    <link href="/Assets/css/bootstrap/bootstrap-toggle.min.css" rel="stylesheet" />
    <script src="/Assets/js/bootstrap/bootstrap-toggle.min.js"></script>
    <link href="/Assets/css/checkout.css" rel="stylesheet" />
    <script src="/assets/js/jquery.mCustomScrollbar.js"></script>
    <link href="/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
    <script src="/Assets/js/erp.30shine.com/service.checkout.js"></script>  
    <style type="text/css">
        .pSpecCus{
            padding-top: 7px; float: left; width: 100%; text-transform: uppercase; color: red!important;
        }
        .cusInfo{
            width: 100%; float: left; border: none;
        }
        .pInfo{
            padding-top: 7px; float: left; width: 100%; color: red!important;
        }
    </style>
</asp:Content>

<asp:Content ID="ServiceAdd" ContentPlaceHolderID="CtMain" runat="server">
<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">
                <li>Dịch vụ &nbsp;&#187; </li>
                <li class="li-listing"><a href="/dich-vu/danh-sach.html">Danh sách</a></li>
                <li class="li-pending"><a href="/dich-vu/pending.html"><div class="pending-1"></div>Pending</a></li>
                <li class="li-pending-complete"><a href="/dich-vu/pending.html"><div class="pending-1"></div>Hoàn tất</a></li>
                <li class="li-add active"><a href="/dich-vu/them-phieu-v3.html">Thêm mới</a></li>
                 <%--<li class="li-add active"><a href="/dich-vu/dat-lich.html">Đặt lịch</a></li>--%>
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add admin-product-add fe-service">    
    <%-- Add --%>                
    <div class="wp960 content-wp">
        <!-- System Message -->
        <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
        <!-- END System Message -->

        <div class="table-wp">
            <table class="table-add admin-product-table-add fe-service-table-add">
                <tbody>
                    <tr class="title-head">
                        <td><strong>Thông tin dịch vụ</strong></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr class="tr-margin" style="height: 20px;"></tr>

                    <tr class="tr-field-ahalf">
                        <td class="col-xs-2 left"><span>Khách hàng</span></td>
                        <td class="col-xs-10 right">
                            <% if(!CusNoInfor){ %>
                            <div class="field-wp">
                                <asp:TextBox ID="CustomerName" CssClass="form-control" runat="server" ClientIDMode="Static" ReadOnly="true" style="width: 30%;"></asp:TextBox>
                                <asp:TextBox ID="CustomerCode" ReadOnly="true" CssClass="mgl form-control" placeholder="Nhập mã khách hàng ở đây" 
                                    runat="server" ClientIDMode="Static" style="width: 30%;"></asp:TextBox>
                                <div class="field cusInfo">
                                    <p class="pSpecCus"></p>
                                </div>
                                <div class="field cusInfo">
                                    <p class="pNote"></p>
                                </div>
                                <div class="field cusInfo">
                                    <p class="pInfo"></p>
                                </div>
                                <div class="field cusInfo">
                                    <p class="pExam"></p>
                                </div>
                                <div class="item-vipcard" onclick="activeVIPCard($(this))" style="display:none;">
                                    <div class="left"><i class="fa fa-square-o" aria-hidden="true"></i></div>
                                    <div class="right">
                                        <input type="text" class="form-control" placeholder="Mã thẻ tặng" disabled="disabled" runat="server" id="VIPGive" onkeydown="validateVIPCard($(this),event)" onchange="validateVIPCardValue($(this), event)" onblur="validateVIPCardValue($(this), event)" ClientIDMode="Static" />
                                    </div>
                                </div>
                                <div class="item-vipcard" onclick="activeVIPCard($(this))" style="display:none;">
                                    <div class="left"><i class="fa fa-square-o" aria-hidden="true"></i></div>
                                    <div class="right">
                                        <input type="text" class="form-control" placeholder="Mã thẻ dùng" disabled="disabled" runat="server" id="VIPUse" onkeydown="validateVIPCard($(this),event)" onchange="validateVIPCardValue($(this), event)" onblur="validateVIPCardValue($(this), event)" ClientIDMode="Static"/>
                                    </div>
                                </div>
                            </div>
                            <% }else{ %>
                            <span class="field-wp">
                                <span class="checkbox cus-no-infor">
                                    <label>
                                        <input type="checkbox" checked="checked" disabled="disabled" id="InputCusNoInfor" runat="server" ClientIDMode="Static"/> Khách không cho thông tin
                                    </label>
                                </span>
                            </span>
                            <% } %>  
                        </td>                        
                    </tr>

                    <tr class="tr-field-ahalf tr-product">
                        <td class="col-xs-2 left"><span>Dịch vụ</span></td>
                        <td class="col-xs-10 right">
                            <%--<span class="field-wp">
                                <asp:DropDownList ID="ServiceName" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <asp:RequiredFieldValidator InitialValue="0" 
                                    ID="rfvDDL_Product" Display="Dynamic" 
                                    ControlToValidate="ServiceName"
                                    runat="server"  Text="Bạn chưa chọn dịch vụ!" 
                                    ErrorMessage="Please Select the Product"
                                    ForeColor="Red"
                                    CssClass="fb-cover-error select-service-error">
                                </asp:RequiredFieldValidator>
                            </span>--%>
                            <div class="row" id="quick-service">                                
                                <asp:Repeater runat="server" ID="Rpt_ServiceFeatured">
                                    <ItemTemplate>
                                        <div class="checkbox cus-no-infor">
                                            <label class="lbl-cus-no-infor">
                                                <input type="checkbox" data-code="<%# Eval("Code") %>" onclick="pushQuickData($(this), 'service', $('#HDF_CustomerType').val(), $('#HDF_DiscountServices').val())" /> <%# Eval("Name") %>
                                            </label>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>

                                <%--<div class="show-product show-item" data-item="service"><i class="fa fa-plus-circle"></i>Thêm dịch vụ</div>--%>
                            </div>                                                    
                            <div class="listing-product item-service" id="ListingServiceWp" runat="server" ClientIDMode="Static">
                                <table class="table table-listing-product table-item-service" id="table-item-service">
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Tên dịch vụ</th>
                                            <th>Mã dịch vụ</th>
                                            <th>Đơn giá</th>
                                            <th>Số lượng</th>
                                            <th>Giảm giá</th>
                                            <th>Thành tiền</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="Rpt_Service_Bill" runat="server">
                                            <ItemTemplate>
                                                <tr data-serviceid="<%# Eval("Id") %>">
                                                    <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                                    <td class="td-product-name"><%# Eval("Name") %></td>
                                                    <td class="td-product-code" data-id="<%# Eval("Id")%>"><%# Eval("Code") %></td>
                                                    <td class="td-product-price" data-price="<%# Eval("Price") %>"><%# Eval("Price") %></td>
                                                    <td class="td-product-quantity">
                                                        <input type="text" class="product-quantity form-control" value="<%# Eval("Quantity") %>" />
                                                    </td>
                                                    <td class="td-product-voucher" data-voucher="<%# Eval("VoucherPercent") %>">
                                                        <div class="row">
                                                            <input type="text" class="product-voucher voucher-services form-control" value="<%# Eval("VoucherPercent") %>" style="margin: 0 auto; float: none; text-align: center; width: 50px;" /> %
                                                        </div>
                                                    </td>
                                                    <td class="map-edit">
                                                        <div class="box-money" style="display:block;">
                                                            <%# Convert.ToInt32(Eval("Quantity")) * Convert.ToInt32(Eval("Price")) * 
                                                            (100-Convert.ToInt32(Eval("VoucherPercent")))/100 %>
                                                        </div>
                                                        <div class="edit-wp">
                                                            <a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),'<%# Eval("Name") %>',<%# Eval("Id") %>,'service')"
                                                                href="javascript://" title="Xóa"></a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>     
                                    </tbody>
                                </table>
                            </div>  

                            <div class="row free-service">
                                <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                    <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">Phụ trợ : </label>
                                </div>
                                <asp:Repeater runat="server" ID="Rpt_FreeService">
                                    <ItemTemplate>
                                        <div class="checkbox cus-no-infor" style="padding-bottom: 4px;">
                                            <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                <input type="checkbox" onclick="pushFreeService($(this), <%# Eval("Id")%>)" data-id="<%# Eval("Id") %>" 
                                                    <%# Eval("Status").ToString() == "1" ? "checked='checked'" : "" %> />
                                                <%# Eval("Name") %>
                                            </label>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>

                        </td>
                    </tr>

                    <tr class="tr-field-ahalf tr-field-third">
                        <td class="col-xs-2 left"><span>Nhân viên</span></td>
                        <td class="col-xs-10 right">
                            <div class="field-wp">
                                <%--<asp:DropDownList ID="Hairdresser" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <asp:DropDownList ID="HairMassage" CssClass="mgl" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <asp:DropDownList ID="Cosmetic" CssClass="mgl" runat="server" ClientIDMode="Static"></asp:DropDownList>--%>
                                <span id="ValidateCosmetic" class="fb-cover-error" style="visibility:hidden; top: -28px; width: 255px;">
                                    Bạn chưa chọn nhân viên bán mỹ phẩm!
                                </span>
                                <div class="filter-item" style="width: 25%;">
                                    <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion auto-select form-control" data-field="Hairdresser" data-value="0"
                                         AutoCompleteType="Disabled" ID="InputStylist" ClientIDMode="Static" placeholder="Stylist" runat="server"></asp:TextBox>
                                    <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                                        <ul class="ul-listing-staff ul-listing-suggestion" id="Ul1"></ul>
                                    </div>
                                    <div class="fake-value" id="FVStylist" runat="server" ClientIDMode="Static"></div>
                                </div>

                                <div class="filter-item" style="width: 25%;">
                                    <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion auto-select form-control" data-field="HairMassage" data-value="0"
                                         AutoCompleteType="Disabled" ID="InputSkinner" ClientIDMode="Static" placeholder="Skinner" runat="server"></asp:TextBox>
                                    <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                                        <ul class="ul-listing-staff ul-listing-suggestion" id="Ul2"></ul>
                                    </div>
                                    <div class="fake-value" id="FVSkinner" runat="server" ClientIDMode="Static"></div>
                                </div>

                                <div class="filter-item" style="width: 25%;">
                                    <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion auto-select form-control" data-field="Cosmetic" data-value="0"
                                         AutoCompleteType="Disabled" ID="InputSeller" ClientIDMode="Static" placeholder="Bán mỹ phẩm" runat="server"></asp:TextBox>
                                    <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                                        <ul class="ul-listing-staff ul-listing-suggestion" id="Ul3"></ul>
                                    </div>
                                    <div class="fake-value" id="FVSeller" runat="server" ClientIDMode="Static"></div>
                                </div>
                                <div class="filter-item" style="width: 25%;">
                                    <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion auto-select form-control" data-field="HDF_Reception" data-value="0"
                                         AutoCompleteType="Disabled" ID="InputReception" ClientIDMode="Static" placeholder="Lễ tân đón khách" runat="server"></asp:TextBox>
                                    <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                                        <ul class="ul-listing-staff ul-listing-suggestion" id="Ul3"></ul>
                                    </div>
                                    <div class="fake-value" id="FVReception" runat="server" ClientIDMode="Static"></div>
                                </div>
                            </div>
                                            
                        </td>
                    </tr>

                    <tr class="tr-field-ahalf tr-product">
                        <td class="col-xs-2 left"><span>Sản phẩm</span></td>
                        <td class="col-xs-10 right">
                            <div class="row" id="quick-product">
                                <div class="col-xs-12">
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <asp:Repeater runat="server" ID="Rpt_ProductFeatured_Combo">
                                                <ItemTemplate>
                                                    <div class="checkbox cus-no-infor">
                                                        <label class="lbl-cus-no-infor">
                                                            <input type="checkbox" data-code="<%# Eval("Code") %>" onclick="pushQuickData($(this), 'product')" />
                                                            <%# Eval("Name") %>
                                                        </label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                        <div class="col-xs-3">
                                            <asp:Repeater runat="server" ID="Rpt_ProductFeatured_Gom">
                                                <ItemTemplate>
                                                    <div class="checkbox cus-no-infor">
                                                        <label class="lbl-cus-no-infor">
                                                            <input type="checkbox" data-code="<%# Eval("Code") %>" onclick="pushQuickData($(this), 'product')" />
                                                            <%# Eval("Name") %>
                                                        </label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                        <div class="col-xs-5">
                                            <asp:Repeater runat="server" ID="Rpt_ProductFeatured_Sap">
                                                <ItemTemplate>
                                                    <div class="checkbox cus-no-infor">
                                                        <label class="lbl-cus-no-infor">
                                                            <input type="checkbox" data-code="<%# Eval("Code") %>" onclick="pushQuickData($(this), 'product')" />
                                                            <%# Eval("Name") %>
                                                        </label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                </div>                                
                                <div class="show-product show-item"  data-item="product"><i class="fa fa-plus-circle"></i>Thêm sản phẩm</div>
                            </div>                            
                            <div class="listing-product item-product" id="ListingProductWp" runat="server" ClientIDMode="Static">
                                <table class="table table-listing-product table-item-product" id="table-item-product">
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Tên sản phẩm</th>
                                            <th>Mã sản phẩm</th>
                                            <th>Đơn giá</th>
                                            <th>Số lượng</th>
                                            <th>Giảm giá</th>
                                            <th>Thành tiền</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="Rpt_Product_Bill" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                                    <td class="td-product-name"><%# Eval("Name") %></td>
                                                    <td class="td-product-code" data-id="<%# Eval("Id") %>"><%# Eval("Code") %></td>
                                                    <td class="td-product-price" data-price="<%# Eval("Price") %>"><%# Eval("Price") %></td>
                                                    <td class="td-product-quantity">
                                                        <input type="text" class="product-quantity form-control" value="<%# Eval("Quantity") %>" />
                                                    </td>
                                                    <td class="td-product-voucher" data-voucher="<%# Eval("VoucherPercent") %>">
                                                        <div class="row">
                                                            <input type="text" class="product-voucher form-control" value="<%# Eval("VoucherPercent") %>" style="margin: 0 auto; float: none; text-align: center;width: 50px; <%# Eval("Promotion").ToString() == "0" ? "" : "text-decoration: line-through;" %>" />
                                                        </div>
                                                        <div class="row promotion-money hidden">
                                                            <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                                                <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                                    <input type="checkbox" class="item-promotion" data-value="35000" data-id="1" style="margin-left: 0;" <%# Eval("Promotion").ToString() == "35000" ? "checked='checked'" : "" %> /> - 35.000 VNĐ </label>

                                                            </div><br />
                                                            <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                                                <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                                    <input type="checkbox" class="item-promotion" data-id="2" data-value="50000" style="margin-left: 0;" <%# Eval("Promotion").ToString() == "50000" ? "checked='checked'" : "" %>/> - 50.000 VNĐ </label>

                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="map-edit">
                                                        <div class="box-money" style="display:block;">
                                                            <%# Eval("Promotion").ToString() == "0" ? Convert.ToInt32(Eval("Quantity")) * Convert.ToInt32(Eval("Price")) * (100-Convert.ToInt32(Eval("VoucherPercent")))/100 : Convert.ToInt32(Eval("Quantity")) * Convert.ToInt32(Eval("Price")) - Convert.ToInt32(Eval("Promotion")) %>
                                                        </div>
                                                        <div class="edit-wp">
                                                            <a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),'<%# Eval("Name") %>','product')"
                                                                href="javascript://" title="Xóa"></a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </div>                                        
                        </td>
                    </tr>

                    <tr class="tr-description tr-field-ahalf">
                        <td class="col-xs-2 left"><span>Tổng số tiền</span></td>
                        <td class="col-xs-10 right">
                            <span class="field-wp">
                                <asp:TextBox ID="TotalMoney" CssClass="form-control" runat="server" ClientIDMode="Static" Text="0" ReadOnly="true"></asp:TextBox>
                                <span class="unit-money">VNĐ</span>
                                <label class="label-paybycard">
                                    <asp:CheckBox runat="server" ID="PayByCard" ClientIDMode="Static"/>
                                    <span>Thanh toán qua thẻ</span>
                                </label>
                            </span>
                        </td>
                    </tr>

                    <%--<tr class="tr-upload" style="display:none;">
                        <td class="col-xs-2 left"><span>Đăng ảnh</span></td>
                        <td class="col-xs-9 right">
                            <div class="wrap btn-upload-wp">
                                <div id="Btn_UploadImg" class="btn-upload-img" onclick="OpenIframeImage()">Chọn ảnh đăng</div>
                                <div class="wrap listing-img-upload"></div>
                            </div>                         
                        </td>
                    </tr>--%>

                    <tr class="tr-description">
                        <td class="col-xs-2 left"><span>Ghi chú</span></td>
                        <td class="col-xs-10 right">
                            <span class="field-wp">
                                <asp:TextBox TextMode="MultiLine" CssClass="form-control" Rows="3" ID="Description" runat="server" ClientIDMode="Static"></asp:TextBox>
                            </span>
                        </td>
                    </tr>

                    <tr class="tr-rating tr-field-ahalf">
                        <td class="col-xs-2 left"><span>ĐIỂM RATING</span></td>
                        <td class="col-xs-10 right">
                            <div class="field-wp">
                                <div class="rating-icon-wrap">
                                    <div class="rating-icon icon-happy" onclick="excRating($(this), 3)">Rất hài lòng</div>
                                    <div class="rating-icon icon-normal" onclick="excRating($(this), 2)">Hài lòng</div>
                                    <div class="rating-icon icon-sad" onclick="excRating($(this), 1)">Chưa hài lòng</div>
                                    <%--<div class="rating-icon icon-happy">Rất Hài lòng</div>                                    
                                    <div class="rating-icon icon-normal">Cũng được</div>                                    
                                    <div class="rating-icon icon-sad">Không hài lòng</div>--%>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <tr class="tr-field-ahalf" runat="server" id="TrSalon" visible="false">
                        <td class="col-xs-2 left"><span>Salon</span></td>
                        <td class="col-xs-10 right">
                            <span class="field-wp">
                                <asp:DropDownList ID="Salon" CssClass="form-control select" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <asp:RequiredFieldValidator InitialValue="0" 
                                    ID="ValidateSalon" Display="Dynamic" 
                                    ControlToValidate="Salon"
                                    runat="server"  Text="Bạn chưa chọn Salon!" 
                                    ErrorMessage="Vui lòng chọn Salon!"
                                    ForeColor="Red"
                                    CssClass="fb-cover-error">
                                </asp:RequiredFieldValidator>
                            </span>
                        </td>
                    </tr>

                    <tr class="tr-upload">
                        <td class="col-xs-2 left"><span>Ảnh KH</span></td>
                        <td class="col-xs-7 right">
                            <div class="wrap btn-upload-wp">
                                <div id="Btn_UploadImg" class="btn-upload-img" onclick="/*OpenIframeImage()*/" style="display:none;">Chọn ảnh đăng</div>
                                <asp:FileUpload ID="UploadFile" ClientIDMode="Static" style="display:none;"
                                    CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                <div class="wrap listing-img-upload">
                                    <% if(HasImages){ %>   
                                    <% for (var i = 0; i < ListImagesName.Length; i++ )
                                       { %>
                                        <div class="thumb-wp" ondblclick ="Exc_cropImage($(this), '<%=ListImagesName[i] %>', true)">
                                            <img class="thumb" alt="" title="" src="<%=ListImagesName[i] %>" 
                                                data-img="<%=ListImagesName[i] %>" />
                                            <span class="delete-thumb" onclick="deleteThum($(this), '<%= ListImagesName[i] %>', true)" style="display:none;"></span>
                                        </div>
                                    <% } %>
                                    <% } %>
                                </div>
                            </div>                         
                        </td>
                    </tr>

                    <tr>
                        <td class="col-xs-1 left"><span style="line-height: 18px;"></span></td>
                        <td class="col-xs-11 right">
                            <div class="row free-service">
                                <div class="checkbox cus-no-infor" style="padding-bottom: 4px;">
                                    <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                        <asp:CheckBox runat="server" ID="isX2" ClientIDMode="Static" />
                                        Tăng ca
                                    </label>
                                </div>
                            </div>
                        </td>
                    </tr>
                    
                    <tr class="tr-send">
                        <td class="col-xs-2 left"></td>
                        <td class="col-xs-10 right no-border" style="padding-top: 10px;">
                            <span class="field-wp">
                                <asp:Panel ID="BtnSend" CssClass="btn-send" runat="server" ClientIDMode="Static">Hoàn Tất</asp:Panel>
                                <asp:Button ID="BtnFakeSend" runat="server" Text="Hoàn tất" 
                                    ClientIDMode="Static" OnClick="CompleteService" style="display:none;"></asp:Button>
                                <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>
                            </span>
                        </td>
                    </tr>
                    <asp:HiddenField runat="server" ID="UrlKeyField"  ClientIDMode="Static"/>
                    <asp:HiddenField runat="server" ID="ImgCoverField"  ClientIDMode="Static"/>
                </tbody>
            </table>

            <!-- input hidden -->
            <asp:HiddenField ID="HDF_CustomerCode" runat="server" ClientIDMode="Static"/>
            <asp:HiddenField ID="HDF_ProductIds" runat="server" ClientIDMode="Static"/>
            <asp:HiddenField ID="HDF_ServiceIds" runat="server" ClientIDMode="Static"/>
            <asp:HiddenField ID="HDF_TotalMoney" runat="server" ClientIDMode="Static"/>
            <asp:HiddenField ID="HDF_UserImages" runat="server" ClientIDMode="Static"/>
            <asp:HiddenField ID="HDF_UserImagesDelete" runat="server" ClientIDMode="Static"/>
            <asp:HiddenField ID="Hairdresser" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HairMassage" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="Cosmetic" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HDF_SalonId" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HDF_lgId" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HDF_BillId" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HDF_FreeService" ClientIDMode="Static" runat="server" />
            <asp:HiddenField ID="HDF_Rating" ClientIDMode="Static" runat="server" />
            <asp:HiddenField ID="HDF_Reception" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HDF_CustomerType" runat="server" ClientIDMode="Static" />
             <asp:HiddenField ID="HDF_DiscountServices" runat="server" ClientIDMode="Static" />
             <asp:HiddenField ID="HDF_DiscountCosmetic" runat="server" ClientIDMode="Static" />
            <!-- END input hidden -->
        </div>
    </div>
    <%-- end Add --%>
</div>

<!-- Danh mục sản phẩm -->
<div class="popup-product-wp popup-product-item">
    <div class="wp popup-product-head">
        <strong>Danh mục sản phẩm</strong>
    </div>
    <div class="wp popup-product-content" >
        <div class="wp popup-product-guide">
            <%--<div class="left">Hướng dẫn:</div>
            <div class="right">
                <p>- Click vào ô check box để chọn sản phẩm</p>
                <p>- Click vào ô số lượng để thay đổi số lượng sản phẩm</p>
            </div>--%>
            <label style="font-family: Roboto Condensed Bold; font-weight: normal; font-size: 14px; color: #222222; margin-right: 10px;">Chọn danh mục</label>
            <select onchange="showProductByCategory($(this))" id="ddlProductCategory" style="color: #222222; font-size: 14px; padding: 0 5px;">
                <asp:Repeater runat="server" ID="rptProductCategory">
                    <ItemTemplate>
                        <option value="<%# Eval("Id") %>" style="color: #222222; font-size: 14px;"><%# Eval("Name") %></option>
                    </ItemTemplate>
                </asp:Repeater>
            </select>
        </div>
        <div class="wp listing-product item-product">
            <table class="table" id="table-product">
                <thead>
                    <tr>
                        <th><input type="checkbox" /></th>
                        <th>STT</th>
                        <th>Tên sản phẩm</th>
                        <th>Mã sản phẩm</th>
                        <th>Đơn giá</th>
                        <th class="th-product-quantity">Số lượng</th>
                        <th>Giảm giá</th>
                        <th>Thành tiền</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="Rpt_Product" runat="server">
                        <ItemTemplate>
                            <tr data-cate="<%# Eval("CategoryId") %>">
                                <td class="td-product-checkbox item-product-checkbox">
                                    <input type="checkbox" value="<%# Eval("Id") %>"
                                        data-code="<%# Eval("Code") %>" data-name="<%# Eval("Name") %>"/>
                                </td>
                                <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                <td class="td-product-name"><%# Eval("Name") %></td>
                                <td class="td-product-code" data-id="<%# Eval("Id") %>" data-code="<%# Eval("Code") %>"><%# Eval("Code") %></td>
                                <td class="td-product-price" data-price="<%# Eval("Price") %>"><%# Eval("Price") %></td>
                                <td class="td-product-quantity">
                                    <input type="text" class="product-quantity form-control" value="1" />                                
                                </td>
                                <td class="td-product-voucher" data-voucher="<%# Eval("VoucherPercent") %>">
                                    <div class="row">
                                        <input type="text" class="product-voucher voucher-cosmetic form-control" value="<%# Eval("VoucherPercent") %>" style="margin: 0 auto; float: none; text-align: center;width: 50px;" /> %
                                    </div>
                                    <div class="row promotion-money hidden">
                                        <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                            <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                <input type="checkbox" class="item-promotion" data-value="35000" data-id="1" style="margin-left: 0;" /> - 35.000 VNĐ </label>

                                        </div><br />
                                        <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                            <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                <input type="checkbox" class="item-promotion" data-id="2" data-value="50000" style="margin-left: 0;" /> - 50.000 VNĐ </label>

                                        </div>
                                    </div>
                                </td>
                                <td class="map-edit">
                                    <div class="box-money"></div>
                                    <div class="edit-wp">
                                        <a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),'<%# Eval("Name") %>','product')"
                                             href="javascript://" title="Xóa"></a>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>                                        
                </tbody>
            </table>
        </div> 
        <div class="wp btn-wp">
            <div class="popup-product-btn btn-complete" data-item="product">Hoàn tất</div>
            <div class="popup-product-btn btn-esc">Thoát</div>
        </div>
    </div>    
</div>
<!-- END Danh mục sản phẩm -->

<!-- Danh mục dịch vụ -->
<div class="popup-product-wp popup-service-item">
    <div class="wp popup-product-head">
        <strong>Danh mục dịch vụ</strong>
    </div>
    <div class="wp popup-product-content" >
        <div class="wp popup-product-guide">
            <div class="left">Hướng dẫn:</div>
            <div class="right">
                <p>- Click vào ô check box để chọn dịch vụ</p>
                <p>- Click vào ô số lượng để thay đổi số lượng dịch vụ</p>
            </div>
        </div>
        <div class="wp listing-product item-product">
            <table class="table" id="table-service">
                <thead>
                    <tr>
                        <th><input type="checkbox" /></th>
                        <th>STT</th>
                        <th>Tên dịch vụ</th>
                        <th>Mã dịch vụ</th>
                        <th>Đơn giá</th>
                        <th class="th-product-quantity">Số lượng</th>
                        <th>Giảm giá</th>
                        <th>Thành tiền</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="Rpt_Service" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td class="td-product-checkbox item-product-checkbox">
                                    <input type="checkbox" value="<%# Eval("Id") %>"
                                        data-code="<%# Eval("Code") %>" data-name="<%# Eval("Name") %>"/>
                                </td>
                                <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                <td class="td-product-name"><%# Eval("Name") %></td>
                                <td class="td-product-code" data-id="<%# Eval("Id") %>" data-code="<%# Eval("Code") %>"><%# Eval("Code") %></td>
                                <td class="td-product-price" data-price="<%# Eval("Price") %>"><%# Eval("Price") %></td>
                                <td class="td-product-quantity">
                                    <input type="text" class="product-quantity form-control" value="1" />                                
                                </td>
                                <td class="td-product-voucher" data-voucher="<%# Eval("VoucherPercent") %>">
                                    <div class="row">
                                        <input type="text" class="product-voucher form-control" value="<%# Eval("VoucherPercent") %>" style="margin: 0 auto; float: none; text-align: center;width: 50px;" />
                                    </div>
                                    
                                </td>
                                <td class="map-edit">
                                    <div class="box-money"></div>
                                    <div class="edit-wp">
                                        <a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),'<%# Eval("Name") %>','service')"
                                            href="javascript://" title="Xóa"></a>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>                                        
                </tbody>
            </table>
        </div> 
        <div class="wp btn-wp">
            <div class="popup-product-btn btn-complete" data-item="service">Hoàn tất</div>
            <div class="popup-product-btn btn-esc">Thoát</div>
        </div>
    </div>    
</div>
<!-- END Danh mục dịch vụ -->

<!-- Popup voucher -->
<div class="popup-voucher">
    <div class="popup-voucher-content">
        <div class="row">
            <p class="voucher-p1">LỄ TÂN CHECKOUT CHÚ Ý PHÁT VOUCHER CHO KHÁCH HÀNG TRƯỚC KHI NHẬN TIỀN THANH TOÁN</p>
            <p class="voucher-p2"><span>(*) Lưu ý</span> : Công ty sẽ gọi điện kiểm tra ngẫu nhiên để đảm bảo tất cả Khách Hàng đều được nhận voucher</p>
        </div>
        <div class="row">
            <div class="yn-wp">
                <div class="yn-elm yn-no" onclick="autoCloseEBPopup();">OK</div>
            </div>
        </div>
    </div>
</div>
<script>
    function GetCustomerType(){    
        $.ajax({
            type: "POST",
            url: "/GUI/FrontEnd/Service/Service_Pending_Complete.aspx/GetSpecialCustomer",
            data: '{phone : "' + $("#HDF_CustomerCode").val() + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.d != null) {
                    $(".cusInfo").removeClass("hidden");
                    $("#HDF_CustomerType").val(response.d.CustomerTypeId);
                    $("#HDF_DiscountServices").val(response.d.DiscountServices);
                    $("#HDF_DiscountCosmetic").val(response.d.DiscountCosmetic);

                    var disService = $("#HDF_DiscountServices").val();
                    var disCosmetic = $("#HDF_DiscountCosmetic").val();
                    
                    if(response.d.CustomerTypeId == 1){
                        $(".pNote").removeClass('hidden');
                        $(".pInfo").removeClass('hidden');
                        $(".pExam").removeClass('hidden');
                        $(".pSpecCus").text("(*) " + response.d.TypeName);
                        $(".pNote").text("Lý do: " + response.d.ReasonDiff);
                        $(".pInfo").text("LỄ TÂN CHÚ Ý THÔNG BÁO FREE CHO KHÁCH");
                        $(".pExam").text("VD: Lần trước anh chưa hài lòng về dịch vụ bên em nên hôm nay chúng em mời anh sử dụng dịch vụ miễn phí ạ.");
                        $("#table-item-service tbody tr input.product-voucher").val(disService);
                        var quantity = parseInt($("#table-item-service tbody tr input.product-quantity").val());
                        var price = parseInt($("#table-item-service tbody tr .td-product-price").text());
                        var thanhtien = quantity * price * (100- parseInt(disService))/100;
                        $("#table-item-service tbody tr .box-money").text(thanhtien);
                        $(".voucher-cosmetic").val(disCosmetic);
                        TotalMoney();
                        getProductIds();
                        getServiceIds();
                        ExcQuantity();
                    }
                    else if (response.d.CustomerTypeId == 2){
                        $(".pNote").addClass('hidden');
                        $(".pInfo").addClass('hidden');
                        $(".pExam").addClass('hidden');
                        $(".pSpecCus").text("(*) " + response.d.TypeName +", lễ tân chú ý thu đúng số tiền hiển thị.");
                        $("#table-item-service tbody tr input.product-voucher").val(disService);
                        var quantity = parseInt($("#table-item-service tbody tr input.product-quantity").val());
                        var price = parseInt($("#table-item-service tbody tr .td-product-price").text());
                        var thanhtien = quantity * price * (100-disService)/100;
                        $("#table-item-service tbody tr .box-money").text(thanhtien);
                        $(".voucher-cosmetic").val(disCosmetic);
                        TotalMoney();
                        getProductIds();
                        getServiceIds();
                        ExcQuantity();
                    }
                }
                else{
                    $(".pSpecCus").addClass('hidden');
                    $(".pNote").addClass('hidden');
                    $(".pInfo").addClass('hidden');
                    $(".pExam").addClass('hidden');
                    $(".tblHistory").addClass("hidden");
                }
            },
            failure: function (response) { alert(response.d); }
        });
    }

   
    function openVoucher()
    {
        var salonID = $("#HDF_SalonId").val();
        if(salonID == "10")
        {
            //if(location.search == "")
            //{
                $(".popup-voucher").openEBPopup();   
            //}                
        }            
    }
    $(document).ready(function(){      
        openVoucher();
        GetCustomerType();
    });
    <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
</script>
<style>
    .popup-voucher { width: 510px; display:none; }
    .popup-voucher .popup-voucher-content { width: 100%; float: left; padding: 30px 30px; }
    .popup-voucher .voucher-p1 { text-align: left!important; font-size: 20px; line-height: 26px; }
    .popup-voucher .voucher-p2 { text-align: left!important; margin-top: 10px; }
    .popup-voucher .voucher-p2 span { font-family: Roboto Condensed Regular; font-style: italic; text-decoration: underline; color: red; }
    .popup-voucher  .yn-wp { text-align: center; }
    .popup-voucher .yn-wp .yn-elm { width: 60px; height: 30px; line-height: 30px; background: #dfdfdf; cursor: pointer; text-align: center; display: inline-block; margin-top: 15px; }
    .popup-voucher .yn-wp .yn-elm:hover { background: #50b347; color: #ffffff; }
</style>
<!--/ Popup voucher -->

</asp:Panel>
</asp:Content>

