﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Service_Add.aspx.cs" Inherits="_30shine.GUI.UIService.Service_Add" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ServiceAdd" ContentPlaceHolderID="CtMain" runat="server">
<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet"/>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>

<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">
                <li>Dịch vụ &nbsp;&#187; </li>
                <li class="li-listing"><a href="/dich-vu/danh-sach.html">Danh sách</a></li>
                <li class="li-pending"><a href="/dich-vu/pending.html"><div class="pending-1"></div>Pending</a></li>
                <%--<li class="li-pending"><a href="/dich-vu/pending-doi.html"><div class="pending-1"></div>Pending đợi</a></li>--%>
                <li class="li-add active"><a href="/dich-vu/them-phieu.html">Thêm mới</a></li>
                 <li class="li-add active"><a href="/dich-vu/dat-lich.html">Đặt lịch</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add admin-product-add fe-service">    
    <%-- Add --%>                
    <div class="wp960 content-wp">
        <!-- System Message -->
        <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
        <!-- END System Message -->

        <div class="table-wp" style="max-width: 960px;">
            <table class="table-add admin-product-table-add fe-service-table-add">
                <tbody>
                    <tr class="title-head">
                        <td><strong>Thông tin dịch vụ</strong></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr class="tr-margin" style="height: 20px;"></tr>

                    <tr class="tr-field-ahalf">
                        <td class="col-xs-2 left"><span>Khách hàng</span></td>
                        <td class="col-xs-10 right">
                            <div class="field-wp">
                                <asp:TextBox ID="CustomerName" CssClass="form-control" runat="server" ClientIDMode="Static" ReadOnly="true" style="width:190px; font-family: Roboto Condensed Bold; font-size: 15px;"></asp:TextBox>
                                <%--<asp:TextBox ID="CustomerCode" CssClass="mgl" placeholder="Nhập mã KH hoặc SĐT" runat="server" 
                                    ClientIDMode="Static" onchange="LoadCustomer(this.value)" style="width: 30%;"></asp:TextBox>--%>

                                <div class="filter-item" style="margin-left: 10px; z-index: 3000; width: 190px;">
                                    <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion form-control" data-field="customer.code" data-value="0"
                                        data-callback="Callback_UpdateCustomerCode" AutoCompleteType="Disabled" ID="CustomerCode" ClientIDMode="Static" 
                                        onchange="CustomerInputOnchange($(this))" placeholder="Nhập mã KH hoặc SĐT" runat="server" style="width: 25%;"></asp:TextBox>
                                    <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                                        <ul class="ul-listing-staff ul-listing-suggestion" id="Ul4"></ul>
                                    </div>
                                    <div class="fake-value"></div>
                                </div>

                                <div class="checkbox cus-no-infor" style="padding-top: 4px;">
                                    <label class="lbl-cus-no-infor">
                                        <input type="checkbox" id="InputCusNoInfor" runat="server" ClientIDMode="Static"/> Không cho thông tin
                                    </label>
                                </div>
                                <div class="addnew-customer" onclick="quickAddCustomer($(this))" style="width: 82px; display:none;"><i class="fa fa-plus-circle"></i>&nbsp;Thêm mới</div>
                                <%--<div class="addnew-customer" onclick="quickAddCustomer($(this))" style="width: 110px;"><i class="fa fa-plus-circle"></i>&nbsp;Đồng bộ vân tay</div>--%>
                                <%--<asp:RequiredFieldValidator ID="ValidateCustomerName" ControlToValidate="CustomerName" runat="server" 
                                    CssClass="fb-cover-error" Text="Bạn chưa nhập mã khách hàng!" ClientIDMode="Static"></asp:RequiredFieldValidator>--%>
                            </div>                            
                        </td>
                    </tr>

                    <tr class="tr-field-ahalf" id="TrIframeAddCustomer">
                        <td class="col-xs-2 left"><span></span></td>
                        <td class="col-xs-10 right">
                            <div class="field-wp">                                
                                <div class="iframe-add-customer" id="IframeAddCustomer">
                                    <iframe src="/khach-hang/them-moi-iframe.html"></iframe>
                                </div>
                                <%--<asp:RequiredFieldValidator ID="ValidateCustomerName" ControlToValidate="CustomerName" runat="server" 
                                    CssClass="fb-cover-error" Text="Bạn chưa nhập mã khách hàng!" ClientIDMode="Static"></asp:RequiredFieldValidator>--%>
                            </div>                            
                        </td>
                    </tr>

                    <tr class="tr-field-ahalf" runat="server" id="TrSalon" visible="false">
                        <td class="col-xs-2 left"><span>Salon</span></td>
                        <td class="col-xs-10 right">
                            <span class="field-wp">
                                <asp:DropDownList CssClass="form-control select" ID="Salon" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <asp:RequiredFieldValidator InitialValue="0" 
                                    ID="ValidateSalon" Display="Dynamic" 
                                    ControlToValidate="Salon"
                                    runat="server"  Text="Bạn chưa chọn Salon!" 
                                    ErrorMessage="Vui lòng chọn Salon!"
                                    ForeColor="Red"
                                    CssClass="fb-cover-error">
                                </asp:RequiredFieldValidator>
                            </span>
                        </td>
                    </tr>

                    <tr class="tr-send">
                        <td class="col-xs-2 left"><span>Chọn team</span></td>
                        <td class="col-xs-10 right">
                            <div class="field-wp">
                                <div class="team-color-select" runat="server" id="TeamColorSelect" ClientIDMode="Static"></div>
                                <asp:Repeater runat="server" ID="Rpt_TeamWork">
                                    <ItemTemplate>
                                        <div class="item-color" style="background: <%# Eval("Color")%> ;" onclick="setTeamColor($(this),<%# Eval("Id") %>,'<%# Eval("Color") %>')"></div>
                                    </ItemTemplate>
                                </asp:Repeater>                                
                                <%--<div class="item-color" style="background: #2771C3;" onclick="setTeamColor($(this),2)"></div>
                                <div class="item-color" style="background: #F40000;" onclick="setTeamColor($(this),3)"></div>
                                <div class="item-color" style="background: #36B04B;" onclick="setTeamColor($(this),4)"></div>
                                <div class="item-color" style="background: #7DFF5C;" onclick="setTeamColor($(this),5)"></div>
                                <div class="item-color" style="background: #57FFFF;" onclick="setTeamColor($(this),6)"></div>
                                <div class="item-color" style="background: #6D31A2;" onclick="setTeamColor($(this),7)"></div>
                                <div class="item-color" style="background: #D9D9D9;" onclick="setTeamColor($(this),8)"></div>--%>
                            </div>
                        </td>
                    </tr>
                    
                    <tr class="tr-send">
                        <td class="col-xs-2 left"></td>
                        <td class="col-xs-10 right no-border">
                            <span class="field-wp">
                                <asp:Panel ID="BtnSend" CssClass="btn-send" runat="server" ClientIDMode="Static">In phiếu</asp:Panel>
                                <asp:Button ID="BtnFakeSend" runat="server" Text="Hoàn tất" 
                                    ClientIDMode="Static" OnClick="AddService" style="display:none;"></asp:Button>
                                <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>
                            </span>
                        </td>
                    </tr>

                    <asp:HiddenField runat="server" ID="UrlKeyField"  ClientIDMode="Static"/>
                    <asp:HiddenField runat="server" ID="ImgCoverField"  ClientIDMode="Static"/>
                </tbody>
            </table>

            <!-- input hidden -->
            <asp:HiddenField ID="HDF_CustomerCode" runat="server" ClientIDMode="Static"/>
            <asp:HiddenField ID="HDF_ProductIds" runat="server" ClientIDMode="Static"/>
            <asp:HiddenField ID="HDF_ServiceIds" runat="server" ClientIDMode="Static"/>
            <asp:HiddenField ID="HDF_TotalMoney" runat="server" ClientIDMode="Static"/>
            <asp:HiddenField ID="HDF_UserImages" runat="server" ClientIDMode="Static"/>
            <asp:HiddenField ID="HDF_UserImagesDelete" runat="server" ClientIDMode="Static"/>
            <asp:HiddenField ID="Hairdresser" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HairMassage" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="Cosmetic" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HDF_SalonId" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HDF_Suggestion_Code" ClientIDMode="Static" runat="server" />
            <asp:HiddenField ID="HDF_Suggestion_Field" ClientIDMode="Static" runat="server" />
            <asp:HiddenField ID="HDF_FreeService" ClientIDMode="Static" runat="server" />
            <asp:HiddenField ID="HDF_TeamId" ClientIDMode="Static" runat="server" />
            <!-- END input hidden -->
        </div>
    </div>
    <%-- end Add --%>
</div>

<!-- Danh mục sản phẩm -->
<div class="popup-product-wp popup-product-item">
    <div class="wp popup-product-head">
        <strong>Danh mục sản phẩm</strong>
    </div>
    <div class="wp popup-product-content" >
        <div class="wp popup-product-guide">
            <div class="left">Hướng dẫn:</div>
            <div class="right">
                <p>- Click vào ô check box để chọn sản phẩm</p>
                <p>- Click vào ô số lượng để thay đổi số lượng sản phẩm</p>
            </div>
        </div>
        <div class="wp listing-product item-product">
            <table class="table" id="table-product">
                <thead>
                    <tr>
                        <th><input type="checkbox" /></th>
                        <th>STT</th>
                        <th>Tên sản phẩm</th>
                        <th>Mã sản phẩm</th>
                        <th>Đơn giá</th>
                        <th class="th-product-quantity">Số lượng</th>
                        <th>Giảm giá (%)</th>
                        <th>Thành tiền</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="Rpt_Product" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td class="td-product-checkbox item-product-checkbox">
                                    <input type="checkbox" value="<%# Eval("Id") %>"
                                        data-code="<%# Eval("Code") %>" data-name="<%# Eval("Name") %>"/>
                                </td>
                                <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                <td class="td-product-name"><%# Eval("Name") %></td>
                                <td class="td-product-code" data-id="<%# Eval("Id") %>" data-code="<%# Eval("Code") %>"><%# Eval("Code") %></td>
                                <td class="td-product-price" data-price="  <%# Eval("Price") %>"><%# Eval("Price") %></td>
                                <td class="td-product-quantity">
                                    <input type="text" class="product-quantity form-control" value="1" />                                
                                </td>
                                <td class="td-product-voucher" data-voucher="<%# Eval("VoucherPercent") %>">
                                    <div class="row">
                                        <input type="text" class="product-voucher form-control" value="<%# Eval("VoucherPercent") %>" style="margin: 0 auto; float: none; text-align: center;width: 50px;" /> %
                                    </div>
                                    <div class="row promotion-money">
                                        <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                            <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                <input type="checkbox" class="item-promotion" onclick="excPromotion($(this), 35000)" data-value="35000" data-id="1" style="margin-left: 0;" /> - 35.000 VNĐ </label>

                                        </div><br />
                                        <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                            <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                <input type="checkbox" class="item-promotion" onclick="excPromotion($(this), 50000)" data-id="2" data-value="50000" style="margin-left: 0;" /> - 50.000 VNĐ </label>

                                        </div>
                                    </div>
                                </td>
                                <td class="map-edit">
                                    <div class="box-money"></div>
                                    <div class="edit-wp">
                                        <a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),'<%# Eval("Name") %>','product')"
                                             href="javascript://" title="Xóa"></a>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>                                        
                </tbody>
            </table>
        </div> 
        <div class="wp btn-wp">
            <div class="popup-product-btn btn-complete" data-item="product">Hoàn tất</div>
            <div class="popup-product-btn btn-esc">Thoát</div>
        </div>
    </div>    
</div>
<!-- END Danh mục sản phẩm -->

<!-- Danh mục dịch vụ -->
<div class="popup-product-wp popup-service-item">
    <div class="wp popup-product-head">
        <strong>Danh mục dịch vụ</strong>
    </div>
    <div class="wp popup-product-content" >
        <div class="wp popup-product-guide">
            <div class="left">Hướng dẫn:</div>
            <div class="right">
                <p>- Click vào ô check box để chọn dịch vụ</p>
                <p>- Click vào ô số lượng để thay đổi số lượng dịch vụ</p>
            </div>
        </div>
        <div class="wp listing-product item-product">
            <table class="table" id="table-service">
                <thead>
                    <tr>
                        <th><input type="checkbox" /></th>
                        <th>STT</th>
                        <th>Tên dịch vụ</th>
                        <th>Mã dịch vụ</th>
                        <th>Đơn giá</th>
                        <th class="th-product-quantity">Số lượng</th>
                        <th>Giảm giá (%)</th>
                        <th>Thành tiền</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="Rpt_Service" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td class="td-product-checkbox item-product-checkbox">
                                    <input type="checkbox" value="<%# Eval("Id") %>"
                                        data-code="<%# Eval("Code") %>" data-name="<%# Eval("Name") %>"/>
                                </td>
                                <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                <td class="td-product-name"><%# Eval("Name") %></td>
                                <td class="td-product-code" data-id="<%# Eval("Id") %>" data-code="<%# Eval("Code") %>"><%# Eval("Code") %></td>
                                <td class="td-product-price" data-price="   <%# Eval("Price") %>"><%# Eval("Price") %></td>
                                <td class="td-product-quantity">
                                    <input type="text" class="product-quantity form-control" value="1" />                                
                                </td>
                                <td class="td-product-voucher" data-voucher="<%# Eval("VoucherPercent") %>">
                                    <div class="row">
                                        <input type="text" class="product-voucher form-control" value="<%# Eval("VoucherPercent") %>" style="margin: 0 auto; float: none; text-align: center; width: 50px" /> %
                                    </div>
                                    <%--<div class="row">
                                        <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                            <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                <input type="checkbox" onclick="excPromotion($(this), -35)" data-id="<%# Eval("Id") %>" style="margin-left: 0;" /> - 35.000 VNĐ </label>

                                        </div><br />
                                        <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                            <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                <input type="checkbox" onclick="excPromotion($(this), -50)" data-id="<%# Eval("Id") %>" style="margin-left: 0;" /> - 50.000 VNĐ </label>

                                        </div>
                                    </div>--%>
                                </td>
                                <td class="map-edit">
                                    <div class="box-money"></div>
                                    <div class="edit-wp">
                                        <a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),'<%# Eval("Name") %>','service')"
                                            href="javascript://" title="Xóa"></a>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>                                        
                </tbody>
            </table>
        </div> 
        <div class="wp btn-wp">
            <div class="popup-product-btn btn-complete" data-item="service">Hoàn tất</div>
            <div class="popup-product-btn btn-esc">Thoát</div>
        </div>
    </div>    
</div>
<!-- END Danh mục dịch vụ -->

<script src="/assets/js/jquery.mCustomScrollbar.js"></script>
<link href="/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
    <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
<script type="text/javascript">
    jQuery(document).ready(function () {
        // Add active menu
        $("#glbService").addClass("active");

        // Mở box listing sản phẩm
        $(".show-product").bind("click", function () {
            $(this).parent().parent().find(".listing-product").show();
        });

        $(".show-item").bind("click", function () {
            var item = $(this).attr("data-item");
            var classItem = ".popup-" + item + "-item";

            $(classItem).openEBPopup();
            ExcCheckboxItem();
            ExcQuantity();
            ExcCompletePopup();

            $('#EBPopup .listing-product').mCustomScrollbar({
                theme: "dark-2",
                scrollInertia: 100
            });

            $("#EBPopup .btn-esc").bind("click", function () { autoCloseEBPopup(0); });
        });
        
        // Btn Send
        $("#BtnSend").bind("click", function () {
            if ($("#CustomerName").val() == "" && $("#InputCusNoInfor").is(":checked") == false) {
                showMsgSystem("Bạn chưa nhập mã khách hàng hoặc chưa tích chọn \"Khách không cho thông tin\".", "warning");
            //} else if ($("#HDF_TotalMoney").val() == "") {
            //    showMsgSystem("Bạn chưa chọn dịch vụ hoặc sản phẩm.", "warning");
            } else {
                // Check cosmetic seller
                if (($("#HDF_ProductIds").val() != "" && $("#HDF_ProductIds").val() != "[]") && $("#Cosmetic").val() == 0) {
                    $("#ValidateCosmetic").css({ "visibility": "visible" });
                } else {
                    $("#ValidateCosmetic").css({ "visibility": "hidden" });
                    var CustomerCode = $("#HDF_CustomerCode").val();
                    // Check duplicate bill
                    $.ajax({
                        type: "POST",
                        url: "/GUI/FrontEnd/Service/Service_Add.aspx/CheckDuplicateBill",
                        data: '{CustomerCode : "' + CustomerCode + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            var mission = JSON.parse(response.d);
                            if (mission.success) {
                                var products = "Sản phẩm : ";
                                var services = "Dịch vụ : ";
                                var confirm_content = "";
                                var bill = JSON.parse(mission.msg);
                                if (bill.ProductIds != "") {
                                    var _Product = $.parseJSON(bill.ProductIds);
                                    $.each(_Product, function (i, v) {
                                        products += v.Name + " ( " + v.Price + " ), ";
                                    });
                                    products = products.replace(/(\,\s)+$/, '');
                                }
                                if (bill.ServiceIds != "") {
                                    var _Service = $.parseJSON(bill.ServiceIds);
                                    $.each(_Service, function (i, v) {
                                        services += v.Name + " ( " + v.Price + " ), ";
                                    });
                                    services = services.replace(/(\,\s)+$/, '');
                                }
                                confirm_content += products + ", " + services + ", ";
                                confirm_content = confirm_content.replace(/(\,\s)+$/, '');

                                // show EBPopup
                                $(".confirm-yn").openEBPopup();
                                $("#EBPopup .confirm-yn-text").text("Khách hàng mã [" + CustomerCode + "] đã được lập hóa đơn trong hôm nay [ " + confirm_content + " ]. Bạn có chắc muốn thêm hóa đơn mới ?");

                                $("#EBPopup .yn-yes").bind("click", function () {
                                    //Bind_UserImagesToHDF();
                                    addLoading();
                                    $("#BtnFakeSend").click();
                                });
                                $("#EBPopup .yn-no").bind("click", function () {
                                    autoCloseEBPopup(0);
                                });
                            } else {
                                //Bind_UserImagesToHDF();
                                addLoading();
                                $("#BtnFakeSend").click();
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    });
                    //Bind_UserImagesToHDF();
                    //$("#BtnFakeSend").click();
                }
            }                        
        });

        // Bind staff suggestion
        Bind_Suggestion();
        AutoSelectStaff();

        /// Mở luôn iframe thêm mới khách hàng khi vào trang
        quickAddCustomer();
        $("#CustomerCode").focus();

        //============================
        // Show System Message
        //============================ 
        var qs = getQueryStrings();
        showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);

        // Enter don't submit
        $(window).on('keyup keypress', function (e) {
            var keyCode = e.keyCode || e.which;
            if (keyCode === 13) {
                e.preventDefault();
                return false;
            }
        });
    });

    // Xử lý mã khách hàng => bind khách hàng
    function LoadCustomer(code) {
        ajaxGetCustomer(code);
    }

    // Xử lý khi chọn checkbox sản phẩm
    function ExcCheckboxItem() {
        $("#EBPopup .td-product-checkbox input[type='checkbox']").unbind("click").bind("click", function () {
            var obj = $(this).parent().parent(),
                boxMoney = obj.find(".box-money"),
                price = obj.find(".td-product-price").data("price"),
                quantity = obj.find("input.product-quantity").val(),
                voucher = obj.find("input.product-voucher").val(),
                checked = $(this).is(":checked"),
                item = obj.attr("data-item"),
                promotion = 0;
            obj.find(".promotion-money input[type='checkbox']:checked").each(function () {
                var value = $(this).attr("data-value").trim();
                promotion += (value != "" ? parseInt(value) : 0);
            });

            // check value
            price = price.toString().trim() != "" ? parseInt(price) : 0;
            quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
            voucher = voucher.trim() != "" ? parseInt(voucher) : 0;
            

            if (checked) {
                boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100 - promotion)).show();
            } else {
                boxMoney.text("").hide();
            }
            
        });
    }

    // Số lượng | Quantity
    function ExcQuantity() {
        $("input.product-quantity").bind("change", function () {
            var obj = $(this).parent().parent(),
                quantity = $(this).val(),
                price = obj.find(".td-product-price").data("price"),
                voucher = obj.find("input.product-voucher").val(),
                boxMoney = obj.find(".box-money"),
                promotion = 0;

            obj.find(".promotion-money input[type='checkbox']:checked").each(function () {
                var value = $(this).attr("data-value").trim();
                promotion += (value != "" ? parseInt(value) : 0);
            });

            // check value
            price = price.toString().trim() != "" ? parseInt(price) : 0;
            quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
            voucher = voucher.trim() != "" ? parseInt(voucher) : 0;

            boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100 - promotion)).show();
            TotalMoney();
            getProductIds();
            getServiceIds();
        });
        $("input.product-voucher").bind("change", function () {
            var obj = $(this).parent().parent().parent(),
                quantity = obj.find("input.product-quantity").val(),
                price = obj.find(".td-product-price").data("price"),
                voucher = obj.find("input.product-voucher").val(),
                boxMoney = obj.find(".box-money"),
                promotion = 0;

            obj.find(".promotion-money input[type='checkbox']:checked").each(function () {
                var value = $(this).attr("data-value").trim();
                promotion += (value != "" ? parseInt(value) : 0);
            });

            // check value
            price = price.toString().trim() != "" ? parseInt(price) : 0;
            quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
            voucher = voucher.trim() != "" ? parseInt(voucher) : 0;

            if (promotion == 0) {
                boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100)).show();
            } else {
                obj.find("input.product-voucher").val(0);
            }
            
            TotalMoney();
            getProductIds();
            getServiceIds();
        });
        $("input.item-promotion").bind("click", function () {
            var obj = $(this).parent().parent().parent().parent().parent(),
                quantity = obj.find("input.product-quantity").val(),
                price = obj.find(".td-product-price").data("price"),
                voucher = obj.find("input.product-voucher").val(),
                boxMoney = obj.find(".box-money"),
                promotion = $(this).attr("data-value"),
                _This = $(this),
                isChecked = $(this).is(":checked");            
            obj.find(".promotion-money input[type='checkbox']:checked").prop("checked", false);
            if (isChecked) {
                $(this).prop("checked", true);
                promotion = promotion.trim() != "" ? parseInt(promotion) : 0;
                obj.find("input.product-voucher").val(0);
            } else {
                promotion = 0;
            }

            // check value
            price = price.toString().trim() != "" ? parseInt(price) : 0;
            quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
            voucher = voucher.trim() != "" ? parseInt(voucher) : 0;
            //promotion = promotion.trim() != "" ? parseInt(promotion) : 0;

            if (promotion > 0) {
                boxMoney.text(FormatPrice(quantity * price - promotion)).show();
                obj.find("input.product-voucher").css({"text-decoration": "line-through"});
            } else {
                boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100)).show();
                obj.find("input.product-voucher").css({ "text-decoration": "none" });
            }
            TotalMoney();
            getProductIds();
            getServiceIds();
        });        
    }

    // Xử lý click hoàn tất chọn item từ popup
    function ExcCompletePopup() {
        $("#EBPopup .btn-complete").unbind("click").bind("click", function () {
            var item = $(this).attr("data-item"),
                tableItem = $("table.table-item-" + item + " tbody"),
                objTmp;

            $("#EBPopup .item-product-checkbox input[type='checkbox']:checked").each(function () {
                var Code = $(this).attr("data-code");
                if (!ExcCheckItemIsChose(Code,tableItem)) {
                    objTmp = $(this).parent().parent().clone().find("td:first-child").remove().end();
                    tableItem.append(objTmp);
                }                
            });

            TotalMoney();
            getProductIds();
            getServiceIds();
            ExcQuantity();
            UpdateItemOrder(tableItem);
            autoCloseEBPopup(0);
        });
    }

    // Check item đã được chọn
    function ExcCheckItemIsChose(Code, itemClass) {
        var result = false;
        $(itemClass).find(".td-product-code").each(function () {
            var _Code = $(this).text().trim();
            if (_Code == Code)
                result = true;            
        });
        return result;
    }

    // Remove item đã được chọn
    function RemoveItem(THIS, name, itemName) {
        // Confirm
        $(".confirm-yn").openEBPopup();

        $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn hủy [ " + name + " ] ?");
        $("#EBPopup .yn-yes").bind("click", function () {
            var Code = THIS.find(".td-product-code").attr("data-code");
            $("#quick-" + itemName).find("input[data-code='" + Code + "']").removeAttr("checked");
            THIS.remove();
            TotalMoney();
            getProductIds();
            getServiceIds();
            UpdateItemDisplay(itemName);
            autoCloseEBPopup(0);
        });
        $("#EBPopup .yn-no").bind("click", function () {
            autoCloseEBPopup(0);
        });        
    }

    // Update table display
    function UpdateItemDisplay(itemName) {
        var dom = $(".table-item-" + itemName);
        var len = dom.find("tbody tr").length;
        if (len == 0) {
            dom.parent().hide();
        } else {
            UpdateItemOrder(dom.find("tbody"));
        }        
    }

    // Update order item
    function UpdateItemOrder(dom) {
        var index = 1;
        dom.find("tr").each(function () {
            $(this).find("td.td-product-index").text(index);
            index++;
        });
    }

    function TotalMoney() {
        var Money = 0;
        $(".fe-service-table-add .box-money:visible").each(function () {
            Money += parseInt($(this).text().replace(/\./gm, ""));
        });
        $("#TotalMoney").val(FormatPrice(Money));
        $("#HDF_TotalMoney").val(Money);
    }

    function showMsgSystem(msg, status) {
        $("#MsgSystem").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);
        setTimeout(function () {
            $("#MsgSystem").fadeTo("slow", 0, function () {
                $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 1);
            });
        }, 5000);
    }

    function getProductIds() {
        var Ids = [];
        var prd = {};
        $("table.table-item-product tbody tr").each(function () {
            prd = {};
            var THIS = $(this);

            var Id = THIS.find("td.td-product-code").attr("data-id"),
                Code = THIS.find("td.td-product-code").text().trim(),
                Name = THIS.find("td.td-product-name").text().trim(),
                Price = THIS.find(".td-product-price").attr("data-price"),
                Quantity = THIS.find("input.product-quantity").val(),
                VoucherPercent = THIS.find("input.product-voucher").val(),
                Promotion = 0;

            THIS.find(".promotion-money input[type='checkbox']:checked").each(function () {
                var value = $(this).attr("data-value").trim();
                Promotion += (value != "" ? parseInt(value) : 0);
            });

            // check value
            Id = Id.toString().trim() != "" ? parseInt(Id) : 0;
            Price = Price.toString().trim() != "" ? parseInt(Price) : 0;
            Quantity = Quantity.trim() != "" ? parseInt(Quantity) : 0;
            VoucherPercent = VoucherPercent.trim() != "" ? parseInt(VoucherPercent) : 0;

            prd.Id = Id;
            prd.Code = Code;
            prd.Name = Name;
            prd.Price = Price;
            prd.Quantity = Quantity;
            prd.VoucherPercent = VoucherPercent;
            prd.Promotion = Promotion;

            Ids.push(prd);

        });
        Ids = JSON.stringify(Ids);
        $("#HDF_ProductIds").val(Ids);
    }

    function getServiceIds() {
        var Ids = [];
        var prd = {};
        $("table.table-item-service tbody tr").each(function () {
            prd = {};
            var THIS = $(this),
                obj = THIS.parent().parent();

            var Id = THIS.find("td.td-product-code").attr("data-id"),
                Code = THIS.find("td.td-product-code").text().trim(),
                Name = THIS.find("td.td-product-name").text().trim(),
                Price = THIS.find(".td-product-price").attr("data-price"),
                Quantity = THIS.find("input.product-quantity").val(),
                VoucherPercent = THIS.find("input.product-voucher").val();

            // check value
            Id = Id.toString().trim() != "" ? parseInt(Id) : 0;
            Price = Price.toString().trim() != "" ? parseInt(Price) : 0;
            Quantity = Quantity.trim() != "" ? parseInt(Quantity) : 0;
            VoucherPercent = VoucherPercent.trim() != "" ? parseInt(VoucherPercent) : 0;

            prd.Id = Id;
            prd.Code = Code;
            prd.Name = Name;
            prd.Price = Price;
            prd.Quantity = Quantity;
            prd.VoucherPercent = VoucherPercent;

            Ids.push(prd);

        });
        Ids = JSON.stringify(Ids);
        $("#HDF_ServiceIds").val(Ids);
    }
    
    // Get Customer
    function ajaxGetCustomer(CustomerCode) {
        $.ajax({
            type: "POST",
            url: "/GUI/FrontEnd/Service/Service_Add.aspx/GetCustomer",
            data: '{CustomerCode : "' + CustomerCode + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                var mission = JSON.parse(response.d);
                if (mission.success) {
                    var customer = JSON.parse(mission.msg);
                    $("#CustomerName").val(customer.Fullname);
                    $("#CustomerName").attr("data-code", customer.Customer_Code);
                    $("#HDF_CustomerCode").val(customer.Customer_Code);
                    $("#HDF_Suggestion_Code").val(customer.Customer_Code);
                } else {
                    $("#CustomerName").val("");
                    $("#CustomerName").attr("data-code", "");
                    $("#HDF_CustomerCode").val("");
                    $("#HDF_Suggestion_Code").val("");
                    //var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                    //showMsgSystem(msg, "warning");
                }
            },
            failure: function (response) { alert(response.d); }
        });
    }

    function pushQuickData(This, typeData) {
        var Code = This.attr("data-code");
        if (This.is(":checked")) {
            var Dom = $("#table-" + typeData).find("input[data-code='" + Code + "']").parent().parent().clone().find("td:first-child").remove().end(),
            quantity = Dom.find(".product-quantity").val(),
            price = Dom.find(".td-product-price").data("price"),
            voucher = Dom.find("input.product-voucher").val(),
            promotion = 0;

            Dom.find(".promotion-money input[type='checkbox']:checked").each(function () {
                var value = $(this).attr("data-value").trim();
                promotion += (value != "" ? parseInt(value) : 0);
            });

            // check value
            price = price.toString().trim() != "" ? parseInt(price) : 0;
            quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
            voucher = voucher.trim() != "" ? parseInt(voucher) : 0;

            Dom.find(".box-money").text(FormatPrice(quantity * price * (100 - voucher) / 100 - promotion)).show();
            $("#table-item-" + typeData).append(Dom);
            $(".item-" + typeData).show();
        } else {
            $("#table-item-" + typeData).find(".td-product-code[data-code='" + Code + "']").parent().remove();
        }

        TotalMoney();
        getProductIds();
        getServiceIds();
        ExcQuantity();
        UpdateItemOrder($("#table-item-" + typeData).find("tbody"));
    }

    function pushFreeService(This, id) {
        var arr = [];
        var sv = {};
        $(".free-service input[type='checkbox']").each(function(){
            if($(this).is(":checked")){
                arr.push(parseInt($(this).attr('data-id')));
            }
        });
        $("#HDF_FreeService").val(JSON.stringify(arr));
    }

    function excPromotion(This, money) {
        //promotion-money
        //if (This.is(":checked")) {
        //    $(".fe-service .promotion-money input[type='checkbox']").prop("checked", false);
        //} else {
        //    $(".fe-service .promotion-money input[type='checkbox']").prop("checked", false);
        //    This.prop("checked", true);
        //}

        //TotalMoney();
        //getProductIds();
        //getServiceIds();
        //ExcQuantity();
        //UpdateItemOrder($("#table-item-" + typeData).find("tbody"));
    }

    //============================
    // Suggestion Functions
    //============================
    function Bind_Suggestion() {
        $(".eb-suggestion").bind("keyup", function (e) {
            if (e.keyCode == 40) {
                UpDownListSuggest($(this));
            } else {
                Call_Suggestion($(this));
            }
        });
        $(".eb-suggestion").bind("focus", function () {
            Call_Suggestion($(this));
        });
        $(".eb-suggestion").bind("blur", function () {
            //Exc_To_Reset_Suggestion($(this));
        });
        $(window).bind("click", function (e) {
            if ((!e.target.className.match("eb-select") && e.target.parentElement.className.match("ul-listing-suggestion")) ||
                (!e.target.className.match("eb-select") && !e.target.parentElement.className.match("ul-listing-suggestion"))) {
                EBSelect_HideBox();
            }
        });
    }

    function UpDownListSuggest(This) {
        var UlSgt = This.parent().find(".ul-listing-suggestion"),
            index = 0,
            LisLen = UlSgt.find(">li").length - 1,
            Value;

        This.blur();
        UlSgt.find(">li.active").removeClass("active");
        UlSgt.find(">li:eq(" + index + ")").addClass("active");

        $(window).unbind("keydown").bind("keydown", function (e) {
            if (e.keyCode == 40) {
                if (index == LisLen) return false;
                UlSgt.find(">li.active").removeClass("active");
                UlSgt.find(">li:eq(" + (++index) + ")").addClass("active");
                return false;
            } else if (e.keyCode == 38) {
                if (index == 0) return false;
                UlSgt.find(">li.active").removeClass("active");
                UlSgt.find(">li:eq(" + (--index) + ")").addClass("active");
                return false;
            } else if (e.keyCode == 13) {
                // Bind data to HDF Field
                var THIS = UlSgt.find(">li.active");
                //var Value = THIS.text().trim();
                var Value = THIS.attr("data-code");
                var dataField = This.attr("data-field");

                BindIdToHDF(THIS, Value, dataField, "#HDF_Suggestion_Code", "#HDF_Suggestion_Field", This);
                EBSelect_HideBox();
            }
        });
    }

    function Exc_To_Reset_Suggestion(This) {
        var value = This.val();
        if (value == "") {
            $(".eb-suggestion").each(function () {
                var THIS = $(this);
                var sgValue = THIS.val();
                if (sgValue != "") {
                    BindIdToHDF(THIS, sgValue, THIS.attr("data-field"), "#HDF_Suggestion_Code", "#HDF_Suggestion_Field", THIS);
                    return false;
                }
            });
        }
    }

    function Call_Suggestion(This) {        
        var text = This.val(),
            field = This.attr("data-field");
        Suggestion(This, text, field);
    }

    function Suggestion(This, text, field) {
        var This = This;
        var text = text || "";
        var field = field || "";
        var InputDomId;
        var HDF_Sgt_Code = "#HDF_Suggestion_Code";
        var HDF_Sgt_Field = "#HDF_Suggestion_Field";
        var Callback = This.attr("data-callback");

        if (text == "") return false;

        switch (field) {
            case "customer.name": InputDomId = "#CustomerName"; break;
            case "customer.phone": InputDomId = "#CustomerPhone"; break;
            case "customer.code": InputDomId = "#CustomerCode"; break;
            case "bill.code": InputDomId = "#BillCode"; break;
        }

        $.ajax({
            type: "POST",
            url: "/GUI/SystemService/Ajax/Suggestion.aspx/Suggestion_Customer",
            data: '{field : "' + field + '", text : "' + text + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                var mission = JSON.parse(response.d);
                if (mission.success) {
                    var OBJ = JSON.parse(mission.msg);
                    if (OBJ.length > 0) {
                        var lis = "";
                        $.each(OBJ, function (i, v) {
                            lis += "<li data-code='" + v.Customer_Code + "'" +
                                         "onclick=\"BindIdToHDF($(this),'" + v.Customer_Code + "','" + field + "','" + HDF_Sgt_Code +
                                         "','" + HDF_Sgt_Field + "','" + InputDomId + "')\" data-callback='" + Callback + "'>" +
                                        v.Value +
                                    "</li>";
                        });
                        This.parent().find("ul.ul-listing-suggestion").empty().append(lis).parent().show();
                        This.parent().find(".eb-select-data").show();
                    } else {
                        This.parent().find(".eb-select-data").hide().find("ul.ul-listing-suggestion").empty();
                    }
                } else {
                    This.parent().find("ul.ul-listing-suggestion").empty();
                    var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                    showMsgSystem(msg, "warning");
                }
            },
            failure: function (response) { alert(response.d); }
        });
    }

    function BindIdToHDF(THIS, Code, Field, HDF_Sgt_Code, HDF_Sgt_Field, Input_DomId) {
        var text = THIS.text().trim();
        $("input.eb-suggestion").val("");
        $(HDF_Sgt_Code).val(Code);
        $(HDF_Sgt_Field).val(Field);
        $(Input_DomId).val(text);
        $(Input_DomId).parent().find(".eb-select-data").hide();
        if (THIS.attr("data-callback") != "") {
            window[THIS.attr("data-callback")].call();
        }
        // Auto post server
        $("#BtnFakeUP").click();
    }

    function EBSelect_HideBox() {
        $(".eb-select-data").hide();
        $("ul.ul-listing-suggestion li.active").removeClass("active");
    }

    function Callback_UpdateCustomerCode() {
        LoadCustomer($("#HDF_Suggestion_Code").val());
    }

    function CustomerInputOnchange(This) {
        ajaxGetCustomer(This.val());
    }

    //===================
    // Auto select staff
    //===================
    function AutoSelectStaff() {
        $(".auto-select").bind("keyup", function (e) {
            var This = $(this);
            var StaffType = This.attr("data-field");
            var code = parseInt(This.val());
            if (!isNaN(code)) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Ajax/Suggestion.aspx/Suggestion_OneStaff",
                    data: '{code : "' + code + '", SalonId : ' + $("#HDF_SalonId").val() + ', StaffType : "' + StaffType + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            var OBJ = JSON.parse(mission.msg);
                            if (OBJ.length > 0) {
                                var lis = "";
                                This.parent().find(".fake-value").text(This.val() + "- " + OBJ[0].Fullname);
                                $("#" + StaffType).val(OBJ[0].Id);
                            } else {
                                This.parent().find(".fake-value").text("");
                                $("#" + StaffType).val("");
                            }
                        } else {
                            This.parent().find("ul.ul-listing-suggestion").empty();
                            var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                            showMsgSystem(msg, "warning");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            } else {
                This.parent().find(".fake-value").text("");
                $("#" + StaffType).val("");
            }
        });
    }

    function quickAddCustomer(This) {
        var display = $("#TrIframeAddCustomer").css("display");
        if (display == "none") {
            $("#TrIframeAddCustomer").show();
        }
        $("#IframeAddCustomer").toggle(0, function () {
            if (display == "table-row") {
                $("#TrIframeAddCustomer").hide();
            }            
        });
    }

    window.callback_QuickAddCustomer = function (id, code, name) {
        $("#CustomerCode").val(code);
        $("#CustomerName").val(name);
        $("#HDF_CustomerCode").val(code);
        $("#HDF_Suggestion_Code").val(code);
    }

    window.callback_HideIframeCustomer = function () {
        //$("#IframeAddCustomer").toggle(0, function () {
        //    $("#TrIframeAddCustomer").hide();
        //});
    }

    /// Chọn team
    function setTeamColor(This, idTeam, color) {
        $(".item-color.active").removeClass("active");
        This.addClass("active");
        $("#HDF_TeamId").val(idTeam);
        // bind color to .team-color-select
        $(".team-color-select").css({"background" : color});
    }

</script>

<!-- In hóa đơn -->
<iframe id="iframePrint" style="display:none;"></iframe>
<script type="text/javascript">
    jQuery(document).ready(function () {
        var qs = getQueryStrings();
        if (qs["msg_print_billcode"] != undefined) {
            openPdfIframe("/Public/PDF/" + qs["msg_print_billcode"] + ".pdf");
        }
        showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);
    });
    function openPdfIframe(src) {
        var PDF = document.getElementById("iframePrint");
        PDF.src = src;
        PDF.onload = function () {
            PDF.focus();
            PDF.contentWindow.print();
            PDF.contentWindow.close();
        }
    }
</script>
<!--/ In hóa đơn -->

</asp:Panel>
</asp:Content>

