﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Service_Add_V3.aspx.cs" Inherits="_30shine.GUI.FrontEnd.Service.Service_Add_V3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="/assets/js/jquery.mCustomScrollbar.js"></script>
    <link href="/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
    <script src="/Assets/js/erp.30shine.com/service.bill.add.js"></script>
    <script src="/Assets/js/bootstrap/bootstrap-toggle.min.js"></script>
    <script src="/Assets/js/check-notify.js"></script>
    <link href="/Assets/css/checkin.css" rel="stylesheet" />
    <link href="/Assets/css/bootstrap/bootstrap-toggle.min.css" rel="stylesheet" />
    <style>
        .wp-booking { width: 550px; }
        .delete-booking { display:none; }
    </style>
    <script>
        function callCustomerData(This)
        {
            
            var phone = This.val();
            var phoneLen = phone.length;
            debugger;
            //if (phoneLen >= 10 && phoneLen <= 11) {
                // Lấy dữ liệu thông qua API
                $.ajax({
                    type: "POST",
                    //url: "/GUI/Booking/OrderBooking.aspx/BindHourWhereStylistNewVersion3",
                    url: "/checkin/customerbyphone",
                    data: '{phone : "'+phone+'"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        console.log(response);
                        if(response.d != null)
                        {
                            $("#CustomerName").val(response.d.Fullname);
                            $("#HDF_CustomerCode").val(response.d.Id);
                            if(phone != null)
                            {
                                $("#CustomerName").val(response.d.Fullname).show();
                            }
                            else
                            {
                                $("#CustomerName").val("");
                            }
                            console.log(response.d.Fullname);
                        }
                        else
                        {
                            $("#CustomerName").val("");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });      
        }   
  
        function reSortBooking()
        {
            $("table#tableBookingList tbody tr").each(function (i, v) {
                console.log(i);
                $(this).find("td:first-child").text((i+1));
            });
        }

        // load danh sách khách đợi tại salon
        var loadBillWaiting = function () {
            var salonId = $("#HDF_SalonId").val();
            $.ajax({
                type: "POST",
                url: "/GUI/FrontEnd/Service/Service_Add_V3.aspx/loadBillWaitAtSalon",
                data: '{salonId : "' + salonId + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var mission = JSON.parse(response.d);
                    if (mission.success) {
                        var list = JSON.parse(mission.msg);
                        var trClass = "", checked = "";
                        var table = '<table class="table-booking-list"><thead><tr><th>ID</th><th>Khách hàng</th><th>Số ĐT</th><th>Giờ book</th><th>Stylist</th><th>Giờ vào</th><th>Đã vào cắt</th></tr></thead>';
                        if (list.length > 0) {
                            $.each(list, function (i, v) {
                                var trClass = "", checked = "";
                                if (v.IsPending == true) {
                                    trClass = "class='is-makebill'";
                                    checked = "checked='checked'";
                                }
                                table += "<tr " + trClass + " id='a'>" +
                                            "<td style='text-align:center;'>" + v.Id + "</td>" +
                                            "<td class='c-name'>" + v.CustomerName + "</td>" +
                                            "<td class='c-phone'>" + v.CustomerPhone + "</td>" +
                                            "<td>" + (v.HourFrame != null ? v.HourFrame : "") + "</td>" +
                                            "<td>" + (v.StylistName != null ? v.StylistName : "") + "</td>" +
                                            "<td class='c-time'>" + convertDate(v.CreatedTime) + "</td>" +
                                            "<td style='text-align:center;'><input type='checkbox' onclick='updateBillWaitStatus($(this), " + v.Id + ")' " + checked + " /></td>" +
                                        "</tr>";
                            });
                        }
                        table += "</table>";
                        $(".customer-waiting-list").empty().append($(table));
                        reSortBooking();
                    } else {
                        //delFailed();
                    }
                },
                failure: function (response) { alert(response.d); }
            });
        }

        // load danh sách khách đặt lịch
        var startBookingID = 0;
        var loadBillBooking = function () {
            var salonId = $("#HDF_SalonId").val();
            $.ajax({
                type: "POST",
                url: "/GUI/SystemService/Webservice/BookingService.asmx/loadBillFromBooking_V2",
                data: '{salonId : ' + salonId + ', startBookingID : ' + startBookingID + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var mission = JSON.parse(response.d);
                    if (mission.success) {
                        var list = JSON.parse(mission.msg);
                        var trClass = "", checked = "";
                        var tr = '';
                        if (list.length > 0) {
                            $.each(list, function (i, v) {
                                startBookingID = v.Id > startBookingID ? v.Id : startBookingID;

                                var trClass = "", checked = "";
                                if (v.IsMakeBill == true) {
                                    trClass = "class='is-makebill'";
                                    checked = "checked='checked'";
                                }

                                tr += "<tr " + trClass + " id='a'>" +
                                            "<td style='text-align:center;' data-id='"+v.Id+"'>" + (i+1) + "</td>" +
                                            "<td class='c-name'>" + v.CustomerName + "</td>" +
                                            "<td class='c-phone'>" + v.CustomerPhone + "</td>" +
                                            "<td style='text-align:center;'>" + v.Hour + "</td>" +                                    
                                            "<td class='c-stylist'>" + v.StylistName + "</td>" +                                    
                                            "<td style='text-align:center;'><input type='checkbox' onclick='updateBookingStatus($(this), " + v.Id + ")' " + checked + " /></td>" +
                                            "<td class='delete-booking'><i onclick='openDeleteBox($(this))' class='fa fa-trash-o' aria-hidden='true'></i>" +
                                                "<div class='note-box'>" +
                                                    "<textarea placeholder='Ghi chú (Bắt buộc)' rows='3'></textarea>" +
                                                    "<div class='col-xs-6'>" +
                                                        "<div class='btn-action btn-complete' onclick='deleteBooking($(this),"+v.Id+")'>OK</div>" +
                                                    "</div>" +
                                                    "<div class='col-xs-6'>" +
                                                        "<div class='btn-action btn-close' onclick='closeDeleteBox($(this))'>Đóng</div>" +
                                                    "</div>" +
                                                "</div>" +
                                            "</td>" +
                                        "</tr>";
                            });
                        }
                        $("#tableBookingList tbody").append($(tr));
                    } else {
                        //delFailed();
                    }
                },
                failure: function (response) { alert(response.d); }
            });
        }

        function callCustomerData(This)
        {
            var phone = This.val();
            var phoneLen = phone.length;
            console.log("phone : " + This.val() + " | " + phoneLen);
            //if (phoneLen >= 10 && phoneLen <= 11) {
            // Lấy dữ liệu thông qua API
            $.ajax({
                type: "POST",
                //url: "/GUI/Booking/OrderBooking.aspx/BindHourWhereStylistNewVersion3",
                url: "/checkin/customerbyphone",
                data: '{phone : "'+phone+'"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    console.log(response);
                    if(response.d != null)
                    {
                        $("#CustomerName").val(response.d.Fullname);
                        $("#HDF_CustomerCode").val(response.d.Id);
                        if(phone != null)
                        {
                            $("#CustomerName").val(response.d.Fullname).show();
                        }
                        else
                        {
                            $("#CustomerName").val("");
                        }
                        console.log(response.d.Fullname);
                    }
                },
                failure: function (response) { alert(response.d); }
            });                
            //}
            //else {
                
            //}
        }
    </script>
    </asp:Content>
    <asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
        <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
        <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">


            <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
            <div class="wp sub-menu">
                <div class="wp960">
                    <div class="wp content-wp">
                        <ul class="ul-sub-menu" id="subMenu">
                            <li>Dịch vụ &nbsp;&#187; </li>
                            <li class="li-listing"><a href="/dich-vu/danh-sach.html">Danh sách</a></li>
                            <li class="li-pending"><a href="/dich-vu/pending.html">
                                <div class="pending-1"></div>
                                Pending</a></li>
                            <li class="li-add"><a href="/dich-vu/them-phieu-v3.html">Thêm mới</a></li>
                            <%--<% if (_salonId == 0 || _salonId == 3)
                            { %>--%>
                            <li class="li-listing">
                                <a href="javascript:void(0);" onclick="toggleListBooking($(this))" class="open-booking-list">Khách đặt lịch</a>
                                <div class="wp-booking customer-booking">
                                    <div class="wp-booking-content">
                                        <p style="font-family: Roboto Condensed Bold; text-transform: uppercase; padding-bottom: 5px; float: left;">Chọn salon</p>
                                        <asp:DropDownList ID="ddlSalon1" CssClass="form-control select" runat="server" Style="width: 245px; margin-left: 10px; margin-bottom: 10px;">
                                        </asp:DropDownList>
                                        <%--<select id="ddlSalon1" style="width: 245px; margin-left: 10px; margin-bottom: 10px;" runat="server" onchange="loadBillBookingBySalon($(this))"></select>--%>
                                        <div class="close-booking-box" onclick="closeListBooking($(this))">X Đóng</div>
                                        <div class="booking-listing customer-booking-list">
                                            <table class="table-booking-list" id="tableBookingList">
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Khách hàng</th>
                                                        <th>Số ĐT</th>
                                                        <th>Khung giờ hẹn</th>
                                                        <th>Stylist</th>
                                                        <th>Đã đến</th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="li-listing">
                                <a href="javascript:void(0);" onclick="toggleListBooking($(this))" class="open-booking-list">Khách chờ tại salon</a>
                                <div class="wp-booking customer-waiting">
                                    <div class="wp-booking-content">
                                        <p style="font-family: Roboto Condensed Bold; text-transform: uppercase; padding-bottom: 5px; float: left;">Chọn salon</p>
                                        <asp:DropDownList ID="ddlSalon2" CssClass="form-control select" runat="server" Style="width: 245px; margin-left: 10px; margin-bottom: 10px;" onchange="loadBillWaitBySalon($(this))">
                                        </asp:DropDownList>
                                        <%--<select id="ddlSalon2" style="width: 245px; margin-left: 10px; margin-bottom: 10px;" runat="server" onchange="loadBillBookingBySalon($(this))"></select>--%>
                                        <div class="close-booking-box" onclick="closeListBooking($(this))">X Đóng</div>
                                        <div class="booking-listing customer-waiting-list">
                                            <table class="table-booking-list" id="tableBillWait">
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <th>Khách hàng</th>
                                                        <th>Số ĐT</th>
                                                        <th>Giờ book</th>
                                                        <th>Stylist</th>
                                                        <th>Giờ vào</th>
                                                        <th>Đã vào cắt</th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <%--<% } %>--%>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="wp customer-add admin-product-add fe-service no-padding no-margin">
                <%-- Add --%>
                <div class="content-wp no-padding">

                    <!-- END System Message -->
                    <div class="container">
                        <!-- System Message -->
                        <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                        <div class="col-xs-12 col-lg-12 com-md-12 col-sm-12" style="padding-left: 0; padding-right: 0;">
                            <%--form input customer booking or pending--%>
                            <div class="col-xs-6 col-ld-6 col-md-6 col-sm-6">
                                <div class="table-wp">
                                    <table class="table-add admin-product-table-add fe-service-table-add">
                                        <tbody>
                                            <%--   <tr class="title-head">
                                            <td><strong>Thông tin dịch vụ</strong></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        <tr class="tr-margin" style="height: 20px;"></tr>--%>

                                            <tr class="tr-field-ahalf tr-send">
                                                <td class="col-xs-1 left"><span>KH</span></td>
                                                <td class="col-xs-11 right">
                                                    <div class="field-wp">
                                                        <div class="filter-item" style="width: 35%; max-width: 250px; z-index: 3000; padding-right: 0;">
                                                            <%--<asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion" data-field="customer.code" data-value="0"
                                                            data-callback="Callback_UpdateCustomerCode" AutoCompleteType="Disabled" ID="CustomerCode" ClientIDMode="Static" onchange="CustomerInputOnchange($(this))" onfocus="CustomerInputOnchange($(this))" onblur="checkBindCustomerDataByPhone($(this));" onkeydown="validateTypingNumber(event, $(this))" placeholder="Số điện thoại" runat="server" Style="width: 25%;"></asp:TextBox>--%>
                                                            <asp:TextBox CssClass="st-head ip-short form-control" data-field="customer.code" data-value="0" data-callback="Callback_UpdateCustomerCode" AutoCompleteType="Disabled" ID="CustomerCode" ClientIDMode="Static" placeholder="Số điện thoại" runat="server" Style="width: 25%;"
                                                                onblur="callCustomerData($(this))" onkeypress="return isNumber(event)">
                                                            </asp:TextBox>
                                                            <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                                                                <ul class="ul-listing-staff ul-listing-suggestion" id="Ul4"></ul>
                                                            </div>
                                                            <div class="fake-value"></div>
                                                        </div>
                                                        <asp:TextBox ID="CustomerName" CssClass="form-control" runat="server" ClientIDMode="Static" placeholder="Tên Khách hàng" Style="width: 35%; max-width: 250px; margin-left: 10px; font-family: Roboto Condensed Bold; font-size: 15px;"></asp:TextBox>

                                                        <asp:Panel ID="AddCustomerWait" onclick="addCustomerWait($(this))" CssClass="btn-send" runat="server" ClientIDMode="Static" style="height: 34px; line-height: 34px; margin-left: 10px;">
                                                        Thêm khách chờ
                                                    </asp:Panel>
                                                    </div>
                                                    <div class="field-wp">
                                                        <p class="validate-phone">(*) Số điện thoại chỉ gồm 10 hoặc 11 chữ số từ 0-9. Bắt đầu bằng số 0.</p>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr id="trFlowInfor">
                                                <td class="col-xs-1 left"></td>
                                                <td class="col-xs-11 right" style="padding-bottom: 0;">
                                                    <div class="field-wp" style="height: auto; line-height: 18px;">
                                                        <p class="_p1">
                                                            <span style="text-transform: uppercase; font-style: normal;" class="_span1">Khách hàng mới</span>
                                                            <span>&nbsp;&nbsp;-&nbsp;&nbsp;Nguồn thông tin biết đến cửa hàng</span>
                                                        </p>
                                                        <asp:Repeater runat="server" ID="Rpt_SocialThread" ClientIDMode="Static">
                                                            <itemtemplate>
                                                            <label class="lbl-cus-no-infor" style="margin-right: 5px; margin-bottom: 8px; cursor: pointer; font-weight: normal; font-size: 13px; padding: 2px 10px; background: #ddd; line-height: 21px;">
                                                                <input name="socialThread" type="radio" value="<%# Eval("Id") %>" <%# Container.ItemIndex == 0 ? "checked='checked'" : "" %> style="position: relative; top: 2px;" onclick="getSocialThreadId()" />
                                                                <%# Eval("Name") %>
                                                            </label>
                                                        </itemtemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </td>
                                            </tr>

                                            <%--<tr class="tr-field-ahalf" id="TrIframeAddCustomer">
                                            <td class="col-xs-1 left"><span></span></td>
                                            <td class="col-xs-11 right">
                                                <div class="field-wp">
                                                    <div class="iframe-add-customer" id="IframeAddCustomer">
                                                        <iframe src="/khach-hang/them-moi-iframe.html"></iframe>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>--%>

                                            <tr class="tr-field-ahalf" runat="server" id="TrSalon" visible="false">
                                                <td class="col-xs-1 left"><span>Salon</span></td>
                                                <td class="col-xs-11 right">
                                                    <span class="field-wp">
                                                        <asp:DropDownList ID="Salon" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="width: 45%; max-width: 250px;"></asp:DropDownList>
                                                        <asp:RequiredFieldValidator InitialValue="0"
                                                            ID="ValidateSalon" Display="Dynamic"
                                                            ControlToValidate="Salon"
                                                            runat="server" Text="Bạn chưa chọn Salon!"
                                                            ErrorMessage="Vui lòng chọn Salon!"
                                                            ForeColor="Red"
                                                            CssClass="fb-cover-error">
                                                        </asp:RequiredFieldValidator>
                                                    </span>
                                                </td>
                                            </tr>

                                            <tr class="tr-field-ahalf tr-product">
                                                <td class="col-xs-1 left"><span style="line-height: 18px;">Dịch vụ</span></td>
                                                <td class="col-xs-11 right">
                                                    <div class="row" id="quick-service">
                                                        <div class="checkbox cus-no-infor">
                                                            <label class="lbl-cus-no-infor">
                                                                <input type="checkbox" data-code="SP00036" disabled="disabled" checked="checked" />
                                                                Shine Combo 100k
                                                            </label>
                                                        </div>
                                                        <asp:Repeater runat="server" ID="Rpt_ServiceFeatured">
                                                            <itemtemplate>
                                                            <div class="checkbox cus-no-infor">
                                                                <label class="lbl-cus-no-infor">
                                                                    <input type="checkbox" data-code="<%# Eval("Code") %>" onclick="pushQuickData($(this), 'service')" />
                                                                    <%# Eval("Name") %>
                                                                </label>
                                                            </div>
                                                        </itemtemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                    <div class="listing-product item-service" id="ListingServiceWp" runat="server" clientidmode="Static">
                                                        <table class="table table-listing-product table-item-service" id="table-item-service">
                                                            <thead>
                                                                <tr>
                                                                    <th>STT</th>
                                                                    <th>Tên dịch vụ</th>
                                                                    <th>Mã dịch vụ</th>
                                                                    <th style="display: none;">Đơn giá</th>
                                                                    <th style="display: none;">Số lượng</th>
                                                                    <th style="display: none;">Giảm giá</th>
                                                                    <th style="display: none;">Thành tiền</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <asp:Repeater ID="Rpt_Service_Bill" runat="server">
                                                                    <itemtemplate>
                                                                    <tr>
                                                                        <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                                                        <td class="td-product-name"><%# Eval("Name") %></td>
                                                                        <td class="td-product-code" data-id="<%# Eval("Id")%>"><%# Eval("Code") %></td>
                                                                        <td class="td-product-price" data-price="<%# Eval("Price") %>"><%# Eval("Price") %></td>
                                                                        <td class="td-product-quantity">
                                                                            <input type="text" class="product-quantity" value="<%# Eval("Quantity") %>" />
                                                                        </td>
                                                                        <td class="td-product-voucher" data-voucher="<%# Eval("VoucherPercent") %>">
                                                                            <div class="row">
                                                                                <input type="text" class="product-voucher" value="<%# Eval("VoucherPercent") %>" style="margin: 0 auto; float: none; text-align: center; width: 50px;" />
                                                                            </div>
                                                                        </td>
                                                                        <td class="map-edit">
                                                                            <div class="box-money" style="display: block;">
                                                                                <%# Convert.ToInt32(Eval("Quantity")) * Convert.ToInt32(Eval("Price")) * 
                                                            (100-Convert.ToInt32(Eval("VoucherPercent")))/100 %>
                                                                            </div>
                                                                            <div class="edit-wp">
                                                                                <a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),'<%# Eval("Name") %>',<%# Eval("Id") %>,'service')"
                                                                                    href="javascript://" title="Xóa"></a>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </itemtemplate>
                                                                </asp:Repeater>
                                                            </tbody>
                                                        </table>
                                                    </div>

                                                    <div class="row free-service">
                                                        <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                                            <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">Phụ trợ : </label>
                                                        </div>
                                                        <asp:Repeater runat="server" ID="Rpt_FreeService">
                                                            <itemtemplate>
                                                            <div class="checkbox cus-no-infor" style="padding-bottom: 4px;">
                                                                <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                                    <input type="checkbox" onclick="pushFreeService($(this),<%# Eval("Id")%>)" data-id="<%# Eval("Id") %>"
                                                                        <%# Eval("Status").ToString() == "1" ? "checked='checked'" : "" %> />
                                                                    <%# Eval("Name") %>
                                                                </label>
                                                            </div>
                                                        </itemtemplate>
                                                        </asp:Repeater>
                                                    </div>

                                                </td>
                                            </tr>

                                            <tr class="tr-send">
                                                <td class="col-xs-1 left"><span>Team</span></td>
                                                <td class="col-xs-11 right">
                                                    <div class="field-wp ">
                                                        <%--<p class="status-team"></p>--%>
                                                        <div class="wr-choose-team">
                                                            <div class="team-color-select" runat="server" id="TeamColorSelect" clientidmode="Static" data-color="">
                                                            </div>
                                                            <p class="time"></p>
                                                        </div>
                                                        <asp:Repeater runat="server" ID="Rpt_TeamWork">
                                                            <itemtemplate>
                                                            <div class="item-color" style="background: <%# Eval("Color")%>;" onclick="setTeamColor($(this),<%# Eval("Id") %>,'<%# Eval("Color") %>')"></div>
                                                        </itemtemplate>
                                                        </asp:Repeater>

                                                    </div>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td class="col-xs-1 left"><span style="line-height: 18px;">Lễ tân</span></td>
                                                <td class="col-xs-11 right">
                                                    <div class="filter-item" style="width: 45%; max-width: 250px;">
                                                        <asp:TextBox CssClass="st-head ip-short auto-select form-control" data-field="Reception" data-value="0"
                                                            AutoCompleteType="Disabled" ID="InputReception" ClientIDMode="Static" placeholder="Nhập mã số" runat="server">
                                                        </asp:TextBox>
                                                        <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                                                            <ul class="ul-listing-staff ul-listing-suggestion" id="Ul1"></ul>
                                                        </div>
                                                        <div class="fake-value" id="FVReception" runat="server" clientidmode="Static"></div>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr style="display: none;">
                                                <td class="col-xs-1 left"><span style="line-height: 18px;"></span></td>
                                                <td class="col-xs-11 right">
                                                    <div class="row free-service">
                                                        <div class="checkbox cus-no-infor" style="padding-bottom: 4px;">
                                                            <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                                <asp:CheckBox runat="server" ID="isX2" ClientIDMode="Static" />
                                                                Tăng ca
                                                            </label>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr class="tr-send">
                                                <td class="col-xs-1 left"></td>
                                                <td class="col-xs-11 right no-border">
                                                    <span class="field-wp">
                                                        <asp:Panel ID="BtnSend" CssClass="btn-send" runat="server" ClientIDMode="Static">In phiếu</asp:Panel>

                                                        <asp:Button ID="BtnFakeSend" runat="server" Text="Hoàn tất"
                                                            ClientIDMode="Static" OnClick="AddService" Style="display: none;"></asp:Button>

                                                        <asp:Panel ID="BtnBook" CssClass="btn-send btn-book" runat="server" ClientIDMode="Static" Style="margin-left: 12px; display: none;">Đặt lịch</asp:Panel>

                                                        <%--<asp:Button ID="BtnFakeBook" runat="server" Text="Hoàn tất"
                                                        ClientIDMode="Static" OnClick="AddServiceBooking" Style="display: none;"></asp:Button>--%>
                                                    </span>

                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>


                                </div>
                            </div>
                            <%--form listing booking--%>
                            <div class="col-xs-6 col-ld-6 col-md-6 col-sm-6">

                                <div class="wrap-booking floor-2">
                                    <div class="head">
                                        <p class="title">TẦNG 2</p>
                                        <%--   <div class="btn btn-add-team"><span><i class="fa fa-plus" aria-hidden="true"></i>Thêm Team</span></div>--%>
                                        <div class="field-wp ">
                                            <div class="wr-choose-team" data-status="0">
                                                <% if (lstTeamService.Count > 0)
                                                    {
                                                        foreach (var v in lstTeamService)
                                                        { %>
                                                <div class="item-color" style="background: <%= v.Color %>;" onclick="addTeamToFloor(<%=v.Id %>,'<%=v.Color %>',2)" data-team-id="<%=v.Id %>">
                                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                                </div>
                                                <% } %>
                                                <% } %>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-ld-12 col-md-12 col-sm-12 wrap-list-added"></div>
                                </div>

                                <div class="wrap-booking floor-3">
                                    <div class="head">
                                        <p class="title">TẦNG 3</p>
                                        <%--   <div class="btn btn-add-team"><span><i class="fa fa-plus" aria-hidden="true"></i>Thêm Team</span></div>--%>
                                        <div class="field-wp ">
                                            <div class="wr-choose-team" data-status="0">
                                                <% if (lstTeamService.Count > 0)
                                                    {
                                                        foreach (var v in lstTeamService)
                                                        { %>
                                                <div class="item-color" style="background: <%= v.Color %>;" onclick="addTeamToFloor(<%=v.Id %>,'<%=v.Color %>',3)" data-team-id="<%=v.Id %>">
                                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                                </div>
                                                <% } %>
                                                <% } %>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-ld-12 col-md-12 col-sm-12 wrap-list-added"></div>
                                </div>

                                <div class="wrap-booking floor-4">
                                    <div class="head">
                                        <p class="title">TẦNG 4</p>
                                        <%--   <div class="btn btn-add-team"><span><i class="fa fa-plus" aria-hidden="true"></i>Thêm Team</span></div>--%>
                                        <div class="field-wp ">
                                            <div class="wr-choose-team" data-status="0">
                                                <% if (lstTeamService.Count > 0)
                                                    {
                                                        foreach (var v in lstTeamService)
                                                        { %>
                                                <div class="item-color" style="background: <%= v.Color %>;" onclick="addTeamToFloor(<%=v.Id %>,'<%=v.Color %>',4)" data-team-id="<%=v.Id %>">
                                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                                </div>
                                                <% } %>
                                                <% } %>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-ld-12 col-md-12 col-sm-12 wrap-list-added"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%-- end Add --%>
                </div>

                <!-- Danh mục sản phẩm -->
                <div class="popup-product-wp popup-product-item">
                    <div class="wp popup-product-head">
                        <strong>Danh mục sản phẩm</strong>
                    </div>
                    <div class="wp popup-product-content">
                        <div class="wp popup-product-guide">
                            <div class="left">Hướng dẫn:</div>
                            <div class="right">
                                <p>- Click vào ô check box để chọn sản phẩm</p>
                                <p>- Click vào ô số lượng để thay đổi số lượng sản phẩm</p>
                            </div>
                        </div>
                        <div class="wp listing-product item-product">
                            <table class="table" id="table-product">
                                <thead>
                                    <tr>
                                        <th>
                                            <input type="checkbox" /></th>
                                        <th>STT</th>
                                        <th>Tên sản phẩm</th>
                                        <th>Mã sản phẩm</th>
                                        <th>Đơn giá</th>
                                        <th class="th-product-quantity">Số lượng</th>
                                        <th>Giảm giá (%)</th>
                                        <th>Thành tiền</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="Rpt_Product" runat="server">
                                        <itemtemplate>
                                        <tr>
                                            <td class="td-product-checkbox item-product-checkbox">
                                                <input type="checkbox" value="<%# Eval("Id") %>"
                                                    data-code="<%# Eval("Code") %>" data-name="<%# Eval("Name") %>" />
                                            </td>
                                            <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                            <td class="td-product-name"><%# Eval("Name") %></td>
                                            <td class="td-product-code" data-id="<%# Eval("Id") %>" data-code="<%# Eval("Code") %>"><%# Eval("Code") %></td>
                                            <td class="td-product-price" data-price="  <%# Eval("Price") %>"><%# Eval("Price") %></td>
                                            <td class="td-product-quantity">
                                                <input type="text" class="product-quantity" value="1" />
                                            </td>
                                            <td class="td-product-voucher" data-voucher="<%# Eval("VoucherPercent") %>">
                                                <div class="row">
                                                    <input type="text" class="product-voucher" value="<%# Eval("VoucherPercent") %>" style="margin: 0 auto; float: none; text-align: center; width: 50px;" />
                                                    %
                                                </div>
                                                <div class="row promotion-money">
                                                    <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                                        <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                            <input type="checkbox" class="item-promotion" onclick="excPromotion($(this), 35000)" data-value="35000" data-id="1" style="margin-left: 0;" />
                                                            - 35.000 VNĐ
                                                        </label>

                                                    </div>
                                                    <br />
                                                    <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                                        <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                            <input type="checkbox" class="item-promotion" onclick="excPromotion($(this), 50000)" data-id="2" data-value="50000" style="margin-left: 0;" />
                                                            - 50.000 VNĐ
                                                        </label>

                                                    </div>
                                                </div>
                                            </td>
                                            <td class="map-edit">
                                                <div class="box-money"></div>
                                                <div class="edit-wp">
                                                    <a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),'<%# Eval("Name") %>','product')"
                                                        href="javascript://" title="Xóa"></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </itemtemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <div class="wp btn-wp">
                            <div class="popup-product-btn btn-complete" data-item="product">Hoàn tất</div>
                            <div class="popup-product-btn btn-esc">Thoát</div>
                        </div>
                    </div>
                </div>
                <!-- END Danh mục sản phẩm -->

                <!-- Danh mục dịch vụ -->
                <div class="popup-product-wp popup-service-item">
                    <div class="wp popup-product-head">
                        <strong>Danh mục dịch vụ</strong>
                    </div>
                    <div class="wp popup-product-content">
                        <div class="wp popup-product-guide">
                            <div class="left">Hướng dẫn:</div>
                            <div class="right">
                                <p>- Click vào ô check box để chọn dịch vụ</p>
                                <p>- Click vào ô số lượng để thay đổi số lượng dịch vụ</p>
                            </div>
                        </div>
                        <div class="wp listing-product item-product">
                            <table class="table" id="table-service">
                                <thead>
                                    <tr>
                                        <th>
                                            <input type="checkbox" /></th>
                                        <th>STT</th>
                                        <th>Tên dịch vụ</th>
                                        <th>Mã dịch vụ</th>
                                        <th style="display: none;">Đơn giá</th>
                                        <th style="display: none;" class="th-product-quantity">Số lượng</th>
                                        <th style="display: none;">Giảm giá (%)</th>
                                        <th style="display: none;">Thành tiền</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="Rpt_Service" runat="server">
                                        <itemtemplate>
                                        <tr>
                                            <td class="td-product-checkbox item-product-checkbox">
                                                <input type="checkbox" value="<%# Eval("Id") %>"
                                                    data-code="<%# Eval("Code") %>" data-name="<%# Eval("Name") %>" />
                                            </td>
                                            <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                            <td class="td-product-name"><%# Eval("Name") %></td>
                                            <td class="td-product-code" data-id="<%# Eval("Id") %>" data-code="<%# Eval("Code") %>"><%# Eval("Code") %></td>
                                            <td style="display: none!important;" class="td-product-price" data-price="   <%# Eval("Price") %>"><%# Eval("Price") %></td>
                                            <td style="display: none;" class="td-product-quantity">
                                                <input type="text" class="product-quantity" value="1" />
                                            </td>
                                            <td style="display: none!important;" class="td-product-voucher" data-voucher="<%# Eval("VoucherPercent") %>">
                                                <div class="row">
                                                    <input type="text" class="product-voucher" value="<%# Eval("VoucherPercent") %>" style="margin: 0 auto; float: none; text-align: center; width: 50px" />
                                                    %
                                                </div>
                                            </td>
                                            <td class="map-edit" style="display: none;">
                                                <div class="box-money"></div>
                                                <div class="edit-wp">
                                                    <a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),'<%# Eval("Name") %>','service')"
                                                        href="javascript://" title="Xóa"></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </itemtemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <div class="wp btn-wp">
                            <div class="popup-product-btn btn-complete" data-item="service">Hoàn tất</div>
                            <div class="popup-product-btn btn-esc">Thoát</div>
                        </div>
                    </div>
                </div>
                <!-- END Danh mục dịch vụ -->
            </div>

            <!-- input hidden -->
            <asp:HiddenField ID="HDF_CustomerCode" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HDF_ProductIds" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HDF_ServiceIds" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HDF_TotalMoney" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HDF_UserImages" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HDF_UserImagesDelete" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="Hairdresser" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HairMassage" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="Cosmetic" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HDF_SalonId" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HDF_Suggestion_Code" ClientIDMode="Static" runat="server" />
            <asp:HiddenField ID="HDF_Suggestion_Field" ClientIDMode="Static" runat="server" />
            <asp:HiddenField ID="HDF_FreeService" ClientIDMode="Static" runat="server" />
            <asp:HiddenField ID="HDF_TeamId" ClientIDMode="Static" runat="server" />
            <asp:HiddenField ID="HDF_TeamId_Booking" ClientIDMode="Static" runat="server" />
            <asp:HiddenField ID="Reception" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HDF_SocialThread" runat="server" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
            <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
            <!-- END input hidden -->

            <!-- In hóa đơn -->
            <iframe id="iframePrint" style="display: none;"></iframe>
            <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
            <script type="text/javascript">
                jQuery(document).ready(function () {
                    var qs = getQueryStrings();
                    if (qs["msg_print_billcode"] != undefined) {
                        openPdfIframe("/Public/PDF/" + qs["msg_print_billcode"] + ".pdf");
                    }
                    showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);
                });
                function openPdfIframe(src) {
                    var PDF = document.getElementById("iframePrint");
                    PDF.src = src;
                    PDF.onload = function () {
                        PDF.focus();
                        PDF.contentWindow.print();
                        PDF.contentWindow.close();
                    }
                }
            </script>
    <!--/ In hóa đơn -->
    </asp:Panel>
</asp:Content>
