﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ServiceBadRatingList.aspx.cs" Inherits="_30shine.GUI.FrontEnd.Service.ServiceBadRatingList" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ServiceBadRatingList" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <script src="/Assets/js/check-notify.js"></script>
        <script src="/Assets/js/date.js"></script>
        <style>
            .is-booking { background: #ff8080; }
            .is-inprocedure { background: #bbff99; }
            ._p2{ float: left; }
            .p-note { font-family: Roboto Condensed Regular!important; margin-left: 20px; }
            .p-note span { width: 10px; height: 10px; display: block; float: left; position: relative; top: 5px; margin-right: 5px; }
            .customer-add .table-add td.map-edit .edit-wp {
                position: absolute;
                right: 3px;
                top: 24%;
                background: none;
                display: block;
            }
        </style>        

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Dịch vụ &nbsp;&#187; </li>
                        <li class="li-listing active"><a href="/dich-vu/danh-sach.html">Danh sách</a></li>
                        <li class="li-pending active">
                            <a href="/dich-vu/danh-sach/khach-chua-hai-long.html"><div class="pending-1"></div> Khách chưa hài lòng</a>
                        </li>
                        <%--<li class="li-add"><a href="/dich-vu/them-phieu-v3.html">Thêm mới</a></li>--%>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row">
                    <strong class="st-head"><i class="fa fa-filter"></i>Lọc kết quả</strong>
                    <%--<div class="filter-item">
                        <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion" data-field="customer.code" data-value="0"
                            AutoCompleteType="Disabled" ID="CustomerCode" ClientIDMode="Static" placeholder="Mã Khách hàng" runat="server"></asp:TextBox>
                        <div class="listing-staff-wp eb-select-data ">
                            <ul class="ul-listing-staff ul-listing-suggestion" id="UlCustomerCode"></ul>
                        </div>
                    </div>
                    <div class="filter-item">
                        <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion" data-field="customer.name" data-value="0"
                            AutoCompleteType="Disabled" ID="CustomerName" ClientIDMode="Static" placeholder="Nhập tên Khách hàng" runat="server"></asp:TextBox>
                        <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                            <ul class="ul-listing-staff ul-listing-suggestion" id="UlCustomerName">
                            </ul>
                        </div>
                    </div>

                    <div class="filter-item">
                        <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion" data-value="0" data-field="customer.phone"
                            AutoCompleteType="Disabled" ID="CustomerPhone" ClientIDMode="Static" placeholder="Số điện thoại" runat="server"></asp:TextBox>
                        <div class="listing-staff-wp eb-select-data">
                            <ul class="ul-listing-staff ul-listing-suggestion" id="UlCustomerPhone"></ul>
                        </div>
                    </div>--%>
                    <asp:DropDownList ID="ddlSalon" CssClass="form-control select" runat="server" Style="width: 180px; margin-left: 10px; margin-bottom: 10px; margin-top: 5px;" onchange="excPaging(1)" ClientIDMode="Static">
                                    </asp:DropDownList>
                    <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <%--<a href="javascript://" class="st-head btn-viewdata" onclick="location.href=location.href">Reset Filter</a>--%>
                </div>

                <!-- End Filter -->
                <div class="pending-content-wrap" style="width: 1000px; margin: 0 auto;">
                    <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                    <%--<p class="_p1" style="width: 100%;">Danh sách chờ hoàn tất</p>--%>
                    <!-- End Row Table Filter -->                    
                    <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                    <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                        <ContentTemplate>
                            <p class="_p2"><i class="fa fa-map-marker"></i><%= (Salon != null && Salon.Name != "" ? Salon.Name : "Salon") %></p>                       
                            </p>
                            <div class="table-wp">
                                <table class="table-add table-listing table-pending" id="tablePending">
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Khách hàng</th>                                            
                                            <th>SĐT</th>                                            
                                            <%--<th>Thời gian chờ</th>--%>
                                            <th>Nhân viên</th>
                                            <th>Lý do</th>
                                            <th>Ghi chú</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="RptBillPending" runat="server" OnItemDataBound="Repeater_ItemDataBound">
                                            <ItemTemplate>
                                                <tr >
                                                    <td><%# Container.ItemIndex + 1 %></td>
                                                    <td><%# Eval("CustomerName") %></td>
                                                    <td><%# Eval("CustomerPhone") %></td>
                                                    <td><%# Eval("Staffs") %></td>
                                                    <td><%# Eval("Rating") %></td>
                                                    <td><%# Eval("BillNote") %></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </div>

                            <!-- Paging -->
                            <div class="site-paging-wp">
                                <% if (PAGING.TotalPage > 1)
                                    { %>
                                <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                    <% if (PAGING._Paging.Prev != 0)
                                        { %>
                                    <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                    <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                    <% } %>
                                    <asp:Repeater ID="RptPaging" runat="server">
                                        <ItemTemplate>
                                            <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                                <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                                <%# Eval("PageNum") %>
                                            </a>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                        { %>
                                    <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                    <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                    <% } %>
                                </asp:Panel>
                                <% } %>
                            </div>
                            <!-- End Paging -->
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>                    
                </div>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                <!-- Hidden Field-->
                <asp:HiddenField ID="HDF_Suggestion_Code" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_Suggestion_Field" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_SalonId" runat="server" ClientIDMode="Static" />
                <!-- END Hidden Field-->
            </div>
            <%-- END Listing --%>
        </div>

    </asp:Panel>
</asp:Content>
