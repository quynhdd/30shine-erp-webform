﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.IO;
using System.Web.Helpers;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Windows.Forms;
using LinqKit;
using System.Text;
using System.Data.Entity.Migrations;

namespace _30shine.GUI.FrontEnd.ServiceBooking
{
    public partial class Service_Booking_Listing : System.Web.UI.Page
    {
        private bool Perm_ShowSalon = false;
        private bool Perm_Access = false;

        private List<Appointment> CustomerHistoryAppoint = new List<Appointment>();
        private List<Appointment> CustomerHistoryProduct = new List<Appointment>();
        private List<ProductBasic> ProductList = new List<ProductBasic>();
        private List<ProductBasic> ServiceList = new List<ProductBasic>();
        protected BillService billBooking = new BillService();
        CultureInfo culture = new CultureInfo("vi-VN");

        //list booking
        protected Paging PAGING = new Paging();
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected List<TeamService> lstTeamService = new List<TeamService>();

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckPermission();
            
            if (!IsPostBack)
            {
                bindData();
            }
        }

        private string genSql()
        {
            /// Lấy các phiếu đặt lịch trong hôm nay
            var sql = "";
            string whereSalon = "";
            int salonId = 0;
            if (Session["SalonId"] != null)
            {
                salonId = Convert.ToInt32(Session["SalonId"]);
            }
            if (salonId > 0)
            {
                whereSalon = " and booking.SalonId = " + salonId;
            }
            else
            {
                whereSalon = " and booking.SalonId > 0";
            }
            sql = @"select booking.*, bookhour.[Hour] 
                    from BooKing 
                    left join BookHour as bookhour
                    on booking.HourId = bookhour.Id
                    where booking.IsDelete != 1 and (booking.IsMakeBill is null or booking.IsMakeBill != 1) " + whereSalon +
                    @" and (booking.CreatedDate >= '" + string.Format("{0:yyyy/MM/dd}", DateTime.Now)+ "' and booking.CreatedDate < '" + string.Format("{0: yyyy/MM/dd}", DateTime.Now.AddDays(1))+"')" +
                    " order by booking.Id asc";
            return sql;
        }

        private List<cls_booking> getData()
        {
            string sql = genSql();
            var list = new List<cls_booking>();
            if (sql != null)
            {
                using (var db = new Solution_30shineEntities())
                {
                    list = db.Database.SqlQuery<cls_booking>(sql).ToList();
                }
            }
            return list;
        }

        private void bindData()
        {
            RptBooking.DataSource = getData();
            RptBooking.DataBind();
        }

        //code listpending
        private void Bind_TeamWork()
        {
            using (var db = new Solution_30shineEntities())
            {
                lstTeamService = db.TeamServices.Where(w => w.IsDelete != 1 && w.Publish == true && w.Status == 1).ToList();
            }
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["User_Id"] == null)
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "salonmanager", "reception", "checkin", "checkout" };
                string[] AllowSalon = new string[] { "root", "admin" };
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }
                if (Array.IndexOf(AllowSalon, Permission) != -1)
                {
                    Perm_ShowSalon = true;
                }
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
                
        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((System.Web.UI.WebControls.Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((System.Web.UI.WebControls.Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
        }

        protected void Bind_Paging()
        {
            // init Paging value
            PAGING._Segment = 50;
            //PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? (HDF_Page.Value != "" ? Convert.ToInt32(HDF_Page.Value) : 1) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                int Count = 0;

                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }



        private struct PrintRec
        {
            public string Key { get; set; }
            public string Value { get; set; }
            public int Height { get; set; }
        }

        private class Appointment2 : Appointment
        {
            public int Order { get; set; }
            public DateTime NotifyTime { get; set; }
            public string TeamColor { get; set; }
        }

        public class cls_booking : Booking
        {
            public string Hour { get; set; }
            //public TimeSpan HourFrame { get; set; }
        }
    }
}