﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Web.Helpers;
using System.Text.RegularExpressions;
using System.Globalization;
using LinqKit;
using System.Text;
using System.Data.Entity.Migrations;
using System.IO;

namespace _30shine.GUI.FrontEnd.Service
{
    public partial class Service_Book_Pending_V3 : System.Web.UI.Page
    {
        private bool Perm_ShowSalon = false;
        private bool Perm_Access = false;
        
        private List<BillService> CustomerHistoryService = new List<BillService>();
        private List<BillService> CustomerHistoryProduct = new List<BillService>();
        private List<ProductBasic> ProductList = new List<ProductBasic>();
        private List<ProductBasic> ServiceList = new List<ProductBasic>();
        protected BillService billBooking = new BillService();
        private string _CustomerCode = "";
        private string _CustomerName = "";
        private string BillCode = "";
        private string PDFname = "";
        private int SalonId;
        protected int _salonId;

        //list booking
        protected Paging PAGING = new Paging();
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;

        protected List<TeamService> lstTeamService = new List<TeamService>();
        protected bool HasService = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckPermission();

            //CheckIsBooking();
            if (!IsPostBack)
            {
                if (Session["SalonId"] != null)
                {
                    HDF_SalonId.Value = Session["SalonId"].ToString();
                    _salonId = Convert.ToInt32(Session["SalonId"]);
                }
                else
                {
                    HDF_SalonId.Value = "0";
                    _salonId = 0;
                }
                bindSalon();
                Bind_TeamWork();
                Bind_RptServiceFeatured();
                Bind_RptService();
                Bind_RptFreeService();
            }
        }

        //protected void bindSalon()
        //{
        //    using (var db = new Solution_30shineEntities())
        //    {
        //        var lstSalon = db.Tbl_Salon.Where(a => a.IsDelete != 1 && a.Publish == true).ToList();
        //        ddlSalon.DataSource = lstSalon;
        //        ddlSalon.DataTextField = "Address";
        //        ddlSalon.DataValueField = "Id";
        //        ddlSalon.DataBind();
        //        ListItem item = new ListItem("Chọn tất cả Salon", "0");
        //        ddlSalon.Items.Insert(0, item);
        //        ddlSalon.SelectedIndex = 0;
        //    }
        //}
        private void bindSalon()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Salons = db.Tbl_Salon.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Id).ToList();
                var Key = 0;
                var SalonId = Convert.ToInt32(Session["SalonId"]);

                ddlSalon.DataTextField = "Salon";
                ddlSalon.DataValueField = "Id";

                if (Perm_ViewAllData)
                {
                    ListItem item = new ListItem("Chọn tất cả Salon", "0");
                    ddlSalon.Items.Insert(0, item);

                    foreach (var v in _Salons)
                    {
                        Key++;
                        item = new ListItem(v.Name, v.Id.ToString());
                        ddlSalon.Items.Insert(Key, item);
                    }
                    ddlSalon.SelectedIndex = 0;
                }
                else
                {
                    foreach (var v in _Salons)
                    {
                        if (v.Id == SalonId)
                        {
                            ListItem item = new ListItem(v.Name, v.Id.ToString());
                            ddlSalon.Items.Insert(Key, item);
                            ddlSalon.SelectedIndex = SalonId;
                            ddlSalon.Enabled = false;
                        }
                    }
                }
            }
        }

        //private void CheckIsBooking()
        //{
        //    /// Check booking
        //    using (var db = new Solution_30shineEntities())
        //    {
        //        int integer;
        //        int billId = int.TryParse(Request.QueryString["b"], out integer) ? integer : 0;
        //        if (billId > 0)
        //        {
        //            billBooking = db.BillServices.FirstOrDefault(w => w.Id == billId);
        //        }
        //    }
        //}

        protected void AddService(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                //// Insert to BillService
                var obj = new BillService();
                int integer;

                if (TrSalon.Visible == true)
                {
                    obj.SalonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
                }
                else
                {
                    obj.SalonId = int.TryParse(Session["SalonId"].ToString(), out integer) ? integer : 0;
                }

                BillCode = GenBillCode(Convert.ToInt32(obj.SalonId), "HD", 4);
                PDFname = BillCode + "_" + UIHelpers.GetUniqueKey(6, 6);
                obj.BillCode = BillCode;

                obj.CustomerNoInfo = InputCusNoInfor.Checked;
                if (obj.CustomerNoInfo == true)
                {
                    /// Insert khách hàng anonymous
                    var cCode = UIHelpers.GetUniqueKey(8, 8);
                    var newCus = new Customer();
                    newCus.Fullname = cCode;
                    newCus.Customer_Code = cCode;
                    newCus.SalonId = obj.SalonId;
                    newCus.IsDelete = 0;
                    newCus.CreatedDate = DateTime.Now;
                    db.Customers.AddOrUpdate(newCus);
                    db.SaveChanges();

                    /// Add CustomerCode cho bill
                    obj.CustomerCode = cCode;
                    obj.CustomerId = newCus.Id;
                    _CustomerCode = newCus.Customer_Code;
                    _CustomerName = newCus.Fullname;
                }
                else
                {
                    _CustomerCode = HDF_CustomerCode.Value;
                    var customer = db.Customers.FirstOrDefault(w => w.Customer_Code == _CustomerCode && w.IsDelete != 1);
                    if (customer != null)
                    {
                        _CustomerName = customer.Fullname;
                        obj.CustomerId = customer.Id;
                    }
                    obj.CustomerCode = _CustomerCode;                    
                }
                obj.ProductIds = "";
                obj.ServiceIds = HDF_ServiceIds.Value != "[]" ? HDF_ServiceIds.Value : "";
                obj.TotalMoney = 0;
                obj.IsDelete = 0;
                if (billBooking != null && billBooking.CreatedDate != null)
                {
                    obj.CreatedDate = billBooking.CreatedDate.Value.Add(TimeSpan.Parse("00:50:00"));
                    obj.IsBooking = true;
                    billBooking.IsBooked = true;
                    db.BillServices.AddOrUpdate(billBooking);
                    db.SaveChanges();
                }
                else
                {
                    obj.CreatedDate = DateTime.Now;
                    obj.IsBooking = false;
                    obj.IsBooked = false;

                    //obj.CreatedDate = DateTime.Now.Add(TimeSpan.Parse("00:50:00"));
                }

                obj.Pending = 1;
                obj.PDFBillCode = PDFname;
                obj.TeamId = int.TryParse(HDF_TeamId.Value, out integer) ? integer : 0;
                if (Reception.Value != "")
                {
                    obj.ReceptionId = int.TryParse(Reception.Value, out integer) ? integer : 0;
                }                

                db.BillServices.Add(obj);
                var exc = db.SaveChanges();
                var Error = exc > 0 ? 0 : 1;

                if (Error == 0)
                {
                    // insert service to flow
                    var objService = new FlowService();
                    //var str = "[{\"Id\":\"19\",\"Price\":340000,\"Quantity\":\"1\"}]";
                    var serializer = new JavaScriptSerializer();
                    ServiceList = serializer.Deserialize<List<ProductBasic>>(obj.ServiceIds);
                    if (ServiceList != null)
                    {
                        foreach (var v in ServiceList)
                        {
                            objService = new FlowService();
                            objService.BillId = obj.Id;
                            objService.ServiceId = v.Id;
                            objService.Price = v.Price;
                            objService.Quantity = v.Quantity;
                            objService.VoucherPercent = Convert.ToInt32(v.VoucherPercent);
                            objService.CreatedDate = DateTime.Now;
                            objService.IsDelete = 0;
                            if (Perm_ShowSalon)
                            {
                                objService.SalonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
                            }
                            else
                            {
                                objService.SalonId = int.TryParse(HDF_SalonId.Value, out integer) ? integer : 0;
                            }
                            objService.SellerId = int.TryParse(Cosmetic.Value, out integer) ? integer : 0;

                            db.FlowServices.Add(objService);
                            Error += db.SaveChanges() > 0 ? 0 : 1;
                        }
                    }

                    if (Error == 0)
                    {
                        // Phụ trợ như : Cạo mặt, Lấy ráy tai...
                        var objPromotion = new FlowPromotion();
                        string promotion = (HDF_FreeService.Value != "[]" || HDF_FreeService.Value != "") ? HDF_FreeService.Value : "";
                        var prms = serializer.Deserialize<List<int>>(promotion);
                        if (prms != null)
                        {
                            foreach (var v in prms)
                            {
                                if (Error == 0)
                                {
                                    objPromotion = new FlowPromotion();
                                    objPromotion.PromotionId = v;
                                    objPromotion.BillId = obj.Id;
                                    objPromotion.Group = 1;
                                    objPromotion.Quantity = 1;
                                    objPromotion.CreatedDate = obj.CreatedDate;
                                    objPromotion.IsDelete = 0;
                                    objPromotion.SalonId = Convert.ToInt32(Session["SalonId"]);

                                    db.FlowPromotions.Add(objPromotion);
                                    exc = db.SaveChanges();
                                    Error = exc > 0 ? Error : ++Error;
                                }
                            }
                        }
                    }

                    if (Error == 0)
                    {
                        // print bill
                        PrintBillPDF.GenPDF(false, obj);
                        // Thông báo thành công
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Thêm thành công!"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_print_billcode", PDFname));
                        UIHelpers.Redirect("/dich-vu/them-phieu-v2.html", MsgParam);
                    }
                    else
                    {
                        MsgSystem.Text = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                        MsgSystem.CssClass = "msg-system warning";
                    }                
                }
                else
                {
                    MsgSystem.Text = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                    MsgSystem.CssClass = "msg-system warning";
                }

            }
        }        

        /// <summary>
        /// Bind danh sách salon
        /// </summary>
        public void Bind_Salon()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Tbl_Salon.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Order).ToList();
                var Key = 0;
                var Count = LST.Count;
                Salon.DataTextField = "Salon";
                Salon.DataValueField = "Id";
                ListItem item = new ListItem("Chọn salon", "0");
                Salon.Items.Insert(Key, item);
                Key++;

                if (Count > 0)
                {
                    foreach (var v in LST)
                    {
                        item = new ListItem(v.Name, v.Id.ToString());
                        Salon.Items.Insert(Key, item);
                        Key++;
                    }
                }
            }
        }

        /// <summary>
        /// Bind danh sách sản phẩm
        /// </summary>
        private void Bind_RptProduct()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Products = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1).OrderByDescending(o => o.Id).ToList();

                Rpt_Product.DataSource = _Products;
                Rpt_Product.DataBind();
            }
        }

        /// <summary>
        /// Bind danh sách dịch vụ
        /// </summary>
        private void Bind_RptService()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Service = db.Services.Where(w => w.IsDelete != 1 && w.Publish == 1 && (w.IsFreeService != 1 || w.IsFreeService == null)).OrderByDescending(o => o.Id).ToList();

                Rpt_Service.DataSource = _Service;
                Rpt_Service.DataBind();
            }
        }

        /// <summary>
        /// Bind danh sách team
        /// </summary>
        private void Bind_TeamWork()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.TeamServices.Where(w => w.IsDelete != 1 && w.Publish == true && w.Status == 1).ToList();
                if (billBooking != null && billBooking.TeamId > 0 && lst.Count > 0)
                {
                    for (var i = 0; i < lst.Count; i++)
                    {
                        if (lst[i].Id != billBooking.TeamId)
                        {
                            lst.RemoveAt(i--);
                        }
                    }
                    HDF_TeamId.Value = billBooking.TeamId.ToString();
                    TeamColorSelect.Visible = false;
                }
                lstTeamService = lst;
                Rpt_TeamWork.DataSource = lst;
                Rpt_TeamWork.DataBind();
            }
        }        

        /// <summary>
        /// Bind dịch vụ nổi bật
        /// </summary>
        private void Bind_RptServiceFeatured()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Services.Where(w => w.IsDelete != 1 && w.Publish == 1 && w.Status == 1).OrderByDescending(o => o.Id).ToList();

                Rpt_ServiceFeatured.DataSource = lst;
                Rpt_ServiceFeatured.DataBind();
            }
        }

        /// <summary>
        /// Bind các gói dịch vụ phụ trợ như : cắt móng tay, cạo mặt
        /// </summary>
        private void Bind_RptFreeService()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Service = db.Services.Where(w => w.IsDelete != 1 && w.Publish == 1 && w.IsFreeService == 1).OrderBy(o => o.Id).ToList();
                //int index = -1;
                //if (_Service.Count > 0 && lstFreeService.Count > 0)
                //{
                //    foreach (var v in _Service)
                //    {
                //        index = lstFreeService.IndexOf(v.Id);
                //        if (index != -1)
                //        {
                //            _Service[index].Status = 1;
                //        }
                //    }
                //}
                Rpt_FreeService.DataSource = _Service;
                Rpt_FreeService.DataBind();
            }
        }

        /// <summary>
        /// Generate bill code
        /// </summary>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public string GenBillCode(int salonId, string prefix, int template = 4)
        {
            using (var db = new Solution_30shineEntities())
            {
                string code = "";
                string temp = "";
                int integer;
                var Where = PredicateBuilder.True<BillService>();
                DateTime _FromDate = DateTime.Today;
                DateTime _ToDate = DateTime.Today.AddDays(1);
                Where = Where.And(w => w.IsDelete != 1 && w.CreatedDate >= _FromDate && w.CreatedDate < _ToDate);
                if (salonId > 0)
                {
                    Where = Where.And(w => w.SalonId == salonId);
                }
                var LastBill = db.BillServices.AsExpandable().Where(Where).OrderByDescending(w => w.Id).Take(1).ToList();
                if (LastBill.Count > 0)
                {
                    if (LastBill[0].BillCode != null && LastBill[0].BillCode != "")
                        temp = (int.TryParse(LastBill[0].BillCode.Substring(LastBill[0].BillCode.Length - template, template), out integer) ? ++integer : 0).ToString();
                    else
                        temp = "1";
                }
                else
                {
                    temp = "1";
                }

                int len = temp.Length;
                for (var i = 0; i < template; i++)
                {
                    if (len > 0)
                    {
                        code = temp[--len].ToString() + code;
                    }
                    else
                    {
                        code = "0" + code;
                    }
                }

                return prefix + String.Format("{0:ddMMyy}", DateTime.Now) + code;
            }
        }

        /// <summary>
        /// Lấy dữ liệu khách hàng theo mã KH
        /// </summary>
        /// <param name="CustomerCode"></param>
        /// <returns></returns>
        [WebMethod]
        public static string GetCustomer(string CustomerCode)
        {
            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var _Customer = db.Customers.FirstOrDefault(w => (w.Customer_Code == CustomerCode || w.Phone == CustomerCode) && w.IsDelete != 1);

                if (_Customer != null)
                {
                    _Msg.success = true;
                    _Msg.msg = serializer.Serialize(_Customer);
                }
                else
                {
                    _Msg.success = false;
                }
            }
            return serializer.Serialize(_Msg);
        }

        /// <summary>
        /// Check khách hàng đã từng lập bill trong ngày, tránh nhập nhầm lần 2
        /// </summary>
        /// <param name="CustomerCode"></param>
        /// <returns></returns>
        [WebMethod]
        public static string CheckDuplicateBill(string CustomerCode)
        {
            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var today = Convert.ToDateTime(DateTime.Now.ToString("d/M/yyyy"), culture);
                var todayL = today.AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                var todayR = today.AddDays(1);
                var _Bill = db.BillServices.FirstOrDefault(w => w.CreatedDate > todayL && w.CreatedDate < todayR && w.CustomerCode == CustomerCode && w.IsDelete != 1 && w.Pending != 1);

                if (_Bill != null)
                {
                    _Msg.success = true;
                    _Msg.msg = serializer.Serialize(_Bill);
                }
                else
                {
                    _Msg.success = false;
                }
            }
            return serializer.Serialize(_Msg);
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["User_Id"] == null)
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "salonmanager", "reception", "checkin", "checkout" };
                string[] AllowSalon = new string[] { "root", "admin" };
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }
                if (Array.IndexOf(AllowSalon, Permission) != -1)
                {
                    Perm_ShowSalon = true;
                    Perm_ViewAllData = true;
                }
              
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
            else if (Perm_ShowSalon)
            {
                TrSalon.Visible = true;
                Bind_Salon();
            }
        }

        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((System.Web.UI.WebControls.Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((System.Web.UI.WebControls.Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }

        public struct PrintRec
        {
            public string Key { get; set; }
            public string Value { get; set; }
            public int Height { get; set; }
        }

        public class BillService2 : BillService
        {
            public int billOrder { get; set; }
            public DateTime completeTime { get; set; }
            public string TeamColor { get; set; }
        }        
    }
}
