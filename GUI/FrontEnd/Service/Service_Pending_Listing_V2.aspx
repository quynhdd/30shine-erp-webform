﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Service_Pending_Listing_V2.aspx.cs" Inherits="_30shine.GUI.UIService.Service_Pending_Listing_V2" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>--%>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Service_Pending_Listing_V2.aspx.cs" Inherits="_30shine.GUI.UIService.Service_Pending_Listing_V2" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ServiceListing" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <script src="/Assets/js/check-notify.js"></script>
        <script src="/Assets/js/date.js"></script>
        <script src="/Assets/js/erp.30shine.com/service.bill.pending.js"></script>
        <style>
            .is-booking {
                background: #ff8080;
            }

            .is-inprocedure {
                background: #bbff99;
            }

            ._p2 {
                float: left;
            }

            .p-note {
                font-family: Roboto Condensed Regular !important;
                margin-left: 20px;
            }

                .p-note span {
                    width: 10px;
                    height: 10px;
                    display: block;
                    float: left;
                    position: relative;
                    top: 5px;
                    margin-right: 5px;
                }

            .customer-add .table-add td.map-edit .edit-wp {
                position: absolute;
                right: 3px;
                top: 24%;
                background: none;
                display: block;
            }

            .wanning {
                width: 10px !important;
                height: 10px !important;
                background-color: red;
                border-radius: 50%;
                position: absolute;
                bottom: 10px;
                right: 5px;
                -webkit-animation-name: wanning-ani;
                -webkit-animation-duration: 1s;
                animation-name: wanning-ani;
                animation-duration: 1s;
                animation-iteration-count: 60;
            }

            @keyframes wanning-ani {
                0% {
                    opacity: 1
                }

                50% {
                    opacity: 0
                }

                100% {
                    opacity: 1;
                }
            }
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Dịch vụ &nbsp;&#187; </li>
                        <li class="li-listing active"><a href="/dich-vu/danh-sach.html">Danh sách</a></li>
                        <li class="li-pending active">
                            <a href="/dich-vu/pending.html">
                                <div class="pending-1"></div>
                                Pending</a>
                        </li>
                        <%--<li class="li-add"><a href="/dich-vu/them-phieu-v3.html">Thêm mới</a></li>--%>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row">
                    <strong class="st-head"><i class="fa fa-filter"></i>Lọc kết quả</strong>
                    <%--<div class="filter-item">
                        <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion" data-field="customer.code" data-value="0"
                            AutoCompleteType="Disabled" ID="CustomerCode" ClientIDMode="Static" placeholder="Mã Khách hàng" runat="server"></asp:TextBox>
                        <div class="listing-staff-wp eb-select-data ">
                            <ul class="ul-listing-staff ul-listing-suggestion" id="UlCustomerCode"></ul>
                        </div>
                    </div>
                    <div class="filter-item">
                        <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion" data-field="customer.name" data-value="0"
                            AutoCompleteType="Disabled" ID="CustomerName" ClientIDMode="Static" placeholder="Nhập tên Khách hàng" runat="server"></asp:TextBox>
                        <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                            <ul class="ul-listing-staff ul-listing-suggestion" id="UlCustomerName">
                            </ul>
                        </div>
                    </div>

                    <div class="filter-item">
                        <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion" data-value="0" data-field="customer.phone"
                            AutoCompleteType="Disabled" ID="CustomerPhone" ClientIDMode="Static" placeholder="Số điện thoại" runat="server"></asp:TextBox>
                        <div class="listing-staff-wp eb-select-data">
                            <ul class="ul-listing-staff ul-listing-suggestion" id="UlCustomerPhone"></ul>
                        </div>
                    </div>--%>
                    <asp:DropDownList ID="ddlSalon" CssClass="form-control select" runat="server" Style="width: 180px; margin-left: 10px; margin-bottom: 10px; margin-top: 5px;" onchange="excPaging(1)" ClientIDMode="Static">
                    </asp:DropDownList>
                    <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata form-control" Style="width: 100px;" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <%--<a href="javascript://" class="st-head btn-viewdata" onclick="location.href=location.href">Reset Filter</a>--%>
                </div>

                <!-- End Filter -->
                <div class="pending-content-wrap" style="width: 1000px; margin: 0 auto;">
                    <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                    <%--<p class="_p1" style="width: 100%;">Danh sách chờ hoàn tất</p>--%>
                    <!-- End Row Table Filter -->
                    <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                    <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                        <ContentTemplate>
                            <% if (Salon != null)
                                { %>
                            <p class="_p2"><i class="fa fa-map-marker"></i><%=Salon.Name %></p>
                            <p class="_p2 p-note"><span class="is-booking"></span>Khách book lịch</p>
                            <p class="_p2 p-note"><span class="is-inprocedure"></span>Khách book lịch đã vào gội</p>
                            <p class="_p2 p-note">
                            </p>
                            <% } %>
                            <div class="table-wp">
                                <table class="table-add table-listing table-pending" id="tablePending">
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Tên Khách Hàng</th>
                                            <th>Số điện thoại</th>
                                            <%--<th>Thời gian chờ</th>--%>
                                            <th>Checkin</th>
                                            <th class="item-timewait">Thời gian chờ</th>
                                            <%--<th>Team</th>--%>
                                            <th class="item-inprocedure">Giờ gội</th>
                                            <th class="item-skinner">Skinner</th>
                                            <th class="item-stylish">Stylist</th>
                                            <%--<th class="item-stylish">Thời gian <br />ước lượng cắt</th>--%>
                                            <th>Print</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="RptBillPending" runat="server" OnItemDataBound="Repeater_ItemDataBound">
                                            <ItemTemplate>
                                                <tr
                                                    <%# (Eval("BookingId") != null && Convert.ToInt32(Eval("BookingId")) > 0) ? (Eval("InProcedureTime") != null ? "class='is-inprocedure'" : "class='is-booking'") : "" %>>
                                                    <td><a href="<%=Salon != null && Salon.IsSalonHoiQuan == true ? "/dich-vu/checkout-club/" : "/dich-vu/checkout-v1/"%><%# Eval("Id") %>.html" style="font-size: 15px; font-family: Roboto Condensed Bold;" target="_blank"><%# Eval("billOrder") %></a></td>
                                                    <td><a href="<%=Salon != null && Salon.IsSalonHoiQuan == true ? "/dich-vu/checkout-club/" : "/dich-vu/checkout-v1/"%><%# Eval("Id") %>.html" target="_blank"><%# Eval("CustomerName") %></a>
                                                    </td>
                                                    <td>
                                                        <%if (Perm_ViewPhone)
                                                            { %>
                                                        <a href="<%=Salon != null && Salon.IsSalonHoiQuan == true ? "/dich-vu/checkout-club/" : "/dich-vu/checkout-v1/"%><%# Eval("Id") %>.html" target="_blank"><%# Eval("CustomerPhone").ToString() %></a>
                                                        <%} %>
                                                        <%else
                                                            { %>
                                                        <a href="<%=Salon != null && Salon.IsSalonHoiQuan == true ? "/dich-vu/checkout-club/" : "/dich-vu/checkout-v1/"%><%# Eval("Id") %>.html" target="_blank"><%# Library.Format.ReplaceFirstPhone(Eval("CustomerPhone").ToString()) %></a>
                                                        <%} %>
                                                        
                                                    </td>
                                                    <%--<td>
                                                        <a href="<%=Salon != null && Salon.IsSalonHoiQuan == true ? "/dich-vu/checkout-club/" : "/dich-vu/checkout-v1/"%><%# Eval("Id") %>.html">
                                                            <%# string.Format("{0:HH}",Eval("TimeWaitAtSalon")) %><%# Eval("TimeWaitAtSalon") != null ? "h" : "" %><%# string.Format("{0:mm}",Eval("TimeWaitAtSalon")) %>
                                                        </a>
                                                    </td>--%>
                                                    <td>
                                                        <a href="<%=Salon != null && Salon.IsSalonHoiQuan == true ? "/dich-vu/checkout-club/" : "/dich-vu/checkout-v1/"%><%# Eval("Id") %>.html" target="_blank">
                                                            <%# string.Format("{0:HH}",Eval("CreatedDate")) %>h<%# string.Format("{0:mm}",Eval("CreatedDate")) %></a></td>
                                                    <td class="item-timewait clock" data-time="<%# Eval("CreatedDate")%>" data-inprocedure="<%# Eval("InProcedureTime") %>" data-booking="<%# Eval("DateTimeBook") %>" data-order="<%# Eval("Id") %>">
                                                        <label class="minute-<%# Eval("Id") %>">00</label>:<label class="second-<%# Eval("Id") %>">00</label>
                                                    </td>
                                                    <%--<td>
                                                        <a href="javascript:void(0);" class="a-team-color">
                                                            <div class="pending-team-color" style="background: <%# Eval("TeamColor") %>!important" onclick="showBoxColorEdit($(this))"></div>
                                                            <div class="team-color-edit-box">
                                                                <%foreach (var v in lstTeamService)
                                                                    { %>
                                                                <div class="color-box-edit" style="background: <%= v.Color %>!important" onclick="updateTeamColor($(this),<%= v.Id %>,'<%=v.Color %>', <%# Eval("Id") %>)"></div>
                                                                <% } %>
                                                            </div>
                                                        </a>
                                                    </td>--%>
                                                    <td class="item-inprocedure"><%# Eval("InProcedureTime") != null ? String.Format("{0:HH:mm:ss}", Eval("InProcedureTime")) : "" %></td>

                                                    <%--                                                    <%# Eval("SkinnerName")!=null ? "<td class="+"item-skinner"+">"+ Eval("SkinnerName") +"</td>" : "<td class="+"' item-skinner'"+"><span class="+"'wanning'"+"></td>"%>--%>
                                                    <%# Eval("SkinnerName")!=null ? "<td class="+"item-skinner"+">"+ Eval("SkinnerName") +"</td>" : "<td class="+"' item-skinner'"+"></td>"%>
                                                    <%--  <%# Eval("Condition") ==null ? "<td class="+"' item-stylist'"+">"+ Eval("StylistName") +"<span class="+"'wanning'"+"></td>"   : "<td class="+"item-stylist"+">"+ Eval("StylistName") +"</td>"%>--%>

                                                    <td class="item-stylist"><%# Eval("StylistName") != null ? (Eval("StylistName")) : " "  %></td>
                                                    <%-- <td>
                                                        <%#Eval("EstimateTimeCut") %>
                                                    </td>--%>
                                                    <td class="map-edit">
                                                        <div class="be-report-price" style="padding: 0 5px;"></div>
                                                        <div class="edit-wp">
                                                            <a class="edit-action-wp print-btn" style="display: block;" href="javascript://" onclick="Print_Bill_V2(<%#Eval("Id") %>)" title="In">
                                                                <i class="fa fa-print" style="font-size: 20px; position: relative; top: 2px; left: 0px; margin-right: 6px; color: #000000;"></i>
                                                            </a>
                                                            <asp:Panel CssClass="edit-action-wp action-edit" runat="server" ID="EAedit" Visible="true">
                                                                <a class="elm edit-btn" href="<%=Salon != null && Salon.IsSalonHoiQuan == true ? "/dich-vu/checkout-club/" : "/dich-vu/checkout-v1/"%><%# Eval("Id") %>.html" title="Chi tiết" target="_blank"></a>
                                                            </asp:Panel>
                                                            <asp:Panel CssClass="edit-action-wp action-delete" runat="server" ID="EAdelete" Visible="false">
                                                                <a class="elm del-btn" onclick="del(this.parentNode.parentNode.parentNode.parentNode,'<%# Eval("Id") %>', '<%# Eval("BillCode") %>')" href="javascript://" title="Xóa"></a>
                                                            </asp:Panel>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>

                                    </tbody>
                                </table>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                <!-- Hidden Field-->
                <asp:HiddenField ID="HDF_Suggestion_Code" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_Suggestion_Field" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_Bill_Id" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_SalonId" runat="server" ClientIDMode="Static" />
                <!-- END Hidden Field-->
            </div>
            <%-- END Listing --%>
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
                z-index: 9999;
                float: left !important;
                margin-top: 5px !important;
                margin-left: 10px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
                z-index: 9999;
                float: left !important;
            }

                .select2-container--default .select2-selection--single .select2-selection__rendered {
                    width: 180px !important;
                }
        </style>
        <!-- In hóa đơn -->
        <iframe id="iframePrint" style="display: none;"></iframe>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                var qs = getQueryStrings();
                if (qs["msg_print_billcode"] != undefined) {
                    openPdfIframe("/Public/PDF/" + qs["msg_print_billcode"] + ".pdf");
                }
                showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);

            });
            function openPdfIframe(src) {
                var PDF = document.getElementById("iframePrint");
                PDF.src = src;
                PDF.onload = function () {
                    PDF.focus();
                    PDF.contentWindow.print();
                    PDF.contentWindow.close();
                }
            }

            $(document).ready(function () {
                $("#ViewDataFilter").click();
                //setInterval(function () {
                //    excPaging(1);
                //}, 60000)
            })

            function Print_Bill_V2(billId) {
                $.ajax({
                    url: '/GUI/FrontEnd/Service/Service_Pending_Listing_V2.aspx/GetDataPrint',
                    data: '{billId:' + billId + '}',
                    dataType: 'json',
                    contentType: 'application/json;charset=UTF-8',
                    type: 'POST',
                    success: function (response) {
                        billResponse = JSON.parse(response.d);
                        // create array service
                        var arrServices = [];
                        for (var i = 0; i < billResponse.lsService.length; i++) {
                            arrServices.push({
                                'Dịch vụ': billResponse.lsService[i].Name,
                                'SL': ' '
                            })
                        };
                        arrServices.push({
                            'Dịch vụ': ' ',
                            'SL': ' '
                        });
                        arrServices.push({
                            'Dịch vụ': ' ',
                            'SL': ' '
                        });
                        arrServices.push({
                            'Dịch vụ': ' ',
                            'SL': ' '
                        });
                        //print bill with javascript
                        let imageStar = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAMDAwMDAwQEBAQFBQUFBQcHBgYHBwsICQgJCAsRCwwLCwwLEQ8SDw4PEg8bFRMTFRsfGhkaHyYiIiYwLTA+PlQBAwMDAwMDBAQEBAUFBQUFBwcGBgcHCwgJCAkICxELDAsLDAsRDxIPDg8SDxsVExMVGx8aGRofJiIiJjAtMD4+VP/CABEIACsALAMBIgACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAJAAgEBwoFBv/aAAgBAQAAAABU4C6jWilTazlXYxJLJjNVWTPWxBebD46LInXl8v28dQpSrOEVCwaoqIi3/8QAFAEBAAAAAAAAAAAAAAAAAAAAAP/aAAgBAhAAAAAAf//EABQBAQAAAAAAAAAAAAAAAAAAAAD/2gAIAQMQAAAAAH//xAAyEAABBAECAwYEBQUAAAAAAAACAQMEBQYHEQAIEgkQEyExURRBQlIVIGFzkTJDRIKi/9oACAEBAAE/AO7m71snam6vT2q2xfWkx15YVajbqoBuMrs7JH9TP0L7eOUXWNzV3SGA5YyVfu6PausiJdzcVtN2ny/dD/rfvy/nY0Bw6yl1x3MuzlQ3TaeGBEJ4esPtcLoAuNTe0NorfFrqpw3HLiLPmw3Y8axmOtNLHV1NvFEG1c3Pu5atfpugOZTLRYLllV2UL4edBB3wlNQXqacFfvDih7RHRWw6W7KsyKqL3KM3ID+W3N+MM190azylS1qMxrUY8UmiGU6kN0TFEXZW3+guOZzlnvNCMgWZCR2bilg+qV87bdWCX/Gkexp8i+v8lZWWFzYxa6uiuy5kt0GY8dkFcccccXYREU81JeNLuz4w0cOhu6hS55X7+7r7EB9AajCXozvsXWY/MuMoxiizOhsaO8rmZ9dPZJqRGdHcSFfP5ehJ6iSeaLxqHT0+PZ7k9PTOPuV1dczIsRx8kVw2mHSAVJRRN1Xbv7OzAcHlUF7mxxfiMkiWZwGnHtlGKwrIH1sexn1bKXdeWjFJSWdo9t4UCE/Jc3+1kFNeJkt+fMflvF1Ovum64XuTi7qv8rxpxy0az6qVUi3x7GnigNMEbUmSYxQlKn0MK5t1kXGQ41kGJW0ipvayXWz2C2djSWiaNP12JOOzWyFG5+fUB/3mIE1of2jNtzusIUSyhSYUxkH40pp1h9o03FxtwSEhJPZU9eOVDRzTO+1bztq1xyLPaoJRfhrMkjeba2f6PMDLY/8AfhpttoBBsBAAFBERTZERE8kRONY9NsD1Ew20byehh2Sw4Eh6M64Ki8yYBuituhsY8dnxi1BBoczu2IQDYlPbhfEdREXgf19Hd//EABQRAQAAAAAAAAAAAAAAAAAAADD/2gAIAQIBAT8AH//EABQRAQAAAAAAAAAAAAAAAAAAADD/2gAIAQMBAT8AH//Z';
                        let imageSystem = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAABGCAMAAABG8BK2AAABAlBMVEVHcExEREBEREBFRUFFRUFFRUFFRUFEREBEREBEREBEREBGRkEAAAAlJSMwMC0BAQEBAQEBAQEBAQEqKicBAQEDAwMaGhgPDw4CAgIBAQECAgELCwoBAQEFBQQMDAsPDw4UFBMODg0AAAAHBwcZGRgWFhUGBgYBAQEAAAAZGRcbGxkDAwMxMS4vLy0BAQELCwoEBAQgIB4YGBYBAQEyMi8mJiQmJiQdHRsLCwo5OTYFBQQMDAwEBAMKCgofHx0gIB4ODg0SEhEbGxkRERAjIyEYGBcZGRcqKigWFhQlJSMDAwMREQ8vLysuLiwAAAADAwMBAQECAgIDAwICAgECAgMGBgX0Mw3/AAAATnRSTlMAAwQHAQkFCwoCCAbPFxPn4vn1K+uYNU3wpstWtNjDZwmEuos9JG2u/Rxjwz0jkstzW4HdSk8Mb+IQeq2g1QF3qbqOtX6YpFdFMGGYZW5hflvKAAAHcUlEQVRYw42Y13bcuBJFN0gQBMlu5RysK1mOkpxkyzmGsSfcA0Ca+f9fuQ9ks9kKntuPXFi7qwqnUAEKh8+srypvM48raGrqhiLDm8pXxtusIDMALuuPGo8ryGvqnMJBkeOdLbOqrExpnScvqGuKhtLZ0lRllZUVg1+TTT7TePKCoqHGUThKY8uqKsk8jSevKRp8+9maEl7fPng8Gt3aWf24fgbYqrSmpPvPHN+Ad5TZlFLMUipTYnikICVFSfP3Xr4GWkrdUhz4hjKjpfRm5kOK87yTopSkEJW08v0B2KktjgKDb22xU4+GFOs8bnxvd+vo1mhhsWVp8fsZme0ppWPikc087npKU4AF8OPDl0eSYtD8U5iY7TOmtpQ3Ujy1bTkAJ0/mpRT0Duoa7/DGeio7jMuQYquS1pa6pnDUGWVVGaj3NqSkrTkmRw2tLf31FTm+wWeUlZlcXV33puAq42FuU0lamCNv7wd8VZXWeJynsb2zQwHUNVn9x6NPq/t7h2OApoCHUtD8WXfUQ08BgOsoOY8kJSlu7PzIsOQVa0sKGmVUZVWVFWbiESe3jz5vgx3KqKCu8YZdKajV4MJDKMg5WYpK93CmKm2Fn1COl5Skt2SO/uqKLujm5+nGaauaeK6DE7Ia1hSi9qC0xk9kzdyizhWinkEvoz4xMuqzD+X4xbcDSSkuboPPeCqF+2OMKcG1d8QTJYULhY2aiaQnlNLZSeT4fVVJSXs0NmdVUV+hxFDgPE3Nn1HhQkFLOQBQXRUjLoPDkc6lp+QNrChoDes8uJK8gGcKFwpRG2f/eb79x481A9QUrTCMMQDk5A3sKEQtYz3rivEzeLqcrhmHpBAVNL8iSXp1b89gO1uqnO2Pe+tz0FDDVyksUGUVI4X7d8BR0BTUOazNh6gQY5CCYpC0uzdReyscrWweQg1sxaRnULIuxS+4gu7RaGCro8Sgi6B0rqR7r7GlsSX8oxgUox5B0TCWgs5sZet5pRGG9gGzsD66aJ3qKSEpnZ6QVWWV8UaSopIOwBa8UYzPqGr2o+IyDXWbeftKlyjnSSFpZa6VNGuHhy9HSoqfcA63qLRABS+U9ISalrJ5iZJ03spIuwZT4hoA7iokvYCC21FapmGsoN+Aoibj3YCShpQY0z0a26UXPJX+PqXKOJb0Bg+PlY6wNOTcvZbSfdNLqkmSOu4p6hjL2VLQV3CsxnjqgMLOxYsZShhSYtCDtgzUFDm/S/EJJbyP6TEU7EdtlOAb/lR/09dR4mOYUPgwn/SWqmJHadcAHxXuP4CM10szlHOlWUrQQyapDlsxPoaGzaiFjJq7MWoZ4OHNlM7RjZyiq6T2leIOjeWr0inUfIzSA6hZjTOU8yuUoNt0FO7cP9c+wFaMWwDPFDbOoOLWDXEZBF1juiL7SFHPyWDlXJ8h51HUaQPY930MbqIEfWqTtOKLtFRVnmVJtykyDmJ8DPDh9N8p51qmocysM3/urFHANyWt0TC3qLRJDtV8nKFcTChx6OgtuvIIeDhbiRcbBvipoGdU0JzqXyghKeiQrnpZT8NYSb+B50mK8RgLjK71KPZXF5JC0i4MGjJu69UYb9iNcQE8tDfVptIsJQ4oQXcx00qasVziHc9jjKvUFsMn/R+UGP+ZY9DWZeAdvGtLg2mfjpsooaco6IAr7Qvb0i1wHnh5LeWip0zSX3fJ7WwTZFjfe03joWFNN9qShlcnPZ2melcDARpPRgErN8VllpKC3kxq4LRpnjSHGQfxsnavo8SgGI9+AjOtd0fJa27HayhBf1+GXwRJoydj8q6t6wXgqLEP++NxQBk4OqyB0tI6Vxsyah7EX1I0rF4pKeh33GVKUcPu5Qu6hpIGWfeeS21dTk7DZryZEq9SgsbMNmQ5FA37v4jLMOidGKU5ZhqyHIp8+Kj/ktK/I18ws21djcNzJ85Szq+V9MQjvb1sSzeWuXn9ijJ4vCS9/QET4dVtW+fadGcrDUz/FeXW3TtMKf0cWGAomakxNzwYknb3jwFMibk0NTkoDRzol5SkqJUvP2GqFzcz2YI3FkZpljKj6iRpc3sOJo/6FUo7ljG3GG+iJEmj/54Aw8nWXZpsDVVp+O9NtlxIr/bvAK7P6clk2w+axrZjGRxdKiYtJUkb746B55vL7XxsppPtZOj1phvLDD91VS+S7n9+AdxZfSUtt7bYfm8wM6tXpcVkH05T132mgSGjN2dw9tdIUlo8oxuyLq0UunDZCs8f0kVQ+ntCkXT6/QRY35GUFOIOtB7NUNxwEQC/DTrhqKSNt+sZHH7f6CvGX1RDSn2ZUoJhZ5p50v2Dh3PA9qLUU95ftmUyZfe2ZNSsJqWgEBW19e0BQMmypgJYGnMdpcwobb8IwB5LSefSwupPwFVVaR3jI3XZ+H4ZP6U0k+j2MmpH1Tzn6YK0sflXBrSLicwDd4/uS9r6xrQklR2lmFJMiStwFLYh+7E9BopuM9W+1PDgxfodYLCw6ubyBu86MU72WLgCAEPRLj+qytusoKkBgCKz3a6MrN+fDdZqBa7gf86ovl5qT37gAAAAAElFTkSuQmCC';
                        let imageCustomer = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABzCAMAAABZ71dFAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAHsUExURUdwTAQEBAoKCgMDAwEBASgoKFVVVSwsLB8fHwAAAF9fXy0tLWZmZhMTEyUlJTExMSoqKisrK4SEhAQEBBISEjAwMFFRUS0tLQcHBwcHB9fX1xMTEwgICDk5OWVlZSgoKB8fHwUFBQ0NDXJycg4ODhAQEElJSQYGBiIiIigoKCgoKBAQEDk5ORgYGD4+Ph0dHUhISDU1NUdHRwMDAxcXFyQkJGpqaiEhIR8fH0RERDY2NhEREQoKCgEBAQoKCiwsLG1tbQoKChISEjs7OxQUFBcXFy8vL1FRUSsrK2BgYAcHB0RERAYGBlNTU0dHRzY2NgICAi0tLRgYGGFhYVZWVhgYGCwsLBUVFVVVVZaWlikpKSMjIxAQEC0tLTc3Nx0dHRYWFgsLCxMTE0lJSTIyMkBAQCEhIQ4ODklJSRkZGRcXFz4+Pjk5OTo6OhUVFRsbG8HBwRoaGklJSZCQkE9PTz09PT09PScnJ0VFRQUFBU1NTQ0NDRgYGFJSUnZ2diMjIwAAACEhIQcHBwICAh0dHX9/fx8fH2RkZCMjI0VFRVBQUH19fVFRUUhISJmZmVtbW2JiYmlpaW9vbwoKCgoKCi0tLSEhIcjIyDs7Ozw8PEVFRUBAQAAAAAEBAQYGBgQEBAICAhISEggICAwMDIg+tMwAAACcdFJOUwDm5+vmFAMHCaYVmgfq7Fu+jxSmpapTOfuwCB34GQ1FwdE2JtSQnZ1Jsg9adsx3tWAfJfHtlD3R7lA10kX93NQsfCpc/MQRbHpYxz/vHoWDoHeveKhpbM6zVib+hoUt8nS98o3HNaQ/quCclp294vUG/eo66m7qxWf2ffNSyzWLlFJN9XlKppihy150SrgcistypL/h6t45/qSJViHSwlgAAAQ0SURBVBgZ7cBlX1tnAAXwA4wkuAQdTodb0Qqlpe7u7u5uc1/nbuckT8IXHaWsJV3kPjf3vtmvf7zxRgbNGy7urg6HwwUFBXuK+lpC8F5gzbWhX+oiRsZIJtJ4o+rAEXirYXWJ4Txx3lT92iA8MzwUFZMwLNkIjxyLk6LE14mMDuTDA+dLmVZ8MbLWPkMapiZGNiFL7RGSYhoiNyEry6foxF5kYTpGB0ROwL1yik6Y0kq4VUaKzpyFS81L6VhsGu6U0TGxE+6co+iQ4e1FcOP9GEXnyuDGCtFGOdzYSSvbeuHCQ9rZDBcmaacf9hbFaOc67K2J0ob4M+ytMbQh3gjA2n5aim2HtS9pRYw2wForLUUbYOu7o7QUbYCt7TFaESMNsHUiRitifBiWmlfSWhEsXaM9UwM7k3ThA1gJ5dGFKlgJ5YnWFIaVUJ5oTWFYCeXRnsKwEhyjC9Wws4oufA87nbSnWtippb2bhbBTXErRhsRDsLWDop3IBGxVltKCRF6CvelGSnRI5Hq4UbiKokPie3BnGZ0zn8GdwG90rBxu3adj3XCrsoSGmUlcAvcG6VAL3Ku8TDEjsRXZ6KYjR5CVcorpiSxCdu7GaZiW4VglsrSWGX2DrI2TYioyZAWyV3yQYioSy+GFYBtTEMmDxfBEsI0SkzBiWxAeCbaJyU0G4ZnQJCkmksglQXgo1MMkvm2GtwZjfE58KXcjPBc81ijOEueMdhbCDxt2H5wRRXJmoH8DfNP0bMepU4NfTyNrAZyouXjgp6vjHaeXLoIDW+u7rhR1ftG3oqYJjoTuX1p/5nHc8IWvbiGj39dRnCUTHy0Z2teCDGpbZ5johwAyuPsHKS6wrqMJqTVtiVDiKxI10Iu0Wm6T4gIiYx1IZVOMFBOIZHQ8HyktbyXFRBJ5tBZJrY4yhU8+L0My+f2no0wh3o4kVhsmZ0RyW/3VR+eDweK3n7tVjPytg0Vb6kQZMRlpqh3/8SkzMXV5Jb8+uHPv3p0HpbnbIkxPnFqO1/xIz5mVQSQoHKWYkSTqOUrMSNyDBNfpi71Y4Bn9caYQrwzQJ8fx0lr6Q3zahH8toW92Yt5m+udmIV6opo8WY85IjD56iDll9NPjJsxqLqfooyeVAFpGKfpGbP0bQAX99XQcQFj0lQkDKKDPCgAUSPSRWACgin4SI1UAdtBP4thfACZKKfrozwnMekQ/NW7HnAuk6JcyzKsSJXpNIiN9eOlshL44WYMFhs9FSYoeMeKsWAVeM9Kzjt4Rp04eb0YS/asaRU9Ecju6kUp+d/U7dcxONG99RW0I6Y08qxhaOWNEa8bMbOnZWdMLZ0LDy54cqqrPrYtHjZhIYiJjIvGZd3b17Nu4tTcIeyOHl727Yn/1la4P65eW5Obk5Lw1LycnJ3fs8q6urgtFfR9/tPlwfgBAAO4E8Mb/0z9r5GHf069dZgAAAABJRU5ErkJggg==';
                        let CusPhone = billResponse.cusPhone.toString().length;
                        let firstNoPhone = '';
                        let endNoPhone = '';
                        let replacePhone = '';
                        if (CusPhone == 10) {
                            firstNoPhone = billResponse.cusPhone.toString().substring(0, 6);
                            replacePhone = billResponse.cusPhone.toString().replace(firstNoPhone, 'xxxxxxx');
                        }
                        else if (CusPhone == 11) {
                            firstNoPhone = billResponse.cusPhone.toString().substring(0, 7);
                            replacePhone = billResponse.cusPhone.toString().replace(firstNoPhone, 'xxxxxxxx');
                        }
                        billResponse.cusPhone = replacePhone;
                        var currentdate = new Date();
                        var datetime = currentdate.getDate() + "/"
                            + (currentdate.getMonth() + 1) + "/"
                            + currentdate.getFullYear() + " "
                            + currentdate.getHours() + ":"
                            + currentdate.getMinutes();
                        // Luu ý margin [0,0,0,0] trái, trên, phải, dưới
                        var doc = {
                            content: [

                                {
                                    text: parseInt(billResponse.billCode.substring(8)), fontSize: 37
                                },
                                { text: 'Ngày ' + datetime, fontSize: 10, italics: true, margin: [3, 3, 3, 3] },
                                {
                                    columns: [
                                        {
                                            height: 15,
                                            width: 20,
                                            text: 'KH: ',
                                            margin: [0, 2, 0, 3]
                                        },
                                        {
                                            height: 30,
                                            width: 160,
                                            text: billResponse.cusName.toUpperCase(),
                                            margin: [0, 0, 0, 3],
                                            bold: true,
                                            fontSize: 14
                                        },
                                    ],
                                    columnGap: 3
                                },
                                {
                                    columns: [
                                        {
                                            width: 50,
                                            text: 'Số ĐT: ',
                                            margin: [0, 3, 0, 3],
                                        },
                                        {
                                            width: 100,
                                            text: billResponse.cusPhone,
                                            margin: [0, 3, 0, 3],
                                            bold: true,
                                            fontSize: 12
                                        },
                                    ],
                                    columnGap: 10
                                },
                                { text: '', fontSize: 3, margin: [0, 3, 0, 0] },
                                GetShineMember(billResponse.isShineMember, billResponse.memberEndTime),
                                {
                                    columns: [
                                        {
                                            width: 51,
                                            text: 'Giờ book:',
                                            margin: [0, 3, 0, 0]
                                        },
                                        {
                                            width: 110,
                                            text: billResponse.bookHour,
                                            margin: [0, 3, 0, 0]
                                        },
                                    ],
                                    columnGap: 10
                                },
                                {
                                    columns: [
                                        {
                                            width: 51,
                                            text: 'Giờ đến:',
                                            margin: [0, 3, 0, 0]
                                        },
                                        {
                                            width: 130,
                                            text: billResponse.timeCustomerCome,
                                            margin: [0, 3, 0, 0]
                                        },
                                    ],
                                },
                                { text: '', fontSize: 3, margin: [0, 5, 0, 0] },
                                GetNeedConsult(billResponse.needConsult),
                                { text: '', fontSize: 3, margin: [0, 5, 0, 0] },
                                getTextNoteTable(billResponse.textNote1, imageSystem, 'NOTE CỦA HỆ THỐNG'),
                                { text: '', fontSize: 3, margin: [0, 5, 0, 0] },
                                getTextNoteTable(billResponse.textNote2, imageCustomer, 'NOTE CỦA KHÁCH'),
                                {
                                    columns: [
                                        {
                                            width: 50,
                                            text: 'Skinner: ',
                                            margin: [0, 3, 0, 3]
                                        },
                                        {
                                            width: 100,
                                            text: billResponse.skinnerName,
                                            margin: [0, 3, 0, 3]
                                        },
                                    ],
                                    columnGap: 10
                                },
                                {
                                    columns: [
                                        {
                                            width: 50,
                                            text: 'Checkin: ',
                                            margin: [0, 3, 0, 3]
                                        },
                                        {
                                            width: 100,
                                            text: billResponse.checkinName,
                                            margin: [0, 3, 0, 3]
                                        },
                                    ],
                                    // optional space between columns
                                    columnGap: 10
                                },
                                {
                                    columns: [
                                        {
                                            width: 40,
                                            text: 'Stylist: ',
                                            margin: [0, 3, 0, 3]
                                        },
                                        {
                                            width: 120,
                                            text: billResponse.IsAutoStylist == false ? billResponse.stylistName + ' (B)' : billResponse.stylistName,
                                            margin: [0, 3, 0, 3]
                                        },
                                    ],
                                    columnGap: 10
                                },
                                table(arrServices, ['Dịch vụ', 'SL']),
                                {
                                    columns: [
                                        {
                                            text: 'Đánh giá: ', width: 50, margin: [0, 10, 0, 3]
                                        },
                                        {
                                            image: imageStar,
                                            width: 14,
                                            height: 14,
                                            margin: [0, 11, 0, 0]
                                        },
                                        {
                                            image: imageStar,
                                            width: 14,
                                            height: 14,
                                            margin: [0, 11, 0, 1]
                                        },
                                        {
                                            image: imageStar,
                                            width: 14,
                                            height: 14,
                                            margin: [0, 11, 0, 1]
                                        },
                                        {
                                            image: imageStar,
                                            width: 14,
                                            height: 14,
                                            margin: [0, 11, 0, 1]
                                        },
                                        {
                                            image: imageStar,
                                            width: 14,
                                            height: 14,
                                            margin: [0, 11, 0, 1]
                                        }
                                    ],
                                    columnGap: 10
                                },
                                {
                                    columns: [
                                        {
                                            text: '', width: 50, margin: [0, 0, 0, 3]
                                        },
                                        {
                                            text: '1',
                                            width: 14,
                                            height: 14,
                                            margin: [0, 0, 0, 2],
                                            alignment: 'center'
                                        },
                                        {
                                            text: '2',
                                            width: 14,
                                            height: 14,
                                            margin: [0, 0, 0, 3],
                                            alignment: 'center'
                                        },
                                        {
                                            text: '3',
                                            width: 14,
                                            height: 14,
                                            margin: [0, 0, 0, 3],
                                            alignment: 'center'
                                        },
                                        {
                                            text: '4',
                                            width: 14,
                                            height: 14,
                                            margin: [0, 0, 0, 3],
                                            alignment: 'center'
                                        },
                                        {
                                            text: '5',
                                            width: 14,
                                            height: 14,
                                            margin: [0, 0, 0, 3],
                                            alignment: 'center'
                                        }
                                    ],
                                    columnGap: 10
                                },
                                {
                                    text: ' \n\n\n\n .'
                                }
                            ],
                            pageMargins: [20, 20, 20, 20],
                            pageSize: 'letter',
                            pageOrientation: 'portrait'
                        };
                        pdfMake.createPdf(doc).print();
                        //pdfMake.createPdf(doc).print({},window);
                    }
                })
            }
            // Hiện thông tin shine member
            function GetShineMember(isShineMember, memberDate) {
                if (typeof isShineMember == 'undefined' || isShineMember == 0) {
                    return {};
                }
                else {
                    return {
                        table: {
                            headerRows: 1,
                            widths: [168],
                            body: [[{ text: 'Member: ' + memberDate }]]
                        }
                    }
                }

            }
            // Hiện thông tin khách cần tư vấn kỹ
            function GetNeedConsult(needConsult) {
                if (typeof needConsult == 'undefined' || needConsult == '') {
                    return {};
                }
                else {
                    return {
                        table: {
                            headerRows: 1,
                            widths: [168],
                            body: [[{ text: needConsult }]]
                        }
                    }
                }

            }
            // Hiện thông tin chiến dịch. và thông tin khách khi book lịch textnote1; của hệ thống
            function getTextNoteTable(textNote, image, caption) {
                if (typeof textNote != 'undefined' && textNote.trim() != "") {
                    return {
                        table: {
                            headerRows: 1,
                            widths: [10, 150],
                            body: [
                                [{ image: image, width: 15, height: 15 }, { text: caption, margin: [3, 5, 0, 0], fontSize: 9, decoration: 'underline', }],
                                [{ text: textNote, colSpan: 2 }, {}]
                            ]
                        },
                        layout: {
                            hLineWidth: function (i, node) {
                                return (i === 0 || i === node.table.body.length) ? 1 : 0;
                            },
                            vLineWidth: function (i, node) {
                                return (i === 0 || i === node.table.widths.length) ? 1 : 0;
                            },
                        },
                    }
                }
                else {
                    return {};
                }
            }
            // pdfmake
            function buildTableBody(data, columns) {
                var body = [];

                body.push(columns);

                data.forEach(function (row) {
                    var dataRow = [];

                    columns.forEach(function (column) {
                        dataRow.push(row[column].toString());
                    })

                    body.push(dataRow);
                });

                return body;
            }

            function table(data, columns) {
                return {
                    table: {
                        headerRows: 1,
                        widths: [140, 20],
                        body: buildTableBody(data, columns)
                    }
                };
            }
        </script>
        <!--/ In hóa đơn -->

    </asp:Panel>
</asp:Content>

