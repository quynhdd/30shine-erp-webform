﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using Excel;
using ExportToExcel;
using LinqKit;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.BO;
using _30shine.MODEL.CustomClass;
using System.Web.Services;
using System.Configuration;

namespace _30shine.GUI.UIService
{
    public partial class Service_Listing_V2 : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        protected List<List<ProductBasic>> ProductList;
        CultureInfo culture = new CultureInfo("vi-VN");

        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        protected bool Perm_ViewPhone = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;

        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

        protected string table = "";
        protected cls_totalReport totalReport = new cls_totalReport();
        protected WaitTimeListing totalWaitTime = new WaitTimeListing();
        private DateTime timeFrom;
        private DateTime timeTo;
        private int salonId;
        private int integer;
        private List<Store_Bill_Listing_Result> data;
        protected static List<ServiceRatingView> serviceRatingList;
        private static Service_Listing_V2 _Instance;

        private Solution_30shineEntities db;

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ViewAllDate = permissionModel.CheckPermisionByAction("Perm_ViewAllDate", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewPhone = permissionModel.CheckPermisionByAction("Perm_ViewPhone", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        public Service_Listing_V2()
        {
            db = new Solution_30shineEntities();
            data = new List<Store_Bill_Listing_Result>();
        }

        protected string RatingLabel(int billServiceMark, int billServiceId)
        {
            var Label = "";

            switch (billServiceMark)
            {
                case 2:
                    var RatingList = ServiceRatingModel.Instance.GetRatingByBillId(billServiceId).Where(Item => Item.Slug == "khong-danh-gia").ToList();
                    var NotRatedItem = RatingList.Find(delegate (ServiceRatingView Item)
                    {
                        return Item.BillServiceId == billServiceId;
                    });

                    if (NotRatedItem != null)
                    {
                        //Label = NotRatedItem.Title;
                        Label = "Không đánh giá";
                    }
                    else
                    {
                        Label = "Hài lòng";
                    }
                    break;
                case 3:
                    Label = "Rất hài lòng";
                    break;
                case 1:
                    Label = "Chưa hài lòng";
                    break;
            }
            return Label;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList>() { Salon }, Perm_ViewAllData);
            }
            else
            {
                //RemoveLoading();
            }
        }

        /// <summary>
        /// thống kê bill chờ
        /// </summary>
        /// <returns></returns>
        private WaitTimeListing getWaitTimeBill()
        {
            try
            {
                totalWaitTime = new WaitTimeListing();
                using (var dbBi = new Solution_30shineEntities())
                {
                    if (TxtDateTimeFrom.Text != "")
                    {

                        timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                        if (TxtDateTimeTo.Text != "")
                        {
                            timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                        }
                        else
                        {
                            timeTo = timeFrom.AddDays(1);
                        }
                        salonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
                        string sql = $@"
                                        DECLARE
	                                        @timeFrom VARCHAR(10), 
	                                        @timeTo VARCHAR(10), 
	                                        @salonId INT
                                            SET @timeFrom = '{String.Format("{0:yyyy/MM/dd}", timeFrom)}'
	                                        SET @timeTo = '{String.Format("{0:yyyy/MM/dd}", timeTo)}'
	                                        SET @salonId = {salonId}

	                                    BEGIN
		                                        SELECT SUM(CASE WHEN (WaitTimeInProcedure < 960) THEN 1 ELSE 0 END) AS BillUnder15,--900 giay tuong duong 15phut
		                                        SUM(CASE WHEN WaitTimeInProcedure >= 960 OR tbl.WaitTimeInProcedure IS NULL  THEN 1 ELSE 0 END) AS BillOver15,--900 giay tuong duong 15phut
		                                        SUM(CASE WHEN (WaitTimeInProcedure = '') THEN 1 ELSE 0 END) AS BillNotTime
		                                        FROM (
			                                        SELECT bill.Id AS BillId, --bill.BookingId, bill.CreatedDate, bill.CompleteBillTime, bill.InProcedureTime, (b.DatedBook + c.HourFrame) AS DateBook,
					                                        CASE 
						                                        WHEN (bill.BookingId IS NOT NULL AND (b.IsBookAtSalon IS NULL OR b.IsBookAtSalon <> 1) AND bill.CreatedDate < b.DatedBook + c.HourFrame) -- check in sau h book
						                                        THEN DATEDIFF(SECOND, bill.CreatedDate, bill.InProcedureTime)

						                                        WHEN (bill.BookingId IS NOT NULL AND (b.IsBookAtSalon IS NULL OR b.IsBookAtSalon <> 1) AND b.DatedBook + c.HourFrame < bill.CreatedDate) -- check in trc h book
						                                        THEN DATEDIFF(SECOND, b.DatedBook + c.HourFrame, bill.InProcedureTime)

						                                        WHEN (bill.BookingId IS NULL OR b.IsBookAtSalon = 1 OR (b.DatedBook +  c.HourFrame) IS NULL) -- khách k book
						                                        THEN DATEDIFF(SECOND, bill.CreatedDate, bill.InProcedureTime)

						                                        ELSE ''
					                                        END AS WaitTimeInProcedure
				                                        FROM BillServiceHis AS bill
				                                        INNER JOIN booking b ON bill.BookingId = b.Id
				                                        INNER JOIN  dbo.BookHour_Sub c ON b.SubHour = c.SubHourId
				                                        WHERE bill.IsDelete = 0 AND bill.Pending = 0
				                                        AND LEN(bill.ServiceIds) > 0
				                                        AND bill.CompleteBillTime BETWEEN @timeFrom AND @timeTo
				                                        AND bill.SalonId = @salonId
														) AS tbl
	                                        END";
                        // Chuyen database sang bi
                        dbBi.Database.Connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLConnectionStringBI"].ConnectionString;
                        //var list = dbBi.Store_Bill_WaitTimeListing(String.Format("{0:yyyy/MM/dd}", timeFrom), String.Format("{0:yyyy/MM/dd}", timeTo), salonId).SingleOrDefault();
                        totalWaitTime = dbBi.Database.SqlQuery<WaitTimeListing>(sql).SingleOrDefault();
                    }
                }
                return totalWaitTime;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private List<BillListting_cls> getData()
        {
            try
            {
                using (var dbBi = new Solution_30shineEntities())
                {

                    var data = new List<BillListting_cls>();
                    if (TxtDateTimeFrom.Text != "")
                    {

                        timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                        if (TxtDateTimeTo.Text != "")
                        {
                            timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                        }
                        else
                        {
                            timeTo = timeFrom.AddDays(1);
                        }
                        salonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
                        //var list = db.Store_Bill_Listing_V3(String.Format("{0:yyyy/MM/dd}", timeFrom), String.Format("{0:yyyy/MM/dd}", timeTo), salonId).ToList();
                        string sql =
                            @"  DECLARE
	                            @timeFrom NVARCHAR (10),
	                            @timeTo NVARCHAR (10),
	                            @salonId INT 
	                            SET @timeFrom = '" + String.Format("{0:yyyy/MM/dd}", timeFrom) + @"'
	                            SET @timeTo ='" + String.Format("{0:yyyy/MM/dd}", timeTo) + @"'
	                            SET @salonId = " + salonId + @"
                                BEGIN
	                             WITH  a AS
	                                (
		                                SELECT  bill.Id AS  BillId, bill.CreatedDate, bill.CompleteBillTime, bill.InProcedureTime, bill.IsBooking, bill.IsPayByCard, bill.TotalMoney, 
                                                bill.Staff_Hairdresser_Id AS  StylistId, bill.Staff_HairMassage_Id AS  SkinnerId, bill.ReceptionId AS  CheckinId, bill.CheckoutId, bill.SecurityId,bill.SellerId, bill.CustomerId, 
                                                bill.Images, bill.Mark, CAST(detail.StarNumber AS NVARCHAR(25)) + ' sao' AS Star,relationRating.Id AS RelationId,bill.BookingId,bill.Note,  bill.CustomerCode1, bill.IsX2, 
			                                    CASE 
				                                    WHEN (bill.BookingId IS NOT NULL AND (b.IsBookAtSalon IS NULL OR b.IsBookAtSalon != 1) AND bill.CreatedDate > Convert(DATETIME, CONVERT(NVARCHAR(10), b.DatedBook, 110) + ' ' + CONVERT(NVARCHAR(10), c.HourFrame))) -- check in sau h book
					                                    THEN DATEDIFF(minute, bill.CreatedDate, bill.InProcedureTime) 
				                                    WHEN (bill.BookingId IS NOT NULL AND (b.IsBookAtSalon IS NULL OR b.IsBookAtSalon != 1) AND Convert(DATETIME, CONVERT(NVARCHAR(10), b.DatedBook, 110) + ' ' + CONVERT(NVARCHAR(10), c.HourFrame)) > bill.CreatedDate) -- check in trc h book
					                                    THEN DATEDIFF(minute, Convert(DATETIME, CONVERT(NVARCHAR(10), b.DatedBook, 110) + ' ' + CONVERT(NVARCHAR(10), c.HourFrame)), bill.InProcedureTime)
				                                    WHEN (bill.BookingId IS NULL OR b.IsBookAtSalon = 1 or Convert(DATETIME, CONVERT(NVARCHAR(10), b.DatedBook, 110) + ' ' + CONVERT(NVARCHAR(10), c.HourFrame)) IS NULL) -- khách k book
					                                    THEN DATEDIFF(MINUTE, bill.CreatedDate, bill.InProcedureTime)
				                                    ELSE  NULL
			                                    END  AS  WaitTimeInProcedure ,
			                                    DATEDIFF(MINUTE, bill.CreatedDate, bill.CompleteBillTime) AS TotalCompleteTime, ServiceId = NULL, ServiceName = NULL, ProductId = NULL, ProductName = NULL, 
                                                Price = NULL, Quantity = NULL, VoucherPercent = NULL, PromotionMoney = NULL, stylist.Fullname AS StylistName, skinner.Fullname AS SkinnerName, 
                                                checkin.Fullname AS CheckinName, checkout.Fullname AS CheckoutName, [security].Fullname AS SecurityName, seller.Fullname AS SellerName, b.CustomerName AS CustomerName, b.CustomerPhone AS CustomerPhone, ListServices = '', ListProducts = '', bill.ProductIds AS ProductIds
		                                FROM BillServiceHis AS bill
			                                    LEFT JOIN Staff AS stylist ON stylist.Id = bill.Staff_Hairdresser_Id
			                                    LEFT JOIN Staff AS skinner ON skinner.Id = bill.Staff_HairMassage_Id
			                                    LEFT JOIN Staff AS checkin ON checkin.Id = bill.ReceptionId
			                                    LEFT JOIN Staff AS checkout ON checkout.Id = bill.CheckoutId
											    LEFT JOIN Staff AS [security] ON [security].Id = bill.SecurityId
											    LEFT JOIN Staff AS seller ON seller.Id = bill.SellerId
			                                    --LEFT JOIN Customer AS customer ON customer.Id = bill.CustomerId
			                                    LEFT JOIN booking b ON bill.BookingId = b.Id
			                                    LEFT JOIN BookHour c ON b.HourId = c.Id
			                                    LEFT JOIN dbo.RatingDetail AS detail ON detail.BillId = bill.Id
			                                    LEFT JOIN dbo.Service_Rating_Relationship AS relationRating ON relationRating.BillServiceId = bill.Id
		                                WHERE bill.IsDelete != 1 AND bill.Pending != 1 AND bill.CompleteBillTime BETWEEN @timeFrom AND @timeTo AND bill.SalonId = @salonId
	                                ),
	                                b AS
	                                (
		                                SELECT  bill.Id AS BillId, CreatedDate = NULL, CompleteBillTime=NULL, InProcedureTime=NULL, IsBooking=NULL, IsPayByCard=NULL, TotalMoney=NULL, StylistId=NULL, 
                                                SkinnerId=NULL, CheckinId=NULL, CheckoutId = NULL, SecurityId = NULL, SellerId=NULL, CustomerId=NULL, Images=NULL, Mark=NULL, Star = NULL, RelationId=NULL, BookingId=NULL, WaitTimeInProcedure=NULL,
                                                TotalCompleteTime=NULL,Note ='', CustomerCode1=NULL, IsX2 = 0, flow.ServiceId, [Service].Name AS ServiceName, ProductId=NULL, ProductName=NULL, flow.Price, flow.Quantity, 
                                                flow.VoucherPercent, PromotionMoney=NULL, StylistName = NULL, SkinnerName = NULL, CheckinName = NULL, CheckoutName = NULL, SercurityName = NULL, SellerName = NULL, CustomerName = NULL, CustomerPhone = NULL, 
                                                ListServices = '', ListProducts = '', ProductIds = NULL
		                                FROM BillServiceHis AS bill
			                                INNER JOIN FlowService AS flow ON flow.BillId = bill.Id AND flow.IsDelete != 1
			                                LEFT JOIN [Service] ON flow.ServiceId = [Service].Id
		                                WHERE bill.IsDelete != 1 AND bill.Pending != 1 AND  bill.CompleteBillTime BETWEEN @timeFrom AND @timeTo AND bill.SalonId = @salonId
	                                ),
	                                c AS
	                                (
		                                SELECT  bill.Id AS BillId, CreatedDate = NULL, CompleteBillTime=NULL, InProcedureTime=NULL, IsBooking=NULL, IsPayByCard=NULL, TotalMoney=NULL, StylistId=NULL, 
                                                SkinnerId=NULL, CheckinId=NULL, CheckoutId = NULL, SecurityId = NULL, SellerId=NULL, CustomerId=NULL, Images=NULL, Mark=NULL,Star = NULL, RelationId= NULL, BookingId=NULL, WaitTimeInProcedure=NULL,
                                                TotalCompleteTime=NULL, Note='', CustomerCode1=NULL, IsX2 = 0, ServiceId=NULL, ServiceName=NULL, flow.ProductId, Product.Name AS ProductName, flow.Price, flow.Quantity, 
                                                flow.VoucherPercent, flow.PromotionMoney, StylistName = NULL, SkinnerName = NULL, CheckinName = NULL, CheckoutName = NULL, SercurityName = NULL, SellerName = NULL, CustomerName = NULL, CustomerPhone = NULL, 
                                                ListServices = '', ListProducts = '', ProductIds = NULL
		                                FROM BillServiceHis AS bill
			                                INNER JOIN FlowProduct AS flow ON flow.BillId = bill.Id AND flow.IsDelete != 1
			                                LEFT JOIN Product ON flow.ProductId = Product.Id
		                                WHERE bill.IsDelete != 1 AND bill.Pending != 1 AND bill.CompleteBillTime BETWEEN @timeFrom AND @timeTo AND bill.SalonId = @salonId  AND flow.ComboId IS NULL
	                                )
	                                SELECT * FROM a
		                                UNION ALL
	                                SELECT * FROM b
		                                UNION ALL
	                                SELECT * FROM c ORDER BY a.CreatedDate DESC
                                END";
                        // Chuyen database sang bi
                        dbBi.Database.Connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLConnectionStringBI"].ConnectionString;
                        var list = dbBi.Database.SqlQuery<BillListting_cls>(sql).ToList();
                        var flowProducts = new List<FlowProduct>();
                        var staff = new List<Staff>();
                        if (list.Count > 0)
                        {
                            var index = -1;
                            var item = new BillListting_cls();
                            foreach (var v in list)
                            {
                                // Bản ghi bill
                                if (v.CreatedDate != null)
                                {
                                    totalReport.all_bills++;
                                    totalReport.all_money += Convert.ToInt32(v.TotalMoney);
                                    totalReport.bill_has_images += (v.Images != null && v.Images != "") ? 1 : 0;
                                    totalReport.paybycard_bill += Convert.ToBoolean(v.IsPayByCard) ? 1 : 0;
                                    totalReport.paybycard_money += Convert.ToBoolean(v.IsPayByCard) ? Convert.ToInt32(v.TotalMoney) : 0;
                                    totalReport.booking_comes_times += (v.BookingId != null && v.BookingId > 0) ? 1 : 0;
                                }

                                index = data.FindIndex(w => w.BillId == v.BillId);
                                if (index == -1)
                                {
                                    data.Add(v);
                                }
                                else
                                {
                                    item = data[index];
                                    // Bản ghi bill
                                    if (v.CreatedDate != null)
                                    {

                                        item.CreatedDate = v.CreatedDate;
                                        item.CompleteBillTime = v.CompleteBillTime;
                                        item.InProcedureTime = v.InProcedureTime;
                                        item.IsBooking = v.IsBooking;
                                        item.IsPayByCard = v.IsPayByCard;
                                        item.TotalMoney = v.TotalMoney;
                                        item.StylistId = v.StylistId;
                                        item.StylistName = v.StylistName;
                                        item.SkinnerId = v.SkinnerId;
                                        item.SkinnerName = v.SkinnerName;
                                        item.CheckinId = v.CheckinId;
                                        item.CheckinName = v.CheckinName;
                                        item.CheckoutId = v.CheckoutId;
                                        item.CheckoutName = v.CheckoutName;
                                        item.SecurityId = v.SecurityId;
                                        item.SecurityName = v.SecurityName;
                                        item.SellerId = v.SellerId;
                                        item.SellerName = v.SellerName;
                                        item.CustomerId = v.CustomerId;
                                        item.CustomerName = v.CustomerName;
                                        item.CustomerPhone = v.CustomerPhone;
                                        item.Images = v.Images;
                                        item.Mark = v.Mark;
                                        item.WaitTimeInProcedure = v.WaitTimeInProcedure;
                                        item.TotalCompleteTime = v.TotalCompleteTime;
                                    }
                                    // Bản ghi dịch vụ
                                    else if (v.ServiceId != null)
                                    {
                                        item.ListServices += "<tr>" +
                                                                "<td class='col-xs-4'>" +
                                                                        "<a href='/admin/dich-vu/" + v.ServiceId + ".html' target='_blank'>" +
                                                                        v.ServiceName +
                                                                        "</a>" +
                                                                "</td>" +
                                                                "<td class='col-xs-4'>" + v.Quantity +
                                                                ((v.VoucherPercent != null && v.VoucherPercent != 0) ? "<span class='sp-promotion'>( KM - " + v.VoucherPercent + "%)</span>" : "")
                                                                + "</td>" +
                                                                "<td class='col-xs-4 no-bdr be-report-price'>" +
                                                                ((v.VoucherPercent != null && v.VoucherPercent != 0) ? (v.Quantity * v.Price * (100 - v.VoucherPercent) / 100) : (v.Quantity * v.Price)) +
                                                                "</td>" +
                                                            "</tr>";
                                        totalReport.service_money += Convert.ToInt32(((v.VoucherPercent != null && v.VoucherPercent != 0) ? (v.Quantity * v.Price * (100 - v.VoucherPercent) / 100) : (v.Quantity * v.Price)));
                                    }

                                    else if (v.ProductId != null)
                                    {
                                        var staffName = "";
                                        flowProducts = dbBi.FlowProducts.Where(a => a.BillId == v.BillId && a.ProductId == v.ProductId).ToList();
                                        var SellerId = flowProducts.FirstOrDefault().SellerId;
                                        if (SellerId != null && SellerId != 0)
                                        {
                                            staff = dbBi.Staffs.Where(w => w.Id == SellerId).ToList();
                                            staffName = staff.FirstOrDefault(w => w.Id == SellerId) != null ? staff.FirstOrDefault(w => w.Id == SellerId).Fullname : "";
                                        }
                                        item.ListProducts += "<tr>" +
                                                                "<td class='col-xs-4' style='width: 60%;'>" +
                                                                     "<a href='/admin/san-pham/" + v.ProductId + ".html' target='_blank'>" +
                                                                        v.ProductName +
                                                                     "</a>" +
                                                                "</td>" +
                                                                "<td class='col-xs-4' style='width: 20%;' >" + v.Quantity +
                                                                ((v.VoucherPercent != null && v.VoucherPercent != 0) ? "<span class='sp-promotion'>( KM - " + v.VoucherPercent + "% )</span>" : "")
                                                                + "</td>" +
                                                                "<td class='col-xs-4 no-bdr be-report-price'>" +
                                                                ((v.VoucherPercent != null && v.VoucherPercent != 0) ? (v.Quantity * v.Price * (100 - v.VoucherPercent) / 100) : (v.Quantity * v.Price)) +
                                                                "</td>" +
                                                                "<td class='col-xs-4' style='width: 60%;'>"
                                                                + staffName +
                                                      "</td>" +
                                                      "</tr>";
                                        totalReport.product_money += Convert.ToInt32(((v.VoucherPercent != null && v.VoucherPercent != 0) ? (v.Quantity * v.Price * (100 - v.VoucherPercent) / 100) : (v.Quantity * v.Price)));
                                    }
                                    item.ProductId = v.ProductId;
                                    data[index] = item;
                                }
                            }

                            totalReport.service_bills = data.Count(w => w.ListServices != "");
                            totalReport.product_bills = data.Count(w => w.ListServices == "" && w.ListProducts != "");
                        }

                        totalReport.booking_times = getTotalBooking();
                        totalReport.wait_times = getTotalWaitAtSalon();
                        totalReport.total_product_return = getTotalMoneyReturn();
                    }
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Lấy dữ liệu truy vấn từ db qua sql store
        /// The conversion of a nvarchar data type to a datetime data type resulted in an out-of-range value.
        /// </summary>
        /// <returns></returns>
        private void bindData()
        {
            var data = getData();
            Bind_Paging(data.Count);
            RptBill.DataSource = data.OrderByDescending(o => o.CompleteBillTime).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
            RptBill.DataBind();
        }

        /// <summary>
        /// Export all data
        /// </summary>
        /// <returns></returns>
        public List<GetDataForExcels> GetDataForExcel()
        {
            List<GetDataForExcels> lstData = new List<GetDataForExcels>();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    List<ProductBasic> lstJson = new List<ProductBasic>();
                    GetDataForExcels record = new GetDataForExcels();
                    var serializer = new JavaScriptSerializer();
                    DateTime TimeFrom = new DateTime();
                    DateTime TimeFrom2 = new DateTime();
                    DateTime TimeTo = new DateTime();
                    if (!String.IsNullOrEmpty(TxtDateTimeFrom.Text.Trim()))
                    {
                        TimeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text.Trim(), culture);
                        TimeFrom2 = new DateTime(TimeFrom.Year, TimeFrom.Month, TimeFrom.Day, 7, 0, 0);
                        if (!String.IsNullOrEmpty(TxtDateTimeTo.Text.Trim()))
                        {
                            TimeTo = Convert.ToDateTime(TxtDateTimeTo.Text.Trim(), culture);
                        }
                        else
                        {
                            TimeTo = TimeFrom.AddDays(1);
                        }
                    }
                    else
                    {
                        TimeFrom2 = DateTime.Now;
                        TimeTo = TimeFrom.AddDays(1);
                    }
                    // Chuyen database sang bi
                    db.Database.Connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLConnectionStringBI"].ConnectionString;
                    List<Tbl_Salon> lstSalon = db.Tbl_Salon.Where(s => s.IsDelete == 0 && s.Publish == true && s.Id != 24 && s.IsSalonHoiQuan == false).ToList();
                    var lstBillService = (
                                            from a in db.BillServiceHis.AsNoTracking()
                                            where
                                            a.CreatedDate >= TimeFrom2 && a.CreatedDate < TimeTo
                                            && a.SalonId > 0 && a.SalonId != 24
                                            && a.Pending == 0 && a.IsDelete == 0
                                            select new
                                            {
                                                SalonId = a.SalonId,
                                                TotalMoney = a.TotalMoney ??
                                                0,
                                                ServiceIds = a.ServiceIds ?? "",
                                                ProductIds = a.ProductIds ?? "",
                                                IsPayByCard = a.IsPayByCard ?? false
                                            }
                                         ).ToList();
                    foreach (var v in lstSalon)
                    {
                        record = new GetDataForExcels();
                        record.SalonName = v.ShortName;
                        record.TotalAllMoneyBill = lstBillService.Where(w => w.SalonId == v.Id).Sum(w => w.TotalMoney);
                        if (lstBillService.Where(w => w.SalonId == v.Id && w.ServiceIds != "").Select(s => s.ServiceIds).Any())
                        {
                            string ServiceIds = String.Join("", lstBillService.Where(w => w.SalonId == v.Id && w.ServiceIds != "").Select(s => s.ServiceIds)).Replace("][", ",").Replace("].", "");
                            lstJson = serializer.Deserialize<List<ProductBasic>>(ServiceIds).ToList();
                            record.TotalAllMoneyService = lstJson.Sum(w => ((w.VoucherPercent > 0) ? w.Quantity * w.Price * (100 - w.VoucherPercent) / 100 : w.Quantity * w.Price));
                        }
                        if (lstBillService.Where(w => w.SalonId == v.Id && w.ProductIds != "").Select(s => s.ProductIds).Any())
                        {
                            string ProductIds = String.Join("", lstBillService.Where(w => w.SalonId == v.Id && w.ProductIds != "").Select(s => s.ProductIds)).Replace("][", ",").Replace("].", "");
                            lstJson = serializer.Deserialize<List<ProductBasic>>(ProductIds).ToList();
                            record.TotalAllMoneyProduct = lstJson.Sum(w => ((w.VoucherPercent > 0) ? w.Quantity * w.Price * (100 - w.VoucherPercent) / 100 : w.Quantity * w.Price));
                        }
                        record.TotalSwipeCard = lstBillService.Where(w => w.IsPayByCard == true && w.SalonId == v.Id).Sum(w => w.TotalMoney);
                        lstData.Add(record);
                    }
                }
            }
            catch (Exception e)
            {
                new Helpers.PushNotiErrorToSlack().PushDefault(e.Message + ", " + e.InnerException, this.ToString() + ".GetDataForExcel", "dungnm");
            }
            return lstData;
        }

        /// <summary>
        /// Lấy số lượng khách booking
        /// </summary>
        /// <returns></returns>
        private int getTotalBooking()
        {
            return db.Database.SqlQuery<int>(@"select Count(*) from ( select COUNT(*) as c from Booking where IsDelete != 1 and SalonId = " + salonId + @" and DatedBook >= '" + String.Format("{0:yyyy/MM/dd}", timeFrom) + "' and DatedBook < '" + String.Format("{0:yyyy/MM/dd}", timeTo) + @"' and CustomerPhone != '' group by CustomerPhone ) as a").FirstOrDefault();
        }

        /// <summary>
        /// Get total money product return
        /// </summary>
        /// <returns></returns>
        private int getTotalMoneyReturn()
        {
            return db.Database.SqlQuery<int>(@"SELECT CASE WHEN SUM(PriceProduct - (PriceProduct*VoucherPercent/100)) IS NOT NULL THEN CAST(SUM(PriceProduct - (PriceProduct*VoucherPercent/100)) as int) ELSE 0 END FROM dbo.ProductReturn WHERE CreatedDate >= '" + String.Format("{0:yyyy/MM/dd}", timeFrom) + "' and CreatedDate < '" + String.Format("{0:yyyy/MM/dd}", timeTo) + @"' AND SalonReceiveId = " + salonId + @"").FirstOrDefault();
        }

        /// <summary>
        /// Lấy số lượng khách đợi tại salon
        /// </summary>
        /// <returns></returns>
        private int getTotalWaitAtSalon()
        {
            return db.Database.SqlQuery<int>(@"select Count(*) from (select COUNT(*) as c from Bill_WaitAtSalon where IsDelete != 1 and SalonId = " + salonId + @" and (CreatedTime between '" + String.Format("{0:yyyy/MM/dd}", timeFrom) + "' and '" + String.Format("{0:yyyy/MM/dd}", timeTo) + @"') and CustomerPhone != '' group by CustomerPhone) as a").FirstOrDefault();
        }

        /// <summary>
        /// Lấy dữ liệu thẻ tặng khách VIP
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        private int getDataVIPCard(string field)
        {
            using (var db = new Solution_30shineEntities())
            {
                int salonId = Convert.ToInt32(Salon.SelectedValue);
                string wSalon = "";
                if (salonId > 0)
                {
                    wSalon = " and SalonId = " + salonId;
                }
                else
                {
                    wSalon = " and SalonId > 0";
                }
                string sql = @"select COUNT(*) as times from BillServiceHis 
                                    where IsDelete != 1 and Pending != 1
                                    and CompleteBillTime between '" + timeFrom + "' and '" + timeTo + "'" + @"
                                    and " + field + " is not null " + wSalon;
                return db.Database.SqlQuery<int>(sql).FirstOrDefault();
            }
        }

        /// <summary>
        /// Event update panel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _BtnClick(object sender, EventArgs e)
        {
            bindData();
            getWaitTimeBill();
            ScriptManager.RegisterStartupScript(UPTableListing, this.GetType(), "Format Price", "UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing();", true);
            RemoveLoading();
        }

        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        protected void Btn_SwitchExcel(object sender, EventArgs e)
        {
            if (HDF_ExcelType.Value == "Export")
            {
                Exc_ExportExcel(sender, e);
            }
            else if (HDF_ExcelType.Value == "Import")
            {
                Exc_ImportExcel();
            }

        }

        /// <summary>
        /// Export All Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Exc_ExportAddData(object sender, EventArgs e)
        {
            //call
            var data = GetDataForExcel();
            var _ExcelHeadRow = new List<string>();
            var RowLST = new List<List<string>>();
            _ExcelHeadRow.Add("Salon");
            _ExcelHeadRow.Add("Tổng doanh thu");
            _ExcelHeadRow.Add("Tổng doanh thu DV");
            _ExcelHeadRow.Add("Tổng doanh thu MP");
            _ExcelHeadRow.Add("Tổng doanh thu thanh toán thẻ");
            if (data.Any())
            {
                foreach (var v in data)
                {
                    var Row = new List<string>();
                    Row.Add(v.SalonName.ToString());
                    Row.Add(v.TotalAllMoneyBill.ToString());
                    Row.Add(v.TotalAllMoneyService.ToString());
                    Row.Add(v.TotalAllMoneyProduct.ToString());
                    Row.Add(v.TotalSwipeCard.ToString());
                    RowLST.Add(Row);
                }
                var ExcelStorePath = Server.MapPath("~") + "Public/Excel/Hoa.Don/";
                var FileName = "Tong_doanh_thu_" + string.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
                var UrlFileName = "http://" + Request.Url.Host + "/Public/Excel/Hoa.Don/" + FileName;
                ExportXcel(_ExcelHeadRow, RowLST, ExcelStorePath + FileName);
                ScriptManager.RegisterStartupScript(BtnFakeExcel, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "');UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing();", true);
            }
        }

        /// <summary>
        /// Export excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Exc_ExportExcel(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                if (TxtDateTimeFrom.Text.Trim() != "")
                {
                    int Segment = !HDF_EXCELSegment.Value.Equals("") ? Convert.ToInt32(HDF_EXCELSegment.Value) : PAGING._Segment;
                    int PageNumber = IsPostBack ? (HDF_EXCELPage.Value != "" ? Convert.ToInt32(HDF_EXCELPage.Value) : 1) : 1;
                    PageNumber = PageNumber > 0 ? PageNumber : 1;
                    int Offset = (PageNumber - 1) * Segment;
                    DateTime d1 = Convert.ToDateTime(TxtDateTimeFrom.Text.Trim(), culture);
                    DateTime d2;
                    if (TxtDateTimeTo.Text.Trim() != "")
                    {
                        d2 = Convert.ToDateTime(TxtDateTimeTo.Text.Trim(), culture).AddDays(1);
                    }
                    else
                    {
                        d2 = d1.AddDays(1);
                    }
                    string timeFrom = String.Format("{0:MM/dd/yyyy}", d1);
                    string timeTo = String.Format("{0:MM/dd/yyyy}", d2);
                    salonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;

                    var LST = db.Store_Bill_Listing_ExportExcel(timeFrom, timeTo, salonId).ToList();

                    var _ExcelHeadRow = new List<string>();
                    var serializer = new JavaScriptSerializer();
                    var BillRowLST = new List<List<string>>();
                    var BillRow = new List<string>();
                    _ExcelHeadRow.Add("Ngày");
                    _ExcelHeadRow.Add("Tgian hoàn thành (phút)");
                    _ExcelHeadRow.Add("Tên KH");
                    // Listing service
                    string sql_service = @"select b.* 
                                    from
                                    (
                                    select ServiceId from FlowService
                                    where IsDelete != 1
                                    and (CreatedDate between '" + timeFrom + "' and '" + timeTo + "')" +
                                        @"group by ServiceId
                                    ) as a
                                    join Service as b
                                    on a.ServiceId = b.Id";
                    var LST_Service = db.Services.SqlQuery(sql_service).ToList();
                    if (LST_Service.Count > 0)
                    {

                        foreach (var v in LST_Service)
                        {
                            _ExcelHeadRow.Add(v.Name + " ( " + v.Code + " ) ");
                        }
                        _ExcelHeadRow.Add("Tổng số lần sử dụng DV");
                        _ExcelHeadRow.Add("D.Thu Dịch vụ");
                    }

                    // Listing product
                    //var LST_Product = db.Products.Where(w => w.IsDelete != 1).OrderByDescending(o => o.Id).ToList();
                    string sql_product = @"select b.* 
                                    from
                                    (
                                    select ProductId from FlowProduct
                                    where IsDelete = 0
                                    and (CreatedDate between '" + timeFrom + "' and '" + timeTo + "')" +
                                        @"group by ProductId
                                    ) as a
                                    join Product as b
                                    on a.ProductId = b.Id";
                    var LST_Product = db.Products.SqlQuery(sql_product).ToList();
                    if (LST_Product.Count > 0)
                    {
                        var lstProduct = LST_Product.Select(a => new { Id = a.Id, Code = a.Code, Name = a.Name }).Distinct();
                        foreach (var v in lstProduct)
                        {
                            _ExcelHeadRow.Add(v.Name + " ( " + v.Code + " ) ");
                        }
                        _ExcelHeadRow.Add("D.Thu Sản phẩm");
                    }
                    _ExcelHeadRow.Add("Tổng D.Thu");
                    _ExcelHeadRow.Add("Stylist");
                    _ExcelHeadRow.Add("Skinner");
                    //_ExcelHeadRow.Add("NV bán mỹ phẩm");
                    _ExcelHeadRow.Add("NV check-in");

                    if (LST.Count > 0)
                    {

                        var ListProduct_Quantity = new List<int>();
                        var ServiceListThis = new List<ProductBasic>();
                        var ProductListThis = new List<ProductBasic>();
                        var Service_TotalMoney = 0;
                        var Product_TotalMoney = 0;
                        var TotalMoney = 0;

                        foreach (var v in LST)
                        {
                            int total_useService = 0;
                            if (v.CreatedDate != null)
                            {
                                Service_TotalMoney = 0;
                                Product_TotalMoney = 0;
                                TotalMoney = 0;
                                BillRow = new List<string>();
                                BillRow.Add(Convert.ToDateTime(v.CreatedDate).ToString("dd/MM/yyyy"));
                                BillRow.Add(v.WaitTimeInProcedure.ToString());
                                BillRow.Add(v.CustomerName);
                                if (v.ServiceIds != null)
                                {
                                    ServiceListThis = serializer.Deserialize<List<ProductBasic>>(v.ServiceIds);
                                }

                                if (v.ProductIds != null)
                                {
                                    ProductListThis = serializer.Deserialize<List<ProductBasic>>(v.ProductIds);
                                }

                                if (LST_Service.Count > 0)
                                {

                                    foreach (var v2 in LST_Service)
                                    {
                                        if (ServiceListThis != null)
                                        {
                                            var ExistId = ServiceListThis.Find(x => x.Id == v2.Id);

                                            if (ExistId.Code != null)
                                            {
                                                Service_TotalMoney += (ExistId.Quantity * ExistId.Price * (100 - ExistId.VoucherPercent) / 100);
                                                total_useService += ExistId.Quantity;
                                                BillRow.Add(ExistId.Quantity.ToString());
                                            }
                                            else
                                            {
                                                BillRow.Add("0");
                                            }
                                        }
                                        else
                                        {
                                            BillRow.Add("0");
                                        }
                                    }
                                }
                                BillRow.Add(total_useService.ToString());
                                BillRow.Add(Service_TotalMoney.ToString());

                                if (LST_Product.Count > 0)
                                {
                                    foreach (var v2 in LST_Product)
                                    {
                                        if (ProductListThis != null)
                                        {
                                            var ExistId = ProductListThis.Find(x => x.Id == v2.Id);
                                            if (ExistId.Code != null)
                                            {
                                                Product_TotalMoney += ExistId.Promotion != 0 ? (ExistId.Quantity * ExistId.Price - ExistId.Promotion) : (ExistId.Quantity * ExistId.Price * (100 - ExistId.VoucherPercent) / 100);
                                                BillRow.Add(ExistId.Quantity.ToString());
                                            }
                                            else
                                            {
                                                BillRow.Add("0");
                                            }
                                        }
                                        else
                                        {
                                            BillRow.Add("0");
                                        }
                                    }
                                }
                                TotalMoney = Service_TotalMoney + Product_TotalMoney;

                                BillRow.Add(Product_TotalMoney.ToString());
                                BillRow.Add(TotalMoney.ToString());
                                BillRow.Add(v.Stylist);
                                BillRow.Add(v.Skinner);
                                BillRow.Add(v.CheckinName);
                                BillRowLST.Add(BillRow);
                            }

                        }
                    }

                    // export
                    var ExcelStorePath = Server.MapPath("~") + "Public/Excel/Hoa.Don/";
                    var FileName = "Hoa_Don_" + string.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
                    var UrlFileName = "http://" + Request.Url.Host + "/Public/Excel/Hoa.Don/" + FileName;

                    ExportXcel(_ExcelHeadRow, BillRowLST, ExcelStorePath + FileName);
                    Bind_Paging(int.Parse(HDF_EXCELPage.Value));
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "');UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing(); alert(\"Xuất excel thành công.\")", true);
                }
            }
        }

        protected void Exc_ImportExcel()
        {
            var _ExcelHeadRow = new List<string>();
            _ExcelHeadRow.Add("STT");
            _ExcelHeadRow.Add("Field1");
            _ExcelHeadRow.Add("Field2");
            _ExcelHeadRow.Add("Field3");

            var LST = new List<Tuple<int, string, string, string>>();

            for (var i = 0; i < 5; i++)
            {
                var v = Tuple.Create(i, "str1-" + i, "str2-" + i, "str3-" + i);
                LST.Add(v);
            }

            // import
            var Path = Server.MapPath("~") + "Public/Excel/Hoa.Don/" + "Hoa_Don_28_07_2015_06_28_13.xlsx";
            Response.Write(Path);
            ImportXcel(Path);
        }

        private static void ExportXcel(List<string> _ExcelHeadRow, List<List<string>> List, string Path)
        {
            try
            {
                CreateExcelFile.ExcelHeadRow = _ExcelHeadRow;
                CreateExcelFile.CreateExcel(List, Path);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi. Không tạo được file excel.\r\nException: " + ex.Message);
            }
        }

        private void ImportXcel(string Path)
        {
            var tblDom = "<table><tbody>";
            foreach (var worksheet in Workbook.Worksheets(@Path))
                foreach (var row in worksheet.Rows)
                {
                    tblDom += "<tr>";
                    foreach (var cell in row.Cells)
                    {
                        if (cell != null)
                        {
                            tblDom += "<td>" + cell.Text + "</td>";
                        }
                    }
                    tblDom += "<tr>";
                }
            // if (cell != null) // Do something with the cells
            tblDom += "</tbody></table>";
            Response.Write(tblDom);
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        public string GenTime(int minutes)
        {
            string time = "";
            int hour = minutes / 60;
            int minute = minutes % 60;
            if (hour > 0)
            {
                time += hour + "h";
                time += minute + "p";
            }
            else
            {
                if (minute > 0)
                {
                    time += minute + " p";
                }
                else
                {
                    time += "-";
                }
            }
            return time;
        }

        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }

        public static Service_Listing_V2 Instance
        {
            get
            {
                if (Service_Listing_V2._Instance == null)
                {
                    Service_Listing_V2._Instance = new Service_Listing_V2();
                }
                return Service_Listing_V2._Instance;
            }
        }

        /// <summary>
        /// trả về ghi chú theo id BillService
        /// </summary>
        /// <param name="billId"></param>
        /// <returns></returns>
        public class cls_totalReport
        {
            public int billX2 { get; set; }
            public int all_bills { get; set; }
            public int all_money { get; set; }
            public int service_bills { get; set; }
            public int product_bills { get; set; }
            public int product_money { get; set; }
            public int service_money { get; set; }
            public int vipcard_give { get; set; }
            public int vipcard_use { get; set; }
            /// <summary>
            /// Số lượt khách booking
            /// </summary>
            public int booking_times { get; set; }
            /// <summary>
            /// Số lượt khách booking và đến
            /// </summary>
            public int booking_comes_times { get; set; }
            /// <summary>
            /// Số lượt khách đợi tại salon
            /// </summary>
            public int wait_times { get; set; }
            /// <summary>
            /// Số bill có ảnh
            /// </summary>
            public int bill_has_images { get; set; }
            /// <summary>
            /// Tổng số thẻ thanh toán qua thẻ
            /// </summary>
            public int paybycard_bill { get; set; }
            /// <summary>
            /// Tổng số tiền thanh toán qua thẻ
            /// </summary>
            public int paybycard_money { get; set; }
            /// <summary>
            /// tổng số tiền hàng trả lại
            /// </summary>
            public int? total_product_return { get; set; }
            public int tong_tra_truoc { get; set; }
            public int tong_giam_tru { get; set; }
            public int? total_money_return { get => total_money_return; set => total_money_return = all_money - paybycard_money + tong_tra_truoc - tong_giam_tru - total_product_return; }
        }

        /// <summary>
        ///  Export all data
        /// </summary>
        public partial class GetDataForExcels
        {
            public string SalonName { get; set; }
            public int TotalAllMoneyBill { get; set; }
            public double TotalAllMoneyService { get; set; }
            public double TotalAllMoneyProduct { get; set; }
            public double TotalSwipeCard { get; set; }
        }

        public class BillListting_cls
        {
            public int BillId { get; set; }
            public string ProductIds { get; set; }
            public Nullable<System.DateTime> CreatedDate { get; set; }
            public Nullable<System.DateTime> CompleteBillTime { get; set; }
            public Nullable<System.DateTime> InProcedureTime { get; set; }
            public Nullable<bool> IsBooking { get; set; }
            public Nullable<bool> IsPayByCard { get; set; }
            public Nullable<int> TotalMoney { get; set; }
            public Nullable<int> StylistId { get; set; }
            public Nullable<int> SkinnerId { get; set; }
            public Nullable<int> CheckinId { get; set; }
            public Nullable<int> CheckoutId { get; set; }
            public Nullable<int> SecurityId { get; set; }
            public Nullable<int> SellerId { get; set; }
            public Nullable<int> CustomerId { get; set; }
            public string Images { get; set; }
            public Nullable<int> Mark { get; set; }
            public string Star { get; set; }
            public Nullable<int> RelationId { get; set; }
            public Nullable<int> BookingId { get; set; }
            public string Note { get; set; }
            public string CustomerCode1 { get; set; }
            public Nullable<bool> IsX2 { get; set; }
            public Nullable<int> WaitTimeInProcedure { get; set; }
            public Nullable<int> TotalCompleteTime { get; set; }
            public Nullable<int> ServiceId { get; set; }
            public string ServiceName { get; set; }
            public Nullable<int> ProductId { get; set; }
            public string ProductName { get; set; }
            public Nullable<int> Price { get; set; }
            public Nullable<int> Quantity { get; set; }
            public Nullable<int> VoucherPercent { get; set; }
            public Nullable<int> PromotionMoney { get; set; }
            public string StylistName { get; set; }
            public string SkinnerName { get; set; }
            public string CheckinName { get; set; }
            public string CheckoutName { get; set; }
            public string SecurityName { get; set; }
            public string SellerName { get; set; }
            public string CustomerName { get; set; }
            public string CustomerPhone { get; set; }
            public string ListServices { get; set; }
            public string ListProducts { get; set; }
        }

        public class ListCustomProduct
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Code { get; set; }
            public int SellerId { get; set; }
            public string FullName { get; set; }
        }

        public class WaitTimeListing
        {
            public int? BillUnder15 { get; set; }
            public int? BillOver15 { get; set; }
            public int? BillNotTime { get; set; }
        }
    }
}