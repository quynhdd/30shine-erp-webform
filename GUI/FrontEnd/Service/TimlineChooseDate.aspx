﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="TimlineChooseDate.aspx.cs" Inherits="_30shine.GUI.FrontEnd.UIService.TimlineChooseDate" %>
<asp:content id="Content1" contentplaceholderid="head" runat="server">
    <title>Timeline</title>
    <%--<script src="https://www.gstatic.com/firebasejs/3.7.3/firebase.js"></script>
    <script>
        // Initialize Firebase
        var config = {
            apiKey: "AIzaSyAna6aEaHXkOJZ8NTChXOlJtvM7QkfH0-s",
            authDomain: "erp-30shine.firebaseapp.com",
            databaseURL: "https://erp-30shine.firebaseio.com",
            storageBucket: "erp-30shine.appspot.com",
            messagingSenderId: "80706579109"
        };
        firebase.initializeApp(config);
    </script>
    <script src="/Assets/js/erp.30shine.com/timeline.firebase.js?v=5"></script>--%>

    <script src="/assets/js/jquery.mCustomScrollbar.js"></script>
    <link href="/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
    <%--<link href="/Assets/css/timeline.css?v=1" rel="stylesheet" />  --%>
    <script src="/Assets/js/erp.30shine.com/timeline_beta_V2.js?v=5379485"></script>
    <script src="/Assets/libs/magicsuggest-master/magicsuggest.js"></script>

    <link href="/Assets/libs/magicsuggest-master/magicsuggest.css" rel="stylesheet" />
    <link href="/Assets/css/erp.30shine.com/timeline_beta.css?v=27" rel="stylesheet" />
    <link href="/Assets/css/checkin.css" rel="stylesheet" />
    <style type="text/css">
        #tableNote tr, #tableNote tr td {
            border: none;
        }

            #tableNote tr td {
                padding: 10px 0px;
            }

        .popup-btn.btn-insert {
            background: #50b347;
        }

        .popup-btn.btn-close {
            background: red;
            margin-left: 10px;
        }

        .popup-btn {
            float: left;
            padding: 7px 10px;
            color: #ffffff;
            width: 80px;
            cursor: pointer;
        }

        .isSalonNote {
            width: 20px;
            height: 20px;
            position: absolute;
            left: 1px;
            bottom: 1px;
        }

        .btnSalonNote {
            border-radius: 50%;
            border: none;
            height: 18px;
            width: 18px;
            margin-left: 1px;
            /*background:red*/
        }

        .board-data .board-cell-item.is-book.is-book-at-salon.col-6 {
            background: #397ad3;
        }

        .board-data-wrap {
            width: 85%;
            float: left;
            background: #dadada;
        }

        .board-data.board-stylist {
            width: 15%;
        }

        .showWaittingTime {
            width: 25%;
            float: left;
            padding-left: 5px;
        }

            .showWaittingTime .estimate-time-head {
                width: 100%;
                float: left;
                text-align: center;
                height: 38px;
                line-height: 38px;
                background: #475866;
                color: #ffffff;
                font-weight: normal;
                font-family: Roboto Condensed Bold;
                font-size: 16px;
            }

            .showWaittingTime .table-estimate-time-cut thead th {
                text-align: center;
                font-weight: normal;
                font-family: Roboto Condensed Bold;
                line-height: 18px !important;
                padding-top: 2px !important;
                padding-bottom: 2px !important;
            }

            .showWaittingTime .table-estimate-time-cut td:not(:first-child) {
                text-align: center;
            }

            .showWaittingTime .table-estimate-time-cut td {
                padding-top: 3px !important;
                padding-bottom: 3px !important;
            }

        .timeline-wp.etc-active .timeline-board {
        }

        .timeline-wp.etc-active .showWaittingTime {
            display: block;
        }

        .note-head .note-head-item .item-text {
            font-size: 13px;
        }

        .note-head-item {
            padding-left: 10px !important;
        }

        .showWaittingTime .note-head .note-head-item .item-text {
            line-height: 30px;
        }

        .estimate-box-issulphite td, .estimate-box-issulphite-color {
            color: red !important;
            font-family: Roboto Condensed Bold !important;
        }

        .estimate-box-wait td, .estimate-box-wait-color {
            background: #708090 !important;
        }

        .estimate-box-free td, .estimate-box-free-color {
            background: lightgreen !important;
        }

        .estimate-box-not-active td, .estimate-box-not-active-color {
            background: orangered !important;
        }

        .showWaittingTime .note-head {
            width: 100% !important;
            padding: 0px !important;
            margin: 0px !important;
        }

            .showWaittingTime .note-head .item-text {
                font-family: Roboto Condensed Bold !important;
            }

        .showWaittingTime .note-head-item {
            width: 50%;
            padding: 0px !important;
            margin: 0px !important;
        }

            .showWaittingTime .note-head-item .item-color {
                width: 20px;
                height: 20px;
                border: 1px solid #ddd;
                float: left;
                display: inline-block;
                font-family: Roboto Condensed Bold !important;
                text-align: center;
            }

        .a_btn_tracking {
            font-family: Roboto Condensed Bold;
            float: left;
            margin-left: 25px;
            background: #053c2c;
            color: #FFF;
            padding: 7px 10px;
            margin-top: -4px;
            font-size: 13px;
        }

            .a_btn_tracking:hover {
                color: #FFF;
                text-decoration: underline !important;
            }

            .a_btn_tracking:active {
                color: #FFF;
                text-decoration: underline !important;
            }

        .event {
            cursor: move;
        }

        .min, .sec {
            color: #ffffff;
        }

        .loadingTable {
            background: #fff;
            opacity: 0.5;
            top: 0;
            left: 0;
            position: fixed;
            width: 100%;
            height: 100%;
            z-index: 888888;
        }

        #count {
            padding: 3px 10px;
            margin-left: 10px;
        }

        .txtDateTime {
            margin-top: -1px;
            color: #444;
            border-color: #aaa;
        }

        #ViewData {
            border: 1px solid #D0D6D8;
            background: #000000;
            float: left;
            color: #ffffff;
            font-family: Roboto Condensed Regular;
            font-size: 14px;
            margin-left: 15px;
            height: 34px;
            line-height: 31px;
            font-weight: normal;
            cursor: pointer;
            margin-top: 15px;
            padding: 0px 16px;
        }

            #ViewData:hover {
                color: #ffe400;
            }
        #fakeViewdata {
            display:none !important;
        }
        .board-cell-item.is-enroll:hover .board-cell-action{display:none !important}
    </style>
    <script>    
        var billClass = function () {
            this.billID = 0;
            this.bookingID = 0; // ID SM_BookingTemp
            this.hourID = 0;
            this.hourFrame = '';
            this.stylistID = 0;
            this.stylistName = '';
            this.customerPhone = '';
            this.customerName = '';
            this.salonID = 0;
            this.salonName = '';
            this.checkinID = 0;
            this.checkinName = '';
            this.services = [];
            this.teamID = 0;
            this.PDFBillCode = '';
            this.HCItem = '';
            this.IsAutoStylist = true;
            this.IsBookAtSalon = true;
        }

        var bill = new billClass();
        bill.salonID = <%=salonID%>;

        var cancelBookingClass = function () {
            this.customerPhone = '';
            this.customerCode = '';
            this.cancelBookingID = 0;
            this.noteCancel = '';
            this.itemBookingCancel = null;
        }
        var bookingCancel = new cancelBookingClass();

        var ms;
        var listService = <%=listService%>;
        var defaultServiceID = 53;

        /* params drag and drop cell timeline */
        var customerPhone = "";
        var stylist = 0;
        var hourId = 0;
        var olderHourdId = 0;
        var bookingId = 0;
        var billIdCurrent = 0;


        jQuery(document).ready(function () {

            //$("#eventBillAdd").openEBPopup();
            setTimeLine();
            //var autoSetTimeLine = setInterval(setTimeLine, 10000);
            window.onload = function () {
                //alert("dfsdf");

                if ($('.event').hasClass('is-makebill')) {
                    $('.is-checkout').removeClass('event');
                }
                if ($('.event').hasClass('is-checkout')) {
                    $(This).parent().find(".customer-text-wrap").find(".fa-warning").css("display", "none");
                }
                loadSetWaitingTime();
            }
            if ($('.event').hasClass('is-makebill')) {
                $('.is-checkout').removeClass('event');
            }
            if ($('.event').hasClass('is-checkout')) {
                $('.is-checkout').find(".board-cell-content").find(".customer-name").find(".customer-text-wrap").find(".fa-warning").css("display", "none");
            }
            loadSetWaitingTime();
        });


    </script>
    <script src="/Assets/js/erp.30shine.com/timeline_beta_V2.js?v=23847"></script>
</asp:content>
<asp:content id="Content2" contentplaceholderid="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="timeline-wp <%= Array.IndexOf(new int[] { 4,5 }, salonID) != -1 ? "etc-active" : "" %>">
            <div class="timeline-head">
                <h2 class="title-head">Timeline</h2>
                <div class="note-head" <%= Perm_ShowSalon == true ? "" : "style='display:none;'"%>>
                    <asp:DropDownList runat="server" CssClass="form-control select" ID="ddlSalon" AutoPostBack="true" Style="margin-top: 4px; width: 160px;" ClientIDMode="Static">
                    </asp:DropDownList>
                </div>
                <div class="note-head">
                    <asp:TextBox CssClass="txtDateTime form-control" ID="TxtDateTimeFrom" placeholder="Từ ngày" autocomplete="off"
                        ClientIDMode="Static" runat="server"></asp:TextBox>
                </div>
                <div id="ViewData" class="st-head btn-viewdata" text="Xem dữ liệu" onclick="viewtdata($(this));FetchDataToTable();">
                    Xem dữ liệu   
                </div>
                <asp:Button  ID="fakeViewdata" runat="server" Text="Button" OnClick="updatePanelBindData" ClientIDMode="Static" OnClientClick="addLoading()" hidden />
                <div class="note-head" style="padding-left: 5px;">
                    <a href="javascript:void(0);" onclick="openFrameBooking()" class="open-booking-list note-head-item">
                        <span class="fa fa-calendar" style="padding-left: 0;"></span>
                        Đặt lịch cho khách</a>
                    <div class="frame-booking-pane">
                        <%--<iframe src="https://30shine.com/erp-booking" class="iframe-booking"></iframe>--%>
                        <%--<iframe src="https://30shine.com/erp-booking" class="iframe-booking"></iframe>--%>
                    </div>
                </div>
                <div class="note-head">
                    <div class="note-head-item">
                        <span class="item-color item-color-book"></span><span class="item-text">Khách book</span>
                    </div>
                    <div class="note-head-item">
                        <span class="item-color item-color-bill"></span><span class="item-text">Đã in bill</span>
                    </div>
                    <div class="note-head-item item-color-customer-wait">
                        <i class="fa fa-check is-call" aria-hidden="true"></i><span class="item-text">Khách đang chờ</span>
                    </div>
                    <div class="note-head-item">
                        <span class="item-color item-color-BookAtSalon"></span><span class="item-text">Khách book tại salon</span>
                    </div>
                    <div class="note-head-item">
                        <i class="fa fa-star" aria-hidden="true" style="color: #fddd4a; font-size: 18px;"></i><span class="item-text">&nbsp; Khách đặc biệt</span>
                    </div>
                    <div class="note-head-item">
                        <i class="fa fa-warning" aria-hidden="true" style="color: #F44336; font-size: 18px;"></i><span class="item-text">&nbsp; Khách đang chờ lâu</span>
                    </div>
                    <a target="_blank" href="/tracking/danh-sach/yeu-cau-dac-biet-cua-khach.html" class="a_btn_tracking">DS KH yêu cầu đặc biệt<span id="count" class="label label-primary">0</span></a>
                </div>
                <div class="search-frame hidden">
                    <div class="btn-search"></div>
                    <div class="input-frame" style="">
                        <input type="text" class="form-control" placeholder="Nhập số điện thoại khách hàng" autofocus="autofocus" onkeypress="searchByPhone(event, $(this))" />
                    </div>
                </div>
            </div>

            <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
            <asp:UpdatePanel runat="server" ID="UP01">
                <ContentTemplate>
                    <div class="timeline-board">
                        <div class="vertical-bar">
                            <div class="vertical-bar-relative">
                                <div class="circle"></div>
                            </div>
                        </div>
                        <div class="board-data board-stylist">
                            <div class="board-cell board-icon" id="board-hour-icon"></div>

                            <% if (dataTimeline.Count > 0)
                                { %>
                            <% int loop = 1; int teamId = 0;
                                foreach (var v in dataTimeline)
                                { %>
                            <div class="board-row <%= (loop++)%2 == 0 ? "highlight" : "" %>" <%--style="<%= (teamId != 0 && teamId != v.TeamId) ? "border-top: 1px solid #000000;" : "" %>"--%>>
                                <div class="board-cell" data-id="<%=v.StylistId %>">
                                    <div class="board-cell-content"><%=v.StylistName %></div>
                                    <div style="position: absolute; z-index: 999; right: 5px; top: -8px; font-family: Roboto Condensed Bold; color: #42cabf;">
                                        <%=v.BillTotal %>
                                    </div>
                                    <div style="position: absolute; z-index: 999; left: -17px; top: 5px; font-family: Roboto Condensed Bold; color: #475866;">
                                        <%=v.TeamId %>
                                    </div>
                                </div>
                            </div>
                            <% teamId = v.TeamId;
                                } %>

                            <% } %>
                        </div>
                        <div class="board-data-wrap">
                            <div class="board-data board-book">
                                <div class="board-book-wrap" id="timeline">
                                    <div class="board-row board-hour">
                                        <% if (listHour.Count > 0)
                                            { %>
                                        <% foreach (var v in listHour)
                                            { %>
                                        <div class="board-cell"><%=v.Substring(0,5) %></div>
                                        <%} %>
                                        <% foreach (var v in listHour)
                                            { %>
                                        <div class="board-cell"></div>
                                        <%} %>
                                        <%} %>
                                    </div>

                                    <% if (dataTimeline.Count > 0)
                                        { %>
                                    <% int loop = 1; int teamId = 0;
                                        foreach (var v in dataTimeline)
                                        {   %>
                                    <% if (v.ListHour.Count > 0)
                                        { %>
                                    <div class="board-row board-book-stylist <%--<%= (loop++)%2 == 0 ? "highlight" : "" %>--%>" <%--style="<%= (teamId != 0 && teamId != v.TeamId) ? "border-top: 1px solid #000000!important;" : "" %>"--%>>
                                        <% foreach (var v2 in v.ListHour)
                                            { %>
                                        <% if (v2.HourData.Count > 0)
                                            { %>
                                        <div class="board-cell" id="<%=v.StylistId %>-<%=v2.HourId %>" ">
                                            <% foreach (var v3 in v2.HourData)
                                                { %>
                                            <div class="board-cell-item event <%= v3.BillID != null ? "is-makebill" : "is-book" %> <%=v3.IsBookAtSalon ? "is-book-at-salon" : "" %> <%=v2.HourData.Count > 1 ? " col-6" : "" %> <%= v2.IsServiced ? "is-enroll" : "not-enroll" %> <%= v3.CompleteBillTime != null ? "is-checkout" : "" %>" id="<%=v.StylistId %>-<%=v3.BookingID %>-bk" >
                                                <div class="board-cell-content">
                                                    <p class="customer-name">
                                                        <span class="customer-text-wrap">
                                                            <i class="fa fa-warning hidden" aria-hidden="true" style="margin-right: 5px; color: #F44336; font-size: 14px; display: inline-block;"></i>
                                                            <%= v3.CustomerName %>
                                                            <i class="fa fa-star <%= v3.CustomerType == null ? "hidden" : "" %>" aria-hidden="true" style="margin-left: 5px; color: #fddd4a; font-size: 14px; display: inline-block;"></i>
                                                            <span class="show-checkout"></span>
                                                        </span>
                                                    </p>
                                                    <p class="customer-phone">
                                                        <%=v3.CustomerPhone %>
                                                    </p>
                                                    <%--<p class="bill-created-time"></p>--%>
                                                    <div class="bill-created-time" style="display: none; color: #ffffff;">
                                                        <label class="min" data-min="00">00</label>:<label class="sec" data-sec="00">00</label>
                                                        <%--<%= v3.BillCreatedDate != null ? String.Format("{0:HH}",v3.BillCreatedDate) + ":" + String.Format("{0:mm}",v3.BillCreatedDate)  : ""%>--%>
                                                    </div>
                                                </div>
                                                <div class="order-checked" style="position: absolute; bottom: 0px; right: 22px; color: #ffffff; display: inline-block;"><%= v3.Order%></div>
                                                <div class="call-phone" title="Khách chờ tại salon" ">

                                                    <i class="fa fa-check <%= v3.IsCall != null && v3.IsCall.Value ? "is-call" : "" %>" aria-hidden="true"></i>
                                                    <%--<span class="text-bill-checkout">1</span>--%>
                                                </div>

                                                <div class="isSalonNote" <% if ((v3.TextNote1 == null || v3.TextNote1 == "") && (v3.TextNote2 == null || v3.TextNote2 == ""))
                                                    { %>
                                                    style="display: none"
                                                    <% } %>>

                                                    <button type="button" class="btnSalonNote" id="btn-<%= v3.BookingID %>"
                                                        <%
                                                        if (!(v3.SalonNote == null || v3.SalonNote == ""))
                                                        {
                                                            %>
                                                        style='background: #42f44e'
                                                        <%
                                                        }
                                                        else
                                                        {
                                                            %>
                                                        style='background: red'
                                                        <%
                                                        }
                                                            %>
                                                        <%--onclick="getNote(<%= v3.BookingID %>)">--%>
                                                    </button>

                                                </div>

                                                <% if (v3.BillID == null)
                                                    { %>
                                                <div class="cancel-book" title="Hủy lịch đặt" >
                                                    <i class="fa fa-times" aria-hidden="true"></i>
                                                </div>
                                                <% } %>
                                                <div class="board-cell-action" data-id="<%=v.StylistId %>-<%=v.StylistName %>-<%=v2.HourId %>-<%=v2.HourFrame %>">
                                                    <div class="col-xs-6 cell-item-action cell-item-action-printbill" >In phiếu</div>
                                                    <div class="col-xs-6 cell-item-action cell-item-action-book" >Book lịch</div>
                                                </div>
                                                <div id="stylist-not-auto-<%=v3.BookingID %>" class="stylist-not-auto <%= !(v3.IsAutoStylist) ? "active" : "" %>">B</div>
                                                <div class="customer-hc <%= (v3.IsHCItem) ? "active" : "" %>">H</div>
                                            </div>
                                            <%} %>
                                        </div>
                                        <%--<div class="board-cell"><div class="board-cell-content">asdf</div></div>--%>
                                        <%}
                                            else
                                            { %>
                                        <div class="board-cell">
                                            <div class="board-cell-item <%= v2.IsServiced ? "is-enroll" : "not-enroll" %>">
                                                <div class="board-cell-content"></div>
                                                <div class="board-cell-action" data-id="<%=v.StylistId %>-<%=v.StylistName %>-<%=v2.HourId %>-<%=v2.HourFrame %>">
                                                    <div class="col-xs-6 cell-item-action cell-item-action-printbill" >In phiếu</div>
                                                    <div class="col-xs-6 cell-item-action cell-item-action-book" ">Book lịch</div>
                                                </div>
                                            </div>
                                        </div>
                                        <% } %>
                                        <%} %>
                                    </div>
                                    <%teamId = v.TeamId;
                                        } %>
                                    <%} %>
                                    <% } %>
                                </div>
                            </div>
                        </div>
                        <div class="showWaittingTime" style="display:none;">
                            <div class="loadingTable" style="display: none">Vui lòng đợi trong giây lát...</div>
                            <div class="row">
                                <div class="note-head">
                                    <%-- <div class="note-head-item">
                                    <span class="item-color estimate-box-wait-color" title="Stylist có khách chờ bằng 0"></span>
                                    <span class="item-text">Khách chờ 0</span>
                                </div>--%>
                                    <div class="note-head-item">
                                        <span class="item-color estimate-box-issulphite-color" title="Stylist,Skinner có khách đang xử lý hóa chất">C</span>
                                        <span class="item-text">KH sử dụng hóa chất</span>
                                    </div>
                                    <div class="note-head-item">
                                        <span class="item-color estimate-box-free-color" title="Stylist,Skinner đang không có khách"></span>
                                        <span class="item-text">Stylist/Skinner Rảnh</span>
                                    </div>
                                    <%-- <div class="note-head-item">
                                    <span class="item-color estimate-box-not-active-color" title="Stylist không áng time cắt"></span>
                                    <span class="item-text">Stylist không áng</span>
                                </div>--%>
                                </div>
                                <strong class="estimate-time-head">ƯỚC LƯỢNG THỜI GIAN GỘI
                                </strong>
                                <table class="table table-listing-product table-bordered table-estimate-time-cut" id='tableEstimateTimeMassage'>
                                    <thead>
                                        <tr>
                                            <%--<th>Khách hàng</th>--%>
                                            <th style="line-height: 50px !important">Skinner</th>
                                            <th style="width: 60px;">TG gội còn lại (phút)</th>
                                            <th style="width: 60px;">SL Đang gội</th>
                                            <th style="width: 60px;">SL Chưa gội</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <%--<div class="row">
                                <strong class="estimate-time-head">ƯỚC LƯỢNG THỜI GIAN CẮT & GỘI
                                </strong>
                                <table class="table table-listing-product table-bordered table-estimate-time-cut" id='tableEstimate'>
                                    <thead>
                                    <tr>                                       
                                        <th style="line-height: 50px !important">Stylist</th>
                                        <th style="width: 60px;">TG còn lại (phút)</th>
                                        <th style="width: 60px;">SL đang cắt</th>
                                        <th style="width: 60px;">SL chưa cắt</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>--%>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlSalon" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>
        </div>

        <!-- Popup thêm mới team -->
        <div class="event-bill-add" id="eventBillAdd">
            <%--<strong class="bill-add-head">Lập bill checkin</strong>--%>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 popup-head">
                        <p class="popup-hour-frame">08:00</p>
                        <p class="popup-stylist"><i class="fa fa-star-o star-not-auto-stylist" aria-hidden="true"></i>Stylist <span style="position: relative; top: -2px;">&nbsp;|&nbsp;</span> <span class="sp-stylist-name"></span></p>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group form-group-lg">
                            <div class="col-xs-12">
                                <label class="label-customer-name"><span>Khách hàng</span> <span class="customer-book-at-salon">( <span style="color: red; float: none;">*</span> Book tại salon )</span></label>
                                <input id="customerPhone" class="form-control customer-phone" type="text" placeholder="Số điện thoại" onchange="callCustomerData($(this))" onkeypress="return isNumber(event)" />
                                <input id="customerName" class="form-control customer-name" type="text" placeholder="Tên Khách hàng" style="margin-top: 10px;" />
                                <button id="addName" style="top: 10px; float: right; margin-top: -34px; height: 34px; background-color: #565656; border-color: #565656;"  class="btn btn-success">Lưu</button>
                            </div>
                        </div>
                        <div class="form-group form-group-lg" <%= !Perm_ViewAllData ? "style='display:none;'" : "" %>>
                            <div class="col-xs-12">
                                <label>Salon</label>
                                <select class="form-control ddl-salon" onchange="setSalon($(this))">
                                    <option value="1">Chọn salon</option>
                                    <% if (listSalon.Count > 0)
                                        { %>
                                    <% foreach (var v in listSalon)
                                        { %>
                                    <option value="<%=v.Id %>"><%=v.Name %></option>
                                    <%} %>
                                    <%} %>
                                </select>
                            </div>
                        </div>
                        <div class="form-group form-group-lg service-wrap">
                            <div class="col-xs-12 suggestion-wrap">
                                <label>Dịch vụ</label>
                                <input class="form-control ms-suggest" type="text" placeholder="Search theo tên dịch vụ" />
                            </div>
                        </div>
                        <div class="form-group form-group-lg checkin-wrap">
                            <div class="col-xs-12 suggestion-wrap">
                                <label>Nhân viên checkin</label>
                                <div class="row">
                                    <input class="form-control checkin-code" type="text" placeholder="STT" style="width: 25%; float: left;" onkeypress="return isNumber(event)" onkeyup="setCheckin($(this))" />
                                    <input class="form-control checkin-name" type="text" placeholder="Nhân viên checkin" disabled="disabled" style="width: 75%; float: left;" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <% if (listTeam.Count > 0)
                            { %>
                        <div class="form-group form-group-lg team-wrap" style="display: none;">
                            <div class="col-xs-12 suggestion-wrap" style="padding-bottom: 5px;">
                                <label>Chọn team</label>
                            </div>
                            <div class="col-xs-12 list-team">
                                <% foreach (var v in listTeam)
                                    { %>
                                <div class="btn btn-success btn-team" style="background: <%=v.Color%>" >
                                    <i class="fa fa-check-square" aria-hidden="true"></i>
                                </div>
                                <%} %>
                            </div>
                        </div>
                        <div class="form-group form-group-lg hidden div_checkCustomer" style="margin-top: 10px; border-radius: 10px; background: #eeeeee; padding: 10px;">
                            <p class="customerType" style="text-transform: uppercase; font-family: 'Roboto Condensed Bold'; font-size: 16px; text-align: center;"></p>
                            <p class="reason" style="font-family: 'Roboto Condensed Bold'; font-size: 14px;"></p>
                        </div>
                        <div class="row list-item-hc">
                            <input type="hidden" id="HDFBillID" />
                            <div class="col-xs-9">
                                <label>
                                    <input type="checkbox" class="is-book-stylist" style="float: left; margin-top: 4px; margin-right: 10px;" id="setStylist"  />Khách book Stylist</label>
                            </div>
                        </div>
                        <div class="row list-item-hc">
                            <div class="col-xs-3">
                                <div class="checkbox" style="float: left;">
                                    <label>
                                        <input type="checkbox" data-value="1"  />
                                        Tẩy
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="checkbox" style="float: left;">
                                    <label>
                                        <input type="checkbox" data-value="2"  />
                                        Uốn
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="checkbox" style="float: left;">
                                    <label>
                                        <input type="checkbox" data-value="3"  />
                                        Nhuộm
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row list-item-book-stylist">
                            <div class="col-xs-12">
                                <div class="checkbox" style="float: left; margin-top: 0px; margin-bottom: 2px;">
                                    <label>
                                        <input type="checkbox"  />
                                        Khách book Stylist
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="checkbox" style="float: left; margin-top: 0px; margin-bottom: 2px; display: none;">
                                    <label>
                                        <input type="checkbox" checked="checked"  />
                                        Khách book tại salon
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-group-lg">
                            <div class="col-xs-12">
                                <div class="btn-print-bill btn-action-print" >In phiếu</div>
                                <div class="btn-print-bill btn-action-print-update" >Cập nhật phiếu</div>
                                <div class="btn-print-bill btn-action-book" >Đặt chỗ</div>
                            </div>
                        </div>
                    </div>
                    <% } %>
                </div>
            </div>
        </div>
        <!--/ Popup thêm mới team -->
        <!-- Popup hiển thị ghi chú -->
        <div class="event-bill-add popupNote">

            <div class="container">
                <div class="row">
                    <div class="col-xs-12 popup-head">
                        <p class="popup-hour-frame" style="font-size: 16px; font-weight: 800">Ghi chú của KH và Salon</p>
                    </div>
                    <div class="col-xs-12">
                        <div class="form-group form-group-lg">
                            <div class="col-xs-12">
                                <input type="hidden" id="HDBookingID" />
                                <input type="hidden" id="HDSalonNote" />
                                <table class="table table-condensed" id="tableNote">
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="button-action-wp">
                            <div class="button-action">
                                <div class="showResult" style="display: none"></div>
                                <div class="popup-btn btn-insert" >Xác nhận</div>
                                <div class="popup-btn btn-close" >Đóng</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Popup hiển thị ghi chú -->
        <!-- Popup hủy lịch đặt -->
        <div class="popup-cancel-book">
            <div class="popup-cancel-book-content">
                <p class="popup-title">Bạn có chắc chắn muốn hủy lịch đặt? Vui lòng nhập lý do:</p>
                <div class="note-wrap">
                    <textarea class="note-cancel" placeholder="Nhập lý do hủy lịch đặt" rows="3"></textarea>
                </div>
                <div class="button-action-wp">
                    <div class="button-action">
                        <div class="popup-btn btn-ok" >Xác nhận</div>
                        <div class="popup-btn btn-close" >Đóng</div>
                    </div>
                </div>
            </div>
        </div>
        <!--// Popup hủy lịch đặt -->

        <!-- Page loading -->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
        <!--/ Page loading -->
        <div class="html_current hidden"></div>
        <!-- Iframe phục vụ in hóa đơn -->
        <iframe id="iframePrint" style="display: none;"></iframe>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 150px !important;
                z-index: 9999;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
                z-index: 9999;
            }
        </style>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                var qs = getQueryStrings();
                if (qs["msg_print_billcode"] != undefined) {
                    openPdfIframe("/Public/PDF/" + qs["msg_print_billcode"] + ".pdf");
                }
                showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        //$input.val($input.val().split(' ')[0]);
                    }
                });
            });
            function openPdfIframe(src) {
                var PDF = document.getElementById("iframePrint");
                PDF.src = src;
                PDF.onload = function () {
                    PDF.focus();
                    PDF.contentWindow.print();
                    PDF.contentWindow.close();
                    window.history.pushState({}, 'timeline', '/ver2-beta-timeline');
                }
            };
        </script>
        <!--/ Iframe phục vụ in hóa đơn -->

        <script>
            //Lấy ghi chú của khách hàng và salon theo BookingID
            function getNote(BookingId) {
                $("#HDBookingID").val(BookingId);
                $.ajax({
                    url: "/GUI/FrontEnd/Service/TimeLine_Beta_V2.aspx/GetNote",
                    data: "{BookingId:" + BookingId + "}",
                    type: "POST",
                    contentType: "application/json;charset:UTF-8",
                    datatype: "JSON",
                    success: function (data) {
                        var jsData = JSON.parse(data.d);
                        var row = "<tr>";
                        row += "<td style='width:20%'>Số điện thoại KH:</td><td>" + jsData.CustomerPhone + "</td>";
                        row += "</tr>";
                        if (jsData.TextNote1 != null && jsData.TextNote2 != null) {
                            row += "<tr>";
                            row += "<td style='width:20%'>Ghi chú của KH: </td><td>" + jsData.TextNote1 + ". " + jsData.TextNote2 + "</td>";
                            row += "</tr>";
                        } else if (jsData.TextNote1 != null && jsData.TextNote2 == null) {
                            row += "<tr>";
                            row += "<td style='width:20%'>Ghi chú của KH: </td><td>" + jsData.TextNote1 + "</td>";
                            row += "</tr>";
                        } else if (jsData.TextNote1 == null && jsData.TextNote2 != null) {
                            row += "<tr>";
                            row += "<td style='width:20%'>Ghi chú của KH: </td><td>" + jsData.TextNote2 + "</td>";
                            row += "</tr>";
                        } else {
                        }
                        if (jsData.SalonNote != null) {
                            $(".btn-insert").hide();
                            row += "<tr>";
                            row += "<td style='width:20%'>Ghi chú của Salon: </td><td>" + jsData.SalonNote + "</td>";
                            row += "</tr>";
                        } else {
                            $(".btn-insert").show();
                            row += "<tr>";
                            row += "<td style='line-height:50px;width:20%'>Ghi chú của Salon: </td><td><textarea id='txtSalonNote' row='3' onchange='getSalonNote($(this))' class='form-control' placeholder='Nhập ghi chú của Salon'></textarea></td>";
                            row += "</tr>";
                        }
                        $("#tableNote").empty().append(row);
                    },
                    complete: function () {
                        $(".popupNote").openEBPopup();
                    }
                });
            }
            function getSalonNote(This) {
                $("#HDSalonNote").val(This.val());
            }
            //Lưu ghi chú vào DB
            function InsertNote() {
                var bk = $("#HDBookingID").val();
                var salonNote = $("#HDSalonNote").val();
                //console.log();
                if (salonNote == "" || salonNote == null) {
                    $(".showResult").show();
                    $(".showResult").empty().append("<span style='color:red'>SalonNote không được để trống</span>");
                } else {
                    $(".showResult").hide();
                    $.ajax({
                        url: "/GUI/FrontEnd/Service/TimeLine_Beta_V2.aspx/InsertNote",
                        data: "{BookingId:" + bk + ",SalonNote:'" + salonNote + "'}",
                        type: "POST",
                        dataType: "JSON",
                        contentType: "application/json;charset:UTF-8",
                        success: function (data) {
                            $(".showResult").empty().append(data.d);
                            if (data.d == "<span style='color:green'>Thêm ghi chú thành công</span>") {
                                autoCloseEBPopup();
                                $("#btn-" + bk + "").css("background", "#42f44e");
                            }
                        }
                    });
                }
            }

            //lấy dữ liệu áng thời gian cắt
            //function GetDataBySalon(SalonId) {
            //    $.ajax({
            //        url: "/GUI/FrontEnd/Service/TimeLine_Beta_V2.aspx/EstimateTime",
            //        data: "{c:" + SalonId + "}",
            //        type: "POST",
            //        dataType: "JSON",
            //        contentType: "application/json;charset:UTF-8",
            //        success: function (data) {
            //            var jsData = JSON.parse(data.d);
            //            var content = "";
            //            if (jsData.length > 0) {
            //                for (var i = 0; i < jsData.length; i++) {
            //                    content += "<tr class='"+
            //                        (jsData[i].IsSulphite == true ? 'estimate-box-issulphite' : '') +
            //                        //((jsData[i].Khach_Cho == '0' && jsData[i].Avg_TimeCut != '0') ? ' estimate-box-wait' : '') +
            //                        ((jsData[i].EstimateTime == '0' && jsData[i].Avg_TimeCut == '0') ? 'estimate-box-free' : '') +
            //                        //((jsData[i].EstimateTime == '0' && jsData[i].Avg_TimeCut != '0') ? 'estimate-box-not-active' : '') +
            //                        "'>";
            //                    //content += "<td>" + jsData[i].CustomerName + "</td>";
            //                    content += "<td>" + jsData[i].StylistName + "</td>";
            //                    content += "<td>" + jsData[i].TimeEstimate + "</td>";
            //                    content += "<td>" + jsData[i].Khach_Dang_Cat + "</td>";
            //                    content += "<td>" + jsData[i].Khach_Cho + "</td>";     
            //                    content += "</tr>";
            //                }
            //                $("#tableEstimate").find("tbody").empty().append(content);
            //            }
            //            else {
            //                content += "<tr colspan='4'>Không có dữ liệu</tr>";
            //            }
            //        }
            //    })
            //}
            //lấy dữ liệu áng thời gian gội
            function EstimateTimeMassage(SalonId) {
                $.ajax({
                    url: "/GUI/FrontEnd/Service/TimeLine_Beta_V2.aspx/EstimateTimeMassage",
                    data: "{c:" + SalonId + "}",
                    type: "POST",
                    dataType: "JSON",
                    contentType: "application/json;charset:UTF-8",
                    success: function (data) {
                        var jsData = JSON.parse(data.d);
                        var content = "";
                        if (jsData.length > 0) {
                            for (var i = 0; i < jsData.length; i++) {
                                content += "<tr class='" +
                                    (jsData[i].hoachat == 1 ? 'estimate-box-issulphite' : '') +
                                    //((jsData[i].Khach_Cho == '0' && jsData[i].Avg_TimeCut != '0') ? ' estimate-box-wait' : '') +
                                    ((jsData[i].danggoiTg == '0' && jsData[i].danggoiSl == '0' && jsData[i].chuagoiSl == '0') ? 'estimate-box-free' : '') +
                                    //((jsData[i].EstimateTime == '0' && jsData[i].Avg_TimeCut != '0') ? 'estimate-box-not-active' : '') +
                                    "'>";
                                //content += "<td>" + jsData[i].CustomerName + "</td>";
                                content += "<td>" + jsData[i].skinnerName + "</td>";
                                content += "<td>" + jsData[i].danggoiTg + "</td>";
                                content += "<td>" + jsData[i].danggoiSl + "</td>";
                                content += "<td>" + jsData[i].chuagoiSl + "</td>";
                                content += "</tr>";
                            }
                            $("#tableEstimateTimeMassage").find("tbody").empty().append(content);
                        }
                        else {
                            content += "<tr colspan='4'>Không có dữ liệu</tr>";
                        }
                    }
                })
            }
            //show dữ liệu ra table
            function FetchDataToTable() {
                var SalonID = $("#ddlSalon").val();
                var sID;
                if (SalonID != 0) {
                    sID = SalonID;
                    window.setInterval(function () {
                        console.log("here 1 " + SalonID);
                        //GetDataBySalon(sID);
                        //EstimateTimeMassage(sID);
                    },
                        60000);
                }
                else {
                    sID = $("#ddlSalon").val();
                    window.setInterval(function () {
                        console.log("here 2 " + SalonID);
                        //GetDataBySalon(sID);
                        EstimateTimeMassage(sID);
                    },
                        60000);
                }
            }

            //request mỗi 60s yêu cầu đặc biệt của KH
            function GetCountSpecialRequire() {
                //window.setInterval(function () {
                //    var salonId = $("#ddlSalon").val();
                //    if (salonId > 0) {
                //        CountSpecialRequire(salonId);
                //    }
                //},
                //    60000);
            }
            //đếm số yêu cầu đặc biệt của KH
            function CountSpecialRequire(SalonID) {
                $.ajax({
                    url: "/GUI/FrontEnd/Service/TimeLine_Beta_V2.aspx/CountSpecialRequire",
                    data: "{salonId:" + SalonID + "}",
                    type: "POST",
                    dataType: "JSON",
                    contentType: "application/json;charset:UTF-8",
                    success: function (data) {
                        if (JSON.parse(data.d) != 0) {
                            $("#count").css("background", "red");
                            $("#count").empty().append(JSON.parse(data.d));
                        } else {
                            $("#count").empty().text("0");
                        }
                    },
                });
            }

            //load dữ liệu sau khi pageload
            $(document).ready(function () {
                // Mặc định load dữ liệu estimate time cut khi vào page
                var SalonID = $("#ddlSalon").val();
                //GetDataBySalon(SalonID);
                EstimateTimeMassage(SalonID);
                // Load dữ liệu khách có yêu cầu đặc biệt
                CountSpecialRequire(SalonID);
                // Auto load data estimate-time-cut
                FetchDataToTable();
                GetCountSpecialRequire();
            });
            function viewtdata(This) {
                // click view data
                $("#fakeViewdata").click();
            };
        </script>
    </asp:Panel>
</asp:content>
