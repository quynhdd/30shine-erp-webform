﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.IO;
using System.Web.Helpers;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Windows.Forms;
using LinqKit;
using System.Text;
using System.Data.Entity.Migrations;

namespace _30shine.GUI.FrontEnd.Service
{
    public partial class Service_Book_Pending_V2 : System.Web.UI.Page
    {
        private bool Perm_ShowSalon = false;
        private bool Perm_Access = false;
        
        private List<Appointment> CustomerHistoryAppoint = new List<Appointment>();
        private List<Appointment> CustomerHistoryProduct = new List<Appointment>();
        private List<ProductBasic> ProductList = new List<ProductBasic>();
        private List<ProductBasic> ServiceList = new List<ProductBasic>();
        protected BillService billBooking = new BillService();
        private string _CustomerCode = "";
        private string _CustomerName = "";
        private string Stylist = "";
        private string Skinner = "";
        private string Seller = "";
        private string BillCode = "";
        private string PDFname = "";
        private int SalonId;
        private int times = 0;

        //list booking
        protected Paging PAGING = new Paging();
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected List<TeamService> lstTeamService = new List<TeamService>();

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckPermission();

            CheckIsBooking();
            if (!IsPostBack)
            {
                if (Session["SalonId"] != null)
                {
                    HDF_SalonId.Value = Session["SalonId"].ToString();
                }
                else
                {
                    HDF_SalonId.Value = "0";
                }
                Bind_RptBillService_Appointment();
                Bind_TeamWork();
                TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
                Bind_RptServiceFeatured();
                Bind_RptService();
            }

        }

        private void Bind_RptServiceFeatured()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Services.Where(w => w.IsDelete != 1 && w.Publish == 1 && w.Status == 1).OrderByDescending(o => o.Id).ToList();

                Rpt_ServiceFeatured.DataSource = lst;
                Rpt_ServiceFeatured.DataBind();
            }
        }

        private void Bind_RptService()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Service = db.Services.Where(w => w.IsDelete != 1 && w.Publish == 1 && (w.IsFreeService != 1 || w.IsFreeService == null)).OrderByDescending(o => o.Id).ToList();

                Rpt_Service.DataSource = _Service;
                Rpt_Service.DataBind();
            }
        }

        private void CheckIsBooking()
        {
            /// Check booking
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                int billId = int.TryParse(Request.QueryString["b"], out integer) ? integer : 0;
                if (billId > 0)
                {
                    billBooking = db.BillServices.FirstOrDefault(w => w.Id == billId);
                }
            }
        }

        protected void AddAppointment(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                //// Insert to BillService
                CultureInfo culture = new CultureInfo("vi-VN");
                var timeAppointment = DateTime.Now;

                if (TxtDateTimeFrom.Text.Trim() != "")
                {
                    timeAppointment = Convert.ToDateTime(TxtDateTimeFrom.Text.Trim(), culture);
                }
                var obj = new Appointment();
                int integer;


                obj.AppointmentTime = timeAppointment;

                int stylistId = int.TryParse(HDF_Stylist_Id.Value.ToString(), out integer) ? integer : 0;
                obj.StylistId = stylistId;
                obj.CreatedTime = DateTime.Now;
                obj.ServiceIds = HDF_ServiceIds.Value != "[]" ? HDF_ServiceIds.Value : "";
                
                if (TrSalon.Visible == true)
                {
                    obj.SalonId = Convert.ToInt32(Salon.SelectedValue);
                }
                else
                {
                    obj.SalonId = Convert.ToInt32(Session["SalonId"]);
                }

                obj.CustomerNoInfo = InputCusNoInfor.Checked;
                if (obj.CustomerNoInfo == true)
                {
                    /// Insert khách hàng anonymous
                    var cCode = UIHelpers.GetUniqueKey(8, 8);
                    var newCus = new _30shine.MODEL.ENTITY.EDMX.Customer();
                    newCus.Fullname = cCode;
                    newCus.Customer_Code = cCode;
                    newCus.SalonId = obj.SalonId;
                    newCus.IsDelete = 0;
                    newCus.IsAppointment = true;
                    newCus.CreatedDate = DateTime.Now;
                    db.Customers.AddOrUpdate(newCus);
                    db.SaveChanges();

                    /// Add CustomerCode cho bill
                    obj.CustomerCode = cCode;
                    _CustomerCode = newCus.Customer_Code;
                    _CustomerName = newCus.Fullname;
                }
                else
                {
                    _CustomerCode = HDF_CustomerCode.Value;
                    var customer = db.Customers.FirstOrDefault(w => w.Customer_Code == _CustomerCode && w.IsDelete != 1);
                    if (customer != null)
                    {
                        _CustomerName = customer.Fullname;
                    }
                    obj.CustomerCode = _CustomerCode;
                }
                obj.IsDelete = 0;
                db.Appointments.Add(obj);
                var exc = db.SaveChanges();
                var Error = exc > 0 ? 0 : 1;

                if (Error == 0)
                {
                    var MsgParam = new List<KeyValuePair<string, string>>();
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Thêm thành công!"));
                    UIHelpers.Redirect("/dich-vu/dat-lich.html", MsgParam);
                }
                else
                {
                    MsgSystem.Text = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                    MsgSystem.CssClass = "msg-system warning";
                }

            }
        }

        public void Bind_Salon()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Tbl_Salon.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Order).ToList();
                var Key = 0;
                var Count = LST.Count;
                Salon.DataTextField = "Salon";
                Salon.DataValueField = "Id";
                ListItem item = new ListItem("Chọn salon", "0");
                Salon.Items.Insert(Key, item);
                Key++;

                if (Count > 0)
                {
                    foreach (var v in LST)
                    {
                        item = new ListItem(v.Name, v.Id.ToString());
                        Salon.Items.Insert(Key, item);
                        Key++;
                    }
                }
            }
        }

        //private void Bind_TeamWork()
        //{
        //    using (var db = new Solution_30shineEntities())
        //    {
        //        var lst = db.TeamServices.Where(w => w.IsDelete != 1 && w.Publish == true && w.Status == 1).ToList();
        //        if (billBooking != null && billBooking.TeamId > 0 && lst.Count > 0)
        //        {
        //            for (var i = 0; i < lst.Count; i++)
        //            {
        //                if (lst[i].Id != billBooking.TeamId)
        //                {
        //                    lst.RemoveAt(i--);
        //                }
        //            }
        //            HDF_TeamId.Value = billBooking.TeamId.ToString();
        //            TeamColorSelect.Visible = false;
        //        }
        //        Rpt_TeamWork.DataSource = lst;
        //        Rpt_TeamWork.DataBind();
        //    }
        //}

        [WebMethod]
        public static string GetCustomer(string CustomerCode)
        {
            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var _Customer = db.Customers.FirstOrDefault(w => (w.Customer_Code == CustomerCode || w.Phone == CustomerCode) && w.IsDelete != 1);

                if (_Customer != null)
                {
                    _Msg.success = true;
                    _Msg.msg = serializer.Serialize(_Customer);
                }
                else
                {
                    _Msg.success = false;
                }
            }
            return serializer.Serialize(_Msg);
        }

        [WebMethod]
        public static string CheckDuplicateBill(string CustomerCode)
        {
            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var today = Convert.ToDateTime(DateTime.Now.ToString("d/M/yyyy"), culture);
                var todayL = today.AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                var todayR = today.AddDays(1);
                var _Bill = db.BillServices.FirstOrDefault(w => w.CreatedDate > todayL && w.CreatedDate < todayR && w.CustomerCode == CustomerCode && w.IsDelete != 1 && w.Pending != 1);

                if (_Bill != null)
                {
                    _Msg.success = true;
                    _Msg.msg = serializer.Serialize(_Bill);
                }
                else
                {
                    _Msg.success = false;
                }
            }
            return serializer.Serialize(_Msg);
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["User_Id"] == null)
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "salonmanager", "reception", "checkin", "checkout" };
                string[] AllowSalon = new string[] { "root", "admin" };
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }
                if (Array.IndexOf(AllowSalon, Permission) != -1)
                {
                    Perm_ShowSalon = true;
                }
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
            else if (Perm_ShowSalon)
            {
                TrSalon.Visible = true;
                Bind_Salon();
            }
        }


        //code listpending
        private void Bind_TeamWork()
        {
            using (var db = new Solution_30shineEntities())
            {
                lstTeamService = db.TeamServices.Where(w => w.IsDelete != 1 && w.Publish == true && w.Status == 1).ToList();
            }
        }

        private void Bind_RptBillService_Appointment()
        {
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var _BillService = new List<BillService>();
                var Where = PredicateBuilder.True<Appointment>();
                string SgtValue;
                string sql = "";

                switch (HDF_Suggestion_Field.Value)
                {
                    case "customer.code":
                        SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "customer.name":
                        SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "customer.phone":
                        SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "bill.code": break;
                }

                Where = Where.And(p => p.IsDelete != 1 && p.IsPending == false);
                //int SalonId = Convert.ToInt32(Salon.SelectedValue);
                int integer;
                int SalonId = Session["SalonId"] != null ? (int.TryParse(Session["SalonId"].ToString(), out integer) ? integer : 0) : 0;
                if (!Perm_ViewAllData)
                {
                    Where = Where.And(p => p.SalonId == SalonId);
                }
                else
                {
                    if (SalonId > 0)
                    {
                        Where = Where.And(p => p.SalonId == SalonId);
                    }
                    else
                    {
                        Where = Where.And(p => p.SalonId >= 0);
                    }
                }
                // Điều kiện chỉ lấy pending trong ngày
                Where = Where.And(w => w.CreatedTime >= DateTime.Today);

                sql = @"select a.*, s.Fullname as HairdresserName,
                        -- s3.Fullname as SellerName, 
	                        c.Fullname as CustomerName, c.Phone as CustomerPhone, d.Color as TeamColor
                        from Appointment as a
                        left join Staff as s
                        on a.StylistId = s.Id                     
                        --left join Staff as s3
                        --on a.SellerId = s3.Id
                        left join Customer as c
                        on a.CustomerCode = c.Customer_Code                   
                        left join TeamService as d
                        on a.TeamId = d.Id   
                        where a.IsDelete != 1 and (a.IsCancel is null or a.IsCancel=0)
                         and a.IsPending is null
                        and a.CreatedTime >= '" + String.Format("{0:yyyy/MM/dd}", DateTime.Today) + "'";

                if (SalonId > 0)
                {
                    sql += " and a.SalonId=" + SalonId;
                }
                var lst = db.Database.SqlQuery<Appointment2>(sql).ToList();
                if (lst.Count > 0)
                {
                    for (var i = 0; i < lst.Count; i++)
                    {
                        lst[i].Order = UIHelpers.getBillOrder(lst[i].AppointmentCode, 4);
                        lst[i].NotifyTime = lst[i].AppointmentTime.Value.Add(-TimeSpan.Parse("00:15:00"));
                    }
                }
                RptAppointment.DataSource = lst.OrderBy(o => o.CreatedTime);
                RptAppointment.DataBind();
            }
        }

        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((System.Web.UI.WebControls.Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((System.Web.UI.WebControls.Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
            Bind_RptBillService_Appointment();
        }

        protected void Bind_Paging()
        {
            // init Paging value
            PAGING._Segment = 50;
            //PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? (HDF_Page.Value != "" ? Convert.ToInt32(HDF_Page.Value) : 1) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                int Count;
                var Where = PredicateBuilder.True<BillService>();
                string SgtValue;

                switch (HDF_Suggestion_Field.Value)
                {
                    case "customer.code":
                        SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "customer.name":
                        SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "customer.phone":
                        SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "bill.code": break;
                }

                Where = Where.And(p => p.IsDelete != 1 && p.Pending == 1);
                int integer;
                int SalonId = Session["SalonId"] != null ? (int.TryParse(Session["SalonId"].ToString(), out integer) ? integer : 0) : 0;
                if (!Perm_ViewAllData)
                {
                    Where = Where.And(p => p.SalonId == SalonId);
                }
                else
                {
                    if (SalonId > 0)
                    {
                        Where = Where.And(p => p.SalonId == SalonId);
                    }
                    else
                    {
                        Where = Where.And(p => p.SalonId >= 0);
                    }
                }

                Count = db.BillServices.AsExpandable().Count(Where);

                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }



        private struct PrintRec
        {
            public string Key { get; set; }
            public string Value { get; set; }
            public int Height { get; set; }
        }

        private class Appointment2 : Appointment
        {
            public int Order { get; set; }
            public DateTime NotifyTime { get; set; }
            public string TeamColor { get; set; }
        }
    }
}