﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Linq.Expressions;
using Project.RealtimeFirebase;
using System.Threading.Tasks;
using System.Data.Linq.SqlClient;
using System.Data.SqlClient;
using Project.SystemService.SendSMS;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.FrontEnd.UIService
{
    public partial class TimlineChooseDate : System.Web.UI.Page
    {
        private string PageID = "BETAV2_TIMELINE";
        protected List<string> listHour = new List<string>() { "08:00:00", "08:30:00", "09:00:00", "09:30:00", "10:00:00", "10:30:00", "11:00:00", "11:30:00", "12:00:00", "12:30:00", "13:00:00", "13:30:00", "14:00:00", "14:30:00", "15:00:00", "15:30:00", "16:00:00", "16:30:00", "17:00:00", "17:30:00", "18:00:00", "18:30:00", "19:00:00", "19:30:00", "20:00:00", "20:30:00", "21:00:00", "21:30:00", "22:00:00", "22:30:00" };
        protected List<cls_timeline_stylist> dataTimeline = new List<cls_timeline_stylist>();
        protected List<cls_EstimateTimeData> estimateTime = new List<cls_EstimateTimeData>();
        public Solution_30shineEntities db;

        private bool Perm_Access = false;
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ShowSalon = false;
        protected bool Perm_ViewAllData = false;
        protected List<Tbl_Salon> listSalon = new List<Tbl_Salon>();
        protected List<TeamService> listTeam = new List<TeamService>();
        protected string listService = "";
        protected int salonID = 0;

        public static int integer;
        private JavaScriptSerializer serializer = new JavaScriptSerializer();
        public static MessageNotification messageNotification;

        public static TimeLine_Beta_V2 instance;
        public static Library.SendEmail sendMail;
        CultureInfo culture = new CultureInfo("vi-VN");
        /// <summary>
        /// Constructor
        /// </summary>
        public TimlineChooseDate()
        {
            db = new Solution_30shineEntities();
            messageNotification = new MessageNotification();
            sendMail = new Library.SendEmail();
        }

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ShowSalon = permissionModel.GetActionByActionNameAndPageId("Perm_ShowSalon", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }


        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                Library.Function.bindSalon_NotHoiQuan(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
                salonID = getSalonID();
                TxtDateTimeFrom.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
                
                ddlSalon.SelectedValue = salonID.ToString();

                dataTimeline = getDataStylist2();
                listSalon = getListSalon();
                listTeam = getTeam();
                listService = getListService();
            }
        }

        /// <summary>
        /// Get instance
        /// </summary>
        /// <returns></returns>
        public static TimeLine_Beta_V2 getInstance()
        {
            if (TimeLine_Beta_V2.instance is TimeLine_Beta_V2)
            {
                return TimeLine_Beta_V2.instance;
            }
            else
            {
                return new TimeLine_Beta_V2();
            }
        }

        /// <summary>
        /// Reload data by update panel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void updatePanelBindData(object sender, EventArgs e)
        {
            try
            {
                salonID = Convert.ToInt32(ddlSalon.SelectedValue);
                setSalonID(salonID);
                //Estimate_Time(salonID);

            }
            catch { }
            dataTimeline = getDataStylist2();
            listSalon = getListSalon();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "set bill.SalonID", "bill.salonID = " + salonID + "; console.log(bill);removeLoading();", true);
        }

        /// <summary>
        /// Lấy dữ liệu dịch vụ
        /// </summary>
        /// <returns></returns>
        private string getListService()
        {
            return serializer.Serialize(db.Services.Where(w => w.IsDelete != 1 && w.Publish == 1).OrderBy(o => o.Order)
                .Select(s => new { s.Id, s.Code, s.Name, s.Price, Quantity = 0, s.VoucherPercent }).ToList());
        }

        /// <summary>
        /// Lấy giá trị salonID theo account đăng nhập
        /// </summary>
        /// <returns></returns>
        private int getSalonID()
        {
            int salonID = 0;
            if (Session["SalonId"] != null)
            {
                salonID = int.TryParse(Session["SalonId"].ToString(), out integer) ? integer : 0;
            }
            return salonID;
        }

        /// <summary>
        /// Set salonID
        /// </summary>
        /// <param name="salonID"></param>
        private void setSalonID(int salonID)
        {
            Session["SalonId"] = salonID;
        }

        /// <summary>
        /// Lấy danh sách salon
        /// </summary>
        /// <returns></returns>
        private List<Tbl_Salon> getListSalon()
        {
            var salons = new List<Tbl_Salon>();
            if (Perm_ViewAllData)
            {
                salons = db.Tbl_Salon.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Order).ToList();
            }
            else
            {
                if (Session["SalonId"] != null)
                {
                    int salonID = Convert.ToInt32(Session["SalonId"]);
                    salons = db.Tbl_Salon.Where(w => w.IsDelete != 1 && w.Publish == true).ToList();
                }
            }
            return salons;
        }

        /// <summary>
        /// Lấy danh sách dịch vụ theo bill
        /// </summary>
        /// <param name="billID"></param>
        /// <returns></returns>
        private static List<cls_timeline_service> getServiceByBillID(int billID)
        {
            using (var db = new Solution_30shineEntities())
            {
                return db.Database.SqlQuery<cls_timeline_service>(
                            @"select sv.Id, sv.Name, sv.Code, fs.Price, fs.Quantity, fs.VoucherPercent
	                        from SM_BillTemp as bill
	                        left join SM_BillTemp_FlowService as fs on fs.BillId = bill.Id and fs.IsDelete != 1
	                        left join [Service] as sv on sv.Id = fs.ServiceId
	                        where bill.Id = " + billID).ToList();
            }
        }

        /// <summary>
        /// Lấy danh sách team
        /// </summary>
        /// <returns></returns>
        private List<TeamService> getTeam()
        {
            return db.TeamServices.Where(w => w.IsDelete != 1 && w.Publish == true).ToList();
        }

        /// <summary>
        /// Lấy thông tin nhân viên theo OrderCode
        /// </summary>
        /// <param name="orderCode"></param>
        /// <returns></returns>
        [WebMethod]
        public static object getStaffByCode(string orderCode, int salonID)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                if (salonID == 0)
                {
                    return db.Staffs.Where(w => w.OrderCode == orderCode && w.IsDelete != 1 && w.Active == 1 && w.Type == 5).Select(s => new { s.Id, s.Fullname, s.Phone, s.OrderCode }).FirstOrDefault();
                }
                else
                {
                    return db.Staffs.Where(w => w.OrderCode == orderCode && w.SalonId == salonID && w.IsDelete != 1 && w.Active == 1 && w.Type == 5).Select(s => new { s.Id, s.Fullname, s.Phone, s.OrderCode }).FirstOrDefault();
                }
            }
        }

        /// <summary>
        /// Lấy dữ liệu billTemp theo ID
        /// </summary>
        /// <param name="billTempID"></param>
        /// <returns></returns>
        [WebMethod]
        public static object getBillInfoByID(int billTempID)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var message = new Library.Class.cls_message();
                var billInfo = new cls_timeline_bill();
                var billTemp = db.SM_BillTemp.FirstOrDefault(w => w.Id == billTempID);
                if (billTemp != null)
                {
                    billInfo.billID = billTempID;
                    billInfo.bookingID = billTemp.BookingId != null ? billTemp.BookingId.Value : 0;
                    billInfo.PDFBillCode = billTemp.PDFBillCode;
                    billInfo.HCItem = billTemp.HCItem;

                    var checkin = db.Staffs.FirstOrDefault(w => w.Id == billTemp.ReceptionId);
                    if (checkin != null)
                    {
                        billInfo.checkinID = int.TryParse(checkin.OrderCode, out integer) ? integer : 0;
                        billInfo.checkinName = checkin.Fullname;
                    }
                    var customer = db.Customers.FirstOrDefault(w => w.Id == billTemp.CustomerId);
                    if (customer != null)
                    {
                        billInfo.customerName = customer.Fullname;
                        billInfo.customerPhone = customer.Phone;
                        billInfo.customerId = customer.Id;
                    }
                    var salon = db.Tbl_Salon.FirstOrDefault(w => w.Id == billTemp.SalonId);
                    if (salon != null)
                    {
                        billInfo.salonID = salon.Id;
                        billInfo.salonName = salon.Name;
                    }
                    var stylist = db.Staffs.FirstOrDefault(w => w.Id == billTemp.Staff_Hairdresser_Id);
                    if (stylist != null)
                    {
                        billInfo.stylistID = stylist.Id;
                        billInfo.stylistName = stylist.Fullname;
                    }
                    billInfo.services = getServiceByBillID(billTemp.Id);

                    var booking = db.SM_BookingTemp.FirstOrDefault(w => w.Id == billTemp.BookingId);
                    if (booking != null)
                    {
                        billInfo.bookingID = booking.Id;
                        var hourFrame = db.BookHours.FirstOrDefault(w => w.Id == booking.HourId);
                        if (hourFrame != null)
                        {
                            billInfo.hourID = hourFrame.Id;
                            billInfo.hourFrame = hourFrame.Hour.Replace('h', ':');
                        }
                        billInfo.IsAutoStylist = booking.IsAutoStylist != null ? booking.IsAutoStylist.Value : true;
                    }

                    message.success = true;
                    message.data = billInfo;
                    message.message = "Success!";
                }
                else
                {
                    message.success = true;
                    message.message = "Lỗi. Bản ghi không tồn tại!";
                }

                return message;
            }
        }

        /// <summary>
        /// Set trạng thái gọi điện cho khách hàng
        /// </summary>
        /// <param name="orderCode"></param>
        /// <returns></returns>
        [WebMethod]
        public static object setCallPhone(int bookingID, bool isCall)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var message = new Library.Class.cls_message();
                var serializer = new JavaScriptSerializer();
                var error = 0;
                var bookingRecord = db.SM_BookingTemp.FirstOrDefault(w => w.Id == bookingID);
                if (bookingRecord != null)
                {
                    string sql = @"select MAX([Order]) from SM_BookingTemp
                                where IsDelete = 0 and DatedBook = '" + DateTime.Today + "'and SalonId =" + bookingRecord.SalonId + " and IsCall = 1 and HourId =" + bookingRecord.HourId;
                    int? order = db.Database.SqlQuery<int?>(sql).SingleOrDefault();
                    if (order != null && isCall == true)
                    {
                        bookingRecord.Order = order + 1;
                    }
                    else if (order == null && isCall == true)
                    {
                        bookingRecord.Order = 1;
                    }
                    else if (isCall == false)
                    {
                        bookingRecord.Order = null;
                    }

                    if (bookingRecord.IsCall == null)
                    {
                        bookingRecord.IsCallTime = DateTime.Now;
                    }
                    else
                    {
                        bookingRecord.IsCallTimeModified = DateTime.Now;
                    }
                    bookingRecord.IsCall = isCall;



                    db.SM_BookingTemp.AddOrUpdate(bookingRecord);
                    error += db.SaveChanges() > 0 ? 0 : 1;

                    if (error == 0)
                    {
                        message.success = true;
                        message.message = serializer.Serialize(bookingRecord);
                    }
                    else
                    {
                        message.success = true;
                        message.message = "Cập nhật thất bại. Vui lòng liên hệ nhóm phát triển!";
                    }
                }
                else
                {
                    message.success = true;
                    message.message = "Lỗi. Bản ghi không tồn tại!";
                }

                return message;
            }
        }

        /// <summary>
        /// Hủy đặt lịch
        /// </summary>
        /// <param name="bookingID"></param>
        /// <returns></returns>
        [WebMethod]
        public static object cancelBook(int bookingID, string noteCancel)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var message = new Library.Class.cls_message();
                var error = 0;
                var bookingTempRecord = db.SM_BookingTemp.FirstOrDefault(w => w.Id == bookingID);
                if (bookingTempRecord != null)
                {
                    if (noteCancel != "")
                    {
                        bookingTempRecord.IsDelete = 1;
                        bookingTempRecord.NoteDelete = noteCancel;
                        bookingTempRecord.ModifiedDate = DateTime.Now;
                        db.SM_BookingTemp.AddOrUpdate(bookingTempRecord);
                        error += db.SaveChanges() > 0 ? 0 : 1;

                        // Cập nhật bảng Booking
                        var bookingTemp = db.Bookings.FirstOrDefault(w => w.CustomerPhone == bookingTempRecord.CustomerPhone && w.DatedBook == bookingTempRecord.DatedBook && w.HourId == bookingTempRecord.HourId && w.IsDelete != 1);
                        if (bookingTemp != null)
                        {
                            bookingTemp.IsDelete = 1;
                            bookingTemp.NoteDelete = noteCancel;
                            bookingTemp.ModifiedDate = DateTime.Now;
                            db.Bookings.AddOrUpdate(bookingTemp);
                            error += db.SaveChanges() > 0 ? 0 : 1;

                            //// Nhắn tin hủy lịch
                            //var objectReturn = SendSMSLib.sendSMS("Đã tới giờ phục vụ mà anh chưa tới, nếu muộn quá 10 phút, chúng em xin phép hủy lịch. Có gì cần anh liên lạc lại chúng em hỗ trợ.", bookingTemp.CustomerPhone);
                            //if (objectReturn != null)
                            //{
                            //    bookingTemp.CancelBook_IsSendSMS = true;
                            //    bookingTemp.CancelBook_TimeSend = DateTime.Now;
                            //    bookingTemp.CancelBook_SMSStatus = 1;
                            //    bookingTemp.CancelBook_From = Convert.ToByte(2);

                            //    db.Bookings.AddOrUpdate(bookingTemp);
                            //    db.SaveChanges();
                            //}
                            //else
                            //{
                            //    message.success = false; string messageBody = @"<p>Url : apibooking.30shine.com/customer/booking/frontend<p></br>
                            //        <p>Path : /GUI/FrontEnd/Service/Timeline_Beta_V2.aspx/cancelBook</p></br>
                            //        <p>Exception : " + "Lỗi gửi tin nhắn hủy lịch." + "</p>";

                            //    sendMail.SendMail("vong.lv@hkphone.com.vn", "Exception | apibooking.30shine.com", messageBody);
                            //}
                        }

                        if (error == 0)
                        {
                            message.success = true;
                            message.message = "Cập nhật thành công!";
                        }
                        else
                        {
                            message.success = true;
                            message.message = "Cập nhật thất bại. Vui lòng liên hệ nhóm phát triển!";
                        }
                    }
                    else
                    {
                        message.success = true;
                        message.message = "Lỗi. Vui lòng nhập lý do hủy lịch đặt!";
                    }
                }
                else
                {
                    message.success = true;
                    message.message = "Lỗi. Bản ghi không tồn tại!";
                }

                return message;
            }
        }

        /// <summary>
        /// Tạo bill dịch vụ
        /// </summary>
        /// <param name="billClient"></param>
        /// <returns></returns>
        [WebMethod]
        public static object insertBill(string billClient)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var message = new Library.Class.cls_message();
                var serializer = new JavaScriptSerializer();
                try
                {
                    cls_timeline_bill bill = serializer.Deserialize<cls_timeline_bill>(billClient);
                    var booking = new Booking();
                    if (bill != null)
                    {
                        // Kiểm tra tạo bản ghi Booking cho khách
                        var bookingTemp = new SM_BookingTemp();
                        if (bill.bookingID <= 0)
                        {
                            bookingTemp = new SM_BookingTemp();
                            bookingTemp.CustomerName = bill.customerName;
                            bookingTemp.CustomerPhone = bill.customerPhone;
                            bookingTemp.HourId = bill.hourID;
                            bookingTemp.SalonId = bill.salonID;
                            bookingTemp.StylistId = bill.stylistID;
                            bookingTemp.DatedBook = DateTime.Now.Date;
                            bookingTemp.IsDelete = 0;
                            bookingTemp.CreatedDate = DateTime.Now;
                            bookingTemp.IsBookViaBill = true;
                            bookingTemp.IsBookAtSalon = true;
                            bookingTemp.IsBookOnline = false;
                            try
                            {
                                // Trường hợp ghi nhận tại salon khách có book Stylist
                                bookingTemp.IsAutoStylist = bill.IsAutoStylist;
                                bookingTemp.IsBookStylist = !bill.IsAutoStylist;
                            }
                            catch
                            {
                                bookingTemp.IsAutoStylist = false;
                                bookingTemp.IsBookStylist = true;
                            }
                            db.SM_BookingTemp.AddOrUpdate(bookingTemp);
                            db.SaveChanges();

                            // Insert vào bảng Booking                            
                            booking.CustomerName = bill.customerName.Trim();
                            booking.CustomerPhone = bill.customerPhone.Trim();
                            booking.HourId = bill.hourID;
                            booking.SalonId = bill.salonID;
                            booking.StylistId = bill.stylistID;
                            booking.DatedBook = DateTime.Now.Date;
                            booking.IsDelete = 0;
                            booking.CreatedDate = DateTime.Now;
                            booking.IsBookAtSalon = true;
                            booking.IsBookOnline = false;
                            try
                            {
                                // Trường hợp ghi nhận tại salon khách có book Stylist
                                booking.IsAutoStylist = bill.IsAutoStylist;
                                booking.IsBookStylist = !bill.IsAutoStylist;
                            }
                            catch
                            {
                                booking.IsAutoStylist = false;
                                booking.IsBookStylist = true;
                            }
                            booking.BookingTempId = bookingTemp.Id;

                            db.Bookings.AddOrUpdate(booking);
                            db.SaveChanges();

                            bill.bookingID = bookingTemp.Id;
                        }
                        else
                        {
                            bookingTemp = db.SM_BookingTemp.FirstOrDefault(w => w.Id == bill.bookingID);
                            booking = db.Bookings.FirstOrDefault(w => w.BookingTempId == bill.bookingID);
                        }

                        var customer = db.Customers.FirstOrDefault(w => w.Phone == bill.customerPhone);
                        if (customer == null)
                        {
                            customer = new Customer();
                            customer.Fullname = bill.customerName;
                            customer.Phone = bill.customerPhone;
                            customer.SalonId = bill.salonID;
                            customer.CreatedDate = DateTime.Now;
                            customer.IsDelete = 0;

                            db.Customers.AddOrUpdate(customer);
                            db.SaveChanges();
                        }
                        else if (customer.Fullname == null || (customer.Fullname != null && customer.Fullname == ""))
                        {
                            customer.Fullname = bill.customerName;
                            db.Customers.AddOrUpdate(customer);
                            db.SaveChanges();
                        }

                        // Kiểm tra đã lập hóa đơn cho khách ở khung giờ này hay chưa
                        //var record = db.SM_BillTemp.FirstOrDefault(w => w.SalonId == bill.salonID && w.Staff_Hairdresser_Id == bill.stylistID && w.CustomerId == customer.Id && w.BookingId == bill.bookingID);
                        //if (record == null)
                        //{
                        // I. Insert BillTemp
                        var billTemp = new SM_BillTemp();
                        billTemp.CustomerId = customer.Id;
                        billTemp.SalonId = bill.salonID;
                        billTemp.ServiceIds = serializer.Serialize(bill.services);
                        billTemp.ReceptionId = bill.checkinID;
                        billTemp.TeamId = bill.teamID;
                        billTemp.HCItem = bill.HCItem.TrimEnd(',');
                        if (bill.stylistID > 0)
                        {
                            billTemp.Staff_Hairdresser_Id = bill.stylistID;
                        }
                        if (bill.bookingID > 0)
                        {
                            billTemp.BookingId = bill.bookingID;
                        }

                        billTemp.BillCode = GenBillCode(Convert.ToInt32(bill.salonID), "HD", 4);
                        billTemp.PDFBillCode = billTemp.BillCode + "_" + UIHelpers.GetUniqueKey(6, 6);

                        billTemp.CreatedDate = DateTime.Now;
                        billTemp.IsDelete = 0;

                        db.SM_BillTemp.AddOrUpdate(billTemp);
                        db.SaveChanges();

                        // II. Insert bản ghi BillService
                        var billService = new BillService();
                        billService.CustomerId = customer.Id;
                        billService.CustomerCode = bill.customerPhone;
                        billService.SalonId = billTemp.SalonId;
                        billService.ServiceIds = billTemp.ServiceIds;
                        billService.ReceptionId = billTemp.ReceptionId;
                        billService.TeamId = billTemp.TeamId;
                        billService.HCItem = bill.HCItem.TrimEnd(',');
                        billService.Staff_Hairdresser_Id = billTemp.Staff_Hairdresser_Id;
                        try
                        {
                            billService.BookingId = booking.Id;
                        }
                        catch { }
                        billService.BillCode = billTemp.BillCode;
                        billService.PDFBillCode = billTemp.PDFBillCode;
                        billService.CreatedDate = DateTime.Now;
                        billService.IsDelete = 0;
                        billService.Pending = 1;

                        db.BillServices.AddOrUpdate(billService);
                        db.SaveChanges();

                        // III. Insert bản ghi chi tiết
                        insertBillFlow(db, bill.salonID, billTemp.Id, billService.Id, bill.services);

                        // IV. Print bill
                        //TimelinePrintBillPDF_Beta.GenPDF(false, billTemp, bookingTemp);

                        // V. Map ID BillService vào BillTemp
                        billTemp.BillServiceID = billService.Id;
                        db.SM_BillTemp.AddOrUpdate(billTemp);
                        db.SaveChanges();

                        // Sync to client system
                        bill.billID = billTemp.Id;
                        bill.IsAutoStylist = booking.IsAutoStylist != null ? booking.IsAutoStylist.Value : false;
                        //var clientIDs = db.Realtime_Firebase_TokenID.Where(w=>w.IsDelete != true && w.Active == true).ToList();
                        //if (clientIDs.Count > 0)
                        //{
                        //    foreach (var v in clientIDs)
                        //    {
                        //        messageNotification.SendNotificationWeb(v.CurrentToken, serializer.Serialize(bill));
                        //    }
                        //}
                        try
                        {
                            bill.checkinName = db.Staffs.FirstOrDefault(s => s.Id == billTemp.ReceptionId).Fullname;
                        }
                        catch { }

                        bill.PDFBillCode = billTemp.BillCode;
                        message.success = true;
                        message.message = "Success!";
                        message.data = bill;
                        //}
                        //else
                        //{
                        //    message.success = false;
                        //    message.status = "error";
                        //    message.message = "Bill cho khách hàng tại khung giờ này đã được đặt 1 lần rồi.";
                        //}
                    }
                    else
                    {
                        message.success = false;
                        message.status = "error";
                        message.message = "Lỗi. Dữ liệu truyền lên không đúng.";
                    }
                }
                catch (Exception ex)
                {
                    message.success = false;
                    message.message = ex.Message;
                    return message;
                }

                return message;
            }
        }

        /// <summary>
        /// Cập nhật bill dịch vụ
        /// </summary>
        /// <param name="billClient"></param>
        /// <returns></returns>
        [WebMethod]
        public static object updateBill(string billClient)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var message = new Library.Class.cls_message();
                var error = 0;
                var serializer = new JavaScriptSerializer();
                try
                {
                    cls_timeline_bill bill = serializer.Deserialize<cls_timeline_bill>(billClient);
                    var booking = new Booking();
                    if (bill != null)
                    {
                        // Tạm thời chỉ cập nhật hóa chất
                        // I. Cập nhật bảng SM_BillTemp
                        var billTemp = db.SM_BillTemp.FirstOrDefault(w => w.Id == bill.billID);
                        if (billTemp != null)
                        {
                            billTemp.HCItem = bill.HCItem.TrimEnd(',');
                            billTemp.ModifiedDate = DateTime.Now;
                            db.SM_BillTemp.AddOrUpdate(billTemp);
                            error += db.SaveChanges() > 0 ? 0 : 1;

                            // II. Cập nhật bảng BillService
                            var billService = db.BillServices.FirstOrDefault(w => w.Id == billTemp.BillServiceID);
                            if (billService != null)
                            {
                                billService.HCItem = bill.HCItem.TrimEnd(',');
                                billService.ModifiedDate = DateTime.Now;
                                db.BillServices.AddOrUpdate(billService);
                                error += db.SaveChanges() > 0 ? 0 : 1;
                            }
                            else
                            {
                                message.success = false;
                                message.status = "error";
                                message.message = "Lỗi. Bản ghi bill service không tồn tại.";
                            }

                            bill.billID = billTemp.Id;
                            bill.PDFBillCode = billTemp.PDFBillCode;
                        }
                        else
                        {
                            message.success = false;
                            message.status = "error";
                            message.message = "Lỗi. Bản ghi bill temp không tồn tại.";
                        }

                        if (error == 0)
                        {
                            message.success = true;
                            message.message = "Sussess!";
                            message.data = bill;
                        }
                        else
                        {
                            message.success = false;
                            message.message = "Failed!";
                        }
                    }
                    else
                    {
                        message.success = false;
                        message.status = "error";
                        message.message = "Lỗi. Dữ liệu truyền lên không đúng.";
                    }
                }
                catch (Exception ex)
                {
                    message.success = false;
                    message.message = ex.Message;
                    return message;
                }

                return message;
            }
        }

        [WebMethod]
        public static object checkBooking(string billClient, int isDelete)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var message = new Library.Class.cls_message();
                var serializer = new JavaScriptSerializer();
                try
                {
                    var bill = serializer.Deserialize<cls_timeline_bill>(billClient);

                    if (bill != null)
                    {
                        var datebook = DateTime.Now.Date;
                        var bookingTemp = new SM_BookingTemp();
                        var countBookTemp = db.SM_BookingTemp.Where(p => p.CustomerPhone.Trim() == bill.customerPhone.Trim() && p.DatedBook == datebook && p.IsDelete != 1);
                        if (countBookTemp.Count() > 0)
                        {
                            message.success = false;
                            message.status = "delete";
                            message.message = "Khách đã book. Bạn có muốn chắc chắc chắc muốn xoá";
                        }
                        else
                        {

                            bookingTemp.CustomerName = bill.customerName.Trim();
                            bookingTemp.CustomerPhone = bill.customerPhone.Trim();
                            bookingTemp.HourId = bill.hourID;
                            bookingTemp.SalonId = bill.salonID;
                            bookingTemp.StylistId = bill.stylistID;
                            bookingTemp.DatedBook = DateTime.Now.Date;
                            bookingTemp.IsDelete = 0;
                            bookingTemp.CreatedDate = DateTime.Now;
                            bookingTemp.IsBookAtSalon = bill.IsBookAtSalon;
                            bookingTemp.IsBookOnline = !bill.IsBookAtSalon;

                            try
                            {
                                // Trường hợp ghi nhận tại salon khách có book Stylist
                                bookingTemp.IsAutoStylist = bill.IsAutoStylist;
                                bookingTemp.IsBookStylist = !bill.IsAutoStylist;
                            }
                            catch
                            {
                                bookingTemp.IsAutoStylist = false;
                                bookingTemp.IsBookStylist = true;
                            }

                            db.SM_BookingTemp.AddOrUpdate(bookingTemp);
                            db.SaveChanges();

                            message.success = true;
                            message.message = serializer.Serialize(bookingTemp);

                            // Kiểm tra insert khách hàng
                            var customer = db.Customers.FirstOrDefault(w => w.Phone == bookingTemp.CustomerPhone);
                            if (customer == null)
                            {
                                customer = new Customer();
                                customer.Fullname = bill.customerName;
                                customer.Phone = bill.customerPhone;
                                customer.SalonId = bill.salonID;
                                customer.CreatedDate = DateTime.Now;
                                customer.IsDelete = 0;
                                db.Customers.AddOrUpdate(customer);
                                db.SaveChanges();
                            }
                            else
                            {
                                try
                                {
                                    //Update ten khach hang khi ban ghi khach hang da ton tai nhung khong co ten trang bang Customer
                                    if (customer.Fullname == null)
                                    {
                                        customer.Fullname = bill.customerName.Trim();
                                    }
                                    else if (customer.Fullname.Trim().Length == 0)
                                    {
                                        customer.Fullname = bill.customerName.Trim();
                                    }
                                    db.SaveChanges();
                                }
                                catch { }
                            }

                            //update tbl BookingLog
                            var modelBook = new BookingModel();
                            var objBookingLog = new BookingChangeStylist();
                            objBookingLog.BookingTmpId = bookingTemp.Id;
                            objBookingLog.HourId = bookingTemp.HourId;
                            objBookingLog.CustomerPhone = bookingTemp.CustomerPhone;
                            objBookingLog.StylistCurrentId = bookingTemp.StylistId;
                            objBookingLog.ModifiedTime = DateTime.Now;
                            var bookings = modelBook.UpdateBkStylist(objBookingLog);
                        }

                        var countBook = db.Bookings.Where(p => p.CustomerPhone.Trim() == bill.customerPhone.Trim() && p.DatedBook == datebook && p.HourId == bill.hourID && p.IsDelete != 1);
                        if (countBook.Count() > 0)
                        {
                            message.success = false;
                            message.status = "delete";
                            message.message = "Khách đã book. Bạn có muốn chắc chắc chắc muốn xoá";
                        }
                        else
                        {
                            var booking = new Booking();
                            booking.CustomerName = bill.customerName.Trim();
                            booking.CustomerPhone = bill.customerPhone.Trim();
                            booking.HourId = bill.hourID;
                            booking.SalonId = bill.salonID;
                            booking.StylistId = bill.stylistID;
                            booking.DatedBook = DateTime.Now.Date;
                            booking.IsDelete = 0;
                            booking.CreatedDate = DateTime.Now;
                            booking.IsBookAtSalon = bill.IsBookAtSalon;
                            booking.IsBookOnline = !bill.IsBookAtSalon;

                            try
                            {
                                // Trường hợp ghi nhận tại salon khách có book Stylist
                                booking.IsAutoStylist = bill.IsAutoStylist;
                                booking.IsBookStylist = !bill.IsAutoStylist;
                            }
                            catch
                            {
                                booking.IsAutoStylist = false;
                                booking.IsBookStylist = true;
                            }
                            booking.BookingTempId = bookingTemp.Id;

                            db.Bookings.AddOrUpdate(booking);
                            db.SaveChanges();
                        }
                    }

                }

                catch (Exception ex)
                {
                    message.success = false;
                    message.message = ex.Message;
                    return message;
                }
                return message;
            }
        }

        /// <summary>
        /// Update lại thông tin cho khách (drag and drop cell timeline)
        /// </summary>
        /// <param name="customerPhone"></param>
        /// <param name="bookingId"></param>
        /// <param name="stylistId"></param>
        /// <param name="hourId"></param>
        /// <returns></returns>
        [WebMethod]
        public static object updateBooking(string customerPhone, int bookingId, int stylistId, int hourId)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var message = new Library.Class.cls_message();
                var serializer = new JavaScriptSerializer();
                try
                {
                    var instance = TimeLine_Beta_V2.getInstance();
                    var dataBooking = new store_Timeline_getInfoBookingTempById_Result();
                    var datebook = DateTime.Now.Date;

                    var booking = db.Bookings.FirstOrDefault(p => p.BookingTempId == bookingId && p.IsDelete != 1);
                    if (booking != null)
                    {
                        //update tbl Booking
                        booking.HourId = hourId;
                        booking.StylistId = stylistId;
                        booking.ModifiedDate = DateTime.Now;
                        db.Bookings.AddOrUpdate(booking);
                        var result1 = db.SaveChanges();
                        //DŨNG EDIT
                        var modelBook = new BookingModel();
                        var objBookingLog = new BookingChangeStylist();
                        //update tbl BookingLog
                        objBookingLog.BookingTmpId = bookingId;
                        objBookingLog.HourId = hourId;
                        objBookingLog.CustomerPhone = customerPhone;
                        objBookingLog.StylistCurrentId = stylistId;
                        objBookingLog.ModifiedTime = DateTime.Now;
                        var bookings = modelBook.UpdateBkStylist(objBookingLog);
                        //END 
                        var bookingTemp = db.SM_BookingTemp.FirstOrDefault(p => p.Id == bookingId && p.IsDelete != 1);
                        if (bookingTemp != null)
                        {
                            //update tbl SM_BookingTemp
                            //bookingTemp.Order = instance.getOrderBooking(bookingTemp, hourId);
                            bookingTemp.HourId = hourId;
                            bookingTemp.StylistId = stylistId;
                            bookingTemp.ModifiedDate = DateTime.Now;
                            db.SM_BookingTemp.AddOrUpdate(bookingTemp);
                            var result = db.SaveChanges();
                        }

                        // Kiểm tra hủy bản ghi [BillService], [SM_BillTemp]
                        instance.updateBooking_DeleteBill(booking);

                        // Lấy data response
                        dataBooking = db.store_Timeline_getInfoBookingTempById(bookingTemp.Id).SingleOrDefault();
                    }

                    message.success = true;
                    message.status = "update";
                    message.message = serializer.Serialize(dataBooking);
                }

                catch (Exception ex)
                {
                    message.success = false;
                    message.message = ex.Message;
                    return message;
                }

                return message;
            }
        }

        /// <summary>
        /// Hủy bản ghi [BillService] trường hợp update kéo thả trên timeline
        /// Chỉ áp dụng hủy cho các bản ghi đang pending (chưa checkout)
        /// Đảm bảo không ảnh hưởng đến doanh số/doanh thu đã ghi nhận
        /// </summary>
        /// <param name="booking"></param>
        public void updateBooking_DeleteBill(Booking booking)
        {
            try
            {
                var bill = this.db.BillServices.FirstOrDefault(w => w.BookingId == booking.Id);
                if (bill != null && bill.Pending == 1)
                {
                    // Delete in [BillService]
                    bill.IsDelete = 1;
                    bill.ModifiedDate = DateTime.Now;
                    db.BillServices.AddOrUpdate(bill);
                    db.SaveChanges();

                    // Delete in [SM_BillTemp]
                    var billTemp = this.db.SM_BillTemp.FirstOrDefault(w => w.BillServiceID == bill.Id);
                    if (billTemp != null)
                    {
                        billTemp.IsDelete = 1;
                        billTemp.ModifiedDate = DateTime.Now;
                        db.SM_BillTemp.AddOrUpdate(billTemp);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Lấy số thứ tự của khách
        /// Áp dụng với các khách được check là đã đến
        /// </summary>
        /// <param name="bookingTemp"></param>
        /// <param name="hourID"></param>
        /// <returns></returns>
        public int? getOrderBooking(SM_BookingTemp bookingTemp, int hourID)
        {
            try
            {
                int? orderRet = null;
                string sql = @"select MAX([Order]) from SM_BookingTemp
                                    where IsDelete = 0 and DatedBook = '" + DateTime.Today + "'and SalonId =" + bookingTemp.SalonId + " and IsCall = 1 and HourId =" + hourID;
                int? order = this.db.Database.SqlQuery<int?>(sql).FirstOrDefault();
                if (order != null && bookingTemp.IsCall == true)
                {
                    if (bookingTemp.Order == order)
                    {
                        orderRet = order;
                    }
                    else if (bookingTemp.HourId != hourID)
                    {
                        orderRet = order + 1;
                    }
                }
                else if (order == null && bookingTemp.IsCall == true)
                {
                    orderRet = 1;
                }
                else if (bookingTemp.IsCall == false)
                {
                    orderRet = null;
                }

                return orderRet;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }


        /// <summary>
        /// Đặt lịch trực tiếp cho khách
        /// </summary>
        /// <param name="billClient"></param>
        /// <returns></returns>
        [WebMethod]
        public static object insertBooking(string billClient, int isDelete)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var message = new Library.Class.cls_message();
                var serializer = new JavaScriptSerializer();
                try
                {
                    var bill = serializer.Deserialize<cls_timeline_bill>(billClient);
                    var datebook = DateTime.Now.Date;
                    var bookingTemp = new SM_BookingTemp();
                    if (bill != null)
                    {
                        var countBookTemp = db.SM_BookingTemp.Where(p => p.CustomerPhone.Trim() == bill.customerPhone.Trim() && p.DatedBook == datebook && p.IsDelete != 1).ToList();
                        if (countBookTemp.Count() > 0)
                        {
                            if (isDelete == 1)
                            {
                                foreach (var item in countBookTemp)
                                {
                                    var deleteBook = db.SM_BookingTemp.Single(p => p.Id == item.Id);
                                    deleteBook.IsDelete = 1;
                                    //deleteBook.NoteDelete = noteCancel;
                                    deleteBook.ModifiedDate = DateTime.Now;
                                }
                                db.SaveChanges();
                            }
                            //var bookingTemp = new SM_BookingTemp();
                            bookingTemp.CustomerName = bill.customerName.Trim();
                            bookingTemp.CustomerPhone = bill.customerPhone.Trim();
                            bookingTemp.HourId = bill.hourID;
                            bookingTemp.SalonId = bill.salonID;
                            bookingTemp.StylistId = bill.stylistID;
                            bookingTemp.DatedBook = DateTime.Now.Date;
                            bookingTemp.IsDelete = 0;
                            bookingTemp.CreatedDate = DateTime.Now;
                            bookingTemp.IsBookAtSalon = true;
                            bookingTemp.IsBookOnline = false;

                            try
                            {
                                // Trường hợp ghi nhận tại salon khách có book Stylist
                                bookingTemp.IsAutoStylist = bill.IsAutoStylist;
                                bookingTemp.IsBookStylist = !bill.IsAutoStylist;
                            }
                            catch
                            {
                                bookingTemp.IsAutoStylist = false;
                                bookingTemp.IsBookStylist = true;
                            }

                            db.SM_BookingTemp.AddOrUpdate(bookingTemp);
                            db.SaveChanges();
                            message.success = true;
                            message.message = serializer.Serialize(bookingTemp);

                        }


                        var countBook = db.Bookings.Where(p => p.CustomerPhone.Trim() == bill.customerPhone.Trim() && p.DatedBook == datebook && p.IsDelete != 1).ToList();
                        if (countBook.Count() > 0)
                        {
                            if (isDelete == 1)
                            {
                                foreach (var item in countBook)
                                {
                                    var deleteBook = db.Bookings.Single(p => p.Id == item.Id);
                                    deleteBook.IsDelete = 1;
                                    // deleteBook.NoteDelete = noteCancel;
                                    deleteBook.ModifiedDate = DateTime.Now;
                                }
                                db.SaveChanges();
                            }
                            // Insert vào bảng Booking
                            var booking = new Booking();
                            booking.CustomerName = bill.customerName.Trim();
                            booking.CustomerPhone = bill.customerPhone.Trim();
                            booking.HourId = bill.hourID;
                            booking.SalonId = bill.salonID;
                            booking.StylistId = bill.stylistID;
                            booking.DatedBook = DateTime.Now.Date;
                            booking.IsDelete = 0;
                            booking.CreatedDate = DateTime.Now;
                            booking.BookingTempId = bookingTemp.Id;
                            booking.IsBookAtSalon = true;
                            booking.IsBookOnline = false;

                            try
                            {
                                // Trường hợp ghi nhận tại salon khách có book Stylist
                                booking.IsAutoStylist = bill.IsAutoStylist;
                                booking.IsBookStylist = !bill.IsAutoStylist;
                            }
                            catch
                            {
                                booking.IsAutoStylist = false;
                                booking.IsBookStylist = true;
                            }
                            booking.BookingTempId = bookingTemp.Id;
                            db.Bookings.AddOrUpdate(booking);
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        message.success = false;
                        message.status = "error";
                        message.message = "Lỗi. Dữ liệu truyền lên không đúng.";
                    }
                }
                catch (Exception ex)
                {
                    message.success = false;
                    message.message = ex.Message;
                    return message;
                }

                return message;
            }
        }

        /// <summary>
        /// Insert TokenID
        /// </summary>
        /// <param name="currentToken"></param>
        /// <returns></returns>
        [WebMethod]
        public static object updateTokenID(string currentToken)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var error = 0;
                var message = new Library.Class.cls_message();
                var record = db.Realtime_Firebase_TokenID.FirstOrDefault(w => w.CurrentToken == currentToken);
                if (record == null)
                {
                    record = new Realtime_Firebase_TokenID();
                    record.CurrentToken = currentToken;
                    record.CreatedTime = DateTime.Now;
                    record.IsDelete = false;
                    record.Active = true;

                    db.Realtime_Firebase_TokenID.AddOrUpdate(record);
                    error += db.SaveChanges() > 0 ? 0 : 1;
                    if (error == 0)
                    {
                        message.success = true;
                        message.message = "Success!";
                    }
                    else
                    {
                        message.success = false;
                        message.message = "Failed!";
                    }
                }
                else
                {
                    message.success = true;
                    message.message = "Success! Is set TokenID";
                }

                return message;
            }
        }

        /// <summary>
        /// Insert bản ghi chi tiết dịch vụ
        /// </summary>
        /// <param name="db"></param>
        /// <param name="billID"></param>
        /// <param name="listService"></param>
        public static void insertBillFlow(Solution_30shineEntities db, int salonID, int billID, int billServiceID, List<cls_timeline_service> listService)
        {
            if (listService.Count > 0)
            {
                var billTempFlow = new SM_BillTemp_FlowService();
                var billFlowService = new FlowService();
                var services = db.Services.Where(w => w.IsDelete != 1 && w.Publish == 1).ToList();
                var service = new MODEL.ENTITY.EDMX.Service();
                foreach (var v in listService)
                {
                    service = services.FirstOrDefault(w => w.Id == v.Id);
                    // I. SM_BillTemp_FlowService
                    billTempFlow = new SM_BillTemp_FlowService();
                    billTempFlow.SalonId = salonID;
                    billTempFlow.BillId = billID;
                    billTempFlow.ServiceId = v.Id;
                    billTempFlow.Quantity = 1;
                    billTempFlow.IsDelete = 0;
                    billTempFlow.CreatedDate = DateTime.Now;
                    if (service != null)
                    {
                        billTempFlow.Price = service.Price;
                        billTempFlow.VoucherPercent = service.VoucherPercent;
                        billTempFlow.CoefficientRating = service.CoefficientRating;
                    }

                    db.SM_BillTemp_FlowService.AddOrUpdate(billTempFlow);
                    db.SaveChanges();

                    // II. Flowservice
                    billFlowService = new FlowService();
                    billFlowService.SalonId = salonID;
                    billFlowService.BillId = billServiceID;
                    billFlowService.ServiceId = v.Id;
                    billFlowService.Quantity = 1;
                    billFlowService.IsDelete = 0;
                    billFlowService.CreatedDate = DateTime.Now;
                    if (service != null)
                    {
                        billFlowService.Price = service.Price;
                        billFlowService.VoucherPercent = service.VoucherPercent;
                        billFlowService.CoefficientRating = service.CoefficientRating;
                    }

                    db.FlowServices.AddOrUpdate(billFlowService);
                    db.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Generate bill code
        /// </summary>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public static string GenBillCode(int salonId, string prefix, int template = 4)
        {
            using (var db = new Solution_30shineEntities())
            {
                string code = "";
                string temp = "";
                int integer;
                var Where = PredicateBuilder.True<BillService>();
                DateTime _FromDate = DateTime.Today;
                DateTime _ToDate = DateTime.Today.AddDays(1);
                Where = Where.And(w => w.IsDelete != 1 && w.CreatedDate >= _FromDate && w.CreatedDate < _ToDate);
                if (salonId > 0)
                {
                    Where = Where.And(w => w.SalonId == salonId);
                }
                var LastBill = db.BillServices.AsExpandable().Where(Where).OrderByDescending(w => w.Id).Take(1).ToList();
                if (LastBill.Count > 0)
                {
                    if (LastBill[0].BillCode != null && LastBill[0].BillCode != "")
                        temp = (int.TryParse(LastBill[0].BillCode.Substring(LastBill[0].BillCode.Length - template, template), out integer) ? ++integer : 0).ToString();
                    else
                        temp = "1";
                }
                else
                {
                    temp = "1";
                }

                int len = temp.Length;
                for (var i = 0; i < template; i++)
                {
                    if (len > 0)
                    {
                        code = temp[--len].ToString() + code;
                    }
                    else
                    {
                        code = "0" + code;
                    }
                }

                return prefix + String.Format("{0:ddMMyy}", DateTime.Now) + code;
            }
        }

        /// <summary>
        /// Lấy dữ liệu Stylist
        /// </summary>
        /// <returns></returns>
        private List<cls_timeline_stylist> getDataStylist2()
        {
            var data = new List<cls_timeline_stylist>();
            var item = new cls_timeline_stylist();
            var itemHour = new cls_timeline_stylist_hour();
            var itemHourData = new cls_timeline_stylist_hour_data();
            var indexData = -1;

            //var salonId = 0;
            //if (Session["SalonId"] != null)
            //{
            //    salonId = Convert.ToInt32(Session["SalonId"]);
            //}

            if (salonID > 0/* || (salonID == 0 && Perm_ViewAllData)*/)
            {
                //var WorkDate = String.Format("{0:yyyy/MM/dd}", DateTime.Now.Date);
                DateTime Date = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                var WorkDate = String.Format("{0:yyyy/MM/dd}", Date);
                var salonHour = genListHour(salonID);
                var stylistHour = db.Store_SM_BookingTemp_StylistHour_Update(WorkDate, salonID).ToList();
                var stylistDataBook = db.Store_SM_BookingTemp_StylistData(WorkDate, salonID).ToList();

                if (stylistHour.Count > 0)
                {
                    // 1. Lấy danh sách Stylist phục vụ hôm nay (Stylist được chấm công)
                    foreach (var v in stylistHour)
                    {
                        // Khởi tạo giá trị cho list data
                        indexData = data.FindIndex(w => w.StylistId == v.StylistId);
                        if (indexData == -1)
                        {
                            try
                            {
                                item = new cls_timeline_stylist();
                                item.StylistId = v.StylistId.Value;
                                item.StylistName = v.StylistName;
                                item.GroupLevelId = v.GroupLevelId.Value;
                                item.TeamId = v.TeamId.Value;
                                try
                                {
                                    item.IsEnroll = Convert.ToBoolean(v.IsEnroll.Value);
                                }
                                catch
                                {
                                    item.IsEnroll = false;
                                }
                                item.ListHour = salonHour;
                                data.Add(item);
                            }
                            catch { }
                        }
                    }
                    // 2. Kiểm tra gán giá trị [Phục vụ] tại các khung giờ mà Stylist điểm danh, và gán các giá trị booking, bill cho khung giờ
                    var dataTemp = new List<cls_timeline_stylist>();
                    var itemDataTemp = new cls_timeline_stylist();
                    if (data.Count > 0)
                    {
                        foreach (var v in data)
                        {
                            itemDataTemp = new cls_timeline_stylist();
                            itemDataTemp.StylistId = v.StylistId;
                            itemDataTemp.StylistName = v.StylistName;
                            itemDataTemp.GroupLevelId = v.GroupLevelId;
                            itemDataTemp.TeamId = v.TeamId;
                            itemDataTemp.BillTotal = stylistDataBook.Count(w => w.StylistId == v.StylistId && w.BillId != null && w.CompleteBillTime != null);
                            itemDataTemp.ListHour = new List<cls_timeline_stylist_hour>();
                            if (v.ListHour.Count > 0)
                            {
                                foreach (var v2 in v.ListHour)
                                {
                                    var itemHourTemp = new cls_timeline_stylist_hour();
                                    itemHourTemp.HourId = v2.HourId;
                                    itemHourTemp.HourFrame = v2.HourFrame;
                                    itemHourTemp.HourData = new List<cls_timeline_stylist_hour_data>();

                                    if (v.IsEnroll)
                                    {
                                        var enroll = stylistHour.FirstOrDefault(w => w.StylistId == v.StylistId && w.HourId == v2.HourId);
                                        if (enroll != null)
                                        {
                                            itemHourTemp.IsServiced = true;
                                        }
                                        else
                                        {
                                            itemHourTemp.IsServiced = false;
                                        }
                                    }
                                    else
                                    {
                                        itemHourTemp.IsServiced = false;
                                    }

                                    var listBooking = stylistDataBook.Where(w => w.StylistId == v.StylistId && w.HourId == v2.HourId).ToList();
                                    if (listBooking.Count > 0)
                                    {
                                        foreach (var v3 in listBooking)
                                        {
                                            var itemHourDataTemp = new cls_timeline_stylist_hour_data();

                                            itemHourDataTemp.CustomerName = v3.CustomerName;
                                            itemHourDataTemp.CustomerPhone = v3.CustomerPhone;
                                            itemHourDataTemp.BookingID = v3.Id;
                                            itemHourDataTemp.IsCall = v3.IsCall;
                                            itemHourDataTemp.IsCallTime = v3.IsCallTime;
                                            itemHourDataTemp.IsCallTimeModified = v3.IsCallTimeModified;
                                            itemHourDataTemp.BillID = v3.BillId;
                                            itemHourDataTemp.InProcedureTime = v3.InProcedureTime;
                                            itemHourDataTemp.InProcedureTimeModifed = v3.InProcedureTimeModifed;
                                            itemHourDataTemp.CompleteBillTime = v3.CompleteBillTime;
                                            itemHourDataTemp.Mark = v3.Mark;
                                            itemHourDataTemp.BillCreatedDate = v3.BillCreatedDate;
                                            itemHourDataTemp.IsHCItem = (v3.HCItem != null && v3.HCItem != "") ? true : false;
                                            itemHourDataTemp.Order = v3.Order;
                                            itemHourDataTemp.SalonNote = v3.SalonNote;
                                            itemHourDataTemp.TextNote1 = v3.TextNote1;
                                            itemHourDataTemp.TextNote2 = v3.TextNote2;
                                            try
                                            {
                                                itemHourDataTemp.IsAutoStylist = v3.IsAutoStylist.Value;
                                            }
                                            catch
                                            {
                                                itemHourDataTemp.IsAutoStylist = true;
                                            }

                                            try
                                            {
                                                itemHourDataTemp.IsBookAtSalon = v3.IsBookAtSalon.Value;
                                            }
                                            catch
                                            {
                                                itemHourDataTemp.IsBookAtSalon = false;
                                            }
                                            var vipCustomer = db.Store_SpecialCustomer_CheckVIPCustomer(0, v3.CustomerPhone).FirstOrDefault();
                                            if (vipCustomer != null)
                                            {
                                                itemHourDataTemp.CustomerType = vipCustomer.CustomerTypeId;
                                            }
                                            else
                                            {
                                                var specialCustomer = db.Store_SpecialCustomer_CheckSpecialCustomer(0, v3.CustomerPhone).FirstOrDefault();
                                                if (specialCustomer != null)
                                                {
                                                    itemHourDataTemp.CustomerType = specialCustomer.CustomerTypeId;
                                                }
                                            }

                                            itemHourTemp.HourData.Add(itemHourDataTemp);
                                        }
                                    }
                                    itemDataTemp.ListHour.Add(itemHourTemp);
                                }
                            }
                            dataTemp.Add(itemDataTemp);
                        }

                        data = dataTemp;
                    }
                }
            }

            return data.OrderBy(w => w.TeamId).ThenBy(w => w.StylistId).ThenBy(w => w.GroupLevelId).ToList();
        }

        /// <summary>
        /// Tạo dữ liệu mẫu các khung giờ theo salon
        /// </summary>
        /// <param name="salonID"></param>
        /// <returns></returns>
        private List<cls_timeline_stylist_hour> genListHour(int salonID)
        {
            var listHourTemp = new List<cls_timeline_stylist_hour>();
            var item = new cls_timeline_stylist_hour();
            var salonHour = db.Store_SM_BookingTemp_SalonHourFrame(salonID).ToList();
            if (listHour.Count > 0)
            {
                foreach (var v in listHour)
                {
                    item = new cls_timeline_stylist_hour();
                    var hourTemp = salonHour.FirstOrDefault(w => w.HourFrameStr == v);
                    if (hourTemp != null)
                    {
                        item.HourId = hourTemp.Id;
                        item.HourFrame = hourTemp.HourFrameStr;
                        item.HourData = new List<cls_timeline_stylist_hour_data>();
                    }
                    listHourTemp.Add(item);
                }
            }
            return listHourTemp;
        }


        /// <summary>
        /// Check khách đặc biệt - khách VIP
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        [WebMethod]
        public static object checkCustomerType(int customerId, string customerPhone)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var message = new Library.Class.cls_message();
                var serializer = new JavaScriptSerializer();
                try
                {
                    var vipCustomer = db.Store_SpecialCustomer_CheckVIPCustomer(customerId, customerPhone).FirstOrDefault();
                    if (vipCustomer != null)
                    {
                        message.success = true;
                        message.message = serializer.Serialize(vipCustomer);
                    }
                    else
                    {
                        var specialCustomer = db.Store_SpecialCustomer_CheckSpecialCustomer(customerId, customerPhone).FirstOrDefault();
                        if (specialCustomer != null)
                        {
                            message.success = true;
                            message.message = serializer.Serialize(specialCustomer);
                        }
                    }
                }
                catch (Exception ex)
                {
                    message.success = false;
                    message.message = ex.Message;
                    return message;
                }
                return message;
            }
        }

        /// <summary>
        /// Update tên khách hàng [CustomerName] trong [dbo].[Booking], [dbo].[SM_BookingTemp]
        /// </summary>
        /// <param name="customerName"></param>
        /// <param name="bookingID"></param>
        /// <returns></returns>
        [WebMethod]
        public static object setCustomerName(string customerName, int bookingID)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var message = new Library.Class.cls_message();
                try
                {

                    var bookingTmp = db.SM_BookingTemp.FirstOrDefault(f => f.Id == bookingID);
                    if (bookingTmp != null)
                    {
                        bookingTmp.CustomerName = customerName;
                        db.SM_BookingTemp.AddOrUpdate(bookingTmp);
                        var booking = db.Bookings.FirstOrDefault(f => f.BookingTempId == bookingTmp.Id);
                        if (booking != null)
                        {
                            booking.CustomerName = customerName;
                            db.Bookings.AddOrUpdate(booking);
                        }
                        else
                        {
                            throw new Exception("Error!!!");
                        }
                        db.SaveChanges();

                        try
                        {
                            //Update ten khach hang khi ban ghi khach hang da ton tai nhung khong co ten trang bang Customer
                            var objCus = db.Customers.Where(a => a.IsDelete == 0 && a.Phone == bookingTmp.CustomerPhone).OrderByDescending(a => a.Id).FirstOrDefault();
                            if (objCus != null)
                            {
                                if (objCus.Fullname == null)
                                {
                                    objCus.Fullname = customerName;
                                }
                                else if (objCus.Fullname.Trim().Length == 0)
                                {
                                    objCus.Fullname = customerName;
                                }
                                db.SaveChanges();
                            }
                        }
                        catch { }

                        message.success = true;
                    }
                    else
                    {
                        throw new Exception("Error!!!");
                    }

                }
                catch (Exception ex)
                {
                    message.success = false;
                }
                return message;
            }
        }
        /// <summary>
        /// Estimate time cut
        /// </summary>
        [WebMethod]
        public static object EstimateTime(int c)
        {
            try
            {
                var db = new Solution_30shineEntities();

                var data = db.Store_Estimate_TimeCut_V2(c).ToList();
                List<cls_EstimateTimeData> listCls = new List<cls_EstimateTimeData>();
                foreach (var item in data)
                {
                    cls_EstimateTimeData cls = new cls_EstimateTimeData();
                    cls.StylistName = item.StylistName;
                    cls.Avg_TimeCut = item.AVG_TIME_CUT != null ? item.AVG_TIME_CUT.Value : 0;
                    cls.Chuagoi = item.CHUAGOI != null ? item.CHUAGOI.Value : 0;
                    cls.Dagoi = item.DAGOI_SL != null ? item.DAGOI_SL.Value : 0;
                    cls.Danggoi_sl = item.DANGGOI_SL != null ? item.DANGGOI_SL.Value : 0;
                    cls.Danggoi_tg = item.DANGGOI_TIME != null ? item.DANGGOI_TIME.Value : 0;
                    cls.Khach_Cho = item.KHACH_CHO != null ? item.KHACH_CHO.Value : 0;
                    cls.Khach_Dang_Cat = item.KHACH_DANG_PHUCVU != null ? item.KHACH_DANG_PHUCVU.Value : 0;
                    cls.EstimateTime = item.ESTIMATETIMECUT != null ? item.ESTIMATETIMECUT.Value : 0;
                    cls.TimeEstimate = cls.EstimateTime + (cls.Avg_TimeCut * cls.Khach_Cho) + cls.Chuagoi * 12 + cls.Danggoi_tg;
                    cls.IsSulphite = item.IsSulphite != null && item.IsSulphite == 1 ? true : false;
                    listCls.Add(cls);
                }
                listCls = listCls.OrderBy(o => o.Khach_Cho).ThenBy(o => o.Khach_Dang_Cat).ThenBy(o => o.TimeEstimate).ToList();
                return new JavaScriptSerializer().Serialize(listCls);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }
        /// <summary>
        /// Estimate time massage
        /// </summary>
        [WebMethod]
        public static object EstimateTimeMassage(int c)
        {
            try
            {
                var db = new Solution_30shineEntities();

                var data = db.Store_Estimate_TimeMassage(c).ToList();
                List<cls_EstimateTimeMassage> listCls = new List<cls_EstimateTimeMassage>();
                foreach (var item in data)
                {
                    cls_EstimateTimeMassage cls = new cls_EstimateTimeMassage();
                    cls.danggoiTg = (item.DANGGOI_TIME_CONLAI != null ? item.DANGGOI_TIME_CONLAI.Value : 0) + (item.CHUAGOI != null ? item.CHUAGOI.Value : 0) * 12;
                    cls.chuagoiSl = item.CHUAGOI != null ? item.CHUAGOI.Value : 0;
                    cls.danggoiSl = item.DANGGOI_SL != null ? item.DANGGOI_SL.Value : 0;
                    cls.skinnerName = item.SkinnerName;
                    cls.hoachat = item.HOACHAT != null ? item.HOACHAT.Value : 0;
                    listCls.Add(cls);
                }
                listCls = listCls.OrderBy(o => o.chuagoiSl).ThenBy(o => o.danggoiTg).ThenBy(o => o.danggoiSl).ToList();
                return new JavaScriptSerializer().Serialize(listCls);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }
        /// <summary>
        /// Lấy dữ liệu từ SM_BookingTemp theo BookingID
        /// </summary>
        /// <param name="BookingId"></param>
        /// <returns></returns>
        [WebMethod]
        public static object GetNote(int BookingId)
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
            var note = db.SM_BookingTemp.FirstOrDefault(b => b.Id == BookingId);
            return new JavaScriptSerializer().Serialize(note);
        }
        /// <summary>
        /// Lưu dữ liệu vào Booking và SM_BookingTemp
        /// </summary>
        /// <param name="BookingId"></param>
        /// <param name="SalonNote"></param>
        /// <returns></returns>
        [WebMethod]
        public static object InsertNote(int BookingId, string SalonNote)
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
            var smBooking = db.SM_BookingTemp.FirstOrDefault(b => b.Id == BookingId);
            var booking = db.Bookings.FirstOrDefault(bk => bk.BookingTempId == BookingId);
            var tracking =
            (from tk in db.TrackingWeb_DataV2
             join b in db.Bookings on tk.Booking_ID equals b.Id
             join sm in db.SM_BookingTemp on b.BookingTempId equals sm.Id
             where sm.Id == BookingId
             select tk).FirstOrDefault();
            if (tracking != null)
            {
                tracking.SalonNote = SalonNote;
                tracking.TimeSalonNote = DateTime.Now;
                db.TrackingWeb_DataV2.AddOrUpdate(tracking);
            }

            smBooking.SalonNote = SalonNote;
            smBooking.TimeSalonNote = DateTime.Now;
            booking.SalonNote = SalonNote;
            booking.TimeSalonNote = DateTime.Now;

            db.SM_BookingTemp.AddOrUpdate(smBooking);
            db.Bookings.AddOrUpdate(booking);
            var r = db.SaveChanges();
            if (r > 0)
            {
                return "<span style='color:green'>Thêm ghi chú thành công</span>";
            }
            else
            {
                return "<span style='color:red'>Có lỗi xảy ra. Vui lòng liên hệ bộ phận phát triển</span>";
            }

        }
        /// <summary>
        /// set isbookstylist
        /// </summary>
        /// <param name="bookingID"></param>
        /// <param name="check"></param>
        /// <returns></returns>
        [WebMethod]
        public static object SetIsBookStylist(int bookingID, int check)
        {
            try
            {
                Solution_30shineEntities db = new Solution_30shineEntities();
                var smbooking = db.SM_BookingTemp.FirstOrDefault(w => w.Id == bookingID);
                if (check == 1)
                {
                    smbooking.IsAutoStylist = false;
                    smbooking.IsBookStylist = true;
                }
                else
                {
                    smbooking.IsAutoStylist = true;
                    smbooking.IsBookStylist = false;
                }
                db.SM_BookingTemp.AddOrUpdate(smbooking);
                db.SaveChanges();

                var booking = db.Bookings.FirstOrDefault(w => w.BookingTempId == bookingID);
                if (check == 1)
                {
                    booking.IsAutoStylist = false;
                    booking.IsBookStylist = true;
                }
                else
                {
                    booking.IsAutoStylist = true;
                    booking.IsBookStylist = false;
                }
                db.Bookings.AddOrUpdate(booking);
                db.SaveChanges();

                return new { IsAutoStylist = booking.IsAutoStylist, BookingTempId = booking.BookingTempId };
            }
            catch
            {
                return new { IsAutoStylist = false, BookingTempId = 0 };
            }
        }

        [WebMethod]
        public static object CountSpecialRequire(int salonId)
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
            var count = db.Tracking_Cus_Special_Requirements_Store(DateTime.Now.Date, salonId).Where(s => s.SalonNote == null || s.SalonNote == "").Count();
            return new JavaScriptSerializer().Serialize(count);
        }



        /// <summary>
        /// Class dữ liệu tính toán thời gian có thể phục
        /// vụ tiếp của stylist
        /// </summary>   
        public class cls_EstimateTimeData
        {
            public int StylistId { get; set; }
            public string StylistName { get; set; }
            public string CustomerName { get; set; }
            public int EstimateTime { get; set; }
            public int Chuagoi { get; set; }
            public int Dagoi { get; set; }
            public int Danggoi_sl { get; set; }
            public int Danggoi_tg { get; set; }
            public int Avg_TimeCut { get; set; }
            public bool IsSulphite { get; set; }
            public int Khach_Cho { get; set; }
            public int Khach_Dang_Cat { get; set; }
            public int TimeEstimate { get; set; }
        }

        public class cls_EstimateTimeMassage
        {
            public int skinnerId { get; set; }
            public string skinnerName { get; set; }
            public int danggoiSl { get; set; }
            public int danggoiTg { get; set; }
            public int chuagoiSl { get; set; }
            public int hoachat { get; set; }
        }
        /// <summary>
        /// Class dữ liệu stylist, bao gồm toàn bộ khung giờ
        /// </summary>

        public class cls_timeline_stylist
        {
            public int StylistId { get; set; }
            public string StylistName { get; set; }
            public int GroupLevelId { get; set; }
            public int TeamId { get; set; }
            public List<cls_timeline_stylist_hour> ListHour { get; set; }
            public bool IsAutoStylist { get; set; }
            public int BillTotal { get; set; }
            public bool IsEnroll { get; set; }
        }

        /// <summary>
        /// Class cấu trúc dữ liệu khung giờ
        /// </summary>
        public class cls_timeline_stylist_hour
        {
            public int HourId { get; set; }
            public string HourFrame { get; set; }
            public bool IsServiced { get; set; }
            public List<cls_timeline_stylist_hour_data> HourData { get; set; }
        }

        /// <summary>
        /// Cấu trúc dữ liệu booking|bill trong 1 khung giờ/1 stylist
        /// </summary>
        public class cls_timeline_stylist_hour_data
        {
            public string CustomerPhone { get; set; }
            public string CustomerName { get; set; }
            public int? BookingID { get; set; }
            public bool? IsCall { get; set; }
            public DateTime? IsCallTime { get; set; }
            public DateTime? IsCallTimeModified { get; set; }
            public int? BillID { get; set; }
            public DateTime? BillCreatedDate { get; set; }
            public DateTime? InProcedureTime { get; set; }
            public DateTime? InProcedureTimeModifed { get; set; }
            public DateTime? CompleteBillTime { get; set; }
            public int? Mark { get; set; }
            public bool IsAutoStylist { get; set; }
            public bool IsBookAtSalon { get; set; }
            public bool IsHCItem { get; set; }
            public int? CustomerType { get; set; }
            public int? Order { get; set; }
            public string SalonNote { get; set; }
            public string TextNote1 { get; set; }
            public string TextNote2 { get; set; }
        }

        /// <summary>
        /// Cấu trúc dữ liệu dịch vụ
        /// </summary>
        public class cls_timeline_service
        {
            public int Id { get; set; }
            public string Code { get; set; }
            public string Name { get; set; }
            public int Price { get; set; }
            public int Quantity { get; set; }
            public double VoucherPercent { get; set; }
        }

        /// <summary>
        /// Cấu trúc dữ liệu bill post lên từ client
        /// </summary>
        public class cls_timeline_bill
        {
            public int billID { get; set; }
            public int bookingID { get; set; }
            public int stylistID { get; set; }
            public string stylistName { get; set; }
            public int salonID { get; set; }
            public string salonName { get; set; }
            public int teamID { get; set; }
            public int checkinID { get; set; }
            public string checkinName { get; set; }
            public string customerName { get; set; }
            public string customerPhone { get; set; }
            public int customerId { get; set; }
            public List<cls_timeline_service> services { get; set; }
            public int hourID { get; set; }
            public string hourFrame { get; set; }
            public string PDFBillCode { get; set; }
            public string HCItem { get; set; }
            public bool IsAutoStylist { get; set; }
            public bool IsBookAtSalon { get; set; }
        }

        public class cls_timeline_sync_bill : cls_timeline_bill
        {

        }
    }
}