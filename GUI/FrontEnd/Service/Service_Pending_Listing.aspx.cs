﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic; 
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Data.Entity.Migrations;

namespace _30shine.GUI.UIService
{
    public partial class Service_Pending_Listing : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected int SalonId;
        protected List<TeamService> lstTeamService = new List<TeamService>();

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckPermission();
            /// khởi tạo store lương cho nhân viên hôm nay
            SalaryLib.initSalaryToday(DateTime.Today);
            // Bind team work
            Bind_TeamWork();

            SalonId = Convert.ToInt32(Session["SalonId"]);
            if (!IsPostBack)
            {
                //Bind_Salon();
                if (Session["SalonId"] != null)
                {
                    HDF_SalonId.Value = Session["SalonId"].ToString();
                }
                else
                {
                    HDF_SalonId.Value = "0";
                }
                if (!Perm_ViewAllData)
                {
                    Bind_RptBillService();
                }
                else
                {
                    Bind_RptBillService_KT();
                    Bind_RptBillService_TDN();
                    Bind_RptBillService_DC();
                    Bind_RptBillService_DL();
                }
            }
        }

        private void Bind_TeamWork()
        {
            using (var db = new Solution_30shineEntities())
            {
                lstTeamService = db.TeamServices.Where(w => w.IsDelete != 1 && w.Publish == true && w.Status == 1).ToList();
            }
        }

        private void Bind_RptBillService()
        {
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var _BillService = new List<BillService>();
                var Where = PredicateBuilder.True<BillService>();
                string SgtValue;
                string sql = "";

                switch (HDF_Suggestion_Field.Value)
                {
                    case "customer.code": SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "customer.name": SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "customer.phone": SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "bill.code": break;
                }

                Where = Where.And(p => p.IsDelete != 1 && p.Pending == 1);
                //int SalonId = Convert.ToInt32(Salon.SelectedValue);
                int integer;
                int SalonId = Session["SalonId"] != null ? (int.TryParse(Session["SalonId"].ToString(), out integer) ? integer : 0) : 0;
                if (!Perm_ViewAllData)
                {
                    Where = Where.And(p => p.SalonId == SalonId);
                }
                else
                {
                    if (SalonId > 0)
                    {
                        Where = Where.And(p => p.SalonId == SalonId);
                    }
                    else
                    {
                        Where = Where.And(p => p.SalonId >= 0);
                    }
                }
                // Điều kiện chỉ lấy pending trong ngày
                Where = Where.And(w => w.CreatedDate >= DateTime.Today);

                sql = @"select a.*, s.Fullname as HairdresserName, s2.Fullname as HairMassageName, s3.Fullname as CosmeticName, 
	                        c.Fullname as CustomerName, c.Phone as CustomerPhone, t.Color as TeamColor 
                        from BillService as a
                        left join Staff as s
                        on a.Staff_Hairdresser_Id = s.Id and s.IsDelete != 1
                        left join Staff as s2
                        on a.Staff_HairMassage_Id = s2.Id and s2.IsDelete != 1
                        left join Staff as s3
                        on a.SellerId = s3.Id and s3.IsDelete != 1
                        left join Customer as c
                        on a.CustomerCode = c.Customer_Code and c.IsDelete != 1
                        left join TeamService as t
                        on a.TeamId = t.Id and t.IsDelete != 1
                        where (a.IsDelete !=1 or a.IsDelete is null) and a.Pending = 1
                        and a.CreatedDate >= '" + String.Format("{0:yyyy/MM/dd}", DateTime.Today) + "'";
                if (SalonId > 0)
                {
                    sql += " and a.SalonId = " + SalonId;
                }
                var lst = db.Database.SqlQuery<BillService2>(sql).ToList();
                if (lst.Count > 0)
                {
                    for (var i = 0; i < lst.Count; i++)
                    {
                        lst[i].billOrder = UIHelpers.getBillOrder(lst[i].BillCode, 4);
                        lst[i].completeTime = lst[i].CreatedDate.Value.Add(TimeSpan.Parse("00:40:00"));
                    }
                }

                if (lst.Count > 0)
                {
                    var Count = 0;
                    foreach (var v in lst)
                    {
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        if (v.ProductIds != null) {
                            var ProductListThis = serializer.Deserialize<List<ProductBasic>>(v.ProductIds);
                            var _ProductName = "";
                            if (ProductListThis != null)
                            {
                                foreach (var v2 in ProductListThis)
                                {
                                    _ProductName += "<a href=\"/admin/san-pham/" + v2.Code + ".html\" target=\"_blank\">" + v2.Name + "</a>, ";
                                }
                                _ProductName = _ProductName.TrimEnd(',', ' ');
                                lst[Count].ProductNames = _ProductName;
                            }
                        }

                        if (v.ServiceIds != null) {
                            var ServiceListThis = serializer.Deserialize<List<ProductBasic>>(v.ServiceIds);
                            var _ServiceName = "";
                            if (ServiceListThis != null)
                            {
                                foreach (var v2 in ServiceListThis)
                                {
                                    _ServiceName += "<a href=\"/admin/dich-vu/" + v2.Code + ".html\" target=\"_blank\">" + v2.Name + "</a>, ";
                                }
                                _ServiceName = _ServiceName.TrimEnd(',', ' ');
                                lst[Count].ServiceNames = _ServiceName;
                            }
                        }

                        Count++;
                    }
                }

                RptBillService.DataSource = lst.OrderBy(o => o.billOrder);
                RptBillService.DataBind();
            }
        }

        private void Bind_RptBillService_KT()
        {
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var _BillService = new List<BillService>();
                var Where = PredicateBuilder.True<BillService>();
                string SgtValue;
                string sql = "";

                switch (HDF_Suggestion_Field.Value)
                {
                    case "customer.code": SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "customer.name": SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "customer.phone": SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "bill.code": break;
                }

                Where = Where.And(p => p.IsDelete != 1 && p.Pending == 1 && p.SalonId == 2);
                //int SalonId = Convert.ToInt32(Salon.SelectedValue);
                int integer;
                int SalonId = Session["SalonId"] != null ? (int.TryParse(Session["SalonId"].ToString(), out integer) ? integer : 0) : 0;
                if (!Perm_ViewAllData)
                {
                    Where = Where.And(p => p.SalonId == SalonId);
                }
                else
                {
                    if (SalonId > 0)
                    {
                        Where = Where.And(p => p.SalonId == SalonId);
                    }
                    else
                    {
                        Where = Where.And(p => p.SalonId >= 0);
                    }
                }
                // Điều kiện chỉ lấy pending trong ngày
                Where = Where.And(w => w.CreatedDate >= DateTime.Today);

                sql = @"select a.*, s.Fullname as HairdresserName, s2.Fullname as HairMassageName, s3.Fullname as CosmeticName, 
	                        c.Fullname as CustomerName, c.Phone as CustomerPhone, t.Color as TeamColor 
                        from BillService as a
                        left join Staff as s
                        on a.Staff_Hairdresser_Id = s.Id and s.IsDelete != 1
                        left join Staff as s2
                        on a.Staff_HairMassage_Id = s2.Id and s2.IsDelete != 1
                        left join Staff as s3
                        on a.SellerId = s3.Id and s3.IsDelete != 1
                        left join Customer as c
                        on a.CustomerCode = c.Customer_Code and c.IsDelete != 1
                        left join TeamService as t
                        on a.TeamId = t.Id and t.IsDelete != 1
                        where a.IsDelete != 1 and a.Pending = 1
                        and a.CreatedDate >= '" + String.Format("{0:yyyy/MM/dd}", DateTime.Today) + "'";
                sql += " and a.SalonId = " + 2;
                var lst = db.Database.SqlQuery<BillService2>(sql).ToList();
                if (lst.Count > 0)
                {
                    for (var i = 0; i < lst.Count; i++)
                    {
                        lst[i].billOrder = UIHelpers.getBillOrder(lst[i].BillCode, 4);
                        lst[i].completeTime = lst[i].CreatedDate.Value.Add(TimeSpan.Parse("00:40:00"));
                    }
                }

                if (lst.Count > 0)
                {
                    var Count = 0;
                    foreach (var v in lst)
                    {
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        if (v.ProductIds != null)
                        {
                            var ProductListThis = serializer.Deserialize<List<ProductBasic>>(v.ProductIds);
                            var _ProductName = "";
                            if (ProductListThis != null)
                            {
                                foreach (var v2 in ProductListThis)
                                {
                                    _ProductName += "<a href=\"/admin/san-pham/" + v2.Code + ".html\" target=\"_blank\">" + v2.Name + "</a>, ";
                                }
                                _ProductName = _ProductName.TrimEnd(',', ' ');
                                lst[Count].ProductNames = _ProductName;
                            }
                        }

                        if (v.ServiceIds != null)
                        {
                            var ServiceListThis = serializer.Deserialize<List<ProductBasic>>(v.ServiceIds);
                            var _ServiceName = "";
                            if (ServiceListThis != null)
                            {
                                foreach (var v2 in ServiceListThis)
                                {
                                    _ServiceName += "<a href=\"/admin/dich-vu/" + v2.Code + ".html\" target=\"_blank\">" + v2.Name + "</a>, ";
                                }
                                _ServiceName = _ServiceName.TrimEnd(',', ' ');
                                lst[Count].ServiceNames = _ServiceName;
                            }
                        }

                        Count++;
                    }
                }

                RptBillService_KT.DataSource = lst.OrderBy(o => o.billOrder); ;
                RptBillService_KT.DataBind();
            }
        }

        private void Bind_RptBillService_TDN()
        {
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var _BillService = new List<BillService>();
                var Where = PredicateBuilder.True<BillService>();
                string SgtValue;
                string sql = "";

                switch (HDF_Suggestion_Field.Value)
                {
                    case "customer.code": SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "customer.name": SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "customer.phone": SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "bill.code": break;
                }

                Where = Where.And(p => p.IsDelete != 1 && p.Pending == 1 && p.SalonId == 3);
                //int SalonId = Convert.ToInt32(Salon.SelectedValue);
                int integer;
                int SalonId = Session["SalonId"] != null ? (int.TryParse(Session["SalonId"].ToString(), out integer) ? integer : 0) : 0;
                if (!Perm_ViewAllData)
                {
                    Where = Where.And(p => p.SalonId == SalonId);
                }
                else
                {
                    if (SalonId > 0)
                    {
                        Where = Where.And(p => p.SalonId == SalonId);
                    }
                    else
                    {
                        Where = Where.And(p => p.SalonId >= 0);
                    }
                }
                // Điều kiện chỉ lấy pending trong ngày
                Where = Where.And(w => w.CreatedDate >= DateTime.Today);

                sql = @"select a.*, s.Fullname as HairdresserName, s2.Fullname as HairMassageName, s3.Fullname as CosmeticName, 
	                        c.Fullname as CustomerName, c.Phone as CustomerPhone, t.Color as TeamColor 
                        from BillService as a
                        left join Staff as s
                        on a.Staff_Hairdresser_Id = s.Id and s.IsDelete != 1
                        left join Staff as s2
                        on a.Staff_HairMassage_Id = s2.Id and s2.IsDelete != 1
                        left join Staff as s3
                        on a.SellerId = s3.Id and s3.IsDelete != 1
                        left join Customer as c
                        on a.CustomerCode = c.Customer_Code and c.IsDelete != 1
                        left join TeamService as t
                        on a.TeamId = t.Id and t.IsDelete != 1
                        where a.IsDelete != 1 and a.Pending = 1
                        and a.CreatedDate >= '" + String.Format("{0:yyyy/MM/dd}", DateTime.Today) + "'";
                sql += " and a.SalonId = " + 3;
                var lst = db.Database.SqlQuery<BillService2>(sql).ToList();
                if (lst.Count > 0)
                {
                    for (var i = 0; i < lst.Count; i++)
                    {
                        lst[i].billOrder = UIHelpers.getBillOrder(lst[i].BillCode, 4);
                        lst[i].completeTime = lst[i].CreatedDate.Value.Add(TimeSpan.Parse("00:40:00"));
                    }
                }

                if (lst.Count > 0)
                {
                    var Count = 0;
                    foreach (var v in lst)
                    {
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        if (v.ProductIds != null)
                        {
                            var ProductListThis = serializer.Deserialize<List<ProductBasic>>(v.ProductIds);
                            var _ProductName = "";
                            if (ProductListThis != null)
                            {
                                foreach (var v2 in ProductListThis)
                                {
                                    _ProductName += "<a href=\"/admin/san-pham/" + v2.Code + ".html\" target=\"_blank\">" + v2.Name + "</a>, ";
                                }
                                _ProductName = _ProductName.TrimEnd(',', ' ');
                                lst[Count].ProductNames = _ProductName;
                            }
                        }

                        if (v.ServiceIds != null)
                        {
                            var ServiceListThis = serializer.Deserialize<List<ProductBasic>>(v.ServiceIds);
                            var _ServiceName = "";
                            if (ServiceListThis != null)
                            {
                                foreach (var v2 in ServiceListThis)
                                {
                                    _ServiceName += "<a href=\"/admin/dich-vu/" + v2.Code + ".html\" target=\"_blank\">" + v2.Name + "</a>, ";
                                }
                                _ServiceName = _ServiceName.TrimEnd(',', ' ');
                                lst[Count].ServiceNames = _ServiceName;
                            }
                        }

                        Count++;
                    }
                }

                RptBillService_TDN.DataSource = lst.OrderBy(o => o.billOrder); ;
                RptBillService_TDN.DataBind();
            }
        }

        private void Bind_RptBillService_DC()
        {
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var _BillService = new List<BillService>();
                var Where = PredicateBuilder.True<BillService>();
                string SgtValue;
                string sql = "";

                switch (HDF_Suggestion_Field.Value)
                {
                    case "customer.code":
                        SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "customer.name":
                        SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "customer.phone":
                        SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "bill.code": break;
                }

                Where = Where.And(p => p.IsDelete != 1 && p.Pending == 1 && p.SalonId == 4);
                //int SalonId = Convert.ToInt32(Salon.SelectedValue);
                int integer;
                int SalonId = Session["SalonId"] != null ? (int.TryParse(Session["SalonId"].ToString(), out integer) ? integer : 0) : 0;
                if (!Perm_ViewAllData)
                {
                    Where = Where.And(p => p.SalonId == SalonId);
                }
                else
                {
                    if (SalonId > 0)
                    {
                        Where = Where.And(p => p.SalonId == SalonId);
                    }
                    else
                    {
                        Where = Where.And(p => p.SalonId >= 0);
                    }
                }
                // Điều kiện chỉ lấy pending trong ngày
                Where = Where.And(w => w.CreatedDate >= DateTime.Today);

                sql = @"select a.*, s.Fullname as HairdresserName, s2.Fullname as HairMassageName, s3.Fullname as CosmeticName, 
	                        c.Fullname as CustomerName, c.Phone as CustomerPhone, t.Color as TeamColor 
                        from BillService as a
                        left join Staff as s
                        on a.Staff_Hairdresser_Id = s.Id and s.IsDelete != 1
                        left join Staff as s2
                        on a.Staff_HairMassage_Id = s2.Id and s2.IsDelete != 1
                        left join Staff as s3
                        on a.SellerId = s3.Id and s3.IsDelete != 1
                        left join Customer as c
                        on a.CustomerCode = c.Customer_Code and c.IsDelete != 1
                        left join TeamService as t
                        on a.TeamId = t.Id and t.IsDelete != 1
                        where a.IsDelete != 1 and a.Pending = 1
                        and a.CreatedDate >= '" + String.Format("{0:yyyy/MM/dd}", DateTime.Today) + "'";
                sql += " and a.SalonId = " + 4;
                var lst = db.Database.SqlQuery<BillService2>(sql).ToList();
                if (lst.Count > 0)
                {
                    for (var i = 0; i < lst.Count; i++)
                    {
                        lst[i].billOrder = UIHelpers.getBillOrder(lst[i].BillCode, 4);
                        lst[i].completeTime = lst[i].CreatedDate.Value.Add(TimeSpan.Parse("00:40:00"));
                    }
                }

                if (lst.Count > 0)
                {
                    var Count = 0;
                    foreach (var v in lst)
                    {
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        if (v.ProductIds != null)
                        {
                            var ProductListThis = serializer.Deserialize<List<ProductBasic>>(v.ProductIds);
                            var _ProductName = "";
                            if (ProductListThis != null)
                            {
                                foreach (var v2 in ProductListThis)
                                {
                                    _ProductName += "<a href=\"/admin/san-pham/" + v2.Code + ".html\" target=\"_blank\">" + v2.Name + "</a>, ";
                                }
                                _ProductName = _ProductName.TrimEnd(',', ' ');
                                lst[Count].ProductNames = _ProductName;
                            }
                        }

                        if (v.ServiceIds != null)
                        {
                            var ServiceListThis = serializer.Deserialize<List<ProductBasic>>(v.ServiceIds);
                            var _ServiceName = "";
                            if (ServiceListThis != null)
                            {
                                foreach (var v2 in ServiceListThis)
                                {
                                    _ServiceName += "<a href=\"/admin/dich-vu/" + v2.Code + ".html\" target=\"_blank\">" + v2.Name + "</a>, ";
                                }
                                _ServiceName = _ServiceName.TrimEnd(',', ' ');
                                lst[Count].ServiceNames = _ServiceName;
                            }
                        }

                        Count++;
                    }
                }

                RptBillService_DC.DataSource = lst.OrderBy(o => o.billOrder); ;
                RptBillService_DC.DataBind();
            }
        }

        private void Bind_RptBillService_DL()
        {
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var _BillService = new List<BillService>();
                var Where = PredicateBuilder.True<BillService>();
                string SgtValue;
                string sql = "";

                switch (HDF_Suggestion_Field.Value)
                {
                    case "customer.code":
                        SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "customer.name":
                        SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "customer.phone":
                        SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "bill.code": break;
                }

                Where = Where.And(p => p.IsDelete != 1 && p.Pending == 1 && p.SalonId == 4);
                //int SalonId = Convert.ToInt32(Salon.SelectedValue);
                int integer;
                int SalonId = Session["SalonId"] != null ? (int.TryParse(Session["SalonId"].ToString(), out integer) ? integer : 0) : 0;
                if (!Perm_ViewAllData)
                {
                    Where = Where.And(p => p.SalonId == SalonId);
                }
                else
                {
                    if (SalonId > 0)
                    {
                        Where = Where.And(p => p.SalonId == SalonId);
                    }
                    else
                    {
                        Where = Where.And(p => p.SalonId >= 0);
                    }
                }
                // Điều kiện chỉ lấy pending trong ngày
                Where = Where.And(w => w.CreatedDate >= DateTime.Today);

                sql = @"select a.*, s.Fullname as HairdresserName, s2.Fullname as HairMassageName, s3.Fullname as CosmeticName, 
	                        c.Fullname as CustomerName, c.Phone as CustomerPhone, t.Color as TeamColor 
                        from BillService as a
                        left join Staff as s
                        on a.Staff_Hairdresser_Id = s.Id and s.IsDelete != 1
                        left join Staff as s2
                        on a.Staff_HairMassage_Id = s2.Id and s2.IsDelete != 1
                        left join Staff as s3
                        on a.SellerId = s3.Id and s3.IsDelete != 1
                        left join Customer as c
                        on a.CustomerCode = c.Customer_Code and c.IsDelete != 1
                        left join TeamService as t
                        on a.TeamId = t.Id and t.IsDelete != 1
                        where a.IsDelete != 1 and a.Pending = 1
                        and a.CreatedDate >= '" + String.Format("{0:yyyy/MM/dd}", DateTime.Today) + "'";
                sql += " and a.SalonId = " + 5;
                var lst = db.Database.SqlQuery<BillService2>(sql).ToList();
                if (lst.Count > 0)
                {
                    for (var i = 0; i < lst.Count; i++)
                    {
                        lst[i].billOrder = UIHelpers.getBillOrder(lst[i].BillCode, 4);
                        lst[i].completeTime = lst[i].CreatedDate.Value.Add(TimeSpan.Parse("00:40:00"));
                    }
                }

                if (lst.Count > 0)
                {
                    var Count = 0;
                    foreach (var v in lst)
                    {
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        if (v.ProductIds != null)
                        {
                            var ProductListThis = serializer.Deserialize<List<ProductBasic>>(v.ProductIds);
                            var _ProductName = "";
                            if (ProductListThis != null)
                            {
                                foreach (var v2 in ProductListThis)
                                {
                                    _ProductName += "<a href=\"/admin/san-pham/" + v2.Code + ".html\" target=\"_blank\">" + v2.Name + "</a>, ";
                                }
                                _ProductName = _ProductName.TrimEnd(',', ' ');
                                lst[Count].ProductNames = _ProductName;
                            }
                        }

                        if (v.ServiceIds != null)
                        {
                            var ServiceListThis = serializer.Deserialize<List<ProductBasic>>(v.ServiceIds);
                            var _ServiceName = "";
                            if (ServiceListThis != null)
                            {
                                foreach (var v2 in ServiceListThis)
                                {
                                    _ServiceName += "<a href=\"/admin/dich-vu/" + v2.Code + ".html\" target=\"_blank\">" + v2.Name + "</a>, ";
                                }
                                _ServiceName = _ServiceName.TrimEnd(',', ' ');
                                lst[Count].ServiceNames = _ServiceName;
                            }
                        }

                        Count++;
                    }
                }

                RptBillService_DL.DataSource = lst.OrderBy(o => o.billOrder); ;
                RptBillService_DL.DataBind();
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
            Bind_RptBillService();
        }

        protected void Bind_Paging()
        {
            // init Paging value
            PAGING._Segment = 50;
            //PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? (HDF_Page.Value != "" ? Convert.ToInt32(HDF_Page.Value) : 1) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                int Count;
                var Where = PredicateBuilder.True<BillService>();
                string SgtValue;

                switch (HDF_Suggestion_Field.Value)
                {
                    case "customer.code": SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "customer.name": SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "customer.phone": SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "bill.code": break;
                }

                Where = Where.And(p => p.IsDelete != 1 && p.Pending == 1);
                int integer;
                int SalonId = Session["SalonId"] != null ? (int.TryParse(Session["SalonId"].ToString(), out integer) ? integer : 0) : 0;
                if (!Perm_ViewAllData)
                {
                    Where = Where.And(p => p.SalonId == SalonId);
                }
                else
                {
                    if (SalonId > 0)
                    {
                        Where = Where.And(p => p.SalonId == SalonId);
                    }
                    else
                    {
                        Where = Where.And(p => p.SalonId >= 0);
                    }
                }

                Count = db.BillServices.AsExpandable().Count(Where);

                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["User_Id"] == null)
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "salonmanager", "reception", "checkin", "checkout" };
                string[] AllowDelete = new string[] { "root", "admin", "salonmanager", "reception", "checkin", "checkout" };
                string[] AllowViewAllData = new string[] { "root", "admin"};
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                    
                }
                if (Array.IndexOf(AllowDelete, Permission) != -1)
                {
                    Perm_Delete = true;
                }
                Perm_Edit = true;

                if (Array.IndexOf(AllowViewAllData, Permission) != -1)
                {
                    Perm_ViewAllData = true;
                }
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }
    }

    public class BillService2 : BillService
    {
        public int billOrder { get; set; }
        public DateTime completeTime { get; set; }
        public string TeamColor { get; set; }
    }
}