﻿using _30shine.Helpers;
using _30shine.Helpers.Http;
//test time
using _30shine.MODEL.BO;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.CustomClass;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using _30shine.MODEL.IO;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using static _30shine.MODEL.CustomClass.AccountModelClass;
using Libraries;
using Newtonsoft.Json;
using System.Net;

namespace _30shine.GUI.FrontEnd.Service.ConfigCheck
{
    public partial class Service_Pending_Complete_V2 : System.Web.UI.Page
    {
        //public string PATH_ASYNC_CHECKOUT_BILL_LOG = @"30ShineSystemLog\erp.30shine.com\" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + "_async_checkout_bill_statistic.txt";
        //public string PATH_ASYNC_CHECKOUT_BILL_LOG_EXCEPTION = @"30ShineSystemLog\erp.30shine.com\" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + "_async_checkout_bill_statistic_exception.txt";
        //public string PATH_TEST_LOG = @"30ShineSystemLog\erp.30shine.com\" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + "_test_log.txt";
        //public string PATH_BILL_MARK_NULL_LOG = @"30ShineSystemLog\erp.30shine.com\" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + "_bill_mark_null_log.txt";

        //private string PageID = "";

        //sdfjgjdkgjkldfg
        private bool Perm_ShowSalon = false;
        private bool Perm_Access = false;
        private bool Perm_ViewAllData = false;
        protected bool Perm_EditRating = false;


        private List<ProductBasic> ProductList = new List<ProductBasic>();
        protected List<Product> _ListProduct = new List<Product>();
        protected string _ListProductJsonString;

        private List<ProductBasic> ServiceList = new List<ProductBasic>();
        private string _CustomerCode = "";
        public string _CustomerName = "";
        private string Stylist = "";
        private string Skinner = "";
        private string Seller = "";
        protected int SalonId = 0;
        private List<int> lstFreeService = new List<int>();

        private BillService OBJ;
        protected bool HasService = false;
        protected bool HasProduct = false;
        protected bool HasImages = false;
        protected List<string> ListImagesUrl = new List<string>();
        protected string[] ListImagesName;
        protected bool CusNoInfor = false;
        protected int billID = 0;
        protected int customerID = 0;
        private static Service_Pending_Complete_V2 instance;
        protected string listOfStaffsSerialized;
        protected List<Service_Rating> ServiceRating;
        public Solution_30shineEntities db = Library.Model.getProjectModel();
        protected AccountModel accountModel;
        protected BillService bill = new BillService();
        protected string billJson = "{}";
        protected CultureInfo culture = new CultureInfo("vi-VN");
        public string link = "";
        IStaffModel staffModel;
        IBillModel billModel = new BillModel();
        AccountSession accountSession = new AccountSession();
        string permission = "";
        public FlowProduct objProduct = new FlowProduct();
        public StatisticSalaryProduct staticProduct = new StatisticSalaryProduct();
        public StatisticSalaryService staticService = new StatisticSalaryService();
        public int? SalonIdBill = 0;
        public string domainERPLiveSite = "";
        //public string domainAPILivesite = "/*http://api.30shine.com*/";
        //public string domainAPILivesite = "http://api.30shine.net";

        private int[] dayOverTime = new int[] { 5, 6, 0 }; // Thu 6, 7, cn
        protected bool IsBillOverTime;

        public DateTime today;
        public DateTime tomorrow;
        public int TotalStatic = 0;
        public double TotalMoneyStatic = 0;

        public Service_Pending_Complete_V2()
        {
        }

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //if (Request.QueryString["Code"] != null)
                //{
                //    PageID = "DV_PENDING_EDIT";
                //}
                //else
                //{
                //    PageID = "DV_PENDING_DETAIL";
                //}
                //IPermissionModel permissionModel = new PermissionModel();
                //var staffId = Convert.ToInt32(Session["User_Id"].ToString());
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, accountModel.AccountInfor.UserPermission);

                //Perm_ShowSalon = permissionModel.GetActionByActionNameAndPageId("Perm_ShowSalon", PageID, accountModel.AccountInfor.UserPermission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, accountModel.AccountInfor.UserPermission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
            else if (Perm_ShowSalon)
            {
                TrSalon.Visible = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            staffModel = new StaffModel();
            accountModel = AccountModel.Instance;
            SetPermission();
            billID = this.getBillID();
            domainERPLiveSite = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host;
            today = DateTime.Now.Date;
            tomorrow = today.AddDays(1);

            //Kiểm tra  cho Rating....
            if (!IsPostBack)
            {
                if (Array.IndexOf(new string[] { "root", "ADMIN" }, accountModel.AccountInfor.UserPermission) != -1)
                {
                    Perm_EditRating = true;
                    BindOBJPageLoad();
                }
                else
                {
                    if (EnableCheckOut(billID))
                    {
                        BindOBJPageLoad();
                        //Thông báo cho app "Đang mở check out"
                        //thêm bản ghi vào bảng RatingCheck....
                        //int? mark = null;
                        //try
                        //{
                        //    mark = GetBillId(this.billID).Mark;
                        //}
                        //catch { }
                        //if (!IsCompleteBill(this.billID) && (mark <= 0 || mark == null))

                        if (!IsCompleteBill(billID))
                        {
                            PushNotificationApprating(billID);
                            AddRatingIsCheck(billID);
                        }
                    }
                    else
                    {
                        ContentWrap.Visible = false;
                        ShowRating.Visible = true;
                        var textMsg = "";
                        var CustomerName = GetCNameRating(this.billID);
                        if (CustomerName == null)
                        {
                            textMsg = "Đã xảy ra lỗi, vui lòng thử lại!!!";
                        }
                        else
                        {
                            textMsg = "Chưa hoàn thành Hóa đơn trước, của khách: " + CustomerName.CustomerName +
                                      ", Số điện thoại: " + GetCNameRating(this.billID).Phone;
                            link = domainERPLiveSite + "/dich-vu/pending/" + CustomerName.BillId + ".html";
                        }

                        ErrorRating.Text = textMsg;
                    }
                }
            }
        }

        private void BindOBJPageLoad()
        {
            if (Bind_Bill())
            {
                Bind_RptService_Bill();
                Bind_RptProduct_Bill();
                Bind_RptProduct();
                Bind_RptService();
                Bind_RptServiceFeatured();
                Bind_RptProductFeatured();
                Bind_RptFreeService();
                // Bind Salon
                Library.Function.bindSalonSpecial(new List<DropDownList>() { Salon }, Perm_ShowSalon);
                if (Perm_ShowSalon)
                {
                    var itemSelected = Salon.Items.FindByValue(OBJ.SalonId.ToString());
                    if (itemSelected != null)
                    {
                        itemSelected.Selected = true;
                    }
                }

                // Lấy danh sách Nhân viên đã được chấm công cho Salon
                List<Object> listOfStaffs = new List<object>();
                StaffModel.getInstance()
                    .GetListStaffEnrollBySalon(OBJ.SalonId.Value, Convert.ToDateTime(OBJ.CreatedDate)).ForEach(
                        delegate (Staff staff)
                        {
                            listOfStaffs.Add(new
                            {
                                Id = staff.Id,
                                OrderCode = staff.OrderCode,
                                TypeId = staff.Type,
                                Fullname = staff.Fullname,
                                Type = staff.StaffTypeName
                            });
                        });

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                this.listOfStaffsSerialized = serializer.Serialize(listOfStaffs);
                this.ServiceRating = ServiceRatingModel.Instance.AllRatingLabel;
            }
            else
            {
                MsgSystem.Text = "Không tồn tại hóa đơn!";
                MsgSystem.CssClass = "msg-system warning";
            }


            /// Reset rating value
            resetRatingTemp();
        }

        /// <summary>
        /// Check bill co ap dung tang ca hay hom nay khong
        /// Quy uoc Stylist/Skinner phai cham ghep doi tang ca chuan, nen chi can check Stylist
        /// </summary>
        /// <param name="stylistId"></param>
        /// <returns></returns>
        private bool IsOverTime(BillService bill)
        {
            try
            {
                return IsDayOverTime(bill.CreatedDate) && IsHourOverTime(bill) ? true : false;
            }
            catch (Exception)
            {
                return false;
            }
        }


        private bool IsDayOverTime(DateTime? date)
        {
            try
            {
                return Array.IndexOf(dayOverTime, (int)date.Value.DayOfWeek) != -1 ? true : false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool IsHourOverTime(BillService bill)
        {
            try
            {
                var workDate = bill.CreatedDate.Value.Date;
                var booking = db.Bookings.FirstOrDefault(w => w.Id == bill.BookingId);
                var bookHour = db.BookHours.FirstOrDefault(w => w.Id == booking.HourId);
                var timeKeeping = db.FlowTimeKeepings.FirstOrDefault(
                    w => w.IsDelete == 0 &&
                         w.IsEnroll == true &&
                         w.WorkDate == workDate &&
                         w.StaffId == bill.Staff_Hairdresser_Id);
                var shift = db.WorkTimes.FirstOrDefault(w => w.Id == timeKeeping.WorkTimeId);
                var bookingHour = workDate.Add(bookHour.HourFrame.Value);
                var shiftStart = workDate.Add(shift.StrartTime.Value).AddMinutes(-10);
                var shiftEnd = workDate.Add(shift.EnadTime.Value).AddMinutes(-10);
                return (bill.CreatedDate.Value < shiftStart || bill.CreatedDate.Value > shiftEnd);
                //&& (shiftStart < bookingHour || bookingHour > shiftEnd);
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool IsHourOverTime(int? stylistId, DateTime? date)
        {
            try
            {
                string sql = @"declare @staffId int,
		                            @workDate date;

                            set @staffId = " + stylistId + @";
                            set @workDate = '" + Library.Format.getDateTimeString(date.Value) + @"';

                            with a as
                            (
	                            SELECT StaffId,WorkDate, WorkTimeId,
		                            LTRIM(RTRIM(m.n.value('.[1]','varchar(1000)'))) AS HourId
	                            FROM
	                            (
		                            SELECT FlowTimeKeeping.StaffId,FlowTimeKeeping.WorkDate,FlowTimeKeeping.WorkTimeId,
			                            CAST('<XMLRoot><RowData>' + REPLACE(FlowTimeKeeping.HourIds,',','</RowData><RowData>') + '</RowData></XMLRoot>' AS XML) AS x
		                            FROM FlowTimeKeeping 
		                            inner join Staff on FlowTimeKeeping.StaffId = Staff.Id
		                            where FlowTimeKeeping.IsDelete != 1 and IsEnroll = 1 
			                            and FlowTimeKeeping.StaffId = @staffId and FlowTimeKeeping.WorkDate = @WorkDate
	                            )t
	                            CROSS APPLY x.nodes('/XMLRoot/RowData')m(n)
                            )

                            select Cast(b.HourFrame as nvarchar(8)) as HourFrame
	                            --a.*, Cast(b.HourFrame as nvarchar(8)) as HourFrame, w.w_Name
                            from a
                            inner join BookHour as b on b.Id = a.HourId
                            inner join WorkTime as w on w.Id = a.WorkTimeId
                            where b.HourFrame < w.StrartTime or b.HourFrame > EnadTime
                            order by b.HourFrame";
                var listhourOverTime = db.Database.SqlQuery<string>(sql).ToList();
                return listhourOverTime.Count > 0 ? true : false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// get instance, phục vụ trường hợp có hàm statistic, sử dụng instance có thẻ gọi các method không phải static
        /// </summary>
        /// <returns></returns>
        public static Service_Pending_Complete_V2 getInstance()
        {
            if (!(Service_Pending_Complete_V2.instance is Service_Pending_Complete_V2))
            {
                Service_Pending_Complete_V2.instance = new Service_Pending_Complete_V2();
            }

            return Service_Pending_Complete_V2.instance;
        }

        /// <summary>
        /// Get bill ID từ url query string
        /// </summary>
        /// <returns></returns>
        public int getBillID()
        {
            var ID = 0;
            try
            {
                var MatchInt = Regex.Match(Request.QueryString["Code"], @"[^0-9]+");
                if (!MatchInt.Success)
                {
                    ID = Convert.ToInt32(Request.QueryString["Code"]);
                }
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message);
            }

            return ID;
        }


        /// <summary>
        /// Kiểm tra khách có vourcher thời gian chờ quá 20 phút hay không
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        [WebMethod]
        public static object checkVoucherWaitTime(int CustomerId)
        {
            var message = new Msg();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var data = new cls_voucher_waitime();
                    data.ServiceIDs = new List<int>();
                    data.ServiceIDs.Add(53);

                    var record = db.CRM_VoucherWaitTime.Where(
                        w => w.CustomerID == CustomerId
                             && w.IsUsed == false
                             && w.IsDelete == false
                    ).OrderByDescending(o => o.ID).FirstOrDefault();
                    if (record != null)
                    {
                        data.ID = record.ID;
                        data.isVoucher = true;
                        data.VoucherPercent = record.VoucherPercent != null ? (float)record.VoucherPercent.Value : 0;
                    }
                    else
                    {
                        data.isVoucher = false;
                    }

                    message.data = data;
                    message.success = true;
                }
            }
            catch (Exception ex)
            {
                message.success = false;
                message.msg = ex.Message;
            }

            return message;
        }

        /// <summary>
        /// Update trạng thái Voucher 20% gói Shine Combo đã được áp dụng cho khách
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public object updateVoucherWaitTimeIsUsed(int? customerId)
        {
            var message = new Msg();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var record = db.CRM_VoucherWaitTime.Where(w => w.CustomerID == customerId && w.IsUsed == false)
                        .OrderByDescending(o => o.ID).FirstOrDefault();
                    if (record != null)
                    {
                        record.IsUsed = true;
                        record.ModifiedTime = DateTime.Now;
                        db.CRM_VoucherWaitTime.AddOrUpdate(record);
                        db.SaveChanges();

                        message.success = true;
                        message.msg = "Succeeded.";
                    }
                }
            }
            catch (Exception ex)
            {
                message.success = false;
                message.msg = ex.Message;
            }

            return message;
        }

        /// <summary>
        /// Tính thời gian chờ
        /// </summary>
        /// <param name="bs"></param>
        /// <returns></returns>
        public static int? GetWaitTime(BillService bs)
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
            var WaitTime = 0;
            try
            {
                var book = db.Bookings.FirstOrDefault(f => f.Id == bs.BookingId);
                if (book != null && book.IsBookOnline == true)
                {
                    DateTime? pvgTime = bs.InProcedureTime;
                    DateTime? checkinTime = bs.CreatedDate;
                    DateTime bookHour = new DateTime();
                    var hour = db.BookHours.FirstOrDefault(w => w.Id == book.HourId);
                    bookHour = bs.CreatedDate.Value.Date.Add(hour.HourFrame.Value);

                    if (checkinTime >= bookHour)
                    {
                        WaitTime = (int)(pvgTime - checkinTime).Value.TotalMinutes;
                    }
                    else
                    {
                        WaitTime = (int)(pvgTime - bookHour).Value.TotalMinutes;
                    }
                }
            }
            catch (Exception ex)
            {
                //throw new Exception("Error");
                return null;
            }

            return WaitTime;
        }

        /// <summary>
        /// Check khách có phải đặc biệt hay không
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public bool isCustomerSpecial(int customerID)
        {
            try
            {
                var ret = false;
                var specialCustomer = new SpecialCustomer();
                var detailSpecialCustomer = new SpecialCusDetail();
                // Kiểm tra khách hàng đặc biệt
                specialCustomer = this.db.SpecialCustomers
                    .Where(a => a.IsDelete == false && a.CustomerTypeId == 1 && a.CustomerId == customerID)
                    .OrderBy(a => a.Id).FirstOrDefault();

                if (specialCustomer != null)
                {
                    detailSpecialCustomer = this.db.SpecialCusDetails
                        .Where(a => a.IsDelete == false && a.QuantityInvited > 0 &&
                                    a.SpecialCusId == specialCustomer.Id).OrderByDescending(a => a.Id).FirstOrDefault();
                    if (detailSpecialCustomer != null)
                    {
                        ret = true;
                    }
                }

                return ret;
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Check khách có phải khách VIP hay không
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public bool isCustomerVIP(int customerID)
        {
            try
            {
                var ret = false;
                var specialCustomer = new SpecialCustomer();
                var detailSpecialCustomer = new SpecialCusDetail();
                // Kiểm tra khách hàng đặc biệt
                specialCustomer = this.db.SpecialCustomers
                    .Where(a => a.IsDelete == false && a.CustomerTypeId == 2 && a.CustomerId == customerID)
                    .OrderBy(a => a.Id).FirstOrDefault();

                if (specialCustomer != null)
                {
                    ret = true;
                }

                return ret;
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Kiểm tra 1 bill có phải bill khách đợi lâu hay không (có thể là 20 phút)
        /// Điều kiện:
        /// 1. Khách chờ lâu quá n phút
        /// 2. Khách không phải đặc biệt, VIP
        /// </summary>
        /// <param name="bill"></param>
        /// <returns></returns>
        public bool isWaitLongTime(BillService bill)
        {
            try
            {
                var isWait = false;
                int integer;
                var configModel = new ConfigModel();
                var configWaitTime =
                    int.TryParse(configModel.GetValueByKey("bill_voucher_waittime_minute"), out integer) ? integer : 0;

                if (bill != null && (!instance.isCustomerSpecial(bill.CustomerId.Value) &&
                                     !instance.isCustomerVIP(bill.CustomerId.Value)))
                {
                    int? time = GetWaitTime(bill);
                    if (time != null && time >= configWaitTime)
                    {
                        isWait = true;
                    }
                }

                return isWait;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Kiểm tra 1 bill có phải bill khách đợi lâu hay không
        /// </summary>
        /// <param name="billID"></param>
        /// <returns></returns>
        [WebMethod]
        public static object checkCustomerIsWaitLongTime(int billID)
        {
            var message = new Library.Class.cls_message();
            var instance = Service_Pending_Complete_V2.getInstance();
            try
            {
                var bill = instance.db.BillServices.FirstOrDefault(w => w.Id == billID);
                if (instance.isWaitLongTime(bill))
                {
                    message.success = true;
                    message.data = true;
                    message.message = "Khách đợi lâu";
                }
                else
                {
                    message.success = true;
                    message.data = false;
                    message.message = "";
                }

                return message;
            }
            catch
            {
                message.success = false;
                message.data = false;
                message.message = "Có lỗi xảy ra. Vui lòng liên hệ nhà phát triển.";
                return message;
            }
        }

        /// <summary>
        /// Tạo voucher khách hàng trường hợp khách đợi quá 20 phút
        /// </summary>
        /// <param name="billID"></param>
        //[WebMethod]
        public void createVoucher(BillService bill)
        {
            try
            {
                if (bill != null)
                {
                    var configModel = new ConfigModel();
                    var payonModel = new PayonConfigModel();
                    float _float;
                    var itemVoucher = new cls_voucher_waitime_custom();
                    var today = DateTime.Now.Date;
                    Staff staff;

                    var staffModel = new StaffModel();
                    // Update cho Stylist
                    if (bill.Staff_Hairdresser_Id != null)
                    {
                        staff = staffModel.GetStaffById(bill.Staff_Hairdresser_Id.Value);
                        itemVoucher.StylistConfigSalary = payonModel.GetValueByKey(staff, "salary_coefficient_rating");
                        itemVoucher.StylistPoint =
                            float.TryParse(configModel.GetValueByKey("bill_voucher_waittime_20p_stylist_point"),
                                out _float)
                                ? _float
                                : 0;
                        this.UpdateSalaryByStaff(staff, today, itemVoucher.StylistPoint.Value,
                            itemVoucher.StylistConfigSalary);
                    }

                    // Update cho Skinner
                    if (bill.Staff_HairMassage_Id != null)
                    {
                        staff = staffModel.GetStaffById(bill.Staff_HairMassage_Id.Value);
                        itemVoucher.SkinnerConfigSalary = payonModel.GetValueByKey(staff, "salary_coefficient_rating");
                        itemVoucher.SkinnerPoint =
                            float.TryParse(configModel.GetValueByKey("bill_voucher_waittime_20p_skinner_point"),
                                out _float)
                                ? _float
                                : 0;
                        this.UpdateSalaryByStaff(staff, today, itemVoucher.SkinnerPoint.Value,
                            itemVoucher.SkinnerConfigSalary);
                    }

                    // Update cho Checkin
                    if (bill.ReceptionId != null)
                    {
                        staff = staffModel.GetStaffById(bill.ReceptionId.Value);
                        itemVoucher.CheckinConfigSalary = payonModel.GetValueByKey(staff, "salary_coefficient_rating");
                        itemVoucher.CheckinPoint =
                            float.TryParse(configModel.GetValueByKey("bill_voucher_waittime_20p_checkin_point"),
                                out _float)
                                ? _float
                                : 0;
                        this.UpdateSalaryByStaff(staff, today, itemVoucher.CheckinPoint.Value,
                            itemVoucher.CheckinConfigSalary);
                    }

                    // insert bản ghi voucher
                    this.vourcherWaitTimeInsert(bill, itemVoucher);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Cập nhật lại lương cho nhân viên (cập nhật điểm trừ trong trường hợp thời gian chờ lâu)
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="date"></param>
        /// <param name="configPoint"></param>
        /// <param name="configSalary"></param>
        public void UpdateSalaryByStaff(Staff staff, DateTime date, double configPoint, int configSalary)
        {
            try
            {
                if (staff != null && date != null)
                {
                    var salaryModel = new SalaryModel();
                    var flowSalary = salaryModel.GetFlowSalaryByStaffDate(staff, date.Date);
                    if (flowSalary != null)
                    {
                        if (flowSalary.waitTimeMistake == null || flowSalary.waitTimeMistake == 0)
                        {
                            flowSalary.waitTimeMistake = configPoint;
                        }
                        else
                        {
                            flowSalary.waitTimeMistake = configPoint + flowSalary.waitTimeMistake;
                        }

                        flowSalary.ratingPoint = flowSalary.ratingPoint != null ? flowSalary.ratingPoint : 0;
                        flowSalary.serviceSalary = (float)configSalary *
                                                   ((float)flowSalary.ratingPoint - flowSalary.waitTimeMistake);

                        salaryModel.Update(flowSalary);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Insert voucher wait-time
        /// </summary>
        /// <param name="bill"></param>
        public void vourcherWaitTimeInsert(BillService bill, cls_voucher_waitime_custom item)
        {
            try
            {
                //add data to CRM_VoucherWaitTime
                CRM_VoucherWaitTime crm_voucher = new CRM_VoucherWaitTime();
                crm_voucher.BillID = bill.Id;
                crm_voucher.CheckinID = bill.ReceptionId;
                crm_voucher.CreatedTime = DateTime.Now;
                crm_voucher.CustomerID = bill.CustomerId;

                crm_voucher.SalonID = bill.SalonId;
                crm_voucher.SkinnerID = bill.Staff_HairMassage_Id;
                crm_voucher.StylistID = bill.Staff_Hairdresser_Id;

                crm_voucher.StylistPoint = item.StylistPoint;
                crm_voucher.SkinnerPoint = item.SkinnerPoint;
                crm_voucher.CheckinPoint = item.CheckinPoint;

                crm_voucher.StylistMoney = item.StylistPoint * item.StylistConfigSalary;
                crm_voucher.SkinnerMoney = item.SkinnerPoint * item.SkinnerConfigSalary;
                crm_voucher.CheckinMoney = item.CheckinPoint * item.CheckinConfigSalary;

                crm_voucher.VoucherPercent = 20;
                crm_voucher.IsUsed = false;
                crm_voucher.IsDelete = false;

                this.db.CRM_VoucherWaitTime.Add(crm_voucher);
                this.db.SaveChanges();
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message);
            }
        }


        /// <summary>
        /// Bind dữ liệu bill
        /// </summary>
        /// <returns></returns>
        private bool Bind_Bill()
        {
            using (var db = new Solution_30shineEntities())
            {
                var ExistOBJ = false;
                var MatchInt = Regex.Match(Request.QueryString["Code"], @"[^0-9]+");
                if (!MatchInt.Success)
                {
                    var _Code = Convert.ToInt32(Request.QueryString["Code"]);
                    OBJ = db.BillServices.FirstOrDefault(w => w.Id == _Code);
                    bill = OBJ;
                    billJson = Library.Format.ObjectToJson(bill);

                    if (OBJ != null)
                    {
                        HDF_SalonId.Value = OBJ.SalonId.ToString();
                        HDF_lgId.Value = accountModel.AccountInfor.UserId.ToString();

                        if (OBJ.CompleteBillTime != null && !Perm_ViewAllData)
                        {
                            ContentWrap.Visible = false;
                            NotAllowAccess.Visible = true;
                            return false;
                        }

                        // Set trang thai bill tang ca neu Stylist dang ky tang ca 
                        // (dua vao gio cham cong, co khung gio nam ngoai ca thi la tang ca)
                        try
                        {
                            IsBillOverTime = IsOverTime(OBJ);
                        }
                        catch
                        {
                            IsBillOverTime = false;
                        }

                        //set HDF_CustomerId (Id khách hàng)
                        HDF_CustomerId.Value = Convert.ToString(OBJ.CustomerId);
                        // Set trạng thái x2
                        //if (OBJ.IsX2 != null && OBJ.IsX2 == true)
                        //{
                        //    isX2.Checked = true;
                        //}                        

                        // Set Customer 30Shine Care
                        if (OBJ.ServiceError != null && OBJ.ServiceError == true)
                        {
                            cbCustomer30ShineCare.Checked = true;
                        }

                        var customerName = db.Customers.FirstOrDefault(w => w.Id == OBJ.CustomerId);
                        CustomerName.Text = customerName.Fullname;
                        _CustomerName = customerName.Fullname;
                        CustomerCode.Text = "Số ĐT : " + OBJ.CustomerCode;
                        TotalMoney.Text = OBJ.TotalMoney.ToString();
                        Description.Text = OBJ.Note;
                        HDF_CustomerCode.Value = OBJ.CustomerCode;
                        HDF_BillId.Value = OBJ.Id.ToString();
                        if (OBJ.Mark != null)
                        {
                            //code kiểm tra cho app rating mới, nếu không phải salon test thì cho mark=2,
                            //if (IsSalonTestRating(SalonIdBill))
                            //{
                            //    OBJ.Mark = 2;
                            //}
                            HDF_Rating.Value = OBJ.Mark.ToString();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "bind rating",
                                "bindRating(" + OBJ.Mark + ");", true);
                        }

                        // Staff information
                        var staff = new Staff();
                        if (OBJ.Staff_Hairdresser_Id > 0)
                        {
                            staff = db.Staffs.FirstOrDefault(w => w.Id == OBJ.Staff_Hairdresser_Id);
                            if (staff != null && staff.OrderCode != null)
                            {
                                FVStylist.InnerText = staff.OrderCode.ToString() + "- " + staff.Fullname;
                                InputStylist.Text = staff.OrderCode.ToString();
                                Hairdresser.Value = staff.Id.ToString();
                            }
                        }

                        if (OBJ.Staff_HairMassage_Id > 0)
                        {
                            staff = db.Staffs.FirstOrDefault(w => w.Id == OBJ.Staff_HairMassage_Id);
                            if (staff != null && staff.OrderCode != null)
                            {
                                FVSkinner.InnerText = staff.OrderCode.ToString() + "- " + staff.Fullname;
                                InputSkinner.Text = staff.OrderCode.ToString();
                                HairMassage.Value = staff.Id.ToString();
                            }
                        }

                        if (OBJ.SellerId > 0)
                        {
                            staff = db.Staffs.FirstOrDefault(w => w.Id == OBJ.SellerId);
                            if (staff != null && staff.OrderCode != null)
                            {
                                FVSeller.InnerText = staff.OrderCode.ToString() + "- " + staff.Fullname;
                                InputSeller.Text = staff.OrderCode.ToString();
                                Cosmetic.Value = staff.Id.ToString();
                            }
                        }

                        if (OBJ.ReceptionId > 0)
                        {
                            staff = db.Staffs.FirstOrDefault(w => w.Id == OBJ.ReceptionId);
                            if (staff != null && staff.OrderCode != null)
                            {
                                FVReception.InnerText = staff.OrderCode.ToString() + "- " + staff.Fullname;
                                InputReception.Text = staff.OrderCode.ToString();
                                HDF_Reception.Value = staff.Id.ToString();
                            }
                        }

                        if (OBJ.CheckoutId > 0)
                        {
                            staff = db.Staffs.FirstOrDefault(w => w.Id == OBJ.CheckoutId);
                            if (staff != null && staff.OrderCode != null)
                            {
                                FVCheckout.InnerText = staff.OrderCode.ToString() + "- " + staff.Fullname;
                                InputCheckout.Text = staff.OrderCode.ToString();
                                HDF_Checkout.Value = staff.Id.ToString();
                            }
                        }

                        if (OBJ.Images != "" && OBJ.Images != null)
                        {
                            ListImagesName = OBJ.Images.Split(',');
                            var Len = ListImagesName.Length;
                            if (Len > 0)
                            {
                                var loop = 0;
                                foreach (var v in ListImagesName)
                                {
                                    var match = Regex.Match(v, @"api.30shine.com");
                                    if (!match.Success)
                                    {
                                        //ListImagesName[loop] = "https://30shine.com/" + v;
                                    }

                                    loop++;
                                }

                                HasImages = true;
                            }
                        }

                        if (OBJ.IsPayByCard != null && OBJ.IsPayByCard.Value)
                        {
                            PayByCard.Checked = true;
                        }

                        //Off hiển thị điểm hài lòng đối với account Checkout
                        //if (Array.IndexOf(new int[] { 3, 6 }, AccountModel.Instance.AccountInfor.UserDepartmentId) != -1)
                        //{
                        //ctrlTrRating.Visible = false;
                        //}

                        _CustomerCode = OBJ.CustomerCode;
                        CusNoInfor = Convert.ToBoolean(OBJ.CustomerNoInfo);
                        ExistOBJ = true;

                        // Get list free service (Phụ trợ)
                        lstFreeService = db.Database
                            .SqlQuery<int>("select PromotionId from FlowPromotion where BillId = " + OBJ.Id +
                                           " and IsDelete != 1").ToList();

                        // Set giá trị cho customerID
                        this.customerID = OBJ.CustomerId.Value;
                    }
                }
                else
                {
                    MsgSystem.Text = "Không tồn tại hóa đơn!";
                    MsgSystem.CssClass = "msg-system warning";
                }

                return ExistOBJ;
            }
        }

        ///// <summary>
        ///// Bind danh sách dịch vụ trong bill
        ///// </summary>
        //private void Bind_RptService_Bill()
        //{
        //    using (var db = new Solution_30shineEntities())
        //    {
        //        //End code cũ
        //        var _Services = db.Store_CheckOut_BindService(OBJ.Id).ToList();
        //        if (_Services.Count > 0)
        //        {
        //            ListingServiceWp.Style.Add("display", "block");
        //            HasService = true;
        //        }
        //        Rpt_Service_Bill.DataSource = _Services;
        //        Rpt_Service_Bill.DataBind();
        //    }
        //}

        /// <summary>
        /// Bind danh sách dịch vụ trong bill
        /// </summary>
        private void Bind_RptService_Bill()
        {
            using (var db = new Solution_30shineEntities())
            {
                //End code cũ
                //khi checkout
                if (OBJ.CompleteBillTime == null && OBJ.Pending == 1)
                {
                    var _Services = db.Store_CheckOut_BindService(OBJ.Id).ToList();
                    if (_Services.Count > 0)
                    {
                        ListingServiceWp.Style.Add("display", "block");
                        HasService = true;
                    }

                    Rpt_Service_Bill.DataSource = _Services;
                    Rpt_Service_Bill.DataBind();
                }
                //khi sửa bill
                else
                {
                    List<ProductBasic> lstService = new List<ProductBasic>();
                    var serializer = new JavaScriptSerializer();
                    if (OBJ.ServiceIds != "")
                    {
                        lstService = serializer.Deserialize<List<ProductBasic>>(OBJ.ServiceIds).ToList();
                        if (lstService.Count > 0)
                        {
                            ListingServiceWp.Style.Add("display", "block");
                            HasService = true;
                        }
                    }

                    Rpt_Service_Bill.DataSource = lstService;
                    Rpt_Service_Bill.DataBind();
                }
            }
        }

        /// <summary>
        // / Bind danh sách sản phẩm trong bill
        // / </summary>
        private void Bind_RptProduct_Bill()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Products = db.Store_CheckOutProduct(OBJ.Id).ToList();
                ;
                if (_Products.Count > 0)
                {
                    ListingProductWp.Style.Add("display", "block");
                    HasProduct = true;
                }

                Rpt_Product_Bill.DataSource = _Products;
                Rpt_Product_Bill.DataBind();
            }
        }

        /// <summary>
        /// Đợi hoàn tất bill, khi check out mở nhầm bill
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void WaitFinish_Click(object sender, EventArgs e)
        {
            PushNotiRatingClose(this.billID);
            DeleteRatingIscheck(this.billID);
            // Thông báo thành công
            var MsgParam = new List<KeyValuePair<string, string>>();
            MsgParam.Add(new KeyValuePair<string, string>("msg_wait_status", "success"));
            MsgParam.Add(new KeyValuePair<string, string>("msg_wait_message",
                "Hóa đơn đã chuyển sang trạng thái đợi!"));
            UIHelpers.Redirect("/dich-vu/pending.html", MsgParam);
        }
        ///// <summary>
        ///// Kiểm tra trừ lương của bill rồi update lại, pending = 0;
        ///// </summary>
        ///// <param name="obj"></param>
        ///// <param name="is_stylist_overtime"></param>
        ///// <param name="is_skinner_overtime"></param>
        ///// <param name="is_voucher_long_time"></param>
        ///// <returns></returns>
        //public async Task PutCheckCheckOut(Input_Checkout obj)
        //{
        //    var msg = "";
        //    var status = "";
        //    var MsgParam = new List<KeyValuePair<string, string>>();
        //    try
        //    {
        //        using (var client = new HttpClient())
        //        {
        //            client.BaseAddress = new Uri(Libraries.AppConstants.URL_API_FINANCIAL);
        //            client.DefaultRequestHeaders.Accept.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //            var Result = client.PutAsJsonAsync("/bill-service/check-check-out", obj).Result;
        //            if (Result.IsSuccessStatusCode)
        //            {
        //                var result = Result.Content.ReadAsStringAsync().Result;
        //                JavaScriptSerializer serializer = new JavaScriptSerializer();
        //                var responseData = serializer.Deserialize<ResponseData>(result);
        //            }
        //            else
        //            {
        //                throw new Exception();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}

        ///// <summary>
        ///// Update lương khi checkout
        ///// </summary>
        ///// <param name="obj"></param>
        ///// <param name="is_stylist_overtime"></param>
        ///// <param name="is_skinner_overtime"></param>
        ///// <param name="is_voucher_long_time"></param>
        ///// <returns></returns>
        //public async Task PutCheckOut(Input_Checkout obj)
        //{
        //    var msg = "";
        //    var status = "";
        //    var MsgParam = new List<KeyValuePair<string, string>>();
        //    try
        //    {
        //        using (var client = new HttpClient())
        //        {
        //            client.BaseAddress = new Uri(Libraries.AppConstants.URL_API_FINANCIAL);
        //            client.DefaultRequestHeaders.Accept.Clear();
        //            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //            var Result = client.PutAsJsonAsync("/bill-service/check-out", obj).Result;
        //            if (Result.IsSuccessStatusCode)
        //            {
        //                var result = Result.Content.ReadAsStringAsync().Result;
        //                JavaScriptSerializer serializer = new JavaScriptSerializer();
        //                var responseData = serializer.Deserialize<ResponseData>(result);
        //            }
        //            else
        //            {
        //                throw new Exception();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}

        /// <summary>
        /// Hoàn tất bill
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CompleteService(object sender, EventArgs e)
        {
            try
            {
                RegisterAsyncTask(new PageAsyncTask(checkout));
            }
            catch (Exception ex)
            {
                //Library.Function.WriteToFile("error asynx " + DateTime.Now + ": " + " ex " + ex.StackTrace,
                //    PATH_ASYNC_CHECKOUT_BILL_LOG_EXCEPTION);
            }
        }

        public async Task checkout()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var MatchInt = Regex.Match(Request.QueryString["Code"], @"[^0-9]+");
                    int integer;
                    bool IsCheckout = false;

                    DeleteRatingIscheck(this.billID);
                    PushNotiRatingClose(this.billID);

                    if (!MatchInt.Success)
                    {
                        var _Code = int.TryParse(Request.QueryString["Code"], out integer) ? integer : 0;
                        /// Cập nhật số lượng sử dụng dịch vụ miễn phí cho khách đặc biệt
                        /// Update 1 lần cho 1 bill
                        /// Bill đang pending
                        var OBJBillService = db.BillServices.FirstOrDefault(w => w.Id == _Code && w.Pending == 1);
                        if (OBJBillService != null)
                        {
                            updateQuantityInvited();
                            IsCheckout = true;
                        }

                        OBJ = db.BillServices.FirstOrDefault(w => w.Id == _Code);
                        //luu lai gia tri ban ghi cu de static
                        BillService OBJStatic = new BillService();
                        ReflectionExtension.CopyObjectValue(OBJ, OBJStatic);

                        if (OBJ != null)
                        {
                            BillService billOld = new BillService();
                            try
                            {
                                Libraries.ReflectionExtension.CopyObjectValue(OBJ, billOld);
                            }
                            catch (Exception ex)
                            {
                                // Push thông báo cho mọi người(by slack) khi có lỗi xảy ra
                                new Helpers.PushNotiErrorToSlack().Push("GA1LZN9AM", ex.Message + ", Libraries.ReflectionExtension.CopyObjectValue(OBJ, billOld) " + ex.StackTrace, this.ToString() + ".checkout", "phucdn",
                                                                        HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host);
                            }
                            //author: dungnm
                            //check
                            var callApi = new CallApiCheckout();
                            if (OBJ.Staff_Hairdresser_Id != null || OBJ.Staff_Hairdresser_Id > 0)
                            {
                                callApi.isStylistOvertimeOld = checkOverTime(OBJ.BookingId, OBJ.Staff_Hairdresser_Id.Value, OBJ.CreatedDate.Value);
                            }
                            if (OBJ.Staff_HairMassage_Id != null || OBJ.Staff_HairMassage_Id > 0)
                            {
                                callApi.isSkinnerOvertimeOld = checkOverTime(OBJ.BookingId, OBJ.Staff_HairMassage_Id.Value, OBJ.CreatedDate.Value);
                            }

                            //end
                            // Update Customer 30Shine Care
                            OBJ.ServiceError = cbCustomer30ShineCare.Checked;

                            //// Insert to BillService
                            _CustomerCode = HDF_CustomerCode.Value;
                            var customer = db.Customers.FirstOrDefault(w => w.Id == OBJ.CustomerId);
                            _CustomerName = customer != null ? customer.Fullname : "";

                            if (OBJ.CompleteBillTime == null)
                            {
                                //Update table Customer khi submit billservice
                                var request = new Request();
                                OBJ.CompleteBillTime = DateTime.Now;
                                if (customer != null && customer.TotalBillservice != null)
                                {
                                    OBJ.CustomerUsedNumber = customer.TotalBillservice + 1;
                                }
                                else if (customer != null && customer.TotalBillservice == null)
                                {
                                    OBJ.CustomerUsedNumber = 1;
                                }
                                var dataCus = new
                                {
                                    DataCustomer = new
                                    {
                                        Id = customer.Id
                                    }
                                };
                                Task taskUpdateCustomer = Task.Run(async () =>
                                {
                                    await request.RunPostAsync(
                                            domainERPLiveSite + "/GUI/SystemService/Webservice/wsBillService.asmx/UpdateTotalBillCustomer",
                                            dataCus
                                        );
                                });
                                var data = new Data_ThongkeVattu
                                {
                                    BillId = OBJ.Id,
                                    StylistId = (int.TryParse(Hairdresser.Value, out integer) ? integer : 0),
                                    SkinnerId = (int.TryParse(HairMassage.Value, out integer) ? integer : 0),
                                    CompletedTime = OBJ.CompleteBillTime
                                };
                                try
                                {
                                    /// Tinh cho stylist

                                    Task taskStatisBillNew = Task.Run(async () =>
                                    {
                                        await request.RunPostAsync(
                                            domainERPLiveSite +
                                            "/GUI/SystemService/Webservice/wsBillService.asmx/ThongKeVatTuNewBill",
                                            data
                                        );
                                    });
                                    //taskStatisBillNew.Wait(100);
                                }
                                catch (Exception ex)
                                {

                                    new Helpers.PushNotiErrorToSlack().Push("GA1LZN9AM", ex.Message + ", taskStatisBillNew: " + ex.StackTrace + ", " + Library.Function.JavaScript.Serialize(data),
                                                                            this.ToString() + ".checkout", "phucdn",
                                                                            HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host);
                                }

                                this.updateVoucherWaitTimeIsUsed(OBJ.CustomerId);
                            }
                            else
                            {
                                //Update giá trị trọn đời của khách hàng
                                if (customer != null)
                                {
                                    customer.TotalMoneyBillservice =
                                        Convert.ToInt32(customer.TotalMoneyBillservice) - OBJ.TotalMoney +
                                        Convert.ToInt32(HDF_TotalMoney.Value);
                                    db.SaveChanges();
                                }

                                // StatisticVatTuUpdateBill 
                                var data = new Data_ThongkeVattu
                                {
                                    BillId = OBJ.Id,
                                    StylistId = (int.TryParse(Hairdresser.Value, out integer) ? integer : 0),
                                    StylistOldId = OBJ.Staff_Hairdresser_Id != null ? OBJ.Staff_Hairdresser_Id : 0,
                                    SkinnerId = (int.TryParse(HairMassage.Value, out integer) ? integer : 0),
                                    SkinnerOldId = OBJ.Staff_HairMassage_Id != null ? OBJ.Staff_HairMassage_Id : 0,
                                    CompletedTime = OBJ.CompleteBillTime
                                };
                                try
                                {
                                    var request = new Request();
                                    /// Tinh cho stylist

                                    Task taskStatisBillUpdate = Task.Run(async () =>
                                    {
                                        await request.RunPostAsync(
                                            domainERPLiveSite +
                                            "/GUI/SystemService/Webservice/wsBillService.asmx/ThongKeVatTuUpdateBill",
                                            new
                                            {
                                                data = data
                                            }
                                        );
                                    });
                                    //taskStatisBillUpdate.Wait(100);
                                }
                                catch (Exception ex)
                                {
                                    new Helpers.PushNotiErrorToSlack().Push("GA1LZN9AM", ex.Message + ", taskStatisBillUpdate" + ex.StackTrace + ", " + Library.Function.JavaScript.Serialize(data),
                                                                            this.ToString() + ".checkout", "phucdn",
                                                                            HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host);
                                }

                            }

                            var stylistId = int.TryParse(Hairdresser.Value, out integer) ? integer : 0;
                            if (stylistId > 0)
                            {
                                var stylist = db.Staffs.FirstOrDefault(w => w.Id == stylistId);
                                Stylist = stylist != null ? stylist.Fullname : "";
                                OBJ.Staff_Hairdresser_Id = stylistId;
                                callApi.isStylistOvertimeNew = checkOverTime(OBJ.BookingId, OBJ.Staff_Hairdresser_Id.Value, OBJ.CreatedDate.Value);
                            }
                            else
                            {
                                OBJ.Staff_Hairdresser_Id = 0;
                            }

                            var skinnerId = int.TryParse(HairMassage.Value, out integer) ? integer : 0;
                            if (skinnerId > 0)
                            {
                                var skinner = db.Staffs.FirstOrDefault(w => w.Id == skinnerId);
                                Skinner = skinner != null ? skinner.Fullname : "";
                                OBJ.Staff_HairMassage_Id = skinnerId;
                                callApi.isSkinnerOvertimeNew = checkOverTime(OBJ.BookingId, OBJ.Staff_HairMassage_Id.Value, OBJ.CreatedDate.Value);
                            }
                            else
                            {
                                OBJ.Staff_HairMassage_Id = 0;
                            }

                            var sellerId = int.TryParse(Cosmetic.Value, out integer) ? integer : 0;
                            if (sellerId > 0)
                            {
                                var seller = db.Staffs.FirstOrDefault(w => w.Id == sellerId);
                                Seller = seller != null ? seller.Fullname : "";
                                OBJ.SellerId = sellerId;
                            }
                            else
                            {
                                OBJ.SellerId = 0;
                            }

                            if (HDF_Reception.Value != "")
                            {
                                OBJ.ReceptionId = Convert.ToInt32(HDF_Reception.Value);
                            }
                            else
                            {
                                OBJ.ReceptionId = 0;
                            }

                            if (HDF_Checkout.Value != "")
                            {
                                OBJ.CheckoutId = Convert.ToInt32(HDF_Checkout.Value);
                            }
                            else
                            {
                                OBJ.CheckoutId = 0;
                            }

                            if (TrSalon.Visible == true)
                            {
                                OBJ.SalonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
                            }

                            OBJ.ProductIds = HDF_ProductIds.Value != "[]" ? HDF_ProductIds.Value : "";
                            OBJ.ServiceIds = HDF_ServiceIds.Value != "[]" ? HDF_ServiceIds.Value : "";
                            OBJ.TotalMoney = Convert.ToInt32(HDF_TotalMoney.Value);
                            OBJ.Note = Description.Text;
                            OBJ.ModifiedDate = DateTime.Now;
                            OBJ.Pending = 0;
                            List<int> serviceRating = new List<int>();

                            OBJ.Mark = int.TryParse(HDF_Rating.Value, out integer) ? integer : 0;
                            if (OBJ.Mark == 0)
                            {
                                OBJ.Mark = 3;
                                // Thêm bản ghi xác nhận là khách không đánh giá
                                ServiceRatingModel.Instance.AddRelationship(OBJ);
                            }
                            else
                            {
                                //Cap nhat xoa ban ghi khach khong danh gia.
                                ServiceRatingModel.Instance.DeleteRelationship(OBJ.Id);
                            }

                            if (VIPGive.Value != "")
                            {
                                OBJ.VIPCard = true;
                                OBJ.VIPCardGive = genVIPCard(VIPGive.Value, 4);
                            }

                            if (VIPUse.Value != "")
                            {
                                OBJ.VIPCardUse = genVIPCard(VIPUse.Value, 4);
                            }

                            // Kiểm tra khách thanh toán qua thẻ hay không
                            OBJ.IsPayByCard = PayByCard.Checked;

                            // Tính lương thử nghiệm cho Checkin
                            db.BillServices.AddOrUpdate(OBJ);
                            var exc = db.SaveChanges();
                            var Error = exc > 0 ? 0 : 1;
                            var serializer = new JavaScriptSerializer();

                            try
                            {
                                // Insert to FlowProduct
                                if (Error == 0)
                                {
                                    //var str = "[{\"Id\":\"19\",\"Price\":340000,\"Quantity\":\"1\"}]";
                                    serializer = new JavaScriptSerializer();
                                    ProductList = serializer.Deserialize<List<ProductBasic>>(OBJ.ProductIds);
                                    var ProductListInserted = db.FlowProducts
                                        .Where(w => w.BillId == OBJ.Id && w.IsDelete != 1).ToList();
                                    var index = -1;
                                    var _product = new Product();

                                    if (ProductList != null)
                                    {
                                        //Dungnm thêm mới đơn hàng khi có sản phẩm
                                        ICosmeticModel cosmetic = new CosmeticModel();
                                        var svProduct = cosmetic.Add(Convert.ToInt32(OBJ.SalonId),
                                            Convert.ToDateTime(OBJ.CreatedDate));
                                        string billcode = svProduct;
                                        DateTime orderDate = Convert.ToDateTime(OBJ.CreatedDate, culture);
                                        var orderDesc = db.QLKho_SalonOrder.FirstOrDefault(w => w.BillCode == billcode);
                                        //dungnm end
                                        // Add/Update product
                                        foreach (var v in ProductList)
                                        {
                                            if (Error == 0)
                                            {
                                                _product = db.Products.FirstOrDefault(w => w.Id == v.Id);
                                                index = ProductListInserted.FindIndex(fi => fi.ProductId == v.Id);
                                                if (index == -1)
                                                {
                                                    //Static product 
                                                    AddStatisticProduct(OBJ, v);
                                                    //end
                                                    objProduct = new FlowProduct();
                                                    objProduct.BillId = OBJ.Id;
                                                    objProduct.ProductId = v.Id;
                                                    objProduct.Price = v.Price;
                                                    objProduct.Quantity = v.Quantity;
                                                    objProduct.VoucherPercent = Convert.ToInt32(v.VoucherPercent);
                                                    objProduct.PromotionMoney = Convert.ToInt32(v.Promotion);
                                                    objProduct.CreatedDate = OBJ.CreatedDate;
                                                    //Dũng yêu cầu thêm đoạn này
                                                    if (v.MapIdProduct == null || v.MapIdProduct == "" ||
                                                        v.MapIdProduct == "undefined")
                                                    {
                                                        objProduct.CheckCombo = false;
                                                    }
                                                    else
                                                    {
                                                        objProduct.CheckCombo = true;
                                                    }

                                                    objProduct.IsDelete = 0;
                                                    if (_product != null)
                                                    {
                                                        objProduct.Cost = Convert.ToInt32(_product.Cost);
                                                        objProduct.ForSalary = _product.ForSalary;
                                                    }

                                                    if (Perm_ShowSalon)
                                                    {
                                                        objProduct.SalonId = int.TryParse(Salon.SelectedValue, out integer)
                                                            ? integer
                                                            : 0;
                                                    }
                                                    else
                                                    {
                                                        objProduct.SalonId = int.TryParse(HDF_SalonId.Value, out integer)
                                                            ? integer
                                                            : 0;
                                                    }

                                                    objProduct.SellerId = int.TryParse(Cosmetic.Value, out integer)
                                                        ? integer
                                                        : 0;
                                                    objProduct.SalonOrder = orderDesc.Id;
                                                    db.FlowProducts.Add(objProduct);
                                                    exc = db.SaveChanges();

                                                    //Check mapid null
                                                    if (v.MapIdProduct != null)
                                                    {
                                                        //Check mapid sản phẩm trùng với mapid combo
                                                        var mapIdProduct = db.Products.FirstOrDefault(f =>
                                                            f.MapIdProduct == v.MapIdProduct);

                                                        //check bill id theo id billservice
                                                        var _id = db.FlowProducts.Where(w => w.BillId == OBJ.Id);
                                                        if (_id != null)
                                                        {
                                                            if (mapIdProduct != null)
                                                            {
                                                                //tách mapid 
                                                                string map = mapIdProduct.MapIdProduct;
                                                                string[] mapid = map.Split(',');
                                                                for (int i = 0; i < mapid.Length; i++)
                                                                {
                                                                    //Insert vào flowproduct sau khi tách mapid
                                                                    var id = Convert.ToInt32(mapid[i]);
                                                                    var objProducts = new FlowProduct();
                                                                    var ProductIds = db.Database
                                                                        .SqlQuery<Product>(
                                                                            "select * from Product where Id =  " + id + "")
                                                                        .FirstOrDefault();
                                                                    objProducts.ComboId = v.Id;
                                                                    objProducts.BillId = OBJ.Id;
                                                                    objProducts.ProductId = ProductIds.Id;
                                                                    objProducts.Price = ProductIds.Price;
                                                                    objProducts.Quantity = v.Quantity;
                                                                    objProducts.VoucherPercent =
                                                                        Convert.ToInt32(v.VoucherPercent);
                                                                    objProducts.PromotionMoney =
                                                                        Convert.ToInt32(v.Promotion);
                                                                    objProducts.CreatedDate = OBJ.CreatedDate;
                                                                    objProducts.IsDelete = 0;
                                                                    objProducts.ForSalary = 0;
                                                                    objProducts.Cost = 0;
                                                                    objProducts.ModifiedDate = DateTime.Now;
                                                                    objProducts.SellerId = OBJ.SellerId;
                                                                    objProducts.SalonId = OBJ.SalonId;
                                                                    //Đổi true thành false
                                                                    objProducts.CheckCombo = false;
                                                                    objProducts.SalonOrder = orderDesc.Id;
                                                                    db.FlowProducts.Add(objProducts);
                                                                    db.SaveChanges();
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    var prd = ProductListInserted[index];
                                                    if (v.MapIdProduct != null)
                                                    {
                                                        //Insert vào flowproduct sau khi tách mapid
                                                        var mapIdProduct = db.Products.FirstOrDefault(f =>
                                                            f.MapIdProduct == v.MapIdProduct);
                                                        var _id = db.FlowProducts.Where(w => w.BillId == prd.BillId);
                                                        if (_id != null)
                                                        {
                                                            if (mapIdProduct != null)
                                                            {
                                                                string map = mapIdProduct.MapIdProduct;
                                                                string[] mapid = map.Split(',');
                                                                for (int i = 0; i < mapid.Length; i++)
                                                                {
                                                                    var id = Convert.ToInt32(mapid[i]);
                                                                    var ProductIds = db.Database
                                                                        .SqlQuery<Product>(
                                                                            "select * from Product where Id =  " + id + "")
                                                                        .FirstOrDefault();
                                                                    var objProducts = new FlowProduct();
                                                                    objProducts.ComboId = v.Id;
                                                                    objProducts.BillId = prd.BillId;
                                                                    objProducts.ProductId = ProductIds.Id;
                                                                    objProducts.Price = ProductIds.Price;
                                                                    objProducts.Quantity = v.Quantity;
                                                                    objProducts.VoucherPercent =
                                                                        Convert.ToInt32(v.VoucherPercent);
                                                                    objProducts.PromotionMoney =
                                                                        Convert.ToInt32(v.Promotion);
                                                                    objProducts.CreatedDate = OBJ.CreatedDate;
                                                                    objProducts.IsDelete = 0;
                                                                    objProducts.ForSalary = 0;
                                                                    objProducts.Cost = 0;
                                                                    objProducts.ModifiedDate = DateTime.Now;
                                                                    objProducts.SellerId = OBJ.SellerId;
                                                                    objProducts.SalonId = OBJ.SalonId;
                                                                    objProducts.CheckCombo = false;
                                                                    objProducts.SalonOrder = orderDesc.Id;
                                                                    db.FlowProducts.AddOrUpdate(objProducts);
                                                                    db.SaveChanges();
                                                                }
                                                            }
                                                        }
                                                    }

                                                    prd.SalonOrder = orderDesc.Id;
                                                    if (v.MapIdProduct == null || v.MapIdProduct == "" ||
                                                        v.MapIdProduct == "undefined")
                                                    {
                                                        prd.CheckCombo = false;
                                                    }
                                                    else
                                                    {
                                                        prd.CheckCombo = true;
                                                    }

                                                    //Static product 
                                                    if (OBJ.CompleteBillTime == null)
                                                    {
                                                        AddStatisticProduct(OBJ, v);
                                                    }
                                                    else
                                                    {
                                                        UpdateStatisticProduct(OBJ, v, prd);
                                                    }

                                                    prd.VoucherPercent = Convert.ToInt32(v.VoucherPercent);
                                                    prd.PromotionMoney = Convert.ToInt32(v.Promotion);
                                                    prd.Quantity = Convert.ToInt32(v.Quantity);
                                                    db.FlowProducts.AddOrUpdate(prd);
                                                    db.SaveChanges();
                                                }
                                            }
                                        }
                                    }

                                    // Delete product
                                    if (ProductListInserted != null)
                                    {
                                        foreach (var v in ProductListInserted)
                                        {
                                            if (ProductList != null)
                                            {
                                                index = ProductList.FindIndex(fi => fi.Id == v.ProductId);
                                                if (index == -1)
                                                {
                                                    var prd = v;
                                                    prd.IsDelete = 1;
                                                    DeleteStatisticProduct(OBJ, v);
                                                    db.FlowProducts.AddOrUpdate(prd);
                                                    db.SaveChanges();
                                                }
                                            }
                                            else
                                            {
                                                var prd = v;
                                                prd.IsDelete = 1;
                                                DeleteStatisticProduct(OBJ, v);
                                                db.FlowProducts.AddOrUpdate(prd);
                                                db.SaveChanges();
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                new Helpers.PushNotiErrorToSlack().Push("GA1LZN9AM", ex.Message + ", Insert to FlowProduct: " + ex.StackTrace + ", " + Library.Function.JavaScript.Serialize(OBJ),
                                                                            this.ToString() + ".checkout", "phucdn",
                                                                            HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host);
                            }

                            try
                            {
                                // Insert to FlowService
                                if (Error == 0)
                                {
                                    var objService = new FlowService();
                                    //var str = "[{\"Id\":\"19\",\"Price\":340000,\"Quantity\":\"1\"}]";
                                    serializer = new JavaScriptSerializer();
                                    var ServiceListInserted = db.FlowServices
                                        .Where(w => w.BillId == OBJ.Id && w.IsDelete != 1).ToList();
                                    var index = -1;
                                    if (OBJ.ServiceIds != "")
                                    {
                                        ServiceList = serializer.Deserialize<List<ProductBasic>>(OBJ.ServiceIds).ToList();
                                        if (ServiceList != null)
                                        {
                                            foreach (var v in ServiceList)
                                            {
                                                if (Error == 0)
                                                {
                                                    index = ServiceListInserted.FindIndex(fi => fi.ServiceId == v.Id);
                                                    if (index == -1)
                                                    {
                                                        //Static service 
                                                        AddStatisticService(OBJ, v);
                                                        //end
                                                        objService = new FlowService();
                                                        objService.BillId = OBJ.Id;
                                                        objService.ServiceId = v.Id;
                                                        objService.Price = v.Price;
                                                        objService.Quantity = v.Quantity;
                                                        objService.VoucherPercent = Convert.ToInt32(v.VoucherPercent);
                                                        var coefficientRating =
                                                            db.Services.FirstOrDefault(w => w.Id == v.Id);
                                                        if (coefficientRating != null &&
                                                            coefficientRating.CoefficientRating != null)
                                                        {
                                                            objService.CoefficientRating =
                                                                coefficientRating.CoefficientRating;
                                                        }
                                                        else
                                                        {
                                                            objService.CoefficientRating = 0;
                                                        }

                                                        objService.CreatedDate = OBJ.CreatedDate;
                                                        objService.IsDelete = 0;
                                                        if (Perm_ShowSalon)
                                                        {
                                                            objService.SalonId =
                                                                int.TryParse(Salon.SelectedValue, out integer)
                                                                    ? integer
                                                                    : 0;
                                                        }
                                                        else
                                                        {
                                                            objService.SalonId =
                                                                int.TryParse(HDF_SalonId.Value, out integer) ? integer : 0;
                                                        }

                                                        objService.SellerId = int.TryParse(Cosmetic.Value, out integer)
                                                            ? integer
                                                            : 0;

                                                        db.FlowServices.Add(objService);
                                                        exc = db.SaveChanges();
                                                        Error = exc > 0 ? Error : ++Error;
                                                    }
                                                    else
                                                    {
                                                        var srv = ServiceListInserted[index];
                                                        //Static service 
                                                        if (IsCheckout)
                                                        {
                                                            AddStatisticService(OBJ, v);
                                                        }
                                                        else
                                                        {
                                                            UpdateStatisticService(OBJ, v, srv);
                                                        }

                                                        srv.VoucherPercent = Convert.ToInt32(v.VoucherPercent);
                                                        srv.Quantity = Convert.ToInt32(v.Quantity);
                                                        db.FlowServices.AddOrUpdate(srv);
                                                        db.SaveChanges();
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if (ServiceListInserted != null)
                                    {
                                        foreach (var v in ServiceListInserted)
                                        {
                                            if (ServiceList != null)
                                            {
                                                index = ServiceList.FindIndex(fi => fi.Id == v.ServiceId);
                                                if (index == -1)
                                                {
                                                    var srv = v;
                                                    //xử lý khi xoá service
                                                    DeleteStatisticService(OBJ, srv);
                                                    srv.IsDelete = 1;
                                                    db.FlowServices.AddOrUpdate(srv);
                                                    db.SaveChanges();
                                                }
                                            }
                                            else
                                            {
                                                var srv = v;
                                                DeleteStatisticService(OBJ, srv);
                                                srv.IsDelete = 1;
                                                db.FlowServices.AddOrUpdate(srv);
                                                db.SaveChanges();
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                new Helpers.PushNotiErrorToSlack().Push("GA1LZN9AM", ex.Message + ", Insert to FlowService: " + ex.StackTrace + ", " + Library.Function.JavaScript.Serialize(OBJ),
                                                                            this.ToString() + ".checkout", "phucdn",
                                                                            HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host);
                            }

                            /// Cập nhật khách quen, số lần khách đã sử dụng dịch vụ vào bill
                            Update_CustomerFamiliar(OBJ);

                            /// Reset giá trị rating temp
                            resetRatingTemp();

                            /// Cập nhật tồn kho
                            //if (OBJ.ProductIds != null && OBJ.ProductIds != "")
                            //{
                            //    Library.Function.updateInventoryByProductExport(db, OBJ.CompleteBillTime.Value.Date,
                            //        OBJ.SalonId.Value, OBJ.ProductIds, 1);
                            //}
                            if (this.isWaitLongTime(OBJ))
                            {
                                /// Cập nhật store lương hôm nay
                                //SalaryLib.updateFlowSalaryByBill(OBJ, db, true);
                                var data = new Input_Checkout
                                {
                                    BillOld = billOld,
                                    StylistOld = billOld.Staff_Hairdresser_Id,
                                    SkinnerOld = billOld.Staff_HairMassage_Id,
                                    ReceptionOld = billOld.ReceptionId,
                                    SellerOld = billOld.SellerId,
                                    BillNew = OBJ,
                                    StylistNew = OBJ.Staff_Hairdresser_Id,
                                    SkinnerNew = OBJ.Staff_HairMassage_Id,
                                    ReceptionNew = OBJ.ReceptionId,
                                    SellerNew = OBJ.SellerId,
                                    isStylistOvertimeOld = callApi.isStylistOvertimeOld,
                                    isSkinnerOvertimeOld = callApi.isSkinnerOvertimeOld,
                                    isStylistOvertimeNew = callApi.isStylistOvertimeNew,
                                    isSkinnerOvertimeNew = callApi.isSkinnerOvertimeNew,
                                    isVoucherLongTime = true,
                                    isDelete = false
                                };
                                try
                                {
                                    var request = new Request();
                                    Task taskStatisSalary = Task.Run(async () =>
                                    {
                                        await request.RunPutAsync(
                                            Libraries.AppConstants.URL_API_FINANCIAL + "/bill-service/check-out",
                                            //PutCheckOut(data).ToString(),
                                            data
                                        );
                                    });
                                }
                                catch (Exception ex)
                                {

                                    new Helpers.PushNotiErrorToSlack().Push("GA1LZN9AM", ex.Message + ", taskStatisSalary: " + ex.StackTrace + ", " + Library.Function.JavaScript.Serialize(data),
                                                                            this.ToString() + ".checkout", "phucdn",
                                                                            HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host);
                                }
                                // Kiểm tra tạo vourcher khách hàng trường hợp thời gian chờ > 20 phút
                                this.createVoucher(OBJ);
                            }
                            else
                            {
                                /// Cập nhật store lương hôm nay
                                //SalaryLib.updateFlowSalaryByBill(OBJ, db);
                                var data = new Input_Checkout
                                {
                                    BillOld = billOld,
                                    StylistOld = billOld.Staff_Hairdresser_Id,
                                    SkinnerOld = billOld.Staff_HairMassage_Id,
                                    ReceptionOld = billOld.ReceptionId,
                                    SellerOld = billOld.SellerId,
                                    BillNew = OBJ,
                                    StylistNew = OBJ.Staff_Hairdresser_Id,
                                    SkinnerNew = OBJ.Staff_HairMassage_Id,
                                    ReceptionNew = OBJ.ReceptionId,
                                    SellerNew = OBJ.SellerId,
                                    isStylistOvertimeOld = callApi.isStylistOvertimeOld,
                                    isSkinnerOvertimeOld = callApi.isSkinnerOvertimeOld,
                                    isStylistOvertimeNew = callApi.isStylistOvertimeNew,
                                    isSkinnerOvertimeNew = callApi.isSkinnerOvertimeNew,
                                    isVoucherLongTime = false,
                                    isDelete = false
                                };
                                try
                                {
                                    var request = new Request();
                                    Task taskStatisSalary = Task.Run(async () =>
                                    {
                                        await request.RunPutAsync(
                                            Libraries.AppConstants.URL_API_FINANCIAL + "/bill-service/check-out",
                                            data
                                        );
                                    });
                                    taskStatisSalary.Wait(300);
                                }
                                catch (Exception ex)
                                {

                                    new Helpers.PushNotiErrorToSlack().Push("GA1LZN9AM", ex.Message + ", taskStatisSalary: " + ex.StackTrace + ", " + Library.Function.JavaScript.Serialize(data),
                                                                            this.ToString() + ".checkout", "phucdn",
                                                                            HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host);
                                }
                            }

                            //==cập nhật  StaticRatingWaittime
                            bool IsUpdate = false;
                            if (OBJStatic.Pending == 0)
                            {
                                IsUpdate = true;
                            }
                            else
                            {
                                IsUpdate = false;
                            }

                            try
                            {
                                //Chay static Bảng StaticRatingWaitTime
                                var request = new Request();
                                Task StaticRatingWaitTime = Task.Run(async () =>
                                {
                                    await request.RunPostAsync(
                                        Libraries.AppConstants.URL_API_STAFF + "/api/static-ranking/add",
                                        new
                                        {
                                            BillId = OBJ.Id,
                                            IsUpdate = IsUpdate,
                                            objBillService = new
                                            {
                                                Id = OBJStatic.Id,
                                                createdDate = OBJStatic.CreatedDate,
                                                serviceIds = OBJStatic.ServiceIds,
                                                bookingId = OBJStatic.BookingId,
                                                mark = OBJStatic.Mark,
                                                isDelete = OBJStatic.IsDelete,
                                                images = OBJStatic.Images,
                                                salonId = OBJStatic.SalonId,
                                                pending = OBJStatic.Pending,
                                                receptionId = OBJStatic.ReceptionId,
                                                completeBillTime = OBJStatic.CompleteBillTime,
                                                inProcedureTime = OBJStatic.InProcedureTime,
                                                uploadImageTime = OBJStatic.UploadImageTime,
                                                staffHairMassageId = OBJStatic.Staff_HairMassage_Id,
                                                staffHairdresserId = OBJStatic.Staff_Hairdresser_Id
                                            }
                                        }
                                    );
                                });
                            }
                            catch (Exception ex)
                            {

                                new Helpers.PushNotiErrorToSlack().Push("GA1LZN9AM", ex.Message + ", " + ex.StackTrace + ", StaticRatingWaitTime: " + Library.Function.JavaScript.Serialize(OBJ),
                                                                        this.ToString() + ".checkout", "phucdn",
                                                                        HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host);
                            }

                            //========================
                            //Chay static Bảng StaticService_Profit
                            try
                            {
                                var request = new Request();
                                Task StaticRatingWaitTime = Task.Run(async () =>
                                {
                                    await request.RunPutAsync(
                                        Libraries.AppConstants.URL_API_STAFF + "/api-static-profit/update-add",
                                        new
                                        {
                                            BillId = OBJStatic.Id,
                                            IsUpdate = IsUpdate,
                                            objBillService = new
                                            {
                                                Id = OBJStatic.Id,
                                                serviceIds = OBJStatic.ServiceIds,
                                                bookingId = OBJStatic.BookingId,
                                                createdDate = OBJStatic.CreatedDate,
                                                mark = OBJStatic.Mark,
                                                isDelete = OBJStatic.IsDelete,
                                                images = OBJStatic.Images,
                                                salonId = OBJStatic.SalonId,
                                                pending = OBJStatic.Pending,
                                                receptionId = OBJStatic.ReceptionId,
                                                completeBillTime = OBJStatic.CompleteBillTime,
                                                inProcedureTime = OBJStatic.InProcedureTime,
                                                uploadImageTime = OBJStatic.UploadImageTime,
                                                staffHairMassageId = OBJStatic.Staff_HairMassage_Id,
                                                staffHairdresserId = OBJStatic.Staff_Hairdresser_Id
                                            }
                                        }
                                    );
                                });
                            }
                            catch (Exception ex)
                            {

                                new Helpers.PushNotiErrorToSlack().Push("GA1LZN9AM", ex.Message + ", StaticRatingWaitTime: " + ex.StackTrace + ", " + Library.Function.JavaScript.Serialize(OBJ),
                                                                        this.ToString() + ".checkout", "phucdn",
                                                                        HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host);
                            }
                            try
                            {
                                var request = new Request();
                                Task taskUpdateStylist = Task.Run(async () =>
                                {
                                    await request.RunPostAsync(
                                        Libraries.AppConstants.URL_API_BOOKING_BETA +
                                        "/api/hourframe/insert-stylist-in-customer",
                                        new
                                        {
                                            Id = OBJ.Id
                                        }
                                    );
                                });
                            }
                            catch (Exception ex)
                            {

                                new Helpers.PushNotiErrorToSlack().Push("GA1LZN9AM", ex.Message + ", taskUpdateStylist: " + ex.StackTrace + ", " + Library.Function.JavaScript.Serialize(OBJ),
                                                                        this.ToString() + ".checkout", "phucdn",
                                                                        HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host);
                            }
                            //=====================================

                            if (Error == 0)
                            {
                                // Thông báo thành công
                                var MsgParam = new List<KeyValuePair<string, string>>();
                                MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                                MsgParam.Add(new KeyValuePair<string, string>("msg_update_message",
                                    "Hoàn tất bill thành công!"));
                                UIHelpers.Redirect("/dich-vu/pending.html", MsgParam);
                            }
                            else
                            {
                                MsgSystem.Text = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                                MsgSystem.CssClass = "msg-system warning";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Push thông báo cho mọi người (by slack) khi có lỗi xảy ra
                new Helpers.PushNotiErrorToSlack().Push("GA1LZN9AM", ex.Message + ", MAIN: " + ex.StackTrace + ", " + Library.Function.JavaScript.Serialize(OBJ),
                                                        this.ToString() + ".checkout", "phucdn",
                                                        HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host);
                // Thông báo thất bại
                var MsgParam = new List<KeyValuePair<string, string>>();
                MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "warning"));
                MsgParam.Add(new KeyValuePair<string, string>("msg_update_message",
                    "Hoàn tất bill thất bại"));
                UIHelpers.Redirect("/dich-vu/pending.html", MsgParam);
            }
        }

        /// <summary>
        /// Add StatisticSalaryProduct
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="product"></param>
        public void AddStatisticProduct(BillService bill, ProductBasic product)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    DateTime workDate = bill.CreatedDate.Value.Date;
                    var statisticRecord = db.StatisticSalaryProducts.FirstOrDefault(w =>
                        w.SalonId == bill.SalonId && w.StaffId == bill.SellerId && w.ProductIds == product.Id &&
                        w.WorkDate == workDate);
                    if (statisticRecord != null)
                    {
                        statisticRecord.Total += product.Quantity;
                        statisticRecord.TotalMoney +=
                            product.Quantity * product.Price * (100 - product.VoucherPercent) / 100;
                        statisticRecord.ModifiedTime = DateTime.Now;
                    }
                    else
                    {
                        statisticRecord = new StatisticSalaryProduct();
                        statisticRecord.SalonId = bill.SalonId;
                        statisticRecord.StaffId = bill.SellerId;
                        statisticRecord.ProductIds = product.Id;
                        statisticRecord.Total = product.Quantity;
                        statisticRecord.TotalMoney =
                            product.Quantity * product.Price * (100 - product.VoucherPercent) / 100;
                        statisticRecord.WorkDate = bill.CreatedDate.Value.Date;
                        statisticRecord.IsDelete = false;
                        statisticRecord.CreatedTime = DateTime.Now;
                    }

                    db.StatisticSalaryProducts.AddOrUpdate(statisticRecord);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                //throw;
            }
        }

        /// <summary>
        /// Update StatisticSalaryProduct
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="product"></param>
        /// <param name="flowProduct"></param>
        public void UpdateStatisticProduct(BillService bill, ProductBasic product, FlowProduct flowProduct)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    DateTime workDate = bill.CreatedDate.Value.Date;
                    var statisticRecord = db.StatisticSalaryProducts.FirstOrDefault(w =>
                        w.SalonId == bill.SalonId && w.StaffId == bill.SellerId && w.ProductIds == product.Id &&
                        w.WorkDate == workDate);
                    if (statisticRecord != null)
                    {
                        //flowProduct = db.FlowProducts.FirstOrDefault(w => w.BillId == OBJ.Id && w.ProductId == flowProduct.ProductId && w.IsDelete == 0);
                        statisticRecord.Total -= flowProduct.Quantity;
                        statisticRecord.TotalMoney -= flowProduct.Quantity * flowProduct.Price *
                                                      (100 - flowProduct.VoucherPercent) / 100;
                        statisticRecord.Total += product.Quantity;
                        statisticRecord.TotalMoney +=
                            product.Quantity * product.Price * (100 - product.VoucherPercent) / 100;
                        statisticRecord.ModifiedTime = DateTime.Now;
                    }
                    else
                    {
                        statisticRecord = new StatisticSalaryProduct();
                        statisticRecord.SalonId = bill.SalonId;
                        statisticRecord.StaffId = bill.SellerId;
                        statisticRecord.ProductIds = product.Id;
                        statisticRecord.Total = product.Quantity;
                        statisticRecord.TotalMoney =
                            product.Quantity * product.Price * (100 - product.VoucherPercent) / 100;
                        statisticRecord.WorkDate = bill.CreatedDate.Value.Date;
                        statisticRecord.IsDelete = false;
                        statisticRecord.CreatedTime = DateTime.Now;
                    }

                    db.StatisticSalaryProducts.AddOrUpdate(statisticRecord);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                //throw;
            }
        }

        /// <summary>
        /// Delete StatisticSalaryProduct
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="flowProduct"></param>
        public void DeleteStatisticProduct(BillService bill, FlowProduct flowProduct)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    DateTime workDate = bill.CreatedDate.Value.Date;
                    var statisticRecord = db.StatisticSalaryProducts.FirstOrDefault(w =>
                        w.SalonId == bill.SalonId && w.StaffId == bill.SellerId &&
                        w.ProductIds == flowProduct.ProductId && w.WorkDate == workDate);
                    if (statisticRecord != null)
                    {
                        statisticRecord.Total -= flowProduct.Quantity;
                        statisticRecord.TotalMoney -= flowProduct.Quantity * flowProduct.Price *
                                                      (100 - flowProduct.VoucherPercent) / 100;
                        statisticRecord.ModifiedTime = DateTime.Now;
                        db.StatisticSalaryProducts.AddOrUpdate(statisticRecord);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                //throw;
            }
        }

        /// <summary>
        /// Add StatisticSalarySevice
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="service"></param>
        public void AddStatisticService(BillService bill, ProductBasic service)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    if (bill.Staff_Hairdresser_Id != null && bill.Staff_Hairdresser_Id > 0)
                    {
                        DateTime workDate = bill.CreatedDate.Value.Date;
                        var statisticRecord = db.StatisticSalaryServices.FirstOrDefault(w =>
                            w.SalonId == bill.SalonId && w.StaffId == bill.Staff_Hairdresser_Id &&
                            w.ServiceIds == service.Id && w.WorkDate == workDate);
                        if (statisticRecord != null)
                        {
                            statisticRecord.Total += service.Quantity;
                            statisticRecord.TotalMoney +=
                                service.Quantity * service.Price * (100 - service.VoucherPercent) / 100;
                            statisticRecord.ModifiedTime = DateTime.Now;
                        }
                        else
                        {
                            statisticRecord = new StatisticSalaryService();
                            statisticRecord.SalonId = bill.SalonId;
                            statisticRecord.StaffId = bill.Staff_Hairdresser_Id;
                            statisticRecord.ServiceIds = service.Id;
                            statisticRecord.Total = service.Quantity;
                            statisticRecord.TotalMoney =
                                service.Quantity * service.Price * (100 - service.VoucherPercent) / 100;
                            statisticRecord.WorkDate = bill.CreatedDate.Value.Date;
                            statisticRecord.IsDelete = false;
                            statisticRecord.CreatedTime = DateTime.Now;
                        }

                        db.StatisticSalaryServices.AddOrUpdate(statisticRecord);
                        db.SaveChanges();
                    }

                    if (bill.Staff_HairMassage_Id != null && bill.Staff_HairMassage_Id > 0)
                    {
                        DateTime workDate = bill.CreatedDate.Value.Date;
                        var statisticRecord = db.StatisticSalaryServices.FirstOrDefault(w =>
                            w.SalonId == bill.SalonId && w.StaffId == bill.Staff_HairMassage_Id &&
                            w.ServiceIds == service.Id && w.WorkDate == workDate);
                        if (statisticRecord != null)
                        {
                            statisticRecord.Total += service.Quantity;
                            statisticRecord.TotalMoney +=
                                service.Quantity * service.Price * (100 - service.VoucherPercent) / 100;
                            statisticRecord.ModifiedTime = DateTime.Now;
                        }
                        else
                        {
                            statisticRecord = new StatisticSalaryService();
                            statisticRecord.SalonId = bill.SalonId;
                            statisticRecord.StaffId = bill.Staff_HairMassage_Id;
                            statisticRecord.ServiceIds = service.Id;
                            statisticRecord.Total = service.Quantity;
                            statisticRecord.TotalMoney =
                                service.Quantity * service.Price * (100 - service.VoucherPercent) / 100;
                            statisticRecord.WorkDate = bill.CreatedDate.Value.Date;
                            statisticRecord.IsDelete = false;
                            statisticRecord.CreatedTime = DateTime.Now;
                        }

                        db.StatisticSalaryServices.AddOrUpdate(statisticRecord);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                //throw;
            }
        }

        /// <summary>
        /// Update StatisticSalarySevice
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="service"></param>
        /// <param name="flowService"></param>
        public void UpdateStatisticService(BillService bill, ProductBasic service, FlowService flowService)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    if (bill.Staff_Hairdresser_Id != null && bill.Staff_Hairdresser_Id > 0)
                    {
                        DateTime workDate = bill.CreatedDate.Value.Date;
                        var statisticRecord = db.StatisticSalaryServices.FirstOrDefault(w =>
                            w.SalonId == bill.SalonId && w.StaffId == bill.Staff_Hairdresser_Id &&
                            w.ServiceIds == service.Id && w.WorkDate == workDate);
                        if (statisticRecord != null)
                        {
                            //flowService = db.FlowServices.FirstOrDefault(w => w.BillId == OBJ.Id && w.ServiceId == flowService.ServiceId && flowService.IsDelete == 0);
                            statisticRecord.Total -= flowService.Quantity;
                            statisticRecord.TotalMoney -=
                                flowService.Quantity * flowService.Price * (100 - flowService.VoucherPercent) / 100;
                            statisticRecord.Total += service.Quantity;
                            statisticRecord.TotalMoney +=
                                service.Quantity * service.Price * (100 - service.VoucherPercent) / 100;
                            statisticRecord.ModifiedTime = DateTime.Now;
                        }
                        else
                        {
                            statisticRecord = new StatisticSalaryService();
                            statisticRecord.SalonId = bill.SalonId;
                            statisticRecord.StaffId = bill.Staff_Hairdresser_Id;
                            statisticRecord.ServiceIds = service.Id;
                            statisticRecord.Total = service.Quantity;
                            statisticRecord.TotalMoney =
                                service.Quantity * service.Price * (100 - service.VoucherPercent) / 100;
                            statisticRecord.WorkDate = bill.CreatedDate.Value.Date;
                            statisticRecord.IsDelete = false;
                            statisticRecord.CreatedTime = DateTime.Now;
                        }

                        db.StatisticSalaryServices.AddOrUpdate(statisticRecord);
                        db.SaveChanges();
                    }

                    if (bill.Staff_HairMassage_Id != null && bill.Staff_HairMassage_Id > 0)
                    {
                        DateTime workDate = bill.CreatedDate.Value.Date;
                        var statisticRecord = db.StatisticSalaryServices.FirstOrDefault(w =>
                            w.SalonId == bill.SalonId && w.StaffId == bill.Staff_HairMassage_Id &&
                            w.ServiceIds == service.Id && w.WorkDate == workDate);
                        if (statisticRecord != null)
                        {
                            statisticRecord.Total -= flowService.Quantity;
                            statisticRecord.TotalMoney -=
                                flowService.Quantity * flowService.Price * (100 - flowService.VoucherPercent) / 100;
                            statisticRecord.Total += service.Quantity;
                            statisticRecord.TotalMoney +=
                                service.Quantity * service.Price * (100 - service.VoucherPercent) / 100;
                            statisticRecord.ModifiedTime = DateTime.Now;
                        }
                        else
                        {
                            statisticRecord = new StatisticSalaryService();
                            statisticRecord.SalonId = bill.SalonId;
                            statisticRecord.StaffId = bill.Staff_Hairdresser_Id;
                            statisticRecord.ServiceIds = service.Id;
                            statisticRecord.Total = service.Quantity;
                            statisticRecord.TotalMoney =
                                service.Quantity * service.Price * (100 - service.VoucherPercent) / 100;
                            statisticRecord.WorkDate = bill.CreatedDate.Value.Date;
                            statisticRecord.IsDelete = false;
                            statisticRecord.CreatedTime = DateTime.Now;
                        }

                        db.StatisticSalaryServices.AddOrUpdate(statisticRecord);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                //throw;
            }
        }

        /// <summary>
        /// Delete StatisticSalarySevice
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="flowService"></param>
        public void DeleteStatisticService(BillService bill, FlowService flowService)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    if (bill.Staff_Hairdresser_Id != null && bill.Staff_Hairdresser_Id > 0)
                    {
                        DateTime workDate = bill.CreatedDate.Value.Date;
                        var statisticRecord = db.StatisticSalaryServices.FirstOrDefault(w =>
                            w.SalonId == bill.SalonId && w.StaffId == bill.Staff_Hairdresser_Id &&
                            w.ServiceIds == flowService.ServiceId && w.WorkDate == workDate);
                        if (statisticRecord != null)
                        {
                            statisticRecord.Total -= flowService.Quantity;
                            statisticRecord.TotalMoney -=
                                flowService.Quantity * flowService.Price * (100 - flowService.VoucherPercent) / 100;
                            statisticRecord.ModifiedTime = DateTime.Now;
                            db.StatisticSalaryServices.AddOrUpdate(statisticRecord);
                            db.SaveChanges();
                        }
                    }

                    if (bill.Staff_HairMassage_Id != null && bill.Staff_HairMassage_Id > 0)
                    {
                        DateTime workDate = bill.CreatedDate.Value.Date;
                        var statisticRecord = db.StatisticSalaryServices.FirstOrDefault(w =>
                            w.SalonId == bill.SalonId && w.StaffId == bill.Staff_HairMassage_Id &&
                            w.ServiceIds == flowService.ServiceId && w.WorkDate == workDate);
                        if (statisticRecord != null)
                        {
                            statisticRecord.Total -= flowService.Quantity;
                            statisticRecord.TotalMoney -=
                                flowService.Quantity * flowService.Price * (100 - flowService.VoucherPercent) / 100;
                            statisticRecord.ModifiedTime = DateTime.Now;
                            db.StatisticSalaryServices.AddOrUpdate(statisticRecord);
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //throw;
            }
        }


        public void UpdateStaticProduct(StatisticSalaryProduct StaticProduct)
        {
            try
            {
                StaticProduct.TotalMoney += staticProduct.TotalMoney + TotalMoneyStatic;
                StaticProduct.Total += staticProduct.Total + TotalStatic;
            }
            catch
            {
                throw new Exception();
            }
        }


        /// <summary>
        /// Update xuất vật tư cho Stylist + Skinner khi tạo bill mới Update to table Statistics_XuatVatTu
        /// </summary>
        /// <param name="Staff_Old_ID"></param>
        /// <param name="Staff_New_ID"></param>
        /// <param name="ComPleteTime"></param>
        protected void StatisticVatTuSubmitBillNew(int Staff_ID, DateTime? ComPleteTime)
        {
            try
            {
                // lay  obj Xuat vat tu gan nhat
                var ObjExportGoods = db.ExportGoods.Where(a => a.IsDelete == 0 && a.RecipientId == Staff_ID)
                    .OrderByDescending(a => a.CreatedDate).OrderByDescending(a => a.Id).Select(a => new { a.Id })
                    .FirstOrDefault();
                // Lay danh sach Tatistic Xuat vat tu
                if (ObjExportGoods != null)
                {
                    var LstSatisticXuatVatTu = db.Statistics_XuatVatTu.Where(a => a.IsDelete == false
                                                                                  && a.ExportGoods_ID ==
                                                                                  ObjExportGoods.Id
                                                                                  && a.Date_XuatVatTu < ComPleteTime)
                        .ToList();
                    var _CountLST = LstSatisticXuatVatTu.Count;
                    if (_CountLST > 0)
                    {
                        for (int i = 0; i < _CountLST; i++)
                        {
                            LstSatisticXuatVatTu[i].TotalBill_XuatVatTu =
                                LstSatisticXuatVatTu[i].TotalBill_XuatVatTu + 1;
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Update xuất vật tư cho Stylist + Skinner insert to table Statistics_XuatVatTu
        /// </summary>
        /// <param name="Staff_Old_ID">Nhân viên cũ</param>
        /// <param name="Staff_New_ID">Nhân viên mới</param>
        /// <param name="ComPleteTime"></param>
        protected void StatisticVatTuUpdateBill(int? Staff_Old_ID, int? Staff_New_ID, DateTime? ComPleteTime)
        {
            try
            {
                if (Staff_Old_ID != Staff_New_ID)
                {
                    // lay  obj Xuat vat tu gan nhat nhan vien cu
                    var ObjExportGoodsOld = db.ExportGoods.Where(a => a.IsDelete == 0 && a.RecipientId == Staff_Old_ID)
                        .OrderByDescending(a => a.CreatedDate).OrderByDescending(a => a.Id).Select(a => new { a.Id })
                        .FirstOrDefault();
                    // Lay danh sach Tatistic Xuat vat tu
                    if (ObjExportGoodsOld != null)
                    {
                        var LstSatisticXuatVatTuOld = db.Statistics_XuatVatTu.Where(a => a.IsDelete == false &&
                                                                                         a.ExportGoods_ID ==
                                                                                         ObjExportGoodsOld.Id &&
                                                                                         a.Date_XuatVatTu <
                                                                                         ComPleteTime).ToList();
                        var _CountLSTOld = LstSatisticXuatVatTuOld.Count;
                        if (_CountLSTOld > 0)
                        {
                            for (int i = 0; i < _CountLSTOld; i++)
                            {
                                LstSatisticXuatVatTuOld[i].TotalBill_XuatVatTu =
                                    LstSatisticXuatVatTuOld[i].TotalBill_XuatVatTu - 1;
                                db.SaveChanges();
                            }
                        }
                    }

                    // lay  obj Xuat vat tu gan nhat nhan vien moi
                    var ObjExportGoodsNew = db.ExportGoods.Where(a => a.IsDelete == 0 && a.RecipientId == Staff_New_ID)
                        .OrderByDescending(a => a.CreatedDate).OrderByDescending(a => a.Id).Select(a => new { a.Id })
                        .FirstOrDefault();
                    // Lay danh sach Statistic Xuat vat tu
                    if (ObjExportGoodsNew != null)
                    {
                        var LstSatisticXuatVatTuNew = db.Statistics_XuatVatTu.Where(a => a.IsDelete == false
                                                                                         && a.ExportGoods_ID ==
                                                                                         ObjExportGoodsNew.Id
                                                                                         && a.Date_XuatVatTu <
                                                                                         ComPleteTime).ToList();
                        var _CountLSTNew = LstSatisticXuatVatTuNew.Count;
                        if (_CountLSTNew > 0)
                        {
                            for (int i = 0; i < _CountLSTNew; i++)
                            {
                                LstSatisticXuatVatTuNew[i].TotalBill_XuatVatTu =
                                    LstSatisticXuatVatTuNew[i].TotalBill_XuatVatTu + 1;
                                db.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Cập nhật số lượng sử dụng dịch vụ miễn phí cho khách đặc biệt
        /// </summary>
        public void updateQuantityInvited()
        {
            var db = new Solution_30shineEntities();

            // Lấy Id trong bảng khách hàng đặc biệt
            int? CusId = Convert.ToInt32(HDF_CustomerId.Value);
            var objSpecialCustomer = db.SpecialCustomers
                .Where(a => a.IsDelete == false && a.CustomerTypeId == 1 && a.CustomerId == CusId).OrderBy(a => a.Id)
                .FirstOrDefault();
            //Khai báo SpecialCusDetail
            var obj = new SpecialCusDetail();
            if (objSpecialCustomer != null)
            {
                int IdSpecialCustomer = Convert.ToInt32(objSpecialCustomer.Id);
                obj = db.SpecialCusDetails
                    .Where(a => a.IsDelete == false && a.QuantityInvited > 0 && a.SpecialCusId == IdSpecialCustomer)
                    .OrderBy(a => a.Id).FirstOrDefault();
            }

            if (obj != null)
            {
                if (obj.QuantityInvited > 0)
                {
                    obj.QuantityInvited = obj.QuantityInvited - 1;
                    db.SpecialCusDetails.AddOrUpdate(obj);
                    db.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Cập nhật khách hàng quen
        /// </summary>
        /// <param name="bill"></param>
        private void Update_CustomerFamiliar(BillService bill)
        {
            using (var db = new Solution_30shineEntities())
            {
                bool isSpecial = false;
                /// 1. Tính số lần đã sử dụng dịch vụ
                string sql = @"select COUNT(*) as times
                                from BillService
                                where IsDelete != 1 and Pending != 1
                                and ServiceIds != '' and ServiceIds is not null
                                and CreatedDate < '" + bill.CreatedDate + "'" +
                             @"and CustomerCode = '" + bill.CustomerCode + "'" +
                             @"group by CustomerCode";

                int timesUsedService = 0;
                try
                {
                    timesUsedService = db.Database.SqlQuery<int>(sql).FirstOrDefault();
                    bill.TimesUsedService = timesUsedService;
                }
                catch
                {
                }

                /// 2. Kiểm tra có phải khách quen (là khách đã dùng dịch vụ bởi stylist này ít nhất 2 lần, hoặc 1 lần nhưng là lần gần đây nhất)
                sql = @"select *
                                from BillService
                                where IsDelete != 1 and Pending != 1
                                and CustomerCode = '" + bill.CustomerCode + @"' 
                                and CustomerCode != '' and CustomerCode is not null
                                and CreatedDate < '" + String.Format("{0:yyyy/MM/dd}", bill.CreatedDate) + "'";
                var billCus = db.BillServices.SqlQuery(sql).ToList();
                var familiar = billCus.Where(w => w.Staff_Hairdresser_Id == bill.Staff_Hairdresser_Id).ToList();
                // Nếu khách đã sử dụng dịch vụ bởi stylist này ít nhất 2 lần
                if (familiar.Count > 1)
                {
                    isSpecial = true;
                }
                // hoặc dùng 1 lần nhưng là lần gần đây nhất
                else if (familiar.Count == 1)
                {
                    if (billCus[billCus.Count - 1].Staff_Hairdresser_Id == bill.Staff_Hairdresser_Id)
                    {
                        isSpecial = true;
                    }
                }

                // Cập nhật bill
                bill.IsCusFamiliar = isSpecial;
                db.BillServices.AddOrUpdate(bill);
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Bind danh sách sản phẩm
        /// </summary>
        private void Bind_RptProduct()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Products = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1).OrderByDescending(o => o.Id)
                    .ToList();
                _ListProduct = _Products;
                var serializer = new JavaScriptSerializer();
                _ListProductJsonString = serializer.Serialize(_ListProduct);
            }
        }

        /// <summary>
        /// Bind danh sách dịch vụ
        /// </summary>
        private void Bind_RptService()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Service = db.Services.Where(w => w.IsDelete != 1 && w.Publish == 1).OrderByDescending(o => o.Id)
                    .ToList();

                Rpt_Service.DataSource = _Service;
                Rpt_Service.DataBind();
            }
        }

        /// <summary>
        /// Bind danh sách dịch vụ phụ trợ như Cạo mặt, Cắt móng tay
        /// </summary>
        private void Bind_RptFreeService()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Service = db.Services.Where(w => w.IsDelete != 1 && w.Publish == 1 && w.IsFreeService == 1)
                    .OrderBy(o => o.Id).ToList();
                int index = -1;
                if (_Service.Count > 0 && lstFreeService.Count > 0)
                {
                    foreach (var v in _Service)
                    {
                        index = lstFreeService.IndexOf(v.Id);
                        if (index != -1)
                        {
                            _Service.FirstOrDefault(w => w.Id == lstFreeService[index]).Status = 1;
                        }
                    }
                }

                Rpt_FreeService.DataSource = _Service;
                Rpt_FreeService.DataBind();
            }
        }

        /// <summary>
        /// Bind danh sách dịch vụ nổi bật
        /// </summary>
        private void Bind_RptServiceFeatured()
        {
            using (var db = new Solution_30shineEntities())
            {
                var MatchInt = Regex.Match(Request.QueryString["Code"], @"[^0-9]+");
                if (!MatchInt.Success)
                {
                    var _Code = Convert.ToInt32(Request.QueryString["Code"]);
                    OBJ = db.BillServices.FirstOrDefault(w => w.Id == _Code);
                }

                var lst = db.Salon_Service_Select(OBJ.SalonId, 0).Where(p => p.IsCheck == true).ToList();
                //var lst = db.Services.Where(w => w.IsDelete != 1 && w.Publish == 1 && w.Status == 1).OrderByDescending(o => o.Id).ToList();

                Rpt_ServiceFeatured.DataSource = lst;
                Rpt_ServiceFeatured.DataBind();
            }
        }

        /// <summary>
        /// Bind danh sách sản phẩm nổi bật
        /// </summary>
        private void Bind_RptProductFeatured()
        {
            using (var db = new Solution_30shineEntities())
            {
                // Combo : ID 95
                int cateId = 95;
                var lst = db.Products
                    .Where(w => w.IsDelete != 1 && w.Publish == 1 && w.Status == 1 && w.CategoryId == cateId)
                    .OrderByDescending(o => o.Id).ToList();
                Rpt_ProductFeatured_Combo.DataSource = lst;
                Rpt_ProductFeatured_Combo.DataBind();

                // Gôm : ID 6
                cateId = 6;
                lst = db.Products
                    .Where(w => w.IsDelete != 1 && w.Publish == 1 && w.Status == 1 && w.CategoryId == cateId)
                    .OrderByDescending(o => o.Id).ToList();
                Rpt_ProductFeatured_Gom.DataSource = lst;
                Rpt_ProductFeatured_Gom.DataBind();

                // Sáp : ID 7
                cateId = 7;
                lst = db.Products
                    .Where(w => w.IsDelete != 1 && w.Publish == 1 && w.Status == 1 && w.CategoryId == cateId)
                    .OrderByDescending(o => o.Id).ToList();
                Rpt_ProductFeatured_Sap.DataSource = lst;
                Rpt_ProductFeatured_Sap.DataBind();
            }
        }

        /// <summary>
        /// Reset Rating Temp Value
        /// </summary>
        private void resetRatingTemp()
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                /// Reset giá trị rating temp
                int accId = Session["User_Id"] != null
                    ? int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0
                    : 0;
                int salonId = Session["SalonId"] != null
                    ? int.TryParse(Session["SalonId"].ToString(), out integer) ? integer : 0
                    : 0;
                var ratingTemp = db.Rating_Temp.FirstOrDefault(w => w.SalonId == salonId && w.AccountId == accId);
                if (ratingTemp != null)
                {
                    ratingTemp.RatingValue = 0;
                    db.Rating_Temp.AddOrUpdate(ratingTemp);
                    db.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Lấy thông tin khách hàng qua mã KH
        /// </summary>
        /// <param name="CustomerCode"></param>
        /// <returns></returns>
        [WebMethod]
        public static string GetCustomer(string CustomerCode)
        {
            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var _Customer = db.Customers.Where(w => w.Customer_Code == CustomerCode).ToList();

                if (_Customer.Count > 0)
                {
                    _Msg.success = true;
                    _Msg.msg = serializer.Serialize(_Customer);
                }
                else
                {
                    _Msg.success = false;
                }
            }

            return serializer.Serialize(_Msg);
        }

        /// <summary>
        /// Kiểm tra đã tạo bill cho khách hàng trong hôm nay qua mã KH
        /// </summary>
        /// <param name="CustomerCode"></param>
        /// <returns></returns>
        [WebMethod]
        public static string CheckDuplicateBill(string CustomerCode)
        {
            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var today = Convert.ToDateTime(DateTime.Now.ToString("d/M/yyyy"), culture);
                var todayL = today.AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                var todayR = today.AddDays(1);
                var _Bill = db.BillServices.FirstOrDefault(w =>
                    w.CreatedDate > todayL && w.CreatedDate < todayR && w.CustomerCode == CustomerCode);

                if (_Bill != null)
                {
                    _Msg.success = true;
                    _Msg.msg = serializer.Serialize(_Bill);
                }
                else
                {
                    _Msg.success = false;
                }
            }

            return serializer.Serialize(_Msg);
        }

        /// <summary>
        /// Gen VIP Card - VD : 0001, 9999 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="template"></param>
        /// <returns></returns>
        private static string genVIPCard(string input, int template)
        {
            string code = "";
            int len = input.Length;
            for (var i = 0; i < template; i++)
            {
                if (len > 0)
                {
                    code = input[--len].ToString() + code;
                }
                else
                {
                    code = "0" + code;
                }
            }

            return code;
        }

        /// <summary>
        /// Kiểm tra đã tồn tại thẻ VIP Card hay chưa
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [WebMethod]
        public static string issetVIPCard(string input)
        {
            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                input = genVIPCard(input, 4);
                var card = db.BillServices.FirstOrDefault(w => w.VIPCardGive == input && w.IsDelete != 1);

                if (card != null)
                {
                    _Msg.success = true;
                    //_Msg.msg = serializer.Serialize(_Bill);
                }
                else
                {
                    _Msg.success = false;
                }
            }

            return serializer.Serialize(_Msg);
        }

        ///// <summary>
        ///// Check permission
        ///// </summary>
        //private void CheckPermission()
        //{
        //    if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
        //    {
        //        var MsgParam = new List<KeyValuePair<string, string>>();
        //        UIHelpers.Redirect("/dang-nhap.html", MsgParam);
        //    }
        //    else
        //    {
        //        string[] Allow = new string[] { "root", "admin", "salonmanager", "reception", "checkin", "checkout" };
        //        string[] AllowSalon = new string[] { "root", "admin" };
        //        var Permission = Session["User_Permission"].ToString().Trim();
        //        if (Array.IndexOf(Allow, Permission) != -1)
        //        {
        //            Perm_Access = true;
        //        }
        //        if (Array.IndexOf(AllowSalon, Permission) != -1)
        //        {
        //            Perm_ShowSalon = true;
        //        }
        //    }

        //    // Call execute function
        //    ExecuteByPermission();
        //}

        ///// <summary>
        ///// Xử lý hậu check permission
        ///// </summary>
        //private void ExecuteByPermission()
        //{
        //    if (!Perm_Access)
        //    {
        //        ContentWrap.Visible = false;
        //        NotAllowAccess.Visible = true;
        //    }
        //    else if (Perm_ShowSalon)
        //    {
        //        TrSalon.Visible = true;
        //        //Bind_Salon();
        //    }
        //}

        /// <summary>
        /// Lấy dữ liệu khách đặc biệt theo số điện thoại
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        [WebMethod]
        public static SpecialCustomers GetSpecialCustomer(string phone)
        {
            var db = new Solution_30shineEntities();
            var query =
                @"select A.CustomerTypeId from SpecialCustomer A left join Customer B on A.CustomerId = B.Id where B.Phone like N'%" +
                phone + "%' and A.IsDelete != 1 and B.IsDelete != 1";
            int customerType = db.Database.SqlQuery<int>(query).FirstOrDefault();
            var sql = @"select A.*, C.TypeName, B.CustomerTypeId from SpecialCusDetail A 
                        left join SpecialCustomer B on A.SpecialCusId = B.Id
                        left join CustomerType C on C.Id = B.CustomerTypeId
						left join Customer D on D.Id = B.CustomerId
                        where A.IsDelete = 0 and D.Phone like N'%" + phone + "%'";
            if (customerType == 1)
            {
                sql += @" and A.QuantityInvited > 0
                        and A.CutErrorDate = (select Min(A.CutErrorDate) from SpecialCusDetail A 
						left join SpecialCustomer B on A.SpecialCusId = B.Id
						left join Customer D on D.Id = B.CustomerId
						where  D.Phone like N'%" + phone + "%' and A.QuantityInvited > 0 and A.IsDelete = 0)";
            }

            return db.Database.SqlQuery<SpecialCustomers>(sql).SingleOrDefault();
        }

        /// <summary>
        /// Hàm xử lý pushNotification cho Apprating theo phương thức async.
        /// </summary>
        /// <returns></returns>
        private async Task PushNotificationApprating(int BillId)
        {
            try
            {
                string myJson = "{'BillId':" + BillId + "}";
                using (var client = new HttpClient())
                {
                    var response = await client.PostAsync(
                        AppConstants.URL_API_LIVESITE + "/apprating/PushNotificationRating",
                        new StringContent(myJson, Encoding.UTF8, "application/json"));
                }
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }
        }

        private async Task PushNotiRatingClose(int BillId)
        {
            try
            {
                string myJson = "{'BillId':" + BillId + "}";
                using (var client = new HttpClient())
                {
                    var response = await client.PostAsync(
                        AppConstants.URL_API_LIVESITE + "/apprating/PushNotiRatingClose",
                        new StringContent(myJson, Encoding.UTF8, "application/json"));
                }
            }
            catch (Exception ex)
            {
                WriteLog(ex.ToString());
            }
        }

        /// <summary>
        /// Kiểm tra bill hoàn thành hay chưa, chưa trả về false, rồi trả về true,
        /// </summary>
        /// <param name="BillId"></param>
        /// <returns></returns>
        private bool IsCompleteBill(int BillId)
        {
            bool check = false;
            using (var db = new Solution_30shineEntities())
            {
                try
                {
                    var data = (from c in db.BillServices
                                where c.Id == BillId
                                select new
                                {
                                    Pending = c.Pending
                                }).FirstOrDefault();
                    check = data == null ? false : (data.Pending == 1 ? false : true);
                }
                catch (Exception)
                {
                    throw;
                }
            }

            return check;
        }

        /// <summary>
        /// Kiểm tra xem nhân viên đã nhấn hoàn thành bill hay chưa
        /// </summary>
        /// <param name="SalonId"></param>
        /// <returns></returns>
        private bool IsCheckout(int BillId)
        {
            bool check = false;
            //int? SalonId = GetBillId(BillId) == null ? 0 : GetBillId(BillId).SalonId;
            using (var db = new Solution_30shineEntities())
            {
                try
                {
                    var data = db.RatingIsChecks
                        .Where(o => o.SalonId == SalonIdBill && o.CreatedTime >= today && o.CreatedTime < tomorrow)
                        .FirstOrDefault();
                    check = data == null ? true : (data.BillId == BillId ? true : false);
                }
                catch (Exception)
                {

                    throw;
                }
            }

            return check;
        }

        private bool EnableCheckOut(int BillId)
        {
            bool check = true;
            check = IsCompleteBill(BillId) ? true : ((IsCheckout(BillId)) ? true : false);

            return check;
        }

        /// <summary>
        /// Thêm bản ghi RatingIscheck khi mở trang check out.
        /// </summary>
        /// <param name="Bill"></param>
        private void AddRatingIsCheck(int BillId)
        {
            var DataRating = new RatingIsCheck();
            try
            {
                var data = GetBillId(BillId);
                if (data != null)
                {
                    var checkRating = GetCNameRating(BillId);
                    if (checkRating == null)
                    {
                        DataRating.BillId = BillId;
                        DataRating.SalonId = data.SalonId;
                        DataRating.CustomerId = data.CustomerId;
                        DataRating.CreatedTime = DateTime.Now;
                        DataRating.IsDelete = false;

                        db.RatingIsChecks.Add(DataRating);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Xóa bản ghi RatingIscheck khi hoàn tất bill,
        /// </summary>
        /// <param name="BillId"></param>
        private void DeleteRatingIscheck(int BillId)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var DelData = db.RatingIsChecks.Where(o => o.SalonId == BillId).FirstOrDefault();
                    if (DelData != null)
                    {
                        db.RatingIsChecks.Remove(DelData);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Lấy thông tin bill đang mở Checkout và chưa hoàn tất bill tại salon đó
        /// </summary>
        /// <param name="SalonId"></param>
        /// <returns></returns>
        private RatingIsCheckObj GetCNameRating(int BillId)
        {
            var RatingCheck = new RatingIsCheckObj();
            //int? SalonId = GetBillId(BillId) == null ? 0 : GetBillId(BillId).SalonId;
            using (var db = new Solution_30shineEntities())
            {
                try
                {
                    //RatingCheck = db.RatingIsChecks.Where(o => o.SalonId == SalonId).FirstOrDefault();                    
                    RatingCheck = (from a in db.RatingIsChecks
                                   join b in db.Customers on a.CustomerId equals b.Id
                                   where a.SalonId == SalonIdBill && a.CreatedTime >= today && a.CreatedTime < tomorrow
                                   select new RatingIsCheckObj
                                   {
                                       CustomerName = b.Fullname,
                                       BillId = a.BillId,
                                       Phone = b.Phone
                                   }).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    throw ex;
                    // throw;
                }
            }

            return RatingCheck;
        }

        /// <summary>
        /// Kiểm tra stylist,skinner có trong giờ tăng ca hay không.
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public bool checkOverTime(int? BookingId, int staffId, DateTime WorkDate)
        {
            bool check = false;
            if (BookingId != null)
            {
                var WorkTime = db.FlowTimeKeepings.Where(r => r.IsDelete == 0 && r.IsEnroll == true && r.WorkDate == WorkDate.Date && r.StaffId == staffId).FirstOrDefault();
                if (WorkTime != null)
                {
                    //Lay khung gio lam viec
                    int WorkTimeId = WorkTime.WorkTimeId.Value;
                    //Lay so gio tang ca
                    int WorkHour = WorkTime.WorkHour.Value;
                    //Lay chuoi gio tang ca
                    string[] strHourIds = WorkTime.HourIds.Split(',');
                    int[] intHourIds = new int[strHourIds.Length];
                    for (int i = 0; i < strHourIds.Length; i++)
                    {
                        intHourIds[i] = strHourIds[i] != "" ? Convert.ToInt32(strHourIds[i]) : 0;
                    }
                    // kiem tra chuoi id gio, co gio tang ca hay khong
                    var BookHourFrame = (
                                               from b in db.Bookings
                                               join a in db.BookHours on b.HourId equals a.Id
                                               where a.IsDelete == 0 && a.Publish == true && b.IsDelete == 0 && b.Id == BookingId.Value
                                               select new
                                               {
                                                   HourFrame = a.HourFrame,
                                                   HourId = a.Id
                                               }).FirstOrDefault();


                    if (BookHourFrame != null)
                    {
                        var WorkTimeHour = db.WorkTimes.Where(r => r.Id == WorkTimeId).FirstOrDefault();
                        if (WorkTimeHour != null)
                        {
                            if (BookHourFrame.HourFrame < WorkTimeHour.StrartTime || BookHourFrame.HourFrame > WorkTimeHour.EnadTime)
                            {
                                check = Array.IndexOf(intHourIds, BookHourFrame.HourId) != -1 ? true : false;
                            }
                        }
                    }
                }
            }
            return check;
        }

        /// <summary>
        /// Lấy ra thông tin của Bill
        /// </summary>
        /// <param name="BillId"></param>
        /// <returns></returns>
        private BillService GetBillId(int BillId)
        {
            var Bill = new BillService();
            using (var db = new Solution_30shineEntities())
            {
                try
                {
                    Bill = db.BillServices.Where(o => o.Id == BillId).FirstOrDefault();
                }
                catch (Exception)
                {
                    //throw;
                }
            }

            return Bill;
        }

        private static void WriteLog(string strLog)
        {
            StreamWriter log;
            FileStream fileStream = null;
            DirectoryInfo logDirInfo = null;
            FileInfo logFileInfo;
            string dirPath = HttpContext.Current.Server.MapPath("~");

            string logFilePath = dirPath + "Logs\\";
            logFilePath = logFilePath + "Log-" + System.DateTime.Today.ToString("MM-dd-yyyy") + "." + "txt";
            logFileInfo = new FileInfo(logFilePath);
            logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
            if (!logDirInfo.Exists) logDirInfo.Create();
            if (!logFileInfo.Exists)
            {
                fileStream = logFileInfo.Create();
            }
            else
            {
                fileStream = new FileStream(logFilePath, FileMode.Append);
            }

            log = new StreamWriter(fileStream);
            log.WriteLine(strLog);
            log.Close();
        }

        public class RatingIsCheckObj
        {
            public string CustomerName { get; set; }
            public int? BillId { get; set; }
            public string Phone { get; set; }
        }

        public class billService : BillService
        {
        }

        public class SpecialCustomers : SpecialCusDetail
        {
            public string TypeName { get; set; }
            public int CustomerTypeId { get; set; }
        }

        /// <summary>
        /// Class sử dụng cho phần update voucher giảm giá khi thời gian chờ >= 20 phút
        /// </summary>
        public class cls_voucher_waitime
        {
            public int ID { get; set; }
            public bool isVoucher { get; set; }
            public float VoucherPercent { get; set; }
            public List<int> ServiceIDs { get; set; }
        }

        public class cls_voucher_waitime_custom : CRM_VoucherWaitTime
        {
            public int StylistConfigSalary { get; set; }
            public int SkinnerConfigSalary { get; set; }
            public int CheckinConfigSalary { get; set; }
        }

        /// <summary>
        /// author: dungnm
        /// </summary>
        public class ResponseData
        {
            public int status { get; set; }
            public string message { get; set; }
            public dynamic data { get; set; }

            public ResponseData()
            {
                data = null;
                status = 0;
                message = "";
            }
        }

        /// <summary>
        /// class call api
        /// </summary>
        public class CallApiCheckout
        {
            public bool isStylistOvertimeOld { get; set; }
            public bool isSkinnerOvertimeOld { get; set; }
            public bool isStylistOvertimeNew { get; set; }
            public bool isSkinnerOvertimeNew { get; set; }
        }
    }
}