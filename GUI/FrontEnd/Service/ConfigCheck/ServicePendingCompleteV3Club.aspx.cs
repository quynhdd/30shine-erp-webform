﻿using _30shine.Helpers;
using _30shine.Helpers.Http;
using _30shine.MODEL.BO;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using _30shine.MODEL.IO;
using Libraries;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.FrontEnd.Service.ConfigCheck
{
    public partial class ServicePendingCompleteV3Club : System.Web.UI.Page
    {
        private bool Perm_ShowSalon = false;
        private bool Perm_Access = false;
        private bool Perm_ViewAllData = false;
        private List<int> lstFreeService = new List<int>();
        private static ServicePendingCompleteV3Club instance;
        private string _CustomerCode = "";
        private IStaffModel staffModel = new StaffModel();
        protected bool Perm_EditRating = false;
        protected bool checkSalonHoiQuan = false;
        protected bool Perm_EditBill = false;
        protected bool Perm_EditCheckout = false;
        protected List<Product> _ListProduct = new List<Product>();
        protected string _ListProductJsonString;
        protected AccountModel accountModel;
        protected bool HasImages = false;
        protected bool CusNoInfor = false;
        protected int billID = 0;
        protected int customerID = 0;
        protected string listOfStaffsSerialized;
        public string _CustomerName = "";
        public BillService OBJ;
        public string[] ListImagesName;
        public DateTime dateCheckEditBill = new DateTime();
        //public Solution_30shineEntities db = Library.Model.getProjectModel();

        /// <summary>
        /// check permission
        /// </summary>
        private void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
            else if (Perm_ShowSalon)
            {
                TrSalon.Visible = true;
            }
        }

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            accountModel = AccountModel.Instance;
            billID = this.getBillID();
            var arrayPermision = Session["User_Permission"].ToString().Split(',');
            if (arrayPermision.Length > 0)
            {
                string checkAdmin = Array.Find(arrayPermision, s => s.Equals("ADMIN"));
                string checkRoot = Array.Find(arrayPermision, s => s.Equals("root"));
                if (!String.IsNullOrEmpty(checkRoot) || !String.IsNullOrEmpty(checkAdmin))
                {
                    Perm_EditRating = true;
                    if (!String.IsNullOrEmpty(checkRoot))
                    {
                        Perm_EditBill = true;
                    }
                }
            }
            if (!IsPostBack)
            {
                BindOBJPageLoad();
            }
        }
        /// <summary>
        /// get instance, phục vụ trường hợp có hàm statistic, sử dụng instance có thẻ gọi các method không phải static
        /// </summary>
        /// <returns></returns>
        /// 
        public static ServicePendingCompleteV3Club getInstance()
        {
            if (!(ServicePendingCompleteV3Club.instance is ServicePendingCompleteV3Club))
            {
                ServicePendingCompleteV3Club.instance = new ServicePendingCompleteV3Club();
            }

            return ServicePendingCompleteV3Club.instance;
        }
        #region Bind Data

        /// <summary>
        /// 
        /// </summary>
        private void BindOBJPageLoad()
        {
            if (Bind_Bill())
            {
                Bind_RptService_Bill();
                Bind_RptProduct_Bill();
                Bind_RptProduct();
                Bind_RptService();
                Bind_RptServiceFeatured();
                Bind_RptProductFeatured();
                Bind_RptFreeService();
                // Bind Salon
                Library.Function.bindSalonSpecial(new List<DropDownList>() { Salon }, Perm_ShowSalon);
                if (Perm_ShowSalon)
                {
                    var itemSelected = Salon.Items.FindByValue(OBJ.SalonId.ToString());
                    if (itemSelected != null)
                    {
                        itemSelected.Selected = true;
                    }
                }
                // Lấy danh sách Nhân viên đã được chấm công cho Salon
                List<Object> listOfStaffs = new List<object>();
                StaffModel.getInstance()
                    .GetListStaffEnrollBySalon(OBJ.SalonId.Value, Convert.ToDateTime(OBJ.CreatedDate)).ForEach(
                        delegate (Staff staff)
                        {
                            listOfStaffs.Add(new
                            {
                                Id = staff.Id,
                                OrderCode = staff.OrderCode,
                                TypeId = staff.Type,
                                Fullname = staff.Fullname,
                                Type = staff.StaffTypeName
                            });
                        });
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                this.listOfStaffsSerialized = serializer.Serialize(listOfStaffs);
            }
            else
            {
                MsgSystem.Text = "Không tồn tại hóa đơn!";
                MsgSystem.CssClass = "msg-system warning";
            }
        }

        /// <summary>
        /// check không cho phép sửa bill sau 24h hàng ngày
        /// </summary>
        public bool CheckEditBill()
        {
            bool kq = false;
            DateTime datetimeNow = DateTime.Now;
            if (Perm_EditBill)
            {
                kq = true;
            }
            else if (dateCheckEditBill.Date == datetimeNow.Date)
            {
                var minuteEditBill = datetimeNow.Hour * 60 + datetimeNow.Minute;
                var minuteEndDate = 22 * 60 + 59;
                if (minuteEditBill <= minuteEndDate)
                {
                    kq = true;
                }
                else
                {
                    kq = false;
                }
            }
            return kq;
        }

        /// <summary>
        /// Bind dữ liệu bill
        /// </summary>
        /// <returns></returns>
        private bool Bind_Bill()
        {
            using (var db = new Solution_30shineEntities())
            {
                var ExistOBJ = false;
                var MatchInt = Regex.Match(Request.QueryString["Code"], @"[^0-9]+");
                if (!MatchInt.Success)
                {
                    var _Code = Convert.ToInt32(Request.QueryString["Code"]);
                    OBJ = db.BillServices.AsNoTracking().FirstOrDefault(w => w.Id == _Code && w.IsDelete == 0);
                    //bill = OBJ;
                    //billJson = Library.Format.ObjectToJson(bill);
                    if (OBJ != null)
                    {
                        HDF_SalonId.Value = OBJ.SalonId.ToString();
                        HDF_lgId.Value = accountModel.AccountInfor.UserId.ToString();

                        if (OBJ.CompleteBillTime != null && !Perm_ViewAllData)
                        {
                            ContentWrap.Visible = false;
                            NotAllowAccess.Visible = true;
                            return false;
                        }
                        //check nếu vượt quá time 23:59 ngày hiện tại thì sẽ không sửa được bill
                        if (OBJ.CreatedDate != null)
                        {
                            dateCheckEditBill = OBJ.CreatedDate.Value;
                        }
                        //set HDF_CustomerId (Id khách hàng)
                        HDF_CustomerId.Value = Convert.ToString(OBJ.CustomerId);
                        // Set Customer 30Shine Care
                        if (OBJ.ServiceError != null && OBJ.ServiceError == true)
                        {
                            cbCustomer30ShineCare.Checked = true;
                        }
                        var customerName = db.Customers.FirstOrDefault(w => w.Id == OBJ.CustomerId);
                        CustomerName.Text = customerName.Fullname;
                        _CustomerName = customerName.Fullname;
                        CustomerCode.Text = "Số ĐT : " + OBJ.CustomerCode;
                        TotalMoney.Text = OBJ.TotalMoney.ToString();
                        Description.Text = OBJ.Note;
                        HDF_CustomerCode.Value = OBJ.CustomerCode;
                        HDF_BillId.Value = OBJ.Id.ToString();
                        HDF_ManagerName.Value = GetManagerName(OBJ.SalonId ?? 0);
                        if (OBJ.Mark != null)
                        {
                            HDF_Rating.Value = OBJ.Mark.ToString();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "bind rating",
                                "bindRatingV2(" + OBJ.Mark + ");", true);
                        }
                        // Staff information
                        var staff = new Staff();
                        if (OBJ.Staff_Hairdresser_Id > 0)
                        {
                            staff = db.Staffs.FirstOrDefault(w => w.Id == OBJ.Staff_Hairdresser_Id);
                            if (staff != null && staff.OrderCode != null)
                            {
                                FVStylist.InnerText = staff.OrderCode.ToString() + "- " + staff.Fullname;
                                InputStylist.Text = staff.OrderCode.ToString();
                                Hairdresser.Value = staff.Id.ToString();
                                var salon = db.Tbl_Salon.FirstOrDefault(w => w.Id == OBJ.SalonId);
                                if (salon != null && salon.IsSalonHoiQuan == false)
                                {
                                    Perm_EditCheckout = true;
                                }
                                if (!Perm_EditRating && salon != null && salon.IsSalonHoiQuan == false)
                                {
                                    InputStylist.Enabled = false;
                                    InputStylist.BackColor = System.Drawing.Color.WhiteSmoke;
                                    checkSalonHoiQuan = true;
                                }
                            }
                        }

                        if (OBJ.Staff_HairMassage_Id > 0)
                        {
                            staff = db.Staffs.FirstOrDefault(w => w.Id == OBJ.Staff_HairMassage_Id);
                            if (staff != null && staff.OrderCode != null)
                            {
                                FVSkinner.InnerText = staff.OrderCode.ToString() + "- " + staff.Fullname;
                                InputSkinner.Text = staff.OrderCode.ToString();
                                HairMassage.Value = staff.Id.ToString();
                            }
                        }

                        if (OBJ.SellerId > 0)
                        {
                            staff = db.Staffs.FirstOrDefault(w => w.Id == OBJ.SellerId);
                            if (staff != null && staff.OrderCode != null)
                            {
                                FVSeller.InnerText = staff.OrderCode.ToString() + "- " + staff.Fullname;
                                InputSeller.Text = staff.OrderCode.ToString();
                                Cosmetic.Value = staff.Id.ToString();
                            }
                        }

                        if (OBJ.ReceptionId > 0)
                        {
                            staff = db.Staffs.FirstOrDefault(w => w.Id == OBJ.ReceptionId);
                            if (staff != null && staff.OrderCode != null)
                            {
                                FVReception.InnerText = staff.OrderCode.ToString() + "- " + staff.Fullname;
                                InputReception.Text = staff.OrderCode.ToString();
                                HDF_Reception.Value = staff.Id.ToString();
                            }
                        }

                        if (OBJ.CheckoutId > 0)
                        {
                            staff = db.Staffs.FirstOrDefault(w => w.Id == OBJ.CheckoutId);
                            if (staff != null && staff.OrderCode != null)
                            {
                                FVCheckout.InnerText = staff.OrderCode.ToString() + "- " + staff.Fullname;
                                InputCheckout.Text = staff.OrderCode.ToString();
                                HDF_Checkout.Value = staff.Id.ToString();
                            }
                        }

                        if (OBJ.SecurityId > 0)
                        {
                            staff = db.Staffs.FirstOrDefault(w => w.Id == OBJ.SecurityId);
                            if (staff != null && staff.OrderCode != null)
                            {
                                FVSecurity.InnerText = staff.OrderCode.ToString() + "- " + staff.Fullname;
                                InputSecurity.Text = staff.OrderCode.ToString();
                                HDF_Security.Value = staff.Id.ToString();
                            }
                        }

                        if (OBJ.Images != "" && OBJ.Images != null)
                        {
                            ListImagesName = OBJ.Images.Split(',');
                            HasImages = true;
                        }
                        else
                        {
                            ListImagesName = new string[] { };
                        }

                        if (OBJ.IsPayByCard != null && OBJ.IsPayByCard.Value)
                        {
                            PayByCard.Checked = true;
                        }
                        _CustomerCode = OBJ.CustomerCode;
                        CusNoInfor = Convert.ToBoolean(OBJ.CustomerNoInfo);
                        ExistOBJ = true;

                        // Get list free service (Phụ trợ)
                        lstFreeService = db.Database
                            .SqlQuery<int>("select PromotionId from FlowPromotion where BillId = " + OBJ.Id +
                                           " and IsDelete != 1").ToList();

                        // Set giá trị cho customerID
                        this.customerID = OBJ.CustomerId.Value;
                    }
                }
                else
                {
                    MsgSystem.Text = "Không tồn tại hóa đơn!";
                    MsgSystem.CssClass = "msg-system warning";
                }

                return ExistOBJ;
            }
        }

        /// <summary>
        /// Bind danh sách dịch vụ trong bill
        /// </summary>
        private void Bind_RptService_Bill()
        {
            using (var db = new Solution_30shineEntities())
            {
                List<ProductBasic> lstService = new List<ProductBasic>();
                var serializer = new JavaScriptSerializer();
                if (OBJ.ServiceIds != "")
                {
                    lstService = serializer.Deserialize<List<ProductBasic>>(OBJ.ServiceIds).ToList();
                    if (lstService.Count > 0)
                    {
                        ListingServiceWp.Style.Add("display", "block");
                        //HasService = true;
                    }
                }

                Rpt_Service_Bill.DataSource = lstService;
                Rpt_Service_Bill.DataBind();

            }
        }

        /// <summary>
        /// Bind danh sách sản phẩm trong bill
        /// </summary>
        private void Bind_RptProduct_Bill()
        {
            using (var db = new Solution_30shineEntities())
            {
                //var _Products = db.Store_CheckOutProduct(OBJ.Id).ToList();
                //var _Products = (from f in db.FlowProducts.AsNoTracking()
                //                 join p in db.Products.AsNoTracking() on f.ProductId equals p.Id
                //                 where f.IsDelete == 0
                //                 && f.ComboId == null
                //                 && f.BillId == OBJ.Id
                //                 select new
                //                 {
                //                     Id = p.Id,
                //                     CategoryId = p.CategoryId,
                //                     Name = p.Name,
                //                     Code = p.Code,
                //                     MapIdProduct = p.MapIdProduct,
                //                     Price = f.Price,
                //                     VoucherPercent = f.VoucherPercent,
                //                     Quantity = f.Quantity,
                //                     Promotion = f.PromotionMoney
                //                 }).ToList();
                List<ProductBasic> lstProduct = new List<ProductBasic>();
                var serializer = new JavaScriptSerializer();
                if (OBJ.ProductIds != "" && OBJ.ProductIds != null)
                {
                    lstProduct = serializer.Deserialize<List<ProductBasic>>(OBJ.ProductIds).ToList();
                }
                if (lstProduct.Count > 0)
                {
                    ListingProductWp.Style.Add("display", "block");
                    //HasProduct = true;
                }
                Rpt_Product_Bill.DataSource = lstProduct;
                Rpt_Product_Bill.DataBind();
            }
        }

        /// <summary>
        /// Bind danh sách sản phẩm
        /// </summary>
        private void Bind_RptProduct()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Products = db.Products.AsNoTracking().Where(w => w.IsDelete != 1 && w.Publish == 1).OrderByDescending(o => o.Id)
                    .ToList();
                _ListProduct = _Products;
                var serializer = new JavaScriptSerializer();
                _ListProductJsonString = serializer.Serialize(_ListProduct);
            }
        }

        /// <summary>
        /// Bind danh sách dịch vụ
        /// </summary>
        private void Bind_RptService()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Service = db.Services.AsNoTracking().Where(w => w.IsDelete != 1 && w.Publish == 1).OrderByDescending(o => o.Id)
                    .ToList();

                Rpt_Service.DataSource = _Service;
                Rpt_Service.DataBind();
            }
        }

        /// <summary>
        /// Bind danh sách dịch vụ phụ trợ như Cạo mặt, Cắt móng tay
        /// </summary>
        private void Bind_RptFreeService()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Service = db.Services.AsNoTracking().Where(w => w.IsDelete != 1 && w.Publish == 1 && w.IsFreeService == 1)
                    .OrderBy(o => o.Id).ToList();
                int index = -1;
                if (_Service.Count > 0 && lstFreeService.Count > 0)
                {
                    foreach (var v in _Service)
                    {
                        index = lstFreeService.IndexOf(v.Id);
                        if (index != -1)
                        {
                            _Service.FirstOrDefault(w => w.Id == lstFreeService[index]).Status = 1;
                        }
                    }
                }

                Rpt_FreeService.DataSource = _Service;
                Rpt_FreeService.DataBind();
            }
        }

        /// <summary>
        /// Bind danh sách dịch vụ nổi bật
        /// </summary>
        private void Bind_RptServiceFeatured()
        {
            using (var db = new Solution_30shineEntities())
            {
                var MatchInt = Regex.Match(Request.QueryString["Code"], @"[^0-9]+");
                if (!MatchInt.Success)
                {
                    var _Code = Convert.ToInt32(Request.QueryString["Code"]);
                    OBJ = db.BillServices.AsNoTracking().FirstOrDefault(w => w.Id == _Code);
                }

                var lst = db.Salon_Service_Select(OBJ.SalonId, 0).Where(p => p.IsCheck == true).ToList();
                Rpt_ServiceFeatured.DataSource = lst;
                Rpt_ServiceFeatured.DataBind();
            }
        }

        /// <summary>
        /// Bind danh sách sản phẩm nổi bật
        /// </summary>
        private void Bind_RptProductFeatured()
        {
            using (var db = new Solution_30shineEntities())
            {
                // Combo : ID 95
                int cateId = 95;
                var lst = db.Products
                    .Where(w => w.IsDelete != 1 && w.Publish == 1 && w.Status == 1 && w.CategoryId == cateId)
                    .OrderByDescending(o => o.Id).ToList();
                Rpt_ProductFeatured_Combo.DataSource = lst;
                Rpt_ProductFeatured_Combo.DataBind();

                // Gôm : ID 6
                cateId = 6;
                lst = db.Products
                    .Where(w => w.IsDelete != 1 && w.Publish == 1 && w.Status == 1 && w.CategoryId == cateId)
                    .OrderByDescending(o => o.Id).ToList();
                Rpt_ProductFeatured_Gom.DataSource = lst;
                Rpt_ProductFeatured_Gom.DataBind();

                // Sáp : ID 7
                cateId = 7;
                lst = db.Products
                    .Where(w => w.IsDelete != 1 && w.Publish == 1 && w.Status == 1 && w.CategoryId == cateId)
                    .OrderByDescending(o => o.Id).ToList();
                Rpt_ProductFeatured_Sap.DataSource = lst;
                Rpt_ProductFeatured_Sap.DataBind();
            }
        }

        #endregion
        /// <summary>
        /// Đợi hoàn tất bill, khi check out mở nhầm bill
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void WaitFinish_Click(object sender, EventArgs e)
        {
            // Thông báo thành công
            var MsgParam = new List<KeyValuePair<string, string>>();
            MsgParam.Add(new KeyValuePair<string, string>("msg_wait_status", "success"));
            MsgParam.Add(new KeyValuePair<string, string>("msg_wait_message",
                "Hóa đơn đã chuyển sang trạng thái đợi!"));
            UIHelpers.Redirect("/dich-vu/pending.html", MsgParam);
        }

        /// <summary>
        /// Hoàn tất bill
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CompleteService(object sender, EventArgs e)
        {

            try
            {
                if (Checkout())
                {
                    // Thông báo thành công
                    var MsgParam = new List<KeyValuePair<string, string>>();
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Hoàn tất bill thành công!"));
                    //UIHelpers.Redirect("/dich-vu/pending.html", MsgParam);
                    // lay ten domain
                    var domain = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host;//VD: http://erp.30shine.com
                    domain += "/dich-vu/pending.html?msg_update_status=success&msg_update_message=Hoàn tất bill thành công!";
                    var exeJavascript = @"window.location.href = '" + domain + "'";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Rederect-Print", exeJavascript, true);
                }
                else
                {
                    MsgSystem.Text = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                    MsgSystem.CssClass = "msg-system warning";
                }
            }
            catch (Exception ex)
            {
                // Push thông báo cho mọi người(by slack) khi có lỗi xảy ra
                new Helpers.PushNotiErrorToSlack().PushDefault(ex.Message + ", MAIN: " + ex.InnerException, this.ToString() + ".checkout", "phucdn");
                Helpers.UIHelpers.TriggerJsMsgSystem_v1(this, "Đã có lỗi xảy ra: " + ex.Message.Replace("'",""), "msg-system warning", 20000);
            }
        }

        /// <summary>
        /// call checkout
        /// </summary>
        public bool Checkout()
        {
            bool check = false;
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    //Start convert to api
                    int integer;
                    var billId = int.TryParse(Request.QueryString["Code"], out integer) ? integer : 0;
                    var objBill = new cls_Billservice();
                    if (billId > 0)
                    {
                        objBill.Id = billId;
                        objBill.ServiceError = cbCustomer30ShineCare.Checked;
                        objBill.CheckoutId = int.TryParse(HDF_Checkout.Value, out integer) ? integer : 0;
                        objBill.SecurityId = int.TryParse(HDF_Security.Value, out integer) ? integer : 0;
                        objBill.Mark = int.TryParse(HDF_Rating.Value, out integer) ? integer : 0;
                        objBill.ProductIds = HDF_ProductIds.Value != "[]" ? HDF_ProductIds.Value : ""; ;
                        objBill.ReceptionId = int.TryParse(HDF_Reception.Value, out integer) ? integer : 0;
                        objBill.SalonId = int.TryParse(HDF_SalonId.Value, out integer) ? integer : 0;
                        objBill.SellerId = int.TryParse(Cosmetic.Value, out integer) ? integer : 0;
                        objBill.ServiceIds = HDF_ServiceIds.Value != "[]" ? HDF_ServiceIds.Value : "";
                        objBill.TotalMoney = int.TryParse(HDF_TotalMoney.Value, out integer) ? integer : 0;
                        objBill.staffHairdresserId = int.TryParse(Hairdresser.Value, out integer) ? integer : 0;
                        objBill.staffHairMassageId = int.TryParse(HairMassage.Value, out integer) ? integer : 0;

                        objBill.IsPayByCard = PayByCard.Checked;
                        objBill.Note = Description.Text;
                        string vipGive = VIPGive.Value;
                        string vipUse = VIPUse.Value;
                        //bool isAppRating = CkRating.Checked;
                        //data input form
                        var data = new { objbill = objBill, vipGive = vipGive, vipUse = vipUse, isAppRating = false };
                        //call api xử lý insert billservice
                        var dataInput = new Request().RunPutAsync(AppConstants.URL_API_CHECKOUT + "/api/checkout-club/billservice", data).Result;
                        //checkout success
                        if (dataInput.IsSuccessStatusCode)
                        {
                            //response data
                            var recordBill = dataInput.Content.ReadAsStringAsync().Result;
                            JavaScriptSerializer serializer = new JavaScriptSerializer();
                            var responseData = serializer.Deserialize<BillServiceOldAndNew>(recordBill);
                            //call api xử lý luồng phụ
                            //var task1 = Task.Run(async () =>
                            //{
                            //    //add or update salary
                            //    await new Request().RunPutAsync(AppConstants.URL_API_CHECKOUT + "/api/salary", responseData);

                            //});
                           // add or update flowservice
                            var task2 = Task.Run(async () =>
                            {
                                await new Request().RunPutAsync(AppConstants.URL_API_CHECKOUT + "/api/flow-service", new
                                {
                                    BillId = responseData.recordBillNew.Id,
                                    StylistId = responseData.recordBillNew.staffHairdresserId ?? 0,
                                    SkinnerId = responseData.recordBillNew.staffHairMassageId ?? 0,
                                    JsonServiceProduct = responseData.recordBillNew.ServiceIds,
                                    CreatedDate = responseData.recordBillNew.CreatedDate.Value,
                                    SalonId = responseData.recordBillNew.SalonId ?? 0,
                                    SellerId = responseData.recordBillNew.SellerId ?? 0,
                                    Pending = responseData.recordBillOld.Pending
                                });
                            });
                            //add or update flowprduct
                            var task3 = Task.Run(async () =>
                            {
                                await new Request().RunPutAsync(AppConstants.URL_API_CHECKOUT + "/api/flow-product", new
                                {
                                    BillId = responseData.recordBillNew.Id,
                                    JsonServiceProduct = responseData.recordBillNew.ProductIds,
                                    CreatedDate = responseData.recordBillNew.CreatedDate.Value,
                                    SalonId = responseData.recordBillNew.SalonId ?? 0,
                                    SellerId = responseData.recordBillNew.SellerId ?? 0,
                                    Pending = responseData.recordBillOld.Pending
                                });
                            });
                            //add or update statist ratting waitime
                            //var task4 = Task.Run(async () =>
                            //{
                            //    await new Request().RunPutAsync(AppConstants.URL_API_CHECKOUT + "/api/static-rating-waittime", responseData);
                            //});
                           // crmvoucherwatitme
                           //var task5 = Task.Run(async () =>
                           //{
                           //    await new Request().RunPutAsync(AppConstants.URL_API_CHECKOUT + "/api/crm-voucher-waittime?customerId=" + responseData.recordBillNew.CustomerId, "");
                           //});
                           // update customer
                            var task6 = Task.Run(async () =>
                            {
                                await new Request().RunPutAsync(AppConstants.URL_API_CHECKOUT + "/api/customer", new
                                {
                                    StaffId = responseData.recordBillNew.staffHairdresserId ?? 0,
                                    SalonId = responseData.recordBillNew.SalonId ?? 0,
                                    CustomerId = responseData.recordBillNew.CustomerId,
                                    TotalMoneyBillNew = responseData.recordBillNew.TotalMoney ?? 0,
                                    TotalMoneyBillOld = responseData.recordBillOld.TotalMoney ?? 0,
                                    CustomerUsedNumber = responseData.recordBillNew.CustomerUsedNumber ?? 0
                                });
                            });

                            check = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return check;
        }

        #region Tinh thoi gian cho de tao Voucher cho khach
        /// <summary>
        /// Check khách có phải khách VIP hay không
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public bool isCustomerVIP(int customerID)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var ret = false;
                    var specialCustomer = new SpecialCustomer();
                    var detailSpecialCustomer = new SpecialCusDetail();
                    // Kiểm tra khách hàng đặc biệt
                    specialCustomer = db.SpecialCustomers.AsNoTracking()
                        .Where(a => a.IsDelete == false && a.CustomerTypeId == 2 && a.CustomerId == customerID)
                        .OrderBy(a => a.Id).FirstOrDefault();

                    if (specialCustomer != null)
                    {
                        ret = true;
                    }
                    return ret;
                }
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Tính thời gian chờ
        /// </summary>
        /// <param name="bs"></param>
        /// <returns></returns>
        public static int? GetWaitTime(BillService bs)
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
            var WaitTime = 0;
            try
            {
                var book = db.Bookings.AsNoTracking().FirstOrDefault(f => f.Id == bs.BookingId);
                if (book != null && book.IsBookOnline == true)
                {
                    DateTime? pvgTime = bs.InProcedureTime;
                    DateTime? checkinTime = bs.CreatedDate;
                    DateTime bookHour = new DateTime();
                    var hour = db.BookHours.FirstOrDefault(w => w.Id == book.HourId);
                    bookHour = bs.CreatedDate.Value.Date.Add(hour.HourFrame.Value);

                    if (checkinTime >= bookHour)
                    {
                        WaitTime = (int)(pvgTime - checkinTime).Value.TotalMinutes;
                    }
                    else
                    {
                        WaitTime = (int)(pvgTime - bookHour).Value.TotalMinutes;
                    }
                }
            }
            catch (Exception ex)
            {
                //throw new Exception("Error");
                return null;
            }

            return WaitTime;
        }

        /// <summary>
        /// Check khách có phải đặc biệt hay không
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public bool isCustomerSpecial(int customerID)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var ret = false;
                    var specialCustomer = new SpecialCustomer();
                    var detailSpecialCustomer = new SpecialCusDetail();
                    // Kiểm tra khách hàng đặc biệt
                    specialCustomer = db.SpecialCustomers.AsNoTracking()
                        .Where(a => a.IsDelete == false && a.CustomerTypeId == 1 && a.CustomerId == customerID)
                        .OrderBy(a => a.Id).FirstOrDefault();

                    if (specialCustomer != null)
                    {
                        detailSpecialCustomer = db.SpecialCusDetails
                            .Where(a => a.IsDelete == false && a.QuantityInvited > 0 &&
                                        a.SpecialCusId == specialCustomer.Id).OrderByDescending(a => a.Id).FirstOrDefault();
                        if (detailSpecialCustomer != null)
                        {
                            ret = true;
                        }
                    }

                    return ret;
                }
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Kiểm tra 1 bill có phải bill khách đợi lâu hay không (có thể là 20 phút)
        /// Điều kiện:
        /// 1. Khách chờ lâu quá n phút
        /// 2. Khách không phải đặc biệt, VIP
        /// </summary>
        /// <param name="bill"></param>
        /// <returns></returns>
        public bool isWaitLongTime(BillService bill)
        {
            try
            {
                var isWait = false;
                int integer;
                var configModel = new ConfigModel();
                var configWaitTime =
                    int.TryParse(configModel.GetValueByKey("bill_voucher_waittime_minute"), out integer) ? integer : 0;

                if (bill != null && (!instance.isCustomerSpecial(bill.CustomerId.Value) &&
                                     !instance.isCustomerVIP(bill.CustomerId.Value)))
                {
                    int? time = GetWaitTime(bill);
                    if (time != null && time >= configWaitTime)
                    {
                        isWait = true;
                    }
                }

                return isWait;
            }
            catch
            {
                return false;
            }
        }

        #endregion
        /// <summary>
        /// Kiểm tra khách có vourcher thời gian chờ quá 20 phút hay không
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        /// 
        /// <summary>
        /// Get bill ID từ url query string
        /// </summary>
        /// <returns></returns>
        public int getBillID()
        {
            var ID = 0;
            try
            {
                var MatchInt = Regex.Match(Request.QueryString["Code"], @"[^0-9]+");
                if (!MatchInt.Success)
                {
                    ID = Convert.ToInt32(Request.QueryString["Code"]);
                }
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message);
            }

            return ID;
        }
        private string GetManagerName(int salonId)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var objSalon = db.Tbl_Salon.Where(r => r.Id == salonId).FirstOrDefault();
                    return objSalon == null ? "" : (objSalon.ManagerName == null ? "" : objSalon.ManagerName);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        #region api call tu form
        [WebMethod]
        public static object checkVoucherWaitTime(int CustomerId)
        {
            var message = new Msg();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var data = new cls_voucher_waitime();
                    data.ServiceIDs = new List<int>();
                    data.ServiceIDs.Add(53);

                    var record = db.CRM_VoucherWaitTime.Where(
                        w => w.CustomerID == CustomerId
                             && w.IsUsed == false
                             && w.IsDelete == false
                    ).OrderByDescending(o => o.ID).FirstOrDefault();
                    if (record != null)
                    {
                        data.ID = record.ID;
                        data.isVoucher = true;
                        data.VoucherPercent = record.VoucherPercent != null ? (float)record.VoucherPercent.Value : 0;
                    }
                    else
                    {
                        data.isVoucher = false;
                    }

                    message.data = data;
                    message.success = true;
                }
            }
            catch (Exception ex)
            {
                message.success = false;
                message.msg = ex.Message;
            }

            return message;
        }

        /// <summary>
        /// Kiểm tra 1 bill có phải bill khách đợi lâu hay không
        /// </summary>
        /// <param name="billID"></param>
        /// <returns></returns>
        [WebMethod]
        public static object checkCustomerIsWaitLongTime(int billID)
        {
            var message = new Library.Class.cls_message();

            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var instance = ServicePendingCompleteV3Club.getInstance();
                    var bill = db.BillServices.FirstOrDefault(w => w.Id == billID);
                    if (instance.isWaitLongTime(bill))
                    {
                        message.success = true;
                        message.data = true;
                        message.message = "Khách đợi lâu";
                    }
                    else
                    {
                        message.success = true;
                        message.data = false;
                        message.message = "";
                    }
                    return message;
                }
            }
            catch
            {
                message.success = false;
                message.data = false;
                message.message = "Có lỗi xảy ra. Vui lòng liên hệ nhà phát triển.";
                return message;
            }
        }

        /// <summary>
        /// Kiểm tra đã tạo bill cho khách hàng trong hôm nay qua mã KH
        /// note: dang dung tren form
        /// </summary>
        /// <param name="CustomerCode"></param>
        /// <returns></returns>
        [WebMethod]
        public static string CheckDuplicateBill(string CustomerCode)
        {
            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var today = Convert.ToDateTime(DateTime.Now.ToString("d/M/yyyy"), culture);
                var todayL = today.AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                var todayR = today.AddDays(1);
                var _Bill = db.BillServices.FirstOrDefault(w =>
                    w.CreatedDate > todayL && w.CreatedDate < todayR && w.CustomerCode == CustomerCode);

                if (_Bill != null)
                {
                    _Msg.success = true;
                    _Msg.msg = serializer.Serialize(_Bill);
                }
                else
                {
                    _Msg.success = false;
                }
            }

            return serializer.Serialize(_Msg);
        }

        [WebMethod]
        public static SpecialCustomers GetSpecialCustomer(int CustomerId)
        {
            var db = new Solution_30shineEntities();
            var query = @"select CustomerTypeId from SpecialCustomer where IsDelete = 0 and CustomerId = " + CustomerId + "";
            int customerType = db.Database.SqlQuery<int>(query).FirstOrDefault();


            var sql = @"select A.*, C.TypeName, B.CustomerTypeId from SpecialCusDetail A 
                        left join SpecialCustomer B on A.SpecialCusId = B.Id
                        left join CustomerType C on C.Id = B.CustomerTypeId
						left join Customer D on D.Id = B.CustomerId
                        where B.Publish =1 and A.IsDelete = 0 and D.Id = " + CustomerId + "";
            if (customerType == 1)
            {
                sql += @" and A.QuantityInvited > 0
                        and A.CutErrorDate = (select Min(A.CutErrorDate) from SpecialCusDetail A 
						left join SpecialCustomer B on A.SpecialCusId = B.Id
						left join Customer D on D.Id = B.CustomerId
						where  D.Id = " + CustomerId + " and A.QuantityInvited > 0 and A.IsDelete = 0)";
            }
            return db.Database.SqlQuery<SpecialCustomers>(sql).SingleOrDefault();
        }
        #endregion

        /// <summary>
        /// Kiểm tra stylist,skinner có trong giờ tăng ca hay không.
        /// </summary>
        /// <param name="staff"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public class cls_Billservice : BillService
        {
            public int? staffHairdresserId { get; set; }
            public int? staffHairMassageId { get; set; }
        }
        public class SpecialCustomers : SpecialCusDetail
        {
            public string TypeName { get; set; }
            public int CustomerTypeId { get; set; }
        }
        /// <summary>
        /// Class sử dụng cho phần update voucher giảm giá khi thời gian chờ >= 20 phút
        /// </summary>
        public class cls_voucher_waitime
        {
            public int ID { get; set; }
            public bool isVoucher { get; set; }
            public float VoucherPercent { get; set; }
            public List<int> ServiceIDs { get; set; }
        }
        /// <summary>
        /// class call api
        /// </summary>
        public class BillServiceOldAndNew
        {
            public cls_Billservice recordBillOld { get; set; }
            public cls_Billservice recordBillNew { get; set; }
        }
    }
}