﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Helpers;
using System.Text.RegularExpressions;
using System.Globalization;
//test time
using System.Diagnostics;
using System.Threading;

namespace _30shine.GUI.FrontEnd.Service.ConfigCheck
{
    public partial class Service_Pending_Complete_V3 : System.Web.UI.Page
    {
        private bool Perm_ShowSalon = false;
        private bool Perm_Access = false;

        private List<ProductBasic> ProductList = new List<ProductBasic>();
        protected List<Product> _ListProduct = new List<Product>();
        private List<ProductBasic> ServiceList = new List<ProductBasic>();
        private string _CustomerCode = "";
        private string _CustomerName = "";
        private string Stylist = "";
        private string Skinner = "";
        private string Seller = "";
        protected int SalonId = 0;
        private List<int> lstFreeService = new List<int>();

        private BillService OBJ;
        protected bool HasService = false;
        protected bool HasProduct = false;
        protected bool HasImages = false;
        protected List<string> ListImagesUrl = new List<string>();
        protected string[] ListImagesName;
        protected bool CusNoInfor = false;
        protected int billID = 0;
        protected int customerID = 0;
        private static Service_Pending_Complete_V3 instance;
        public Solution_30shineEntities db = Library.Model.getProjectModel();

        protected void Page_Load(object sender, EventArgs e)
        {

            CheckPermission();
            this.billID = this.getBillID();
            if (!IsPostBack)
            {
                if (Bind_Bill())
                {
                    if (Session["SalonId"] != null)
                    {
                        HDF_SalonId.Value = Session["SalonId"].ToString();
                    }
                    else
                    {
                        HDF_SalonId.Value = "0";
                    }
                    if (Session["User_Id"] != null)
                    {
                        HDF_lgId.Value = Session["User_Id"].ToString();
                    }
                    else
                    {
                        HDF_lgId.Value = "0";
                    }
                    Bind_RptService_Bill();
                    Bind_RptProduct_Bill();
                    Bind_RptProduct();
                    Bind_RptService();
                    Bind_RptServiceFeatured();
                    Bind_RptProductFeatured();
                    Bind_RptFreeService();
                    Bind_Salon();

                }
                else
                {
                    MsgSystem.Text = "Không tồn tại hóa đơn!";
                    MsgSystem.CssClass = "msg-system warning";
                }

                /// Reset rating value
                resetRatingTemp();
            }
        }

        /// <summary>
        /// get instance, phục vụ trường hợp có hàm statistic, sử dụng instance có thẻ gọi các method không phải static
        /// </summary>
        /// <returns></returns>
        public static Service_Pending_Complete_V3 getInstance()
        {
            if (!(Service_Pending_Complete_V3.instance is Service_Pending_Complete_V3))
            {
                Service_Pending_Complete_V3.instance = new Service_Pending_Complete_V3();
            }

            return Service_Pending_Complete_V3.instance;
        }

        /// <summary>
        /// Get bill ID từ url query string
        /// </summary>
        /// <returns></returns>
        public int getBillID()
        {
            var ID = 0;
            try
            {
                var MatchInt = Regex.Match(Request.QueryString["Code"], @"[^0-9]+");
                if (!MatchInt.Success)
                {
                    ID = Convert.ToInt32(Request.QueryString["Code"]);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return ID;
        }

        /// <summary>
        /// Kiểm tra khách có vourcher thời gian chờ quá 20 phút hay không
        /// </summary>
        /// <param name="CustomerId"></param>
        /// <returns></returns>
        [WebMethod]
        public static object checkVoucherWaitTime(int CustomerId)
        {
            var message = new Msg();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var data = new cls_voucher_waitime();
                    data.ServiceIDs = new List<int>();
                    data.ServiceIDs.Add(53);

                    var record = db.CRM_VoucherWaitTime.Where(
                        w => w.CustomerID == CustomerId
                        && w.IsUsed == false
                        && w.IsDelete == false
                    ).OrderByDescending(o => o.ID).FirstOrDefault();
                    if (record != null)
                    {
                        data.ID = record.ID;
                        data.isVoucher = true;
                        data.VoucherPercent = record.VoucherPercent != null ? (float)record.VoucherPercent.Value : 0;
                    }
                    else
                    {
                        data.isVoucher = false;
                    }
                    message.data = data;
                    message.success = true;
                }
            }
            catch (Exception ex)
            {
                message.success = false;
                message.msg = ex.Message;
            }
            return message;
        }

        /// <summary>
        /// Update trạng thái Voucher 20% gói Shine Combo đã được áp dụng cho khách
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [WebMethod]
        public static object updateVoucherWaitTimeIsUsed(int Id)
        {
            var message = new Msg();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var record = db.CRM_VoucherWaitTime.FirstOrDefault(w => w.ID == Id);
                    if (record != null)
                    {
                        record.IsUsed = true;
                        record.ModifiedTime = DateTime.Now;
                        db.CRM_VoucherWaitTime.AddOrUpdate(record);
                        db.SaveChanges();

                        message.success = true;
                        message.msg = "Succeeded.";
                    }
                }
            }
            catch (Exception ex)
            {
                message.success = false;
                message.msg = ex.Message;
            }
            return message;
        }

        /// <summary>
        /// Tính thời gian chờ
        /// </summary>
        /// <param name="bs"></param>
        /// <returns></returns>
        public static int? GetWaitTime(BillService bs)
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
            var WaitTime = 0;
            try
            {
                var book = db.Bookings.FirstOrDefault(f => f.Id == bs.BookingId);
                if (book != null && book.IsBookOnline == true)
                {
                    DateTime? pvgTime = bs.InProcedureTime;
                    DateTime? checkinTime = bs.CreatedDate;
                    DateTime bookHour = new DateTime();
                    var hour = db.BookHours.FirstOrDefault(w => w.Id == book.HourId);
                    bookHour = book.CreatedDate.Value.Date.Add(hour.HourFrame.Value);

                    if (checkinTime >= bookHour)
                    {
                        WaitTime = (int)(pvgTime - checkinTime).Value.TotalMinutes;
                    }
                    else
                    {
                        WaitTime = (int)(pvgTime - bookHour).Value.TotalMinutes;
                    }
                }

            }
            catch (Exception ex)
            {
                //throw new Exception("Error");
                return null;
            }

            return WaitTime;
        }

        /// <summary>
        /// Check khách có phải đặc biệt hay không
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public bool isCustomerSpecial(int customerID)
        {
            try
            {
                var ret = false;
                var specialCustomer = new SpecialCustomer();
                var detailSpecialCustomer = new SpecialCusDetail();
                // Kiểm tra khách hàng đặc biệt
                specialCustomer = this.db.SpecialCustomers.Where(a => a.IsDelete == false && a.CustomerTypeId == 1 && a.CustomerId == customerID).OrderBy(a => a.Id).FirstOrDefault();

                if (specialCustomer != null)
                {
                    detailSpecialCustomer = this.db.SpecialCusDetails.Where(a => a.IsDelete == false && a.QuantityInvited > 0 && a.SpecialCusId == specialCustomer.Id).OrderByDescending(a => a.Id).FirstOrDefault();
                    if (detailSpecialCustomer != null)
                    {
                        ret = true;
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Check khách có phải khách VIP hay không
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public bool isCustomerVIP(int customerID)
        {
            try
            {
                var ret = false;
                var specialCustomer = new SpecialCustomer();
                var detailSpecialCustomer = new SpecialCusDetail();
                // Kiểm tra khách hàng đặc biệt
                specialCustomer = this.db.SpecialCustomers.Where(a => a.IsDelete == false && a.CustomerTypeId == 2 && a.CustomerId == customerID).OrderBy(a => a.Id).FirstOrDefault();

                if (specialCustomer != null)
                {
                    ret = true;
                }
                return ret;
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Tạo voucher khách hàng
        /// </summary>
        /// <param name="billID"></param>
        //[WebMethod]
        public void createVoucher(BillService bill)
        {
            try
            {
                Service_Pending_Complete_V2 instance = Service_Pending_Complete_V2.getInstance();
                //var bill = instance.db.BillServices.FirstOrDefault(bs => bs.Id == billID);
                if (bill != null && (!instance.isCustomerSpecial(bill.CustomerId.Value) && !instance.isCustomerVIP(bill.CustomerId.Value)))
                {
                    int? time = GetWaitTime(bill);
                    if (time != null && time >= 20 && bill.Mark > 1)
                    {
                        // insert bản ghi voucher
                        instance.vourcherWaitTimeInsert(bill);

                        //update data to FlowSalary
                        Tbl_Payon tp = new Tbl_Payon();
                        Staff st = new Staff();
                        //he so luong dich vu cua stylist
                        var level_stylist = instance.db.Staffs.FirstOrDefault(s => s.Id == bill.Staff_Hairdresser_Id);
                        var heso_stylist = instance.db.Tbl_Payon.FirstOrDefault(p => p.TypeStaffId == level_stylist.Type && p.ForeignId == level_stylist.SkillLevel && p.PayByTime == 5);
                        int value_stylist = (Int32)heso_stylist.Value;

                        //he so luong dich vu cua skinner
                        var level_skinner = instance.db.Staffs.FirstOrDefault(s => s.Id == bill.Staff_HairMassage_Id);
                        var heso_skinner = instance.db.Tbl_Payon.FirstOrDefault(p => p.TypeStaffId == level_skinner.Type && p.ForeignId == level_skinner.SkillLevel && p.PayByTime == 5);
                        int value_skinner = (Int32)heso_skinner.Value;

                        //he so luong dich vu cua checkin
                        var level_checkin = instance.db.Staffs.FirstOrDefault(s => s.Id == bill.ReceptionId);
                        var heso_checkin = instance.db.Tbl_Payon.FirstOrDefault(p => p.TypeStaffId == level_checkin.Type && p.ForeignId == level_checkin.SkillLevel && p.PayByTime == 5);
                        int value_checkin = (Int32)heso_checkin.Value;

                        FlowSalary _fs = new FlowSalary();
                        //update waitTimeMistake of checkin
                        var today = DateTime.Now.Date;
                        var update_checkin = (
                            from fs in instance.db.FlowSalaries
                            where fs.staffId == bill.ReceptionId
                                && fs.sDate == today
                            select fs
                            ).FirstOrDefault();
                        if (update_checkin.waitTimeMistake == null)
                        {
                            update_checkin.waitTimeMistake = 5;
                        }
                        else
                        {
                            update_checkin.waitTimeMistake = 5 + update_checkin.waitTimeMistake;
                        }
                        update_checkin.serviceSalary = value_checkin * (update_checkin.ratingPoint - update_checkin.waitTimeMistake);
                        instance.db.FlowSalaries.AddOrUpdate(update_checkin);
                        instance.db.SaveChanges();

                        //update waitTimeMistake of stylist
                        var update_stylist = instance.db.FlowSalaries.Where(s => s.staffId == bill.Staff_Hairdresser_Id && s.sDate == today).FirstOrDefault();
                        if (update_stylist.waitTimeMistake == null)
                        {
                            update_stylist.waitTimeMistake = 0.5;
                        }
                        else
                        {
                            update_stylist.waitTimeMistake = 0.5 + update_stylist.waitTimeMistake;
                        }
                        update_stylist.serviceSalary = value_stylist * (update_stylist.ratingPoint - update_stylist.waitTimeMistake);
                        instance.db.FlowSalaries.AddOrUpdate(update_stylist);
                        instance.db.SaveChanges();

                        //update waitTimeMistake of skinner
                        var update_skinner = instance.db.FlowSalaries.Where(s => s.staffId == bill.Staff_HairMassage_Id && s.sDate == today).FirstOrDefault();
                        if (update_skinner.waitTimeMistake == null || update_skinner.waitTimeMistake == 0)
                        {
                            update_skinner.waitTimeMistake = 0.5;
                        }
                        else
                        {
                            update_skinner.waitTimeMistake = 0.5 + update_skinner.waitTimeMistake;
                        }
                        update_skinner.serviceSalary = value_skinner * (float)(update_skinner.ratingPoint - update_skinner.waitTimeMistake);
                        update_stylist.serviceSalary = value_stylist * (update_stylist.ratingPoint - update_stylist.waitTimeMistake);
                        instance.db.FlowSalaries.AddOrUpdate(update_skinner);
                        instance.db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Insert voucher wait-time
        /// </summary>
        /// <param name="bill"></param>
        public void vourcherWaitTimeInsert(BillService bill)
        {
            try
            {
                //add data to CRM_VoucherWaitTime
                CRM_VoucherWaitTime crm_voucher = new CRM_VoucherWaitTime();
                crm_voucher.BillID = bill.Id;
                crm_voucher.CheckinID = bill.ReceptionId;
                crm_voucher.CreatedTime = DateTime.Now;
                crm_voucher.CustomerID = bill.CustomerId;
                crm_voucher.SalonID = bill.SalonId;
                crm_voucher.SkinnerID = bill.Staff_HairMassage_Id;
                crm_voucher.StylistID = bill.Staff_Hairdresser_Id;
                crm_voucher.VoucherPercent = 20;
                crm_voucher.IsUsed = false;
                crm_voucher.IsDelete = false;

                this.db.CRM_VoucherWaitTime.Add(crm_voucher);
                this.db.SaveChanges();
            }
            catch (Exception ex)
            {
                //throw new Exception(ex.Message);
            }
        }


        /// <summary>
        /// Bind dữ liệu bill
        /// </summary>
        /// <returns></returns>
        private bool Bind_Bill()
        {
            using (var db = new Solution_30shineEntities())
            {
                var ExistOBJ = false;
                var MatchInt = Regex.Match(Request.QueryString["Code"], @"[^0-9]+");
                if (!MatchInt.Success)
                {
                    var _Code = Convert.ToInt32(Request.QueryString["Code"]);
                    OBJ = db.BillServices.FirstOrDefault(w => w.Id == _Code);
                    if (OBJ != null)
                    {
                        //set HDF_CustomerId (Id khách hàng)
                        HDF_CustomerId.Value = Convert.ToString(OBJ.CustomerId);
                        // Set trạng thái x2
                        if (OBJ.IsX2 != null && OBJ.IsX2 == true)
                        {
                            isX2.Checked = true;
                        }

                        var customerName = db.Customers.FirstOrDefault(w => w.Id == OBJ.CustomerId);
                        CustomerName.Text = customerName.Fullname;
                        CustomerCode.Text = "Số ĐT : " + OBJ.CustomerCode;
                        TotalMoney.Text = OBJ.TotalMoney.ToString();
                        Description.Text = OBJ.Note;
                        HDF_CustomerCode.Value = OBJ.CustomerCode;
                        HDF_BillId.Value = OBJ.Id.ToString();
                        if (OBJ.Mark != null)
                        {
                            HDF_Rating.Value = OBJ.Mark.ToString();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "bind rating", "bindRating(" + OBJ.Mark + ");", true);
                        }
                        // Staff information
                        var staff = new Staff();
                        if (OBJ.Staff_Hairdresser_Id > 0)
                        {
                            staff = db.Staffs.FirstOrDefault(w => w.Id == OBJ.Staff_Hairdresser_Id);
                            if (staff != null)
                            {
                                FVStylist.InnerText = staff.OrderCode.ToString() + "- " + staff.Fullname;
                                InputStylist.Text = staff.OrderCode.ToString();
                                Hairdresser.Value = staff.Id.ToString();
                            }
                        }
                        if (OBJ.Staff_HairMassage_Id > 0)
                        {
                            staff = db.Staffs.FirstOrDefault(w => w.Id == OBJ.Staff_HairMassage_Id);
                            if (staff != null)
                            {
                                FVSkinner.InnerText = staff.OrderCode.ToString() + "- " + staff.Fullname;
                                InputSkinner.Text = staff.OrderCode.ToString();
                                HairMassage.Value = staff.Id.ToString();
                            }
                        }
                        if (OBJ.SellerId > 0)
                        {
                            staff = db.Staffs.FirstOrDefault(w => w.Id == OBJ.SellerId);
                            if (staff != null)
                            {
                                FVSeller.InnerText = staff.OrderCode.ToString() + "- " + staff.Fullname;
                                InputSeller.Text = staff.OrderCode.ToString();
                                Cosmetic.Value = staff.Id.ToString();
                            }
                        }
                        if (OBJ.ReceptionId > 0)
                        {
                            staff = db.Staffs.FirstOrDefault(w => w.Id == OBJ.ReceptionId);
                            if (staff != null)
                            {
                                FVReception.InnerText = staff.OrderCode.ToString() + "- " + staff.Fullname;
                                InputReception.Text = staff.OrderCode.ToString();
                                HDF_Reception.Value = staff.Id.ToString();
                            }
                        }

                        if (OBJ.Images != "" && OBJ.Images != null)
                        {
                            ListImagesName = OBJ.Images.Split(',');
                            var Len = ListImagesName.Length;
                            if (Len > 0)
                            {
                                var loop = 0;
                                foreach (var v in ListImagesName)
                                {
                                    var match = Regex.Match(v, @"api.30shine.com");
                                    if (!match.Success)
                                    {
                                        ListImagesName[loop] = "https://30shine.com/" + v;
                                    }
                                    loop++;
                                }
                                HasImages = true;
                            }
                        }

                        if (OBJ.IsPayByCard != null && OBJ.IsPayByCard.Value)
                        {
                            PayByCard.Checked = true;
                        }

                        _CustomerCode = OBJ.CustomerCode;
                        CusNoInfor = Convert.ToBoolean(OBJ.CustomerNoInfo);
                        ExistOBJ = true;

                        // Get list free service (Phụ trợ)
                        lstFreeService = db.Database.SqlQuery<int>("select PromotionId from FlowPromotion where BillId = " + OBJ.Id + " and IsDelete != 1").ToList();

                        // Set giá trị cho customerID
                        this.customerID = OBJ.CustomerId.Value;
                    }
                }
                else
                {
                    MsgSystem.Text = "Không tồn tại hóa đơn!";
                    MsgSystem.CssClass = "msg-system warning";
                }

                return ExistOBJ;
            }
        }

        /// <summary>
        /// Bind danh sách dịch vụ trong bill
        /// </summary>
        private void Bind_RptService_Bill()
        {
            using (var db = new Solution_30shineEntities())
            {
                //End code cũ
                var _Services = db.Store_CheckOut_BindService(OBJ.Id).ToList();
                if (_Services.Count > 0)
                {
                    ListingServiceWp.Style.Add("display", "block");
                    HasService = true;
                }
                Rpt_Service_Bill.DataSource = _Services;
                Rpt_Service_Bill.DataBind();
            }
        }

        /// <summary>
        // / Bind danh sách sản phẩm trong bill
        // / </summary>
        private void Bind_RptProduct_Bill()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Products = db.Store_CheckOutProduct(OBJ.Id).ToList(); ;
                if (_Products.Count > 0)
                {
                    ListingProductWp.Style.Add("display", "block");
                    HasProduct = true;
                }
                Rpt_Product_Bill.DataSource = _Products;
                Rpt_Product_Bill.DataBind();
            }
        }

        /// <summary>
        /// Hoàn tất bill
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CompleteService(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var MatchInt = Regex.Match(Request.QueryString["Code"], @"[^0-9]+");
                int integer;
                if (!MatchInt.Success)
                {
                    var _Code = int.TryParse(Request.QueryString["Code"], out integer) ? integer : 0;
                    /// Cập nhật số lượng sử dụng dịch vụ miễn phí cho khách đặc biệt
                    /// Update 1 lần cho 1 bill
                    /// Bill đang pending
                    var OBJBillService = db.BillServices.FirstOrDefault(w => w.Id == _Code && w.Pending == 1);
                    if (OBJBillService != null)
                    {
                        updateQuantityInvited();
                    }
                    OBJ = db.BillServices.FirstOrDefault(w => w.Id == _Code);
                    if (OBJ != null)
                    {
                        // Cập nhật trạng thái x2
                        OBJ.IsX2 = isX2.Checked;

                        //// Insert to BillService
                        _CustomerCode = HDF_CustomerCode.Value;
                        var customer = db.Customers.FirstOrDefault(w => w.Customer_Code == _CustomerCode);
                        _CustomerName = customer != null ? customer.Fullname : "";

                        var stylistId = int.TryParse(Hairdresser.Value, out integer) ? integer : 0;
                        if (stylistId > 0)
                        {
                            var stylist = db.Staffs.FirstOrDefault(w => w.Id == stylistId);
                            Stylist = stylist != null ? stylist.Fullname : "";
                            OBJ.Staff_Hairdresser_Id = stylistId;
                        }
                        else
                        {
                            OBJ.Staff_Hairdresser_Id = 0;
                        }

                        var skinnerId = int.TryParse(HairMassage.Value, out integer) ? integer : 0;
                        if (skinnerId > 0)
                        {
                            var skinner = db.Staffs.FirstOrDefault(w => w.Id == skinnerId);
                            Skinner = skinner != null ? skinner.Fullname : "";
                            OBJ.Staff_HairMassage_Id = skinnerId;
                        }
                        else
                        {
                            OBJ.Staff_HairMassage_Id = 0;
                        }

                        var sellerId = int.TryParse(Cosmetic.Value, out integer) ? integer : 0;
                        if (sellerId > 0)
                        {
                            var seller = db.Staffs.FirstOrDefault(w => w.Id == sellerId);
                            Seller = seller != null ? seller.Fullname : "";
                            OBJ.SellerId = sellerId;
                        }
                        else
                        {
                            OBJ.SellerId = 0;
                        }

                        if (HDF_Reception.Value != "")
                        {
                            OBJ.ReceptionId = Convert.ToInt32(HDF_Reception.Value);
                        }
                        else
                        {
                            OBJ.ReceptionId = 0;
                        }

                        if (TrSalon.Visible == true)
                        {
                            OBJ.SalonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
                        }

                        OBJ.ProductIds = HDF_ProductIds.Value != "[]" ? HDF_ProductIds.Value : "";
                        OBJ.ServiceIds = HDF_ServiceIds.Value != "[]" ? HDF_ServiceIds.Value : "";
                        OBJ.TotalMoney = Convert.ToInt32(HDF_TotalMoney.Value);
                        OBJ.Note = Description.Text;
                        OBJ.ModifiedDate = DateTime.Now;
                        if (OBJ.CompleteBillTime == null)
                        {
                            OBJ.CompleteBillTime = DateTime.Now;
                        }
                        OBJ.Pending = 0;

                        OBJ.Mark = int.TryParse(HDF_Rating.Value, out integer) ? integer : 0;
                        if (VIPGive.Value != "")
                        {
                            OBJ.VIPCard = true;
                            OBJ.VIPCardGive = genVIPCard(VIPGive.Value, 4);
                        }
                        if (VIPUse.Value != "")
                        {
                            OBJ.VIPCardUse = genVIPCard(VIPUse.Value, 4);
                        }

                        // Kiểm tra khách thanh toán qua thẻ hay không
                        OBJ.IsPayByCard = PayByCard.Checked;

                        // Tính lương thử nghiệm cho Checkin
                        db.BillServices.AddOrUpdate(OBJ);
                        var exc = db.SaveChanges();
                        var Error = exc > 0 ? 0 : 1;
                        var serializer = new JavaScriptSerializer();

                        // Insert to FlowProduct
                        if (Error == 0)
                        {
                            var objProduct = new FlowProduct();
                            //var str = "[{\"Id\":\"19\",\"Price\":340000,\"Quantity\":\"1\"}]";
                            serializer = new JavaScriptSerializer();
                            ProductList = serializer.Deserialize<List<ProductBasic>>(OBJ.ProductIds);
                            var ProductListInserted = db.FlowProducts.Where(w => w.BillId == OBJ.Id && w.IsDelete != 1).ToList();
                            var index = -1;
                            var _product = new Product();

                            if (ProductList != null)
                            {
                                // Add/Update product
                                foreach (var v in ProductList)
                                {
                                    if (Error == 0)
                                    {
                                        _product = db.Products.FirstOrDefault(w => w.Id == v.Id);
                                        index = ProductListInserted.FindIndex(fi => fi.ProductId == v.Id);
                                        if (index == -1)
                                        {
                                            objProduct = new FlowProduct();
                                            objProduct.BillId = OBJ.Id;
                                            objProduct.ProductId = v.Id;
                                            objProduct.Price = v.Price;
                                            objProduct.Quantity = v.Quantity;
                                            objProduct.VoucherPercent = Convert.ToInt32(v.VoucherPercent);
                                            objProduct.PromotionMoney = Convert.ToInt32(v.Promotion);
                                            objProduct.CreatedDate = OBJ.CreatedDate;
                                            objProduct.IsDelete = 0;
                                            if (_product != null)
                                            {
                                                objProduct.Cost = Convert.ToInt32(_product.Cost);
                                                objProduct.ForSalary = _product.ForSalary;
                                            }
                                            if (Perm_ShowSalon)
                                            {
                                                objProduct.SalonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
                                            }
                                            else
                                            {
                                                objProduct.SalonId = int.TryParse(HDF_SalonId.Value, out integer) ? integer : 0;
                                            }
                                            objProduct.SellerId = int.TryParse(Cosmetic.Value, out integer) ? integer : 0;

                                            db.FlowProducts.Add(objProduct);
                                            exc = db.SaveChanges();
                                            Error = exc > 0 ? Error : ++Error;
                                        }
                                        else
                                        {
                                            var prd = ProductListInserted[index];
                                            prd.VoucherPercent = Convert.ToInt32(v.VoucherPercent);
                                            prd.PromotionMoney = Convert.ToInt32(v.Promotion);
                                            prd.Quantity = Convert.ToInt32(v.Quantity);
                                            db.FlowProducts.AddOrUpdate(prd);
                                            db.SaveChanges();
                                        }
                                    }
                                }
                            }

                            // Delete product
                            if (ProductListInserted.Count > 0)
                            {
                                foreach (var v in ProductListInserted)
                                {
                                    if (ProductList != null)
                                    {
                                        index = ProductList.FindIndex(fi => fi.Id == v.ProductId);
                                        if (index == -1)
                                        {
                                            var prd = v;
                                            prd.IsDelete = 1;
                                            db.FlowProducts.AddOrUpdate(prd);
                                            db.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        var prd = v;
                                        prd.IsDelete = 1;
                                        db.FlowProducts.AddOrUpdate(prd);
                                        db.SaveChanges();
                                    }
                                }
                            }
                        }

                        // Insert to FlowService
                        if (Error == 0)
                        {
                            var objService = new FlowService();
                            //var str = "[{\"Id\":\"19\",\"Price\":340000,\"Quantity\":\"1\"}]";
                            serializer = new JavaScriptSerializer();
                            ServiceList = serializer.Deserialize<List<ProductBasic>>(OBJ.ServiceIds);
                            var ServiceListInserted = db.FlowServices.Where(w => w.BillId == OBJ.Id && w.IsDelete != 1).ToList();
                            var index = -1;
                            if (ServiceList != null)
                            {
                                foreach (var v in ServiceList)
                                {
                                    if (Error == 0)
                                    {
                                        index = ServiceListInserted.FindIndex(fi => fi.ServiceId == v.Id);
                                        if (index == -1)
                                        {
                                            objService = new FlowService();
                                            objService.BillId = OBJ.Id;
                                            objService.ServiceId = v.Id;
                                            objService.Price = v.Price;
                                            objService.Quantity = v.Quantity;
                                            objService.VoucherPercent = Convert.ToInt32(v.VoucherPercent);
                                            var coefficientRating = db.Services.FirstOrDefault(w => w.Id == v.Id);
                                            if (coefficientRating != null && coefficientRating.CoefficientRating != null)
                                            {
                                                objService.CoefficientRating = coefficientRating.CoefficientRating;
                                            }
                                            else
                                            {
                                                objService.CoefficientRating = 0;
                                            }
                                            objService.CreatedDate = OBJ.CreatedDate;
                                            objService.IsDelete = 0;
                                            if (Perm_ShowSalon)
                                            {
                                                objService.SalonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
                                            }
                                            else
                                            {
                                                objService.SalonId = int.TryParse(HDF_SalonId.Value, out integer) ? integer : 0;
                                            }
                                            objService.SellerId = int.TryParse(Cosmetic.Value, out integer) ? integer : 0;

                                            db.FlowServices.Add(objService);
                                            exc = db.SaveChanges();
                                            Error = exc > 0 ? Error : ++Error;
                                        }
                                        else
                                        {
                                            var srv = ServiceListInserted[index];
                                            srv.VoucherPercent = Convert.ToInt32(v.VoucherPercent);
                                            srv.Quantity = Convert.ToInt32(v.Quantity);
                                            db.FlowServices.AddOrUpdate(srv);
                                        }
                                    }
                                }
                            }

                            if (ServiceListInserted.Count > 0)
                            {
                                foreach (var v in ServiceListInserted)
                                {
                                    if (ServiceList != null)
                                    {
                                        index = ServiceList.FindIndex(fi => fi.Id == v.ServiceId);
                                        if (index == -1)
                                        {
                                            var srv = v;
                                            srv.IsDelete = 1;
                                            db.FlowServices.AddOrUpdate(srv);
                                            db.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        var srv = v;
                                        srv.IsDelete = 1;
                                        db.FlowServices.AddOrUpdate(srv);
                                        db.SaveChanges();
                                    }
                                }
                            }
                        }

                        /// Cập nhật khách quen, số lần khách đã sử dụng dịch vụ vào bill
                        Update_CustomerFamiliar(OBJ);

                        /// Reset giá trị rating temp
                        resetRatingTemp();

                        /// Cập nhật store lương hôm nay
                        SalaryLib.updateFlowSalaryByBill(OBJ, db);

                        /// Cập nhật tồn kho
                        Library.Function.updateInventoryByProductExport(db, OBJ.CompleteBillTime.Value.Date, OBJ.SalonId.Value, OBJ.ProductIds, 1);

                        // Kiểm tra tạo vourcher khách hàng trường hợp thời gian chờ > 20 phút
                        this.createVoucher(OBJ);

                        if (Error == 0)
                        {
                            // Thông báo thành công
                            var MsgParam = new List<KeyValuePair<string, string>>();
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Hoàn tất bill thành công!"));
                            UIHelpers.Redirect("/dich-vu/pending.html", MsgParam);
                        }
                        else
                        {
                            MsgSystem.Text = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                            MsgSystem.CssClass = "msg-system warning";
                        }
                    }

                }
            }
        }

        //public int GetWaitTime(int BillID)
        //{


        //    try
        //    {
        //        //Check Id
        //        var _id = db.BillServices.Where(o => o.Id == BillID).ToString();
        //        if (_id != null)
        //        {
        //            //Check bookid 
        //            var book = db.Bookings.FirstOrDefault(f => f.Id == bs.BookingId);
        //            if (book != null && book.IsBookOnline == true)
        //            {
        //                DateTime? pvgTime = bs.InProcedureTime;
        //                DateTime? checkinTime = bs.CreatedDate;
        //                DateTime bookHour = new DateTime();
        //                var hour = db.BookHours.FirstOrDefault(w => w.Id == book.HourId);

        //                //Check các error
        //                if (bs.CreatedDate == null)
        //                {
        //                    throw new Exception("Lỗi thời gian checkin");
        //                }

        //                if (bs.InProcedureTime == null)
        //                {
        //                    throw new Exception("Lỗi thời gian gội");
        //                }

        //                if (book.CreatedDate == null)
        //                {
        //                    throw new Exception("Lỗi thời gian checkin");
        //                }

        //                //Nếu khác null gán bookHour có value là Datetime
        //                if (hour != null && hour.HourFrame != null)
        //                {
        //                    bookHour = book.CreatedDate.Value.Date.Add(hour.HourFrame.Value);
        //                }
        //                else
        //                {
        //                    throw new Exception("Lỗi giờ book.");
        //                }

        //                //TH1: Nếu checkin sau giờ book
        //                if (checkinTime >= bookHour)
        //                {
        //                    WaitTime = ((DateTime)pvgTime - (DateTime)checkinTime).Minutes;

        //                }
        //                //TH2: Nếu checkin trước giờ book
        //                else
        //                {
        //                    WaitTime = ((DateTime)pvgTime - (DateTime)bookHour).Minutes;

        //                }
        //            }

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("Đã xảy ra lỗi trong quá trình thực hiện!!! " + ex.Message);
        //    }

        //    return WaitTime;
        //}

        /// <summary>
        /// Cập nhật số lượng sử dụng dịch vụ miễn phí cho khách đặc biệt
        /// </summary>
        public void updateQuantityInvited()
        {
            var db = new Solution_30shineEntities();

            // Lấy Id trong bảng khách hàng đặc biệt
            int? CusId = Convert.ToInt32(HDF_CustomerId.Value);
            var objSpecialCustomer = db.SpecialCustomers.Where(a => a.IsDelete == false && a.CustomerTypeId == 1 && a.CustomerId == CusId).OrderBy(a => a.Id).FirstOrDefault();
            //Khai báo SpecialCusDetail
            var obj = new SpecialCusDetail();
            if (objSpecialCustomer != null)
            {
                int IdSpecialCustomer = Convert.ToInt32(objSpecialCustomer.Id);
                obj = db.SpecialCusDetails.Where(a => a.IsDelete == false && a.QuantityInvited > 0 && a.SpecialCusId == IdSpecialCustomer).OrderBy(a => a.Id).FirstOrDefault();
            }
            if (obj != null)
            {
                if (obj.QuantityInvited > 0)
                {
                    obj.QuantityInvited = obj.QuantityInvited - 1;
                    db.SpecialCusDetails.AddOrUpdate(obj);
                    db.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Cập nhật khách hàng quen
        /// </summary>
        /// <param name="bill"></param>
        private void Update_CustomerFamiliar(BillService bill)
        {
            using (var db = new Solution_30shineEntities())
            {
                bool isSpecial = false;
                /// 1. Tính số lần đã sử dụng dịch vụ
                string sql = @"select COUNT(*) as times
                                from BillService
                                where IsDelete != 1 and Pending != 1
                                and ServiceIds != '' and ServiceIds is not null
                                and CreatedDate < '" + bill.CreatedDate + "'" +
                        @"and CustomerCode = '" + bill.CustomerCode + "'" +
                        @"group by CustomerCode";

                int timesUsedService = 0;
                try
                {
                    timesUsedService = db.Database.SqlQuery<int>(sql).FirstOrDefault();
                    bill.TimesUsedService = timesUsedService;
                }
                catch { }

                /// 2. Kiểm tra có phải khách quen (là khách đã dùng dịch vụ bởi stylist này ít nhất 2 lần, hoặc 1 lần nhưng là lần gần đây nhất)
                sql = @"select *
                                from BillService
                                where IsDelete != 1 and Pending != 1
                                and CustomerCode = '" + bill.CustomerCode + @"' 
                                and CustomerCode != '' and CustomerCode is not null
                                and CreatedDate < '" + String.Format("{0:yyyy/MM/dd}", bill.CreatedDate) + "'";
                var billCus = db.BillServices.SqlQuery(sql).ToList();
                var familiar = billCus.Where(w => w.Staff_Hairdresser_Id == bill.Staff_Hairdresser_Id).ToList();
                // Nếu khách đã sử dụng dịch vụ bởi stylist này ít nhất 2 lần
                if (familiar.Count > 1)
                {
                    isSpecial = true;
                }
                // hoặc dùng 1 lần nhưng là lần gần đây nhất
                else if (familiar.Count == 1)
                {
                    if (billCus[billCus.Count - 1].Staff_Hairdresser_Id == bill.Staff_Hairdresser_Id)
                    {
                        isSpecial = true;
                    }
                }

                // Cập nhật bill
                bill.IsCusFamiliar = isSpecial;
                db.BillServices.AddOrUpdate(bill);
                db.SaveChanges();
            }
        }

        /// <summary>
        /// Bind danh sách salon
        /// </summary>
        private void Bind_Salon()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Salons = db.Tbl_Salon.Where(w => w.IsDelete != 1).OrderBy(o => o.Order).ToList();
                var Key = 0;
                int integer;
                SalonId = int.TryParse(OBJ.SalonId.ToString(), out integer) ? integer : 0;

                Salon.DataTextField = "Salon";
                Salon.DataValueField = "Id";

                if (Perm_ShowSalon)
                {
                    ListItem item = new ListItem("Chọn Salon", "0");
                    Salon.Items.Insert(0, item);

                    foreach (var v in _Salons)
                    {
                        Key++;
                        item = new ListItem(v.Name, v.Id.ToString());
                        Salon.Items.Insert(Key, item);
                    }
                }
                else
                {
                    foreach (var v in _Salons)
                    {
                        if (v.Id == SalonId)
                        {
                            ListItem item = new ListItem(v.Name, v.Id.ToString());
                            Salon.Items.Insert(Key, item);
                            Salon.Enabled = false;
                        }

                    }
                }

                var ItemSelected = Salon.Items.FindByValue(OBJ.SalonId.ToString());
                if (ItemSelected != null)
                {
                    ItemSelected.Selected = true;
                }
                else
                {
                    Salon.SelectedIndex = 0;
                }
            }
        }

        /// <summary>
        /// Bind danh sách sản phẩm
        /// </summary>
        private void Bind_RptProduct()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Products = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1).OrderByDescending(o => o.Id).ToList();
                _ListProduct = _Products;
            }
        }

        /// <summary>
        /// Bind danh sách dịch vụ
        /// </summary>
        private void Bind_RptService()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Service = db.Services.Where(w => w.IsDelete != 1 && w.Publish == 1).OrderByDescending(o => o.Id).ToList();

                Rpt_Service.DataSource = _Service;
                Rpt_Service.DataBind();
            }
        }

        /// <summary>
        /// Bind danh sách dịch vụ phụ trợ như Cạo mặt, Cắt móng tay
        /// </summary>
        private void Bind_RptFreeService()
        {
            using (var db = new Solution_30shineEntities())
            {

                var _Service = db.Services.Where(w => w.IsDelete != 1 && w.Publish == 1 && w.IsFreeService == 1).OrderBy(o => o.Id).ToList();
                int index = -1;
                if (_Service.Count > 0 && lstFreeService.Count > 0)
                {
                    foreach (var v in _Service)
                    {
                        index = lstFreeService.IndexOf(v.Id);
                        if (index != -1)
                        {
                            _Service.FirstOrDefault(w => w.Id == lstFreeService[index]).Status = 1;
                        }
                    }
                }
                Rpt_FreeService.DataSource = _Service;
                Rpt_FreeService.DataBind();
            }
        }

        /// <summary>
        /// Bind danh sách dịch vụ nổi bật
        /// </summary>
        private void Bind_RptServiceFeatured()
        {
            using (var db = new Solution_30shineEntities())
            {
                var MatchInt = Regex.Match(Request.QueryString["Code"], @"[^0-9]+");
                if (!MatchInt.Success)
                {
                    var _Code = Convert.ToInt32(Request.QueryString["Code"]);
                    OBJ = db.BillServices.FirstOrDefault(w => w.Id == _Code);

                }
                var lst = db.Salon_Service_Select(OBJ.SalonId, 0).Where(p => p.IsCheck == true).ToList();
                //var lst = db.Services.Where(w => w.IsDelete != 1 && w.Publish == 1 && w.Status == 1).OrderByDescending(o => o.Id).ToList();

                Rpt_ServiceFeatured.DataSource = lst;
                Rpt_ServiceFeatured.DataBind();
            }
        }

        /// <summary>
        /// Bind danh sách sản phẩm nổi bật
        /// </summary>
        private void Bind_RptProductFeatured()
        {
            using (var db = new Solution_30shineEntities())
            {
                // Combo : ID 95
                int cateId = 95;
                var lst = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1 && w.Status == 1 && w.CategoryId == cateId).OrderByDescending(o => o.Id).ToList();
                Rpt_ProductFeatured_Combo.DataSource = lst;
                Rpt_ProductFeatured_Combo.DataBind();

                // Gôm : ID 6
                cateId = 6;
                lst = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1 && w.Status == 1 && w.CategoryId == cateId).OrderByDescending(o => o.Id).ToList();
                Rpt_ProductFeatured_Gom.DataSource = lst;
                Rpt_ProductFeatured_Gom.DataBind();

                // Sáp : ID 7
                cateId = 7;
                lst = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1 && w.Status == 1 && w.CategoryId == cateId).OrderByDescending(o => o.Id).ToList();
                Rpt_ProductFeatured_Sap.DataSource = lst;
                Rpt_ProductFeatured_Sap.DataBind();
            }
        }

        /// <summary>
        /// Reset Rating Temp Value
        /// </summary>
        private void resetRatingTemp()
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                /// Reset giá trị rating temp
                int accId = Session["User_Id"] != null ? int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0 : 0;
                int salonId = Session["SalonId"] != null ? int.TryParse(Session["SalonId"].ToString(), out integer) ? integer : 0 : 0;
                var ratingTemp = db.Rating_Temp.FirstOrDefault(w => w.SalonId == salonId && w.AccountId == accId);
                if (ratingTemp != null)
                {
                    ratingTemp.RatingValue = 0;
                    db.Rating_Temp.AddOrUpdate(ratingTemp);
                    db.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Lấy thông tin khách hàng qua mã KH
        /// </summary>
        /// <param name="CustomerCode"></param>
        /// <returns></returns>
        [WebMethod]
        public static string GetCustomer(string CustomerCode)
        {
            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var _Customer = db.Customers.Where(w => w.Customer_Code == CustomerCode).ToList();

                if (_Customer.Count > 0)
                {
                    _Msg.success = true;
                    _Msg.msg = serializer.Serialize(_Customer);
                }
                else
                {
                    _Msg.success = false;
                }
            }
            return serializer.Serialize(_Msg);
        }

        /// <summary>
        /// Kiểm tra đã tạo bill cho khách hàng trong hôm nay qua mã KH
        /// </summary>
        /// <param name="CustomerCode"></param>
        /// <returns></returns>
        [WebMethod]
        public static string CheckDuplicateBill(string CustomerCode)
        {
            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var today = Convert.ToDateTime(DateTime.Now.ToString("d/M/yyyy"), culture);
                var todayL = today.AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                var todayR = today.AddDays(1);
                var _Bill = db.BillServices.FirstOrDefault(w => w.CreatedDate > todayL && w.CreatedDate < todayR && w.CustomerCode == CustomerCode);

                if (_Bill != null)
                {
                    _Msg.success = true;
                    _Msg.msg = serializer.Serialize(_Bill);
                }
                else
                {
                    _Msg.success = false;
                }
            }
            return serializer.Serialize(_Msg);
        }

        /// <summary>
        /// Gen VIP Card - VD : 0001, 9999 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="template"></param>
        /// <returns></returns>
        private static string genVIPCard(string input, int template)
        {
            string code = "";
            int len = input.Length;
            for (var i = 0; i < template; i++)
            {
                if (len > 0)
                {
                    code = input[--len].ToString() + code;
                }
                else
                {
                    code = "0" + code;
                }
            }
            return code;
        }

        /// <summary>
        /// Kiểm tra đã tồn tại thẻ VIP Card hay chưa
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [WebMethod]
        public static string issetVIPCard(string input)
        {
            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                input = genVIPCard(input, 4);
                var card = db.BillServices.FirstOrDefault(w => w.VIPCardGive == input && w.IsDelete != 1);

                if (card != null)
                {
                    _Msg.success = true;
                    //_Msg.msg = serializer.Serialize(_Bill);
                }
                else
                {
                    _Msg.success = false;
                }
            }
            return serializer.Serialize(_Msg);
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["User_Id"] == null)
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "salonmanager", "reception", "checkin", "checkout" };
                string[] AllowSalon = new string[] { "root", "admin" };
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }
                if (Array.IndexOf(AllowSalon, Permission) != -1)
                {
                    Perm_ShowSalon = true;
                }
            }

            // Call execute function
            ExecuteByPermission();
        }

        /// <summary>
        /// Xử lý hậu check permission
        /// </summary>
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
            else if (Perm_ShowSalon)
            {
                TrSalon.Visible = true;
                //Bind_Salon();
            }
        }

        /// <summary>
        /// Lấy dữ liệu khách đặc biệt theo số điện thoại
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        [WebMethod]
        public static SpecialCustomers GetSpecialCustomer(string phone)
        {
            var db = new Solution_30shineEntities();
            var query = @"select A.CustomerTypeId from SpecialCustomer A left join Customer B on A.CustomerId = B.Id where B.Phone like N'%" + phone + "%' and A.IsDelete != 1 and B.IsDelete != 1";
            int customerType = db.Database.SqlQuery<int>(query).FirstOrDefault();
            var sql = @"select A.*, C.TypeName, B.CustomerTypeId from SpecialCusDetail A 
                        left join SpecialCustomer B on A.SpecialCusId = B.Id
                        left join CustomerType C on C.Id = B.CustomerTypeId
						left join Customer D on D.Id = B.CustomerId
                        where A.IsDelete = 0 and D.Phone like N'%" + phone + "%'";
            if (customerType == 1)
            {
                sql += @" and A.QuantityInvited > 0
                        and A.CutErrorDate = (select Min(A.CutErrorDate) from SpecialCusDetail A 
						left join SpecialCustomer B on A.SpecialCusId = B.Id
						left join Customer D on D.Id = B.CustomerId
						where  D.Phone like N'%" + phone + "%' and A.QuantityInvited > 0 and A.IsDelete = 0)";
            }
            return db.Database.SqlQuery<SpecialCustomers>(sql).SingleOrDefault();
        }

        public class billService : BillService
        { }
        public class SpecialCustomers : SpecialCusDetail
        {
            public string TypeName { get; set; }
            public int CustomerTypeId { get; set; }
        }

        /// <summary>
        /// Class sử dụng cho phần update voucher giảm giá khi thời gian chờ >= 20 phút
        /// </summary>
        public class cls_voucher_waitime
        {
            public int ID { get; set; }
            public bool isVoucher { get; set; }
            public float VoucherPercent { get; set; }
            public List<int> ServiceIDs { get; set; }
        }
    } 
}