﻿<%@ Page Language="C#" Async="true" AutoEventWireup="true" CodeBehind="Service_Pending_Complete_V2.aspx.cs" Inherits="_30shine.GUI.FrontEnd.Service.ConfigCheck.Service_Pending_Complete_V2" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>30Shine - Checkout</title>
    <link href="/Assets/css/bootstrap/bootstrap-toggle.min.css" rel="stylesheet" />
    <script src="/Assets/js/bootstrap/bootstrap-toggle.min.js"></script>
    <link href="/Assets/css/checkout.css" rel="stylesheet" />
    <script src="/assets/js/jquery.mCustomScrollbar.js"></script>
    <link href="/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
    <script src="/Assets/js/erp.30shine.com/fn.js"></script>
    <script src="/Assets/js/jquery.scannerdetection.js"></script>
    <style type="text/css">
        .btn-send {
            margin-top: 3px;
        }

        .Wait-Finish {
            height: 30px;
            width: 80px;
            color: white;
            margin-left: 37px;
            background-color: navy;
            line-height: 30px;
        }

            .Wait-Finish:hover {
                color: yellow;
            }

        .pSpecCus {
            padding-top: 7px;
            float: left;
            width: 100%;
            text-transform: uppercase;
            color: red !important;
        }

        .cusInfo {
            width: 100%;
            float: left;
            border: none;
        }

        .modal-body .cusInfo {
            float: none !important;
        }

        .pInfo {
            padding-top: 7px;
            float: left;
            width: 100%;
            color: green !important;
        }

        .fe-service .tr-product .show-product.addproduct {
            padding: 9px 12px;
            border-radius: 0;
            margin-left: 16px;
        }

        .wp_add_product {
            width: 100%;
            float: left;
            margin-top: 11px;
        }

        .customer-30shine-care {
            width: 36%;
            float: left;
            font-weight: normal;
            padding: 5px;
            cursor: pointer;
        }

            .customer-30shine-care input {
                width: auto !important;
                margin-left: 5px;
                margin-right: 5px;
            }

        .ErrorRating1 {
            font-family: 'Times New Roman', Times, serif;
            font-weight: bold;
            text-align: center;
            font-size: 20px;
            color: red
        }

        .BlockError {
            margin: 0 auto;
        }
    </style>
    <script src="/Assets/js/erp.30shine.com/service.checkout.js?822348943">
    </script>
    <script>
        <%--//var ratingTestSalon = <%= Library.Function.JavaScript.Serialize(SalonIdTest)%>;
        //console.log(ratingTestSalon);--%>
        var isAccountAdmin = <%= Perm_EditRating.ToString().ToLower() %>;

        $(document).ready(function () {
            console.log("ready!");

            var listProduct = <%= _ListProductJsonString %>;

            function findProductByBarcode(barcode) {
                barcode = (barcode + "").trim();
                for (var i = 0; i < listProduct.length; i++) {
                    var product = listProduct[i];
                    if (product.BarCode === barcode) {
                        return product.Id;
                    }
                }
                return 0;
            }
            //Barcode detection
            $(document).scannerDetection({
                timeBeforeScanTest: 200, // wait for the next character for upto 200ms
                avgTimeByChar: 40, // it's not a barcode if a character takes longer than 100ms
                preventDefault: false,

                endChar: ['\n'],
                onComplete: function (barcode, qty) {
                    var productId = findProductByBarcode(barcode);

                    if (productId !== 0) {
                        var listOption = $("#ddlProduct").find('option');
                        $(listOption).each(function (index, element) {
                            item = $(element);
                            if (item.val() == productId) {
                                item.prop('selected', true);
                                return;
                            }
                        });
                        ClickAddProduct($(this));
                    } else {
                        alert("Không tìm thấy sản phẩm!!!");
                    }
                }
            });

        });


    </script>

</asp:Content>

<asp:Content ID="ServiceAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel ID="ShowRating" runat="server" Visible="false">
        <div class="BlockError" style="width: 500px; padding-top: 70px; padding-bottom: 50px;">
            <asp:Label ID="ErrorRating" CssClass="ErrorRating1" runat="server" Text=""></asp:Label>
            <br />
            <a class="ErrorRating1" style="color: green; font-style: italic; text-decoration: underline !important;" href="<%= link %>">
                <% if (link != "")
                    { %>Click Để trở Lại Hóa đơn chưa hoàn thành<% } %></a>
        </div>
    </asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Dịch vụ &nbsp;&#187; </li>
                        <li class="li-listing">
                            <a href="/dich-vu/danh-sach.html">Danh sách</a>
                        </li>
                        <li class="li-pending">
                            <a href="/dich-vu/pending.html">
                                <div class="pending-1"></div>
                                Pending
                            </a>
                        </li>
                        <li class="li-pending-complete">
                            <a href="/dich-vu/pending.html">
                                <div class="pending-1"></div>
                                Hoàn tất
                            </a>
                        </li>
                        <%--<li class="li-add active"><a href="/dich-vu/them-phieu-v3.html">Thêm mới</a></li>--%>
                        <%--<li class="li-add active"><a href="/dich-vu/dat-lich.html">Đặt lịch</a></li>--%>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add admin-product-add fe-service">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <!-- System Message -->
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>

                <!-- END System Message -->

                <div class="table-wp">
                    <table class="table-add admin-product-table-add fe-service-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td>
                                    <strong>Thông tin dịch vụ</strong>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-2 left">
                                    <span>Khách hàng</span>
                                </td>
                                <td class="col-xs-10 right">
                                    <% if (!CusNoInfor)
                                        { %>
                                    <div class="field-wp">
                                        <asp:TextBox ID="CustomerName" runat="server" ClientIDMode="Static" ReadOnly="true" Style="width: 30%;"></asp:TextBox>
                                        <asp:TextBox ID="CustomerCode" runat="server" ClientIDMode="Static" ReadOnly="true" CssClass="mgl" placeholder="Nhập mã khách hàng ở đây" Style="width: 30%;"></asp:TextBox>
                                        <label class="customer-30shine-care">
                                            <input type="checkbox" id="cbCustomer30ShineCare" runat="server" />
                                            Khách bảo hành dịch vụ theo 30Shine Care
                                        </label>
                                        <div class="field cusInfo">
                                            <p class="pSpecCus">
                                            </p>
                                        </div>
                                        <div class="field cusInfo">
                                            <p class="pNote">
                                            </p>
                                        </div>
                                        <div class="field cusInfo">
                                            <p class="pInfo">
                                            </p>
                                        </div>
                                        <div class="field cusInfo">
                                            <p class="pExam">
                                            </p>
                                        </div>
                                        <div class="item-vipcard" onclick="activeVIPCard($(this))" style="display: none;">
                                            <div class="left">
                                                <i aria-hidden="true" class="fa fa-square-o"></i>
                                            </div>
                                            <div class="right">
                                                <input type="text" placeholder="Mã thẻ tặng" disabled="disabled" runat="server" id="VIPGive" onkeydown="validateVIPCard($(this),event)" onchange="validateVIPCardValue($(this), event)" onblur="validateVIPCardValue($(this), event)" clientidmode="Static" />
                                            </div>
                                        </div>
                                        <div class="item-vipcard" onclick="activeVIPCard($(this))" style="display: none;">
                                            <div class="left">
                                                <i aria-hidden="true" class="fa fa-square-o"></i>
                                            </div>
                                            <div class="right">
                                                <input type="text" placeholder="Mã thẻ dùng" disabled="disabled" runat="server" id="VIPUse" onkeydown="validateVIPCard($(this),event)" onchange="validateVIPCardValue($(this), event)" onblur="validateVIPCardValue($(this), event)" clientidmode="Static" />
                                            </div>
                                        </div>
                                    </div>
                                    <% }
                                        else
                                        { %>
                                    <span class="field-wp">
                                        <span class="checkbox cus-no-infor">
                                            <label>
                                                <input type="checkbox" checked="checked" disabled="disabled" id="InputCusNoInfor" runat="server" clientidmode="Static" />
                                                Khách không cho thông tin
                                            </label>
                                        </span>
                                    </span><% } %>
                                </td>
                            </tr>

                            <tr class="tr-field-ahalf tr-product">
                                <td class="col-xs-2 left">
                                    <span>Dịch vụ</span>
                                </td>
                                <td class="col-xs-10 right">
                                    <% if (!CusNoInfor)
                                        { %>
                                    <div class="row" id="quick-service">
                                        <asp:Repeater ID="Rpt_ServiceFeatured" runat="server">
                                            <ItemTemplate>
                                                <div class="checkbox cus-no-infor">
                                                    <label class="lbl-cus-no-infor">
                                                        <input type="checkbox" data-code="<%# Eval("Code") %>" onclick="pushQuickData($(this), 'service', $('#HDF_CustomerType').val(), $('#HDF_DiscountServices').val()), CalCampaignService($(this))" />
                                                        <%# Eval("Service") %>
                                                    </label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <%--    <div class="show-product show-item" data-item="service"><i class="fa fa-plus-circle"></i>Thêm dịch vụ</div>--%>
                                    </div>
                                    <div id="ListingServiceWp" runat="server" class="listing-product item-service" clientidmode="Static">
                                        <table id="table-item-service" class="table table-listing-product table-item-service">
                                            <thead>
                                                <tr>
                                                    <th>STT</th>
                                                    <th>Tên dịch vụ</th>
                                                    <th>Mã dịch vụ</th>
                                                    <th>Đơn giá</th>
                                                    <th>Số lượng</th>
                                                    <th>Giảm giá</th>
                                                    <th>Thành tiền</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="Rpt_Service_Bill" runat="server">
                                                    <ItemTemplate>
                                                        <tr data-serviceid='<%# Eval("Id") %>'>
                                                            <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                                            <td class="td-product-name"><%# Eval("Name") %></td>
                                                            <td class="td-product-code" data-id='<%# Eval("Id") %>'><%# Eval("Code") %></td>
                                                            <td class="td-product-price" data-price='<%# Eval("Price") %>'><%# Eval("Price") %></td>
                                                            <td class="td-product-quantity">
                                                                <input type="text" class="product-quantity" value="<%# Eval("Quantity") %>" />
                                                            </td>
                                                            <td class="td-product-voucher" data-voucher='<%# Eval("VoucherPercent") %>'>
                                                                <div class="row">
                                                                    <input type="text" class="product-voucher voucher-services" value="<%# Eval("VoucherPercent") %>" style="margin: 0 auto; float: none; text-align: center; width: 50px;" />
                                                                    %
                                                                </div>
                                                            </td>
                                                            <td class="map-edit">
                                                                <div class="box-money" style="display: block;">
                                                                    <%--<%# Convert.ToInt32(Eval("Quantity")) * Convert.ToInt32(Eval("Price")) * 
                                                                    (100-Convert.ToInt32(Eval("VoucherPercent")))/100 %>--%>
                                                                    <%# String.Format("{0:#,###}", Convert.ToInt32(Eval("Price")) * Convert.ToInt32(Eval("Quantity")) * ((Convert.ToDouble(100 - Convert.ToInt32(Eval("VoucherPercent")))) / 100)).Replace(",", ".") == "" ? "0"
                                                : String.Format("{0:#,###}", Convert.ToInt32(Eval("Price")) * Convert.ToInt32(Eval("Quantity")) * ((Convert.ToDouble(100 - Convert.ToInt32(Eval("VoucherPercent")))) / 100)).Replace(",", ".") %>
                                                                </div>
                                                                <div class="edit-wp">
                                                                    <a class="elm del-btn" href="javascript://" onclick="RemoveItem($(this).parent().parent().parent(),'<%# Eval("Name") %>' ,'<%# Eval("Id") %>','service')" title="Xóa"></a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                        <div class="MKT-label">
                                        </div>
                                    </div>
                                    <div class="row free-service">
                                        <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                            <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                Phụ trợ :
                                            </label>
                                        </div>
                                        <asp:Repeater ID="Rpt_FreeService" runat="server">
                                            <ItemTemplate>
                                                <div class="checkbox cus-no-infor" style="padding-bottom: 4px;">
                                                    <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                        <input type="checkbox" onclick="pushFreeService($(this), <%# Eval("Id") %>)" data-id="<%# Eval("Id") %>"
                                                            <%# Eval("Status").ToString() == "1" ? "checked='checked'" : "" %> />
                                                        <%# Eval("Name") %>
                                                    </label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </td>
                                <% } %>
                            </tr>

                            <tr class="tr-field-ahalf tr-field-third">
                                <td class="col-xs-2 left">
                                    <span>Nhân viên</span>
                                </td>
                                <td class="col-xs-10 right">
                                    <div class="field-wp">

                                        <%--    <div class="show-product show-item" data-item="service"><i class="fa fa-plus-circle"></i>Thêm dịch vụ</div>--%>
                                        <span id="ValidateCosmetic" class="fb-cover-error" style="visibility: hidden; top: -28px; width: 255px;">Bạn chưa chọn nhân viên bán mỹ phẩm! </span>
                                        <div class="filter-item" style="width: 20%;">
                                            <asp:TextBox ID="InputStylist" runat="server" AutoCompleteType="Disabled" ClientIDMode="Static" CssClass="st-head ip-short eb-select eb-suggestion staff-auto-selection" data-staff-type="Stylist" data-field="Hairdresser" data-value="0" placeholder="Stylist"></asp:TextBox>
                                            <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                                                <ul id="Ul1" class="ul-listing-staff ul-listing-suggestion">
                                                </ul>
                                            </div>
                                            <div id="FVStylist" runat="server" class="fake-value" clientidmode="Static">
                                            </div>
                                        </div>
                                        <div class="filter-item" style="width: 20%;">
                                            <asp:TextBox ID="InputSkinner" runat="server" AutoCompleteType="Disabled" ClientIDMode="Static" CssClass="st-head ip-short eb-select eb-suggestion staff-auto-selection" data-staff-type="Skinner" data-field="HairMassage" data-value="0" placeholder="Skinner"></asp:TextBox>
                                            <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                                                <ul id="Ul2" class="ul-listing-staff ul-listing-suggestion">
                                                </ul>
                                            </div>
                                            <div id="FVSkinner" runat="server" class="fake-value" clientidmode="Static">
                                            </div>
                                        </div>
                                        <div class="filter-item" style="width: 20%;">
                                            <asp:TextBox ID="InputSeller" runat="server" AutoCompleteType="Disabled" ClientIDMode="Static" CssClass="st-head ip-short eb-select eb-suggestion staff-auto-selection" data-staff-type="Seller" data-field="Cosmetic" data-value="0" placeholder="Bán mỹ phẩm"></asp:TextBox>
                                            <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                                                <ul id="Ul3" class="ul-listing-staff ul-listing-suggestion">
                                                </ul>
                                            </div>
                                            <div id="FVSeller" runat="server" class="fake-value" clientidmode="Static">
                                            </div>
                                        </div>
                                        <div class="filter-item" style="width: 20%;">
                                            <asp:TextBox ID="InputReception" runat="server" AutoCompleteType="Disabled" ClientIDMode="Static" CssClass="st-head ip-short eb-select eb-suggestion staff-auto-selection" data-staff-type="Checkin" data-field="HDF_Reception" data-value="0" placeholder="Checkin"></asp:TextBox>
                                            <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                                                <ul id="Ul4" class="ul-listing-staff ul-listing-suggestion">
                                                </ul>
                                            </div>
                                            <div id="FVReception" runat="server" class="fake-value" clientidmode="Static">
                                            </div>
                                        </div>
                                        <div class="filter-item" style="width: 20%;">
                                            <asp:TextBox ID="InputCheckout" runat="server" AutoCompleteType="Disabled" ClientIDMode="Static" CssClass="st-head ip-short eb-select eb-suggestion staff-auto-selection" data-staff-type="Checkout" data-field="HDF_Checkout" data-value="0" placeholder="Checkout"></asp:TextBox>
                                            <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                                                <ul id="Ul5" class="ul-listing-staff ul-listing-suggestion">
                                                </ul>
                                            </div>
                                            <div id="FVCheckout" runat="server" class="fake-value" clientidmode="Static">
                                            </div>
                                        </div>
                                    </div>

                                </td>
                            </tr>

                            <tr class="tr-field-ahalf tr-product">
                                <td class="col-xs-2 left">
                                    <span>Sản phẩm</span>
                                </td>
                                <td class="col-xs-10 right">
                                    <div class="row" id="quick-product">
                                        <div class="col-xs-12">
                                            <div class="row" style="margin-left: -15px; margin-right: -15px;">
                                                <div class="col-xs-4">
                                                    <asp:Repeater ID="Rpt_ProductFeatured_Combo" runat="server">
                                                        <ItemTemplate>
                                                            <div class="checkbox cus-no-infor">
                                                                <label class="lbl-cus-no-infor">
                                                                    <!--thêm Mapid-->
                                                                    <input type="checkbox" data-id="<%# Eval("Id") %>" data-code="<%# Eval("Code") %>" data-mapid="<%#Eval("MapIdProduct") %>" data-price="<%#Eval("Price") %>" data-discount="<%#Eval("VoucherPercent") != null ? Eval("VoucherPercent") : 0 %>" data-cate="<%#Eval("CategoryId") %>" data-name="<%# Eval("Name") %>" onclick="pushQuickproductData($(this))" />
                                                                    <%# Eval("Name") %>
                                                                </label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </div>
                                                <div class="col-xs-3">
                                                    <asp:Repeater ID="Rpt_ProductFeatured_Gom" runat="server">
                                                        <ItemTemplate>
                                                            <div class="checkbox cus-no-infor">
                                                                <label class="lbl-cus-no-infor">
                                                                    <input type="checkbox" data-id="<%# Eval("Id") %>" data-code="<%# Eval("Code") %>" data-price="<%#Eval("Price") %>" data-discount="<%#Eval("VoucherPercent") != null ? Eval("VoucherPercent") : 0 %>" data-cate="<%#Eval("CategoryId") %>" data-name="<%# Eval("Name") %>" onclick="pushQuickproductData($(this))" />
                                                                    <%# Eval("Name") %>
                                                                </label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </div>
                                                <div class="col-xs-5">
                                                    <asp:Repeater ID="Rpt_ProductFeatured_Sap" runat="server">
                                                        <ItemTemplate>
                                                            <div class="checkbox cus-no-infor">
                                                                <label class="lbl-cus-no-infor">
                                                                    <input type="checkbox" data-id="<%# Eval("Id") %>" data-code="<%# Eval("Code") %>" data-price="<%#Eval("Price") %>" data-discount="<%#Eval("VoucherPercent") != null ? Eval("VoucherPercent") : 0 %>" data-cate="<%#Eval("CategoryId") %>" data-name="<%# Eval("Name") %>" onclick="pushQuickproductData($(this))" />
                                                                    <%# Eval("Name") %>
                                                                </label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="wp_add_product">
                                        <div class="wp_ddproduct">
                                            <select id="ddlProduct" class="select2">
                                                <option data-cate="0" data-code="0" data-id="0" data-price="0" value="0">Chọn sản phẩm ...</option>
                                                <% foreach (var item in _ListProduct)
                                                    { %>
                                                <!--thêm Mapid-->
                                                <option data-cate="<%= item.CategoryId %>" data-code="<%= item.Code %>" data-barcode="<%= item.BarCode %>" data-id="<%= item.Id %>" data-mapid="<%= item.MapIdProduct %>" data-name="<%= item.Name %>" data-price="<%= item.Price %>" data-discount="<%= item.VoucherPercent %>" value="<%= item.Id %>"><%= item.Name %></option>
                                                <% } %>
                                            </select>
                                        </div>
                                        <div id="AddProduct" class="show-product  addproduct" data-code="" data-id="0" onclick="ClickAddProduct($(this))">
                                            <i class="fa fa-plus-circle"></i>Thêm sản phẩm
                                        </div>
                                    </div>
                                    <div id="ListingProductWp" runat="server" class="listing-product item-product" clientidmode="Static">
                                        <table id="table-item-product" class="table table-listing-product table-item-product">
                                            <thead>
                                                <tr>
                                                    <th>STT</th>
                                                    <th>Tên sản phẩm</th>
                                                    <th>Mã sản phẩm</th>
                                                    <th style="display: none;"></th>
                                                    <th>Đơn giá</th>
                                                    <th>Số lượng</th>
                                                    <th>Giảm giá</th>
                                                    <th>Thành tiền</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbodyProduct">
                                                <asp:Repeater ID="Rpt_Product_Bill" runat="server">
                                                    <ItemTemplate>
                                                        <tr data-cate='<%#Eval("CategoryId") %>'>
                                                            <td class="td-product-index">1</td>
                                                            <td class="td-product-name"><%#Eval("Name") %></td>
                                                            <td class="td-product-code" data-code='<%#Eval("Code") %>' data-id='<%#Eval("Id") %>'><%#Eval("Code") %></td>
                                                            <!--thêm Mapid-->
                                                            <%--<td class="td-product-map" data-mapid='<%#Eval("MapIdProduct") %>' style="display: none;"><%#Eval("MapIdProduct") %></td>--%>
                                                            <td class="td-product-price" data-price='<%#Eval("Price") %>'><%#Eval("Price") %></td>
                                                            <td class="td-product-quantity">
                                                                <input class="product-quantity" type="text" value='<%#Eval("Quantity") %>'>
                                                                </input>
                                                            </td>
                                                            <td class="td-product-voucher" data-voucher="0">
                                                                <div class="row">
                                                                    <input class="product-voucher voucher-cosmetic" style="margin: 0 auto; float: none; text-align: center; width: 50px;" type="text" value='<%#Eval("VoucherPercent") %>'>%
                                                                    </input>
                                                                </div>
                                                            </td>
                                                            <td class="map-edit">
                                                                <%--  <div class="box-money" style="display: block;"><%# string.Format("{0:#,0.##}",Eval("Price")) %></div>--%>
                                                                <div class="box-money" style="display: block;">
                                                                    <%--   <%# (Eval("Price") != null && Eval("Quantity") != null) ? string.Format("{0:#,###}",Convert.ToInt32(Eval("Price")) * Convert.ToInt32(Eval("Quantity")) * ((Convert.ToDouble(100 - Convert.ToInt32(Eval("VoucherPercent")))) / 100)).Replace(",",".") : "0" %>--%>
                                                                    <%# String.Format("{0:#,###}", Convert.ToInt32(Eval("Price")) * Convert.ToInt32(Eval("Quantity")) * ((Convert.ToDouble(100 - Convert.ToInt32(Eval("VoucherPercent")))) / 100)).Replace(",", ".") == "" ? "0"
                                            : String.Format("{0:#,###}", Convert.ToInt32(Eval("Price")) * Convert.ToInt32(Eval("Quantity")) * ((Convert.ToDouble(100 - Convert.ToInt32(Eval("VoucherPercent")))) / 100)).Replace(",", ".") %>
                                                                </div>
                                                                <div class="edit-wp">
                                                                    <a class="elm del-btn" href="javascript://" onclick="RemoveItem($(this).parent().parent().parent(),'<%#Eval("Name") %>','product')" title="Xóa"></a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                    </div>

                                </td>
                            </tr>

                            <tr class="tr-description tr-field-ahalf">
                                <td class="col-xs-2 left">
                                    <span>Tổng số tiền</span>
                                </td>
                                <td class="col-xs-10 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="TotalMoney" runat="server" ClientIDMode="Static" Text="0" ReadOnly="true"></asp:TextBox>
                                        <span class="unit-money">VNĐ</span>
                                        <label class="label-paybycard">
                                            <asp:CheckBox runat="server" ID="PayByCard" ClientIDMode="Static" />
                                            <span>Thanh toán qua thẻ</span>
                                        </label>
                                    </span>
                                </td>
                            </tr>

                            <%--<tr class="tr-upload" style="display:none;">
                        <td class="col-xs-2 left"><span>Đăng ảnh</span></td>
                        <td class="col-xs-9 right">
                            <div class="wrap btn-upload-wp">
                                <div id="Btn_UploadImg" class="btn-upload-img" onclick="OpenIframeImage()">Chọn ảnh đăng</div>
                                <div class="wrap listing-img-upload"></div>
                            </div>                         
                        </td>
                    </tr>--%>

                            <tr class="tr-description">
                                <td class="col-xs-2 left">
                                    <span>Ghi chú</span>
                                </td>
                                <td class="col-xs-10 right">
                                    <span class="field-wp">
                                        <asp:TextBox TextMode="MultiLine" Rows="3" ID="Description" runat="server" ClientIDMode="Static" Style="width: 49%;"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-rating tr-field-ahalf">
                                <td class="col-xs-2 left">
                                    <span>ĐIỂM RATING</span>
                                </td>
                                <td class="col-xs-10 right">
                                    <div class="field-wp <% if (accountModel.IsCheckin() || accountModel.IsCheckout())
                                        { %>hidden <% } %>">

                                        <div class="rating-icon-wrap" style="float: left; width: 100%; margin-bottom: 10px;">

                                            <div class="rating-icon icon-happy" onclick="excRating($(this), 3)">Rất hài lòng</div>
                                            <div class="rating-icon icon-normal" onclick="excRating($(this), 2)">Hài lòng</div>
                                            <div class="rating-icon icon-sad" onclick="excRating($(this), 1)">Chưa hài lòng</div>

                                            <%--<div class="rating-icon icon-happy">Rất Hài lòng</div>                                    
                                    <div class="rating-icon icon-normal">Cũng được</div>                                    
                                    <div class="rating-icon icon-sad">Không hài lòng</div>--%>
                                        </div>

                                    </div>
                                    <div>

                                        <input type="checkbox" id="CkRating" style="float: left; width: auto; margin-right: 10px" />
                                        <div class="rating-icon-wrap1" style="color: red; float: left; width: auto">
                                        </div>


                                    </div>

                                    <%-- <div id="not-pleasure-reason" style="display:none;">
                                        <p>Lý do khách chọn Chưa hài lòng</p>
                                        <ul class="not-pleasure-reason-wp col-sm-5" style="list-style: none">
                                            <% int counterForSplitCol = 0; List<int> excludedID = new List<int>(); foreach (var rating in ServiceRating)
                                                { %>
                                            <% 
                                                if (ServiceRating.Count() / 2 <= counterForSplitCol)
                                                {
                                                    break;
                                                }
                                                excludedID.Add(rating.Id);
                                            %>
                                            <li>
                                                <div class="checkbox">
                                                    <span class="input-checkbox">
                                                        <input type="checkbox" checked="checked" value="<%= rating.Id %>">
                                                        <label><%= rating.Title %></label>
                                                    </span>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </div>
                                            </li>

                                            <% counterForSplitCol++;
                                                } %>
                                        </ul>

                                        <ul class="not-pleasure-reason-wp col-sm-5" style="list-style: none">
                                            <% foreach (var rating in ServiceRating)
                                                { %>
                                            <% if (excludedID.Contains(rating.Id)) { continue; } %>
                                            <li>
                                                <div class="checkbox">
                                                    <span class="input-checkbox">
                                                        <input type="checkbox" value="<%= rating.Id %>">
                                                        <label><%= rating.Title %></label>
                                                    </span>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </div>
                                            </li>

                                            <% } %>
                                        </ul>

                                        <style>
                                            #not-pleasure-reason {
                                                clear: left;
                                                padding: 20px 10px;
                                                display: none;
                                            }

                                            .not-pleasure-reason > input {
                                                width: 30px;
                                            }

                                            ul.not-pleasure-reason-wp {
                                                list-style: none;
                                                padding: 5px;
                                                margin: 10px 0 15px;
                                            }

                                                ul.not-pleasure-reason-wp > li {
                                                    display: block;
                                                    padding: 3px 6px;
                                                    border: 1px solid #ddd;
                                                    margin-top: 3px;
                                                    cursor: pointer;
                                                    -webkit-touch-callout: none;
                                                    -webkit-user-select: none;
                                                    -khtml-user-select: none;
                                                    -moz-user-select: none;
                                                    -ms-user-select: none;
                                                    user-select: none;
                                                }

                                                    ul.not-pleasure-reason-wp > li:hover, ul.not-pleasure-reason-wp > li.selected {
                                                        border-color: #ddd;
                                                        background: #EFEFEF;
                                                    }

                                                ul.not-pleasure-reason-wp li > div.checkbox {
                                                }

                                                ul.not-pleasure-reason-wp li input[type="checkbox"] {
                                                    width: 30px !important;
                                                }

                                                ul.not-pleasure-reason-wp li div.checkbox label {
                                                    display: inline-block;
                                                    vertical-align: middle;
                                                    margin-top: -18px;
                                                    font-size: 100%;
                                                }

                                            .hidden {
                                                display: none;
                                            }
                                        </style>
                                    </div>--%>
                                </td>
                            </tr>

                            <tr class="tr-field-ahalf" runat="server" id="TrSalon" visible="false">
                                <td class="col-xs-2 left">
                                    <span>Salon</span>
                                </td>
                                <td class="col-xs-10 right">
                                    <span class="field-wp">
                                        <asp:DropDownList ID="Salon" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                        <asp:RequiredFieldValidator InitialValue="0"
                                            ID="ValidateSalon" Display="Dynamic"
                                            ControlToValidate="Salon"
                                            runat="server" Text="Bạn chưa chọn Salon!"
                                            ErrorMessage="Vui lòng chọn Salon!"
                                            ForeColor="Red"
                                            CssClass="fb-cover-error">
                                        </asp:RequiredFieldValidator>
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-upload">
                                <td class="col-xs-2 left">
                                    <span>Ảnh KH</span>
                                </td>
                                <td class="col-xs-7 right">
                                    <div class="wrap btn-upload-wp">
                                        <div id="Btn_UploadImg" class="btn-upload-img" onclick="  /*OpenIframeImage()*/" style="display: none;">Chọn ảnh đăng</div>
                                        <asp:FileUpload ID="UploadFile" ClientIDMode="Static" Style="display: none;"
                                            CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                        <div class="wrap listing-img-upload">
                                            <% if (HasImages)
                                                { %>
                                            <% for (var i = 0; i < ListImagesName.Length; i++)
                                                { %>
                                            <div class="thumb-wp" ondblclick="Exc_cropImage($(this), '<%= ListImagesName[i] %>', true)">
                                                <img class="thumb" alt="" title="" src="<%= ListImagesName[i] %>"
                                                    data-img="<%= ListImagesName[i] %>" />
                                                <span class="delete-thumb" onclick="deleteThum($(this), '<%= ListImagesName[i] %>', true)" style="display: none;"></span>
                                            </div>
                                            <% } %>
                                            <% } %>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <%--    <tr>
                                <td class="col-xs-1 left"><span style="line-height: 18px;"></span></td>
                                <td class="col-xs-11 right">
                                    <div class="row free-service">
                                        <div class="checkbox cus-no-infor" style="padding-bottom: 4px; color: green;">
                                            <label class="lbl-cus-no-infor" style="color: red; font-family: Roboto Condensed Bold;">
                                                <asp:CheckBox runat="server" ID="isX2" ClientIDMode="Static" />
                                                Tăng ca
                                            </label>
                                        </div>
                                    </div>
                                </td>
                            </tr>--%>

                            <tr class="tr-send">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-10 right no-border" style="padding-top: 10px;">

                                    <span class="field-wp">
                                        <asp:Panel ID="BtnSend" CssClass="btn-send" runat="server" ClientIDMode="Static"
                                            onclick="//clientScriptBtnSend();">
                                            Hoàn Tất
                                        </asp:Panel>
                                        <asp:Button ID="BtnFakeSend" runat="server" Text="Hoàn tất"
                                            ClientIDMode="Static" OnClick="CompleteService" Style="display: none;"></asp:Button>
                                        <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>
                                    </span>
                                    <span>

                                        <asp:Button ClientIDMode="Static" CssClass="btn-send warning" Style="padding-left: 10px; padding-right: 10px; margin-left: 15px; width: auto !important;" Width="80px" Height="30px" ID="WaitFinish" runat="server" Text="Đóng bill nhầm" OnClick="WaitFinish_Click" />

                                    </span>
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
                        </tbody>
                    </table>

                    <!-- input hidden -->
                    <asp:HiddenField ID="HDF_CustomerId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_CustomerCode" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_ProductIds" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_ServiceIds" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_TotalMoney" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_UserImages" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_UserImagesDelete" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="Hairdresser" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HairMassage" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="Cosmetic" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_SalonId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_lgId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_BillId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_FreeService" ClientIDMode="Static" runat="server" />
                    <asp:HiddenField ID="HDF_Rating" ClientIDMode="Static" runat="server" />
                    <asp:HiddenField ID="HDF_Reception" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_Checkout" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_CustomerType" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_DiscountServices" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_DiscountCosmetic" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="NotPleasureReasonList" runat="server" ClientIDMode="Static" />
                    <!-- END input hidden -->
                </div>
            </div>
            <%-- end Add --%>
        </div>

        <!-- Danh mục sản phẩm -->
        <%--<div class="popup-product-wp popup-product-item">--%>
        <%-- <div class="wp popup-product-head">
        <strong>Danh mục sản phẩm</strong>
    </div>
    <div class="wp popup-product-content" >
        <div class="wp popup-product-guide">
           
            <label style="font-family: Roboto Condensed Bold; font-weight: normal; font-size: 14px; color: #222222; margin-right: 10px;">Chọn danh mục</label>
            <select onchange="showProductByCategory($(this))" id="ddlProductCategory" style="color: #222222; font-size: 14px; padding: 0 5px;">
                <asp:Repeater runat="server" ID="rptProductCategory">
                    <ItemTemplate>
                        <option value="<%# Eval("Id") %>" style="color: #222222; font-size: 14px;"><%# Eval("Name") %></option>
                    </ItemTemplate>
                </asp:Repeater>
            </select>
        </div>
        <div class="wp listing-product item-product">
            <table class="table" id="table-product">
                <thead>
                    <tr>
                        <th><input type="checkbox" /></th>
                        <th>STT</th>
                        <th>Tên sản phẩm</th>
                        <th>Mã sản phẩm</th>
                        <th>Đơn giá</th>
                        <th class="th-product-quantity">Số lượng</th>
                        <th>Giảm giá</th>
                        <th>Thành tiền</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="Rpt_Product" runat="server">
                        <ItemTemplate>
                            <tr data-cate="<%# Eval("CategoryId") %>">
                                <td class="td-product-checkbox item-product-checkbox">
                                    <input type="checkbox" value="<%# Eval("Id") %>"
                                        data-code="<%# Eval("Code") %>" data-name="<%# Eval("Name") %>"/>
                                </td>
                                <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                <td class="td-product-name"><%# Eval("Name") %></td>
                                <td class="td-product-code" data-id="<%# Eval("Id") %>" data-code="<%# Eval("Code") %>"><%# Eval("Code") %></td>
                                <td class="td-product-price" data-price="<%# Eval("Price") %>"><%# Eval("Price") %></td>
                                <td class="td-product-quantity">
                                    <input type="text" class="product-quantity" value="1" />                                
                                </td>
                                <td class="td-product-voucher" data-voucher="<%# Eval("VoucherPercent") %>">
                                    <div class="row">
                                        <input type="text" class="product-voucher voucher-cosmetic" value="<%# Eval("VoucherPercent") %>" style="margin: 0 auto; float: none; text-align: center;width: 50px;" /> %
                                    </div>
                                    <div class="row promotion-money hidden">
                                        <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                            <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                <input type="checkbox" class="item-promotion" data-value="35000" data-id="1" style="margin-left: 0;" /> - 35.000 VNĐ </label>

                                        </div><br />
                                        <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                            <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                <input type="checkbox" class="item-promotion" data-id="2" data-value="50000" style="margin-left: 0;" /> - 50.000 VNĐ </label>

                                        </div>
                                    </div>
                                </td>
                                <td class="map-edit">
                                    <div class="box-money"></div>
                                    <div class="edit-wp">
                                        <a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),'<%# Eval("Name") %>','product')"
                                             href="javascript://" title="Xóa"></a>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>                                        
                </tbody>
            </table>
        </div> 
        <div class="wp btn-wp">
            <div class="popup-product-btn btn-complete" data-item="product">Hoàn tất</div>
            <div class="popup-product-btn btn-esc">Thoát</div>
        </div>
    </div>    
</div>--%>
        <!-- END Danh mục sản phẩm -->

        <!-- Danh mục dịch vụ -->
        <div class="popup-product-wp popup-service-item">
            <div class="wp popup-product-head">
                <strong>Danh mục dịch vụ</strong>
            </div>
            <div class="wp popup-product-content">
                <div class="wp popup-product-guide">
                    <div class="left">Hướng dẫn:</div>
                    <div class="right">
                        <p>- Click vào ô check box để chọn dịch vụ</p>
                        <p>- Click vào ô số lượng để thay đổi số lượng dịch vụ</p>
                    </div>
                </div>
                <div class="wp listing-product item-product">
                    <table class="table" id="table-service">
                        <thead>
                            <tr>
                                <th>
                                    <input type="checkbox" />
                                </th>
                                <th>STT</th>
                                <th>Tên dịch vụ</th>
                                <th>Mã dịch vụ</th>
                                <th>Đơn giá</th>
                                <th class="th-product-quantity">Số lượng</th>
                                <th>Giảm giá</th>
                                <th>Thành tiền</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="Rpt_Service" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td class="td-product-checkbox item-product-checkbox">
                                            <input type="checkbox" value="<%# Eval("Id") %>"
                                                data-code="<%# Eval("Code") %>" data-name="<%# Eval("Name") %>" />
                                        </td>
                                        <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                        <td class="td-product-name"><%# Eval("Name") %></td>
                                        <td class="td-product-code" data-id="<%# Eval("Id") %>" data-code="<%# Eval("Code") %>"><%# Eval("Code") %></td>
                                        <td class="td-product-price" data-price="<%# Eval("Price") %>"><%# Eval("Price") %></td>
                                        <td class="td-product-quantity">
                                            <input type="text" class="product-quantity" value="1" />
                                        </td>
                                        <td class="td-product-voucher" data-voucher="<%# Eval("VoucherPercent") %>">
                                            <div class="row">
                                                <input type="text" class="product-voucher" value="<%# Eval("VoucherPercent") %>" style="margin: 0 auto; float: none; text-align: center; width: 50px;" />
                                            </div>

                                        </td>
                                        <td class="map-edit">
                                            <div class="box-money"></div>
                                            <div class="edit-wp">
                                                <a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(), '<%# Eval("Name") %>', 'service')"
                                                    href="javascript://" title="Xóa"></a>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
                <div class="wp btn-wp">
                    <div class="popup-product-btn btn-complete" data-item="service">Hoàn tất</div>
                    <div class="popup-product-btn btn-esc">Thoát</div>
                </div>
            </div>
        </div>
        <!-- END Danh mục dịch vụ -->

        <!-- Modal -->
        <div id="modal-page" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Modal Header</h4>
                    </div>
                    <div class="modal-body">
                        <p>Some text in the modal.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

        <!-- Popup voucher -->
        <div class="popup-voucher">
            <div class="popup-voucher-content">
                <div class="row">
                    <p class="voucher-p1">LỄ TÂN CHECKOUT CHÚ Ý PHÁT VOUCHER CHO KHÁCH HÀNG TRƯỚC KHI NHẬN TIỀN THANH TOÁN</p>
                    <p class="voucher-p2"><span>(*) Lưu ý</span> : Công ty sẽ gọi điện kiểm tra ngẫu nhiên để đảm bảo tất cả Khách Hàng đều được nhận voucher</p>
                </div>
                <div class="row">
                    <div class="yn-wp">
                        <div class="yn-elm yn-no" onclick="autoCloseEBPopup();">OK</div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Popup rating -->
        <div class="popup-rating eb-popup " style="display: none; width: 350px;">
            <div class="">
                <div class="confirm-yn-content">
                    <div class="row">
                        <p class="rating-p1">Khách không đánh giá, bill sẽ được hoàn tất với mức điểm tương đương Hài lòng.</p>
                        <p class="rating-p2">Trưởng salon gọi cho khách hỏi sau!</p>
                    </div>
                    <div class="row">
                        <div class="yn-wp">
                            <div class="yn-elm yn-no" onclick="autoCloseEBPopup();">OK</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Popup rating -->
        <div class="popup-staff-input-error eb-popup " style="display: none; width: 500px;">
            <div class="">
                <div class="confirm-yn-content">
                    <div class="row" id="contentStaffInputError">
                    </div>
                    <div class="row">
                        <div class="yn-wp">
                            <div class="yn-elm yn-no" onclick="autoCloseEBPopup();">OK</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <style>
            .popup-rating .yn-elm.yn-no, .popup-staff-input-error .yn-elm.yn-no {
                width: 60px;
                height: 30px;
                line-height: 30px;
                background: #dfdfdf;
                float: none;
                cursor: pointer;
                text-align: center;
                margin-top: 15px;
                margin: 25px auto 0;
                background: #50b347;
                color: white;
            }
        </style>


        <!-- Modal -->
        <div class="modal fade" id="myModalRating" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        ...
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Loading-->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js">
        </script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }

            #Salon {
                width: 285px !important;
            }
        </style>
        <script>

            var _accountModel = <%= accountModel.ToJson() %>;
            _accountModel.IsCheckout = "<%= accountModel.IsCheckout() %>".toUpperCase() === "TRUE";
            _accountModel.IsCheckin = "<%= accountModel.IsCheckin() %>".toUpperCase() === "TRUE";
            var domainApiBillService = '<%= Libraries.AppConstants.URL_API_BILL_SERVICE %>';
            var URL_API_PUSH_NOTIC = "<%= Libraries.AppConstants.URL_API_PUSH_NOTICE %>";
            var billId = <%= billID %>;
            //Push noti Rating
            $("#WaitFinish").click(function () {
                closeAppRating()
            });
            //Biến đếm số lần gọi hàm CalCampaignService -- sử dụng cho gọi hàm lần đầu thì lưu id chiến dịch  (campaignIdOld);
            //var CountCalCampaignService = 0;
            var campaignMktObj = {
                listCampaign: [],
                listObj: [],
                record: {
                    customerId: 0,
                    used: 0,
                    maxUsed: 0,
                    campaignId: 0,
                    campaignIdOld: 0,
                    discountMoney: 0,
                    serviceId: 0,
                    discountPercent: 0,
                    serviceName: "",
                    campaignLabel: "",
                }
            }

            function GetCustomerType() {
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/Service/Service_Pending_Complete.aspx/GetSpecialCustomer",
                    data: '{CustomerId : "' + $("#HDF_CustomerId").val() + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.d != null) {
                            $(".cusInfo").removeClass("hidden");
                            $("#HDF_CustomerType").val(response.d.CustomerTypeId);
                            $("#HDF_DiscountServices").val(response.d.DiscountServices);
                            $("#HDF_DiscountCosmetic").val(response.d.DiscountCosmetic);

                            var disService = $("#HDF_DiscountServices").val();
                            var disCosmetic = $("#HDF_DiscountCosmetic").val();

                            if (response.d.CustomerTypeId == 1) {
                                $(".pNote").removeClass('hidden');
                                $(".pInfo").removeClass('hidden');
                                $(".pExam").removeClass('hidden');
                                $(".pSpecCus").text("(*) " + response.d.TypeName);
                                $(".pNote").text("Lý do: " + response.d.ReasonDiff);
                                $(".pInfo").text("LỄ TÂN CHÚ Ý THÔNG BÁO FREE CHO KHÁCH");
                                $(".pExam").text(
                                    "VD: Lần trước anh chưa hài lòng về dịch vụ bên em nên hôm nay chúng em mời anh sử dụng dịch vụ miễn phí ạ.");

                                $("#table-item-service tbody tr input.product-voucher").val(disService);
                                var quantity = parseInt($("#table-item-service tbody tr input.product-quantity").val());
                                var price = parseInt($("#table-item-service tbody tr .td-product-price").text());
                                var thanhtien = quantity * price * (100 - parseInt(disService)) / 100;
                                $("#table-item-service tbody tr .box-money").text(thanhtien);
                                $(".voucher-cosmetic").val(disCosmetic);
                                TotalMoney();
                                getProductIds();
                                getServiceIds();
                                ExcQuantity();
                            } else if (response.d.CustomerTypeId == 2) {
                                $(".pSpecCus").text("(*) " +
                                    response.d.TypeName +
                                    ", lễ tân chú ý thu đúng số tiền hiển thị.");
                                $("#table-item-service tbody tr input.product-voucher").val(disService);
                                var quantity = parseInt($("#table-item-service tbody tr input.product-quantity").val());
                                var price = parseInt($("#table-item-service tbody tr .td-product-price").text());
                                var thanhtien = quantity * price * (100 - disService) / 100;
                                $("#table-item-service tbody tr .box-money").text(thanhtien);
                                $(".voucher-cosmetic").val(disCosmetic);
                                TotalMoney();
                                getProductIds();
                                getServiceIds();
                                ExcQuantity();
                            }
                        } else {
                            $(".tblHistory").addClass("hidden");
                        }
                    },
                });
            }


            function openVoucher() {
                var salonID = $("#HDF_SalonId").val();
                //if (salonID == "10") {
                //    $(".popup-voucher").openEBPopup();               
                //}
            }

            jQuery(document).ready(function ($) {
                openRating();
                InforMTKCampaign();
                openVoucher();
                GetCustomerType();
                //select2
                $(".select2").select2();

                //===========
                // Set auto suggestion for staff field
                $('.staff-auto-selection').staffSuggestion((function (list) {
                    var _list = {
                        Seller: []
                    };
                    for (var i in list) {
                        var staff = list[i];

                        if (typeof _list[staff.Type] === 'undefined') {
                            _list[staff.Type] = [];
                        }

                        _list[staff.Type].push(staff);
                        // Add staff as a seller
                        _list.Seller.push($.extend({}, staff, { Type: "Seller" }));

                    }

                    return _list;
                })(<% = listOfStaffsSerialized %>),
                    "input propertychange");

               (function () {
                    var NotPleasureReasonList = $('ul.not-pleasure-reason-wp > li');
                    var NotPleasureReasonListInput = $('input#NotPleasureReasonList');

                    NotPleasureReasonList.bind('click', function (e) {
                        var liElement = $(this);
                        var checkboxElement = liElement.find('.input-checkbox > input[type="checkbox"]');
                        var isChecked = checkboxElement.is(":checked");

                        if (e.target.tagName == "INPUT") {
                            isChecked = !isChecked;
                        }

                        liElement.toggleClass('selected');
                        checkboxElement.prop('checked', !isChecked);

                        var Value = NotPleasureReasonList.find('.input-checkbox > input[type="checkbox"]:checked').map(function () {
                            return this.value;
                        }).get().join();
                        NotPleasureReasonListInput.val(Value);
                    });

                    $(".rating-icon-wrap > .rating-icon").bind('click', function (e) {
                        if ($(this).hasClass("icon-sad")) {
                            $("#not-pleasure-reason").fadeIn(200);
                        }
                        else {
                            $("#not-pleasure-reason").hide();
                            NotPleasureReasonList.removeClass("selected");
                            NotPleasureReasonList.find('.input-checkbox > input[type="checkbox"]').prop("checked", false);
                            NotPleasureReasonListInput.val("");
                        }
                    });

                    // Hien thi popup KH khong danh gia
                    $(document).on("onBeforeCompleteBillService", function (event) {
                        var fnSilence = function () { };
                        var submitHandler = null;
                        var data = event._data;
                        var wait5S = function () {
                            var counter = 7;
                            var id;
                            id = setInterval(function () {
                                $("#EBCloseBtn").click();
                                 $(".popup-rating .yn-elm.yn-no").hide();
                                $("#EBCloseBtn").hide();
                                $(".rating-p2").text("");
                                $(".rating-p1").text("");
                                $(".popup-rating").openEBPopup();
                                counter--;
                                if (counter < 0) {
                                    $("#EBCloseBtn").click();
                                    clearInterval(id);
                                    event.submitHandler.call(null);
                                } else {
                                    $(".rating-p1").text("Hệ thống chưa nhận đánh giá.");
                                    $(".rating-p2").text("Bạn vui lòng chờ " + counter.toString() + " giây để hoàn tất !");
                                }
                            }, 1000);
                        }
                        if (data.Stylist || data.Skinner) {
                            if ($("#HDF_Rating").val().length < 1) {
                                // show EBPopup
                                $(".popup-rating").openEBPopup();
                                $(".popup-rating .confirm-yn-text")
                                    .text("Khách không đánh giá, bill sẽ được hoàn tất với mức điểm tương đương Hài lòng.\n Trưởng salon gọi cho khách hỏi sau!")
                                $(".popup-rating .yn-elm.yn-no").unbind('click').bind('click', function (e) {
                                    if (typeof event.submitHandler == "function") {
                                        if ($("#HDF_Rating").val() == "") {
                                            wait5S.call();
                                        }
                                        else {
                                            event.submitHandler.call(null);
                                        }

                                    }
                                });
                                submitHandler = fnSilence;;
                            }
                        }

                        return submitHandler;
                    });

<%--                    <% if (bill.Mark == 3)
            { %>
                    $("#not-pleasure-reason").show();
                    <% } %>--%>

                })();

                (function () {

                })();

            });

            //Push open rating
            function openRating() {
                let salonId = $("#HDF_SalonId").val();
                let key_event = 'rating_' + salonId;
                let data = {
                    key_event: key_event,
                    push_data: {
                        isClose: false,
                        billId: <%= this.billID %>,
                        customerName: '<%= this._CustomerName %>'
                    }
                }
                $.ajax({
                    type: "POST",
                    //url: "https://api-push-notic.30shine.com/api/pushNotice/socketIO",
                    url: URL_API_PUSH_NOTIC + "/api/pushNotice/socketIO",
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                    }
                });
            }

            //push close rating
            function closeAppRating() {
                let salonId = $("#HDF_SalonId").val();
                let key_event = 'rating_' + salonId;
                let data = {
                    key_event: key_event,
                    push_data: { isClose: true }
                }
                $.ajax({
                    type: "POST",
                    //url: "https://api-push-notic.30shine.com/api/pushNotice/socketIO",
                    url: URL_API_PUSH_NOTIC + "/api/pushNotice/socketIO",
                    data: JSON.stringify(data),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                    }
                });
            }

            //Ham tinh toan goi api xu ly chiến dịch markerting.
            function CallApiMktCampaign() {
                if (GetPendding() == 0) {
                    UpdateMktCampaignBill();
                } else if (GetPendding() == 1) {
                    //pendding=1 thì thêm mới
                    InsertMktCampaignBill();
                }
            }

            //Tính toán ưu đãi cho khach
            function CalCampaignService(checkBox) {
                //Reset gia tri uu dai
                campaignMktObj.listObj = [];
                var customerId = parseInt($("#HDF_CustomerId").val());

                // Nếu ko có chiến dịch nào thì ko xử lý gì cả
                if (campaignMktObj.listCampaign.length <= 0) {
                    return true;
                }
                var listService = $("#table-item-service tbody tr");
                $.each(listService,
                    function (u, v) {
                        var tdServiceId = $(this).find(".td-product-code").data("id");
                        $.each(campaignMktObj.listCampaign,
                            function (u1, v1) {
                                if (v1.serviceId == tdServiceId) {
                                    tam = 0;
                                    $.each(campaignMktObj.listObj,
                                        function (u2, v2) {
                                            if (v2.serviceId == v1.serviceId) {
                                                tam++;
                                                //Tinh toan dich vu duoc giam gia nhieu nhat
                                                if (v2.discountPercent < v1.discountPercent) {
                                                    var serviceCode = $(v).find(".td-product-code").text();
                                                    var Dom = $("#table-service")
                                                        .find("input[data-code='" + serviceCode + "']").parent()
                                                        .parent().clone().find("td:first-child").remove().end();
                                                    var discountMoney =
                                                        parseFloat(Dom.find(".td-product-price").attr("data-price")) *
                                                        (parseFloat(v1.discountPercent / 100));
                                                    v2.discountMoney = discountMoney;
                                                    v2.serviceId = v1.serviceId;
                                                    v2.discountPercent = v1.discountPercent;
                                                    v2.serviceName = v1.serviceName;
                                                    v2.campaignId = v1.campaignId;
                                                    v2.used = v1.used;
                                                    v2.maxUsed = v1.maxUsed;
                                                    v2.customerId = customerId;
                                                    v2.campaignLabel = v1.campaignLabel;
                                                }
                                            }
                                        });
                                    if (tam == 0) {
                                        //Tinh toan so tien giam gia.
                                        var serviceCode = $(v).find(".td-product-code").text();
                                        var Dom = $("#table-service").find("input[data-code='" + serviceCode + "']")
                                            .parent().parent().clone().find("td:first-child").remove().end();
                                        var discountMoney =
                                            parseFloat(Dom.find(".td-product-price").attr("data-price")) *
                                            (parseFloat(v1.discountPercent / 100));
                                        var item = {
                                            billId: billId,
                                            discountMoney: discountMoney,
                                            serviceId: v1.serviceId,
                                            discountPercent: v1.discountPercent,
                                            serviceName: v1.serviceName,
                                            campaignId: v1.campaignId,
                                            used: v1.used,
                                            maxUsed: v1.maxUsed,
                                            customerId: customerId,
                                            campaignLabel: v1.campaignLabel,
                                        }
                                        campaignMktObj.listObj.push(item);
                                    }
                                }
                            });
                    });
                if (campaignMktObj.listObj.length > 0) {
                    $(".MKT-label").html('Khách đang trong chương trình khuyến mãi');
                    $.each(campaignMktObj.listObj,
                        function (u1, v1) {
                            $(".MKT-label").append('(' + v1.campaignLabel + '): ');
                            $(".MKT-label").append(v1.serviceName + " , " + v1.discountPercent + "% ");
                            if (u1 < campaignMktObj.listObj.length - 1) {
                                $(".MKT-label").append(", ")
                            }
                        });
                    $(".MKT-label").css('color', 'red')
                    //Tự động điền % giảm giá lên bảng thông tin dịch vụ khi thêm mới bill.
                    if (GetPendding() == 1) {
                        $.each(listService,
                            function (u, v) {
                                var tdServiceId = $(this).find(".td-product-code").data("id");
                                var tdServiceVoucher = $(this).find("input.product-voucher");
                                var DiscountPercenMax = 0;
                                $.each(campaignMktObj.listObj,
                                    function (u1, v1) {
                                        if (tdServiceId == v1.serviceId) {
                                            if (DiscountPercenMax < v1.discountPercent) {
                                                DiscountPercenMax = v1.discountPercent
                                            }
                                        }
                                    });
                                var tam = tdServiceVoucher.val();
                                //var ServiceIdCheckbox = checkBox.data();
                                if (checkBox == 0) {
                                    var NumberVoucher = parseInt(DiscountPercenMax) + parseInt(tam);
                                    NumberVoucher = NumberVoucher < 0 ? 0 : (NumberVoucher > 100 ? 100 : NumberVoucher);
                                    tdServiceVoucher.val(NumberVoucher);
                                    tdServiceVoucher.change();
                                } else if (checkBox.prop("checked") == false) {
                                    var Dom = $("#table-service")
                                        .find("input[data-code='" + checkBox.attr("data-code") + "']").parent().parent()
                                        .clone().find("td:first-child").remove().end();
                                    //Lấy ServiceId
                                    var id = Dom.find(".td-product-code").attr("data-id");
                                    if (id == tdServiceId) {
                                        var NumberVoucher = parseInt(tam) - parseInt(DiscountPercenMax);
                                        NumberVoucher = NumberVoucher < 0
                                            ? 0
                                            : (NumberVoucher > 100 ? 100 : NumberVoucher);
                                        tdServiceVoucher.val(NumberVoucher);
                                        tdServiceVoucher.change();
                                    }
                                } else if (checkBox.prop("checked") == true) {
                                    var Dom = $("#table-service")
                                        .find("input[data-code='" + checkBox.attr("data-code") + "']").parent().parent()
                                        .clone().find("td:first-child").remove().end();
                                    //Lấy ServiceId
                                    var id = Dom.find(".td-product-code").attr("data-id");
                                    if (id == tdServiceId) {
                                        var NumberVoucher = parseInt(DiscountPercenMax) + parseInt(tam);
                                        NumberVoucher = NumberVoucher < 0
                                            ? 0
                                            : (NumberVoucher > 100 ? 100 : NumberVoucher);
                                        tdServiceVoucher.val(NumberVoucher);
                                        tdServiceVoucher.change();
                                    }
                                }
                            });
                    }
                }
                console.log(campaignMktObj.listObj);
                return true;
            }

            //Lấy thông tin chiến dịch
            function InforMTKCampaign() {
                var customerId = <%= this.customerID %>;
                $.ajax({
                    //async: false,
                    type: "GET",
                    url: domainApiBillService + '/api/mkt-bill?customerId=' + customerId,
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.status) {
                            if (response.data.length > 0) {
                                campaignMktObj.listCampaign = response.data;
                                //Tinh toan uu dai cho khach hang.
                                CalCampaignService(0);
                                console.log(campaignMktObj.listCampaign);
                            }
                        }
                    }
                });
            }

            // cap nhat chiến dịch makerting theo bill
            function UpdateMktCampaignBill() {
                //var objCampaignBill = {
                //    CampaignId: campaignMktObj.campaignId,
                //    CampaignIdOld: campaignMktObj.campaignIdOld,
                //    CustomerId: campaignMktObj.customerId,
                //    BillId: billId,
                //    DiscountMoney: campaignMktObj.discountMoney,
                //    ServiceId: campaignMktObj.serviceId
                //}
                //trường hợp xóa chiến dịch theo bill
                if (campaignMktObj.listObj.length <= 0) {
                    DeleteMktCampaignBill();
                }
                //trường hợp update chiến dịch theo bill
                //else {
                //    $.ajax({
                //        //async: false,
                //        type: "PUT",
                //        url: domainApiBillService + '/api/mkt-bill',
                //        data: JSON.stringify(campaignMktObj.listObj),
                //        contentType: "application/json;charset=utf-8",
                //        dataType: "json",
                //        success: function (response) {
                //            if (response.status) {
                //                console.log("Update MKT Success!");
                //            }
                //            else {
                //                console.log("Update MKT Failed!");
                //            }
                //        }
                //    });
                //}
            }

            // Them moi chiến dịch makerting theo bill
            function InsertMktCampaignBill() {
                $.ajax({
                    //async: false,
                    type: "POST",
                    url: domainApiBillService + '/api/mkt-bill',
                    data: JSON.stringify(campaignMktObj.listObj),
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.status) {
                            console.log("Insert MKT Success!");
                        } else {
                            console.log("Insert MKT Failed!");
                        }
                    }
                });
            }

            // xoa chiến dịch makerting theo bill
            function DeleteMktCampaignBill() {
                $.ajax({
                    //async: false,
                    type: "DELETE",
                    url: domainApiBillService +
                        '/api/mkt-bill?BillId=' +
                        billId +
                        '&CustomerId=' +
                <%= this.customerID %>,
                    contentType: "application/json;charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.status) {
                            console.log("Delete MKT Success! ");
                        } else {
                            console.log("Delete MKT Failed! ");
                        }
                    }
                });
            }

            function GetPendding() {
                return <%= bill.Pending %>;
            }
    //=================================================================
        </script>

        <!-- Popup voucher -->
        <style>
            .popup-voucher {
                width: 510px;
                display: none;
            }

                .popup-voucher .popup-voucher-content {
                    width: 100%;
                    float: left;
                    padding: 30px 30px;
                }

                .popup-voucher .voucher-p1 {
                    text-align: left !important;
                    font-size: 20px;
                    line-height: 26px;
                }

                .popup-voucher .voucher-p2 {
                    text-align: left !important;
                    margin-top: 10px;
                }

                    .popup-voucher .voucher-p2 span {
                        font-family: Roboto Condensed Regular;
                        font-style: italic;
                        text-decoration: underline;
                        color: red;
                    }

                .popup-voucher .yn-wp {
                    text-align: center;
                }

                    .popup-voucher .yn-wp .yn-elm {
                        width: 60px;
                        height: 30px;
                        line-height: 30px;
                        background: #dfdfdf;
                        cursor: pointer;
                        text-align: center;
                        display: inline-block;
                        margin-top: 15px;
                    }

                        .popup-voucher .yn-wp .yn-elm:hover {
                            background: #50b347;
                            color: #ffffff;
                        }

            .select2-container--default .select2-selection--single .select2-selection__arrow b {
                margin-left: -16px;
                margin-top: 2px;
                left: inherit;
                right: 10px;
            }

            .customer-add .table-add tr.tr-product span {
                border-radius: inherit;
                width: 100% !important;
            }

            .wp_ddproduct {
                width: 49%;
                float: left;
            }

            .select2-dropdown {
                z-index: 99999;
            }

            .fe-service .tr-product .show-product.addproduct {
                height: 36px;
                padding-top: 0px;
                padding-bottom: 0px;
                line-height: 38px;
                margin: 0px;
                border-radius: inherit;
                margin-left: 10px;
            }

            .wp_add_product {
                width: 100%;
                margin: 20px 0px;
                float: left;
            }
        </style>
        <!--/ Popup voucher -->

    </asp:Panel>


    <!-- Xử lý voucher giảm giá 20% cho khách hàng chờ lâu -->
    <script type="text/javascript">
        var customerID = <%= this.customerID %>;
        var billID = <%= this.billID %>;

    </script>
    <script src="/Assets/js/erp.30shine.com/service.checkout.voucherWaitTime.js?2374983"></script>
    <!--// Xử lý voucher giảm giá 20% cho khách hàng chờ lâu -->
</asp:Content>
