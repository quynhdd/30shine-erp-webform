﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TemplateMaster/SiteMaster.Master" CodeBehind="ServicePendingCompleteV3Club.aspx.cs" Inherits="_30shine.GUI.FrontEnd.Service.ConfigCheck.ServicePendingCompleteV3Club" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>30Shine - Checkout</title>
    <link href="/Assets/css/bootstrap/bootstrap-toggle.min.css" rel="stylesheet" />
    <link href="/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
    <link href="../../../../Assets/css/checkout-club.css" rel="stylesheet" />
    <link href="../../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
    <script src="/assets/js/jquery.mCustomScrollbar.js"></script>
    <script src="/Assets/js/erp.30shine.com/fn.js"></script>
    <script src="/Assets/js/jquery.scannerdetection.js"></script>
    <script src="/Assets/js/bootstrap/bootstrap-toggle.min.js"></script>
    <script src="../../../../Assets/js/erp.30shine.com/service.checkout.club.js"></script>
    <script src="../../../../Assets/js/select2/select2.min.js"></script>
    <!--// Xử lý voucher giảm giá 20% cho khách hàng chờ lâu -->
    <%--<script src="/Assets/js/erp.30shine.com/service.checkout.voucherWaitTime.js?2374983"></script>--%>

</asp:Content>
<asp:Content ID="ServiceAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
       <%-- <% if (!Perm_EditRating && checkSalonHoiQuan)
            { %>--%>
        <%--<style>
            .fe-service .filter-item #FVStylist {
                z-index: 5000 !important
            }
        </style>--%>
       <%-- <%} %>--%>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Dịch vụ &nbsp;&#187; </li>
                        <li class="li-listing">
                            <a href="/dich-vu/danh-sach.html">Danh sách</a>
                        </li>
                        <li class="li-pending">
                            <a href="/dich-vu/pending.html">
                                <div class="pending-1"></div>
                                Pending
                            </a>
                        </li>
                        <li class="li-pending-complete">
                            <a href="/dich-vu/pending.html">
                                <div class="pending-1"></div>
                                Hoàn tất
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add admin-product-add fe-service">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <!-- System Message -->
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <!-- END System Message -->
                <div class="table-wp">
                    <table class="table-add admin-product-table-add fe-service-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Thông tin dịch vụ</strong></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-2 left"><span>Khách hàng</span> </td>
                                <td class="col-xs-10 right">
                                    <% if (!CusNoInfor)
                                        { %>
                                    <div class="field-wp">
                                        <asp:TextBox ID="CustomerName" runat="server" ClientIDMode="Static" ReadOnly="true" Style="width: 30%;"></asp:TextBox>
                                        <asp:TextBox ID="CustomerCode" runat="server" ClientIDMode="Static" ReadOnly="true" CssClass="mgl" placeholder="Nhập mã khách hàng ở đây" Style="width: 30%;"></asp:TextBox>
                                        <label class="customer-30shine-care">
                                            <input type="checkbox" id="cbCustomer30ShineCare" runat="server" />
                                            Khách bảo hành dịch vụ theo 30Shine Care
                                        </label>
                                        <div class="field cusInfo">
                                            <p class="pSpecCus"></p>
                                        </div>
                                        <div class="field cusInfo">
                                            <p class="pNote"></p>
                                        </div>
                                        <div class="field cusInfo">
                                            <p class="pInfo"></p>
                                        </div>
                                        <div class="field cusInfo">
                                            <p class="pExam"></p>
                                        </div>
                                        <div class="item-vipcard" onclick="activeVIPCard($(this))" style="display: none;">
                                            <div class="left"><i aria-hidden="true" class="fa fa-square-o"></i></div>
                                            <div class="right">
                                                <input type="text" placeholder="Mã thẻ tặng" disabled="disabled" runat="server" id="VIPGive" onkeydown="validateVIPCard($(this),event)" onchange="validateVIPCardValue($(this), event)" onblur="validateVIPCardValue($(this), event)" clientidmode="Static" />
                                            </div>
                                        </div>
                                        <div class="item-vipcard" onclick="activeVIPCard($(this))" style="display: none;">
                                            <div class="left">
                                                <i aria-hidden="true" class="fa fa-square-o"></i>
                                            </div>
                                            <div class="right">
                                                <input type="text" placeholder="Mã thẻ dùng" disabled="disabled" runat="server" id="VIPUse" onkeydown="validateVIPCard($(this),event)" onchange="validateVIPCardValue($(this), event)" onblur="validateVIPCardValue($(this), event)" clientidmode="Static" />
                                            </div>
                                        </div>
                                    </div>
                                    <% }
                                        else
                                        { %>
                                    <span class="field-wp">
                                        <span class="checkbox cus-no-infor">
                                            <label>
                                                <input type="checkbox" checked="checked" disabled="disabled" id="InputCusNoInfor" runat="server" clientidmode="Static" />
                                                Khách không cho thông tin
                                            </label>
                                        </span>
                                    </span><% } %>
                                </td>
                            </tr>

                            <tr class="tr-field-ahalf tr-product">
                                <td class="col-xs-2 left">
                                    <span>Dịch vụ</span>
                                </td>
                                <td class="col-xs-10 right">
                                    <% if (!CusNoInfor)
                                        { %>
                                    <div class="row" id="quick-service">
                                        <asp:Repeater ID="Rpt_ServiceFeatured" runat="server">
                                            <ItemTemplate>
                                                <div class="checkbox cus-no-infor">
                                                    <label class="lbl-cus-no-infor">
                                                        <input type="checkbox" data-code="<%# Eval("Code") %>" onclick="pushQuickData($(this), 'service', $('#HDF_CustomerType').val(), $('#HDF_DiscountServices').val())" />
                                                        <%# Eval("Service") %>
                                                    </label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                    <div id="ListingServiceWp" runat="server" class="listing-product item-service" clientidmode="Static">
                                        <table id="table-item-service" class="table table-listing-product table-item-service">
                                            <thead>
                                                <tr>
                                                    <th>STT</th>
                                                    <th>Tên dịch vụ</th>
                                                    <th>Mã dịch vụ</th>
                                                    <th>Đơn giá</th>
                                                    <th>Số lượng</th>
                                                    <th>Giảm giá</th>
                                                    <th>Thành tiền</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="Rpt_Service_Bill" runat="server">
                                                    <ItemTemplate>
                                                        <tr data-serviceid='<%# Eval("Id") %>'>
                                                            <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                                            <td class="td-product-name"><%# Eval("Name") %></td>
                                                            <td class="td-product-code" data-id='<%# Eval("Id") %>'><%# Eval("Code") %></td>
                                                            <td class="td-product-price" data-price='<%# Eval("Price") %>'><%# Eval("Price") %></td>
                                                            <td class="td-product-quantity">
                                                                <input type="text" class="product-quantity" value="<%# Eval("Quantity") %>" />
                                                            </td>
                                                            <td class="td-product-voucher" data-voucher='<%# Eval("VoucherPercent") %>'>
                                                                <div class="row">
                                                                    <input type="text" class="product-voucher voucher-services" value="<%# Eval("VoucherPercent") %>" style="margin: 0 auto; float: none; text-align: center; width: 50px;" />
                                                                    %
                                                                </div>
                                                            </td>
                                                            <td class="map-edit">
                                                                <div class="box-money" style="display: block;">
                                                                    <%# String.Format("{0:#,###}", Convert.ToInt32(Eval("Price")) * Convert.ToInt32(Eval("Quantity")) * ((Convert.ToDouble(100 - Convert.ToInt32(Eval("VoucherPercent")))) / 100)).Replace(",", ".") == "" ? "0"
                                                : String.Format("{0:#,###}", Convert.ToInt32(Eval("Price")) * Convert.ToInt32(Eval("Quantity")) * ((Convert.ToDouble(100 - Convert.ToInt32(Eval("VoucherPercent")))) / 100)).Replace(",", ".") %>
                                                                </div>
                                                                <div class="edit-wp">
                                                                    <a class="elm del-btn" href="javascript://" onclick="RemoveItem($(this).parent().parent().parent(),'<%# Eval("Name") %>' ,'<%# Eval("Id") %>','service')" title="Xóa"></a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                        <div class="MKT-label">
                                        </div>
                                    </div>
                                    <div class="row free-service">
                                        <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                            <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                Phụ trợ :
                                            </label>
                                        </div>
                                        <asp:Repeater ID="Rpt_FreeService" runat="server">
                                            <ItemTemplate>
                                                <div class="checkbox cus-no-infor" style="padding-bottom: 4px;">
                                                    <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                        <input type="checkbox" onclick="pushFreeService($(this), <%# Eval("Id") %>)" data-id="<%# Eval("Id") %>"
                                                            <%# Eval("Status").ToString() == "1" ? "checked='checked'" : "" %> />
                                                        <%# Eval("Name") %>
                                                    </label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </td>
                                <% } %>
                            </tr>

                            <tr class="tr-field-ahalf tr-field-third">
                                <td class="col-xs-2 left">
                                    <span>Nhân viên</span>
                                </td>
                                <td class="col-xs-10 right">
                                    <div class="field-wp">
                                        <span id="ValidateCosmetic" class="fb-cover-error" style="visibility: hidden; top: -28px; width: 255px;">Bạn chưa chọn nhân viên bán mỹ phẩm! </span>
                                        <div class="filter-item" style="width: 16.6%;">
                                            <asp:TextBox ID="InputStylist" runat="server" AutoCompleteType="Disabled" ClientIDMode="Static" CssClass="st-head ip-short eb-select eb-suggestion staff-auto-selection" data-staff-type="Stylist" data-field="Hairdresser" data-value="0" placeholder="Stylist"></asp:TextBox>
                                            <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                                                <ul id="Ul1" class="ul-listing-staff ul-listing-suggestion"></ul>
                                            </div>
                                            <div id="FVStylist" runat="server" class="fake-value" clientidmode="Static">
                                            </div>
                                        </div>
                                        <div class="filter-item" style="width: 16.6%;">
                                            <asp:TextBox ID="InputSkinner" runat="server" AutoCompleteType="Disabled" ClientIDMode="Static" CssClass="st-head ip-short eb-select eb-suggestion staff-auto-selection" data-staff-type="Skinner" data-field="HairMassage" data-value="0" placeholder="Skinner"></asp:TextBox>
                                            <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                                                <ul id="Ul2" class="ul-listing-staff ul-listing-suggestion"></ul>
                                            </div>
                                            <div id="FVSkinner" runat="server" class="fake-value" clientidmode="Static">
                                            </div>
                                        </div>
                                        <div class="filter-item" style="width: 16.6%;">
                                            <asp:TextBox ID="InputSeller" runat="server" AutoCompleteType="Disabled" ClientIDMode="Static" CssClass="st-head ip-short eb-select eb-suggestion staff-auto-selection" data-staff-type="Seller" data-field="Cosmetic" data-value="0" placeholder="Bán mỹ phẩm"></asp:TextBox>
                                            <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                                                <ul id="Ul3" class="ul-listing-staff ul-listing-suggestion"></ul>
                                            </div>
                                            <div id="FVSeller" runat="server" class="fake-value" clientidmode="Static">
                                            </div>
                                        </div>
                                        <div class="filter-item" style="width: 16.6%;">
                                            <asp:TextBox ID="InputReception" runat="server" AutoCompleteType="Disabled" ClientIDMode="Static" CssClass="st-head ip-short eb-select eb-suggestion staff-auto-selection" data-staff-type="Checkin" data-field="HDF_Reception" data-value="0" placeholder="Checkin"></asp:TextBox>
                                            <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                                                <ul id="Ul4" class="ul-listing-staff ul-listing-suggestion"></ul>
                                            </div>
                                            <div id="FVReception" runat="server" class="fake-value" clientidmode="Static">
                                            </div>
                                        </div>
                                        <div class="filter-item" style="width: 16.6%;">
                                            <asp:TextBox ID="InputCheckout" runat="server" AutoCompleteType="Disabled" ClientIDMode="Static" CssClass="st-head ip-short eb-select eb-suggestion staff-auto-selection" data-staff-type="Checkout" data-field="HDF_Checkout" data-value="0" placeholder="Checkout"></asp:TextBox>
                                            <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                                                <ul id="Ul5" class="ul-listing-staff ul-listing-suggestion"></ul>
                                            </div>
                                            <div id="FVCheckout" runat="server" class="fake-value" clientidmode="Static">
                                            </div>
                                        </div>
                                        <div class="filter-item" style="width: 16.6%;">
                                            <asp:TextBox ID="InputSecurity" runat="server" AutoCompleteType="Disabled" ClientIDMode="Static" CssClass="st-head ip-short eb-select eb-suggestion staff-auto-selection" data-staff-type="Bảo vệ" data-field="HDF_Security" data-value="0" placeholder="Security"></asp:TextBox>
                                            <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                                                <ul id="Ul6" class="ul-listing-staff ul-listing-suggestion"></ul>
                                            </div>
                                            <div id="FVSecurity" runat="server" class="fake-value" clientidmode="Static">
                                            </div>
                                        </div>
                                    </div>

                                </td>
                            </tr>

                            <tr class="tr-field-ahalf tr-product">
                                <td class="col-xs-2 left">
                                    <span>Sản phẩm</span>
                                </td>
                                <td class="col-xs-10 right">
                                    <div class="row" id="quick-product">
                                        <div class="col-xs-12">
                                            <div class="row" style="margin-left: -15px; margin-right: -15px;">
                                                <div class="col-xs-4">
                                                    <asp:Repeater ID="Rpt_ProductFeatured_Combo" runat="server">
                                                        <ItemTemplate>
                                                            <div class="checkbox cus-no-infor">
                                                                <label class="lbl-cus-no-infor">
                                                                    <!--thêm Mapid-->
                                                                    <input type="checkbox" data-id="<%# Eval("Id") %>" data-code="<%# Eval("Code") %>" data-mapid="<%#Eval("MapIdProduct") %>" data-price="<%#Eval("Price") %>" data-discount="<%#Eval("VoucherPercent") != null ? Eval("VoucherPercent") : 0 %>" data-cate="<%#Eval("CategoryId") %>" data-name="<%# Eval("Name") %>" onclick="pushQuickproductData($(this))" />
                                                                    <%# Eval("Name") %>
                                                                </label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </div>
                                                <div class="col-xs-3">
                                                    <asp:Repeater ID="Rpt_ProductFeatured_Gom" runat="server">
                                                        <ItemTemplate>
                                                            <div class="checkbox cus-no-infor">
                                                                <label class="lbl-cus-no-infor">
                                                                    <input type="checkbox" data-id="<%# Eval("Id") %>" data-code="<%# Eval("Code") %>" data-price="<%#Eval("Price") %>" data-discount="<%#Eval("VoucherPercent") != null ? Eval("VoucherPercent") : 0 %>" data-cate="<%#Eval("CategoryId") %>" data-name="<%# Eval("Name") %>" onclick="pushQuickproductData($(this))" />
                                                                    <%# Eval("Name") %>
                                                                </label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </div>
                                                <div class="col-xs-5">
                                                    <asp:Repeater ID="Rpt_ProductFeatured_Sap" runat="server">
                                                        <ItemTemplate>
                                                            <div class="checkbox cus-no-infor">
                                                                <label class="lbl-cus-no-infor">
                                                                    <input type="checkbox" data-id="<%# Eval("Id") %>" data-code="<%# Eval("Code") %>" data-price="<%#Eval("Price") %>" data-discount="<%#Eval("VoucherPercent") != null ? Eval("VoucherPercent") : 0 %>" data-cate="<%#Eval("CategoryId") %>" data-name="<%# Eval("Name") %>" onclick="pushQuickproductData($(this))" />
                                                                    <%# Eval("Name") %>
                                                                </label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="wp_add_product">
                                        <div class="wp_ddproduct">
                                            <select id="ddlProduct" class="form-control select">
                                                <option data-cate="0" data-code="0" data-id="0" data-price="0" value="0">Chọn sản phẩm ...</option>
                                                <% foreach (var item in _ListProduct)
                                                    { %>
                                                <!--thêm Mapid-->
                                                <option data-cate="<%= item.CategoryId %>" data-code="<%= item.Code %>" data-barcode="<%= item.BarCode %>" data-id="<%= item.Id %>" data-mapid="<%= item.MapIdProduct %>" data-name="<%= item.Name %>" data-price="<%= item.Price %>" data-discount="<%= item.VoucherPercent %>" value="<%= item.Id %>"><%= item.Name %></option>
                                                <% } %>
                                            </select>
                                        </div>
                                        <div id="AddProduct" class="show-product  addproduct" data-code="" data-id="0" onclick="ClickAddProduct($(this))">
                                            <i class="fa fa-plus-circle"></i>Thêm sản phẩm
                                        </div>
                                    </div>
                                    <div id="ListingProductWp" runat="server" class="listing-product item-product" clientidmode="Static">
                                        <table id="table-item-product" class="table table-listing-product table-item-product">
                                            <thead>
                                                <tr>
                                                    <th>STT</th>
                                                    <th>Tên sản phẩm</th>
                                                    <th>Mã sản phẩm</th>
                                                    <th style="display: none;"></th>
                                                    <th>Đơn giá</th>
                                                    <th>Số lượng</th>
                                                    <th>Giảm giá</th>
                                                    <th>Thành tiền</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbodyProduct">
                                                <asp:Repeater ID="Rpt_Product_Bill" runat="server">
                                                    <ItemTemplate>
                                                        <tr <%--data-cate='<%#Eval("CategoryId") %>'--%>>
                                                            <td class="td-product-index">1</td>
                                                            <td class="td-product-name"><%#Eval("Name") %></td>
                                                            <td class="td-product-code" data-code='<%#Eval("Code") %>' data-id='<%#Eval("Id") %>'><%#Eval("Code") %></td>
                                                            <!--thêm Mapid-->
                                                            <td class="td-product-price" data-price='<%#Eval("Price") %>'><%#Eval("Price") %></td>
                                                            <td class="td-product-quantity">
                                                                <input class="product-quantity" type="text" value='<%#Eval("Quantity") %>'> </input>
                                                            </td>
                                                            <td class="td-product-voucher" data-voucher="0">
                                                                <div class="row">
                                                                    <input class="product-voucher voucher-cosmetic" style="margin: 0 auto; float: none; text-align: center; width: 50px;" type="text" value='<%#Eval("VoucherPercent") %>'>%
                                                                </div>
                                                            </td>
                                                            <td class="map-edit">
                                                                <div class="box-money" style="display: block;">
                                                                    <%# String.Format("{0:#,###}", Convert.ToInt32(Eval("Price")) * Convert.ToInt32(Eval("Quantity")) * ((Convert.ToDouble(100 - Convert.ToInt32(Eval("VoucherPercent")))) / 100)).Replace(",", ".") == "" ? "0"
                                            : String.Format("{0:#,###}", Convert.ToInt32(Eval("Price")) * Convert.ToInt32(Eval("Quantity")) * ((Convert.ToDouble(100 - Convert.ToInt32(Eval("VoucherPercent")))) / 100)).Replace(",", ".") %>
                                                                </div>
                                                                <div class="edit-wp">
                                                                    <a class="elm del-btn" href="javascript://" onclick="RemoveItem($(this).parent().parent().parent(),'<%#Eval("Name") %>','product')" title="Xóa"></a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                    </div>

                                </td>
                            </tr>

                            <tr class="tr-description tr-field-ahalf">
                                <td class="col-xs-2 left">
                                    <span>Tổng số tiền</span>
                                </td>
                                <td class="col-xs-10 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="TotalMoney" runat="server" ClientIDMode="Static" Text="0" ReadOnly="true"></asp:TextBox>
                                        <span class="unit-money">VNĐ</span>
                                        <label class="label-paybycard">
                                            <asp:CheckBox runat="server" ID="PayByCard" ClientIDMode="Static" />
                                            <span>Thanh toán qua thẻ</span>
                                        </label>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-description">
                                <td class="col-xs-2 left">
                                    <span>Ghi chú</span>
                                </td>
                                <td class="col-xs-10 right">
                                    <span class="field-wp">
                                        <asp:TextBox TextMode="MultiLine" Rows="3" ID="Description" runat="server" ClientIDMode="Static" Style="width: 49%;"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>

                           <%-- <tr class="tr-rating tr-field-ahalf">
                                <td class="col-xs-2 left">
                                    <span>STAR RATING</span>
                                </td>
                                <td class="col-xs-10 right">
                                    <div class="field-wp" <% if (accountModel.IsCheckin() || accountModel.IsCheckout() || accountModel.IsReception())
                                        { %>hidden<% } %>>
                                        <div class="rating-icon-wrap" style="float: left; width: 100%; margin-bottom: 10px;">
                                            <fieldset class="rating">
                                                <input type="radio" class="rating-icon icon-very-happy" onclick="excRatingV2($(this), 5)" id="5star" name="rating" value="5" />
                                                <label class="full" for="5star" title="Excellent"></label>
                                                <input type="radio" class="rating-icon icon-happy" onclick="excRatingV2($(this), 4)" id="4star" name="rating" value="4" />
                                                <label class="full" for="4star" title="Pretty good"></label>
                                                <input type="radio" class="rating-icon icon-normal" onclick="excRatingV2($(this), 3)" id="3star" name="rating" value="3" />
                                                <label class="full" for="3star" title="Ok"></label>
                                                <input type="radio" class="rating-icon icon-sad" onclick="excRatingV2($(this), 2)" id="2star" name="rating" value="2" />
                                                <label class="full" for="2star" title="Bad"></label>
                                                <input type="radio" class="rating-icon icon-very-sad" onclick="excRatingV2($(this), 1)" id="1star" name="rating" value="1" />
                                                <label class="full" for="1star" title="Very bad"></label>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <div>
                                        <asp:CheckBox ID="CkRating" runat="server" ClientIDMode="Static" />
                                        <div class="rating-icon-wrap1" style="color: red; float: left; width: auto">
                                        </div>
                                    </div>
                                </td>
                            </tr>--%>

                            <tr class="tr-field-ahalf" runat="server" id="TrSalon" visible="false">
                                <td class="col-xs-2 left">
                                    <span>Salon</span>
                                </td>
                                <td class="col-xs-10 right">
                                    <span class="field-wp">
                                        <asp:DropDownList ID="Salon" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                        <asp:RequiredFieldValidator InitialValue="0" ID="ValidateSalon" Display="Dynamic" ControlToValidate="Salon" runat="server" Text="Bạn chưa chọn Salon!" ErrorMessage="Vui lòng chọn Salon!" ForeColor="Red" CssClass="fb-cover-error"></asp:RequiredFieldValidator>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-upload">
                                <td class="col-xs-2 left">
                                    <span>Ảnh KH</span>
                                </td>
                                <td class="col-xs-7 right">
                                    <div class="wrap btn-upload-wp">
                                        <div id="Btn_UploadImg" class="btn-upload-img" onclick="  /*OpenIframeImage()*/" style="display: none;">Chọn ảnh đăng</div>
                                        <asp:FileUpload ID="UploadFile" ClientIDMode="Static" Style="display: none;"
                                            CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                        <div class="wrap listing-img-upload">
                                            <% if (HasImages)
                                                { %>
                                            <% for (var i = 0; i < ListImagesName.Length; i++)
                                                { %>
                                            <div class="thumb-wp" ondblclick="Exc_cropImage($(this), '<%= ListImagesName[i] %>', true)">
                                                <img class="thumb" alt="" title="" src="<%= ListImagesName[i] %>"
                                                    data-img="<%= ListImagesName[i] %>" />
                                                <span class="delete-thumb" onclick="deleteThum($(this), '<%= ListImagesName[i] %>', true)" style="display: none;"></span>
                                            </div>
                                            <% } %>
                                            <% } %>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="tr-send">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-10 right no-border" style="padding-top: 10px;">
                                    <%if (CheckEditBill())
                                        { %>
                                    <span class="field-wp">
                                        <asp:Panel ID="BtnSend" CssClass="btn-send" runat="server" ClientIDMode="Static" onclick="//clientScriptBtnSend();">Hoàn Tất </asp:Panel>
                                        <asp:Button ID="BtnFakeSend" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="CompleteService" Style="display: none;"></asp:Button>
                                    </span>
                                    <%} %>
                                    <span>
                                        <asp:Button ClientIDMode="Static" CssClass="btn-send warning" Style="padding-left: 10px; padding-right: 10px; margin-left: 15px; width: auto !important;" Width="80px" Height="30px" ID="WaitFinish" runat="server" Text="Đóng bill nhầm" OnClick="WaitFinish_Click" />
                                    </span>
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
                        </tbody>
                    </table>
                    <!-- input hidden -->
                    <asp:HiddenField ID="HDF_CustomerId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_CustomerCode" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_ProductIds" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_ServiceIds" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_TotalMoney" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_UserImages" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_UserImagesDelete" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="Hairdresser" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HairMassage" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="Cosmetic" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_SalonId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_lgId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_BillId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_FreeService" ClientIDMode="Static" runat="server" />
                    <asp:HiddenField ID="HDF_Rating" ClientIDMode="Static" runat="server" />
                    <asp:HiddenField ID="HDF_Reception" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_Checkout" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_Security" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_CustomerType" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_DiscountServices" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_DiscountCosmetic" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="NotPleasureReasonList" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_ManagerName" runat="server" ClientIDMode="Static" />
                    <!-- END input hidden -->
                </div>
            </div>
            <%-- end Add --%>
        </div>
        <!-- Danh mục dịch vụ -->
        <div class="popup-product-wp popup-service-item">
            <div class="wp popup-product-head">
                <strong>Danh mục dịch vụ</strong>
            </div>
            <div class="wp popup-product-content">
                <div class="wp popup-product-guide">
                    <div class="left">Hướng dẫn:</div>
                    <div class="right">
                        <p>- Click vào ô check box để chọn dịch vụ</p>
                        <p>- Click vào ô số lượng để thay đổi số lượng dịch vụ</p>
                    </div>
                </div>
                <div class="wp listing-product item-product">
                    <table class="table" id="table-service">
                        <thead>
                            <tr>
                                <th>
                                    <input type="checkbox" />
                                </th>
                                <th>STT</th>
                                <th>Tên dịch vụ</th>
                                <th>Mã dịch vụ</th>
                                <th>Đơn giá</th>
                                <th class="th-product-quantity">Số lượng</th>
                                <th>Giảm giá</th>
                                <th>Thành tiền</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="Rpt_Service" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td class="td-product-checkbox item-product-checkbox">
                                            <input type="checkbox" value="<%# Eval("Id") %>" data-code="<%# Eval("Code") %>" data-name="<%# Eval("Name") %>" />
                                        </td>
                                        <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                        <td class="td-product-name"><%# Eval("Name") %></td>
                                        <td class="td-product-code" data-id="<%# Eval("Id") %>" data-code="<%# Eval("Code") %>"><%# Eval("Code") %></td>
                                        <td class="td-product-price" data-price="<%# Eval("Price") %>"><%# Eval("Price") %></td>
                                        <td class="td-product-quantity">
                                            <input type="text" class="product-quantity" value="1" />
                                        </td>
                                        <td class="td-product-voucher" data-voucher="<%# Eval("VoucherPercent") %>">
                                            <div class="row">
                                                <input type="text" class="product-voucher" value="<%# Eval("VoucherPercent") %>" style="margin: 0 auto; float: none; text-align: center; width: 50px;" />
                                            </div>
                                        </td>
                                        <td class="map-edit">
                                            <div class="box-money"></div>
                                            <div class="edit-wp">
                                                <a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(), '<%# Eval("Name") %>', 'service')" href="javascript://" title="Xóa"></a>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
                <div class="wp btn-wp">
                    <div class="popup-product-btn btn-complete" data-item="service">Hoàn tất</div>
                    <div class="popup-product-btn btn-esc">Thoát</div>
                </div>
            </div>
        </div>
        <!-- END Danh mục dịch vụ -->
        <!-- Modal -->
        <div id="modal-page" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Modal Header</h4>
                    </div>
                    <div class="modal-body">
                        <p>Some text in the modal.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <!-- Popup voucher -->
        <div class="popup-voucher">
            <div class="popup-voucher-content">
                <div class="row">
                    <p class="voucher-p1">LỄ TÂN CHECKOUT CHÚ Ý PHÁT VOUCHER CHO KHÁCH HÀNG TRƯỚC KHI NHẬN TIỀN THANH TOÁN</p>
                    <p class="voucher-p2"><span>(*) Lưu ý</span> : Công ty sẽ gọi điện kiểm tra ngẫu nhiên để đảm bảo tất cả Khách Hàng đều được nhận voucher</p>
                </div>
                <div class="row">
                    <div class="yn-wp">
                        <div class="yn-elm yn-no" onclick="autoCloseEBPopup();">OK</div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Popup rating -->
        <div class="popup-rating eb-popup " style="display: none; width: 350px;">
            <div class="">
                <div class="confirm-yn-content">
                    <div class="row">
                        <p class="rating-p1">Khách không đánh giá, bill sẽ được hoàn tất với mức điểm tương đương Hài lòng.</p>
                        <p class="rating-p2">Trưởng salon gọi cho khách hỏi sau!</p>
                    </div>
                    <div class="row">
                        <div class="yn-wp">
                            <div class="yn-elm yn-no" onclick="autoCloseEBPopup();">OK</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Popup rating -->
        <div class="popup-staff-input-error eb-popup " style="display: none; width: 500px;">
            <div class="">
                <div class="confirm-yn-content">
                    <div class="row" id="contentStaffInputError">
                    </div>
                    <div class="row">
                        <div class="yn-wp">
                            <div class="yn-elm yn-no" onclick="autoCloseEBPopup();">OK</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="myModalRating" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        ...
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Loading-->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
        <script>
            var checkSalon = <%=Perm_EditCheckout == true ? 1 : 0%>
            var pending =<%= OBJ != null ? OBJ.Pending : 0  %>;
            var listProduct = <%= string.IsNullOrEmpty(_ListProductJsonString) ? "''" : _ListProductJsonString %>;
            var isAccountAdmin = <%= Perm_EditRating.ToString().ToLower() %>;
            var customerID = parseInt($("#HDF_CustomerId").val());
            var customerName = '<%= this._CustomerName %>';
            var billID = <%= this.billID %>;
            var billId = billID;
            var disCosmetic = 0;
            var abcbcbcbcb = billID;
            var listOfStaffsSerialized = <% = string.IsNullOrEmpty(listOfStaffsSerialized) ? "''" : listOfStaffsSerialized %>;
            var _accountModel = <%= accountModel.ToJson() %>;
            _accountModel.IsCheckout = "<%= accountModel.IsCheckout() %>".toUpperCase() === "TRUE";
            _accountModel.IsCheckin = "<%= accountModel.IsCheckin() %>".toUpperCase() === "TRUE";
            var domainApiBillService = '<%= Libraries.AppConstants.URL_API_BILL_SERVICE %>';
            var URL_API_PUSH_NOTIC = "<%= Libraries.AppConstants.URL_API_PUSH_NOTICE %>";
            var campaignMktObj = {
                listCampaign: [],
                listObj: [],
                record: {
                    customerId: 0,
                    used: 0,
                    maxUsed: 0,
                    campaignId: 0,
                    campaignIdOld: 0,
                    discountMoney: 0,
                    serviceId: 0,
                    discountPercent: 0,
                    serviceName: "",
                    campaignLabel: "",
                }
            }
            // Form load jquerry
            jQuery(document).ready(function ($) {
                //PrintBill();
                $('.select').select2();
                //openRating();
                //InforMTKCampaign();
                GetCustomerType();
                //Push noti Rating
                //$("#WaitFinish").click(function () {
                //    closeAppRating()
                //});
                //Barcode detection
                $(document).scannerDetection({
                    timeBeforeScanTest: 200, // wait for the next character for upto 200ms
                    avgTimeByChar: 40, // it's not a barcode if a character takes longer than 100ms
                    preventDefault: false,
                    endChar: ['\n'],
                    onComplete: function (barcode, qty) {
                        var productId = findProductByBarcode(barcode);

                        if (productId !== 0) {
                            var listOption = $("#ddlProduct").find('option');
                            $(listOption).each(function (index, element) {
                                item = $(element);
                                if (item.val() == productId) {
                                    item.prop('selected', true);
                                    return;
                                }
                            });
                            ClickAddProduct($(this));
                        } else {
                            alert("Không tìm thấy sản phẩm!!!");
                        }
                    }
                });
                // Set auto suggestion for staff field
                $('.staff-auto-selection').staffSuggestion((function (list) {
                    var _list = {
                        Seller: []
                    };
                    for (var i in list) {
                        var staff = list[i];

                        if (typeof _list[staff.Type] === 'undefined') {
                            _list[staff.Type] = [];
                        }

                        _list[staff.Type].push(staff);
                        // Add staff as a seller
                        _list.Seller.push($.extend({}, staff, { Type: "Seller" }));
                    }
                    return _list;
                })(listOfStaffsSerialized), "input propertychange");
                // Hien thi popup KH khong danh gia
                $(document).on("onBeforeCompleteBillService", function (event) {
                    var fnSilence = function () { };
                    var submitHandler = null;
                    var data = event._data;
                    //var wait5S = function () {
                    //    var counter = 7;
                    //    var id;
                    //    id = setInterval(function () {
                    //        $("#EBCloseBtn").click();
                    //        $(".popup-rating .yn-elm.yn-no").hide();
                    //        $("#EBCloseBtn").hide();
                    //        $(".rating-p2").text("");
                    //        $(".rating-p1").text("");
                    //        $(".popup-rating").openEBPopup();
                    //        counter--;
                    //        if (counter < 0) {
                    //            $("#EBCloseBtn").click();
                    //            clearInterval(id);
                    //            event.submitHandler.call(null);
                    //        } else {
                    //            $(".rating-p1").text("Hệ thống chưa nhận đánh giá.");
                    //            $(".rating-p2").text("Bạn vui lòng chờ " + counter.toString() + " giây để hoàn tất !");
                    //        }
                    //    }, 1000);
                    //}
                    event.submitHandler.call(null);

                    //if (data.Stylist || data.Skinner) {
                    //    if (!$("#CkRating").prop("checked")) {
                    //        // show EBPopup
                    //        $(".popup-rating").openEBPopup();
                    //        $(".popup-rating .confirm-yn-text")
                    //            .text("Khách không đánh giá, bill sẽ được hoàn tất với mức điểm tương đương Hài lòng.\n Trưởng salon gọi cho khách hỏi sau!")
                    //        $(".popup-rating .yn-elm.yn-no").unbind('click').bind('click', function (e) {
                    //            if (typeof event.submitHandler == "function") {
                    //                //if (!$("#CkRating").prop("checked")) {
                    //                //    wait5S.call();
                    //                //}
                    //                //else {
                    //                    event.submitHandler.call(null);
                    //                //}
                    //            }
                    //        });
                    //        submitHandler = fnSilence;;
                    //    }
                    //}
                    return submitHandler;
                });
            });
            function findProductByBarcode(barcode) {
                barcode = (barcode + "").trim();
                for (var i = 0; i < listProduct.length; i++) {
                    var product = listProduct[i];
                    if (product.BarCode === barcode) {
                        return product.Id;
                    }
                }
                return 0;
            }
            function GetCustomerType() {
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/Service/ConfigCheck/ServicePendingCompleteV3.aspx/GetSpecialCustomer",
                    data: '{CustomerId : "' + $("#HDF_CustomerId").val() + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.d != null) {
                            $(".cusInfo").removeClass("hidden");
                            $("#HDF_CustomerType").val(response.d.CustomerTypeId);
                            $("#HDF_DiscountServices").val(response.d.DiscountServices);
                            $("#HDF_DiscountCosmetic").val(response.d.DiscountCosmetic);
                            var disService = $("#HDF_DiscountServices").val();
                            disCosmetic = $("#HDF_DiscountCosmetic").val();
                            if (response.d.CustomerTypeId == 1) {
                                $(".pNote").removeClass('hidden');
                                $(".pInfo").removeClass('hidden');
                                $(".pExam").removeClass('hidden');
                                $(".pSpecCus").text("(*) " + response.d.TypeName);
                                $(".pNote").text("Lý do: " + response.d.ReasonDiff);
                                $(".pInfo").text("LỄ TÂN CHÚ Ý THÔNG BÁO GIẢM GIÁ CHO KHÁCH!");
                                $(".pExam").text(
                                    "VD: Lần trước anh chưa hài lòng về dịch vụ bên em nên hôm nay chúng em sẽ giảm giá (miễn phí) các dịch vụ cho anh ạ.");

                                $("#table-item-service tbody tr input.voucher-services").val(disService);
                                var quantity = parseInt($("#table-item-service tbody tr input.product-quantity").val());
                                var price = parseInt($("#table-item-service tbody tr .td-product-price").text());
                                var thanhtien = quantity * price * (100 - parseInt(disService)) / 100;
                                $("#table-item-service tbody tr .box-money").text(thanhtien);
                                TotalMoney();
                                getProductIds();
                                getServiceIds();
                                ExcQuantity();
                            } else if (response.d.CustomerTypeId == 2) {
                                $(".pSpecCus").text("(*) " +
                                    response.d.TypeName +
                                    ", lễ tân chú ý thu đúng số tiền hiển thị.");
                                $("#table-item-service tbody tr input.product-voucher").val(disService);
                                var quantity = parseInt($("#table-item-service tbody tr input.product-quantity").val());
                                var price = parseInt($("#table-item-service tbody tr .td-product-price").text());
                                var thanhtien = quantity * price * (100 - disService) / 100;
                                $("#table-item-service tbody tr .box-money").text(thanhtien);
                                TotalMoney();
                                getProductIds();
                                getServiceIds();
                                ExcQuantity();
                            }
                        } else {
                            $(".tblHistory").addClass("hidden");
                        }
                    },
                });
            }
            //Push open rating
            //function openRating() {
            //    if (pending == 1) {
            //        let salonId = $("#HDF_SalonId").val();
            //        let key_event = 'rating_' + salonId;
            //        let data = {
            //            key_event: key_event,
            //            push_data: {
            //                isClose: false,
            //                billId: billID,
            //                customerName: customerName
            //            }
            //        }
            //        $.ajax({
            //            type: "POST",
            //            //url: "https://api-push-notic.30shine.com/api/pushNotice/socketIO",
            //            url: URL_API_PUSH_NOTIC + "/api/pushNotice/socketIO",
            //            data: JSON.stringify(data),
            //            contentType: "application/json; charset=utf-8",
            //            dataType: "json",
            //            success: function (response) {
            //            }
            //        });
            //    }
            //}
            //push close rating
            //function closeAppRating() {
            //    let salonId = $("#HDF_SalonId").val();
            //    let key_event = 'rating_' + salonId;
            //    let data = {
            //        key_event: key_event,
            //        push_data: { isClose: true }
            //    }
            //    $.ajax({
            //        type: "POST",
            //        //url: "https://api-push-notic.30shine.com/api/pushNotice/socketIO",
            //        url: URL_API_PUSH_NOTIC + "/api/pushNotice/socketIO",
            //        data: JSON.stringify(data),
            //        contentType: "application/json; charset=utf-8",
            //        dataType: "json",
            //        success: function (response) {
            //        }
            //    });
            //}
            //Ham tinh toan goi api xu ly chiến dịch markerting.
            //function CallApiMktCampaign() {
            //    if (pending == 0) {
            //        UpdateMktCampaignBill();
            //    } else if (pending == 1) {
            //        //pendding=1 thì thêm mới
            //        InsertMktCampaignBill();
            //    }
            //}
            //Tính toán ưu đãi cho khach
            //function CalCampaignService(checkBox) {
            //    //Reset gia tri uu dai
            //    campaignMktObj.listObj = [];
            //    // Nếu ko có chiến dịch nào thì ko xử lý gì cả
            //    if (campaignMktObj.listCampaign.length <= 0) {
            //        return true;
            //    }
            //    var listService = $("#table-item-service tbody tr");
            //    $.each(listService,
            //        function (u, v) {
            //            var tdServiceId = $(this).find(".td-product-code").data("id");
            //            $.each(campaignMktObj.listCampaign,
            //                function (u1, v1) {
            //                    if (v1.serviceId == tdServiceId) {
            //                        tam = 0;
            //                        $.each(campaignMktObj.listObj,
            //                            function (u2, v2) {
            //                                if (v2.serviceId == v1.serviceId) {
            //                                    tam++;
            //                                    //Tinh toan dich vu duoc giam gia nhieu nhat
            //                                    if (v2.discountPercent < v1.discountPercent) {
            //                                        var serviceCode = $(v).find(".td-product-code").text();
            //                                        var Dom = $("#table-service")
            //                                            .find("input[data-code='" + serviceCode + "']").parent()
            //                                            .parent().clone().find("td:first-child").remove().end();
            //                                        var discountMoney =
            //                                            parseFloat(Dom.find(".td-product-price").attr("data-price")) *
            //                                            (parseFloat(v1.discountPercent / 100));
            //                                        v2.discountMoney = discountMoney;
            //                                        v2.serviceId = v1.serviceId;
            //                                        v2.discountPercent = v1.discountPercent;
            //                                        v2.serviceName = v1.serviceName;
            //                                        v2.campaignId = v1.campaignId;
            //                                        v2.used = v1.used;
            //                                        v2.maxUsed = v1.maxUsed;
            //                                        v2.customerId = customerID;
            //                                        v2.campaignLabel = v1.campaignLabel;
            //                                    }
            //                                }
            //                            });
            //                        if (tam == 0) {
            //                            //Tinh toan so tien giam gia.
            //                            var serviceCode = $(v).find(".td-product-code").text();
            //                            var Dom = $("#table-service").find("input[data-code='" + serviceCode + "']")
            //                                .parent().parent().clone().find("td:first-child").remove().end();
            //                            var discountMoney =
            //                                parseFloat(Dom.find(".td-product-price").attr("data-price")) *
            //                                (parseFloat(v1.discountPercent / 100));
            //                            var item = {
            //                                billId: billId,
            //                                discountMoney: discountMoney,
            //                                serviceId: v1.serviceId,
            //                                discountPercent: v1.discountPercent,
            //                                serviceName: v1.serviceName,
            //                                campaignId: v1.campaignId,
            //                                used: v1.used,
            //                                maxUsed: v1.maxUsed,
            //                                customerId: customerID,
            //                                campaignLabel: v1.campaignLabel,
            //                            }
            //                            campaignMktObj.listObj.push(item);
            //                        }
            //                    }
            //                });
            //        });
            //    if (campaignMktObj.listObj.length > 0) {
            //        $(".MKT-label").html('Khách đang trong chương trình khuyến mãi');
            //        $.each(campaignMktObj.listObj,
            //            function (u1, v1) {
            //                $(".MKT-label").append('(' + v1.campaignLabel + '): ');
            //                $(".MKT-label").append(v1.serviceName + " , " + v1.discountPercent + "% ");
            //                if (u1 < campaignMktObj.listObj.length - 1) {
            //                    $(".MKT-label").append(", ")
            //                }
            //            });
            //        $(".MKT-label").css('color', 'red')
            //        //Tự động điền % giảm giá lên bảng thông tin dịch vụ khi thêm mới bill.
            //        if (pending == 1) {
            //            $.each(listService,
            //                function (u, v) {
            //                    var tdServiceId = $(this).find(".td-product-code").data("id");
            //                    var tdServiceVoucher = $(this).find("input.product-voucher");
            //                    var DiscountPercenMax = 0;
            //                    $.each(campaignMktObj.listObj,
            //                        function (u1, v1) {
            //                            if (tdServiceId == v1.serviceId) {
            //                                if (DiscountPercenMax < v1.discountPercent) {
            //                                    DiscountPercenMax = v1.discountPercent
            //                                }
            //                            }
            //                        });
            //                    var tam = tdServiceVoucher.val();
            //                    //var ServiceIdCheckbox = checkBox.data();
            //                    if (checkBox == 0) {
            //                        var NumberVoucher = parseInt(DiscountPercenMax) + parseInt(tam);
            //                        NumberVoucher = NumberVoucher < 0 ? 0 : (NumberVoucher > 100 ? 100 : NumberVoucher);
            //                        tdServiceVoucher.val(NumberVoucher);
            //                        tdServiceVoucher.change();
            //                    } else if (checkBox.prop("checked") == false) {
            //                        var Dom = $("#table-service")
            //                            .find("input[data-code='" + checkBox.attr("data-code") + "']").parent().parent()
            //                            .clone().find("td:first-child").remove().end();
            //                        //Lấy ServiceId
            //                        var id = Dom.find(".td-product-code").attr("data-id");
            //                        if (id == tdServiceId) {
            //                            var NumberVoucher = parseInt(tam) - parseInt(DiscountPercenMax);
            //                            NumberVoucher = NumberVoucher < 0 ? 0 : (NumberVoucher > 100 ? 100 : NumberVoucher);
            //                            tdServiceVoucher.val(NumberVoucher);
            //                            tdServiceVoucher.change();
            //                        }
            //                    } else if (checkBox.prop("checked") == true) {
            //                        var Dom = $("#table-service")
            //                            .find("input[data-code='" + checkBox.attr("data-code") + "']").parent().parent()
            //                            .clone().find("td:first-child").remove().end();
            //                        //Lấy ServiceId
            //                        var id = Dom.find(".td-product-code").attr("data-id");
            //                        if (id == tdServiceId) {
            //                            var NumberVoucher = parseInt(DiscountPercenMax) + parseInt(tam);
            //                            NumberVoucher = NumberVoucher < 0 ? 0 : (NumberVoucher > 100 ? 100 : NumberVoucher);
            //                            tdServiceVoucher.val(NumberVoucher);
            //                            tdServiceVoucher.change();
            //                        }
            //                    }
            //                });
            //        }
            //    }
            //    return true;
            //}
            ////Lấy thông tin chiến dịch
            //function InforMTKCampaign() {
            //    $.ajax({
            //        //async: false,
            //        type: "GET",
            //        url: domainApiBillService + '/api/mkt-bill?customerId=' + customerID,
            //        contentType: "application/json;charset=utf-8",
            //        dataType: "json",
            //        success: function (response) {
            //            if (response.status) {
            //                if (response.data.length > 0) {
            //                    campaignMktObj.listCampaign = response.data;
            //                    //Tinh toan uu dai cho khach hang.
            //                    CalCampaignService(0);
            //                }
            //            }
            //        }
            //    });
            //}
            // cap nhat chiến dịch makerting theo bill
            //function UpdateMktCampaignBill() {
            //    //trường hợp xóa chiến dịch theo bill
            //    if (campaignMktObj.listObj.length <= 0) {
            //        DeleteMktCampaignBill();
            //    }
            //}
            // Them moi chiến dịch makerting theo bill
            //function InsertMktCampaignBill() {
            //    $.ajax({
            //        async: false,
            //        type: "POST",
            //        url: domainApiBillService + '/api/mkt-bill',
            //        data: JSON.stringify(campaignMktObj.listObj),
            //        contentType: "application/json;charset=utf-8",
            //        dataType: "json",
            //        success: function (response) {
            //            if (response.status) {
            //                console.log("Insert MKT Success!");
            //            } else {
            //                console.log("Insert MKT Failed!");
            //            }
            //        }
            //    });
            //}
            // xoa chiến dịch makerting theo bill
            //function DeleteMktCampaignBill() {
            //    $.ajax({
            //        //async: false,
            //        type: "DELETE",
            //        url: domainApiBillService + '/api/mkt-bill?BillId=' + billId + '&CustomerId=' + customerID,
            //        contentType: "application/json;charset=utf-8",
            //        dataType: "json",
            //        success: function (response) {
            //            if (response.status) {
            //                console.log("Delete MKT Success! ");
            //            } else {
            //                console.log("Delete MKT Failed! ");
            //            }
            //        }
            //    });
            //}
            //function PrintBill() {
            //    var arrServicesProducts = [];
            //    var listServices = $('table#table-item-service tr');
            //    var listProducts = $('table#table-item-product tr');
            //    var totalServiceMoney = 0;
            //    var totalProductMoney = 0;
            //    var salonName = $('#Salon option:selected').text();
            //    salonName = salonName == "" || typeof salonName == 'undefined' ? "" : salonName.toUpperCase();
            //    var customerName = $('#CustomerName').val();
            //    var customerPhone = $('#CustomerCode').val().replace('Số ĐT : ', "").trim();//cat chuoi lay so
            //    var customerPhoneHide = customerPhone != "" ? "xxxxx" + customerPhone.substring(6) : "";
            //    var managerName = $('#HDF_ManagerName').val();//'-Hotline: 0903456024 \n -CSKH: 0903456024  \n -Quản lý: 0903456024';
            //    managerName = typeof managerName == 'undefined' ? "" : managerName.toString();

            //    // Get service array
            //    for (var i = 1; i < listServices.length; i++) {
            //        // Get Total service
            //        var serviceMoney = $(listServices[i]).find('.box-money').text().trim();
            //        if (serviceMoney != "" && typeof serviceMoney != 'undefined') {
            //            totalServiceMoney += parseInt(serviceMoney.replace(/\./g, "").replace(/,/g, "").trim());
            //        }
            //        //Push to array
            //        arrServicesProducts.push({
            //            'Tên': $(listServices[i]).find('.td-product-name').text().trim(),
            //            'SL': $(listServices[i]).find('.product-quantity').val(),
            //            'T.Tiền': $(listServices[i]).find('.box-money').text().trim()
            //        })
            //    };
            //    // Get product array
            //    for (var i = 1; i < listProducts.length; i++) {
            //        // Get Total service

            //        var productMoney = $(listProducts[i]).find('.box-money').text().trim();
            //        if (productMoney != "" && typeof productMoney != 'undefined') {
            //            totalProductMoney += parseInt(productMoney.replace(/\./g, "").replace(/,/g, "").trim());
            //        }
            //        //push to array
            //        arrServicesProducts.push({
            //            'Tên': $(listProducts[i]).find('.td-product-name').text().trim(),
            //            'SL': $(listProducts[i]).find('.product-quantity').val(),
            //            'T.Tiền': $(listProducts[i]).find('.box-money').text().trim()
            //        })
            //    }
            //    // Get Total  product
            //    //print bill with javascript
            //    var currentdate = new Date();
            //    var datetime = currentdate.getDate() + "/" + (currentdate.getMonth() + 1) + "/" + currentdate.getFullYear() + " " + currentdate.getHours() + ":" + currentdate.getMinutes();
            //    var doc = {
            //        content: [
            //            {
            //                columns: [
            //                    { width: 30, text: "", margin: [0, 5, 0, 5] },
            //                    {
            //                        image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAAA8CAIAAAAfXYiZAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2ZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpFRUQ0MDE1RDU1MjVFNTExQjZFRENCOEFENzc4QzkyRCIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDo4MDUzM0U1NDg5MDYxMUU1QjQ3MUU3MzUyMjAxNTg5NyIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo4MDUzM0U1Mzg5MDYxMUU1QjQ3MUU3MzUyMjAxNTg5NyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M2IChXaW5kb3dzKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOjk5ODA3REQ5ODE1NTExRTU4MUU2Q0QxQ0U5MzVDNzA5IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOjk5ODA3RERBODE1NTExRTU4MUU2Q0QxQ0U5MzVDNzA5Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+5vkOZAAAFUNJREFUeNrsnHnQl2Pbx5XbLglJSlq0ke1p14iisqQspaKZQllGyxiJ7GVtUZYZI0vSnpZBjShRydZK+564lVJSspP3894fDufzu5dMzfs+ZZ7rj2uu33Wd2/E9j+V7nOd534V+//33/fK5fv311/33379QoUKU2blzJ8+85KFw4cI88JJPPnhZIL1s3GJcv/zyywEHHLDfPnsVKgCskDYVWLx+++03IAMdYRI+31MyKysroBHTn3766aCDDrI6ZbjnRnbvvwoXoFZi5JXCBzSggLSgwyeRojw/eQ9SP//8M8V4tsC8efO+//57GwSp/XOufVGz8gVL7UA2hRQmhM9T9XiflXP588ADD+SBiuD48ccfd+vWDS0TIO5q1j8KrFAiUduZc/EzXNj/Vs756UNYqAZoxQULFjRp0gRFO+aYYyyAPYJXTMA/B6zUAPVQwuF7IMjQEf0XD3rxcePGNWrU6Ouvvz7ppJOsAoh4Lu5OwD535TtoQIkABy4//vjjV1999fnnnx988ME8oyBHH310xYoVixYtGg5bu+MZhXr00UdHjx4tfI0bN+bTATlXRqz4h4Cl2ACxZs2axYsXb9++HY1ATb788kuwAK9mzZqVKVNGWEEB+FatWjV79uxhw4a9++67FFbLDj/88NNPPz0YAxWBe1+lDpKAiIAYiG+AgJ+vvfYaPxFv8+bNmzZt+uabb1Co8847r0aNGpRH0ZYuXfrRRx/Nnz9/4cKF2dnZYJHCTSOXXnrp2LFj1dDcPOuHH3445JBDDJHB4PZ2nhUMMzw3YowcObJ48eIAtGHDhiJFivCyTp06JUqUAJfJkydPnTp1+fLlcIIUGn2Tjl83P23atLPPPjt4VuBCjygpX0HzsMMO0+Xv5b6skONOIxoXEAwZMuSEE07YsmXLjh078NDghZzTp0+fNGkSL8XUyBi1YAx4Nx4OPfRQWkBlIA09e/akGOVBn684PrSPFgB64sSJGOmoUaPCNlPuujf6LBXKu9POA5aFuQETQqJN77///pgxYz744AOUJcPK0p9ggWrQlOpWr169unXrojvoJm94f+yxx9IFqAHW1q1ba9WqhTuLedIk92oHz+jRjuCTDnrdunUIwCRXrVoVBtC3b19JplFPjLzrgwDRn5Y544wz7rvvPuyLgIDKoGj08u233+L10FCwA1Yi6caNG9FZKkpiue/t0TAyj0jxkOG777476qijeMZqBgwYoGPW6CQKkUuHrh2fc5122mktWrTASQEHwREgcEw8gxpY0CzdHXnkkeXKlZOFVKpUKTzm3u/jsxyfw9UYYQmqCVK98MILKBp6JyjIg8xoHG94OOKII7DWsmXLnnjiiaVKlcIBUXH9+vUoI02BL2aFG+Iuoa9SpQqAYoDQEUwSWq/Zhqru5TljViAVPgt0sBQQIUi9/vrrsU5QoUKF8uXLEx8BBctC9YiMAEEVfurCXWbQprhoAUSIpDAy6CswzZ07F9xxVVOmTGnfvn2aLbmGsTdT1qywAvk6JsOca3TkwBgOkhQrVuz8888nOFIADUJskAIIAMITcVfXSpYsyUugQSV5oDB1ARpLXLt2LVaJR2vQoAEgonpXX311ULxY9to9pFwXCtARQfeXLh+FjHsyGVlpZkevdIMuMPlABpOiadSnefPmuqRTTz1VLaOA6zBoGaAAlu+3bdsGNFABv7oUwXtwvOiiiyhMSF2yZMlll11GR8GqQpLdSIZS4xX9NFDYWsC3h2qbFWAHFcCykOqLnAt5mjZtih7hm7A13hTJuXiD9kkXMC7MFt9kbohaEemwVsqLCDLAFUiSVqxYAWqtW7fWi2WIlMHa/u5KwJ/Ddpko3gdAEbL3PHpkhaKmBLp69eq4djw9vgZLRHg4EXdgStNGLRchnVLuFCaYEukwYbBjuJo2wy1dunTLli0xT2DNGHRM+O7NvCHCWXG9LPTLdbTILvbUacUKusJ7IU+rVq0wLhShX79+9GRivMvLfMC80ga1R0lGNOLqGAUsv4cXo7V9OoU/M82+wZ/6PqTbw+72S5ujs/iAcwHKG2+8UanSOg4rZE7hDrAyhhUw+QCCO/+8Ar7dw84qGHjXrl1XrlxJks+wyf/79OlDGKG7jMHvyfVXypKudoLFmWee2atXr06dOpkYq1wWiEV041fKVDMMKnwQ5a3uCr3RNrW73fPuUQVaM3/+/KVLl5I84Cvhuq+++iqkt3DOlbH5ske5IQLo/IJDcxEKoQinnHJKBOBYn0hjtgBJkWLoaYQygLjmpRPBm0QSGuFlt3lDeO527dq9+OKLZFR08dhjj0FxiL/xNd1q2v1VBzcHMwgE8D3wwAPEL/JnO3OpK4Up3UbkzijffvtteAOTTP4M/4ghUhfHP2nSJBJD6D7JELHVNZyM5S2FIbaSZqEXMDuDGu9JxSV0FMMZkTy4nM+oaJx5ha8MHDgQxkO/VCf/r1GjBk3RjgxGuXRk9EKwIoLxgCYyEtrnTih32LzMI4CmHif1WaTBt99+e+pEGE1qwL/lXDqgZ555BraB5AQ7w8L48eOjLhOOMH7lDlKDBw/O7XdsCtvHA3Tv3r1x48bNmjXLzs7208yZMyHGJN4E5XvuuYdZueGGGypXrgz6FCP4UoY09l85Fzr14YcfIk7//v2rVavGeDp27OiYx44dW7NmzVtvvRWI0Yb69euTlsAfO3TocPfdd9euXZsGeQlJytvBx4jd+3OBAePv27dveMc0kKURjYdhw4YZp6+88kp+1qtXj9lgZhCPnyNGjHDhpU2bNpSHuDtXrtCn3nfHjh3dunXjE0iROVGdaSczR00s0LlzZ74yGdBamjIE0dqDDz7o8BYuXIjG8bJLly5WQd+xR9ohwULLFA1w582b5/TTl+PB39EIgzTnJfOnC6PqXw4+dmh0PSoXurpu3TrS4wxXJSEIe9H+J0yYgEZQ97jjjuMnfJVu0OqJEyfyMGjQICTnPVpAedo0pDz33HPhcXyzefPmp556SlaM7aCG9AIuhDaDg9aBADEGnawkiztfJcb07oApoKtatGgRTsAqlAFx7w7ApVobQRaZY+5Voz82uAIylwFWr16NlqKfgshXXU+kL9qgyrhs2TLTHXmpoFOGcM4bUkKXKJwGnRQ/0XNESqPV+vXr5bFxEsBatG/vIEgVBLMXB5ZxJEAhDTvGGafK7V4NgtGKL226SIuw6b4nnyL+5L1vqKapLMuXLwfgIUOGTJs2DS0LHK0c3t2FF2fbDCPocqDgmJzwEMDV55g30yZ9op94gww+O+He7QXfzLMr0bHFbSNx6iB2hS2JVsJXHRWjjQwJLXaTJbZUbJDuUib0F1gREGOhhuSZ5yeffBKO17BhQ70DjVrAFMcBKU9aNyJ09ORMUktMLeOAfCNweApXTSO5sy+rx+YTb1RJ1MF20o1xLcs3FEatihUr5shx+TYCdspLXyLFQ+wqaDqmupmalbGvo9Y0aNCgbdu29jpnzpxGjRoNHz6cPjS9lDG4/qlrk6kxvlh6jnTEpijs3MYEKpsuKdQzgrJfaV+l1ipDm/hJxYxVgGA/vDde4SsJVrzECxNDYyEz9NHWbIc23TRxySRDs7LCxQTbkv4QmIcOHerggP/OO+8kHsMA0hUomuNTjx49cJ88Q6BE06HrKaMnBQt3ni4zeN7GdTRaQJH5BB9GF3hDpxZ2MKFHIWFKm0MXYjsdmyUU4rAIIIRLo1CcRnBITqFU46677lLNU878B1jqdhxX08TcQA0nxwU/BO9wxo7M/VcYWevWraNdvIA7hghZ8AEQJVRmHooXLw5RQKrp06fPmDED6NGIGJ7BQUemeJLV/Bi5KKAmlD/55JNVH1IirMTolMZ0MxDucOnjjz8+NbI8omFGl9ScNWtW0E5G1qpVKwNz2LbmHabET+0fuuRA4ZAFbJqG4RgTeABc+JGd3nvvvXIckRJNg1ecCwsbzG8mIquFpjoSnW+aigVh1A4iFotdhs/6Y78g/abWzJ49O6QCbNhaugOkSC5X8RNf8Pjjj9MUJJCg43IF/DP3ea6MhC7d1mUy0FBY1bhx41AuONdtt92md0uzbvTrk08+Abi5c+dm7F1mrDUbDSgJQUdlICuYIR1FDEltFja/bds2ZkjGn3sZ9t/W4NNvCA+jjeyvRYsWdKbni6N7KVnFqZPlM3RYMlSDB5IVyWEBJwudFQ/YqJ6IQU4KX9myZUvv3r0vuOAC5AzaJUdDcwFr69atK1euDLdVwGE8xsZk4wERCt/y6aefBgdK/TXmv3HjRh5ACr8Wu3OZ1CEMIWI2/JDhBhO59tprY3E26qfHtUqXLg24ixcvXrt27VlnncVoLr/8cvIV09E8r9jLcehBc6pWrYoN8swA0CwJhBtrhkuS0Hbt2nXq1Mnlyb+z6MydpFoSR6ITp4pjsrlfd911JFs33XQTmhW8J8PnFg5XF/Kb65K16LAuvPBC6muAcS40jWiBI8KgGvhp2SnBtGAxYnrTU3B0cfPNN+OGaerNN9+EGEcvxkE0y6kFqaBpefqsWHqjzTp16tgOXiIMKHKAsEdJYmQIGT63cBw0TgM8GZynEHi+/vrrMw6PqIm5CY6n+nQW3MlFC96/CjoWFFePzvs+ffpoSiTJJNKhwhRA02M9dpcL87ptBok4So76GzpS7pJGmzhSnLv9wkHK040QXtavX9+2pkyZwnCtn7H+mSqaeb9bPrqhoIXWDZZgO/7M09/TDrrcsWNHqmdnZ7vRG1oQB8hTRupQbVPW9oci/BlDoT4QCMqQydEmNMrWUkKeMvP0567PlEJzBAWehnfs1asX/i8mxJMgztVVV11FgSpVqjzyyCPiFeTTxDuSFYce8xa+Uuw+++wzvBWOg1BILcxfjopXplbqbiMbT/PZgFuWFymxF2UAy5McUHkTpug6doNGjRrVvXt3BMlI8ncBFkZeqlQp5xlfizkQ1wkZOjI1SGKFMMuWLcO1ExY8mBsbZakqha3l5i9WWbNmzcMPPwwFeeeddyhWpkwZNdd0J+Y57DHVytwmExNGI+4wMf1+wuuZJ+/353l1U30ePDIEayGsF7TqkHHhGuBy4YaZ8DFjxpglqFm8NJs3lgmB3kSAcGGxwBAuJqwpdc9qiubDVxcLw1HG0rPWbVO2o8pEWhr7mHEAkQL+fQOfPNqZBt9YzIncNoKjhxP+7tFuakLB3Who3749cZrBEdfdmg8BYjlMS3F8PoSpqhcRaoN/p1qgJqZLLmolmLrY5FfnJt2djL9aiE2miK16Ul0qTVWsWBHCpQZZN2NhPbKfWCPNY4kmP4Zdr149MwCyBLQMfggvh1LBoaCFgWmwkvAdjk9TdT0r3aMPyU3uXcby3FbkBlSET7liEemUC1imDTKvUMxQilhoS4+5KjZOndwz+EQ68qhI476Mv7f5u5ql22ZiSWjxJhdffDG+CUeGAyZ5xjDxVp7zYzSaZHh9l73NyzRqhcFxKircNSBwxEWKFKlUqRLVBUX2D60955xzhDv+VEj7BWXVViXVhxoH4iRimJjVHUx6usazBOp76KxWknHONlMJc+8ho0qA3aNHj3TpHjNcsWIFysXg0IIlS5aULVuWpmrWrDl27Fg4Gj3xTFhwrxjjFVO8XvXq1XnGeRPCQ61ir99TcDBS/OuAAQMAAqauALwhwigDZILhjRw5UhGg3eQ0tDB58mTfNG/enKyLxmfNmsWUM8ca7IQJE1yVJ1WiBfSABFZQRo8ezcsGDRqogx4UzsAk34MrksZJkyY1bdqU4ZbKuUiamOrOnTujF+7ouYRCNz179nzrrbc2b94MoyGn46fHbRkHoRqS+fLLL0NTyQ1atmx5xx13gJfWYUdx6ggQn3/+eZdWLrnkEtTZ9zBvCL3lSVRxBUwMDbrc2KFDB7TyiSeeACN+MuBmzZrVrVv32WefZWwoFD6XukDw9NNPIx0iMBJS8VdeecUlzHCj7hh06dIldjF2rVleaAcApavRGMWMGTPcPlEdYmGTXhkNgTnjpEks9XgmPD2pEUup0VR6AsWW7SvOmETFcPC2GacoMkaVdpQOSesB2bTxVNlzo5FVwKoAoJIhNmzYcPz48TSBZvXu3Ru9ja1wQGQmMS66NIgwG/4pJu9x2BqvCshdkqk8Ov7CyQXKujYXrWLvAwcvoQteagGjR/wFWmx6mnKnOzSGv1iw1yUZH2JTKt1b42vbtm2VJY+/sMgvEXU3tE2bNrQ4bNgwV7UCSh9c2NZtux0AS8ZbI/n27dt12Hgu1ArjRU6EF1y9u34a90EV3a2HLmfPnk1OzgzRiCdUeQ9x3bRpE2Gal3EyA7EB2upgR5u2P3PmTMJfsWLFJOjEJTwDPN5tzWBbNMXYcG0ql4ECZ5Jpg0TM+++/P79EVG9SoUKFwYMH0yL2iDMK52JUooOBAwe+9957OOBt27YxIIY+b948HDnO4o033uAn/hUZtm7diov54osvgBIngjecM2cOHaGYRsZ+/frxFUMGDhJSFJOSeHGUlKBBU6BAC4sWLfLvW0gqEJJPQ4YMIcJMnToV8SBTeDf8OvCRCTBg+n3ooYcYACm0f7gwYsQI3nvqAIxMrVetWsWsk4SsXr2a8VeuXDn3+lJBf0IXZPKWW26BKAwfPpw4xWgA3hjsbOMpmT28L+BCjooWLeqy56BBg0iGQWrp0qWMEs/NPOPvCJRNmjShBegIg0MMsCZiMkSep02bRo+oCV4cJwBkTHK5cuUoo8KSvl1xxRWISoJFj3QH7hR+6aWXaJ8sDZgI0Mwc4+crc0wj9M7IuW/YsIFQQ0BACnABenpEFajLfMAlqUW2q2FmKFdBPEs6AyLEb4zi3HPPveaaa5gK+EQsAMQ2B0Mhn2DQCxYs0BmVL19+6NChjEY7AlNAJLZicYDCe5SOeUZZ0A4+1a5dm0SaoIZWAhDY8YnJ8A80aJ8yKBcPRDE3L+gdyLBKLK5r165Aj8rTmn/ugDGSnCtOrVq1qOVSeLVq1ZhabBwmSDQsWbKkdoqAaDGuqn///sCacUi1IJ8VoKb/mwADYdLoxuVA/R+2EAu1yMDoEZUpVfMxENABKaBnMlEWsOPOaEABl4T+o/M/5Fy8R0iGS13kJFtQDCADKWeIWlgWs4LOUp424cZMIe95hiKhO/RIXbogvylRogQjROWzs7MpwwhRNN5wRxaQogqNUxcddCKxR1xb7oNWhfb8PNx/9nJVK/dyyv/rX7LuK1fs/WTE8f+Lv9TY5zUrP8bzX7D+w9f/CDAAErc2n3P9FeEAAAAASUVORK5CYII=',
            //                        width: 100, height: 60, alignment: 'center', margin: [0, 5, 0, 5],
            //                    },
            //                ],
            //                columnGap: 5
            //            },
            //            {
            //                columns: [
            //                    { width: 175, text: salonName, alignment: 'center' },
            //                ],
            //            },
            //            {
            //                columns: [
            //                    { width: 175, text: 'Ngày ' + datetime, fontSize: 10, italics: true, margin: [3, 3, 3, 3], alignment: 'center' }
            //                ],
            //            },
            //            {
            //                columns: [
            //                    { width: 175, text: GetManagerName(managerName), alignment: 'center' }
            //                ]
            //            },
            //            {
            //                columns: [
            //                    { width: 175, text: '-------------------------------', alignment: 'center' }
            //                ]
            //            },
            //            {
            //                columns: [
            //                    { width: 25, text: 'KH : ' },
            //                    { text: customerName + " - " + customerPhoneHide },
            //                ],
            //            },
            //            {
            //                columns: [
            //                    { width: 175, text: '-------------------------------', alignment: 'center' }
            //                ]
            //            },
            //            {
            //                table: {
            //                    body: [[{ text: 'DỊCH VỤ - MỸ PHẨM' }]]
            //                },
            //            },
            //            {
            //                table: {
            //                    headerRows: 1,
            //                    widths: [109, 14, 50],
            //                    body: buildTableBody(arrServicesProducts, ['Tên', 'SL', 'T.Tiền'])
            //                },
            //                layout: 'noBorders'
            //            },
            //            {
            //                table: {
            //                    widths: [109, 66],
            //                    body: [
            //                        [{ text: 'TỔNG HÓA ĐƠN: ', bold: true, border: [false, true, false, false] },
            //                        {
            //                            alignment: 'right', text: (totalProductMoney + totalServiceMoney).toLocaleString('it-IT'), bold: true, border: [false, true, false, false],
            //                        }]
            //                    ]
            //                },
            //            },
            //            {
            //                columns: [
            //                    { width: 175, text: '-------------------------------', alignment: 'center' }
            //                ]
            //            },
            //            {
            //                columns: [
            //                    { width: 175, text: 'Chính sách 30shine Care', alignment: 'center', italics: true }
            //                ]
            //            },
            //            {
            //                columns: [
            //                    { width: 175, text: ' --> 7 ngày sửa cắt Free', alignment: 'left' }
            //                ]
            //            },
            //            {
            //                columns: [
            //                    { width: 175, text: '--> 3 ngày đổi trả sản phẩm Free', alignment: 'left' }
            //                ]
            //            },
            //            {
            //                columns: [
            //                    { width: 175, text: '--> 15 ngày sửa uốn, nhuộm Free', alignment: 'left' }
            //                ]
            //            },
            //            {
            //                columns: [
            //                    { width: 175, text: 'Chi tiết: 30shine.com', alignment: 'center', bold: true, italics: true, margin: [0, 4, 0, 0] }
            //                ]
            //            },
            //            {
            //                text: ' \n\n\n\n   .'
            //            },
            //        ],
            //        pageMargins: [20, 20, 20, 20],
            //        pageSize: 'letter',
            //        pageOrientation: 'portrait'
            //    };
            //    pdfMake.createPdf(doc).print();
            //}
            //function GetManagerName(managerName) {
            //    if (managerName != '' && typeof managerName != 'undefined') {
            //        var result = '';
            //        var data = [];
            //        data = managerName.split(/\\n/g);
            //        for (var i = 0; i < data.length; i++) {
            //            result += data[i].trim();
            //            if (i < data.length - 1) {
            //                result += ' \n ';
            //            }
            //        }
            //        return result;
            //    }
            //    else {
            //        return "";
            //    }
            //}
            // pdfmake
            //function buildTableBody(data, columns) {
            //    var body = [];
            //    var header = [{ text: columns[0], bold: true }, { text: columns[1], bold: true }, { text: columns[2], bold: true, alignment: 'right' }];
            //    body.push(header);
            //    data.forEach(function (row) {
            //        var dataRow = [];
            //        columns.forEach(function (column) {
            //            let alignment = 'left';
            //            if (column == 'T.Tiền') {
            //                alignment = 'right';
            //            }
            //            let record = {
            //                text: row[column].toString(),
            //                alignment: alignment
            //            }
            //            dataRow.push(record);
            //        })
            //        body.push(dataRow);
            //    });
            //    return body;
            //}

        </script>
    </asp:Panel>
</asp:Content>
