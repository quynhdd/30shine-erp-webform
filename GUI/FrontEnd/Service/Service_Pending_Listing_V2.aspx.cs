﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Data.Entity.Migrations;
using System.Web.Services;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using System.Text.RegularExpressions;

namespace _30shine.GUI.UIService
{
    public partial class Service_Pending_Listing_V2 : System.Web.UI.Page
    {
        private string PageID = "HD_PENDING";
        //protected Paging PAGING = new Paging();
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected bool Perm_ViewPhone = false;
        protected int SalonId;
        protected Tbl_Salon Salon;
        protected List<TeamService> lstTeamService = new List<TeamService>();
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                ////Perm_ShowSalon = permissionModel.GetActionByActionNameAndPageId("Perm_ShowSalon", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                pageId = permissionModel.GetRegexPageIdSpecial().Replace(pageId, ".html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewPhone = permissionModel.CheckPermisionByAction("Perm_ViewPhone", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }


        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {

            SetPermission();
            /// khởi tạo store lương cho nhân viên hôm nay
            //SalaryLib.initSalaryToday(DateTime.Today);
            // Bind team work
            Bind_TeamWork();

            if (!IsPostBack)
            {
                Library.Function.bindSalonSpecial(new List<DropDownList>() { ddlSalon }, Perm_ViewAllData);

                //bindData();
                //RemoveLoading();

            }
        }

        private string genSql()
        {
            string timeFrom = String.Format("{0:yyyy/MM/dd}", DateTime.Now);
            string timeTo = String.Format("{0:yyyy/MM/dd}", DateTime.Now.AddDays(1));
            string sql = "";
            SalonId = Convert.ToInt32(ddlSalon.SelectedValue);
            if (SalonId > 0)
            {
                sql = @"select bill.Id, bill.BillCode, team.Color as TeamColor, bill.CreatedDate,
                            customer.Fullname as CustomerName, customer.Phone as CustomerPhone, bill.PDFBillCode, billWait.CreatedTime as TimeWaitAtSalon, bill.IsBooking, bill.InProcedureTime, bill.InProcedureTimeModifed, skinner.Fullname as SkinnerName
                        from BillService as bill
                        left join TeamService as team
                        on bill.TeamId = team.Id
                        left join Staff as skinner on bill.Staff_HairMassage_Id = skinner.Id
                        left join Customer as customer
                        on bill.CustomerId = Customer.Id
                        left join Bill_WaitAtSalon as billWait
                        on customer.Phone = billWait.CustomerPhone 
	                        and billWait.CreatedTime between '" + timeFrom + @"' and '" + timeTo + @"'
	                        and billWait.IsDelete != 1
                        where bill.IsDelete != 1 and bill.Pending = 1
                        and bill.CreatedDate between '" + timeFrom + "' and '" + timeTo + @"'
                        and bill.SalonId = " + SalonId + @"
                        order by bill.Id asc";
            }
            return sql;
        }




        private void bindData()
        {
            using (var db = new Solution_30shineEntities())
            {
                string timeFrom = String.Format("{0:yyyy/MM/dd}", DateTime.Now);
                string timeTo = String.Format("{0:yyyy/MM/dd}", DateTime.Now.AddDays(1));
                SalonId = int.TryParse(ddlSalon.SelectedValue, out var integer) ? integer : 0;
                if (SalonId > 0)
                {
                    var data = db.Store_Pending_ListingV2(timeFrom, timeTo, SalonId).ToList();
                    Salon = db.Tbl_Salon.FirstOrDefault(w => w.Id == SalonId);
                    for (var i = 0; i < data.Count; i++)
                    {
                        data[i].billOrder = UIHelpers.getBillOrder(data[i].BillCode, 4);
                    }
                    RptBillPending.DataSource = data;
                    RptBillPending.DataBind();
                }
            }
        }

        private void Bind_TeamWork()
        {
            using (var db = new Solution_30shineEntities())
            {
                //lstTeamService = db.TeamServices.Where(w => w.IsDelete != 1 && w.Publish == true && w.Status == 1).ToList();
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            RemoveLoading();
            bindData();

        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
        /// <summary>
        /// Check permission
        /// </summary>
        //private void CheckPermission()
        //{
        //    if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
        //    {
        //        var MsgParam = new List<KeyValuePair<string, string>>();
        //        UIHelpers.Redirect("/dang-nhap.html", MsgParam);
        //    }
        //    else
        //    {
        //        string[] Allow = new string[] { "root", "admin", "salonmanager", "reception", "checkin", "checkout" };
        //        string[] AllowDelete = new string[] { "root", "admin"};
        //        string[] AllowViewAllData = new string[] { "root", "admin" };
        //        var Permission = Session["User_Permission"].ToString().Trim();
        //        if (Array.IndexOf(Allow, Permission) != -1)
        //        {
        //            Perm_Access = true;

        //        }
        //        if (Array.IndexOf(AllowDelete, Permission) != -1)
        //        {
        //            Perm_Delete = true;
        //        }
        //        Perm_Edit = true;

        //        if (Array.IndexOf(AllowViewAllData, Permission) != -1)
        //        {
        //            Perm_ViewAllData = true;
        //        }
        //    }

        //    // Call execute function
        //    ExecuteByPermission();
        //}

        //private void ExecuteByPermission()
        //{
        //    if (!Perm_Access)
        //    {
        //        ContentWrap.Visible = false;
        //        NotAllowAccess.Visible = true;
        //    }
        //}
        [WebMethod]
        public static object GetDataPrint(int billId)
        {

            try
            {
                Solution_30shineEntities db = new Solution_30shineEntities();

                var data = (from b in db.BillServices
                            join bo in db.Bookings on b.BookingId equals bo.Id
                            join bh in db.BookHours on bo.HourId equals bh.Id
                            join sm in db.SM_BookingTemp on bo.BookingTempId equals sm.Id
                            join cus in db.Customers on b.CustomerId equals cus.Id  into d
                            from cus in d.DefaultIfEmpty()
                            where b.Id == billId
                            select new
                            {

                                billCode = b.BillCode,
                                cusName = bo.CustomerName,
                                cusPhone = bo.CustomerPhone,
                                bookHour = bh.HourFrame,
                                isAutoStylist = sm.IsAutoStylist,
                                stylistName = (from s in db.Staffs where s.Id == b.Staff_Hairdresser_Id select new { s.Fullname }).FirstOrDefault().Fullname,
                                skinnerName = (from s in db.Staffs where s.Id == b.Staff_HairMassage_Id select new { s.Fullname }).FirstOrDefault().Fullname,
                                checkinName = (from s in db.Staffs where s.Id == b.ReceptionId select new { s.Fullname }).FirstOrDefault().Fullname,
                                textNote1 = bo.TextNote1,
                                textNote2 = bo.TextNote2,
                                totalBillservice = cus.TotalBillservice,
                                timeCustomerCome = sm.IsCallTime,
                                isShineMember = cus.MemberType,
                                memberEndTime = cus.MemberEndTime
                            }).FirstOrDefault();
                BillInfo billInfo = new BillInfo();
                if (data != null)
                {
                    billInfo.cusName = data.cusName;
                    billInfo.cusPhone = data.cusPhone;
                    billInfo.billCode = data.billCode;
                    billInfo.bookHour = data.bookHour.ToString();
                    billInfo.stylistName = data.stylistName != null ? data.stylistName : "";
                    billInfo.skinnerName = data.skinnerName != null ? data.skinnerName : "";
                    billInfo.checkinName = data.checkinName != null ? data.checkinName : "";
                    billInfo.isAutoStylist = data.isAutoStylist != null ? data.isAutoStylist.Value : false;
                    billInfo.lsService = new List<ServiceInfo>();
                    billInfo.textNote1 = data.textNote1 ?? "";
                    billInfo.textNote2 =  data.textNote2 ?? "";
                    billInfo.needConsult = data.totalBillservice == null || data.totalBillservice <= 2 ? "KHÁCH MỚI CẦN TƯ VẤN KỸ" : "";
                    billInfo.timeCustomerCome = data.timeCustomerCome == null ? "" : string.Format("{0:dd/MM/yyyy HH:mm:ss}",data.timeCustomerCome) ;
                    billInfo.isShineMember = data.isShineMember ?? 0;
                    billInfo.memberEndTime = string.Format("{0:dd/MM/yyyy HH:mm:ss}", data.memberEndTime);
                }

                var ls = (from fs in db.FlowServices join sv in db.Services on fs.ServiceId equals sv.Id where fs.BillId == billId select new { sv.Id, sv.Name }).ToList();

                foreach (var item in ls)
                {
                    ServiceInfo si = new ServiceInfo();
                    si.Id = item.Id;
                    si.ServiceName = item.Name;
                    billInfo.lsService.Add(si);
                }
                return new JavaScriptSerializer().Serialize(billInfo);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }
        public class BillInfo
        {
            public string timeCustomerCome { get; set; }
            public string  needConsult { get; set; }
            public string textNote1 { get; set; }
            public string textNote2 { get; set; }
            public string billCode { get; set; }
            public string cusName { get; set; }
            public string cusPhone { get; set; }
            public string bookHour { get; set; }
            public List<ServiceInfo> lsService { get; set; }
            public string stylistName { get; set; }
            public string skinnerName { get; set; }
            public string checkinName { get; set; }
            public bool isAutoStylist { get; set; }
            public int isShineMember { get; set; }
            public string memberEndTime { get; set; }
        }
        public class ServiceInfo
        {
            public int Id { get; set; }
            public string ServiceName { get; set; }
        }

    }
}