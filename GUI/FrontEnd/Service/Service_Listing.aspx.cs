﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic; 
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;

namespace _30shine.GUI.UIService
{
    public partial class Service_Listing : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected int SalonId;
        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckPermission();

            if (!IsPostBack)
            {
                Bind_Salon();
                Bind_Paging();
                Bind_RptBillService();
                RemoveLoading();
            }
        }

        private void Bind_Salon()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Salons = db.Tbl_Salon.Where(w => w.IsDelete != 1).OrderBy(o => o.Id).ToList();
                var Key = 0;
                SalonId = Convert.ToInt32(Session["SalonId"]);

                Salon.DataTextField = "Salon";
                Salon.DataValueField = "Id";

                if (Perm_ViewAllData)
                {
                    ListItem item = new ListItem("Chọn Salon", "0");
                    Salon.Items.Insert(0, item);

                    foreach (var v in _Salons)
                    {
                        Key++;
                        item = new ListItem(v.Name, v.Id.ToString());
                        Salon.Items.Insert(Key, item);
                    }
                    Salon.SelectedIndex = 0;
                }
                else
                {
                    foreach (var v in _Salons)
                    {
                        if (v.Id == SalonId)
                        {
                            ListItem item = new ListItem(v.Name, v.Id.ToString());
                            Salon.Items.Insert(Key, item);
                            Salon.SelectedIndex = SalonId;
                            Salon.Enabled = false;
                        }
                        
                    }
                }
            }
        }

        private void Bind_RptBillService()
        {
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var _BillService = new List<BillService>();
                var Where = PredicateBuilder.True<BillService>();
                string SgtValue;

                if (TxtDateTimeFrom.Text != "")
                {
                    DateTime _FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                    DateTime _ToDate;
                    if (TxtDateTimeTo.Text != "")
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                    }
                    else
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(1);
                    }
                    Where = Where.And(w => w.CreatedDate > _FromDate && w.CreatedDate < _ToDate);              
                }

                switch (HDF_Suggestion_Field.Value)
                {
                    case "customer.code": SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "customer.name": SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "customer.phone": SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "bill.code": break;
                }

                Where = Where.And(p => p.IsDelete != 1 && p.Pending != 1);
                int SalonId = Convert.ToInt32(Salon.SelectedValue);
                if (!Perm_ViewAllData)
                {
                    Where = Where.And(p => p.SalonId == SalonId);
                }
                else
                {
                    if (SalonId > 0)
                    {
                        Where = Where.And(p => p.SalonId == SalonId);
                    }
                    else
                    {
                        Where = Where.And(p => p.SalonId > 0);
                    }                    
                }

                _BillService = db.BillServices.AsExpandable().Where(Where).OrderByDescending(o => o.ModifiedDate).OrderByDescending(o=>o.Id).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                var lst = (from bill in _BillService
                           join customer1 in db.Customers on bill.CustomerCode equals customer1.Customer_Code into customer2
                           from customer in customer2.DefaultIfEmpty()
                           select new BillService
                           {
                               Id = bill.Id,
                               CustomerName = customer != null ? customer.Fullname : "",
                               CustomerCode = customer != null ? customer.Customer_Code : "",
                               CustomerPhone = customer != null ? customer.Phone : "",
                               TotalMoney = bill.TotalMoney != null ? (int)bill.TotalMoney : 0,
                               SalonId = bill.SalonId != null ? (int)bill.SalonId : 0,
                               Mark = bill.Mark != null ? (int)bill.Mark : 0,
                               IsDelete = bill.IsDelete != null ? bill.IsDelete : 0,
                               Note = bill.Note != null ? bill.Note : "",
                               CustomerNoInfo = bill.CustomerNoInfo != null ? (bool)bill.CustomerNoInfo : false,
                               HairdresserName = "",
                               HairMassageName = "",
                               ProductIds = bill.ProductIds != null ? bill.ProductIds : "",
                               ServiceIds = bill.ServiceIds != null ? bill.ServiceIds : "",
                               ProductNames = "",
                               ServiceNames = "",
                               ModifiedDate = bill.ModifiedDate,
                               CreatedDate = bill.CreatedDate
                           }).ToList();

                if (lst.Count > 0)
                {
                    var Count = 0;
                    foreach (var v in lst)
                    {
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        if (v.ProductIds != null && v.ProductIds != "") {
                            var ProductListThis = serializer.Deserialize<List<ProductBasic>>(v.ProductIds);
                            var _ProductName = "";
                            if (ProductListThis != null)
                            {
                                foreach (var v2 in ProductListThis)
                                {
                                    _ProductName += "<a href=\"/admin/san-pham/" + v2.Code + ".html\" target=\"_blank\">" + v2.Name + "</a>, ";
                                }
                                _ProductName = _ProductName.TrimEnd(',', ' ');
                                lst[Count].ProductNames = _ProductName;
                            }
                        }

                        if (v.ServiceIds != null && v.ServiceIds != "") {
                            var ServiceListThis = serializer.Deserialize<List<ProductBasic>>(v.ServiceIds);
                            var _ServiceName = "";
                            if (ServiceListThis != null)
                            {
                                foreach (var v2 in ServiceListThis)
                                {
                                    _ServiceName += "<a href=\"/admin/dich-vu/" + v2.Code + ".html\" target=\"_blank\">" + v2.Name + "</a>, ";
                                }
                                _ServiceName = _ServiceName.TrimEnd(',', ' ');
                                lst[Count].ServiceNames = _ServiceName;
                            }
                        }                        

                        Count++;
                    }
                }

                RptBillService.DataSource = lst;
                RptBillService.DataBind();
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
            Bind_RptBillService();
            RemoveLoading();
        }

        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? (HDF_Page.Value != "" ? Convert.ToInt32(HDF_Page.Value) : 1) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                int Count;
                var Where = PredicateBuilder.True<BillService>();
                string SgtValue;

                if (TxtDateTimeFrom.Text != "")
                {
                    DateTime _FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                    DateTime _ToDate;
                    if (TxtDateTimeTo.Text != "")
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                    }
                    else
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(1);
                    }
                    Where = Where.And(w => w.CreatedDate > _FromDate && w.CreatedDate < _ToDate);
                }

                switch (HDF_Suggestion_Field.Value)
                {
                    case "customer.code": SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "customer.name": SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "customer.phone": SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "bill.code": break;
                }

                Where = Where.And(p => p.IsDelete != 1 && p.Pending != 1);
                int SalonId = Convert.ToInt32(Salon.SelectedValue);
                if (!Perm_ViewAllData)
                {
                    Where = Where.And(p => p.SalonId == SalonId);
                }
                else
                {
                    if (SalonId > 0)
                    {
                        Where = Where.And(p => p.SalonId == SalonId);
                    }
                    else
                    {
                        Where = Where.And(p => p.SalonId > 0);
                    }
                }

                Count = db.BillServices.AsExpandable().Count(Where);

                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["User_Id"] == null)
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "salonmanager", "reception" };
                string[] AllowDelete = new string[] { "root", "admin", "salonmanager" };
                string[] AllowViewAllData = new string[] { "root", "admin"};
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                    
                }
                if (Array.IndexOf(AllowDelete, Permission) != -1)
                {
                    Perm_Delete = true;
                }
                Perm_Edit = true;

                if (Array.IndexOf(AllowViewAllData, Permission) != -1)
                {
                    Perm_ViewAllData = true;
                }
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }
    }
}