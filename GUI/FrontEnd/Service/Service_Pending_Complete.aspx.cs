﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Helpers;
using System.Text.RegularExpressions;
using System.Globalization;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.UIService
{
    public partial class Service_Pending_Complete : System.Web.UI.Page
    {
        private bool Perm_ShowSalon = false;
        private bool Perm_Access = false;

        private List<ProductBasic> ProductList = new List<ProductBasic>();
        private List<ProductBasic> ServiceList = new List<ProductBasic>();
        private string _CustomerCode = "";
        private string _CustomerName = "";
        private string Stylist = "";
        private string Skinner = "";
        private string Seller = "";
        protected int SalonId = 0;
        private List<int> lstFreeService = new List<int>();

        private BillService OBJ;
        protected bool HasService = false;
        protected bool HasProduct = false;
        protected bool HasImages = false;
        protected List<string> ListImagesUrl = new List<string>();
        protected string[] ListImagesName;
        protected bool CusNoInfor = false;
        //   Solution_30shineEntities db = new Solution_30shineEntities();
        /// <summary>
        /// check permission
        /// </summary>
        //protected void checkPermission()
        //{
        //    if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
        //    {
        //        var MsgParam = new List<KeyValuePair<string, string>>();
        //        UIHelpers.Redirect("/dang-nhap.html", MsgParam);
        //    }
        //    else
        //    {
        //        string perName = Session["User_Permission"].ToString();
        //        string url = Request.Url.AbsolutePath.ToString();
        //        Solution_30shineEntities db = new Solution_30shineEntities();
        //        var pem = (from m in db.Tbl_Permission_Map
        //                   join mn in db.Tbl_Permission_Menu on m.mID equals mn.mID
        //                   where m.pName == perName && mn.Url_Main.EndsWith(url) || mn.Url_Rewrite.EndsWith(url)
        //                   select new { m.aID }
        //                   ).FirstOrDefault();
        //        if (pem != null)
        //        {
        //            var pemArray = pem.aID.Split(',');
        //            if (Array.IndexOf(pemArray, "1") > -1)
        //            {
        //                Perm_Access = true;
        //                if (Array.IndexOf(pemArray, "6") > -1)
        //                {
        //                    Perm_ShowSalon = true;
        //                }
                       
        //            }
        //            else
        //            {
        //                ContentWrap.Visible = false;
        //                NotAllowAccess.Visible = true;
        //            }

        //        }


        //    }
        //}

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckPermission();

            if (!IsPostBack)
            {
                if (Bind_Bill())
                {
                    if (Session["SalonId"] != null)
                    {
                        HDF_SalonId.Value = Session["SalonId"].ToString();
                    }
                    else
                    {
                        HDF_SalonId.Value = "0";
                    }
                    if (Session["User_Id"] != null)
                    {
                        HDF_lgId.Value = Session["User_Id"].ToString();
                    }
                    else
                    {
                        HDF_lgId.Value = "0";
                    }
                    Bind_RptProduct_Bill();
                    Bind_RptService_Bill();
                    Bind_RptProduct();
                    Bind_RptService();
                    Bind_RptServiceFeatured();
                    Bind_RptProductFeatured();
                    Bind_RptFreeService();
                    Library.Function.bindSalon(new List<DropDownList>() { Salon }, Perm_ShowSalon);
                    bindProductCategory();
                }
                else
                {
                    MsgSystem.Text = "Không tồn tại hóa đơn!";
                    MsgSystem.CssClass = "msg-system warning";
                }

                /// Reset rating value
                resetRatingTemp();
            }
        }

        /// <summary>
        /// Bind dữ liệu bill
        /// </summary>
        /// <returns></returns>
        private bool Bind_Bill()
        {
            using (var db = new Solution_30shineEntities())
            {
                var ExistOBJ = false;
                var MatchInt = Regex.Match(Request.QueryString["Code"], @"[^0-9]+");
                if (!MatchInt.Success)
                {
                    var _Code = Convert.ToInt32(Request.QueryString["Code"]);
                    OBJ = db.BillServices.FirstOrDefault(w => w.Id == _Code);
                    if (OBJ != null)
                    {
                        // Set trạng thái x2
                        if (OBJ.IsX2 != null && OBJ.IsX2 == true)
                        {
                            isX2.Checked = true;
                        }

                        var customerName = db.Customers.FirstOrDefault(w => w.Id == OBJ.CustomerId);
                        CustomerName.Text = customerName.Fullname;
                        CustomerCode.Text = "Số ĐT : " + OBJ.CustomerCode;
                        TotalMoney.Text = OBJ.TotalMoney.ToString();
                        Description.Text = OBJ.Note;
                        HDF_CustomerCode.Value = OBJ.CustomerCode;
                        HDF_BillId.Value = OBJ.Id.ToString();
                        if (OBJ.Mark != null)
                        {
                            HDF_Rating.Value = OBJ.Mark.ToString();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "bind rating", "bindRating(" + OBJ.Mark + ");", true);
                        }
                        // Staff information
                        var staff = new Staff();
                        if (OBJ.Staff_Hairdresser_Id > 0) {
                            staff = db.Staffs.FirstOrDefault(w => w.Id == OBJ.Staff_Hairdresser_Id);
                            if (staff != null)
                            {
                                FVStylist.InnerText = staff.OrderCode.ToString() + "- " + staff.Fullname;
                                InputStylist.Text = staff.OrderCode.ToString();
                                Hairdresser.Value = staff.Id.ToString();
                            }
                        }
                        if (OBJ.Staff_HairMassage_Id > 0)
                        {
                            staff = db.Staffs.FirstOrDefault(w => w.Id == OBJ.Staff_HairMassage_Id);
                            if (staff != null)
                            {
                                FVSkinner.InnerText = staff.OrderCode.ToString() + "- " + staff.Fullname;
                                InputSkinner.Text = staff.OrderCode.ToString();
                                HairMassage.Value = staff.Id.ToString();
                            }
                        }
                        if (OBJ.SellerId > 0)
                        {
                            staff = db.Staffs.FirstOrDefault(w => w.Id == OBJ.SellerId);
                            if (staff != null)
                            {
                                FVSeller.InnerText = staff.OrderCode.ToString() + "- " + staff.Fullname;
                                InputSeller.Text = staff.OrderCode.ToString();
                                Cosmetic.Value = staff.Id.ToString();
                            }
                        }
                        if (OBJ.ReceptionId > 0)
                        {
                            staff = db.Staffs.FirstOrDefault(w => w.Id == OBJ.ReceptionId);
                            if (staff != null)
                            {
                                FVReception.InnerText = staff.OrderCode.ToString() + "- " + staff.Fullname;
                                InputReception.Text = staff.OrderCode.ToString();
                                HDF_Reception.Value = staff.Id.ToString();
                            }
                        }

                        if (OBJ.Images != "" && OBJ.Images != null)
                        {
                            ListImagesName = OBJ.Images.Split(',');
                            var Len = ListImagesName.Length;
                            if (Len > 0)
                            {
                                var loop = 0;
                                foreach (var v in ListImagesName)
                                {
                                    var match = Regex.Match(v, @"api.30shine.com");
                                    if (!match.Success)
                                    {
                                        //ListImagesName[loop] = "https://30shine.com/" + v;
                                    }
                                    loop++;
                                }
                                HasImages = true;
                            }
                        }

                        if (OBJ.IsPayByCard != null && OBJ.IsPayByCard.Value)
                        {
                            PayByCard.Checked = true;
                        }

                        _CustomerCode = OBJ.CustomerCode;
                        CusNoInfor = Convert.ToBoolean(OBJ.CustomerNoInfo);
                        ExistOBJ = true;

                        // Get list free service (Phụ trợ)
                        lstFreeService = db.Database.SqlQuery<int>("select PromotionId from FlowPromotion where BillId = " + OBJ.Id + " and IsDelete != 1").ToList();
                    }
                }
                else
                {
                    MsgSystem.Text = "Không tồn tại hóa đơn!";
                    MsgSystem.CssClass = "msg-system warning";
                }

                return ExistOBJ;
            }
        }

        /// <summary>
        /// Bind danh sách dịch vụ trong bill
        /// </summary>
        private void Bind_RptService_Bill()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Services = db.Services.OrderBy(o => o.Id).ToList();
                var Count = _Services.Count;
                if (Count > 0)
                {
                    for (var i = 0; i < _Services.Count; i++)
                    {
                        var serviceId = _Services[i].Id;
                        var thisService = db.FlowServices.FirstOrDefault(w => w.BillId == OBJ.Id && w.ServiceId == serviceId && w.IsDelete != 1);
                        int quantity;

                        if (thisService == null)
                        {
                            _Services.RemoveAt(i);
                            i--;
                        }
                        else
                        {
                            quantity = Convert.ToInt32(thisService.Quantity);
                            _Services[i].Quantity = quantity;
                            _Services[i].VoucherPercent = Convert.ToInt32(thisService.VoucherPercent);
                        }
                    }
                    if (_Services.Count > 0)
                    {
                        ListingServiceWp.Style.Add("display", "block");
                        HasService = true;
                    }

                }

                Rpt_Service_Bill.DataSource = _Services;
                Rpt_Service_Bill.DataBind();
            }
        }

        /// <summary>
        /// Bind danh sách sản phẩm trong bill
        /// </summary>
        private void Bind_RptProduct_Bill()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Products = db.Products.OrderBy(o => o.Id).ToList();
                var Count = _Products.Count;
                if (Count > 0)
                {
                    for (var i = 0; i < _Products.Count; i++)
                    {
                        var productId = _Products[i].Id;
                        var thisProduct = db.FlowProducts.FirstOrDefault(w => w.BillId == OBJ.Id && w.ProductId == productId && w.IsDelete != 1);
                        int quantity;

                        if (thisProduct == null)
                        {
                            _Products.RemoveAt(i);
                            i--;
                        }
                        else
                        {
                            quantity = Convert.ToInt32(thisProduct.Quantity);
                            _Products[i].Quantity = quantity;
                            _Products[i].VoucherPercent = Convert.ToInt32(thisProduct.VoucherPercent);
                            _Products[i].Promotion = Convert.ToInt32(thisProduct.PromotionMoney);
                        }
                    }
                    if (_Products.Count > 0)
                    {
                        ListingProductWp.Style.Add("display", "block");
                        HasProduct = true;
                    }

                }

                Rpt_Product_Bill.DataSource = _Products;
                Rpt_Product_Bill.DataBind();
            }
        }

        /// <summary>
        /// Hoàn tất bill
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CompleteService(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var MatchInt = Regex.Match(Request.QueryString["Code"], @"[^0-9]+");
                int integer;
                if (!MatchInt.Success)
                {
                    var _Code = int.TryParse(Request.QueryString["Code"], out integer) ? integer : 0;
                    OBJ = db.BillServices.FirstOrDefault(w => w.Id == _Code);
                    if (OBJ != null)
                    {
                        // Cập nhật trạng thái x2
                        OBJ.IsX2 = isX2.Checked;

                        //// Insert to BillService
                        _CustomerCode = HDF_CustomerCode.Value;
                        var customer = db.Customers.FirstOrDefault(w => w.Customer_Code == _CustomerCode);
                        _CustomerName = customer != null ? customer.Fullname : "";

                        var stylistId = int.TryParse(Hairdresser.Value, out integer) ? integer : 0;
                        if (stylistId > 0)
                        {
                            var stylist = db.Staffs.FirstOrDefault(w => w.Id == stylistId);
                            Stylist = stylist != null ? stylist.Fullname : "";
                            OBJ.Staff_Hairdresser_Id = stylistId;
                        }
                        else
                        {
                            OBJ.Staff_Hairdresser_Id = 0;
                        }

                        var skinnerId = int.TryParse(HairMassage.Value, out integer) ? integer : 0;
                        if (skinnerId > 0)
                        {
                            var skinner = db.Staffs.FirstOrDefault(w => w.Id == skinnerId);
                            Skinner = skinner != null ? skinner.Fullname : "";
                            OBJ.Staff_HairMassage_Id = skinnerId;
                        }
                        else
                        {
                            OBJ.Staff_HairMassage_Id = 0;
                        }

                        var sellerId = int.TryParse(Cosmetic.Value, out integer) ? integer : 0;
                        if (sellerId > 0)
                        {
                            var seller = db.Staffs.FirstOrDefault(w => w.Id == sellerId);
                            Seller = seller != null ? seller.Fullname : "";
                            OBJ.SellerId = sellerId;
                        }
                        else
                        {
                            OBJ.SellerId = 0;
                        }

                        if (HDF_Reception.Value != "")
                        {
                            OBJ.ReceptionId = Convert.ToInt32(HDF_Reception.Value);
                        }
                        else
                        {
                            OBJ.ReceptionId = 0;
                        }

                        if (TrSalon.Visible == true)
                        {
                            OBJ.SalonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
                        }

                        OBJ.ProductIds = HDF_ProductIds.Value != "[]" ? HDF_ProductIds.Value : "";
                        OBJ.ServiceIds = HDF_ServiceIds.Value != "[]" ? HDF_ServiceIds.Value : "";
                        OBJ.TotalMoney = Convert.ToInt32(HDF_TotalMoney.Value);
                        OBJ.Note = Description.Text;
                        OBJ.ModifiedDate = DateTime.Now;
                        if (OBJ.CompleteBillTime == null)
                        {
                            OBJ.CompleteBillTime = DateTime.Now;
                        }
                        OBJ.Pending = 0;

                        OBJ.Mark = int.TryParse(HDF_Rating.Value, out integer) ? integer : 0;
                        if (VIPGive.Value != "")
                        {
                            OBJ.VIPCard = true;
                            OBJ.VIPCardGive = genVIPCard(VIPGive.Value,4);
                        }
                        if (VIPUse.Value != "")
                        {
                            OBJ.VIPCardUse = genVIPCard(VIPUse.Value, 4);
                        }

                        // Kiểm tra khách thanh toán qua thẻ hay không
                        OBJ.IsPayByCard = PayByCard.Checked;

                        // Tính lương thử nghiệm cho Checkin
                        

                        db.BillServices.AddOrUpdate(OBJ);
                        var exc = db.SaveChanges();
                        var Error = exc > 0 ? 0 : 1;
                        var serializer = new JavaScriptSerializer();

                        // Insert to FlowProduct
                        if (Error == 0)
                        {
                            var objProduct = new FlowProduct();
                            //var str = "[{\"Id\":\"19\",\"Price\":340000,\"Quantity\":\"1\"}]";
                            serializer = new JavaScriptSerializer();
                            ProductList = serializer.Deserialize<List<ProductBasic>>(OBJ.ProductIds);
                            var ProductListInserted = db.FlowProducts.Where(w => w.BillId == OBJ.Id && w.IsDelete != 1).ToList();
                            var index = -1;
                            var _product = new Product();

                            if (ProductList != null)
                            {
                                // Add/Update product
                                foreach (var v in ProductList)
                                {
                                    if (Error == 0)
                                    {
                                        _product = db.Products.FirstOrDefault(w => w.Id == v.Id);
                                        index = ProductListInserted.FindIndex(fi => fi.ProductId == v.Id);
                                        if (index == -1)
                                        {
                                            objProduct = new FlowProduct();
                                            objProduct.BillId = OBJ.Id;
                                            objProduct.ProductId = v.Id;
                                            objProduct.Price = v.Price;
                                            objProduct.Quantity = v.Quantity;
                                            objProduct.VoucherPercent = Convert.ToInt32(v.VoucherPercent);
                                            objProduct.PromotionMoney = Convert.ToInt32(v.Promotion);
                                            objProduct.CreatedDate = OBJ.CreatedDate;
                                            objProduct.IsDelete = 0;
                                            if (_product != null)
                                            {
                                                objProduct.Cost = Convert.ToInt32(_product.Cost);
                                                objProduct.ForSalary = _product.ForSalary;
                                            }
                                            if (Perm_ShowSalon)
                                            {
                                                objProduct.SalonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
                                            }
                                            else
                                            {
                                                objProduct.SalonId = int.TryParse(HDF_SalonId.Value, out integer) ? integer : 0;
                                            }
                                            objProduct.SellerId = int.TryParse(Cosmetic.Value, out integer) ? integer : 0;

                                            db.FlowProducts.Add(objProduct);
                                            exc = db.SaveChanges();
                                            Error = exc > 0 ? Error : ++Error;
                                        }
                                        else
                                        {
                                            var prd = ProductListInserted[index];
                                            prd.VoucherPercent = Convert.ToInt32(v.VoucherPercent);
                                            prd.PromotionMoney = Convert.ToInt32(v.Promotion);
                                            prd.Quantity = Convert.ToInt32(v.Quantity);
                                            db.FlowProducts.AddOrUpdate(prd);
                                            db.SaveChanges();
                                        }
                                    }
                                }
                            }

                            // Delete product
                            if (ProductListInserted.Count > 0)
                            {
                                foreach (var v in ProductListInserted)
                                {
                                    if (ProductList != null)
                                    {
                                        index = ProductList.FindIndex(fi => fi.Id == v.ProductId);
                                        if (index == -1)
                                        {
                                            var prd = v;
                                            prd.IsDelete = 1;
                                            db.FlowProducts.AddOrUpdate(prd);
                                            db.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        var prd = v;
                                        prd.IsDelete = 1;
                                        db.FlowProducts.AddOrUpdate(prd);
                                        db.SaveChanges();
                                    }
                                }
                            }
                        }

                        // Insert to FlowService
                        if (Error == 0)
                        {
                            var objService = new FlowService();
                            //var str = "[{\"Id\":\"19\",\"Price\":340000,\"Quantity\":\"1\"}]";
                            serializer = new JavaScriptSerializer();
                            ServiceList = serializer.Deserialize<List<ProductBasic>>(OBJ.ServiceIds);
                            var ServiceListInserted = db.FlowServices.Where(w => w.BillId == OBJ.Id && w.IsDelete != 1).ToList();
                            var index = -1;
                            if (ServiceList != null)
                            {
                                foreach (var v in ServiceList)
                                {
                                    if (Error == 0)
                                    {
                                        index = ServiceListInserted.FindIndex(fi => fi.ServiceId == v.Id);
                                        if (index == -1)
                                        {
                                            objService = new FlowService();
                                            objService.BillId = OBJ.Id;
                                            objService.ServiceId = v.Id;
                                            objService.Price = v.Price;
                                            objService.Quantity = v.Quantity;
                                            objService.VoucherPercent = Convert.ToInt32(v.VoucherPercent);
                                            var coefficientRating = db.Services.FirstOrDefault(w => w.Id == v.Id);
                                            if (coefficientRating != null && coefficientRating.CoefficientRating != null)
                                            {
                                                objService.CoefficientRating = coefficientRating.CoefficientRating;
                                            }
                                            else
                                            {
                                                objService.CoefficientRating = 0;
                                            }
                                            objService.CreatedDate = OBJ.CreatedDate;
                                            objService.IsDelete = 0;
                                            if (Perm_ShowSalon)
                                            {
                                                objService.SalonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
                                            }
                                            else
                                            {
                                                objService.SalonId = int.TryParse(HDF_SalonId.Value, out integer) ? integer : 0;
                                            }
                                            objService.SellerId = int.TryParse(Cosmetic.Value, out integer) ? integer : 0;

                                            db.FlowServices.Add(objService);
                                            exc = db.SaveChanges();
                                            Error = exc > 0 ? Error : ++Error;
                                        }
                                        else
                                        {
                                            var srv = ServiceListInserted[index];
                                            srv.VoucherPercent = Convert.ToInt32(v.VoucherPercent);
                                            srv.Quantity = Convert.ToInt32(v.Quantity);
                                            db.FlowServices.AddOrUpdate(srv);
                                        }
                                    }
                                }
                            }

                            if (ServiceListInserted.Count > 0)
                            {
                                foreach (var v in ServiceListInserted)
                                {
                                    if (ServiceList != null)
                                    {
                                        index = ServiceList.FindIndex(fi => fi.Id == v.ServiceId);
                                        if (index == -1)
                                        {
                                            var srv = v;
                                            srv.IsDelete = 1;
                                            db.FlowServices.AddOrUpdate(srv);
                                            db.SaveChanges();
                                        }
                                    }
                                    else
                                    {
                                        var srv = v;
                                        srv.IsDelete = 1;
                                        db.FlowServices.AddOrUpdate(srv);
                                        db.SaveChanges();
                                    }
                                }
                            }

                            // Phụ trợ như : Cạo mặt, Lấy ráy tai...
                            //var objPromotion = new FlowPromotion();
                            //if (HDF_FreeService.Value != "[]" && HDF_FreeService.Value != "")
                            //{
                            //    try
                            //    {
                            //        var prms = serializer.Deserialize<List<int>>(HDF_FreeService.Value);
                            //        var lstInserted = db.FlowPromotions.Where(w => w.BillId == OBJ.Id && w.IsDelete != 1).ToList();
                            //        if (prms != null)
                            //        {
                            //            foreach (var v in prms)
                            //            {
                            //                if (Error == 0)
                            //                {
                            //                    index = lstInserted.FindIndex(fi => fi.PromotionId == v);
                            //                    if (index == -1)
                            //                    {
                            //                        objPromotion = new FlowPromotion();
                            //                        objPromotion.PromotionId = v;
                            //                        objPromotion.BillId = OBJ.Id;
                            //                        objPromotion.Group = 1;
                            //                        objPromotion.Quantity = 1;
                            //                        objPromotion.CreatedDate = OBJ.CreatedDate;
                            //                        objPromotion.IsDelete = 0;
                            //                        objPromotion.SalonId = Convert.ToInt32(Session["SalonId"]);

                            //                        db.FlowPromotions.Add(objPromotion);
                            //                        exc = db.SaveChanges();
                            //                        Error = exc > 0 ? Error : ++Error;
                            //                    }
                            //                }
                            //            }
                            //        }

                            //        // Delete gói phụ trợ
                            //        if (lstInserted.Count > 0)
                            //        {
                            //            foreach (var v in lstInserted)
                            //            {
                            //                if (prms != null)
                            //                {
                            //                    index = prms.FindIndex(fi => fi == v.PromotionId);
                            //                    if (index == -1)
                            //                    {
                            //                        var prm = v;
                            //                        prm.IsDelete = 1;
                            //                        db.FlowPromotions.AddOrUpdate(prm);
                            //                        db.SaveChanges();
                            //                    }
                            //                }
                            //                else
                            //                {
                            //                    var prm = v;
                            //                    prm.IsDelete = 1;
                            //                    db.FlowPromotions.AddOrUpdate(prm);
                            //                    db.SaveChanges();
                            //                }
                            //            }
                            //        }
                            //    }
                            //    catch { }
                            //}                            
                        }

                        updateQuantityInvited();
                        /// Cập nhật khách quen, số lần khách đã sử dụng dịch vụ vào bill
                        Update_CustomerFamiliar(OBJ);

                        /// Reset giá trị rating temp
                        resetRatingTemp();

                        /// Cập nhật store lương hôm nay
                        SalaryLib.Instance.updateFlowSalaryByBill(OBJ);

                        /// Cập nhật tồn kho
                        Library.Function.updateInventoryByProductExport(db, OBJ.CompleteBillTime.Value.Date, OBJ.SalonId.Value, OBJ.ProductIds, 1);

                        if (Error == 0)
                        {
                            // Thông báo thành công
                            var MsgParam = new List<KeyValuePair<string, string>>();
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Hoàn tất bill thành công!"));
                            UIHelpers.Redirect("/dich-vu/pending.html", MsgParam);
                        }
                        else
                        {
                            MsgSystem.Text = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                            MsgSystem.CssClass = "msg-system warning";
                        }
                    }
                    
                }
            }
        }

        /// <summary>
        /// Cập nhật số lượng sử dụng dịch vụ miễn phí cho khách đặc biệt
        /// </summary>
        public void updateQuantityInvited()
        {
            var db = new Solution_30shineEntities();
            var query = @"select A.Id from SpecialCustomer A left join Customer B on A.CustomerId = B.Id where A.IsDelete = 0 and B.Phone like N'%" + _CustomerCode + "%'";
            int specialCusId = db.Database.SqlQuery<int>(query).SingleOrDefault();
            var sql = @"select A.*, B.CustomerTypeId, C.TypeName from SpecialCusDetail A
                        left join SpecialCustomer B on A.SpecialCusId = B.Id
                        left join CustomerType C on C.Id = B.CustomerTypeId
                        where A.IsDelete = 0 and A.SpecialCusId = " + specialCusId;
                sql+=@" and A.CutErrorDate = (select Min(A.CutErrorDate) from SpecialCusDetail A 
                        left join SpecialCustomer B on A.SpecialCusId = B.Id
                        where  A.SpecialCusId = " +specialCusId+ " and A.QuantityInvited > 0 and A.IsDelete = 0)";
            SpecialCustomers RECORD = db.Database.SqlQuery<SpecialCustomers>(sql).SingleOrDefault();
            if (RECORD != null)
            {
                if(RECORD.CustomerTypeId == 1)
                {
                    if(RECORD.QuantityInvited > 0)
                    {
                        RECORD.QuantityInvited = RECORD.QuantityInvited - 1;
                        db.SpecialCusDetails.AddOrUpdate(RECORD);
                        db.SaveChanges();
                    }
                }
                //foreach (var item in RECORD)
                //{
                //    if (item.QuantityInvited > 0)
                //    {
                //        item.QuantityInvited = item.QuantityInvited - 1;
                //        db.SpecialCusDetails.AddOrUpdate(item);
                //        db.SaveChanges();
                //        break;
                //    }
                //    else if(item.QuantityInvited == 0)
                //    {
                //        continue;
                //    }
                   
                //}
            }
            
        }

        /// <summary>
        /// Cập nhật khách hàng quen
        /// </summary>
        /// <param name="bill"></param>
        private void Update_CustomerFamiliar(BillService bill)
        {
            using (var db = new Solution_30shineEntities())
            {
                bool isSpecial = false;
                /// 1. Tính số lần đã sử dụng dịch vụ
                string sql = @"select COUNT(*) as times
                                from BillService
                                where IsDelete != 1 and Pending != 1
                                and ServiceIds != '' and ServiceIds is not null
                                and CreatedDate < '" + bill.CreatedDate + "'" +
                        @"and CustomerCode = '" + bill.CustomerCode + "'" +
                        @"group by CustomerCode";

                int timesUsedService = 0;
                try
                {
                    timesUsedService = db.Database.SqlQuery<int>(sql).FirstOrDefault();
                    bill.TimesUsedService = timesUsedService;
                }
                catch { }                

                /// 2. Kiểm tra có phải khách quen (là khách đã dùng dịch vụ bởi stylist này ít nhất 2 lần, hoặc 1 lần nhưng là lần gần đây nhất)
                sql = @"select *
                                from BillService
                                where IsDelete != 1 and Pending != 1
                                and CustomerCode = '" + bill.CustomerCode + @"' 
                                and CustomerCode != '' and CustomerCode is not null
                                and CreatedDate < '" + String.Format("{0:yyyy/MM/dd}", bill.CreatedDate) + "'";
                var billCus = db.BillServices.SqlQuery(sql).ToList();
                var familiar = billCus.Where(w => w.Staff_Hairdresser_Id == bill.Staff_Hairdresser_Id).ToList();
                // Nếu khách đã sử dụng dịch vụ bởi stylist này ít nhất 2 lần
                if (familiar.Count > 1)
                {
                    isSpecial = true;
                }
                // hoặc dùng 1 lần nhưng là lần gần đây nhất
                else if (familiar.Count == 1)
                {
                    if (billCus[billCus.Count - 1].Staff_Hairdresser_Id == bill.Staff_Hairdresser_Id)
                    {
                        isSpecial = true;
                    }
                }

                // Cập nhật bill
                bill.IsCusFamiliar = isSpecial;
                db.BillServices.AddOrUpdate(bill);
                db.SaveChanges();
            }
        }
       
        /// <summary>
        /// Bind danh sách sản phẩm
        /// </summary>
        private void Bind_RptProduct()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Products = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1).OrderByDescending(o => o.Id).ToList();

                Rpt_Product.DataSource = _Products;
                Rpt_Product.DataBind();
            }
        }

        /// <summary>
        /// Bind danh sách dịch vụ
        /// </summary>
        private void Bind_RptService()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Service = db.Services.Where(w => w.IsDelete != 1 && w.Publish == 1).OrderByDescending(o => o.Id).ToList();

                Rpt_Service.DataSource = _Service;
                Rpt_Service.DataBind();
            }
        }

        /// <summary>
        /// Bind danh sách dịch vụ phụ trợ như Cạo mặt, Cắt móng tay
        /// </summary>
        private void Bind_RptFreeService()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Service = db.Services.Where(w => w.IsDelete != 1 && w.Publish == 1 && w.IsFreeService == 1).OrderBy(o => o.Id).ToList();
                int index = -1;
                if (_Service.Count > 0 && lstFreeService.Count > 0)
                {
                    foreach (var v in _Service)
                    {
                        index = lstFreeService.IndexOf(v.Id);
                        if (index != -1)
                        {
                            _Service.FirstOrDefault(w => w.Id == lstFreeService[index]).Status = 1;
                        }
                    }
                }
                Rpt_FreeService.DataSource = _Service;
                Rpt_FreeService.DataBind();
            }
        }

        /// <summary>
        /// Bind danh sách dịch vụ nổi bật
        /// </summary>
        private void Bind_RptServiceFeatured()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Services.Where(w => w.IsDelete != 1 && w.Publish == 1 && w.Status == 1).OrderByDescending(o => o.Id).ToList();

                Rpt_ServiceFeatured.DataSource = lst;
                Rpt_ServiceFeatured.DataBind();
            }
        }

        /// <summary>
        /// Bind danh sách sản phẩm nổi bật
        /// </summary>
        private void Bind_RptProductFeatured()
        {
            using (var db = new Solution_30shineEntities())
            {
                // Combo : ID 95
                int cateId = 95;
                var lst = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1 && w.Status == 1 && w.CategoryId == cateId).OrderByDescending(o => o.Id).ToList();
                Rpt_ProductFeatured_Combo.DataSource = lst;
                Rpt_ProductFeatured_Combo.DataBind();

                // Gôm : ID 6
                cateId = 6;
                lst = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1 && w.Status == 1 && w.CategoryId == cateId).OrderByDescending(o => o.Id).ToList();
                Rpt_ProductFeatured_Gom.DataSource = lst;
                Rpt_ProductFeatured_Gom.DataBind();

                // Sáp : ID 7
                cateId = 7;
                lst = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1 && w.Status == 1 && w.CategoryId == cateId).OrderByDescending(o => o.Id).ToList();
                Rpt_ProductFeatured_Sap.DataSource = lst;
                Rpt_ProductFeatured_Sap.DataBind();
            }
        }

        private void bindProductCategory()
        {
            using (var db = new Solution_30shineEntities())
            {
                //var list = db.Tbl_Category.Where(w=>w.Pid == 1 && w.IsDelete != 1 && w.Publish == true).ToList();
                ////var c = new Tbl_Category();
                ////c.Id = 0;
                ////c.Name = "Chọn danh mục";
                ////list.Insert(0, c);
                //rptProductCategory.DataSource = list;
                //rptProductCategory.DataBind();
                var list = new List<Tbl_Category>();
                var node = new List<Tbl_Category>();
                var c = new Tbl_Category();
                c.Id = 0;
                c.Name = "Chọn danh mục";
                list.Insert(0, c);
                var cateRoot = db.Tbl_Category.Where(w => w.IsDelete != 1 && w.Pid == 1 && w.Publish == true && w.Id != 20).ToList();
                if (cateRoot.Count > 0)
                {
                    foreach (var v in cateRoot)
                    {
                        node = db.Tbl_Category.Where(w => w.IsDelete != 1 && w.Pid == v.Id).ToList();
                        list.Add(v);
                        if (node.Count > 0)
                        {
                            foreach (var v2 in node)
                            {
                                v2.Name = "----- " + v2.Name;
                                list.Add(v2);
                            }
                        }
                    }
                }
                rptProductCategory.DataSource = list;
                rptProductCategory.DataBind();
            }
        }

        /// <summary>
        /// Reset Rating Temp Value
        /// </summary>
        private void resetRatingTemp()
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                /// Reset giá trị rating temp
                int accId = Session["User_Id"] != null ? int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0 : 0;
                int salonId = Session["SalonId"] != null ? int.TryParse(Session["SalonId"].ToString(), out integer) ? integer : 0 : 0;
                var ratingTemp = db.Rating_Temp.FirstOrDefault(w => w.SalonId == salonId && w.AccountId == accId);
                if (ratingTemp != null)
                {
                    ratingTemp.RatingValue = 0;
                    db.Rating_Temp.AddOrUpdate(ratingTemp);
                    db.SaveChanges();
                }
            }
        }

        /// <summary>
        /// Lấy thông tin khách hàng qua mã KH
        /// </summary>
        /// <param name="CustomerCode"></param>
        /// <returns></returns>
        [WebMethod]
        public static string GetCustomer(string CustomerCode)
        {
            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var _Customer = db.Customers.Where(w => w.Customer_Code == CustomerCode).ToList();

                if (_Customer.Count > 0)
                {
                    _Msg.success = true;
                    _Msg.msg = serializer.Serialize(_Customer);
                }
                else
                {
                    _Msg.success = false;
                }
            }
            return serializer.Serialize(_Msg);
        }

        /// <summary>
        /// Kiểm tra đã tạo bill cho khách hàng trong hôm nay qua mã KH
        /// </summary>
        /// <param name="CustomerCode"></param>
        /// <returns></returns>
        [WebMethod]
        public static string CheckDuplicateBill(string CustomerCode)
        {
            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var today = Convert.ToDateTime(DateTime.Now.ToString("d/M/yyyy"), culture);
                var todayL = today.AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                var todayR = today.AddDays(1);
                var _Bill = db.BillServices.FirstOrDefault(w => w.CreatedDate > todayL && w.CreatedDate < todayR && w.CustomerCode == CustomerCode);

                if (_Bill != null)
                {
                    _Msg.success = true;
                    _Msg.msg = serializer.Serialize(_Bill);
                }
                else
                {
                    _Msg.success = false;
                }
            }
            return serializer.Serialize(_Msg);
        }

        /// <summary>
        /// Gen VIP Card - VD : 0001, 9999 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="template"></param>
        /// <returns></returns>
        private static string genVIPCard(string input, int template)
        {
            string code = "";
            int len = input.Length;
            for (var i = 0; i < template; i++)
            {
                if (len > 0)
                {
                    code = input[--len].ToString() + code;
                }
                else
                {
                    code = "0" + code;
                }
            }
            return code;
        }

        [WebMethod]
        public static string issetVIPCard(string input)
        {
            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                input = genVIPCard(input, 4);
                var card = db.BillServices.FirstOrDefault(w=>w.VIPCardGive == input && w.IsDelete != 1);

                if (card != null)
                {
                    _Msg.success = true;
                    //_Msg.msg = serializer.Serialize(_Bill);
                }
                else
                {
                    _Msg.success = false;
                }
            }
            return serializer.Serialize(_Msg);
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //string[] Allow = new string[] { "root", "admin", "salonmanager", "reception", "checkin", "checkout" };
                //string[] AllowSalon = new string[] { "root", "admin" };
                //var Permission = Session["User_Permission"].ToString().Trim();
                //if (Array.IndexOf(Allow, Permission) != -1)
                //{
                //    Perm_Access = true;
                //}
                //if (Array.IndexOf(AllowSalon, Permission) != -1)
                //{
                //    Perm_ShowSalon = true;
                //}
                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
            else if (Perm_ShowSalon)
            {
                TrSalon.Visible = true;
            }
        }

        [WebMethod]
        public static SpecialCustomers GetSpecialCustomer(int CustomerId)
        {
            var db = new Solution_30shineEntities();
            var query = @"select CustomerTypeId from SpecialCustomer where IsDelete = 0 and CustomerId = "+ CustomerId + "";
            int customerType = db.Database.SqlQuery<int>(query).FirstOrDefault();


            var sql = @"select A.*, C.TypeName, B.CustomerTypeId from SpecialCusDetail A 
                        left join SpecialCustomer B on A.SpecialCusId = B.Id
                        left join CustomerType C on C.Id = B.CustomerTypeId
						left join Customer D on D.Id = B.CustomerId
                        where B.Publish =1 and A.IsDelete = 0 and D.Id = " + CustomerId + "";
            if(customerType == 1)
            {
                sql += @" and A.QuantityInvited > 0
                        and A.CutErrorDate = (select Min(A.CutErrorDate) from SpecialCusDetail A 
						left join SpecialCustomer B on A.SpecialCusId = B.Id
						left join Customer D on D.Id = B.CustomerId
						where  D.Id = " + CustomerId + " and A.QuantityInvited > 0 and A.IsDelete = 0)";
            }
            return db.Database.SqlQuery<SpecialCustomers>(sql).SingleOrDefault();
        }

        
        public class billService : BillService
        { }
        public class SpecialCustomers : SpecialCusDetail
        {
            public string TypeName { get; set; }
            public int CustomerTypeId { get; set; }
        }
    }

}
