﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Service_Detail.aspx.cs" Inherits="_30shine.GUI.UIService.Service_Detail" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CtMain" runat="server">

<link href="/Assets/css/plugin.upload.images/pui.style.css" rel="stylesheet" />
<%--<script src="/Assets/js/dropzone.js"></script>
<link href="/Assets/css/dropzone.css" rel="stylesheet" />--%>

<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="Ul1">
                <li class="li-listing"><a href="/dich-vu/danh-sach.html">Danh sách</a></li>
                <li class="li-add"><a href="/dich-vu/them-moi.html">Thêm mới</a></li>
                <li class="li-edit active"><a href="javascript://">Chi tiết</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add admin-product-add fe-service" onload="MsgHandler()">
    <%-- Add --%>                
    <div class="wp960 content-wp">
        <!-- System Message -->
        <asp:Label ID="Label1" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
        <!-- END System Message -->

        <div class="table-wp">
            <table class="table-add admin-product-table-add fe-service-table-add">
                <tbody>
                    <tr class="title-head">
                        <td><strong>Thông tin dịch vụ</strong></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr class="tr-margin" style="height: 20px;"></tr>

                    <tr class="tr-field-ahalf">
                        <td class="col-xs-3 left"><span>Khách hàng</span></td>
                        <td class="col-xs-7 right">
                            <span class="field-wp">
                                <asp:TextBox ID="TextBox1" runat="server" ClientIDMode="Static" ReadOnly="true"></asp:TextBox>
                                <asp:TextBox ID="TextBox2" ReadOnly="true" CssClass="mgl" placeholder="Nhập mã khách hàng ở đây" runat="server" ClientIDMode="Static"></asp:TextBox>
                            </span>
                                            
                        </td>
                        <td class="col-xs-2"></td>
                    </tr>

                    <tr class="tr-field-ahalf tr-product">
                        <td class="col-xs-3 left"><span>Dịch vụ</span></td>
                        <td class="col-xs-7 right">
                            <div class="row">
                                <% if(HasService){ %>
                                <div class="show-product">Danh sách dịch vụ</div>
                                <% }else{ %>
                                <div class="no-item" style="font-style: italic;">Không có dịch vụ nào</div>
                                <% } %>
                            </div>                            
                            <div class="listing-product" id="Div1" runat="server" ClientIDMode="Static">
                                <table class="table table-listing-product">
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Tên dịch vụ</th>
                                            <th>Mã dịch vụ</th>
                                            <th>Đơn giá</th>
                                            <th>Số lượng</th>
                                            <th>Thành tiền</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="Repeater1" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                                    <td class="td-product-name"><%# Eval("Name") %></td>
                                                    <td class="td-product-code"><%# Eval("Code") %></td>
                                                    <td class="td-product-price" data-price="<%# Eval("Price") %>"><%# Eval("Price") %></td>
                                                    <td class="td-product-quantity">
                                                        <input type="text" class="product-quantity" value="<%# Eval("Quantity") %>" disabled="disabled" />
                                                    </td>
                                                    <td>
                                                        <div class="box-money" style="display:block;">
                                                            <%# Convert.ToInt32(Eval("Quantity")) * Convert.ToInt32(Eval("Price")) %>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>                                        
                                    </tbody>
                                </table>
                            </div>                                           
                        </td>
                        <td class="col-xs-2"></td>
                    </tr>

                    <tr class="tr-field-ahalf">
                        <td class="col-xs-3 left"><span>Nhân viên</span></td>
                        <td class="col-xs-7 right">
                            <span class="field-wp">
                                <asp:TextBox ID="TextBox3" ReadOnly="true" runat="server" ClientIDMode="Static"></asp:TextBox>
                                <asp:TextBox ID="TextBox4" ReadOnly="true" CssClass="mgl" runat="server" ClientIDMode="Static"></asp:TextBox>
                            </span>
                                            
                        </td>
                        <td class="col-xs-2"><span class=""></span></td>
                    </tr>

                    <tr class="tr-field-ahalf tr-product">
                        <td class="col-xs-3 left"><span>Sản phẩm</span></td>
                        <td class="col-xs-7 right">
                            <div class="row">
                                <% if(HasProduct){ %>
                                <div class="show-product">Danh sách sản phẩm</div>
                                <% }else{ %>
                                <div class="no-item" style="font-style: italic;">Không có sản phẩm nào</div>
                                <% } %>
                            </div>                            
                            <div class="listing-product" id="Div2" runat="server" ClientIDMode="Static">
                                <table class="table table-listing-product">
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Tên sản phẩm</th>
                                            <th>Mã sản phẩm</th>
                                            <th>Đơn giá</th>
                                            <th>Số lượng</th>
                                            <th>Thành tiền</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="Repeater2" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                                    <td class="td-product-name"><%# Eval("Name") %></td>
                                                    <td class="td-product-code"><%# Eval("Code") %></td>
                                                    <td class="td-product-price" data-price="<%# Eval("Price") %>"><%# Eval("Price") %></td>
                                                    <td class="td-product-quantity">
                                                        <input type="text" class="product-quantity" value="<%# Eval("Quantity") %>" disabled="disabled" />
                                                    </td>
                                                    <td>
                                                        <div class="box-money" style="display:block;">
                                                            <%# Convert.ToInt32(Eval("Quantity")) * Convert.ToInt32(Eval("Price")) %>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>                                        
                                    </tbody>
                                </table>
                            </div>                                           
                        </td>
                        <td class="col-xs-2"></td>
                    </tr>

                    <tr class="tr-description tr-field-ahalf">
                        <td class="col-xs-3 left"><span>Tổng số tiền</span></td>
                        <td class="col-xs-7 right">
                            <span class="field-wp">
                                <asp:TextBox ID="TextBox5" runat="server" ClientIDMode="Static" Text="0" ReadOnly="true"></asp:TextBox>
                                <span class="unit-money">VNĐ</span>
                            </span>
                        </td>
                        <td class="col-xs-2"></td>
                    </tr>

                    <tr class="tr-upload">
                        <td class="col-xs-3 left"><span>Ảnh</span></td>
                        <td class="col-xs-7 right">
                            <div class="wrap btn-upload-wp">
                                <div id="Div3" class="btn-upload-img" onclick="OpenIframeImage()">Chọn ảnh đăng</div>
                                <asp:FileUpload ID="FileUpload1" ClientIDMode="Static" style="display:none;"
                                    CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                <div class="wrap listing-img-upload">
                                    <% if(HasImages){ %>   
                                    <% for (var i = 0; i < ListImagesName.Length; i++ )
                                       { %>
                                        <div class="thumb-wp">
                                            <img class="thumb" alt="" title="" src="http://kh.30shine.com<%=ListImagesUrl[i] %>" 
                                                data-img="<%=ListImagesUrl[i] %>" />
                                            <span class="delete-thumb" onclick="deleteThum($(this), '<%= ListImagesName[i] %>', true)"></span>
                                        </div>
                                    <% } %>
                                    <% } %>
                                </div>
                            </div>                         
                        </td>
                    </tr>

                    <tr class="tr-description">
                        <td class="col-xs-3 left"><span>Ghi chú</span></td>
                        <td class="col-xs-7 right">
                            <span class="field-wp">
                                <asp:TextBox TextMode="MultiLine" Rows="5" ID="TextBox6" runat="server" ClientIDMode="Static" ReadOnly="true"></asp:TextBox>
                            </span>
                        </td>
                        <td class="col-xs-2"></td>
                    </tr>

                    <tr class="tr-send">
                        <td class="col-xs-3 left"></td>
                        <td class="col-xs-9 right no-border">
                            <span class="field-wp">
                                <asp:Panel ID="Panel1" CssClass="btn-send" runat="server" ClientIDMode="Static">Hoàn tất</asp:Panel>
                                <asp:Button ID="Button1" runat="server" Text="Hoàn tất" 
                                    ClientIDMode="Static" OnClick="AddImages" style="display:none;"></asp:Button>
                            </span>
                        </td>
                    </tr>
                    
                    <asp:HiddenField runat="server" ID="HiddenField1"  ClientIDMode="Static"/>
                    <asp:HiddenField runat="server" ID="HiddenField2"  ClientIDMode="Static"/>
                </tbody>
            </table>

            <!-- input hidden -->
            <asp:HiddenField ID="HiddenField3" runat="server" ClientIDMode="Static"/>
            <asp:HiddenField ID="HiddenField4" runat="server" ClientIDMode="Static"/>
            <asp:HiddenField ID="HiddenField5" runat="server" ClientIDMode="Static"/>
            <asp:HiddenField ID="HiddenField6" runat="server" ClientIDMode="Static"/>
            <asp:HiddenField ID="HiddenField7" runat="server" ClientIDMode="Static"/>
            <!-- END input hidden -->
        </div>
    </div>
    <%-- end Add --%>
</div>

<!-- Popup Images -->
<div class="plugin-image">
    <div class="plugin-image-head be-report">
        <!-- Filter -->
        <div class="row">
            <div class="filter-item">
                <asp:TextBox CssClass="txtDateTime st-head TxtDateTimeFrom" ID="TextBox7" placeholder="Từ ngày"
                    ClientIDMode="Static" runat="server"></asp:TextBox>
            </div>
            <strong class="st-head" style="margin-left: 10px;">
                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
            </strong>
            <div class="filter-item">
                <asp:TextBox CssClass="txtDateTime st-head TxtDateTimeTo" ID="TextBox8" placeholder="Đến ngày"
                    ClientIDMode="Static" runat="server"></asp:TextBox>
            </div>
            <div class="filter-item">
                <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion SgCustomerCode" data-field="customer.code" data-value="0"
                    AutoCompleteType="Disabled" ID="TextBox9" ClientIDMode="Static" placeholder="Mã Khách hàng" runat="server"></asp:TextBox>
                <div class="listing-staff-wp eb-select-data ">
                    <ul class="ul-listing-staff ul-listing-suggestion" id="Ul2"></ul>
                </div>
            </div>
            <div class="filter-item">
                <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                    <ul class="ul-listing-staff ul-listing-suggestion" id="Ul3">
                    </ul>
                </div>
            </div>
            
            <div class="filter-item">
                <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion SgCustomerPhone" data-value="0" data-field="customer.phone"
                        AutoCompleteType="Disabled" ID="TextBox10" ClientIDMode="Static" placeholder="Số điện thoại" runat="server"></asp:TextBox>
                <div class="listing-staff-wp eb-select-data">
                    <ul class="ul-listing-staff ul-listing-suggestion" id="Ul4"></ul>
                </div>
            </div>
            <a href="javascript://" class="st-head btn-viewdata plugin-image-view" onclick="Plugin_Images_Load()">Xem ảnh</a>
            <%--<a href="javascript://" class="st-head btn-viewdata plugin-image-upload" onclick="Plugin_Images_Upload('<%=_CustomerCode %>')">Upload</a>--%>
            <a href="javascript://" class="st-head btn-viewdata plugin-image-push" onclick="Plugin_Images_Push()">Chèn ảnh</a>
        </div>
        <!-- End Filter -->
    </div>
    <div class="plugin-image-content">
        <div class="listing-img-upload">                  
            <asp:Repeater ID="Repeater3" runat="server">
                <ItemTemplate>
                    <div class="thumb-wp" onclick="Plugin_Images_Chose(event, $(this))">
                        <img class="thumb" alt="" title="" src="http://kh.30shine.com<%#Eval("Value") %>" 
                            data-img="<%#Eval("Value") %>" />
                        <span class="delete-thumb" onclick="deleteThum($(this), '<%#Eval("Value") %>', false)"></span>
                        <div class="thumb-cover"></div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</div>
<div id="Div4" class="dropzone dz-work-flow">
    <div class="dz-default dz-message wrap listing-img-upload">
        Drop image here. 
    </div>
</div>
<!-- Hidden Field-->
<asp:HiddenField ID="HiddenField8" ClientIDMode="Static" runat="server" />
<asp:HiddenField ID="HiddenField9" ClientIDMode="Static" runat="server" />
<!-- END Hidden Field-->

<!-- END Popup Images -->

<!-- Suggestion -->
<script src="/assets/js/jquery.mCustomScrollbar.js"></script>
<link href="/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
<script type="text/javascript">
    var Imgs = [],
        ImgsDelete = [],
        DbImgs = [],
        InitDropZone = false;

    jQuery(document).ready(function ($) {
        // execute thumbnail size
        excThumbWidth();

        $(".fe-service-table-add .listing-img-upload .thumb-wp").each(function () {
            var dataImg = $(this).find("img.thumb").attr("data-img");
            DbImgs.push(dataImg);
        });

        //if (!InitDropZone) {
        //    $("#dZUpload").dropzone({
        //        url: "http://kh.30shine.com/GUI/BackEnd/UploadImage/DropZoneUploader.ashx?Code=" + "abc",
        //        maxFiles: 100,
        //        addRemoveLinks: true,
        //        success: function (file, response) {
        //            $("#dZUpload").show();
        //            var imgName = response;
        //            file.previewElement.classList.add("dz-success");
        //        },
        //        error: function (file, response) {
        //            file.previewElement.classList.add("dz-error");
        //        }
        //    });
        //    InitDropZone = true;
        //}
    });

    function Plugin_Images_Upload(Code) {

        //$("#dZUpload").click();
    }

    function Plugin_Images_Chose(event, This) {
        if (This.hasClass("active")) {
            This.removeClass("active");
        } else {
            This.addClass("active");
        }
    }

    function Plugin_Images_Push() {
        $('#EBPopup .listing-img-upload .thumb-wp.active').each(function () {
            var dataImg = $(this).find("img.thumb").attr("data-img");
            if (Imgs.indexOf(dataImg) == -1 && DbImgs.indexOf(dataImg) == -1) {
                $(".fe-service-table-add .listing-img-upload").append($(this).removeClass("active"));
            }
        });
        $(".fe-service-table-add .listing-img-upload .thumb-wp").each(function () {
            var dataImg = $(this).find("img.thumb").attr("data-img");
            if (Imgs.indexOf(dataImg) == -1 && DbImgs.indexOf(dataImg) == -1) {
                Imgs.push(dataImg);
            }
        });
        autoCloseEBPopup();
        console.log(Imgs);
    }

    function deleteThum(THIS, data, oldImg) {
        oldImg = oldImg || false;
        if (!oldImg) {
            var index = Imgs.indexOf(data);
            if (index != -1) {
                Imgs.splice(index, 1);
            }
        } else {
            ImgsDelete.push(data);
        }
        THIS.parent().remove();
        console.log(Imgs);
        console.log(ImgsDelete);
    }

    function Bind_UserImagesToHDF() {
        var jsonImages = JSON.stringify(Imgs);
        var jsonImagesDel = JSON.stringify(ImgsDelete);
        $("#HDF_UserImages").val(jsonImages);
        $("#HDF_UserImagesDelete").val(jsonImagesDel);
    }

    function OpenIframeImage() {

        $(".plugin-image").openEBPopup();
        $(".plugin-image-view").click();
        // Bind Suggestion
        Bind_Suggestion();

        $('#EBPopup .listing-img-upload').mCustomScrollbar({
            theme: "dark-2",
            scrollInertia: 100
        });

        //============================
        // Datepicker
        //============================
        $('.txtDateTime').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'vi',
            startDate: '2014/10/10',
            format: 'd/m/Y',
            dateonly: true,
            showHour: false,
            showMinute: false,
            timepicker: false,
            onChangeDateTime: function (dp, $input) { }
        });

        // execute thumbnail size
        //excThumbWidth();

    }

    function Plugin_Images_Load() {
        var TimeFrom = $("#EBPopup .TxtDateTimeFrom").val() != undefined ? $("#EBPopup .TxtDateTimeFrom").val() : "",
            TimeTo = $("#EBPopup .TxtDateTimeTo").val() != undefined ? $("#EBPopup .TxtDateTimeTo").val() : "",
            SgCode = $("#HDF_Suggestion_Code").val(),
            SgField = $("#HDF_Suggestion_Field").val();
        $.ajax({
            type: "POST",
            url: "/GUI/FrontEnd/Service/Service_Detail.aspx/Ajax_LoadImages",
            data: '{TimeFrom : "' + TimeFrom + '", TimeTo : "' + TimeTo + '", SgCode : "' + SgCode + '", SgField : "' + SgField + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                var mission = JSON.parse(response.d);
                if (mission.success) {
                    var OBJ = JSON.parse(mission.msg);
                    var Thumbs = "";
                    if (OBJ.length > 0) {
                        $.each(OBJ, function (i, v) {
                            Thumbs += '<div class="thumb-wp" onclick="Plugin_Images_Chose(event, $(this))">' +
                                        '<img class="thumb" alt="" title="" src="http://kh.30shine.com' + v.Value + '" data-img="' + v.Value + '"/>' +
                                        '<span class="delete-thumb" onclick="deleteThum($(this), ' + "'" + v.Value + "'" + ', false)"></span>' +
                                        '<div class="thumb-cover"></div>' +
                                    '</div>';
                        });
                    } else {
                        //
                    }
                    $("#EBPopup .listing-img-upload").empty().append($(Thumbs));
                    // execute thumbnail size
                    excThumbWidth();
                } else {
                    //
                }
            },
            failure: function (response) { alert(response.d); }
        });
    }

    //============================
    // Suggestion Functions
    //============================
    function Bind_Suggestion() {
        $(".eb-suggestion").bind("keyup", function (e) {
            if (e.keyCode == 40) {
                UpDownListSuggest($(this));
            } else {
                Call_Suggestion($(this));
            }
        });
        $(".eb-suggestion").bind("focus", function () {
            Call_Suggestion($(this));
        });
        $(".eb-suggestion").bind("blur", function () {
            //Exc_To_Reset_Suggestion($(this));
        });
        $(window).unbind("click").bind("click", function (e) {
            if ((!e.target.className.match("eb-select") && e.target.parentElement.className.match("ul-listing-suggestion")) ||
                (!e.target.className.match("eb-select") && !e.target.parentElement.className.match("ul-listing-suggestion"))) {
                EBSelect_HideBox();
            }
        });
    }

    function UpDownListSuggest(This) {
        var UlSgt = This.parent().find(".ul-listing-suggestion"),
            index = 0,
            LisLen = UlSgt.find(">li").length - 1,
            Value;

        This.blur();
        UlSgt.find(">li.active").removeClass("active");
        UlSgt.find(">li:eq(" + index + ")").addClass("active");

        $(window).unbind("keydown").bind("keydown", function (e) {
            if (e.keyCode == 40) {
                if (index == LisLen) return false;
                UlSgt.find(">li.active").removeClass("active");
                UlSgt.find(">li:eq(" + (++index) + ")").addClass("active");
                return false;
            } else if (e.keyCode == 38) {
                if (index == 0) return false;
                UlSgt.find(">li.active").removeClass("active");
                UlSgt.find(">li:eq(" + (--index) + ")").addClass("active");
                return false;
            } else if (e.keyCode == 13) {
                // Bind data to HDF Field
                var THIS = UlSgt.find(">li.active");
                var Value = THIS.text().trim();
                //var Value = THIS.attr("data-code");
                var dataField = This.attr("data-field");

                BindIdToHDF(THIS, Value, dataField, "#HDF_Suggestion_Code", "#HDF_Suggestion_Field", This);
                EBSelect_HideBox();
            }
        });
    }

    function Exc_To_Reset_Suggestion(This) {
        var value = This.val();
        if (value == "") {
            $(".eb-suggestion").each(function () {
                var THIS = $(this);
                var sgValue = THIS.val();
                if (sgValue != "") {
                    BindIdToHDF(THIS, sgValue, THIS.attr("data-field"), "#HDF_Suggestion_Code", "#HDF_Suggestion_Field", THIS);
                    return false;
                }
            });
        }
    }

    function Call_Suggestion(This) {
        var text = This.val(),
            field = This.attr("data-field");
        Suggestion(This, text, field);
    }

    function Suggestion(This, text, field) {
        var This = This;
        var text = text || "";
        var field = field || "";
        var InputDomId;
        var HDF_Sgt_Code = "#HDF_Suggestion_Code";
        var HDF_Sgt_Field = "#HDF_Suggestion_Field";

        if (text == "") return false;

        switch (field) {
            //case "customer.name": InputDomId = "#CustomerName"; break;
            case "customer.phone": InputDomId = "#EBPopup .SgCustomerPhone"; break;
            case "customer.code": InputDomId = "#EBPopup .SgCustomerCode"; break;
                //case "bill.code": InputDomId = "#BillCode"; break;
        }

        $.ajax({
            type: "POST",
            url: "/GUI/SystemService/Ajax/Suggestion.aspx/Suggestion_Customer",
            data: '{field : "' + field + '", text : "' + text + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                var mission = JSON.parse(response.d);
                if (mission.success) {
                    var OBJ = JSON.parse(mission.msg);
                    if (OBJ.length > 0) {
                        var lis = "";
                        $.each(OBJ, function (i, v) {
                            lis += "<li data-code='" + v.Customer_Code + "'" +
                                         "onclick=\"BindIdToHDF($(this),'" + v.Value + "','" + field + "','" + HDF_Sgt_Code +
                                         "','" + HDF_Sgt_Field + "','" + InputDomId + "')\">" +
                                        v.Value +
                                    "</li>";
                        });
                        This.parent().find("ul.ul-listing-suggestion").empty().append(lis).parent().show();
                        This.parent().find(".eb-select-data").show();
                    } else {
                        This.parent().find(".eb-select-data").hide().find("ul.ul-listing-suggestion").empty();
                    }
                } else {
                    This.parent().find("ul.ul-listing-suggestion").empty();
                    var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                    showMsgSystem(msg, "warning");
                }
            },
            failure: function (response) { alert(response.d); }
        });
    }

    function BindIdToHDF(THIS, Code, Field, HDF_Sgt_Code, HDF_Sgt_Field, Input_DomId) {
        var text = THIS.text().trim();
        $("input.eb-suggestion").val("");
        $(HDF_Sgt_Code).val(Code);
        $(HDF_Sgt_Field).val(Field);
        $(Input_DomId).val(text);
        $(Input_DomId).parent().find(".eb-select-data").hide();
        // Auto post server
        //$("#BtnFakeUP").click();
    }

    function EBSelect_HideBox() {
        $(".eb-select-data").hide();
        $("ul.ul-listing-suggestion li.active").removeClass("active");
    }
</script>
<!-- END Suggestion -->

<script type="text/javascript">
    jQuery(document).ready(function () {
        // Add active menu
        $("#glbService").addClass("active");
        $(".li-edit").addClass("active");

        // Price Format
        $("#BoxMoneyPrice").text(FormatPrice($("#BoxMoneyPrice").text())).show();
        $("#TotalMoney").val(FormatPrice($("#TotalMoney").val()));

        $(".box-money").each(function () {
            $(this).text(FormatPrice($(this).text().trim()));
        });

        $(".td-product-price").each(function () {
            $(this).text(FormatPrice($(this).text().trim()));
        });

        //============================
        // Show System Message
        //============================ 
        var qs = getQueryStrings();
        showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);

        //==============================
        // Upload Images
        //==============================
        $("#UploadFile").unbind("change").bind("change", function () {
            readImage(this);
        });

        // Btn Send
        $("#BtnSend").bind("click", function () {
            Bind_UserImagesToHDF();
            $("#BtnFakeSend").click();
        });


        //============================
        // Datepicker
        //============================
        $('.txtDateTime').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'vi',
            startDate: '2014/10/10',
            format: 'd/m/Y',
            dateonly: true,
            showHour: false,
            showMinute: false,
            timepicker: false,
            onChangeDateTime: function (dp, $input) { }
        });

    });

    function showMsgSystem(msg, status) {
        $("#MsgSystem").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);
        setTimeout(function () {
            $("#MsgSystem").fadeTo("slow", 0, function () {
                $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 1);
            });
        }, 5000);
    }

    //=================================================
    // Xử lý ảnh
    //=================================================
    //draw image while upload
    var ListImgName = [],
        ListImgSrc = [],
        LIstImgDelete = [],
        ImgStandardWidth = 120,
        ImgStandardHeight = 120;

    function readImage(THIS) {
        var LoopName = 0;
        if (THIS.files.length > 0) {
            for (var i = 0; i < THIS.files.length; i++) {
                var FR = new FileReader();
                FR.onload = function (e) {
                    var img = new Image();
                    img.src = e.target.result;
                    img.onload = function () {
                        ListImgSrc.push(img.src);
                        var width = ImgStandardWidth;
                        var height = ImgStandardWidth * img.height / img.width;
                        var left = 0;
                        var top = (ImgStandardHeight - height) / 2;
                        var ThumbWp = "<div class='thumb-wp'>" +
                                        "<img class='thumb' style='width:" + width + "px;height:" +
                                        height + "px;left:" + left + "px;top:" + top + "px;' " + "src='" + img.src + "' />" +
                                      "<span class=\"delete-thumb\" onclick=\"deleteThum($(this), '" + img.src + "')\"></span></div>";
                        $(".listing-img-upload").append($(ThumbWp));

                    };
                };
                FR.readAsDataURL(THIS.files[i]);
            }
        }
    }

    function Bind_UserImagesToHDF1() {
        var jsonImages = JSON.stringify(ListImgSrc);
        var jsonImagesDel = JSON.stringify(LIstImgDelete);
        $("#HDF_UserImages").val(jsonImages);
        $("#HDF_UserImagesDelete").val(jsonImagesDel);
    }

    function deleteThum1(THIS, data, oldImg) {
        oldImg = oldImg || false;
        if (!oldImg) {
            var index = ListImgSrc.indexOf(data);
            if (index != -1) {
                ListImgSrc.splice(index, 1);
            }
        } else {
            LIstImgDelete.push(data);
        }
        THIS.parent().remove();
    }

    function excThumbWidth() {
        var ImgStandardWidth = 120,
            ImgStandardHeight = 120;
        var width = ImgStandardWidth,
            height, left, top;
        $(".thumb-wp .thumb").each(function () {
            height = ImgStandardWidth * $(this).height() / $(this).width();
            left = 0;
            top = (ImgStandardHeight - height) / 2;
            $(this).css({ "width": width, "height": height, "left": left, "top": top });
        });
    }

    function AjaxUploadImage_BillService(ImageName) {
        $.ajax({
            type: "POST",
            url: "/GUI/SystemService/Ajax/UploadImage.aspx/Add_User_Hair",
            data: '{ImageName : "' + ImageName + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                var mission = JSON.parse(response.d);
                if (mission.success) {
                    +
                        ListImgName.push(mission.msg);
                } else {
                    var msg = "Đã có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển";
                    showMsgSystem(msg, "warning");
                }
            },
            failure: function (response) { alert(response.d); }
        });
    }



</script>

</asp:Content>
