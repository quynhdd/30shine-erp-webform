﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Data.Entity.Migrations;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.Survey
{
    public partial class Survey_Staff : System.Web.UI.Page
    {
        private string PageID = "KS_ADD";
        private bool Perm_Access = false;
        public CultureInfo culture = new CultureInfo("vi-VN");
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                ////Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                ////Perm_ShowSalon = permissionModel.GetActionByActionNameAndPageId("Perm_ShowSalon", PageID, permission);
                ////Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }


        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Today);
                Library.Function.bindSalon(new List<DropDownList>() { Salon }, Perm_Access);
                Bind_ratingValue();
                Bind_infoThread();
                var SalonId = Session["staffServey_salonId"] != null ? Convert.ToInt32(Session["staffServey_salonId"]) : 0;
                if (SalonId > 0)
                {
                    var items = Salon.Items.FindByValue(SalonId.ToString());
                    if (items != null)
                    {
                        items.Selected = true;
                    }
                }
            }
        }

        protected void AddServey(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                if (TxtDateTimeFrom.Text != "")
                {
                    var obj = new Tbl_Staff_Survey();
                    obj.SalonId = int.TryParse(Salon.SelectedValue.ToString(), out integer) ? integer : 0;
                    obj.StylistId = int.TryParse(HDF_StylistId.Value.ToString(), out integer) ? integer : 0;
                    obj.SkinnerId = int.TryParse(HDF_SkinnerId.Value.ToString(), out integer) ? integer : 0;
                    obj.ServeyDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                    obj.Point = int.TryParse(Mark.SelectedValue, out integer) ? integer : 0;
                    obj.Thread = int.TryParse(Thread.SelectedValue, out integer) ? integer : 0;
                    obj.Note = Note.Text;
                    obj.CreatedDate = DateTime.Now;
                    obj.IsDelete = 0;

                    if (obj.SalonId > 0)
                    {
                        Session["staffServey_salonId"] = obj.SalonId;
                    }

                    db.Tbl_Staff_Survey.AddOrUpdate(obj);
                    var exc = db.SaveChanges();
                    // Thông báo thành công
                    var MsgParam = new List<KeyValuePair<string, string>>();
                    if (exc > 0)
                    {
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));                        
                    }
                    else
                    {
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "warning"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thất bại!"));
                    }
                    UIHelpers.Redirect("/khao-sat/nhan-vien.html", MsgParam);
                }
            }
        }

        private void Bind_ratingValue()
        {
            using (var db = new Solution_30shineEntities())
            {
                var ratingValue = db.Rating_ConfigPoint.Where(w => w.IsDelete != 1 && w.Status == 1 && w.Hint == 2).ToList();
                var Key = 0;
                var item = new ListItem();
                if (ratingValue.Count > 0)
                {
                    foreach (var v in ratingValue)
                    {
                        item = new ListItem(v.ConventionName, v.ConventionPoint.ToString());
                        Mark.Items.Insert(Key, item);
                        Key++;
                    }
                    //Mark.SelectedIndex = 0;
                }
            }
        }

        private void Bind_infoThread()
        {
            using (var db = new Solution_30shineEntities())
            {
                var thread = db.SocialThreads.Where(w => w.IsDelete != 1 && w.Publish == 1 && w.stType == 2).ToList();
                var Key = 0;
                var item = new ListItem();
                if (thread.Count > 0)
                {
                    foreach (var v in thread)
                    {
                        item = new ListItem(v.Name, v.Id.ToString());
                        Thread.Items.Insert(Key, item);
                        Key++;
                    }
                    //Thread.SelectedIndex = 0;
                }
            }
        }

        /// <summary>
        /// Check permission
        /// </summary>
        //private void CheckPermission()
        //{
        //    if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
        //    {
        //        var MsgParam = new List<KeyValuePair<string, string>>();
        //        UIHelpers.Redirect("/dang-nhap.html", MsgParam);
        //    }
        //    else
        //    {
        //        string[] Allow = new string[] { "root", "admin", "servey" };
        //        var Permission = Session["User_Permission"].ToString().Trim();
        //        if (Array.IndexOf(Allow, Permission) != -1)
        //        {
        //            Perm_Access = true;
        //        }
        //    }

        //    // Call execute function
        //    ExecuteByPermission();
        //}

        //private void ExecuteByPermission()
        //{
        //    if (!Perm_Access)
        //    {
        //        ContentWrap.Visible = false;
        //        NotAllowAccess.Visible = true;
        //    }
        //}
    }
}