﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Script.Serialization;
using ExportToExcel;
using Excel;
using LinqKit;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.Survey
{
    public partial class Survey_Listing : System.Web.UI.Page
    {
        private string PageID = "KS_NV";
        protected Paging PAGING = new Paging();
        protected List<List<ProductBasic>> ProductList;
        private Expression<Func<BillService, bool>> Where = PredicateBuilder.True<BillService>();
        CultureInfo culture = new CultureInfo("vi-VN");

        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;
        List<_30shine.MODEL.ENTITY.EDMX.Staff> _LstNhanVien;
        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

        protected string table = "";

        private string sql = "";
        private string where = "";
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDate", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ViewAllDate = permissionModel.CheckPermisionByAction("Perm_ViewAllDate", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }


        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList>() { Salon }, Perm_ViewAllData);
                Bind_StaffType();
                GenWhere();
                Bind_Paging();
                Bind_Data();
            }
            else
            {
                GenWhere();
            }
        }

        protected void FillLstStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                _LstNhanVien = db.Staffs.ToList();
            }
        }

        private void GenWhere()
        {
            int integer;
            int salonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
            int staffType = int.TryParse(StaffType.SelectedValue, out integer) ? integer : 0;
            int staffId = int.TryParse(HDF_StaffId.Value, out integer) ? integer : 0;
            DateTime timeFrom = Convert.ToDateTime("23/3/2016", culture);
            DateTime timeTo = DateTime.Now;

            if (TxtDateTimeFrom.Text != "")
            {
                timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                if (TxtDateTimeTo.Text != "")
                {
                    timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                    if (timeTo.Year >= timeFrom.Year && timeTo.Month >= timeFrom.Month && timeTo.Day >= timeFrom.Day)
                    {
                        timeTo = timeTo.AddDays(1);
                    }
                }
                else
                {
                    timeTo = timeFrom.AddDays(1);
                }
            }

            /// Generate sql
            // Trường hợp chọn nhân viên
            if (staffId > 0)
            {
                sql = @"select a.*, e.Name as threadName, f.ConventionName as ratingName, b.Name as salonName, c.Fullname as stylistName, d.Fullname as skinnerName
                        from Tbl_Staff_Survey as a
                        inner join Tbl_Salon as b
                        on a.SalonId = b.Id
                        left join Staff as c
                        on a.StylistId = c.Id
                        left join Staff as d
                        on a.SkinnerId = d.Id
                        inner join SocialThread as e
                        on a.Thread = e.Id
                        inner join Rating_ConfigPoint as f
                        on a.Point = f.ConventionPoint and f.Hint = 2
                        where a.IsDelete != 1
                        and ( a.ServeyDate between '" + String.Format("{0:yyyy/MM/dd}", timeFrom) + "' and '" + String.Format("{0:yyyy/MM/dd}", timeTo) + "' )" +
                        "and (StylistId = " + staffId + " or SkinnerId = " + staffId + ") ";
                if (salonId > 0)
                {
                    sql += " and a.SalonId = " + salonId;
                }
            }
            // Trường hợp filter theo bộ phận nhân viên
            else if (staffType > 0)
            {
                sql = @"select a.*, e.Name as threadName, f.ConventionName as ratingName, b.Name as salonName, c.Fullname as stylistName, d.Fullname as skinnerName
                        from Tbl_Staff_Survey as a
                        inner join Tbl_Salon as b
                        on a.SalonId = b.Id ";
                if (salonId > 0)
                {
                    sql += " and a.SalonId = " + salonId;
                }
                // stylist
                if (staffType == 1)
                {
                    sql += @"inner join Staff as c
                            on a.StylistId = c.Id and c.[Type] = " + staffType +
                            @"left join Staff as d
                            on a.SkinnerId = d.Id
                            inner join SocialThread as e
                            on a.Thread = e.Id
                            inner join Rating_ConfigPoint as f
                            on a.Point = f.ConventionPoint and f.Hint = 2
                            where a.IsDelete != 1
                            and ( a.ServeyDate between '" + String.Format("{0:yyyy/MM/dd}", timeFrom) + "' and '" + String.Format("{0:yyyy/MM/dd}", timeTo) + @"' )
                            and a.StylistId > 0 ";
                }
                // skinner
                else if (staffType == 2)
                {
                    sql += @"left join Staff as c
                            on a.StylistId = c.Id
                            inner join Staff as d
                            on a.SkinnerId = d.Id and d.[Type] = " + staffType +
                            @"inner join SocialThread as e
                            on a.Thread = e.Id
                            inner join Rating_ConfigPoint as f
                            on a.Point = f.ConventionPoint and f.Hint = 2
                            where a.IsDelete != 1
                            and ( a.ServeyDate between '" + String.Format("{0:yyyy/MM/dd}", timeFrom) + "' and '" + String.Format("{0:yyyy/MM/dd}", timeTo) + @"' )
                            and a.SkinnerId > 0 ";
                }
            }
            // Trường hợp chung
            else
            {
                sql = @"select a.*, e.Name as threadName, f.ConventionName as ratingName, b.Name as salonName, c.Fullname as stylistName, d.Fullname as skinnerName
                        from Tbl_Staff_Survey as a
                        inner join Tbl_Salon as b
                        on a.SalonId = b.Id
                        left join Staff as c
                        on a.StylistId = c.Id
                        left join Staff as d
                        on a.SkinnerId = d.Id
                        inner join SocialThread as e
                        on a.Thread = e.Id
                        inner join Rating_ConfigPoint as f
                        on a.Point = f.ConventionPoint and f.Hint = 2
                        where a.IsDelete != 1
                        and ( a.ServeyDate between '" + String.Format("{0:yyyy/MM/dd}", timeFrom) + "' and '" + String.Format("{0:yyyy/MM/dd}", timeTo) + "' ) ";
                
            }
            if (salonId > 0)
            {
                sql += " and a.SalonId = " + salonId;
            }
        }

        private void Bind_Data()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Database.SqlQuery<cls_Servey>(sql).OrderBy(o=>o.Id).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                Rpt_Servey.DataSource = lst;
                Rpt_Servey.DataBind();
            }
        }

        private void Bind_StaffType()
        {
            using (var db = new Solution_30shineEntities())
            {
                var staffType = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Id).ToList();
                var Key = 0;

                StaffType.DataTextField = "Type";
                StaffType.DataValueField = "Id";

                ListItem item = new ListItem("Chọn bộ phận", "0");
                StaffType.Items.Insert(0, item);

                foreach (var v in staffType)
                {
                    Key++;
                    item = new ListItem(v.Name, v.Id.ToString());
                    StaffType.Items.Insert(Key, item);
                }
                StaffType.SelectedIndex = 0;
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
            Bind_Data();
            ScriptManager.RegisterStartupScript(UPTableListing, this.GetType(), "Format Price", "UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing();", true);
            RemoveLoading();
        }

        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? (HDF_Page.Value != "" ? Convert.ToInt32(HDF_Page.Value) : 1) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                var Count = db.Tbl_Staff_Survey.SqlQuery(sql).Count();
                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        protected void Btn_SwitchExcel(object sender, EventArgs e)
        {
            if (HDF_ExcelType.Value == "Export")
            {
                //Exc_ExportExcel();
            }
            else if (HDF_ExcelType.Value == "Import")
            {
                //Exc_ImportExcel();
            }

        }

        //        protected void Exc_ExportExcel(object sender, EventArgs e)
        //        {
        //            using (var db = new Solution_30shineEntities())
        //            {
        //                if (TxtDateTimeFrom.Text.Trim() != "")
        //                {
        //                    int Segment = !HDF_EXCELSegment.Value.Equals("") ? Convert.ToInt32(HDF_EXCELSegment.Value) : PAGING._Segment;
        //                    int PageNumber = IsPostBack ? (HDF_EXCELPage.Value != "" ? Convert.ToInt32(HDF_EXCELPage.Value) : 1) : 1;
        //                    PageNumber = PageNumber > 0 ? PageNumber : 1;
        //                    int Offset = (PageNumber - 1) * Segment;
        //                    DateTime d1 = Convert.ToDateTime(TxtDateTimeFrom.Text.Trim(), culture);
        //                    DateTime d2;
        //                    if (TxtDateTimeTo.Text.Trim() != "")
        //                    {
        //                        d2 = Convert.ToDateTime(TxtDateTimeTo.Text.Trim(), culture).AddDays(1);
        //                    }
        //                    else
        //                    {
        //                        d2 = d1.AddDays(1);
        //                    }
        //                    string timeFrom = String.Format("{0:MM/dd/yyyy}", d1);
        //                    string timeTo = String.Format("{0:MM/dd/yyyy}", d2);

        //                    var LST = Get_DataByDate(0, 1000000, "CreatedDate");

        //                    var _ExcelHeadRow = new List<string>();
        //                    var serializer = new JavaScriptSerializer();
        //                    var BillRowLST = new List<List<string>>();
        //                    var BillRow = new List<string>();

        //                    _ExcelHeadRow.Add("Ngày");
        //                    _ExcelHeadRow.Add("Tgian hoàn thành (phút)");
        //                    _ExcelHeadRow.Add("Tên KH");
        //                    _ExcelHeadRow.Add("Mã KH");
        //                    _ExcelHeadRow.Add("Số ĐT");
        //                    _ExcelHeadRow.Add("Tổng số lần sử dụng DV");
        //                    _ExcelHeadRow.Add("Đánh giá");

        //                    // Listing service
        //                    //var LST_Service = db.Services.Where(w=>w.IsDelete != 1).OrderByDescending(o => o.Id).ToList();
        //                    string sql_service = @"select b.* 
        //                                    from
        //                                    (
        //                                    select ServiceId from FlowService
        //                                    where IsDelete != 1
        //                                    and (CreatedDate between '" + timeFrom + "' and '" + timeTo + "')" +
        //                                        @"group by ServiceId
        //                                    ) as a
        //                                    join Service as b
        //                                    on a.ServiceId = b.Id";
        //                    var LST_Service = db.Services.SqlQuery(sql_service).ToList();
        //                    if (LST_Service.Count > 0)
        //                    {

        //                        foreach (var v in LST_Service)
        //                        {
        //                            _ExcelHeadRow.Add(v.Name + " ( " + v.Code + " ) ");
        //                        }
        //                        _ExcelHeadRow.Add("D.Thu Dịch vụ");
        //                    }

        //                    // Listing product
        //                    //var LST_Product = db.Products.Where(w => w.IsDelete != 1).OrderByDescending(o => o.Id).ToList();
        //                    string sql_product = @"select b.* 
        //                                    from
        //                                    (
        //                                    select ProductId from FlowProduct
        //                                    where IsDelete != 1
        //                                    and (CreatedDate between '" + timeFrom + "' and '" + timeTo + "')" +
        //                                        @"group by ProductId
        //                                    ) as a
        //                                    join Product as b
        //                                    on a.ProductId = b.Id";
        //                    var LST_Product = db.Products.SqlQuery(sql_product).ToList();
        //                    if (LST_Product.Count > 0)
        //                    {
        //                        foreach (var v in LST_Product)
        //                        {
        //                            _ExcelHeadRow.Add(v.Name + " ( " + v.Code + " ) ");
        //                        }
        //                        _ExcelHeadRow.Add("D.Thu Sản phẩm");
        //                    }
        //                    _ExcelHeadRow.Add("Tổng D.Thu");
        //                    _ExcelHeadRow.Add("Stylist");
        //                    _ExcelHeadRow.Add("Skinner");
        //                    _ExcelHeadRow.Add("NV bán mỹ phẩm");

        //                    if (LST.Count > 0)
        //                    {
        //                        var ListService_Quantity = new List<int>();
        //                        var ListProduct_Quantity = new List<int>();
        //                        var ServiceListThis = new List<ProductBasic>();
        //                        var ProductListThis = new List<ProductBasic>();
        //                        var Service_TotalMoney = 0;
        //                        var Product_TotalMoney = 0;
        //                        var TotalMoney = 0;

        //                        foreach (var v in LST)
        //                        {
        //                            Service_TotalMoney = 0;
        //                            Product_TotalMoney = 0;
        //                            TotalMoney = 0;
        //                            BillRow = new List<string>();

        //                            BillRow.Add(Convert.ToDateTime(v.CreatedDate).ToString("dd/MM/yyyy"));
        //                            BillRow.Add(v.TimeCompleteBill_Int.ToString());
        //                            BillRow.Add(v.CustomerName);
        //                            BillRow.Add(v.CustomerCode);
        //                            BillRow.Add(v.CustomerPhone);
        //                            BillRow.Add(v.CustomerReturn.ToString());
        //                            BillRow.Add(v.Mark.ToString());

        //                            if (v.ServiceIds != null)
        //                            {
        //                                ServiceListThis = serializer.Deserialize<List<ProductBasic>>(v.ServiceIds);
        //                            }

        //                            if (v.ProductIds != null)
        //                            {
        //                                ProductListThis = serializer.Deserialize<List<ProductBasic>>(v.ProductIds);
        //                            }

        //                            if (LST_Service.Count > 0)
        //                            {
        //                                foreach (var v2 in LST_Service)
        //                                {
        //                                    if (ServiceListThis != null)
        //                                    {
        //                                        var ExistId = ServiceListThis.Find(x => x.Id == v2.Id);
        //                                        if (ExistId.Code != null)
        //                                        {
        //                                            Service_TotalMoney += (ExistId.Quantity * ExistId.Price * (100 - ExistId.VoucherPercent) / 100);
        //                                            BillRow.Add(ExistId.Quantity.ToString());
        //                                        }
        //                                        else
        //                                        {
        //                                            BillRow.Add("0");
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        BillRow.Add("0");
        //                                    }
        //                                }
        //                            }

        //                            BillRow.Add(Service_TotalMoney.ToString());

        //                            if (LST_Product.Count > 0)
        //                            {
        //                                foreach (var v2 in LST_Product)
        //                                {
        //                                    if (ProductListThis != null)
        //                                    {
        //                                        var ExistId = ProductListThis.Find(x => x.Id == v2.Id);
        //                                        if (ExistId.Code != null)
        //                                        {
        //                                            Product_TotalMoney += ExistId.Promotion != 0 ? (ExistId.Quantity * ExistId.Price - ExistId.Promotion) : (ExistId.Quantity * ExistId.Price * (100 - ExistId.VoucherPercent) / 100);
        //                                            BillRow.Add(ExistId.Quantity.ToString());
        //                                        }
        //                                        else
        //                                        {
        //                                            BillRow.Add("0");
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        BillRow.Add("0");
        //                                    }
        //                                }
        //                            }

        //                            TotalMoney = Service_TotalMoney + Product_TotalMoney;

        //                            BillRow.Add(Product_TotalMoney.ToString());
        //                            BillRow.Add(TotalMoney.ToString());
        //                            BillRow.Add(v.HairdresserName);
        //                            BillRow.Add(v.HairMassageName);
        //                            BillRow.Add(v.CosmeticName);
        //                            BillRowLST.Add(BillRow);
        //                        }
        //                    }

        //                    // export
        //                    var ExcelStorePath = Server.MapPath("~") + "Public/Excel/Hoa.Don/";
        //                    var FileName = "Hoa_Don_" + string.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
        //                    var UrlFileName = "http://" + Request.Url.Host + "/Public/Excel/Hoa.Don/" + FileName;

        //                    ExportXcel(_ExcelHeadRow, BillRowLST, ExcelStorePath + FileName);
        //                    Bind_Paging();
        //                    ScriptManager.RegisterStartupScript(BtnFakeExcel, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "');UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing();", true);  
        //                }              
        //            }            
        //        }

        protected void Exc_ImportExcel()
        {
            var _ExcelHeadRow = new List<string>();
            _ExcelHeadRow.Add("STT");
            _ExcelHeadRow.Add("Field1");
            _ExcelHeadRow.Add("Field2");
            _ExcelHeadRow.Add("Field3");

            var LST = new List<Tuple<int, string, string, string>>();

            for (var i = 0; i < 5; i++)
            {
                var v = Tuple.Create(i, "str1-" + i, "str2-" + i, "str3-" + i);
                LST.Add(v);
            }

            // import
            var Path = Server.MapPath("~") + "Public/Excel/Hoa.Don/" + "Hoa_Don_28_07_2015_06_28_13.xlsx";
            Response.Write(Path);
            ImportXcel(Path);
        }

        private static void ExportXcel(List<string> _ExcelHeadRow, List<List<string>> List, string Path)
        {
            try
            {
                CreateExcelFile.ExcelHeadRow = _ExcelHeadRow;
                CreateExcelFile.CreateExcel(List, Path);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi. Không tạo được file excel.\r\nException: " + ex.Message);
            }
        }

        private void ImportXcel(string Path)
        {
            var tblDom = "<table><tbody>";
            foreach (var worksheet in Workbook.Worksheets(@Path))
                foreach (var row in worksheet.Rows)
                {
                    tblDom += "<tr>";
                    foreach (var cell in row.Cells)
                    {
                        if (cell != null)
                        {
                            tblDom += "<td>" + cell.Text + "</td>";
                        }
                    }
                    tblDom += "<tr>";
                }
            // if (cell != null) // Do something with the cells
            tblDom += "</tbody></table>";
            Response.Write(tblDom);
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        public string GenTime(int minutes)
        {
            string time = "";
            int hour = minutes / 60;
            int minute = minutes % 60;
            if (hour > 0)
            {
                time += hour + "h";
                time += minute + "p";
            }
            else
            {
                if (minute > 0)
                {
                    time += minute + " p";
                }
                else
                {
                    time += "-";
                }
            }
            return time;
        }

        /// <summary>
        /// Check permission
        /// </summary>
        //private void CheckPermission()
        //{
        //    if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
        //    {
        //        var MsgParam = new List<KeyValuePair<string, string>>();
        //        UIHelpers.Redirect("/dang-nhap.html", MsgParam);
        //    }
        //    else
        //    {
        //        string[] Allow = new string[] { "root", "admin", "servey" };
        //        string[] AllowDelete = new string[] { "root", "admin", "servey" };
        //        var Permission = Session["User_Permission"].ToString().Trim();
        //        if (Array.IndexOf(Allow, Permission) != -1)
        //        {
        //            Perm_Access = true;
        //        }
        //        if (Array.IndexOf(AllowDelete, Permission) != -1)
        //        {
        //            Perm_Delete = true;
        //        }
        //    }

        //    // Call execute function
        //    ExecuteByPermission();
        //}

        //private void ExecuteByPermission()
        //{
        //    if (!Perm_Access)
        //    {
        //        ContentWrap.Visible = false;
        //        NotAllowAccess.Visible = true;
        //    }
        //}

        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }
    }

    public class cls_Servey : Tbl_Staff_Survey
    {
        public string salonName { get; set; }
        public string stylistName { get; set; }
        public string skinnerName { get; set; }
        public string threadname { get; set; }
        public string ratingName { get; set; }
    }
}