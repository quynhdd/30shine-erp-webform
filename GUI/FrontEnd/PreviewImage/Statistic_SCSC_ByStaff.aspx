﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Statistic_SCSC_ByStaff.aspx.cs"  MasterPageFile="~/TemplateMaster/SiteMaster.Master"  Inherits="_30shine.GUI.FrontEnd.PreviewImage.Statistic_SCSC_ByStaff" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">

    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <%--<uc1:ReportMenu runat="server" id="ReportMenu" />--%>
        <style>
            .table-wp table thead th { font-weight: normal; font-family: Roboto Condensed Bold; }
            table.table-add.table-listing thead { background: #ddd; }
           .sub-menu ul.ul-sub-menu li  a#aSalon:active ::after :before :visited{
                color: #00ff21;
            }
            .sub-menu ul.ul-sub-menu li a#aStylist:active ::after :before :visited{
                color: #00ff21;
            }
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>SCSC &nbsp;&#187; </li>
                             <li class="be-report-li"><a href="thong-ke-anh.html"><i class="fa fa-th-large"></i>Thống kê số lượng ảnh theo salon</a></li>
                        <li class="be-report-li"><a href="thong-ke-anh-theo-nhan-vien.html"><i class="fa fa-th-large"></i>Thống kê số lượng ảnh theo Stylist</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report" onload="abc()">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row">
                    <div class="filter-item" style="margin-left: 0;">
                        <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="datepicker-wp">
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                                ClientIDMode="Static" runat="server"></asp:TextBox>

                            <strong class="st-head" style="margin-left: 10px; margin-right: 10px;">
                                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                            </strong>
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeTo" placeholder="Đến ngày"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlSalon" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="width: 190px;" OnSelectedIndexChanged="Salon_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlStylist" runat="server" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                    </div>

                    <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" Text="Xem dữ liệu"
                        ClientIDMode="Static" onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>


                    <%--<asp:Panel ID="Btn_ExportExcel" CssClass="st-head btn-viewdata"
                ClientIDMode="Static" onclick="excExcel('Export')" runat="server">Xuất File Excel</asp:Panel>--%>
                    <%--<asp:Panel ID="Btn_ImportExcel" CssClass="st-head btn-viewdata"
                ClientIDMode="Static" onclick="excExcel('Import')" runat="server">Nhập File Excel</asp:Panel>--%>
                </div>
                <!-- End Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <asp:UpdatePanel ID="UPTotal" runat="server" ClientIDMode="Static" style="margin-top: 15px;">
                    <ContentTemplate>
                        <div class="row">
                            <strong class="st-head"><i class="fa fa-file-text"></i>Thống kê SCSC</strong>

                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-wp">
                                    <table class="table-add table-listing" id="tblAllSalon" style="float: left; width: 100%;">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Tổng bill</th>
                                                <th>Bill không ảnh + ảnh Mờ/Lệch</th>
                                                <th>Bill chưa đánh giá</th>
                                                <th>Lỗi Shape</th>
                                                <th>Lỗi Connective</th>
                                                <th>Lỗi SharpNess</th>
                                                <th>Lỗi Completion</th>
                                                <th>Điểm SCSC TB</th>
                                                <th>% Lỗi KCS</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="rptAllSalon" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><b><%# Eval("ShortName") %></b></td>
                                                        <td class="td-SoLanOK"><%#Eval("TongBill") %> </td>
                                                        <td class="td-HinhKhoiChuaChuan"><%#Eval("TongkoAnh") %></td>
                                                        <td class="td-DuongCatChuaNet"><%#Eval("BillChuaDanhGia") %></td>
                                                        <td class="td-HinhKhoiChuaLienKet"><%#Eval("TotalShapeError") %></td>
                                                        <td class="td-NgonTocQuaDay"><%#Eval("TotalConnectiveError") %></td>
                                                        <td class="td-CaoMaiGayLoi"><%#Eval("TotalSharpNessError") %></td>
                                                        <td class="td-VuotSapLoi"><%#Eval("TotalCompletionError") %></td>
                                                        <td class="td-AnhThieuMo"><%#Eval("PointSCSC_TB") %></td>
                                                        <td class="td-BillCoAnh"><%#Eval("Total_SCSC_Error") %>%</td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <asp:Repeater ID="rptOnlySalon" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><b><%# Eval("Fullname") %></b></td>
                                                         <td class="td-SoLanOK"><%#Eval("TotalBill") %> </td>
                                                        <td class="td-SoLanOK"><%#Eval("BillNotImages") %></td>
                                                        <td class="td-HinhKhoiChuaChuan"><%#Eval("TotalBillChuaDanhGia") %></td>
                                                        <td class="td-DuongCatChuaNet"><%#Eval("TotalBillErrorShape") %></td>
                                                        <td class="td-HinhKhoiChuaLienKet"><%#Eval("TotalBillErrorConnective") %></td>
                                                        <td class="td-NgonTocQuaDay"><%#Eval("TotalBillErrorSharpNess") %></td>
                                                        <td class="td-CaoMaiGayLoi"><%#Eval("TotalBillErrorCompletion") %></td>
                                                        <td class="td-VuotSapLoi"><%#Eval("PointSCSC_TB") %></td>
                                                        <td class="td-AnhThieuMo"><%#Eval("Total_SCSC_Error")  %>%</td>

                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>

                                        </tbody>
                                    </table>

                                </div>
                            </div>
                            <div class="col-xs-6">
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>

                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
            </div>
            <%-- END Listing --%>
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                // Add active menu
                //$("#glbAdminSales").addClass("active");
                //$("#glbAdminReportSales").addClass("active");
                //$("li.be-report-li").addClass("active");

                //// Price format
                //UP_FormatPrice('.be-report-price');

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        //$input.val($input.val().split(' ')[0]);
                    }
                });


            });

        </script>

    </asp:Panel>
</asp:Content>