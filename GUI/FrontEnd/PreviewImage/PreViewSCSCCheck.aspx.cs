﻿using _30shine.Helpers;
using _30shine.MODEL.CustomClass;
using _30shine.MODEL.ENTITY.EDMX;
using Project.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Migrations;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.Interface;

namespace _30shine.GUI.FrontEnd.PreviewImage
{
    public partial class PreViewSCSCCheck : System.Web.UI.Page
    {
        private string PageID = "VIEW_KCS";
        protected string Permission = "";
        protected bool Perm_Access = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_Stylist = false;
        protected string listElement_Perm = string.Empty;
        protected int? TotalPointCate = 0;
        protected int? ShapMaxPoint = 0;
        protected int? ConnectiveMaxPoint = 0;
        protected int? SharpNessMaxPoint = 0;
        protected int? CompletionMaxPoint = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            txtToDate.Value = DateTime.Now.ToString("dd/MM/yyyy");
            //PassDataFromServerToClient();
            if (!IsPostBack)
            {
                BindSCSC_Category();
            }

        }
        private void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_ShowSalon = permissionModel.GetActionByActionNameAndPageId("Perm_ShowSalon", PageID, permission);
                ////Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();

            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }


        protected void BindSCSC_Category()
        {
            using (var db = new Solution_30shineEntities())
            {
                var ListCate_1 = db.SCSC_Category.Where(a => a.Publish == true && a.IsDelete == false && a.SCSC_Cate_IDCate == 1).OrderBy(a => a.SCSC_Cate_Point).ToList();
                rpt_Shape.DataSource = ListCate_1;
                rpt_Shape.DataBind();
                var ListCate_2 = db.SCSC_Category.Where(a => a.Publish == true && a.IsDelete == false && a.SCSC_Cate_IDCate == 2).OrderBy(a => a.SCSC_Cate_Point).ToList();
                rpt_Connective.DataSource = ListCate_2;
                rpt_Connective.DataBind();
                var ListCate_3 = db.SCSC_Category.Where(a => a.Publish == true && a.IsDelete == false && a.SCSC_Cate_IDCate == 3).OrderBy(a => a.SCSC_Cate_Point).ToList();
                rptSharpness.DataSource = ListCate_3;
                rptSharpness.DataBind();
                var ListCate_4 = db.SCSC_Category.Where(a => a.Publish == true && a.IsDelete == false && a.SCSC_Cate_IDCate == 4).OrderBy(a => a.SCSC_Cate_Point).ToList();
                rpt_Completion.DataSource = ListCate_4;
                rpt_Completion.DataBind();

                TotalPointCate = ListCate_1[ListCate_1.Count - 1].SCSC_Cate_Point + ListCate_2[ListCate_2.Count - 1].SCSC_Cate_Point + ListCate_3[ListCate_3.Count - 1].SCSC_Cate_Point + ListCate_4[ListCate_4.Count - 1].SCSC_Cate_Point;

                ShapMaxPoint = ListCate_1[ListCate_1.Count - 1].SCSC_Cate_Point;
                ConnectiveMaxPoint = ListCate_2[ListCate_2.Count - 1].SCSC_Cate_Point;
                SharpNessMaxPoint = ListCate_3[ListCate_3.Count - 1].SCSC_Cate_Point;
                CompletionMaxPoint = ListCate_4[ListCate_4.Count - 1].SCSC_Cate_Point;

            }
        }

        private void PassDataFromServerToClient()
        {
            List<ElementEnable> list = listElement();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jSon = serializer.Serialize(list);
            listElement_Perm = jSon;
        }

        protected List<ElementEnable> listElement()
        {
            List<ElementEnable> list = new List<ElementEnable>();
            list.Add(new ElementEnable() { ElementName = "btn-ok", Enable = Perm_Stylist, Type = "disable" });
            list.Add(new ElementEnable() { ElementName = "btn-cube", Enable = Perm_Stylist, Type = "disable" });
            list.Add(new ElementEnable() { ElementName = "btn-line", Enable = Perm_Stylist, Type = "disable" });
            list.Add(new ElementEnable() { ElementName = "btnAddCycle", Enable = Perm_Stylist, Type = "hidden" });
            list.Add(new ElementEnable() { ElementName = "btnUndoCycle", Enable = Perm_Stylist, Type = "hidden" });
            list.Add(new ElementEnable() { ElementName = "btnSaveError", Enable = Perm_Stylist, Type = "hidden" });
            list.Add(new ElementEnable() { ElementName = "ErrorNote", Enable = Perm_Stylist, Type = "disable" });
            list.Add(new ElementEnable() { ElementName = "btnReset", Enable = Perm_Stylist, Type = "hidden" });
            return list;
        }

        [WebMethod]
        public static List<Staff> ReturnAllStylistBySalon(int _SalonId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Staffs.Where(a => a.IsDelete == 0 && a.Active == 1 && a.Type == 1 && a.SalonId == _SalonId/* && (a.IsHoiQuan != true || a.IsHoiQuan == null)*/ ).ToList();
                return lst;
            }
        }

        [WebMethod]
        public static List<Stylist4Men_Class> ReturnAllClass()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Stylist4Men_Class.Where(a => a.IsDelete == false && a.Publish == true).ToList();
                return lst;
            }
        }

        [WebMethod]
        public static List<Staff> ReturnAllStudentByClass(int _ClassId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Staffs.Where(a => a.IsDelete == 0 && a.Active == 1 && a.Type == 1 /*&& a.S4MClassId == _ClassId && a.IsHoiQuan == true*/).ToList();
                return lst;
            }
        }
        #region Tính tỉ lệ stylist chụp đủ ảnh
        [WebMethod]
        public static string checkStylistFullImage(int stylistID, DateTime date)
        {
            //return date.ToString();
            try
            {
                Solution_30shineEntities db = new Solution_30shineEntities();
                //var bill = (from bs in db.BillServices where bs.Staff_HairMassage_Id == stylistID && bs.CreatedDate.)
                int bill = db.BillServices.Where(bs => bs.CreatedDate.Value.Day == date.Day
                        && bs.CreatedDate.Value.Month == date.Month && bs.CreatedDate.Value.Year == date.Year
                        && bs.Staff_Hairdresser_Id == stylistID
                        && bs.IsDelete != 1 && bs.Pending != 0
                        ).Count();
                int bill2 = db.BillServices.Where(bs => bs.CreatedDate.Value.Day == date.Day
                        && bs.CreatedDate.Value.Month == date.Month && bs.CreatedDate.Value.Year == date.Year
                        && bs.Staff_Hairdresser_Id == stylistID
                        && bs.IsDelete != 1 && bs.Pending != 0 && bs.Images != null
                        ).Count();
                if (bill2 != 0)
                {
                    float tile = bill / bill2;
                    if (tile >= 0.8)
                    {
                        return "Success";
                    }
                    else
                    {
                        return "Errors";
                    }
                }
                else return "Success";

            }
            catch (Exception ex)
            {

                return ex.ToString();
            }

        }
        #endregion
        #region Get bill theo salon
        [WebMethod]
        public static List<BillServiceView> GetRandomBillImage(string _Date, int _SalonId, int _StylistId, string _imageStatus)
        {
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                DateTime _FilterDate = Convert.ToDateTime(_Date, culture);
                string _FromDate = String.Format("{0:yyyy/MM/dd}", _FilterDate);
                string _ToDate = String.Format("{0:yyyy/MM/dd}", _FilterDate.AddDays(1));

                //var sqlCount = @"select count(*) from BillService a where a.CreatedDate between '" + _FromDate + "' and '" + _ToDate + "' and a.IsDelete != 1 and a.Pending = 0 and a.Images is not null and a.SalonId = " + _SalonId;

                //int _TotalBill = db.Database.SqlQuery<int>(sqlCount).First();

                //var sql = "";

                //if (_StylistId != 0)
                //{
                //    sql = @"select a.*,b.Fullname as CustomerName, b.Phone as CustomerPhone ,b.Id as IdCustomer ,c.ID as ID_SCSC, c.ImageError,c.NoteError,c.Shape_ID,c.SharpNess_ID,c.ComPlatetion_ID,c.ConnectTive_ID, c.TotalPointSCSC  from BillService a left join Customer b on a.CustomerId = b.Id left join SCSC_CheckError as c on a.Id = c.BillService_ID where a.Staff_Hairdresser_Id = " + _StylistId + " and a.CreatedDate between '" + _FromDate + "' and '" + _ToDate + "' and a.IsDelete != 1 and a.Pending = 0 and   a.Images is not null and a.SalonId = " + _SalonId;
                //}
                //else
                //{
                //    sql = @"select a.*,b.Fullname as CustomerName, b.Phone as CustomerPhone ,b.Id as IdCustomer, c.ID as ID_SCSC, c.ImageError,c.NoteError,c.Shape_ID,c.SharpNess_ID,c.ComPlatetion_ID,c.ConnectTive_ID , c.TotalPointSCSC   from BillService a left join Customer b on a.CustomerId = b.Id  left join SCSC_CheckError as c on a.Id = c.BillService_ID where a.CreatedDate between '" + _FromDate + "' and '" + _ToDate + "' and a.IsDelete != 1 and a.Pending = 0 and   a.Images is not null and a.SalonId = " + _SalonId;
                //}

                //1-bill không có lỗi cắt || 2-bill có lỗi hình khối || 3-bill lỗi đường cắt  || 4-bill lỗi hình khối chưa liên kết
                //5-bill lỗi ngọn tóc quá dày || 6-bill cạo mai gáy lỗi || 7-bill vuốt sáp lỗi || 8-bill ảnh thiếu/mờ
                //9-bill có ảnh || 10-bill đã đánh giá || 11-bill chưa đánh giá 

                //if (_imageStatus == "1" || _imageStatus == "2" || _imageStatus == "3" || _imageStatus == "4" || _imageStatus == "5" || _imageStatus == "6" || _imageStatus == "7" || _imageStatus == "8")
                //{
                //    sql += " and ImageStatusId like '%" + _imageStatus + "%' ";
                //}
                //else if (_imageStatus == "10")
                //{
                //    sql += " and a.ImageStatusId is not null";
                //}
                //else if (_imageStatus == "11")
                //{
                //    sql += " and a.ImageStatusId is null";
                //}
                //var list = db.Database.SqlQuery<custom_BillService>("exec [Store_SCSC_GetBillImage_V1] {0},{1},{2},{3} ", _FromDate, _ToDate, _SalonId, _StylistId).ToList();

                var sql = @"declare 
                                        @timeFrom datetime,
                                        @timeTo datetime,
                                        @salonId int,
                                        @stylistId int
                                        set @timeFrom='" + _FromDate + @"'
                                        set @timeTo='" + _ToDate + @"'
                                        set @salonId='" + _SalonId + @"'
                                        set @stylistId='" + _StylistId + @"'
                                SELECT a.Id,a.ServiceIds,a.Images,b.Fullname as CustomerName, b.Phone as CustomerPhone ,b.Id as IdCustomer ,c.ID as ID_SCSC,            c.ImageError,c.NoteError,c.Shape_ID,c.SharpNess_ID,c.ComPlatetion_ID,c.ConnectTive_ID, c.TotalPointSCSC,
                               (case when c.TotalPointSCSC is null then -1 else 1 end ) as PointSCSC  
                                from BillService a 
                                left join Customer b on a.CustomerId = b.Id 
                                left join SCSC_CheckError as c on a.Id = c.BillService_ID 
                                WHERE a.CreatedDate between @timeFrom  AND @timeTo
                                and  a.SalonId = @salonId
                                AND ((a.Staff_Hairdresser_Id=@stylistId) OR (@stylistId=0))
                                and a.IsDelete = 0 and a.Pending = 0 
                                and a.Images is not NULL
                                ORDER BY PointSCSC asc, Id asc";
                var list = db.Database.SqlQuery<custom_BillService>(sql).ToList();
                var objSalon = db.Tbl_Salon.Find(_SalonId);

                List<BillServiceView> lstBillView = new List<BillServiceView>();
                BillServiceView obj = new BillServiceView();
                string[] ListImg;

                string _Domain = ConfigurationManager.AppSettings["DomainKey_PreviewImg"];

                foreach (var item in list)
                {
                    ListImg = item.Images.Split(',');
                    obj = new BillServiceView();
                    obj.Id = item.Id;
                    obj.BillDate = string.Format("{0:dd/MM/yyyy}", item.CreatedDate);
                    obj.CustomerPhone = item.CustomerPhone;
                    obj.CustomerName = item.CustomerName;
                    obj.CustomerId = item.IdCustomer;
                    obj.ServiceIds = item.ServiceIds;
                    obj.ID_SCSC = item.ID_SCSC;
                    obj.PointSCSC = item.PointSCSC;
                    if (item.ImageError == true)
                    {
                        obj.ImageError = 1;
                    }
                    else
                    {
                        obj.ImageError = 0;
                    }
                    obj.NoteError = item.NoteError;
                    //obj.Shape_ID = item.Shape_ID;
                    //obj.SharpNess_ID = item.SharpNess_ID;
                    //obj.ComPlatetion_ID = item.ComPlatetion_ID;
                    //obj.ConnectTive_ID = item.ConnectTive_ID;
                    if (item.ImageStatusId != null)
                    {
                        obj.ImageStatusId = item.ImageStatusId;
                    }

                    for (int i = 0; i < ListImg.Length; i++)
                    {
                        if (!(ListImg[i].StartsWith("http")))
                        {
                            ListImg[i] = _Domain + ListImg[i];
                        }
                    }
                    if (ListImg.Count() == 4)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                        obj.Img3 = ListImg[2];
                        obj.Img4 = ListImg[3];
                    }
                    if (ListImg.Count() == 3)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                        obj.Img3 = ListImg[2];
                    }
                    if (ListImg.Count() == 2)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                    }
                    if (ListImg.Count() == 1)
                        obj.Img1 = ListImg[0];

                    obj.SalonName = objSalon.Name;
                    obj.ErrorNote = item.ErrorNote;
                    obj.NoteByStylist = item.NoteByStylist;
                    if (item.ImageChecked1 != null)
                    {
                        obj.Img1 = item.ImageChecked1;
                    }
                    if (item.ImageChecked2 != null)
                    {
                        obj.Img2 = item.ImageChecked2;
                    }
                    if (item.ImageChecked3 != null)
                    {
                        obj.Img3 = item.ImageChecked3;
                    }
                    if (item.ImageChecked4 != null)
                    {
                        obj.Img4 = item.ImageChecked4;
                    }

                    lstBillView.Add(obj);
                }

                return lstBillView;
            }
        }
        #endregion

        #region Get bill theo S4M
        [WebMethod]
        public static List<S4M_Bill> GetBillImage_S4M(string _Date, int _ClassId, int _StylistId, string _imageStatus)
        {
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                DateTime _FilterDate = Convert.ToDateTime(_Date, culture);
                string _FromDate = String.Format("{0:yyyy/MM/dd}", _FilterDate);
                string _ToDate = String.Format("{0:yyyy/MM/dd}", _FilterDate.AddDays(1));

                var sqlCount = @"select count(*) from Stylist4Men_BillCutFree a where a.CreatedTime between '" + _FromDate + "' and '" + _ToDate + "' and a.IsDelete != 1 and a.Images is not null and a.ClassId =" + _ClassId;

                int _TotalBill = db.Database.SqlQuery<int>(sqlCount).First();

                var sql = "";

                if (_StylistId != 0)
                {
                    sql = "select _bill.*, _cus.CustomerName, _cus.CustomerPhone from Stylist4Men_BillCutFree _bill left join Stylist4Men_Customer _cus on _cus.Id = _bill.CustomerId where _bill.CreatedTime between '" + _FromDate + "' and '" + _ToDate + "' and _bill.IsDelete != 1 and _bill.Images is not null and _bill.ClassId = " + _ClassId + " and _bill.StudentId = " + _StylistId;
                }
                else
                {
                    sql = "select _bill.*, _cus.CustomerName, _cus.CustomerPhone from Stylist4Men_BillCutFree _bill left join Stylist4Men_Customer _cus on _cus.Id = _bill.CustomerId where _bill.CreatedTime between '" + _FromDate + "' and '" + _ToDate + "' and _bill.IsDelete != 1 and _bill.Images is not null and _bill.ClassId = " + _ClassId;
                }

                //1-bill không có lỗi cắt || 2-bill có lỗi hình khối || 3-bill lỗi đường cắt  || 4-bill lỗi hình khối chưa liên kết
                //5-bill lỗi ngọn tóc quá dày || 6-bill cạo mai gáy lỗi || 7-bill vuốt sáp lỗi || 8-bill ảnh thiếu/mờ
                //9-bill có ảnh || 10-bill đã đánh giá || 11-bill chưa đánh giá 

                if (_imageStatus == "1" || _imageStatus == "2" || _imageStatus == "3" || _imageStatus == "4" || _imageStatus == "5" || _imageStatus == "6" || _imageStatus == "7" || _imageStatus == "8")
                {
                    sql += " and ImageStatusId like '%" + _imageStatus + "%' ";
                }
                else if (_imageStatus == "10")
                {
                    sql += " and a.ImageStatusId is not null";
                }
                else if (_imageStatus == "11")
                {
                    sql += " and a.ImageStatusId is null";
                }
                var list = db.Database.SqlQuery<S4M_custom_BillService>(sql).ToList();

                var objClass = db.Stylist4Men_Class.Find(_ClassId);

                List<S4M_Bill> lstBillView = new List<S4M_Bill>();
                S4M_Bill obj = new S4M_Bill();
                string[] ListImg;

                string _Domain = ConfigurationManager.AppSettings["DomainKey_PreviewImg"];

                foreach (var item in list)
                {
                    ListImg = item.Images.Split(',');
                    obj = new S4M_Bill();
                    obj.Id = item.Id;
                    obj.NoteByStylist = item.NoteByStylist;
                    obj.BillDate = string.Format("{0:dd/MM/yyyy}", item.CreatedTime);
                    obj.CustomerPhone = item.CustomerPhone;
                    obj.CustomerName = item.CustomerName;
                    obj.CustomerId = item.CustomerId;

                    if (item.ImageStatusId != null)
                    {
                        obj.ImageStatusId = item.ImageStatusId;
                    }

                    for (int i = 0; i < ListImg.Length; i++)
                    {
                        if (!(ListImg[i].StartsWith("http")))
                        {
                            ListImg[i] = _Domain + ListImg[i];
                        }
                    }
                    if (ListImg.Count() == 4)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                        obj.Img3 = ListImg[2];
                        obj.Img4 = ListImg[3];
                    }
                    if (ListImg.Count() == 3)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                        obj.Img3 = ListImg[2];
                    }
                    if (ListImg.Count() == 2)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                    }
                    if (ListImg.Count() == 1)
                        obj.Img1 = ListImg[0];

                    obj.ClassName = objClass.Name;
                    obj.ErrorNote = item.ErrorNote;
                    if (item.ImageChecked1 != null)
                    {
                        obj.Img1 = item.ImageChecked1;
                    }
                    if (item.ImageChecked2 != null)
                    {
                        obj.Img2 = item.ImageChecked2;
                    }
                    if (item.ImageChecked3 != null)
                    {
                        obj.Img3 = item.ImageChecked3;
                    }
                    if (item.ImageChecked4 != null)
                    {
                        obj.Img4 = item.ImageChecked4;
                    }

                    lstBillView.Add(obj);
                }

                return lstBillView;
            }
        }
        #endregion

        [WebMethod(EnableSession = true)]
        public static List<Tbl_Salon> ReturnAllSalon()
        {
            using (var db = new Solution_30shineEntities())
            {
                List<Tbl_Salon> list = new List<Tbl_Salon>();
                var SalonId = Convert.ToInt32(HttpContext.Current.Session["SalonId"].ToString());
                if (SalonId > 0)
                {
                    list = db.Tbl_Salon.Where(w =>
                        w.IsDelete == 0 && w.Publish == true && w.Id == SalonId).ToList();
                }
                else
                {
                    list = db.Tbl_Salon.Where(w => w.IsDelete == 0 && w.Publish == true).ToList();
                }

                return list;
            }
        }

        [WebMethod]
        public static string updateStatus(int billID, string statusID)
        {
            var Message = new Library.Class.cls_message();
            var serialize = new JavaScriptSerializer();
            var error = 0;
            try
            {
                var db = new Solution_30shineEntities();
                var RECORD = db.BillServices.FirstOrDefault(w => w.Id == billID);
                if (RECORD != null)
                {
                    RECORD.ImageStatusId = statusID;
                    RECORD.ModifiedDate = DateTime.Now;
                    db.BillServices.AddOrUpdate(RECORD);
                    error += db.SaveChanges() > 0 ? 0 : 1;
                    if (error == 0)
                    {
                        Message.success = true;
                        Message.message = "Cập nhật thành công.";
                    }
                    else
                    {
                        Message.success = false;
                        Message.message = "Cập nhật thất bại.";
                    }
                }
                else
                {
                    error = 1;
                    Message.success = false;
                    Message.message = "Bản ghi không tồn tại";
                }

                return serialize.Serialize(Message);
            }
            catch (Exception ex)
            {
                Message.success = false;
                Message.message = ex.Message;
                return serialize.Serialize(Message);
            }
        }

        [WebMethod]
        public static cls_Note GetErrorNoteById(int billID)
        {
            cls_Note note = new cls_Note();
            var db = new Solution_30shineEntities();
            var RECORD = db.BillServices.FirstOrDefault(w => w.Id == billID);
            note.NoteByManager = RECORD.ErrorNote;
            note.NoteByStylist = RECORD.NoteByStylist;
            return note;
        }

        //[WebMethod]
        //public static List<KCS_FaceType> GetFaceType()
        //{
        //    var db = new Solution_30shineEntities();
        //    var lst = db.KCS_FaceType.Where(a => a.IsDelete != true).ToList();
        //    return lst;
        //}

        #region Lưu ảnh checked của salon
        [WebMethod]
        public static string LoadImage(int bID, int order, string base64)
        {
            var Message = new Library.Class.cls_message();
            var serialize = new JavaScriptSerializer();
            var error = 0;
            BillServiceView obj = new BillServiceView();
            string[] ListImg;
            string _Domain = ConfigurationManager.AppSettings["DomainKey_PreviewImg"];
            try
            {
                string imgPath = "";
                string ImageName = "";
                string ImgRootPath = "";
                byte[] image64 = Convert.FromBase64String(base64);
                Image image;

                using (MemoryStream ms = new MemoryStream(image64))
                {
                    image = Image.FromStream(ms, true);
                    ImgRootPath = "/Resource/Images/CheckImages/";
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath("~" + ImgRootPath)))
                    {
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~" + ImgRootPath));
                    }
                    string dirFullPath = HttpContext.Current.Server.MapPath("~" + ImgRootPath);
                    ImageName = "img_" + DateTime.Now.ToString("ddMMyyhhmmss") + ".png";
                    image.Save(dirFullPath + ImageName, ImageFormat.Png);
                }
                var db = new Solution_30shineEntities();
                var RECORD = db.BillServices.FirstOrDefault(w => w.Id == bID);
                ListImg = RECORD.Images.Split(',');
                //imgPath = "http://api.30shine.com" + ImgRootPath + ImageName;
                imgPath = "https://erp.30shine.com" + ImgRootPath + ImageName;
                if (RECORD != null)
                {
                    for (int i = 0; i < ListImg.Length; i++)
                    {
                        if (!(ListImg[i].StartsWith("http")))
                        {
                            ListImg[i] = _Domain + ListImg[i];
                        }
                    }

                    if (ListImg.Count() == 4)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                        obj.Img3 = ListImg[2];
                        obj.Img4 = ListImg[3];
                    }
                    if (ListImg.Count() == 3)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                        obj.Img3 = ListImg[2];
                    }
                    if (ListImg.Count() == 2)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                    }
                    if (ListImg.Count() == 1)
                    {
                        obj.Img1 = ListImg[0];
                    }

                    if (order == 1)
                    {
                        RECORD.ImageChecked1 = imgPath;
                    }
                    else if (order == 2)
                    {
                        RECORD.ImageChecked2 = imgPath;
                    }
                    else if (order == 3)
                    {
                        RECORD.ImageChecked3 = imgPath;
                    }
                    else if (order == 4)
                    {
                        RECORD.ImageChecked4 = imgPath;
                    }

                    if (RECORD.ImageChecked1 == null && obj.Img1 != null)
                    {
                        RECORD.ImageChecked1 = obj.Img1;
                    }
                    if (RECORD.ImageChecked2 == null && obj.Img2 != null)
                    {
                        RECORD.ImageChecked2 = obj.Img2;
                    }
                    if (RECORD.ImageChecked3 == null && obj.Img3 != null)
                    {
                        RECORD.ImageChecked3 = obj.Img3;
                    }
                    if (RECORD.ImageChecked4 == null && obj.Img4 != null)
                    {
                        RECORD.ImageChecked4 = obj.Img4;
                    }

                    RECORD.ModifiedDate = DateTime.Now;
                    db.BillServices.AddOrUpdate(RECORD);
                    error += db.SaveChanges() > 0 ? 0 : 1;
                    if (error == 0)
                    {
                        Message.success = true;
                        Message.message = "Cập nhật thành công.";
                    }
                    else
                    {
                        Message.success = false;
                        Message.message = "Cập nhật thất bại.";
                    }
                }
                else
                {
                    error = 1;
                    Message.success = false;
                    Message.message = "Bản ghi không tồn tại";
                }
                return serialize.Serialize(Message);
            }
            catch (Exception ex)
            {
                Message.success = false;
                Message.message = ex.Message;
                return serialize.Serialize(Message);
            }
        }
        #endregion


        #region Lưu ảnh checked của S4M
        [WebMethod]
        public static string LoadImage_S4M(int bID, int order, string base64)
        {
            var Message = new Library.Class.cls_message();
            var serialize = new JavaScriptSerializer();
            var error = 0;
            S4M_Bill obj = new S4M_Bill();
            string[] ListImg;
            string _Domain = ConfigurationManager.AppSettings["DomainKey_PreviewImg"];
            try
            {
                string imgPath = "";
                string ImageName = "";
                string ImgRootPath = "";
                byte[] image64 = Convert.FromBase64String(base64);
                Image image;

                using (MemoryStream ms = new MemoryStream(image64))
                {
                    image = Image.FromStream(ms, true);
                    ImgRootPath = "/Resource/Images/S4M_CheckImages/";
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath("~" + ImgRootPath)))
                    {
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~" + ImgRootPath));
                    }
                    string dirFullPath = HttpContext.Current.Server.MapPath("~" + ImgRootPath);
                    ImageName = "img_" + DateTime.Now.ToString("ddMMyyhhmmss") + ".png";
                    image.Save(dirFullPath + ImageName, ImageFormat.Png);
                }
                var db = new Solution_30shineEntities();
                var RECORD = db.Stylist4Men_BillCutFree.FirstOrDefault(w => w.Id == bID);
                ListImg = RECORD.Images.Split(',');
                //imgPath = "http://api.30shine.com" + ImgRootPath + ImageName;
                imgPath = "https://erp.30shine.com" + ImgRootPath + ImageName;
                //imgPath = "http://erp.30shine.net" + ImgRootPath + ImageName;
                if (RECORD != null)
                {
                    for (int i = 0; i < ListImg.Length; i++)
                    {
                        if (!(ListImg[i].StartsWith("http")))
                        {
                            ListImg[i] = _Domain + ListImg[i];
                        }
                    }

                    if (ListImg.Count() == 4)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                        obj.Img3 = ListImg[2];
                        obj.Img4 = ListImg[3];
                    }
                    if (ListImg.Count() == 3)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                        obj.Img3 = ListImg[2];
                    }
                    if (ListImg.Count() == 2)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                    }
                    if (ListImg.Count() == 1)
                    {
                        obj.Img1 = ListImg[0];
                    }

                    if (order == 1)
                    {
                        RECORD.ImageChecked1 = imgPath;
                    }
                    else if (order == 2)
                    {
                        RECORD.ImageChecked2 = imgPath;
                    }
                    else if (order == 3)
                    {
                        RECORD.ImageChecked3 = imgPath;
                    }
                    else if (order == 4)
                    {
                        RECORD.ImageChecked4 = imgPath;
                    }

                    if (RECORD.ImageChecked1 == null && obj.Img1 != null)
                    {
                        RECORD.ImageChecked1 = obj.Img1;
                    }
                    if (RECORD.ImageChecked2 == null && obj.Img2 != null)
                    {
                        RECORD.ImageChecked2 = obj.Img2;
                    }
                    if (RECORD.ImageChecked3 == null && obj.Img3 != null)
                    {
                        RECORD.ImageChecked3 = obj.Img3;
                    }
                    if (RECORD.ImageChecked4 == null && obj.Img4 != null)
                    {
                        RECORD.ImageChecked4 = obj.Img4;
                    }

                    RECORD.ModifiedTime = DateTime.Now;
                    db.Stylist4Men_BillCutFree.AddOrUpdate(RECORD);
                    error += db.SaveChanges() > 0 ? 0 : 1;
                    if (error == 0)
                    {
                        Message.success = true;
                        Message.message = "Cập nhật thành công.";
                    }
                    else
                    {
                        Message.success = false;
                        Message.message = "Cập nhật thất bại.";
                    }
                }
                else
                {
                    error = 1;
                    Message.success = false;
                    Message.message = "Bản ghi không tồn tại";
                }
                return serialize.Serialize(Message);
            }
            catch (Exception ex)
            {
                Message.success = false;
                Message.message = ex.Message;
                return serialize.Serialize(Message);
            }
        }
        #endregion

        #region Load lại ảnh đã check của salon
        [WebMethod]
        public static string RefreshCheckedImage(int bID, int order)
        {
            string imageOlder = "";
            BillServiceView obj = new BillServiceView();
            string[] ListImg;
            string _Domain = ConfigurationManager.AppSettings["DomainKey_PreviewImg"];
            var db = new Solution_30shineEntities();
            var RECORD = db.BillServices.FirstOrDefault(w => w.Id == bID);
            ListImg = RECORD.Images.Split(',');
            if (RECORD != null)
            {
                for (int i = 0; i < ListImg.Length; i++)
                {
                    if (!(ListImg[i].StartsWith("http")))
                    {
                        ListImg[i] = _Domain + ListImg[i];
                    }
                }
                if (ListImg.Count() == 4)
                {
                    obj.Img1 = ListImg[0];
                    obj.Img2 = ListImg[1];
                    obj.Img3 = ListImg[2];
                    obj.Img4 = ListImg[3];
                }
                if (ListImg.Count() == 3)
                {
                    obj.Img1 = ListImg[0];
                    obj.Img2 = ListImg[1];
                    obj.Img3 = ListImg[2];
                }
                if (ListImg.Count() == 2)
                {
                    obj.Img1 = ListImg[0];
                    obj.Img2 = ListImg[1];
                }
                if (ListImg.Count() == 1)
                {
                    obj.Img1 = ListImg[0];
                }
                if (order == 1 && (obj.Img1 != null || obj.Img1 != string.Empty))
                {
                    imageOlder = obj.Img1;
                }
                else if (order == 2 && (obj.Img2 != null || obj.Img2 != string.Empty))
                {
                    imageOlder = obj.Img2;
                }
                else if (order == 3 && (obj.Img3 != null || obj.Img3 != string.Empty))
                {
                    imageOlder = obj.Img3;
                }
                else if (order == 4 && (obj.Img4 != null || obj.Img4 != string.Empty))
                {
                    imageOlder = obj.Img4;
                }

            }
            return imageOlder;
        }
        #endregion

        #region Load lại ảnh đã check của S4M 
        [WebMethod]
        public static string RefreshCheckedImage_S4M(int bID, int order)
        {
            string imageOlder = "";
            S4M_Bill obj = new S4M_Bill();
            string[] ListImg;
            string _Domain = ConfigurationManager.AppSettings["DomainKey_PreviewImg"];
            var db = new Solution_30shineEntities();
            var RECORD = db.BillServices.FirstOrDefault(w => w.Id == bID);
            ListImg = RECORD.Images.Split(',');
            if (RECORD != null)
            {
                for (int i = 0; i < ListImg.Length; i++)
                {
                    if (!(ListImg[i].StartsWith("http")))
                    {
                        ListImg[i] = _Domain + ListImg[i];
                    }
                }
                if (ListImg.Count() == 4)
                {
                    obj.Img1 = ListImg[0];
                    obj.Img2 = ListImg[1];
                    obj.Img3 = ListImg[2];
                    obj.Img4 = ListImg[3];
                }
                if (ListImg.Count() == 3)
                {
                    obj.Img1 = ListImg[0];
                    obj.Img2 = ListImg[1];
                    obj.Img3 = ListImg[2];
                }
                if (ListImg.Count() == 2)
                {
                    obj.Img1 = ListImg[0];
                    obj.Img2 = ListImg[1];
                }
                if (ListImg.Count() == 1)
                {
                    obj.Img1 = ListImg[0];
                }
                if (order == 1 && (obj.Img1 != null || obj.Img1 != string.Empty))
                {
                    imageOlder = obj.Img1;
                }
                else if (order == 2 && (obj.Img2 != null || obj.Img2 != string.Empty))
                {
                    imageOlder = obj.Img2;
                }
                else if (order == 3 && (obj.Img3 != null || obj.Img3 != string.Empty))
                {
                    imageOlder = obj.Img3;
                }
                else if (order == 4 && (obj.Img4 != null || obj.Img4 != string.Empty))
                {
                    imageOlder = obj.Img4;
                }

            }
            return imageOlder;
        }
        #endregion

        [WebMethod]
        public static List<ErrorCutHair> GetErrorList()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.ErrorCutHairs.Where(a => a.IsDelete == false).ToList();
                return lst;
            }
        }

        [WebMethod]
        public static List<Api_HairMode> ReturnAllHairMode()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Api_HairMode.Where(a => a.IsDelete == false && a.Publish == true).ToList();
                return lst;
            }
        }

        [WebMethod]
        public static string updateCustomer_HairMode_Bill(int cusHairId, int? hairStyleId, int CustomerId, int billID)
        {
            var Message = new Library.Class.cls_message();
            var serialize = new JavaScriptSerializer();
            var error = 0;
            var db = new Solution_30shineEntities();
            var RECORD = db.Customer_HairMode_Bill.FirstOrDefault(w => w.Id == cusHairId);
            var obj = new Customer_HairMode_Bill();
            if (RECORD == null)
            {
                obj.BillId = billID;
                obj.CreateDate = DateTime.Now;
                obj.HairStyleId = hairStyleId;
                //obj.FaceTypeId = faceTypeId;
                obj.CustomerId = CustomerId;
                //if(_salonId > 0)
                //{
                //    obj.IsHoiQuan = false;
                //}
                //else if (_classId > 0)
                //{
                //    obj.IsHoiQuan = true;
                //}
                db.Customer_HairMode_Bill.Add(obj);
                error += db.SaveChanges() > 0 ? 0 : 1;
                if (error == 0)
                {
                    Message.success = true;
                    Message.message = "Thêm thành công.";
                }
                else
                {
                    Message.success = false;
                    Message.message = "Thêm thất bại.";
                }
            }
            else
            {
                RECORD.BillId = billID;
                if (hairStyleId != null)
                {
                    RECORD.HairStyleId = hairStyleId;
                }
                //if(faceTypeId != null)
                //{
                //    RECORD.FaceTypeId = faceTypeId;
                //}

                RECORD.CustomerId = CustomerId;
                RECORD.ModifiledDate = DateTime.Now;
                db.Customer_HairMode_Bill.AddOrUpdate(RECORD);
                error += db.SaveChanges() > 0 ? 0 : 1;
                if (error == 0)
                {
                    Message.success = true;
                    Message.message = "Cập nhật thành công.";
                }
                else
                {
                    Message.success = false;
                    Message.message = "Cập nhật thất bại.";
                }
            }
            return serialize.Serialize(Message);
            //}
            //catch (Exception ex)
            //{
            //    Message.success = false;
            //    Message.message = ex.Message;
            //    return serialize.Serialize(Message);
            //}
        }

        [WebMethod]
        public static Customer_HairMode_Bill getHairStyleById(int billId)
        {
            using (var db = new Solution_30shineEntities())
            {
                try
                {
                    var lst = new Customer_HairMode_Bill();
                    lst = db.Customer_HairMode_Bill.Where(a => a.BillId == billId /*&& (a.IsHoiQuan == false || a.IsHoiQuan == null)*/ && (a.IsDelete == false || a.IsDelete == null)).SingleOrDefault();

                    return lst;
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
        }

        [WebMethod]
        public static string updateErrorNote(int billID, string errorNote, string noteByStylist)
        {
            var Message = new Library.Class.cls_message();
            var serialize = new JavaScriptSerializer();
            var error = 0;
            try
            {
                var db = new Solution_30shineEntities();
                var RECORD = db.BillServices.FirstOrDefault(w => w.Id == billID);
                if (RECORD != null)
                {
                    RECORD.ErrorNote = errorNote;
                    //RECORD.NoteByStylist = noteByStylist;
                    RECORD.ModifiedDate = DateTime.Now;
                    db.BillServices.AddOrUpdate(RECORD);
                    error += db.SaveChanges() > 0 ? 0 : 1;
                    if (error == 0)
                    {
                        Message.success = true;
                        Message.message = "Cập nhật thành công.";
                    }
                    else
                    {
                        Message.success = false;
                        Message.message = "Cập nhật thất bại.";
                    }
                }
                else
                {
                    error = 1;
                    Message.success = false;
                    Message.message = "Bản ghi không tồn tại";
                }
                //return serialize.Serialize(Message);
                return serialize.Serialize(Message);

            }
            catch (Exception ex)
            {
                Message.success = false;
                Message.message = ex.Message;
                return serialize.Serialize(Message);
            }
        }

        /// <summary>
        /// InsertAndUpdateSCSC_CheckError
        /// </summary>
        /// <param name="ID_SCSC_Check"></param>
        /// <param name="Bill_ID"></param>
        /// <param name="ImageError"></param>
        /// <param name="NoteError"></param>
        /// <param name="_TotalPoint_SCSC"></param>
        /// <param name="SHAPE_ID"></param>
        /// <param name="CONNECTIVE_ID"></param>
        /// <param name="SHARPNESS_ID"></param>
        /// <param name="COMPLETION_ID"></param>
        /// <returns></returns>
        [WebMethod]
        public static object InsertAndUpdateSCSC_CheckError(int ID_SCSC_Check, int Bill_ID, int ImageError, string NoteError, int _TotalPoint_SCSC, int SHAPE_ID, int CONNECTIVE_ID, int SHARPNESS_ID, int COMPLETION_ID, int SCSC_PointError, int MaxPointShap_ID, int MaxPointConnective_ID, int MaxPointSharpNess_ID, int MaxPointCompletion_ID)
        {
            using (var db = new Solution_30shineEntities())
            {
                var Msg = new Library.Class.cls_message();
                var objSCSC_Check = new SCSC_CheckError();
                if (Bill_ID != 0)
                {
                    objSCSC_Check = db.SCSC_CheckError.FirstOrDefault(a => a.ID == ID_SCSC_Check && a.BillService_ID == Bill_ID && a.Publish == true && a.IsDelete == false);
                    if (objSCSC_Check == null)
                    {
                        objSCSC_Check = new SCSC_CheckError();
                        objSCSC_Check.BillService_ID = Bill_ID;
                        objSCSC_Check.CreateDate = DateTime.Now;
                        objSCSC_Check.IsDelete = false;
                        objSCSC_Check.Publish = true;
                    }
                    else
                    {
                        objSCSC_Check.ModifiledDate = DateTime.Now;
                    }

                    objSCSC_Check.NoteError = NoteError;

                    if (ImageError == 1)
                    {
                        objSCSC_Check.ImageError = true;
                    }
                    else
                    {
                        objSCSC_Check.ImageError = false;
                    }
                    objSCSC_Check.MaxPoint_Shap_ID = MaxPointShap_ID;
                    objSCSC_Check.MaxPoint_Connective_ID = MaxPointConnective_ID;
                    objSCSC_Check.MaxPoint_SharpNess_ID = MaxPointSharpNess_ID;
                    objSCSC_Check.MaxPoint_ComPlatetion_ID = MaxPointCompletion_ID;
                    objSCSC_Check.PointError = SCSC_PointError;
                    objSCSC_Check.TotalPointSCSC = _TotalPoint_SCSC;
                    objSCSC_Check.Shape_ID = SHAPE_ID;
                    objSCSC_Check.ConnectTive_ID = CONNECTIVE_ID;
                    objSCSC_Check.SharpNess_ID = SHARPNESS_ID;
                    objSCSC_Check.ComPlatetion_ID = COMPLETION_ID;
                    db.SCSC_CheckError.AddOrUpdate(objSCSC_Check);
                    db.SaveChanges();
                    Msg.data = objSCSC_Check;
                    Msg.status = "success";
                    Msg.success = true;
                }
                else
                {
                    Msg.status = "Not Bill";
                    Msg.success = false;
                }

                return Msg;
            }
        }

        [WebMethod]
        public static object BillDataWhereBillID(int Bill_ID)
        {
            using (var db = new Solution_30shineEntities())
            {
                var msg = new Msg();
                var obj = db.SCSC_CheckError.FirstOrDefault(a => a.BillService_ID == Bill_ID && a.IsDelete == false && a.Publish == true);
                if (obj != null)
                {
                    msg.data = obj;
                    msg.msg = "success";

                }
                else
                {
                    msg.msg = "Fail";
                }
                return msg;
            }

        }

        [WebMethod]
        public static object BindFlowserService(int Bill_ID)
        {
            using (var db = new Solution_30shineEntities())
            {
                var msg = new Msg();
                var lstService = (from a in db.FlowServices
                                  join b in db.Services on a.ServiceId equals b.Id
                                  where a.IsDelete == 0 && a.BillId == Bill_ID
                                  select new { b.Name }).ToList();
                //var lst = db.FlowServices.Where(a => a.BillId == Bill_ID).ToList();
                msg.data = lstService;
                return msg;
            }
        }


        public class S4M_Bill
        {
            public int Id { get; set; }
            public string BillDate { get; set; }
            public string ClassName { get; set; }
            public string CustomerPhone { get; set; }
            public string CustomerName { get; set; }
            public string Img1 { get; set; }
            public string Img2 { get; set; }
            public string Img3 { get; set; }
            public string Img4 { get; set; }
            public string ImageStatusId { get; set; }
            public string ErrorNote { get; set; }
            public string NoteByStylist { get; set; }
            public string Student { get; set; }
            public int? CustomerId { get; set; }
        }
        public class S4M_custom_BillService : Stylist4Men_BillCutFree
        {
            public string CustomerPhone { get; set; }
            public string CustomerName { get; set; }
        }
        public class custom_BillService : BillService
        {
            public string CustomerPhone { get; set; }
            public string CustomerName { get; set; }
            public int? IdCustomer { get; set; }
            public int? ID_SCSC { get; set; }
            public bool? ImageError { get; set; }
            public string NoteError { get; set; }
            public int? Shape_ID { get; set; }
            public int? SharpNess_ID { get; set; }
            public int? ComPlatetion_ID { get; set; }
            public int? ConnectTive_ID { get; set; }
            public int? TotalPointSCSC { get; set; }
            public int? PointSCSC { get; set; }
        }
        public class cls_Note
        {
            public string NoteByManager { get; set; }
            public string NoteByStylist { get; set; }
        }
    }

}