﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.FrontEnd.PreviewImage
{
    public partial class StatisticScscStaffV3 : System.Web.UI.Page
    {
        private bool Perm_ViewAllData = false;

        public bool Perm_Delete { get; private set; }
        public bool Perm_ShowSalon { get; private set; }
        public bool Perm_Access { get; private set; }
        public bool Perm_Edit { get; private set; }

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var msgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", msgParam);
            }
            else
            {

                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();

            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList>() { ddlSalon }, Perm_ViewAllData);
            }
        }
        protected void Salon_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var salon = Convert.ToInt32(ddlSalon.SelectedValue);
                var staff = new List<Staff>();
                var whereSalon = "";
                var sql = "";

                if (salon > 0)
                {
                    whereSalon = " and SalonId = " + salon;
                }
                sql = @"select * from Staff 
                        where IsDelete != 1 and Active = 1  and Type = 1
                        and (isAccountLogin != 1 or isAccountLogin is null)" + whereSalon;
                staff = db.Staffs.SqlQuery(sql).ToList();

                var first = new Staff();
                first.Id = 0;
                first.Fullname = "Chọn nhân viên";
                staff.Insert(0, first);

                ddlStylist.DataTextField = "Fullname";
                ddlStylist.DataValueField = "Id";
                ddlStylist.DataSource = staff;
                ddlStylist.DataBind();
            }
        }
        private void BindOnlySalon()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    if (TxtDateTimeFrom.Text != "")
                    {
                        DateTime fromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, new CultureInfo("vi-VN"));
                        DateTime toDate;
                        if (TxtDateTimeTo.Text != "")
                        {
                            toDate = Convert.ToDateTime(TxtDateTimeTo.Text, new CultureInfo("vi-VN"));
                        }
                        else
                        {
                            toDate = fromDate;
                        }
                        int salonId = Convert.ToInt32(ddlSalon.SelectedValue);
                        int stylistId = Convert.ToInt32(ddlStylist.SelectedValue);
                        //var list = db.SCSC_BindStaffWhereNhanVien_Store_V2(_FromDate, _ToDate, SalonId, StylistId).ToList();
                        //var item = new SCSC_BindStaffWhereNhanVien_Store_V2_Result();
                        var list = GetDataOnlyStaff(stylistId, salonId, fromDate, toDate);
                        var item = new cls_SCSCUonOnlyStaff();
                        var countList = list.Count;
                        item.FullName = "Tổng";
                        item.TotalBill = list.Sum(a => a.TotalBill);
                        item.Bill_NotImg = list.Sum(a => a.TotalBill ?? 0) != 0
                            ? Math.Round(
                                ((Double)list.Sum(a => a.TotalBillNotImg ?? 0) /
                                 (Double)list.Sum(a => a.TotalBill ?? 0) * 100), 2)
                            : 0;
                        item.TotalBill_ChuaDanhGia = list.Sum(a => a.TotalBill_ChuaDanhGia);
                        item.Percent_BillError = Math.Round(
                            ((Double)list.Sum(r => r.TotalErrorSCSC ?? 0) -
                             (Double)list.Sum(r => r.TongBillDanhGiaAnhMo ?? 0)) /
                            (Double)list.Sum(r => r.TotalBill ?? 0) *
                            100, 2);

                        item.Percent_BillImg_MoLech = list.Sum(a => a.TotalBill ?? 0) != 0
                            ? Math.Round(
                                (Double)list.Sum(r => r.TongBillDanhGiaAnhMo ?? 0) /
                                (Double)list.Sum(r => r.TotalBill ?? 0) * 100, 2)
                            : 0;
                        item.PointSCSC_TB =
                            (list.Sum(r => r.TotalBillDanhGiaNotImageMoLech ?? 0) +
                             list.Sum(r => r.TongBillDanhGiaAnhMo ?? 0)) == 0
                                ? 0
                                : Math.Round(
                                    (Double)list.Sum(r => r.TotalPointSCSC ?? 0) /
                                    ((Double)list.Sum(r => r.TotalBillDanhGiaNotImageMoLech ?? 0) +
                                     (Double)list.Sum(r => r.TongBillDanhGiaAnhMo ?? 0)), 2);
                        item.ErrorShapeId = list.Sum(a => a.ErrorShapeId);
                        item.ErrorSharpNessId = list.Sum(a => a.ErrorSharpNessId);
                        item.ErrorConnectiveId = list.Sum(a => a.ErrorConnectiveId);
                        item.ErrorComplatetionId = list.Sum(a => a.ErrorComplatetionId);
                        item.Percent_ErrorKCS = list.Sum(a => a.TotalBill ?? 0) != 0
                            ? Math.Round(
                                (Double)list.Sum(a => a.TotalErrorSCSCUon ?? 0) /
                                (Double)list.Sum(r => r.TotalBill ?? 0) * 100, 2)
                            : 0;
                        item.BillNotImgUon = list.Sum(a => a.TotalBillUon) != 0
                            ? Math.Round(
                                (Double)list.Sum(a => a.TongBillNotAnhUon ?? 0) /
                                (Double)list.Sum(r => r.TotalBillUon) * 100, 2)
                            : 0;
                        item.BillNotImgUonMoLech = list.Sum(a => a.TotalBillUon) != 0
                            ? Math.Round(
                                (Double)list.Sum(a => a.TongBillAnhMoLech ?? 0) /
                                (Double)list.Sum(r => r.TotalBillUon) * 100, 2)
                            : 0;
                        item.BillUonChamLoi =
                            (list.Sum(r => r.TongBillNotAnhUon ?? 0) +
                             list.Sum(r => r.TongBillAnhUon ?? 0)) == 0
                                ? 0
                                : Math.Round(
                                    (Double)list.Sum(r => r.TongBillUonChamLoi ?? 0) /
                                    ((Double)list.Sum(r => r.TongBillNotAnhUon ?? 0) +
                                     (Double)list.Sum(r => r.TongBillAnhUon ?? 0)) * 100, 2);

                        item.TotalDiemUonTB = list.Sum(a => a.TongBillAnhUon ?? 0) == 0
                            ? 0
                            : Math.Round(
                                (Double)list.Sum(a => a.TotalPointSCSCCurling ?? 0) /
                                (Double)list.Sum(r => r.TongBillAnhUon ?? 0), 2);

                        item.TotalBillUon = list.Sum(a => a.TotalBillUon);

                        item.Percent_ErrorKCSUon = list.Sum(a => a.TotalBillUon) != 0
                            ? Math.Round(
                                ((Double)list.Sum(r => r.TongBillUonChamLoi ?? 0) -
                                (Double)list.Sum(r => r.TongBillAnhMoLech ?? 0)) / (Double)list.Sum(r => r.TotalBillUon) *
                                100, 2)
                            : 0;
                        item.ErrorHairTip = list.Sum(a => a.ErrorHairTip);
                        item.ErrorHairRoots = list.Sum(a => a.ErrorHairRoots);
                        item.ErrorHairWaves = list.Sum(a => a.ErrorHairWaves);
                        list.Insert(0, item);
                        rptOnlySalon.DataSource = list;
                        rptOnlySalon.DataBind();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Message System", "ShowMessage('','BẠN CHƯA CHỌN NGÀY',4,7000);", true);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Bind data all salon
        /// </summary>
        private void BindAllSalon()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    if (TxtDateTimeFrom.Text != "")
                    {
                        DateTime _FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, new CultureInfo("vi-VN"));
                        DateTime _ToDate;
                        if (TxtDateTimeTo.Text != "")
                        {
                            _ToDate = Convert.ToDateTime(TxtDateTimeTo.Text, new CultureInfo("vi-VN"));
                        }
                        else
                        {
                            _ToDate = _FromDate;
                        }
                        //var list = db.SCSC_BindStaffWhere_AllNhanVien_Store_V2(_FromDate, _ToDate).ToList();
                        //var item = new SCSC_BindStaffWhere_AllNhanVien_Store_V2_Result();
                        var list = GetDataAllSalon(_FromDate, _ToDate);
                        var item = new cls_SCSCUonAllSalon();
                        var countList = list.Count;
                        item.ShortName = "Tổng";
                        item.TotalBill = list.Sum(a => a.TotalBill);
                        item.Percent_Bill_NotImg = list.Sum(a => a.TotalBill ?? 0) != 0
                            ? Math.Round(
                                ((Double)list.Sum(a => a.TotalBillNotImage ?? 0) /
                                 (Double)list.Sum(a => a.TotalBill ?? 0) * 100), 2)
                            : 0;
                        item.Percent_BillImg_MoLech = list.Sum(a => a.TotalBill ?? 0) != 0
                            ? Math.Round(
                                (Double)list.Sum(r => r.TotalBill_DanhGiaImgMoLech ?? 0) /
                                (Double)list.Sum(r => r.TotalBill ?? 0) * 100, 2)
                            : 0;
                        item.ErrorShapeId = list.Sum(a => a.ErrorShapeId);
                        item.ErrorSharpNess = list.Sum(a => a.ErrorSharpNess);
                        item.ErrorConnective = list.Sum(a => a.ErrorConnective);
                        item.ErrorComplatetion = list.Sum(a => a.ErrorComplatetion);
                        item.BillChuaDanhGia = list.Sum(a => a.BillChuaDanhGia);
                        item.Percent_BillError = Math.Round(
                            ((Double) list.Sum(r => r.TotalErrorSCSC ?? 0) -
                             (Double) list.Sum(r => r.TotalBill_DanhGiaImgMoLech ?? 0)) /
                            (Double) list.Sum(r => r.TotalBill ?? 0) *
                            100, 2);

                        item.Percent_ErrorKCS = list.Sum(a => a.TotalBill ?? 0) != 0
                            ? Math.Round(
                                (Double)list.Sum(a => a.TotalErrorSCSCUon ?? 0) /
                                (Double)list.Sum(r => r.TotalBill ?? 0) * 100, 2)
                            : 0;
                        item.PointSCSC_TB = (list.Sum(r => r.TotalBill_DanhGiaNotMolech ?? 0) +
                                             list.Sum(r => r.TotalBill_DanhGiaImgMoLech ?? 0)) == 0
                            ? 0
                            : Math.Round(
                                (Double)list.Sum(r => r.TotalSCSC ?? 0) /
                                ((Double)list.Sum(r => r.TotalBill_DanhGiaNotMolech ?? 0) +
                                 (Double)list.Sum(r => r.TotalBill_DanhGiaImgMoLech ?? 0)), 2);

                        item.BillNotImgUon = list.Sum(a => a.TotalBillUon) != 0
                            ? Math.Round(
                                (Double)list.Sum(a => a.TongBillNotAnhUon ?? 0) /
                                (Double)list.Sum(r => r.TotalBillUon) * 100, 2)
                            : 0;
                        item.BillNotImgUonMoLech = list.Sum(a => a.TotalBillUon) != 0
                            ? Math.Round(
                                (Double)list.Sum(a => a.TongBillAnhMoLech ?? 0) /
                                (Double)list.Sum(r => r.TotalBillUon) * 100, 2)
                            : 0;
                        item.BillUonChamLoi = (list.Sum(r => r.TongBillNotAnhUon ?? 0) +
                                               list.Sum(r => r.TongBillAnhUon ?? 0)) == 0
                            ? 0
                            : Math.Round(
                                (Double)list.Sum(r => r.TongBillUonChamLoi ?? 0) /
                                ((Double)list.Sum(r => r.TongBillNotAnhUon ?? 0) +
                                 (Double)list.Sum(r => r.TongBillAnhUon ?? 0)) * 100, 2);
                       
                        item.TotalDiemUonTB = list.Sum(a => a.TongBillAnhUon ?? 0) == 0
                            ? 0
                            : Math.Round(
                                (Double)list.Sum(a => a.TotalPointSCSCCurling ?? 0) /
                                (Double)list.Sum(r => r.TongBillAnhUon ?? 0), 2);
                        item.TotalBillUon = list.Sum(a => a.TotalBillUon);
                        item.Percent_ErrorKCSUon = list.Sum(a => a.TotalBillUon) != 0
                            ? Math.Round(
                                ((Double)list.Sum(r => r.TongBillUonChamLoi ?? 0) -
                                 (Double)list.Sum(r => r.TongBillAnhMoLech ?? 0)) / (Double)list.Sum(r => r.TotalBillUon) *
                                100, 2)
                            : 0;
                        item.ErrorHairTip = list.Sum(a => a.ErrorHairTip);
                        item.ErrorHairRoots = list.Sum(a => a.ErrorHairRoots);
                        item.ErrorHairWaves = list.Sum(a => a.ErrorHairWaves);
                        list.Insert(0, item);
                        rptAllSalon.DataSource = list;
                        rptAllSalon.DataBind();
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Message System", "ShowMessage('','BẠN CHƯA CHỌN NGÀY',4,7000);", true);
                    }
                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            try
            {
                if (CheckLimitTime())
                {
                    int SalonId = Convert.ToInt32(ddlSalon.SelectedValue);
                    if (SalonId == 0)
                    {
                        BindAllSalon();
                        rptOnlySalon.DataSource = "";
                        rptOnlySalon.DataBind();
                    }
                    else
                    {
                        BindOnlySalon();
                        rptAllSalon.DataSource = "";
                        rptAllSalon.DataBind();
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Message System", "ShowMessage('','LƯU Ý: BẠN CHỈ ĐƯỢC XEM TỪ 21H - 8h30 HÔM SAU',4,7000);", true);
                }
            }
            catch (Exception ex)
            {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Message System", "ShowMessage('','Đã xảy ra lỗi: " + ex.Message.Replace("'", "") + "',4,7000);", true);
            }
            RemoveLoading();
        }

        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "finishLoading();ConfigTableListing();", true);
        }

        private List<cls_SCSCUonOnlyStaff> GetDataOnlyStaff(int stylistId, int salonId, DateTime timeFrom, DateTime timeTo)
        {
            try
            {
                var list = new List<cls_SCSCUonOnlyStaff>();
                var domain = Libraries.AppConstants.URL_API_REPORTS;
                var api = @"/api/scsc-statistic/staff-only-staff?stylistId=" + stylistId  + "&salonId=" + salonId + "&fromDate=" + string.Format("{0:yyyy/MM/dd}",timeFrom) + "&toDate=" + string.Format("{0:yyyy/MM/dd}",timeTo);
                var response = new HttpClient().GetAsync(domain + api).Result;
                if (response.IsSuccessStatusCode)
                {
                    list = response.Content.ReadAsAsync<List<cls_SCSCUonOnlyStaff>>().Result;
                }
                else
                {
                    throw new Exception(response.Content.ReadAsStringAsync().Result);
                }
                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private List<cls_SCSCUonAllSalon> GetDataAllSalon(DateTime timeFrom, DateTime timeTo)
        {
            try
            {
                var list = new List<cls_SCSCUonAllSalon>();
                var domain = Libraries.AppConstants.URL_API_REPORTS;
                var api = @"/api/scsc-statistic/staff-all-salon?fromDate=" + string.Format("{0:yyyy/MM/dd}",timeFrom) + "&toDate=" + string.Format("{0:yyyy/MM/dd}",timeTo);
                var response = new HttpClient().GetAsync(domain + api).Result;
                if (response.IsSuccessStatusCode)
                {
                    list = response.Content.ReadAsAsync<List<cls_SCSCUonAllSalon>>().Result;
                }
                else
                {
                    throw new Exception(response.Content.ReadAsStringAsync().Result);
                }
                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool CheckLimitTime()
        {
            try
            {
                var timeNow = DateTime.Now.TimeOfDay;
                var timeTo = new TimeSpan(11, 00, 00);
                var timeFrom = new TimeSpan(21, 00, 00);

                string perName = Session["User_Permission"].ToString();
                string[] arrayPermision = perName.Split(',');
                if (arrayPermision.Length == 0)
                {
                    arrayPermision = new[] { perName };
                }
                string checkAdmin = Array.Find(arrayPermision, s => s.Equals("ADMIN"));
                string checkRoot = Array.Find(arrayPermision, s => s.Equals("root"));
                if (timeNow >= timeFrom || timeNow <= timeTo || checkAdmin != null || checkRoot != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public class cls_SCSCUonOnlyStaff
        {
            public int TotalBillUon { get; set; }
            public string FullName { get; set; }
            public int? Stylist_ID { get; set; }
            public int? TotalBill { get; set; }
            public double? Bill_NotImg { get; set; }
            public double? Percent_BillError { get; set; }
            public double? Percent_BillImg_MoLech { get; set; }
            public int? TotalBill_ChuaDanhGia { get; set; }
            public double? Percent_ErrorKCS { get; set; }
            public double? PointSCSC_TB { get; set; }
            public int? Stylist_ID2 { get; set; }
            public int ErrorShapeId { get; set; }
            public int ErrorConnectiveId { get; set; }
            public int ErrorSharpNessId { get; set; }
            public int ErrorComplatetionId { get; set; }
            public double? BillNotImgUon { get; set; }
            public double? BillNotImgUonMoLech { get; set; }
            public double BillUonChamLoi { get; set; }
            public double? TotalDiemUonTB { get; set; }
            public double? Percent_ErrorKCSUon { get; set; }
            public int ErrorHairTip { get; set; }
            public int ErrorHairRoots { get; set; }
            public int ErrorHairWaves { get; set; }

            public int? TotalBillNotImg { get; set; }
            public int? TotalErrorSCSC { get; set; }
            public int? TongBillDanhGiaAnhMo { get; set; }
            public int? TotalBillDanhGiaNotImageMoLech { get; set; }
            public int? TotalPointSCSC { get; set; }
            public int? TongBillNotAnhUon { get; set; }
            public int? TongBillAnhUon { get; set; }
            public int? TongBillAnhMoLech { get; set; }
            public int? TongBillUonChamLoi { get; set; }
            public int? TotalPointSCSCCurling { get; set; }
            public int? TotalMoLechSCSCUon { get; set; }
            public int? TotalErrorSCSCUon { get; set; }

        }
        public class cls_SCSCUonAllSalon
        {
            public int TotalBillUon { get; set; }
            public string ShortName { get; set; }
            public int? SalonId { get; set; }
            public int? TotalBill { get; set; }
            public double? Percent_Bill_NotImg { get; set; }
            public int? BillChuaDanhGia { get; set; }
            public double? Percent_BillError { get; set; }
            public double? Percent_BillImg_MoLech { get; set; }
            public double? PointSCSC_TB { get; set; }
            public double? Percent_ErrorKCS { get; set; }
            public int ErrorShapeId { get; set; }
            public int ErrorConnective { get; set; }
            public int ErrorSharpNess { get; set; }
            public int ErrorComplatetion { get; set; }
            public double? BillNotImgUon { get; set; }
            public double? BillNotImgUonMoLech { get; set; }
            public double BillUonChamLoi { get; set; }
            public double? TotalDiemUonTB { get; set; }
            public double? Percent_ErrorKCSUon { get; set; }
            public int ErrorHairTip { get; set; }
            public int ErrorHairRoots { get; set; }
            public int ErrorHairWaves { get; set; }

            public int? TotalBillNotImage { get; set; }
            public int? TotalBill_DanhGiaNotMolech { get; set; }
            public int? TotalBill_DanhGiaImgMoLech { get; set; }
            public int? TotalErrorSCSC { get; set; }
            public int? TotalSCSC { get; set; }
            public int? TongBillNotAnhUon { get; set; }
            public int? TongBillAnhUon { get; set; }
            public int? TongBillAnhMoLech { get; set; }
            public int? TongBillUonChamLoi { get; set; }
            public int? TotalPointSCSCCurling { get; set; }
            public int? TotalMoLechSCSCUon { get; set; }
            public int? TotalErrorSCSCUon { get; set; }

        }
    }
}