﻿<%@ Page Language="C#" MasterPageFile="~/TemplateMaster/SiteMasterVersion1.Master" AutoEventWireup="true" CodeBehind="StatisticScscStaffV3.aspx.cs" Inherits="_30shine.GUI.FrontEnd.PreviewImage.StatisticScscStaffV3" %>

<asp:Content ID="CustomerAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel ID="ContentWrap" runat="server" ClientIDMode="Static">
        <div class="container-fluid sub-menu form-group">
            <div class="row">
                <div class="col-md-12 pt-3 pb-2">
                    <ul>
                        <li>Báo cáo scsc theo nhân viên</li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <div class="sub-menu-boder"></div>
                </div>
            </div>
        </div>
        <form runat="server">
            <div class="container-fluid">
                <div class="row">
                    <div class="form-group col-auto">
                        <strong><i class="fa fa-filter"></i>Lọc kết quả &nbsp;&#187; </strong>
                    </div>
                    <div class="col-auto form-group">
                        <asp:TextBox CssClass="datetime-picker form-control float-left w-auto" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                        <i class="fa fa-arrow-circle-right float-left mt-2 mr-2 ml-2" style="color: #D0D6D8;"></i>
                        <asp:TextBox CssClass="datetime-picker form-control float-left w-auto" ID="TxtDateTimeTo" placeholder="Đến ngày"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group col-auto">
                        <asp:DropDownList ID="ddlSalon" data-name="salonId" ClientIDMode="Static" CssClass="select form-control" runat="server" OnSelectedIndexChanged="Salon_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>
                    <div class="form-group col-auto">
                        <asp:DropDownList ID="ddlStylist" data-name="stylistId" ClientIDMode="Static" CssClass="select form-control" runat="server" ></asp:DropDownList>
                    </div>
                    <div class="form-group col-auto">
                        <asp:Panel ID="ViewData" CssClass="form-group btn-viewdata text-center" Text="Xem dữ liệu"
                            ClientIDMode="Static" onclick="excPaging(1)" runat="server">
                            Xem dữ liệu
                        </asp:Panel>
                    </div>
                    <div class="col-md-12 col-xl-12 form-group">
                        <div class="sub-menu-boder-low"></div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <asp:UpdatePanel ID="UPTotal" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="row">
                            <div class="col-12">
                                <strong class="st-head"><i class="fas fa-file-alt mr-1"></i>Thống kê SCSC</strong>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <table class="table table-bordered selectable" id="tblAllSalon">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Tổng bill</th>
                                            <th>% Bill không ảnh</th>
                                            <th>% Bill mờ lệch</th>
                                            <th>Lỗi Shape</th>
                                            <th>Lỗi Connective</th>
                                            <th>Lỗi SharpNess</th>
                                            <th>Lỗi Completion</th>
                                            <th>Tổng lỗi SCSC</th>
                                            <th>Điểm SCSC TB</th>
                                            <th>Tổng bill uốn</th>
                                            <th>% Bill không ảnh Uốn</th>
                                            <th>% Bill mờ lệch Uốn</th>
                                            <th>Lỗi Ngọn tóc</th>
                                            <th>Lỗi Chân tóc</th>
                                            <th>Lỗi Sóng tóc</th>
                                            <th>Tổng lỗi uốn</th>
                                            <th>Điểm TB Uốn</th>
                                            <th>Bill chưa đánh giá</th>
                                            <th>% Lỗi KCS</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="rptAllSalon" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td><b><%# Eval("ShortName") %></b></td>
                                                    <td class="td-SoLanOK"><%#Eval("TotalBill") %> </td>
                                                    <td class="td-HinhKhoiChuaChuan"><%#Eval("Percent_Bill_NotImg") %>%</td>
                                                    <td><%# Eval("Percent_BillImg_MoLech") %>%</td>
                                                    <td class="td-HinhKhoiChuaLienKet"><%#Eval("ErrorShapeId") %></td>
                                                    <td class="td-NgonTocQuaDay"><%#Eval("ErrorSharpNess") %></td>
                                                    <td class="td-CaoMaiGayLoi"><%#Eval("ErrorConnective") %></td>
                                                    <td class="td-VuotSapLoi"><%#Eval("ErrorComplatetion") %></td>
                                                    <td><%# Eval("Percent_BillError") %>%</td>
                                                    <td><%# Eval("PointSCSC_TB") %></td>
                                                    <td><%# Eval("TotalBillUon") %></td>
                                                    <td><%# Eval("BillNotImgUon") %>%</td>
                                                    <td><%# Eval("BillNotImgUonMoLech") %>%</td>
                                                    <td><%# Eval("ErrorHairTip") %></td>
                                                    <td><%# Eval("ErrorHairRoots") %></td>
                                                    <td><%# Eval("ErrorHairWaves") %></td>
                                                    <td><%# Eval("Percent_ErrorKCSUon") %>%</td>
                                                    <td><%# Eval("TotalDiemUonTB") %></td>
                                                    <td class="td-DuongCatChuaNet"><%#Eval("BillChuaDanhGia") %></td>
                                                    <td><%# Eval("Percent_ErrorKCS") %> %</td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <asp:Repeater ID="rptOnlySalon" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <b><%# Eval("Fullname") %></b></td>
                                                    <td class="td-SoLanOK"><%#Eval("TotalBill") %> </td>
                                                    <td class="td-HinhKhoiChuaChuan"><%#Eval("Bill_NotImg") %>%</td>
                                                    <td><%# Eval("Percent_BillImg_MoLech") %>%</td>
                                                    <td class="td-HinhKhoiChuaLienKet"><%#Eval("ErrorShapeId") %></td>
                                                    <td class="td-NgonTocQuaDay"><%#Eval("ErrorConnectiveId") %></td>
                                                    <td class="td-CaoMaiGayLoi"><%#Eval("ErrorSharpNessId") %></td>
                                                    <td class="td-VuotSapLoi"><%#Eval("ErrorComplatetionId") %></td>
                                                    <td><%#Eval("Percent_BillError") %>%</td>
                                                    <td><%# Eval("PointSCSC_TB") %></td>
                                                    <td><%# Eval("TotalBillUon") %></td>
                                                    <td><%# Eval("BillNotImgUon") %>%</td>
                                                    <td><%# Eval("BillNotImgUonMoLech") %>% </td>
                                                    <td><%# Eval("ErrorHairTip") %></td>
                                                    <td><%# Eval("ErrorHairRoots") %></td>
                                                    <td><%# Eval("ErrorHairWaves") %></td>
                                                    <td><%# Eval("Percent_ErrorKCSUon") %></td>
                                                    <td><%# Eval("TotalDiemUonTB") %></td>
                                                    <td class="td-DuongCatChuaNet"><%#Eval("TotalBill_ChuaDanhGia") %></td>
                                                    <td><%# Eval("Percent_ErrorKCS") %>%</td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-xs-6">
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
            </div>
        </form>
        <style>
            .form-control {
                height: 31px !important;
                width: 150px !important;
            }

            .search-content {
                height: 31px;
            }

            /*table thead {
                background: rgb(221, 221, 221);
            }

            table.table-listing thead tr th, table.table-listing tbody tr td {
                border: 1px solid black !important;
            }*/
        </style>
        <script>
            var table = null;
            function excPaging(page) {
                $("#HDF_Page").val(page);
                $("#BtnFakeUP").click();
                startLoading();
            }
            // ConfigTableListing
            function ConfigTableListing() {
                table = $('#tblAllSalon').DataTable(
                    {
                        paging: false,
                        searching: false,
                        info: false,
                        fixedHeader: true,
                        ordering: false,
                        fixedColumns: {
                            leftColumns: 1
                        },
                        scrollY: "80vh",
                        scrollX: true,
                        scrollCollapse: true,
                    });
            }
        </script>
    </asp:Panel>
</asp:Content>
