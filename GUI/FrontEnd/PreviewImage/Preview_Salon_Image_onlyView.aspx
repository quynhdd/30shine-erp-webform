﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Preview_Salon_Image_onlyView.aspx.cs" Inherits="_30shine.GUI.FrontEnd.PreviewImage.Preview_Salon_Image_onlyView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <title>Xem ảnh theo Salon</title>
    <script src="/Assets/js/jquery.v1.11.1.js"></script>
    <link href="../../../Assets/css/bootstrap/bootstrap.css" rel="stylesheet" />
    <link href="/Assets/css/font.css" rel="stylesheet" />
    <link href="/Assets/css/font-awesome.css" rel="stylesheet" />
    <link href="/Assets/css/csvc.css" rel="stylesheet" />

    <script src="../../../Assets/js/bootstrap/bootstrap.js"></script>
    <script src="/Assets/libs/canvasjs-1.9.8/canvasjs.min.js"></script>
    <script src="/Assets/libs/canvasjs-1.9.8/jquery.canvasjs.min.js"></script>
    <script src="/Assets/js/bootstrap/bootstrap-modal.js"></script>
    <%--<script src="/Assets/js/bootstrap/bootstrap-carousel.js"></script>--%>
    <link href="/Assets/css/jquery.datetimepicker.css?v=2" rel="stylesheet" />
    <script src="/Assets/js/jquery.datetimepicker.js"></script>
    <script src="../../../Assets/js/common.js"></script>
    <style>
        /*.modal.and.carousel {
          position: absolute; 
        }*/
        a.disabled {
            color: gray;
        }

        .btn {
            width: 100%;
            background: #ccc;
            padding: 10px 5px;
            text-align: center;
            margin-top: 5px;
            margin-bottom: 10px;
            font-size: 14px;
            white-space: normal;
        }

        .item-status-wrap {
            padding-left: 5px;
            padding-right: 5px;
        }

        .item-status.btn-ok.active {
            background: #50b347;
        }

        .item-status.btn-error.active {
            background: red;
        }

        .top-info {
            border: none !important;
        }

        .preview-img {
            background: #fff;
        }

        .heading-title {
            background: #000;
            padding: 10px;
            text-align: center;
            color: #fff;
            text-transform: uppercase;
            font-size: 16px;
        }

        .top-filter .form-control {
            margin-bottom: 5px;
        }

        .top-filter {
            padding: 5px;
            border-bottom: 1px solid #f2f2f2;
        }

        .top-info {
            padding: 5px;
            border-bottom: 1px solid #f2f2f2;
        }

            .top-info .bill-info {
                text-align: center;
            }

            .top-info .salon-name {
                text-align: center;
                padding-top: 4px;
            }

            .top-info strong {
                font-weight: bold;
            }

        .top4-img {
            padding: 5px;
            border-bottom: 1px solid #f2f2f2;
        }

            .top4-img .img-item {
                background: #808080;
                padding: 10px;
                text-align: center;
                border-right: 1px solid #fff;
            }
                /*.top4-img .img-item:first-child { border-right: 1px solid #fff;}*/
                .top4-img .img-item img {
                    max-width: 60%;
                }

            .top4-img .col1 {
                border-bottom: 1px solid #fff;
                width: 100%;
                float: left;
            }

            .top4-img .col2 {
                width: 100%;
                float: left;
            }

        .img-nav {
            padding: 5px;
            border-bottom: 1px solid #f2f2f2;
            text-align: center;
        }
        /*#btnNext, #btnPreview { font-size: 20px;}*/
        .btn-default {
            background-color: #e6e6e6;
        }

        canvas {
        }

        .modal-dialog {
            max-width: 400px;
            margin: 10px auto;
        }

        .modal-body {
            padding: 0;
        }

        .modal-header {
            padding: 5px 15px;
        }

        .modal-footer {
            margin-top: 0;
            padding: 5px 10px;
        }

            .modal-footer i {
                font-size: 46px;
                cursor: pointer;
            }

        .carousel-control {
            top: inherit;
            bottom: 5%;
            padding-top: 10px;
            padding-bottom: 10px;
        }

        @media(max-width: 768px) {
            .modal-dialog {
                width: 100%;
            }
        }

        .page-loading {
            width: 100%;
            height: 100%;
            position: fixed;
            top: 0;
            left: 0;
            z-index: 100000;
            background: rgba(0,0,0, 0.65) url(/Assets/images/load_ring.svg) center no-repeat;
            text-align: center;
            display: none;
        }

            .page-loading p {
                color: #fcd344;
                position: relative;
                top: 50%;
                padding-top: 37px;
                line-height: 22px;
            }

        legend {
            padding: 0 10px;
            margin-bottom: 5px;
            font-size: inherit;
            font-family: Roboto Condensed Regular;
        }

        fieldset {
            padding: 0 5px;
            margin-top: 10px;
        }
    </style>
</head>
<body>
    <div class="wp">
        <div class="preview-img">
            <div class="heading-title">Xem ảnh lịch sử</div>
            <div class="top-filter">
                <div>
                    <select name="ddlSalon" id="ddlSalon" class="form-control select" onchange="getStylistBySalon()">
                    </select>
                </div>
                <div>
                    <select name="ddlStylist" id="ddlStylist" class="form-control">
                        <option value="0">Chọn Stylist</option>
                    </select>
                </div>
                <div>
                    <input runat="server" name="txtToDate" type="text" value="" id="txtToDate" class="form-control txtDateTime" placeholder="Ngày ..." />
                    <input type="submit" id="btnTimKiem" class="form-control btn-default btnSearch" value="Tìm" />
                </div>
            </div>
            <div class="top-info">
                <div class="bill-info">KH: <strong id="pre-salon"></strong> &nbsp - &nbsp  ĐT: &nbsp<strong id="pre-date"></strong></div>
                <div class="salon-name hidden">Id: &nbsp<strong id="pre-id"></strong></div>
                <div class="salon-name">DV: &nbsp<strong id="pre-dv"></strong></div>
            </div>
            <fieldset>
                <legend><b>Kiểu tóc</b></legend>
                <div class="top-info text-center btnHairList" style="padding-top: 0;">
                </div>
            </fieldset>

            <div class="top4-img">
                <div class="container">
                    <div class="row">
                        <div class="col1">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 img-item" data-toggle="modal" data-target="#myModal" data-slide-to="0">
                                <img src="https://30shine.com/images/1.jpg" alt="" id="pre-img1" class="image" />
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 img-item" data-toggle="modal" data-target="#myModal" data-slide-to="1">
                                <img src="https://30shine.com/images/2.jpg" alt="" id="pre-img2" class="image" />
                            </div>
                        </div>
                        <div class="col2">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 img-item" data-toggle="modal" data-target="#myModal" data-slide-to="2">
                                <img src="https://30shine.com/images/3.jpg" alt="" id="pre-img3" class="image" />
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 img-item" data-toggle="modal" data-target="#myModal" data-slide-to="3">
                                <img src="https://30shine.com/images/4.jpg" alt="" id="pre-img4" class="image" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <fieldset>
                <legend><b>Lỗi cắt</b></legend>
                <div class="top-info text-center btnList" style="padding-top: 0;">
                </div>
            </fieldset>
            <%--<fieldset>
                <legend><b>Kiểu khuôn mặt</b></legend>
                <div class="top-info text-center btnFaceList" style="padding-top: 0;">
                </div>
            </fieldset>--%>
            <div class="top-info">
                <table style="width: 100%">
                    <tr>
                        <td style="width: 25%; text-align: center;">Ghi chú của stylist</td>
                        <td style="width: 75%;">
                            <textarea id="txtNoteByStylist" class="form-control" disabled rows="2"></textarea></td>
                    </tr>
                </table>
            </div>
            <div class="top-info">
                <table style="width: 100%">
                    <tr>
                        <td style="width: 25%; text-align: center;">Ghi chú check lỗi</td>
                        <td style="width: 75%;">
                            <textarea id="txtErrorNote" class="form-control ErrorNote" disabled rows="2"></textarea></td>
                    </tr>
                </table>
            </div>
            
            <div class="img-nav">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-4 col-sm-4">
                            <input type="submit" id="btnPreview" class="form-control btn-default" value="Trước" />
                        </div>
                        <div class="col-xs-4 col-sm-4">
                            &nbsp(Đang xem hóa đơn <strong id="current-index">0</strong>/<strong id="total-index">0</strong>)&nbsp
                        </div>
                        <div class="col-xs-4 col-sm-4">
                            <input type="submit" id="btnNext" class="form-control btn-default" value="Sau" />
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" value="0" id="hdfIndexPage" />
            <input type="hidden" value="0" id="HDF_BillID" />
        </div>
    </div>

    <!-- Pop up-->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog" id="modalDialog">
            <!-- Modal content-->
            <div class="modal-content" id="modalContent">
                <div class="modal-header" id="modalHeader">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Check lỗi cắt</h4>
                </div>
                <div class="modal-body">
                    <div id="myCarousel" class="carousel" data-ride="carousel" data-interval="false">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                            <li data-target="#myCarousel" data-slide-to="3"></li>
                        </ol>
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox" id="canvasWrap">
                            <div class="item active" id="div-image1">
                                <%--<canvas id="pre-img1-can" data-order="1" style="background-repeat: no-repeat;" title="Before"></canvas>--%>
                                <img src="" alt="" id="pre-img1-view" data-order="1" />
                            </div>
                            <div class="item" id="div-image2">
                                <img src="" alt="" id="pre-img2-view" data-order="2" />
                            </div>
                            <div class="item" id="div-image3">
                                <img src="" alt="" id="pre-img3-view" data-order="3" />
                            </div>
                            <div class="item" id="div-image4">
                                <img src="" alt="" id="pre-img4-view" data-order="4" />
                            </div>
                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev" id="prev-Slide">
                            <%--<i class="fa fa-arrow-left" aria-hidden="true"></i>--%>
                            <span class="fa fa-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next" id="next-Slide">
                            <span class="fa fa-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                            <%--<i class="fa fa-arrow-right" aria-hidden="true"></i>--%>
                        </a>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-9"></div>
                        <div class="col-xs-3">
                            <i class="fa fa-times-circle" aria-hidden="true" data-dismiss="modal" title="Đóng"></i>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="page-loading">
        <p>Vui lòng đợi trong giây lát...</p>
    </div>
    <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
    <script type="text/ecmascript">
        var _ListPreviewImg = [];
        var objPreviewImg = {};
        var billID;

        function getStylistBySalon(stylistId) {
            var salonId = $("#ddlSalon").val();
            var select = $('#ddlStylist');
            if (salonId != "0") {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/PreviewImage/Preview_Random_Salon_Images.aspx/ReturnAllStylistBySalon",
                    data: '{_SalonId: ' + salonId + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {

                        select.html('');
                        select.append('<option selected="selected" value="0">Chọn Stylist</option>');
                        $.each(response.d, function (key, value) {
                            select.append('<option value="' + value.Id + '">' + value.Fullname + '</option>');
                        });
                        $('#ddlStylist option').removeAttr('selected').filter('[value=' + stylistId + ']').attr('selected', 'selected');
                        finishLoading();
                    },
                    failure: function (response) { console.log(response.d); }
                });
            } else {
                select.html('');
                select.append('<option selected="selected" value="0">Chọn Stylist</option>');
            }

        }

        $(document).ready(function () {
            startLoading();
            //Bind list salon
            $.ajax({
                type: "POST",
                url: "/GUI/FrontEnd/PreviewImage/Preview_Random_Salon_Images.aspx/ReturnAllSalon",
                data: '',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var select = $('#ddlSalon');

                    select.append('<option selected="selected" value="0">Chọn Salon</option>');
                    $.each(response.d, function (key, value) {
                        select.append('<option value="' + value.Id + '">' + value.Name + '</option>');
                    });
                    finishLoading();
                },
                failure: function (response) { console.log(response.d); }
            });
            $('#btnTimKiem').click(function () {
                //initCircles();

                var _SalonId = $("#ddlSalon").val();
                if (_SalonId == "0") {
                    alert("Bạn chưa chọn salon");
                    return;
                }

                var _StylistId = $("#ddlStylist").val();

                var _ToDate = $("#txtToDate").val();
                if (_ToDate == "") {
                    alert("Bạn chưa chọn ngày");
                    return;
                }

                startLoading();
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/PreviewImage/Preview_Random_Salon_Images.aspx/GetRandomBillImage",
                    data: '{_Date: "' + _ToDate + '", _SalonId: ' + _SalonId + ', _StylistId: ' + _StylistId + ', _imageStatus:"" }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        _ListPreviewImg = [];

                        $.each(response.d, function (key, value) {
                            objPreviewImg = {};
                            objPreviewImg.Id = value.Id;
                            $("#HDF_BillID").val(objPreviewImg.Id);
                            objPreviewImg.BillDate = value.BillDate;
                            objPreviewImg.SalonName = value.SalonName;
                            objPreviewImg.CustomerPhone = value.CustomerPhone;
                            objPreviewImg.CustomerName = value.CustomerName;
                            objPreviewImg.Img1 = value.Img1;
                            objPreviewImg.Img2 = value.Img2;
                            objPreviewImg.Img3 = value.Img3;
                            objPreviewImg.Img4 = value.Img4;
                            objPreviewImg.ImageStatusId = value.ImageStatusId;
                            objPreviewImg.ErrorNote = value.ErrorNote;
                            objPreviewImg.NoteByStylist = value.NoteByStylist;
                            objPreviewImg.ServiceIds = value.ServiceIds;
                            _ListPreviewImg.push(objPreviewImg);

                            // Set giá trị mặc định billID;
                            if (key == 0) {
                                setBillID(value.Id);
                                bindBillStatus(value.ImageStatusId);
                            }
                        });

                        if (_ListPreviewImg.length > 0) {
                            SetPreviewInfo(0);
                            $('#hdfIndexPage').val("0");
                            $('#total-index').text(_ListPreviewImg.length);
                            $('#current-index').text('1');
                        }

                        finishLoading();
                    },
                    failure: function (response) { console.log(response.d); }
                });

            });

            //Next Click
            $('#btnNext').click(function () {
                //updateErrorNote();
                var totalIndex = parseInt(_ListPreviewImg.length);
                var index = parseInt($('#hdfIndexPage').val());

                if ((totalIndex > 0) && (index < totalIndex - 1)) {
                    SetPreviewInfo(index + 1);
                    $('#hdfIndexPage').val(index + 1);
                    $('#current-index').text((index + 2));
                }
                cusHairId = "";
            });

            //Next Click
            $('#btnPreview').click(function () {
                //updateErrorNote();
                var totalIndex = parseInt(_ListPreviewImg.length);
                var index = parseInt($('#hdfIndexPage').val());

                if ((totalIndex > 0) && (index > 0)) {
                    SetPreviewInfo(index - 1);
                    $('#hdfIndexPage').val(index - 1);
                    $('#current-index').text((index));
                }
                cusHairId = "";
            });

            //============================
            // Datepicker
            //============================
            $('.txtDateTime').datetimepicker({
                dayOfWeekStart: 1,
                lang: 'vi',
                startDate: '2014/10/10',
                format: 'd/m/Y',
                dateonly: true,
                showHour: false,
                showMinute: false,
                timepicker: false,
                scrollMonth: false,
                scrollInput: false,
                onChangeDateTime: function (dp, $input) { }
            });


            $("#myModal").on('shown', function () {
            });
        });

        function testObject(jsonStrig) {

            var obj = JSON.parse(jsonStrig);
            var arr = [];
            for (var i = 0; i < obj.length; i++) {
                arr.push(obj[i].Name);
            }
            var result = arr.join();
            $("#pre-dv").text(result);
        }
        function SetPreviewInfo(index) {
            testObject(_ListPreviewImg[index].ServiceIds);
            $('#pre-id').text(_ListPreviewImg[index].Id);
            $('#pre-date').text(_ListPreviewImg[index].CustomerPhone);
            $('#pre-salon').text(_ListPreviewImg[index].CustomerName);
            $("#txtErrorNote").val(_ListPreviewImg[index].ErrorNote);
            $("#txtNoteByStylist").val(_ListPreviewImg[index].NoteByStylist);

            $('#pre-img1').attr('src', _ListPreviewImg[index].Img1);
            $('#pre-img2').attr('src', _ListPreviewImg[index].Img2);
            $('#pre-img3').attr('src', _ListPreviewImg[index].Img3);
            $('#pre-img4').attr('src', _ListPreviewImg[index].Img4);

            $('#pre-img1-view').attr('src', _ListPreviewImg[index].Img1);
            $('#pre-img2-view').attr('src', _ListPreviewImg[index].Img2);
            $('#pre-img3-view').attr('src', _ListPreviewImg[index].Img3);
            $('#pre-img4-view').attr('src', _ListPreviewImg[index].Img4);

            // set giá trị cho billID
            setBillID(_ListPreviewImg[index].Id);
            // reset trạng thái active của item trạng thái
            resetItemStatus();
            // bind lại trạng thái
            bindBillStatus(_ListPreviewImg[index].ImageStatusId);
            getHairStyleById(billID);
            //initCircles();
        }

        // Cập nhật giá trị cho billID
        function setBillID(id) {
            billID = id;
        }

        function bindBillStatus(statusID) {
            $(".btnErrorList").removeClass("active");
            if (statusID != null && statusID.length > 0) {
                var stt = statusID.toString().split(',');
                for (var i = 0; i < stt.length ; i++) {
                    if (stt[i] == "1") {
                        $(".btnErrorList[data-id='" + stt[i] + "']").addClass("btn-ok");
                    }
                    else {
                        $(".btnErrorList[data-id='" + stt[i] + "']").addClass("btn-error");
                    }
                    $(".btnErrorList[data-id='" + stt[i] + "']").addClass("active");
                }
            }
        }

        function resetItemStatus() {
            $(".btnErrorList").removeClass("active");
            $(".btnHairMode").removeClass("active");
        }

        function findIndexByBillID(billID) {
            var index = null;
            if (_ListPreviewImg != null) {
                for (var i = 0; i < _ListPreviewImg.length; i++) {
                    if (billID == _ListPreviewImg[i].Id) {
                        index = i;
                        break;
                    }
                }
            }
            return index;
        }

        /*
        * Full screen
        */
        var videoElement = document.getElementById("myModal");

        function toggleFullScreen() {
            if (!document.mozFullScreen && !document.webkitFullScreen) {
                if (videoElement.mozRequestFullScreen) {
                    videoElement.mozRequestFullScreen();
                } else {
                    console.log('hehedfd');
                    videoElement.webkitRequestFullScreen();
                }
            } else {
                if (document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else {
                    document.webkitCancelFullScreen();
                }
            }
        }

        //fn get Error List
        function GetErrorList() {
            $.ajax({
                type: "POST",
                url: "/GUI/FrontEnd/PreviewImage/Preview_Salon_Image_onlyView.aspx/GetErrorList",
                data: '',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var btnList = "";
                    $.each(response.d, function (i, v) {
                        btnList += '<div class="col-xs-3 item-status-wrap">' +
                                '<a data-id="' + v.Id + '" href="javascript:void(0);" class="btn btn-default btn-xs item-status btnErrorList" disabled>' + v.ErrorCut + '</a>' +
                                '</div>';
                    });
                    $(".btnList").append(btnList);
                    var heightArray = [];
                    for (var i = 0; i < $(".btnList").find(".btnErrorList").length; i++) {
                        var height = $(".btnList").find(".btnErrorList").eq(i).height();
                        heightArray.push(height);
                    }
                    var maxHeight = Math.max.apply(null, heightArray);
                    $(".btnList").find(".btnErrorList").css("height", maxHeight + 16);
                    finishLoading();
                    finishLoading();
                },
                failure: function (response) { console.log(response.d); }
            });
        }

        GetErrorList();

        function GetListHairMode() {
            $.ajax({
                type: "POST",
                url: "/GUI/FrontEnd/PreviewImage/Preview_Salon_Image_onlyView.aspx/ReturnAllHairMode",
                data: '',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var btnList = "";
                    $.each(response.d, function (i, v) {
                        btnList += '<div class="col-xs-3 item-status-wrap">' +
                                '<a data-id="' + v.Id + '" href="javascript:void(0);" disabled class="btn btn-default btn-xs btn-ok item-status btnHairMode">' + v.Title + '</a>' +
                                '</div>';
                    });
                    $(".btnHairList").append(btnList);
                    var heightArray = [];
                    for (var i = 0; i < $(".btnHairList").find(".btnHairMode").length; i++) {
                        var height = $(".btnHairList").find(".btnHairMode").eq(i).height();
                        heightArray.push(height);
                    }
                    var maxHeight = Math.max.apply(null, heightArray);
                    $(".btnHairList").find(".btn").css("height", maxHeight + 16);
                    finishLoading();
                },
                failure: function (response) { console.log(response.d); }
            });
        }
        GetListHairMode();
        var cusHairId;
        var hairStyleId;
        //fn get list HairMode by Id
        function getHairStyleById(billID) {
            $.ajax({
                type: "POST",
                url: "/GUI/FrontEnd/PreviewImage/Preview_Salon_Image_onlyView.aspx/getHairStyleById",
                data: '{billId:' + billID + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    if (response.d != null) {
                        cusHairId = response.d.Id;
                        hairStyleId = response.d.HairStyleId;
                        //faceTypeId = response.d.FaceTypeId;
                    }
                    bindBillHairStylist(hairStyleId);
                    hairStyleId = "";
                    //faceTypeId = "";
                },
                failure: function (response) { console.log(response.d); }
            });
        }
        function bindBillHairStylist(hairStyleId) {
            $(".btnHairMode").removeClass("active");
            //$(".btnFaceType").removeClass("active");
            if (hairStyleId != null) {
                $(".btnHairMode").removeClass("active");
                $(".btnHairMode[data-id='" + hairStyleId + "']").addClass("active");
            }
            //if (faceTypeId != null) {
            //    $(".btnFaceType").removeClass("active");
            //    $(".btnFaceType[data-id='" + faceTypeId + "']").addClass("active");
            //}
        }

        //Get Kiểu khuôn mặt 
        //function GetFaceType() {
        //    $.ajax({
        //        url: "/GUI/FrontEnd/PreviewImage/Preview_Random_Salon_Images.aspx/GetFaceType",
        //        type: "post",
        //        data: '',
        //        contentType: "application/json; charset=utf-8",
        //        dataType: "json", success: function (response) {
        //            var btnList = "";
        //            $.each(response.d, function (i, v) {
        //                btnList += '<div class="col-xs-3 item-status-wrap">' +
        //                        '<a data-id="' + v.Id + '" href="javascript:void(0);"  disabled class="btn btn-default btn-xs btn-ok item-status btnFaceType">' + v.Shade + '</a>' +
        //                        '</div>';
        //            });
        //            $(".btnFaceList").append(btnList);
        //            var heightArray = [];
        //            for (var i = 0; i < $(".btnFaceList").find(".btnFaceType").length; i++) {
        //                var height = $(".btnFaceList").find(".btnFaceType").eq(i).height();
        //                heightArray.push(height);
        //            }
        //            var maxHeight = Math.max.apply(null, heightArray);
        //            $(".btnFaceList").find(".btn").css("height", maxHeight + 16);
        //            finishLoading();
        //        },
        //        failure: function (response) { console.log(response.d); }
        //    });
        //}
        //GetFaceType();

        document.addEventListener("keydown", function (e) {
            if (e.keyCode == 13) {
                toggleFullScreen();
            }
        }, false);
    </script>
</body>
</html>
