﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.FrontEnd.PreviewImage
{
    public partial class StatisticScscSalonV3 : System.Web.UI.Page
    {
        private bool Perm_ViewAllData = false;

        public bool Perm_Delete { get; private set; }
        public bool Perm_ShowSalon { get; private set; }
        public bool Perm_Access { get; private set; }
        public bool Perm_Edit { get; private set; }
        public CultureInfo culture = new CultureInfo("vi-VN");

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var msgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", msgParam);
            }
            else
            {

                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();

            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList>() { ddlSalon }, Perm_ViewAllData);
            }
        }
        protected void Salon_SelectedIndexChanged(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var salon = Convert.ToInt32(ddlSalon.SelectedValue);
                var staff = new List<Staff>();
                var whereSalon = "";
                var sql = "";

                if (salon > 0)
                {
                    whereSalon = " and SalonId = " + salon;
                }
                sql = @"select * from Staff 
                        where IsDelete != 1 and Active = 1  and Type = 1
                        and (isAccountLogin != 1 or isAccountLogin is null)" + whereSalon;
                staff = db.Staffs.SqlQuery(sql).ToList();

                var first = new Staff();
                first.Id = 0;
                first.Fullname = "Chọn nhân viên";
                staff.Insert(0, first);

                ddlStylist.DataTextField = "Fullname";
                ddlStylist.DataValueField = "Id";
                ddlStylist.DataSource = staff;
                ddlStylist.DataBind();
            }
        }
        private void BindAllSalon()
        {
            using (var db = new Solution_30shineEntities())
            {
                if (TxtDateTimeFrom.Text != "")
                {
                    DateTime _FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                    DateTime _ToDate;
                    if (TxtDateTimeTo.Text != "")
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                    }
                    else
                    {
                        _ToDate = _FromDate;
                    }
                    //var list = db.SCSC_TotalAllSalon_Store_V2(_FromDate, _ToDate).ToList();
                    //var item = new SCSC_TotalAllSalon_Store_V2_Result();
                    var list = GetDataAllSalon(_FromDate, _ToDate);
                    var item = new cls_SCSCUonAllSalon();
                    var countList = list.Count;
                    item.ShortName = "Tổng";
                    item.TotalBill = list.Sum(a => a.TotalBill);
                    item.Percent_Bill_NotImg = list.Sum(a => a.TotalBill ?? 0) != 0
                        ? Math.Round(
                            ((Double)list.Sum(a => a.TotalBillNotImage ?? 0) /
                             (Double)list.Sum(a => a.TotalBill ?? 0) * 100), 2)
                        : 0;
                    item.Percent_BillImg_MoLech = list.Sum(a => a.TotalBill ?? 0) != 0
                        ? Math.Round(
                            (Double)list.Sum(r => r.TotalBill_DanhGiaImgMoLech ?? 0) /
                            (Double)list.Sum(r => r.TotalBill ?? 0) * 100, 2)
                        : 0;
                    item.ErrorShapeId = list.Sum(a => a.ErrorShapeId);
                    item.ErrorSharpNess = list.Sum(a => a.ErrorSharpNess);
                    item.ErrorConnective = list.Sum(a => a.ErrorConnective);
                    item.ErrorComplatetion = list.Sum(a => a.ErrorComplatetion);
                    item.BillChuaDanhGia = list.Sum(a => a.BillChuaDanhGia);
                    item.Percent_BillError = Math.Round(
                        ((Double)list.Sum(r => r.TotalErrorSCSC ?? 0) -
                         (Double)list.Sum(r => r.TotalBill_DanhGiaImgMoLech ?? 0)) /
                        (Double)list.Sum(r => r.TotalBill ?? 0) *
                        100, 2);

                    item.Percent_ErrorKCS = list.Sum(a => a.TotalBill ?? 0) != 0
                        ? Math.Round(
                            (Double)list.Sum(a => a.TotalErrorSCSCUon ?? 0) /
                            (Double)list.Sum(r => r.TotalBill ?? 0) * 100, 2)
                        : 0;
                    item.PointSCSC_TB = (list.Sum(r => r.TotalBill_DanhGiaNotMolech ?? 0) +
                                         list.Sum(r => r.TotalBill_DanhGiaImgMoLech ?? 0)) == 0
                        ? 0
                        : Math.Round(
                            (Double)list.Sum(r => r.TotalSCSC ?? 0) /
                            ((Double)list.Sum(r => r.TotalBill_DanhGiaNotMolech ?? 0) +
                             (Double)list.Sum(r => r.TotalBill_DanhGiaImgMoLech ?? 0)), 2);
                    item.BillNotImgUon = list.Sum(a => a.TotalBillUon) != 0
                        ? Math.Round(
                            (Double)list.Sum(a => a.TongBillNotAnhUon ?? 0) /
                            (Double)list.Sum(r => r.TotalBillUon) * 100, 2)
                        : 0;
                    item.BillNotImgUonMoLech = list.Sum(a => a.TotalBillUon) != 0
                        ? Math.Round(
                            (Double)list.Sum(a => a.TongBillAnhMoLech ?? 0) /
                            (Double)list.Sum(r => r.TotalBillUon) * 100, 2)
                        : 0;
                    item.BillUonChamLoi = (list.Sum(r => r.TongBillNotAnhUon ?? 0) +
                                           list.Sum(r => r.TongBillAnhUon ?? 0)) == 0
                        ? 0
                        : Math.Round(
                            (Double)list.Sum(r => r.TongBillUonChamLoi ?? 0) /
                            ((Double)list.Sum(r => r.TongBillNotAnhUon ?? 0) +
                             (Double)list.Sum(r => r.TongBillAnhUon ?? 0)) * 100, 2);

                    item.TotalDiemUonTB = list.Sum(a => a.TongBillAnhUon ?? 0) == 0
                        ? 0
                        : Math.Round(
                            (Double)list.Sum(a => a.TotalPointSCSCCurling ?? 0) /
                            (Double)list.Sum(r => r.TongBillAnhUon ?? 0), 2);
                    item.TotalBillUon = list.Sum(a => a.TotalBillUon);

                    item.Percent_ErrorKCSUon = list.Sum(a => a.TotalBillUon) != 0
                        ? Math.Round(
                            ((Double)list.Sum(r => r.TongBillUonChamLoi ?? 0) -
                             (Double)list.Sum(r => r.TongBillAnhMoLech ?? 0)) / (Double)list.Sum(r => r.TotalBillUon) *
                            100, 2)
                        : 0;
                    item.ErrorHairTip = list.Sum(a => a.ErrorHairTip);
                    item.ErrorHairRoots = list.Sum(a => a.ErrorHairRoots);
                    item.ErrorHairWaves = list.Sum(a => a.ErrorHairWaves);
                    list.Insert(0, item);
                    rptAllSalon.DataSource = list;
                    rptAllSalon.DataBind();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Message System", "ShowMessage('','" + "BẠN CHƯA CHỌN NGÀY',4,7000);", true);
                }
            }
        }

        /// <summary>
        /// Bind data theo từng salon
        /// </summary>
        private void BindOnlySalon()
        {
            using (var db = new Solution_30shineEntities())
            {
                if (TxtDateTimeFrom.Text != "")
                {
                    DateTime _FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                    DateTime _ToDate;
                    if (TxtDateTimeTo.Text != "")
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                    }
                    else
                    {
                        _ToDate = _FromDate;
                    }
                    int SalonId = Convert.ToInt32(ddlSalon.SelectedValue);
                    int StylistId = Convert.ToInt32(ddlStylist.SelectedValue);
                    //var list = db.SCSC_BindStaffWhereSalon_Store_V2(_FromDate, _ToDate, SalonId, StylistId).ToList();
                    //var item = new SCSC_BindStaffWhereSalon_Store_V2_Result();
                    var list = GetDataOnlyStaff(StylistId, SalonId, _FromDate, _ToDate);
                    var item = new cls_SCSCUonOnlyStaff();
                    var countList = list.Count;
                    item.FullName = "Tổng";
                    item.TotalBill = list.Sum(a => a.TotalBill);
                    item.Bill_NotImg = list.Sum(a => a.TotalBill ?? 0) != 0
                        ? Math.Round(
                            ((Double)list.Sum(a => a.TotalBillNotImg ?? 0) /
                             (Double)list.Sum(a => a.TotalBill ?? 0) * 100), 2)
                        : 0;
                    item.TotalBill_ChuaDanhGia = list.Sum(a => a.TotalBill_ChuaDanhGia);
                    item.Percent_BillError = Math.Round(
                        ((Double)list.Sum(r => r.TotalErrorSCSC ?? 0) -
                         (Double)list.Sum(r => r.TongBillDanhGiaAnhMo ?? 0)) /
                        (Double)list.Sum(r => r.TotalBill ?? 0) *
                        100, 2);
                    item.Percent_ErrorKCS = list.Sum(a => a.TotalBill ?? 0) != 0
                        ? Math.Round(
                            (Double)list.Sum(a => a.TotalErrorSCSCUon ?? 0) /
                            (Double)list.Sum(r => r.TotalBill ?? 0) * 100, 2)
                        : 0;
                    item.Percent_BillImg_MoLech = list.Sum(a => a.TotalBill ?? 0) != 0
                        ? Math.Round(
                            (Double)list.Sum(r => r.TongBillDanhGiaAnhMo ?? 0) /
                            (Double)list.Sum(r => r.TotalBill ?? 0) * 100, 2)
                        : 0;
                    item.PointSCSC_TB =
                        (list.Sum(r => r.TotalBillDanhGiaNotImageMoLech ?? 0) +
                         list.Sum(r => r.TongBillDanhGiaAnhMo ?? 0)) == 0
                            ? 0
                            : Math.Round(
                                (Double)list.Sum(r => r.TotalPointSCSC ?? 0) /
                                ((Double)list.Sum(r => r.TotalBillDanhGiaNotImageMoLech ?? 0) +
                                 (Double)list.Sum(r => r.TongBillDanhGiaAnhMo ?? 0)), 2);
                    item.ErrorShapeId = list.Sum(a => a.ErrorShapeId);
                    item.ErrorSharpNessId = list.Sum(a => a.ErrorSharpNessId);
                    item.ErrorConnectiveId = list.Sum(a => a.ErrorConnectiveId);
                    item.ErrorComplatetionId = list.Sum(a => a.ErrorComplatetionId);

                    item.BillNotImgUon = list.Sum(a => a.TotalBillUon) != 0
                        ? Math.Round(
                            (Double)list.Sum(a => a.TongBillNotAnhUon ?? 0) /
                            (Double)list.Sum(r => r.TotalBillUon) * 100, 2)
                        : 0;
                    item.BillNotImgUonMoLech = list.Sum(a => a.TotalBillUon) != 0
                        ? Math.Round(
                            (Double)list.Sum(a => a.TongBillAnhMoLech ?? 0) /
                            (Double)list.Sum(r => r.TotalBillUon) * 100, 2)
                        : 0;
                    item.BillUonChamLoi =
                        (list.Sum(r => r.TongBillNotAnhUon ?? 0) +
                         list.Sum(r => r.TongBillAnhUon ?? 0)) == 0
                            ? 0
                            : Math.Round(
                                (Double)list.Sum(r => r.TongBillUonChamLoi ?? 0) /
                                ((Double)list.Sum(r => r.TongBillNotAnhUon ?? 0) +
                                 (Double)list.Sum(r => r.TongBillAnhUon ?? 0)) * 100, 2);

                    item.TotalDiemUonTB = list.Sum(a => a.TongBillAnhUon ?? 0) == 0
                        ? 0
                        : Math.Round(
                            (Double)list.Sum(a => a.TotalPointSCSCCurling ?? 0) /
                            (Double)list.Sum(r => r.TongBillAnhUon ?? 0), 2);

                    item.TotalBillUon = list.Sum(a => a.TotalBillUon);

                    item.Percent_ErrorKCSUon = list.Sum(a => a.TotalBillUon) != 0
                        ? Math.Round(
                            ((Double)list.Sum(r => r.TongBillUonChamLoi ?? 0) -
                             (Double)list.Sum(r => r.TongBillAnhMoLech ?? 0)) / (Double)list.Sum(r => r.TotalBillUon) *
                            100, 2)
                        : 0;
                    item.ErrorHairTip = list.Sum(a => a.ErrorHairTip);
                    item.ErrorHairRoots = list.Sum(a => a.ErrorHairRoots);
                    item.ErrorHairWaves = list.Sum(a => a.ErrorHairWaves);
                    list.Insert(0, item);
                    rptOnlySalon.DataSource = list;
                    rptOnlySalon.DataBind();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Message System", "ShowMessage('','" + "BẠN CHƯA CHỌN NGÀY',4,7000);", true);
                }
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            try
            {
                if (CheckLimitTime())
                {
                    int SalonId = Convert.ToInt32(ddlSalon.SelectedValue);
                    if (SalonId == 0)
                    {
                        BindAllSalon();
                        rptOnlySalon.DataSource = "";
                        rptOnlySalon.DataBind();
                    }
                    else
                    {
                        BindOnlySalon();
                        rptAllSalon.DataSource = "";
                        rptAllSalon.DataBind();
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Message System", "ShowMessage('','LƯU Ý: BẠN CHỈ ĐƯỢC XEM TỪ 10H TỐI',4,7000);", true);
                }
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Message System", "ShowMessage('','" + "Đã xảy ra lỗi: " + ex.Message.Replace("'", "") + "',4,7000);", true);
            }

            RemoveLoading();
        }

        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "finishLoading();ConfigTableListing()", true);
        }

        /// <summary>
        /// Bind kiểu tóc
        /// </summary>
        //private void Bind_HairName()
        //{
        //    using (var db = new Solution_30shineEntities())
        //    {
        //        var list = db.Api_HairMode.Where(w => w.IsDelete == false && w.Publish == true).ToList();
        //        if (list.Count > 0)
        //        {
        //            for (int i = 0; i < list.Count; i++)
        //            {
        //                ddlHairStyle.Items.Add(new ListItem(list[i].Title, list[i].Id.ToString()));
        //            }
        //            ddlHairStyle.DataBind();
        //        }
        //    }
        //}

        /// <summary>
        /// BindHairStyle
        /// </summary>
        /// <param name="timeFrom"></param>
        /// <param name="timeTo"></param>
        /// <param name="staffID"></param>
        /// <returns></returns>
        //[WebMethod]
        //public static string BindHairStyle(string timeFrom, string timeTo, int staffID)
        //{
        //    using (var db = new Solution_30shineEntities())
        //    {
        //        //var list = db.Store_SCSC_BindHairStyle(timeFrom, timeTo, staffID).ToList();
        //        //return new JavaScriptSerializer().Serialize(list);
        //        try
        //        {
        //            var name = db.Staffs.Where(w => w.Id == staffID).FirstOrDefault();
        //            DateTime _TimeFrom = Convert.ToDateTime(timeFrom, new CultureInfo("vi-VN"));
        //            DateTime _TimeTo = Convert.ToDateTime(timeTo, new CultureInfo("vi-VN"));
        //            var sql = @"DECLARE @timeFrom DATETIME, @timeTo DATETIME, @staffId INT
        //                        SET @timeFrom ='" + _TimeFrom + @"'
        //                        SET @timeTo ='" + _TimeTo + @"'
        //                        SET @staffId ='" + staffID + @"'
        //                        BEGIN
        //                        WITH TempTotalBill as (
	       //                         select c.HairStyleId as HairStyle_ID, COUNT(b.Id) as TotalBill 
	       //                         from BillService as b 
		      //                          inner join Customer_HairMode_Bill as c on c.BillId=b.Id
	       //                         where b.CreatedDate BETWEEN @timeFrom AND @timeTo
		      //                          and b.ServiceIds!='' and b.IsDelete=0  and b.Pending=0
		      //                          and ((b.Staff_Hairdresser_Id=@staffId) or (@staffId=0))
		      //                          group by  c.HairStyleId
        //                        ),	
        //                        TempTotalHairStyle AS (
	       //                         select c.HairStyleId as HairStyle_ID,COUNT(c.HairStyleId) as TotalHairStyle 
	       //                             from Customer_HairMode_Bill as c 
			     //                           inner join BillService as b on c.BillId=b.Id
			     //                           inner join Staff as s on b.Staff_Hairdresser_Id=s.Id
	       //                         where  b.CreatedDate BETWEEN @timeFrom AND @timeTo
			     //                           and (s.Id = @StaffId) or (@StaffId = 0)
			     //                           GROUP BY c.HairStyleId
        //                        ),
        //                         TempTotalPointSCSC as(
	       //                         select c.HairStyleId as HairStyle_ID,SUM(s.PointError) as TotalPointSCSC 
	       //                         from BillService as b 
		      //                          inner join SCSC_CheckError as s on b.Id=s.BillService_ID
		      //                          inner join Customer_HairMode_Bill as c on c.BillId=b.Id
	       //                         where  b.CreatedDate BETWEEN @timeFrom AND @timeTo and ((b.Staff_Hairdresser_Id = @staffId) or (@staffId=0)) 
        //                                 and b.Pending=0 and b.IsDelete=0 and b.ServiceIds!=''
		      //                          and s.Publish=1 and s.IsDelete=0
		      //                          Group by c.HairStyleId),
        //                        TempTotalErrorSCSC as (
        //                            select c.HairStyleId as HairStyle_ID,COUNT( b.Id) as TotalErrorSCSC 
	       //                         from BillService as b
		      //                           inner join SCSC_CheckError as error on b.Id = error.BillService_ID
		      //                           inner join Customer_HairMode_Bill as c on c.BillId=b.Id
        //                           where b.CreatedDate BETWEEN @timeFrom AND @timeTo and b.Pending = 0 
        //                                  AND ((b.Staff_Hairdresser_Id = @staffId ) or (@staffId = 0))
		      //                            and b.ServiceIds !='' and b.IsDelete =0 
		      //                            and error.IsDelete = 0 and error.Publish = 1  and error.PointError = 0 and error.ImageError = 0
        //                           group by  c.HairStyleId),
        //                        TempTotalBillNotImage as(
        //                           select c.HairStyleId as HairStyle_ID, a.Title as NameHair, COUNT(b.Id) as TotalBillNotImage 
        //                           from BillService as b 
	       //                            inner join  Customer_HairMode_Bill as c on c.BillId=b.Id
	       //                            inner join Api_HairMode as a on a.Id = c.HairStyleId
        //                            where b.CreatedDate BETWEEN @timeFrom AND @timeTo
	       //                           and ((b.Staff_Hairdresser_Id=@staffId) or (@staffId=0))
	       //                           and b.Pending=0 and b.ServiceIds!='' and b.Images is null
	       //                           and b.IsDelete=0
	       //                           group by a.Title, c.HairStyleId),
        //                        TempTotalErrorShape as (
        //                           select c.HairStyleId as HairStyle_ID, COUNT (b.Id) as TotalErrorShape 
        //                           from BillService as b 
	       //                            inner join SCSC_CheckError as error on b.Id=error.BillService_ID
	       //                            inner join SCSC_Category as cate on error.Shape_ID=cate.ID_SCSC_Cate
	       //                            inner join Customer_HairMode_Bill as c on c.BillId=b.Id
        //                          where b.CreatedDate BETWEEN @timeFrom AND @timeTo AND ((b.Staff_Hairdresser_Id=@staffId) or (@staffId=0)) AND b.Pending=0 and b.ServiceIds!='' and b.IsDelete=0
	       //                            and error.IsDelete=0 and error.Publish=1 
	       //                            and cate.SCSC_Cate_Point=0 
	       //                            group by  c.HairStyleId
	       //                            ),
        //                        TempTotalErrorConnective as (
        //                           select c.HairStyleId as HairStyle_ID,  COUNT(b.Id) as TotalErrorConnective 
        //                           from BillService as b
	       //                            inner join SCSC_CheckError as error on b.Id=error.BillService_ID
	       //                            inner join SCSC_Category as cate on error.ConnectTive_ID = cate.ID_SCSC_Cate
	       //                            inner join Customer_HairMode_Bill as c on c.BillId=b.Id
	       //                         where b.CreatedDate BETWEEN @timeFrom AND @timeTo and ((b.Staff_Hairdresser_Id = @staffId) or (@staffId=0))	and b.Pending=0 and b.ServiceIds!=''
	       //                            and error.IsDelete=0 and error.Publish=1  and b.IsDelete=0
	       //                            and cate.SCSC_Cate_Point=0 
	       //                             group by c.HairStyleId
	       //                            ),
        //                        TempTotalErrorSharpNess as(
        //                          select c.HairStyleId as HairStyle_ID,COUNT(b.Id) as TotalErrorSharpNess 
        //                          from BillService as b
	       //                           inner join SCSC_CheckError as error on b.Id= error.BillService_ID
	       //                           inner join SCSC_Category as cate on error.SharpNess_ID = cate.ID_SCSC_Cate
	       //                           inner join Customer_HairMode_Bill as c on c.BillId = b.Id
        //                          where b.CreatedDate BETWEEN @timeFrom AND @timeTo
	       //                           and ((b.Staff_Hairdresser_Id=@staffId) or (@staffId=0))
	       //                           and error.IsDelete=0 and error.Publish=1 and b.IsDelete=0
	       //                           and b.Pending=0 and b.ServiceIds!=''
	       //                           and cate.SCSC_Cate_Point=0 
	       //                            group BY   c.HairStyleId
	       //                           ),
        //                        TempTotalErrorCompletion as (
        //                          select c.HairStyleId as HairStyle_ID, COUNT(b.Id) as TotalErrorCompletion 
        //                          from BillService as b 
	       //                           inner join SCSC_CheckError as error on b.Id = error.BillService_ID
	       //                           inner join SCSC_Category as cate on error.ComPlatetion_ID = cate.ID_SCSC_Cate
	       //                           inner join Customer_HairMode_Bill as c on c.BillId = b.Id
        //                           where b.CreatedDate BETWEEN @timeFrom AND @timeTo
	       //                           and ((b.Staff_Hairdresser_Id=@staffId) or (@staffId=0))
	       //                           and error.IsDelete=0 and error.Publish=1 and b.IsDelete=0
	       //                           and b.Pending=0 and b.ServiceIds!=''
	       //                           and cate.SCSC_Cate_Point=0 
	       //                          group by  c.HairStyleId
	       //                           ),
        //                        TempTotalBill_ImgMoLech as
	       //                           (
	       //                            select c.HairStyleId as HairStyle_ID, COUNT(b.Id) as TotalBill_ImgMoLech
        //                               from BillService as b 
	       //                            inner join  Customer_HairMode_Bill as c on c.BillId=b.Id
	       //                            inner join SCSC_CheckError as d on b.Id = d.BillService_ID
        //                            where b.CreatedDate BETWEEN @timeFrom AND @timeTo
	       //                           and ((b.Staff_Hairdresser_Id=@staffId) or (@staffId=0))
	       //                           and b.Pending=0 and b.ServiceIds!='' and d.ImageError = 1
	       //                           and b.IsDelete=0
	       //                           group BY  c.HairStyleId) ,	 
        //                        TempSCSC1 as 
	       //                         (
		      //                          select a.HairStyle_ID,t.TotalHairStyle,		                    
        //                                Round(cast( (ISNULL(e.TotalPointSCSC,0)) as float) / CAST((CASE WHEN ISNULL(a.TotalBill,0) = 0 THEN 1 ELSE a.TotalBill END )AS FLOAT) ,2) as PointSCSC_TB,
		      //                          Round((cast(ISNULL( k.TotalErrorSCSC,0) as float)) / ((cast((ISNULL(a.TotalBill,0)) as float) - cast((ISNULL(b.TotalBillNotImage,0)) as float) - cast((ISNULL(j.TotalBill_ImgMoLech,0)) as float))) * 100 ,2) as Percent_KCS, 
		      //                          ROUND(cast((ISNULL(b.TotalBillNotImage,0)) as float) / CAST(a.TotalBill as float)* 100,2) as Total_NotImages
	       //                         from TempTotalBill as a
		      //                          left join TempTotalHairStyle as t on a.HairStyle_ID=t.HairStyle_ID
		      //                          left join TempTotalBillNotImage as b on a.HairStyle_ID = b.HairStyle_ID
		      //                          left join TempTotalPointSCSC as e on a.HairStyle_ID = e.HairStyle_ID
		      //                          left join TempTotalErrorSCSC as k on a.HairStyle_ID = k.HairStyle_ID
		      //                          left join TempTotalBill_ImgMoLech as j on a.HairStyle_ID = j.TotalBill_ImgMoLech
	       //                         ),
	       //                         TemSCSC2 as
	       //                         (
	       //                         select a.HairStyle_ID as HairStyle_ID2,
		      //                          ISNULL( d.TotalErrorShape,0)  as TotalBillErrorShape,
		      //                          ISNULL(b.TotalErrorSharpNess,0) as TotalBillErrorSharpNess,
		      //                          ISNULL(c.TotalErrorCompletion,0) as TotalBillErrorCompletion,
		      //                          ISNULL (e.TotalErrorConnective,0) as TotalBillErrorConnective
	       //                         from TempTotalBill as a
		      //                          left join TempTotalErrorSharpNess as b on a.HairStyle_ID = b.HairStyle_ID
		      //                          left join TempTotalErrorCompletion as c on a.HairStyle_ID = c.HairStyle_ID
		      //                          left join TempTotalErrorShape as d  on a.HairStyle_ID = d.HairStyle_ID
		      //                          left join TempTotalErrorConnective as e on a.HairStyle_ID = e.HairStyle_ID
	       //                         )	
	       //                         select  a.HairStyle_ID ,s.Title as NameHair, 
			     //                           a.TotalHairStyle, a.PointSCSC_TB, a.Percent_KCS,a.Total_NotImages,
			     //                           b.TotalBillErrorShape, b.TotalBillErrorSharpNess, 
			     //                           b.TotalBillErrorConnective, b.TotalBillErrorCompletion 
	       //                         from TempSCSC1 as a
			     //                           inner join TemSCSC2 as b on a.HairStyle_ID= b.HairStyle_ID2
			     //                           inner join Api_HairMode as s on a.HairStyle_ID = s.Id
	       //                         END";
        //            var list = db.Database.SqlQuery<Store_SCSC_BindHairStyle_Result>(sql).ToList();
        //            return new JavaScriptSerializer().Serialize(list);
        //        }
        //        catch (Exception ex)
        //        {
        //            throw ex;
        //        }
        //    }
        //}

        /// <summary>
        ///  Báo cáo điểm SCSC TB vị trí Stylist
        ///  Author: QuynhDD
        /// </summary>
        /// <param name="timeFrom"></param>
        /// <param name="timeTo"></param>
        /// <param name="salonID"></param>
        /// <param name="hairStyleID"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        //[WebMethod]
        //public static string BindHairStyle_V1(string timeFrom, string timeTo, int salonID, int hairStyleID, int order)
        //{
        //    try
        //    {
        //        using (var db = new Solution_30shineEntities())
        //        {
        //            DateTime _Timefrom = Convert.ToDateTime(timeFrom, new CultureInfo("vi-VN"));
        //            DateTime _Timeto = Convert.ToDateTime(timeTo, new CultureInfo("vi-VN"));
        //            var sql = @" declare @timeFrom datetime, @timeTo datetime, @hairStyleID int, @salonID int
        //                         set @timeFrom ='" + _Timefrom + @"'
        //                         set @timeTo = '" + _Timeto + @"'
        //                         set @HairStyleID ='" + hairStyleID + @"'
        //                         set @SalonID ='" + salonID + @"'
        //                          begin
        //                        with TempTotalBill as(
        //                        select b.Staff_Hairdresser_Id as IDNhanVien,  COUNT(b.Id) as TotalBill
        //                        from BillService as b
	       //                     inner join Staff as staff on b.Staff_Hairdresser_Id = staff.Id
        //                        inner join Tbl_Salon as salon on staff.SalonId=salon.Id
        //                        inner join Customer_HairMode_Bill as c on c.BillId = b.Id
	       //                     INNER JOIN FlowService AS flow ON b.Id=flow.BillId
	       //                     where 
        //                        b.CreatedDate between @timeFrom and @timeTo and b.ServiceIds !='' and b.IsDelete =0 and b.Pending = 0 	and ((b.SalonId = @salonID) or (@salonID = 0))     AND LEN(b.ServiceIds) =101
        //                        and  salon.IsDelete =0 and salon.IsSalonHoiQuan = 0
        //                        and c.HairStyleId = @HairStyleID	
	       //                     and flow.ServiceId=53
        //                        group by b.Staff_Hairdresser_Id 
        //                        )
        //                       --select IDNhanVien from TempTotalBill group by IDNhanVien having COUNT(IDNhanVien) > 1  end
        //                        ,
        //                      TempTotalPointSCSC as (
        //                     select b.Staff_Hairdresser_Id as IDNhanVien, SUM(s.PointError) as TotalPointSCSC  
        //                     from BillService as b 
	       //                      inner join Staff as staff on b.Staff_Hairdresser_Id = staff.Id
        //                         inner join Tbl_Salon as salon on staff.SalonId=salon.Id
        //                         inner join SCSC_CheckError as s on b.Id=s.BillService_ID
        //                         inner join Customer_HairMode_Bill as c on c.BillId= b.Id
	       //                      INNER JOIN FlowService AS flow ON b.Id=flow.BillId
        //                     where 
        //                         b.CreatedDate between @timeFrom and @timeTo  and b.ServiceIds !='' and b.IsDelete =0 and b.Pending = 0  AND LEN(b.ServiceIds) =101
        //                         and ((b.SalonId=@salonID) or (@salonID=0))
        //                         and c.HairStyleId=@HairStyleID
        //                         and  salon.IsDelete =0 and salon.IsSalonHoiQuan = 0 
        //                         and s.IsDelete=0 and s.Publish=1 and s.PointError > 0
	       //                      and flow.ServiceId=53
	       //                     Group by b.Staff_Hairdresser_Id
        //                     ) 
        //                     --select * from TempTotalPointSCSC where IDNhanVien = 1044 end
        //                     ,
        //                    TempAVGTimeCat as (
        //                        select  
        //                        a.Staff_Hairdresser_Id as IDNhanVien, 
        //                    SUM (DATEDIFF(MINUTE, DATEADD(MINUTE, 12, a.InProcedureTime ),case when a.UploadImageTime is null then a.CompleteBillTime ELSE a.UploadImageTime END )) as AVGTimeCat
        //                        from BillService as a 
	       //                     inner join Staff as staff on a.Staff_Hairdresser_Id = staff.Id
        //                        INNER JOIN Customer_HairMode_Bill as b on a.Id = b.BillId
        //                        INNER JOIN Tbl_Salon as salon on staff.SalonId = salon.Id
        //                        INNER JOIN FlowService AS flow ON a.Id=flow.BillId

        //                        where
        //                         a.CreatedDate between @timeFrom  AND @timeTo AND a.ServiceIds !='' 
        //                         AND LEN(a.ServiceIds) =101 AND a.IsDelete = 0 AND a.Pending = 0
        //                         AND ((a.SalonId=@salonID) or (@salonID=0)) AND a.Staff_Hairdresser_Id != 0
        //                         AND b.HairStyleId=@HairStyleID
        //                         AND salon.IsDelete = 0  AND salon.Publish = 1  AND salon.IsSalonHoiQuan = 0
        //                         AND flow.ServiceId=53
        //                         GROUP BY a.Staff_Hairdresser_Id
        //                    )

        //                    --select * from TempAVGTimeCat end
        //                    select a.IDNhanVien,s.Fullname as TenNV, skillLever.Name as BacKyNang, salon.Name as TenSalon,
        //                        MAX(ROUND(CAST((ISNULL(e.TotalPointSCSC,0)) as float) / CAST((case when ISNULL(a.TotalBill,0) = 0 then 1 else a.TotalBill end)   as float) ,2)) as PointSCSC_TB,
        //                        MAX(ROUND(CAST((ISNULL(t.AVGTimeCat,0)) as float) / CAST((case when ISNULL(a.TotalBill,0) = 0 then 1 else a.TotalBill end)  as float) ,2)) as TimeCatTB
        //                       from TempTotalBill as a
        //                       left join TempTotalPointSCSC as e on a.IDNhanVien = e.IDNhanVien
        //                       left join TempAVGTimeCat as t on t.IDNhanVien=a.IDNhanVien
        //                       left join Staff as s on a.IDNhanVien = s.Id
        //                       left join Tbl_SkillLevel as skillLever on s.SkillLevel = skillLever.Id
        //                       inner join Tbl_Salon as salon on s.SalonId = salon.Id
        //                       --where a.IDNhanVien = 597
        //                       GROUP BY a.IDNhanVien, s.Fullname , skillLever.Name,salon.Name
        //                      end ";
        //            var list = new List<cls_SCSC_BindHairStyle_Vesion1>();

        //            if (order == 1)
        //            {
        //                list = db.Database.SqlQuery<cls_SCSC_BindHairStyle_Vesion1>(sql).OrderByDescending(w => w.PointSCSC_TB).ToList();
        //            }
        //            else
        //            {
        //                list = db.Database.SqlQuery<cls_SCSC_BindHairStyle_Vesion1>(sql).OrderBy(w => w.TimeCatTB).ToList();
        //            }
        //            return new JavaScriptSerializer().Serialize(list);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stylistId"></param>
        /// <param name="salonId"></param>
        /// <param name="timeFrom"></param>
        /// <param name="timeTo"></param>
        /// <returns></returns>
        private List<cls_SCSCUonOnlyStaff> GetDataOnlyStaff(int stylistId, int salonId, DateTime timeFrom, DateTime timeTo)
        {
            try
            {
                var list = new List<cls_SCSCUonOnlyStaff>();
                var domain = Libraries.AppConstants.URL_API_REPORTS;
                var api = @"/api/scsc-statistic/salon-only-staff?stylistId=" + stylistId  + "&salonId=" + salonId + "&fromDate=" + string.Format("{0:yyyy/MM/dd}",timeFrom) + "&toDate=" + string.Format("{0:yyyy/MM/dd}",timeTo);
                var response = new HttpClient().GetAsync(domain + api).Result;
                if (response.IsSuccessStatusCode)
                {
                    list = response.Content.ReadAsAsync<List<cls_SCSCUonOnlyStaff>>().Result;
                }
                else
                {
                    throw new Exception(response.Content.ReadAsStringAsync().Result);
                }
                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="timeFrom"></param>
        /// <param name="timeTo"></param>
        /// <returns></returns>
        private List<cls_SCSCUonAllSalon> GetDataAllSalon(DateTime timeFrom, DateTime timeTo)
        {
            try
            {
                var list = new List<cls_SCSCUonAllSalon>();
                var domain = Libraries.AppConstants.URL_API_REPORTS;
                var api = @"/api/scsc-statistic/salon-all-salon?fromDate=" + string.Format("{0:yyyy/MM/dd}",timeFrom) + "&toDate=" + string.Format("{0:yyyy/MM/dd}",timeTo);
                var response = new HttpClient().GetAsync(domain + api).Result;
                if (response.IsSuccessStatusCode)
                {
                    list = response.Content.ReadAsAsync<List<cls_SCSCUonAllSalon>>().Result;
                }
                else
                {
                    throw new Exception(response.Content.ReadAsStringAsync().Result);
                }
                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool CheckLimitTime()
        {
            try
            {
                var timeNow = DateTime.Now.TimeOfDay;
                var timeTo = new TimeSpan(11, 00, 00);
                var timeFrom = new TimeSpan(21, 00, 00);

                string perName = Session["User_Permission"].ToString();
                string[] arrayPermision = perName.Split(',');
                if (arrayPermision.Length == 0)
                {
                    arrayPermision = new[] { perName };
                }
                string checkAdmin = Array.Find(arrayPermision, s => s.Equals("ADMIN"));
                string checkRoot = Array.Find(arrayPermision, s => s.Equals("root"));
                if (timeNow >= timeFrom || timeNow <= timeTo || checkAdmin != null || checkRoot != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public class cls_SCSCUonOnlyStaff
        {
            public int TotalBillUon { get; set; }
            public string FullName { get; set; }
            public int? Stylist_ID { get; set; }
            public int? TotalBill { get; set; }
            public double? Bill_NotImg { get; set; }
            public double? Percent_BillError { get; set; }
            public double? Percent_BillImg_MoLech { get; set; }
            public int? TotalBill_ChuaDanhGia { get; set; }
            public double? Percent_ErrorKCS { get; set; }
            public double? PointSCSC_TB { get; set; }
            public int? Stylist_ID2 { get; set; }
            public int ErrorShapeId { get; set; }
            public int ErrorConnectiveId { get; set; }
            public int ErrorSharpNessId { get; set; }
            public int ErrorComplatetionId { get; set; }
            public double? BillNotImgUon { get; set; }
            public double? BillNotImgUonMoLech { get; set; }
            public double BillUonChamLoi { get; set; }
            public double? TotalDiemUonTB { get; set; }
            public double? Percent_ErrorKCSUon { get; set; }
            public int ErrorHairTip { get; set; }
            public int ErrorHairRoots { get; set; }
            public int ErrorHairWaves { get; set; }

            public int? TotalBillNotImg { get; set; }
            public int? TotalErrorSCSC { get; set; }
            public int? TongBillDanhGiaAnhMo { get; set; }
            public int? TotalBillDanhGiaNotImageMoLech { get; set; }
            public int? TotalPointSCSC { get; set; }
            public int? TongBillNotAnhUon { get; set; }
            public int? TongBillAnhUon { get; set; }
            public int? TongBillAnhMoLech { get; set; }
            public int? TongBillUonChamLoi { get; set; }
            public int? TotalPointSCSCCurling { get; set; }
            public int? TotalMoLechSCSCUon { get; set; }
            public int? TotalErrorSCSCUon { get; set; }


        }
        public class cls_SCSCUonAllSalon
        {
            public int TotalBillUon { get; set; }
            public string ShortName { get; set; }
            public int? SalonId { get; set; }
            public int? TotalBill { get; set; }
            public double? Percent_Bill_NotImg { get; set; }
            public int? BillChuaDanhGia { get; set; }
            public double? Percent_BillError { get; set; }
            public double? Percent_BillImg_MoLech { get; set; }
            public double? PointSCSC_TB { get; set; }
            public double? Percent_ErrorKCS { get; set; }
            public int ErrorShapeId { get; set; }
            public int ErrorConnective { get; set; }
            public int ErrorSharpNess { get; set; }
            public int ErrorComplatetion { get; set; }
            public double? BillNotImgUon { get; set; }
            public double? BillNotImgUonMoLech { get; set; }
            public double BillUonChamLoi { get; set; }
            public double? TotalDiemUonTB { get; set; }
            public double? Percent_ErrorKCSUon { get; set; }
            public int ErrorHairTip { get; set; }
            public int ErrorHairRoots { get; set; }
            public int ErrorHairWaves { get; set; }

            public int? TotalBillNotImage { get; set; }
            public int? TotalBill_DanhGiaNotMolech { get; set; }
            public int? TotalBill_DanhGiaImgMoLech { get; set; }
            public int? TotalErrorSCSC { get; set; }
            public int? TotalSCSC { get; set; }
            public int? TongBillNotAnhUon { get; set; }
            public int? TongBillAnhUon { get; set; }
            public int? TongBillAnhMoLech { get; set; }
            public int? TongBillUonChamLoi { get; set; }
            public int? TotalPointSCSCCurling { get; set; }
            public int? TotalMoLechSCSCUon { get; set; }
            public int? TotalErrorSCSCUon { get; set; }

        }
        //public partial class cls_SCSC_BindHairStyle_Vesion1
        //{
        //    public int IDNhanVien { get; set; }
        //    public string TenNV { get; set; }
        //    public string BacKyNang { get; set; }
        //    public string TenSalon { get; set; }
        //    public Nullable<double> PointSCSC_TB { get; set; }
        //    public Nullable<double> TimeCatTB { get; set; }
        //}
    }
}