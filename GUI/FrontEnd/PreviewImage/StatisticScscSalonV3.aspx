﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TemplateMaster/SiteMasterVersion1.Master" CodeBehind="StatisticScscSalonV3.aspx.cs" Inherits="_30shine.GUI.FrontEnd.PreviewImage.StatisticScscSalonV3" %>

<asp:Content ID="CustomerAdd" ContentPlaceHolderID="CtMain" runat="server">
    
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel ID="ContentWrap" runat="server" ClientIDMode="Static">
        <style>
            .skill-table {
                display: table;
                width: 100%;
            }

                .skill-table .item-row {
                    display: table-row;
                    border-bottom: 1px solid #ddd;
                }

                .skill-table .item-cell {
                    display: table-cell;
                    border: 1px solid #ddd;
                    padding: 5px;
                }

                    .skill-table .item-cell > span {
                        float: left;
                        width: 100%;
                        text-align: left;
                    }

                .skill-table .item-row .item-cell .checkbox {
                    float: left;
                    padding-top: 0 !important;
                    padding-bottom: 0 !important;
                    margin-top: 6px !important;
                    margin-bottom: 0px !important;
                    margin-right: 9px;
                    text-align: left;
                    background: #ddd;
                    padding: 2px 10px !important;
                }

                    .skill-table .item-row .item-cell .checkbox input[type='checkbox'] {
                        margin-left: 0 !important;
                        margin-right: 3px !important;
                    }

                .skill-table .item-cell-order {
                    width: 30px !important;
                }


            .modal {
                z-index: 9998;
            }

            .be-report .row-filter {
                z-index: 0;
            }

            @media(min-width: 768px) {
                .modal-content, .modal-dialog {
                    width: 1000px !important;
                    margin: auto;
                }
            }

            table.dataTable {
                border-collapse: collapse !important;
            }

            .sub-menu ul.ul-sub-menu li.active a,
            .sub-menu ul.ul-sub-menu li:hover a {
                color: green;
            }

            .sub-menu ul.ul-sub-menu li#report:hover a,
            .sub-menu ul.ul-sub-menu li#report:active a {
                color: green !important;
            }

            .form-control {
                height: 31px !important;
                width: 150px !important;
            }

            .search-content {
                height: 31px;
            }

            /*table thead {
                background: rgb(221, 221, 221);
            }

            table.table-listing thead tr th, table.table-listing tbody tr td {
                border: 1px solid black !important;
            }*/
        </style>
        <div class="container-fluid sub-menu form-group">
            <div class="row">
                <div class="col-md-12 pt-3 pb-2">
                    <ul>
                        <li>Báo cáo scsc theo salon</li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <div class="sub-menu-boder"></div>
                </div>
            </div>
        </div>
        <form runat="server">
            <div class="container-fluid">
                <div class="row">
                    <div class="form-group col-auto">
                        <strong><i class="fa fa-filter"></i>Lọc kết quả &nbsp;&#187; </strong>
                    </div>
                    <div class="col-auto form-group">
                        <asp:TextBox CssClass="datetime-picker form-control float-left w-auto" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                        <i class="fa fa-arrow-circle-right float-left mt-2 mr-2 ml-2" style="color: #D0D6D8;"></i>
                        <asp:TextBox CssClass="datetime-picker form-control float-left w-auto" ID="TxtDateTimeTo" placeholder="Đến ngày"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                    </div>
                    <div class="form-group col-auto">
                        <asp:DropDownList ID="ddlSalon" data-name="salonId" ClientIDMode="Static" CssClass="select form-control" runat="server" OnSelectedIndexChanged="Salon_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>
                    <div class="form-group col-auto">
                        <asp:DropDownList ID="ddlStylist" data-name="stylistId" ClientIDMode="Static" CssClass="select form-control" runat="server"></asp:DropDownList>
                    </div>
                    <div class="form-group col-auto">
                        <asp:Panel ID="ViewData" CssClass="form-group btn-viewdata text-center" Text="Xem dữ liệu"
                            ClientIDMode="Static" onclick="excPaging(1)" runat="server">
                            Xem dữ liệu
                        </asp:Panel>
                    </div>
                    <%--<div class="form-group col-auto">
                        <asp:Panel ID="Panel1" CssClass="form-group btn-viewdata text-center details1" Text="Xem dữ liệu"
                            ClientIDMode="Static" runat="server">
                            SCSC
                        </asp:Panel>
                    </div>--%>
                    <div class="col-12 form-group">
                        <div class="sub-menu-boder-low"></div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <asp:UpdatePanel ID="UPTotal" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="row">
                            <div class="col-12">
                                <strong class="st-head"><i class="fas fa-file-alt mr-1"></i>Thống kê SCSC</strong>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <table class="table table-bordered selectable" id="tblAllSalon">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th >Tổng bill</th>
                                            <th>% Bill không ảnh</th>
                                            <th>% Bill mờ lệch</th>
                                            <th>Lỗi Shape</th>
                                            <th>Lỗi Connective</th>
                                            <th>Lỗi SharpNess</th>
                                            <th>Lỗi Completion</th>
                                            <th>% Tổng lỗi SCSC</th>
                                            <th>Điểm SCSC TB</th>
                                            <th>Tổng bill uốn</th>
                                            <th>% Bill không ảnh Uốn</th>
                                            <th>% Bill mờ lệch Uốn</th>
                                            <th>Lỗi Ngọn tóc</th>
                                            <th>Lỗi Chân tóc</th>
                                            <th>Lỗi Sóng tóc</th>
                                            <th>%Tổng lỗi uốn</th>
                                            <th>Điểm TB Uốn</th>
                                            <th>Bill chưa đánh giá</th>
                                            <th>% Lỗi KCS</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="rptAllSalon" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td><b><%# Eval("ShortName") %></b></td>
                                                    <td class="td-SoLanOK"><%#Eval("TotalBill") %> </td>
                                                    <td class="td-HinhKhoiChuaChuan"><%#Eval("Percent_Bill_NotImg") %>%</td>
                                                    <td><%# Eval("Percent_BillImg_MoLech") %>%</td>
                                                    <td class="td-HinhKhoiChuaLienKet"><%#Eval("ErrorShapeId") %></td>
                                                    <td class="td-NgonTocQuaDay"><%#Eval("ErrorSharpNess") %></td>
                                                    <td class="td-CaoMaiGayLoi"><%#Eval("ErrorConnective") %></td>
                                                    <td class="td-VuotSapLoi"><%#Eval("ErrorComplatetion") %></td>
                                                    <td><%# Eval("Percent_BillError") %>%</td>
                                                    <td><%# Eval("PointSCSC_TB") %></td>
                                                    <td><%# Eval("TotalBillUon") %></td>
                                                    <td><%# Eval("BillNotImgUon") %>%</td>
                                                    <td><%# Eval("BillNotImgUonMoLech") %>%</td>
                                                    <td><%# Eval("ErrorHairTip") %></td>
                                                    <td><%# Eval("ErrorHairRoots") %></td>
                                                    <td><%# Eval("ErrorHairWaves") %></td>
                                                    <td><%# Eval("Percent_ErrorKCSUon") %>%</td>
                                                    <td><%# Eval("TotalDiemUonTB") %></td>
                                                    <td class="td-DuongCatChuaNet"><%#Eval("BillChuaDanhGia") %></td>
                                                    <td><%# Eval("Percent_ErrorKCS") %>%</td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <asp:Repeater ID="rptOnlySalon" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td>
                                                        <b><%# Eval("Fullname") %></b></td>
                                                    <td class="td-SoLanOK"><%#Eval("TotalBill") %> </td>
                                                    <td class="td-HinhKhoiChuaChuan"><%#Eval("Bill_NotImg") %>%</td>
                                                    <td><%# Eval("Percent_BillImg_MoLech") %>%</td>
                                                    <td class="td-HinhKhoiChuaLienKet"><%#Eval("ErrorShapeId") %></td>
                                                    <td class="td-NgonTocQuaDay"><%#Eval("ErrorConnectiveId") %></td>
                                                    <td class="td-CaoMaiGayLoi"><%#Eval("ErrorSharpNessId") %></td>
                                                    <td class="td-VuotSapLoi"><%#Eval("ErrorComplatetionId") %></td>
                                                    <td><%#Eval("Percent_BillError") %>%</td>
                                                    <td><%# Eval("PointSCSC_TB") %></td>
                                                    <td><%# Eval("TotalBillUon") %></td>
                                                    <td><%# Eval("BillNotImgUon") %>%</td>
                                                    <td><%# Eval("BillNotImgUonMoLech") %>%</td>
                                                    <td><%# Eval("ErrorHairTip") %></td>
                                                    <td><%# Eval("ErrorHairRoots") %></td>
                                                    <td><%# Eval("ErrorHairWaves") %></td>
                                                    <td><%# Eval("Percent_ErrorKCSUon") %>%</td>
                                                    <td><%# Eval("TotalDiemUonTB") %></td>
                                                    <td class="td-DuongCatChuaNet"><%#Eval("TotalBill_ChuaDanhGia") %></td>
                                                    <td><%# Eval("Percent_ErrorKCS") %>%</td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-6">
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
            </div>
            <!-- END Listing -->
            <!-- Modal View SCSC theo từng kiểu tóc của từng nhân viên  -->
           <%-- <div id="systemModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="filter-item" style="margin-left: 20px; padding: 10px">
                         Stylist: <span id="Spanname" style="margin-left: 45px; font-size: 15px;"></span>
                            <div class="datepicker-wp" style="margin-top: 10px">
                                <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o" style="padding: 5px"></i>Thời gian</strong>
                                <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TextBox1" placeholder="Từ ngày" Style="height: 34px; width: 150px!important; border: 1px solid #ddd; padding: 10px"
                                    ClientIDMode="Static" runat="server"></asp:TextBox>
                                <strong class="st-head" style="margin-left: 10px; margin-right: 10px;">
                                    <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                                </strong>
                                <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TextBox2" placeholder="Đến ngày" Style="height: 34px; width: 150px!important; border: 1px solid #ddd; padding: 10px"
                                    ClientIDMode="Static" runat="server"></asp:TextBox>
                                <button type="button" class="btn btn-default confirm" id="Confirm_Ratting" data-name="" onclick="ConfirmRatting($(this))">Tìm kiếm</button>
                            </div>
                        </div>
                        <div class="modal-header">
                            <strong class="st-head"><i class="fa fa-file-text" style="padding: 5px"></i>Thống kê SCSC</strong>
                        </div>
                        <div class="modal-body">
                            <div class=" tab-content">
                                <table class="skill-table display" id="totalService">
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" id="closeModal">Đóng</button>
                        </div>
                    </div>
                </div>
            </div>--%>

            <!-- Modal View SCSC theo từng kiểu tóc của từng nhân viên 2  -->
          <%--  <div id="systemModal1" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="filter-item" style="margin-left: 20px; padding: 10px">
                            <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o" style="padding: 5px"></i>Thời gian</strong>
                            <div class="datepicker-wp" style="margin-top: 10px">
                                <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TextBox3" placeholder="Từ ngày" Style="height: 34px; width: 150px!important; border: 1px solid #ddd; padding: 10px"
                                    ClientIDMode="Static" runat="server"></asp:TextBox>
                                <strong class="st-head" style="margin-left: 10px; margin-right: 10px;">
                                    <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                                </strong>
                                <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TextBox4" placeholder="Đến ngày" Style="height: 34px; width: 150px!important; border: 1px solid #ddd; padding: 10px"
                                    ClientIDMode="Static" runat="server"></asp:TextBox>

                                <asp:DropDownList ID="ddlHairStyle" runat="server" ClientIDMode="Static" Style="height: 34px; width: 190px!important; border: 1px solid #ddd;"></asp:DropDownList>
                                <asp:DropDownList ID="salon" runat="server" ClientIDMode="Static" Style="height: 34px; width: 190px!important; border: 1px solid #ddd;"></asp:DropDownList>
                                <asp:DropDownList ID="DropDownList1" runat="server" ClientIDMode="Static" Style="height: 34px; width: 150px!important; border: 1px solid #ddd;" onchange="ClickOrder($(this))">
                                    <asp:ListItem Text="Điểm SCSC TB" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Tốc độ cắt" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                                <button type="button" class="btn btn-default confirm" id="Confirm" onclick="clickConfirm($(this))">Tìm kiếm</button>
                            </div>
                        </div>
                        <div class="modal-header">
                            <strong class="st-head"><i class="fa fa-file-text" style="padding: 5px"></i>Thống kê SCSC</strong>
                        </div>
                        <div class="modal-body">
                            <div class=" tab-content">
                                <table class="skill-table display" id="totalBill">
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" id="closeModal1">Đóng</button>
                        </div>
                    </div>
                </div>
            </div>--%>
        </form>
        <script type="text/javascript">
            var table = null;
            
            jQuery(document).ready(function () {

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        //$input.val($input.val().split(' ')[0]);
                    }
                });
                $('.select').select2();
            });
            //$("#subMenu .li-listing1").addClass("active");
            //var staffID;
            //var timeFrom;
            //var timeTo;
            //$(document).on("click", "#closeModal", function () {
            //    $("#systemModal").modal("hide");
            //    $("table tbody tr").css("background-color", "white");
            //    $("#TextBox1").val("");
            //    $("#TextBox2").val("");
            //})

            //$(document).on("click", ".details", function () {
            //    $("#systemModal").modal();
            //    staffID = $(this).attr("staffID");
            //    var NameStylist = $(this).attr("data-name");
            //    $('#Spanname').html(NameStylist);
            //})
            //Click confirmRatting 
            //function ConfirmRatting(This) {
            //    //debugger;
            //    var name = This.find("td").attr("data-name");
            //    timeFrom = $("#TextBox1").val();
            //    timeTo = $("#TextBox2").val();
            //    if (timeFrom != "" && timeTo != "") {
            //        $.ajax({
            //            type: "POST",
            //            data: '{timeFrom:"' + timeFrom + '", timeTo:"' + timeTo + '", staffID:' + staffID + '}',
            //            contentType: "application/json; charset=utf-8",
            //            datatype: "JSON",
            //            url: "/GUI/FrontEnd/PreviewImage/StatisticScscSalonV3.aspx/BindHairStyle",
            //            success: function (data) {
            //                var jsonData = $.parseJSON(data.d);
            //                var itemRows = '  <thead>' +
            //                    '<tr class="item-row item-row-head">' +
            //                    '<th class="item-cell item-cell-skill"><span>Kiểu tóc</span></th>' +
            //                    '<th class="item-cell item-cell-skill"><span>Số lần cắt</span></th>' +
            //                    '<th class="item-cell item-cell-skill"><span>Lỗi Shape</span></th>' +
            //                    '<th class="item-cell item-cell-skill"><span>Lỗi Connective</span></th>' +
            //                    '<th class="item-cell item-cell-skill"><span>Lỗi SharpNess</span></th>' +
            //                    '<th class="item-cell item-cell-skill"><span>Lỗi Completion</span></th>' +
            //                    '<th class="item-cell item-cell-skill"><span>Điểm SCSC TB</span></th>' +
            //                    '<th class="item-cell item-cell-skill"><span>% Lỗi KCS</span></th>' +
            //                    '<th class="item-cell item-cell-skill"><span>% Thiếu ảnh</span></th>' +
            //                    '</tr>' +
            //                    '</thead>' +
            //                    '<tbody>';
            //                //for (var i = 0; i < jsonData.lenght; i++) {
            //                $.each(jsonData, function (i, v) {
            //                    itemRows += '<tr>' +
            //                        '<td class="item-cell item-cell-skill">' + v.NameHair + '</td>' +
            //                        '<td class="item-cell item-cell-skill">' + v.TotalHairStyle + '</td>' +
            //                        '<td class="item-cell item-cell-skill">' + v.TotalBillErrorShape + '</td>' +
            //                        '<td class="item-cell item-cell-skill">' + v.TotalBillErrorConnective + '</td>' +
            //                        '<td class="item-cell item-cell-skill">' + v.TotalBillErrorSharpNess + '</td>' +
            //                        '<td class="item-cell item-cell-skill">' + v.TotalBillErrorCompletion + '</td>' +
            //                        '<td class="item-cell item-cell-skill">' + v.PointSCSC_TB + '</td>' +
            //                        '<td class="item-cell item-cell-skill">' + v.Percent_KCS + '</td>' +
            //                        '<td class="item-cell item-cell-skill">' + v.Total_NotImages + '</td>' +
            //                        '</tr>';
            //                })
            //                itemRows += '</tbody>';

            //                $("#systemModal .modal-body #totalService").html(itemRows);
            //            }
            //        });
            //    }
            //    else {
            //        alert("Bạn phải chọn ngày tháng!");
            //    }
            //}
            //$("#totalBill").DataTable({
            //    "bSort": false,
            //    'aoColumns': [
            //        { bSearchable: false, bSortable: false }
            //    ],
            //    "pageLength": 10,
            //    "language": {
            //        "lengthMenu": " Bản ghi / trang _MENU_",
            //        "zeroRecords": "Không tồn tại bản ghi",
            //        "info": "Hiển thị trang _PAGE_ của _PAGES_",
            //        "paginate": {
            //            "first": "Đầu",
            //            "last": "Cuối",
            //            "next": "+",
            //            "previous": "-"
            //        },
            //    },
            //    "bLengthChange": false,
            //    "bFilter": false,
            //});

            //$("#subMenu .li-listing1").addClass("active");
            //var salonID;
            //var hairStyleID;
            //var timeFrom;
            //var timeTo;
            //var order;
            //$(document).on("click", "#closeModal1", function () {
            //    $("#systemModal1").modal("hide");
            //    $("table tbody tr").css("background-color", "white");
            //    $("#TextBox3").val("");
            //    $("#TextBox4").val("");
            //})

            //$(document).on("click", ".details1", function () {
            //    $("#systemModal1").modal();
            //    $(this).closest("tr").css("background-color", "green");
            //})

            // click Confirm SCSC
            //function clickConfirm(This) {
            //    timeFrom = $("#TextBox3").val();
            //    timeTo = $("#TextBox4").val();
            //    var ddlsalon = $("[Id*=salon]");
            //    var ddlhair = $("[Id*=ddlHairStyle]");
            //    var ddlorder = $("[Id*=DropDownList1]");
            //    salonID = ddlsalon.find("option:selected").val();
            //    hairStyleID = ddlhair.find("option:selected").val();
            //    order = ddlorder.find("option:selected").val();
            //    $.ajax({
            //        type: "POST",
            //        data: '{timeFrom:"' + timeFrom + '", timeTo:"' + timeTo + '", salonID:' + salonID + ', hairStyleID:' + hairStyleID + ', order:"' + order + '" }',
            //        contentType: "application/json; charset=utf-8",
            //        datatype: "JSON",
            //        url: "/GUI/FrontEnd/PreviewImage/StatisticScscSalonV3.aspx/BindHairStyle_V1",
            //        success: function (data) {
            //            var jsonData = $.parseJSON(data.d);
            //            var itemRows = '  <thead>' +
            //                '<tr class="item-row item-row-head">' +
            //                '<th class="item-cell item-cell-skill"><span>STT</span></th>' +
            //                '<th class="item-cell item-cell-skill"><span>ID</span></th>' +
            //                '<th class="item-cell item-cell-skill"><span>Tên nhân viên</span></th>' +
            //                '<th class="item-cell item-cell-skill"><span>Bậc</span></th>' +
            //                '<th class="item-cell item-cell-skill"><span>Salon</span></th>' +
            //                '<th class="item-cell item-cell-skill"><span>Điểm SCSC TB</span></th>' +
            //                '<th class="item-cell item-cell-skill"><span>Tốc độ cắt</span></th>' +
            //                '</tr>' +
            //                '</thead>' +
            //                '<tbody>';
            //            //for (var i = 0; i < jsonData.lenght; i++) {
            //            $.each(jsonData, function (i, v) {
            //                itemRows += '<tr>' +
            //                    '<td class="item-cell item-cell-skill">' + (i + 1) + '</td>' +
            //                    '<td class="item-cell item-cell-skill">' + v.IDNhanVien + '</td>' +
            //                    '<td class="item-cell item-cell-skill">' + v.TenNV + '</td>' +
            //                    '<td class="item-cell item-cell-skill">' + v.BacKyNang + '</td>' +
            //                    '<td class="item-cell item-cell-skill">' + v.TenSalon + '</td>' +
            //                    '<td class="item-cell item-cell-skill">' + v.PointSCSC_TB + '</td>' +
            //                    '<td class="item-cell item-cell-skill">' + v.TimeCatTB + '</td>' +
            //                    '</tr>';
            //            })
            //            itemRows += '</tbody>';
            //            $("#systemModal1 .modal-body #totalBill").html(itemRows);
            //        }
            //    });
            //}
            ///// Click sắp xếp theo SCSC TB, Tốc độ cắt
            //function ClickOrder(This) {
            //    clickConfirm();
            //}
            function excPaging(page) {
                $("#HDF_Page").val(page);
                $("#BtnFakeUP").click();
                startLoading();
            }
             // ConfigTableListing
            function ConfigTableListing() {
                table = $('#tblAllSalon').DataTable(
                    {
                        paging: false,
                        searching: false,
                        info: false,
                        fixedHeader: true,
                        ordering: false,
                        fixedColumns: {
                            leftColumns: 1
                        },
                        scrollY: "80vh",
                        scrollX: true,
                        scrollCollapse: true,
                    });
            }
           
        </script>

    </asp:Panel>
</asp:Content>
