﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Globalization;
using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.Interface;
using System.Configuration;

namespace _30shine.GUI.FrontEnd.PreviewImage
{
    public partial class Statistic_SCSC_V2_Stylist : System.Web.UI.Page
    {
        protected string Permission = "";
        protected bool Perm_ShowSalon = false;
        private bool Perm_Access = false;
        public CultureInfo culture = new CultureInfo("vi-VN");
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        private bool Perm_ViewAllData = false;
        /// <summary>
        /// Check permission
        /// </summary>
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var msgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", msgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = DateTime.Now.ToString("dd/MM/yyyy");
                //Bind_Salon();
                Library.Function.bindSalon(new List<DropDownList>() { ddlSalon }, Perm_ViewAllData);
                Bind_Staff();
            }
        }

        protected void Salon_SelectedIndexChanged(object sender, EventArgs e)
        {
            Bind_Staff();
        }

        /// <summary>
        /// Bind Staff
        /// </summary>
        private void Bind_Staff()
        {
            using (var db = new Solution_30shineEntities())
            {
                var salon = Convert.ToInt32(ddlSalon.SelectedValue);
                var staff = new List<Staff>();
                var whereSalon = "";
                var sql = "";

                if (salon > 0)
                {
                    whereSalon = " and SalonId = " + salon;
                }
                sql = @"select * from Staff 
                        where IsDelete != 1 and Active = 1  and Type = 1
                        and (isAccountLogin != 1 or isAccountLogin is null)" + whereSalon;
                staff = db.Staffs.SqlQuery(sql).ToList();

                var first = new Staff();
                first.Id = 0;
                first.Fullname = "Chọn nhân viên";
                staff.Insert(0, first);

                ddlStylist.DataTextField = "Fullname";
                ddlStylist.DataValueField = "Id";
                ddlStylist.DataSource = staff;
                ddlStylist.DataBind();
            }
        }

        /// <summary>
        /// Bind data to rpt
        /// </summary>
        private void BindOnlySalon()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    if (TxtDateTimeFrom.Text != "")
                    {
                        DateTime fromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                        DateTime toDate;
                        if (TxtDateTimeTo.Text != "")
                        {
                            toDate = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                        }
                        else
                        {
                            toDate = fromDate.AddDays(1);
                        }
                        int salonId = Convert.ToInt32(ddlSalon.SelectedValue);
                        int stylistId = Convert.ToInt32(ddlStylist.SelectedValue);
                        //var list = db.SCSC_BindStaffWhereNhanVien_Store_V2(_FromDate, _ToDate, SalonId, StylistId).ToList();
                        //var item = new SCSC_BindStaffWhereNhanVien_Store_V2_Result();
                        var list = GetDataOnlyStaff(stylistId, salonId, fromDate, toDate);
                        var item = new cls_SCSCUonOnlyStaff();
                        var countList = list.Count;
                        item.FullName = "Tổng";
                        item.TotalBill = list.Sum(a => a.TotalBill);
                        item.Bill_NotImg = list.Sum(a => a.TotalBill ?? 0) != 0
                            ? Math.Round(
                                ((Double)list.Sum(a => a.TotalBillNotImg ?? 0) /
                                 (Double)list.Sum(a => a.TotalBill ?? 0) * 100), 2)
                            : 0;
                        item.TotalBill_ChuaDanhGia = list.Sum(a => a.TotalBill_ChuaDanhGia);
                        item.Percent_BillError = Math.Round(
                            ((Double)list.Sum(r => r.TotalErrorSCSC ?? 0) -
                             (Double)list.Sum(r => r.TongBillDanhGiaAnhMo ?? 0)) /
                            (Double)list.Sum(r => r.TotalBill ?? 0) *
                            100, 2);

                        item.Percent_BillImg_MoLech = list.Sum(a => a.TotalBill ?? 0) != 0
                            ? Math.Round(
                                (Double)list.Sum(r => r.TongBillDanhGiaAnhMo ?? 0) /
                                (Double)list.Sum(r => r.TotalBill ?? 0) * 100, 2)
                            : 0;
                        item.PointSCSC_TB =
                            (list.Sum(r => r.TotalBillDanhGiaNotImageMoLech ?? 0) +
                             list.Sum(r => r.TongBillDanhGiaAnhMo ?? 0)) == 0
                                ? 0
                                : Math.Round(
                                    (Double)list.Sum(r => r.TotalPointSCSC ?? 0) /
                                    ((Double)list.Sum(r => r.TotalBillDanhGiaNotImageMoLech ?? 0) +
                                     (Double)list.Sum(r => r.TongBillDanhGiaAnhMo ?? 0)), 2);
                        item.ErrorShapeId = list.Sum(a => a.ErrorShapeId);
                        item.ErrorSharpNessId = list.Sum(a => a.ErrorSharpNessId);
                        item.ErrorConnectiveId = list.Sum(a => a.ErrorConnectiveId);
                        item.ErrorComplatetionId = list.Sum(a => a.ErrorComplatetionId);
                        item.Percent_ErrorKCS = list.Sum(a => a.TotalBill ?? 0) != 0
                            ? Math.Round(
                                (Double)list.Sum(a => a.TotalErrorSCSCUon ?? 0) /
                                (Double)list.Sum(r => r.TotalBill ?? 0) * 100, 2)
                            : 0;
                        item.BillNotImgUon = list.Sum(a => a.TotalBillUon) != 0
                            ? Math.Round(
                                (Double)list.Sum(a => a.TongBillNotAnhUon ?? 0) /
                                (Double)list.Sum(r => r.TotalBillUon) * 100, 2)
                            : 0;
                        item.BillNotImgUonMoLech = list.Sum(a => a.TotalBillUon) != 0
                            ? Math.Round(
                                (Double)list.Sum(a => a.TongBillAnhMoLech ?? 0) /
                                (Double)list.Sum(r => r.TotalBillUon) * 100, 2)
                            : 0;
                        item.BillUonChamLoi =
                            (list.Sum(r => r.TongBillNotAnhUon ?? 0) +
                             list.Sum(r => r.TongBillAnhUon ?? 0)) == 0
                                ? 0
                                : Math.Round(
                                    (Double)list.Sum(r => r.TongBillUonChamLoi ?? 0) /
                                    ((Double)list.Sum(r => r.TongBillNotAnhUon ?? 0) +
                                     (Double)list.Sum(r => r.TongBillAnhUon ?? 0)) * 100, 2);

                        item.TotalDiemUonTB = list.Sum(a => a.TongBillAnhUon ?? 0) == 0
                            ? 0
                            : Math.Round(
                                (Double)list.Sum(a => a.TotalPointSCSCCurling ?? 0) /
                                (Double)list.Sum(r => r.TongBillAnhUon ?? 0), 2);

                        item.TotalBillUon = list.Sum(a => a.TotalBillUon);

                        item.Percent_ErrorKCSUon = list.Sum(a => a.TotalBillUon) != 0
                            ? Math.Round(
                                ((Double)list.Sum(r => r.TongBillUonChamLoi ?? 0) -
                                (Double)list.Sum(r => r.TongBillAnhMoLech ?? 0)) / (Double)list.Sum(r => r.TotalBillUon) *
                                100, 2)
                            : 0;
                        item.ErrorHairTip = list.Sum(a => a.ErrorHairTip);
                        item.ErrorHairRoots = list.Sum(a => a.ErrorHairRoots);
                        item.ErrorHairWaves = list.Sum(a => a.ErrorHairWaves);
                        list.Insert(0, item);
                        rptOnlySalon.DataSource = list;
                        rptOnlySalon.DataBind();
                    }
                    else
                    {
                        Helpers.UIHelpers.TriggerJsMsgSystem_v1(this, "BẠN CHƯA CHỌN NGÀY", "warning", 30000);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Bind data all salon
        /// </summary>
        private void BindAllSalon()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    if (TxtDateTimeFrom.Text != "")
                    {
                        DateTime _FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                        DateTime _ToDate;
                        if (TxtDateTimeTo.Text != "")
                        {
                            _ToDate = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                        }
                        else
                        {
                            _ToDate = _FromDate.AddDays(1);
                        }
                        //var list = db.SCSC_BindStaffWhere_AllNhanVien_Store_V2(_FromDate, _ToDate).ToList();
                        //var item = new SCSC_BindStaffWhere_AllNhanVien_Store_V2_Result();
                        var list = GetDataAllSalon(_FromDate, _ToDate);
                        var item = new cls_SCSCUonAllSalon();
                        var countList = list.Count;
                        item.ShortName = "Tổng";
                        item.TotalBill = list.Sum(a => a.TotalBill);
                        item.Percent_Bill_NotImg = list.Sum(a => a.TotalBill ?? 0) != 0
                            ? Math.Round(
                                ((Double)list.Sum(a => a.TotalBillNotImage ?? 0) /
                                 (Double)list.Sum(a => a.TotalBill ?? 0) * 100), 2)
                            : 0;
                        item.Percent_BillImg_MoLech = list.Sum(a => a.TotalBill ?? 0) != 0
                            ? Math.Round(
                                (Double)list.Sum(r => r.TotalBill_DanhGiaImgMoLech ?? 0) /
                                (Double)list.Sum(r => r.TotalBill ?? 0) * 100, 2)
                            : 0;
                        item.ErrorShapeId = list.Sum(a => a.ErrorShapeId);
                        item.ErrorSharpNess = list.Sum(a => a.ErrorSharpNess);
                        item.ErrorConnective = list.Sum(a => a.ErrorConnective);
                        item.ErrorComplatetion = list.Sum(a => a.ErrorComplatetion);
                        item.BillChuaDanhGia = list.Sum(a => a.BillChuaDanhGia);
                        item.Percent_BillError = Math.Round(
                            ((Double) list.Sum(r => r.TotalErrorSCSC ?? 0) -
                             (Double) list.Sum(r => r.TotalBill_DanhGiaImgMoLech ?? 0)) /
                            (Double) list.Sum(r => r.TotalBill ?? 0) *
                            100, 2);

                        item.Percent_ErrorKCS = list.Sum(a => a.TotalBill ?? 0) != 0
                            ? Math.Round(
                                (Double)list.Sum(a => a.TotalErrorSCSCUon ?? 0) /
                                (Double)list.Sum(r => r.TotalBill ?? 0) * 100, 2)
                            : 0;
                        item.PointSCSC_TB = (list.Sum(r => r.TotalBill_DanhGiaNotMolech ?? 0) +
                                             list.Sum(r => r.TotalBill_DanhGiaImgMoLech ?? 0)) == 0
                            ? 0
                            : Math.Round(
                                (Double)list.Sum(r => r.TotalSCSC ?? 0) /
                                ((Double)list.Sum(r => r.TotalBill_DanhGiaNotMolech ?? 0) +
                                 (Double)list.Sum(r => r.TotalBill_DanhGiaImgMoLech ?? 0)), 2);

                        item.BillNotImgUon = list.Sum(a => a.TotalBillUon) != 0
                            ? Math.Round(
                                (Double)list.Sum(a => a.TongBillNotAnhUon ?? 0) /
                                (Double)list.Sum(r => r.TotalBillUon) * 100, 2)
                            : 0;
                        item.BillNotImgUonMoLech = list.Sum(a => a.TotalBillUon) != 0
                            ? Math.Round(
                                (Double)list.Sum(a => a.TongBillAnhMoLech ?? 0) /
                                (Double)list.Sum(r => r.TotalBillUon) * 100, 2)
                            : 0;
                        item.BillUonChamLoi = (list.Sum(r => r.TongBillNotAnhUon ?? 0) +
                                               list.Sum(r => r.TongBillAnhUon ?? 0)) == 0
                            ? 0
                            : Math.Round(
                                (Double)list.Sum(r => r.TongBillUonChamLoi ?? 0) /
                                ((Double)list.Sum(r => r.TongBillNotAnhUon ?? 0) +
                                 (Double)list.Sum(r => r.TongBillAnhUon ?? 0)) * 100, 2);
                       
                        item.TotalDiemUonTB = list.Sum(a => a.TongBillAnhUon ?? 0) == 0
                            ? 0
                            : Math.Round(
                                (Double)list.Sum(a => a.TotalPointSCSCCurling ?? 0) /
                                (Double)list.Sum(r => r.TongBillAnhUon ?? 0), 2);
                        item.TotalBillUon = list.Sum(a => a.TotalBillUon);
                        item.Percent_ErrorKCSUon = list.Sum(a => a.TotalBillUon) != 0
                            ? Math.Round(
                                ((Double)list.Sum(r => r.TongBillUonChamLoi ?? 0) -
                                 (Double)list.Sum(r => r.TongBillAnhMoLech ?? 0)) / (Double)list.Sum(r => r.TotalBillUon) *
                                100, 2)
                            : 0;
                        item.ErrorHairTip = list.Sum(a => a.ErrorHairTip);
                        item.ErrorHairRoots = list.Sum(a => a.ErrorHairRoots);
                        item.ErrorHairWaves = list.Sum(a => a.ErrorHairWaves);
                        list.Insert(0, item);
                        rptAllSalon.DataSource = list;
                        rptAllSalon.DataBind();
                    }
                    else
                    {
                        Helpers.UIHelpers.TriggerJsMsgSystem_v1(this, "BẠN CHƯA CHỌN NGÀY", "warning", 30000);
                    }
                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            try
            {
                if (CheckLimitTime())
                {
                    int SalonId = Convert.ToInt32(ddlSalon.SelectedValue);
                    if (SalonId == 0)
                    {
                        BindAllSalon();
                        rptOnlySalon.DataSource = "";
                        rptOnlySalon.DataBind();
                    }
                    else
                    {
                        BindOnlySalon();
                        rptAllSalon.DataSource = "";
                        rptAllSalon.DataBind();
                    }
                }
                else
                {
                    Helpers.UIHelpers.TriggerJsMsgSystem_v1(this, "LƯU Ý: BẠN CHỈ ĐƯỢC XEM TỪ 21H - 8h30 HÔM SAU", "warning", 30000);
                }
            }
            catch (Exception ex)
            {
                Helpers.UIHelpers.TriggerJsMsgSystem_v1(this, "Đã xảy ra lỗi: " + ex.Message.Replace("'", ""), "warning", 15000);
            }
            RemoveLoading();
        }

        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        private List<cls_SCSCUonOnlyStaff> GetDataOnlyStaff(int stylistId, int salonId, DateTime timeFrom, DateTime timeTo)
        {
            try
            {
                var list = new List<cls_SCSCUonOnlyStaff>();
                string sql = $@"
                        DECLARE 
	                        @TimeFrom DATETIME ,
	                        @TimeTo DATETIME ,
	                        @SalonID INT ,
	                        @StylistID INT
	                        SET @TimeFrom = '{string.Format("{0:yyyy-MM-dd}", timeFrom)}'
	                        SET @TimeTo = '{string.Format("{0:yyyy-MM-dd}", timeTo)}'
	                        SET @SalonID = {salonId}
	                        SET @StylistID = {stylistId}
	                        BEGIN
	                           WITH 
		                        BillTem AS(
			                        SELECT bill.Id, bill.IsImages AS  Images,  bill.Staff_Hairdresser_Id
			                        FROM dbo.BillService AS bill 
			                        WHERE bill.IsDelete = 0 AND bill.Pending = 0 AND bill.CreatedDate BETWEEN @TimeFrom AND @TimeTo AND LEN(bill.ServiceIds ) > 0 
		                        ),
		                        Temp AS (
			                        SELECT bill.Id, bill.Images , 
				                        bill.Staff_Hairdresser_Id  AS StylistID, s.SalonId,  c.ImageError, c.Shape_ID, c.ConnectTive_ID, 
				                        c.SharpNess_ID, c.ComPlatetion_ID, c.PointError, c.HairTip_ID, c.HairRoot_ID, c.HairWaves_ID, c.TotalPointSCSCCurling,
				                        (CASE WHEN fl.ServiceId = 16 THEN 1 ELSE 0 END ) AS IsUon ,c.ImageErrorCurling
				                        ,(CASE WHEN LEN(imgd.ImageBefore) > 0 AND LEN(imgd.ImageAfter) > 0 THEN 1  ELSE 0 END)  AS isImgUon
			                        FROM BillTem AS bill 
			                        LEFT JOIN dbo.ImageData AS imgd ON bill.Id = imgd.OBJId AND imgd.Slugkey = 'image_bill_curling'
			                        LEFT JOIN dbo.FlowService AS fl ON bill.Id = fl.BillId AND fl.ServiceId = 16 AND fl.IsDelete = 0
			                        INNER JOIN dbo.Staff AS s ON s.Id = bill.Staff_Hairdresser_Id
                                    INNER JOIN dbo.Tbl_Salon AS salon ON salon.Id = s.SalonId
			                        LEFT JOIN dbo.SCSC_CheckError AS c ON c.BillService_ID = bill.Id  AND c.IsDelete = 0 
			                        WHERE  salon.IsSalonHoiQuan = 0 AND ((bill.Staff_Hairdresser_Id = @StylistID) OR (@StylistID = 0)) AND 	
                                        ((s.SalonId = @SalonID) OR (@SalonID = 0)) AND s.Active = 1 AND s.IsDelete = 0
		                        ) ,
		                        -- Tổng bill
		                        TempTotalBill AS (
			                        SELECT COUNT(Id) AS TotalBill, StylistID,SalonId 
			                        FROM Temp 
			                        GROUP BY SalonId , StylistID
		                        ),
		                        -- Tổng Bill không ảnh
		                        TempTotalBillNotImg AS (
			                        SELECT COUNT(Id) AS TotalBillNotImg, StylistID,SalonId
			                        FROM Temp
			                        WHERE Images = 0 OR Temp.Images IS NULL
			                        GROUP BY SalonId , StylistID
		                        ),
		                        -- Tổng Bill mờ lệch
		                        TempTotalBillImgMoLech AS (
			                        SELECT COUNT(Id) AS TotalBillImgMoLech, StylistID,SalonId
			                        FROM Temp
			                        WHERE ImageError = 1
			                        GROUP BY SalonId , StylistID
		                        ),
		                        -- Tổng bill đã đánh giá
		                        TempTotalBillDanhGiaNoteImageMoLech AS (
			                        SELECT COUNT(Id) AS TotalBillDanhGiaNotImageMoLech , StylistID,SalonId
			                        FROM Temp
			                        WHERE ImageError = 0
			                        GROUP BY SalonId , StylistID
		                        ),
		                        -- Tổng Bill lỗi ShapeId
		                        TempTotalBillErrorShapeId AS (
			                        SELECT COUNT(Temp.Id) as TotalBillErrorShapeId, Temp.StylistID, Temp.SalonId 
			                        FROM Temp
			                        INNER JOIN SCSC_Category AS cate ON cate.ID_SCSC_Cate = Temp.Shape_ID
			                        WHERE cate.SCSC_Cate_Point = 0
			                        GROUP BY SalonId , StylistID
		                        ),
		                        -- Tổng bill lỗi Connective
		                        TempTotalBillErrorConnectiveId AS (
			                        SELECT COUNT(Temp.Id) as TotalBillErrorConnectiveId, Temp.StylistID, Temp.SalonId  
			                        FROM Temp
			                        INNER JOIN SCSC_Category AS cate ON cate.ID_SCSC_Cate = Temp.ConnectTive_ID
			                        WHERE cate.SCSC_Cate_Point = 0
			                        GROUP BY SalonId , StylistID
		                        ),
		                        -- Tổng bill lỗi SharpNess
		                        TempTotalBillErrorSharpNessId AS (
			                        SELECT COUNT(Temp.Id) as TotalBillErrorSharpNessId, Temp.StylistID, Temp.SalonId  
			                        FROM Temp
			                        INNER JOIN SCSC_Category AS cate ON cate.ID_SCSC_Cate = Temp.SharpNess_ID
			                        WHERE cate.SCSC_Cate_Point = 0
			                        GROUP BY SalonId , StylistID
		                        ),
		                        -- Tổng bill lỗi Complatetion 
		                        TempTotalBillErrorComplatetionId AS (
			                        SELECT COUNT(Temp.Id) AS TotalBillErrorComplatetionId, Temp.StylistID, Temp.SalonId   
			                        FROM Temp
			                        INNER JOIN SCSC_Category AS cate ON cate.ID_SCSC_Cate = Temp.ComPlatetion_ID
			                        WHERE cate.SCSC_Cate_Point = 0
			                        GROUP BY SalonId , StylistID
		                        ),
		                        -- Tổng điểm chấm..
		                        TempTotalPointSCSC AS (
			                        SELECT SUM(ISNULL(Temp.PointError,0)) AS TotalPointSCSC, Temp.StylistID, Temp.SalonId  
			                        FROM Temp
			                        GROUP BY SalonId , StylistID
		                        ),
		                        -- % Lỗi KCS = Tổng lỗi chấm SCSC ( colum PoinError = 0  ) + bill không ảnh / tổng Bill
		                        -- Tổng ảnh chấm lỗi
		                        TempTotalErrorSCSC AS (
			                        SELECT COUNT(Temp.Id) AS TotalErrorSCSC, Temp.StylistID, Temp.SalonId  
			                        FROM Temp
			                        WHERE PointError = 0
			                        GROUP BY SalonId , StylistID
		                        ),
		                        -- Tổng ảnh đánh giá mờ
		                        TempDanhGiaAnhMo AS (
			                        SELECT COUNT(Temp.Id) AS TongBillDanhGiaAnhMo, Temp.StylistID, Temp.SalonId 
			                        FROM Temp
			                        WHERE ImageError = 1
			                        GROUP BY SalonId , StylistID
		                        ),
		                        -- Tổng bill không có ảnh uốn
		                        TempTotalNotImageCurling AS (
			                        SELECT COUNT(Temp.Id) AS TongBillNotAnhUon, temp.StylistID, temp.SalonId
			                        FROM Temp
			                        WHERE Temp.IsUon = 1 AND Temp.isImgUon = 0 
			                        GROUP BY temp.StylistID, temp.SalonId
		                        ),
                                -- Tổng bill có ảnh uốn
								TempTotalImageCurling AS (
									SELECT COUNT(Temp.Id) AS TongBillAnhUon, temp.StylistID, temp.SalonId
			                        FROM Temp
			                        WHERE Temp.IsUon = 1 AND Temp.isImgUon = 1 
			                        GROUP BY temp.StylistID, temp.SalonId
								),
								-- Tong bill uon
								TempTotalBillCurling AS (
									SELECT COUNT(Temp.Id) AS TongBillUon, temp.StylistID, temp.SalonId
			                        FROM Temp
			                        WHERE Temp.IsUon = 1 
			                        GROUP BY temp.StylistID, temp.SalonId
								),
		                        -- Tổng bill có ảnh uốn. nhưng bị chấm mờ lệch
		                        TempImageCurlingImageError AS (
			                        SELECT COUNT(Temp.Id) AS TongBillAnhMoLech, temp.StylistID, temp.SalonId
			                        FROM Temp
			                        WHERE Temp.IsUon = 1 AND Temp.ImageErrorCurling = 1 AND Temp.isImgUon = 1
			                        GROUP BY temp.StylistID, temp.SalonId
		                        ),
		                        -- Tổng bill chấm lỗi N (lỗi ngọn tóc)
		                        TempImageCurlingErrorHairTip AS (
			                        SELECT COUNT(Temp.Id) AS TongBillUonChamLoiNgonToc, temp.StylistID, temp.SalonId
			                        FROM Temp
			                        INNER JOIN dbo.SCSC_Category AS cate ON cate.ID_SCSC_Cate = Temp.HairTip_ID
			                        WHERE Temp.IsUon = 1 AND cate.SCSC_Cate_Point = 0 AND Temp.isImgUon = 1
			                        GROUP BY temp.StylistID, temp.SalonId
		                        ),
		                        -- Tổng bill chấm lỗi C (chân tóc)
		                        TempImageCurlingErrorHairRoots AS (
			                        SELECT COUNT(Temp.Id) AS TongBillUonChamLoiChanToc, temp.StylistID, temp.SalonId
			                        FROM Temp
			                        INNER JOIN dbo.SCSC_Category AS cate ON cate.ID_SCSC_Cate = Temp.HairRoot_ID
			                        WHERE Temp.IsUon = 1 AND  cate.SCSC_Cate_Point = 0  AND Temp.isImgUon = 1
			                        GROUP BY temp.StylistID, temp.SalonId
		                        ),
		                        -- Tổng bill chấm lỗi S (lỗi sóng tóc)
		                        TempImageCurlingErrorHairWaves AS (
			                        SELECT COUNT(Temp.Id) AS TongBillUonChamLoiSongToc, temp.StylistID, temp.SalonId
			                        FROM Temp
			                        INNER JOIN dbo.SCSC_Category AS cate ON cate.ID_SCSC_Cate = Temp.HairWaves_ID
			                        WHERE Temp.IsUon = 1 AND  cate.SCSC_Cate_Point = 0 AND Temp.isImgUon = 1
			                        GROUP BY temp.StylistID, temp.SalonId
		                        ),
		                        -- tổng bill uốn chấm lỗi 
		                        TempTotalImageCurlingError AS (
			                        SELECT COUNT(Temp.Id) AS TongBillUonChamLoi, temp.StylistID, temp.SalonId
			                        FROM Temp
			                        WHERE Temp.IsUon = 1 AND Temp.TotalPointSCSCCurling = 0 AND Temp.isImgUon =1
			                        GROUP BY temp.StylistID, temp.SalonId
		                        ),
		                        -- Tổng điểm uốn 
		                        TempTotalPointImageCurling AS (
			                        SELECT SUM(Temp.TotalPointSCSCCurling) AS TotalPointSCSCCurling, temp.StylistID, temp.SalonId
			                        FROM Temp
			                        WHERE Temp.IsUon = 1 AND Temp.isImgUon = 1
			                        GROUP BY temp.StylistID, temp.SalonId
		                        ),
		                        -- Tổng Bill không ảnh uốn hoặc scsc
		                        TempTotalMoLechUonSCSC AS  (
			                        SELECT COUNT(Temp.Id) AS TotalMoLechSCSCUon, temp.StylistID, temp.SalonId
			                        FROM Temp 
			                        WHERE Temp.ImageError = 1 OR (Temp.IsUon = 1 AND Temp.ImageErrorCurling = 1)
			                        GROUP BY temp.StylistID, temp.SalonId
		                        ),
		                        -- Tổng bill chấm lỗi uốn hoặc scsc
		                        TempTotalErrorSCSCUon AS (
			                        SELECT COUNT(Temp.Id) AS TotalErrorSCSCUon, temp.StylistID, temp.SalonId
			                        FROM Temp
			                        WHERE  Temp.PointError = 0 OR (Temp.IsUon = 1 AND Temp.TotalPointSCSCCurling = 0)
			                        GROUP BY temp.StylistID, temp.SalonId
		                        ),
		                        -- 
		                        --Tổng Bill
		                        --Bill không ảnh + mờ/lệch
		                        --Bill chưa đánh giá = Tổng Bill - Bill không ảnh - Bill đã đánh giá
		                        TempTBL1 AS (
			                        SELECT a.StylistID, a.TotalBill,b.TotalBillNotImg,g.TotalErrorSCSC,am.TongBillDanhGiaAnhMo,e.TotalBillDanhGiaNotImageMoLech,k.TotalPointSCSC,
											aa.TongBillNotAnhUon,ak.TongBillAnhUon,ac.TongBillAnhMoLech,ag.TongBillUonChamLoi,ah.TotalPointSCSCCurling,ba.TotalMoLechSCSCUon,bc.TongBillUon,
											bb.TotalErrorSCSCUon,
				                        ROUND(CAST((ISNULL(b.TotalBillNotImg,0)) AS FLOAT)/a.TotalBill * 100, 2) AS Bill_NotImg,
				                        ROUND((CAST(ISNULL(g.TotalErrorSCSC,0) AS FLOAT) - CAST(ISNULL(am.TongBillDanhGiaAnhMo,0) AS FLOAT))/a.TotalBill *100,2) AS Percent_BillError,
				                        ROUND(CAST((ISNULL(am.TongBillDanhGiaAnhMo,0)) AS FLOAT)/a.TotalBill * 100, 2) AS Percent_BillImg_MoLech,
				                        (a.TotalBill - ISNULL(b.TotalBillNotImg,0) - ISNULL(e.TotalBillDanhGiaNotImageMoLech,0) - ISNULL(am.TongBillDanhGiaAnhMo,0) ) AS TotalBill_ChuaDanhGia,
                                        ROUND(CAST(ISNULL(bb.TotalErrorSCSCUon,0) AS FLOAT)/a.TotalBill * 100, 2) AS Percent_ErrorKCS,
				                        ROUND((CAST((ISNULL(k.TotalPointSCSC,0)) AS FLOAT))
					                          /CAST((CASE WHEN (ISNULL(e.TotalBillDanhGiaNotImageMoLech,0) + ISNULL(am.TongBillDanhGiaAnhMo,0)) = 0 
							                        THEN 1 ELSE (ISNULL(e.TotalBillDanhGiaNotImageMoLech,0) + ISNULL(am.TongBillDanhGiaAnhMo,0)) END ) AS FLOAT ) ,2) AS PointSCSC_TB,
				                        ISNULL(m.TotalBillErrorShapeId,0) AS ErrorShapeId,
				                        ISNULL(l.TotalBillErrorConnectiveId,0) AS ErrorConnectiveId,
				                        ISNULL(o.TotalBillErrorSharpNessId,0) AS ErrorSharpNessId,
				                        ISNULL(p.TotalBillErrorComplatetionId,0) AS ErrorComplatetionId,
				                        -- Uốn
				                        ROUND(CAST((ISNULL(aa.TongBillNotAnhUon,0)) AS FLOAT)/bc.TongBillUon * 100, 2) AS BillNotImgUon,
				                        ROUND(CAST((ISNULL(ac.TongBillAnhMoLech,0)) AS FLOAT)/bc.TongBillUon * 100, 2) AS BillNotImgUonMoLech,
				                         ROUND( CAST(ISNULL(ag.TongBillUonChamLoi,0) AS FLOAT)/ 
										(CASE WHEN ISNULL(bc.TongBillUon,0) = 0 THEN 1  ELSE ISNULL(bc.TongBillUon,0) END) * 100,2 ) AS BillUonChamLoi,
				                        ROUND( CAST(ISNULL(ah.TotalPointSCSCCurling,0) AS FLOAT)/ CAST(ISNULL(ak.TongBillAnhUon,1) AS FLOAT),2) AS TotalDiemUonTB,
                                        (ISNULL(aa.TongBillNotAnhUon,0) + ISNULL(ak.TongBillAnhUon,0)) AS TotalBillUon,
				                        ROUND((CAST(ISNULL(ag.TongBillUonChamLoi,0) AS FLOAT) - CAST( ISNULL(ac.TongBillAnhMoLech,0) AS FLOAT))/bc.TongBillUon * 100,2) AS Percent_ErrorKCSUon,
				                        ISNULL(ad.TongBillUonChamLoiNgonToc,0) AS ErrorHairTip,
				                        ISNULL(ae.TongBillUonChamLoiChanToc,0) AS ErrorHairRoots,
				                        ISNULL(af.TongBillUonChamLoiSongToc,0) AS ErrorHairWaves
			                        FROM TempTotalBill AS a
			                        LEFT JOIN TempTotalBillNotImg AS b ON b.StylistID = a.StylistID
			                        LEFT JOIN TempTotalBillDanhGiaNoteImageMoLech AS e ON e.StylistID = a.StylistID
			                        LEFT JOIN TempTotalErrorSCSC AS g ON g.StylistID = a.StylistID
			                        LEFT JOIN TempDanhGiaAnhMo AS am ON am.StylistID = a.StylistID
			                        LEFT JOIN TempTotalPointSCSC AS k ON k.StylistID = a.StylistID
			                        LEFT JOIN TempTotalBillErrorShapeId AS m ON a.StylistID = m.StylistID
			                        LEFT JOIN TempTotalBillErrorConnectiveId AS l ON a.StylistID = l.StylistID
			                        LEFT JOIN TempTotalBillErrorSharpNessId AS o ON a.StylistID = o.StylistID
			                        LEFT JOIN TempTotalBillErrorComplatetionId AS p ON a.StylistID = p.StylistID
			                        --join uốn
			                        LEFT JOIN TempTotalNotImageCurling AS aa ON aa.StylistID = a.StylistID
			                        LEFT JOIN TempImageCurlingImageError AS ac ON ac.StylistID = a.StylistID
			                        LEFT JOIN TempImageCurlingErrorHairTip AS ad ON ad.StylistID = a.StylistID
			                        LEFT JOIN TempImageCurlingErrorHairRoots AS ae ON ae.StylistID = a.StylistID
			                        LEFT JOIN TempImageCurlingErrorHairWaves AS af ON af.StylistID = a.StylistID
			                        LEFT JOIN TempTotalImageCurlingError AS ag ON ag.StylistID = a.StylistID
			                        LEFT JOIN TempTotalPointImageCurling AS ah ON ah.StylistID = a.StylistID
                                    LEFT JOIN TempTotalImageCurling AS ak ON ak.StylistID = a.StylistID
			                        LEFT JOIN TempTotalMoLechUonSCSC AS ba ON ba.StylistID = a.StylistID
			                        LEFT JOIN TempTotalErrorSCSCUon AS bb ON bb.StylistID = a.StylistID
									LEFT JOIN TempTotalBillCurling AS bc ON bc.StylistID = a.StylistID
		                        )
		                        SELECT  b.Fullname AS FullName, a.* 
		                        FROM TempTBL1 AS a
		                        LEFT JOIN Staff AS b ON a.StylistID = b.Id
		                        ORDER BY a.StylistID DESC
	                        END";
                using (var db = new Solution_30shineEntities())
                {
                    // chuyen sang database bi 
                    db.Database.Connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLConnectionStringBI"].ConnectionString;
                    list = db.Database.SqlQuery<cls_SCSCUonOnlyStaff>(sql).ToList();
                }
                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private List<cls_SCSCUonAllSalon> GetDataAllSalon(DateTime timeFrom, DateTime timeTo)
        {
            try
            {
                var list = new List<cls_SCSCUonAllSalon>();
                string sql = $@"
                        DECLARE
                            @timeFrom DATETIME,
                            @timeTo DATETIME
                            SET @timeFrom = '{string.Format("{0:yyyy-MM-dd}", timeFrom)}'
                            SET @timeTo = '{string.Format("{0:yyyy-MM-dd}", timeTo)}'
                            BEGIN
	                            WITH TempBill AS (
		                            SELECT bill.Staff_Hairdresser_Id,bill.Id,
			                            bill.IsImages AS  Images
		                            FROM dbo.BillService AS bill
		                            WHERE bill.CreatedDate BETWEEN @timeFrom AND @timeTo AND  LEN(bill.ServiceIds) > 0 AND bill.IsDelete = 0 AND bill.Pending = 0
	                            ),
	                            Temp AS (
		                            SELECT bill.Id, bill.Images,bill.Staff_Hairdresser_Id  AS StylistID, s.SalonId,  
			                            c.ImageError, c.Shape_ID, c.ConnectTive_ID, 
				                            c.SharpNess_ID, c.ComPlatetion_ID, c.PointError, c.HairTip_ID, c.HairRoot_ID, c.HairWaves_ID, c.TotalPointSCSCCurling,
				                            (CASE WHEN fl.ServiceId = 16 THEN 1 ELSE 0 END ) AS IsUon ,c.ImageErrorCurling
				                            ,(CASE WHEN LEN(imgd.ImageBefore) > 0 AND LEN(imgd.ImageAfter) > 0 THEN 1  ELSE 0 END)  AS isImgUon
		                            FROM TempBill AS bill
		                            INNER JOIN dbo.Staff AS s ON s.Id = bill.Staff_Hairdresser_Id
		                            INNER JOIN dbo.Tbl_Salon AS salon ON salon.Id = s.SalonId
		                            LEFT JOIN dbo.FlowService AS fl ON fl.BillId = bill.Id AND fl.IsDelete = 0 AND fl.ServiceId = 16
		                            LEFT JOIN dbo.ImageData AS imgd ON imgd.OBJId = bill.Id AND imgd.Slugkey = 'image_bill_curling'
		                            LEFT JOIN dbo.SCSC_CheckError AS c ON c.BillService_ID = bill.Id AND c.IsDelete = 0
		                            WHERE salon.IsSalonHoiQuan = 0 AND salon.Publish = 1 AND salon.IsDelete = 0 AND
                                        s.Active = 1 AND s.IsDelete = 0
	                            ),
	                            TempTotalBill AS (
		                            SELECT Temp.SalonId,COUNT(Temp.Id) AS TotalBill
		                            FROM Temp
		                            GROUP BY Temp.SalonId
	                            ),
	                            TempTotalBillNotImage AS (
		                            SELECT temp.SalonId,COUNT(Temp.Id) AS TotalBillNotImage
		                            FROM Temp
		                            WHERE Temp.Images = 0 OR Temp.Images IS NULL
		                            GROUP BY Temp.SalonId
	                            ),
	                            TempTotalBill_DanhGiaImgMoLech AS (
		                            SELECT Temp.SalonId, COUNT(Temp.Id) AS TotalBill_DanhGiaImgMoLech
		                            FROM Temp
		                            WHERE Temp.ImageError = 1
		                            GROUP BY Temp.SalonId
	                            ),
	                            TempTotalShapeError AS (
		                            SELECT Temp.SalonId, COUNT(Temp.Id) AS TotalShapeError
		                            FROM Temp
		                            INNER JOIN dbo.SCSC_Category AS cate ON cate.ID_SCSC_Cate = temp.Shape_ID
		                            WHERE cate.SCSC_Cate_Point = 0
		                            GROUP BY Temp.SalonId
	                            ),
	                            TempTotalConnectiveError AS (
		                            SELECT Temp.SalonId, COUNT(Temp.Id) AS TotalConnectiveError
		                            FROM Temp
		                            INNER JOIN dbo.SCSC_Category AS cate ON cate.ID_SCSC_Cate = temp.ConnectTive_ID
		                            WHERE cate.SCSC_Cate_Point = 0
		                            GROUP BY Temp.SalonId
	                            ),
	                            TempTotalSharpNessError AS (
		                            SELECT Temp.SalonId, COUNT(Temp.Id) AS TotalSharpNessError 
		                            FROM Temp
		                            INNER JOIN dbo.SCSC_Category AS cate ON cate.ID_SCSC_Cate = temp.SharpNess_ID
		                            WHERE cate.SCSC_Cate_Point = 0
		                            GROUP BY Temp.SalonId
	                            ),
	                            TempTotalCompletionError AS (
		                            SELECT Temp.SalonId, COUNT(Temp.Id) AS TotalCompletionError 
		                            FROM Temp
		                            INNER JOIN dbo.SCSC_Category AS cate ON cate.ID_SCSC_Cate = Temp.ComPlatetion_ID
		                            WHERE cate.SCSC_Cate_Point = 0 
		                            GROUP BY Temp.SalonId
	                            ),
	                            -- Tổng lỗi SCSC
	                            TempTotalErrorSCSC AS (
		                            SELECT COUNT(Temp.Id) AS TotalErrorSCSC, Temp.SalonId
		                            FROM Temp
		                            WHERE Temp.PointError = 0
		                            GROUP BY Temp.SalonId
	                            ),
	                            -- Tổng điểm chấm SCSC
	                            TempTotalSCSC AS (
		                            SELECT SUM(Temp.PointError) AS TotalSCSC, Temp.SalonId
		                            FROM Temp
		                            GROUP BY Temp.SalonId
	                            ),
	                            -- tong bill loi khong tinh loi anh mo lech
	                            TempTotalBill_DanhGiaNotMolech AS (
		                            SELECT Temp.SalonId, COUNT(Temp.Id) as TotalBill_DanhGiaNotMolech
		                            FROM Temp
		                            WHERE Temp.ImageError = 0
		                            GROUP BY Temp.SalonId
	                            ),
	                            -- Tổng bill không có ảnh uốn
		                            TempTotalNotImageCurling AS (
			                            SELECT COUNT(Temp.Id) AS TongBillNotAnhUon,temp.SalonId
			                            FROM Temp
			                            WHERE Temp.IsUon = 1 AND Temp.isImgUon = 0 
			                            GROUP BY temp.SalonId
		                            ),
                                    -- Tổng bill có ảnh uốn
		                            TempTotalImageCurling AS (
			                            SELECT COUNT(Temp.Id) AS TongBillAnhUon,temp.SalonId
			                            FROM Temp
			                            WHERE Temp.IsUon = 1 AND Temp.isImgUon = 1
			                            GROUP BY temp.SalonId
		                            ),
									-- Tong bill uon
									TempTotalBillCurling AS (
										SELECT COUNT(Temp.Id) AS TongBillUon,temp.SalonId
										FROM Temp
										WHERE Temp.IsUon = 1 
										GROUP BY temp.SalonId
									),
		                            -- Tổng bill có ảnh uốn. nhưng bị chấm mờ lệch
		                            TempImageCurlingImageError AS (
			                            SELECT COUNT(Temp.Id) AS TongBillAnhMoLech, temp.SalonId
			                            FROM Temp
			                            WHERE Temp.IsUon = 1 AND Temp.ImageErrorCurling = 1 AND Temp.isImgUon = 1
			                            GROUP BY temp.SalonId
		                            ),
		                            -- Tổng bill chấm lỗi N (lỗi ngọn tóc)
		                            TempImageCurlingErrorHairTip AS (
			                            SELECT COUNT(Temp.Id) AS TongBillUonChamLoiNgonToc, temp.SalonId
			                            FROM Temp
			                            INNER JOIN dbo.SCSC_Category AS cate ON cate.ID_SCSC_Cate = Temp.HairTip_ID
			                            WHERE Temp.IsUon = 1 AND cate.SCSC_Cate_Point = 0 AND Temp.isImgUon = 1
			                            GROUP BY temp.SalonId
		                            ),
		                            -- Tổng bill chấm lỗi C (chân tóc)
		                            TempImageCurlingErrorHairRoots AS (
			                            SELECT COUNT(Temp.Id) AS TongBillUonChamLoiChanToc, temp.SalonId
			                            FROM Temp
			                            INNER JOIN dbo.SCSC_Category AS cate ON cate.ID_SCSC_Cate = Temp.HairRoot_ID
			                            WHERE Temp.IsUon = 1 AND  cate.SCSC_Cate_Point = 0  AND Temp.isImgUon = 1
			                            GROUP BY temp.SalonId
		                            ),
		                            -- Tổng bill chấm lỗi S (lỗi sóng tóc)
		                            TempImageCurlingErrorHairWaves AS (
			                            SELECT COUNT(Temp.Id) AS TongBillUonChamLoiSongToc, temp.SalonId
			                            FROM Temp
			                            INNER JOIN dbo.SCSC_Category AS cate ON cate.ID_SCSC_Cate = Temp.HairWaves_ID
			                            WHERE Temp.IsUon = 1 AND  cate.SCSC_Cate_Point = 0 AND Temp.isImgUon = 1
			                            GROUP BY temp.SalonId
		                            ),
		                            -- tổng bill uốn chấm lỗi 
		                            TempTotalImageCurlingError AS (
			                            SELECT COUNT(Temp.Id) AS TongBillUonChamLoi, temp.SalonId
			                            FROM Temp
			                            WHERE Temp.IsUon = 1 AND Temp.TotalPointSCSCCurling = 0 AND Temp.isImgUon =1
			                            GROUP BY temp.SalonId
		                            ),
		                            -- Tổng điểm uốn 
		                            TempTotalPointImageCurling AS (
			                            SELECT SUM(Temp.TotalPointSCSCCurling) AS TotalPointSCSCCurling, temp.SalonId
			                            FROM Temp
			                            WHERE Temp.IsUon = 1 AND Temp.isImgUon = 1
			                            GROUP BY temp.SalonId
		                            ),
		                            -- Tổng Bill không ảnh uốn hoặc scsc
		                            TempTotalMoLechUonSCSC AS  (
			                            SELECT COUNT(Temp.Id) AS TotalMoLechSCSCUon, temp.SalonId
			                            FROM Temp 
			                            WHERE Temp.ImageError = 1 OR (Temp.IsUon = 1 AND Temp.ImageErrorCurling = 1)
			                            GROUP BY temp.SalonId
		                            ),
		                            -- Tổng bill chấm lỗi uốn hoặc scsc
		                            TempTotalErrorSCSCUon AS (
			                            SELECT COUNT(Temp.Id) AS TotalErrorSCSCUon, temp.SalonId
			                            FROM Temp
			                            WHERE  Temp.PointError = 0 OR (Temp.IsUon = 1 AND Temp.TotalPointSCSCCurling = 0)
			                            GROUP BY temp.SalonId
		                            ),

	                            Temp_TBL1 AS (
		                            SELECT  a.SalonId, a.TotalBill,b.TotalBillNotImage,e.TotalBill_DanhGiaNotMolech,am.TotalBill_DanhGiaImgMoLech,c.TotalErrorSCSC,d.TotalSCSC,
											aa.TongBillNotAnhUon,ac.TongBillAnhMoLech,ag.TongBillUonChamLoi,ak.TongBillAnhUon,ba.TotalMoLechSCSCUon,bb.TotalErrorSCSCUon,ah.TotalPointSCSCCurling,

			                            ROUND(CAST((ISNULL(b.TotalBillNotImage,0)) AS FLOAT)/CAST(a.TotalBill AS FLOAT) * 100, 2) AS Percent_Bill_NotImg,
			                            (ISNULL(a.TotalBill,0)- ISNULL(b.TotalBillNotImage,0) - ISNULL(e.TotalBill_DanhGiaNotMolech,0) - ISNULL(am.TotalBill_DanhGiaImgMoLech,0)  ) as BillChuaDanhGia,
			                            ROUND((CAST(ISNULL(c.TotalErrorSCSC,0) AS FLOAT) - CAST(ISNULL(am.TotalBill_DanhGiaImgMoLech,0) AS FLOAT))/a.TotalBill *100,2) AS Percent_BillError,
			                            ROUND(CAST((ISNULL(am.TotalBill_DanhGiaImgMoLech,0)) AS FLOAT)/cast(a.TotalBill AS FLOAT ) * 100, 2) AS Percent_BillImg_MoLech,
			                            ROUND(  (CAST((ISNULL(d.TotalSCSC,0)) AS FLOAT)  )/  CAST((CASE WHEN (ISNULL(e.TotalBill_DanhGiaNotMolech,0) + ISNULL(am.TotalBill_DanhGiaImgMoLech,0)) = 0 THEN 1 ELSE (ISNULL(e.TotalBill_DanhGiaNotMolech,0) +ISNULL(am.TotalBill_DanhGiaImgMoLech,0)) END  ) AS FLOAT),2) AS PointSCSC_TB,
                                        ROUND(CAST(ISNULL(bb.TotalErrorSCSCUon,0) AS FLOAT)/a.TotalBill * 100, 2) AS Percent_ErrorKCS,

			                            ISNULL(ca.TotalShapeError,0) AS ErrorShapeId,
			                            ISNULL(cd.TotalConnectiveError,0) AS ErrorConnective,
			                            ISNULL(cc.TotalSharpNessError,0) AS ErrorSharpNess,
			                            ISNULL(cb.TotalCompletionError,0) AS ErrorComplatetion,
			                            -- uốn
			                            ROUND(CAST((ISNULL(aa.TongBillNotAnhUon,0)) AS FLOAT)/bc.TongBillUon * 100, 2) AS BillNotImgUon,
			                            ROUND(CAST((ISNULL(ac.TongBillAnhMoLech,0)) AS FLOAT)/bc.TongBillUon * 100, 2) AS BillNotImgUonMoLech,
			                             ROUND( CAST(ISNULL(ag.TongBillUonChamLoi,0) AS FLOAT)/ 
										(CASE WHEN CAST((ISNULL(bc.TongBillUon,0)) AS FLOAT) = 0 THEN 1  ELSE 
										CAST((ISNULL(bc.TongBillUon,0)) AS FLOAT) END) * 100,2 ) AS BillUonChamLoi,
			                            ROUND( CAST(ISNULL(ah.TotalPointSCSCCurling,0) AS FLOAT)/ CAST(ISNULL(ak.TongBillAnhUon,1) AS FLOAT),2) AS TotalDiemUonTB,
                                        (ISNULL(bc.TongBillUon,0)) AS TotalBillUon,
			                            ROUND((CAST(ISNULL(ag.TongBillUonChamLoi,0) AS FLOAT) - CAST( ISNULL(ac.TongBillAnhMoLech,0) AS FLOAT))/bc.TongBillUon * 100,2) AS Percent_ErrorKCSUon,
			                            ISNULL(ad.TongBillUonChamLoiNgonToc,0) AS ErrorHairTip,
			                            ISNULL(ae.TongBillUonChamLoiChanToc,0) AS ErrorHairRoots,
			                            ISNULL(af.TongBillUonChamLoiSongToc,0) AS ErrorHairWaves
		                            FROM TempTotalBill AS a
		                            LEFT JOIN TempTotalBillNotImage AS b ON a.SalonId=b.SalonId
		                            LEFT JOIN TempTotalErrorSCSC as c on a.SalonId=c.SalonId
		                            LEFT JOIN TempTotalSCSC as d on a.SalonId=d.SalonId
		                            LEFT JOIN TempTotalBill_DanhGiaNotMolech as e on a.SalonId=e.SalonId
		                            LEFT JOIN TempTotalBill_DanhGiaImgMoLech as am on a.SalonId=am.SalonId
		                            LEFT JOIN TempTotalShapeError as ca on ca.SalonId=a.SalonId
		                            LEFT JOIN TempTotalCompletionError as cb on cb.SalonId=a.SalonId
		                            LEFT JOIN TempTotalSharpNessError as cc on cc.SalonId=a.SalonId
		                            LEFT JOIN TempTotalConnectiveError as cd on cd.SalonId=a.SalonId
		                            --join uốn
		                            LEFT JOIN TempTotalNotImageCurling AS aa ON aa.SalonId = a.SalonId
		                            LEFT JOIN TempImageCurlingImageError AS ac ON ac.SalonId = a.SalonId
		                            LEFT JOIN TempImageCurlingErrorHairTip AS ad ON ad.SalonId = a.SalonId
		                            LEFT JOIN TempImageCurlingErrorHairRoots AS ae ON ae.SalonId = a.SalonId
		                            LEFT JOIN TempImageCurlingErrorHairWaves AS af ON af.SalonId = a.SalonId
		                            LEFT JOIN TempTotalImageCurlingError AS ag ON ag.SalonId = a.SalonId
		                            LEFT JOIN TempTotalPointImageCurling AS ah ON ah.SalonId = a.SalonId
                                    LEFT JOIN TempTotalImageCurling AS ak ON ak.SalonId = a.SalonId
		                            LEFT JOIN TempTotalMoLechUonSCSC AS ba ON ba.SalonId = a.SalonId
		                            LEFT JOIN TempTotalErrorSCSCUon AS bb ON bb.SalonId = a.SalonId
									LEFT JOIN TempTotalBillCurling AS bc ON bc.SalonId = a.SalonId
	                            )
	                            SELECT s.ShortName,a.*
	                            FROM Temp_TBL1 AS a
	                            INNER JOIN dbo.Tbl_Salon AS s ON s.Id = a.SalonId
                                ORDER BY s.[Order] ASC
                            END";
                using (var db = new Solution_30shineEntities())
                {
                    db.Database.Connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLConnectionStringBI"].ConnectionString;
                    list = db.Database.SqlQuery<cls_SCSCUonAllSalon>(sql).ToList();
                }
                return list;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool CheckLimitTime()
        {
            try
            {
                var timeNow = DateTime.Now.TimeOfDay;
                var timeTo = new TimeSpan(11, 00, 00);
                var timeFrom = new TimeSpan(21, 00, 00);

                string perName = Session["User_Permission"].ToString();
                string[] arrayPermision = perName.Split(',');
                if (arrayPermision.Length == 0)
                {
                    arrayPermision = new[] { perName };
                }
                string checkAdmin = Array.Find(arrayPermision, s => s.Equals("ADMIN"));
                string checkRoot = Array.Find(arrayPermision, s => s.Equals("root"));
                if (timeNow >= timeFrom || timeNow <= timeTo || checkAdmin != null || checkRoot != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public class cls_SCSCUonOnlyStaff
        {
            public int TotalBillUon { get; set; }
            public string FullName { get; set; }
            public int? Stylist_ID { get; set; }
            public int? TotalBill { get; set; }
            public double? Bill_NotImg { get; set; }
            public double? Percent_BillError { get; set; }
            public double? Percent_BillImg_MoLech { get; set; }
            public int? TotalBill_ChuaDanhGia { get; set; }
            public double? Percent_ErrorKCS { get; set; }
            public double? PointSCSC_TB { get; set; }
            public int? Stylist_ID2 { get; set; }
            public int ErrorShapeId { get; set; }
            public int ErrorConnectiveId { get; set; }
            public int ErrorSharpNessId { get; set; }
            public int ErrorComplatetionId { get; set; }
            public double? BillNotImgUon { get; set; }
            public double? BillNotImgUonMoLech { get; set; }
            public double BillUonChamLoi { get; set; }
            public double? TotalDiemUonTB { get; set; }
            public double? Percent_ErrorKCSUon { get; set; }
            public int ErrorHairTip { get; set; }
            public int ErrorHairRoots { get; set; }
            public int ErrorHairWaves { get; set; }

            public int? TotalBillNotImg { get; set; }
            public int? TotalErrorSCSC { get; set; }
            public int? TongBillDanhGiaAnhMo { get; set; }
            public int? TotalBillDanhGiaNotImageMoLech { get; set; }
            public int? TotalPointSCSC { get; set; }
            public int? TongBillNotAnhUon { get; set; }
            public int? TongBillAnhUon { get; set; }
            public int? TongBillAnhMoLech { get; set; }
            public int? TongBillUonChamLoi { get; set; }
            public int? TotalPointSCSCCurling { get; set; }
            public int? TotalMoLechSCSCUon { get; set; }
            public int? TotalErrorSCSCUon { get; set; }

        }
        public class cls_SCSCUonAllSalon
        {
            public int TotalBillUon { get; set; }
            public string ShortName { get; set; }
            public int? SalonId { get; set; }
            public int? TotalBill { get; set; }
            public double? Percent_Bill_NotImg { get; set; }
            public int? BillChuaDanhGia { get; set; }
            public double? Percent_BillError { get; set; }
            public double? Percent_BillImg_MoLech { get; set; }
            public double? PointSCSC_TB { get; set; }
            public double? Percent_ErrorKCS { get; set; }
            public int ErrorShapeId { get; set; }
            public int ErrorConnective { get; set; }
            public int ErrorSharpNess { get; set; }
            public int ErrorComplatetion { get; set; }
            public double? BillNotImgUon { get; set; }
            public double? BillNotImgUonMoLech { get; set; }
            public double BillUonChamLoi { get; set; }
            public double? TotalDiemUonTB { get; set; }
            public double? Percent_ErrorKCSUon { get; set; }
            public int ErrorHairTip { get; set; }
            public int ErrorHairRoots { get; set; }
            public int ErrorHairWaves { get; set; }

            public int? TotalBillNotImage { get; set; }
            public int? TotalBill_DanhGiaNotMolech { get; set; }
            public int? TotalBill_DanhGiaImgMoLech { get; set; }
            public int? TotalErrorSCSC { get; set; }
            public int? TotalSCSC { get; set; }
            public int? TongBillNotAnhUon { get; set; }
            public int? TongBillAnhUon { get; set; }
            public int? TongBillAnhMoLech { get; set; }
            public int? TongBillUonChamLoi { get; set; }
            public int? TotalPointSCSCCurling { get; set; }
            public int? TotalMoLechSCSCUon { get; set; }
            public int? TotalErrorSCSCUon { get; set; }

        }
    }
}