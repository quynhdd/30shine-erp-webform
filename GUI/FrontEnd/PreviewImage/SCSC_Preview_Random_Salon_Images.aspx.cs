﻿using _30shine.Helpers;
using _30shine.MODEL.CustomClass;
using _30shine.MODEL.ENTITY.EDMX;
using Project.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Migrations;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.Interface;
using System.Net.Http;
using System.Net.Http.Headers;
using _30shine.Helpers.Http;
using System.Threading.Tasks;
using Libraries;
using System.Text.RegularExpressions;

namespace _30shine.GUI.FrontEnd.PreviewImage
{
    public partial class SCSC_Preview_Random_Salon_Images : System.Web.UI.Page
    {
        private string PageID = "KCS_CHECK";
        protected string Permission = "";
        protected bool Perm_Access = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_Stylist = false;
        protected string listElement_Perm = string.Empty;
        protected int? TotalPointCate;
        protected int? TotalPointCurling;
        protected int? ShapMaxPoint;
        protected int? ConnectiveMaxPoint;
        protected int? SharpNessMaxPoint;
        protected int? CompletionMaxPoint;
        public static CultureInfo culture = new CultureInfo("vi-VN");
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            txtToDate.Value = DateTime.Now.ToString("dd/MM/yyyy");
            BindCategorySCSC();
            if (!IsPostBack)
            {
                Library.Function.bindSalonSpecial(new List<System.Web.UI.WebControls.DropDownList> { ddlSalon }, Perm_ViewAllData);
                GetListStaff();
            }
        }

        private void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        // get list  Staff where Salon
        private void GetListStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                int salonId = int.TryParse(ddlSalon.SelectedValue, out var integer) ? integer : 0;
                int[] arrSkillLevel = { 2, 6, 7, 8 };
                var listStaff = db.Staffs.Where(a => a.Active == 1 && a.IsDelete == 0 && (a.isAccountLogin == null || a.isAccountLogin == 0) && (a.isAppLogin == null || a.isAppLogin == 0) && !arrSkillLevel.Contains(a.SkillLevel ?? 0) && a.SalonId == salonId && a.Type == 1).ToList();
                var record = new Staff();
                record.Id = 0;
                record.Fullname = "Chọn nhân viên";
                listStaff.Insert(0, record);
                ddlStaff.DataTextField = "FullName";
                ddlStaff.DataValueField = "Id";
                ddlStaff.DataSource = listStaff;
                ddlStaff.DataBind();
            }
        }

        /// <summary>
        /// Bind Category SCSC
        /// </summary>
        protected void BindCategorySCSC()
        {
            using (var db = new Solution_30shineEntities())
            {
                int[] arrId = { 1, 2, 3, 4, 20, 21, 22 };
                var listCategory = db.SCSC_Category.Where(a => a.Publish == true && a.IsDelete == false && arrId.Contains(a.SCSC_Cate_IDCate ?? 0)).OrderBy(a => a.SCSC_Cate_Point).ToList();
                // hình khối
                var listShape = listCategory.Where(a => a.Publish == true && a.IsDelete == false && a.SCSC_Cate_IDCate == 1).OrderBy(a => a.SCSC_Cate_Point).ToList();
                rpt_Shape.DataSource = listShape;
                rpt_Shape.DataBind();
                // liên kết
                var listConnective = listCategory.Where(a => a.Publish == true && a.IsDelete == false && a.SCSC_Cate_IDCate == 2).OrderBy(a => a.SCSC_Cate_Point).ToList();
                rpt_Connective.DataSource = listConnective;
                rpt_Connective.DataBind();
                // độ sắc nét
                var listSharpness = listCategory.Where(a => a.Publish == true && a.IsDelete == false && a.SCSC_Cate_IDCate == 3).OrderBy(a => a.SCSC_Cate_Point).ToList();
                rptSharpness.DataSource = listSharpness;
                rptSharpness.DataBind();
                // độ hoàn chỉnh
                var listCompletion = listCategory.Where(a => a.Publish == true && a.IsDelete == false && a.SCSC_Cate_IDCate == 4).OrderBy(a => a.SCSC_Cate_Point).ToList();
                rpt_Completion.DataSource = listCompletion;
                rpt_Completion.DataBind();

                //Uốn
                // Ngọc tóc
                var listHairTips = listCategory.Where(a => a.Publish == true && a.IsDelete == false && a.SCSC_Cate_IDCate == 20).OrderBy(a => a.SCSC_Cate_Point).ToList();
                rptHairTip.DataSource = listHairTips;
                rptHairTip.DataBind();
                // Chân tóc
                var listHairRoot = listCategory.Where(a => a.Publish == true && a.IsDelete == false && a.SCSC_Cate_IDCate == 21).OrderBy(a => a.SCSC_Cate_Point).ToList();
                rptHairRoot.DataSource = listHairRoot;
                rptHairRoot.DataBind();
                // Sóng tóc
                var listHairWaves = listCategory.Where(a => a.Publish == true && a.IsDelete == false && a.SCSC_Cate_IDCate == 22).OrderBy(a => a.SCSC_Cate_Point).ToList();
                rptHairWave.DataSource = listHairWaves;
                rptHairWave.DataBind();
                //Tổng điểm SCSC
                TotalPointCate = (
                                    listShape[listShape.Count - 1].SCSC_Cate_Point +
                                    listConnective[listConnective.Count - 1].SCSC_Cate_Point +
                                    listSharpness[listSharpness.Count - 1].SCSC_Cate_Point +
                                    listCompletion[listCompletion.Count - 1].SCSC_Cate_Point
                                 ) ?? 0;
                //Tổng điểm KCS
                TotalPointCurling = (
                                        listHairTips[listHairTips.Count - 1].SCSC_Cate_Point +
                                        listHairRoot[listHairRoot.Count - 1].SCSC_Cate_Point +
                                        listHairWaves[listHairWaves.Count - 1].SCSC_Cate_Point
                                    ) ?? 0;
                ShapMaxPoint = listShape[listShape.Count - 1].SCSC_Cate_Point ?? 0;
                ConnectiveMaxPoint = listConnective[listConnective.Count - 1].SCSC_Cate_Point ?? 0;
                SharpNessMaxPoint = listSharpness[listSharpness.Count - 1].SCSC_Cate_Point ?? 0;
                CompletionMaxPoint = listCompletion[listCompletion.Count - 1].SCSC_Cate_Point ?? 0;
            }
        }


        protected void Salon_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetListStaff();
        }

        private void PassDataFromServerToClient()
        {
            List<ElementEnable> list = listElement();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jSon = serializer.Serialize(list);
            listElement_Perm = jSon;
        }

        protected List<ElementEnable> listElement()
        {
            List<ElementEnable> list = new List<ElementEnable>();
            list.Add(new ElementEnable() { ElementName = "btn-ok", Enable = Perm_Stylist, Type = "disable" });
            list.Add(new ElementEnable() { ElementName = "btn-cube", Enable = Perm_Stylist, Type = "disable" });
            list.Add(new ElementEnable() { ElementName = "btn-line", Enable = Perm_Stylist, Type = "disable" });
            list.Add(new ElementEnable() { ElementName = "btnAddCycle", Enable = Perm_Stylist, Type = "hidden" });
            list.Add(new ElementEnable() { ElementName = "btnUndoCycle", Enable = Perm_Stylist, Type = "hidden" });
            list.Add(new ElementEnable() { ElementName = "btnSaveError", Enable = Perm_Stylist, Type = "hidden" });
            list.Add(new ElementEnable() { ElementName = "ErrorNote", Enable = Perm_Stylist, Type = "disable" });
            list.Add(new ElementEnable() { ElementName = "btnReset", Enable = Perm_Stylist, Type = "hidden" });
            return list;
        }


        [WebMethod]
        public static string GetImageS3(string url)
        {

            var Client = new HttpClient();
            var URI = new Uri(url);
            Client.BaseAddress = URI;
            Client.DefaultRequestHeaders.Accept.Clear();
            Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = Client.GetAsync(URI.AbsolutePath);
            byte[] imageByte = response.Result.Content.ReadAsByteArrayAsync().Result;
            return Convert.ToBase64String(imageByte) ?? "";
        }

        #region Tính tỉ lệ stylist chụp đủ ảnh
        [WebMethod]
        public static string checkStylistFullImage(int stylistID, DateTime date)
        {
            //return date.ToString();
            try
            {
                Solution_30shineEntities db = new Solution_30shineEntities();
                //var bill = (from bs in db.BillServices where bs.Staff_HairMassage_Id == stylistID && bs.CreatedDate.)
                int bill = db.BillServices.Where(bs => bs.CreatedDate.Value.Day == date.Day
                        && bs.CreatedDate.Value.Month == date.Month && bs.CreatedDate.Value.Year == date.Year
                        && bs.Staff_Hairdresser_Id == stylistID
                        && bs.IsDelete != 1 && bs.Pending != 0
                        ).Count();
                int bill2 = db.BillServices.Where(bs => bs.CreatedDate.Value.Day == date.Day
                        && bs.CreatedDate.Value.Month == date.Month && bs.CreatedDate.Value.Year == date.Year
                        && bs.Staff_Hairdresser_Id == stylistID
                        && bs.IsDelete != 1 && bs.Pending != 0 && bs.Images != null
                        ).Count();
                if (bill2 != 0)
                {
                    float tile = bill / bill2;
                    if (tile >= 0.8)
                    {
                        return "Success";
                    }
                    else
                    {
                        return "Errors";
                    }
                }
                else return "Success";

            }
            catch (Exception ex)
            {

                return ex.ToString();
            }

        }
        #endregion
        #region Get bill theo salon
        [WebMethod]
        public static List<BillServiceView> GetRandomBillImage(string _Date, int _SalonId, int _StylistId, string _imageStatus)
        {
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                DateTime dateTime = Convert.ToDateTime(_Date, culture);
                string timeFrom = String.Format("{0:yyyy/MM/dd}", dateTime);
                string timeTo = String.Format("{0:yyyy/MM/dd}", dateTime.AddDays(1));
                var sql = $@"DECLARE 
                            @timeFrom DATETIME,
                            @timeTo DATETIME,
                            @salonId INT,
                            @stylistId INT
                            SET @timeFrom= '{timeFrom}'
                            SET @timeTo= '{timeTo}'
                            SET @salonId= {_SalonId}
                            SET @stylistId= {_StylistId}
				            SELECT a.Id,a.ServiceIds,a.Images,b.Fullname AS CustomerName, b.Phone AS CustomerPhone,
				            b.Id AS IdCustomer ,c.ID AS ID_SCSC,c.ImageError,c.NoteError,
				            c.Shape_ID,c.SharpNess_ID,c.ComPlatetion_ID,c.ConnectTive_ID, c.TotalPointSCSC,
							c.HairTip_ID,c.HairRoot_ID,c.HairWaves_ID,c.ImageErrorCurling, c.TotalPointSCSCCurling,
				            (CASE
					            WHEN
					            c.TotalPointSCSC 
					            IS NULL THEN -1 ELSE 1 END ) AS PointSCSC,
				            a.ImageChecked1,a.ImageChecked2,a.ImageChecked3,a.ImageChecked4,s.SkillLevel,
				            ISNULL(d.Id,0) AS ImgId, d.ImageBefore, d.ImageAfter, d.ImageCheckBefore, d.ImageCheckAfter
                            FROM BillService a 
                            JOIN Staff AS s ON a.Staff_Hairdresser_Id = s.Id
                            LEFT JOIN Customer b ON a.CustomerId = b.Id 
                            LEFT JOIN SCSC_CheckError AS c ON a.Id = c.BillService_ID 
				            LEFT JOIN dbo.ImageData AS d ON d.[OBJId] = a.Id AND d.Slugkey = 'image_bill_curling' AND d.IsDelete = 0
                            WHERE a.CreatedDate BETWEEN @timeFrom AND @timeTo
                            AND  a.SalonId = @salonId
                            AND ((a.Staff_Hairdresser_Id=@stylistId) OR (@stylistId=0))
                            AND a.IsDelete = 0 AND a.Pending = 0 
                            AND a.Images IS NOT NULL
                            AND s.SkillLevel NOT IN (2, 6, 7)
                            ORDER BY PointSCSC ASC, Id ASC";
                var list = db.Database.SqlQuery<custom_BillService>(sql).ToList();
                List<BillServiceView> lstBillView = new List<BillServiceView>();
                BillServiceView obj = new BillServiceView();
                string[] ListImg;
                foreach (var item in list)
                {
                    ListImg = item.Images.Split(',');
                    obj = new BillServiceView();
                    obj.Id = item.Id;
                    obj.BillDate = string.Format("{0:dd/MM/yyyy}", item.CreatedDate);
                    obj.CustomerPhone = item.CustomerPhone;
                    obj.CustomerName = item.CustomerName;
                    obj.CustomerId = item.IdCustomer;
                    obj.ServiceIds = item.ServiceIds;
                    obj.PointSCSC = item.PointSCSC;
                    obj.ImgId = item.ImgId;
                    obj.ImageCurlingBefore = (!String.IsNullOrEmpty(item.ImageCheckBefore) ? item.ImageCheckBefore : item.ImageBefore) ?? "";
                    obj.ImageCurlingAfter = (!String.IsNullOrEmpty(item.ImageCheckAfter) ? item.ImageCheckAfter : item.ImageAfter) ?? "";
                    obj.ImageStatusId = item.ImageStatusId ?? "";
                    switch (ListImg.Count())
                    {
                        case 4:
                            obj.Img1 = (!String.IsNullOrEmpty(item.ImageChecked1) ? item.ImageChecked1 : ListImg[0]) ?? "";
                            obj.Img2 = (!String.IsNullOrEmpty(item.ImageChecked2) ? item.ImageChecked2 : ListImg[1]) ?? "";
                            obj.Img3 = (!String.IsNullOrEmpty(item.ImageChecked3) ? item.ImageChecked3 : ListImg[2]) ?? "";
                            obj.Img4 = (!String.IsNullOrEmpty(item.ImageChecked4) ? item.ImageChecked4 : ListImg[3]) ?? "";
                            break;
                        case 3:
                            obj.Img1 = (!String.IsNullOrEmpty(item.ImageChecked1) ? item.ImageChecked1 : ListImg[0]) ?? "";
                            obj.Img2 = (!String.IsNullOrEmpty(item.ImageChecked2) ? item.ImageChecked2 : ListImg[1]) ?? "";
                            obj.Img3 = (!String.IsNullOrEmpty(item.ImageChecked3) ? item.ImageChecked3 : ListImg[2]) ?? "";
                            break;
                        case 2:
                            obj.Img1 = (!String.IsNullOrEmpty(item.ImageChecked1) ? item.ImageChecked1 : ListImg[0]) ?? "";
                            obj.Img2 = (!String.IsNullOrEmpty(item.ImageChecked2) ? item.ImageChecked2 : ListImg[1]) ?? "";
                            break;
                        case 1:
                            obj.Img1 = (!String.IsNullOrEmpty(item.ImageChecked1) ? item.ImageChecked1 : ListImg[0]) ?? "";
                            break;
                        default:
                            obj.Img1 = "";
                            obj.Img2 = "";
                            obj.Img3 = "";
                            obj.Img4 = "";
                            break;
                    }
                    obj.ErrorNote = item.NoteError;
                    obj.NoteByStylist = item.NoteByStylist;
                    lstBillView.Add(obj);
                }
                return lstBillView;
            }
        }
        #endregion

        [WebMethod]
        public static string updateStatus(int billID, string statusID)
        {
            var Message = new Library.Class.cls_message();
            var serialize = new JavaScriptSerializer();
            var error = 0;
            try
            {
                var db = new Solution_30shineEntities();
                var RECORD = db.BillServices.FirstOrDefault(w => w.Id == billID);
                if (RECORD != null)
                {
                    RECORD.ImageStatusId = statusID;
                    RECORD.ModifiedDate = DateTime.Now;
                    db.BillServices.AddOrUpdate(RECORD);
                    error += db.SaveChanges() > 0 ? 0 : 1;
                    if (error == 0)
                    {
                        Message.success = true;
                        Message.message = "Cập nhật thành công.";
                    }
                    else
                    {
                        Message.success = false;
                        Message.message = "Cập nhật thất bại.";
                    }
                }
                else
                {
                    error = 1;
                    Message.success = false;
                    Message.message = "Bản ghi không tồn tại";
                }

                return serialize.Serialize(Message);
            }
            catch (Exception ex)
            {
                Message.success = false;
                Message.message = ex.Message;
                return serialize.Serialize(Message);
            }
        }

        [WebMethod]
        public static cls_Note GetErrorNoteById(int billID)
        {
            cls_Note note = new cls_Note();
            var db = new Solution_30shineEntities();
            var RECORD = db.BillServices.FirstOrDefault(w => w.Id == billID);
            note.NoteByManager = RECORD.ErrorNote;
            note.NoteByStylist = RECORD.NoteByStylist;
            return note;
        }


        #region Lưu ảnh checked của salon
        [WebMethod]
        public static string LoadImage(int bID, int order, string base64)
        {
            var Message = new Library.Class.cls_message();
            var serialize = new JavaScriptSerializer();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    Random random = new Random();
                    var imgName = "Img_" + random.Next(100000, 999999) + "_" + String.Format("{0:dd_MM_yyyy_HH_mm_ss}", DateTime.Now) + "_Curling_KCS_ERP";
                    var response = _30shine.BackEnd.UploadImage_V2.Upload_V2.getInstance().PushImageToAws(imgName, base64 ?? "");
                    var link = response != null ? response.link : "";
                    if (order == 1 || order == 2 || order == 3 || order == 4)
                    {
                        var record = db.BillServices.FirstOrDefault(w => w.Id == bID);
                        string[] ListImg;
                        ListImg = record.Images.Split(',');
                        if (record != null)
                        {
                            switch (order)
                            {
                                case 1:
                                    record.ImageChecked1 = link;
                                    break;
                                case 2:
                                    record.ImageChecked2 = link;
                                    break;
                                case 3:
                                    record.ImageChecked3 = link;
                                    break;
                                case 4:
                                    record.ImageChecked4 = link;
                                    break;
                            }
                            switch (ListImg.Count())
                            {
                                case 4:
                                    if (String.IsNullOrEmpty(record.ImageChecked1) && !String.IsNullOrEmpty(ListImg[0]))
                                    {
                                        record.ImageChecked1 = ListImg[0] ?? "";
                                    }
                                    if (String.IsNullOrEmpty(record.ImageChecked2) && !String.IsNullOrEmpty(ListImg[1]))
                                    {
                                        record.ImageChecked2 = ListImg[1] ?? "";
                                    }
                                    if (String.IsNullOrEmpty(record.ImageChecked3) && !String.IsNullOrEmpty(ListImg[2]))
                                    {
                                        record.ImageChecked3 = ListImg[2] ?? "";
                                    }
                                    if (String.IsNullOrEmpty(record.ImageChecked4) && !String.IsNullOrEmpty(ListImg[3]))
                                    {
                                        record.ImageChecked4 = ListImg[3] ?? "";
                                    }
                                    break;
                                case 3:
                                    if (String.IsNullOrEmpty(record.ImageChecked1) && !String.IsNullOrEmpty(ListImg[0]))
                                    {
                                        record.ImageChecked1 = ListImg[0] ?? "";
                                    }
                                    if (String.IsNullOrEmpty(record.ImageChecked2) && !String.IsNullOrEmpty(ListImg[1]))
                                    {
                                        record.ImageChecked2 = ListImg[1] ?? "";
                                    }
                                    if (String.IsNullOrEmpty(record.ImageChecked3) && !String.IsNullOrEmpty(ListImg[2]))
                                    {
                                        record.ImageChecked3 = ListImg[2] ?? "";
                                    }
                                    break;
                                case 2:
                                    if (String.IsNullOrEmpty(record.ImageChecked1) && !String.IsNullOrEmpty(ListImg[0]))
                                    {
                                        record.ImageChecked1 = ListImg[0] ?? "";
                                    }
                                    if (String.IsNullOrEmpty(record.ImageChecked2) && !String.IsNullOrEmpty(ListImg[1]))
                                    {
                                        record.ImageChecked2 = ListImg[1] ?? "";
                                    }
                                    break;
                                case 1:
                                    if (String.IsNullOrEmpty(record.ImageChecked1) && !String.IsNullOrEmpty(ListImg[0]))
                                    {
                                        record.ImageChecked1 = ListImg[0] ?? "";
                                    }
                                    break;
                                default:
                                    record.ImageChecked1 = "";
                                    record.ImageChecked2 = "";
                                    record.ImageChecked3 = "";
                                    record.ImageChecked4 = "";
                                    break;
                            }
                            record.ModifiedDate = DateTime.Now;
                            db.BillServices.AddOrUpdate(record);
                            var exc = db.SaveChanges();
                            if (exc > 0)
                            {
                                Message.success = true;
                                Message.message = "Cập nhật thành công.";
                            }
                            else
                            {
                                Message.success = false;
                                Message.message = "Cập nhật thất bại.";
                            }
                        }
                        else
                        {
                            Message.success = false;
                            Message.message = "Bản ghi không tồn tại";
                        }
                    }
                    else if (order == 5 || order == 6)
                    {
                        var record = db.ImageDatas.FirstOrDefault(w => w.OBJId == bID);
                        if (record != null)
                        {
                            switch (order)
                            {
                                case 5:
                                    record.ImageCheckBefore = link;
                                    break;
                                case 6:
                                    record.ImageCheckAfter = link;
                                    break;
                            }
                            if (String.IsNullOrEmpty(record.ImageCheckBefore) && !String.IsNullOrEmpty(record.ImageBefore))
                            {
                                record.ImageCheckBefore = record.ImageBefore ?? "";
                            }
                            if (String.IsNullOrEmpty(record.ImageCheckAfter) && !String.IsNullOrEmpty(record.ImageAfter))
                            {
                                record.ImageCheckAfter = record.ImageAfter ?? "";
                            }
                            record.ModifiedTime = DateTime.Now;
                            db.ImageDatas.AddOrUpdate(record);
                            var exc = db.SaveChanges();
                            if (exc > 0)
                            {
                                Message.success = true;
                                Message.message = "Cập nhật thành công.";
                            }
                            else
                            {
                                Message.success = false;
                                Message.message = "Cập nhật thất bại.";
                            }
                        }
                        else
                        {
                            Message.success = false;
                            Message.message = "Bản ghi không tồn tại";
                        }
                    }
                }
                return serialize.Serialize(Message);
            }
            catch (Exception ex)
            {
                Message.success = false;
                Message.message = ex.Message;
                return serialize.Serialize(Message);
            }
        }
        #endregion

        #region Lưu ảnh checked của S4M
        [WebMethod]
        public static string LoadImage_S4M(int bID, int order, string base64)
        {
            var Message = new Library.Class.cls_message();
            var serialize = new JavaScriptSerializer();
            var error = 0;
            S4M_Bill obj = new S4M_Bill();
            string[] ListImg;
            string _Domain = ConfigurationManager.AppSettings["DomainKey_PreviewImg"];
            try
            {
                string imgPath = "";
                string ImageName = "";
                string ImgRootPath = "";
                byte[] image64 = Convert.FromBase64String(base64);
                Image image;

                using (MemoryStream ms = new MemoryStream(image64))
                {
                    image = Image.FromStream(ms, true);
                    ImgRootPath = "/Resource/Images/S4M_CheckImages/";
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath("~" + ImgRootPath)))
                    {
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~" + ImgRootPath));
                    }
                    string dirFullPath = HttpContext.Current.Server.MapPath("~" + ImgRootPath);
                    ImageName = "img_" + DateTime.Now.ToString("ddMMyyhhmmss") + ".png";
                    image.Save(dirFullPath + ImageName, ImageFormat.Png);
                }
                var db = new Solution_30shineEntities();
                var RECORD = db.Stylist4Men_BillCutFree.FirstOrDefault(w => w.Id == bID);
                ListImg = RECORD.Images.Split(',');
                //imgPath = "http://api.30shine.com" + ImgRootPath + ImageName;
                imgPath = "https://erp.30shine.com" + ImgRootPath + ImageName;
                //imgPath = "http://erp.30shine.net" + ImgRootPath + ImageName;
                if (RECORD != null)
                {
                    for (int i = 0; i < ListImg.Length; i++)
                    {
                        if (!(ListImg[i].StartsWith("http")))
                        {
                            ListImg[i] = _Domain + ListImg[i];
                        }
                    }

                    if (ListImg.Count() == 4)
                    {
                        obj.Img1 = ListImg[0] ?? "";
                        obj.Img2 = ListImg[1] ?? "";
                        obj.Img3 = ListImg[2] ?? "";
                        obj.Img4 = ListImg[3] ?? "";
                    }
                    if (ListImg.Count() == 3)
                    {
                        obj.Img1 = ListImg[0] ?? "";
                        obj.Img2 = ListImg[1] ?? "";
                        obj.Img3 = ListImg[2] ?? "";
                    }
                    if (ListImg.Count() == 2)
                    {
                        obj.Img1 = ListImg[0] ?? "";
                        obj.Img2 = ListImg[1] ?? "";
                    }
                    if (ListImg.Count() == 1)
                    {
                        obj.Img1 = ListImg[0] ?? "";
                    }

                    if (order == 1)
                    {
                        RECORD.ImageChecked1 = imgPath;
                    }
                    else if (order == 2)
                    {
                        RECORD.ImageChecked2 = imgPath;
                    }
                    else if (order == 3)
                    {
                        RECORD.ImageChecked3 = imgPath;
                    }
                    else if (order == 4)
                    {
                        RECORD.ImageChecked4 = imgPath;
                    }

                    if (RECORD.ImageChecked1 == null && obj.Img1 != null)
                    {
                        RECORD.ImageChecked1 = obj.Img1;
                    }
                    if (RECORD.ImageChecked2 == null && obj.Img2 != null)
                    {
                        RECORD.ImageChecked2 = obj.Img2;
                    }
                    if (RECORD.ImageChecked3 == null && obj.Img3 != null)
                    {
                        RECORD.ImageChecked3 = obj.Img3;
                    }
                    if (RECORD.ImageChecked4 == null && obj.Img4 != null)
                    {
                        RECORD.ImageChecked4 = obj.Img4;
                    }

                    RECORD.ModifiedTime = DateTime.Now;
                    db.Stylist4Men_BillCutFree.AddOrUpdate(RECORD);
                    error += db.SaveChanges() > 0 ? 0 : 1;
                    if (error == 0)
                    {
                        Message.success = true;
                        Message.message = "Cập nhật thành công.";
                    }
                    else
                    {
                        Message.success = false;
                        Message.message = "Cập nhật thất bại.";
                    }
                }
                else
                {
                    error = 1;
                    Message.success = false;
                    Message.message = "Bản ghi không tồn tại";
                }
                return serialize.Serialize(Message);
            }
            catch (Exception ex)
            {
                Message.success = false;
                Message.message = ex.Message;
                return serialize.Serialize(Message);
            }
        }
        #endregion

        #region Load lại ảnh đã check của salon
        [WebMethod]
        public static string RefreshCheckedImage(int bID, int order)
        {
            string imageOlder = "";
            BillServiceView obj = new BillServiceView();
            string[] ListImg;
            using (var db = new Solution_30shineEntities())
            {

                if (order == 1 || order == 2 || order == 3 || order == 4)
                {
                    var RECORD = db.BillServices.FirstOrDefault(w => w.Id == bID);
                    if (RECORD != null)
                    {
                        ListImg = RECORD.Images.Split(',');
                        if (ListImg.Count() == 1 && order == 1 && !String.IsNullOrEmpty(ListImg[0]))
                        {
                            imageOlder = ListImg[0];
                        }
                        else if (ListImg.Count() == 2 && order == 2 && !String.IsNullOrEmpty(ListImg[1]))
                        {
                            imageOlder = ListImg[1];
                        }
                        else if (ListImg.Count() == 3 && order == 3 && !String.IsNullOrEmpty(ListImg[2]))
                        {
                            imageOlder = ListImg[2];
                        }
                        else if (ListImg.Count() == 4 && order == 4 && !String.IsNullOrEmpty(ListImg[3]))
                        {
                            imageOlder = ListImg[3];
                        }
                    }
                }
                else if (order == 5 || order == 6)
                {
                    var record = db.ImageDatas.FirstOrDefault(w => w.OBJId == bID);
                    if (record != null)
                    {
                        if (order == 5 && !String.IsNullOrEmpty(record.ImageBefore))
                        {
                            imageOlder = record.ImageBefore;
                        }
                        else if (order == 6 && !String.IsNullOrEmpty(record.ImageAfter))
                        {
                            imageOlder = record.ImageAfter;
                        }
                    }
                }
            }
            return imageOlder;
        }
        #endregion

        #region Load lại ảnh đã check của S4M 
        [WebMethod]
        public static string RefreshCheckedImage_S4M(int bID, int order)
        {
            string imageOlder = "";
            S4M_Bill obj = new S4M_Bill();
            string[] ListImg;
            string _Domain = ConfigurationManager.AppSettings["DomainKey_PreviewImg"];
            var db = new Solution_30shineEntities();
            var RECORD = db.BillServices.FirstOrDefault(w => w.Id == bID);
            ListImg = RECORD.Images.Split(',');
            if (RECORD != null)
            {
                if (ListImg.Count() == 4)
                {
                    obj.Img1 = ListImg[0] ?? "";
                    obj.Img2 = ListImg[1] ?? "";
                    obj.Img3 = ListImg[2] ?? "";
                    obj.Img4 = ListImg[3] ?? "";
                }
                if (ListImg.Count() == 3)
                {
                    obj.Img1 = ListImg[0] ?? "";
                    obj.Img2 = ListImg[1] ?? "";
                    obj.Img3 = ListImg[2] ?? "";
                }
                if (ListImg.Count() == 2)
                {
                    obj.Img1 = ListImg[0] ?? "";
                    obj.Img2 = ListImg[1] ?? "";
                }
                if (ListImg.Count() == 1)
                {
                    obj.Img1 = ListImg[0] ?? "";
                }
                if (order == 1 && (obj.Img1 != null || obj.Img1 != string.Empty))
                {
                    imageOlder = obj.Img1;
                }
                else if (order == 2 && (obj.Img2 != null || obj.Img2 != string.Empty))
                {
                    imageOlder = obj.Img2;
                }
                else if (order == 3 && (obj.Img3 != null || obj.Img3 != string.Empty))
                {
                    imageOlder = obj.Img3;
                }
                else if (order == 4 && (obj.Img4 != null || obj.Img4 != string.Empty))
                {
                    imageOlder = obj.Img4;
                }

            }
            return imageOlder;
        }
        #endregion

        [WebMethod]
        public static List<ErrorCutHair> GetErrorList()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.ErrorCutHairs.Where(a => a.IsDelete == false).ToList();
                return lst;
            }
        }

        [WebMethod]
        public static List<Api_HairMode> ReturnAllHairMode()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Api_HairMode.Where(a => a.IsDelete == false && a.Publish == true).ToList();
                return lst;
            }
        }

        [WebMethod]
        public static string updateCustomer_HairMode_Bill(int cusHairId, int? hairStyleId, int CustomerId, int billID)
        {
            var Message = new Library.Class.cls_message();
            var serialize = new JavaScriptSerializer();
            var error = 0;
            var db = new Solution_30shineEntities();
            var RECORD = db.Customer_HairMode_Bill.FirstOrDefault(w => w.Id == cusHairId);
            var obj = new Customer_HairMode_Bill();
            if (RECORD == null)
            {
                obj.BillId = billID;
                obj.CreateDate = DateTime.Now;
                obj.HairStyleId = hairStyleId;
                //obj.FaceTypeId = faceTypeId;
                obj.CustomerId = CustomerId;
                //if(_salonId > 0)
                //{
                //    obj.IsHoiQuan = false;
                //}
                //else if (_classId > 0)
                //{
                //    obj.IsHoiQuan = true;
                //}
                db.Customer_HairMode_Bill.Add(obj);
                error += db.SaveChanges() > 0 ? 0 : 1;
                if (error == 0)
                {
                    Message.success = true;
                    Message.message = "Thêm thành công.";
                }
                else
                {
                    Message.success = false;
                    Message.message = "Thêm thất bại.";
                }
            }
            else
            {
                RECORD.BillId = billID;
                if (hairStyleId != null)
                {
                    RECORD.HairStyleId = hairStyleId;
                }
                //if(faceTypeId != null)
                //{
                //    RECORD.FaceTypeId = faceTypeId;
                //}

                RECORD.CustomerId = CustomerId;
                RECORD.ModifiledDate = DateTime.Now;
                db.Customer_HairMode_Bill.AddOrUpdate(RECORD);
                error += db.SaveChanges() > 0 ? 0 : 1;
                if (error == 0)
                {
                    Message.success = true;
                    Message.message = "Cập nhật thành công.";
                }
                else
                {
                    Message.success = false;
                    Message.message = "Cập nhật thất bại.";
                }
            }
            return serialize.Serialize(Message);
            //}
            //catch (Exception ex)
            //{
            //    Message.success = false;
            //    Message.message = ex.Message;
            //    return serialize.Serialize(Message);
            //}
        }

        [WebMethod]
        public static Customer_HairMode_Bill getHairStyleById(int billId)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var lst = db.Customer_HairMode_Bill.SingleOrDefault(a => a.BillId == billId /*&& (a.IsHoiQuan == false || a.IsHoiQuan == null)*/ && (a.IsDelete == false || a.IsDelete == null));

                    return lst;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [WebMethod]
        public static string updateErrorNote(int billID, string errorNote, string noteByStylist)
        {
            var Message = new Library.Class.cls_message();
            var serialize = new JavaScriptSerializer();
            var error = 0;
            try
            {
                var db = new Solution_30shineEntities();
                var RECORD = db.BillServices.FirstOrDefault(w => w.Id == billID);
                if (RECORD != null)
                {
                    RECORD.ErrorNote = errorNote;
                    //RECORD.NoteByStylist = noteByStylist;
                    RECORD.ModifiedDate = DateTime.Now;
                    db.BillServices.AddOrUpdate(RECORD);
                    error += db.SaveChanges() > 0 ? 0 : 1;
                    if (error == 0)
                    {
                        Message.success = true;
                        Message.message = "Cập nhật thành công.";
                    }
                    else
                    {
                        Message.success = false;
                        Message.message = "Cập nhật thất bại.";
                    }
                }
                else
                {
                    error = 1;
                    Message.success = false;
                    Message.message = "Bản ghi không tồn tại";
                }
                //return serialize.Serialize(Message);
                return serialize.Serialize(Message);

            }
            catch (Exception ex)
            {
                Message.success = false;
                Message.message = ex.Message;
                return serialize.Serialize(Message);
            }
        }

        /// <summary>
        /// InsertAndUpdateSCSC_CheckError
        /// </summary>
        /// <param name="ID_SCSC_Check"></param>
        /// <param name="Bill_ID"></param>
        /// <param name="ImageError"></param>
        /// <param name="NoteError"></param>
        /// <param name="_TotalPoint_SCSC"></param>
        /// <param name="SHAPE_ID"></param>
        /// <param name="CONNECTIVE_ID"></param>
        /// <param name="SHARPNESS_ID"></param>
        /// <param name="COMPLETION_ID"></param>
        /// <returns></returns>
        [WebMethod]
        public static object InsertAndUpdateSCSC_CheckError(int ID_SCSC_Check, int Bill_ID, int ImageError, string NoteError, int _TotalPoint_SCSC, int SHAPE_ID, int CONNECTIVE_ID, int SHARPNESS_ID, int COMPLETION_ID, int SCSC_PointError, int MaxPointShap_ID, int MaxPointConnective_ID, int MaxPointSharpNess_ID, int MaxPointCompletion_ID, string WorkDate, int CURLING_PointError, int HAIRTIP_ID, int HAIRROOT_ID, int HAIRWAVE_ID, int ImageErrorCurling)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var Msg = new Library.Class.cls_message();
                    var objSCSC_Check = new SCSC_CheckError();
                    bool IsUpdate = false;
                    if (Bill_ID != 0)
                    {
                        objSCSC_Check = db.SCSC_CheckError.FirstOrDefault(a => a.BillService_ID == Bill_ID && a.Publish == true && a.IsDelete == false);

                        SCSC_CheckError objSCSCErrorStatic = new SCSC_CheckError();
                        if (objSCSC_Check == null)
                        {
                            objSCSC_Check = new SCSC_CheckError();
                            objSCSC_Check.BillService_ID = Bill_ID;
                            objSCSC_Check.CreateDate = DateTime.Now;
                            objSCSC_Check.NoteError = NoteError;
                            if (ImageError == 1)
                            {
                                objSCSC_Check.ImageError = true;
                            }
                            else
                            {
                                objSCSC_Check.ImageError = false;
                            }
                            objSCSC_Check.MaxPoint_Shap_ID = MaxPointShap_ID;
                            objSCSC_Check.MaxPoint_Connective_ID = MaxPointConnective_ID;
                            objSCSC_Check.MaxPoint_SharpNess_ID = MaxPointSharpNess_ID;
                            objSCSC_Check.MaxPoint_ComPlatetion_ID = MaxPointCompletion_ID;
                            objSCSC_Check.PointError = SCSC_PointError;
                            objSCSC_Check.TotalPointSCSC = _TotalPoint_SCSC;
                            objSCSC_Check.Shape_ID = SHAPE_ID;
                            objSCSC_Check.ConnectTive_ID = CONNECTIVE_ID;
                            objSCSC_Check.SharpNess_ID = SHARPNESS_ID;
                            objSCSC_Check.ComPlatetion_ID = COMPLETION_ID;
                            if (ImageErrorCurling == 1)
                            {
                                objSCSC_Check.ImageErrorCurling = true;
                            }
                            else
                            {
                                objSCSC_Check.ImageErrorCurling = false;
                            }
                            objSCSC_Check.HairTip_ID = HAIRTIP_ID;
                            objSCSC_Check.HairRoot_ID = HAIRROOT_ID;
                            objSCSC_Check.HairWaves_ID = HAIRWAVE_ID;
                            objSCSC_Check.TotalPointSCSCCurling = CURLING_PointError;
                            objSCSC_Check.IsDelete = false;
                            objSCSC_Check.Publish = true;
                            db.SCSC_CheckError.Add(objSCSC_Check);
                            //gia tri static = null
                            objSCSCErrorStatic = new SCSC_CheckError();
                        }
                        else
                        {
                            //Luu lai gia tri ban ghi cu de static.
                            ReflectionExtension.CopyObjectValue(objSCSC_Check, objSCSCErrorStatic);
                            IsUpdate = true;
                            //====================================objSCSC_Check.NoteError = NoteError;
                            if (ImageError == 1)
                            {
                                objSCSC_Check.ImageError = true;
                            }
                            else
                            {
                                objSCSC_Check.ImageError = false;
                            }
                            if (ImageErrorCurling == 1)
                            {
                                objSCSC_Check.ImageErrorCurling = true;
                            }
                            else
                            {
                                objSCSC_Check.ImageErrorCurling = false;
                            }
                            objSCSC_Check.MaxPoint_Shap_ID = MaxPointShap_ID;
                            objSCSC_Check.MaxPoint_Connective_ID = MaxPointConnective_ID;
                            objSCSC_Check.MaxPoint_SharpNess_ID = MaxPointSharpNess_ID;
                            objSCSC_Check.MaxPoint_ComPlatetion_ID = MaxPointCompletion_ID;
                            objSCSC_Check.PointError = SCSC_PointError;
                            objSCSC_Check.TotalPointSCSC = _TotalPoint_SCSC;
                            objSCSC_Check.Shape_ID = SHAPE_ID;
                            objSCSC_Check.ConnectTive_ID = CONNECTIVE_ID;
                            objSCSC_Check.SharpNess_ID = SHARPNESS_ID;
                            objSCSC_Check.ComPlatetion_ID = COMPLETION_ID;
                            objSCSC_Check.HairTip_ID = HAIRTIP_ID;
                            objSCSC_Check.HairRoot_ID = HAIRROOT_ID;
                            objSCSC_Check.HairWaves_ID = HAIRWAVE_ID;
                            objSCSC_Check.TotalPointSCSCCurling = CURLING_PointError;
                            objSCSC_Check.NoteError = NoteError;
                            objSCSC_Check.IsDelete = false;
                            objSCSC_Check.Publish = true;
                            objSCSC_Check.ModifiledDate = DateTime.Now;
                        }
                        var rs = db.SaveChanges();
                        if (rs > 0 && _TotalPoint_SCSC >= 8)
                        {
                            InsertStyleMaster(Bill_ID, WorkDate);
                        }
                        //chạy static thêm số ảnh lỗi mới vào bảng StaticRatingWaitTime
                        try
                        {
                            var request = new Request();

                            Task StaticRatingWaitTime = Task.Run(async () =>
                            {
                                await request.RunPostAsync(
                                    Libraries.AppConstants.URL_API_STAFF + "/api/static-ranking/add-img-error",
                                        new
                                        {
                                            BillId = Bill_ID,
                                            IsUpdate = IsUpdate,
                                            objSCSCError = new
                                            {
                                                BillId = objSCSCErrorStatic.BillService_ID,
                                                Id = objSCSCErrorStatic.ID,
                                                TotalPointScsc = objSCSCErrorStatic.TotalPointSCSC,
                                                BillServiceId = objSCSCErrorStatic.BillService_ID
                                            }
                                        }
                                    );
                            });

                        }
                        catch (Exception ex)
                        {
                            //Library.Function.WriteToFile("ThongKeVatTuUpdateBill Exception First " + DateTime.Now + ": " + Library.Function.JavaScript.Serialize(OBJ) + " ex " + ex.StackTrace, PATH_ASYNC_CHECKOUT_BILL_LOG_EXCEPTION);
                        }

                        //chạy static Bảng StaticService_Profit
                        try
                        {
                            var request = new Request();

                            Task StaticRatingWaitTime = Task.Run(async () =>
                            {
                                await request.RunPostAsync(
                                    Libraries.AppConstants.URL_API_STAFF + "/api-static-profit/ad-update-img-error",
                                        new
                                        {
                                            BillId = Bill_ID,
                                            IsUpdate = IsUpdate,
                                            objSCSCError = new
                                            {
                                                BillId = objSCSCErrorStatic.BillService_ID,
                                                Id = objSCSCErrorStatic.ID,
                                                TotalPointScsc = objSCSCErrorStatic.TotalPointSCSC,
                                                BillServiceId = objSCSCErrorStatic.BillService_ID
                                            }
                                        }
                                    );
                            });

                        }
                        catch (Exception ex)
                        {
                            //Library.Function.WriteToFile("ThongKeVatTuUpdateBill Exception First " + DateTime.Now + ": " + Library.Function.JavaScript.Serialize(OBJ) + " ex " + ex.StackTrace, PATH_ASYNC_CHECKOUT_BILL_LOG_EXCEPTION);
                        }
                        Msg.data = objSCSC_Check;
                        Msg.status = "success";
                        Msg.success = true;
                    }
                    else
                    {
                        Msg.status = "Not Bill";
                        Msg.success = false;
                    }

                    return Msg;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Paramater...", ex);
            }

        }

        public static void InsertStyleMaster(int billId, string WorkDate)
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
            //var sc = db.SCSC_CheckError.FirstOrDefault(w => w.BillService_ID == billId);
            var b = db.BillServices.FirstOrDefault(bill => bill.Id == billId);
            var staff = (from s in db.Staffs
                         join bill in db.BillServices on s.Id equals bill.Staff_Hairdresser_Id
                         where bill.Id == billId
                         select new { s.Id, s.Fullname }).FirstOrDefault();
            string[] listImage;
            var postNumber = "1";
            var workDate = Convert.ToDateTime(WorkDate, culture);
            DateTime dateTimeNow = DateTime.Now;
            var firstDayOfMonth = new DateTime(dateTimeNow.Year, dateTimeNow.Month, 1);
            var lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
            if (b != null)
            {
                if ((workDate >= firstDayOfMonth) && (workDate <= lastDayOfMonth))
                {
                    var checkMaster = db.StyleMasters.FirstOrDefault(f => f.BillID == billId);
                    if (checkMaster == null)
                    {
                        listImage = b.Images.Split(',');
                        StyleMaster sm = new StyleMaster();
                        sm.ImageSource = 1;
                        sm.CreatedTime = DateTime.Now;
                        switch (listImage.Count())
                        {
                            case 4:
                                sm.Image1 = listImage[0];
                                sm.Image2 = listImage[1];
                                sm.Image3 = listImage[2];
                                sm.Image4 = listImage[3];
                                break;
                            case 3:
                                sm.Image1 = listImage[0];
                                sm.Image2 = listImage[1];
                                sm.Image3 = listImage[2];
                                sm.Image4 = "";
                                break;
                            case 2:
                                sm.Image1 = listImage[0];
                                sm.Image2 = listImage[1];
                                sm.Image3 = "";
                                sm.Image4 = "";
                                break;
                            case 1:
                                sm.Image1 = listImage[0];
                                sm.Image2 = "";
                                sm.Image3 = "";
                                sm.Image4 = "";
                                break;
                            default:
                                sm.Image1 = "";
                                sm.Image2 = "";
                                sm.Image3 = "";
                                sm.Image4 = "";
                                break;
                        }
                        sm.StylistId = staff.Id;
                        sm.BillID = billId;
                        sm.StylistName = staff.Fullname;
                        var ss = db.StyleMasters.OrderByDescending(s => s.Id).FirstOrDefault();
                        if (ss != null)
                        {
                            postNumber = (Convert.ToInt32(ss.PostNumber) + 1).ToString().PadLeft(4, '0');
                        }
                        else postNumber = "0001";
                        sm.PostNumber = postNumber;

                        sm.StyleMasterStatusId = 1;
                        sm.IsDelete = false;
                        db.StyleMasters.Add(sm);
                        db.SaveChanges();
                    }
                    else
                    {
                        checkMaster = new StyleMaster();
                    }
                }
            }
        }

        [WebMethod]
        public static object BillDataWhereBillID(int Bill_ID)
        {
            using (var db = new Solution_30shineEntities())
            {
                var msg = new Msg();
                var obj = db.SCSC_CheckError.FirstOrDefault(a => a.BillService_ID == Bill_ID && a.IsDelete == false && a.Publish == true);
                if (obj != null)
                {
                    msg.data = obj;
                    msg.msg = "success";

                }
                else
                {
                    msg.msg = "Fail";
                }
                return msg;
            }

        }

        [WebMethod]
        public static object BindFlowserService(int Bill_ID)
        {
            using (var db = new Solution_30shineEntities())
            {
                var msg = new Msg();
                var lstService = (from a in db.FlowServices
                                  join b in db.Services on a.ServiceId equals b.Id
                                  where a.IsDelete == 0 && a.BillId == Bill_ID
                                  select new { a.ServiceId, b.Name }).ToList();
                //var lst = db.FlowServices.Where(a => a.BillId == Bill_ID).ToList();
                msg.data = lstService;
                return msg;
            }
        }


        public class S4M_Bill
        {
            public int Id { get; set; }
            public string BillDate { get; set; }
            public string ClassName { get; set; }
            public string CustomerPhone { get; set; }
            public string CustomerName { get; set; }
            public string Img1 { get; set; }
            public string Img2 { get; set; }
            public string Img3 { get; set; }
            public string Img4 { get; set; }
            public string ImageStatusId { get; set; }
            public string ErrorNote { get; set; }
            public string NoteByStylist { get; set; }
            public string Student { get; set; }
            public int? CustomerId { get; set; }
        }
        public class S4M_custom_BillService : Stylist4Men_BillCutFree
        {
            public string CustomerPhone { get; set; }
            public string CustomerName { get; set; }
        }
        public class custom_BillService : BillService
        {
            public string CustomerPhone { get; set; }
            public string CustomerName { get; set; }
            public int? IdCustomer { get; set; }
            public int? ID_SCSC { get; set; }
            public bool? ImageError { get; set; }
            public string NoteError { get; set; }
            public int? Shape_ID { get; set; }
            public int? SharpNess_ID { get; set; }
            public int? ComPlatetion_ID { get; set; }
            public int? ConnectTive_ID { get; set; }
            public int? HairTip_ID { get; set; }
            public int? HairRoot_ID { get; set; }
            public int? HairWaves_ID { get; set; }
            public int? TotalPointSCSC { get; set; }
            public bool? ImageErrorCurling { get; set; }
            public int? TotalPointSCSCCurling { get; set; }
            public int? PointSCSC { get; set; }
            public int ImgId { get; set; }
            public string ImageBefore { get; set; }
            public string ImageAfter { get; set; }
            public string ImageCheckBefore { get; set; }
            public string ImageCheckAfter { get; set; }
        }
        public class cls_Note
        {
            public string NoteByManager { get; set; }
            public string NoteByStylist { get; set; }
        }

        public class ListStylist
        {
            public int? Id { get; set; }
            public string Fullname { get; set; }
        }

        public class ImageAws
        {
            public string img_name { get; set; }
            public string img_base64 { get; set; }
        }

        public class ResponseData
        {
            public string img_name { get; set; }
            public string link { get; set; }
            public bool status { get; set; }
            public string message { get; set; }
        }
    }
}