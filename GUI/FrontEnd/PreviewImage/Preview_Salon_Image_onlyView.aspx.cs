﻿using _30shine.Helpers;
using _30shine.MODEL.CustomClass;
using _30shine.MODEL.ENTITY.EDMX;
using Project.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Migrations;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace _30shine.GUI.FrontEnd.PreviewImage
{
    public partial class Preview_Salon_Image_onlyView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txtToDate.Value = DateTime.Now.ToString("dd/MM/yyyy");
        }
        
        [WebMethod]
        public static List<Staff> ReturnAllStylistBySalon(int _SalonId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Staffs.Where(a => a.IsDelete == 0 && a.Active == 1 && a.Type == 1 && a.SalonId == _SalonId).ToList();
                return lst;
            }
        }
        [WebMethod]
        public static List<Stylist4Men_Class> ReturnAllClass()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Stylist4Men_Class.Where(a => a.IsDelete == false && a.Publish == true).ToList();
                return lst;
            }
        }

        [WebMethod]
        public static List<Staff> ReturnAllStudentByClass(int _ClassId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Staffs.Where(a => a.IsDelete == 0 && a.Active == 1 && a.Type == 1 && a.S4MClassId == _ClassId/* && a.IsHoiQuan == true*/).ToList();
                return lst;
            }
        }

        #region Get Bill theo salon
        [WebMethod]
        public static List<BillServiceView> GetRandomBillImage(string _Date, int _SalonId, int _StylistId, string _imageStatus)
        {
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                DateTime _FilterDate = Convert.ToDateTime(_Date, culture);
                string _FromDate = String.Format("{0:yyyy/MM/dd}", _FilterDate);
                string _ToDate = String.Format("{0:yyyy/MM/dd}", _FilterDate.AddDays(1));

                var sqlCount = @"select count(*) from BillService a where a.CreatedDate between '" + _FromDate + "' and '" + _ToDate + "' and a.IsDelete != 1 and a.Pending = 0 and a.Images is not null and a.SalonId = " + _SalonId;

                int _TotalBill = db.Database.SqlQuery<int>(sqlCount).First();

                var sql = "";

                if (_StylistId != 0)
                {
                    sql = @"select a.*,b.Fullname as CustomerName, b.Phone as CustomerPhone from BillService a left join Customer b on a.CustomerId = b.Id where a.Staff_Hairdresser_Id = " + _StylistId + " and a.CreatedDate between '" + _FromDate + "' and '" + _ToDate + "' and a.IsDelete != 1 and a.Pending = 0 and a.Images is not null and a.SalonId = " + _SalonId;
                }
                else
                {
                    sql = @"select a.*,b.Fullname as CustomerName, b.Phone as CustomerPhone from BillService a left join Customer b on a.CustomerId = b.Id where a.CreatedDate between '" + _FromDate + "' and '" + _ToDate + "' and a.IsDelete != 1 and a.Pending = 0 and a.Images is not null and a.SalonId = " + _SalonId;
                }

                //1-bill không có lỗi cắt || 2-bill có lỗi hình khối || 3-bill lỗi đường cắt  || 4-bill lỗi hình khối chưa liên kết
                //5-bill lỗi ngọn tóc quá dày || 6-bill cạo mai gáy lỗi || 7-bill vuốt sáp lỗi || 8-bill ảnh thiếu/mờ
                //9-bill có ảnh || 10-bill đã đánh giá || 11-bill chưa đánh giá 

                if (_imageStatus == "1" || _imageStatus == "2" || _imageStatus == "3" || _imageStatus == "4" || _imageStatus == "5" || _imageStatus == "6" || _imageStatus == "7" || _imageStatus == "8")
                {
                    sql += " and ImageStatusId like '%" + _imageStatus + "%' ";
                }
                else if (_imageStatus == "10")
                {
                    sql += " and a.ImageStatusId is not null";
                }
                else if (_imageStatus == "11")
                {
                    sql += " and a.ImageStatusId is null";
                }
                var list = db.Database.SqlQuery<custom_BillService>(sql).ToList();

                var objSalon = db.Tbl_Salon.Find(_SalonId);

                List<BillServiceView> lstBillView = new List<BillServiceView>();
                BillServiceView obj = new BillServiceView();
                string[] ListImg;

                string _Domain = ConfigurationManager.AppSettings["DomainKey_PreviewImg"];

                foreach (var item in list)
                {
                    ListImg = item.Images.Split(',');
                    obj = new BillServiceView();
                    obj.Id = item.Id;
                    obj.BillDate = string.Format("{0:dd/MM/yyyy}", item.CreatedDate);
                    obj.CustomerPhone = item.CustomerPhone;
                    obj.CustomerName = item.CustomerName;
                    obj.ServiceIds = item.ServiceIds;
                    if (item.ImageStatusId != null)
                    {
                        obj.ImageStatusId = item.ImageStatusId;
                    }

                    for (int i = 0; i < ListImg.Length; i++)
                    {
                        if (!(ListImg[i].StartsWith("http")))
                        {
                            ListImg[i] = _Domain + ListImg[i];
                        }
                    }
                    if (ListImg.Count() == 4)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                        obj.Img3 = ListImg[2];
                        obj.Img4 = ListImg[3];
                    }
                    if (ListImg.Count() == 3)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                        obj.Img3 = ListImg[2];
                    }
                    if (ListImg.Count() == 2)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                    }
                    if (ListImg.Count() == 1)
                        obj.Img1 = ListImg[0];

                    obj.SalonName = objSalon.Name;
                    obj.ErrorNote = item.ErrorNote;
                    obj.NoteByStylist = item.NoteByStylist;
                    if (item.ImageChecked1 != null)
                    {
                        obj.Img1 = item.ImageChecked1;
                    }
                    if (item.ImageChecked2 != null)
                    {
                        obj.Img2 = item.ImageChecked2;
                    }
                    if (item.ImageChecked3 != null)
                    {
                        obj.Img3 = item.ImageChecked3;
                    }
                    if (item.ImageChecked4 != null)
                    {
                        obj.Img4 = item.ImageChecked4;
                    }

                    lstBillView.Add(obj);
                }

                return lstBillView;
            }
        }
        #endregion

        #region Get bill theo S4M
        [WebMethod]
        public static List<S4M_Bill> GetBillImage_S4M(string _Date, int _ClassId, int _StylistId, string _imageStatus)
        {
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                DateTime _FilterDate = Convert.ToDateTime(_Date, culture);
                string _FromDate = String.Format("{0:yyyy/MM/dd}", _FilterDate);
                string _ToDate = String.Format("{0:yyyy/MM/dd}", _FilterDate.AddDays(1));

                var sqlCount = @"select count(*) from Stylist4Men_BillCutFree a where a.CreatedTime between '" + _FromDate + "' and '" + _ToDate + "' and a.IsDelete != 1 and a.Images is not null and a.ClassId =" + _ClassId;

                int _TotalBill = db.Database.SqlQuery<int>(sqlCount).First();

                var sql = "";

                if (_StylistId != 0)
                {
                    sql = "select _bill.*, _cus.CustomerName, _cus.CustomerPhone from Stylist4Men_BillCutFree _bill left join Stylist4Men_Customer _cus on _cus.Id = _bill.CustomerId where _bill.CreatedTime between '" + _FromDate + "' and '" + _ToDate + "' and _bill.IsDelete != 1 and _bill.Images is not null and _bill.ClassId = " + _ClassId + " and _bill.StudentId = " + _StylistId;
                }
                else
                {
                    sql = "select _bill.*, _cus.CustomerName, _cus.CustomerPhone from Stylist4Men_BillCutFree _bill left join Stylist4Men_Customer _cus on _cus.Id = _bill.CustomerId where _bill.CreatedTime between '" + _FromDate + "' and '" + _ToDate + "' and _bill.IsDelete != 1 and _bill.Images is not null and _bill.ClassId = " + _ClassId;
                }

                //1-bill không có lỗi cắt || 2-bill có lỗi hình khối || 3-bill lỗi đường cắt  || 4-bill lỗi hình khối chưa liên kết
                //5-bill lỗi ngọn tóc quá dày || 6-bill cạo mai gáy lỗi || 7-bill vuốt sáp lỗi || 8-bill ảnh thiếu/mờ
                //9-bill có ảnh || 10-bill đã đánh giá || 11-bill chưa đánh giá 

                if (_imageStatus == "1" || _imageStatus == "2" || _imageStatus == "3" || _imageStatus == "4" || _imageStatus == "5" || _imageStatus == "6" || _imageStatus == "7" || _imageStatus == "8")
                {
                    sql += " and ImageStatusId like '%" + _imageStatus + "%' ";
                }
                else if (_imageStatus == "10")
                {
                    sql += " and a.ImageStatusId is not null";
                }
                else if (_imageStatus == "11")
                {
                    sql += " and a.ImageStatusId is null";
                }
                var list = db.Database.SqlQuery<S4M_custom_BillService>(sql).ToList();

                var objClass = db.Stylist4Men_Class.Find(_ClassId);

                List<S4M_Bill> lstBillView = new List<S4M_Bill>();
                S4M_Bill obj = new S4M_Bill();
                string[] ListImg;

                string _Domain = ConfigurationManager.AppSettings["DomainKey_PreviewImg"];

                foreach (var item in list)
                {
                    ListImg = item.Images.Split(',');
                    obj = new S4M_Bill();
                    obj.Id = item.Id;
                    obj.NoteByStylist = item.NoteByStylist;
                    obj.BillDate = string.Format("{0:dd/MM/yyyy}", item.CreatedTime);
                    obj.CustomerPhone = item.CustomerPhone;
                    obj.CustomerName = item.CustomerName;
                    obj.CustomerId = item.CustomerId;

                    if (item.ImageStatusId != null)
                    {
                        obj.ImageStatusId = item.ImageStatusId;
                    }

                    for (int i = 0; i < ListImg.Length; i++)
                    {
                        if (!(ListImg[i].StartsWith("http")))
                        {
                            ListImg[i] = _Domain + ListImg[i];
                        }
                    }
                    if (ListImg.Count() == 4)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                        obj.Img3 = ListImg[2];
                        obj.Img4 = ListImg[3];
                    }
                    if (ListImg.Count() == 3)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                        obj.Img3 = ListImg[2];
                    }
                    if (ListImg.Count() == 2)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                    }
                    if (ListImg.Count() == 1)
                        obj.Img1 = ListImg[0];

                    obj.ClassName = objClass.Name;
                    obj.ErrorNote = item.ErrorNote;
                    if (item.ImageChecked1 != null)
                    {
                        obj.Img1 = item.ImageChecked1;
                    }
                    if (item.ImageChecked2 != null)
                    {
                        obj.Img2 = item.ImageChecked2;
                    }
                    if (item.ImageChecked3 != null)
                    {
                        obj.Img3 = item.ImageChecked3;
                    }
                    if (item.ImageChecked4 != null)
                    {
                        obj.Img4 = item.ImageChecked4;
                    }

                    lstBillView.Add(obj);
                }

                return lstBillView;
            }
        }
        #endregion


        [WebMethod]
        public static List<Tbl_Salon> ReturnAllSalon()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Tbl_Salon.Where(a => a.IsDelete == 0 && a.Publish == true).ToList();
                return lst;
            }
        }

        [WebMethod]
        public static List<ErrorCutHair> GetErrorList()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.ErrorCutHairs.Where(a => a.IsDelete == false).ToList();
                return lst;
            }
        }

        [WebMethod]
        public static List<Api_HairMode> ReturnAllHairMode()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Api_HairMode.Where(a => a.IsDelete == false && a.Publish == true).ToList();
                return lst;
            }
        }

        [WebMethod]
        public static Customer_HairMode_Bill getHairStyleById(int billId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = new Customer_HairMode_Bill();
                lst = db.Customer_HairMode_Bill.Where(a => a.BillId == billId /*&& (a.IsHoiQuan == false || a.IsHoiQuan == null)*/ && (a.IsDelete == false || a.IsDelete == null)).SingleOrDefault();
                return lst;
            }
        }

        public class custom_BillService : BillService
        {
            public string CustomerPhone { get; set; }
            public string CustomerName { get; set; }
        }
        public class S4M_Bill
        {
            public int Id { get; set; }
            public string BillDate { get; set; }
            public string ClassName { get; set; }
            public string CustomerPhone { get; set; }
            public string CustomerName { get; set; }
            public string Img1 { get; set; }
            public string Img2 { get; set; }
            public string Img3 { get; set; }
            public string Img4 { get; set; }
            public string ImageStatusId { get; set; }
            public string ErrorNote { get; set; }
            public string NoteByStylist { get; set; }
            public string Student { get; set; }
            public int? CustomerId { get; set; }
        }
        public class S4M_custom_BillService : Stylist4Men_BillCutFree
        {
            public string CustomerPhone { get; set; }
            public string CustomerName { get; set; }
        }
    }
}