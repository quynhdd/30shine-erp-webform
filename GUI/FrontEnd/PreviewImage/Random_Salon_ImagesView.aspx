﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Random_Salon_ImagesView.aspx.cs" Inherits="_30shine.GUI.FrontEnd.PreviewImage.Random_Salon_ImagesView" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .table-listing .uv-avatar {
                width: 120px;
            }

            .table-row-detail td {
                border: 1px solid black;
                padding: 5px;
            }

            td a:hover {
                color: deepskyblue !important;
            }
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Thống kê số lượng ảnh</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report be-report-timekeeping">
            <%-- Listing --%>
            <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
            <div class="wp960 content-wp">
                <div class="row">
                    <div class="filter-item">
                        <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="time-wp">
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Từ ngày" Style="margin-left: 10px!important"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ValidateTime" ControlToValidate="TxtDateTimeFrom" runat="server" CssClass="fb-cover-error" Text="Vui lòng chọn thời gian!"></asp:RequiredFieldValidator>
                        </div>
                        <strong class="st-head" style="margin-left: 10px;">
                            <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                        </strong>
                        <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeTo" placeholder="Đến ngày" Style="margin-left: 10px!important"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                        <br />
                    </div>
                    <div class="filter-item">
                        <%-- <select name="ddlSalon" id="ddlSalon" class="form-control" style="margin: 12px 0; width: 190px;" onchange="getStylistBySalon()">
                        </select>--%>
                        <asp:UpdatePanel runat="server" ID="UPsalon">
                            <ContentTemplate>
                                <div class="filter-item">
                                    <asp:DropDownList ID="ddlSalon" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="width: 190px;" OnSelectedIndexChanged="Salon_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <asp:UpdatePanel runat="server" ID="UPStaff">
                        <ContentTemplate>
                            <div class="filter-item">
                                <asp:DropDownList ID="ddlStylist" runat="server" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                            </div>
                        </ContentTemplate>
                        <Triggers></Triggers>
                    </asp:UpdatePanel>
                    <%--<asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <a href="/admin/bao-cao/salary.html" class="st-head btn-viewdata">Reset Filter</a>
                    <div class="export-wp drop-down-list-wp">
                        <asp:Panel ID="fff" CssClass="st-head btn-viewdata"
                            ClientIDMode="Static" runat="server" onclick="EntryExportExcel('all');">
                            Xuất File Excel
                        </asp:Panel>
                    </div>--%>

                    <a href="javascript:void(0);" onclick="getThongKeAnh()" class="st-head btn-viewdata">Xem dữ liệu</a>
                    <a href="javascript:void(0);" onclick="location.href=location.href" class="st-head btn-viewdata">Reset Filter</a>

                </div>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>Thống kê số lượng check ảnh</strong>
                </div>


                <div class="row">
                    <table class="table-add table-listing" id="tblReport" style="float: left; width: 100%">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Số lần OK</th>
                                <th>Hình khối chưa cân</th>
                                <th>Đường cắt chưa nét</th>
                                <th>Hình khối chưa liên kết</th>
                                <th>Ngọn tóc quá dày</th>
                                <th>Cạo mai gáy lỗi</th>
                                <th>vuốt sáp lỗi</th>
                                <th>Ảnh thiếu/mờ</th>
                                <th>Bill có ảnh</th>
                                <th>Bill đã đánh giá</th>
                                <th>Bill chưa đánh giá</th>
                                <th>Bill chưa có ảnh</th>
                                <th>Tổng bill</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr id="totalReport">
                                <td><b>TỔNG</b></td>
                                <td class="td-SoLanOK">-</td>
                                <td class="td-HinhKhoiChuaChuan">-</td>
                                <td class="td-DuongCatChuaNet">-</td>
                                <td class="td-HinhKhoiChuaLienKet">-</td>
                                <td class="td-NgonTocQuaDay">-</td>
                                <td class="td-CaoMaiGayLoi">-</td>
                                <td class="td-VuotSapLoi">-</td>
                                <td class="td-AnhThieuMo">-</td>
                                <td class="td-BillCoAnh">-</td>
                                <td class="td-BillDaCheck">-</td>
                                <td class="td-BillChuaCheck">-</td>
                                <td class="td-BillChuaCoAnh">-</td>
                                <td class="td-TongBill">-</td>
                            </tr>

                        </tbody>
                    </table>


                </div>
            </div>
            <%-- END Listing --%>
        </div>

        <script src="../../../Assets/libs/canvasjs-1.9.8/canvasjs.min.js"></script>
        <script src="../../../Assets/libs/canvasjs-1.9.8/jquery.canvasjs.min.js"></script>
        <link href="/Assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <script src="/Assets/js/jquery.mCustomScrollbar.js"></script>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/ecmascript">
            jQuery(document).ready(function () {
                //getThongKeAnh();
                //GetListSalon();
                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });
            });

            //Bind list salon
            function GetListSalon() {
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/PreviewImage/Random_Salon_ImagesView.aspx/ReturnAllSalon",
                    data: '',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var select = $('#ddlSalon');

                        select.append('<option selected="selected" value="0">Chọn Salon</option>');
                        $.each(response.d, function (key, value) {
                            select.append('<option value="' + value.Id + '">' + value.Name + '</option>');
                        });
                    },
                    failure: function (response) { console.log(response.d); }
                });
            }

            function getStylistBySalon() {
                var salonId = $("#ddlSalon").val();
                var select = $('#ddlStylist');
                if (salonId != "0") {
                    addLoading();
                    $.ajax({
                        type: "POST",
                        url: "/GUI/FrontEnd/PreviewImage/Preview_Random_Salon_Images.aspx/ReturnAllStylistBySalon",
                        data: '{_SalonId: ' + salonId + '}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {

                            select.html('');
                            select.append('<option selected="selected" value="0">Chọn Stylist</option>');
                            $.each(response.d, function (key, value) {
                                select.append('<option value="' + value.Id + '">' + value.Fullname + '</option>');
                            });
                            removeLoading();
                        },
                        failure: function (response) { console.log(response.d); }
                    });
                } else {
                    select.html('');
                    select.append('<option selected="selected" value="0">Chọn Stylist</option>');
                }
            }

            function redirectPage(This, imageStatus) {
                var time = $('#TxtDateTimeFrom').val();
                var salonId = "";
                var stylistId = 0;
                if ($("#ddlSalon").val() == "0") {
                    salonId = $(This).attr("data-id");
                    stylistId = 0;
                }
                else {
                    salonId = $("#ddlSalon").val();
                    stylistId = $(This).attr("data-id");
                }
                if (time != undefined && time != null) {
                    window.location = '/anh-ngau-nhien?time=' + time + '&salonId=' + salonId + '&stylistId=' + stylistId + '&imageStatus=' + imageStatus;
                }
            }

            function getThongKeAnh() {
                addLoading();
                if ($('#ddlSalon').val() != 0) {
                    var salonId = $('#ddlSalon').val();
                }
                else {
                    var salonId = null;
                }

                if ($('#ddlStylist').val() != 0) {
                    var stylistId = $('#ddlStylist').val();
                }
                else {
                    var stylistId = null;
                }

                var timeFrom = ConvertDateToTypeMMddyyyy($('#TxtDateTimeFrom').val());

                if ($('#TxtDateTimeTo').val() != '') {
                    var timeTo = ConvertDateToTypeMMddyyyy($('#TxtDateTimeTo').val());
                }
                else {
                    var timeTo = ConvertDateToTypeMMddyyyy($('#TxtDateTimeFrom').val());
                }
                var dataJSON = {
                    timeFrom: timeFrom, timeTo: timeTo, salonId: salonId, stylistId: stylistId
                }
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/PreviewImage/Random_Salon_ImagesView.aspx/ImageReport",
                    data: JSON.stringify(dataJSON),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        if (response.d != null) {
                            $("#tblReport tbody").find(".tr-next").remove();
                            var row = "";
                            var totalOK = 0, totalHinhKhoiChuaChuan = 0, totalDuongCatChuaNet = 0, totalHinhKhoiChuaLK = 0,
                                totalNgonTocQuaDay = 0, totalCaoMaiGayLoi = 0, totalVuotSapLoi = 0, totalAnhThieuMo = 0,
                                totalCoAnh = 0, totalChecked = 0, totalNotCheck = 0, totalNotImg = 0, totalBill = 0;

                            $.each(response.d, function (key, value) {
                                row += '<tr class="tr-next"><td>' + value.Name + '</td>' +
                                        '<td>' + value.SoLanOK + '</td>' +
                                        '<td>' + value.HinhKhoiChuaChuan + '</td>' +
                                        '<td>' + value.DuongCatChuaNet + '</td>' +
                                        '<td>' + value.HinhKhoiChuaLienKet + '</td>' +
                                        '<td>' + value.NgonTocQuaDay + '</td>' +
                                        '<td>' + value.CaoMaiGayLoi + '</td>' +
                                        '<td>' + value.VuotSapLoi + '</td>' +
                                        '<td>' + value.AnhThieuMo + '</td>' +
                                        '<td>' + value.TongBillCoAnh + '</td>' +
                                        '<td>' + value.TongBillDaDanhGia + '</td>' +
                                        '<td>' + value.TongBillChuaDanhGia + '</td>' +
                                        '<td>' + value.TongBillChuaCoAnh + '</td>' +
                                        '<td>' + value.TongBill + '</td>' +
                                        '</tr>';
                                totalOK += value.SoLanOK;
                                totalHinhKhoiChuaChuan += value.HinhKhoiChuaChuan;
                                totalDuongCatChuaNet += value.DuongCatChuaNet;
                                totalHinhKhoiChuaLK += value.HinhKhoiChuaLienKet;
                                totalNgonTocQuaDay += value.NgonTocQuaDay;
                                totalCaoMaiGayLoi += value.CaoMaiGayLoi;
                                totalVuotSapLoi += value.VuotSapLoi;
                                totalAnhThieuMo += value.AnhThieuMo;
                                totalCoAnh += value.TongBillCoAnh;
                                totalChecked += value.TongBillDaDanhGia;
                                totalNotCheck += value.TongBillChuaDanhGia;
                                totalNotImg += value.TongBillChuaCoAnh;
                                totalBill += value.TongBill;
                            });

                            $(row).insertAfter("#totalReport");
                            $('.td-SoLanOK').text(totalOK);
                            $('.td-HinhKhoiChuaChuan').text(totalHinhKhoiChuaChuan);
                            $('.td-DuongCatChuaNet').text(totalDuongCatChuaNet);
                            $('.td-HinhKhoiChuaLienKet').text(totalHinhKhoiChuaLK);
                            $('.td-NgonTocQuaDay').text(totalNgonTocQuaDay);
                            $('.td-CaoMaiGayLoi').text(totalCaoMaiGayLoi);
                            $('.td-VuotSapLoi').text(totalVuotSapLoi);
                            $('.td-AnhThieuMo').text(totalAnhThieuMo);
                            $('.td-BillCoAnh').text(totalCoAnh);
                            $('.td-BillDaCheck').text(totalChecked);
                            $('.td-BillChuaCheck').text(totalNotCheck);
                            $('.td-BillChuaCoAnh').text(totalNotImg);
                            $('.td-TongBill').text(totalBill);

                            if (response.d.length <= 1) {
                                $("#totalReport").addClass("hidden");
                            }
                            else {
                                $("#totalReport").removeClass("hidden");
                            }
                        }
                        

                        removeLoading();
                    },
                    failure: function (response) { console.log(response.d); }
                });
            }

        </script>
    </asp:Panel>
</asp:Content>





