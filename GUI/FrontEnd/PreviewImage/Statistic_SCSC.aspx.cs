﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Linq.Expressions;
using ExportToExcel;
using Newtonsoft.Json.Linq;
using Project.Helpers;
using System.Data.Objects;
using System.Data.SqlClient;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.Interface;

namespace _30shine.GUI.FrontEnd.PreviewImage
{
    public partial class Statistic_SCSC : System.Web.UI.Page
    {
        protected string Permission = "";
        protected bool Perm_ShowSalon = false;
        private bool Perm_Access = false;

        public CultureInfo culture = new CultureInfo("vi-VN");
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckPermission();
            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = DateTime.Now.ToString("dd/MM/yyyy");
                Library.Function.bindSalon_NotHoiQuan(new List<DropDownList> { ddlSalon }, Perm_ShowSalon);
                bindStaff();
                //Library.Function.bindSalon_NotHoiQuan(new List<DropDownList> { salon }, Perm_ShowSalon);
                //Bind_HairName();
            }
        }

        /// <summary>
        /// Get list salon
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static List<Tbl_Salon> ReturnAllSalon()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Tbl_Salon.Where(a => a.IsDelete == 0 && a.Publish == true).OrderBy(o => o.Order).ToList();
                return lst;
            }
        }
        /// <summary>
        /// Bind data hair Style error
        /// Author: QuynhDD
        /// </summary>
        /// <param name="timeFrom"></param>
        /// <param name="timeTo"></param>
        /// <param name="staffID"></param>
        /// <returns></returns>
        [WebMethod]
        public static string BindHairStyle(string timeFrom, string timeTo, int staffID)
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = db.Store_SCSC_BindHairStyle(timeFrom, timeTo, staffID).ToList();
                return new JavaScriptSerializer().Serialize(list);
            }
        }
        /// <summary>
        ///  Báo cáo điểm SCSC TB vị trí Stylist
        ///  Author: QuynhDD
        /// </summary>
        /// <param name="timeFrom"></param>
        /// <param name="timeTo"></param>
        /// <param name="salonID"></param>
        /// <param name="hairStyleID"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        //[WebMethod]
        //public static string BindHairStyle_V1(string timeFrom, string timeTo, int salonID, int hairStyleID, int order)
        //{
        //    using (var db = new Solution_30shineEntities())
        //    {
        //        var list = new List<Store_SCSC_BindHairStyle_V1_Result>();
        //        if (order == 1)
        //        {
        //            list = db.Store_SCSC_BindHairStyle_V1(timeFrom, timeTo, hairStyleID, salonID).OrderByDescending(w => w.PointSCSC_TB).ToList();
        //        }
        //        else
        //        {
        //            list = db.Store_SCSC_BindHairStyle_V1(timeFrom, timeTo, hairStyleID, salonID).OrderBy(w => w.TimeCatTB).ToList();
        //        }
        //        return new JavaScriptSerializer().Serialize(list);
        //    }
        //}
        /// <summary>
        /// Get list Stylist by salon
        /// </summary>
        /// <param name="_SalonId"></param>
        /// <returns></returns>
        [WebMethod]
        public static List<Staff> ReturnAllStylistBySalon(int _SalonId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Staffs.Where(a => a.IsDelete == 0 && a.Active == 1 && a.Type == 1 && a.SalonId == _SalonId).ToList();
                return lst;
            }
        }
        ///// <summary>
        ///// Bind kiểu tóc
        ///// </summary>
        //private void Bind_HairName()
        //{
        //    using (var db = new Solution_30shineEntities())
        //    {
        //        var list = db.Api_HairMode.Where(w => w.IsDelete == false && w.Publish == true).ToList();
        //        if (list.Count > 0)
        //        {
        //            for (int i = 0; i < list.Count; i++)
        //            {
        //                ddlHairStyle.Items.Add(new ListItem(list[i].Title, list[i].Id.ToString()));
        //            }
        //            ddlHairStyle.DataBind();
        //        }
        //    }
        //}
        private void bindStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                var salon = Convert.ToInt32(ddlSalon.SelectedValue);
                var staff = new List<Staff>();
                var whereSalon = "";
                var sql = "";

                if (salon > 0)
                {
                    whereSalon = " and SalonId = " + salon;
                }
                sql = @"select * from Staff 
                        where IsDelete != 1 and Active = 1  and Type = 1 and (IsHoiQuan = 0 or IsHoiQuan is null)
                        and (isAccountLogin != 1 or isAccountLogin is null)" + whereSalon;
                staff = db.Staffs.SqlQuery(sql).ToList();

                var first = new Staff();
                first.Id = 0;
                first.Fullname = "Chọn nhân viên";
                staff.Insert(0, first);

                ddlStylist.DataTextField = "Fullname";
                ddlStylist.DataValueField = "Id";
                ddlStylist.DataSource = staff;
                ddlStylist.DataBind();
            }
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //string[] Allow = new string[] { "root", "admin", "salonmanager" };
                //string[] AllowSalon = new string[] { "root", "admin" };
                //var Permission = Session["User_Permission"].ToString().Trim();
                //if (Array.IndexOf(Allow, Permission) != -1)
                //{
                //    Perm_Access = true;
                //}
                //if (Array.IndexOf(AllowSalon, Permission) != -1)
                //{
                //    Perm_ShowSalon = true;
                //}

                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();

            }
            // Call execute function
            //ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        protected void Salon_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindStaff();
        }
        // Binf SCSC chon 1 salon
        protected void binDataOnlySalon()
        {
            using (var db = new Solution_30shineEntities())
            {
                if (TxtDateTimeFrom.Text != "")
                {
                    DateTime _FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                    DateTime _ToDate;
                    if (TxtDateTimeTo.Text != "")
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                    }
                    else
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                    }
                    int SalonId = Convert.ToInt32(ddlSalon.SelectedValue);
                    int StylistID = Convert.ToInt32(ddlStylist.SelectedValue);

                    var LstData = db.SCSC_BindStaffWhereSalon_Store(_FromDate, _ToDate, SalonId, StylistID).ToList();
                    int _countLstDate = LstData.Count();
                    var item = new SCSC_BindStaffWhereSalon_Store_Result();
                    item.Fullname = "Tổng";
                    item.TotalBill = LstData.Sum(a => a.TotalBill);
                    item.BillNotImages = LstData.Sum(a => a.BillNotImages);
                    item.TotalBillChuaDanhGia = LstData.Sum(a => a.TotalBillChuaDanhGia);
                    item.TotalBillErrorShape = LstData.Sum(a => a.TotalBillErrorShape);
                    item.TotalBillErrorConnective = LstData.Sum(a => a.TotalBillErrorConnective);
                    item.TotalBillErrorSharpNess = LstData.Sum(a => a.TotalBillErrorSharpNess);
                    item.TotalBillErrorCompletion = LstData.Sum(a => a.TotalBillErrorCompletion);
                    item.PointSCSC_TB = Math.Round((Double)(LstData.Sum(a => a.PointSCSC_TB)) / _countLstDate, 2);
                    item.Total_SCSC_Error = Math.Round((Double)(LstData.Sum(a => a.Total_SCSC_Error)) / _countLstDate, 2);
                    LstData.Insert(0, item);
                    rptOnlySalon.DataSource = LstData;
                    rptOnlySalon.DataBind();

                }
            }
        }

        //Bind SCSC chọn toàn salon
        protected void binDataAllSalon()
        {
            using (var db = new Solution_30shineEntities())
            {
                if (TxtDateTimeFrom.Text != "")
                {
                    DateTime _FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                    DateTime _ToDate;
                    if (TxtDateTimeTo.Text != "")
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                    }
                    else
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                    }
                    var LstData = db.SCSC_TotalAllSalon_Store(_FromDate, _ToDate).ToList();
                    int _countLstData = LstData.Count();
                    var item = new SCSC_TotalAllSalon_Store_Result();
                    item.ShortName = "Tổng";
                    item.TongBill = LstData.Sum(a => a.TongBill);
                    item.BillChuaDanhGia = LstData.Sum(a => a.BillChuaDanhGia);
                    item.TongkoAnh = LstData.Sum(a => a.TongkoAnh);
                    item.TotalShapeError = LstData.Sum(a => a.TotalShapeError);
                    item.TotalConnectiveError = LstData.Sum(a => a.TotalConnectiveError);
                    item.TotalSharpNessError = LstData.Sum(a => a.TotalSharpNessError);
                    item.TotalCompletionError = LstData.Sum(a => a.TotalCompletionError);
                    item.PointSCSC_TB = Math.Round((Double)(LstData.Sum(a => a.PointSCSC_TB)) / _countLstData, 2);
                    item.Total_SCSC_Error = Math.Round((Double)(LstData.Sum(a => a.Total_SCSC_Error)) / _countLstData, 2);
                    LstData.Insert(0, item);
                    rptAllSalon.DataSource = LstData;
                    rptAllSalon.DataBind();
                }

            }
        }

        protected void _BtnClick(Object sender, EventArgs e)
        {
            int SalonId = Convert.ToInt32(ddlSalon.SelectedValue);
            if (SalonId == 0)
            {
                binDataAllSalon();
                rptOnlySalon.DataSource = "";
                rptOnlySalon.DataBind();
            }
            else
            {
                //rptOnlySalon.Style.Add("display", "block");
                binDataOnlySalon();
                rptAllSalon.DataSource = "";
                rptAllSalon.DataBind();
            }
            RemoveLoading();
        }

        /// <summary>
        /// RemoveLoading
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
    }
}