﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SCSC_Preview_Random_Salon_Images.aspx.cs" MasterPageFile="~/TemplateMaster/SCSCMaster.master" Inherits="_30shine.GUI.FrontEnd.PreviewImage.SCSC_Preview_Random_Salon_Images" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMainSCSC" runat="server">

    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false" Style="text-align: center; padding: 50px 0px; color: red; border-bottom: 1px solid #ddd;"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="preview-img">
            <%--<div class="heading-title">Xem ảnh lịch sử</div>--%>
            <!-- Filter-->
            <div class="top-filter">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <asp:DropDownList ID="ddlSalon" CssClass="select form-control" runat="server" ClientIDMode="Static" AutoPostBack="true" OnSelectedIndexChanged="Salon_SelectedIndexChanged"></asp:DropDownList>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <asp:DropDownList ID="ddlStaff" runat="server" CssClass="select form-control" ClientIDMode="static"></asp:DropDownList>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <input runat="server" name="txtToDate" type="text" value="" id="txtToDate" class="form-control txtDateTime" placeholder="Ngày ..." />

                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <input type="button" id="btnTimKiem" class="form-control btn-default btnSearch" value="Tìm" />
                </div>
            </div>
            <!-- Customer info-->
            <div class="top-info" style="margin-bottom: 5px; width: 100%; float: left;">
                <div class="salon-name">KH: &nbsp<strong id="pre-cus"></strong>   &nbsp&nbsp&nbsp&nbsp&nbsp &nbsp &nbsp SĐT: &nbsp<strong id="pre-date"></strong> </div>


                <div class="salon-name" style="display: none;">Bill ID: &nbsp<strong id="pre-id"></strong></div>
                <div class="salon-name " id="Service"><strong>Dịch vụ : </strong></div>
                <%-- <div class="salon-name" id="Service">
                </div>--%>

                <%--<div class="salon-name">DV: &nbsp<strong id="pre-dv"></strong></div>--%>
                <%--<div class="salon-name" id="status"></div>--%>
            </div>
            <!-- Kiểu tóc-->
            <fieldset>
                <%--  <legend><b>Kiểu tóc</b></legend>--%>
                <select class="col-lg-6 col-md-6 col-sm-6 col-xs-12 form-control btnHairList" id="sltHairStyle" style="padding-top: 0; text-align: center">
                    <option value="0">--Chọn kiểu tóc--</option>
                </select>

            </fieldset>
            <!-- Ảnh chụp khách-->
            <div class="top4-img">
                <div class="container">
                    <div class="row">
                        <div class="col1">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 img-item" data-toggle="modal" data-target="#myModal" data-slide-to="0">
                                <img src="https://30shine.com/images/1.jpg" alt="" id="pre-img1" class="image" />
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 img-item" data-toggle="modal" data-target="#myModal" data-slide-to="1">
                                <img src="https://30shine.com/images/2.jpg" alt="" id="pre-img2" class="image" />
                            </div>
                        </div>
                        <div class="col2">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 img-item" data-toggle="modal" data-target="#myModal" data-slide-to="2">
                                <img src="https://30shine.com/images/3.jpg" alt="" id="pre-img3" class="image" />
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 img-item" data-toggle="modal" data-target="#myModal" data-slide-to="3">
                                <img src="https://30shine.com/images/4.jpg" alt="" id="pre-img4" class="image" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Lỗi cắt-->
            <div class="wp_scsc">
                <div class="wp_title_scsc">
                    <div>
                        <p class="text_title_scsc">SCSC (<span class="span_score">score: &nbsp;</span><span id="TotalPoinSCSC">0</span>/<%=TotalPointCate %>) </p>
                        <p class="triangle-top-left"></p>
                    </div>
                    <div>
                        <a class="text_title_scsc ImgErrorSCSC" id="ImgError" onclick="ErrorImage($(this), 0)">Ảnh thiếu MỜ/LỆCH</a>
                        <p class="triangle-top-left"></p>
                    </div>
                </div>
                <div class="ctn_scsc scsc_shape" id="SHAPE">
                    <div class="wp_left_scsc">
                        <p class="p_cate_parent_scsc">
                            1.SHAPE
                        </p>
                    </div>
                    <ul class="ul_children_scsc " id="uate">
                        <asp:Repeater ID="rpt_Shape" runat="server">
                            <ItemTemplate>
                                <li class="li_children_scsc li_children_scsc<%#Eval("SCSC_Cate_IDCate") %>" data-catid="<%#Eval("SCSC_Cate_IDCate") %>" data-id="<%#Eval("ID_SCSC_Cate") %>" data-point="<%#Eval("SCSC_Cate_Point") %>" onclick="SCSC_CheckError($(this), 0)">
                                    <div class="wp_img">
                                        <img class="scsc_img img_often" src="<%#Eval("SCSC_Cate_Image") %>" />
                                        <img class="scsc_img img_active" src="<%#Eval("SCSC_Cate_Image_Active") %>" />
                                    </div>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
                <div class="ctn_scsc" id="CONNECTIVE">
                    <div class="wp_left_scsc">
                        <p class="p_cate_parent_scsc">
                            2.CONNECTIVE
                        </p>
                    </div>
                    <ul class="ul_children_scsc">
                        <asp:Repeater ID="rpt_Connective" runat="server">
                            <ItemTemplate>
                                <li class="li_children_scsc li_children_scsc<%#Eval("SCSC_Cate_IDCate") %>" data-catid="<%#Eval("SCSC_Cate_IDCate") %>" data-id="<%#Eval("ID_SCSC_Cate") %>" data-point="<%#Eval("SCSC_Cate_Point") %>" onclick="SCSC_CheckError($(this), 0)">
                                    <div class="wp_img">
                                        <img class="scsc_img img_often" src="<%#Eval("SCSC_Cate_Image") %>" />
                                        <img class="scsc_img img_active" src="<%#Eval("SCSC_Cate_Image_Active") %>" />
                                    </div>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
                <div class="ctn_scsc" id="SHARPNESS">
                    <div class="wp_left_scsc">
                        <p class="p_cate_parent_scsc">
                            3.SHARPNESS
                        </p>
                    </div>
                    <ul class="ul_children_scsc">
                        <asp:Repeater ID="rptSharpness" runat="server">
                            <ItemTemplate>
                                <li class="li_children_scsc li_children_scsc<%#Eval("SCSC_Cate_IDCate") %>" data-catid="<%#Eval("SCSC_Cate_IDCate") %>" data-id="<%#Eval("ID_SCSC_Cate") %>" data-point="<%#Eval("SCSC_Cate_Point") %>" onclick="SCSC_CheckError($(this), 0)">
                                    <div class="wp_img">
                                        <img class="scsc_img img_often" src="<%#Eval("SCSC_Cate_Image") %>" />
                                        <img class="scsc_img img_active" src="<%#Eval("SCSC_Cate_Image_Active") %>" />
                                    </div>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
                <div class="ctn_scsc" id="COMPLETION">
                    <div class="wp_left_scsc">
                        <p class="p_cate_parent_scsc">
                            4.COMPLETION
                        </p>
                    </div>
                    <ul class="ul_children_scsc">
                        <asp:Repeater ID="rpt_Completion" runat="server">
                            <ItemTemplate>
                                <li class="li_children_scsc li_children_scsc<%#Eval("SCSC_Cate_IDCate") %>" data-catid="<%#Eval("SCSC_Cate_IDCate") %>" data-id="<%#Eval("ID_SCSC_Cate") %>" data-point="<%#Eval("SCSC_Cate_Point") %>" onclick="SCSC_CheckError($(this), 0)">
                                    <div class="wp_img">
                                        <img class="scsc_img img_often" src="<%#Eval("SCSC_Cate_Image") %>" />
                                        <img class="scsc_img img_active" src="<%#Eval("SCSC_Cate_Image_Active") %>" />
                                    </div>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>

                    </ul>
                </div>
                <!-- Ảnh chụp khách uốn-->
                <div class="top4-img">
                    <div class="container">
                        <div class="row">
                            <div class="col1">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 img-item" data-toggle="modal" data-target="#myModal" data-slide-to="4">
                                    <img src="https://30shine.com/images/1.jpg" alt="" id="pre-image-1" class="image" />
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 img-item" data-toggle="modal" data-target="#myModal" data-slide-to="5">
                                    <img src="https://30shine.com/images/2.jpg" alt="" id="pre-image-2" class="image" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wp_title_scsc">
                    <div>
                        <p class="text_title_scsc">KCS Uốn (<span class="span_score">score: &nbsp;</span><span id="TotalPointCurling">0</span>/<%=TotalPointCurling %>) </p>
                        <p class="triangle-top-left"></p>
                    </div>
                    <div>
                        <a class="text_title_scsc ImgErrorSCSC" id="ImgErrorCurling" onclick="ErrorImage($(this), 1)">Ảnh thiếu MỜ/LỆCH</a>
                        <p class="triangle-top-left"></p>
                    </div>
                </div>
                <div class="ctn_scsc scsc_shape" id="HairTip">
                    <div class="wp_left_scsc">
                        <p class="p_cate_parent_scsc">
                            1.NGỌN TÓC
                        </p>
                    </div>
                    <ul class="ul_children_scsc">
                        <asp:Repeater ID="rptHairTip" runat="server">
                            <ItemTemplate>
                                <li class="li_children_scsc li_children_scsc<%#Eval("SCSC_Cate_IDCate") %>" data-catid="<%#Eval("SCSC_Cate_IDCate") %>" data-id="<%#Eval("ID_SCSC_Cate") %>" data-point="<%#Eval("SCSC_Cate_Point") %>" onclick="SCSC_CheckError($(this), 1)">
                                    <div class="wp_img">
                                        <img class="scsc_img img_often" src="<%#Eval("SCSC_Cate_Image") %>" />
                                        <img class="scsc_img img_active" src="<%#Eval("SCSC_Cate_Image_Active") %>" />
                                    </div>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
                <div class="ctn_scsc" id="HairRoot">
                    <div class="wp_left_scsc">
                        <p class="p_cate_parent_scsc">
                            2.CHÂN TÓC
                        </p>
                    </div>
                    <ul class="ul_children_scsc">
                        <asp:Repeater ID="rptHairRoot" runat="server">
                            <ItemTemplate>
                                <li class="li_children_scsc li_children_scsc<%#Eval("SCSC_Cate_IDCate") %>" data-catid="<%#Eval("SCSC_Cate_IDCate") %>" data-id="<%#Eval("ID_SCSC_Cate") %>" data-point="<%#Eval("SCSC_Cate_Point") %>" onclick="SCSC_CheckError($(this), 1)">
                                    <div class="wp_img">
                                        <img class="scsc_img img_often" src="<%#Eval("SCSC_Cate_Image") %>" />
                                        <img class="scsc_img img_active" src="<%#Eval("SCSC_Cate_Image_Active") %>" />
                                    </div>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
                <div class="ctn_scsc" id="HairWave">
                    <div class="wp_left_scsc">
                        <p class="p_cate_parent_scsc">
                            3.SÓNG TÓC
                        </p>
                    </div>
                    <ul class="ul_children_scsc">
                        <asp:Repeater ID="rptHairWave" runat="server">
                            <ItemTemplate>
                                <li class="li_children_scsc li_children_scsc<%#Eval("SCSC_Cate_IDCate") %>" data-catid="<%#Eval("SCSC_Cate_IDCate") %>" data-id="<%#Eval("ID_SCSC_Cate") %>" data-point="<%#Eval("SCSC_Cate_Point") %>" onclick="SCSC_CheckError($(this), 1)">
                                    <div class="wp_img">
                                        <img class="scsc_img img_often" src="<%#Eval("SCSC_Cate_Image") %>" />
                                        <img class="scsc_img img_active" src="<%#Eval("SCSC_Cate_Image_Active") %>" />
                                    </div>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
                <div class="wp_img_error">
                    <div class="wp_note_Error">
                        <textarea id="txtNoteByStylist" cols="20" rows="1" style="margin-bottom: 5px;" class="form-control ErrorNote" placeholder="Ghi chú Stylist"></textarea>
                        <textarea id="txtErrorNote" class="form-control ErrorNote" placeholder="SCSC note" rows="2"></textarea>
                    </div>
                </div>
                <!-- btn-->
                <div class="img-nav" style="margin-bottom: 40px;">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-4 col-sm-4">
                                <input type="button" id="btnPreview" class="form-control btn-default" value="Trước" />
                            </div>
                            <div class="col-xs-4 col-sm-4 fix_style_count_item">
                                &nbsp(Đang xem hóa đơn <strong id="current-index">0</strong>/<strong id="total-index">0</strong>)&nbsp
                            <br />
                                &nbsp(Hóa đơn chưa check <strong id="total-billcheck">0</strong>)&nbsp
                            <input style="display: none" type="text" id="txtsearch" style="width: 50px" onchange="jQuery('#btnTimKiem').click();" onkeypress="return ValidateKeypress(/\d/,event);" />
                            </div>
                            <div class="col-xs-4 col-sm-4">

                                <input type="button" id="btnNext" class="form-control btn-default" value="Sau" />
                            </div>
                        </div>
                    </div>
                </div>
                <%--<div class="wp_click_submit">

                <a class="a_submit_scsc" onclick="InsertOrUpdateSCSC_CheckError()">Hoàn tất
                </a>
                <div class="alert alert-success" id="danhgia">
                    <strong>Đánh giá thành công.</strong>
                </div>
            </div>--%>

                <input type="hidden" value="0" id="hdfIndexPage" />
                <input type="hidden" value="0" id="HDF_BillID" />
            </div>

            <!-- Pop up-->
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog" id="modalDialog">
                    <!-- Modal content-->
                    <div class="modal-content" id="modalContent">
                        <div class="modal-header" id="modalHeader">
                            <h4 class="modal-title">Check lỗi cắt</h4>
                        </div>
                        <div class="modal-body">
                            <div id="myCarousel" class="carousel" data-ride="carousel" data-interval="false">
                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                    <li data-target="#myCarousel" data-slide-to="1"></li>
                                    <li data-target="#myCarousel" data-slide-to="2"></li>
                                    <li data-target="#myCarousel" data-slide-to="3"></li>
                                    <li data-target="#myCarousel" data-slide-to="4"></li>
                                    <li data-target="#myCarousel" data-slide-to="5"></li>
                                </ol>
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner" role="listbox" id="canvasWrap">
                                    <div class="item active" id="div-image1">
                                        <%--<canvas id="pre-img1-can" data-order="1" style="background-repeat: no-repeat; background-size: 100% auto!important;" title="Before"></canvas>--%>
                                        <canvas id="pre-img1-can" data-order="1" style="background-repeat: no-repeat; background-size: 100% 100%;" title="Before"></canvas>
                                    </div>
                                    <div class="item" id="div-image2">
                                        <canvas id="pre-img2-can" data-order="2" style="background-repeat: no-repeat; background-size: 100% 100%;" title="Left"></canvas>
                                    </div>
                                    <div class="item" id="div-image3">
                                        <canvas id="pre-img3-can" data-order="3" style="background-repeat: no-repeat; background-size: 100% 100%;" title="Right"></canvas>
                                    </div>
                                    <div class="item" id="div-image4">
                                        <canvas id="pre-img4-can" data-order="4" style="background-repeat: no-repeat; background-size: 100% 100%;" title="After"></canvas>
                                    </div>
                                    <div class="item" id="div-image5">
                                        <canvas id="pre-image-curling-1-can" data-order="5" style="background-repeat: no-repeat; background-size: 100% 100%;" title="Before"></canvas>
                                    </div>
                                    <div class="item" id="div-image6">
                                        <canvas id="pre-image-curling-2-can" data-order="6" style="background-repeat: no-repeat; background-size: 100% 100%;" title="After"></canvas>
                                    </div>
                                </div>
                                <!-- Left and right controls -->
                                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev" id="prev-Slide">
                                    <%--<i class="fa fa-arrow-left" aria-hidden="true"></i>--%>
                                    <span class="fa fa-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next" id="next-Slide">
                                    <span class="fa fa-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                    <%--<i class="fa fa-arrow-right" aria-hidden="true"></i>--%>
                                </a>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="row">
                                <div class="col-xs-1"></div>
                                <div class="col-xs-2">
                                    <i class="fa fa-plus-circle btnAddCycle" aria-hidden="true" onclick="addCycleToCanvas()" title="Thêm vòng lỗi"></i>
                                </div>
                                <div class="col-xs-2">
                                    <i class="fa fa-minus-circle btnUndoCycle" aria-hidden="true" onclick="undoCycle()" title="undo"></i>
                                </div>
                                <div class="col-xs-2">
                                    <i class="fa fa-floppy-o btnSaveError" aria-hidden="true" onclick="saveCheckedImages()" title="Lưu ảnh"></i>
                                </div>
                                <div class="col-xs-2">
                                    <i class="fa fa-refresh btnReset" aria-hidden="true" onclick="refreshCheckedImage()" title="Sửa lại"></i>
                                </div>
                                <div class="col-xs-2">
                                    <i class="fa fa-times-circle" aria-hidden="true" onclick="closeCanvas()" data-dismiss="modal" title="Đóng"></i>
                                </div>
                                <div class="col-xs-1"></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <%--Hidden Field AV--%>
            <input id="HDFBill_ID" type="hidden" value="0" />
            <input id="HDF_ID_SCSC" type="hidden" value="0" />
            <input id="HDFErrorImage" type="hidden" value="0" />
            <input id="HDFErrorImageCurling" type="hidden" value="0" />
            <input id="HDFShapMaxPoint" type="hidden" value="<%=ShapMaxPoint %>" />
            <input id="HDFConnectiveMaxPoint" type="hidden" value="<%=ConnectiveMaxPoint %>" />
            <input id="HDFSharpNessMaxPoint" type="hidden" value="<%=SharpNessMaxPoint %>" />
            <input id="HDFCompletionMaxPoint" type="hidden" value="<%=CompletionMaxPoint %>" />
        </div>
        <!-- Loading-->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
        <script src="/Assets/js/sweetalert.min.js"> </script>
        <link rel="stylesheet" type="text/css" href="/Assets/css/sweetalert.css" />
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 100% !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 35px !important;
                padding-top: 1px !important;
            }

            .top4-img .img-item img {
                width: 306px !important;
                width: 408px !important;
            }
        </style>
        <script type="text/ecmascript">
            var OBJ = "";
            var objCount = {};
            var _List = [];
            var totalBill = 0;
            var checkCurling = false;
        //var listElement = <%= listElement_Perm %>;

            $(document).ready(function () {
                startLoading();
                //GetErrorList();
                GetListHairMode();
                //GetFaceType();
                cusId = "";
                /* fn tìm kiếm bill ảnh theo điều kiện lọc */
                $('#btnTimKiem').click(function () {
                    //Reset Field
                    $("#total-billcheck").html("0");
                    $("#HDFBill_ID").val(0);
                    $("#HDF_ID_SCSC").val(0);
                    $("HDFErrorImage").val(0);
                    $("HDFErrorImageCurling").val(0);
                    //initCircles();
                    var _SalonId = $("#ddlSalon").val();
                    if (_SalonId == "0") {
                        swal("Bạn chưa chọn salon.", "", "error");
                        return;
                    }
                    var _StylistId = $("#ddlStaff").val();
                    var _ToDate = $("#CtMainSCSC_txtToDate").val();
                    if (_ToDate == "") {
                        swal("Bạn chưa chọn ngày.", "", "error");
                        return;
                    }
                    var value = $("#txtsearch").val();
                    if (value == "")
                        value = 0;

                    startLoading();
                    $.ajax({
                        type: "POST",
                        url: "/GUI/FrontEnd/PreviewImage/SCSC_Preview_Random_Salon_Images.aspx/GetRandomBillImage",
                        data: '{_Date: "' + _ToDate + '", _SalonId: ' + _SalonId + ', _StylistId: ' + _StylistId + ',value:' + value + ', _imageStatus:"" }',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            _ListPreviewImg = [];

                            $.each(response.d, function (key, value) {
                                //console.log(response);
                                objPreviewImg = {};
                                objPreviewImg.Id = value.Id;
                                $("#HDF_BillID").val(objPreviewImg.Id);
                                objPreviewImg.CustomerPhone = value.CustomerPhone;
                                objPreviewImg.CustomerName = value.CustomerName;
                                objPreviewImg.Img1 = value.Img1;
                                objPreviewImg.Img2 = value.Img2;
                                objPreviewImg.Img3 = value.Img3;
                                objPreviewImg.Img4 = value.Img4;
                                objPreviewImg.ImageStatusId = value.ImageStatusId;
                                objPreviewImg.ErrorNote = value.ErrorNote;
                                objPreviewImg.CustomerId = value.CustomerId;
                                objPreviewImg.ServiceIds = value.ServiceIds;
                                objPreviewImg.ID_SCSC = value.ID_SCSC;
                                objPreviewImg.ImageError = value.ImageError;
                                objPreviewImg.NoteError = value.NoteError;
                                objPreviewImg.Shape_ID = value.Shape_ID;
                                objPreviewImg.SharpNess_ID = value.SharpNess_ID;
                                objPreviewImg.ComPlatetion_ID = value.ComPlatetion_ID;
                                objPreviewImg.ConnectTive_ID = value.ConnectTive_ID;
                                objPreviewImg.TotalPointSCSC = value.TotalPointSCSC;
                                objPreviewImg.ImgId = value.ImgId;
                                objPreviewImg.ImageCurlingBefore = value.ImageCurlingBefore;
                                objPreviewImg.ImageCurlingAfter = value.ImageCurlingAfter;
                                _ListPreviewImg.push(objPreviewImg);
                                // Set giá trị mặc định billID;
                                if (key == 0) {
                                    setBillID(value.Id);
                                    bindBillStatus(value.ImageStatusId);
                                }
                            });

                            if (_ListPreviewImg.length > 0) {
                                SetPreviewInfo(0);
                                $('#hdfIndexPage').val("0");
                                $('#total-index').text(_ListPreviewImg.length);
                                $('#current-index').text('1');

                            }
                            _List = [];
                            $.each(response.d, function (key, value) {
                                objCount = {};
                                if (value.PointSCSC == -1) {
                                    objCount.PointSCSC = value.PointSCSC;
                                    _List.push(objCount);
                                }
                            });
                            if (_List.length > 0) {
                                $("#total-billcheck").text(_List.length);
                                totalBill = parseInt(_List.length);
                            }
                            $("#txtsearch").html(' ');
                            finishLoading();
                        },

                        failure: function (response) { console.log(response.d); }
                    });

                });

                /* fn khi chuyển tiếp sang bill tiếp theo */
                $('#btnNext').click(function () {
                    //Insert and update SCSC
                    InsertOrUpdateSCSC_CheckError("Next");
                    var totalIndex = parseInt(_ListPreviewImg.length);
                    var index = parseInt($('#hdfIndexPage').val());

                    if ((totalIndex > 0) && (index < totalIndex - 1)) {
                        SetPreviewInfo(index + 1);
                        $('#hdfIndexPage').val(index + 1);
                        $('#current-index').text((index + 2));
                    }
                    cusHairId = "";
                });

                /* fn khi quay lại bill trước đó */
                $('#btnPreview').click(function () {
                    //Insert and update SCSC
                    InsertOrUpdateSCSC_CheckError("Preview");
                    var totalIndex = parseInt(_ListPreviewImg.length);
                    var index = parseInt($('#hdfIndexPage').val());

                    if ((totalIndex > 0) && (index > 0)) {
                        SetPreviewInfo(index - 1);
                        $('#hdfIndexPage').val(index - 1);
                        $('#current-index').text((index));
                    }
                    cusHairId = "";

                });

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    scrollMonth: false,
                    scrollInput: false,
                    onChangeDateTime: function (dp, $input) { }
                });

                /* mở popup ảnh chỉ tiết */
                $("#myModal").on('shown', function () {
                    initCircles();
                    var $activeCanvas = $(".carousel-inner .item.active").find("canvas");
                    var canvasId = $activeCanvas.attr("id");
                    canvas = document.getElementById(canvasId);
                    if (canvas != undefined) {
                        ctx = canvas.getContext("2d");
                    }
                    for (var j = 0; j < Circles.length; j++) {
                        if (Circles[j].idCustomer == $('#pre-id').text() && Circles[j].key == canvasId) {
                            for (var i = Circles[j].circles.length - 1; i >= 0; i--) {
                                Circles[j].circles[i].draw();
                            }
                        }
                    }
                });

                /* quay về slide trước khi show popup */
                $("#prev-Slide").click(function () {
                    var $activeCanvas = $(".carousel-inner .item.active").prev().find("canvas");

                    var canvasId = $activeCanvas.attr("id");
                    canvas = document.getElementById(canvasId);
                    if (canvas != undefined) {
                        ctx = canvas.getContext("2d");
                    }
                    for (var j = 0; j < Circles.length; j++) {
                        if (Circles[j].idCustomer == $('#pre-id').text() && Circles[j].key == canvasId) {
                            for (var i = Circles[j].circles.length - 1; i >= 0; i--) {
                                Circles[j].circles[i].draw();
                            }
                        }
                    }
                });

                /* chuyển tiếp silde sau khi show popup */
                $("#next-Slide").click(function () {
                    //var $activeCanvas = $(".carousel-inner .item.active").find("canvas").next();
                    var $activeCanvas = $(".carousel-inner .item.active").next().find("canvas");

                    var canvasId = $activeCanvas.attr("id");
                    canvas = document.getElementById(canvasId);
                    if (canvas != undefined) {
                        ctx = canvas.getContext("2d");
                    }
                    for (var j = 0; j < Circles.length; j++) {
                        if (Circles[j].idCustomer == $('#pre-id').text() && Circles[j].key == canvasId) {
                            for (var i = Circles[j].circles.length - 1; i >= 0; i--) {
                                Circles[j].circles[i].draw();
                            }
                        }
                    }
                })
            });
            function testObject(jsonStrig) {

                var obj = JSON.parse(jsonStrig);
                var arr = [];
                for (var i = 0; i < obj.length; i++) {
                    arr.push(obj[i].Name);
                }
                var result = arr.join();
                $("#pre-dv").text(result);
            }

            /* Khai báo biến */
            var _ListPreviewImg = [];
            var objPreviewImg = {};
            var billID;
            var cusHairId;
            var hairStyleId;
            //var faceTypeId;

            /* Check quyền element */
            function checkElement() {
                for (var j = 0; j < listElement.length; j++) {
                    var nameElement = listElement[j].ElementName;
                    var enabled = listElement[j].Enable;
                    var type = listElement[j].Type;
                    if (type == "hidden" && enabled == true) {
                        $("." + nameElement).addClass('hidden');
                    }
                    else if (type == "disable" && enabled == true && nameElement.substr(0, 3) == "btn") {
                        $("." + nameElement).addClass('disabled');
                    }
                    else {
                        $("." + nameElement).prop('disabled', enabled);
                    }
                }
            }

            /* set list kiểu tóc vào btn */
            function GetListHairMode() {
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/PreviewImage/SCSC_Preview_Random_Salon_Images.aspx/ReturnAllHairMode",
                    data: '',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var btnList = "";
                        $.each(response.d, function (i, v) {
                            btnList += '<option value="' + v.Id + '" >' + v.Title + '</option>';
                        });
                        $(".btnHairList").append(btnList);
                        finishLoading();
                    },
                    failure: function (response) { console.log(response.d); }
                });
            }

            /* Set thông tin của bill ảnh : bill, khách hàng, ảnh, lỗi cắt, kiểu cắt, .... */
            function SetPreviewInfo(index) {

                if (_ListPreviewImg[index].ServiceIds != "" || _ListPreviewImg[index].ServiceIds != null) {
                    testObject(_ListPreviewImg[index].ServiceIds);
                }


                //console.log(_ListPreviewImg[index]);
                if (_ListPreviewImg[index].CustomerName == null) {
                    $('#pre-cus').text("Không có");
                }
                else {
                    $('#pre-cus').text(_ListPreviewImg[index].CustomerName);
                }
                if (_ListPreviewImg[index].CustomerPhone == null) {
                    $('#pre-date').text("Không có");
                }
                else {
                    if (_ListPreviewImg[index].CustomerPhone.length == 11) {
                        $('#pre-date').text("******" + _ListPreviewImg[index].CustomerPhone.substr(7, 11));
                    }
                    else {
                        $('#pre-date').text("*****" + _ListPreviewImg[index].CustomerPhone.substr(6, 10));
                    }
                }

                $('#pre-id').text(_ListPreviewImg[index].Id);
                $("#HDFBill_ID").val(_ListPreviewImg[index].Id);
                $('#phone-id').text(_ListPreviewImg[index].CustomerPhone);
                //$('#pre-salon').text(_ListPreviewImg[index].CustomerName);
                $("#txtErrorNote").val(_ListPreviewImg[index].ErrorNote);
                $("#txtNoteByStylist").val(_ListPreviewImg[index].NoteByStylist);
                $("#pre-customerid").text(_ListPreviewImg[index].CustomerId);

                $('#pre-img1').attr('src', _ListPreviewImg[index].Img1);
                $('#pre-img2').attr('src', _ListPreviewImg[index].Img2);
                $('#pre-img3').attr('src', _ListPreviewImg[index].Img3);
                $('#pre-img4').attr('src', _ListPreviewImg[index].Img4);
                $('#pre-image-1').attr('src', _ListPreviewImg[index].ImageCurlingBefore);
                $('#pre-image-2').attr('src', _ListPreviewImg[index].ImageCurlingAfter);

                $('#pre-img1-can').css({ 'background-image': "url(" + _ListPreviewImg[index].Img1 + ")" });
                $('#pre-img2-can').css({ 'background-image': "url(" + _ListPreviewImg[index].Img2 + ")" });
                $('#pre-img3-can').css({ 'background-image': "url(" + _ListPreviewImg[index].Img3 + ")" });
                $('#pre-img4-can').css({ 'background-image': "url(" + _ListPreviewImg[index].Img4 + ")" });
                $('#pre-img1-can').attr('data-src', _ListPreviewImg[index].Img1);
                $('#pre-img2-can').attr('data-src', _ListPreviewImg[index].Img2);
                $('#pre-img3-can').attr('data-src', _ListPreviewImg[index].Img3);
                $('#pre-img4-can').attr('data-src', _ListPreviewImg[index].Img4);
                $('#pre-image-curling-1-can').css({ 'background-image': "url(" + _ListPreviewImg[index].ImageCurlingBefore + ")" });
                $('#pre-image-curling-2-can').css({ 'background-image': "url(" + _ListPreviewImg[index].ImageCurlingAfter + ")" });
                $('#pre-image-curling-1-can').attr('data-src', _ListPreviewImg[index].ImageCurlingBefore);
                $('#pre-image-curling-2-can').attr('data-src', _ListPreviewImg[index].ImageCurlingAfter);
                // Bind Service 
                BindFlowserService(_ListPreviewImg[index].Id);
                // set giá trị cho billID
                setBillID(_ListPreviewImg[index].Id);
                // reset trạng thái active của item trạng thái
                resetItemStatus();
                // bind lại trạng thái
                bindBillStatus(_ListPreviewImg[index].ImageStatusId);
                //getHairStyleById( billID );
                getHairStyleById(_ListPreviewImg[index].Id);
                getErrorNote();

                //Set SCSC
                bindataSCSC(_ListPreviewImg[index].Id);

                //initCircles();
            }

            /* cập nhật giá trị cho billID */
            function setBillID(id) {
                billID = id;
            }

            /* set giá trị cho list lỗi cắt (đã đc đánh giá hoặc chưa)  */
            function bindBillStatus(statusID) {
                $(".btnErrorList").removeClass("active");
                if (statusID != null && statusID.length > 0) {
                    var stt = statusID.toString().split(',');
                    for (var i = 0; i < stt.length; i++) {
                        if (stt[i] == "1") {
                            $(".btnErrorList[data-id='" + stt[i] + "']").addClass("btn-ok");
                        }
                        else {
                            $(".btnErrorList[data-id='" + stt[i] + "']").addClass("btn-act");
                        }
                        $(".btnErrorList[data-id='" + stt[i] + "']").addClass("active");
                    }
                }
            }

            /* reset các giá trị của list btn  */
            function resetItemStatus() {
                $(".btnErrorList").removeClass("active");
                $(".btnHairMode").removeClass("active");
                //$(".btnFaceType").removeClass("active");
            }

            /* set trạng thái active cho btn lỗi cắt */
            function setActiveOnclick(This) {
                $(".btnErrorList").removeClass("active");
                This.addClass("active");
            }

            /* update lỗi cắt cho bill tương ứng */
            function updateStatus(This) {
                var statusID = new Array();

                if (This.attr("data-id") == 1) {
                    $(".btnErrorList").removeClass("active");
                    This.addClass("btn-ok");
                    This.addClass("active");
                    statusID.push("1");
                }
                else {
                    $(".btnErrorList").find(".btn-ok").removeClass("active");
                    if (This.hasClass("active")) {
                        This.removeClass("active");
                    }
                    else {
                        This.addClass("btn-act");
                        This.addClass("active");
                    }

                    for (var i = 0; i < $(".btnErrorList").length; i++) {
                        if ($(".btnErrorList").eq(i).attr("data-id") != 1 && $(".btnErrorList").eq(i).hasClass("active")) {
                            statusID.push($(".btnErrorList").eq(i).attr("data-id"));
                        }
                    }
                }

                if (billID > 0 && statusID.length > 0) {
                    startLoading();
                    $.ajax({
                        url: "/GUI/FrontEnd/PreviewImage/SCSC_Preview_Random_Salon_Images.aspx/updateStatus",
                        type: "post",
                        data: '{billID:' + billID + ', statusID : "' + statusID + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var msg = JSON.parse(data.d);
                            if (msg.success) {
                                var index = findIndexByBillID(billID);
                                if (index != null) {
                                    _ListPreviewImg[index].ImageStatusId = statusID;
                                }
                            }
                            finishLoading();
                        },
                        error: function (data) {
                            swal(response.d.responseText, "", "error");
                        }
                    });
                }
            }

            /* lấy index theo mã bill */
            function findIndexByBillID(billID) {
                var index = null;
                if (_ListPreviewImg != null) {
                    for (var i = 0; i < _ListPreviewImg.length; i++) {
                        if (billID == _ListPreviewImg[i].Id) {
                            index = i;
                            break;
                        }
                    }
                }
                return index;
            }

            /* lấy nội dung ghi chú của bill */
            function getErrorNote() {
                if (billID > 0) {
                    $.ajax({
                        url: "/GUI/FrontEnd/PreviewImage/SCSC_Preview_Random_Salon_Images.aspx/GetErrorNoteById",
                        type: "post",
                        data: '{billID:' + billID + '}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            // $("#txtErrorNote").val(data.d.NoteByManager);
                            $("#txtNoteByStylist").val(data.d.NoteByStylist);
                        },
                        error: function (data) {

                        }
                    });
                }
            }

            /* Hiển thị thông tin của bill có paras truyền từ module thống kê ảnh */
            function checkTotalBill() {
                var params = {};
                if (location.search) {
                    var parts = location.search.substring(1).split('&');
                    for (var i = 0; i < parts.length; i++) {
                        var nv = parts[i].split('=');
                        if (!nv[0]) continue;
                        params[nv[0]] = nv[1] || true;
                    }
                }
                if ([params.time, params.salonId, params.stylistId, params.imageStatus].indexOf(undefined) == -1) {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/FrontEnd/PreviewImage/SCSC_Preview_Random_Salon_Images.aspx/GetRandomBillImage",
                        data: '{_Date: "' + params.time + '", _SalonId: ' + params.salonId + ', _StylistId: ' + params.stylistId + ', _imageStatus:' + params.imageStatus + '}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            _ListPreviewImg = [];

                            $.each(response.d, function (key, value) {
                                objPreviewImg = {};
                                objPreviewImg.Id = value.Id;
                                $("#HDF_BillID").val(objPreviewImg.Id);
                                objPreviewImg.BillDate = value.BillDate;
                                objPreviewImg.SalonName = value.SalonName;
                                objPreviewImg.CustomerPhone = value.CustomerPhone;
                                objPreviewImg.CustomerName = value.CustomerName;
                                objPreviewImg.Img1 = value.Img1;
                                objPreviewImg.Img2 = value.Img2;
                                objPreviewImg.Img3 = value.Img3;
                                objPreviewImg.Img4 = value.Img4;
                                objPreviewImg.ImageStatusId = value.ImageStatusId;
                                objPreviewImg.ErrorNote = value.ErrorNote;
                                _ListPreviewImg.push(objPreviewImg);

                                // Set giá trị mặc định billID;
                                if (key == 0) {
                                    setBillID(value.Id);
                                    bindBillStatus(value.ImageStatusId);
                                }
                            });

                            if (_ListPreviewImg.length > 0) {
                                SetPreviewInfo(0);
                                $('#hdfIndexPage').val("0");
                                $('#total-index').text(_ListPreviewImg.length);
                                $('#current-index').text('1');
                                $("#CtMainSCSC_txtToDate").val(params.time);
                                $('#ddlSalon option').removeAttr('selected').filter('[value=' + params.salonId + ']').attr('selected', 'selected');
                                getStylistBySalon(params.stylistId);
                            }
                            finishLoading();
                        },
                        failure: function (response) { console.log(response.d); }
                    });
                }
            }

            /* set lại kiểu tóc khi selected change */
            /*author: Phú Phạm - Update on: 05/08/2017 */
            $("#sltHairStyle").on('change', function () {

                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/PreviewImage/SCSC_Preview_Random_Salon_Images.aspx/getHairStyleById",
                    data: '{billId:' + billID + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        if (response.d != null) {
                            console.log(response.d);
                            cusHairId = response.d.Id;
                            hairStyleId = response.d.HairStyleId;
                            cusId = response.d.CustomerId;
                            //faceTypeId = response.d.FaceTypeId;
                        }
                        updateCustomer_HairMode_Bill();
                    },
                    failure: function (response) { console.log(response.d); }
                });


            })

            /* update kiểu tóc và kiểu khuôn mặt ứng với từng bill */
            function updateCustomer_HairMode_Bill() {
                if (billID > 0) {
                    startLoading();

                    if (cusHairId == "" || cusHairId == undefined) {
                        cusHairId = 0;
                    }
                    if (cusId == "" || cusId == undefined || cusId == 'null') {
                        cusId = 0;
                    }
                    $.ajax({
                        url: "/GUI/FrontEnd/PreviewImage/SCSC_Preview_Random_Salon_Images.aspx/updateCustomer_HairMode_Bill",
                        type: "post",
                        data: '{cusHairId:' + cusHairId + ', hairStyleId : ' + $("#sltHairStyle").val() + ', CustomerId: "' + cusId + '", billID: ' + billID + '}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            finishLoading();
                        },
                        error: function (data) {
                        }
                    });
                }
            }

            /* get kiểu tóc và kiểu khuôn mặt của khách theo mã bill */
            function getHairStyleById(billID) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/PreviewImage/SCSC_Preview_Random_Salon_Images.aspx/getHairStyleById",
                    data: '{billId:' + billID + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        if (response.d != null) {
                            // console.log(response);
                            cusHairId = response.d.Id;
                            hairStyleId = response.d.HairStyleId;
                            //$( "#sltHairStyle option[value='" + hairStyleId + "']" ).attr( "selected", "selected" );

                            $("#sltHairStyle").val(hairStyleId);
                            //faceTypeId = response.d.FaceTypeId;
                            //$.ajax({
                            //    type: "POST",
                            //    url: "/GUI/FrontEnd/PreviewImage/SCSC_Preview_Random_Salon_Images.aspx/Get_HairMode_Title",
                            //    data: '{hairstyleID:' + hairStyleId + '}',
                            //    contentType: "application/json; charset=utf-8",
                            //    dataType: "json",
                            //    success: function (response) {
                            //        $("#current-style").text("("+response.d.Title+")");
                            //    }

                            //})
                        }
                        //bindBillHairStylist(hairStyleId);
                        // hairStyleId = "";
                        //faceTypeId = "";
                    },
                    failure: function (response) { console.log(response.d); }
                });
            }

            /********************************/
            // Xử lý canvas điểm cắt lỗi
            /********************************/
            var mousePosition;
            var isMouseDown;
            var isTouchStart;

            var focused = { key: 0, state: false };
            var canvasId;
            var $canvas;
            var canvas;
            var ctx;
            var canvasOrder;
            var classCircle = function () {
                this.key = "";
                this.circles = [];
            }
            var classCustomer = function () {
                this.idCustomer = "";
                this.key = "";
                this.circles = [];
            }
            var Circles = [];
            var currentCircle = new classCircle();
            var circles = [];

            function getCircleByKey(key, idCustomer) {

                for (var i = 0; i < Circles.length; i++) {
                    if (Circles[i].key === key && Circles[i].idCustomer == idCustomer) {
                        return Circles[i];
                    }
                }
            }

            function setCircleByKey(key, array) {
                for (var i = 0; i < Circles.length; i++) {
                    if (Circles[i].key === key) {
                        Circles[i].circles = array;
                    }
                }
            }

            /* main draw method */
            function draw() {
                //clear canvas
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                for (var i = circles.length - 1; i >= 0; i--) {
                    circles[i].draw();
                }

            }

            //circle Object
            function Circle(x, y, r, stroke) {
                this.startingAngle = 0;
                this.endAngle = 2 * Math.PI;
                this.x = x;
                this.y = y;
                this.r = r;
                this.stroke = stroke;
                this.draw = function () {
                    ctx.beginPath();
                    ctx.arc(this.x, this.y, this.r, 0, this.endAngle, false);
                    ctx.lineWidth = 2;
                    ctx.strokeStyle = this.stroke;
                    ctx.stroke();
                }
            }

            function move(e) {
                var idCustomer = $("#pre-id").text();
                var $activeCanvas = $(".carousel-inner .item.active").find("canvas");
                var canvasId = $activeCanvas.attr("id");
                canvas = document.getElementById($activeCanvas.attr("id"));
                ctx = canvas.getContext("2d");
                circles = getCircleByKey(canvasId, idCustomer).circles;

                if (!isMouseDown) {
                    return;
                }
                var rect = canvas.getBoundingClientRect();
                mousePosition = {
                    x: Math.round(e.x - rect.left),
                    y: Math.round(e.y - rect.top)
                };
                //if any circle is focused
                if (focused.state) {
                    circles[focused.key].x = mousePosition.x;
                    circles[focused.key].y = mousePosition.y;
                    draw();
                    return;
                }
                //no circle currently focused check if circle is hovered
                for (var i = 0; i < circles.length; i++) {
                    if (intersects(circles[i])) {
                        circles.move(i, 0);
                        focused.state = true;
                        break;
                    }
                }
                draw();
            }

            /* Event touch trên mobile */
            function touchMove(e) {

                var idCustomer = $("#pre-id").text();
                var $activeCanvas = $(".carousel-inner .item.active").find("canvas");
                var canvasId = $activeCanvas.attr("id");
                canvas = document.getElementById(canvasId);
                ctx = canvas.getContext("2d");
                circles = getCircleByKey(canvasId, idCustomer).circles;

                if (!e) {
                    var e = event;
                    e.stopPropagation();
                    e.preventDefault();

                }
                var modalDialog = document.getElementById("modalDialog");
                var modalHeader = document.getElementById("modalHeader");

                mousePosition = {
                    x: Math.round(e.targetTouches[0].pageX - modalDialog.offsetLeft),
                    y: Math.round(e.targetTouches[0].pageY - window.pageYOffset - (modalDialog.offsetTop + modalHeader.offsetHeight))
                };

                //if any circle is focused
                if (focused.state) {
                    circles[focused.key].x = mousePosition.x;
                    circles[focused.key].y = mousePosition.y;
                    draw();
                    return;
                }
                for (var i = 0; i < circles.length; i++) {
                    if (intersects(circles[i])) {
                        circles.move(i, 0);
                        focused.state = true;
                        break;
                    }
                }
                draw();
            }

            //set mousedown state
            function setDraggable(e) {
                var t = e.type;
                if (t === "mousedown") {
                    isMouseDown = true;
                } else if (t === "mouseup") {
                    isMouseDown = false;
                    focused.state = false;
                }
            }

            function setDraggableMobile(e) {
                var t = e.type;
                if (t === "touchstart") {
                    isTouchStart = true;
                } else if (t === "touchend") {
                    isTouchStart = false;
                    focused.state = false;
                }
            }

            function intersects(circle) {
                var areaX = mousePosition.x - circle.x;
                var areaY = mousePosition.y - circle.y;
                return areaX * areaX + areaY * areaY <= circle.r * circle.r;
            }
            Array.prototype.move = function (old_index, new_index) {
                if (new_index >= this.length) {
                    var k = new_index - this.length;
                    while ((k--) + 1) {
                        this.push(undefined);
                    }
                }
                this.splice(new_index, 0, this.splice(old_index, 1)[0]);
            };

            /* Khởi tạo mảng lưu lỗi */
            function initCircles() {
                $(".carousel-inner canvas").each(function () {
                    canvas = document.getElementById($(this).attr("id"));
                    canvas.width = $(".modal-content").width();
                    canvas.height = canvas.width * 4 / 3;

                    var circle = new classCustomer();
                    circle.idCustomer = $('#pre-id').text();
                    circle.key = $(this).attr("id");
                    circle.circles = [];
                    Circles.push(circle);
                });
            }

            /*  Truy vấn canvas hiện tại */
            function getActiveCanvas() {
                var idCustomer = $("#pre-id").text();
                $canvas = canvasId = $(".carousel-inner .item.active").find("canvas");
                canvasId = $canvas.attr("id");
                canvasOrder = $canvas.attr("data-order");
                canvas = document.getElementById(canvasId);
                ctx = canvas.getContext("2d");
                circles = getCircleByKey(canvasId, idCustomer).circles;
            }

            /* Thêm vòng tròn lỗi */
            function addCycleToCanvas() {
                getActiveCanvas();
                var idCustomer = $("#pre-id").text();
                ctx = canvas.getContext("2d");
                circles = getCircleByKey(canvasId, idCustomer).circles;

                var c1 = new Circle(70, 70, 40, "red");
                c1.draw();

                circles.push(c1);

                canvas.addEventListener('mousemove', move, false);
                canvas.addEventListener('mousedown', setDraggable, false);
                canvas.addEventListener('mouseup', setDraggable, false);

                canvas.addEventListener("touchmove", touchMove, false);
                canvas.addEventListener("touchstart", setDraggableMobile, false);
                canvas.addEventListener("touchend", setDraggableMobile, false);

            }

            /*  Hủy vòng tròn lỗi */
            function undoCycle() {
                getActiveCanvas();
                circles.shift();
                setCircleByKey(canvasId, circles);
                draw();
            }

            function bindCircleFromArray() {
                getActiveCanvas();
                draw();
            }

            /*  Đóng khung canvas */
            function closeCanvas() {
                canvas = null;
                ctx = null;
                ////circles = [];
                //$("#btnTimKiem").click();
            }

            /* Mở layer loading */
            function startLoading() {
                $(".page-loading").fadeIn();
            }

            /* Đóng layer loading */
            function finishLoading() {
                $(".page-loading").fadeOut();
            }

            /* Lưu ảnh đã check */
            function saveCheckedImages() {
                getActiveCanvas();
                ctx.globalCompositeOperation = "destination-over";
                var dataUrl = "";
                var image = new Image();
                image.setAttribute('crossOrigin', 'anonymous');
                //image.src = $canvas.attr("data-src");
                // get chuoi src
                image.src = GetImageS3Base64($canvas.attr("data-src"));
                image.onload = function () {
                    //câu này không khớp với css nên bị lệch ảnh
                    ctx.drawImage(image, 0, 0, canvas.width, canvas.height);

                    dataUrl = canvas.toDataURL("image/jpg").replace(/^data:image\/(png|jpg);base64,/, "");
                    startLoading();
                    $.ajax({
                        url: "/GUI/FrontEnd/PreviewImage/SCSC_Preview_Random_Salon_Images.aspx/LoadImage",
                        type: "post",
                        data: '{bID : ' + billID + ', order : ' + $canvas.attr("data-order") + ', base64:' + JSON.stringify(dataUrl) + '}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            setCircleByKey(canvasId, circles);
                            finishLoading();
                            displayMessageSuccess("Lưu ảnh thành công!");
                        },
                        error: function (data) {
                        }
                    });
                };
            }
            //Neu la link s3 thi get chuoi base64, neu khong thi tra ve link http://api.30shine.com.......
            function GetImageS3Base64(url) {
                var Image64 = "";
                var domain = url.substr(0, 39);
                if (domain != "https://s3-ap-southeast-1.amazonaws.com" && domain != "https://s3.ap-southeast-1.amazonaws.com")
                    return url;
                $.ajax({
                    url: "/GUI/FrontEnd/PreviewImage/SCSC_Preview_Random_Salon_Images.aspx/GetImageS3",
                    async: false,
                    type: "post",
                    data: '{url:"' + url + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.d != null) {
                            Image64 = 'data:image/jpeg;base64,' + response.d;
                        }
                    },
                    error: function (data) {

                    }

                });
                return Image64;
            }

            /* Load lại ảnh đã checked ra màn hiển thị */
            function refreshCheckedImage() {
                startLoading();
                var $activeCanvas = $(".carousel-inner .item.active").find("canvas");
                var canvasId = $activeCanvas.attr("id");
                $.ajax({
                    url: "/GUI/FrontEnd/PreviewImage/SCSC_Preview_Random_Salon_Images.aspx/RefreshCheckedImage",
                    type: "post",
                    async: false,
                    data: '{bID : ' + billID + ', order : ' + $activeCanvas.attr("data-order") + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $("#" + canvasId).css({ 'background-image': "url(" + data.d + ")" });
                        $("#" + canvasId).attr('data-src', data.d);
                        finishLoading();
                    },
                    error: function (data) {
                    }
                });
            }

            /* Full screen */
            var videoElement = document.getElementById("myModal");

            function toggleFullScreen() {
                if (!document.mozFullScreen && !document.webkitFullScreen) {
                    if (videoElement.mozRequestFullScreen) {
                        videoElement.mozRequestFullScreen();
                    } else {
                        videoElement.webkitRequestFullScreen();
                    }
                } else {
                    if (document.mozCancelFullScreen) {
                        document.mozCancelFullScreen();
                    } else {
                        document.webkitCancelFullScreen();
                    }
                }
            }

            document.addEventListener("keydown", function (e) {
                if (e.keyCode == 13) {
                    toggleFullScreen();
                }
            }, false);

            // Xử lý SCSC
            function SCSC_CheckError(This, type) {
                var Bill_ID = $("#HDFBill_ID").val();
                if (Bill_ID == 0) {
                    swal("Chưa có Bill để đánh giá hoặc có lỗi ! Bạn kiểm tra lại nếu không được hãy liên hệ với nhà phát triển!", "", "error");
                    return false;
                }
                else {
                    if (type == 0) {
                        if ($("#HDFErrorImage").val() == 0) {
                            var ParentCateId = This.data("catid");
                            if (This.hasClass("active")) {
                            }
                            else {
                                $(".li_children_scsc" + ParentCateId + "  ").removeClass("active");
                                This.addClass("active");
                                CalculatorTotalPoint();
                            }
                        }
                        else {
                            swal("Bill Ảnh mờ, lệch được coi là Bill thiếu ảnh không cần đánh giá!", "", "error");
                        }
                    }
                    else if (type == 1) {
                        var ParentCateId = This.data("catid");
                        if ($("#HDFErrorImageCurling").val() == 0) {
                            $(".li_children_scsc" + ParentCateId + "  ").removeClass("active");
                            This.addClass("active");
                            CalculatorTotalCurling();
                        }
                        else {
                            swal("Bill ảnh uốn mờ, lệch được coi là bill thiếu ảnh không cần đánh giá!", "", "error");
                        }
                    }
                }
            }
            // Xử lý Ảnh thiếu or mờ 
            function ErrorImage(This, value) {
                if (value == 0) {
                    if (This.hasClass("active")) {
                        This.removeClass("active");
                        $("#HDFErrorImage").val(0);
                    }
                    else {
                        This.addClass("active");
                        $("#HDFErrorImage").val(1)
                        $(".li_children_scsc1").removeClass("active");
                        $(".li_children_scsc2").removeClass("active");
                        $(".li_children_scsc3").removeClass("active");
                        $(".li_children_scsc4").removeClass("active");
                        $("#TotalPoinSCSC").text(0);
                    }
                }
                else {
                    if (This.hasClass("active")) {
                        This.removeClass("active");
                        $("#HDFErrorImageCurling").val(0);
                    }
                    else {
                        This.addClass("active");
                        $("#HDFErrorImageCurling").val(1)
                        $(".li_children_scsc20").removeClass("active");
                        $(".li_children_scsc21").removeClass("active");
                        $(".li_children_scsc22").removeClass("active");
                        $("#TotalPointCurling").text(0);
                    }
                }
            }


            // Insert or Update SCSC Check Error
            function InsertOrUpdateSCSC_CheckError(Click) {
                debugger;
                var SCSC_PointError = 0;
                var CURLING_PointError = 0;
                ///  Tinh bill co  loi hay khong
                var PointShape = 0;
                var PointConnective = 0;
                var PointSharpNess = 0;
                var PointComPletion = 0;
                var HairTip = 0;
                var HairRoot = 0;
                var HairWave = 0;
                //End
                var ImageError = $("#HDFErrorImage").val();
                var ImageErrorCurling = $("#HDFErrorImageCurling").val();
                ImageError = parseInt(ImageError);
                ImageErrorCurling = parseInt(ImageErrorCurling);
                var ID_SCSC_Check = $("#HDF_ID_SCSC").val();
                var _TotalPoint_SCSC = $("#TotalPoinSCSC").text().replace(/[^\d]+/g, '');
                var TotalPointCurling = $("#TotalPointCurling").text().replace(/[^\d]+/g, '');
                var Bill_ID = $("#HDFBill_ID").val();
                if (Bill_ID == 0) {
                    swal("Chưa có Bill để đánh giá hoặc có lỗi ! Bạn kiểm tra lại nếu không được hãy liên hệ với nhà phát triển!", "", "error");
                    return false;
                }
                var SHAPE_ID = $(".li_children_scsc1.active").data("id");
                var CONNECTIVE_ID = $(".li_children_scsc2.active").data("id");
                var SHARPNESS_ID = $(".li_children_scsc3.active").data("id");
                var COMPLETION_ID = $(".li_children_scsc4.active").data("id");
                var HAIRTIP_ID = $(".li_children_scsc20.active").data("id");
                var HAIRROOT_ID = $(".li_children_scsc21.active").data("id");
                var HAIRWAVE_ID = $(".li_children_scsc22.active").data("id");
                if (ImageError === 0) {
                    if (SHAPE_ID === undefined) {
                        swal("Chưa có đánh giá SHAPE.", "", "error");
                        return false;
                    }
                    else {
                        PointShape = $(".li_children_scsc1.active").data("point");
                    }

                    if (CONNECTIVE_ID === undefined) {
                        swal("Chưa có đánh giá CONNECTIVE.", "", "error");
                        return false;
                    }
                    else {
                        PointConnective = $(".li_children_scsc2.active").data("point");
                    }

                    if (SHARPNESS_ID === undefined) {
                        swal("Chưa có đánh giá SHARPNESS.", "", "error");
                        return false;
                    }
                    else {
                        PointSharpNess = $(".li_children_scsc3.active").data("point");
                    }

                    if (COMPLETION_ID === undefined) {
                        swal("Chưa có đánh giá COMPLETION.", "", "error");
                        return false;
                    }
                    else {
                        PointComPletion = $(".li_children_scsc4.active").data("point");
                    }
                }
                else {
                    SHAPE_ID = 0;
                    CONNECTIVE_ID = 0;
                    SHARPNESS_ID = 0;
                    COMPLETION_ID = 0;
                }
                if (ImageErrorCurling === 0) {
                    if (checkCurling) {
                        //checkCurling = false;
                        let srcImg1 = $('img#pre-image-1').attr('src');
                        let srcImg2 = $('img#pre-image-2').attr('src');
                        if (srcImg1 != '' && srcImg2 != '') {
                            if (HAIRTIP_ID === undefined) {
                                swal("Chưa có đánh giá ngọn tóc.", "", "error");
                                return false;
                            }
                            else {
                                HairTip = $(".li_children_scsc20.active").data("point");
                            }

                            if (HAIRROOT_ID === undefined) {
                                swal("Chưa có đánh giá chân tóc.", "", "error");
                                return false;
                            }
                            else {
                                HairRoot = $(".li_children_scsc21.active").data("point");
                            }
                            if (HAIRWAVE_ID === undefined) {
                                swal("Chưa có đánh giá sóng tóc.", "", "error");
                                return false;
                            }
                            else {
                                HairWave = $(".li_children_scsc22.active").data("point");
                            }
                        }
                        else {
                            HAIRTIP_ID = 0;
                            HAIRROOT_ID = 0;
                            HAIRWAVE_ID = 0;
                        }
                    }
                    else {
                        HAIRTIP_ID = 0;
                        HAIRROOT_ID = 0;
                        HAIRWAVE_ID = 0;
                    }
                }
                else {
                    HAIRTIP_ID = 0;
                    HAIRROOT_ID = 0;
                    HAIRWAVE_ID = 0;
                }

                if (HairTip !== 0 && HairRoot !== 0 && HairWave !== 0) {
                    CURLING_PointError = parseInt(HairTip) + parseInt(HairRoot) + parseInt(HairWave);
                }
                else {
                    CURLING_PointError = 0;
                }

                var NoteError = $("#txtErrorNote").val();

                if (PointShape != 0 && PointConnective != 0 && PointSharpNess != 0 && PointComPletion != 0) {
                    SCSC_PointError = parseInt(PointShape) + parseInt(PointConnective) + parseInt(PointSharpNess) + parseInt(PointComPletion);
                    if (totalBill > 0 && OBJ == null) {
                        totalBill = totalBill - 1;
                        $('#total-billcheck').text(totalBill);
                    }
                }
                else {
                    SCSC_PointError = 0;
                }

                var MaxPointShap_ID = $("#HDFShapMaxPoint").val();
                var MaxPointConnective_ID = $("#HDFConnectiveMaxPoint").val();
                var MaxPointSharpNess_ID = $("#HDFSharpNessMaxPoint").val();
                var MaxPointCompletion_ID = $("#HDFCompletionMaxPoint").val();
                var _Date = $("#CtMainSCSC_txtToDate").val();
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/PreviewImage/SCSC_Preview_Random_Salon_Images.aspx/InsertAndUpdateSCSC_CheckError",
                    data: '{ ID_SCSC_Check : ' + ID_SCSC_Check + ' , Bill_ID : ' + Bill_ID + ' , ImageError : ' + ImageError +
                        ', NoteError : "' + NoteError + '" , _TotalPoint_SCSC : ' + _TotalPoint_SCSC +
                        ', SHAPE_ID : ' + SHAPE_ID + ' , CONNECTIVE_ID : ' + CONNECTIVE_ID + ', SHARPNESS_ID : ' + SHARPNESS_ID +
                        ', COMPLETION_ID : ' + COMPLETION_ID + ', SCSC_PointError : ' + SCSC_PointError +
                        ', MaxPointShap_ID : ' + MaxPointShap_ID + ', MaxPointConnective_ID : ' + MaxPointConnective_ID +
                        ', MaxPointSharpNess_ID : ' + MaxPointSharpNess_ID + ', MaxPointCompletion_ID : ' + MaxPointCompletion_ID +
                        ',WorkDate : "' + _Date + '", CURLING_PointError : ' + CURLING_PointError + ' , HAIRTIP_ID : ' + HAIRTIP_ID +
                        ', HAIRROOT_ID : ' + HAIRROOT_ID + ' , HAIRWAVE_ID : ' + HAIRWAVE_ID + ' , ImageErrorCurling : ' + ImageErrorCurling + ' }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        if (response.d != null) {
                            $("#HDF_ID_SCSC").val(response.d.data.ID);
                            finishLoading();
                        }
                    },
                    failure: function (response) { console.log(response.d); }
                });
            }

            // Tinh tong point scsc
            function CalculatorTotalCurling() {
                var totalPointCurling = 0;
                var HairTips = $(".li_children_scsc20.active").data("point");
                if (HairTips == undefined) {
                    HairTips = 0;
                }
                var HairRoot = $(".li_children_scsc21.active").data("point");
                if (HairRoot == undefined) {
                    HairRoot = 0;
                }
                var HairWaves = $(".li_children_scsc22.active").data("point");
                if (HairWaves == undefined) {
                    HairWaves = 0;
                }
                if (HairTips != 0 && HairRoot != 0 && HairWaves != 0) {
                    totalPointCurling = parseInt(HairTips) + parseInt(HairRoot) + parseInt(HairWaves);
                }
                else {
                    totalPointCurling = 0;
                }
                $("#TotalPointCurling").text(totalPointCurling);
            };

            // Tinh tong point scsc
            function CalculatorTotalPoint() {
                var _TotalPointSCSC = 0;
                var PointShape = $(".li_children_scsc1.active").data("point");
                if (PointShape == undefined) {
                    PointShape = 0;
                }
                var PointConnective = $(".li_children_scsc2.active").data("point");
                if (PointConnective == undefined) {
                    PointConnective = 0;
                }
                var PointSharpNess = $(".li_children_scsc3.active").data("point");
                if (PointSharpNess == undefined) {
                    PointSharpNess = 0;
                }
                var PointComPletion = $(".li_children_scsc4.active").data("point");
                if (PointComPletion == undefined) {
                    PointComPletion = 0;
                }
                if (PointShape != 0 && PointConnective != 0 && PointSharpNess != 0 && PointComPletion != 0) {
                    _TotalPointSCSC = parseInt(PointShape) + parseInt(PointConnective) + parseInt(PointSharpNess) + parseInt(PointComPletion);

                }
                else {
                    _TotalPointSCSC = 0;
                }
                $("#TotalPoinSCSC").text(_TotalPointSCSC);
            };

            // xu lý scsc khi click truoc sau
            function bindataSCSC(Bill_ID) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/PreviewImage/SCSC_Preview_Random_Salon_Images.aspx/BillDataWhereBillID",
                    data: '{ Bill_ID : ' + Bill_ID + '    }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        OBJ = response.d.data;
                        if (response.d.msg == "success") {
                            $("#HDF_ID_SCSC").val(OBJ.ID);
                            $(".li_children_scsc1 ").removeClass('active');
                            $(".li_children_scsc2 ").removeClass('active');
                            $(".li_children_scsc3 ").removeClass('active');
                            $(".li_children_scsc4 ").removeClass('active');
                            $(".li_children_scsc20 ").removeClass('active');
                            $(".li_children_scsc21 ").removeClass('active');
                            $(".li_children_scsc22 ").removeClass('active');
                            $('.li_children_scsc[data-id="' + OBJ.Shape_ID + '"]').addClass('active');
                            $('.li_children_scsc[data-id="' + OBJ.ConnectTive_ID + '"]').addClass('active');
                            $('.li_children_scsc[data-id="' + OBJ.SharpNess_ID + '"]').addClass('active');
                            $('.li_children_scsc[data-id="' + OBJ.ComPlatetion_ID + '"]').addClass('active');
                            $('.li_children_scsc[data-id="' + OBJ.HairTip_ID + '"]').addClass('active');
                            $('.li_children_scsc[data-id="' + OBJ.HairRoot_ID + '"]').addClass('active');
                            $('.li_children_scsc[data-id="' + OBJ.HairWaves_ID + '"]').addClass('active');
                            $("#TotalPoinSCSC").text(OBJ.PointError);
                            $("#TotalPointCurling").text(OBJ.TotalPointSCSCCurling);
                            $("#txtErrorNote").val(OBJ.NoteError);
                            if (OBJ.ImageError == 1) {
                                $("#HDFErrorImage").val(1);
                                $("#ImgError").addClass('active');
                            }
                            else {
                                $("#HDFErrorImage").val(0)
                                $("#ImgError").removeClass('active');
                            }
                            if (OBJ.ImageErrorCurling == 1) {
                                $("#HDFErrorImageCurling").val(1);
                                $("#ImgErrorCurling").addClass('active');
                            }
                            else {
                                $("#HDFErrorImageCurling").val(0)
                                $("#ImgErrorCurling").removeClass('active');
                            }
                        }
                        else {
                            ResetSCSC();
                        }



                    },
                    failure: function (response) { console.log(response.d); }
                });
            };

            // xu lý scsc khi click truoc sau
            function BindFlowserService(Bill_ID) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/PreviewImage/SCSC_Preview_Random_Salon_Images.aspx/BindFlowserService",
                    data: '{ Bill_ID : ' + Bill_ID + '    }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        console.log(response.d)
                        var OBJ = response.d.data;

                        var _Count = OBJ.length;
                        $('#Service').html('');
                        if (_Count > 0) {
                            for (var i = 0; i < _Count; i++) {
                                var html = '<p>' + OBJ[i].Name + '. &nbsp;' + '</p>';
                                if (OBJ[i].ServiceId == 16 || OBJ[i].ServiceId == 107) {
                                    checkCurling = true;
                                }
                            }
                        }
                        else {
                            $('#Service').html('');
                        }
                        $('#Service').append('<strong>Dịch vụ  : &nbsp; </strong> ' + html);
                    },
                    failure: function (response) { console.log(response.d); }
                });
            };

            function ResetSCSC() {
                $('.li_children_scsc').removeClass('active');
                $("#HDF_ID_SCSC").val(0);
                $("#TotalPoinSCSC").text(0);
                $("#TotalPointCurling").text(0);
                $("#HDFErrorImage").val(0)
                $("#HDFErrorImageCurling").val(0)
                $("#ImgError").removeClass('active');
                $("#ImgErrorCurling").removeClass('active');
                $("#txtErrorNote").val('');
            }

            // Validate keypress
            function ValidateKeypress(numcheck, e) {
                var keynum, keychar, numcheck;
                if (window.event) {
                    keynum = e.keyCode;
                }
                else if (e.which) {
                    keynum = e.which;
                }
                if (keynum == 8 || keynum == 127 || keynum == null || keynum == 9 || keynum == 0 || keynum == 13) return true;
                keychar = String.fromCharCode(keynum);
                var result = numcheck.test(keychar);
                return result;
            }
        </script>
    </asp:Panel>
</asp:Content>
