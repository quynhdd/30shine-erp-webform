﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Linq.Expressions;
using ExportToExcel;
using Newtonsoft.Json.Linq;
using Project.Helpers;
using System.Data.Objects;
using System.Data.SqlClient;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.FrontEnd.PreviewImage
{
    public partial class Random_Salon_ImagesView : System.Web.UI.Page
    {
        private string PageID = "CL_KCS";
        protected string Permission = "";
        protected bool Perm_ShowSalon = false;
        private bool Perm_Access = false;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ShowSalon = permissionModel.GetActionByActionNameAndPageId("Perm_ShowSalon", PageID, permission);
                ////Perm_AllPermission = permissionModel.GetActionByActionNameAndPageId("Perm_AllPermission", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();

            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        public CultureInfo culture = new CultureInfo("vi-VN");
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = DateTime.Now.ToString("dd/MM/yyyy");
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ShowSalon);
                bindStaff();
            }
        }

        /// <summary>
        /// Get list salon
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static List<Tbl_Salon> ReturnAllSalon()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Tbl_Salon.Where(a => a.IsDelete == 0 && a.Publish == true).OrderBy(o=>o.Order).ToList();
                return lst;
            }
        }

        /// <summary>
        /// Get list Stylist by salon
        /// </summary>
        /// <param name="_SalonId"></param>
        /// <returns></returns>
        [WebMethod]
        public static List<Staff> ReturnAllStylistBySalon(int _SalonId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Staffs.Where(a => a.IsDelete == 0 && a.Active == 1 && a.Type == 1 && a.SalonId == _SalonId).ToList();
                return lst;
            }
        }

        [WebMethod]
        public static List<store_reportErrorImages_ERP_Result> ImageReport(DateTime timeFrom, DateTime timeTo, int? salonId, int? stylistId)
        {
            var db = new Solution_30shineEntities();

            List<store_reportErrorImages_ERP_Result> list = new List<store_reportErrorImages_ERP_Result>();
            
            List<Tbl_Salon> listSalon = db.Tbl_Salon.Where(w => w.IsDelete != 1 && w.Publish == true && w.IsSalonHoiQuan != true).OrderBy(w => w.Order).ToList();
            List<Staff> listStylist = db.Staffs.Where(a => a.IsDelete == 0 && a.Active == 1 && a.Type == 1 && a.SalonId == salonId).ToList();
            List<PreviewImages_Report> listReport = db.PreviewImages_Report.Where(w => w.IsDelete != true).ToList();
            if (salonId == null)
            {
                foreach (var item in listSalon)
                {
                    var report = db.store_reportErrorImages_ERP(timeFrom, timeTo.AddDays(1), item.Id, 0).SingleOrDefault();

                    report.Id = item.Id;
                    report.Name = item.Name;
                    list.Add(report);
                }
            }
            else
            {
                if (stylistId == null)
                {
                    foreach (var item in listStylist)
                    {
                        var report = db.store_reportErrorImages_ERP(timeFrom, timeTo.AddDays(1), salonId, item.Id).SingleOrDefault();
                        report.Id = item.Id;
                        report.Name = item.Fullname;
                        list.Add(report);
                    }
                }
                else
                {
                    var stl = db.Staffs.Where(a => a.IsDelete == 0 && a.Active == 1 && a.Type == 1 && a.Id == stylistId).SingleOrDefault();
                    var report = db.store_reportErrorImages_ERP(timeFrom, timeTo.AddDays(1), salonId, stylistId).SingleOrDefault();
                    report.Id = stylistId;
                    report.Name = stl.Fullname;
                    list.Add(report);
                }
                
            }
            return list;
        }

        private void bindStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                var salon = Convert.ToInt32(ddlSalon.SelectedValue);
                var staff = new List<Staff>();
                var whereSalon = "";
                var sql = "";
                
                if (salon > 0)
                {
                    whereSalon = " and SalonId = " + salon;
                }
                sql = @"select * from Staff 
                        where IsDelete != 1 and Active = 1  and Type = 1 and (IsHoiQuan = 0 or IsHoiQuan is null)
                        and (isAccountLogin != 1 or isAccountLogin is null)" + whereSalon;
                staff = db.Staffs.SqlQuery(sql).ToList();

                var first = new Staff();
                first.Id = 0;
                first.Fullname = "Chọn nhân viên";
                staff.Insert(0, first);

                ddlStylist.DataTextField = "Fullname";
                ddlStylist.DataValueField = "Id";
                ddlStylist.DataSource = staff;
                ddlStylist.DataBind();
            }
        }

        

        protected void Salon_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindStaff();
        }
    }

    public class ReportBySalon : store_reportErrorImages_ERP_Result
    {
        public int? Id { get; set; }
        public string Name { get; set; }
    }
}