﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Check3S.aspx.cs" Inherits="FormService.Moduls.CheckCleaning" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Check List 3S</title>
    <!--jquery-->
    <%--<script src="../Scripts/jquery-1.10.2.min.js"></script>--%>
    <script src="../../../Assets/js/jquery.v1.11.1.js"></script>
    <!-- Bootstrap 3-->
    <%--<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" media="screen" />
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css" rel="stylesheet" media="screen" />--%>
    <link href="../../../Assets/css/bootstrap/bootstrap.min.css" rel="stylesheet" />
    <link href="../../../Assets/css/bootstrap/bootstrap-theme.min.css" rel="stylesheet" />
    <%--<link href="../Content/check-list.css" rel="stylesheet" />--%>
    <link href="../../../Assets/css/check-list.css" rel="stylesheet" />
    <!--dropzone-->
    <!--<link href="../Content/dropzone.css" rel="stylesheet" />
    <script src="../Scripts/dropzone.js"></script>-->
</head>
<body>
    <form id="form1" runat="server">
        <div class="title background black">
            <label><a href="/" title="Trang chủ solution">Home |&nbsp</a></label>
            <label>Check List 3S</label>
        </div>
        <div class="title message background red">
            <p></p>
        </div>
        <!--listing check list-->
        <div id="main-screen" class="">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr class="info">
                            <td>STT</td>
                            <td>Công việc</td>
                            <td>OK</td>
                            <td>Not OK</td>
                        </tr>
                    </thead>
                    <tbody id="table-listing"></tbody>
                </table>
            </div>
            <div class="container no-padding padding">
                <textarea id="description" rows="3" placeholder="Ghi chú thêm về check list trên"></textarea>
            </div>
            <div class="container no-padding padding disable-click">
                <input type="button" class="col-xs-6 col-sm-6 btn btn-info" onclick="uncheckAll()" value="Bỏ chọn" />
                <input type="button" class="col-xs-6 col-sm-6 btn btn-primary" onclick="saveCheckList()" value="Submit" />
            </div>
        </div>

        <!--dialog prompt comment-->
        <div class="dialog" id="dialog-prompt" itemid="" staffid="" comment="" image="">
            <p class="title"></p>
            <textarea rows="3" id="envident" placeholder="Bình luận"></textarea>
            <div class="container no-padding padding">
                <div class="col-xs-8 col-sm-8">
                    Ảnh
                    <input id="inputFileToLoad" type="file" onchange="encodeImageFileAsURL();" />
                </div>

                <div id="imgTest" class="col-xs-4 col-sm-4"></div>
                <!--<form action="/file-upload" class="dropzone">
                <div class="fallback">
                    <input name="file" type="file" multiple />
                </div>
            </form>-->
            </div>
            <div class="container no-padding">
                <div class="col-xs-4 col-sm-4">Nhân viên: </div>
                <input type="button" id="btnChooseStaff" staffid="" class="col-xs-8 col-sm-8 btn btn-default" onclick="openDialogStaff()" value="Chon nhân viên" />
                <div id="staff-listing" class="dialog container no-padding">
                </div>
            </div>
            <div class="container no-padding padding">
                <input type="button" onclick="canclePromptDialog()" class="col-xs-6 col-sm-6 btn btn-default" value="Hủy bỏ" />
                <input type="button" onclick="confirmPromptDialog()" class="col-xs-6 col-sm-6 btn btn-primary" value="OK" />
            </div>
        </div>

        <!--dialog notic submit success-->
        <div class="dialog" id="notic-success">
            <p class="title"></p>
            <div class="container">
                <input type="button" onclick="viewCheckList()" class="col-xs-6 col-sm-6 btn btn-default" value="Xem lại CL" />
                <input type="button" onclick="checkListAgain()" class="col-xs-6 col-sm-6 btn btn-primary" value="Làm thêm CL" />
            </div>
        </div>


        <%--script--%>
        <script>
            //setting befor load data checkist category and salon
            //category=1 is cleaning
            //staffId whom login use this app
            //is save all or only checked item
            var category = 1;
            var salonId;
            var staffId;
            var isSaveAll = true;

            //init by run scriptmanager in backend
            function init(param1, param2) {             
                staffId = param1;
                salonId = param2;
            }

            //jquery function
            //onload
            $(document).ready(function () {
                getItems(category);
                getStaffs(salonId);
            });


            //submmit
            function saveCheckList() {
                if (mustCheck() > 0) {
                    showMessage(true, "Bạn cần làm đủ check list");
                } else if (moreCheck() > 0) {
                    showMessage(true, "Có item bạn check OK mà lại nhận xét Not OK?");
                } else {
                    var dataChList = getDataCheckList(isSaveAll);
                    var description = $('#description').val();
                    salonId = parseInt(salonId);
                    salonId = !isNaN(salonId) ? salonId : 0;
                    staffId = parseInt(staffId);
                    staffId = !isNaN(staffId) ? staffId : 0;
                    category = parseInt(category);
                    category = !isNaN(category) ? category : 0;
                    var dataJSON = {
                        salonId: salonId,
                        creatorId: staffId,
                        category: category,
                        description: description,
                        list: dataChList

            //            public int salonId { get; set; }
            //public int creatorId { get; set; }
            //public int category { get; set; }
            //public string description { get; set; }
            //public List<CheckItem3SView> list { get; set; }
                    }
                    postCheckList(dataJSON);
                    $('#notic-success .title').text("Đã hoàn thành Check List");
                    dialogSuccessShow(true);

                }
            }

            //check must be checked
            function mustCheck() {
                var miss = 0;
                var tableListing = $('#table-listing');
                tableListing.find('tr').each(function () {
                    var isChecked = $(this).find('input[type=checkbox].check').is(':checked');
                    //var isReport = $(this).find('input[type=checkbox].report').is(':checked');
                    var isComment = $(this).attr("isComment");
                    if (isChecked == false && isComment == 0) {
                        miss++;
                    }
                    console.log(miss);
                });
                return miss;
            }

            //check more be checked
            function moreCheck() {
                var miss = 0;
                var tableListing = $('#table-listing');
                tableListing.find('tr').each(function () {
                    var isChecked = $(this).find('input[type=checkbox].check').is(':checked');
                    //var isReport = $(this).find('input[type=checkbox].report').is(':checked');                
                    var isComment = $(this).attr("isComment");
                    if (isChecked == true && isComment == 1) {
                        miss++;
                    }
                });
                return miss;
            }

            //uncheck all
            function uncheckAll() {
                var tableListing = $('#table-listing');
                tableListing.find('input[type=checkbox]:checked').attr("checked", false);
            }

            //checkbox action
            function getDataCheckList(isSaveAll) {
                var tableListing = $('#table-listing');
                var items = [];
                var num = 0;
                tableListing.find('tr').each(function () {
                    var isChecked = $(this).find('input[type=checkbox].check').is(':checked');
                    //var isReport = $(this).find('input[type=checkbox].report').is(':checked');
                    var itemId = parseInt($(this).attr('itemId'));
                    itemId = !isNaN(itemId) ? itemId : 0;
                    var staffId = parseInt($(this).attr('staffId'));
                    staffId = !isNaN(staffId) ? staffId : 0;
                    num++;
                    var obj = {
                        'isOk': isChecked,
                        //'isReported': true,
                        'itemId': itemId,
                        'staffId': staffId,
                        'comment': $(this).attr('comment'),
                        'image': $(this).attr('image')
                    };
                    if (isSaveAll)
                        items.push(obj);
                    else if (isSaveAll == false && isChecked)
                        items.push(obj);
                });
                return items;
            }

            //comment actions
            //add comment via dialog
            function addComment(This) {
                var title = $('#dialog-prompt .title');
                var dialog = $('#dialog-prompt');
                var image = $('img');
                var tr = $(This).parent().parent();
                var itemId = tr.attr("itemId");
                var comment = tr.attr("comment");
                var staffName = tr.attr("staffName");
                var itemName = tr.attr("itemName");
                var srcImg = tr.attr("image");

                dialog.attr("itemId", itemId);
                dialog.find("textarea").empty().val(comment);
                if (srcImg != "") {
                    //console.log(srcImg);
                    image.attr("src", srcImg);
                }
                else
                    image.attr("src", "");
                if (staffName != "")
                    dialog.find('#btnChooseStaff').val(staffName);
                else
                    dialog.find('#btnChooseStaff').val("Chọn nhân viên");
                title.text("Nhận xét " + itemName);
                dialogPromptShow(true);
            }

            //action cancle dialog
            function canclePromptDialog() {
                var itemId = $('#dialog-prompt').attr("itemId");
                var staffId = $('#btnChooseStaff').attr("staffId");
                var staffName = $('#btnChooseStaff').val();
                var comment = $('#dialog-prompt textarea').val();
                var tableListing = $('#table-listing');
                var srcImg = $('#imgTest img').attr("src");

                tableListing.find('tr').each(function () {
                    var id = $(this).attr("itemId");
                    if (id === itemId) {
                        $(this).attr("staffId", "");
                        $(this).attr("staffName", "");
                        $(this).attr("comment", "");
                        $(this).attr("image", "");
                        $(this).attr("isComment", 0);
                        $(this).find('input[type=button]').removeClass("btn-info");
                        $(this).find('input[type=button]').addClass("btn-default");       
                    }
                });
                dialogPromptShow(false);
            }

            //action confirm dialog
            function confirmPromptDialog() {
                var itemId = $('#dialog-prompt').attr("itemId");
                var staffId = $('#btnChooseStaff').attr("staffId");
                var staffName = $('#btnChooseStaff').val();
                var comment = $('#dialog-prompt textarea').val();
                var tableListing = $('#table-listing');
                var srcImg = $('#imgTest img').attr("src");

                tableListing.find('tr').each(function () {
                    var id = $(this).attr("itemId");
                    if (id === itemId) {
                        $(this).attr("staffId", staffId);
                        $(this).attr("staffName", staffName);
                        $(this).attr("comment", comment);
                        $(this).attr("image", srcImg);
                        if (staffId != "" || comment != "" || srcImg != "") {
                            $(this).attr("isComment", 1);
                            $(this).find('input[type=button]').removeClass("btn-default");
                            $(this).find('input[type=button]').addClass("btn-info");
                        }
                    }
                });
                dialogPromptShow(false);
            }


            //dialog show or not
            function dialogPromptShow(isShow) {
                var mainScreen = $('#main-screen');
                var dialog = $('#dialog-prompt');
                if (isShow) {
                    mainScreen.hide();
                    dialog.show();
                } else {
                    mainScreen.show();
                    dialog.hide();
                }
            }

            //choose staff
            function chooseStaff(id, fullName) {
                var btnChooseStaff = $('#btnChooseStaff');
                btnChooseStaff.attr("staffId", id);
                btnChooseStaff.val(fullName);
                staffListingShow(false);
            }

            //open dialog staff
            function openDialogStaff() {
                staffListingShow(true)
            }

            //staff-listing show or hide
            function staffListingShow(isShow) {
                var staffListing = $('#staff-listing');
                if (isShow) {
                    staffListing.show();
                } else {
                    staffListing.hide();
                }
            }

            //dialog success show or hide
            function dialogSuccessShow(isShow) {
                var mainScreen = $('#main-screen');
                var dialogSuccess = $('#notic-success');
                if (isShow) {
                    mainScreen.hide();
                    dialogSuccess.show();
                    showMessage(false, "");
                } else {
                    dialogSuccess.hide();
                    mainScreen.show();
                }
            }

            //show message
            function showMessage(isShow, msg) {
                var message = $('.title.message');
                if (isShow) {
                    message.show();
                    message.text(msg);
                } else {
                    message.hide();
                    message.text("");
                }
            }

            //check list again
            function checkListAgain() {
                //clean table listing check list
                $('#table-listing').find('tr').each(function () {
                    $(this).attr("staffId", "");
                    $(this).attr("staffName", "");
                    $(this).attr("comment", "");
                    $(this).attr("image", "");
                    $(this).attr("isComment", 0);
                    $(this).find('input[type=button]').addClass("btn-default");
                    $(this).find('input[type=button]').removeClass("btn-info");
                });
                //clean prompt dialog
                $('#dialog-prompt').attr("itemId", "");
                $('#btnChooseStaff').attr("staffId", "");
                $('#btnChooseStaff').val("");
                //clean textarea
                $('textarea').val("");

                //uncheck all
                uncheckAll();

                dialogSuccessShow(false);
            }

            //view check list nearleast
            function viewCheckList() {
                dialogSuccessShow(false);
                //$('.disable-click input[type=button]').click(false);
            }


            //ajax
            //get jobs to do
            function getItems(category) {
                console.log('here');
                var tableListing = $('#table-listing');
                //$.getJSON("/GUI/SystemService/Ajax/Api3s.aspx/GetItemCheck/" + category,
                //    function (data) {
                //        console.log(data)
                //        //console.log(data);
                //        //clean table
                //        tableListing.empty();
                //        //loop append jobs
                //        var num = 0;
                //        $.each(data, function (key, val) {
                //            // Add row
                //            num++;
                //            var row = '<tr itemId=' + val.id + ' itemName=\'' + val.name + '\' staffName=\'\' comment=\'\' image=\'\' isComment=0><td>' + num + '</td><td>' + val.name + '</td><td><input class="check" type="checkbox"/></td><td>' + '<input type="button" class="btn btn-default btn-sm" onclick=\'addComment($(this))\' value="Nhận xét"/>' + '</td></tr>';
                //            tableListing.append(row);
                //        });
                //    });


                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Ajax/Api3s.aspx/GetItemCheck",
                    data: '{id : "' + category + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        console.log(response.d)
                        //console.log(data);
                        //clean table
                        tableListing.empty();
                        //loop append jobs
                        var num = 0;
                        $.each(response.d, function (key, val) {
                            // Add row
                            num++;
                            var row = '<tr itemId=' + val.id + ' itemName=\'' + val.name + '\' staffName=\'\' comment=\'\' image=\'\' isComment=0><td>' + num + '</td><td>' + val.name + '</td><td><input class="check" type="checkbox"/></td><td>' + '<input type="button" class="btn btn-default btn-sm" onclick=\'addComment($(this))\' value="Nhận xét"/>' + '</td></tr>';
                            tableListing.append(row);
                        });
                    },
                    failure: function (response) { console.log(response.d); }
                });

            }

            //get jobs to do
            function getStaffs(salonId) {
                var staffListing = $('#staff-listing');
                //$.getJSON("/GUI/SystemService/Ajax/Api3s.aspx/GetStaff/" + salonId,
                //    function (data) {
                //        //console.log(data);
                //        //clean table
                //        staffListing.empty();
                //        //loop append jobs
                //        $.each(data, function (key, val) {
                //            // Add row
                //            var row = '<div class="radio"><label><input id="' + val.id + '" type="radio" name="optradio" onclick=\'chooseStaff(' + val.id + ',"' + val.fullName + '")\'>' + val.fullName + '</label></div>';
                //            staffListing.append(row);
                //        });
                //    });


                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Ajax/Api3s.aspx/GetStaff",
                    data: '{id : "' + salonId + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        //console.log(data);
                        //clean table
                        staffListing.empty();
                        //loop append jobs
                        $.each(response.d, function (key, val) {
                            // Add row
                            var row = '<div class="radio"><label><input id="' + val.id + '" type="radio" name="optradio" onclick=\'chooseStaff(' + val.id + ',"' + val.fullName + '")\'>' + val.fullName + '</label></div>';
                            staffListing.append(row);
                        });
                    },
                    failure: function (response) { console.log(response.d); }
                });

            }

            //push confirm dialog
            function postCheckList(dataJSON) {
                console.log(dataJSON);
                $.ajax({
                    type: 'POST',
                    url: '/GUI/SystemService/Ajax/Api3s.aspx/SaveCheck3S',
                    data: '{obj : \'' + JSON.stringify(dataJSON) + '\'}',
                    //data: JSON.stringify(dataJSON),
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json'
                });
            }

            //base 64 file upload
            function encodeImageFileAsURL() {
                var filesSelected = document.getElementById("inputFileToLoad").files;
                if (filesSelected.length > 0) {
                    var fileToLoad = filesSelected[0];
                    var fileReader = new FileReader();
                    fileReader.onload = function (fileLoadedEvent) {
                        var srcData = fileLoadedEvent.target.result; // data: base64
                        var newImage = document.createElement('img');
                        newImage.src = srcData;
                        document.getElementById("imgTest").innerHTML = newImage.outerHTML;
                    }
                    fileReader.readAsDataURL(fileToLoad);
                }
            }
            //dropzone file upload
        </script>
    </form>
</body>

</html>
