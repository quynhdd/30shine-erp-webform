﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/CSVC.Master" AutoEventWireup="true" CodeBehind="Upload_KTVS.aspx.cs" Inherits="_30shine.GUI.FrontEnd.KiemTraVS.Upload_KTVS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="main-content">
        <div class="container">
            <div class="row">
                <div class="top-title">Kiểm Tra Vệ Sinh</div>
                <div class="action-group">
                    <div class="col-sm-12 col-xs-12 button">
                        <a class="choose-image active" href="javascript://">Chọn ảnh</a>
                        <a class="upload" href="javascript://">Tải lên</a>
                        <a class="choose-again" href="javascript://">Làm lại</a>
                    </div>

                    <div class="col-sm-12 col-xs-12 hour-note">Khung giờ up ảnh: 9h - 10h / 19h - 20h</div>
                </div>
                <div class="img-preview col-sm-12 col-xs-12">
                    <div id="dZUpload" class="dropzone dz-work-flow">
                        <div class="dz-default dz-message wrap listing-img-upload">
                            Drop image here. 
                        </div>
                    </div>


                    <div>
                        <img src="/Assets/images/vssalon/vssalon.jpg" />
                    </div>
                    <div>
                        <img src="/Assets/images/vssalon/vssalon.jpg" />
                    </div>
                    <div>
                        <img src="/Assets/images/vssalon/vssalon.jpg" />
                    </div>
                    <div>
                        <img src="/Assets/images/vssalon/vssalon.jpg" />
                    </div>
                </div>
            </div>
        </div>
    </div>


    <style>
        #swipebox-slider .slide img { -webkit-transform: rotate(90deg); -moz-transform: rotate(90deg); -o-transform: rotate(90deg); -ms-transform: rotate(90deg); transform: rotate(90deg);}
        .delete-pending-img { margin-top: 10px; margin-bottom: 25px;}
        /*.upload-image-box:hover { background: #b91a1a;}
        .upload-image-box { margin: 0 auto;width: 150px; height: 32px; line-height: 32px; text-align: center; cursor: pointer; margin: 0 auto; color: #ffffff; margin-top: 25px; font-size: 13px; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; background: rgba(241,111,92,1); background: -moz-radial-gradient(center, ellipse cover, rgba(241,111,92,1) 0%, rgba(80,179,71,1) 0%, rgba(61,224,29,1) 0%, rgba(80,179,71,1) 100%); background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%, rgba(241,111,92,1)), color-stop(0%, rgba(80,179,71,1)), color-stop(0%, rgba(61,224,29,1)), color-stop(100%, rgba(80,179,71,1))); background: -webkit-radial-gradient(center, ellipse cover, rgba(241,111,92,1) 0%, rgba(80,179,71,1) 0%, rgba(61,224,29,1) 0%, rgba(80,179,71,1) 100%); background: -o-radial-gradient(center, ellipse cover, rgba(241,111,92,1) 0%, rgba(80,179,71,1) 0%, rgba(61,224,29,1) 0%, rgba(80,179,71,1) 100%); background: -ms-radial-gradient(center, ellipse cover, rgba(241,111,92,1) 0%, rgba(80,179,71,1) 0%, rgba(61,224,29,1) 0%, rgba(80,179,71,1) 100%); background: radial-gradient(ellipse at center, rgba(241,111,92,1) 0%, rgba(80,179,71,1) 0%, rgba(61,224,29,1) 0%, rgba(80,179,71,1) 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f16f5c', endColorstr='#50b347', GradientType=1 ); margin-bottom: 15px; }*/
        .degree90 { -webkit-transform: rotate(90deg); -moz-transform: rotate(90deg); -o-transform: rotate(90deg); -ms-transform: rotate(90deg); transform: rotate(90deg); height: 118px!important; width: 118px!important;}
        .dropzone .dz-preview .dz-success-mark { bottom: 0;}
        .dropzone .dz-preview .dz-success-mark svg { width: 41px;}
        .nhanvien { padding-left: 0; padding-right: 0;}
        .dropzone.dz-work-flow { border: none; display: none; padding: 0; }
        .dropzone.dz-work-flow .dz-preview .dz-remove { position: relative; top: 5px; display: none; }
        .dropzone.dz-work-flow .dz-preview { margin: 16px 16px 5px 16px; }
        /*#BtnUploadImg { background: url('/images/arrow-upload-icon.png'); width: 79px; height: 79px; background-size: 100% auto; position: fixed; right: 5px; bottom: 5px; z-index: 2000; cursor: pointer; }*/
        /*.table-listing td { border: 1px solid #bababa; padding: 7px 7px; text-align: center; }
        .table-listing tr td:first-child { font-family: Roboto Condensed Bold; }
        .table-listing tr td { text-align: left; }


        .table-listing tr.tr-service-color {
    background: /*#C0E6C0*/ /*#f7f7f7;*/
/*}*/
        .table-listing { position: relative; z-index: 1000; background: #ffffff; max-width: 260px; margin: 0 auto; min-width: 260px; margin-top: 20px;}
        .wrap { max-width: 480px; margin: 0 auto; min-width: 260px; display: table;}
        .listing-img-upload .thumb-wp { width: 120px; height: 120px; margin-right: 10px; margin-top: 10px; overflow: hidden; float: left; position: relative; border: 1px solid #dfdfdf; cursor: pointer; }
        .listing-img-upload .thumb-wp:last-child { margin-right: 0; margin-left: 10px;}*/
    </style>
    <form id="Form1" runat="server">
        <asp:HiddenField ID="HDF_CustomerCode" runat="server" ClientIDMode="Static" />
    </form>

        <script type="text/javascript">
            jQuery(document).ready(function () {

                //$('.delete-pending-img').bind('click', function () {
                //    var _BillId = $("#HDF_CustomerCode").val();
                //    Delete_Customer_Images_After_Upload(_BillId);
                //});
                //$('#btnXoaAnh').bind('click', function () {
                //    var _BillId = $("#HDF_CustomerCode").val();
                //    Delete_Customer_Images(_BillId);
                //});

                //==============================
                // Upload Images
                //==============================
                //$("#UploadFile").unbind("change").bind("change", function () {
                //    //readImage(this,$(this));
                //    //Plugin_UploadImages(this, $(this));
                //});
                excThumbWidth();

                //==============================
                // Upload Images - Dropzone
                //==============================
                Dropzone.autoDiscover = false;
                var Code, InitDropZone = true;
                $(".choose-image").bind("click", function () {
                    Code = $("#HDF_CustomerCode").val();
                    console.log('hello');
                    //if (!InitDropZone) {
                        $("#dZUpload").dropzone({
                            init: function () {
                                var myDropzone = this;
                                this.on("maxfilesexceeded", function (file) {
                                    myDropzone.removeFile(file);
                                });
                                this.on("success", function (file) {
                                    $('.dz-image').css({ "width": "100%", "height": "auto" });
                                });

                                var myDropZone = this;
                                $("#btnXoaAnh").bind('click', function () {
                                    myDropZone.removeAllFiles();
                                });
                            },
                            url: "/GUI/BackEnd/UploadImage/DropzoneVSSalon.ashx?Code=" + Code,
                            maxFiles: 4,
                            addRemoveLinks: true,
                            thumbnailWidth: null,
                            thumbnailHeight: null,
                            success: function (file, response) {
                                $("#dZUpload").show();
                                $("#btnXoaAnh").show();
                                var imgName = response;
                            },
                            error: function (file, response) {
                                $("#btnXoaAnh").show();
                                file.previewElement.classList.add("dz-error");
                            }
                        });
                        InitDropZone = true;
                    //}
                    $("#dZUpload").click();
                });

                // View Customer Detail
                $("#ViewDetail").bind("click", function () {
                    var code = $("#Code").val();
                    window.location.assign("/workflow/khach-hang?code=" + code);
                });

                $("#Code").bind("keydown", function (e) {
                    if (e.keyCode == 13) {
                        $("#ViewDetail").click();
                        return false;
                    }
                });

            });

            //=================================================
            // Xử lý ảnh
            //=================================================
            //draw image while upload
            var ListImgName = [],
                ListImgSrc = [],
                LIstImgDelete = [],
                ImgStandardWidth = null,
                ImgStandardHeight = null;

            function readImage(THIS, This) {
                var LoopName = 0;
                if (THIS.files.length > 0) {
                    for (var i = 0; i < THIS.files.length; i++) {
                        var FR = new FileReader();
                        FR.onload = function (e) {
                            var img = new Image();
                            img.src = e.target.result;
                            img.onload = function () {
                                ListImgSrc.push(img.src);
                                var width = ImgStandardWidth;
                                var height = ImgStandardWidth * img.height / img.width;
                                var left = 0;
                                var top = (ImgStandardHeight - height) / 2;
                                var ThumbWp = "<div class='thumb-wp'>" +
                                                "<img class='thumb' style='width:" + width + "px;height:" +
                                                height + "px;left:" + left + "px;top:" + top + "px;' " + "src='" + img.src + "' />" +
                                              "<span class=\"delete-thumb\" onclick=\"deleteThum($(this), '" + img.src + "')\"></span></div>";
                                This.parent().parent().find(".listing-img-upload").append($(ThumbWp));

                            };
                            console.log(e.target.result);
                        };
                        FR.readAsDataURL(THIS.files[i]);
                    }
                    THIS.parentNode.setAttribute("class", "upload-image-box active");
                }
            }

            function Plugin_UploadImages(THIS, This) {

            }

            function Bind_UserImagesToHDF() {
                var jsonImages = JSON.stringify(ListImgSrc);
                var jsonImagesDel = JSON.stringify(LIstImgDelete);
                $("#HDF_UserImages").val(jsonImages);
                $("#HDF_UserImagesDelete").val(jsonImagesDel);
            }

            function deleteThum(THIS, data, oldImg) {
                oldImg = oldImg || false;
                if (!oldImg) {
                    var index = ListImgSrc.indexOf(data);
                    if (index != -1) {
                        ListImgSrc.splice(index, 1);
                    }
                } else {
                    LIstImgDelete.push(data);
                }
                THIS.parent().remove();
            }

            function excThumbWidth() {
                var ImgStandardWidth = 600,
                    ImgStandardHeight = 600;
                var width = ImgStandardWidth,
                    height, left, top;
                $(".thumb-wp .thumb").each(function () {
                    height = ImgStandardWidth * $(this).height() / $(this).width();
                    left = 0;
                    top = (ImgStandardHeight - height) / 2;
                    $(this).css({ "width": width, "height": height, "left": left, "top": top });
                });
            }

            function AjaxUploadImage_BillService(ImageName) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Ajax/UploadImage.aspx/Add_User_Hair",
                    data: '{ImageName : "' + ImageName + '"}',
                    contentType: "application/Assets/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            +
                                ListImgName.push(mission.msg);
                        } else {
                            var msg = "Đã có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển";
                            showMsgSystem(msg, "warning");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function Delete_Customer_Images(BillId) {
                $.ajax({
                    type: "POST",
                    url: "/Ajax/Ajax.aspx/DeleteImgages",
                    data: '{_BillId : "' + BillId + '"}',
                    contentType: "application/Assets/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        if (response.d == "success") {
                            $('.dz-preview').remove();
                            $('#btnXoaAnh').hide();
                        }
                    },
                    failure: function (response) { }
                });
            }

            function Delete_Customer_Images_After_Upload(BillId) {
                $.ajax({
                    type: "POST",
                    url: "/Ajax/Ajax.aspx/DeleteImgages",
                    data: '{_BillId : "' + BillId + '"}',
                    contentType: "application/Assets/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        if (response.d == "success") {
                            var str = '<div class="wrap listing-img-upload"><div class="thumb-wp"><img src="/images/1.jpg" alt=""></div><div class="thumb-wp"><img src="/images/2.jpg" alt=""></div></div><div class="wrap listing-img-upload"><div class="thumb-wp"><img src="/images/3.jpg" alt=""></div><div class="thumb-wp"><img src="/images/4.jpg" alt=""></div></div>'
                            $('#img-pending').html('');
                            $('#img-pending').html(str);
                            $('.delete-pending-img').hide();
                        }
                    },
                    failure: function (response) { }
                });
            }
        </script>
    <%--<script src="/Assets/js/exif.js?v=1"></script>--%>
    <script src="/Assets/js/dropzone.js?v=16"></script>
    <link href="/Assets/css/dropzone.css" rel="stylesheet" />

    <script>
        $(window).load(function () {
            $('.listing-img-upload .thumb-wp').each(function () {
                var THIS = $(this);

                $("<img>").attr("src", $(".thumb").attr("src")).load(function () {
                    var ImgW = this.width;
                    var ImgH = this.height;
                    console.log(ImgW);
                    if ((ImgW > ImgH)) {
                        (THIS).find('img').addClass('degree90');
                    }
                });
            });
        });
    </script>
    <%--<link href="/Assets/css/swipebox.min.css" rel="stylesheet" />
    <script src="/Assets/js/jquery.swipebox.min.js"></script>
    <script type="text/javascript">
        ; (function ($) {

            $('.swipebox').swipebox();
            $('.swipebox').find('img').addClass('degree90');

        })(jQuery);
</script>--%>

    <style>
        .dropzone .dz-preview .dz-image {
      width: 600px;
      height: auto;
    }
    </style>

</asp:Content>
