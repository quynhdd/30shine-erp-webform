﻿using _30shine.Helpers;
using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Linq;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace FormService.Moduls
{
    public partial class CheckThings : System.Web.UI.Page
    {
        private string PageID = "CHECKCSVC";
        private bool Perm_Access = false;
        private bool Perm_ShowSalon = false;
        private int _StaffId = 0;
        private int _SalonId = 0;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermission_V2 permissionModel = new PermissionModel_V2();
                //var staffId = Convert.ToInt32(Session["User_Id"].ToString());
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, staffId);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, staffId);
                ////Perm_AllPermission = permissionModel.GetActionByActionNameAndPageId("Perm_AllPermission", PageID, staffId);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                //ContentWrap.Visible = false;
                //NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "Callback function", "init('" + _StaffId + "','" + _SalonId + "')", true);

        }

       
    }
}