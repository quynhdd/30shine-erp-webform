﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Customer_Filter.aspx.cs" Inherits="_30shine.UICustomer.Customer_Filter" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="CustomerFilter" ContentPlaceHolderID="CtMain" runat="server">


    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <%-- <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">--%>

    <style>
        .imageapent {
            width: 15px;
            height: 15px;
        }

        .image12 {
            width: 30px;
            height: 30px;
        }

        .degree90 {
            -webkit-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            -o-transform: rotate(90deg);
            -ms-transform: rotate(90deg);
            transform: rotate(90deg);
            height: 118px !important;
            width: 118px !important;
        }

        .btn-filter {
            background: #000000;
            color: #ffffff;
            width: 80px !important;
            height: 35px;
            line-height: 30px;
            float: left;
            width: auto;
            text-align: center;
            cursor: pointer;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            border-radius: 2px;
            margin: 0 5px;
        }

        .downImage .btn-filter:hover {
            color: #ffd800;
        }
    </style>
    
    <div class="wp sub-menu">
        <div class="wp960">
            <div class="wp content-wp">
                <ul class="ul-sub-menu" id="subMenu">
                    <li>Tìm kiếm khách hàng &nbsp;&#187; </li>
                    <li>
                        <%--<input type="text" id="txtFilter" class="form-control txtFilter" placeholder="Số điện thoại" runat="server" />--%>
                        <asp:TextBox data-value="0" data-field="customer.phone" CssClass="txtFilter form-control"
                            AutoCompleteType="Disabled" ID="txtFilter" ClientIDMode="Static" placeholder="Số điện thoại" runat="server"></asp:TextBox>
                    </li>
                    <li>
                        <span class="field-wp">
                            <asp:Button ID="btnFilter" CssClass="btn-send perm-btn-field btn-filter " runat="server" Text="Tìm kiếm" ClientIDMode="Static" OnClick="btnFilter_Click"></asp:Button>
                            <%--<input type="button" id="btnFilter" class="btn-send perm-btn-field btn-filter" value="Tìm kiếm" />--%>
                            <%--<input type="button" id="btnReset" class="btn-send perm-btn-field btn-filter" value="Reset Filter" onClick="window.location.reload();" />--%>
                        </span>
                    </li>
                </ul>

            </div>
        </div>
    </div>
    
    <div class="wp customer-add customer-listing"
        <!-- Customer History -->
        <div class="wp960 content-wp content-customer-history">
            <div class="container">
                <div class="row">
                    <% if (objReport.CustomerId > 0)
                        { %>
                    <!-- Common Information -->
                    <div class="col-xs-12 line-head">
                        <strong class="st-head" style="margin-top: 0px;">
                            <i class="fa fa-file-text"></i>
                            Thông tin khách hàng
                        <%--<span class="view-more-infor">(Xem thông tin đầy đủ)</span>--%>
                        </strong>
                    </div>
                     <div class="col-xs-12 common-infor">
                            <p>
                                Họ tên&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<%= objReport.CustomerName %>
                            </p>
                            <p>
                                Tuổi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<%= objReport.CustomerAge %>
                            </p>
                            <p>
                                Số ĐT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<%= objReport.CustomerPhone %>
                            </p>
                            <p>
                                Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<%= objReport.Email %>
                            </p>
                            <p>
                                Salon đến lần đầu&nbsp;:&nbsp;&nbsp;<%= objReport.FirstSalon %>
                            </p>
                         <strong></strong>
                         <% 
                             // Tạm lấy data
                             var elementMember = "";
                             bool isMember = objReport.MemberType == 1 ? true : false;
                             bool isPeriod = isMember && objReport.MemberEndTime >= DateTime.Now.Date ? false : true;

                             elementMember += "<p> ShineMember &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<strong>" + (isMember ? " Có" : " Không");
                             elementMember += (isMember && !isPeriod ? " - Còn hạn" : (isMember && isPeriod ? " - Hết hạn" : ""));
                             elementMember += (!string.IsNullOrEmpty(objReport.ProductName) && isMember && !isPeriod ? (" - " + objReport.ProductName) : "");
                             elementMember += "</strong></p>";
                             elementMember += (!isMember && isPeriod ? "" : ("<p> Ngày bắt đầu &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: " + string.Format("{0:dd-MM-yyyy}", objReport.MemberStartTime) + 
                                 "</p><p> Ngày kết thúc &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : " + string.Format("{0:dd-MM-yyyy}", objReport.MemberEndTime) + "</p>"));
                         %>

                         <%=elementMember %>
                        </div>
                    <!--/ Common Information -->

                    <!-- Statistic Value -->
                     <% if (objReport.TotalReport != null)
                         { %>
                    <div class="col-xs-12 line-head">
                        <strong class="st-head" style="border-bottom: none; margin-bottom: 3px;">
                            <i class="fa fa-file-text"></i>Thống kê giá trị
                        </strong>
                    </div>
                    <div class="col-xs-12 customer-value-statistic">
                        <div class="table-wp">
                            <table class="table-add table-listing" style="width: 400px;">
                                <tbody>
                                    <tr>
                                        <td>Tổng số lần sử dụng dịch vụ</td>
                                        <td class="be-report-price">
                                            <%= objReport.TotalReport.TongSoDV %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Tổng chi phí</td>
                                        <td class="be-report-price">
                                            <%= objReport.TotalReport.TongChiPhi %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Chi phí dịch vụ</td>
                                        <td class="be-report-price">
                                            <%= objReport.TotalReport.ChiPhiDV %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Chi phí sản phẩm</td>
                                        <td class="be-report-price">
                                            <%= objReport.TotalReport.ChiPhiMP %>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="customer-detail-service">
                            <p class="_p">- Chi tiết</p>
                            <div class="tbl-wp">
                                <div class="table-wp">
                                    <table class="table-add table-listing" style="width: 400px;">
                                        <tbody>
                                            <tr>
                                                <td style="font-family: Roboto Condensed Bold;">Dịch vụ</td>
                                                <td><%= objReport.TotalUsesService %></td>
                                                <td class="be-report-price"><%= objReport.TotalMoneyService %></td>
                                            </tr>
                                            <% if (objReport.ServiceDetail.Count > 0)
                                                { %>
                                            <% foreach (var v in objReport.ServiceDetail)
                                                { %>

                                            <tr>
                                                <td style="font-family: Roboto Condensed Bold;"><%= v.Name %></td>
                                                <td><%= v.SolanSD %></td>
                                                <td class="be-report-price"><%= v.ChiPhiDV %></td>
                                            </tr>

                                            <% } %>
                                            <% } %>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="table-wp">
                                    <table class="table-add table-listing" style="width: 400px; margin-top: 15px;">
                                        <tbody>
                                            <tr>
                                                <td style="font-family: Roboto Condensed Bold;">Sản phẩm</td>
                                                <td><%= objReport.TotalUsesProduct %></td>
                                                <td class="be-report-price"><%= objReport.TotalMoneyProduct %></td>
                                            </tr>
                                            <% if (objReport.ProductDetail.Count > 0)
                                                { %>
                                            <% foreach (var v in objReport.ProductDetail)
                                                { %>
                                            <tr>
                                                <td style="font-family: Roboto Condensed Bold;"><%= v.Name %></td>
                                                <td><%= v.SolanSD %></td>
                                                <td class="be-report-price"><%= v.ChiPhiDV %></td>
                                            </tr>
                                            <% } %>
                                            <% } %>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/ Statistic Value -->
                    <% } %>

                    <!-- History -->
                    <% if (lstHis.Count > 0)
                        { %>
                    <div class="history-timeline">
                        <div class="line-h"></div>
                        <div class="col-xs-12 elm">
                            <div class="row">
                                <div class="col-xs-2 left">
                                    <div class="wrap">
                                        <div class="time-value" style="font-family: Roboto Condensed Bold; font-size: 15px;">Lịch sử</div>
                                    </div>
                                </div>
                                <div class="col-xs-10 right"></div>
                            </div>
                        </div>
                        <% foreach (var v in lstHis)
                            {%>
                                <div class="col-xs-9 elm">
                                <div class="row">
                                    <div class="col-xs-2 left">
                                        <div class="wrap">
                                            <div class="time-value"><%= String.Format("{0:dd/MM/yyyy}", v.CreatedDate)  %></div>
                                            <div class="line-v"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-10 right">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <table class="table-add table-listing">
                                                    <tbody>
                                                        <tr class="tr-service-color">
                                                            <td>Thông tin chung</td>
                                                            <td colspan="2">
                                                                <i style="width: 100%; float: left;"> - Stylist : <%= v.StylistName %></i>
                                                                <i style="width: 100%; float: left;"> - Tgian checkin : <%= String.Format("{0:HH:mm:ss dd/MM/yyyy}", v.CreatedDate)  %></i>
                                                                <i style="width: 100%; float: left;"> - Ngày book : <%= String.Format("{0:dd/MM/yyyy}", v.DatedBook) %></i>
                                                                <i style="width: 100%; float: left;"> - Tgian chờ : <%= v.WaitTimeInProcedure %></i>
                                                                <i style="width: 100%; float: left;"> - Salon : <%= v.SalonName %></i>
                                                                <i style="width: 100%; float: left;"> - Đánh giá : <%= v.DanhGia %></i>
                                                            </td>
                                                        </tr>
                                                        <% if (v.ServiceIds != null && v.ServiceIds.Count > 0)
                                                            {
                                                                foreach (var v1 in v.ServiceIds)
                                                                { %>
                                                                        <tr class="tr-service-color">
                                                                            <% if (v1.Id == v.ServiceIds[0].Id)
                                                                                {%>
                                                                                    <td style="min-width: 90px;">Dịch vụ</td>
                                                                            <% }
                                                                                else
                                                                                { %>
                                                                            <td style="min-width: 90px;"></td>
                                                                            <% } %>
                                                                            <td><%= v1.Name %></td>
                                                                            <td class="be-report-price"><%= v1.Price %></td>
                                                                        </tr>
                                                                    <% } %>
                                                             <% } %>
                                                       
                                                         <% if (v.ProductIds != null && v.ProductIds.Count > 0)
                                                             {
                                                                 foreach (var v2 in v.ProductIds)
                                                                 { %>
                                                                       <tr class="tr-service-color">
                                                                           <% if (v2.Id == v.ProductIds[0].Id)
                                                                               {%>
                                                                                    <td style="min-width: 90px;">Sản phẩm</td>
                                                                            <% }
                                                                                else
                                                                                { %>
                                                                            <td style="min-width: 90px;"></td>
                                                                            <% } %>
                                                                            <td><%= v2.Name %></td>
                                                                            <td class="be-report-price"><%= v2.Price %></td>
                                                                        </tr> 
                                                                  <% } %>
                                                             <% } %>

                                                        <tr class="tr-total-color">
                                                            <td>Tổng chi phí</td>
                                                            <td></td>
                                                            <td class="be-report-price"><%= v.TotalMoney %></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="wrap listing-img-upload">
                                                    <% if (v.Images != null && v.Images.Length > 0)
                                                        {
                                                            foreach (var v3 in v.Images)
                                                            { %>
                                                    <div class="thumb-wp">
                                                        <img class="thumb" alt="" title="" src="<%= v3 %>" style="width: 120px; height: 160px; left: 0px; top: -20px; z-index:1;">
                                                        <div class="thumb-cover"></div>
                                                        <div style="z-index: 2; position:absolute; display:none;" class="downImage">
                                                            <a href="<%= v3 %>" class="btn-send perm-btn-field btn-filter" download style="margin-top: 40px; margin-left: 20px;">Tải ảnh</a>
                                                        </div>
                                                    </div>
                                                          <% } %>
                                                             <% } %>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         <% } %>
                    </div>
                     <% } %>
                    <!--/ History -->
                    <% } %>
                </div>
            </div>
        </div> 
        <!--/ Customer History -->

        <%--<asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />--%>
        <asp:HiddenField runat="server" ID="HDF_CustomerId" ClientIDMode="Static" />
    </div>
    <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
    <script>

        //console.log(customerID);
        jQuery(document).ready(function () {
            $(".thumb-wp").hover(
                function () {
                    $(this).find(".downImage").css("display", "block");
                }, function () {
                    $(this).find(".downImage").css("display", "none");
                }
            );
            $("#glbCustomer").addClass("active");
            $(".li-edit").addClass("active");

            // Price format
            UP_FormatPrice('.be-report-price');

            // Show form customer information
            $(".view-more-infor").bind("click", function () {
                $(".content-customer-detail").show();
            });
            $("#CalcelAddFbcv").bind("click", function () {
                $(".content-customer-detail").hide();
            });

            // fix thumbnail
            excThumbWidth();

            //============================
            // Show System Message
            //============================ 
            var qs = getQueryStrings();
            showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);

            if (qs["show_form_infor"] == "true") {
                $(".content-customer-detail").show();
            }

            $(window).load(function () {
                $('.listing-img-upload .thumb-wp').each(function () {
                    var THIS = $(this);

                    $("<img>").attr("src", $(".thumb").attr("src")).load(function () {
                        var ImgW = this.width;
                        var ImgH = this.height;
                        //console.log(ImgW);
                        if ((ImgW > ImgH)) {
                            (THIS).find('img').addClass('degree90');
                        }
                    });
                });
            });

            //$("#btnFilter").click(function(){
            //    addLoading();
            //    var phone = $(".txtFilter").val().trim();
            //    if (!checkPhone(phone)) {
            //        alert("Sai định dạng số điện thoại.");
            //        removeLoading();
            //        $(".txtFilter").focus();
            //        return;
            //    }
            //    else {
            //        $("#BtnFakeUP").click();
            //    }
            //});
        });
        /*
            Check số phone
        */
        function checkPhone(phone) {
            phone = String(phone);
            var x = phone.substring(0, 1);

            if (phone.length < 10 || phone.length > 11 || x != '0' || validatePhone(phone) == false)
                return false;
            else
                return true;
        }
        function validatePhone(txtPhone) {
            var filter = /^[0-9-+]+$/;
            if (filter.test(txtPhone)) {
                return true;
            }
            else {
                return false;
            }
        }

        function excThumbWidth() {
            var ImgStandardWidth = 120,
                ImgStandardHeight = 120;
            var width = ImgStandardWidth,
                height, left, top;
            $(".thumb-wp .thumb").each(function () {
                height = ImgStandardWidth * $(this).height() / $(this).width();
                left = 0;
                top = (ImgStandardHeight - height) / 2;
                $(this).css({ "width": width, "height": height, "left": left, "top": top });
            });
        }

        function showDom(selector, status) {
            status = typeof status == "boolean" ? status : false;
            console.log(status);
            if (status) {
                $(selector).show();
            }
        }
    </script>

    <%--</asp:Panel>--%>
</asp:Content>

