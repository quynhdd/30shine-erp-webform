﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using LinqKit;
using ExportToExcel;
using System.Web.UI.HtmlControls;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.Interface;

namespace _30shine.GUI.UICustomer
{
    public partial class Customer_Listing : System.Web.UI.Page
    {
        private string PageID = "";
        protected Paging PAGING = new Paging();
        private int TotalPage;
        private bool Perm_Access = false;
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        private bool Perm_ViewAllData = false;
        private Expression<Func<Customer, bool>> Where = PredicateBuilder.True<Customer>();
        private DateTime StartTime = new DateTime();
        private int SalonId;
        /// <summary>
        /// check permission
        /// </summary>
        protected void checkPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //string perName = Session["User_Permission"].ToString();
                //string url = Request.Url.AbsolutePath.ToString();
                //Solution_30shineEntities db = new Solution_30shineEntities();
                //var pem = (from m in db.Tbl_Permission_Map
                //           join mn in db.Tbl_Permission_Menu on m.mID equals mn.mID
                //           where m.pName == perName && mn.Url_Main.EndsWith(url) || mn.Url_Rewrite.EndsWith(url)
                //           select new { m.aID }
                //           ).FirstOrDefault();
                //if (pem != null)
                //{
                //    var pemArray = pem.aID.Split(',');
                //    if (Array.IndexOf(pemArray, "1") > -1)
                //    {
                //        Perm_Access = true;
                //        if (Array.IndexOf(pemArray, "2") > -1)
                //        {
                //            Perm_ViewAllData = true;
                //        }

                //        if (Array.IndexOf(pemArray, "4") > -1)
                //        {
                //            Perm_Edit = true;
                //        }
                //        if (Array.IndexOf(pemArray, "5") > -1)
                //        {
                //            Perm_Delete = true;
                //        }
                //    }
                //    else
                //    {
                //        ContentWrap.Visible = false;
                //        NotAllowAccess.Visible = true;
                //    }

                //}
                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();

            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            checkPermission();

            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                Bind_Thread();
                GenWhere();
                Init_Paging();
                Bind_RptCustomer();
                Bind_Paging();
                RemoveLoading();
            }
            else
            {
                GenWhere();
            }
        }

        private void GenWhere()
        {
            CultureInfo culture = new CultureInfo("vi-VN");
            var _BillService = new List<BillService>();
            StartTime = Convert.ToDateTime("1/8/2015", culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
            string SgtValue;
            int threadId = Convert.ToInt32(Thread.SelectedValue);

            if (IsPostBack)
            {
                if (TxtDateTimeFrom.Text != "")
                {
                    DateTime _FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                    DateTime _ToDate;
                    if (TxtDateTimeTo.Text != "")
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                    }
                    else
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(1);
                    }
                    Where = Where.And(w => w.CreatedDate > _FromDate && w.CreatedDate < _ToDate);
                }

                switch (HDF_Suggestion_Field.Value)
                {
                    case "customer.code": SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.Customer_Code.Contains(SgtValue));
                        break;
                    case "customer.name": SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.Fullname.Contains(SgtValue));
                        break;
                    case "customer.phone": SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.Phone.Contains(SgtValue));
                        break;
                }

                SalonId = Convert.ToInt32(Salon.SelectedValue);
                if (SalonId > 0)
                {
                    Where = Where.And(p => p.SalonId == SalonId);
                }
                else
                {
                    //Where = Where.And(p => p.SalonId > 0);
                }
            }

            if (threadId > 0)
            {
                Where = Where.And(w=>w.Info_Flow == threadId);
            }


            Where = Where.And(w => w.IsDelete != 1);
        }

        private void Bind_Thread()
        {
            using (var db = new Solution_30shineEntities())
            {
                var Key = 0;
                var thread = db.SocialThreads.Where(w=>w.IsDelete != 1 && w.stType == 1).ToList();
                Thread.DataTextField = "Name";
                Thread.DataValueField = "Id";

                ListItem item = new ListItem("Chọn nguồn thông tin", "0");
                Thread.Items.Insert(Key, item);
                if (thread.Count > 0)
                {
                    foreach (var v in thread)
                    {
                        item = new ListItem(v.Name, v.Id.ToString());
                        Thread.Items.Insert(++Key, item);
                    }
                }
                Thread.SelectedIndex = 0;
            }
        }

        private void Bind_RptCustomer()
        {
            var _Customers = Get_LSTCustomer(true);
            RptCustomer.DataSource = _Customers;
            RptCustomer.DataBind();
        }

        public List<Customer_Listing_Struct> Get_LSTCustomer(bool paging)
        {
            using (var db = new Solution_30shineEntities())
            {
                List<Customer_Listing_Struct> _CustomersReturn = new List<Customer_Listing_Struct>();
                List<Customer_Listing_Struct> LST = new List<Customer_Listing_Struct>();
                Customer_Listing_Struct _Cus = new Customer_Listing_Struct();
                var _Customers = new List<Customer>();
                string Where_Time = "";
                string Where_SalonId = "";
                string sql;
                int SalonId = Convert.ToInt32(Salon.SelectedValue);
                if (SalonId > 0)
                {
                    Where_SalonId += "and SalonId = " + SalonId;
                }

                if (TxtDateTimeFrom.Text != "")
                {
                    CultureInfo culture = new CultureInfo("vi-VN");
                    DateTime _FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                    DateTime _ToDate;
                    string TimeFrom = String.Format("{0:MM/dd/yyyy}", _FromDate);
                    if (TxtDateTimeTo.Text != "")
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                    }
                    else
                    {

                        _ToDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(1);
                    }
                    string TimeTo = String.Format("{0:MM/dd/yyyy}", _ToDate);
                    Where_Time += "and (CreatedDate between '" + TimeFrom + "' and '" + TimeTo + "')";
                }

                if (JustBuyProduct.Checked == true)
                {
                    sql = @"select d.*,
                            (select TenTinhThanh from TinhThanh where ID = d.CityId) as CityName,
                            (select TenQuanHuyen from QuanHuyen where ID = d.DistrictId) as DistrictName,
                            (select Name from SocialThread where Id = d.Info_Flow) as InforFlowName
                            from
                            (select *
                            from BillService as b
                            where (ProductIds is not null and ProductIds != '') and (ServiceIds is null or ServiceIds = '') " +
                            Where_Time +
                            Where_SalonId +
                        //@"and ((select COUNT(*) from BillService where CustomerCode = b.CustomerCode) > 1)
                            @"and CustomerCode != ''
                            and IsDelete != 1
                            ) as c
                            inner join Customer as d
                            on c.CustomerCode = d.Customer_Code";
                    List<Customer> LST2 = db.Customers.SqlQuery(sql).OrderByDescending(o => o.Id).ToList();
                    List<Customer_Listing_Struct> Temp = new List<Customer_Listing_Struct>();

                    if (LST2.Count > 0)
                    {
                        int CountService = 0;
                        int index = -1;
                        int integer;
                        foreach (var v in LST2)
                        {
                            CountService = db.BillServices.Count(w => w.CustomerCode == v.Customer_Code && w.ServiceIds != null && w.ServiceIds != "" && w.IsDelete != 1);
                            if (CountService == 0)
                            {
                                index = Temp.FindIndex(fi => fi.Id == v.Id);
                                if (index == -1)
                                {
                                    var money = db.BillServices.Where(w => w.CustomerCode == v.Customer_Code && w.IsDelete != 1).Sum(s => s.TotalMoney);
                                    var times = db.BillServices.Count(w => w.CustomerCode == v.Customer_Code && w.IsDelete != 1);
                                    _Cus = new Customer_Listing_Struct();
                                    _Cus.Id = v.Id;
                                    _Cus.Fullname = v.Fullname;
                                    _Cus.Customer_Code = v.Customer_Code;
                                    _Cus.TotalMoney = 0;
                                    _Cus.Phone = v.Phone;
                                    _Cus.Age = Convert.ToInt32(v.Age);
                                    _Cus.Address = v.Address;
                                    _Cus.CreatedDate = Convert.ToDateTime(v.CreatedDate);
                                    _Cus.Times = int.TryParse(times.ToString(), out integer) ? integer : 0;
                                    _Cus.LifeValue = int.TryParse(money.ToString(), out integer) ? integer : 0;
                                    _Cus.TenTinhThanh = v.CityName;
                                    _Cus.TenQuanHuyen = v.DistrictName;
                                    _Cus.InforFlowName = v.InforFlowName;
                                    Temp.Add(_Cus);
                                }
                            }
                        }
                        if (paging)
                        {
                            _CustomersReturn = Temp.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                        }
                        else
                        {
                            _CustomersReturn = Temp;
                        }
                    }
                    TotalPage = Convert.ToInt32(Math.Ceiling((double)Temp.Count / PAGING._Segment));
                }
                else if (FirstTimeService.Checked == true)
                {
                    sql = @"select d.Id, d.Fullname, d.Customer_Code, c.LifeValue as TotalMoney, d.Phone, 
                            COALESCE(d.Age, 0) as Age, d.[Address], tt.TenTinhThanh, qh.TenQuanHuyen, st.Name as InforFlowName,
                            d.CreatedDate, Times,  c.LifeValue, ProductIds = '', ServiceIds = ''
                            from Customer as d
                            inner join
                            (
	                            select a.CustomerCode, COUNT(a.Id) as Times, COALESCE(SUM(a.TotalMoney), 0) as LifeValue
	                            from BillService as a
	                            inner join
	                            (
		                            select CustomerCode
		                            from BillService
		                            where IsDelete != 1 and Pending != 1
		                            and CustomerCode != ''" +
		                            Where_Time +
                                    Where_SalonId +
		                            @"group by CustomerCode
	                            ) as b
	                            on a.CustomerCode = b.CustomerCode
	                            where a.IsDelete != 1 and a.Pending != 1
	                            and a.ServiceIds != '' and a.ServiceIds is not null
	                            group by a.CustomerCode
                            ) as c
                            on c.CustomerCode = d.Customer_Code
                            inner join TinhThanh as tt on d.CityId = tt.ID
                            inner join QuanHuyen as qh on d.DistrictId = qh.ID
                            inner join SocialThread as st on d.Info_Flow = st.Id
                            where c.Times = 1";
                    var Temp = db.Database.SqlQuery<Customer_Listing_Struct>(sql).OrderByDescending(o => o.Id).ToList();
                    if (paging)
                    {
                        _CustomersReturn = Temp.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                    }
                    else
                    {
                        _CustomersReturn = Temp;
                    }
                    TotalPage = Convert.ToInt32(Math.Ceiling((double)Temp.Count / PAGING._Segment));
                }
                else
                {
                    if (paging)
                    {
                        _Customers = db.Customers.AsExpandable().Where(Where).OrderByDescending(o => o.Id).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                    }
                    else
                    {
                        _Customers = db.Customers.AsExpandable().Where(Where).OrderByDescending(o => o.Id).ToList();
                    }
                    LST = _Customers.Join(db.BillServices,
                                        a => a.Customer_Code,
                                        b => b.CustomerCode,
                                        (a, b) => new
                                        {
                                            a.Id,
                                            a.Fullname,
                                            a.Customer_Code,
                                            b.TotalMoney,
                                            a.Phone,
                                            a.Age,
                                            a.Address,
                                            a.CityId,
                                            a.DistrictId,
                                            a.Info_Flow,
                                            a.CreatedDate,
                                            BillIsDelete = b.IsDelete,
                                            BillCreatedDate = b.CreatedDate,
                                            b.ProductIds,
                                            b.ServiceIds
                                        }
                                    ).Where(w => w.BillCreatedDate >= StartTime && w.BillIsDelete != 1)
                                    .Join(db.TinhThanhs,
                                        a => a.CityId,
                                        b => b.ID,
                                        (a, b) => new
                                        {
                                            a.Id,
                                            a.Fullname,
                                            a.Customer_Code,
                                            a.TotalMoney,
                                            a.Phone,
                                            a.Age,
                                            a.Address,
                                            b.TenTinhThanh,
                                            a.DistrictId,
                                            a.Info_Flow,
                                            a.CreatedDate
                                        }
                                    )
                                    .Join(db.QuanHuyens,
                                        a => a.DistrictId,
                                        b => b.ID,
                                        (a, b) => new
                                        {
                                            a.Id,
                                            a.Fullname,
                                            a.Customer_Code,
                                            a.TotalMoney,
                                            a.Phone,
                                            a.Age,
                                            a.Address,
                                            a.TenTinhThanh,
                                            b.TenQuanHuyen,
                                            a.Info_Flow,
                                            a.CreatedDate
                                        }
                                    )
                                    .Join(db.SocialThreads,
                                        a => a.Info_Flow,
                                        b => b.Id,
                                        (a, b) => new
                                        {
                                            a.Id,
                                            a.Fullname,
                                            a.Customer_Code,
                                            a.TotalMoney,
                                            a.Phone,
                                            a.Age,
                                            a.Address,
                                            a.TenTinhThanh,
                                            a.TenQuanHuyen,
                                            InforFlowName = b.Name,
                                            a.CreatedDate
                                        }
                                    )
                                    .GroupBy(g => g.Customer_Code)
                                    .Select(s => new Customer_Listing_Struct
                                    {
                                        Id = s.First().Id,
                                        Fullname = s.First().Fullname,
                                        Customer_Code = s.First().Customer_Code,
                                        TotalMoney = Convert.ToInt32(s.First().TotalMoney),
                                        Phone = s.First().Phone,
                                        Age = Convert.ToInt32(s.First().Age),
                                        Address = s.First().Address,
                                        TenTinhThanh = s.First().TenTinhThanh,
                                        TenQuanHuyen = s.First().TenQuanHuyen,
                                        InforFlowName = s.First().InforFlowName,
                                        CreatedDate = Convert.ToDateTime(s.First().CreatedDate),
                                        Times = s.Count(),
                                        LifeValue = Convert.ToInt32(s.Sum(sum => sum.TotalMoney))
                                    }).ToList();


                    if (_Customers.Count > 0)
                    {
                        var index = -1;
                        var loop = 0;
                        foreach (var v in _Customers)
                        {
                            index = LST.FindIndex(fi => fi.Id == v.Id);
                            if (index != -1)
                            {
                                _CustomersReturn.Add(LST[index]);
                            }
                            else
                            {
                                _Cus = new Customer_Listing_Struct();
                                _Cus.Id = v.Id;
                                _Cus.Fullname = v.Fullname;
                                _Cus.Customer_Code = v.Customer_Code;
                                _Cus.TotalMoney = 0;
                                _Cus.Phone = v.Phone;
                                _Cus.Age = Convert.ToInt32(v.Age);
                                _Cus.Address = v.Address;
                                _Cus.CreatedDate = Convert.ToDateTime(v.CreatedDate);
                                _Cus.Times = 0;
                                _Cus.LifeValue = 0;
                                var TinhThanh = db.TinhThanhs.FirstOrDefault(w => w.ID == v.CityId);
                                _Cus.TenTinhThanh = TinhThanh != null ? TinhThanh.TenTinhThanh : "";
                                var QuanHuyen = db.QuanHuyens.FirstOrDefault(w => w.ID == v.DistrictId);
                                _Cus.TenQuanHuyen = QuanHuyen != null ? QuanHuyen.TenQuanHuyen : "";
                                var FlowName = db.SocialThreads.FirstOrDefault(w => w.Id == v.Info_Flow);
                                _Cus.InforFlowName = FlowName != null ? FlowName.Name : "";

                                _CustomersReturn.Add(_Cus);
                            }
                            loop++;
                        }
                    }
                    TotalPage = Get_TotalPage();                    
                }
                

                return _CustomersReturn;     
            }
        }

        public List<Customer_Listing_Struct> Get_LSTCustomer_NotBuyProduct(bool paging)
        {
            using (var db = new Solution_30shineEntities())
            {
                List<Customer_Listing_Struct> _CustomersReturn = new List<Customer_Listing_Struct>();
                List<Customer_Listing_Struct> LST = new List<Customer_Listing_Struct>();
                Customer_Listing_Struct _Cus = new Customer_Listing_Struct();
                var _Customers = new List<Customer>();

                if (paging)
                {
                    _Customers = db.Customers.AsExpandable().Where(Where).OrderByDescending(o => o.Id).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                }
                else
                {
                    _Customers = db.Customers.AsExpandable().Where(Where).OrderByDescending(o => o.Id).ToList();
                }
                LST = _Customers.Join(db.BillServices,
                                    a => a.Customer_Code,
                                    b => b.CustomerCode,
                                    (a, b) => new
                                    {
                                        a.Id,
                                        a.Fullname,
                                        a.Customer_Code,
                                        b.TotalMoney,
                                        a.Phone,
                                        a.Age,
                                        a.Address,
                                        a.CityId,
                                        a.DistrictId,
                                        a.Info_Flow,
                                        a.CreatedDate,
                                        BillIsDelete = b.IsDelete,
                                        BillCreatedDate = b.CreatedDate,
                                        b.ProductIds,
                                        b.ServiceIds
                                    }
                                ).Where(w => w.BillCreatedDate >= StartTime && w.BillIsDelete != 1 && w.ProductIds != "")
                                .Join(db.TinhThanhs,
                                    a => a.CityId,
                                    b => b.ID,
                                    (a, b) => new
                                    {
                                        a.Id,
                                        a.Fullname,
                                        a.Customer_Code,
                                        a.TotalMoney,
                                        a.Phone,
                                        a.Age,
                                        a.Address,
                                        b.TenTinhThanh,
                                        a.DistrictId,
                                        a.Info_Flow,
                                        a.CreatedDate,
                                        a.ProductIds
                                    }
                                )
                                .Join(db.QuanHuyens,
                                    a => a.DistrictId,
                                    b => b.ID,
                                    (a, b) => new
                                    {
                                        a.Id,
                                        a.Fullname,
                                        a.Customer_Code,
                                        a.TotalMoney,
                                        a.Phone,
                                        a.Age,
                                        a.Address,
                                        a.TenTinhThanh,
                                        b.TenQuanHuyen,
                                        a.Info_Flow,
                                        a.CreatedDate,
                                        a.ProductIds
                                    }
                                )
                                .Join(db.SocialThreads,
                                    a => a.Info_Flow,
                                    b => b.Id,
                                    (a, b) => new
                                    {
                                        a.Id,
                                        a.Fullname,
                                        a.Customer_Code,
                                        a.TotalMoney,
                                        a.Phone,
                                        a.Age,
                                        a.Address,
                                        a.TenTinhThanh,
                                        a.TenQuanHuyen,
                                        InforFlowName = b.Name,
                                        a.CreatedDate,
                                        a.ProductIds
                                    }
                                )
                                .GroupBy(g => g.Customer_Code)
                                .Select(s => new Customer_Listing_Struct
                                {
                                    Id = s.First().Id,
                                    Fullname = s.First().Fullname,
                                    Customer_Code = s.First().Customer_Code,
                                    TotalMoney = Convert.ToInt32(s.First().TotalMoney),
                                    Phone = s.First().Phone,
                                    Age = Convert.ToInt32(s.First().Age),
                                    Address = s.First().Address,
                                    TenTinhThanh = s.First().TenTinhThanh,
                                    TenQuanHuyen = s.First().TenQuanHuyen,
                                    InforFlowName = s.First().InforFlowName,
                                    CreatedDate = Convert.ToDateTime(s.First().CreatedDate),
                                    Times = s.Count(),
                                    LifeValue = Convert.ToInt32(s.Sum(sum => sum.TotalMoney)),
                                }).ToList();

                var _CustomerFull = Get_LSTCustomer(false);
                if (_CustomerFull.Count > 0)
                {
                    var index = -1;
                    foreach (var v in _CustomerFull)
                    {
                        index = LST.FindIndex(fi => fi.Id == v.Id);
                        if (index == -1)
                        {
                            _CustomersReturn.Add(v);
                        }
                    }
                }

                return _CustomersReturn;
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            Init_Paging();
            Bind_RptCustomer();
            Bind_Paging();
            ScriptManager.RegisterStartupScript(UPTableListing, this.GetType(), "Format Price", "UP_FormatPrice('.be-report-price'); SetActiveTableListing();", true);
            RemoveLoading();
        }

        protected void Init_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? (HDF_Page.Value != "" ? Convert.ToInt32(HDF_Page.Value) : 1) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;            
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;            
        }

        protected void Bind_Paging()
        {
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? TotalPage : PAGING.TotalPage;
            PAGING._Paging = PAGING.Make_Paging();
            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }      

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var _BillService = new List<BillService>();
                var Where = PredicateBuilder.True<Customer>();
                string SgtValue;
                int Count = 0;

                if (JustBuyProduct.Checked != true)
                {
                    if (TxtDateTimeFrom.Text != "")
                    {
                        DateTime _FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                        DateTime _ToDate;
                        if (TxtDateTimeTo.Text != "")
                        {
                            _ToDate = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                        }
                        else
                        {
                            _ToDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(1);
                        }
                        Where = Where.And(w => w.CreatedDate > _FromDate && w.CreatedDate < _ToDate);
                    }

                    switch (HDF_Suggestion_Field.Value)
                    {
                        case "customer.code": SgtValue = HDF_Suggestion_Code.Value;
                            Where = Where.And(w => w.Customer_Code.Contains(SgtValue));
                            break;
                        case "customer.name": SgtValue = HDF_Suggestion_Code.Value;
                            Where = Where.And(w => w.Fullname.Contains(SgtValue));
                            break;
                        case "customer.phone": SgtValue = HDF_Suggestion_Code.Value;
                            Where = Where.And(w => w.Phone.Contains(SgtValue));
                            break;
                    }

                    Where = Where.And(w => w.IsDelete != 1);

                    int SalonId = Convert.ToInt32(Salon.SelectedValue);
                    if (SalonId > 0)
                    {
                        Where = Where.And(p => p.SalonId == SalonId);
                    }
                    else
                    {
                        Where = Where.And(p => p.SalonId > 0);
                    }

                    Count = db.Customers.AsExpandable().Count(Where);
                }
                else
                {
                    string Condition_Time = "";
                    if (TxtDateTimeFrom.Text != "")
                    {
                        DateTime _FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                        DateTime _ToDate;
                        string TimeFrom = String.Format("{0:MM/dd/yyyy}", _FromDate);
                        if (TxtDateTimeTo.Text != "")
                        {
                            _ToDate = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                        }
                        else
                        {
                            _ToDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(1);
                        }
                        string TimeTo = String.Format("{0:MM/dd/yyyy}", _ToDate);
                        Condition_Time += "and (CreatedDate between '" + TimeFrom + "' and '" + TimeTo + "')";
                    }

                    string sql = @"select d.*, c.times_come_salon as CustomerReturn,
                            (select TenTinhThanh from TinhThanh where ID = d.CityId) as CityName,
                            (select TenQuanHuyen from QuanHuyen where ID = d.DistrictId) as DistrictName,
                            (select Name from SocialThread where Id = d.Info_Flow) as InforFlowName
                            from
                            (select *, (select COUNT(*) from BillService where CustomerCode = b.CustomerCode) as times_come_salon
                            from BillService as b
                            where (ProductIds is not null and ProductIds != '') and (ServiceIds is not null and ServiceIds != '') " +
                            Condition_Time +
                            @" and ((select COUNT(*) from BillService where CustomerCode = b.CustomerCode) > 1)
                            and CustomerCode != ''
                            and IsDelete != 1
                            ) as c
                            inner join Customer as d
                            on c.CustomerCode = d.Customer_Code
                            where c.times_come_salon > 1
                            order by c.Id desc";

                    var LST2 = db.Customers.SqlQuery(sql).ToList();
                    Count = LST2.Count;
                }
                
                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }            
        }

        public void AddLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "addLoading();", true);
        }

        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Export Excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Btn_SwitchExcel(object sender, EventArgs e)
        {
            if (HDF_ExcelType.Value == "all")
            {
                Exc_ExportExcel(Get_LSTCustomer(false));
            }
            else if (HDF_ExcelType.Value == "notbuyproduct")
            {
                Exc_ExportExcel(Get_LSTCustomer_NotBuyProduct(false));
            }

        }

        protected void Exc_ExportExcel(List<Customer_Listing_Struct> _Customers)
        {
            using (var db = new Solution_30shineEntities())
            {
                var _ExcelHeadRow = new List<string>();
                var BillRowLST = new List<List<string>>();
                var BillRow = new List<string>();
                var Where = PredicateBuilder.True<Customer>();

                _ExcelHeadRow.Add("Ngày");
                _ExcelHeadRow.Add("Tên KH");
                _ExcelHeadRow.Add("Mã KH");
                _ExcelHeadRow.Add("Số ĐT");
                _ExcelHeadRow.Add("Tuổi");
                _ExcelHeadRow.Add("Tỉnh/Thành");
                _ExcelHeadRow.Add("Quận/Huyện");
                _ExcelHeadRow.Add("Địa chỉ");
                _ExcelHeadRow.Add("Nguồn thông tin");
                _ExcelHeadRow.Add("Số lần đến cửa hàng");
                _ExcelHeadRow.Add("Giá trị trọn đời");

                if (_Customers.Count > 0)
                {
                    foreach (var v in _Customers)
                    {
                        BillRow = new List<string>();
                        BillRow.Add(Convert.ToDateTime(v.CreatedDate).ToString("dd/MM/yyyy"));
                        BillRow.Add(v.Fullname);
                        BillRow.Add(v.Customer_Code);
                        BillRow.Add(v.Phone);
                        BillRow.Add(v.Age.ToString());
                        BillRow.Add(v.TenTinhThanh);
                        BillRow.Add(v.TenQuanHuyen);
                        BillRow.Add(v.Address);
                        BillRow.Add(v.InforFlowName);
                        BillRow.Add(v.Times.ToString());
                        BillRow.Add(v.LifeValue.ToString());
                        BillRowLST.Add(BillRow);
                    }
                }

                // export
                var ExcelStorePath = Server.MapPath("~") + "Public/Excel/Khach.Hang/";
                var FileName = "Khach_Hang_" + string.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
                var UrlFileName = "http://" + Request.Url.Host + "/Public/Excel/Khach.Hang/" + FileName;

                ExportXcel(_ExcelHeadRow, BillRowLST, ExcelStorePath + FileName);
                Bind_Paging();
                ScriptManager.RegisterStartupScript(BtnFakeExcel, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "');UP_FormatPrice('.be-report-price'); SetActiveTableListing();", true);
            }
        }

        private static void ExportXcel(List<string> _ExcelHeadRow, List<List<string>> List, string Path)
        {
            try
            {
                CreateExcelFile.ExcelHeadRow = _ExcelHeadRow;
                CreateExcelFile.CreateExcel(List, Path);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi. Không tạo được file excel.\r\nException: " + ex.Message);
            }
        }

        /// <summary>
        /// Check permission
        /// </summary>
        //private void CheckPermission()
        //{
        //    if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
        //    {
        //        var MsgParam = new List<KeyValuePair<string, string>>();
        //        UIHelpers.Redirect("/dang-nhap.html", MsgParam);
        //    }
        //    else
        //    {
        //        string[] AllowAccess = new string[] { "root", "admin", "salonmanager", "reception", "checkin", "checkout" };
        //        string[] AllowDelete = new string[] { "root", "admin", "salonmanager" };
        //        string[] AllowViewAllDate = new string[] { "root", "admin" };
        //        var Permission = Session["User_Permission"].ToString().Trim();
        //        if (Array.IndexOf(AllowAccess, Permission) != -1)
        //        {
        //            Perm_Access = true;
        //        }
        //        if (Array.IndexOf(AllowDelete, Permission) != -1)
        //        {
        //            Perm_Delete = true;
        //        }
        //        Perm_Edit = true;

        //        if (Array.IndexOf(AllowViewAllDate, Permission) != -1)
        //        {
        //            Perm_ViewAllData = true;
        //        }
        //    }

        //    // Call execute function
        //    ExecuteByPermission();
        //}

        //private void ExecuteByPermission()
        //{
        //    if (!Perm_Access)
        //    {
        //        ContentWrap.Visible = false;
        //        NotAllowAccess.Visible = true;
        //    }
        //}

        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }
    }
}