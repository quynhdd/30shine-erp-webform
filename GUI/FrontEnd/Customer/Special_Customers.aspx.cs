﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.CustomClass;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using Project.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace _30shine.GUI.FrontEnd.UICustomer
{
    public partial class Special_Customers : System.Web.UI.Page
    {
        private string PageID = "KH_DB";
        protected Paging PAGING = new Paging();
        public static CultureInfo culture = new CultureInfo("vi-VN");
        private DateTime timeFrom;
        private DateTime timeTo;
        protected int integer;

        protected bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected string Permission = "";
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_ShowSalon = permissionModel.GetActionByActionNameAndPageId("Perm_ShowSalon", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();

            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (Session["User_Permission"] != null)
            {
                Permission = Convert.ToString(Session["User_Permission"]);
            }
            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { ddlSalonFilter, ddlSalon }, Perm_ViewAllData);
                bindErrorReason();
                //bindData();

            }
            RemoveLoading();
        }



        [WebMethod]
        public static Customer GetCustomerListByPhone(string _filter)
        {
            using (var db = new Solution_30shineEntities())
            {
                var sql = @"select * from Customer where IsDelete != 1 and Phone = '" + _filter + "'";
                var list = db.Database.SqlQuery<Customer>(sql).FirstOrDefault();
                return list;
            }
        }

        [WebMethod]
        public static List<Tbl_Salon> GetSalon()
        {
            using (var db = new Solution_30shineEntities())
            {
                string Permission = HttpContext.Current.Session["User_Permission"].ToString();
                int Salon_ID = Convert.ToInt32(HttpContext.Current.Session["SalonId"].ToString());
                var sql = "";
                if (Permission == "admin")
                {
                    sql = @"select * from Tbl_Salon where IsDelete != 1 and Publish = 1 order by [Order]";
                }
                else if (Permission == "root")
                {
                    sql = @"select * from Tbl_Salon where IsDelete != 1 and Publish = 1 order by [Order]";
                }
                else
                {
                    sql = @"select * from Tbl_Salon where IsDelete != 1 and Publish = 1 and ((Id= " + Salon_ID + ") or (Id = 0))  order by [Order]";
                }


                var list = db.Database.SqlQuery<Tbl_Salon>(sql).ToList();


                return list;
            }
        }

        [WebMethod]
        public static List<CustomerType> GetCustomerType()
        {
            using (var db = new Solution_30shineEntities())
            {
                var sql = @"select * from CustomerType where IsDelete != 1 order by Id ASC";
                var list = db.Database.SqlQuery<CustomerType>(sql).ToList();
                return list;
            }
        }

        [WebMethod]
        public static object GetErrorCutReason()
        {
            using (var db = new Solution_30shineEntities())
            {
                //var sql = @"select * from ErrorCutReason where IsDelete != 1";
                var list = db.ErrorCutReasons.Where(a => a.IsDelete == false).OrderBy(a => a.OrderBy).Select(s => new { s.Id, s.Reason }).ToList();
                return list;
            }
        }

        [WebMethod]
        public static object GetStaffBySalon(int _SalonId)
        {
            using (var db = new Solution_30shineEntities())
            {
                if (_SalonId == 0)
                {
                    return db.Staffs.Where(w => w.IsDelete != 1 && w.Active == 1 && (w.isAccountLogin == null || w.isAccountLogin != 1) && (w.isAppLogin == null || w.isAppLogin != 1)).Select(s => new { s.Id, s.Fullname }).ToList();
                }
                else
                {
                    return db.Staffs.Where(w => w.SalonId == _SalonId && w.IsDelete != 1 && w.Active == 1 && (w.isAccountLogin == null || w.isAccountLogin != 1) && (w.isAppLogin == null || w.isAppLogin != 1)).Select(s => new { s.Id, s.Fullname }).ToList();
                }

            }
        }

        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            bindData();
            bindErrorReason();
            RemoveLoading();
            //Page.ClientScript.RegisterStartupScript(this.GetType(), "resetInsert", "resetInsert()", true);
            //ScriptManager.RegisterStartupScript(this.Page, Page.GetType(), "resetInsert", "resetInsert()", true);

        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        private void bindErrorReason()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _reason = db.ErrorCutReasons.Where(w => w.IsDelete != true).OrderBy(o => o.Id).ToList();
                var Key = 0;

                ddlReasonFilter.DataTextField = "ErrorReason";
                ddlReasonFilter.DataValueField = "Id";


                ListItem item = new ListItem("Chọn lý do", "0");
                ddlReasonFilter.Items.Insert(0, item);

                foreach (var v in _reason)
                {
                    Key++;
                    item = new ListItem(v.Reason, v.Id.ToString());
                    ddlReasonFilter.Items.Insert(Key, item);
                }
                ddlReasonFilter.SelectedIndex = 0;

            }
        }


        private void bindData()
        {
            using (var db = new Solution_30shineEntities())
            {
                DateTime? FromDate;
                DateTime? ToDate;
                if (TxtDateTimeFrom.Text != "")
                {
                    FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                }
                else
                {
                    FromDate = Convert.ToDateTime(new DateTime(2015, 1, 1), culture);
                }
                if (TxtDateTimeTo.Text != "")
                {
                    ToDate = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                }
                else
                {
                    ToDate = Convert.ToDateTime(DateTime.Today, culture);
                }
                var Salon_ID = Convert.ToInt32(ddlSalonFilter.SelectedValue);
                var PhoneCus = txtPhone.Text.Trim();
                var TypeError_ID = Convert.ToInt32(ddlReasonFilter.SelectedValue);
                var Browse_ID = Convert.ToInt32(ddlDuyet.SelectedValue);
                var data = db.Store_ERP_BindSpecialCus(FromDate, ToDate, Salon_ID, PhoneCus, TypeError_ID, Browse_ID).ToList();
                Bind_Paging(data.Count);
                RptCustomer.DataSource = data.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                RptCustomer.DataBind();
            }
        }

        [WebMethod]
        public static object InsertOrUpdateCustomer(int SpecialCusID, int Customer_ID, int TypeCusSpecail_ID, string Date_Cut_Error, int Salon_ID, int NumberFree, int DiscountCosmetics, int DiscountService, string ReasonDiff, string Permission, string ListStaffError, string ListTypeError)
        {
            using (var db = new Solution_30shineEntities())
            {
                int IdOld = 0;
                var Msg = new Library.Class.cls_message();
                var serialize = new JavaScriptSerializer();
                var ccSpecailCustomerOld = new SpecialCustomer();
                var ccSpecailCustomerNew = new SpecialCustomer();
                var objSpecailCusDetail = new SpecialCusDetail();
                if (Customer_ID > 0)
                {
                    ccSpecailCustomerOld = db.SpecialCustomers.FirstOrDefault(a => a.CustomerId == Customer_ID && a.IsDelete == false);
                    if (ccSpecailCustomerOld != null)
                    {
                        IdOld = ccSpecailCustomerOld.Id;
                        ccSpecailCustomerOld.IsDelete = true;
                        ccSpecailCustomerOld.Publish = false;
                        db.SaveChanges();
                    }
                    ccSpecailCustomerNew = new SpecialCustomer();
                    if ("ADMIN".Equals(Permission) || "root".Equals(Permission) || "ASM".Equals(Permission))
                    {
                        ccSpecailCustomerNew.Publish = true;
                        ccSpecailCustomerNew.Browse_ID = 2;
                    }
                    else
                    {
                        ccSpecailCustomerNew.Publish = false;
                        ccSpecailCustomerNew.Browse_ID = 1;
                    }
                    ccSpecailCustomerNew.SMSStatus = 0;
                    ccSpecailCustomerNew.CustomerId = Customer_ID;
                    ccSpecailCustomerNew.CustomerTypeId = TypeCusSpecail_ID;
                    ccSpecailCustomerNew.CreatedDate = DateTime.Now;
                    ccSpecailCustomerNew.IsSMS = true;
                    ccSpecailCustomerNew.IsDelete = false;
                    db.SpecialCustomers.Add(ccSpecailCustomerNew);
                    db.SaveChanges();
                    db.Database.ExecuteSqlCommand($"UPDATE dbo.StaffErrorSpecailCus SET IsDelete = 1, Publish = 0 WHERE SpecialCus_ID = {IdOld}");
                    var ListStaff_DB = db.StaffErrorSpecailCus.Where(a => a.Publish == true && a.IsDelete == false && a.SpecialCus_ID == ccSpecailCustomerNew.Id).ToList();
                    if ((ListStaffError.StartsWith("{") && ListStaffError.EndsWith("}")) || (ListStaffError.StartsWith("[") && ListStaffError.EndsWith("]")))
                    {
                        var ListStaff = serialize.Deserialize<List<cls_temp_Staff>>(ListStaffError).ToList();
                        //Insert and  Update List Error Staff
                        //1. Kiểm tra trong listStaff truyền lên, nếu có item mới => insert
                        var index = -1;
                        foreach (var v in ListStaff)
                        {
                            index = ListStaff_DB.FindIndex(w => w.Staff_ID == v.Id);
                            if (index == -1)
                            {
                                var TeamStaffErrorSpecailCus = new StaffErrorSpecailCu();
                                TeamStaffErrorSpecailCus.SpecialCus_ID = ccSpecailCustomerNew.Id;
                                TeamStaffErrorSpecailCus.Staff_ID = v.Id;
                                TeamStaffErrorSpecailCus.Createdate = DateTime.Now;
                                TeamStaffErrorSpecailCus.IsDelete = false;
                                TeamStaffErrorSpecailCus.Publish = true;
                                db.StaffErrorSpecailCus.Add(TeamStaffErrorSpecailCus);
                                db.SaveChanges();
                            }
                        }

                        //1. Kiểm tra trong listStaffDB, nếu item nào ko có trong list listStaff truyền lên => xóa
                        if (ListStaff_DB.Count > 0)
                        {
                            foreach (var v in ListStaff_DB)
                            {
                                index = ListStaff.FindIndex(w => w.Id == v.Staff_ID);
                                if (index == -1)
                                {
                                    v.IsDelete = true;
                                    v.Publish = false;
                                    db.SaveChanges();
                                }
                            }
                        }
                    }
                    else
                    {
                        if (ListStaff_DB.Count > 0)
                        {
                            foreach (var v in ListStaff_DB)
                            {
                                v.IsDelete = true;
                                v.Publish = false;
                                db.StaffErrorSpecailCus.AddOrUpdate(v);
                                db.SaveChanges();
                            }
                        }
                    }
                    //
                    //Insert and  Update List Error Staff
                    var ListReason = serialize.Deserialize<List<cls_temp_Staff>>(ListTypeError).ToList();
                    db.Database.ExecuteSqlCommand($"UPDATE dbo.TypeError_SpecailCus SET IsDelete = 1, Publish = 0 WHERE SpecailCus_ID = {IdOld}");
                    var ListReasonDB = db.TypeError_SpecailCus.Where(a => a.Publish == true && a.IsDelete == false && a.SpecailCus_ID == ccSpecailCustomerNew.Id).ToList();
                    var indexReason = -1;
                    foreach (var I in ListReason)
                    {
                        indexReason = ListReasonDB.FindIndex(w => w.TypeError_SpecialCus_ID == I.Id);
                        if (indexReason == -1)
                        {
                            var item_TypeError_SpecailCus = new TypeError_SpecailCus();
                            item_TypeError_SpecailCus.SpecailCus_ID = ccSpecailCustomerNew.Id;
                            item_TypeError_SpecailCus.TypeError_SpecialCus_ID = I.Id;
                            item_TypeError_SpecailCus.CreateDate = DateTime.Now;
                            item_TypeError_SpecailCus.IsDelete = false;
                            item_TypeError_SpecailCus.Publish = true;
                            db.TypeError_SpecailCus.Add(item_TypeError_SpecailCus);
                            db.SaveChanges();
                        }
                    }
                    //1. Kiểm tra trong listStaffDB, nếu item nào ko có trong list listStaff truyền lên => xóa
                    if (ListReasonDB.Count > 0)
                    {
                        foreach (var v in ListReasonDB)
                        {
                            indexReason = ListReason.FindIndex(w => w.Id == v.TypeError_SpecialCus_ID);
                            if (indexReason == -1)
                            {
                                v.IsDelete = true;
                                v.Publish = false;
                                v.ModifiedTime = DateTime.Now;
                                db.SaveChanges();
                            }
                        }
                    }
                    /// End  SpecialCustomers
                    DateTime _CutErrorDate = Convert.ToDateTime(Date_Cut_Error, culture);
                    objSpecailCusDetail = new SpecialCusDetail();
                    int speId = ccSpecailCustomerOld != null ? ccSpecailCustomerOld.Id : 0;
                    objSpecailCusDetail = db.SpecialCusDetails.FirstOrDefault(a => a.SpecialCusId == speId && a.IsDelete == false);
                    if (objSpecailCusDetail != null)
                    {
                        objSpecailCusDetail.IsDelete = true;
                        db.SaveChanges();
                    }
                    objSpecailCusDetail = new SpecialCusDetail();
                    objSpecailCusDetail.SpecialCusId = ccSpecailCustomerNew.Id;
                    objSpecailCusDetail.CutErrorDate = _CutErrorDate;
                    objSpecailCusDetail.SalonId = Salon_ID;
                    objSpecailCusDetail.QuantityInvited = NumberFree;
                    objSpecailCusDetail.QuantityFree = NumberFree;
                    objSpecailCusDetail.ReasonDiff = ReasonDiff;
                    objSpecailCusDetail.DiscountCosmetic = DiscountCosmetics;
                    objSpecailCusDetail.DiscountServices = DiscountService;
                    objSpecailCusDetail.IsDelete = false;
                    db.SpecialCusDetails.Add(objSpecailCusDetail);
                    db.SaveChanges();
                }
                return serialize.Serialize(Msg);
            }
        }

        [WebMethod]
        public static bool GetCustomerSpecial(int customerId)
        {
            using (var db = new Solution_30shineEntities())
            {
                bool check = false;
                var ccSpecailCustomer = new SpecialCustomer();
                ccSpecailCustomer = db.SpecialCustomers.FirstOrDefault(a => a.CustomerId == customerId);
                if (ccSpecailCustomer != null)
                {
                    check = true;
                }
                return check;
            }
        }

        [WebMethod]
        public static object BindDataUpdateSpecialCus(int Special_ID)
        {
            using (var db = new Solution_30shineEntities())
            {
                var data = new cls_BindData();
                var itemStaff = new cls_temp_Staff();
                var itemReason = new cls_temp_Reason();
                var obj = db.Bind_SpecialCus_Update(Special_ID).FirstOrDefault();
                if (obj != null)
                {
                    data.SpecialCustomer_ID = obj.Id;
                    data.Customer_ID = obj.CustomerId;
                    data.CustomerType_ID = obj.CustomerTypeId;
                    data.CusName = obj.Fullname;
                    data.DiscountCosmetic = obj.DiscountCosmetic;
                    data.DiscountService = obj.DiscountServices;
                    data.NgayCatLoi = obj.CutErrorDate;
                    data.Phone = obj.Phone;
                    data.QuantityInvited = obj.QuantityInvited;
                    data.Salon_ID = obj.SalonId;
                    data.TextNote = obj.ReasonDiff;
                    data.ListStaff = new List<cls_temp_Staff>();
                    data.ListReason = new List<cls_temp_Reason>();
                    var _ListStaff = (from a in db.StaffErrorSpecailCus
                                      join b in db.Staffs on a.Staff_ID equals b.Id
                                      where a.IsDelete == false && a.Publish == true && a.SpecialCus_ID == obj.Id
                                      select new
                                      {
                                          b.Id,
                                          b.Fullname
                                      }).ToList();
                    if (_ListStaff.Count > 0)
                    {
                        foreach (var v in _ListStaff)
                        {
                            itemStaff = new cls_temp_Staff();
                            itemStaff.Id = v.Id;
                            itemStaff.Fullname = v.Fullname;
                            data.ListStaff.Add(itemStaff);
                        }

                    }

                    var _ListReason = (from a in db.TypeError_SpecailCus
                                       join b in db.ErrorCutReasons on a.TypeError_SpecialCus_ID equals b.Id
                                       where a.Publish == true && b.IsDelete == false && a.SpecailCus_ID == obj.Id
                                       select new
                                       {
                                           b.Id,
                                           b.Reason
                                       }).ToList();
                    if (_ListReason.Count > 0)
                    {
                        foreach (var v in _ListReason)
                        {
                            itemReason = new cls_temp_Reason();
                            itemReason.Id = v.Id;
                            itemReason.Reason = v.Reason;
                            data.ListReason.Add(itemReason);
                        }
                    }
                }
                return data;
            }

        }

        [WebMethod]
        public static string DeleteCustomer(int _idspec)
        {
            var Message = new Library.Class.cls_message();
            var serialize = new JavaScriptSerializer();
            var exc = 0;
            var db = new Solution_30shineEntities();
            var record = db.SpecialCustomers.FirstOrDefault(w => w.Id == _idspec && w.IsDelete == false);
            if (record != null)
            {
                record.IsDelete = true;
                exc = db.SaveChanges();
                if (exc > 0)
                {
                    db.Database.ExecuteSqlCommand($"UPDATE dbo.StaffErrorSpecailCus SET IsDelete = 1, Publish = 0 WHERE SpecialCus_ID = {_idspec}");
                    db.Database.ExecuteSqlCommand($"UPDATE dbo.TypeError_SpecailCus SET IsDelete = 1, Publish = 0 WHERE SpecailCus_ID = {_idspec}");
                    db.Database.ExecuteSqlCommand($"UPDATE dbo.SpecialCusDetail SET IsDelete = 0 WHERE SpecialCusId = {_idspec}");
                    Message.success = true;
                    Message.message = "Xóa thành công.";
                }
                else
                {
                    Message.success = false;
                    Message.message = "Xóa thất bại.";
                }
            }
            return serialize.Serialize(Message);
        }

        [WebMethod]
        public static string DeleteDetail(int _id)
        {
            var Message = new Library.Class.cls_message();
            var serialize = new JavaScriptSerializer();
            var error = 0;
            var db = new Solution_30shineEntities();
            var RECORD = db.SpecialCusDetails.FirstOrDefault(w => w.Id == _id);
            if (RECORD != null)
            {
                RECORD.IsDelete = true;
                db.SpecialCusDetails.AddOrUpdate(RECORD);
                error += db.SaveChanges() > 0 ? 0 : 1;
                if (error == 0)
                {
                    Message.success = true;
                    Message.message = "Xóa thành công.";
                }
                else
                {
                    Message.success = false;
                    Message.message = "Xóa thất bại.";
                }
            }
            return serialize.Serialize(Message);
        }

        [WebMethod]
        public static SpecialCustomer_GetByID GetCustomerById(int _idCus)
        {
            using (var db = new Solution_30shineEntities())
            {
                var sql = @"select A.*, B.Fullname as CustomerName , B.Phone from SpecialCustomer A  left join Customer B on A.CustomerId = B.Id  where A.IsDelete = 0 and A.Id=" + _idCus;
                return db.Database.SqlQuery<SpecialCustomer_GetByID>(sql).SingleOrDefault();
            }
        }

        [WebMethod]
        public static List<SpecialCustomerDetail_GetByID> GetHistoryById(int _idspec, int id, int reasonId, string _timeFrom, string _timeTo)
        {
            using (var db = new Solution_30shineEntities())
            {
                var sql = @"select A.* , B.Name as SalonName, C.Fullname as StaffName, D.Reason from SpecialCusDetail A
                        left join Tbl_Salon B on B.Id = A.SalonId left join Staff C on C.Id = A.StaffId
						left join ErrorCutReason D on D.Id = A.Note
                        where A.IsDelete=0 and A.SpecialCusId = " + _idspec;
                DateTime dtFrom = new DateTime();
                DateTime dtTo = new DateTime();
                CultureInfo culture = new CultureInfo("vi-VN");
                if (_timeFrom != "")
                {
                    dtFrom = Convert.ToDateTime(_timeFrom, culture);
                    if (_timeTo != "")
                    {
                        dtTo = Convert.ToDateTime(_timeTo, culture);
                    }
                    else
                    {
                        dtTo = Convert.ToDateTime(_timeFrom, culture);
                    }
                    sql += @" and A.CutErrorDate between '" + String.Format("{0:yyyy/MM/dd}", dtFrom) + "' and  '" + String.Format("{0:yyyy/MM/dd}", dtTo.AddDays(1)) + "'";
                }
                if (reasonId != 0)
                {
                    sql += @" and A.Note =" + reasonId;
                }
                if (id != 0)
                {
                    sql += " and A.Id =" + id;
                }

                return db.Database.SqlQuery<SpecialCustomerDetail_GetByID>(sql).ToList();
            }
        }
        /// <summary>
        /// UpdateBrowse
        /// </summary>
        /// <param name="ID_SpeCus"></param>
        /// <param name="ID_Status"></param>
        /// <returns></returns>
        [WebMethod]
        public static object UpdateBrowse(int ID_SpeCus, int ID_Status)
        {
            using (var db = new Solution_30shineEntities())
            {
                var msg = new Msg();
                string Permission = HttpContext.Current.Session["User_Permission"].ToString();
                var obj = db.SpecialCustomers.FirstOrDefault(a => a.Id == ID_SpeCus);
                if (obj != null && Permission.IndexOf("ADMIN") < 0 || Permission.IndexOf("root") < 0 || Permission.IndexOf("ASM") < 0)
                {
                    if (ID_Status == 2)
                    {
                        obj.Publish = true;

                    }
                    else if (ID_Status == 3)
                    {
                        obj.Publish = false;

                    }
                    obj.Browse_ID = ID_Status;
                    db.SaveChanges();
                    msg.data = obj;
                    msg.msg = "success";
                }
                else
                {
                    msg.msg = "fail";
                }
                return msg;
            }

        }
    }
    public class SpecialCustomer_View : SpecialCustomer
    {
        public string CustomerName { get; set; }
        public string Phone { get; set; }
        public string TypeName { get; set; }
        public int? QuantityInvited { get; set; }
        public int? TotalQuantity { get; set; }
        public DateTime? ReecentlyDate { get; set; }

    }
    public class SpecialCustomer_Insert
    {
        public SpecialCustomer SpecialCustomer { get; set; }
        public SpecialCusDetail SpecialCusDetail { get; set; }
    }

    public class SpecialCustomer_GetByID : SpecialCustomer
    {
        public string CustomerName { get; set; }
        public string Phone { get; set; }
    }
    public class SpecialCustomerDetail_GetByID : SpecialCusDetail
    {
        public string SalonName { get; set; }
        public string StaffName { get; set; }
        public string Reason { get; set; }
    }

    public class cls_temp_Staff
    {
        public int Id { get; set; }
        public string Fullname { get; set; }

    }

    public class cls_temp_Reason
    {
        public int Id { get; set; }
        public string Reason { get; set; }
    }

    public class cls_BindData
    {
        public int? SpecialCustomer_ID { get; set; }
        public int? Customer_ID { get; set; }
        public string Phone { get; set; }
        public string CusName { get; set; }
        public int? CustomerType_ID { get; set; }
        public DateTime? NgayCatLoi { get; set; }
        public int? Salon_ID { get; set; }
        public int? QuantityInvited { get; set; }
        public double? DiscountCosmetic { get; set; }
        public double? DiscountService { get; set; }
        public string TextNote { get; set; }
        public List<cls_temp_Staff> ListStaff { get; set; }
        public List<cls_temp_Reason> ListReason { get; set; }
    }
}