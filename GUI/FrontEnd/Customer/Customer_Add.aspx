﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Customer_Add.aspx.cs" Inherits="_30shine.UICustomer.Customer_Add" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="CustomerAdd" ContentPlaceHolderID="CtMain" runat="server">
<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
    <link href="../../../Assets/libs/toastr/toastr.css" rel="stylesheet" />
    <script src="../../../Assets/libs/toastr/toastr.js"></script>
<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">
                <li>Khách hàng &nbsp;&#187; </li>
                <li class="li-listing"><a href="/khach-hang/danh-sach.html">Danh sách</a></li>
                <%--<li class="li-listing active"><a href="/khach-hang/chua-mua-my-pham.html">Khách chưa mua mỹ phẩm</a></li>
                <li class="li-listing"><a href="/khach-hang/chua-mua-dung-dich-vu.html">Khách chưa dùng dịch vụ</a></li>--%>
                <li class="li-add active"><a href="/khach-hang/them-moi.html">Thêm mới</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add">
    <%-- Add --%>                
    <div class="wp960 content-wp">
        <!-- System Message -->
        <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
        <!-- END System Message -->

        <div class="table-wp">
            <table class="table-add">
                <tbody>
                    <tr class="title-head">
                        <td><strong>Thông tin khách hàng</strong></td>
                        <td></td>
                    </tr>
                    <tr class="tr-margin" style="height: 20px;"></tr>
                    <tr>
                        <td class="col-xs-3 left"><span>Họ và tên</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="FullName" CssClass="form-control" runat="server" ClientIDMode="Static" onkeyup="fnCheckName($(this))"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="FullNameValidate" ControlToValidate="FullName" runat="server" CssClass="fb-cover-error " Text="Bạn chưa nhập họ tên!"></asp:RequiredFieldValidator>
                            </span>
                                            
                        </td>
                    </tr>

                    <tr>
                        <td class="col-xs-3 left"><span>Số điện thoại</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="Phone" CssClass="form-control" runat="server" ClientIDMode="Static"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="PhoneValidate" ControlToValidate="Phone" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập số điện thoại!"></asp:RequiredFieldValidator>
                            </span>
                        </td>
                    </tr>

                    <%--<tr style="display:none;">
                        <td class="col-xs-3 left"><span>Email</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="Email" runat="server" ClientIDMode="Static" placeholder="example@gmail.com"></asp:TextBox>
                            </span>
                        </td>
                    </tr>--%>

                    <tr class="tr-birthday" style="display:none;">
                        <td class="col-xs-3 left"><span>Sinh nhật</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="Day" CssClass="form-control" runat="server" ClientIDMode="Static" placeholder="Ngày"></asp:TextBox>
                                <asp:TextBox ID="Month" CssClass="form-control" runat="server" ClientIDMode="Static" placeholder="Tháng"></asp:TextBox>
                                <asp:TextBox ID="Year" CssClass =" form-control" runat="server" ClientIDMode="Static" placeholder="Năm"></asp:TextBox>
                            </span>
                        </td>
                    </tr>

                    <%--<tr class="tr-field-ahalf">
                        <td class="col-xs-3 left"><span>Tuổi</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="Age" runat="server" ClientIDMode="Static" placeholder="Ví dụ : 30"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="ValidateAge" ControlToValidate="Age" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập tuổi!"></asp:RequiredFieldValidator>
                            </span>
                        </td>
                    </tr>--%>

                    <tr>
                        <td class="col-xs-3 left"><span>Địa chỉ</span></td>
                        <td class="col-xs-9 right">
                            <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
                            <asp:UpdatePanel runat="server" ID="UP01">
                                <ContentTemplate>
                                    <div class="city">
                                        <asp:DropDownList AutoPostBack="true" ID="City" runat="server" CssClass="select form-control"
                                            OnSelectedIndexChanged="Reload_District" ClientIDMode="Static">
                                        </asp:DropDownList>                                        
                                    </div>
                                    <div class="district">
                                        <asp:DropDownList ID="District" runat="server" CssClass="select form-control" 
                                            ClientIDMode="Static">
                                        </asp:DropDownList>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <%--<asp:AsyncPostBackTrigger ControlID="OrderCloseBtn" EventName="Click"/>--%>
                                </Triggers>
                            </asp:UpdatePanel>
                            <asp:TextBox ID="Address" CssClass ="form-control" runat="server" placeholder="Địa chỉ" ClientIDMode="Static"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td class="col-xs-3 left"><span style="line-height: 18px; padding-top: 4px;">Nguồn thông tin <br />biết đến cửa hàng</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:DropDownList ID="SocialThread" CssClass="form-control select" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <asp:RequiredFieldValidator InitialValue="0" 
                                    ID="ValidateSocialThread" Display="Dynamic" 
                                    ControlToValidate="SocialThread"
                                    runat="server"  Text="Bạn chưa chọn nguồn thông tin!" 
                                    ErrorMessage="Vui lòng chọn nguồn thông tin!"
                                    ForeColor="Red"
                                    CssClass="fb-cover-error">
                                </asp:RequiredFieldValidator>
                            </span>
                        </td>
                    </tr>

                    <tr>
                        <td class="col-xs-3 left"><span>Mã khách hàng</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox CssClass="form-control" ID="CustomerCode" runat="server" ClientIDMode="Static" placeholder="Ví dụ : CL0001"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="ValidateCustomerCode" ControlToValidate="CustomerCode" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập mã Khách Hàng!"></asp:RequiredFieldValidator>--%>
                            </span>
                        </td>
                    </tr>

                    <tr class="tr-field-ahalf" runat="server" id="TrSalon" visible="false">
                        <td class="col-xs-3 left"><span>Salon</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:DropDownList ID="Salon" CssClass="form-control select" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <asp:RequiredFieldValidator InitialValue="0" 
                                    ID="ValidateSalon" Display="Dynamic" 
                                    ControlToValidate="Salon"
                                    runat="server"  Text="Bạn chưa chọn Salon!" 
                                    ErrorMessage="Vui lòng chọn Salon!"
                                    ForeColor="Red"
                                    CssClass="fb-cover-error">
                                </asp:RequiredFieldValidator>
                            </span>
                        </td>
                    </tr>
                    
                    <tr class="tr-send">
                        <td class="col-xs-3 left"></td>
                        <td class="col-xs-9 right no-border">
                            <span class="field-wp">
                                <asp:Button ID="Send" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="AddCustomer"></asp:Button>
                                <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>
                            </span>
                        </td>
                    </tr>
                    <asp:HiddenField runat="server" ID="UrlKeyField"  ClientIDMode="Static"/>
                    <asp:HiddenField runat="server" ID="ImgCoverField"  ClientIDMode="Static"/>
                </tbody>
            </table>
        </div>
    </div>
    <%-- end Add --%>
</div>
    <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
<script>
    jQuery(document).ready(function () {
        $("#glbCustomer").addClass("active");
    });


    function fnCheckName(THIS) {
        var sValue = $(THIS).val();
        var rgx = /[a-zàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđ\s]/i;
        //var rgx = /[a-zàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹđàáạảãâầấậẩẫăằắặẳẵèéẹẻẽêềếệểễìíịỉĩòóọỏõôồốộổỗơờớợởỡùúụủũưừứựửữỳýỵỷỹ\s]/i;
        for (var i = 0; i < sValue.length; i++) {
            if (rgx.test(sValue[i])) {
            }
            else {
                $(THIS).val(sValue.substr(0, sValue.length - 1));
                //displayMessageWarning("Chỉ cho phép nhập chữ và dấu cách.");
                alert("Chỉ cho phép nhập chữ và dấu cách.");
                return false;
            } 
        }
        return true;
            
    }
</script>

</asp:Panel>
</asp:Content>

