﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OleDb;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;


namespace _30shine.GUI.FrontEnd.UICustomer
{
    public partial class Finger : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;" + @"Data source= C:\Users\Public\att2000.mdb;";

            string queryString = "SELECT * FROM TEMPLATE";
            try
            {
                using (OleDbConnection connection = new OleDbConnection(connectionString))
                {
                    OleDbCommand command = new OleDbCommand(queryString, connection);
                    connection.Open();
                    OleDbDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        byte[] Tpl01 = ObjectToByteArray(reader[3]);
                        byte[] Tpl04 = ObjectToByteArray(reader[14]);
                        string text = reader[3].ToString();
                        var _Tpl01 = Convert.ToBase64String(Tpl01);
                        var _Tpl04 = Convert.ToBase64String(Tpl04);
                        string base64Decoded;
                        byte[] data = System.Convert.FromBase64String(_Tpl04);
                        base64Decoded = System.Text.ASCIIEncoding.ASCII.GetString(data);
                        Response.Write("Tpl01 : " + _Tpl01 + "<br/>");
                        Response.Write("Tpl04 : " + _Tpl04 + "<br/>");
                        //Response.Write("base64Decoded : " + base64Decoded + "<br/>");
                        Response.Write("------------------------------ <br/>");
                        // Insert code to process data.
                    }
                    reader.Close();
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show("Failed to connect to data source");
                throw new Exception(ex.Message);
            }
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        private byte[] ObjectToByteArray(Object obj)
        {
            if (obj == null)
                return null;
            BinaryFormatter bf = new BinaryFormatter();
            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, obj);
                return ms.ToArray();
            }
        }
    }
}
