﻿using _30shine.Helpers;
using _30shine.MODEL.CustomClass;
using _30shine.MODEL.ENTITY.EDMX;
using Project.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;


namespace _30shine.GUI.FrontEnd.UICustomer
{
    public partial class Customer_hairMode : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txtToDate.Value = DateTime.Now.ToString("dd/MM/yyyy");
        }
        [WebMethod]
        public static List<BillServiceView> GetBillImage(string _Date, int _SalonId, int customerClass, string telCustomer, int _stylistId)
        {
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                DateTime _FilterDate = Convert.ToDateTime(_Date, culture);
                string _FromDate = String.Format("{0:yyyy/MM/dd}", _FilterDate);
                string _ToDate = String.Format("{0:yyyy/MM/dd}", _FilterDate.AddDays(1));

                var sqlCount = @"select count(*) from BillService a where a.CreatedDate between '" + _FromDate + "' and '" + _ToDate + "' and a.IsDelete != 1 and a.Pending = 0 and a.Images is not null and a.SalonId = " + _SalonId;

                int _TotalBill = db.Database.SqlQuery<int>(sqlCount).First();
                
                object[] paras =
                { 
                    new SqlParameter("@timeFrom", _FilterDate),
                    new SqlParameter("@timeTo",  _FilterDate.AddDays(1)),
                    new SqlParameter("@salonId", _SalonId),
                    new SqlParameter("@stylistId", _stylistId),
                    new SqlParameter("@customerPhone", telCustomer ?? Convert.DBNull),
                    new SqlParameter("@customerType", customerClass)
                };
                
                var list = db.Database.SqlQuery<custom_BillService>("store_Customer_GetListHairModeFilter @timeFrom, @timeTo, @salonId, @stylistId, @customerPhone, @customerType", paras).ToList();
                var objSalon = db.Tbl_Salon.Find(_SalonId);

                List<BillServiceView> lstBillView = new List<BillServiceView>();
                BillServiceView obj = new BillServiceView();
                string[] ListImg;

                string _Domain = ConfigurationManager.AppSettings["DomainKey_PreviewImg"];
                foreach (var item in list)
                {
                    ListImg = item.Images.Split(',');
                    obj = new BillServiceView();
                    obj.Id = item.Id;
                    obj.BillDate = string.Format("{0:dd/MM/yyyy}", item.CreatedDate);
                    obj.CustomerPhone = item.CustomerPhone;
                    obj.CustomerName = item.CustomerName;
                    obj.CustomerId = item.CustomerId;

                    obj.Stylist = item.Stylist;

                    for (int i = 0; i < ListImg.Length; i++)
                    {
                        if (!(ListImg[i].StartsWith("http")))
                        {
                            ListImg[i] = _Domain + ListImg[i];
                        }
                    }
                    if (ListImg.Count() == 4)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                        obj.Img3 = ListImg[2];
                        obj.Img4 = ListImg[3];
                    }
                    if (ListImg.Count() == 3)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                        obj.Img3 = ListImg[2];
                    }
                    if (ListImg.Count() == 2)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                    }
                    if (ListImg.Count() == 1)
                        obj.Img1 = ListImg[0];

                    obj.SalonName = objSalon.Name;

                    lstBillView.Add(obj);
                }

                return lstBillView;
            }
        }

        [WebMethod]
        public static List<Tbl_Salon> ReturnAllSalon()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Tbl_Salon.Where(a => a.IsDelete == 0 && a.Publish == true).ToList();
                return lst;
            }
        }

        [WebMethod]
        public static List<Api_HairMode> ReturnAllHairMode()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Api_HairMode.Where(a => a.IsDelete == false && a.Publish == true).ToList();
                return lst;
            }
        }

        [WebMethod]
        public static string updateCustomer_HairMode_Bill(int cusHairId, int hairStyleId, int CustomerId, int billID)
        {
            var Message = new Library.Class.cls_message();
            var serialize = new JavaScriptSerializer();
            var error = 0;
            var db = new Solution_30shineEntities();
            var RECORD = db.Customer_HairMode_Bill.FirstOrDefault(w => w.Id == cusHairId);
            var obj = new Customer_HairMode_Bill();
            if (RECORD == null)
            {
                obj.BillId = billID;
                obj.CreateDate = DateTime.Now;
                obj.HairStyleId = hairStyleId;
                obj.CustomerId = CustomerId;
                db.Customer_HairMode_Bill.Add(obj);
                error += db.SaveChanges() > 0 ? 0 : 1;
                if (error == 0)
                {
                    Message.success = true;
                    Message.message = "Thêm thành công.";
                }
                else
                {
                    Message.success = false;
                    Message.message = "Thêm thất bại.";
                }
            }
            else
            {
                RECORD.BillId = billID;
                RECORD.HairStyleId = hairStyleId;
                RECORD.CustomerId = CustomerId;
                RECORD.ModifiledDate = DateTime.Now;
                db.Customer_HairMode_Bill.AddOrUpdate(RECORD);
                error += db.SaveChanges() > 0 ? 0 : 1;
                if (error == 0)
                {
                    Message.success = true;
                    Message.message = "Cập nhật thành công.";
                }
                else
                {
                    Message.success = false;
                    Message.message = "Cập nhật thất bại.";
                }
            }
            return serialize.Serialize(Message);
            //}
            //catch (Exception ex)
            //{
            //    Message.success = false;
            //    Message.message = ex.Message;
            //    return serialize.Serialize(Message);
            //}
        }

        [WebMethod]
        public static Customer_HairMode_Bill getHairStyleById(int billId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Customer_HairMode_Bill.Where(a => a.BillId == billId).SingleOrDefault();
                return lst;
            }
        }

        [WebMethod]
        public static List<Staff> ReturnAllStylistBySalon(int _SalonId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Staffs.Where(a => a.IsDelete == 0 && a.Active == 1 && a.Type == 1 && a.SalonId == _SalonId).ToList();
                return lst;
            }
        }

        public class custom_BillService
        {
            public string CustomerName { get; set; }
            public int Id { get; set; }
            public string CustomerCode { get; set; }
            public Nullable<int> Staff_Hairdresser_Id { get; set; }
            public string ProductIds { get; set; }
            public Nullable<int> TotalMoney { get; set; }
            public Nullable<System.DateTime> CreatedDate { get; set; }
            public Nullable<int> Staff_HairMassage_Id { get; set; }
            public Nullable<byte> IsDelete { get; set; }
            public string ServiceIds { get; set; }
            public string Images { get; set; }
            public Nullable<System.DateTime> ModifiedDate { get; set; }
            public Nullable<int> SalonId { get; set; }
            public Nullable<byte> Pending { get; set; }
            public string BillCode { get; set; }
            public Nullable<System.DateTime> CompleteBillTime { get; set; }
            public int CustomerId { get; set; }
            public Nullable<int> BookingId { get; set; }
            public string Stylist { get; set; }
            public string CustomerPhone { get; set; }
        }




    }
}
