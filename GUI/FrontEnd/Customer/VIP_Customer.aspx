﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="VIP_Customer.aspx.cs" Inherits="_30shine.GUI.FrontEnd.UICustomer.VIP_Customer" %>


<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <link href="../../../Assets/libs/toastr/toastr.css" rel="stylesheet" />
    <link href="/Assets/libs/magicsuggest-master/magicsuggest.css" rel="stylesheet" />
    <script src="/Assets/libs/magicsuggest-master/magicsuggest.js"></script>
    <script src="../../../Assets/libs/toastr/toastr.js"></script>
    <style type="text/css">
        .table-listing .uv-avatar { width: 120px; }

        .table-row-detail { display: inline-block; min-width: 500px; float: none !important; }

        .quantity { width: 70px !important; }
         
        .lbl { padding: 6px 12px; }
        .be-report input[type='text'], .be-report select { width: 145px; }
        .toast-message { color: #ffffff !important; }
    </style>
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Khách hàng VIP &nbsp;&#187; </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <div class="container">
                    <div class="row form-group">
                        <div class="col-xs-4">
                            <div class="row form-group">
                                <div class="col-xs-12" style="padding-top: 10px;">
                                    <strong class="st-head" style="font-family: Roboto Condensed Bold; font-weight: normal;"><i class="fa fa-plus-circle" style="margin-right: 10px;"></i>Thêm khách hàng VIP</strong>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-xs-4 lbl">Khách hàng</div>
                                <div class="col-xs-8" style="padding-right: 10px;">
                                    <input id="txtCustomer" class="form-control" type="text" placeholder="Nhập số điện thoại khách hàng" onchange="GetCustomerByPhone();" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-xs-4"></div>
                                <div class="col-xs-8" style="padding-right: 10px;">
                                    <input type="text" id="txtCustommerName" class="form-control" readonly />
                                    <input type="text" id="txtCustomerId" hidden />
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-xs-4 lbl">Loại khách</div>
                                <div class="col-xs-8" style="padding-right: 10px;">
                                    <%--<select id="ddlCustomerType" class="form-control" onchange="fnChange_ddlCustomerType();">
                                        <option value="0" selected="selected">Chọn loại khách</option>
                                        <option value="1">Khách đặc biệt</option>
                                        <option value="2">Khách VIP</option>
                                    </select>--%>
                                    <select name="ddlCustomerType" id="ddlCustomerType" class="form-control" disabled="disabled"></select>

                                </div>
                            </div>
                            <%--<div class="form-group row divCustomer">
                                <div class="col-xs-4 lbl">Ngày cắt lỗi</div>
                                <div class="col-xs-8">
                                    <input id="txtCreatedDate" class="txtDateTime st-head form-control" type="text" />
                                </div>
                            </div>

                            <div class="form-group row divCustomer">
                                <div class="col-xs-4 lbl">Salon</div>
                                <div class="col-xs-8">
                                    <select name="ddlSalon" id="ddlSalon" class="form-control" onchange="GetStafftBySalon()"></select>
                                </div>
                            </div>
                            <div class="form-group row divCustomer">
                                <div class="col-xs-4 lbl">Nhân viên</div>
                                <div class="col-xs-8">
                                    <select name="ddlStaff" id="ddlStaff" class="form-control">
                                        <option value="0">Chọn Staff</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row quantityInvited">
                                <div class="col-xs-4 lbl">Số lần mời dùng DV</div>
                                <div class="col-xs-8">
                                    <input id="txtQuantityInvited" class="form-control quantity" type="text" onkeypress="return isNumber(event)" />
                                </div>
                            </div>--%>
                            <div class="form-group row">
                                <div class="col-xs-4 lbl">Giảm giá mỹ phẩm (%)</div>
                                <div class="col-xs-3">
                                    <input id="txtDiscountCosmetic" class="form-control quantity" type="text" onkeypress="return isNumber(event)" value="10" />
                                </div>
                                <div class="col-xs-3 lbl">Giảm giá DV (%)</div>
                                <div class="col-xs-2" style="padding-left: 0;">
                                    <input id="txtDiscountService" class="form-control quantity" type="text" onkeypress="return isNumber(event)" value="100" />
                                </div>
                            </div>
                            <%--<div class="form-group row divCustomer">
                                <div class="col-xs-4 lbl">Lý do</div>
                                <div class="col-xs-8">
                                    <select name="txtNote" id="txtNote" class="form-control" onchange="ddlReason_Change();">
                                    </select>
                                    <textarea id="txtNote" class="form-control" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="form-group row hidden textNote">
                                <div class="col-xs-4 lbl"></div>
                                <div class="col-xs-8">
                                    <textarea id="txtTextNote" class="form-control" rows="3" ></textarea>
                                    <textarea id="txtNote" class="form-control" rows="3"></textarea>
                                </div>
                            </div>--%>
                            <div class="form-group row be-report">
                                <div class="col-xs-4"></div>
                                <div class="col-xs-8">
                                    <a href="javascript:void(0);" class="st-head btn-viewdata" onclick="InsertOrUpdateCustomer();">Lưu</a>
                                    <%--<a href="javascript:void(0);" onclick="location.href=location.href" class="st-head btn-viewdata">Làm mới</a>--%>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-8 be-report be-report-timekeeping">

                            <!-- Row Table Filter -->
                            <div class="table-func-panel">
                                <div class="filter-item">
                                    <strong class="st-head"><i class="fa fa-th-list"></i>Danh sách</strong>
                                </div>
                                <div class="table-func-elm">
                                    <span>Số hàng / Page : </span>
                                    <div class="table-func-input-wp">
                                        <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                                        <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                                        <ul class="ul-opt-segment">
                                            <li data-value="10">10</li>
                                            <li data-value="20">20</li>
                                            <li data-value="30">30</li>
                                            <li data-value="40">40</li>
                                            <li data-value="50">50</li>
                                            <li data-value="200">200</li>
                                            <%--<li data-value="1000000">Tất cả</li>--%>
                                        </ul>
                                        <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <!-- End Row Table Filter -->
                            <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                            <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                                <ContentTemplate>
                                    <div class="table-wp">
                                        <table class="table-add table-listing" id="tblSpecialCustomer">
                                            <thead>
                                                <tr>
                                                    <th>STT</th>
                                                    <th>Khách hàng</th>
                                                    <th>Điện thoại</th>
                                                    <th>Giảm giá DV</th>
                                                    <th>Giảm giá MP</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="RptCustomer" runat="server">
                                                    <ItemTemplate>
                                                        <tr class="parent" data-id="<%# Eval("Id") %>" data-custype="<%# Eval("CustomerTypeId") %>">
                                                            <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                            <td><%# Eval("CustomerName") %></td>
                                                            <td><%# Eval("Phone") %></td>
                                                            <td><%# Eval("DiscountServices") %> %</td>
                                                            <td class="map-edit">
                                                                <%# Eval("DiscountCosmetic") %> %
                                                                <div class="edit-wp">
                                                                    <%--<a href="javascript:void(0);" class="btn btn-xs btn-success" title="Thêm" onclick="EditSpecialCustomer($(this));" data-id="<%# Eval("Id") %>" data-custype="<%# Eval("CustomerTypeId") %>">Thêm</a>--%>
                                                                    <a class="btn btn-xs btn-danger" href="javascript://" title="Xóa" onclick="del($(this),'<%# Eval("Id") %>', '<%# Eval("CustomerName") %>')">Xóa</a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                    </div>

                                    <!-- Paging -->
                                    <div class="site-paging-wp">
                                        <% if (PAGING.TotalPage > 1)
                                            { %>
                                        <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                            <% if (PAGING._Paging.Prev != 0)
                                                { %>
                                            <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                            <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                            <% } %>
                                            <asp:Repeater ID="RptPaging" runat="server">
                                                <ItemTemplate>
                                                    <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                                        <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                                        <%# Eval("PageNum") %>
                                                    </a>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                                { %>
                                            <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                            <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                            <% } %>
                                        </asp:Panel>
                                        <% } %>
                                    </div>
                                    <!-- End Paging -->
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                            <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                        </div>
                    </div>


                </div>
            </div>
            <%-- END Listing --%>
        </div>

        <script type="text/javascript">
            $( document ).ready( function ()
            {
                // Add active menu
                $( "#glbCustomer" ).addClass( "active" );
                //============================
                // Datepicker
                //============================
                $( '.txtDateTime' ).datetimepicker( {
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function ( dp, $input ) { }
                } );
                //============================
                // Filter
                //============================
                $( ".filter-item input[type='text']" ).bind( "keydown", function ( e )
                {
                    if ( e.keyCode == 13 )
                    {
                        $( "#ViewDataFilter" ).click();
                    }
                } );
                GetCustomerType();
            } );
            var specialId = 0;
            var id = 0;

            //load ds loại khách hàng
            function GetCustomerType()
            {
                $.ajax( {
                    type: "POST",
                    url: "/GUI/FrontEnd/Customer/Special_Customers.aspx/GetCustomerType",
                    data: '',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function ( response )
                    {
                        var select = $( '#ddlCustomerType' );
                        select.append( '<option selected="selected" value="0">Chọn loại khách</option>' );
                        $.each( response.d, function ( key, value )
                        {
                            select.append( '<option value="' + value.Id + '">' + value.TypeName + '</option>' );
                        } );
                        $( '#ddlCustomerType option' ).removeAttr( 'selected' ).filter( '[value="2"]' ).attr( 'selected', 'selected' );
                        removeLoading();
                    },
                    failure: function ( response ) { console.log( response.d ); }
                } );
            }

            //get mã khách hàng theo điện thoại
            function GetCustomerByPhone()
            {
                var Phone = $( "#txtCustomer" ).val();
                if ( Phone == "" )
                {
                    displayMessageWarning( "Đã xảy ra lỗi khi tìm khách hàng! Vui lòng nhập lại số điện thoại." );
                    return false;
                }

                addLoading();
                $.ajax( {
                    type: "POST",
                    url: "/GUI/FrontEnd/Customer/VIP_Customer.aspx/GetCustomerByPhone",
                    data: '{Phone: "' + Phone + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function ( response )
                    {
                        console.log( response.d )
                        if ( response.d != null )
                        {
                            $( "#txtCustommerName" ).val( response.d.Fullname );
                            $( "#txtCustomerId" ).val( response.d.Id );
                            removeLoading();
                        }
                        else
                        {
                            displayMessageWarning( "Chưa tồn tại khách hàng!" );
                            $( "#txtCustomer" ).focus();
                            removeLoading();
                            return;
                        }

                    },
                    failure: function ( response ) { console.log( response.d ); }
                } );
            }

            // insert hoặc update thông tin khách hàng VIP
            function InsertOrUpdateCustomer()
            {
                var customerId = $( "#txtCustomerId" ).val();
                if ( customerId == "" )
                {
                    displayMessageWarning( "Đã xảy ra lỗi khi tìm khách hàng! Vui lòng nhập lại số điện thoại." );
                    $( "#txtCustommer" ).focus();
                    return false;
                }
                var customerType = $( "#ddlCustomerType" ).val();
                var discountServices = $( "#txtDiscountService" ).val();
                var discountCosmetic = $( "#txtDiscountCosmetic" ).val();
                addLoading();
                $.ajax( {
                    type: "POST",
                    url: "/GUI/FrontEnd/Customer/VIP_Customer.aspx/InsertOrUpdateCustomer",
                    data: "{CusId : " + customerId + ", TypeCusSpecail_ID : " + customerType + ", DiscountCosmetics : " + discountCosmetic + ", DiscountService : " + discountServices + " }",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function ( response )
                    {
                        if ( response.d.status == "Cusold" )
                        {
                            displayMessageSuccess( "Thêm khách mới thất bại !  Khách Vip đã tồn tại !" );
                        }
                        else
                        {
                            displayMessageSuccess( "Thêm mới khách VIP thành công" );
                            specialId = 0;
                            setTimeout( function ()
                            {
                                $( "#ViewData" ).click();
                                resetInsert();
                            }, 800 );
                        }
                        removeLoading();
                    },
                    failure: function ( response ) { console.log( response.d ); displayMessageError( "Đã xảy ra lỗi. Vui lòng nhập lại thông tin." ) }
                } );

            }

            //xóa khách hàng
            function del( This, code, name, e )
            {
                var code = code || null,
                    name = name || null,
                    Row = This;
                if ( !code ) return false;

                // show EBPopup
                $( ".confirm-yn" ).openEBPopup();
                $( "#EBPopup .confirm-yn-text" ).text( "Bạn có chắc chắn muốn xóa [ " + name + " ] ?" );

                $( "#EBPopup .yn-yes" ).bind( "click", function ()
                {
                    $.ajax( {
                        type: "POST",
                        url: "/GUI/FrontEnd/Customer/Special_Customers.aspx/DeleteCustomer",
                        data: '{_idspec : ' + code + '}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function ( response )
                        {
                            var mission = JSON.parse( response.d );
                            if ( mission.success )
                            {
                                delSuccess();
                                Row.remove();
                                setTimeout( function ()
                                {
                                    $( "#ViewData" ).click();
                                    resetInsert();
                                }, 1000 );

                            } else
                            {
                                delFailed();
                            }
                        },
                        failure: function ( response ) { alert( response.d ); }
                    } );
                } );
                $( "#EBPopup .yn-no" ).bind( "click", function ()
                {
                    autoCloseEBPopup( 0 );
                } );
                if ( !e ) var e = window.event;
                e.cancelBubble = true;
                if ( e.stopPropagation ) e.stopPropagation();
            }

            //làm mới textbox sau khi thêm hoặc update thông tin khách hàng
            function resetInsert()
            {
                specialId = 0; id = 0;
                $( "#txtCustomer" ).removeAttr( "disabled" );
                $( "#txtCustommerName" ).val( "" );
                $( "#txtCustomerId" ).val( "" );
                $( "#txtCustomer" ).val( "" );
                $( "#ddlCustomerType" ).val( 2 );
                $( "#txtDiscountService" ).val( "100" );
                $( "#txtDiscountCosmetic" ).val( "10" );
            }
        </script>

    </asp:Panel>
</asp:Content>
