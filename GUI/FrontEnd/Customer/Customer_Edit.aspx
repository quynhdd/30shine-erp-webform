﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Customer_Edit.aspx.cs" Inherits="_30shine.UICustomer.Customer_Edit" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="CustomerAdd" ContentPlaceHolderID="CtMain" runat="server">

    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <style>
            .imageapent {
                width: 15px;
                height: 15px;
            }

            .image12 {
                width: 30px;
                height: 30px;
            }

            .degree90 {
                -webkit-transform: rotate(90deg);
                -moz-transform: rotate(90deg);
                -o-transform: rotate(90deg);
                -ms-transform: rotate(90deg);
                transform: rotate(90deg);
                height: 118px !important;
                width: 118px !important;
            }

            .btn-filter {
                background: #000000;
                color: #ffffff;
                width: 80px !important;
                height: 35px;
                line-height: 30px;
                float: left;
                width: auto;
                text-align: center;
                cursor: pointer;
                -webkit-border-radius: 2px;
                -moz-border-radius: 2px;
                border-radius: 2px;
                margin: 0 5px;
            }

            .downImage .btn-filter:hover {
                color: #ffd800;
            }
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Khách hàng &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/khach-hang/danh-sach.html">Danh sách</a></li>
                        <%--<li class="li-listing active"><a href="/khach-hang/chua-mua-my-pham.html">Khách chưa mua mỹ phẩm</a></li>
                <li class="li-listing"><a href="/khach-hang/chua-mua-dung-dich-vu.html">Khách chưa dùng dịch vụ</a></li>--%>
                        <li class="li-add"><a href="/khach-hang/them-moi.html">Thêm mới</a></li>
                        <li class="li-edit"><a href="javascript://">Chi tiết</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing">
            <%-- Add --%>
            <div class="wp960 content-wp content-customer-detail">
                <!-- System Message -->
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <!-- END System Message -->
                <div class="table-wp">
                    <table class="table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Thông tin khách hàng</strong></td>
                                <td></td>
                            </tr>
                            <tr class="tr-margin " style="height: 20px;"></tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Khách hàng</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="FullName" runat="server" ClientIDMode="Static" CssClass="perm-input-field form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="FullNameValidate" ControlToValidate="FullName" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập họ tên!"></asp:RequiredFieldValidator>
                                    </span>

                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-2 left"><span>Số điện thoại</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="Phone" runat="server" ClientIDMode="Static" CssClass="perm-input-field form-control"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="PhoneValidate" ControlToValidate="Phone" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập số điện thoại!"></asp:RequiredFieldValidator>
                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-2 left"><span>Email</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="Email" runat="server" ClientIDMode="Static" placeholder="example@gmail.com" CssClass="perm-input-field form-control"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-birthday" style="display: none;">
                                <td class="col-xs-2 left"><span>Sinh nhật</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="Day" runat="server" ClientIDMode="Static" placeholder="Ngày" CssClass="perm-input-field form-control"></asp:TextBox>
                                        <asp:TextBox ID="Month" runat="server" ClientIDMode="Static" placeholder="Tháng" CssClass="perm-input-field form-control"></asp:TextBox>
                                        <asp:TextBox ID="Year" runat="server" ClientIDMode="Static" placeholder="Năm" CssClass="perm-input-field form-control"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>

                            <%--<tr class="tr-field-ahalf">
                        <td class="col-xs-2 left"><span>Tuổi</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="Age" runat="server" ClientIDMode="Static" placeholder="Ví dụ : 30" CssClass="perm-input-field"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="ValidateAge" ControlToValidate="Age" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập tuổi!"></asp:RequiredFieldValidator>
                            </span>
                        </td>
                    </tr>--%>

                            <%-- <tr>
                                <td class="col-xs-2 left"><span>Địa chỉ</span></td>
                                <td class="col-xs-9 right">
                                   <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
                                    <asp:UpdatePanel runat="server" ID="UP01">
                                        <ContentTemplate>
                                            <div class="city">
                                                <asp:DropDownList AutoPostBack="true" ID="City" runat="server" CssClass="select perm-ddl-field"
                                                    OnSelectedIndexChanged="Reload_District" ClientIDMode="Static">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="district">
                                                <asp:DropDownList ID="District" runat="server" CssClass="select perm-ddl-field"
                                                    ClientIDMode="Static">
                                                </asp:DropDownList>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>--%>
                            <%--<asp:AsyncPostBackTrigger ControlID="OrderCloseBtn" EventName="Click"/>--%>
                            <%--</Triggers>
                                    </asp:UpdatePanel>
                                    <asp:TextBox ID="Address" runat="server" placeholder="Địa chỉ" ClientIDMode="Static" CssClass="perm-input-field"></asp:TextBox>
                                </td>
                            </tr>--%>

                            <%--<tr>
                                <td class="col-xs-2 left"><span style="line-height: 18px; padding-top: 4px;">Nguồn thông tin
                                    <br />
                                    biết đến cửa hàng</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:DropDownList ID="SocialThread" runat="server" ClientIDMode="Static" CssClass="perm-ddl-field"></asp:DropDownList>
                                        <asp:RequiredFieldValidator InitialValue="0"
                                            ID="ValidateSocialThread" Display="Dynamic"
                                            ControlToValidate="SocialThread"
                                            runat="server" Text="Bạn chưa chọn nguồn thông tin!"
                                            ErrorMessage="Vui lòng chọn nguồn thông tin!"
                                            ForeColor="Red"
                                            CssClass="fb-cover-error">
                                        </asp:RequiredFieldValidator>
                                    </span>
                                </td>
                            </tr>--%>

                            <%--<tr class="hidden">
                                <td class="col-xs-2 left"><span>Mã khách hàng</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="CustomerCode" runat="server" ClientIDMode="Static" ReadOnly="true" placeholder="Ví dụ : ABCD123456" CssClass="perm-input-field"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="ValidateCustomerCode" ControlToValidate="CustomerCode" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập mã Khách Hàng!"></asp:RequiredFieldValidator>
                                    </span>
                                </td>
                            </tr>--%>

                            <tr class="tr-field-ahalf" runat="server" id="TrSalon" visible="false">
                                <td class="col-xs-2 left"><span>Salon đến lần đầu</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:DropDownList ID="Salon" runat="server" ClientIDMode="Static" CssClass="perm-ddl-field form-control select"></asp:DropDownList>
                                        <asp:RequiredFieldValidator InitialValue="0"
                                            ID="ValidateSalon" Display="Dynamic"
                                            ControlToValidate="Salon"
                                            runat="server" Text="Bạn chưa chọn Salon!"
                                            ErrorMessage="Vui lòng chọn Salon!"
                                            ForeColor="Red"
                                            CssClass="fb-cover-error">
                                        </asp:RequiredFieldValidator>
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-send">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <asp:Button ID="Send" CssClass="btn-send perm-btn-field" runat="server" Text="Cập nhật" ClientIDMode="Static" OnClick="Update_OBJ"></asp:Button>
                                        <span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;">Đóng</span>
                                    </span>
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
                        </tbody>
                    </table>
                </div>
            </div>
            <%-- end Add --%>

            <!-- Customer History -->
            <div class="wp960 content-wp content-customer-history">
                <div class="container">
                    <div class="row">
                        <% if (objReport.CustomerId > 0)
                            { %>
                        <!-- Common Information -->
                        <div class="col-xs-12 line-head">
                            <strong class="st-head" style="margin-top: 0px;">
                                <i class="fa fa-file-text"></i>
                                Thông tin khách hàng
                        <span class="view-more-infor">(Xem thông tin đầy đủ)</span>
                            </strong>
                        </div>
                        <div class="col-xs-12 common-infor">
                            <p>
                                Họ tên&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<%= objReport.CustomerName %>
                            </p>
                            <p>
                                Tuổi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<%= objReport.CustomerAge %>
                            </p>
                            <p>
                                Số ĐT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<%= objReport.CustomerPhone %>
                            </p>
                            <p>
                                Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<%= objReport.Email %>
                            </p>
                        </div>
                        <!--/ Common Information -->

                        <!-- Statistic Value -->
                        <% if (objReport.TotalReport != null)
                            { %>
                        <div class="col-xs-12 line-head">
                            <strong class="st-head" style="border-bottom: none; margin-bottom: 3px;">
                                <i class="fa fa-file-text"></i>Thống kê giá trị
                            </strong>
                        </div>
                        <div class="col-xs-12 customer-value-statistic">
                            <div class="table-wp">
                                <table class="table-add table-listing" style="width: 400px;">
                                    <tbody>
                                        <tr>
                                            <td>Tổng số lần sử dụng dịch vụ</td>
                                            <td class="be-report-price">
                                                <%= objReport.TotalReport.TongSoDV %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Tổng chi phí</td>
                                            <td class="be-report-price">
                                                <%= objReport.TotalReport.TongChiPhi %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Chi phí dịch vụ</td>
                                            <td class="be-report-price">
                                                <%= objReport.TotalReport.ChiPhiDV %>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Chi phí sản phẩm</td>
                                            <td class="be-report-price">
                                                <%= objReport.TotalReport.ChiPhiMP %>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="customer-detail-service">
                                <p class="_p">- Chi tiết</p>
                                <div class="tbl-wp">
                                    <div class="table-wp">
                                        <table class="table-add table-listing" style="width: 400px;">
                                            <tbody>
                                                <tr>
                                                    <td style="font-family: Roboto Condensed Bold;">Dịch vụ</td>
                                                    <td><%= objReport.TotalUsesService %></td>
                                                    <td class="be-report-price"><%= objReport.TotalMoneyService %></td>
                                                </tr>
                                                <% if (objReport.ServiceDetail.Count > 0)
                                                    { %>
                                                <% foreach (var v in objReport.ServiceDetail)
                                                    { %>

                                                <tr>
                                                    <td style="font-family: Roboto Condensed Bold;"><%= v.Name %></td>
                                                    <td><%= v.SolanSD %></td>
                                                    <td class="be-report-price"><%= v.ChiPhiDV %></td>
                                                </tr>

                                                <% } %>
                                                <% } %>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="table-wp">
                                        <table class="table-add table-listing" style="width: 400px; margin-top: 15px;">
                                            <tbody>
                                                <tr>
                                                    <td style="font-family: Roboto Condensed Bold;">Sản phẩm</td>
                                                    <td><%= objReport.TotalUsesProduct %></td>
                                                    <td class="be-report-price"><%= objReport.TotalMoneyProduct %></td>
                                                </tr>
                                                <% if (objReport.ProductDetail.Count > 0)
                                                    { %>
                                                <% foreach (var v in objReport.ProductDetail)
                                                    { %>
                                                <tr>
                                                    <td style="font-family: Roboto Condensed Bold;"><%= v.Name %></td>
                                                    <td><%= v.SolanSD %></td>
                                                    <td class="be-report-price"><%= v.ChiPhiDV %></td>
                                                </tr>
                                                <% } %>
                                                <% } %>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ Statistic Value -->
                        <% } %>

                        <!-- History -->
                        <% if (lstHis.Count > 0)
                            { %>
                        <div class="history-timeline">
                            <div class="line-h"></div>
                            <div class="col-xs-12 elm">
                                <div class="row">
                                    <div class="col-xs-2 left">
                                        <div class="wrap">
                                            <div class="time-value" style="font-family: Roboto Condensed Bold; font-size: 15px;">Lịch sử</div>
                                        </div>
                                    </div>
                                    <div class="col-xs-10 right"></div>
                                </div>
                            </div>
                            <% foreach (var v in lstHis)
                                {%>
                            <div class="col-xs-9 elm">
                                <div class="row">
                                    <div class="col-xs-2 left">
                                        <div class="wrap">
                                            <div class="time-value"><%= String.Format("{0:dd/MM/yyyy}", v.CreatedDate)  %></div>
                                            <div class="line-v"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-10 right">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <table class="table-add table-listing">
                                                    <tbody>
                                                        <tr class="tr-service-color">
                                                            <td>Thông tin chung</td>
                                                            <td colspan="2">
                                                                <i style="width: 100%; float: left;">- Stylist : <%= v.StylistName %></i>
                                                                <i style="width: 100%; float: left;">- Skinner : <%= v.SkinnerName %></i>
                                                                <i style="width: 100%; float: left;">- Checkin : <%= v.CheckinName %></i>
                                                                <i style="width: 100%; float: left;">- Tgian checkin : <%= String.Format("{0:HH:mm:ss dd/MM/yyyy}", v.CreatedDate)  %></i>
                                                                <% if (v.IsBookOnline == true)
                                                                    {%>
                                                                <i style="width: 100%; float: left;">- Ngày book : <%= String.Format("{0:dd/MM/yyyy}", v.DatedBook) %></i>
                                                                <i style="width: 100%; float: left;">- Tgian book: <%= v.HourFrame %></i>
                                                                <%} %>
                                                                <%else
                                                                {%> 
                                                                <i style="width: 100%; float: left;">- Ngày book : Khách book tại salon</i>
                                                                <i style="width: 100%; float: left;">- Tgian book: Khách book tại salon</i>
                                                                <%}%>
                                                                <i style="width: 100%; float: left;">- Tgian chờ : <%= v.WaitTimeInProcedure %></i>
                                                                <i style="width: 100%; float: left;">- Salon : <%= v.SalonName %></i>
                                                                <i style="width: 100%; float: left;">- Đánh giá : <%= v.DanhGia %></i>
                                                            </td>
                                                        </tr>
                                                        <% if (v.ServiceIds != null && v.ServiceIds.Count > 0)
                                                            {
                                                                foreach (var v1 in v.ServiceIds)
                                                                { %>
                                                        <tr class="tr-service-color">
                                                            <% if (v1.Id == v.ServiceIds[0].Id)
                                                                {%>
                                                            <td style="min-width: 90px;">Dịch vụ</td>
                                                            <% }
                                                                else
                                                                { %>
                                                            <td style="min-width: 90px;"></td>
                                                            <% } %>
                                                            <td><%= v1.Name %></td>
                                                            <td class="be-report-price"><%= v1.Price %></td>
                                                        </tr>
                                                        <% } %>
                                                        <% } %>

                                                        <% if (v.ProductIds != null && v.ProductIds.Count > 0)
                                                            {
                                                                foreach (var v2 in v.ProductIds)
                                                                { %>
                                                        <tr class="tr-service-color">
                                                            <% if (v2.Id == v.ProductIds[0].Id)
                                                                {%>
                                                            <td style="min-width: 90px;">Sản phẩm</td>
                                                            <% }
                                                                else
                                                                { %>
                                                            <td style="min-width: 90px;"></td>
                                                            <% } %>
                                                            <td><%= v2.Name %></td>
                                                            <td class="be-report-price"><%= v2.Price %></td>
                                                        </tr>
                                                        <% } %>
                                                        <% } %>

                                                        <tr class="tr-total-color">
                                                            <td>Tổng chi phí</td>
                                                            <td></td>
                                                            <td class="be-report-price"><%= v.TotalMoney %></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="wrap listing-img-upload">
                                                    <% if (v.Images != null && v.Images.Length > 0)
                                                        {
                                                            foreach (var v3 in v.Images)
                                                            { %>
                                                    <div class="thumb-wp">
                                                        <img class="thumb" alt="" title="" src="<%= v3 %>" style="width: 120px; height: 160px; left: 0px; top: -20px; z-index: 1;">
                                                        <div class="thumb-cover"></div>
                                                        <div style="z-index: 2; position: absolute; display: none;" class="downImage">
                                                            <a href="<%= v3 %>" class="btn-send perm-btn-field btn-filter" download style="margin-top: 40px; margin-left: 20px;">Tải ảnh</a>
                                                        </div>
                                                    </div>
                                                    <% } %>
                                                    <% } %>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <% } %>
                        </div>
                        <% } %>
                        <!--/ History -->
                        <% } %>
                    </div>
                </div>
            </div>

            <%-- <% if (OBJ != null)
                { %>
            <div class="wp960 content-wp content-customer-history">
                <div class="container">
                    <div class="row">
                        <!-- Common Information -->
                        <div class="col-xs-12 line-head">
                            <strong class="st-head" style="margin-top: 0px;">
                                <i class="fa fa-file-text"></i>
                                Thông tin khách hàng
                        <span class="view-more-infor">(Xem thông tin đầy đủ)</span>
                            </strong>
                        </div>
                        <div class="col-xs-12 common-infor">
                            <p>
                                Họ tên&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<%=OBJ.Fullname %>
                            </p>
                            <p>
                                Tuổi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<%=OBJ.Age %>
                            </p>
                            <p>
                                Số ĐT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<%=OBJ.Phone %>
                            </p>
                            <p>
                                Mã KH&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<%=OBJ.Customer_Code %>
                            </p>
                            <p>
                                Địa chỉ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<%=OBJ.CityName %>, <%=OBJ.DistrictName %>
                            </p>
                            <p>
                                Kiểu tóc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp<%=OBJ.Title %>
                            </p>


                        </div>
                        <!--/ Common Information -->

                        <!-- Statistic Value -->
                        <div class="col-xs-12 line-head">
                            <strong class="st-head" style="border-bottom: none; margin-bottom: 3px;">
                                <i class="fa fa-file-text"></i>Thống kê giá trị
                            </strong>
                        </div>
                        <div class="col-xs-12 customer-value-statistic">
                            <div class="table-wp">
                                <table class="table-add table-listing" style="width: 400px;">
                                    <tbody>
                                        <tr>
                                            <td>Tổng số lần sử dụng dịch vụ</td>
                                            <td class="be-report-price"><%=Ctm_Statistic.TimesUsed_Service %></td>
                                        </tr>
                                        <tr>
                                            <td>Tổng chi phí</td>
                                            <td class="be-report-price"><%=Ctm_Statistic.TotalMoney %></td>
                                        </tr>
                                        <tr>
                                            <td>Chi phí dịch vụ</td>
                                            <td class="be-report-price"><%=Ctm_Statistic.TotalMoney_Service %></td>
                                        </tr>
                                        <tr>
                                            <td>Chi phí sản phẩm</td>
                                            <td class="be-report-price"><%=Ctm_Statistic.TotalMoney_Product %></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="customer-detail-service">
                                <p class="_p">- Chi tiết</p>
                                <div class="tbl-wp">
                                    <div class="table-wp">
                                        <table class="table-add table-listing" style="width: 400px;">
                                            <tbody>
                                                <tr>
                                                    <td style="font-family: Roboto Condensed Bold;">Dịch vụ</td>
                                                    <td><%=Ctm_Statistic.TimesUsed_Service %></td>
                                                    <td class="be-report-price"><%=Ctm_Statistic.TotalMoney_Service %></td>
                                                </tr>
                                                <%=Ctm_Statistic.Service_Statistic %>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="table-wp">
                                        <table class="table-add table-listing" style="width: 400px; margin-top: 15px;">
                                            <tbody>
                                                <tr>
                                                    <td style="font-family: Roboto Condensed Bold;">Sản phẩm</td>
                                                    <td><%=Ctm_Statistic.TimesUsed_Product %></td>
                                                    <td class="be-report-price"><%=Ctm_Statistic.TotalMoney_Product %></td>
                                                </tr>
                                                <%=Ctm_Statistic.Product_Statistic %>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--/ Statistic Value -->

                        <!-- History -->
                        <div class="history-timeline">
                            <div class="line-h"></div>
                            <div class="col-xs-12 elm">
                                <div class="row">
                                    <div class="col-xs-2 left">
                                        <div class="wrap">
                                            <div class="time-value" style="font-family: Roboto Condensed Bold; font-size: 15px;">Lịch sử</div>
                                        </div>
                                    </div>
                                    <div class="col-xs-10 right"></div>
                                </div>
                            </div>

                            <% foreach (var v in Ctm_History)
                                { %>
                            <!-- elm -->
                            <div class="col-xs-9 elm">
                                <div class="row">
                                    <div class="col-xs-2 left">
                                        <div class="wrap">
                                            <div class="time-value"><%=v.Date %></div>
                                            <div class="line-v"></div>
                                        </div>
                                    </div>
                                    <div class="col-xs-10 right">
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <%=v.Table %>
                                                <div class="wrap listing-img-upload">
                                                    <% if (v.Images != null && v.Images.Length > 0)
                                                        { %>
                                                    <% foreach (var v2 in v.Images)
                                                        { %>
                                                    <div class="thumb-wp" onclick="Plugin_Images_Chose(event, $(this))">
                                                        <img class="thumb" alt="" title="" src="<%=v2 %>" />
                                                        <span class="delete-thumb" onclick="deleteThum($(this), '<%=v2 %>', false)"></span>
                                                        <div class="thumb-cover"></div>
                                                    </div>
                                                    <% } %>
                                                    <%} %>
                                                </div>
                                            </div>
                                            <div class="col-xs-3 text-left addHairStyle">
                                                <a href="javascript:void(0);" class="btn btn-default" id="clickone" onclick="addTrToTable($(this),<%=v.Id%>)" title="Sửa">
                                                    <img class="image12" src="/Assets/images/hairmode/hairmode4.png" />                                             
                                                </a>
                                                <div class="form-group list-hairmode currentClass"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/ elm -->
                            <% } %>
                        </div>
                        <!--/ History -->

                    </div>
                </div>
            </div>
            <% } %>--%>
            <!--/ Customer History -->
            <asp:HiddenField runat="server" ID="HDF_CustomerId" ClientIDMode="Static" />
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script>
           
            
            var customerID = <%=Code%>;
            //console.log(customerID);
            jQuery(document).ready(function () {
                $(".thumb-wp").hover(
               function () {
                   $(this).find(".downImage").css("display", "block");
               }, function () {
                   $(this).find(".downImage").css("display", "none");
               }
           );
                $("#glbCustomer").addClass("active");
                $(".li-edit").addClass("active");

                // Price format
                UP_FormatPrice('.be-report-price');

                // Show form customer information
                $(".view-more-infor").bind("click", function () {
                    $(".content-customer-detail").show();
                });
                $("#CalcelAddFbcv").bind("click", function () {
                    $(".content-customer-detail").hide();
                });

                // fix thumbnail
                excThumbWidth();

                //============================
                // Show System Message
                //============================ 
                var qs = getQueryStrings();
                showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);

                if (qs["show_form_infor"] == "true") {
                    $(".content-customer-detail").show();
                }

                $(window).load(function () {
                    $('.listing-img-upload .thumb-wp').each(function () {
                        var THIS = $(this);

                        $("<img>").attr("src", $(".thumb").attr("src")).load(function () {
                            var ImgW = this.width;
                            var ImgH = this.height;
                            //console.log(ImgW);
                            if ((ImgW > ImgH)) {
                                (THIS).find('img').addClass('degree90');
                            }
                        });
                    });
                });
            });

            function excThumbWidth() {
                var ImgStandardWidth = 120,
                    ImgStandardHeight = 120;
                var width = ImgStandardWidth,
                    height, left, top;
                $(".thumb-wp .thumb").each(function () {
                    height = ImgStandardWidth * $(this).height() / $(this).width();
                    left = 0;
                    top = (ImgStandardHeight - height) / 2;
                    $(this).css({ "width": width, "height": height, "left": left, "top": top });
                });
            }

            function showDom(selector, status) {
                status = typeof status == "boolean" ? status : false;
                console.log(status);
                if (status) {
                    $(selector).show();
                }
            }
        </script>
        <script>                
            function addTrToTable(This,id) {
                if(This.parent().find(".list-hairmode").children().html() != undefined){
                    $(".list-hairmode").empty();
                }
                else{
                    //$(".list-hairmode").empty();
                    var billId = id;
             
                    $.ajax({
                        type: "POST",
                        url: "/GUI/FrontEnd/Customer/Customer_Edit.aspx/HairStyleList",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                       
                            var thisHairMode = This.parent().find(".form-group");                        
                        
                            if (response.d.length > 0) {
                                var trs = "<table class=\"table table-bordered tblDetail\">" +
                                                "<thead>" +
                                                    "<tr>" +
                                                        "<th>Stt</th>" +
                                                        "<th>Kiểu tóc</th>" +
                                                    "</tr>" +
                                                "</thead>" +
                                                "<tbody>";
                                var tongOrder = 0;
                                $.each(response.d, function (i, v) {
                                    trs +=
                                        '<tr>' +
                                            '<td class="STT">' + (i + 1) + '</td>' +
                                            '<td class="Id"><a href="javascript:void(0);" onclick="addHairStyleCustomer($(this),'+billId+')" data-id='+v.Id+'> ' + v.Title + '</a></td>' +                                            
                                            '</tr>';
                                });
                                trs += "<tbody></table>";
                                $(".list-hairmode").empty();
                                thisHairMode.append(trs);
                            
                           
                            }
                        },
                        error: function (error) {
                            console.log(error);
                        }
                    });
                }
            }

            function addHairStyleCustomer(This, billId)
            {
                
                customerID;
                var BillId = billId;
                //console.log($(This).attr("data-id"));
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/Customer/Customer_Edit.aspx/SaveHistory",
                    data: '{HairStyleId : ' + $(This).attr("data-id") + ', CustomerID : '+customerID+',BillId:'+ BillId +'}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        console.log(response);
                        finishLoading();
                        window.location.reload();
                    },
                    failure: function (response) { alert(response.d); }
                });
            }
        </script>

    </asp:Panel>
</asp:Content>

