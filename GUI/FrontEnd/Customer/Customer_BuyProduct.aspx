﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Customer_BuyProduct.aspx.cs" Inherits="_30shine.GUI.UICustomer.Customer_BuyProduct" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">
                <li>Khách hàng &nbsp;&#187; </li>
                <li class="li-listing active"><a href="/khach-hang/danh-sach.html">Danh sách</a></li>
                <li class="li-listing"><a href="/khach-hang/chua-mua-my-pham.html">Khách chưa mua mỹ phẩm</a></li>
                <li class="li-listing"><a href="/khach-hang/chua-mua-dung-dich-vu.html">Khách chưa dùng dịch vụ</a></li>
                <li class="li-add"><a href="/khach-hang/them-moi.html">Thêm mới</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add customer-listing be-report">
    <%-- Listing --%>                
    <div class="wp960 content-wp">
        <!-- Filter -->
        <div class="row">
            <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
            <div class="filter-item">
                <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                   ClientIDMode="Static" runat="server"></asp:TextBox>
            </div>
            <strong class="st-head" style="margin-left: 10px;">
                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
            </strong>
            <div class="filter-item">
                <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeTo" placeholder="Đến ngày"
                    ClientIDMode="Static" runat="server"></asp:TextBox>
            </div>
        </div>        
        <div class="row row-filter">
            <strong class="st-head"><i class="fa fa-filter"></i>Lọc kết quả</strong>
            <div class="filter-item">
                <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion form-control" data-field="customer.code" data-value="0"
                   AutoCompleteType="Disabled" ID="CustomerCode" ClientIDMode="Static" placeholder="Mã Khách hàng" runat="server"></asp:TextBox>
                <div class="listing-staff-wp eb-select-data ">
                    <ul class="ul-listing-staff ul-listing-suggestion" id="UlCustomerCode"></ul>
                </div>
            </div>
            <div class="filter-item">
                <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion form-control" data-field="customer.name" data-value="0"
                     AutoCompleteType="Disabled" ID="CustomerName" ClientIDMode="Static" placeholder="Nhập tên Khách hàng" runat="server"></asp:TextBox>
                <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                    <ul class="ul-listing-staff ul-listing-suggestion" id="UlCustomerName">
                    </ul>
                </div>
            </div>
            
            <div class="filter-item">
                <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion form-control" data-value="0" data-field="customer.phone"
                     AutoCompleteType="Disabled" ID="CustomerPhone" ClientIDMode="Static" placeholder="Số điện thoại" runat="server"></asp:TextBox>
                <div class="listing-staff-wp eb-select-data">
                    <ul class="ul-listing-staff ul-listing-suggestion" id="UlCustomerPhone"></ul>
                </div>
            </div> 

            <div class="filter-item">
                <asp:DropDownList ID="Salon" CssClass ="form-control select" runat="server" ClientIDMode="Static" style="margin: 12px 0; width: 190px;"></asp:DropDownList>
            </div>   

            <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" ClientIDMode="Static" 
                onclick="excPaging(1)" runat="server">Xem dữ liệu</asp:Panel>
            <a href="/khach-hang/danh-sach.html" class="st-head btn-viewdata">Reset Filter</a>

            <div class="export-wp drop-down-list-wp">
                <asp:Panel ID="fff" CssClass="st-head btn-viewdata"
                    ClientIDMode="Static" runat="server">Xuất File Excel</asp:Panel><%--OnClick="Exc_ExportExcel" --%>
                <ul class="ul-drop-down-list" style="display:none;">
                    <li onclick="EntryExportExcel('all')">Tất cả dữ liệu</li>
                    <li onclick="EntryExportExcel('notbuyproduct')">Khách chưa mua mỹ phẩm</li>
                </ul>                
            </div>  
        </div>
        <!-- End Filter -->
        <!-- Row Table Filter -->
        <div class="table-func-panel">
            <div class="table-func-elm">
                <span>Số hàng / Page : </span>
                <div class="table-func-input-wp">
                    <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                    <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                    <ul class="ul-opt-segment">
                        <li data-value="10">10</li>
                        <li data-value="20">20</li>
                        <li data-value="30">30</li>
                        <li data-value="40">40</li>
                        <li data-value="50">50</li>
                        <li data-value="200">200</li>
                        <%--<li data-value="1000000">Tất cả</li>--%>
                    </ul>
                    <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                </div>                
            </div>            
        </div>
        <!-- End Row Table Filter -->
        <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
        <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
            <ContentTemplate>
                
                <div class="table-wp">
                    <table class="table-add table-listing">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Thời gian</th>
                                <th>Mã Khách Hàng</th>
                                <th>Họ và tên</th>                                
                                <%--<th>Số điện thoại</th>--%>
                                <th>Số lần đến cửa hàng</th>
                                <th>Giá trị trọn đời</th>
                                <%--<th>Email</th>--%>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="RptCustomer" runat="server" OnItemDataBound="Repeater_ItemDataBound">
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                        <td><asp:Panel ID="Panel1" CssClass="abc" runat="server"></asp:Panel>
                                            <a href="/khach-hang/<%# Eval("Customer_Code") %>.html">
                                                <%# string.Format("{0:dd/MM/yyyy}",Eval("CreatedDate")) %>
                                            </a>
                                        </td>                                        
                                        <td><%# Eval("Customer_Code") %></td>
                                        <td class="map-edit">
                                            <a href="/khach-hang/<%# Eval("Customer_Code") %>.html"><%# Eval("Fullname") %></a>
                                        </td>
                                        <%--<td><%# Eval("Phone") %></td>--%>
                                        <td><%# Eval("Times") %></td>
                                        <td class="map-edit">
                                            <%# Eval("LifeValue") %>
                                            <div class="edit-wp">
                                                <asp:Panel CssClass="edit-action-wp action-edit" runat="server" ID="EAedit" Visible="false">
                                                    <a class="elm edit-btn" href="/khach-hang/<%# Eval("Customer_Code") %>.html" title="Sửa"></a>
                                                </asp:Panel>
                                                <asp:Panel CssClass="edit-action-wp action-delete" runat="server" ID="EAdelete" Visible="false">
                                                    <a class="elm del-btn" onclick="del(this.parentNode.parentNode.parentNode.parentNode,'<%# Eval("Customer_Code") %>', '<%# Eval("Fullname") %>')" href="javascript://" title="Xóa"></a>
                                                </asp:Panel>
                                                
                                            </div>
                                        </td>
                                        <%--<td class="map-edit">
                                            <%# Eval("Email") %>
                                            <div class="edit-wp">
                                                <a class="elm edit-btn" href="/khach-hang/<%# Eval("Customer_Code") %>.html" title="Sửa"></a>
                                                <a class="elm del-btn" onclick="del(this.parentNode.parentNode.parentNode,'<%# Eval("Customer_Code") %>', '<%# Eval("Fullname") %>')" href="javascript://" title="Xóa"></a>
                                            </div>
                                        </td>--%>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>                    
                        </tbody>
                    </table>
                </div>

                <!-- Paging -->
                <div class="site-paging-wp">
                <% if(PAGING.TotalPage > 1){ %>
                    <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                        <% if (PAGING._Paging.Prev != 0)
                           { %>
                        <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                        <% } %>
                        <asp:Repeater ID="RptPaging" runat="server">
                            <ItemTemplate>
                                <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                    <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>
                                >
                                    <%# Eval("PageNum") %>
                                </a>
                            </ItemTemplate>                    
                        </asp:Repeater>
                        <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                           { %>                
                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                        <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                        <% } %>
                    </asp:Panel>
                <% } %>
                </div>
		        <!-- End Paging -->                
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" style="display:none;" />
        <asp:Button ID="BtnFakeExcel" ClientIDMode="Static" runat="server" OnClick="Btn_SwitchExcel" style="display:none;" />
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_ExcelType" />
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
        <!-- Hidden Field-->
        <asp:HiddenField ID="HDF_Suggestion_Code" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="HDF_Suggestion_Field" ClientIDMode="Static" runat="server" />
        <!-- END Hidden Field-->
    </div>   
    <%-- END Listing --%>
</div>
    <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
<script>
    jQuery(document).ready(function () {
        // Add active menu
        $("#glbCustomer").addClass("active");

        //============================
        // Datepicker
        //============================
        $('.txtDateTime').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'vi',
            startDate: '2014/10/10',
            format: 'd/m/Y',
            dateonly: true,
            showHour: false,
            showMinute: false,
            timepicker: false,
            onChangeDateTime: function (dp, $input) { }
        });

        //============================
        // Filter
        //============================
        $(".filter-item input[type='text']").bind("keydown", function (e) {
            if (e.keyCode == 13) {
                $("#ViewDataFilter").click();
            }
        });
        // Bind Suggestion
        Bind_Suggestion();

        // Toggle export excel button
        $(".export-wp").bind("click", function (e) {
            $(this).find("ul.ul-drop-down-list").toggle();
        });
    });

    //============================
    // Event delete
    //============================
    function del(This, code, name) {
        var code = code || null,
            name = name || null,
            Row = This;
        if (!code) return false;

        // show EBPopup
        $(".confirm-yn").openEBPopup();
        $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

        $("#EBPopup .yn-yes").bind("click", function () {
            $.ajax({
                type: "POST",
                url: "/GUI/SystemService/Ajax/Del.aspx/Delele_Custumer",
                data: '{Code : "' + code + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var mission = JSON.parse(response.d);
                    if (mission.success) {
                        delSuccess();
                        Row.remove();
                    } else {
                        delFailed();
                    }
                },
                failure: function (response) { alert(response.d); }
            });
        });
        $("#EBPopup .yn-no").bind("click", function () {
            autoCloseEBPopup(0);
        });
    }

    //============================
    // Suggestion Functions
    //============================
    function Bind_Suggestion() {
        $(".eb-suggestion").bind("keyup", function (e) {
            if (e.keyCode == 40) {
                UpDownListSuggest($(this));
            } else {
                Call_Suggestion($(this));
            }
        });
        $(".eb-suggestion").bind("focus", function () {
            Call_Suggestion($(this));
        });
        $(".eb-suggestion").bind("blur", function () {
            //Exc_To_Reset_Suggestion($(this));
        });
        $(window).bind("click", function (e) {
            if ((!e.target.className.match("eb-select") && e.target.parentElement.className.match("ul-listing-suggestion")) ||
                (!e.target.className.match("eb-select") && !e.target.parentElement.className.match("ul-listing-suggestion"))) {
                EBSelect_HideBox();
            }
        });
    }

    function UpDownListSuggest(This) {
        var UlSgt = This.parent().find(".ul-listing-suggestion"),
            index = 0,
            LisLen = UlSgt.find(">li").length - 1,
            Value;

        This.blur();
        UlSgt.find(">li.active").removeClass("active");
        UlSgt.find(">li:eq(" + index + ")").addClass("active");

        $(window).unbind("keydown").bind("keydown", function (e) {
            if (e.keyCode == 40) {
                if (index == LisLen) return false;
                UlSgt.find(">li.active").removeClass("active");
                UlSgt.find(">li:eq(" + (++index) + ")").addClass("active");
                return false;
            } else if (e.keyCode == 38) {
                if (index == 0) return false;
                UlSgt.find(">li.active").removeClass("active");
                UlSgt.find(">li:eq(" + (--index) + ")").addClass("active");
                return false;
            } else if (e.keyCode == 13) {
                // Bind data to HDF Field
                var THIS = UlSgt.find(">li.active");
                var Value = THIS.text().trim();
                //var Value = THIS.attr("data-code");
                var dataField = This.attr("data-field");

                BindIdToHDF(THIS, Value, dataField, "#HDF_Suggestion_Code", "#HDF_Suggestion_Field", This);
                EBSelect_HideBox();
            }
        });
    }

    function Exc_To_Reset_Suggestion(This) {
        var value = This.val();
        if (value == "") {
            $(".eb-suggestion").each(function () {
                var THIS = $(this);
                var sgValue = THIS.val();
                if (sgValue != "") {
                    BindIdToHDF(THIS, sgValue, THIS.attr("data-field"), "#HDF_Suggestion_Code", "#HDF_Suggestion_Field", THIS);
                    return false;
                }
            });
        }
    }

    function Call_Suggestion(This) {
        var text = This.val(),
            field = This.attr("data-field");
        Suggestion(This, text, field);
    }

    function Suggestion(This, text, field) {
        var This = This;
        var text = text || "";
        var field = field || "";
        var InputDomId;
        var HDF_Sgt_Code = "#HDF_Suggestion_Code";
        var HDF_Sgt_Field = "#HDF_Suggestion_Field";

        if (text == "") return false;

        switch (field) {
            case "customer.name": InputDomId = "#CustomerName"; break;
            case "customer.phone": InputDomId = "#CustomerPhone"; break;
            case "customer.code": InputDomId = "#CustomerCode"; break;
            case "bill.code": InputDomId = "#BillCode"; break;
        }

        $.ajax({
            type: "POST",
            url: "/GUI/SystemService/Ajax/Suggestion.aspx/Suggestion_Customer",
            data: '{field : "' + field + '", text : "' + text + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                var mission = JSON.parse(response.d);
                if (mission.success) {
                    var OBJ = JSON.parse(mission.msg);
                    if (OBJ.length > 0) {
                        var lis = "";
                        $.each(OBJ, function (i, v) {
                            lis += "<li data-code='" + v.Customer_Code + "'" +
                                         "onclick=\"BindIdToHDF($(this),'" + v.Value + "','" + field + "','" + HDF_Sgt_Code +
                                         "','" + HDF_Sgt_Field + "','" + InputDomId + "')\">" +
                                        v.Value +
                                    "</li>";
                        });
                        This.parent().find("ul.ul-listing-suggestion").empty().append(lis).parent().show();
                        This.parent().find(".eb-select-data").show();
                    } else {
                        This.parent().find(".eb-select-data").hide().find("ul.ul-listing-suggestion").empty();
                    }
                } else {
                    This.parent().find("ul.ul-listing-suggestion").empty();
                    var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                    showMsgSystem(msg, "warning");
                }
            },
            failure: function (response) { alert(response.d); }
        });
    }

    function BindIdToHDF(THIS, Code, Field, HDF_Sgt_Code, HDF_Sgt_Field, Input_DomId) {
        var text = THIS.text().trim();
        $("input.eb-suggestion").val("");
        $(HDF_Sgt_Code).val(Code);
        $(HDF_Sgt_Field).val(Field);
        $(Input_DomId).val(text);
        $(Input_DomId).parent().find(".eb-select-data").hide();

        // Auto post server
        $("#BtnFakeUP").click();
    }

    function EBSelect_HideBox() {
        $(".eb-select-data").hide();
        $("ul.ul-listing-suggestion li.active").removeClass("active");
    }

</script>

</asp:Panel>
</asp:Content>