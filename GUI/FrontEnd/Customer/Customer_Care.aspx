﻿<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="true" Title="Danh sách 30Shine Care" MasterPageFile="~/TemplateMaster/SiteMaster.Master" CodeBehind="Customer_Care.aspx.cs" Inherits="_30shine.GUI.FrontEnd.Customer_Care" %>
<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<style>
 
    #total-bill {
        color: darkred;
    }
</style>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">
                <li>Khách hàng &nbsp;&#187; </li>
                <li class="li-listing active"><a href="/khach-hang/danh-sach.html">Danh sách</a></li>
                <li class="li-add"><a href="/khach-hang/them-moi.html">Thêm mới</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add customer-listing be-report">
    <%-- Listing --%>                
    <div class="wp960 content-wp">
        <!-- Filter -->
        <div class="row">
            <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
            <div class="filter-item">
                <asp:TextBox CssClass="txtDateTime st-head form-control" ID="txtTimeFrom" placeholder="Từ ngày"
                   ClientIDMode="Static" runat="server"></asp:TextBox>
            </div>
            <strong class="st-head" style="margin-left: 10px;">
                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
            </strong>
            <div class="filter-item">
                <asp:TextBox CssClass="txtDateTime st-head form-control" ID="txtTimeTo" placeholder="Đến ngày"
                    ClientIDMode="Static" runat="server"></asp:TextBox>
            </div>
            <div class="filter-item">
                <asp:DropDownList ID="ddlVoucher" runat="server" ClientIDMode="Static" style="margin: 12px 0; width: 190px;">
                    
                </asp:DropDownList>
            </div>
            <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" ClientIDMode="Static" 
                       onclick="excPaging(1);" runat="server">Xem dữ liệu</asp:Panel>
            <a href="/khach-hang/customer-care" class="st-head btn-viewdata">Reset Filter</a>
        </div>     
        <div class="row row-filter">
            <strong class="st-head" style=""><i class="fa fa-filter"></i>Bộ lọc</strong>
            <div class="filter-item">
                <asp:DropDownList ID="ddlSalon" runat="server" ClientIDMode="Static" style="margin: 12px 0; width: 190px;"></asp:DropDownList>
            </div>
            <div class="filter-item">
                <asp:DropDownList ID="ddlDeparment" AutoPostBack="true" OnSelectedIndexChanged="Bind_Staff" runat="server" ClientIDMode="Static" style="margin: 12px 0; width: 190px;">
                    <asp:ListItem value="0">Chọn bộ phận</asp:ListItem>
                    <asp:ListItem value="1">Stylist</asp:ListItem>
                    <asp:ListItem value="2">Skinner</asp:ListItem>
                    <asp:ListItem value="5">Checkin</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="filter-item">
                <asp:DropDownList ID="ddlStaff" runat="server"  ClientIDMode="Static" style="margin: 12px 0; width: 190px;">
                <asp:ListItem Value="0">Chọn nhân viên</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="filter-item">
                <asp:TextBox data-value="0" CssClass="form-control" data-field="customer.phone"
                             AutoCompleteType="Disabled" ID="txtCustomerPhone" ClientIDMode="Static" placeholder="Số điện thoại" runat="server"></asp:TextBox>
                <div class="listing-staff-wp eb-select-data">
                    <ul class="ul-listing-staff ul-listing-suggestion" id="UlCustomerPhone"></ul>
                </div>
            </div> 
        </div>
        <!-- End Filter -->
        <!-- Row Table Filter -->
        
        <div class="table-func-panel">
         
            <div class="table-func-elm">
                <span>Số hàng / Page : </span>
                <div class="table-func-input-wp">
                    <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                    <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                    <ul class="ul-opt-segment">
                        <li data-value="10">10</li>
                        <li data-value="20">20</li>
                        <li data-value="30">30</li>
                        <li data-value="40">40</li>
                        <li data-value="50">50</li>
                        <li data-value="200">200</li>
                        <li data-value="1000000">Tất cả</li>
                    </ul>
                    <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server"  />
                </div>                
            </div>  
        
        </div>
        <!-- End Row Table Filter -->
        <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
        <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
            <ContentTemplate>
                
                <div class="table-wp">
                    <table class="table-add table-listing" id="table-bill">
                        <thead>
                            <tr>
                                <th>STT /<span id="total-bill">Tổng Bill: <%=Count %></span></th>
                                <th>Tên khách hàng</th>
                                <th>Số ĐT</th>
                                <th>Ngày tạo</th>
                                <th>Checkin</th>
                                <th>Stylist</th>
                                <th>Skinner</th>
                                <th>Checkout</th>
                                <th>Trạng thái</th>
                            </tr>
                        </thead>
                        <tbody>
                 
                            <asp:Repeater ID="RptCustomer" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                        <td><a href="/khach-hang/<%#Eval("CustomerId")%>.html"><%#Eval("CustomerName") %></a></td>
                                        <td><%#Eval("Phone") %></td>
                                        <td><%#Eval("CreatedTime") %></td>
                                        <td><%#Eval("CheckIn") %></td>
                                        <td><%#Eval("Stylist") %></td>
                                        <td><%#Eval("Skinner") %></td>
                                        <td><%#Eval("Checkout") %></td>
                                        <td><input  type="checkbox" <%# Convert.ToBoolean(Eval("IsUsing")) ? "checked='checked'" : "" %> /></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>                    
                        </tbody>
                    </table>
                </div>

                <!-- Paging -->
                <div class="site-paging-wp">
     
                <% if(PAGING.TotalPage > 1){ %>
                    <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                        <% if (PAGING._Paging.Prev != 0)
                           { %>
                        <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                        <% } %>
                        <asp:Repeater ID="RptPaging" runat="server">
                            <ItemTemplate>
                                <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                    <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>
                                >
                                    <%# Eval("PageNum") %>
                                </a>
                            </ItemTemplate>                    
                        </asp:Repeater>
                        <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                           { %>                
                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                        <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                        <% } %>
                    </asp:Panel>
                <% 
                   } %>
                </div>
		        <!-- End Paging -->                
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" style="display:none;" />

        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page"  />

        <!-- END Hidden Field-->
    </div>   
    <%-- END Listing --%>
</div>
    <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
<script>

    jQuery(document).ready(function () {
      
        // Add active menu
        $("#glbCustomer").addClass("active");
        //$("#ddlStaff").empty();
        //============================
        // Datepicker
        //============================
        $('.txtDateTime').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'vi',
            startDate: '2014/10/10',
            format: 'd/m/Y',
            dateonly: true,
            showHour: false,
            showMinute: false,
            timepicker: false,
            onChangeDateTime: function (dp, $input) { }
        });

        //============================
        // Filter
        //============================
        $(".filter-item input[type='text']").bind("keydown", function (e) {
            if (e.keyCode == 13) {
                $("#ViewDataFilter").click();
            }
        });
       
        $("#CustomerPhone").change(function () {
            addLoading();
            var phone = $("#CustomerPhone").val().trim();
            if (!checkPhone(phone)) {
                alert("Sai định dạng số điện thoại.");
                removeLoading();
                $("#CustomerPhone").focus();
                return;
            }
            else {
                $("#BtnFakeUP").click();
            }
            
        });
    });
    /*
    Check số phone
    */

    function checkPhone(phone) {
        phone = String(phone);
        var x = phone.substring(0, 1);

        if (phone.length < 10 || phone.length > 11 || x != '0' || validatePhone(phone) == false)
            return false;
        else
            return true;
    }
    function validatePhone(txtPhone) {
        var filter = /^[0-9-+]+$/;
        if (filter.test(txtPhone)) {
            return true;
        }
        else {
            return false;
        }
    }


</script>

</asp:Panel>
</asp:Content>
