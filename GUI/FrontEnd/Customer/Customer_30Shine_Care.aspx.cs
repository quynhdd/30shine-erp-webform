﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.Interface;

namespace _30shine.GUI.FrontEnd.Customer
{
    public partial class Customer_30Shine_Care : System.Web.UI.Page
    {
        private string PageID = "30SHINE_CARE";
        protected bool Perm_Access = false;
        protected bool Perm_ViewAllData = false;

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["User_Id"] == null)
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                var permission = Session["User_Permission"].ToString();
                Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
        }
    }
}