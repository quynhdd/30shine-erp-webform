﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using LinqKit;
using ExportToExcel;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.Interface;

namespace _30shine.GUI.FrontEnd
{
    public partial class Customer_Listing_V2 : System.Web.UI.Page
    {
        //khởi tạo PageID
        protected string PageID = "KH_KH";

        protected Paging PAGING = new Paging();
        private int TotalPage;
        private bool Perm_Access = false;
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        private bool Perm_ViewAllData = false;
        private int salonId;
        private string sql = "";
        private string sqlTotalRow = "";
        private string sqlBindView = "";

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {
            
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();

            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        CultureInfo culture = new CultureInfo("vi-VN");

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                Bind_Thread();
                genSql();
                Bind_Customer();
                Bind_Paging();
                RemoveLoading();
            }
            else
            {
                genSql();
            }
        }

        private void genSql()
        {
            var startTime = "2015/8/1";
            int threadId = Convert.ToInt32(Thread.SelectedValue);
            string suggestionCondition = "";
            string threadCondition = "";
            string salonCondition = "";
            string justproductCondition = "";
            string justserviceCondition = "";
            string timeCondition = "";
            string timescomeCondition = "";

            DateTime timeFrom = Convert.ToDateTime(startTime, culture);
            DateTime timeTo = new DateTime();

            // Điều kiện thời gian
            if (TxtDateTimeFrom.Text != "")
            {
                timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                if (TxtDateTimeTo.Text != "")
                {
                    timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                }
                else
                {
                    timeTo = timeFrom.AddDays(1);
                }
                timeCondition = " and a.CreatedDate between '" + String.Format("{0:yyyy/MM/dd}", timeFrom) + "' and '" + String.Format("{0:yyyy/MM/dd}", timeTo) + "'";
            }

            // Điều kiện filter suggestion : mã KH, tên KH, sđt
            switch (HDF_Suggestion_Field.Value)
            {
                case "customer.code":
                    suggestionCondition = " and a.Customer_Code like '%" + HDF_Suggestion_Code.Value + "%'";
                    break;
                case "customer.name":
                    suggestionCondition = " and a.Fullname like '%" + HDF_Suggestion_Code.Value + "%'";
                    break;
                case "customer.phone":
                    suggestionCondition = " and a.Phone like '%" + HDF_Suggestion_Code.Value + "%'";
                    break;
            }
            // Điều kiện nguồn thông tin biết đến cửa hàng
            if (threadId > 0)
            {
                threadCondition = " and a.Info_Flow = " + threadId;
            }
            // Điều kiện salon
            salonId = Convert.ToInt32(Salon.SelectedValue);
            if (salonId > 0)
            {
                salonCondition = " and a.SalonId = " + salonId;
            }
            // Điều kiên khách chỉ mua mỹ phẩm (chưa từng dùng dịch vụ)
            if (JustProduct.Checked == true)
            {
                justproductCondition = @" and 
                                        (
	                                        select COUNT(*) from BillService 
	                                        where IsDelete != 1 and Pending != 1 
	                                        and (ServiceIds != '' and ServiceIds is not null)
	                                        and CustomerId = a.Id
                                        ) = 0";
            }
            // Điều kiện khách chỉ dùng dịch vụ (chưa từng mua mỹ phẩm)
            if (JustService.Checked == true)
            {
                justserviceCondition = @" and 
                                        (
	                                        select COUNT(*) from BillService 
	                                        where IsDelete != 1 and Pending != 1 
	                                        and (ProductIds != '' and ProductIds is not null)
	                                        and CustomerId = a.Id
                                        ) = 0";
            }
            // Điều kiện khách lần đầu sử dụng dịch vụ
            if (FirstTimeService.Checked == true)
            {
                timescomeCondition = @" where c.timesComeSalon = 1";
            }
            else if (JustProduct.Checked == true || JustService.Checked == true)
            {
                timescomeCondition = @" where c.timesComeSalon >= 1";
            }

            // Build sql string
            //Sql exportExcel
            sql = @"select d.*, c.timesComeSalon, c.totalMoney, 
                        case 
		                    when CHARINDEX(' ', LTRIM(RTRIM(d.Fullname))) > 0 then LEFT(LTRIM(RTRIM(d.Fullname)), CHARINDEX(' ', LTRIM(RTRIM(d.Fullname))) - 1)
		                    end AS [firstName],
	                    case 
		                    when CHARINDEX(' ', LTRIM(RTRIM(d.Fullname))) > 0 then RIGHT(LTRIM(RTRIM(d.Fullname)), CHARINDEX(' ', Reverse(LTRIM(RTRIM(d.Fullname))))) 
		                    else d.Fullname
		                    end AS [lastName]
                    from
                    (
	                    select a.Id, COUNT(b.Id) as timesComeSalon, Coalesce(SUM(b.TotalMoney), 0) as totalMoney
	                    from Customer as a
	                    left join BillService as b
	                    on a.Id = b.CustomerId and b.IsDelete != 1 and b.Pending != 1
	                    where a.IsDelete != 1 and a.Phone != '' " +
                        timeCondition + suggestionCondition + salonCondition + justproductCondition + justserviceCondition +
                        @" group by a.Id
                    ) as c
                    inner join Customer as d
                    on c.Id = d.Id " + timescomeCondition;

            //sql TONG SO row
            sqlTotalRow = @"select d.Id
                    from
                    (
	                    select a.Id, COUNT(b.Id) as timesComeSalon
	                    from Customer as a
	                    left join BillService as b
	                    on a.Id = b.CustomerId and b.IsDelete != 1 and b.Pending != 1
	                    where a.IsDelete != 1 and a.Phone != '' " +
                        timeCondition + suggestionCondition + salonCondition + justproductCondition + justserviceCondition +
                        @" group by a.Id
                    ) as c
                    inner join Customer as d
                    on c.Id = d.Id " + timescomeCondition;

            //khai bao PageNumber
            int PageNumber = 1;
            int RowspPage = PAGING._Segment;
            //int HDFPage = Convert.ToInt32(HDF_Page.Value);
            //int HDFOPTSegment = Convert.ToInt32(HDF_OPTSegment.Value);
            if (HDF_Page.Value != "" && Convert.ToInt32(HDF_Page.Value) != 0)
            {
                PageNumber = Convert.ToInt32(HDF_Page.Value);
            }
            if (HDF_OPTSegment.Value != "" && Convert.ToInt32(HDF_OPTSegment.Value) != 0)
            {
                RowspPage = Convert.ToInt32(HDF_OPTSegment.Value);
            }

            sqlBindView = @"select * from 
                    (select  ROW_NUMBER() OVER(ORDER BY d.Id) AS Numero, d.Id,d.CreatedDate,d.Customer_Code,d.Fullname, c.timesComeSalon, c.totalMoney, 
                        case 
		                    when CHARINDEX(' ', LTRIM(RTRIM(d.Fullname))) > 0 then LEFT(LTRIM(RTRIM(d.Fullname)), CHARINDEX(' ', LTRIM(RTRIM(d.Fullname))) - 1)
		                    end AS [firstName],
	                    case 
		                    when CHARINDEX(' ', LTRIM(RTRIM(d.Fullname))) > 0 then RIGHT(LTRIM(RTRIM(d.Fullname)), CHARINDEX(' ', Reverse(LTRIM(RTRIM(d.Fullname))))) 
		                    else d.Fullname
		                    end AS [lastName]
                    from
                    (
	                    select a.Id, COUNT(b.Id) as timesComeSalon, Coalesce(SUM(b.TotalMoney), 0) as totalMoney
	                    from Customer as a
	                    left join BillService as b
	                    on a.Id = b.CustomerId and b.IsDelete != 1 and b.Pending != 1
	                    where a.IsDelete != 1 and a.Phone != ''";
            if (CustomerPhone.Text != "")
            {
                sqlBindView += " and a.Phone = '" + CustomerPhone.Text + "'";
            }
            sqlBindView += timeCondition + suggestionCondition + salonCondition + justproductCondition + justserviceCondition +
                       @" group by a.Id
                    ) as c
                    inner join Customer as d
                    on c.Id = d.Id  " + timescomeCondition + " ) as Customer where Numero BETWEEN (( " + PageNumber + " - 1) * " + RowspPage + " + 1) AND (" + PageNumber + " * " + RowspPage + ")  ORDER BY Id ";
        }

        private List<cls_customer> Get_Customer(bool isPaging)
        {
            using (var db = new Solution_30shineEntities())
            {
                List<cls_customer> lst = new List<cls_customer>();
                if (isPaging)
                {
                    lst = db.Database.SqlQuery<cls_customer>(sqlBindView).ToList();
                }
                else
                {
                    lst = db.Database.SqlQuery<cls_customer>(sql).ToList();
                }
                return lst;
            }
        }
        
        private void Bind_Customer()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Database.SqlQuery<cls_customerBindData>(sqlBindView).ToList();
                RptCustomer.DataSource = lst;
                RptCustomer.DataBind();
            }
        }
        private void Bind_Thread()
        {
            using (var db = new Solution_30shineEntities())
            {
                var Key = 0;
                var thread = db.SocialThreads.Where(w => w.IsDelete != 1 && w.stType == 1).ToList();
                Thread.DataTextField = "Name";
                Thread.DataValueField = "Id";

                ListItem item = new ListItem("Chọn nguồn thông tin", "0");
                Thread.Items.Insert(Key, item);
                if (thread.Count > 0)
                {
                    foreach (var v in thread)
                    {
                        item = new ListItem(v.Name, v.Id.ToString());
                        Thread.Items.Insert(++Key, item);
                    }
                }
                Thread.SelectedIndex = 0;
            }
        }

        //private void Bind_RptCustomer()
        //{
        //    var _Customers = Get_LSTCustomer(true);
        //    RptCustomer.DataSource = _Customers;
        //    RptCustomer.DataBind();
        //}

        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
            Bind_Customer();
            ScriptManager.RegisterStartupScript(UPTableListing, this.GetType(), "Format Price", "UP_FormatPrice('.be-report-price'); SetActiveTableListing();", true);
            RemoveLoading();
        }

        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? (HDF_Page.Value != "" ? Convert.ToInt32(HDF_Page.Value) : 1) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Database.SqlQuery<cls_customerBindData>(sqlTotalRow).ToList();
                int Count = lst.Count;
                var adfasdf = PAGING._Segment;
                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((float)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        public void AddLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "addLoading();", true);
        }

        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Export Excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Btn_SwitchExcel(object sender, EventArgs e)
        {
            Exc_ExportExcel(Get_Customer(false));
        }

        protected void Exc_ExportExcel(List<cls_customer> _Customers)
        {
            using (var db = new Solution_30shineEntities())
            {
                var _ExcelHeadRow = new List<string>();
                var BillRowLST = new List<List<string>>();
                var BillRow = new List<string>();
                var Where = PredicateBuilder.True<_30shine.MODEL.ENTITY.EDMX.Customer>();

                _ExcelHeadRow.Add("Ngày");
                _ExcelHeadRow.Add("Tên KH");
                _ExcelHeadRow.Add("Mã KH");
                _ExcelHeadRow.Add("Số ĐT");
                _ExcelHeadRow.Add("Tuổi");
                //_ExcelHeadRow.Add("Tỉnh/Thành");
                //_ExcelHeadRow.Add("Quận/Huyện");
                _ExcelHeadRow.Add("Địa chỉ");
                _ExcelHeadRow.Add("Nguồn thông tin");
                _ExcelHeadRow.Add("Số lần đến cửa hàng");
                _ExcelHeadRow.Add("Giá trị trọn đời");

                if (_Customers.Count > 0)
                {
                    foreach (var v in _Customers)
                    {
                        BillRow = new List<string>();
                        BillRow.Add(Convert.ToDateTime(v.CreatedDate).ToString("dd/MM/yyyy"));
                        BillRow.Add(v.lastName);
                        BillRow.Add(v.Customer_Code);
                        BillRow.Add(v.Phone);
                        BillRow.Add(v.Age.ToString());
                        //BillRow.Add(v.TenTinhThanh);
                        //BillRow.Add(v.TenQuanHuyen);
                        BillRow.Add(v.Address);
                        BillRow.Add(v.InforFlowName);
                        BillRow.Add(v.timesComeSalon.ToString());
                        BillRow.Add(v.totalMoney.ToString());
                        BillRowLST.Add(BillRow);
                    }
                }

                // export
                var ExcelStorePath = Server.MapPath("~") + "Public/Excel/Khach.Hang/";
                var FileName = "Khach_Hang_" + string.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
                var UrlFileName = "http://" + Request.Url.Host + "/Public/Excel/Khach.Hang/" + FileName;

                ExportXcel(_ExcelHeadRow, BillRowLST, ExcelStorePath + FileName);
                Bind_Paging();
                ScriptManager.RegisterStartupScript(BtnFakeExcel, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "');UP_FormatPrice('.be-report-price'); SetActiveTableListing();", true);
            }
        }

        private static void ExportXcel(List<string> _ExcelHeadRow, List<List<string>> List, string Path)
        {
            try
            {
                CreateExcelFile.ExcelHeadRow = _ExcelHeadRow;
                CreateExcelFile.CreateExcel(List, Path);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi. Không tạo được file excel.\r\nException: " + ex.Message);
            }
        }

        


        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }
        
    }

    public class cls_customer : _30shine.MODEL.ENTITY.EDMX.Customer
    {
        public int timesComeSalon { get; set; }
        public int totalMoney { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
    }

    public class cls_customerBindData
    {

        public int timesComeSalon { get; set; }
        public int totalMoney { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public int Id { get; set; }
        public string Fullname { get; set; }
        public string Customer_Code { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }

    }

}