﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using _30shine.MODEL.Structure.CRM;
using _30shine.MODEL.Data.CRM;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.FrontEnd.UICustomer
{
    public partial class Customer_HairMode_Report : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        protected string THead = "";
        public CultureInfo culture = new CultureInfo("vi-VN");
        private DateTime timeFrom;
        private DateTime timeTo;
        protected int salonId;
        protected int StaffID;
        private int integer;
        public string SMS_Signature;
        private static Customer_HairMode_Report instance;
        private string PageID = "KH_CS";
        protected bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected string Permission = "";

        public Solution_30shineEntities db = new Solution_30shineEntities();
        public CustomerChemistryData customerChemistryData;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                ////Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        public Customer_HairMode_Report()
        {
            customerChemistryData = new CustomerChemistryData(db);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                salonId = Convert.ToInt32(Session["SalonId"]);
                TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Today);
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
                bindSMS_Signature();
                bindData();
            }
            else
            {
                //Exc_Filter();
            }
            RemoveLoading();
        }

        /// <summary>
        /// Bind chữ ký checkout
        /// dungnm
        /// </summary>
        private void bindSMS_Signature()
        {
            var salon = Convert.ToInt32(ddlSalon.SelectedValue);
            var mapSMS = db.Tbl_Salon.Where(f => f.Id == salon).ToList();
            cls_SMS objSMS = new cls_SMS();
            foreach (var item in mapSMS)
            {
                string map = item.SMS_Signature;
                string[] mapSms = map.Split(',');
                for (int i = 0; i < mapSms.Length; i++)
                {
                    var sms = mapSms[i].ToString();
                    objSMS.id = item.Id;
                    objSMS.Signature = sms;
                    ddlSignature.Items.Add(new ListItem(objSMS.Signature.ToString()));
                }
            }
            ddlSignature.DataBind();
        }

        /// <summary>
        /// Set trạng thái đã nhắn tin cho khách
        /// </summary>
        /// dungnm
        /// <returns></returns>
        [WebMethod]
        public static object checkIsMessage(bool IsMessage, int BillID)
        {
            using (var db = new Solution_30shineEntities())
            {
                var message = new Library.Class.cls_message();
                var record = db.Customer_HairMode_Bill.FirstOrDefault(w => w.BillId == BillID);
                if (record != null)
                {
                    record.IsMessage = IsMessage;
                    db.Customer_HairMode_Bill.AddOrUpdate(record);
                    db.SaveChanges();
                }
                else
                {
                    message.success = false;
                    message.message = "Error! Record not found!";
                }

                return message;
            }
        }

        /// <summary>
        /// Bind data khách hàng
        /// dungnm
        /// </summary>
        private void bindData()
        {
            using (var db = new Solution_30shineEntities())
            {
                try
                {
                    if (TxtDateTimeFrom.Text != "")
                    {
                        timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                        if (TxtDateTimeTo.Text != "")
                        {
                            timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                        }
                        else
                        {
                            timeTo = timeFrom.AddDays(1);
                        }
                        salonId = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                        this.SMS_Signature = this.ddlSignature.SelectedValue;


                        var data = new List<CustomerChemistryStruct.cls_khach_hoa_chat>();
                        var data1 = new List<CustomerChemistryStruct.cls_khach_hoa_chat>();
                        var data2 = new List<CustomerChemistryStruct.cls_khach_hoa_chat>();
                        var data3 = new CustomerChemistryStruct.cls_khach_hoa_chat();

                        data1 = customerChemistryData.getListCustomer_NoChemistry(salonId, timeFrom, timeTo, this.SMS_Signature);
                        data2 = customerChemistryData.getListCustomer_ChemistryFirstTime(salonId, timeFrom, timeTo, this.SMS_Signature);
                        data = data1.Union(data2).ToList();
                     
                            Bind_Paging(data.Count);
                        rptDanhSach.DataSource = data.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                        rptDanhSach.DataBind();
                    }
                }
                catch
                {
                }
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            bindData();
            RemoveLoading();
        }

        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        

        protected void ddlSalon_SelectedIndexChanged(object sender, EventArgs e)
        {
            var salon = Convert.ToInt32(ddlSalon.SelectedValue);
            cls_SMS objSMS = new cls_SMS();
            var mapSMS = db.Tbl_Salon.Where(f => f.Id == salon).ToList();
            ddlSignature.Items.Clear();
            foreach (var item in mapSMS)
            {
                string map = item.SMS_Signature;
                string[] mapSms = map.Split(',');
                for (int i = 0; i < mapSms.Length; i++)
                {
                    //Insert vào flowproduct sau khi tách mapid
                    var sms = mapSms[i].ToString();
                    objSMS.id = item.Id;
                    objSMS.Signature = sms;
                    ddlSignature.Items.Add(new ListItem(objSMS.Signature.ToString()));
                }
            }
            ddlSignature.DataBind();
        }
    }
    public class custom_BillService
    {
        public string CustomerName { get; set; }
        public int Id { get; set; }
        public string CustomerCode { get; set; }
        public Nullable<int> Staff_Hairdresser_Id { get; set; }
        public string ProductIds { get; set; }
        public Nullable<int> TotalMoney { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> Staff_HairMassage_Id { get; set; }
        public Nullable<byte> IsDelete { get; set; }
        public string ServiceIds { get; set; }
        public string Images { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> SalonId { get; set; }
        public Nullable<byte> Pending { get; set; }
        public string BillCode { get; set; }
        public Nullable<System.DateTime> CompleteBillTime { get; set; }
        public Nullable<int> CustomerId { get; set; }
        public Nullable<int> BookingId { get; set; }
        public int? HairStyleId { get; set; }
        public string Title { get; set; }
        public string Phone { get; set; }
        public string SalonName { get; set; }
    }
    public class cls_SMS
    {
        public int id { get; set; }
        public string Signature { get; set; }
    }
}