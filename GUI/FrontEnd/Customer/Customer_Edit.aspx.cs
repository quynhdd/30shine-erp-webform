﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;
using System.Globalization;
using _30shine.Helpers;
using System.Web.Services;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.UICustomer
{
    public partial class Customer_Edit : System.Web.UI.Page
    {
        private string PageID = "KH_EDIT";
        protected string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
        protected Store_Customer_ChiTiet_Result OBJ;
        protected Customer_TotalReport Ctm_Statistic;
        private int billId;
        protected List<History_Listing> Ctm_History = new List<History_Listing>();
        private bool Perm_Access = false;
        private bool Perm_Edit = false;
        private bool Perm_ShowSalon = false;
        protected int Code;
        protected cls_Total_report objReport = new cls_Total_report();
        protected List<cls_History> lstHis = new List<cls_History>();
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            ApplyPermission();

            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ShowSalon);
                if (Load_Customer_report())
                {
                    //Bind_SocialThread();
                    //Bind_City();
                    //Bind_District();
                    Load_detail_History();
                    if (Perm_ShowSalon)
                    {
                        TrSalon.Visible = true;
                    }
                }
            }
            else
            {
                //
            }
        }

        private bool Bind_Customer()
        {
            int integer;
            Code = int.TryParse(Request.QueryString["Code"], out integer) ? integer : 0;
            var ExitsCustomer = true;
            using (var db = new Solution_30shineEntities())
            {
                OBJ = db.Store_Customer_ChiTiet(Code).FirstOrDefault();
                if (OBJ != null)
                {
                    HDF_CustomerId.Value = OBJ.Id.ToString();
                    FullName.Text = OBJ.Fullname;
                    //CustomerCode.Text = OBJ.Customer_Code;
                    Phone.Text = OBJ.Phone;
                    Email.Text = OBJ.Email;
                    //Age.Text = OBJ.Age.ToString();
                    //Address.Text = OBJ.Address;

                    // History     
                    CultureInfo culture = new CultureInfo("vi-VN");
                    DateTime _FromDate = Convert.ToDateTime("1/8/2015", culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                    var TotalBill = db.BillServices.Where(w => w.CreatedDate >= _FromDate && w.IsDelete != 1 && w.CustomerId == Code).OrderByDescending(o => o.Id).ToList();
                    var Count = TotalBill.Count;
                    var serializer = new JavaScriptSerializer();
                    if (Count > 0)
                    {
                        double TotalMoney = 0;
                        double TotalMoney_Product = 0;
                        double TotalMoney_Service = 0;
                        foreach (var v in TotalBill)
                        {
                            TotalMoney += Convert.ToInt32(v.TotalMoney);
                            // Execute services
                            var _History = new History_Listing();
                            var _Table = "<table class='table-add table-listing'><tbody>";
                            var _Count = 0;
                            double _TotalServiceMoney = 0;
                            double _TotalProductMoney = 0;

                            _History.Date = String.Format("{0:dd/MM/yyyy}", v.CreatedDate);

                            if (v.ServiceIds != null)
                            {
                                var ServiceListThis = serializer.Deserialize<List<ProductBasic>>(v.ServiceIds);
                                var Stylist = db.Staffs.FirstOrDefault(w => w.Id == v.Staff_Hairdresser_Id);
                                var Skinner = db.Staffs.FirstOrDefault(w => w.Id == v.Staff_HairMassage_Id);
                                //var HairMode = db.Customer_HairMode_Bill.FirstOrDefault(w=>w.BillId==v.Id);
                                //var HairModeName = db.Api_HairMode.FirstOrDefault(w=>w.Id==HairMode.HairStyleId);
                                var title = (from a in db.Customer_HairMode_Bill
                                             join b in db.Api_HairMode on a.HairStyleId equals b.Id
                                             where a.BillId == v.Id
                                             select new { HairModeId = a.Id, HairModeName = b.Title }).FirstOrDefault();


                                var Rating = db.Rating_ConfigPoint.FirstOrDefault(w => w.RealPoint == v.Mark && w.Status == 1 && w.IsDelete != 1);
                                var StylistName = Stylist != null ? "<i style='width: 100%; float: left;'> - Stylist : " + Stylist.Fullname + "</i>" : "";
                                var SkinnerName = Skinner != null ? "<i style='width: 100%; float: left;'> - Skinner : " + Skinner.Fullname + "</i>" : "";
                                var CheckinTime = v.CreatedDate != null ? "<i style='width: 100%; float: left;'> - Tgian checkin : " + String.Format("{0:HH:mm:ss dd/MM/yyyy}", v.CreatedDate) + "</i>" : "";
                                var RatingName = Rating != null ? "<i style='width: 100%; float: left;'> - Đánh giá : " + Rating.ConventionName + "</i>" : "";
                                var Title = title != null ? "<i style='width: 100%; float: left;'> - Kểu tóc : " + title.HairModeName + "</i>" : "";
                                if (ServiceListThis != null)
                                {
                                    foreach (var v2 in ServiceListThis)
                                    {
                                        TotalMoney_Service += v2.Price * v2.Quantity * (double)(100 - v2.VoucherPercent) / 100;
                                        _TotalServiceMoney += v2.Price * v2.Quantity * (double)(100 - v2.VoucherPercent) / 100;
                                        _Table += "<tr class='tr-service-color'>" +
                                                        "<td style='min-width: 90px;'>" + (_Count == 0 ? "Dịch vụ" : "") + "</td>" +
                                                        "<td>" + v2.Name + StylistName + SkinnerName + CheckinTime + RatingName + Title + "</td>" +
                                                        "<td class='be-report-price'>" + v2.Price + "</td>" +
                                                    "</tr>";
                                        _Count++;
                                    }
                                }
                            }
                            _Count = 0;

                            if (v.ProductIds != null)
                            {
                                var ProductListThis = serializer.Deserialize<List<ProductBasic>>(v.ProductIds);
                                if (ProductListThis != null)
                                {
                                    foreach (var v2 in ProductListThis)
                                    {
                                        TotalMoney_Product += v2.Price * v2.Quantity * (double)(100 - v2.VoucherPercent) / 100;
                                        _TotalProductMoney += v2.Price * v2.Quantity * (double)(100 - v2.VoucherPercent) / 100;
                                        _Table += "<tr class='tr-product-color'>" +
                                                        "<td style='min-width: 90px;'>" + (_Count == 0 ? "Sản phẩm" : "") + "</td>" +
                                                        "<td>" + v2.Name + "</td>" +
                                                        "<td class='be-report-price'>" + v2.Price + "</td>" +
                                                    "</tr>";
                                        _Count++;
                                    }
                                }
                            }

                            if (TotalMoney_Product > 0 || TotalMoney_Service > 0)
                            {
                                _Table += "<tr class='tr-total-color'>" +
                                                "<td>Tổng chi phí</td>" +
                                                "<td></td>" +
                                                "<td class='be-report-price'>" + (_TotalProductMoney + _TotalServiceMoney) + "</td>" +
                                            "</tr>";
                            }

                            _Table += "</tbody></table>";
                            _History.Table = _Table;
                            _History.Id = v.Id;

                            // images
                            if (v.Images != "" && v.Images != null)
                            {
                                String[] ListImagesName;
                                List<string> ListImagesUrl = new List<string>();
                                ListImagesName = v.Images.Split(',');
                                var Len = ListImagesName.Length;

                                if (Len > 0)
                                {
                                    var loop = 0;
                                    foreach (var v2 in ListImagesName)
                                    {
                                        var match = Regex.Match(v2, @"api.30shine.com");
                                        if (!match.Success)
                                        {
                                            //ListImagesName[loop] = "https://30shine.com/" + v2;
                                        }
                                        loop++;
                                    }
                                }
                                _History.Images = ListImagesName;
                            }
                            //Add to list
                            Ctm_History.Add(_History);
                        }

                        // Get product and service statistic                        
                        var ProductStatistic = "";
                        var ServiceStatistic = "";
                        var _TimesUsedService = 0;
                        var _TimesUsedProduct = 0;
                        var LST0 = db.BillServices.Where(w => w.CreatedDate >= _FromDate && w.IsDelete != 1 && w.CustomerId == Code).ToList();
                        // -- service
                        var LST = LST0.Join(db.FlowServices,
                                                    a => a.Id,
                                        b => b.BillId,
                                        (a, b) => new
                                        {
                                            a.CustomerCode,
                                            b.ServiceId,
                                            b.Price
                                        }
                                        )
                                        .GroupBy(g => g.ServiceId)
                                        .Select(s => new
                                        {
                                            s.First().ServiceId,
                                            s.First().CustomerCode,
                                            Money = s.Sum(sum => sum.Price),
                                            Times = s.Count()
                                        })
                                        .Join(db.Services,
                                        a => a.ServiceId,
                                        b => b.Id,
                                        (a, b) => new
                                        {
                                            a.ServiceId,
                                            a.CustomerCode,
                                            a.Money,
                                            a.Times,
                                            b.Name
                                        }
                                        )
                                        .ToList();
                        if (LST.Count > 0)
                        {
                            foreach (var v in LST)
                            {
                                ServiceStatistic += "<tr><td>" + v.Name + "</td>" +
                                                    "<td>" + v.Times + "</td>" +
                                                    "<td class='be-report-price'>" + v.Money + "</td></tr>";
                                _TimesUsedService += v.Times;
                            }
                        }

                        // -- product
                        var LST2 = LST0.Join(db.FlowProducts,
                                        a => a.Id,
                                        b => b.BillId,
                                        (a, b) => new
                                        {
                                            a.CustomerCode,
                                            b.ProductId,
                                            b.Price
                                        }
                                        )
                                        .GroupBy(g => g.ProductId)
                                        .Select(s => new
                                        {
                                            s.First().ProductId,
                                            s.First().CustomerCode,
                                            Money = s.Sum(sum => sum.Price),
                                            Times = s.Count()
                                        })
                                        .Join(db.Products,
                                        a => a.ProductId,
                                        b => b.Id,
                                        (a, b) => new
                                        {
                                            a.ProductId,
                                            a.CustomerCode,
                                            a.Money,
                                            a.Times,
                                            b.Name
                                        }
                                        )
                                        .ToList();
                        if (LST2.Count > 0)
                        {
                            foreach (var v in LST2)
                            {
                                ProductStatistic += "<tr><td>" + v.Name + "</td>" +
                                                    "<td>" + v.Times + "</td>" +
                                                    "<td class='be-report-price'>" + v.Money + "</td></tr>";
                                _TimesUsedProduct += v.Times;
                            }
                        }

                        Ctm_Statistic.TimesUsed_Service = _TimesUsedService;
                        Ctm_Statistic.TimesUsed_Product = _TimesUsedProduct;
                        Ctm_Statistic.TotalMoney = TotalMoney;
                        Ctm_Statistic.TotalMoney_Product = TotalMoney_Product;
                        Ctm_Statistic.TotalMoney_Service = TotalMoney_Service;
                        Ctm_Statistic.Service_Statistic = ServiceStatistic;
                        Ctm_Statistic.Product_Statistic = ProductStatistic;

                    }
                }
                else
                {
                    var msg = "Mã khách hàng không tồn tại!";
                    var status = "msg-system warning";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + msg + "','" + status + "');showDom('.content-customer-detail', true);", true);
                    ExitsCustomer = false;
                }
                return ExitsCustomer;
            }
        }

        //public void Bind_SocialThread()
        //{
        //    using (var db = new Solution_30shineEntities())
        //    {
        //        var lst = db.SocialThreads.OrderBy(p => p.Id).ToList();
        //        var Key = 0;

        //        SocialThread.DataTextField = "SocialThread";
        //        SocialThread.DataValueField = "Id";
        //        ListItem item = new ListItem("Chọn nguồn", "0");
        //        SocialThread.Items.Insert(0, item);

        //        foreach (var v in lst)
        //        {
        //            Key++;
        //            item = new ListItem(v.Name, v.Id.ToString());
        //            SocialThread.Items.Insert(Key, item);
        //        }

        //        var ItemSelected = SocialThread.Items.FindByValue(OBJ.Info_Flow.ToString());
        //        if (ItemSelected != null)
        //            ItemSelected.Selected = true;
        //    }
        //}

        //protected void Bind_City()
        //{
        //    using (var db = new Solution_30shineEntities())
        //    {
        //        var lst = db.TinhThanhs.OrderBy(p => p.ThuTu).ToList();

        //        City.DataTextField = "TenTinhThanh";
        //        City.DataValueField = "Id";

        //        City.DataSource = lst;
        //        City.DataBind();
        //        var ItemSelected = City.Items.FindByValue(OBJ.CityId.ToString());
        //        if (ItemSelected != null)
        //            ItemSelected.Selected = true;
        //    }
        //}

        //protected void Bind_District()
        //{
        //    using (var db = new Solution_30shineEntities())
        //    {
        //        int _TinhThanhID = Convert.ToInt32(City.SelectedValue);
        //        if (_TinhThanhID != 0)
        //        {
        //            var lst = db.QuanHuyens.Where(p => p.TinhThanhID == _TinhThanhID).OrderBy(p => p.ThuTu).ToList();

        //            District.DataTextField = "TenQuanHuyen";
        //            District.DataValueField = "Id";

        //            District.DataSource = lst;
        //            District.DataBind();

        //            if (!IsPostBack)
        //            {
        //                var ItemSelected = District.Items.FindByValue(OBJ.DistrictId.ToString());
        //                if (ItemSelected != null)
        //                {
        //                    ItemSelected.Selected = true;
        //                }
        //                else
        //                {
        //                    District.SelectedIndex = 0;
        //                }
        //            }
        //            else
        //            {
        //                District.SelectedIndex = 0;
        //            }
        //        }
        //    }
        //}

        //protected void Reload_District(object sender, EventArgs e)
        //{
        //    Bind_District();
        //}

        protected void Update_OBJ(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                var _Code = int.TryParse(Request.QueryString["Code"], out integer) ? integer : 0;
                var obj = db.Customers.Where(w => w.Id == _Code).FirstOrDefault();

                if (obj != null)
                {
                    obj.Fullname = FullName.Text;
                    obj.Phone = Phone.Text;
                    obj.Email = Email.Text;
                    obj.ModifiedDate = DateTime.Now;
                    if (Perm_ShowSalon)
                    {
                        obj.SalonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
                    }

                    db.Customers.AddOrUpdate(obj);
                    var exc = db.SaveChanges();
                    if (exc > 0)
                    {
                        var msg = "Cập nhật thành công!";
                        var status = "msg-system success";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + msg + "','" + status + "');showDom('.content-customer-detail', true);", true);
                    }
                    else
                    {
                        var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                        var status = "msg-system warning";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + msg + "','" + status + "');showDom('.content-customer-detail', true);", true);
                    }
                }
                else
                {
                    var msg = "Lỗi! Không tìm thấy khách hàng.";
                    var status = "msg-system warning";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + msg + "','" + status + "');showDom('.content-customer-detail', true);", true);
                }

                //Bind_Customer();
                if (Load_Customer_report())
                {
                    Load_detail_History();
                }

            }
        }

       
        // bin hairStyle
        [WebMethod]
        public static object HairStyleList()
        {
            using (var db = new Solution_30shineEntities())
            {
                return db.Api_HairMode.Where(w => w.IsDelete != true && w.Publish == true).ToList();
            }
        }
        [WebMethod]
        public static object SaveHistory(int HairStyleId, int CustomerID, int BillId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var obj = db.Customer_HairMode_Bill.FirstOrDefault(v => v.BillId == BillId);
                if (obj != null)
                {
                    // string test = DateTime.Now.ToString("yyyy/MM/dd");

                    obj.CustomerId = CustomerID;

                    obj.HairStyleId = HairStyleId;
                    obj.ModifiledDate = DateTime.Now;
                    obj.IsDelete = false;
                    db.Customer_HairMode_Bill.AddOrUpdate(obj);
                    db.SaveChanges();
                }
                else
                {
                    var OBJ = new Customer_HairMode_Bill();
                    OBJ.CustomerId = CustomerID;
                    OBJ.BillId = BillId;
                    OBJ.HairStyleId = HairStyleId;
                    OBJ.CreateDate = DateTime.Now;
                    OBJ.IsDelete = false;
                    db.Customer_HairMode_Bill.AddOrUpdate(OBJ);
                    db.SaveChanges();
                }
                return obj;
            }
        }

        private void ApplyPermission()
        {
            if (!Perm_Edit)
            {
                var Inputs = UIHelpers.GetMarkedControls(this.Controls, "perm-input-field");
                if (Inputs.Count() > 0)
                {
                    foreach (var v in Inputs)
                    {
                        ((TextBox)(v)).Enabled = false;
                    }
                }

                var DropDownLists = UIHelpers.GetMarkedControls(this.Controls, "perm-ddl-field");
                if (DropDownLists.Count() > 0)
                {
                    foreach (var v in DropDownLists)
                    {
                        ((DropDownList)(v)).Enabled = false;
                    }
                }

                var Buttons = UIHelpers.GetMarkedControls(this.Controls, "perm-btn-field");
                if (Buttons.Count() > 0)
                {
                    foreach (var v in Buttons)
                    {
                        ((Button)(v)).Visible = false;
                    }
                }
            }
        }


        public bool Load_Customer_report()
        {
            var db = new Solution_30shineEntities();
            var ExitsCustomer = true;
            int integer;
            Code = int.TryParse(Request.QueryString["Code"], out integer) ? integer : 0;
            var customer = db.Store_Customer_ChiTiet(Code).FirstOrDefault();
            if (customer != null)
            {
                HDF_CustomerId.Value = customer.Phone.ToString();
                Salon.SelectedValue = customer.SalonId.ToString();
                FullName.Text = customer.Fullname;
                Phone.Text = customer.Phone;
                Email.Text = customer.Email;
                
                objReport.CustomerId = customer.Id;
                objReport.CustomerName = customer.Fullname;
                objReport.CustomerPhone = customer.Phone;
                objReport.CustomerAge = customer.Age;
                objReport.Email = customer.Email;
                objReport.FirstSalonId = customer.SalonId;
                objReport.TotalReport = db.Store_Customer_Filter_totalServiceProductReport(Code).SingleOrDefault();
                objReport.ServiceDetail = db.Store_Customer_Filter_ServiceDetail(Code).ToList();
                objReport.ProductDetail = db.Store_Customer_Filter_ProductDetail(Code).ToList();
                objReport.TotalUsesService = 0;
                objReport.TotalMoneyService = 0;
                objReport.TotalUsesProduct = 0;
                objReport.TotalMoneyProduct = 0;
                foreach (var v in objReport.ServiceDetail)
                {
                    objReport.TotalUsesService += v.SolanSD;
                    objReport.TotalMoneyService += v.ChiPhiDV;
                }
                foreach (var v in objReport.ProductDetail)
                {
                    objReport.TotalUsesProduct += v.SolanSD;
                    objReport.TotalMoneyProduct += v.ChiPhiDV;
                }
            }
            else
            {
                var msg = "Mã khách hàng không tồn tại!";
                var status = "msg-system warning";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + msg + "','" + status + "');showDom('.content-customer-detail', true);", true);
                ExitsCustomer = false;
            }
            return ExitsCustomer;
        }

        public List<cls_History> Load_detail_History()
        {
            var db = new Solution_30shineEntities();
            var serializer = new JavaScriptSerializer();
            var bill = db.BillServices.Where(w => w.Pending == 0 && (w.IsDelete == 0 || w.IsDelete == null) && w.CustomerId == Code).OrderByDescending(w => w.CreatedDate).ToList();
            if (bill != null)
            {
                foreach (var v in bill)
                {
                    var his = db.Store_Customer_Filter_GetDetailBill(v.Id).SingleOrDefault();
                    var objHis = new cls_History();
                    if (v.ServiceIds != null)
                    {
                        objHis.billId = his.billId;
                        objHis.ReceptionId = his.ReceptionId;
                        objHis.CheckinName = his.CheckinName;
                        objHis.Staff_Hairdresser_Id = his.Staff_Hairdresser_Id;
                        objHis.StylistName = his.StylistName;
                        objHis.Staff_HairMassage_Id = his.Staff_HairMassage_Id;
                        objHis.SkinnerName = his.SkinnerName;
                        objHis.CreatedDate = his.CreatedDate;
                        objHis.DanhGia = his.DanhGia;
                        objHis.SalonId = his.SalonId;
                        objHis.SalonName = his.SalonName;
                        objHis.DatedBook = his.DatedBook;
                        objHis.HourFrame = his.HourFrame;
                        objHis.InProcedureTime = his.InProcedureTime;
                        objHis.WaitTimeInProcedure = his.WaitTimeInProcedure;
                        objHis.ServiceIds = serializer.Deserialize<List<ProductBasic>>(his.ServiceIds);
                        objHis.IsBookOnline = his.IsBookOnline != null ?  (bool)his.IsBookOnline : false;
                        if (objHis.ServiceIds != null)
                        {
                            foreach (var v2 in objHis.ServiceIds)
                            {
                                objHis.TotalMoneyServices += v2.Price * v2.Quantity * (double)(100 - v2.VoucherPercent) / 100;
                            }
                        }
                        objHis.ProductIds = serializer.Deserialize<List<ProductBasic>>(his.ProductIds);
                        if (objHis.ProductIds != null)
                        {
                            foreach (var v2 in objHis.ProductIds)
                            {
                                objHis.TotalMoneyProducts += v2.Price * v2.Quantity * (double)(100 - v2.VoucherPercent) / 100;
                            }
                        }
                        objHis.TotalMoney = objHis.TotalMoneyServices + objHis.TotalMoneyProducts;
                        if (v.Images != "" && v.Images != null)
                        {
                            String[] ListImagesName;
                            List<string> ListImagesUrl = new List<string>();
                            ListImagesName = v.Images.Split(',');
                            var Len = ListImagesName.Length;

                            if (Len > 0)
                            {
                                var loop = 0;
                                foreach (var v2 in ListImagesName)
                                {
                                    var match = Regex.Match(v2, @"api.30shine.com");
                                    if (!match.Success)
                                    {
                                        //ListImagesName[loop] = "https://30shine.com/" + v2;
                                    }
                                    loop++;
                                }
                            }
                            objHis.Images = ListImagesName;
                        }
                    }
                    lstHis.Add(objHis);
                }
            }
            return lstHis;
        }

        public class cls_Total_report
        {
            public int CustomerId { get; set; }
            public string CustomerName { get; set; }
            public string CustomerPhone { get; set; }
            public int? CustomerAge { get; set; }
            public string Email { get; set; }
            public string FirstSalon { get; set; }
            public int? FirstSalonId { get; set; }
            public Store_Customer_Filter_totalServiceProductReport_Result TotalReport { get; set; }
            public List<Store_Customer_Filter_ServiceDetail_Result> ServiceDetail { get; set; }
            public List<Store_Customer_Filter_ProductDetail_Result> ProductDetail { get; set; }
            public int? TotalUsesService { get; set; }
            public int? TotalMoneyService { get; set; }
            public int? TotalUsesProduct { get; set; }
            public int? TotalMoneyProduct { get; set; }
        }

        public class cls_History
        {
            public int billId { get; set; }
            public Nullable<int> Staff_Hairdresser_Id { get; set; }
            public string StylistName { get; set; }
            public Nullable<int> Staff_HairMassage_Id { get; set; }
            public string SkinnerName { get; set; }
            public Nullable<int> ReceptionId { get; set; }
            public string CheckinName { get; set; }
            public Nullable<System.DateTime> CreatedDate { get; set; }
            public string DanhGia { get; set; }
            public Nullable<int> SalonId { get; set; }
            public string SalonName { get; set; }
            public String[] Images { get; set; }
            public List<ProductBasic> ServiceIds { get; set; }
            public List<ProductBasic> ProductIds { get; set; }
            public Nullable<System.DateTime> DatedBook { get; set; }
            public Nullable<System.TimeSpan> HourFrame { get; set; }
            public Nullable<System.DateTime> InProcedureTime { get; set; }
            public Nullable<int> WaitTimeInProcedure { get; set; }
            public double TotalMoneyServices { get; set; }
            public double TotalMoneyProducts { get; set; }
            public double TotalMoney { get; set; }
            public bool IsBookOnline { get; set; }
        }
    }

    public struct Customer_TotalReport
    {
        public int TimesUsed { get; set; }
        public int TimesUsed_Service { get; set; }
        public int TimesUsed_Product { get; set; }
        public double TotalMoney { get; set; }
        public double TotalMoney_Service { get; set; }
        public double TotalMoney_Product { get; set; }
        public string Service_Statistic { get; set; }
        public string Product_Statistic { get; set; }
    }

    public struct History_Listing
    {
        public int? Id;
        public string Date { get; set; }
        public string Table { get; set; }
        public String[] Images { get; set; }
    }
}