﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Script.Serialization;
using LinqKit;
using ExportToExcel;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.Interface;


namespace _30shine.GUI.FrontEnd
{
	public partial class Customer_Care : System.Web.UI.Page
	{
	    private string PageID = "30SHINE_CARE";
        protected Paging PAGING = new Paging();
        protected bool Perm_Access = false;
	    protected int? Count = 0;
	    protected bool Perm_ViewAllData = false;
	    CultureInfo culture = new CultureInfo("vi-VN");
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
	    {

	        if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
	        {
	            var MsgParam = new List<KeyValuePair<string, string>>();
	            UIHelpers.Redirect("/dang-nhap.html", MsgParam);
	        }
	        else
	        {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                ExecuteByPermission();
            }
	    }
	    private void ExecuteByPermission()
	    {
	        if (!Perm_Access)
	        {
	            ContentWrap.Visible = false;
	            NotAllowAccess.Visible = true;
	        }
	    }
        protected void Page_Load(object sender, EventArgs e)
		{
            SetPermission();
		    if (!IsPostBack)
		    {
		        txtTimeFrom.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now));
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
                Bind_Voucher();
		        Bind_Data();
		       
		    }
		}
      
        /// <summary>
        /// lấy kiểu nhân viên
        /// </summary>
	    protected void Bind_StaffType()
	    {
	        ListItem item1 = new ListItem("Bộ phận", "0");
	        ListItem item2 = new ListItem("Stylist", "1");
	        ListItem item3 = new ListItem("Skinner", "2");
            ListItem item4 = new ListItem("Checkin", "5");
	       
            ddlDeparment.Items.Add(item1);
	        ddlDeparment.Items.Add(item2);
	        ddlDeparment.Items.Add(item3);
	        ddlDeparment.Items.Add(item4);
        }
        /// <summary>
        /// lấy trạng thái sử dụng voucher
        /// </summary>
        protected void Bind_Voucher()
	    {
	        ListItem item1 = new ListItem("Chưa sử dụng", "0");
            ListItem item2 = new ListItem("Đã sử dụng", "1");
	        ListItem item3 = new ListItem("Chọn trạng thái", "2");
            ddlVoucher.Items.Add(item3);
            ddlVoucher.Items.Add(item1);
	        ddlVoucher.Items.Add(item2);
	        ddlVoucher.SelectedIndex = 0;
	    }

        /// <summary>
        /// lấy danh sách nhân viên
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
	    protected void Bind_Staff(object sender, EventArgs e)
	    {
	
	        
            Solution_30shineEntities db = new Solution_30shineEntities();
	        int _SalonId = Convert.ToInt32(ddlSalon.SelectedValue != "" ? ddlSalon.SelectedValue : "0");
	        int _StaffType = Convert.ToInt32(ddlDeparment.SelectedValue != "" ? ddlDeparment.SelectedValue : "0");
            var listStaff = (from s in db.Staffs where s.SalonId == _SalonId && s.Type == _StaffType && s.Active == 1 select s).ToList();
            //var Key = 0;
	        ddlStaff.Items.Clear();
            ListItem item = new ListItem("Chọn Nhân viên", "0");
	        ddlStaff.Items.Add(item);

            foreach (var v in listStaff) 
	        {
	            //Key++;
	             ListItem item1 = new ListItem(v.Fullname, v.Id.ToString());
                 ddlStaff.Items.Add(item1);
                
            }
	       
            ddlStaff.SelectedIndex = 0;
        }
        protected void _BtnClick(object sender, EventArgs e)
	    {
	        Bind_Data();
	        RemoveLoading();
	    }

        /// <summary>
        /// lấy dữ liệu từ database hiển thị ra page
        /// </summary>
	    protected void Bind_Data()
	    {
            try
            {
                //khai bao PageNumber
                int PageNumber = 1;
                int RowspPage = PAGING._Segment;
                if (HDF_Page.Value != "" && Convert.ToInt32(HDF_Page.Value) != 0)
                {
                    PageNumber = Convert.ToInt32(HDF_Page.Value);
                }
                if (HDF_OPTSegment.Value != "" && Convert.ToInt32(HDF_OPTSegment.Value) != 0)
                {
                    RowspPage = Convert.ToInt32(HDF_OPTSegment.Value);
                }
                Solution_30shineEntities db = new Solution_30shineEntities();
                DateTime _StartDate = Convert.ToDateTime(txtTimeFrom.Text, culture);
                DateTime _ToDate = Convert.ToDateTime(txtTimeTo.Text != "" ? txtTimeTo.Text : txtTimeFrom.Text, culture);
                int _SalonId = Convert.ToInt32(ddlSalon.SelectedValue != "" ? ddlSalon.SelectedValue : "0");
                int _StaffID = Convert.ToInt32(ddlStaff.SelectedValue != "" ? ddlStaff.SelectedValue : "0");
                int _IsUsed = Convert.ToInt32(ddlVoucher.SelectedValue);
                string _Phone = txtCustomerPhone.Text;
                var lstData = db.Store_List_Customer_30ShineCare(_StartDate, _ToDate, _SalonId, _StaffID, _Phone, _IsUsed, PageNumber, RowspPage).ToList();
                if (lstData.Count > 0)
                {
                    //count total voucher by timefrom and timeto
                    Count = lstData[0].iCount;
                    //phân trang
                    Bind_Paging((int)lstData[0].iCount);
                }
                RptCustomer.DataSource = lstData;
                RptCustomer.DataBind();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        public void AddLoading()
	    {
	        ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "addLoading();", true);
	    }

	    public void RemoveLoading()
	    {
	        ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
	    }
    }
}