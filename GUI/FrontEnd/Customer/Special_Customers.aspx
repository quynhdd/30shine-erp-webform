﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Special_Customers.aspx.cs" Inherits="_30shine.GUI.FrontEnd.UICustomer.Special_Customers" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <link href="../../../Assets/libs/toastr/toastr.css" rel="stylesheet" />
    <link href="/Assets/libs/magicsuggest-master/magicsuggest.css" rel="stylesheet" />
    <script src="/Assets/libs/magicsuggest-master/magicsuggest.js"></script>
    <script src="../../../Assets/libs/toastr/toastr.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <style type="text/css">
        .table-listing .uv-avatar {
            width: 120px;
        }

        .table-row-detail {
            display: inline-block;
            min-width: 500px;
            float: none !important;
        }

        .quantity {
            width: 50px !important;
        }

        .lbl {
            padding: 6px 12px;
        }

        .be-report input[type='text'], .be-report select {
            width: 145px;
        }

        .toast-message {
            color: #ffffff !important;
        }
    </style>
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Khách hàng đặc biệt &nbsp;&#187; </li>
                        <li class="li-listing active"><a href="/khach-hang/danh-sach.html">Danh sách</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <div class="container" style="padding-left: 0px; padding-right: 0px;">
                    <div class="row form-group">
                        <div class="col-xs-3">
                            <div class="row form-group">
                                <div class="col-xs-12" style="padding-top: 10px;">
                                    <strong class="st-head" style="font-family: Roboto Condensed Bold; font-weight: normal;"><i class="fa fa-plus-circle"></i>Thêm mới</strong>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-xs-4 lbl">Khách hàng</div>
                                <div class="col-xs-8">
                                    <input id="txtCustomer" class="form-control" type="text" placeholder="Nhập điện thoại khách hàng" onchange="GetCustomerByPhone();" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-xs-4"></div>
                                <div class="col-xs-8">
                                    <input type="text" id="txtCustommerName" class="form-control" readonly />
                                    <input type="text" id="txtCustomerId" hidden />
                                    <input type="text" id="txtSpecialCusID" value="0" hidden />
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-xs-4 lbl">Loại khách</div>
                                <div class="col-xs-8">
                                    <%--<select id="ddlCustomerType" class="form-control" onchange="fnChange_ddlCustomerType();">
                                        <option value="0" selected="selected">Chọn loại khách</option>
                                        <option value="1">Khách đặc biệt</option>
                                        <option value="2">Khách VIP</option>
                                    </select>--%>
                                    <select name="ddlCustomerType" id="ddlCustomerType" class="form-control" disabled="disabled"></select>

                                </div>
                            </div>
                            <div class="form-group row divCustomer">
                                <div class="col-xs-4 lbl">Ngày cắt lỗi</div>
                                <div class="col-xs-8">
                                    <input id="txtCreatedDate" class="txtDateTime st-head form-control" type="text" />
                                </div>
                            </div>

                            <div class="form-group row divCustomer">
                                <div class="col-xs-4 lbl">Salon</div>
                                <div class="col-xs-8">
                                    <asp:DropDownList ID="ddlSalon" runat="server" ClientIDMode="Static" onchange="GetStafftBySalon()" CssClass="form-control select"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group row divCustomer">
                                <div class="col-xs-4 lbl">Nhân viên</div>
                                <div class="col-xs-8">
                                    <div class="suggestion-wrap">
                                        <input class="form-control ms-suggest" id="ddlStaff" type="text" placeholder="Search theo tên nhân viên" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row quantityInvited">
                                <div class="col-xs-4 lbl">Số lần mời dùng DV</div>
                                <div class="col-xs-8">
                                    <input id="txtQuantityInvited" class="form-control quantity" type="text" onkeypress="return isNumber(event)" />
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-xs-4 lbl">Giảm giá MP (%)</div>
                                <div class="col-xs-3">
                                    <input id="txtDiscountCosmetic" class="form-control quantity" type="text" onkeypress="return isNumber(event)" value="0" />
                                </div>
                                <div class="col-xs-3 lbl">Giảm giá DV (%)</div>
                                <div class="col-xs-2" style="padding-left: 0; margin-left: -5px;">
                                    <input id="txtDiscountService" class="form-control quantity" type="text" onkeypress="return isNumber(event)" value="100" />
                                </div>
                            </div>
                            <div class="form-group row divCustomer">
                                <div class="col-xs-4 lbl" style="margin-bottom: 15px;">Lý do</div>
                                <div class="col-xs-8" style="margin-bottom: 15px;">
                                    <div class="suggestion-wrap">
                                        <input class="form-control ms-suggest" id="txtNote" type="text" placeholder="Search theo tên lỗi" />
                                    </div>
                                </div>
                                <div class="form-group row textNote">
                                    <div class="col-xs-4 lbl">Ghi chú</div>
                                    <div class="col-xs-8">
                                        <textarea id="txtTextNote" class="form-control" rows="3"></textarea>
                                        <%--<textarea id="txtNote" class="form-control" rows="3"></textarea>--%>
                                    </div>
                                </div>
                                <div class="form-group row be-report">
                                    <div class="col-xs-4"></div>
                                    <div class="col-xs-8">
                                        <a href="javascript:void(0);" class="st-head btn-viewdata" onclick="InsertOrUpdateCustomer($(this));">Lưu</a>
                                        <a href="javascript:void(0);" onclick="location.href=location.href" class="st-head btn-viewdata">Làm mới</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-9 be-report be-report-timekeeping" style="padding-right: 0px; padding-left: 0;">
                            <div class="row">
                                <div class="filter-item">
                                    <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                                    <div class="time-wp">
                                        <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Từ ngày" Style="margin-left: 10px!important; width: 100px;"
                                            ClientIDMode="Static" runat="server"></asp:TextBox>

                                    </div>
                                    <strong class="st-head" style="margin-left: 10px;">
                                        <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                                    </strong>
                                    <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeTo" placeholder="Đến ngày" Style="margin-left: 10px!important; width: 100px;"
                                        ClientIDMode="Static" runat="server"></asp:TextBox>
                                    <br />
                                </div>
                                <div class="filter-item">
                                    <asp:DropDownList ID="ddlSalonFilter" CssClass="form-control select" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                </div>
                                <div class="filter-item">
                                    <asp:TextBox CssClass="form-control" ID="txtPhone" placeholder="Điện thoại" Style="margin-left: 0px!important"
                                        ClientIDMode="Static" runat="server"></asp:TextBox>
                                </div>
                                <div class="filter-item">
                                    <asp:DropDownList ID="ddlReasonFilter" runat="server" ClientIDMode="Static" Style="width: 130px;"></asp:DropDownList>
                                </div>
                                <div class="filter-item">
                                    <asp:DropDownList ID="ddlDuyet" runat="server" ClientIDMode="Static" Style="width: 130px;">
                                        <asp:ListItem Value="0" Text="Chọn Duyệt"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="Chưa duyệt"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Đã duyệt"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Không duyệt"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                                    onclick="excPaging(1); resetInsert();" runat="server">
                                    Xem dữ liệu
                                </asp:Panel>
                                <a href="javascript:void(0);" onclick="location.href=location.href" class="st-head btn-viewdata btn-reset">Reset Filter</a>

                            </div>


                            <!-- Row Table Filter -->
                            <div class="table-func-panel">
                                <div class="table-func-elm">
                                    <span>Số hàng / Page : </span>
                                    <div class="table-func-input-wp">
                                        <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                                        <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                                        <ul class="ul-opt-segment">
                                            <li data-value="10">10</li>
                                            <li data-value="20">20</li>
                                            <li data-value="30">30</li>
                                            <li data-value="40">40</li>
                                            <li data-value="50">50</li>
                                            <li data-value="200">200</li>
                                            <%--<li data-value="1000000">Tất cả</li>--%>
                                        </ul>
                                        <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <!-- End Row Table Filter -->
                            <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                            <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                                <ContentTemplate>
                                    <div class="table-wp">
                                        <table class="table-add table-listing" id="tblSpecialCustomer">
                                            <thead>
                                                <tr>
                                                    <th>STT</th>
                                                    <th>Khách hàng</th>
                                                    <th>Điện thoại</th>
                                                    <%--   <th>Nhân viên(cũ)</th>--%>
                                                    <th>Nhân viên (mới)</th>
                                                    <%--    <th>Lý do(cũ)</th>--%>
                                                    <th>Lý do(mới)</th>
                                                    <th>Ghi chú</th>
                                                    <th>Ngày dùng miễn phí gần nhất</th>
                                                    <th>Duyệt</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="RptCustomer" runat="server">
                                                    <ItemTemplate>
                                                        <tr onclick="addTrToTable($(this));" class="parent cls_setAC_<%# Eval("Id") %>" data-id="<%# Eval("Id") %>" data-custype="<%# Eval("CustomerTypeId") %>">
                                                            <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                            <%--  <td class="hidden"><%# Eval("TypeName") %></td>--%>
                                                            <td><%# Eval("Fullname") %></td>
                                                            <td><%# Eval("Phone") %></td>
                                                            <%-- <td><%# Eval("NhanVien") %></td>--%>
                                                            <td><%# Eval("LstErrorStaff_Name") %></td>
                                                            <%--  <td><%# Eval("LoaiLoi") %></td>--%>
                                                            <td><%# Eval("LstErrorTypeCutReason") %></td>
                                                            <td><%# Eval("ReasonDiff") %></td>
                                                            <td class="map-edit">
                                                                <%# Eval("maxDate") != null ? String.Format("{0:dd/MM/yyyy}", Eval("maxDate")) + " - " + String.Format("{0:HH:mm}", Eval("maxDate")) : "" %>
                                                                <div class="edit-wp">
                                                                    <a href="javascript:void(0);" class="btn btn-xs btn-success" title="sửa" onclick="EditSpecialCustomer($(this));" data-id="<%# Eval("Id") %>" data-custype="<%# Eval("CustomerTypeId") %>">Sửa</a>
                                                                    <a class="btn btn-xs btn-danger" href="javascript://" title="Xóa" onclick="del($(this),'<%# Eval("Id") %>', '<%# Eval("Fullname") %>')">Xóa</a>
                                                                </div>
                                                            </td>
                                                            <td style="position: relative;" class="per_mission">
                                                                <%--<% if ("ADMIN".Equals(Permission) || "root".Equals(Permission)  || "ASM".Equals(Permission))--%>
                                                                <% if (Permission.IndexOf("ADMIN") < 0 || Permission.IndexOf("root") < 0  || Permission.IndexOf("ASM") < 0)
                                                                {%>
                                                                        <div <%# Convert.ToInt32(Eval("Browse_ID")) == 1 ? "class=\"edit-wp display_duyet active\"" : "class=\"edit-wp display_duyet \"" %> <%# Convert.ToInt32(Eval("Browse_ID")) == 1 ? "style=\"display:block ;\"" : "style=\"display:none !important; \"" %> />
                                                                        <a href="javascript:void(0);" class="btn btn-xs btn-success   " title="sửa" onclick="ClickXetDuyet($(this));" data-id="<%# Eval("Id") %>" data-status="2">Duyệt</a>
                                                                        <a class="btn btn-xs btn-danger" data-id="<%# Eval("Id") %>" href="javascript://" onclick="ClickXetDuyet($(this))" data-status="3">Không duyệt</a>
                                                                        </div>
                                                                <%} %>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                    </div>

                                    <!-- Paging -->
                                    <div class="site-paging-wp">
                                        <% if (PAGING.TotalPage > 1)
                                            { %>
                                        <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                            <% if (PAGING._Paging.Prev != 0)
                                                { %>
                                            <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                            <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                            <% } %>
                                            <asp:Repeater ID="RptPaging" runat="server">
                                                <ItemTemplate>
                                                    <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                                        <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                                        <%# Eval("PageNum") %>
                                                    </a>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                                { %>
                                            <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                            <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                            <% } %>
                                        </asp:Panel>
                                        <% } %>
                                    </div>
                                    <!-- End Paging -->
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                            <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                        </div>
                    </div>
                </div>
            </div>
            <%-- END Listing --%>
        </div>
        <input id="hdfSessionPermission" type="hidden" value="<%=Permission%>" />
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 261px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/javascript">
            //khai bao suggestion Staff
            var msStaff;
            var listStaff = "";

            //khai bao suggestion Type Erorr
            var msErorr;
            var listTypeErorr = "";

            $(document).ready(function () {
                // Add active menu
                $("#glbCustomer").addClass("active");
                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });
                //============================
                // Filter
                //============================
                $(".filter-item input[type='text']").bind("keydown", function (e) {
                    if (e.keyCode == 13) {
                        $("#ViewDataFilter").click();
                    }
                });
                GetCustomerType();
                GetErrorCutReason();
                setTimeout(function () { GetStafftBySalon(); }, 300);

            });
            var specialId = 0;
            var id = 0;

            //load ds lý do cắt lỗi
            function GetErrorCutReason() {
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/Customer/Special_Customers.aspx/GetErrorCutReason",
                    data: '',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        console.log(response);
                        msErorr = $('#txtNote').magicSuggest({
                            maxSelection: 10,
                            //data: response.d,
                            valueField: 'Id',
                            displayField: 'Reason'
                        });
                        $(msErorr).on('selectionchange', function (e, m) {
                            listTypeErorr = this.getSelection();
                        });

                        msErorr.setData(response.d);

                        removeLoading();
                    },
                    failure: function (response) { console.log(response.d); }
                });
            }

            //load ds loại khách hàng
            function GetCustomerType() {
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/Customer/Special_Customers.aspx/GetCustomerType",
                    data: '',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var select = $('#ddlCustomerType');
                        //select.append('<option selected="selected" value="0">Chọn loại khách</option>');
                        $.each(response.d, function (key, value) {
                            select.append('<option value="' + value.Id + '">' + value.TypeName + '</option>');
                        });
                        $('#ddlCustomerType option').removeAttr('selected').filter('[value="1"]').attr('selected', 'selected');
                        removeLoading();
                    },
                    failure: function (response) { console.log(response.d); }
                });
            }

            //load ds nhân viên theo salon
            function GetStafftBySalon() {
                addLoading();
                var salonId = $("#ddlSalon").val();
                if (salonId == null) {
                    salonId = 0;
                }


                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/Customer/Special_Customers.aspx/GetStaffBySalon",
                    data: '{_SalonId: ' + salonId + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        console.log(response);
                        msStaff = $('#ddlStaff').magicSuggest({
                            maxSelection: 10,
                            //data: response.d,
                            valueField: 'Id',
                            displayField: 'Fullname'
                        });
                        $(msStaff).on('selectionchange', function (e, m) {
                            listStaff = this.getSelection();
                        });

                        msStaff.setData(response.d);
                        removeLoading();
                    },
                    failure: function (response) { console.log(response.d); }
                });

                //else
                //{
                //    select.html( '' );
                //    select.append( '<option selected="selected" value="0">Chọn Staff</option>' );
                //}
            }

            //get mã khách hàng theo điện thoại
            function GetCustomerByPhone() {
                addLoading();
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/Customer/Special_Customers.aspx/GetCustomerListByPhone",
                    data: '{_filter: "' + $("#txtCustomer").val().trim() + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.d != null) {
                            $("#txtCustommerName").val(response.d.Fullname);
                            $("#txtCustomerId").val(response.d.Id);
                            removeLoading();
                        }
                        else {
                            displayMessageWarning("Chưa tồn tại khách hàng");
                            $("#txtCustomer").focus();
                            removeLoading();
                            return;
                        }

                    },
                    failure: function (response) { console.log(response.d); }
                });
            }

            // insert hoặc update thông tin khách hàng đặc biệt
            function InsertOrUpdateCustomer(This) {
                var SpecialCusID = $("#txtSpecialCusID").val();
                var customerId = $("#txtCustomerId").val();
                if (customerId == "") {
                    displayMessageWarning("Đã xảy ra lỗi khi tìm khách hàng! Vui lòng nhập lại số điện thoại.");
                    $("#txtCustommer").focus();
                    return false;
                }
                var createdDate = $("#txtCreatedDate").val();
                if (createdDate == "") {
                    displayMessageWarning("Bạn chưa nhập ngày cắt lỗi ");
                    return false;
                }
                var salonId = $("#ddlSalon").val();
                if (salonId == "0") {
                    displayMessageWarning("Bạn chưa chọn Salon !");
                    return false;
                }
                var customerType = $("#ddlCustomerType").val();
                var quantityInvited = $("#txtQuantityInvited").val();
                if (quantityInvited == "") {
                    displayMessageWarning("Bạn chưa nhập số lần mời sử dụng dịch vụ! ");
                    return false;
                }
                var discountServices = $("#txtDiscountService").val();
                var discountCosmetic = $("#txtDiscountCosmetic").val();
                if (listTypeErorr.length == 0) {
                    displayMessageWarning("Bạn chưa nhập Lý do lỗi ! ");
                    return false;
                }
                var textNote = $("#txtTextNote").val();
                if (textNote == "") {
                    displayMessageWarning("Bạn chưa ghi chú ! ");
                    return false;
                }
                var Permission = $("#hdfSessionPermission").val();

                if (customerId != null && parseInt(customerId) > 0) {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/FrontEnd/Customer/Special_Customers.aspx/GetCustomerSpecial",
                        data: '{ customerId : "' + customerId + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            if (response.d != false) {
                                swal({
                                    text: "Khách hàng tồn tại trong danh sách, bạn có chắc chắn muốn cập nhật?",
                                    icon: "warning",
                                    buttons: true,
                                    dangerMode: true,
                                }).then((value) => {
                                    if (value) {
                                        $.ajax({
                                            type: "POST",
                                            url: "/GUI/FrontEnd/Customer/Special_Customers.aspx/InsertOrUpdateCustomer",
                                            data: '{ SpecialCusID : "' + SpecialCusID + '"  ,Customer_ID : "' + customerId + '", TypeCusSpecail_ID : "' + customerType + '", Date_Cut_Error : "' + createdDate + '", Salon_ID : "' + salonId + '", NumberFree : "' + quantityInvited + '" , DiscountCosmetics : "' + discountCosmetic + '", DiscountService : "' + discountServices + '", ReasonDiff : "' + textNote + '" , Permission : "' + Permission + '", ListStaffError : \'' + JSON.stringify(listStaff) + '\', ListTypeError :\'' + JSON.stringify(listTypeErorr) + '\' }',
                                            contentType: "application/json; charset=utf-8",
                                            dataType: "json",
                                            success: function (response) {
                                                alert("Cập nhật thành công");
                                                specialId = 0;
                                                window.location.href = "/khach-hang/dac-biet";
                                                resetInsert();
                                            },
                                            failure: function (response) { console.log(response.d); }
                                        });
                                    }
                                });
                            } else {
                                $.ajax({
                                    type: "POST",
                                    url: "/GUI/FrontEnd/Customer/Special_Customers.aspx/InsertOrUpdateCustomer",
                                    data: '{ SpecialCusID : "' + SpecialCusID + '"  ,Customer_ID : "' + customerId + '", TypeCusSpecail_ID : "' + customerType + '", Date_Cut_Error : "' + createdDate + '", Salon_ID : "' + salonId + '", NumberFree : "' + quantityInvited + '" , DiscountCosmetics : "' + discountCosmetic + '", DiscountService : "' + discountServices + '", ReasonDiff : "' + textNote + '" , Permission : "' + Permission + '", ListStaffError : \'' + JSON.stringify(listStaff) + '\', ListTypeError :\'' + JSON.stringify(listTypeErorr) + '\' }',
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    success: function (response) {
                                        alert("Thêm mới thành công");
                                        specialId = 0;
                                        window.location.href = "/khach-hang/dac-biet";
                                        resetInsert();
                                    },
                                    failure: function (response) { console.log(response.d); }
                                });
                            }
                            removeLoading();
                        },
                        failure: function (response) { console.log(response.d); }
                    });
                }
            }

            // fn khi click view chi tiết thông tin khách hàng đặc biệt
            function EditSpecialCustomer(THIS) {
                var Special_ID = THIS.data("id");
                addLoading();
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/Customer/Special_Customers.aspx/BindDataUpdateSpecialCus",
                    data: "{Special_ID: " + Special_ID + " }",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var OBJ = response.d;
                        // set gia tri cac field
                        $("#txtSpecialCusID").val(OBJ.SpecialCustomer_ID);
                        $("#txtCreatedDate").val(ConvertDateTime(OBJ.NgayCatLoi));
                        //console.log( $( "#txtSpecialCusID" ).val())
                        $("#txtCustomer").val(OBJ.Phone);
                        $("#txtCustommerName").val(OBJ.CusName);
                        $("#txtCustomerId").val(OBJ.Customer_ID);
                        $("#ddlCustomerType").val(OBJ.CustomerType_ID);
                        $("#ddlSalon").val(OBJ.Salon_ID);
                        $("#txtQuantityInvited").val(OBJ.QuantityInvited);
                        $("#txtDiscountCosmetic").val(OBJ.DiscountCosmetic);
                        $("#txtDiscountService").val(OBJ.DiscountService);
                        $("#txtTextNote").val(OBJ.TextNote);
                        // set gia tri nhan vien
                        if (response.d.ListStaff.length > 0) {
                            var arrNV = [];
                            $.each(response.d.ListStaff, function (i, v) {
                                arrNV.push({ Fullname: v.Fullname, Id: v.Id });
                            });
                            msStaff.setSelection(arrNV);
                        }
                        // set gia tri Reason
                        if (response.d.ListReason.length > 0) {
                            var arrErorr = [];
                            $.each(response.d.ListReason, function (i, v) {
                                arrErorr.push({ Reason: v.Reason, Id: v.Id });
                            });
                            msErorr.setSelection(arrErorr);
                        }
                        removeLoading();
                        setTimeout(function () { GetStafftBySalon(); }, 100);
                    },
                    failure: function (response) { console.log(response.d); }
                });
                if (!e) var e = window.event;
                e.cancelBubble = true;
                if (e.stopPropagation) e.stopPropagation();
            }

            //formar date jquery
            function ConvertDateTime(dateJson) {
                var dateString = dateJson.substr(6);
                var currentTime = new Date(parseInt(dateString));
                var month = currentTime.getMonth() + 1;
                month = month.toString();
                if (month.length < 2) {
                    month = "0" + month;
                }
                var day = currentTime.getDate();
                day = day.toString();
                if (day.length < 2) {
                    day = "0" + day;
                }
                var year = currentTime.getFullYear();
                var date = day + "/" + month + "/" + year;
                return date
            }
            //xóa khách hàng
            function del(This, code, name, e) {
                var code = code || null,
                    name = name || null,
                    Row = This;
                if (!code) return false;

                // show EBPopup
                $(".confirm-yn").openEBPopup();
                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

                $("#EBPopup .yn-yes").bind("click", function () {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/FrontEnd/Customer/Special_Customers.aspx/DeleteCustomer",
                        data: '{_idspec : ' + code + '}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            var mission = JSON.parse(response.d);
                            if (mission.success) {
                                delSuccess();
                                Row.remove();
                                setTimeout(function () {
                                    $("#ViewData").click();
                                    resetInsert();
                                }, 1000);

                            } else {
                                delFailed();
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    });
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
                if (!e) var e = window.event;
                e.cancelBubble = true;
                if (e.stopPropagation) e.stopPropagation();
            }

            //xóa chi tiết số lần mời dùng DV của từng khách hàng
            function delDetail(This, code, name, e) {
                var code = code || null,
                    name = name || null,
                    Row = This;
                if (!code) return false;

                // show EBPopup
                $(".confirm-yn").openEBPopup();
                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa ?");

                $("#EBPopup .yn-yes").bind("click", function () {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/FrontEnd/Customer/Special_Customers.aspx/DeleteDetail",
                        data: '{_id : ' + code + '}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            var mission = JSON.parse(response.d);
                            if (mission.success) {
                                delSuccess();
                                Row.remove();
                                setTimeout(function () {
                                    $("#ViewData").click();
                                    $("#txtCreatedDate").val("");
                                    $("select#ddlSalon").prop('selectedIndex', 0);
                                    setTimeout(function () { GetStafftBySalon() }, 300);
                                    GetErrorCutReason();
                                    $("#ddlCustomerType").val(1);
                                    $("#txtQuantityInvited").val("");
                                    $("#txtDiscountService").val("100");
                                    $("#txtDiscountCosmetic").val("0");
                                    $("#txtNote").val(0);

                                    specialId = $(This).attr("data-id");
                                    $("#txtCustomer").prop("disabled", "true");
                                }, 1000);
                            } else {
                                delFailed();
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    });
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
                if (!e) var e = window.event;
                e.cancelBubble = true;
                if (e.stopPropagation) e.stopPropagation();
            }

            //làm mới textbox sau khi thêm hoặc update thông tin khách hàng
            function resetInsert() {

                specialId = 0; id = 0;
                $("#txtCustomer").removeAttr("disabled");
                $("#txtCustommerName").val("");
                $("#txtCustomerId").val("");
                $("#txtCustomer").val("");
                $("#txtCreatedDate").val("");
                //$( "#ddlSalon" ).val( 0 );
                //GetStafftBySalon(  );
                $("select#ddlSalon").prop('selectedIndex', 0);
                setTimeout(function () { GetStafftBySalon() }, 300);
                GetErrorCutReason();
                $("#ddlCustomerType").val(1);
                $("#txtQuantityInvited").val("");
                $("#txtDiscountService").val("100");
                $("#txtDiscountCosmetic").val("0");
                $("#txtTextNote").val("");
                $("#txtNote").val(0);
                $(".quantityInvited").removeClass("hidden");
                $(".divCustomer").removeClass("hidden");
                setTimeout(function () {
                    permission();

                }, 100);



            }

            //fn hiển thị bảng thông tin chi tiết của từng khách hàng
            function addTrToTable(This, e) {
                addLoading();
                var rowIndex = $('#tblSpecialCustomer tbody tr.parent').index(This);
                var specialId = $(This).attr("data-id");
                var cusType = $(This).attr("data-custype");
                var newRow = $('<tr class="expand"> <td colspan="9">'
                    + '  <table class="table-row-detail">                       '
                    + '      <tbody>                                                                                                  '
                    + '         <tr><td colspan="6" style="border:none; text-align: left">Lịch sử </td></tr>                '
                    + '          <tr class="tr-field-ahalf tr-product">                                                                '
                    + '              <td style="border: none;" colspan="6">                                    '
                    + '                  <div class="listing-product item-product">                              '
                    + '                      <table class="table table-listing-product table-item-product" id="table-item-history">    '
                    + '                          <thead>                                                                               '
                    + '                              <tr>                                                                              '
                    + '                                  <th>STT</th>                                                                 '
                    + '                                  <th class="td-cutErrorDate">Ngày cắt</th>                                                                  '
                    + '                                  <th class="td-staff">Nhân viên </th>                                     '
                    + '                                  <th class="td-salon">Salon</th>                                                        '

                    + '                                  <th>Giảm giá DV</th>                                         '
                    + '                                  <th>Giảm giá MP</th>                              '
                    + '                                  <th>Số lần miễn phí</th>                                          '
                    + '                              </tr>                                                                             '
                    + '                          </thead>                                                                              '
                    + '                          <tbody>                                                                               '
                    + '                                                                                                                '
                    + '                          </tbody>                                                                              '
                    + '                      </table>                                                                                  '
                    + '                  </div>                                                                                        '
                    + '              </td>                                                                                             '
                    + '         </tr>                                                                                                  '
                    + '      </tbody>                                                                                                   '
                    + '  </table>                                                                                                       '
                    + '    </td></tr>');

                var nextIndex = rowIndex + 1;
                if ($('#tblSpecialCustomer tbody tr:nth(' + nextIndex + ')').hasClass('expand')) {
                    $('#tblSpecialCustomer tbody').find('.expand').remove();
                    $('#tblSpecialCustomer tbody tr').removeClass('test');
                }
                else {
                    for (var i = 0; i < $('#tblSpecialCustomer tbody tr').length; i++) {
                        $('#tblSpecialCustomer tbody tr').removeClass('test');
                        $('#tblSpecialCustomer tbody').find('.expand').remove();
                    }
                    newRow.insertAfter($('#tblSpecialCustomer tbody tr:nth(' + rowIndex + ')'));
                    $(This).addClass('test');
                }
                LoadDetailById(specialId, cusType);
                if (!e) var e = window.event;
                e.cancelBubble = true;
                if (e.stopPropagation) e.stopPropagation();
            }

            //Load thông tin chi tiết của từng khách hàng
            function LoadDetailById(specialId, cusType) {
                var reasonId = $("#ddlReasonFilter").val();
                var timeFrom = $("#TxtDateTimeFrom").val();


                var timeTo = $("#TxtDateTimeTo").val();

                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/Customer/Special_Customers.aspx/GetHistoryById",
                    data: '{_idspec: ' + specialId + ', id:' + id + ', reasonId: ' + reasonId + ', _timeFrom: "' + timeFrom + '", _timeTo: "' + timeTo + '" }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.d.length > 0) {
                            var trs = "";
                            var cutErrorDate = "-";
                            var quantityInvited = "-";
                            var stylist = "-";
                            var salon = "-";
                            $.each(response.d, function (i, v) {
                                if (v.CutErrorDate != null) {
                                    cutErrorDate = ConvertJsonDateToStringFormat(v.CutErrorDate)
                                }
                                if (v.QuantityInvited != null) {
                                    quantityInvited = v.QuantityInvited;
                                }
                                else {
                                    quantityInvited = "Không giới hạn";
                                }
                                if (v.StaffName != null) {
                                    stylist = v.StaffName;
                                }
                                if (v.SalonName != null) {
                                    salon = v.SalonName;
                                }
                                trs += '<tr>' +
                                    '<td class="td-STT">' + (i + 1) + '</td>' +
                                    '<td class="td-cutErrorDate">' + cutErrorDate + '</td>' +
                                    '<td class="td-staff">' + stylist + '</td>' +
                                    '<td class="td-salon">' + salon + '</td>' +
                                    '<td class="td-discountServices">' + v.DiscountServices + '</td>' +
                                    '<td class="td-discountCosmetic">' + v.DiscountCosmetic + '</td>' +
                                    '<td class="td-discountCosmetic">' + quantityInvited + '</td>' +
                                    //'<td class=" map-edit">' + v.Reason +
                                    //'<div class="edit-wp">' +
                                    //'<a href="javascript:void(0);" class="elm edit-btn" title="Sửa" onclick="EditDetail($(this));" data-id="' + v.Id + '" data-specId="' + v.SpecialCusId + '"></a>' +
                                    //'<a class="elm del-btn" href="javascript://" title="Xóa" onclick="delDetail($(this),' + v.Id + ')"></a>' +
                                    //' </div>' +
                                    //'</td>' +
                                    '</tr>';
                            });
                        }
                        $('#table-item-history tbody').append(trs);
                        if (cusType == "2") {
                            $(".td-cutErrorDate").addClass("hidden");
                            $(".td-staff").addClass("hidden");
                            $(".td-salon").addClass("hidden");
                        }
                        removeLoading();
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }

            //fn khi click để sửa thông tin chi tiết nhận dịch vụ
            function EditDetail(THIS) {
                addLoading();
                id = $(THIS).attr("data-id");
                specialId = $(THIS).attr("data-specId");
                $("#txtCustomer").prop("disabled", "true");
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/Customer/Special_Customers.aspx/GetCustomerById",
                    data: "{_idCus: " + specialId + " }",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {

                        $("#txtCustommerName").val(response.d.CustomerName);
                        $("#txtCustomerId").val(response.d.CustomerId);
                        $("#txtCustomer").val(response.d.Phone);
                        $("#ddlCustomerType").val(response.d.CustomerTypeId);
                        if (response.d.CustomerTypeId == "1") {
                            $(".quantityInvited").removeClass("hidden");
                            $(".divCustomer").removeClass("hidden");
                        }
                        else if (response.d.CustomerTypeId == "2") {
                            $(".quantityInvited").addClass("hidden");
                            $(".divCustomer").addClass("hidden");
                        }

                        $.ajax({
                            type: "POST",
                            url: "/GUI/FrontEnd/Customer/Special_Customers.aspx/GetHistoryById",
                            data: '{_idspec: ' + specialId + ', id:' + id + ' , reasonId: 0 , _timeFrom: "", _timeTo: ""}',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                $.each(response.d, function (i, v) {
                                    var cutErrorDate = "";
                                    var salon = 0;
                                    var stylist = 0;
                                    if (v.CutErrorDate != null) {
                                        cutErrorDate = ConvertJsonDateToStringFormat(v.CutErrorDate);
                                    }
                                    if (v.SalonId != null) {
                                        salon = v.SalonId;
                                    }
                                    if (v.StaffId != null) {
                                        stylist = v.StaffId;
                                    }
                                    $("#txtCreatedDate").val(cutErrorDate);
                                    $("#ddlSalon").val(salon);
                                    GetStafftBySalon(stylist);
                                    $("#txtQuantityInvited").val(v.QuantityInvited);
                                    $("#txtDiscountService").val(v.DiscountServices);
                                    $("#txtDiscountCosmetic").val(v.DiscountCosmetic);
                                    $("#txtNote").val(v.Note);
                                    $("#txtTextNote").val(v.ReasonDiff);
                                });
                                //removeLoading();
                            },
                            failure: function (response) { console.log(response.d); }
                        });

                    },
                    failure: function (response) { console.log(response.d); }
                });
            }

            //sự kiện khi thay đổi dropdownlist loại khách
            function fnChange_ddlCustomerType() {
                var val = $("#ddlCustomerType").val();
                if (val == "1") {
                    $("#txtDiscountCosmetic").val("0");
                    $(".quantityInvited").removeClass("hidden");
                    $(".divCustomer").removeClass("hidden");
                }
                else if (val == "2") {
                    $(".quantityInvited").addClass("hidden");
                    $(".divCustomer").addClass("hidden");
                    $("#txtDiscountCosmetic").val("10");
                }
            }

            // ClickXetDuyet khach dac biet
            function ClickXetDuyet(This) {
                var ID_SpeCus = This.data("id");
                var ID_Status = This.data("status");
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/Customer/Special_Customers.aspx/UpdateBrowse",
                    data: '{ID_SpeCus : ' + ID_SpeCus + ', ID_Status :' + ID_Status + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var OBJ = response.d;
                        if (OBJ.msg == "success") {
                            alert("Duyệt thành công !");
                            $(".cls_setAC_" + OBJ.data.Id + " .edit-wp.display_duyet   ").remove();
                        }
                        else {
                            alert("Có lỗi xảy ra ! Vui lòng liên hệ với nhóm phát triển !");
                        }
                    },
                });

            }

            $(document).ready(function () {
                setTimeout(function () {
                    permission();
                }, 300);
            });

            function permission() {
                var _permission = $("#hdfSessionPermission").val();

                if (_permission == 'salonmanager') {
                    $("#tblSpecialCustomer").addClass("salonmanager");
                }
            };
        </script>
    </asp:Panel>
    <style>
        .wp_duyet {
        }

        .cls_duyet {
            padding: 3px 15px;
        }

        .edit-wp .display_duyet {
            display: none;
        }

            .edit-wp .display_duyet.active {
                display: block;
            }


        /*td.map-edit .edit-wp{ display: none !important; }*/

        .customer-add #tblSpecialCustomer.salonmanager.table-add td.map-edit .edit-wp {
            display: none !important;
        }

        .customer-add #tblSpecialCustomer.salonmanager.table-add td.per_mission .edit-wp {
            display: none !important;
        }
    </style>
</asp:Content>

