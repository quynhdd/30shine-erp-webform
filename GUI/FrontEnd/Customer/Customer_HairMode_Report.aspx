﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Customer_HairMode_Report.aspx.cs" Inherits="_30shine.GUI.FrontEnd.UICustomer.Customer_HairMode_Report" %>



<asp:Content ID="pageHead" ContentPlaceHolderID="head" runat="server">
    <title>Báo cáo danh sách khách hàng</title>

    <style>
        .table-listing .uv-avatar {
            width: 120px;
        }

        .table-row-detail {
            display: inline-block;
            min-width: 500px;
            float: none !important;
        }
        .customer-listing  td:hover .ipcopy {
            display: block !important;
        }

        .ipcopy-phone
        {
            text-decoration: none !important;
            padding: 4px;
            position: absolute;
            top: 3px;
            right: 6px;
            display:none; 
        }
    </style>

</asp:Content>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Khách hàng &nbsp;&#187; </li>
                        <li class="be-report-li"><a href="/khach-hang/hoa-chat"><i class="fa fa-th-large"></i>Khách sử dụng hóa chất</a></li>
                        <%--<li class="be-report-li"><a href="/khach-hang/hoa-chat/cau-hinh-tin-nhan"><i class="fa fa-th-large"></i>Cấu hình mẫu tin nhắn</a></li>--%>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report be-report-timekeeping">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
                <div class="row">
                    <div class="filter-item">
                        <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="time-wp">
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Từ ngày" Style="margin-left: 10px!important"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ValidateTime" ControlToValidate="TxtDateTimeFrom" runat="server" CssClass="fb-cover-error" Text="Vui lòng chọn thời gian!"></asp:RequiredFieldValidator>
                        </div>
                        <strong class="st-head" style="margin-left: 10px;">
                            <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                        </strong>
                        <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeTo" placeholder="Đến ngày" Style="margin-left: 10px!important"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlSalon" CssClass="form-control select" runat="server" AutoPostBack="true"  ClientIDMode="Static" Style="width: 190px;" OnSelectedIndexChanged="ddlSalon_SelectedIndexChanged"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlSignature" runat="server" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                    </div>
                    <div class="filter-item" style="display: none;">
                        <asp:DropDownList ID="ddlCustomerClass" runat="server" ClientIDMode="Static" Style="width: 190px;">
                            <asp:ListItem Value="0">Chọn phân loại khách</asp:ListItem>
                            <asp:ListItem Value="1">Khách đến lần đầu, không sử dụng hóa chất</asp:ListItem>
                            <asp:ListItem Value="2">Khách lần đầu dùng hóa chất</asp:ListItem>
                            <%--<asp:ListItem Value="3">Khách đến lần đầu và sử dụng hóa chất</asp:ListItem>--%>
                        </asp:DropDownList>
                    </div>

                    <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <a href="javascript:void(0);" onclick="location.href=location.href" class="st-head btn-viewdata">Reset Filter</a>

                </div>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>Danh sách khách hàng</strong>
                </div>
                <!-- Row Table Filter -->
                <div class="table-func-panel" style="margin-top: -33px;">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="row table-wp">
                            <table class="table-add table-listing" id="tblHistorySalon">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Điện thoại</th>
                                        <th>Khách hàng</th>
                                        <th>Nội dung tin nhắn</th>
                                        <th>Đã nhắn</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptDanhSach" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td>
                                                    <a style="text-decoration: none !important;" id="customerPhone"><%# Eval("CustomerPhone") %></a>
                                                </td>
                                                <td><%# Eval("CustomerName") %></td>
                                                <td>
                                                    <%# Eval("HairMode") %>
                                                    <a class="ipcopy-phone btn btn-success" data-clipboard-text="<%# Eval("CustomerPhone") %>">Copy SĐT</a>
                                                </td>
                                                <td>
                                                <input onclick="updateIsMessage($(this), <%# Eval("BillID") %>)" value="<%#Eval("IsMessage")%>" type="checkbox" <%# Convert.ToBoolean(Eval("IsMessage")) ? "checked='checked'" : "" %> class="ipcopy-message" data-clipboard-text="<%# Eval("HairMode") %>" />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
            </div>
            <%-- END Listing --%>
        </div>


        <!-- Page loading -->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
        <!--/ Page loading -->
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <link href="/Assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <script src="/Assets/js/jquery.mCustomScrollbar.js"></script>
        <script src="/Assets/js/clipboard/clipboard.min.js"></script>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                // Add active menu
                //$("#glbAdminSales").addClass("active");
                //$("#glbAdminReportService").addClass("active");
                //$("li.be-report-li").addClass("active");

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });

                // Copy phone
                var clipboardPhone = new Clipboard('.ipcopy-phone', {});
                clipboardPhone.on('error', function (e) {
                    alert("Lỗi coppy. Vui lòng liên hệ nhóm phát triển.");
                });
                // Copy tin nhắn
                var clipboardMessage = new Clipboard('.ipcopy-message', {});
                clipboardMessage.on('error', function (e) {
                    alert("Lỗi coppy. Vui lòng liên hệ nhóm phát triển.");
                });

            });

            function CopyToClipboard(text) {
                Copied = text.createTextRange();
                Copied.execCommand("Copy");

            }

            var paramClass = function () {
                this.BillID = 0;
                this.IsMessage = false;

            }
            var paramObject = new paramClass();

            /*
            * Cập nhật trạng thái IsMessage : đã nhắn tin cho khách
            */
            function updateIsMessage(This, BillID) {
                paramObject.BillID = BillID;
                paramObject.IsMessage = This.prop("checked");
                addLoading();
                $.ajax({
                    type: 'POST',
                    url: '/GUI/FrontEnd/Customer/Customer_HairMode_Report.aspx/checkIsMessage',
                    data: "{ IsMessage : " + paramObject.IsMessage + ",  BillID : " + paramObject.BillID + "}",
                    contentType: 'application/json',
                }).success(function (response) {
                    alert("Cập nhật thành công!");
                    This.prop("checked") == true;
                    removeLoading();
                });
            }
        </script>



    </asp:Panel>
</asp:Content>
