﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Text.RegularExpressions;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.UICustomer
{
    public partial class Customer_Add : System.Web.UI.Page
    {
        private string PageID = "KH_TM";
        private bool Perm_Access = false;
        private bool Perm_ShowSalon = false;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
           
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ShowSalon = permissionModel.GetActionByActionNameAndPageId("Perm_ShowSalon", PageID, permission);
                ////Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                Bind_SocialThread();
                Bind_City();
                Bind_District();
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ShowSalon);
                if (Perm_ShowSalon)
                {
                    TrSalon.Visible = true;
                }                
            }
        }

        public void Bind_SocialThread()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.SocialThreads.Where(w=>w.IsDelete != 1 && w.Publish == 1 && w.stType == 1).OrderBy(p => p.Id).ToList();                
                var Key = 0;

                SocialThread.DataTextField = "SocialThread";
                SocialThread.DataValueField = "Id";
                ListItem item = new ListItem("Chọn nguồn", "0");
                SocialThread.Items.Insert(0, item);

                foreach (var v in lst)
                {
                    Key++;
                    item = new ListItem(v.Name, v.Id.ToString());
                    SocialThread.Items.Insert(Key, item);
                }
            }
        }

        protected void Bind_City()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.TinhThanhs.OrderBy(p => p.ThuTu).ToList();

                City.DataTextField = "TenTinhThanh";
                City.DataValueField = "Id";
                City.SelectedIndex = 1;

                City.DataSource = lst;
                City.DataBind();
            }
        }

        protected void Bind_District()
        {
            using (var db = new Solution_30shineEntities())
            {
                int _TinhThanhID = Convert.ToInt32(City.SelectedValue);
                if (_TinhThanhID != 0)
                {
                    var lst = db.QuanHuyens.Where(p => p.TinhThanhID == _TinhThanhID).OrderBy(p => p.ThuTu).ToList();

                    District.DataTextField = "TenQuanHuyen";
                    District.DataValueField = "Id";
                    District.SelectedIndex = 0;

                    District.DataSource = lst;
                    District.DataBind();
                }
            }
        }

        protected void Reload_District(object sender, EventArgs e)
        {
            Bind_District();
        }

        protected void AddCustomer(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var obj = new Customer();
                obj.Fullname = FullName.Text.Trim();
                obj.Phone = Phone.Text.Trim();

                if (Library.Function.checkName(obj.Fullname, 30) != true)
                {
                    var msg = "Lỗi. Tên chỉ gồm các chữ cái từ a-z, dấu cách. Tối đa 30 ký tự.";
                    var status = "msg-system warning";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + msg + "','" + status + "')", true);
                    return;
                }

                if (Library.Function.checkPhone(obj.Phone) != true)
                {
                    var msg = "Lỗi. Số điện thoại bắt đầu bằng số 0, gồm 10 hoặc 11 chữ số.";
                    var status = "msg-system warning";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + msg + "','" + status + "')", true);
                    return;
                }

                //obj.Email = Email.Text;
                if (Day.Text.Length > 0)
                {
                    obj.SN_day = Convert.ToInt32(Day.Text);
                }

                if (Month.Text.Length > 0)
                {
                    obj.SN_month = Convert.ToInt32(Month.Text);
                }
                if (Year.Text.Length > 0)
                {
                    obj.SN_year = Convert.ToInt32(Year.Text);
                }

                //obj.Age = int.TryParse(Age.Text, out integer) ? integer : 0;
                //obj.Address = Address.Text;
                obj.Customer_Code = CustomerCode.Text != "" ? CustomerCode.Text.Trim().ToUpper() : Phone.Text.Trim().ToUpper();
                obj.Info_Flow = Convert.ToInt32(SocialThread.SelectedValue);
                obj.CreatedDate = DateTime.Now;
                obj.CityId = Convert.ToInt32(City.SelectedValue);
                obj.DistrictId = Convert.ToInt32(District.SelectedValue);
                obj.Address = Address.Text;
                obj.IsDelete = 0;
                if (Perm_ShowSalon)
                {
                    obj.SalonId = Convert.ToInt32(Salon.SelectedValue);
                }
                else
                {
                    obj.SalonId = Convert.ToInt32(Session["SalonId"]);
                }

                // Validate
                // Check trùng mã dịch vụ
                var check = db.Customers.Where(w => w.IsDelete != 1 && w.Customer_Code == obj.Customer_Code).ToList();
                var Error = false;
                if (check.Count > 0)
                {
                    Error = true;
                    var msg = "Mã khách hàng đã tồn tại. Bạn vui lòng nhập mã khách hàng khác.";
                    var status = "msg-system warning";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + msg + "','" + status + "')", true);
                }

                if (!Error)
                {
                    db.Customers.Add(obj);
                    db.SaveChanges();

                    var msg = "Cập nhật thành công!";
                    var status = "msg-system success";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + msg + "','" + status + "')", true);
                }
                            
            }
        }

        
    }
}