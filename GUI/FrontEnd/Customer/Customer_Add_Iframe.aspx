﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Customer_Add_Iframe.aspx.cs" Inherits="_30shine.UICustomer.Customer_Add_Iframe" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Shortcut Icon" href="/Assets/images/favicon.png" type="image/x-icon" />

    <link href="/Assets/css/bootstrap/bootstrap.min.css" rel="stylesheet" />
    <link href="/Assets/css/font.css?v=2" rel="stylesheet" />
    <link href="/Assets/css/font-awesome.css" rel="stylesheet" />
    <link href="/Assets/css/style.css?v=3" rel="stylesheet" />    
    <link href="/Assets/css/jquery.datetimepicker.css?v=2" rel="stylesheet" />        

    <script src="/Assets/js/jquery.v1.11.1.js"></script>
    <script src="/Assets/js/bootstrap/bootstrap.min.js"></script>
    <script src="/Assets/js/jquery.datetimepicker.js"></script>    

    <script src="/Assets/js/ebstore/ebpopup.js"></script>
    <script src="/Assets/js/common.js"></script>
    <style type="text/css">
        .customer-add .table-add td input[type="text"], 
        .customer-add .table-add td textarea, 
        .customer-add .table-add td { height: 30px; line-height: 30px; }
        .customer-add .table-add td span, 
        .customer-add .table-add td input[type="text"], 
        .customer-add .table-add td select { height: 30px; line-height: 30px; }
        .msg-system { min-height: 3px;}
    </style>
</head>
<body>

<form id="form1" runat="server">

    <div class="wp customer-add" style="min-width: 480px; min-height: 260px; margin: 0;">
        <%-- Add --%>
        <div class="wp960 content-wp" style="padding: 0 20px;">
            <!-- System Message -->
            <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
            <!-- END System Message -->

            <div class="table-wp">
                <table class="table-add">
                    <tbody>
                        <tr class="title-head">
                            <td><strong style="font-family: Roboto Condensed Regular; font-size: 15px;">Thêm mới khách hàng</strong></td>
                            <td></td>
                        </tr>
                        <tr class="tr-margin" style="height: 5px;"></tr>
                        <tr>
                            <td class="col-xs-3 left"><span>Họ và tên</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:TextBox ID="FullName" CssClass="form-control" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="FullNameValidate" ControlToValidate="FullName" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập họ tên!"></asp:RequiredFieldValidator>
                                </span>

                            </td>
                        </tr>

                        <tr>
                            <td class="col-xs-3 left"><span>Số điện thoại</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:TextBox ID="Phone" CssClass="form-control" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator ID="PhoneValidate" ControlToValidate="Phone" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập số điện thoại!"></asp:RequiredFieldValidator>--%>
                                </span>
                            </td>
                        </tr>

                        <%--<tr style="display:none;">
                    <td class="col-xs-3 left"><span>Email</span></td>
                    <td class="col-xs-9 right">
                        <span class="field-wp">
                            <asp:TextBox ID="Email" runat="server" ClientIDMode="Static" placeholder="example@gmail.com"></asp:TextBox>
                        </span>
                    </td>
                </tr>--%>

                        <tr class="tr-birthday" style="display: none;">
                            <td class="col-xs-3 left"><span>Sinh nhật</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:TextBox ID="Day" CssClass ="form-control" runat="server" ClientIDMode="Static" placeholder="Ngày"></asp:TextBox>
                                    <asp:TextBox ID="Month" CssClass ="form-control" runat="server" ClientIDMode="Static" placeholder="Tháng"></asp:TextBox>
                                    <asp:TextBox ID="Year" CssClass ="form-control" runat="server" ClientIDMode="Static" placeholder="Năm"></asp:TextBox>
                                </span>
                            </td>
                        </tr>

                        <%--<tr class="tr-field-ahalf">
                    <td class="col-xs-3 left"><span>Tuổi</span></td>
                    <td class="col-xs-9 right">
                        <span class="field-wp">
                            <asp:TextBox ID="Age" runat="server" ClientIDMode="Static" placeholder="Ví dụ : 30"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ValidateAge" ControlToValidate="Age" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập tuổi!"></asp:RequiredFieldValidator>
                        </span>
                    </td>
                </tr>--%>

                        <tr style="display:none;">
                            <td class="col-xs-3 left"><span>Địa chỉ</span></td>
                            <td class="col-xs-9 right">
                                <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
                                <asp:UpdatePanel runat="server" ID="UP01">
                                    <ContentTemplate>
                                        <div class="city">
                                            <asp:DropDownList AutoPostBack="true" ID="City" runat="server" CssClass="select form-control"
                                                OnSelectedIndexChanged="Reload_District" ClientIDMode="Static">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="district">
                                            <asp:DropDownList ID="District" runat="server" CssClass="select form-control"
                                                ClientIDMode="Static">
                                            </asp:DropDownList>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <%--<asp:AsyncPostBackTrigger ControlID="OrderCloseBtn" EventName="Click"/>--%>
                                    </Triggers>
                                </asp:UpdatePanel>
                                <%--<asp:TextBox ID="Address" runat="server" placeholder="Địa chỉ" ClientIDMode="Static"></asp:TextBox>--%>
                            </td>
                        </tr>

                        <tr>
                            <td class="col-xs-3 left"><span style="line-height: 18px; padding-top: 4px;">Nguồn thông tin
                                <br />
                                biết đến cửa hàng</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp" style="height: auto; line-height: 18px;">
                                    <%--<asp:DropDownList ID="SocialThread" runat="server" ClientIDMode="Static"></asp:DropDownList>--%>
                                    <%--<asp:RequiredFieldValidator InitialValue="0"
                                        ID="ValidateSocialThread" Display="Dynamic"
                                        ControlToValidate="SocialThread"
                                        runat="server" Text="Bạn chưa chọn nguồn thông tin!"
                                        ErrorMessage="Vui lòng chọn nguồn thông tin!"
                                        ForeColor="Red"
                                        CssClass="fb-cover-error">
                                    </asp:RequiredFieldValidator>--%>
                                    <asp:Repeater runat="server" ID="Rpt_SocialThread" ClientIDMode="Static">
                                        <ItemTemplate>
                                            <label class="lbl-cus-no-infor" style="margin-right: 5px; margin-bottom: 8px; cursor: pointer; font-weight: normal; padding: 3px 10px; background: #ddd;">
                                                <input name="socialThread" type="radio" value="<%# Eval("Id") %>" <%# Container.ItemIndex == 0 ? "checked='checked'" : "" %> style="position: relative; top: 2px;"/> <%# Eval("Name") %>
                                            </label>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </span>
                            </td>
                        </tr>

                        <tr style="display:none;">
                            <td class="col-xs-3 left"><span>Mã khách hàng</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:TextBox ID="CustomerCode" CssClass ="form-control" runat="server" ClientIDMode="Static" placeholder="Ví dụ : CL0001"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator ID="ValidateCustomerCode" ControlToValidate="CustomerCode" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập mã Khách Hàng!"></asp:RequiredFieldValidator>--%>
                                </span>
                            </td>
                        </tr>

                        <tr class="tr-field-ahalf" runat="server" id="TrSalon" visible="false">
                            <td class="col-xs-3 left"><span>Salon</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:DropDownList ID="Salon" CssClass="form-control select" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                    <asp:RequiredFieldValidator InitialValue="0"
                                        ID="ValidateSalon" Display="Dynamic"
                                        ControlToValidate="Salon"
                                        runat="server" Text="Bạn chưa chọn Salon!"
                                        ErrorMessage="Vui lòng chọn Salon!"
                                        ForeColor="Red"
                                        CssClass="fb-cover-error">
                                    </asp:RequiredFieldValidator>
                                </span>
                            </td>
                        </tr>

                        <tr class="tr-send">
                            <td class="col-xs-3 left"></td>
                            <td class="col-xs-9 right no-border" style="padding-top: 7px;">
                                <span class="field-wp">
                                    <asp:Button ID="Send" CssClass="btn-send" runat="server" Text="Hoàn tất thêm KH" 
                                        ClientIDMode="Static"  OnClientClick="scriptBeforePost()" OnClick="AddCustomer" style="width: 126px!important;"></asp:Button>
                                    <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" onclick="window.parent.callback_HideIframeCustomer()" >Đóng</span>--%>
                                </span>
                            </td>
                        </tr>
                        <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                        <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
                        <asp:HiddenField runat="server" ID="HDF_SocialThread" ClientIDMode="Static" />
                    </tbody>
                </table>
            </div>
        </div>
        <%-- end Add --%>
    </div>
    <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
    <script>
        jQuery(document).ready(function () {
            $("#glbCustomer").addClass("active");
            //$("#FullName").focus();
            // Add style to parent window
            var style = "<style>" +
                            "@media only screen and (min-width : 768px) {" +
                                ".fe-service table.fe-service-table-add .iframe-add-customer iframe { min-height: " + ($(".customer-add").height() + 20) + "px !important; }" +
                            "}" +
                            "@media only screen and (min-width : 992px) {" +
                                ".fe-service table.fe-service-table-add .iframe-add-customer iframe { min-height: " + ($(".customer-add").height() + 20) + "px !important; }" +
                            "}" +
                        "</style>";
            window.parent.callback_Iframe_SetStyle(style);
        });

        function scriptBeforePost() {
            getSocialThreadId();
        }

        function getSocialThreadId()
        {
            $("#HDF_SocialThread").val($("input[name='socialThread']:checked").val());
        }
    </script>
</form>

</body>
</html>
