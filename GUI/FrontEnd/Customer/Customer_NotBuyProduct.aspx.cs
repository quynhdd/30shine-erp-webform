﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using LinqKit;
using ExportToExcel;
using System.Web.UI.HtmlControls;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.UICustomer
{
    public partial class Customer_NotBuyProduct : System.Web.UI.Page
    {
        private string PageID = "";
        protected Paging PAGING = new Paging();
        private bool Perm_Access = false;
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        private bool Perm_ViewAllData = false;
        private Expression<Func<BillService, bool>> Where = PredicateBuilder.True<BillService>();
        private Expression<Func<BillService, bool>> WhereBill = PredicateBuilder.True<BillService>();
        private Expression<Func<BillService, bool>> WhereTimeNotBuy = PredicateBuilder.True<BillService>();
        private DateTime StartTime = new DateTime();
        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();

            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                //ContentWrap.Visible = false;
                //NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            GenWhere();
            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                Bind_Paging();
                Bind_RptCustomer();
                Bind_TimeNotBuyProduct();
                RemoveLoading();
            }
        }

        private void GenWhere()
        {
            CultureInfo culture = new CultureInfo("vi-VN");
            var _BillService = new List<BillService>();
            StartTime = Convert.ToDateTime("1/8/2015", culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
            string SgtValue;
            DateTime _FromDate;
            DateTime _ToDate;
            DateTime _TimeNoBuy;

            if (IsPostBack)
            {
                if (TxtDateTimeFrom.Text != "")
                {
                    _FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                    // Thời gian chưa mua sản phẩm
                    switch (TimeNotBuyProduct.SelectedValue)
                    {
                        case "1": _TimeNoBuy = _FromDate.AddMonths(-1);
                            WhereTimeNotBuy = WhereTimeNotBuy.And(w => w.CreatedDate >= _TimeNoBuy);
                            break;
                        case "2": _TimeNoBuy = _FromDate.AddMonths(-2);
                            WhereTimeNotBuy = WhereTimeNotBuy.And(w => w.CreatedDate >= _TimeNoBuy);
                            break;
                        case "3": _TimeNoBuy = _FromDate.AddMonths(-3);
                            WhereTimeNotBuy = WhereTimeNotBuy.And(w => w.CreatedDate >= _TimeNoBuy);
                            break;
                        case "4": _TimeNoBuy = _FromDate.AddMonths(-4);
                            WhereTimeNotBuy = WhereTimeNotBuy.And(w => w.CreatedDate >= _TimeNoBuy);
                            break;
                        case "5": _TimeNoBuy = _FromDate.AddMonths(-5);
                            WhereTimeNotBuy = WhereTimeNotBuy.And(w => w.CreatedDate >= _TimeNoBuy);
                            break;
                        case "6": _TimeNoBuy = _FromDate.AddMonths(-6);
                            WhereTimeNotBuy = WhereTimeNotBuy.And(w => w.CreatedDate >= _TimeNoBuy);
                            break;
                        case "7": _TimeNoBuy = _FromDate.AddMonths(-6);
                            WhereTimeNotBuy = WhereTimeNotBuy.And(w => w.CreatedDate < _TimeNoBuy);
                            break;
                        default: WhereTimeNotBuy = WhereTimeNotBuy.And(w => w.CreatedDate >= _FromDate);
                            break;
                    }

                    // Where customer
                    if (TxtDateTimeTo.Text != "")
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                    }
                    else
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(1);
                    }
                    Where = Where.And(w => w.CreatedDate >= _FromDate && w.CreatedDate < _ToDate);
                }
                else
                {
                    // Mặc định dữ liệu ngày hôm nay
                    _FromDate = DateTime.Today;
                    Where = Where.And(w => w.CreatedDate >= _FromDate);
                }

                //switch (HDF_Suggestion_Field.Value)
                //{
                //    case "customer.code": SgtValue = HDF_Suggestion_Code.Value;
                //        Where = Where.And(w => w.Customer_Code.Contains(SgtValue));
                //        break;
                //    case "customer.name": SgtValue = HDF_Suggestion_Code.Value;
                //        Where = Where.And(w => w.Fullname.Contains(SgtValue));
                //        break;
                //    case "customer.phone": SgtValue = HDF_Suggestion_Code.Value;
                //        Where = Where.And(w => w.Phone.Contains(SgtValue));
                //        break;
                //}

                int SalonId = Convert.ToInt32(Salon.SelectedValue);
                if (SalonId > 0)
                {
                    Where = Where.And(p => p.SalonId == SalonId);
                }
                else
                {
                    Where = Where.And(p => p.SalonId > 0);
                }
            }
            else
            {
                // Mặc định dữ liệu ngày hôm nay
                _FromDate = DateTime.Today;
                Where = Where.And(w => w.CreatedDate >= _FromDate);
            }

            Where = Where.And(w => w.IsDelete != 1);
        }

        private void Bind_TimeNotBuyProduct()
        {
            TimeNotBuyProduct.DataTextField = "TimeNotBuyProduct";
            TimeNotBuyProduct.DataValueField = "Value";
            ListItem item = new ListItem("Thời gian chưa mua mỹ phẩm", "0");
            TimeNotBuyProduct.Items.Insert(0, item);
            item = new ListItem("1 tháng", "1");
            TimeNotBuyProduct.Items.Insert(1, item);
            item = new ListItem("2 tháng", "2");
            TimeNotBuyProduct.Items.Insert(2, item);
            item = new ListItem("3 tháng", "3");
            TimeNotBuyProduct.Items.Insert(3, item);
            item = new ListItem("4 tháng", "4");
            TimeNotBuyProduct.Items.Insert(4, item);
            item = new ListItem("5 tháng", "5");
            TimeNotBuyProduct.Items.Insert(5, item);
            item = new ListItem("6 tháng", "6");
            TimeNotBuyProduct.Items.Insert(6, item);
            item = new ListItem("Trên 6 tháng", "7");
            TimeNotBuyProduct.Items.Insert(7, item);
        }

        private void Bind_RptCustomer()
        {
            var _Customers = Get_LSTCustomer(true);
            RptCustomer.DataSource = _Customers;
            RptCustomer.DataBind();
        }

        public void Get_CustomerByTime()
        {
            using (var db = new Solution_30shineEntities())
            {
                // Lọc danh sách khách hàng không mua mỹ phẩm trong 1 thời gian
                var customerCodeList = new List<string>();
                var index = -1;
                // Get all bill in time
                var bills = db.BillServices.AsExpandable().Where(Where).ToList();
                if (bills.Count > 0)
                {
                    foreach (var v in bills)
                    {
                        customerCodeList.Add(v.CustomerCode);
                    }
                }
                WhereBill = WhereBill.And(w => customerCodeList.Contains(w.CustomerCode) && w.ProductIds != "");
                var bills2 = db.BillServices.AsExpandable().Where(WhereBill).ToList();
                if (bills2.Count > 0)
                {
                    foreach (var v in bills2)
                    {
                        index = customerCodeList.FindIndex(fi=>fi == v.CustomerCode);
                        if (index != -1)
                        {
                            customerCodeList.RemoveAt(index);
                        }
                    }
                }

                // Lấy dữ liệu khách hàng theo danh sách đã lọc
                var _Customers = db.Customers.Where(w => customerCodeList.Contains(w.Customer_Code)).OrderByDescending(o => o.Id).ToList();
            }
        }

        public List<Customer_Listing_Struct> Get_LSTCustomer(bool paging)
        {
            using (var db = new Solution_30shineEntities())
            {
                List<Customer_Listing_Struct> _CustomersReturn = new List<Customer_Listing_Struct>();
                List<Customer_Listing_Struct> LST = new List<Customer_Listing_Struct>();
                Customer_Listing_Struct _Cus = new Customer_Listing_Struct();
                var _Customers = new List<Customer>();

                // Lọc danh sách khách hàng không mua mỹ phẩm trong 1 thời gian
                var customerCodeList = new List<string>();
                var index = -1;
                // Get all bill in time
                var bills = db.BillServices.AsExpandable().Where(Where).ToList();
                if (bills.Count > 0)
                {
                    foreach (var v in bills)
                    {
                        customerCodeList.Add(v.CustomerCode);
                    }
                }
                WhereTimeNotBuy = WhereTimeNotBuy.And(w => customerCodeList.Contains(w.CustomerCode) && w.ProductIds != "");
                var bills2 = db.BillServices.AsExpandable().Where(WhereTimeNotBuy).ToList();
                if (bills2.Count > 0)
                {
                    foreach (var v in bills2)
                    {
                        index = customerCodeList.FindIndex(fi => fi == v.CustomerCode);
                        if (index != -1)
                        {
                            customerCodeList.RemoveAt(index);
                        }
                    }
                }

                // Lấy dữ liệu khách hàng theo danh sách đã lọc                
                if (paging)
                {
                    //_Customers = db.Customers.AsExpandable().Where(Where).OrderByDescending(o=>o.Id).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                    _Customers = db.Customers.Where(w => customerCodeList.Contains(w.Customer_Code)).OrderByDescending(o => o.Id).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                }
                else
                {
                    _Customers = db.Customers.Where(w => customerCodeList.Contains(w.Customer_Code)).OrderByDescending(o => o.Id).ToList();
                }

                LST = _Customers.Join(db.BillServices,
                                    a => a.Customer_Code,
                                    b => b.CustomerCode,
                                    (a, b) => new
                                    { a.Id, a.Fullname, a.Customer_Code, b.TotalMoney, a.Phone, a.Age, a.Address, a.CityId, a.DistrictId, a.Info_Flow, a.CreatedDate, 
                                        BillIsDelete = b.IsDelete, BillCreatedDate = b.CreatedDate, b.ProductIds, b.ServiceIds
                                    }
                                ).Where(w=> w.BillCreatedDate >= StartTime && w.BillIsDelete != 1)
                                .Join(db.TinhThanhs,
                                    a => a.CityId,
                                    b => b.ID,
                                    (a, b) => new
                                    {
                                        a.Id, a.Fullname, a.Customer_Code, a.TotalMoney, a.Phone, a.Age, a.Address, 
                                        b.TenTinhThanh, a.DistrictId, a.Info_Flow, a.CreatedDate
                                    }
                                )
                                .Join(db.QuanHuyens,
                                    a => a.DistrictId,
                                    b => b.ID,
                                    (a, b) => new
                                    {
                                        a.Id, a.Fullname, a.Customer_Code, a.TotalMoney, a.Phone, a.Age, a.Address, 
                                        a.TenTinhThanh, b.TenQuanHuyen, a.Info_Flow, a.CreatedDate
                                    }
                                )
                                .Join(db.SocialThreads,
                                    a => a.Info_Flow,
                                    b => b.Id,
                                    (a, b) => new {
                                        a.Id, a.Fullname, a.Customer_Code, a.TotalMoney, a.Phone, a.Age, a.Address, 
                                        a.TenTinhThanh, a.TenQuanHuyen, InforFlowName = b.Name, a.CreatedDate
                                    }
                                )
                                .GroupBy(g => g.Customer_Code)
                                .Select(s => new Customer_Listing_Struct
                                {
                                    Id = s.First().Id, 
                                    Fullname = s.First().Fullname,
                                    Customer_Code = s.First().Customer_Code, 
                                    TotalMoney = Convert.ToInt32(s.First().TotalMoney), 
                                    Phone = s.First().Phone, 
                                    Age = Convert.ToInt32(s.First().Age), 
                                    Address = s.First().Address, 
                                    TenTinhThanh = s.First().TenTinhThanh, 
                                    TenQuanHuyen = s.First().TenQuanHuyen, 
                                    InforFlowName = s.First().InforFlowName,
                                    CreatedDate = Convert.ToDateTime(s.First().CreatedDate),
                                    Times = s.Count(),
                                    LifeValue = Convert.ToInt32(s.Sum(sum => sum.TotalMoney))
                                }).ToList();
                

                if (_Customers.Count > 0)
                {
                    var loop = 0;
                    foreach (var v in _Customers)
                    { 
                        index = LST.FindIndex(fi=>fi.Id == v.Id);
                        if (index != -1)
                        {
                            _CustomersReturn.Add(LST[index]);
                        }
                        else
                        {
                            _Cus = new Customer_Listing_Struct();
                            _Cus.Id = v.Id;
                            _Cus.Fullname = v.Fullname;
                            _Cus.Customer_Code = v.Customer_Code;
                            _Cus.TotalMoney = 0;
                            _Cus.Phone = v.Phone;
                            _Cus.Age = Convert.ToInt32(v.Age);
                            _Cus.Address = v.Address;
                            _Cus.CreatedDate = Convert.ToDateTime(v.CreatedDate);
                            _Cus.Times = 0;
                            _Cus.LifeValue = 0;
                            var TinhThanh = db.TinhThanhs.FirstOrDefault(w=>w.ID == v.CityId);
                            _Cus.TenTinhThanh = TinhThanh != null ? TinhThanh.TenTinhThanh : "";
                            var QuanHuyen = db.QuanHuyens.FirstOrDefault(w=>w.ID == v.DistrictId);
                            _Cus.TenQuanHuyen = QuanHuyen != null ? QuanHuyen.TenQuanHuyen : "";
                            var FlowName = db.SocialThreads.FirstOrDefault(w=>w.Id == v.Info_Flow);
                            _Cus.InforFlowName = FlowName != null ? FlowName.Name : "";

                            _CustomersReturn.Add(_Cus);
                        }
                        loop++;
                    }
                }

                return _CustomersReturn;     
            }
        }

        public List<Customer_Listing_Struct> Get_LSTCustomer_NotBuyProduct(bool paging)
        {
            using (var db = new Solution_30shineEntities())
            {
                List<Customer_Listing_Struct> _CustomersReturn = new List<Customer_Listing_Struct>();
                List<Customer_Listing_Struct> LST = new List<Customer_Listing_Struct>();
                Customer_Listing_Struct _Cus = new Customer_Listing_Struct();
                var _Customers = new List<Customer>();

                if (paging)
                {
                    //_Customers = db.Customers.AsExpandable().Where(Where).OrderByDescending(o => o.Id).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                }
                else
                {
                    //_Customers = db.Customers.AsExpandable().Where(Where).OrderByDescending(o => o.Id).ToList();
                }
                LST = _Customers.Join(db.BillServices,
                                    a => a.Customer_Code,
                                    b => b.CustomerCode,
                                    (a, b) => new
                                    {
                                        a.Id,
                                        a.Fullname,
                                        a.Customer_Code,
                                        b.TotalMoney,
                                        a.Phone,
                                        a.Age,
                                        a.Address,
                                        a.CityId,
                                        a.DistrictId,
                                        a.Info_Flow,
                                        a.CreatedDate,
                                        BillIsDelete = b.IsDelete,
                                        BillCreatedDate = b.CreatedDate,
                                        b.ProductIds,
                                        b.ServiceIds
                                    }
                                ).Where(w => w.BillCreatedDate >= StartTime && w.BillIsDelete != 1 && w.ProductIds != "")
                                .Join(db.TinhThanhs,
                                    a => a.CityId,
                                    b => b.ID,
                                    (a, b) => new
                                    {
                                        a.Id,
                                        a.Fullname,
                                        a.Customer_Code,
                                        a.TotalMoney,
                                        a.Phone,
                                        a.Age,
                                        a.Address,
                                        b.TenTinhThanh,
                                        a.DistrictId,
                                        a.Info_Flow,
                                        a.CreatedDate,
                                        a.ProductIds
                                    }
                                )
                                .Join(db.QuanHuyens,
                                    a => a.DistrictId,
                                    b => b.ID,
                                    (a, b) => new
                                    {
                                        a.Id,
                                        a.Fullname,
                                        a.Customer_Code,
                                        a.TotalMoney,
                                        a.Phone,
                                        a.Age,
                                        a.Address,
                                        a.TenTinhThanh,
                                        b.TenQuanHuyen,
                                        a.Info_Flow,
                                        a.CreatedDate,
                                        a.ProductIds
                                    }
                                )
                                .Join(db.SocialThreads,
                                    a => a.Info_Flow,
                                    b => b.Id,
                                    (a, b) => new
                                    {
                                        a.Id,
                                        a.Fullname,
                                        a.Customer_Code,
                                        a.TotalMoney,
                                        a.Phone,
                                        a.Age,
                                        a.Address,
                                        a.TenTinhThanh,
                                        a.TenQuanHuyen,
                                        InforFlowName = b.Name,
                                        a.CreatedDate,
                                        a.ProductIds
                                    }
                                )
                                .GroupBy(g => g.Customer_Code)
                                .Select(s => new Customer_Listing_Struct
                                {
                                    Id = s.First().Id,
                                    Fullname = s.First().Fullname,
                                    Customer_Code = s.First().Customer_Code,
                                    TotalMoney = Convert.ToInt32(s.First().TotalMoney),
                                    Phone = s.First().Phone,
                                    Age = Convert.ToInt32(s.First().Age),
                                    Address = s.First().Address,
                                    TenTinhThanh = s.First().TenTinhThanh,
                                    TenQuanHuyen = s.First().TenQuanHuyen,
                                    InforFlowName = s.First().InforFlowName,
                                    CreatedDate = Convert.ToDateTime(s.First().CreatedDate),
                                    Times = s.Count(),
                                    LifeValue = Convert.ToInt32(s.Sum(sum => sum.TotalMoney)),
                                }).ToList();

                var _CustomerFull = Get_LSTCustomer(false);
                if (_CustomerFull.Count > 0)
                {
                    var index = -1;
                    foreach (var v in _CustomerFull)
                    {
                        index = LST.FindIndex(fi => fi.Id == v.Id);
                        if (index == -1)
                        {
                            _CustomersReturn.Add(v);
                        }
                    }
                }

                return _CustomersReturn;
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
            Bind_RptCustomer();
            RemoveLoading();
        }

        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? (HDF_Page.Value != "" ? Convert.ToInt32(HDF_Page.Value) : 1) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }      

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var _BillService = new List<BillService>();
                var Where = PredicateBuilder.True<Customer>();
                string SgtValue;

                if (TxtDateTimeFrom.Text != "")
                {
                    DateTime _FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                    DateTime _ToDate;
                    if (TxtDateTimeTo.Text != "")
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                    }
                    else
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(1);
                    }
                    Where = Where.And(w => w.CreatedDate > _FromDate && w.CreatedDate < _ToDate);
                }

                switch (HDF_Suggestion_Field.Value)
                {
                    case "customer.code": SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.Customer_Code.Contains(SgtValue));
                        break;
                    case "customer.name": SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.Fullname.Contains(SgtValue));
                        break;
                    case "customer.phone": SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.Phone.Contains(SgtValue));
                        break;
                }

                Where = Where.And(w => w.IsDelete != 1);

                int SalonId = Convert.ToInt32(Salon.SelectedValue);
                if (SalonId > 0)
                {
                    Where = Where.And(p => p.SalonId == SalonId);
                }
                else
                {
                    Where = Where.And(p => p.SalonId > 0);
                }

                var Count = db.Customers.AsExpandable().Count(Where);
                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }            
        }

        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Export Excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Btn_SwitchExcel(object sender, EventArgs e)
        {
            if (HDF_ExcelType.Value == "all")
            {
                Exc_ExportExcel(Get_LSTCustomer(false));
            }
            else if (HDF_ExcelType.Value == "notbuyproduct")
            {
                Exc_ExportExcel(Get_LSTCustomer_NotBuyProduct(false));
            }

        }

        protected void Exc_ExportExcel(List<Customer_Listing_Struct> _Customers)
        {
            using (var db = new Solution_30shineEntities())
            {
                var _ExcelHeadRow = new List<string>();
                var BillRowLST = new List<List<string>>();
                var BillRow = new List<string>();
                var Where = PredicateBuilder.True<Customer>();

                _ExcelHeadRow.Add("Ngày");
                _ExcelHeadRow.Add("Tên KH");
                _ExcelHeadRow.Add("Mã KH");
                _ExcelHeadRow.Add("Số ĐT");
                _ExcelHeadRow.Add("Tuổi");
                _ExcelHeadRow.Add("Tỉnh/Thành");
                _ExcelHeadRow.Add("Quận/Huyện");
                _ExcelHeadRow.Add("Địa chỉ");
                _ExcelHeadRow.Add("Nguồn thông tin");
                _ExcelHeadRow.Add("Số lần đến cửa hàng");
                _ExcelHeadRow.Add("Giá trị trọn đời");

                if (_Customers.Count > 0)
                {
                    foreach (var v in _Customers)
                    {
                        BillRow = new List<string>();
                        BillRow.Add(Convert.ToDateTime(v.CreatedDate).ToString("dd/MM/yyyy"));
                        BillRow.Add(v.Fullname);
                        BillRow.Add(v.Customer_Code);
                        BillRow.Add(v.Phone);
                        BillRow.Add(v.Age.ToString());
                        BillRow.Add(v.TenTinhThanh);
                        BillRow.Add(v.TenQuanHuyen);
                        BillRow.Add(v.Address);
                        BillRow.Add(v.InforFlowName);
                        BillRow.Add(v.Times.ToString());
                        BillRow.Add(v.LifeValue.ToString());
                        BillRowLST.Add(BillRow);
                    }
                }

                // export
                var ExcelStorePath = Server.MapPath("~") + "Public/Excel/Khach.Hang/";
                var FileName = "Khach_Hang_" + string.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
                var UrlFileName = "http://" + Request.Url.Host + "/Public/Excel/Khach.Hang/" + FileName;

                ExportXcel(_ExcelHeadRow, BillRowLST, ExcelStorePath + FileName);
                Bind_Paging();
                ScriptManager.RegisterStartupScript(BtnFakeExcel, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "');UP_FormatPrice('.be-report-price'); SetActiveTableListing();", true);
            }
        }

        private static void ExportXcel(List<string> _ExcelHeadRow, List<List<string>> List, string Path)
        {
            try
            {
                CreateExcelFile.ExcelHeadRow = _ExcelHeadRow;
                CreateExcelFile.CreateExcel(List, Path);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi. Không tạo được file excel.\r\nException: " + ex.Message);
            }
        }

        

        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }
    }

    public class Customer_Listing_Struct 
    {
        public int Id { get; set; }
        public string Fullname { get; set; }
        public string Customer_Code { get; set; }
        public int TotalMoney{ get; set; }
        public string Phone{ get; set; }
        public int Age{ get; set; }
        public string Address{ get; set; }
        public string TenTinhThanh{ get; set; }
        public string TenQuanHuyen{ get; set; }
        public string InforFlowName { get; set; }
        public DateTime CreatedDate { get; set; }
        public int Times { get; set; }
        public int LifeValue { get; set; }
        public string ProductIds { get; set; }
        public string ServiceIds { get; set; }
    }
}