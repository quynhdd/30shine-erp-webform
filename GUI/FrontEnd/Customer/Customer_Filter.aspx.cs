﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;
using System.Globalization;
using _30shine.Helpers;
using System.Web.Services;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using System.Configuration;

namespace _30shine.UICustomer
{
    public partial class Customer_Filter : System.Web.UI.Page
    {
        private string PageID = "KH_TK";
        protected cls_Total_report objReport = new cls_Total_report();
        protected List<cls_History> lstHis = new List<cls_History>();
        private bool Perm_Access = false;
        private bool Perm_Edit = false;
        protected int Code;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                //ContentWrap.Visible = false;
                //NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
        }

        public cls_Total_report Load_Customer_report()
        {
            try
            {
                using (var dbBI = new Solution_30shineEntities())
                {
                    var db = new Solution_30shineEntities();
                    if (txtFilter.Text.Trim() != "")
                    {
                        // Set lai connection string sang bi
                        dbBI.Database.Connection.ConnectionString = ConfigurationManager.ConnectionStrings["SQLConnectionStringBI"].ConnectionString;

                        var customer = (from a in dbBI.Customers
                                        join b in dbBI.Tbl_Salon on a.SalonId equals b.Id into bb
                                        from b in bb.DefaultIfEmpty()
                                        join c in dbBI.Products on a.MemberProductId equals c.Id into cc
                                        from c in cc.DefaultIfEmpty()
                                        where (a.IsDelete == 0 || a.IsDelete == null) &&
                                        a.Phone == txtFilter.Text.Trim()
                                        select new CustomerByPhone
                                        {
                                            Id = a.Id,
                                            Fullname = a.Fullname,
                                            Phone = a.Phone,
                                            Email = a.Email,
                                            Age = a.Age,
                                            SalonId = a.Age,
                                            SalonName = b.Name,
                                            MemberType = a.MemberType,
                                            MemberStartTime = a.MemberStartTime,
                                            MemberEndTime = a.MemberEndTime,
                                            ProductName = c.Name
                                        }).FirstOrDefault();
                        if (customer != null)
                        {
                            Code = customer.Id;
                            objReport.CustomerId = customer.Id;
                            objReport.CustomerName = customer.Fullname;
                            objReport.CustomerPhone = customer.Phone;
                            objReport.CustomerAge = customer.Age;
                            objReport.Email = customer.Email;
                            objReport.FirstSalon = customer.SalonName;
                            objReport.TotalReport = db.Store_Customer_Filter_totalServiceProductReport(Code).SingleOrDefault();
                            objReport.ServiceDetail = db.Store_Customer_Filter_ServiceDetail(Code).ToList();
                            objReport.ProductDetail = db.Store_Customer_Filter_ProductDetail(Code).ToList();
                            objReport.TotalUsesService = 0;
                            objReport.TotalMoneyService = 0;
                            objReport.TotalUsesProduct = 0;
                            objReport.TotalMoneyProduct = 0;
                            objReport.MemberType = customer.MemberType;
                            objReport.MemberStartTime = customer.MemberStartTime;
                            objReport.MemberEndTime = customer.MemberEndTime;
                            objReport.ProductName = customer.ProductName;
                            foreach (var v in objReport.ServiceDetail)
                            {
                                objReport.TotalUsesService += v.SolanSD;
                                objReport.TotalMoneyService += v.ChiPhiDV;
                            }
                            foreach (var v in objReport.ProductDetail)
                            {
                                objReport.TotalUsesProduct += v.SolanSD;
                                objReport.TotalMoneyProduct += v.ChiPhiDV;
                            }
                        }
                    }
                    return objReport;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public List<cls_History> Load_detail_History()
        {
            var db = new Solution_30shineEntities();
            var serializer = new JavaScriptSerializer();
            var bill = db.BillServiceHis.Where(w => w.Pending == 0 && (w.IsDelete == 0 || w.IsDelete == null) && w.CustomerId == Code).OrderByDescending(w => w.CreatedDate).ToList();
            if (bill != null)
            {
                foreach (var v in bill)
                {
                    var his = db.Store_Customer_Filter_GetDetailBill(v.Id).SingleOrDefault();
                    var objHis = new cls_History();
                    if (v.ServiceIds != null)
                    {
                        objHis.billId = his.billId;
                        objHis.Staff_Hairdresser_Id = his.Staff_Hairdresser_Id;
                        objHis.StylistName = his.StylistName;
                        objHis.Staff_HairMassage_Id = his.Staff_HairMassage_Id;
                        objHis.SkinnerName = his.SkinnerName;
                        objHis.CreatedDate = his.CreatedDate;
                        objHis.DanhGia = his.DanhGia;
                        objHis.SalonId = his.SalonId;
                        objHis.SalonName = his.SalonName;
                        objHis.DatedBook = his.DatedBook;
                        objHis.HourFrame = his.HourFrame;
                        objHis.InProcedureTime = his.InProcedureTime;
                        objHis.WaitTimeInProcedure = his.WaitTimeInProcedure;
                        objHis.ServiceIds = serializer.Deserialize<List<ProductBasic>>(his.ServiceIds);
                        if (objHis.ServiceIds != null)
                        {
                            foreach (var v2 in objHis.ServiceIds)
                            {
                                objHis.TotalMoneyServices += v2.Price * v2.Quantity * (double)(100 - v2.VoucherPercent) / 100;
                            }
                        }
                        objHis.ProductIds = serializer.Deserialize<List<ProductBasic>>(his.ProductIds);
                        if (objHis.ProductIds != null)
                        {
                            foreach (var v2 in objHis.ProductIds)
                            {
                                objHis.TotalMoneyProducts += v2.Price * v2.Quantity * (double)(100 - v2.VoucherPercent) / 100;
                            }
                        }
                        objHis.TotalMoney = objHis.TotalMoneyServices + objHis.TotalMoneyProducts;
                        if (v.Images != "" && v.Images != null)
                        {
                            String[] ListImagesName;
                            List<string> ListImagesUrl = new List<string>();
                            ListImagesName = v.Images.Split(',');
                            var Len = ListImagesName.Length;

                            if (Len > 0)
                            {
                                var loop = 0;
                                foreach (var v2 in ListImagesName)
                                {
                                    var match = Regex.Match(v2, @"api.30shine.com");
                                    if (!match.Success)
                                    {
                                        //ListImagesName[loop] = "https://30shine.com/" + v2;
                                    }
                                    loop++;
                                }
                            }
                            objHis.Images = ListImagesName;
                        }
                    }
                    lstHis.Add(objHis);
                }
            }
            return lstHis;
        }


        public void AddLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "addLoading();", true);
        }

        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }



        protected void btnFilter_Click(object sender, EventArgs e)
        {
            Load_Customer_report();
            Load_detail_History();
            RemoveLoading();
        }

        public class cls_Total_report
        {
            public int CustomerId { get; set; }
            public string CustomerName { get; set; }
            public string CustomerPhone { get; set; }
            public int? CustomerAge { get; set; }
            public string Email { get; set; }
            public string FirstSalon { get; set; }
            public Store_Customer_Filter_totalServiceProductReport_Result TotalReport { get; set; }
            public List<Store_Customer_Filter_ServiceDetail_Result> ServiceDetail { get; set; }
            public List<Store_Customer_Filter_ProductDetail_Result> ProductDetail { get; set; }
            public int? TotalUsesService { get; set; }
            public int? TotalMoneyService { get; set; }
            public int? TotalUsesProduct { get; set; }
            public int? TotalMoneyProduct { get; set; }
            public int? MemberType { get; set; }
            public DateTime? MemberStartTime { get; set; }
            public DateTime? MemberEndTime { get; set; }
            public string ProductName { get; set; }
        }

        public class cls_History
        {
            public int billId { get; set; }
            public Nullable<int> Staff_Hairdresser_Id { get; set; }
            public string StylistName { get; set; }
            public Nullable<int> Staff_HairMassage_Id { get; set; }
            public string SkinnerName { get; set; }
            public Nullable<System.DateTime> CreatedDate { get; set; }
            public string DanhGia { get; set; }
            public Nullable<int> SalonId { get; set; }
            public string SalonName { get; set; }
            public String[] Images { get; set; }
            public List<ProductBasic> ServiceIds { get; set; }
            public List<ProductBasic> ProductIds { get; set; }
            public Nullable<System.DateTime> DatedBook { get; set; }
            public Nullable<System.TimeSpan> HourFrame { get; set; }
            public Nullable<System.DateTime> InProcedureTime { get; set; }
            public Nullable<int> WaitTimeInProcedure { get; set; }
            public double TotalMoneyServices { get; set; }
            public double TotalMoneyProducts { get; set; }
            public double TotalMoney { get; set; }
        }
        public class CustomerByPhone
        {
            public int Id { get; set; }
            public string Fullname { get; set; }
            public string Phone { get; set; }
            public string Email { get; set; }
            public int? Age { get; set; }
            public int? SalonId { get; set; }
            public string SalonName { get; set; }
            public int? MemberType { get; set; }
            public DateTime? MemberStartTime { get; set; }
            public DateTime? MemberEndTime { get; set; }
            public string ProductName { get; set; }
        }
    }
}