﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Customer_hairMode.aspx.cs" Inherits="_30shine.GUI.FrontEnd.UICustomer.Customer_hairMode" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <title>Chọn kiểu tóc cho khách hàng</title>
    <script src="/Assets/js/jquery.v1.11.1.js"></script>
    <link href="/Assets/css/bootstrap/bootstrap.min.css" rel="stylesheet" />
    <link href="/Assets/css/font.css" rel="stylesheet" />
    <link href="/Assets/css/font-awesome.css" rel="stylesheet" />
    <link href="/Assets/css/csvc.css" rel="stylesheet" />

    <script src="../../../Assets/js/bootstrap/bootstrap.js"></script>
    <link href="/Assets/css/jquery.datetimepicker.css?v=2" rel="stylesheet" />
    <script src="/Assets/js/jquery.datetimepicker.js"></script>
    <style>
        .btn { width: 100%; background: #ccc; padding: 10px 5px; text-align: center; margin-top: 5px; margin-bottom: 10px; font-size: 14px; }
        .item-status-wrap { padding-left: 5px; padding-right: 5px; }
        .item-status.btn-ok.active { background: #50b347; }
        .item-status.btn-error.active { background: red; }
        .top-info { border: none !important; }

        .preview-img { background: #fff; }
        .heading-title { background: #000; padding: 10px; text-align: center; color: #fff; text-transform: uppercase; font-size: 16px; }
        .top-filter .form-control { margin-bottom: 5px; }
        .top-filter { padding: 5px; border-bottom: 1px solid #f2f2f2; }
        .top-info { padding: 5px; border-bottom: 1px solid #f2f2f2; }
        .top-info .bill-info { text-align: center; }
        .top-info .salon-name { text-align: center; padding-top: 4px; }
        
        .top-info strong { font-weight: bold; }

        .top4-img { padding: 5px; border-bottom: 1px solid #f2f2f2; }
        .top4-img .img-item { background: #808080; padding: 10px; text-align: center; border-right: 1px solid #fff; }
        /*.top4-img .img-item:first-child { border-right: 1px solid #fff;}*/
        .top4-img .img-item img { max-width: 100%; }
        .top4-img .col1 { border-bottom: 1px solid #fff; width: 100%; float: left; }
        .top4-img .col2 { width: 100%; float: left; }
        .img-nav { padding: 5px; border-bottom: 1px solid #f2f2f2; text-align: center; }
        /*#btnNext, #btnPreview { font-size: 20px;}*/
        .btn-default { background-color: #e6e6e6; }
        
        @media(max-width: 768px) {
            .modal-dialog {  width: 100%; }
        }

        .page-loading { width: 100%; height: 100%; position: fixed; top: 0; left: 0; z-index: 100000; background: rgba(0,0,0, 0.65) url(/Assets/images/load_ring.svg) center no-repeat; text-align: center; display: none; }
        .page-loading p { color: #fcd344; position: relative; top: 50%; padding-top: 37px; line-height: 22px; }
        .btn { white-space: inherit; }
    </style>
</head>
<body>
    <div class="wp">
        <div class="preview-img">
            <div class="heading-title">Kiểu tóc khách cắt</div>
            <div class="top-filter">
                <div>
                    <select name="ddlSalon" id="ddlSalon" class="form-control select" onchange="getStylistBySalon()">
                    </select>
                </div>
                <div>
                    <select name="ddlStylist" id="ddlStylist" class="form-control">
                        <option value="0">Chọn Stylist</option>
                    </select>
                </div>
                <div>
                    <input runat="server" name="txtToDate" type="text" value="" id="txtToDate" class="form-control txtDateTime" placeholder="Ngày ..." />
                </div>
                <div>
                    <select name="ddlCustomerClass" id="ddlCustomerClass" class="form-control">
                        <option value="0">Chọn phân loại khách</option>
                        <option value="1">Khách đến lần đầu, không sử dụng hóa chất</option>
                        <option value="2">Khách lần đầu dùng hóa chất</option>
                        <option value="3">Khách đến lần đầu và sử dụng hóa chất</option>
                    </select>
                </div>
                <div>
                    <input name="txtTelSearch" type="text" value="" id="txtTelSearch" class="form-control" placeholder="Nhập tìm kiếm số điện thoại..." />
                </div>
                <div>
                     <input type="submit" id="btnTimKiem" class="form-control btn-default btnSearch" value="Tìm" />
                </div>
            </div>
            <div class="top-info">
                 <div class="salon-name">Stylist: <strong id="pre-stylist"></strong></div>
                <div class="bill-info">Id: &nbsp<strong id="pre-id"></strong> &nbsp - &nbsp  Số điện thoại: &nbsp<strong id="pre-date"></strong></div>
                <div class="salon-name">Tên khách hàng: <strong id="pre-salon"></strong></div>
               <label id="pre-customerid" class="hidden"></label>
            </div>
            <div class="top-info text-center btnList btnHairList" style="padding-top: 0;">

            </div>
            <div class="top4-img">
                <div class="container">
                    <div class="row">
                        <div class="col1">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 img-item">
                                <img src="https://30shine.com/images/1.jpg" alt="" id="pre-img1" class="image" />
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 img-item">
                                <img src="https://30shine.com/images/2.jpg" alt="" id="pre-img2" class="image" />
                            </div>
                        </div>
                        <div class="col2">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 img-item">
                                <img src="https://30shine.com/images/3.jpg" alt="" id="pre-img3" class="image" />
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 img-item">
                                <img src="https://30shine.com/images/4.jpg" alt="" id="pre-img4" class="image"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="img-nav">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-4 col-sm-4">
                            <input type="submit" id="btnPreview" class="form-control btn-default" value="Trước" />
                        </div>
                        <div class="col-xs-4 col-sm-4">
                            &nbsp(Đang xem hóa đơn <strong id="current-index">0</strong>/<strong id="total-index">0</strong>)&nbsp
                        </div>
                        <div class="col-xs-4 col-sm-4">
                            <input type="submit" id="btnNext" class="form-control btn-default" value="Sau" />
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" value="0" id="hdfIndexPage" />
            <input type="hidden" value="0" id="HDF_BillID" />
        </div>
    </div>


    <div class="page-loading">
        <p>Vui lòng đợi trong giây lát...</p>
    </div>
    <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
    <script type="text/ecmascript">
       <%--var listElement = <%= listElement_Perm %>;--%>
        var _ListPreviewImg = [];
        var objPreviewImg = {};
        var billID;
        var cusHairId;
        var hairStyleId;

        function checkElement() {
            for (var j = 0; j < listElement.length; j++) {
                var nameElement = listElement[j].ElementName;
                var enabled = listElement[j].Enable;
                var type = listElement[j].Type;
                if (type == "hidden" && enabled == true) {
                    $("." + nameElement).addClass('hidden');
                }
                else if(type == "disable" && enabled == true && nameElement.substr(0, 3) == "btn"){
                    $("." + nameElement).addClass('disabled');
                }
                else {
                    $("." + nameElement).prop('disabled', enabled);
                }
            }
        }
        //checkElement();
        
        $(document).ready(function () {
            startLoading();
            //Bind list salon
            $.ajax({
                type: "POST",
                url: "/GUI/FrontEnd/Customer/Customer_HairMode.aspx/ReturnAllSalon",
                data: '',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var select = $('#ddlSalon');

                    select.append('<option selected="selected" value="0">Chọn Salon</option>');
                    $.each(response.d, function (key, value) {
                        select.append('<option value="' + value.Id + '">' + value.Name + '</option>');
                    });
                    finishLoading();
                },
                failure: function (response) { console.log(response.d); }
            });

            $('#btnTimKiem').click(function () {
                var _SalonId = $("#ddlSalon").val();
                if (_SalonId == "0") {
                    alert("Bạn chưa chọn salon");
                    return;
                }
                var _StylistId = $("#ddlStylist").val();
                
                var _ToDate = $("#txtToDate").val();
                if (_ToDate == "") {
                    alert("Bạn chưa chọn ngày");
                    return;
                }
                var customerClass = $('#ddlCustomerClass').val();
                if (customerClass == "") {
                    alert("Bạn chưa chọn phân loại khách");
                    return;
                }
                var telCustomer = $('#txtTelSearch').val();
                if (telCustomer == "") {
                    telCustomer = null;
                }
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/Customer/Customer_HairMode.aspx/GetBillImage",
                    data: '{_Date: "' + _ToDate + '", _SalonId: ' + _SalonId + ', customerClass: ' + customerClass + ', telCustomer:' + telCustomer + ', _stylistId: ' + _StylistId + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        _ListPreviewImg = [];
                        $.each(response.d, function (key, value) {
                            objPreviewImg = {};
                            objPreviewImg.Id = value.Id;
                            $("#HDF_BillID").val(objPreviewImg.Id);
                            objPreviewImg.BillDate = value.BillDate;
                            objPreviewImg.SalonName = value.SalonName;
                            objPreviewImg.CustomerPhone = value.CustomerPhone;
                            objPreviewImg.CustomerName = value.CustomerName;
                            objPreviewImg.CustomerId = value.CustomerId;
                            objPreviewImg.Img1 = value.Img1;
                            objPreviewImg.Img2 = value.Img2;
                            objPreviewImg.Img3 = value.Img3;
                            objPreviewImg.Img4 = value.Img4;
                            objPreviewImg.Stylist = value.Stylist;
                            _ListPreviewImg.push(objPreviewImg);
                            // Set giá trị mặc định billID;
                            if (key == 0) {
                                setBillID(value.Id);
                            }
                        });

                        if (_ListPreviewImg.length > 0) {
                            SetPreviewInfo(0);
                            $('#hdfIndexPage').val("0");
                            $('#total-index').text(_ListPreviewImg.length);
                            $('#current-index').text('1');
                        }
                        else {
                            $('#pre-id').text("");
                            $('#pre-customerid').text("");
                            $('#pre-date').text("");
                            $('#pre-salon').text("");
                            $("#pre-stylist").text("");
                            $('#pre-img1').attr('src', "https://30shine.com/images/1.jpg");
                            $('#pre-img2').attr('src', "https://30shine.com/images/2.jpg");
                            $('#pre-img3').attr('src', "https://30shine.com/images/3.jpg");
                            $('#pre-img4').attr('src', "https://30shine.com/images/4.jpg");
                            $('#hdfIndexPage').val("0");
                            $('#total-index').text("0");
                            $('#current-index').text('0');
                            $(".item-status").removeClass("active");
                        }
                        finishLoading();
                    },
                    failure: function (response) { console.log(response.d); }
                });

            });

            //Next Click
            $('#btnNext').click(function () {
                var totalIndex = parseInt(_ListPreviewImg.length);
                var index = parseInt($('#hdfIndexPage').val());

                if ((totalIndex > 0) && (index < totalIndex - 1)) {
                    SetPreviewInfo(index + 1);
                    $('#hdfIndexPage').val(index + 1);
                    $('#current-index').text((index + 2));
                }
                cusHairId = "";
            });

            //Pre Click
            $('#btnPreview').click(function () {
                var totalIndex = parseInt(_ListPreviewImg.length);
                var index = parseInt($('#hdfIndexPage').val());

                if ((totalIndex > 0) && (index > 0)) {
                    SetPreviewInfo(index - 1);
                    $('#hdfIndexPage').val(index - 1);
                    $('#current-index').text((index));
                }
                cusHairId = "";
            });

            //============================
            // Datepicker
            //============================
            $('.txtDateTime').datetimepicker({
                dayOfWeekStart: 1,
                lang: 'vi',
                startDate: '2014/10/10',
                format: 'd/m/Y',
                dateonly: true,
                showHour: false,
                showMinute: false,
                timepicker: false,
                scrollMonth: false,
                scrollInput: false,
                onChangeDateTime: function (dp, $input) { }
            });
                        

        });

        function getStylistBySalon(stylistId) {
            var salonId = $("#ddlSalon").val();
            var select = $('#ddlStylist');
            if (salonId != "0") {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/Customer/Customer_HairMode.aspx/ReturnAllStylistBySalon",
                    data: '{_SalonId: ' + salonId + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {

                        select.html('');
                        select.append('<option selected="selected" value="0">Chọn Stylist</option>');
                        $.each(response.d, function (key, value) {
                            select.append('<option value="' + value.Id + '">' + value.Fullname + '</option>');
                        });
                        $('#ddlStylist option').removeAttr('selected').filter('[value=' + stylistId + ']').attr('selected', 'selected');
                        finishLoading();
                    },
                    failure: function (response) { console.log(response.d); }
                });
            } else {
                select.html('');
                select.append('<option selected="selected" value="0">Chọn Stylist</option>');
            }
        }

        function SetPreviewInfo(index) {
            
            $('#pre-id').text(_ListPreviewImg[index].Id);
            $("#pre-stylist").text(_ListPreviewImg[index].Stylist);
            $('#pre-date').text(_ListPreviewImg[index].CustomerPhone);
            $('#pre-salon').text(_ListPreviewImg[index].CustomerName);
            $('#pre-customerid').text(_ListPreviewImg[index].CustomerId);

            $('#pre-img1').attr('src', _ListPreviewImg[index].Img1);
            $('#pre-img2').attr('src', _ListPreviewImg[index].Img2);
            $('#pre-img3').attr('src', _ListPreviewImg[index].Img3);
            $('#pre-img4').attr('src', _ListPreviewImg[index].Img4);
            
            // set giá trị cho billID
            setBillID(_ListPreviewImg[index].Id);
            // reset trạng thái active của item trạng thái
            resetItemHairStyle();
            // bind lại trạng thái
            getHairStyleById(billID);
        }

        // Cập nhật giá trị cho billID
        function setBillID(id) {
            billID = id;
        }

        function bindBillHairStylist(hairStyleId) {
            $(".item-status").removeClass("active");
            if (hairStyleId != null) {
                $(".item-status").removeClass("active");
                $(".item-status[data-id='" + hairStyleId + "']").addClass("active");
            }
        }

        function resetItemHairStyle() {
            $(".item-status").removeClass("active");
        }

        function setActiveOnclick(This) {
            $(".item-status").removeClass("active");
            This.addClass("active");  
        }
       
        // đánh giá trạng thái
        function updateCustomer_HairMode_Bill(This) {
            // set trạng thái active cho button khi click
            setActiveOnclick(This);
            var hairStyleCurrent = ""; 
            for (var i = 0; i < $(".item-status").length; i++) {
                if ($(".item-status").eq(i).hasClass("active")) {
                    hairStyleCurrent = $(".item-status").eq(i).attr("data-id");
                }
            }
            
            if (billID > 0) {
                startLoading();
                if (cusHairId == "" || cusHairId == undefined) {
                    cusHairId = 0;
                }
                $.ajax({
                    url: "/GUI/FrontEnd/Customer/Customer_HairMode.aspx/updateCustomer_HairMode_Bill",
                    type: "post",
                    data: '{cusHairId:' + cusHairId + ', hairStyleId : ' + hairStyleCurrent + ', CustomerId: "' + $('#pre-customerid').text() + '", billID: ' + billID + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var msg = JSON.parse(data.d);
                        if (msg.success) {
                            var index = findIndexByBillID(billID);
                            var totalIndex = parseInt(_ListPreviewImg.length);
                            var index = parseInt($('#hdfIndexPage').val());
                            if ((totalIndex > 0) && (index < totalIndex - 1)) {
                                SetPreviewInfo(index + 1);
                                $('#hdfIndexPage').val(index + 1);
                                $('#current-index').text((index + 2));
                            }
                            cusHairId = "";
                            
                        }
                        finishLoading();
                        
                    },
                    error: function (data) {
                       
                    }
                });
            }
        }

        function findIndexByBillID(billID) {
            var index = null;
            if (_ListPreviewImg != null) {
                for (var i = 0; i < _ListPreviewImg.length; i++) {
                    if (billID == _ListPreviewImg[i].Id) {
                        index = i;
                        break;
                    }
                }
            }
            return index;
        }

        //fn get list HairMode
        function GetListHairMode() {
            $.ajax({
                type: "POST",
                url: "/GUI/FrontEnd/Customer/Customer_HairMode.aspx/ReturnAllHairMode",
                data: '',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var btnList = "";
                    $.each(response.d, function (i, v) {
                        btnList += '<div class="col-xs-3 item-status-wrap">' +
                                '<a data-id="' + v.Id + '" href="javascript:void(0);" onclick="updateCustomer_HairMode_Bill($(this))" class="btn btn-default btn-xs btn-ok item-status btnHairMode">' + v.Title + '</a>' +
                                '</div>';
                    });
                    $(".btnList").append(btnList);
                    var heightArray = [];
                    for (var i = 0; i < $(".btnHairList").find(".btnHairMode").length; i++) {
                        var height = $(".btnHairList").find(".btnHairMode").eq(i).height();
                        heightArray.push(height);
                    }
                    var maxHeight = Math.max.apply(null, heightArray);
                    $(".btnHairList").find(".btn").css("height", maxHeight + 16);
                    finishLoading();
                },
                failure: function (response) { console.log(response.d); }
            });
        }
        GetListHairMode();
       
        //fn get list HairMode
        function getHairStyleById(billID) {
            $.ajax({
                type: "POST",
                url: "/GUI/FrontEnd/Customer/Customer_HairMode.aspx/getHairStyleById",
                data: '{billId:' + billID + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    console.log(response);
                    if (response.d != null) {
                        cusHairId = response.d.Id;
                        hairStyleId = response.d.HairStyleId;
                    }
                    bindBillHairStylist(hairStyleId);
                    hairStyleId = "";
                },
                failure: function (response) { console.log(response.d); }
            });
        }

        /*
       * Mở layer loading
       */
        function startLoading() {
            $(".page-loading").fadeIn();
        }

        /*
        * Đóng layer loading
        */
        function finishLoading() {
            $(".page-loading").fadeOut();
        }
    </script>
</body>
</html>
