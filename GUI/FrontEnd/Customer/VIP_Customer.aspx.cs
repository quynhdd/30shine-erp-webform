﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.CustomClass;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using Project.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Migrations;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace _30shine.GUI.FrontEnd.UICustomer
{
    public partial class VIP_Customer : System.Web.UI.Page
    {
        private string PageID = "KH_VIP";
        protected Paging PAGING = new Paging();
        public CultureInfo culture = new CultureInfo("vi-VN");
        private DateTime timeFrom;
        private DateTime timeTo;
        protected int integer;

        protected bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected string Permission = "";
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                //ContentWrap.Visible = false;
                //NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                bindData();
            }
            else
            {

            }
            RemoveLoading();
        }

        

        [WebMethod]
        public static Customer GetCustomerListByPhone(string _filter)
        {
            using (var db = new Solution_30shineEntities())
            {
                var sql = @"select * from Customer where IsDelete != 1 and Phone = '" + _filter + "'";
                var list = db.Database.SqlQuery<Customer>(sql).FirstOrDefault();
                return list;
            }
        }

        [WebMethod]
        public static Customer GetCustomerByPhone(string Phone)
        {
            using (var db = new Solution_30shineEntities())
            {
                 var objCus = db.Customers.Where(a => a.Phone == Phone && a.IsDelete == 0).OrderByDescending(a => a.Id).FirstOrDefault();
                return objCus;
            }
        }

        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            bindData();
            RemoveLoading();
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        private void bindData()
        {
            using (var db = new Solution_30shineEntities())
            {
                try
                {
                    var sql = @"SELECT A.*, B.Fullname as CustomerName , B.Phone, C.TypeName as TypeName, D.DiscountServices, D.DiscountCosmetic
                                from SpecialCustomer A
                                left join Customer B on A.CustomerId = B.Id
                                left join CustomerType C on A.CustomerTypeId = C.Id
                                left join SpecialCusDetail D on D.SpecialCusId = A.Id
                                where A.IsDelete = 0
                                and A.CustomerTypeId = 2";
                    var data = db.Database.SqlQuery<SpecialCustomer_View>(sql).ToList();
                    Bind_Paging(data.Count);
                    RptCustomer.DataSource = data.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                    RptCustomer.DataBind();
                }
                catch { }
            }
        }

        [WebMethod]
        public static object InsertOrUpdateCustomer(int CusId, int TypeCusSpecail_ID, int DiscountCosmetics, int DiscountService)
        {
            using (var db = new Solution_30shineEntities())
            {
                var msg = new Library.Class.cls_message();
                //var serialize = new JavaScriptSerializer();
                var obj = new SpecialCusDetail();
                var obj1 = new SpecialCustomer();
                var checkCus = db.SpecialCustomers.FirstOrDefault(a => a.IsDelete == false && a.CustomerId == CusId && a.CustomerTypeId == 2);
                if(checkCus == null)
                {
                    obj1 = new SpecialCustomer();
                    obj1.CustomerId = CusId;
                    obj1.CustomerTypeId = TypeCusSpecail_ID;
                    obj1.IsDelete = false;
                    obj1.IsSMS = true;
                    obj1.SMSStatus = 0;
                    obj1.Publish = true;
                    db.SpecialCustomers.Add(obj1);
                    db.SaveChanges();

                    obj = new SpecialCusDetail();
                    obj.SpecialCusId = obj1.Id;
                    obj.DiscountCosmetic = DiscountCosmetics;
                    obj.DiscountServices = DiscountService;
                    obj.IsDelete = false;
                    db.SpecialCusDetails.Add(obj);
                    db.SaveChanges();
                    msg.status = "CusNew";
                    msg.success = true;
                    msg.data = obj1;
                }
                else
                {
                    msg.status= "Cusold";
                    msg.success = false;

                }
                return msg;
            }
                
        }

        [WebMethod]
        public static string DeleteCustomer(int _idspec)
        {
            var Message = new Library.Class.cls_message();
            var serialize = new JavaScriptSerializer();
            var error = 0;
            var db = new Solution_30shineEntities();
            var RECORD = db.SpecialCustomers.FirstOrDefault(w => w.Id == _idspec);
            if (RECORD != null)
            {
                RECORD.IsDelete = true;
                db.SpecialCustomers.AddOrUpdate(RECORD);
                error += db.SaveChanges() > 0 ? 0 : 1;
                if (error == 0)
                {
                    var sql = "select * from SpecialCusDetail where IsDelete = 0 and SpecialCusId =" + _idspec;
                    var list = db.Database.SqlQuery<SpecialCusDetail>(sql).ToList();
                    foreach (var item in list)
                    {
                        item.IsDelete = true;
                        db.SpecialCusDetails.AddOrUpdate(item);
                        error += db.SaveChanges() > 0 ? 0 : 1;
                    }
                    Message.success = true;
                    Message.message = "Xóa thành công.";
                }
                else
                {
                    Message.success = false;
                    Message.message = "Xóa thất bại.";
                }
            }
            return serialize.Serialize(Message);
        }

        [WebMethod]
        public static SpecialCustomer_GetByID GetCustomerById(int _idCus)
        {
            using (var db = new Solution_30shineEntities())
            {
                var sql = @"select A.*, B.Fullname as CustomerName , B.Phone from SpecialCustomer A  left join Customer B on A.CustomerId = B.Id  where A.IsDelete = 0 and A.Id=" + _idCus;
                return db.Database.SqlQuery<SpecialCustomer_GetByID>(sql).SingleOrDefault();
            }
        }

        
        
        public class SpecialCustomer_View : SpecialCustomer
        {
            public string CustomerName { get; set; }
            public string Phone { get; set; }
            public string TypeName { get; set; }
            public double DiscountServices { get; set; }
            public double DiscountCosmetic { get; set; }

        }
        public class SpecialCustomer_Insert
        {
            public SpecialCustomer SpecialCustomer { get; set; }
            public SpecialCusDetail SpecialCusDetail { get; set; }
        }

        public class SpecialCustomer_GetByID : SpecialCustomer
        {
            public string CustomerName { get; set; }
            public string Phone { get; set; }
        }
        public class SpecialCustomerDetail_GetByID : SpecialCusDetail
        {
            public string SalonName { get; set; }
            public string StaffName { get; set; }
            public string Reason { get; set; }
        }
    }
    
}