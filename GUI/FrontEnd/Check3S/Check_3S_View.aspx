﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Check_3S_View.aspx.cs" Inherits="_30shine.GUI.FrontEnd.Check3S.Check_3S_View" %>



<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .table-listing .uv-avatar {
                width: 120px;
            }
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý check 3S</li>
                        <%--<li class="li-listing"><a href="/dat-hang-kho/kho/danh-sach">Danh sách</a></li>--%>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report be-report-timekeeping">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
                <!-- System Message -->
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <!-- END System Message -->
                <div class="row">
                    <div class="filter-item">
                        <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="time-wp">
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Từ ngày" Style="margin-left: 10px!important"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ValidateTime" ControlToValidate="TxtDateTimeFrom" runat="server" CssClass="fb-cover-error" Text="Vui lòng chọn thời gian!"></asp:RequiredFieldValidator>
                        </div>
                        <strong class="st-head" style="margin-left: 10px;">
                            <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                        </strong>
                        <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeTo" placeholder="Đến ngày" Style="margin-left: 10px!important"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                        <br />
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlSalon" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px;"></asp:DropDownList>
                    </div>
                    <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <a href="javascript:void(0);" onclick="location.href=location.href" class="st-head btn-viewdata">Reset Filter</a>

                </div>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>Danh sách check 3S</strong>
                </div>
                <!-- Row Table Filter -->
                <div class="table-func-panel" style="margin-top: -33px;">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="row table-wp">
                            <table class="table-add table-listing">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th class="hidden">ID</th>
                                        <th>Người check</th>
                                        <th>Note</th>
                                        <th>Ngày check</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptCheck3S" runat="server">
                                        <ItemTemplate>
                                            <tr data-toggle="modal" data-target="#myModal" data-id="<%# Eval("Id") %>" onclick="GetImagesCheck3S($(this));">
                                            <%--<tr onclick="GetImagesCheck3S();">--%>
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td class="hidden"><%# Eval("Id") %></td>
                                                <td><%# Eval("UserName") %></td>
                                                <td> <%# Eval("Note") %></td>
                                                <td class="map-edit">
                                                    <%# Eval("CreatedTime") != null ? String.Format("{0:dd/MM/yyyy}", Eval("CreatedTime")) + " - " + String.Format("{0:HH:mm}", Eval("CreatedTime")) : "" %>
                                                   
                                                    <div class="edit-wp">
                                                        <%--<a href="javascript:void(0);" class="elm edit-btn"  title="Sửa" %>"></a>--%>
                                                        <a class="elm del-btn del_Bill" href="javascript://" title="Xóa" onclick="del($(this), <%# Eval("Id") %>);"></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />

                <!-- Hidden Field-->
                <asp:HiddenField ID="HDF_TypeStaff" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_Staff" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_ExcelType" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_ExportType" ClientIDMode="Static" runat="server" />
                <!-- END Hidden Field-->
            </div>
            <%-- END Listing --%>
        </div>

        <!-- Pop up-->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog" id="modalDialog">
            <!-- Modal content-->
            <div class="modal-content" id="modalContent">
                <div class="modal-header" id="modalHeader">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Hình ảnh chi tiết</h4>
                </div>
                <div class="modal-body">
                    <div id="myCarousel" class="carousel" data-ride="carousel" data-interval="false">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                           
                        </ol>
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox" id="canvasWrap">
                            
                        </div>
                        

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev" id="prev-Slide">
                            <%--<i class="fa fa-arrow-left" aria-hidden="true"></i>--%>
                            <span class="fa fa-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next" id="next-Slide">
                            <span class="fa fa-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                            <%--<i class="fa fa-arrow-right" aria-hidden="true"></i>--%>
                        </a>
                    </div>
                    <br />
                        <div class="row">
                            <div class="col-xs-2">Ghi chú</div>
                            <div class="col-xs-10">
                                <textarea id="txtNote" rows="3" class="form-control"></textarea>
                            </div>
                        </div>
                </div>
                <%--<div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-9"></div>
                        <div class="col-xs-3">
                           <i class="fa fa-times-circle" aria-hidden="true" data-dismiss="modal" title="Đóng"></i>                         
                        </div>
                    </div>
                </div>--%>
            </div>

        </div>
    </div>   
        <!-- Pop up-->

        <link href="/Assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <script src="/Assets/js/jquery.mCustomScrollbar.js"></script>
        <script src="/Assets/js/bootstrap/bootstrap-modal.js"></script>

        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>

        <script type="text/ecmascript">
            jQuery(document).ready(function () {
               
                // Add active menu
                //$("#glbDatHangNoiBo").addClass("active");
                //$("#glbDatHangNoiBo_ChoKho").addClass("active");
                //$("#subMenu li.li-listing").addClass("active");

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });

               

            });

            function showMsgSystem(msg, status) {
                $("#MsgSystem").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);
                setTimeout(function () {
                    $("#MsgSystem").fadeTo("slow", 0, function () {
                        $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 1);
                    });
                }, 5000);
            }

           

            function EBSelect_ShowBox(Dom) {
                EBSelect_HideBox();
                Dom.show();
            }

            function EBSelect_HideBox() {
                $(".eb-select-data").hide();
            }
            
            function GetImagesCheck3S(THIS) {
                var id = $(THIS).attr("data-id");
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/Check3S/Check_3S_View.aspx/GetImagesCheck3S",
                    data: '{id: ' + id + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        $("#myCarousel .carousel-indicators").children().remove();
                        $("#canvasWrap").children().remove();
                        var images = response.d.Images;
                        $("#txtNote").val(response.d.Note);
                        if (images != null) {
                            var array = images.split(",");
                            var divAppend = "";
                            var liAppend ="";
                            var dem = 0;
                            $.each(array, function (i, v) {
                                console.log(v);
                                dem++;
                                divAppend += '<div class="item divImage" data-stt="' + dem + '">' +
                                    '<img src="' + v + '" alt="" /></div>';
                                liAppend += '<li class="liSlide" data-target="#myCarousel" data-slide-to="'+ dem +'"></li>';
                            });
                            $("#myCarousel .carousel-indicators").append(liAppend);
                            $('.liSlide[data-slide-to="1"]').addClass("active");
                            $("#canvasWrap").append(divAppend);
                            $('.divImage[data-stt="1"]').addClass("active");
                        }
                        
                    },
                    failure: function (response) { console.log(response.d); }
                });
            }


            function del(This, code, e) {

                var code = code || null,
                    Row = This;
                if (!code) return false;

                // show EBPopup
                $(".confirm-yn").openEBPopup();
                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa ?");

                $("#EBPopup .yn-yes").bind("click", function () {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/FrontEnd/Check3S/Check_3S_View.aspx/deleteCheck3S",
                        data: '{Code : ' + code + '}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            if (response.d == true) {
                                delSuccess();
                                Row.remove();
                                setTimeout(function () {
                                    $("#ViewData").click();
                                }, 1000);
                            } else {
                                delFailed();
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    });
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
                if (!e) var e = window.event;
                e.cancelBubble = true;
                if (e.stopPropagation) e.stopPropagation();
            }
        </script>

    </asp:Panel>
</asp:Content>
