﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="History_Check3S.aspx.cs" Inherits="_30shine.GUI.FrontEnd.Check3S.History_Check3S" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <title>Lịch sử check3s</title>
    <script src="/Assets/js/jquery.v1.11.1.js"></script>
    <link href="/Assets/css/bootstrap/bootstrap.min.css" rel="stylesheet" />
    <link href="/Assets/css/font.css" rel="stylesheet" />
    <link href="/Assets/css/csvc.css" rel="stylesheet" />
</head>
<body>
    <div class="wp">
        <div class="preview-img">
            <div class="heading-title">Lịch sử check3s</div>
            <div class="top-filter">
                <div>
                    <select name="ddlSalon" id="ddlSalon" class="form-control">                
                    </select>
                </div>            
                <div>
                    <input runat="server" name="txtToDate" type="text" value="" id="txtToDate" class="form-control txtDateTime" placeholder="Ngày ..." />
                    <input type="submit" id="btnTimKiem" class="form-control btn-default" value="Tìm" />
                </div>
            </div>
            <div class="top4-img">
                <div class="container">
                    <div class="row">
                        <div class="history-check3s-item">
                            <div><strong id="lblCateName"></strong></div>
                            <div><strong> - Salon:</strong> <label id="lblSalon"></label></div>
                            <div><strong> - Ngày tạo:</strong> <label id="lblNgayTao"></label></div>
                            <div><strong> - Lần:</strong> <label id="lblSoLan"></label></div>
                            <div><strong> - Mô tả:</strong> <label id="lblMoTa"></label></div>
                            <div><strong> - Ảnh:</strong></div>
                            <div>
                                <img class="img-bug" src="" alt="" /></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="img-nav">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-4 col-sm-4">
                            <input type="submit" id="btnPreview" class="form-control btn-default" value="Trước" />
                        </div>
                        <div class="col-xs-4 col-sm-4">
                            &nbsp(Đang xem check3s <strong id="current-index">0</strong>/<strong id="total-index">0</strong>)&nbsp
                        </div>
                        <div class="col-xs-4 col-sm-4">
                            <input type="submit" id="btnNext" class="form-control btn-default" value="Sau" />
                        </div>
                    </div>
                </div>

                
               <%-- <a id="btnPreview" href="javascript://">Trước</a>
                
                <a id="btnNext" href="javascript://">Sau</a>--%>
                
            </div>
            <input type="hidden" value="0" id="hdfIndexPage"/>
        </div>
    </div>
    <style>
        .img-bug { max-width: 100%;}
        .preview-img { background: #fff; }
        .heading-title { background: #000; padding: 10px; text-align: center; color: #fff; text-transform: uppercase; font-size: 16px; }
        .top-filter .form-control { margin-bottom: 5px;}
        .top-filter { padding: 5px; border-bottom: 1px solid #f2f2f2;}
        .top-info { padding: 5px; border-bottom: 1px solid #f2f2f2;}
        .top-info .bill-info { text-align: center;}
        .top-info .salon-name { text-align: center; padding-top: 4px;}
        .top-info strong { font-weight: bold;}


        .top4-img { padding: 5px; border-bottom: 1px solid #f2f2f2;}
        .top4-img .img-item { background: #808080; padding: 10px; text-align: center;  border-right: 1px solid #fff;}
        /*.top4-img .img-item:first-child { border-right: 1px solid #fff;}*/
        .top4-img .img-item img { max-width: 100%;}
        .top4-img .col1 { border-bottom: 1px solid #fff; width: 100%; float: left;}
        .top4-img .col2 { width: 100%; float: left;}
        .img-nav { padding: 5px; border-bottom: 1px solid #f2f2f2; text-align: center;}
        /*#btnNext, #btnPreview { font-size: 20px;}*/
        .btn-default { background-color: #e6e6e6;}
    </style>

    <script>

        var _ListCheck3S = [];
        var objCheck3S = {};

        $(document).ready(function () {
            //Bind list salon
            $.ajax({
                type: "POST",
                url: "/GUI/FrontEnd/PreviewImage/Preview_Random_Salon_Images.aspx/ReturnAllSalon",
                data: '',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var select = $('#ddlSalon');

                    select.append('<option selected="selected" value="0">Chọn Salon</option>');
                    $.each(response.d, function (key, value) {
                        select.append('<option value="' + value.Id + '">' + value.Name + '</option>');
                    });
                },
                failure: function (response) { console.log(response.d); }
            });


            $('#btnTimKiem').click(function () {
                var _SalonId = $("#ddlSalon").val();
                //if (_SalonId == "0") {
                //    alert("Bạn chưa chọn salon");
                //    return;
                //}

                var _ToDate = $("#txtToDate").val();

                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/Check3S/History_Check3S.aspx/History_CheckList3S",
                    data: '{_SalonId: ' + _SalonId + ',_DateTime: "' + _ToDate + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        _ListCheck3S = [];

                        $.each(response.d, function (key, value) {
                            objCheck3S = {};
                            objCheck3S.Id = value.Id;
                            objCheck3S.Des = value.Des;
                            objCheck3S.Img = value.Img;
                            objCheck3S.Time = value.Time;
                            objCheck3S.CreatedDate = value.CreatedDate;
                            objCheck3S.Icon = value.Icon;
                            objCheck3S.CateName = value.CateName;
                            objCheck3S.SalonName = value.SalonName;
                            _ListCheck3S.push(objCheck3S);
                        });

                        //console.log(_ListCheck3S);
                        if (_ListCheck3S.length > 0) {
                            SetPreviewInfo(0);
                            $('#hdfIndexPage').val("0");
                            $('#total-index').text(_ListCheck3S.length);
                            $('#current-index').text('1');
                        }
                    },
                    failure: function (response) { console.log(response.d); }
                });

            });



            //Next Click
            $('#btnNext').click(function () {
                var totalIndex = parseInt(_ListCheck3S.length);
                var index = parseInt($('#hdfIndexPage').val());



                if ((totalIndex > 0) && (index < totalIndex - 1)) {
                    SetPreviewInfo(index + 1);
                    $('#hdfIndexPage').val(index + 1);
                    $('#current-index').text((index + 2));
                }
            });

            //Next Click
            $('#btnPreview').click(function () {
                var totalIndex = parseInt(_ListCheck3S.length);
                var index = parseInt($('#hdfIndexPage').val());

                if ((totalIndex > 0) && (index > 0)) {
                    SetPreviewInfo(index - 1);
                    $('#hdfIndexPage').val(index - 1);
                    $('#current-index').text((index));
                }
            });



            //============================
            // Datepicker
            //============================
            $('.txtDateTime').datetimepicker({
                dayOfWeekStart: 1,
                lang: 'vi',
                startDate: '2014/10/10',
                format: 'd/m/Y',
                dateonly: true,
                showHour: false,
                showMinute: false,
                timepicker: false,
                scrollMonth: false,
                scrollInput: false,
                onChangeDateTime: function (dp, $input) { }
            });
        });

        function SetPreviewInfo(index) {
            if (_ListCheck3S.length > 0) {
                $('#lblCateName').text(_ListCheck3S[index].CateName);
                $('#lblSalon').text(_ListCheck3S[index].SalonName);
                $('#lblNgayTao').text(_ListCheck3S[index].CreatedDate);
                $('#lblSoLan').text(_ListCheck3S[index].Time);
                $('#lblMoTa').text(_ListCheck3S[index].Des);
                if (_ListCheck3S[index].Des != null)
                    $('.img-bug').attr('src', _ListCheck3S[index].Img);
            }
        }
    </script>
    <link href="/Assets/css/jquery.datetimepicker.css?v=2" rel="stylesheet" />
    <script src="/Assets/js/jquery.datetimepicker.js"></script>
</body>
</html>