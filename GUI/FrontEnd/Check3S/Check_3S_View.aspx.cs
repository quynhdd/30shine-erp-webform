﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.FrontEnd.Check3S
{
    public partial class Check_3S_View : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        public CultureInfo culture = new CultureInfo("vi-VN");
        private DateTime timeFrom;
        private DateTime timeTo;
        protected int salonId;
        private int integer;

        private string PageID = "CL_CHECK3S";
        protected bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected bool Perm_ShowElement = false;
        protected string Permission = "";
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ShowElement = permissionModel.GetActionByActionNameAndPageId("Perm_ShowElement", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_ShowElement = permissionModel.CheckPermisionByAction("Perm_ShowElement", pageId, staffId);
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();

            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                salonId = Convert.ToInt32(Session["SalonId"]);
                TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Today);
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
                bindData();
            }
            else
            {
                //Exc_Filter();
            }
            RemoveLoading();
        }

        private void bindData()
        {
            using (var db = new Solution_30shineEntities())
            {
                try
                {
                    if (TxtDateTimeFrom.Text != "")
                    {
                        timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                        if (TxtDateTimeTo.Text != "")
                        {
                            timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                        }
                        else
                        {
                            timeTo = timeFrom;
                        }
                        salonId = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                        var query = @"select A.*, B.Fullname as UserName from KCS_Check3S A left join Staff B on A.UserId = B.Id and B.IsDelete != 1 and B.Active = 1
                                     where  A.IsDelete = 0 and A.SalonId = " + salonId + "  and A.[CreatedTime] between '" + timeFrom + "' and '"+ timeTo.AddDays(1) + "'";
                        var data = db.Database.SqlQuery<KCS_Check3S_View>(query).ToList();
                        Bind_Paging(data.Count);
                        rptCheck3S.DataSource = data.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                        rptCheck3S.DataBind();
                    }
                }
                catch { }
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            bindData();
            RemoveLoading();
        }

        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

       
        [WebMethod]
        public static ImagesCheck3S GetImagesCheck3S(int id)
        {
            using (var db = new Solution_30shineEntities())
            {
                var sql = @"select Images, Note from KCS_Check3S where Id=" + id;
                var list = db.Database.SqlQuery<ImagesCheck3S>(sql).SingleOrDefault();
                return list;
            }
        }

        [WebMethod]
        public static bool deleteCheck3S(int Code)
        {
            using (var db = new Solution_30shineEntities())
            {
                var OBJ = db.KCS_Check3S.SingleOrDefault(s => s.Id == Code && s.IsDelete != true);
               
                if (OBJ != null)
                {
                    OBJ.IsDelete = true;
                    OBJ.ModifiedTime = DateTime.Now;
                    db.KCS_Check3S.AddOrUpdate(OBJ);
                    db.SaveChanges();
                }
                return true;
            }
        }


        public class ImagesCheck3S
        {
            public string Images { get; set; }
            public string Note { get; set; }
        }

        public class KCS_Check3S_View : KCS_Check3S
        {
            public string UserName { get; set; }
        }
    }
}