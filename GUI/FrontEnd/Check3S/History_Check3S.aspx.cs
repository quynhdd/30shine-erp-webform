﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Services;

namespace _30shine.GUI.FrontEnd.Check3S
{
    public partial class History_Check3S : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static List<cls_checklist_3s> History_CheckList3S(int _SalonId, string _DateTime)
        {
            using (var db = new Solution_30shineEntities())
            {


                string strDatetime = "";
                if (!string.IsNullOrEmpty(_DateTime))
                {
                    CultureInfo culture = new CultureInfo("vi-VN");
                    strDatetime = " and a.DateTime between '" + Convert.ToDateTime(_DateTime, culture).ToShortDateString() + "' and '" + Convert.ToDateTime(_DateTime, culture).ToShortDateString() + " 23:59:59.999'";
                }

                string strSalon = "";
                if (_SalonId != 0)
                {
                    strSalon = " and a.SalonId = " + _SalonId;
                }

                var sql = @"select a.Id, a.Comment as Des, a.Image as Img, a.Time, CONVERT(VARCHAR(10),a.DateTime,103) AS CreatedDate, b.Name as CateName, b.Icon, c.Name as SalonName from [ERP.Check3S]  a left join [ERP.ItemCheck] b on a.ItemId = b.Id left join Tbl_Salon c  on a.SalonId = c.Id Where ItemId != 0 " + strSalon + strDatetime + " order by a.DateTime asc";

                var lst = db.Database.SqlQuery<cls_checklist_3s>(sql).ToList();

                return lst;
            }
        }
    }

    public class cls_checklist_3s
    {
        public int Id { get; set; }
        public string Des { get; set; }
        public string Img { get; set; }
        public int Time { get; set; }
        public string CreatedDate { get; set; }
        public string CateName { get; set; }
        public string Icon { get; set; }
        public string SalonName { get; set; }
    }
}