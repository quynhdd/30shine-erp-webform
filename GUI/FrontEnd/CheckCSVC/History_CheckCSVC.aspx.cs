﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Services;

namespace _30shine.GUI.FrontEnd.CheckCSVC
{
    public partial class History_CheckCSVC : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        [WebMethod]
        public static List<cls_checklist_csvc> History_CheckListCSVC(int _SalonId, string _DateTime)
        {
            using (var db = new Solution_30shineEntities())
            {


                string strDatetime = "";
                if (!string.IsNullOrEmpty(_DateTime))
                {
                    CultureInfo culture = new CultureInfo("vi-VN");
                    strDatetime = " and a.DateTime between '" + Convert.ToDateTime(_DateTime, culture).ToShortDateString() + "' and '" + Convert.ToDateTime(_DateTime, culture).ToShortDateString() + " 23:59:59.999'";
                }

                string strSalon = "";
                if (_SalonId != 0)
                {
                    strSalon = " and a.SalonId = " + _SalonId;
                }

                var sql = @"select a.Id, a.Comment as Des, a.Image as Img, CONVERT(VARCHAR(10),a.DateTime,103) AS CreatedDate,CONVERT(VARCHAR(10),a.DoingCreateDate,103) AS DoingDate, CONVERT(VARCHAR(10),a.ConfirmCreateDate,103) AS ConfirmDate,b.Name as CateName, b.Icon, c.Name as SalonName from [ERP.CheckCSVC] a left join [ERP.ItemCheck] b on a.ItemId = b.Id left join Tbl_Salon c on a.SalonId = c.Id where a.Status = 3 " + strSalon + strDatetime + " order by a.DateTime asc";

                var lst = db.Database.SqlQuery<cls_checklist_csvc>(sql).ToList();

                return lst;
            }
        }
    }

    public class cls_checklist_csvc
    {
        public int Id { get; set; }
        public string Des { get; set; }
        public string Img { get; set; }
        public string CreatedDate { get; set; }
        public string DoingDate { get; set; }
        public string ConfirmDate { get; set; }
        public string CateName { get; set; }
        public string Icon { get; set; }
        public string SalonName { get; set; }
    }
}