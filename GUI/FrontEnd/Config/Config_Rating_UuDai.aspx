﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Config_Rating_UuDai.aspx.cs" Inherits="_30shine.GUI.FrontEnd.Config.Config_Rating_UuDai" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .edit { margin: 8px; width: 15px; }
            .del { margin: 8px; width: 15px; }
            .del:hover { color: red; }
            .edit:hover { color: seagreen; }
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Cấu hình xếp hạng ưu đãi &nbsp;&#187; </li>
                        <li class="li-listing" style="cursor: pointer"><a onclick="show()">Thêm mới</a></li>
                        <li class="li-listing" style="cursor: pointer"><a href="/admin/membership/quan-ly-uu-dai">Quản lý ưu đãi</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing be-report woktime-listing">
            <%-- Listing --%>

            <div class="wp960 content-wp">
                <div class="row" id="ql" hidden="hidden">
                    <ul class="form-group">
                        <li class="list-group-item style-budget">
                            <div class="col-md-12 col-xs-12" style="margin-top: 10px">
                                <label>Xếp hạng</label>
                                <asp:DropDownList CssClass="form-control" ID="ddlRating" Width="30%"
                                    ClientIDMode="Static" runat="server">
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-12 col-xs-12" style="margin-top: 10px">
                                <label>Ưu đãi</label>
                                <asp:DropDownList CssClass="form-control" ID="ddlUuDai" Width="30%"
                                    ClientIDMode="Static" runat="server">
                                </asp:DropDownList>
                            </div>
                            <a class="btn btn-primary" style="margin-left: 128px; margin-right: 10px; width: 100px; margin-top: 10px" onclick="SaveConfig($(this))">Lưu lại</a>
                            <a class="btn btn-default" style="margin-left: 10px; margin-right: 10px; width: 100px; margin-top: 10px" onclick="huy()">Hủy</a>
                        </li>
                    </ul>
                </div>
            </div>


            <div class="col-md-12 col-xs-12">
                <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Danh sách xếp hạng và ưu đãi</strong>
                <table class="table-add table-listing">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Xếp hạng</th>
                            <th>ƯU đãi</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <%if (ListRT.Count > 0)
                            { %>
                        <%int i = 1; foreach (var v in ListRT)
                            { %>
                        <tr class="remove<%= v.Id %>">
                            <td><%= i++ %></td>
                            <td><%=v.Rating %></td>
                            <td><%= v.UuDai %></td>
                            <td class="map-edit">
                                <div class="edit-wp">
                                    <a onclick="EditConfig($(this) ,<%=v.Id %>)" data-id="<%=v.Id %>"
                                        data-rating="<%= v.RatingId %>" data-uudai="<%= v.UuDaiId %>" data-stt="<%= i %>"
                                        id="sua" class="elm edit-btn"></a>
                                    <a onclick="DeletelConfig($(this),<%=v.Id %>)" id="xoa" class="elm del-btn"></a>
                                </div>
                            </td>
                        </tr>
                        <%} %>
                        <%} %>
                    </tbody>
                </table>
            </div>
        </div>
    </asp:Panel>
    <style>
        .style-budget label { float: left; width: 100px; line-height: 30px; }
        .style-budget input, textarea { width: 300px !important; float: left; margin-left: 12px; }
    </style>
    <script>
        var id = 0;

        function SaveConfig(This) {
            var Id = parseInt(id);
            Id = !isNaN(Id) ? Id : 0;

            var ratingId = $("#ddlRating").val();
            ratingId = !isNaN(ratingId) ? ratingId : 0;
            if (ratingId == 0) {
                swal({
                    title: "",
                    type: "error",
                    text: "Vui lòng chọn xếp hạng",
                    timer: 2000,
                    showConfirmButton: false
                });
                return;
            }

            var uuDaiId = $("#ddlUuDai").val();
            uuDaiId = !isNaN(uuDaiId) ? uuDaiId : 0;
            if (uuDaiId == 0) {
                swal({
                    title: "",
                    type: "error",
                    text: "Vui lòng chọn ưu đãi",
                    timer: 2000,
                    showConfirmButton: false
                });
                return;
            }

            console.log(Id, ratingId, uuDaiId);
            var tdla = parseInt($('.table-add tbody tr:last td:first').text());

            addLoading();
            $.ajax({
                type: "POST",
                url: "/GUI/FrontEnd/Config/Config_Rating_UuDai.aspx/SaveData",
                data: '{Id:"' + Id + '", RatingId : "' + ratingId + '", UuDaiId:"' + uuDaiId + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.d != null) {
                        console.log(response.d.data);
                        if (Id == 0) {
                            $(".table-add tbody").append($(AddTr(response.d.data.Id, response.d.data.Rating, response.d.data.UuDai, tdla, response.d.data.RatingId, response.d.data.UuDaiId)));
                            swal({
                                title: "",
                                type: "success",
                                text: response.d.message,
                                timer: 2000,
                                showConfirmButton: false
                            });
                        } else {
                            window.location = "/admin/cau-hinh-xep-hang-uu-dai.html";
                        }
                        removeLoading();


                        $("#ql").hide();
                    }
                },
                failure: function (response) { alert(response.d); }
            });

        }

        function AddTr(Id, rating, uudai, tdla, RatingId, UuDaiId) {
            return '<tr class="remove' + Id + '">' +
                        '<td>' + (tdla + 1) + '</td>' +
                        '<td>' + rating + '</td>' +
                        '<td>' + uudai + '</td>' +
                        '<td class="map-edit">' +
                            '<div class="edit-wp">' +
                                '<a onclick="EditConfig($(this) ,' + Id + ')" data-id="' + Id + '" data-rating="' + RatingId + '" data-uudai="' + UuDaiId + '" id="sua" class="elm edit-btn"></a>' +
                                '<a onclick="DeletelConfig($(this),' + Id + ')" id="xoa" class="elm del-btn"></a>' +
                             '</div>' +
                         '</td>' +
                     '</tr>';
        }

        function EditConfig(This, Id) {
            $("#ql").show();
            id = Id;
            $("#ddlRating").val(This.attr("data-rating"));
            $("#ddlUuDai").val(This.attr("data-uudai"));
        }

        function DeletelConfig(This, Id) {
            var id = Id;
            console.log(id);
            swal({
                title: "",
                text: "Bạn có chắc chắn muốn xóa hay không?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                closeOnConfirm: false
            },
            function () {
                addLoading();
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/Config/Config_Rating_UuDai.aspx/DeleteData",
                    data: '{Id:' + id + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        console.log(response.d.data);
                        removeLoading();
                        $(".remove" + id).remove();
                        swal({
                            title: "",
                            type: "success",
                            text: response.d.message,
                            timer: 2000,
                            showConfirmButton: false
                        });
                    },
                    failure: function (response) { alert(response.d); }
                });
            });
        }

        function show() {
            $("#ql").show();
            id = 0;
            $("#ddlRating").val("0");
            $("#ddlUuDai").val("0");
        }

        function huy() {
            $("#ql").hide();
            id = 0;
            $("#ddlRating").val("0");
            $("#ddlUuDai").val("0");
        }
    </script>
</asp:Content>
