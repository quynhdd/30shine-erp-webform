﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Config_Rank.aspx.cs" Inherits="_30shine.GUI.FrontEnd.Config.Config_Rank" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Quản lý ưu đãi</title>
    <script src="/Assets/js/sweetalert.min.js"></script>
    <link href="/Assets/css/sweetalert.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .edit { margin: 8px; width: 15px; }
            .del { margin: 8px; width: 15px; }
            .del:hover { color: red; }
            .edit:hover { color: seagreen; }
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Cấu hình ưu đãi &nbsp;&#187; </li>
                        <li class="li-listing" style="cursor: pointer"><a onclick="show()">Thêm mới</a></li>
                        <li class="li-listing" style="cursor: pointer"><a href="/admin/membership/cau-hinh-uu-dai-xep-hang">Quản lý cấu hình Xếp hạng - Ưu đãi</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing be-report woktime-listing">
            <%-- Listing --%>

            <div class="wp960 content-wp">
                <div class="row" id="ql" hidden="hidden">
                    <ul class="form-group">
                        <li class="list-group-item style-budget">
                            <div class="col-md-12 col-xs-12" style="margin-top: 10px">
                                <label>Tên gói ưu đãi</label>
                                <asp:TextBox CssClass="form-control" ID="txtTitle" placeholder="Nhập tên ưu đãi"
                                    ClientIDMode="Static" runat="server"></asp:TextBox>
                            </div>
                            <div class="col-md-12 col-xs-12" style="margin-top: 10px">
                                <label>Mô tả</label>
                                <asp:TextBox runat="server" CssClass="form-control" ID="txtGhiChu" TextMode="MultiLine" ClientIDMode="Static" placeholder="Nhập mô tả"></asp:TextBox>
                            </div>
                            <a class="btn btn-primary" style="margin-left: 128px; margin-right: 10px; width: 100px; margin-top: 10px" onclick="SaveConfig($(this))">Lưu lại</a>
                            <a class="btn btn-default" style="margin-left: 10px; margin-right: 10px; width: 100px; margin-top: 10px" onclick="huy()">Hủy</a>
                        </li>
                    </ul>
                </div>
            </div>


            <div class="col-md-12 col-xs-12">
                <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Danh sách cấu hình ưu đãi</strong>
                <table class="table-add table-listing">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tiêu đề</th>
                            <th>Mô tả</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <%if (obj.Count > 0)
                            { %>
                        <%int i = 1; foreach (var v in obj)
                            { %>
                        <tr class="remove<%= v.Id %>">
                            <td><%= i++ %></td>
                            <td><%=v.Name %></td>
                            <td><%= v.Description %></td>
                            <td class="map-edit">
                                <div class="edit-wp">
                                    <a onclick="EditConfig($(this) ,<%=v.Id %>)" data-id="<%=v.Id %>"
                                        data-title="<%= v.Name %>" data-mota="<%= v.Description %>"
                                        id="sua" class="elm edit-btn"></a>
                                    <a onclick="DeletelConfig($(this),<%=v.Id %>)" id="xoa" class="elm del-btn"></a>
                                </div>
                            </td>
                        </tr>
                        <%} %>
                        <%} %>
                    </tbody>
                </table>
            </div>
        </div>
    </asp:Panel>
    <style>
        .style-budget label { float: left; width: 100px; line-height: 30px; }
        .style-budget input, textarea { width: 300px !important; float: left; margin-left: 12px; }
    </style>
    <script>
        var id = 0;

        function SaveConfig(This) {
            var Id = parseInt(id);
            Id = !isNaN(Id) ? Id : 0;
            var Name = $("#txtTitle").val();
            if (Name == '')
            {
                swal({
                    title: "",
                    type: "error",
                    text: "Không để trống tên",
                    timer: 2000,
                    showConfirmButton: false
                });
                $("#txtTitle").focus();
                return;
            }
            var mota = $("#txtGhiChu").val();
            var tdla = parseInt($('.table-add tbody tr:last td:first').text());
            addLoading();
            
            $.ajax({
                type: "POST",
                url: "/GUI/FrontEnd/Config/Config_Rank.aspx/SaveData",
                data: '{Id:"' + Id + '", Name : "' + Name + '",mota : "'+ mota +'"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.d != null) {
                        console.log(response.d.data);
                        if (Id == 0) {
                            $(".table-add tbody").append($(AddTr(response.d.data.Id, response.d.data.Name, response.d.data.Description,tdla)));
                        } else {
                            location.href = location.href;
                        }
                        removeLoading();
                        swal({
                            title: "",
                            type: "success",
                            text: response.d.message,
                            timer: 2000,
                            showConfirmButton: false
                        });

                        $("#ql").hide();
                    }
                },
                failure: function (response) { alert(response.d); }
            });

        }

        function AddTr(Id, Name, Description, tdla) {
            return '<tr class="remove' + Id + '">' +
                        '<td>' + (tdla + 1) + '</td>' +
                        '<td>' + Name + '</td>' +
                        '<td>' + Description + '</td>' +
                        '<td class="map-edit">' +
                            '<div class="edit-wp">' +
                                '<a onclick="EditConfig($(this) ,' + Id + ')" data-id="' + Id + '" data-title="' + Name + '" data-mota="' + Description + '</a>' +
                                '<a onclick="DeletelConfig($(this),' + Id + ')" id="xoa" class="elm del-btn"></a>' +
                             '</div>' +
                         '</td>' +
                     '</tr>';
        }

        function EditConfig(This, Id) {
            $("#ql").show();
            id = Id;
            $("#txtTitle").val(This.attr("data-title"));
            $("#txtGhiChu").val(This.attr("data-mota"));
            $("#txtKey").attr("ReadOnly", "true");
            $("#txtTitle").focus();
        }

        function DeletelConfig(This, Id) {
            var id = Id;
            console.log(id);
            swal({
                title: "",
                text: "Bạn có chắc chắn muốn xóa hay không?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                closeOnConfirm: false
            },
            function () {
                addLoading();
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/Config/Config_Rank.aspx/DeleteData",
                    data: '{Id:' + id + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        console.log(response.d.data);
                        removeLoading();
                        $(".remove" + id).remove();
                        location.href = location.href;
                        swal({
                            title: "",
                            type: "success",
                            text: response.d.message,
                            timer: 2000,
                            showConfirmButton: false
                        });
                    },
                    failure: function (response) { alert(response.d); }
                });
            });
        }

        function show() {
            $("#ql").show();
            id = 0;
            $("#txtTitle").val("");
            $("#txtGhiChu").val("");
            $("#txtKey").removeAttr("readonly");
        }

        function huy() {
            $("#ql").hide();
            id = 0;
            $("#txtTitle").val("");
            $("#txtGhiChu").val("");
        }
    </script>
</asp:Content>
