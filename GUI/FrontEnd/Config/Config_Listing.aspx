﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Config_Listing.aspx.cs" Inherits="_30shine.GUI.FrontEnd.Config.Config_Listing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .edit { margin: 8px; width: 15px; }
            .del { margin: 8px; width: 15px; }
            .del:hover { color: red; }
            .edit:hover { color: seagreen; }
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Cấu hình hệ thống &nbsp;&#187; </li>
                        <li class="li-listing" style="cursor: pointer"><a onclick="show()">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing be-report woktime-listing">
            <%-- Listing --%>

            <div class="wp960 content-wp">
                <div class="row" id="ql" hidden="hidden">
                    <ul class="form-group">
                        <li class="list-group-item style-budget">
                            <div class="col-md-12 col-xs-12" style="margin-top: 10px">
                                <label>Tiêu đề</label>
                                <asp:TextBox CssClass="form-control" ID="txtTitle" placeholder="Nhập tiêu đề"
                                    ClientIDMode="Static" runat="server"></asp:TextBox>
                            </div>
                            <div class="col-md-12 col-xs-12" style="margin-top: 10px">
                                <label>Key</label>
                                <asp:TextBox CssClass="form-control" ID="txtKey" placeholder="Nhập key"
                                    ClientIDMode="Static" runat="server"></asp:TextBox>
                            </div>
                            <div class="col-md-12 col-xs-12" style="margin-top: 10px">
                                <label>Value</label>
                                <asp:TextBox CssClass="form-control" ID="txtValue" placeholder="Nhập giá trị"
                                    ClientIDMode="Static" runat="server"></asp:TextBox>
                            </div>
                            <div class="col-md-12 col-xs-12" style="margin-top: 10px">
                                <label>Mô tả</label>
                                <asp:TextBox runat="server" CssClass="form-control" ID="txtGhiChu" TextMode="MultiLine" ClientIDMode="Static" placeholder="Nhập mô tả"></asp:TextBox>
                            </div>
                            <%--<input type="checkbox" runat="server" class="form-control" id="chkPublish" clientidmode="Static" /> Publish--%>

                            <a class="btn btn-primary" style="margin-left: 128px; margin-right: 10px; width: 100px; margin-top: 10px" onclick="SaveConfig($(this))">Lưu lại</a>
                            <a class="btn btn-default" style="margin-left: 10px; margin-right: 10px; width: 100px; margin-top: 10px" onclick="huy()">Hủy</a>
                        </li>
                    </ul>
                </div>
            </div>


            <div class="col-md-12 col-xs-12">
                <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Danh sách cấu hình</strong>
                <table class="table-add table-listing">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tiêu đề</th>
                            <th>Key</th>
                            <th>Value</th>
                            <th>Mô tả</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <%if (obj.Count > 0)
                            { %>
                        <%int i = 1; foreach (var v in obj)
                            { %>
                        <tr class="remove<%= v.Id %>">
                            <td><%= i++ %></td>
                            <td><%=v.Title %></td>
                            <td><%= v.Key %></td>
                            <td><%= v.Value %></td>
                            <td><%= v.Description %></td>
                            <td class="map-edit">
                                <div class="edit-wp">
                                    <a onclick="EditConfig($(this) ,<%=v.Id %>)" data-id="<%=v.Id %>"
                                        data-title="<%= v.Title %>" data-mota="<%= v.Description %>"
                                        data-key="<%=v.Key %>" data-value="<%=v.Value %>"
                                        id="sua" class="elm edit-btn"></a>
                                    <a onclick="DeletelConfig($(this),<%=v.Id %>)" id="xoa" class="elm del-btn"></a>
                                </div>
                            </td>
                        </tr>
                        <%} %>
                        <%} %>
                    </tbody>
                </table>
            </div>
        </div>
    </asp:Panel>
    <style>
        .style-budget label { float: left; width: 100px; line-height: 30px; }
        .style-budget input, textarea { width: 300px !important; float: left; margin-left: 12px; }
    </style>
    <script>
        var id = 0;

        function SaveConfig(This) {
            var Id = parseInt(id);
            Id = !isNaN(Id) ? Id : 0;
            var title = $("#txtTitle").val();

            var key = $("#txtKey").val();
            if (key == "") {
                alert("Vui lòng nhập key");
                return;
            }
            var value = $("#txtValue").val();
            if (value == "") {
                alert("Vui lòng nhập giá trị");
                return;
            }
            var mota = $("#txtGhiChu").val();
            console.log(Id, key, value, title);
            var tdla = parseInt($('.table-add tbody tr:last td:first').text());

            addLoading();
            $.ajax({
                type: "POST",
                url: "/GUI/FrontEnd/Config/Config_Listing.aspx/SaveData",
                data: '{Id:"' + Id + '", Title : "' + title + '", Key:"' + key + '", Value:"' + value
                    + '", MoTa:"' + mota + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.d != null) {
                        console.log(response.d.data);
                        if (response.d.data.Id != 0) {
                            $(".table-add tbody").append($(AddTr(response.d.data.Id, response.d.data.Title, response.d.data.Description, response.d.data.Key, response.d.data.Value, tdla)));
                        }
                        removeLoading();
                        swal({
                            title: "",
                            type: "success",
                            text: response.d.message,
                            timer: 2000,
                            showConfirmButton: false
                        });

                        $("#ql").hide();
                    }
                },
                failure: function (response) { alert(response.d); }
            });

        }

        function AddTr(Id, Title, Description, Key, Value, tdla) {
            return '<tr class="remove' + Id + '">' +
                        '<td>' + (tdla + 1) + '</td>' +
                        '<td>' + Title + '</td>' +
                        '<td>' + Key + '</td>' +
                        '<td>' + Value + '</td>' +
                        '<td>' + Description + '</td>' +
                        '<td class="map-edit">' +
                            '<div class="edit-wp">' +
                                '<a onclick="EditConfig($(this) ,' + Id + ')" data-id="' + Id + '" data-title="' + Title + '" data-mota="' + Description + '" data-key="' + Key + '" data-value="' + Value + '" id="sua" class="elm edit-btn"></a>' +
                                '<a onclick="DeletelConfig($(this),' + Id + ')" id="xoa" class="elm del-btn"></a>' +
                             '</div>' +
                         '</td>' +
                     '</tr>';
        }

        function EditConfig(This, Id) {
            $("#ql").show();
            id = Id;
            $("#txtTitle").val(This.attr("data-title"));
            $("#txtKey").val(This.attr("data-key"));
            $("#txtValue").val(This.attr("data-value"));
            $("#txtGhiChu").val(This.attr("data-mota"));
            $("#txtKey").attr("ReadOnly", "true");
        }

        function DeletelConfig(This, Id) {
            var id = Id;
            console.log(id);
            swal({
                title: "",
                text: "Bạn có chắc chắn muốn xóa hay không?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                closeOnConfirm: false
            },
            function () {
                addLoading();
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/Config/Config_Listing.aspx/DeleteData",
                    data: '{Id:' + id + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        console.log(response.d.data);
                        removeLoading();
                        $(".remove" + id).remove();
                        swal({
                            title: "",
                            type: "success",
                            text: response.d.message,
                            timer: 2000,
                            showConfirmButton: false
                        });
                    },
                    failure: function (response) { alert(response.d); }
                });
            });
        }

        function show() {
            $("#ql").show();
            id = 0;
            $("#txtTitle").val("");
            $("#txtKey").val("");
            $("#txtValue").val("");
            $("#txtGhiChu").val("");
            $("#txtKey").removeAttr("readonly");
        }

        function huy() {
            $("#ql").hide();
            id = 0;
            $("#txtTitle").val("");
            $("#txtKey").val("");
            $("#txtValue").val("");
            $("#txtGhiChu").val("");
        }
    </script>
</asp:Content>
