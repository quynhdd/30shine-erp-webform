﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Script.Serialization;
using ExportToExcel;
using Excel;
using LinqKit;
using System.Web.Services;
using System.Data.Entity.Migrations;
using Project.Model.Structure;
using static Library.Class;

namespace _30shine.GUI.FrontEnd.Config
{
    public partial class Config_Rating_UuDai : System.Web.UI.Page
    {
        Solution_30shineEntities db = new Solution_30shineEntities();
        public List<Store_Customer_Rating_UuDai_Result> ListRT;
        UIHelpers helper = new UIHelpers();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                helper.BindRating(ddlRating);
                helper.BindUuDai(ddlUuDai);
                LoadData();
            }

        }

        public void LoadData()
        {
            ListRT = db.Store_Customer_Rating_UuDai().ToList();
        }

        /// <summary>
        /// thêm 
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="item_Id"></param>
        /// <param name="giaTri"></param>
        /// <param name="ngayChi"></param>
        /// <returns></returns>
        [WebMethod]
        public static object SaveData(int Id, int RatingId, int UuDaiId)
        {
            var msg = new cls_message();
            CultureInfo culture = new CultureInfo("vi-VN");
            using (var db = new Solution_30shineEntities())
            {
                var objCF = new Customer_Rating_UuDai();
                if (Id != 0)
                {
                    objCF = db.Customer_Rating_UuDai.Single(p => p.Id == Id);
                    objCF.ModifiedDate = DateTime.Now;
                }
                else
                {
                    objCF.CreatedDate = DateTime.Now;
                }

                objCF.RatingId = RatingId;
                objCF.UuDaiId = UuDaiId;
                objCF.IsDelete = false;
                objCF.Publish = true;
                db.Customer_Rating_UuDai.AddOrUpdate(objCF);
                db.SaveChanges();
                try
                {
                    var objRT = db.Store_Customer_Rating_UuDai().Where(p => p.Id == objCF.Id).Single();
                    msg.data = objRT;
                }
                catch
                { }

                msg.status = "success";
                msg.message = "Dữ liệu đã được lưu!";
            }
            return msg;
        }

        /// <summary>
        /// xóa  
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [WebMethod]
        public static object DeleteData(int Id)
        {
            using (var db = new Solution_30shineEntities())
            {
                var msg = new cls_message();
                if (Id > 0)
                {
                    var obj = db.Customer_Rating_UuDai.FirstOrDefault(w => w.Id == Id);
                    if (obj != null)
                    {
                        obj.IsDelete = true;
                        obj.Publish = false;
                        obj.ModifiedDate = DateTime.Now;
                        db.Customer_Rating_UuDai.AddOrUpdate(obj);
                        db.SaveChanges();

                        msg.success = true;
                        msg.data = obj;
                        msg.message = "Đã xoá dữ liệu thành công";
                    }
                    else
                    {
                        msg.success = false;
                        msg.message = "Lỗi!. vui lòng liên hệ với nhà phát triển để được giải đáp";
                    }
                }
                else
                {
                    msg.success = false;
                    msg.message = "Lỗi!. vui lòng liên hệ với nhà phát triển để được giải đáp";
                }
                return msg;
            }
        }
    }
}