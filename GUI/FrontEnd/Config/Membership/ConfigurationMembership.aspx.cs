﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Web.Services;
using System.Data.Entity.Migrations;
using System.Web.Script.Serialization;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.FrontEnd.Config.Membership
{
    public partial class ConfigurationMembership : System.Web.UI.Page
    {
        private bool Perm_Access = false;
        protected bool Perm_ViewAllData = false;
        public List<_30shine.MODEL.CustomClass.SalonServiceConfig> LstServiceConfig;
        private static ConfigurationMembership instance;
        private const int ServiceType = 1;
        private const int ProductType = 2;
        private const int CategoryId = 98;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {

            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        /// <summary>
        /// Get SalonServiceConfig instance
        /// </summary>
        /// <returns></returns>
        public static ConfigurationMembership getInstance()
        {
            if (!(ConfigurationMembership.instance is ConfigurationMembership))
            {
                return new ConfigurationMembership();
            }
            else
            {
                return ConfigurationMembership.instance;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                BindPackageMember();
                BindProduct();
                BindService();
                BindData();
            }
        }

        /// <summary>
        /// Bind PackageMember to ddl
        /// </summary>
        protected void BindPackageMember()
        {
            using (var db = new Solution_30shineEntities())
            {
                List<Product> packageMember = db.Products.Where(w => w.IsDelete == 0 && w.Publish == 1 && w.CategoryId == 98).OrderBy(o => o.Id).ToList();
                ddlPackageMembership.DataTextField = "Name";
                ddlPackageMembership.DataValueField = "Id";
                ddlPackageMembership.DataSource = packageMember;
                ddlPackageMembership.DataBind();
            }
        }

        /// <summary>
        /// Bind Product to ddl
        /// </summary>
        protected void BindProduct()
        {
            using (var db = new Solution_30shineEntities())
            {
                var products = db.Products.Where(w => w.IsDelete == 0 && w.Publish == 1 && w.CategoryId != 98).OrderBy(o => o.Id).ToList();
                var item = new _30shine.MODEL.ENTITY.EDMX.Product();
                item.Name = "Chọn tất cả sản phẩm";
                item.Id = 0;
                products.Insert(0, item);
                ddlServiceProduct.DataTextField = "Name";
                ddlServiceProduct.DataValueField = "Id";
                ddlServiceProduct.DataSource = products;
                ddlServiceProduct.DataBind();
            }
        }

        /// <summary>
        /// Bind Service to ddl
        /// </summary>
        protected void BindService()
        {
            using (var db = new Solution_30shineEntities())
            {
                var services = db.Services.Where(w => w.IsDelete == 0 && w.Publish == 1).OrderBy(o => o.Id).ToList();
                var item = new _30shine.MODEL.ENTITY.EDMX.Service();
                item.Name = "Chọn tất cả dịch vụ";
                item.Id = 0;
                services.Insert(0, item);
                ddlServiceProduct.DataTextField = "Name";
                ddlServiceProduct.DataValueField = "Id";
                ddlServiceProduct.DataSource = services;
                ddlServiceProduct.DataBind();
            }
        }

        /// <summary>
        /// onchange dropdownlist get data
        /// </summary>
        protected void BindServiceOrProduct(object sender, EventArgs e)
        {
            if ((int.TryParse(ddlTypeSvOrPd.SelectedValue, out var integer) ? integer : 1) == 1)
            {
                BindService();
                svOrpd.InnerHtml = "Dịch vụ";
            }
            else
            {
                BindProduct();
                svOrpd.InnerHtml = "Sản phẩm";
            }
            BindData();
        }



        /// <summary>
        /// bind data 
        /// </summary>
        private void BindData()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    if ((int.TryParse(ddlTypeSvOrPd.SelectedValue, out var integer) ? integer : 1) == ServiceType)
                    {
                        GetListConfigService(int.TryParse(ddlPackageMembership.SelectedValue, out integer) ? integer : 0,
                                             int.TryParse(ddlServiceProduct.SelectedValue, out integer) ? integer : 0);
                    }
                    else
                    {
                        GetListConfigProduct(int.TryParse(ddlPackageMembership.SelectedValue, out integer) ? integer : 0,
                                             int.TryParse(ddlServiceProduct.SelectedValue, out integer) ? integer : 0);
                    }
                }
            }
            catch (Exception e)
            {
                TriggerJsMsgSystem(this, "Lỗi: " + e.Message, "msg-system warning", 5000);
            }
        }

        /// <summary>
        /// get list config service
        /// </summary>
        /// <param name="idProMemer"></param>
        /// <param name="idSvOrPd"></param>
        public void GetListConfigService(int idProMemer, int idSvOrPd)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    if (idProMemer <= 0)
                    {
                        return;
                    }
                    string sql = $@"SELECT 
	                                    c.Id AS PackageMemberId, 
	                                    c.Name AS PackageMemberName, a.Id AS PrdOrSrvId, a.Name AS PrdOrSrvName, 
	                                    a.Price AS PriceNormal, ISNULL(b.PriceMember, 0) AS PriceMember, ISNULL(b.IsPublish,0) AS Publish  
                                    FROM dbo.[Service] a
                                    LEFT JOIN dbo.ConfigMembership b ON a.Id = b.IdServiceOrProduct AND b.IsDeleted = 0 AND b.ProductMemberId = {idProMemer}  AND b.Type = {ServiceType}
                                    INNER JOIN dbo.Product c ON c.Id = {idProMemer}
                                    WHERE 
                                    a.Publish = 1 AND a.IsDelete = 0
                                    AND ((a.Id = {idSvOrPd}) OR ({idSvOrPd} = 0)) 
                                    GROUP BY c.Id, c.Name, a.Id, a.Name, a.Price, ISNULL(b.PriceMember, 0), ISNULL(b.IsPublish, 0)";
                    var config = db.Database.SqlQuery<PackageMember>(sql).ToList();
                    RptMembership.DataSource = config;
                    RptMembership.DataBind();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// get list config product
        /// </summary>
        /// <param name="idProMemer"></param>
        /// <param name="idSvOrPd"></param>
        public void GetListConfigProduct(int idProMemer, int idSvOrPd)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    if (idProMemer <= 0)
                    {
                        return;
                    }
                    string sql = $@"SELECT 
                                        c.Id AS PackageMemberId,
                                        c.Name AS PackageMemberName, a.Id AS PrdOrSrvId, a.Name AS PrdOrSrvName, 
                                        a.Price AS PriceNormal, ISNULL(b.PriceMember, 0) AS PriceMember, ISNULL(b.IsPublish,0) AS Publish  
                                    FROM dbo.Product a
                                    LEFT JOIN dbo.ConfigMembership b ON a.Id = b.IdServiceOrProduct AND b.IsDeleted = 0 AND b.ProductMemberId = {idProMemer}  AND b.Type = {ProductType}
                                    INNER JOIN dbo.Product c ON c.Id = {idProMemer}
                                    WHERE 
                                    a.Publish = 1 AND a.IsDelete = 0
                                    AND a.CategoryId NOT IN({CategoryId})
                                    AND ((a.Id = {idSvOrPd}) OR ({idSvOrPd} = 0)) 
                                    GROUP BY c.Id, c.Name, a.Id, a.Name, a.Price, ISNULL(b.PriceMember, 0), ISNULL(b.IsPublish, 0)";
                    var config = db.Database.SqlQuery<PackageMember>(sql).ToList();
                    RptMembership.DataSource = config;
                    RptMembership.DataBind();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _BtnClick(object sender, EventArgs e)
        {
            BindData();
            RegisterScript();
        }

        /// <summary>
        /// Finish Loading Status
        /// </summary>
        public void RegisterScript()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "select2();finishLoading();", true);
        }

        protected static void TriggerJsMsgSystem(Page OBJ, string msg, string status, int duration)
        {
            ScriptManager.RegisterStartupScript(OBJ, OBJ.GetType(), "Message System",
                                    "showMsgSystem('" + msg + "','" + status + "'," + duration + ");", true);
        }

        [WebMethod]
        public static object AddOrUpdate(List<PackageMember> listConfig)
        {
            Msg msg = new Msg();
            try
            {
                if (listConfig.Count > 0)
                {
                    using (var db = new Solution_30shineEntities())
                    {
                        int idPackage = listConfig[0].PackageMemberId;
                        int type = listConfig[0].Type;
                        int svOrpdId = listConfig.Count == 1 ? listConfig[0].PrdOrSrvId : 0;
                        var listItem = db.ConfigMemberships.Where(w => w.ProductMemberId == idPackage && w.Type == type && 
                                                                       ((w.IdServiceOrProduct == svOrpdId) || (svOrpdId == 0))).ToList();
                        var item = new ConfigMembership();
                        // kiểm tra xóa Item
                        if (listItem.Count > 0)
                        {
                            foreach (var v in listItem)
                            {
                                var record = listConfig.FirstOrDefault(w => w.PrdOrSrvId == v.IdServiceOrProduct);
                                if (record != null)
                                {
                                    v.Type = record.Type;
                                    v.PriceMember = record.PriceMember;
                                    v.IsPublish = record.Publish;
                                    v.IsDeleted = false;
                                    v.ModifiedDate = DateTime.Now;
                                    db.SaveChanges();
                                }
                                else
                                {
                                    v.ModifiedDate = DateTime.Now;
                                    db.SaveChanges();
                                }
                            }
                        }
                        // kiểm tra thêm mới Item
                        if (listConfig.Count > 0)
                        {
                            foreach (var v in listConfig)
                            {
                                var record = listItem.FirstOrDefault(w => w.IdServiceOrProduct == v.PrdOrSrvId);
                                if (record is null)
                                {
                                    record = new ConfigMembership();
                                    record.ProductMemberId = v.PackageMemberId;
                                    record.IdServiceOrProduct = v.PrdOrSrvId;
                                    record.Type = v.Type;
                                    record.PriceMember = v.PriceMember;
                                    record.CreatedDate = DateTime.Now;
                                    record.IsPublish = v.Publish;
                                    record.IsDeleted = false;
                                    db.ConfigMemberships.Add(record);
                                }
                            }
                            db.SaveChanges();
                        }
                    }
                    msg.success = true;
                    msg.msg = "Hoàn tất thành công!";
                }
                else
                {
                    msg.success = false;
                    msg.msg = "Hoàn tất thất bại!";
                }
            }
            catch (Exception e)
            {
                msg.success = false;
                msg.msg = "Lỗi: " + e.Message;
            }
            return msg;
        }

        public class PackageMember
        {
            public int PackageMemberId { get; set; }
            public string PackageMemberName { get; set; }
            public int PrdOrSrvId { get; set; }
            public string PrdOrSrvName { get; set; }
            public int Type { get; set; }
            public int PriceNormal { get; set; }
            public int PriceMember { get; set; }
            public bool Publish { get; set; }
        }
    }
}