﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="ConfigurationMembership.aspx.cs" Inherits="_30shine.GUI.FrontEnd.Config.Membership.ConfigurationMembership" %>

<asp:Content ID="MembershipAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <link href="../../../../Assets/css/erp.30shine.com/salon-service-config/common.min.css" rel="stylesheet" />
        <style>
            div.complete input.btn-complete {
                color: #fff !important;
                background-color: black !important;
                padding: 10px 37px !important;
                margin-left: 5% !important;
                margin-bottom: 15px !important;
                font-size: 16px !important;
            }

            .table-sub tr:nth-child(even) {
                background-color: #d4d4d5 !important;
            }

            .table-sub td {
                font-size: 16px;
                font-weight: 500;
            }

            .div-wp {
                background: rgba(0,0,0,0.2) !important;
            }
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Cấu hình dịch vụ và sản phẩm của Membership &nbsp;&#187; </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add admin-product-add admin-service-add">
            <%-- Add --%>
            <div class="container wp960 content-wp">
                <!-- System Message -->
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <!-- END System Message -->
                <div class="row div-wp">
                    <div class="table-wp">
                        <div class="title-head">
                            <span>
                                <strong class="main-name">Cấu hình dịch vụ và sản phẩm
                                </strong>
                            </span>
                        </div>
                        <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                        <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                            <ContentTemplate>
                                <table class="table-add admin-product-table-add admin-service-table-add">
                                    <tbody>
                                        <tr id="tr-filter">
                                            <td class="col-xs-2">
                                                <span class="field-wp">
                                                    <asp:DropDownList ID="ddlPackageMembership" CssClass="form-control select" OnSelectedIndexChanged="BindServiceOrProduct" AutoPostBack="true" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                                </span>
                                            </td>
                                            <td class="col-xs-2">
                                                <span class="field-wp">
                                                    <asp:DropDownList ID="ddlTypeSvOrPd" CssClass="form-control select" OnSelectedIndexChanged="BindServiceOrProduct" AutoPostBack="true" runat="server" ClientIDMode="Static">
                                                        <asp:ListItem Value="1">Dịch vụ</asp:ListItem>
                                                        <asp:ListItem Value="2">Sản phẩm</asp:ListItem>
                                                    </asp:DropDownList>
                                                </span>
                                            </td>
                                            <td class="col-xs-4">
                                                <span class="field-wp"></span>
                                            </td>
                                            <td class="col-xs-2 field-service">
                                                <span class="field-wp">
                                                    <asp:DropDownList ID="ddlServiceProduct" CssClass="form-control select" runat="server" ClientIDMode="Static" onchange="getDataPackageMember();"></asp:DropDownList>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <table class="admin-product-table-add admin-service-table-add table-sub">
                                                <thead>
                                                    <tr id="tr-header">
                                                        <th width="6%">STT</th>
                                                        <th width="22%">Gói Membership</th>
                                                        <th id="svOrpd" runat="server" width="25%">Dịch vụ</th>
                                                        <th width="20%">Giá ( Thường )</th>
                                                        <th width="21%">Giá ( Membership )</th>
                                                        <th width="6%">Sử dụng</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr class="set-data">
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>
                                                            <input class="form-control" style="width: 50% !important; text-align: center; margin: auto; padding: 10px;" onchange="changeValue($(this));" onkeypress="return ValidateKeypress(/\d/,event);" onkeyup="reformatText(this)" value="" maxlength="10" />
                                                        </td>
                                                        <td>
                                                            <div class="checkbox box-notification" style="margin: auto; padding: 10px;">
                                                                <label>
                                                                    <input type="checkbox" onclick="ChbxForAll()" class="check-for-all" />
                                                                    <span class="cr" style="float: none !important;"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <asp:Repeater ID="RptMembership" runat="server">
                                                        <ItemTemplate>
                                                            <tr class="set-data">
                                                                <td><%# Container.ItemIndex + 1%></td>
                                                                <td><%# Eval("PackageMemberName") %></td>
                                                                <td class="td-svorpd-name" data-id="<%# Eval("PrdOrSrvId") %>"><%# Eval("PrdOrSrvName") %></td>
                                                                <td><%#String.Format("{0:#,###}", Convert.ToInt32(Eval("PriceNormal"))) != "" ? String.Format("{0:#,###}", Convert.ToInt32(Eval("PriceNormal"))).Replace(".", ",") : "0" %></td>
                                                                <td>
                                                                    <input class="form-control price-member" style="width: 40% !important; text-align: center; margin: auto; padding: 10px;" onkeypress="return ValidateKeypress(/\d/,event);" onkeyup="reformatText(this)"
                                                                        value="<%#String.Format("{0:#,###}", Convert.ToInt32(Eval("PriceMember"))) != "" ? String.Format("{0:#,###}", Convert.ToInt32(Eval("PriceMember"))).Replace(".", ",") : "0" %>" maxlength="10" />
                                                                </td>
                                                                <td>
                                                                    <div class="checkbox" style="margin: auto; padding: 10px;">
                                                                        <label>
                                                                            <input type="checkbox" <%#Convert.ToBoolean(Eval("Publish")) == true ? "Checked=\"True\"" : "" %> />
                                                                            <span class="cr" style="float: none !important"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                        </label>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </tbody>
                                            </table>
                                        </tr>
                                    </tbody>
                                </table>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                        <div class="complete">
                            <input class="btn-complete" onclick="AddOrUpdate();" type="button" value="Hoàn tất" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="../../../../Assets/js/select2/select2.min.js"></script>
        <link href="../../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script type="text/javascript">
            let salonId;
            let departmentId;
            let serviceId;
            let ArrDatas = [];
            let index;
            jQuery(document).ready(function () {
                select2();
            });

            function getDataPackageMember() {
                startLoading();
                $("#BtnFakeUP").click();
            }

            function select2() {
                $('.select').select2();
                return true;
            }

            function getListData(MemberId, Type, PrdOrSrvId, PriceMember, IsPublish) {
                var recd = {};
                recd.PackageMemberId = parseInt(MemberId);
                recd.Type = parseInt(Type);
                recd.PrdOrSrvId = parseInt(PrdOrSrvId);
                recd.PriceMember = parseInt(PriceMember);
                recd.Publish = IsPublish ? true : false;
                ArrDatas.push(recd);
            }

            //scroll tr-filter
            window.onscroll = function () { fucnFilter() };
            let filter = document.getElementById("tr-filter");
            let filters = filter.offsetTop;
            function fucnFilter() {
                if (window.pageYOffset > filters) {
                    filter.classList.add("filter-fixed");
                } else {
                    filter.classList.remove("filter-fixed");
                }
            }

            // Validate keypress
            function ValidateKeypress(numcheck, e) {
                var keynum, keychar, numcheck;
                if (window.event) {
                    keynum = e.keyCode;
                }
                else if (e.which) {
                    keynum = e.which;
                }
                if (keynum == 8 || keynum == 127 || keynum == null || keynum == 9 || keynum == 0 || keynum == 13 || keynum == 46) return true;
                keychar = String.fromCharCode(keynum);
                var result = numcheck.test(keychar);
                return result;
            }

            //Tự động nhập theo value all 
            function changeValue(This) {
                let data = $('.set-data');
                let $value = This.val();
                $.each(data, function (key, value) {
                    if ($value != '') {
                        $(this).find('.price-member').val($value);
                    }
                })
            }

            //Show thông báo lên màn hình
            function showMsgSystem(msg, status, duration) {
                $("#MsgSystem").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);
                setTimeout(function () {
                    $("#MsgSystem").fadeTo("slow", 0, function () {
                        $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 1);
                    });
                }, duration);
                $('html, body').animate({ scrollTop: 0 }, 'fast');

            }

            //checkbox for all
            function ChbxForAll() {
                $(".check-for-all").change(function () {
                    $("input:checkbox").prop('checked', $(this).prop("checked"));
                });
            }

            //000.000
            String.prototype.reverse = function () {
                return this.split("").reverse().join("");
            }

            //format money
            function reformatText(input) {
                var x = input.value;
                x = x.replace(/,/g, "");
                x = x.reverse();
                x = x.replace(/.../g, function (e) {
                    return e + ",";
                });
                x = x.reverse();
                x = x.replace(/^,/, "");
                input.value = x;
            }

            //Thêm hoặc update 
            function AddOrUpdate() {
                startLoading();
                var data = $(".set-data");
                let MemberId = $("#ddlPackageMembership :selected").val(),
                    Type = $("#ddlTypeSvOrPd :selected").val();
                if (MemberId === undefined || MemberId === null || MemberId === '' || Type === undefined || Type === null && Type === '') {
                    showMsgSystem(`Có lỗi xảy ra vui lòng liên hệ nhà phát triển. Member: ${MemberId}, Type: ${Type} `, 'msg-system warning', 5000);
                    return;
                }
                $.each(data, function (key, value) {
                    let PrdOrSrvId = $(this).find('td.td-svorpd-name').attr('data-id'),
                        PriceMember = $(this).find('td input.price-member').val(),
                        IsPublish = $(this).find("input[type='checkbox']").prop("checked");
                    if (PriceMember != undefined && PriceMember != null && PriceMember != '') {
                        PriceMember = PriceMember.toString().replace(',', '').replace(',', '').replace(',', '').replace(',', '');
                    }
                    else {
                        PriceMember = 0;
                    }
                    if (PrdOrSrvId != undefined && PrdOrSrvId != null && PrdOrSrvId != '' && parseInt(PrdOrSrvId) > 0) {
                        getListData(MemberId, Type, PrdOrSrvId, PriceMember, IsPublish);
                    }
                });
                if (ArrDatas != null) {
                    let data = JSON.stringify({ listConfig: ArrDatas });
                    $.ajax({
                        type: "POST",
                        url: "/GUI/FrontEnd/Config/Membership/ConfigurationMembership.aspx/AddOrUpdate",
                        data: data,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            if (response.d.success) {
                                showMsgSystem(response.d.msg, "msg-system success", 5000);
                            }
                            else {
                                showMsgSystem(response.d.msg, "msg-system warning", 5000);
                            }
                            finishLoading();
                        },
                        failure: function (response) { showMsgSystem(response.d.msg, "msg-system danger", 5000); }
                    });
                }
            }
        </script>
        <!-- Page loading -->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
    </asp:Panel>
</asp:Content>
