﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Script.Serialization;
using ExportToExcel;
using Excel;
using LinqKit;
using System.Web.Services;
using System.Data.Entity.Migrations;
using Project.Model.Structure;

namespace _30shine.GUI.FrontEnd.Config
{
    public partial class Config_Rank : System.Web.UI.Page
    {
        Solution_30shineEntities db = new Solution_30shineEntities();
        protected List<Customer_UuDai> obj;
        UIHelpers helper = new UIHelpers();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadData();
            }

        }

        public void LoadData()
        {
           obj = db.Customer_UuDai.Where(u => u.Publish == true).ToList();
        }

        /// <summary>
        /// thêm 
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="item_Id"></param>
        /// <param name="giaTri"></param>
        /// <param name="ngayChi"></param>
        /// <returns></returns>
        [WebMethod]
        public static object SaveData(int Id, string Name, string mota)
        {
            var msg = new Library.Class.cls_message();
            CultureInfo culture = new CultureInfo("vi-VN");
            using (var db = new Solution_30shineEntities())
            {
                if (Id == 0)
                {
                    var objCF = new Customer_UuDai();
                    objCF.Name = Name;
                    objCF.Images = "";
                    objCF.Description = mota;
                    objCF.IsDelete = false;
                    objCF.Publish = true;
                    objCF.CreatedDate = DateTime.Now;
                    db.Customer_UuDai.AddOrUpdate(objCF);
                    msg.data = objCF;
                }
                else
                {
                    var objCF = db.Customer_UuDai.Single(p => p.Id == Id);
                    objCF.Name = Name;
                    objCF.Images = "";
                    objCF.Description = mota;
                    objCF.IsDelete = false;
                    objCF.Publish = true;
                    objCF.ModifiedDate = DateTime.Now;
                    db.Customer_UuDai.AddOrUpdate(objCF);
                    msg.data = objCF;
                }
                db.SaveChanges();
                
                msg.success = true;
                msg.message = "Dữ liệu đã được lưu!";
            }
            return msg;
        }

        /// <summary>
        /// xóa  
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [WebMethod]
        public static object DeleteData(int Id)
        {
            using (var db = new Solution_30shineEntities())
            {
                var msg = new Library.Class.cls_message();
                if (Id > 0)
                {
                    var obj = db.Customer_UuDai.FirstOrDefault(w => w.Id == Id);
                    if (obj != null)
                    {
                        obj.IsDelete = true;
                        obj.Publish = false;
                        obj.ModifiedDate = DateTime.Now;
                        db.Customer_UuDai.AddOrUpdate(obj);
                        db.SaveChanges();

                        msg.success = true;
                        msg.data = obj;
                        msg.message = "Đã xoá dữ liệu thành công";
                    }
                    else
                    {
                        msg.success = false;
                        msg.message = "Lỗi!. vui lòng liên hệ với nhà phát triển để được giải đáp";
                    }
                }
                else
                {
                    msg.success = false;
                    msg.message = "Lỗi!. vui lòng liên hệ với nhà phát triển để được giải đáp";
                }
                return msg;
            }
        }
    }
}