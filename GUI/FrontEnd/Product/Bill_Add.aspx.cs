﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Web.Script.Serialization;
using System.Globalization;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.FrontEnd.ProductSale
{
    public partial class Bill_Add : System.Web.UI.Page
    {
        private int billId;
        protected bool _IsUpdate = false;
        protected bool HasProduct = false;
        protected int customerThreadId;
        protected int billPayMethod;
        private BillService Bill;
        CultureInfo culture = new CultureInfo("vi-VN");
        private string PageID = "DH_EDIT";
        private bool Perm_Access = false;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_ShowSalon = permissionModel.GetActionByActionNameAndPageId("Perm_ShowSalon", PageID, permission);
                ////Perm_AllPermission = permissionModel.GetActionByActionNameAndPageId("Perm_AllPermission", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();

            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                Bind_City();
                Bind_District();
                Bind_RptProduct();
                Bind_RptProductFeatured();
                bindProductCategory();                
                if (IsUpdate())
                {
                    if (Bind_OBJ())
                    {
                        Bind_RptProduct_Bill();
                    }
                }
                else
                {
                    var today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
                    BillDate.Text = today;
                    PayDate.Text = today;
                }
                bindSocialThread();
                bindPayMethod();
            }
        }

        protected void ExcAddOrUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
            {
                Update();
            }
            else
            {
                Add();
            }
        }

        private void Add()
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                // Insert/Update customer
                int customerId = updateCustomer();
                // Insert bill
                var obj = new BillService();
                obj.CustomerId = customerId;
                obj.ProductIds = HDF_ProductIds.Value != "[]" ? HDF_ProductIds.Value : "";
                obj.TotalMoney = int.TryParse(HDF_TotalMoney.Value, out integer) ? integer : 0;
                obj.Note = Note.Text;
                obj.FeeCOD = int.TryParse(MoneyCOD.Text, out integer) ? integer : 0;
                obj.FeeExtra = int.TryParse(MoneyExtra.Text, out integer) ? integer : 0;
                obj.IsOnline = true;
                obj.IsDelete = 0;
                obj.PayMethodId = int.TryParse(HDF_PayMethod.Value, out integer) ? integer : 0;
                if (BillDate.Text != "")
                {
                    obj.CreatedDate = Convert.ToDateTime(BillDate.Text, culture);
                }
                if (PayDate.Text != "")
                {
                    obj.Paid = true;
                    obj.CompleteBillTime = Convert.ToDateTime(PayDate.Text, culture);
                }
                else
                {
                    obj.Paid = false;
                }                

                db.BillServices.AddOrUpdate(obj);
                var exc = db.SaveChanges();
                var Error = exc > 0 ? 0 : 1;
                var serializer = new JavaScriptSerializer();

                // Insert to FlowProduct
                if (Error == 0)
                {
                    var objProduct = new FlowProduct();
                    //var str = "[{\"Id\":\"19\",\"Price\":340000,\"Quantity\":\"1\"}]";
                    serializer = new JavaScriptSerializer();
                    var ProductList = serializer.Deserialize<List<ProductBasic>>(obj.ProductIds);
                    var ProductListInserted = db.FlowProducts.Where(w => w.BillId == obj.Id && w.IsDelete != 1).ToList();
                    var index = -1;
                    var _product = new Product();

                    if (ProductList != null)
                    {
                        // Add/Update product
                        foreach (var v in ProductList)
                        {
                            if (Error == 0)
                            {
                                _product = db.Products.FirstOrDefault(w => w.Id == v.Id);
                                index = ProductListInserted.FindIndex(fi => fi.ProductId == v.Id);
                                if (index == -1)
                                {
                                    objProduct = new FlowProduct();
                                    objProduct.BillId = obj.Id;
                                    objProduct.ProductId = v.Id;
                                    objProduct.Price = v.Price;
                                    objProduct.Quantity = v.Quantity;
                                    objProduct.VoucherPercent = Convert.ToInt32(v.VoucherPercent);
                                    objProduct.PromotionMoney = Convert.ToInt32(v.Promotion);
                                    objProduct.CreatedDate = obj.CreatedDate;
                                    objProduct.IsDelete = 0;
                                    if (_product != null)
                                    {
                                        objProduct.Cost = Convert.ToInt32(_product.Cost);
                                        objProduct.ForSalary = _product.ForSalary;
                                    }

                                    db.FlowProducts.Add(objProduct);
                                    exc = db.SaveChanges();
                                    Error = exc > 0 ? Error : ++Error;
                                }
                                else
                                {
                                    var prd = ProductListInserted[index];
                                    prd.VoucherPercent = Convert.ToInt32(v.VoucherPercent);
                                    prd.PromotionMoney = Convert.ToInt32(v.Promotion);
                                    prd.Quantity = Convert.ToInt32(v.Quantity);
                                    db.FlowProducts.AddOrUpdate(prd);
                                    db.SaveChanges();
                                }
                            }
                        }
                    }

                    // Delete product
                    if (ProductListInserted.Count > 0)
                    {
                        foreach (var v in ProductListInserted)
                        {
                            if (ProductList != null)
                            {
                                index = ProductList.FindIndex(fi => fi.Id == v.ProductId);
                                if (index == -1)
                                {
                                    var prd = v;
                                    prd.IsDelete = 1;
                                    db.FlowProducts.AddOrUpdate(prd);
                                    db.SaveChanges();
                                }
                            }
                            else
                            {
                                var prd = v;
                                prd.IsDelete = 1;
                                db.FlowProducts.AddOrUpdate(prd);
                                db.SaveChanges();
                            }
                        }
                    }
                }

                if (Error == 0)
                {
                    // Thông báo thành công
                    var MsgParam = new List<KeyValuePair<string, string>>();
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Hoàn tất bill thành công!"));
                    UIHelpers.Redirect("/don-hang/them-moi.html", MsgParam);
                }
                else
                {
                    MsgSystem.Text = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                    MsgSystem.CssClass = "msg-system warning";
                }
            }
        }

        private void Update()
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                billId = int.TryParse(Request.QueryString["Id"], out integer) ? integer : 0;                
                var bill = db.BillServices.FirstOrDefault(w => w.Id == billId);
                int moneyCOD = int.TryParse(MoneyCOD.Text, out integer) ? integer : 0;
                int moneyExtra = int.TryParse(MoneyExtra.Text, out integer) ? integer : 0;
                if (bill != null)
                {
                    if (PayDate.Text != "")
                    {
                        bill.CompleteBillTime = Convert.ToDateTime(PayDate.Text, culture);
                        bill.Paid = true;                        
                    }
                    else
                    {
                        bill.CompleteBillTime = null;
                        bill.Paid = false;
                    }
                    bill.FeeCOD = moneyCOD;
                    bill.FeeExtra = moneyExtra;
                    bill.Note = Note.Text;
                    bill.ModifiedDate = DateTime.Now;
                    
                    db.BillServices.AddOrUpdate(bill);
                    var Error = db.SaveChanges() > 0 ? 0 : 1;
                    if (Error == 0)
                    {
                        // Thông báo thành công
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật bill thành công!"));
                        UIHelpers.Redirect("/don-hang/" + billId + ".html", MsgParam);
                    }
                    else
                    {
                        MsgSystem.Text = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                        MsgSystem.CssClass = "msg-system warning";
                    }
                }
                else
                {
                    UIHelpers.Redirect("/don-hang/" + billId + ".html");
                }
            }
        }

        private int updateCustomer()
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                var customerId = int.TryParse(HDF_CustomerId.Value, out integer) ? integer : 0;
                var customer = db.Customers.FirstOrDefault(w=>w.Id == customerId);
                if (customer == null)
                {
                    customer = new _30shine.MODEL.ENTITY.EDMX.Customer();
                    customer.Fullname = CustomerName.Text;
                    customer.Phone = CustomerCode.Text;
                    customer.CreatedDate = DateTime.Now;
                    customer.IsDelete = 0;
                }
                
                customer.Info_Flow = int.TryParse(HDF_SocialThread.Value, out integer) ? integer : 0;
                customer.CityId = int.TryParse(City.SelectedValue, out integer) ? integer : 0;
                customer.DistrictId = int.TryParse(District.SelectedValue, out integer) ? integer : 0;
                customer.Address = Address.Text;

                db.Customers.AddOrUpdate(customer);
                db.SaveChanges();

                return customer.Id;
            }
        }

        private bool IsUpdate()
        {
            int integer; 
            billId = int.TryParse( Request.QueryString["Id"], out integer) ? integer : 0;
            if (billId > 0)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }

        private bool Bind_OBJ()
        {
            using (var db = new Solution_30shineEntities())
            {
                disableControl();
                var ExistOBJ = true;
                Bill = db.BillServices.FirstOrDefault(w => w.Id == billId);                
                if (Bill != null)
                {
                    var customer = db.Customers.FirstOrDefault(w => w.Id == Bill.CustomerId.Value);
                    if (customer != null)
                    {
                        CustomerCode.Text = customer.Phone;
                        CustomerName.Text = customer.Fullname;

                        var selectedItem = City.SelectedItem;
                        if (selectedItem != null)
                        {
                            selectedItem.Selected = false;
                        }
                        var itemSelected = City.Items.FindByValue(customer.CityId.ToString());
                        if (itemSelected != null)
                        {
                            itemSelected.Selected = true;
                            Bind_District();
                        }

                        selectedItem = District.SelectedItem;
                        if (selectedItem != null)
                        {
                            selectedItem.Selected = false;
                        }
                        itemSelected = District.Items.FindByValue(customer.DistrictId.ToString());
                        if (itemSelected != null)
                        {
                            itemSelected.Selected = true;
                        }
                        Address.Text = customer.Address;
                        customerThreadId = customer.Info_Flow.Value;                                                
                    }

                    billPayMethod = Bill.PayMethodId.Value;
                    TotalMoney.Text = Bill.TotalMoney.ToString();
                    if (Bill.CreatedDate != null)
                    {
                        BillDate.Text = String.Format("{0:dd/MM/yyyy}", Bill.CreatedDate);
                    }
                    if (Bill.CompleteBillTime != null)
                    {
                        PayDate.Text = String.Format("{0:dd/MM/yyyy}", Bill.CompleteBillTime);
                        //PayDate.Enabled = false;
                    }
                    Note.Text = Bill.Note;
                    MoneyCOD.Text = Bill.FeeCOD.ToString();
                    MoneyExtra.Text = Bill.FeeExtra.ToString();
                }
                else
                {
                    MsgSystem.Text = "Hóa đơn không tồn tại.";
                    MsgSystem.CssClass = "msg-system warning";
                }

                return ExistOBJ;
            }
        }

        private void disableControl()
        {
            CustomerCode.Enabled = false;
            CustomerName.Enabled = false;
            City.Enabled = false;
            District.Enabled = false;
            Address.Enabled = false;
            BillDate.Enabled = false;
        }

        /// <summary>
        /// Bind danh sách sản phẩm nổi bật
        /// </summary>
        private void Bind_RptProductFeatured()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1 && w.Status == 1).OrderByDescending(o => o.Id).ToList();

                Rpt_ProductFeatured.DataSource = lst;
                Rpt_ProductFeatured.DataBind();
            }
        }

        /// <summary>
        /// Bind danh sách sản phẩm trong bill
        /// </summary>
        private void Bind_RptProduct_Bill()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Products = db.Products.OrderBy(o => o.Id).ToList();
                var Count = _Products.Count;
                if (Count > 0)
                {
                    for (var i = 0; i < _Products.Count; i++)
                    {
                        var productId = _Products[i].Id;
                        var thisProduct = db.FlowProducts.FirstOrDefault(w => w.BillId == Bill.Id && w.ProductId == productId && w.IsDelete != 1);
                        int quantity;

                        if (thisProduct == null)
                        {
                            _Products.RemoveAt(i);
                            i--;
                        }
                        else
                        {
                            quantity = Convert.ToInt32(thisProduct.Quantity);
                            _Products[i].Quantity = quantity;
                            _Products[i].VoucherPercent = Convert.ToInt32(thisProduct.VoucherPercent);
                            _Products[i].Promotion = Convert.ToInt32(thisProduct.PromotionMoney);
                        }
                    }
                    if (_Products.Count > 0)
                    {
                        ListingProductWp.Style.Add("display", "block");
                        HasProduct = true;
                    }

                }

                Rpt_Product_Bill.DataSource = _Products;
                Rpt_Product_Bill.DataBind();
            }
        }

        /// <summary>
        /// Bind danh sách sản phẩm
        /// </summary>
        private void Bind_RptProduct()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Products = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1).OrderByDescending(o => o.Id).ToList();

                Rpt_Product.DataSource = _Products;
                Rpt_Product.DataBind();
            }
        }

        public void bindSocialThread()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.SocialThreads.Where(w => w.IsDelete != 1 && w.Publish == 1 && w.stType == 1).OrderBy(p => p.Id).ToList();
                Rpt_SocialThread.DataSource = lst;
                Rpt_SocialThread.DataBind();
            }
        }

        public void bindPayMethod()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.PayMethods.Where(w => w.IsDelete != true && w.Publish == true).OrderBy(o => o.Order).ToList();
                Rpt_PayMethod.DataSource = lst;
                Rpt_PayMethod.DataBind();
            }
        }

        private void bindProductCategory()
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = new List<Tbl_Category>();
                var node = new List<Tbl_Category>();
                var c = new Tbl_Category();
                c.Id = 0;
                c.Name = "Chọn danh mục";
                list.Insert(0, c);
                var cateRoot = db.Tbl_Category.Where(w => w.IsDelete != 1 && w.Pid == 1 && w.Publish == true && w.Id != 20).ToList();
                if (cateRoot.Count > 0)
                {
                    foreach (var v in cateRoot)
                    {
                        node = db.Tbl_Category.Where(w => w.IsDelete != 1 && w.Pid == v.Id).ToList();
                        list.Add(v);
                        if (node.Count > 0)
                        {
                            foreach (var v2 in node)
                            {
                                v2.Name = "----- " + v2.Name;
                                list.Add(v2);
                            }
                        }
                    }
                }
                rptProductCategory.DataSource = list;
                rptProductCategory.DataBind();
            }
        }

        protected void Bind_City()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.TinhThanhs.OrderBy(p => p.ThuTu).ToList();

                City.DataTextField = "TenTinhThanh";
                City.DataValueField = "Id";
                City.SelectedIndex = 1;

                City.DataSource = lst;
                City.DataBind();
            }
        }

        protected void Bind_District()
        {
            using (var db = new Solution_30shineEntities())
            {
                int _TinhThanhID = Convert.ToInt32(City.SelectedValue);
                if (_TinhThanhID != 0)
                {
                    var lst = db.QuanHuyens.Where(p => p.TinhThanhID == _TinhThanhID).OrderBy(p => p.ThuTu).ToList();

                    District.DataTextField = "TenQuanHuyen";
                    District.DataValueField = "Id";
                    District.SelectedIndex = 0;

                    District.DataSource = lst;
                    District.DataBind();
                }
            }
        }

        protected void Reload_District(object sender, EventArgs e)
        {
            Bind_District();
        }

        
    }
}