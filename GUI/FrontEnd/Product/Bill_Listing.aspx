﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Bill_Listing.aspx.cs" Inherits="_30shine.GUI.FrontEnd.ProductSale.Bill_Listing" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .bill-status-paid { color: #50b347; }
            .bill-status-notpaid { color: orange; }

            table.table-total-report { }
            table.table-total-report {
                border-collapse: collapse;
            }

            table.table-total-report, .table-total-report th, .table-total-report td {
                border: 1px solid #bababa;
            }
            .table-total-report th { font-weight:normal; font-family: Roboto Condensed Bold; padding: 5px 10px;}
            .table-total-report td { padding: 5px 10px;}
            .table-total-report td span { width: 100%; font-family: Roboto Condensed Bold; padding-left: 20px; text-align: right; float: left;}

            .table-func-panel { margin-top: -20px;}
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý đơn hàng &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/don-hang/danh-sach.html">Danh sách</a></li>
                        <li class="li-add"><a href="/don-hang/them-moi.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing be-report">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row row-filter">
                    <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                    <div class="filter-item">
                        <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                           ClientIDMode="Static" runat="server"></asp:TextBox>
                    </div>
                    <strong class="st-head" style="margin-left: 10px;">
                        <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                    </strong>
                    <div class="filter-item">
                        <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeTo" placeholder="Đến ngày"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlPayMethod" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px;"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlPaid" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px;">
                            <asp:ListItem Text="Trạng thái thanh toán" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Đã thanh toán" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Chưa thanh toán" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <a href="/admin/don-hang/danh-sach.html" class="st-head btn-viewdata">Reset Filter</a>
                </div>
                <!-- End Filter -->
                <asp:ScriptManager runat="server" ID="ScriptManager1"></asp:ScriptManager>    
                <div class="row mgt" style="margin-top: 10px; margin-bottom: 5px;">
                    <strong class="st-head"><i class="fa fa-file-text"></i>Danh sách đơn hàng</strong>
                </div>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" ClientIDMode="Static">
                    <ContentTemplate>  
                        <div class="row">
                            <table class="table-total-report">
                                <tbody>
                                    <tr>
                                        <td>Số bill</td>
                                        <td>
                                            <span><%=totalReport.totalBill > 0 ? String.Format("{0:#,###}",totalReport.totalBill).Replace(',','.') : "0" %></span>
                                        </td>
                                    </tr>                                    
                                    <tr>
                                        <td>Doanh thu</td>
                                        <td>
                                            <span><%=totalReport.totalMoney > 0 ? String.Format("{0:#,###}",totalReport.totalMoney).Replace(',','.') : "0" %></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Phí COD</td>
                                        <td>
                                            <span><%=totalReport.feeCOD > 0 ? String.Format("{0:#,###}",totalReport.feeCOD).Replace(',','.') : "0" %></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Phí thu thêm</td>
                                        <td>
                                            <span><%=totalReport.feeExtra > 0 ? String.Format("{0:#,###}",totalReport.feeExtra).Replace(',','.') : "0" %></span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Thanh toán</td>
                                        <td>
                                            <span><%=totalReport.paidMoney > 0 ? String.Format("{0:#,###}",totalReport.paidMoney).Replace(',','.') : "0" %></span>
                                        </td>
                                    </tr>                                    
                                    <tr>
                                        <td>Công nợ</td>
                                        <td>
                                            <span><%=totalReport.oweMoney > 0 ? String.Format("{0:#,###}",totalReport.oweMoney).Replace(',','.') : "0" %></span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Row Table Filter -->
                <div class="table-func-panel">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="table-wp">
                            <table class="table-add table-listing">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <%if (Perm_Develop)
                                            { %>
                                        <th>ID</th>
                                        <% } %>
                                        <th>Ngày lập bill</th>
                                        <th>Ngày thanh toán</th>
                                        <th style="min-width: 100px;">Khách hàng</th>
                                        <th>Số ĐT</th>
                                        <th class="has-sub" style="min-width: 300px;">
                                            <div class="title">Mỹ phẩm</div>
                                            <table class="sub-table">
                                                <tbody>
                                                    <tr class="first">
                                                        <td class="col-xs-7">Tên</td>
                                                        <td class="col-xs-2">SL</td>
                                                        <td class="col-xs-3" style="border-right:none;">D.Thu</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </th>
                                        <th style="width: 120px;">Phương thức thanh toán</th>
                                        <th style="width: 120px;">Trạng thái</th>
                                        <th style="width:70px;">Phí COD</th>
                                        <th style="width:70px;">Phí thu thêm</th>
                                        <th>Tổng D.Thu</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="RptBill" runat="server" OnItemDataBound="Repeater_ItemDataBound">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <% if (Perm_Develop) { %>
                                                <td>
                                                    <a href="/don-hang/<%# Eval("Id") %>.html"><%# Eval("Id") %></a>
                                                </td>
                                                <% } %>
                                                <td><a href="/don-hang/<%# Eval("Id") %>.html"><%# String.Format("{0:dd/MM/yyyy}",Eval("CreatedDate")) %></a></td>
                                                <td><a href="/don-hang/<%# Eval("Id") %>.html"><%# Eval("CompleteBillTime") != null ? String.Format("{0:dd/MM/yyyy}",Eval("CompleteBillTime")) : "" %></a></td>
                                                <td><a href="/khach-hang/<%# Eval("CustomerId") %>.html"><%# Eval("CustomerName") %></a></td>
                                                <td><%# Eval("CustomerPhone") %></td>
                                                <td class="has-sub">
                                                    <table class="sub-table">
                                                        <tbody>
                                                            <%# Eval("productSet") %>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td><%# Eval("payMethodName") %></td>
                                                <td
                                                    <%# Convert.ToBoolean(Eval("Paid")) == true ? "class='bill-status-paid'" : "" %>
                                                    <%# Convert.ToBoolean(Eval("Paid")) == false ? "class='bill-status-notpaid'" : "" %>
                                                    >
                                                    <%# Eval("billStatusName") %>

                                                </td>
                                                <td><%# Eval("feeCOD") %></td>
                                                <td><%# Eval("feeExtra") %></td>
                                                <td class="map-edit" style="text-align: right;">
                                                    <%# String.Format("{0:#,###}",Eval("TotalMoney")).Replace(',', '.') %>
                                                    <div class="edit-wp">
                                                        <asp:Panel CssClass="edit-action-wp action-edit" runat="server" ID="EAedit" Visible="false">
                                                            <a class="elm edit-btn" href="/don-hang/<%# Eval("Id") %>.html" title="Sửa"></a>
                                                        </asp:Panel>
                                                        <asp:Panel CssClass="edit-action-wp action-delete" runat="server" ID="EAdelete" Visible="false">
                                                            <a class="elm del-btn" onclick="del(this.parentNode.parentNode.parentNode.parentNode,'<%# Eval("Id") %>', '<%# Eval("Id") %>')" href="javascript://" title="Xóa"></a>
                                                        </asp:Panel>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>

                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                            { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
            </div>
            <%-- END Listing --%>
        </div>

        <script>
            jQuery(document).ready(function () {
                $("#glbBillSale").addClass("active");
                $("#subMenu .li-listing").addClass("active");

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        //$input.val($input.val().split(' ')[0]);
                    }
                });
            });

            //============================
            // Event delete
            //============================
            function del(This, code, name) {
                var code = code || null,
                    name = name || null,
                    Row = This;
                if (!code) return false;

                // show EBPopup
                $(".confirm-yn").openEBPopup();
                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

                $("#EBPopup .yn-yes").bind("click", function () {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/SystemService/Ajax/Del.aspx/Delele_Bill_Product_Online",
                        data: '{Id : "' + code + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            var mission = JSON.parse(response.d);
                            if (mission.success) {
                                delSuccess();
                                Row.remove();
                            } else {
                                delFailed();
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    });
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
            }

            //============================
            // Event publish, featured
            //============================
            function checkFeatured(This, table) {
                var Id = This.attr("data-id");
                var isChecked = This.is(":checked");
                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Ajax/Publish_Featured.aspx/Featured",
                    data: '{Id : "' + Id + '", Table : "' + table + '", isChecked : "' + isChecked + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            alert("Chuyển trạng thái thành công.");
                        } else {
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function checkPublish(This, table) {
                var Id = This.attr("data-id");
                var isChecked = This.is(":checked");
                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Ajax/Publish_Featured.aspx/Publish",
                    data: '{Id : "' + Id + '", Table : "' + table + '", isChecked : "' + isChecked + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            alert("Chuyển trạng thái thành công.");
                        } else {
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }
        </script>

    </asp:Panel>
</asp:Content>


