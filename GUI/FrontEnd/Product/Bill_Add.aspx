﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Bill_Add.aspx.cs" Inherits="_30shine.GUI.FrontEnd.ProductSale.Bill_Add" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="CustomerAdd" ContentPlaceHolderID="CtMain" runat="server">
<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

<link href="/Assets/css/checkin.css" rel="stylesheet" />
<style>
    .fe-service table.fe-service-table-add .checkbox.cus-no-infor input[type="checkbox"] { margin-left: -15px;}
    label { font-weight: normal;}
    .cod-pay{ display: none; }
</style>

<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">
                <li>Quản lý đơn hàng &nbsp;&#187; </li>
                <li class="li-listing"><a href="/don-hang/danh-sach.html">Danh sách</a></li>
                <li class="li-add"><a href="/don-hang/them-moi.html">Thêm mới</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add admin-product-add fe-service no-padding no-margin">
    <%-- Add --%>                
    <div class="wp960 content-wp">
        <!-- System Message -->
        <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
        <!-- END System Message -->

        <div class="table-wp">
            <table class="table-add admin-product-table-add fe-service-table-add" style="margin-bottom: 30px;">
                <tbody>
                    <tr class="title-head">
                        <td><strong>Thông tin đơn hàng</strong></td>
                        <td></td>
                    </tr>
                    <tr class="tr-margin" style="height: 20px;"></tr>

                    <tr class="tr-field-ahalf">
                        <td class="col-xs-2 left"><span>KH</span></td>
                        <td class="col-xs-10 right">
                            <div class="field-wp">
                                <div class="filter-item" style="width: 45%; max-width: 250px; z-index: 3000; padding-right: 0;">
                                    <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion" data-field="customer.code" data-value="0"
                                        data-callback="Callback_UpdateCustomerCode" AutoCompleteType="Disabled" ID="CustomerCode" ClientIDMode="Static" onchange="CustomerInputOnchange($(this))" onfocus="CustomerInputOnchange($(this))" onblur="checkBindCustomerDataByPhone($(this));" onkeydown="validateTypingNumber(event, $(this))" placeholder="Số điện thoại" runat="server" Style="width: 25%;"></asp:TextBox>
                                    <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                                        <ul class="ul-listing-staff ul-listing-suggestion" id="Ul4"></ul>
                                    </div>
                                    <div class="fake-value"></div>
                                </div>
                                <asp:TextBox ID="CustomerName" runat="server" ClientIDMode="Static" placeholder="Tên Khách hàng" Style="width: 45%; max-width: 250px; margin-left: 10px; font-family: Roboto Condensed Bold; font-size: 15px;"></asp:TextBox>

                                <%--<div class="checkbox cus-no-infor" style="padding-top: 4px;">
                                    <label class="lbl-cus-no-infor">
                                        <input type="checkbox" id="InputCusNoInfor" runat="server" clientidmode="Static" />
                                        Không cho thông tin
                                    </label>
                                </div>--%>
                            </div>
                            <div class="field-wp">
                                <p class="validate-phone">(*) Số điện thoại chỉ gồm 10 hoặc 11 chữ số từ 0-9. Bắt đầu bằng số 0.</p>
                            </div>
                        </td>
                    </tr>

                    <tr id="trFlowInfor">
                        <td class="col-xs-2 left"></td>
                        <td class="col-xs-10 right" style="padding-bottom: 0;">
                            <div class="field-wp" style="height: auto; line-height: 18px;">
                                <p class="_p1">
                                    <%--<span style="text-transform: uppercase; font-style: normal;" class="_span1">Khách hàng mới</span>--%>
                                    <span>&nbsp;&nbsp;-&nbsp;&nbsp;Nguồn thông tin biết đến cửa hàng</span>
                                </p>
                                <asp:Repeater runat="server" ID="Rpt_SocialThread" ClientIDMode="Static">
                                    <ItemTemplate>
                                        <label class="lbl-cus-no-infor" style="margin-right: 5px; margin-bottom: 8px; cursor: pointer; font-weight: normal; font-size: 13px; padding: 2px 10px; background: #ddd; line-height: 21px;">
                                            <% if (customerThreadId > 0)
                                                { %>
                                                <input name="socialThread" type="radio" value="<%# Eval("Id") %>" <%# Convert.ToInt32(Eval("Id")) == customerThreadId ? "checked='checked'" : "" %> style="position: relative; top: 2px;" onclick="bindRadioValue('#HDF_SocialThread', 'socialThread')"  disabled="disabled" />
                                            <%# Eval("Name") %>
                                            <% }
    else
    { %>
                                            <input name="socialThread" type="radio" value="<%# Eval("Id") %>" <%# Container.ItemIndex == 0 ? "checked='checked'" : "" %> style="position: relative; top: 2px;" onclick="bindRadioValue('#HDF_SocialThread', 'socialThread')" />
                                            <%# Eval("Name") %>
                                            <% } %>                                            
                                        </label>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td class="col-xs-2 left"><span>Địa chỉ</span></td>
                        <td class="col-xs-10 right">
                            <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
                            <asp:UpdatePanel runat="server" ID="UP01">
                                <ContentTemplate>
                                    <div class="city">
                                        <asp:DropDownList AutoPostBack="true" ID="City" runat="server" CssClass="select"
                                            OnSelectedIndexChanged="Reload_District" ClientIDMode="Static">
                                        </asp:DropDownList>                                        
                                    </div>
                                    <div class="district">
                                        <asp:DropDownList ID="District" runat="server" CssClass="select" 
                                            ClientIDMode="Static">
                                        </asp:DropDownList>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <%--<asp:AsyncPostBackTrigger ControlID="OrderCloseBtn" EventName="Click"/>--%>
                                </Triggers>
                            </asp:UpdatePanel>
                            <asp:TextBox ID="Address" runat="server" placeholder="Địa chỉ" ClientIDMode="Static"></asp:TextBox>
                        </td>
                    </tr>

                    <tr class="tr-field-ahalf tr-product">
                        <td class="col-xs-2 left"><span>Sản phẩm</span></td>
                        <td class="col-xs-10 right">
                            <% if (!_IsUpdate)
                                { %>
                            <div class="row" id="quick-product">
                                <asp:Repeater runat="server" ID="Rpt_ProductFeatured">
                                    <ItemTemplate>
                                        <div class="checkbox cus-no-infor">
                                            <label class="lbl-cus-no-infor">
                                                <input type="checkbox" data-code="<%# Eval("Code") %>" onclick="pushQuickData($(this), 'product')" /> <%# Eval("Name") %>
                                            </label>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>

                                <div class="show-product show-item"  data-item="product"><i class="fa fa-plus-circle"></i>Thêm sản phẩm</div>
                            </div>
                            <%} %>                            
                            <div class="listing-product item-product" id="ListingProductWp" runat="server" ClientIDMode="Static">
                                <table class="table table-listing-product table-item-product" id="table-item-product">
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Tên sản phẩm</th>
                                            <th>Mã sản phẩm</th>
                                            <th>Đơn giá</th>
                                            <th>Số lượng</th>
                                            <th>Giảm giá</th>
                                            <th>Thành tiền</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="Rpt_Product_Bill" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                                    <td class="td-product-name"><%# Eval("Name") %></td>
                                                    <td class="td-product-code" data-id="<%# Eval("Id") %>"><%# Eval("Code") %></td>
                                                    <td class="td-product-price" data-price="<%# Eval("Price") %>"><%# Eval("Price") %></td>
                                                    <td class="td-product-quantity">
                                                        <input type="text" class="product-quantity" value="<%# Eval("Quantity") %>" />
                                                    </td>
                                                    <td class="td-product-voucher" data-voucher="<%# Eval("VoucherPercent") %>">
                                                        <div class="row">
                                                            <% if (_IsUpdate)
                                                                { %>
                                                            <input type="text" class="product-voucher" value="<%# Eval("VoucherPercent") %>" style="margin: 0 auto; float: none; text-align: center;width: 50px; <%# Eval("Promotion").ToString() == "0" ? "" : "text-decoration: line-through;" %>" disabled="disabled" />
                                                            <% }
    else
    { %>
                                                            <input type="text" class="product-voucher" value="<%# Eval("VoucherPercent") %>" style="margin: 0 auto; float: none; text-align: center;width: 50px; <%# Eval("Promotion").ToString() == "0" ? "" : "text-decoration: line-through;" %>" />
                                                            <% } %>
                                                        </div>
                                                        <div class="row promotion-money" style="display:none;">
                                                            <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                                                <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                                    <input type="checkbox" class="item-promotion" data-value="35000" data-id="1" style="margin-left: 0;" <%# Eval("Promotion").ToString() == "35000" ? "checked='checked'" : "" %> /> - 35.000 VNĐ </label>

                                                            </div><br />
                                                            <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                                                <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                                    <input type="checkbox" class="item-promotion" data-id="2" data-value="50000" style="margin-left: 0;" <%# Eval("Promotion").ToString() == "50000" ? "checked='checked'" : "" %>/> - 50.000 VNĐ </label>

                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="map-edit">
                                                        <div class="box-money" style="display:block;">
                                                            <%# Eval("Promotion").ToString() == "0" ? Convert.ToInt32(Eval("Quantity")) * Convert.ToInt32(Eval("Price")) * (100-Convert.ToInt32(Eval("VoucherPercent")))/100 : Convert.ToInt32(Eval("Quantity")) * Convert.ToInt32(Eval("Price")) - Convert.ToInt32(Eval("Promotion")) %>
                                                        </div>
                                                        <div class="edit-wp">
                                                            <% if (!_IsUpdate)
                                                                { %>
                                                            <a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),'<%# Eval("Name") %>','product')"
                                                                href="javascript://" title="Xóa"></a>
                                                            <% } %>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </div>                                        
                        </td>
                    </tr>

                    <tr class="tr-description tr-field-ahalf">
                        <td class="col-xs-2 left"><span>Tổng số tiền</span></td>
                        <td class="col-xs-10 right">
                            <span class="field-wp">
                                <asp:TextBox ID="TotalMoney" runat="server" ClientIDMode="Static" Text="0" ReadOnly="true"></asp:TextBox>
                                <span class="unit-money">VNĐ</span>
                            </span>
                        </td>
                    </tr>

                    <tr>
                        <td class="col-xs-2 left">Hình thức thanh toán</td>
                        <td class="col-xs-10 right" style="padding-bottom: 0;">
                            <div class="field-wp" style="height: auto; line-height: 18px;">
                                <asp:Repeater runat="server" ID="Rpt_PayMethod" ClientIDMode="Static">
                                    <ItemTemplate>
                                        <label class="lbl-cus-no-infor" style="margin-right: 5px; margin-bottom: 8px; cursor: pointer; font-weight: normal; font-size: 13px; padding: 2px 10px; background: #ddd; line-height: 21px;">
                                            <% if (billPayMethod > 0)
                                                                        { %>
                                            <input name="payMethod" type="radio" value="<%# Eval("Id") %>" <%# Convert.ToInt32(Eval("Id")) == billPayMethod ? "checked='checked'  onclick='addPayDate();bindRadioValue(\"#HDF_PayMethod\", \"payMethod\")'" : " onclick='clearPayDate();bindRadioValue(\"#HDF_PayMethod\", \"payMethod\")'" %> style="position: relative; top: 2px;" disabled="disabled" />
                                            <%# Eval("Name") %>
                                                                    <% }
    else
    { %>
                                            <input name="payMethod" type="radio" value="<%# Eval("Id") %>" <%# Container.ItemIndex == 0 ? "checked='checked'  onclick='addPayDate();bindRadioValue(\"#HDF_PayMethod\", \"payMethod\")'" : " onclick='clearPayDate();bindRadioValue(\"#HDF_PayMethod\", \"payMethod\")'" %> style="position: relative; top: 2px;" />
                                            <%# Eval("Name") %>
                                                                    <% } %>                                            
                                        </label>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td class="col-xs-2 left"></td>
                        <td class="col-xs-10 right" style="padding-bottom: 0;">
                            <div class="field-wp" style="height: auto; line-height: 18px; width: 160px; float: left; margin-right: 10px;">
                                <label style="font-style: italic;">Ngày lên đơn hàng</label>
                                <asp:TextBox runat="server" ID="BillDate" ClientIDMode="Static" CssClass="txtDateTime"></asp:TextBox>                                
                            </div>
                            <div class="field-wp" style="height: auto; line-height: 18px; width: 160px; float: left; margin-right: 10px;">
                                <label style="font-style: italic;">Ngày thanh toán</label>
                                <asp:TextBox runat="server" ID="PayDate" ClientIDMode="Static" CssClass="txtDateTime"></asp:TextBox>
                            </div>
                            <% if (_IsUpdate && billPayMethod == 2)
                                { %>
                            <div class="field-wp cod-pay" style="height: auto; line-height: 18px; width: 160px; float: left; margin-right: 10px; display:block;">
                                <label style="font-style: italic;">Phí COD</label>
                                <asp:TextBox runat="server" ID="MoneyCOD" ClientIDMode="Static" placeholder="VNĐ"></asp:TextBox>                                
                            </div>
                            <div class="field-wp cod-pay" style="height: auto; line-height: 18px; width: 160px; float: left; display:block;">
                                <label style="font-style: italic;">Phí thu thêm</label>
                                <asp:TextBox runat="server" ID="MoneyExtra" ClientIDMode="Static" placeholder="VNĐ"></asp:TextBox>
                            </div>
                            <% }
                            else
                            { %>
                            <div class="field-wp cod-pay" style="height: auto; line-height: 18px; width: 160px; float: left; margin-right: 10px;">
                                <label style="font-style: italic;">Phí COD</label>
                                <asp:TextBox runat="server" ID="TextBox1" ClientIDMode="Static" placeholder="VNĐ"></asp:TextBox>                                
                            </div>
                            <div class="field-wp cod-pay" style="height: auto; line-height: 18px; width: 160px; float: left;">
                                <label style="font-style: italic;">Phí thu thêm</label>
                                <asp:TextBox runat="server" ID="TextBox2" ClientIDMode="Static" placeholder="VNĐ"></asp:TextBox>
                            </div>
                            <% } %>
                        </td>
                    </tr>

                    <tr class="tr-description">
                        <td class="col-xs-2 left"><span>Ghi chú</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox TextMode="MultiLine" Rows="3" ID="Note" runat="server" ClientIDMode="Static"></asp:TextBox>
                            </span>
                        </td>
                    </tr>
                    
                    <tr class="tr-send">
                        <td class="col-xs-2 left"></td>
                        <td class="col-xs-10 right no-border">
                            <span class="field-wp">
                                <% if (!_IsUpdate)
                                    { %>
                                <asp:Panel ID="BtnSend" CssClass="btn-send" runat="server" ClientIDMode="Static" onclick="scriptBeforePost_Add()">Hoàn Tất</asp:Panel>
                                <% }
    else
    { %>
                                <asp:Panel ID="Panel1" CssClass="btn-send" runat="server" ClientIDMode="Static" onclick="scriptBeforePost_Update()">Hoàn Tất</asp:Panel>
                                <% } %>
                                <asp:Button ID="BtnFakeSend" runat="server" Text="Hoàn tất" 
                                    ClientIDMode="Static" OnClick="ExcAddOrUpdate" style="display:none;"></asp:Button>
                                <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>
                            </span>
                        </td>
                    </tr>
                    <asp:HiddenField runat="server" ID="UrlKeyField"  ClientIDMode="Static"/>
                    <asp:HiddenField runat="server" ID="ImgCoverField"  ClientIDMode="Static"/>
                    <asp:HiddenField ID="HDF_SocialThread" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_PayMethod" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_Suggestion_Code" ClientIDMode="Static" runat="server" />
                    <asp:HiddenField ID="HDF_Suggestion_Field" ClientIDMode="Static" runat="server" />
                    <asp:HiddenField ID="HDF_CustomerId" ClientIDMode="Static" runat="server" />  
                    <asp:HiddenField ID="HDF_ProductIds" runat="server" ClientIDMode="Static"/>                  
                    <asp:HiddenField ID="HDF_TotalMoney" runat="server" ClientIDMode="Static"/>
                </tbody>
            </table>
        </div>
    </div>
    <%-- end Add --%>
</div>

<!-- Danh mục sản phẩm -->
<div class="popup-product-wp popup-product-item">
    <div class="wp popup-product-head">
        <strong>Danh mục sản phẩm</strong>
    </div>
    <div class="wp popup-product-content" >
        <div class="wp popup-product-guide">
            <%--<div class="left">Hướng dẫn:</div>
            <div class="right">
                <p>- Click vào ô check box để chọn sản phẩm</p>
                <p>- Click vào ô số lượng để thay đổi số lượng sản phẩm</p>
            </div>--%>
            <label style="font-family: Roboto Condensed Bold; font-weight: normal; font-size: 14px; color: #222222; margin-right: 10px;">Chọn danh mục</label>
            <select onchange="showProductByCategory($(this))" id="ddlProductCategory" style="color: #222222; font-size: 14px; padding: 0 5px;">
                <asp:Repeater runat="server" ID="rptProductCategory">
                    <ItemTemplate>
                        <option value="<%# Eval("Id") %>" style="color: #222222; font-size: 14px;"><%# Eval("Name") %></option>
                    </ItemTemplate>
                </asp:Repeater>
            </select>
        </div>
        <div class="wp listing-product item-product">
            <table class="table" id="table-product">
                <thead>
                    <tr>
                        <th><input type="checkbox" /></th>
                        <th>STT</th>
                        <th>Tên sản phẩm</th>
                        <th>Mã sản phẩm</th>
                        <th>Đơn giá</th>
                        <th class="th-product-quantity">Số lượng</th>
                        <th>Giảm giá</th>
                        <th>Thành tiền</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="Rpt_Product" runat="server">
                        <ItemTemplate>
                            <tr data-cate="<%# Eval("CategoryId") %>">
                                <td class="td-product-checkbox item-product-checkbox">
                                    <input type="checkbox" value="<%# Eval("Id") %>"
                                        data-code="<%# Eval("Code") %>" data-name="<%# Eval("Name") %>"/>
                                </td>
                                <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                <td class="td-product-name"><%# Eval("Name") %></td>
                                <td class="td-product-code" data-id="<%# Eval("Id") %>" data-code="<%# Eval("Code") %>"><%# Eval("Code") %></td>
                                <td class="td-product-price" data-price="<%# Eval("Price") %>"><%# Eval("Price") %></td>
                                <td class="td-product-quantity">
                                    <input type="text" class="product-quantity" value="1" />                                
                                </td>
                                <td class="td-product-voucher" data-voucher="<%# Eval("VoucherPercent") %>">
                                    <div class="row">
                                        <input type="text" class="product-voucher" value="<%# Eval("VoucherPercent") %>" style="margin: 0 auto; float: none; text-align: center;width: 50px;" /> %
                                    </div>
                                    <div class="row promotion-money" style="display:none;">
                                        <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                            <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                <input type="checkbox" class="item-promotion" data-value="35000" data-id="1" style="margin-left: 0;" /> - 35.000 VNĐ </label>

                                        </div><br />
                                        <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                            <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                <input type="checkbox" class="item-promotion" data-id="2" data-value="50000" style="margin-left: 0;" /> - 50.000 VNĐ </label>

                                        </div>
                                    </div>
                                </td>
                                <td class="map-edit">
                                    <div class="box-money"></div>
                                    <div class="edit-wp">
                                        <a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),'<%# Eval("Name") %>','product')"
                                             href="javascript://" title="Xóa"></a>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>                                        
                </tbody>
            </table>
        </div> 
        <div class="wp btn-wp">
            <div class="popup-product-btn btn-complete" data-item="product">Hoàn tất</div>
            <div class="popup-product-btn btn-esc">Thoát</div>
        </div>
    </div>    
</div>
<!-- END Danh mục sản phẩm -->

<script src="/assets/js/jquery.mCustomScrollbar.js"></script>
<link href="/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
<script>
    var validate = { service: false, team: false, checkin: false, phone: false };

    jQuery(document).ready(function () {
        $("#glbBillSale").addClass("active");
        // Add active menu
        $("#subMenu .li-pending-complete").addClass("active");

        // bind SocialThreadId, PayMethodId
        bindRadioValue("#HDF_SocialThread", "socialThread");
        bindRadioValue("#HDF_PayMethod", "payMethod");

        // Bind staff suggestion
        Bind_Suggestion();
        AutoSelectStaff();

        /// Mở luôn iframe thêm mới khách hàng khi vào trang
        $("#CustomerCode").focus();

        // Mở box listing sản phẩm
        $(".show-product").bind("click", function () {
            $(this).parent().parent().find(".listing-product").show();
            $("select#ddlProductCategory").val($("select#ddlProductCategory option:first-child").val());
            showProductByCategory($("select#ddlProductCategory"));
        });

        $(".show-item").bind("click", function () {
            var item = $(this).attr("data-item");
            var classItem = ".popup-" + item + "-item";

            $(classItem).openEBPopup();
            ExcCheckboxItem();
            ExcQuantity();
            ExcCompletePopup();

            $('#EBPopup .listing-product').mCustomScrollbar({
                theme: "dark-2",
                scrollInertia: 100
            });

            $("#EBPopup .btn-esc").bind("click", function () { autoCloseEBPopup(0); });
        });

        //============================
        // Datepicker
        //============================
        $('.txtDateTime').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'vi',
            startDate: '2014/10/10',
            format: 'd/m/Y',
            dateonly: true,
            showHour: false,
            showMinute: false,
            timepicker: false,
            onChangeDateTime: function (dp, $input) {
                //$input.val($input.val().split(' ')[0]);
            },
            scrollMonth: false,
            scrollTime: false,
            scrollInput: false
        });

        //============================
        // Show System Message
        //============================ 
        var qs = getQueryStrings();
        showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);
    });

    function scriptBeforePost_Add() {
        if ($("#CustomerCode").val() == "") {
            showMsgSystem("Bạn chưa nhập khách hàng.", "warning");
        } else if (($("#HDF_ProductIds").val() == "" || $("#HDF_ProductIds").val() == "[]")) {
            showMsgSystem("Bạn chưa chọn mỹ phẩm.", "warning");
        } else {
            $("#BtnFakeSend").click();
        }
    }

    function scriptBeforePost_Update() {
        // condition...
        $("#BtnFakeSend").click();
    }

    function bindRadioValue(domId, radioName) { //SocialThreadId
        //$("#HDF_SocialThread").val($("input[name='socialThread']:checked").val());
        $(domId).val($("input[name='" + radioName + "']:checked").val());
    }

    function addPayDate() {
        $("#PayDate").val(getDate("/"));
        $(".cod-pay").hide();
    }

    function clearPayDate() {
        $("#PayDate").val("");
        $(".cod-pay").show();
    }

    function getDate(split) {
        split = split == null ? "/" : split;
        var date = new Date();
        return date.getDay() + split + (date.getMonth() + 1) + split + date.getFullYear();
    }

    // Xử lý mã khách hàng => bind khách hàng
    function LoadCustomer(code) {
        ajaxGetCustomer(code);
    }

    // Get Customer
    function ajaxGetCustomer(CustomerId) {
        if (CustomerId != undefined) {
            $.ajax({
                type: "POST",
                url: "/GUI/SystemService/Ajax/Suggestion.aspx/GetCustomer",
                data: '{CustomerId : "' + CustomerId + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var mission = JSON.parse(response.d);
                    if (mission.success) {
                        var customer = JSON.parse(mission.msg);
                        //$("#CustomerName").val(customer.Fullname).attr("disabled", "disabled");
                        //$("#CustomerName").attr("data-code", customer.Customer_Code);
                        //$("#CustomerName").attr("data-id", customer.Id);
                        //$("#HDF_CustomerCode").val(customer.Id);
                        //$("#HDF_Suggestion_Code").val(customer.Id);
                        //validatePhone(customer.Customer_Code);
                        bindCustomerField(customer);
                        // $("#trFlowInfor").hide();
                    } else {
                        $("#CustomerName").val("");
                        $("#CustomerName").attr("data-code", "");
                        $("#CustomerName").attr("data-id", "");
                        $("#HDF_CustomerCode").val("");
                        $("#HDF_Suggestion_Code").val("");
                        //var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                        //showMsgSystem(msg, "warning");
                    }
                },
                failure: function (response) { alert(response.d); }
            });
        }
    }

    /// clear data từ input khách hàng
    function clearDataFromCustomerInput() {
        $("#CustomerCode").val("").removeAttr("disabled");
        $("#CustomerName").val("").removeAttr("disabled");
        $("#HDF_CustomerCode").val(0);
        $("#trFlowInfor").show();
    }

    /// kiểm tra và bind dữ liệu khách hàng bằng số điện thoại khi blur khỏi input số điện thoại
    function checkBindCustomerDataByPhone(This) {
        if (validatePhone(This)) {
            $.ajax({
                type: "POST",
                url: "/GUI/SystemService/Ajax/Suggestion.aspx/GetCustomerByPhone",
                data: '{phone : "' + This.val() + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var mission = JSON.parse(response.d);
                    if (mission.success) {
                        var customer = JSON.parse(mission.msg);
                        if (customer.Phone != "") {
                            //$("#CustomerCode").val(customer.Phone);
                            //$("#CustomerName").val(customer.Fullname).attr("disabled", "disabled");
                            //$("#HDF_CustomerCode").val(customer.Id);
                            bindCustomerField(customer);
                            //$("#trFlowInfor").hide();
                        }
                    } else {
                        //
                    }
                },
                failure: function (response) { alert(response.d); }
            });
        }
    }

    function bindCustomerField(customer) {
        $("#CustomerName").val(customer.Fullname).attr("disabled", "disabled");
        $("#CustomerName").attr("data-code", customer.Customer_Code);
        $("#CustomerName").attr("data-id", customer.Id);
        $("#HDF_CustomerId").val(customer.Id);
        $("#HDF_Suggestion_Code").val(customer.Id);
        
        var cityId = parseInt(customer.CityId);
        var districtId = parseInt(customer.DistrictId);
        if (!isNaN(cityId) && cityId > 0) {
            $("#City").val(customer.CityId).change();
            $("#City").click();
        }
        if (!isNaN(districtId) && districtId > 0) {
            $("#District").val(customer.DistrictId);
        }
        $("#Address").val(customer.Address);
        // bind social thread
        bindSocialThread(customer.Info_Flow);
        validatePhone(customer.Phone);
        $(".eb-select-data").hide();
    }

    function bindSocialThread(threadId) {
        $("input[name='socialThread']").each(function () {
            if ($(this).val() == threadId) {
                $(this).prop("checked", true);
                $("#HDF_SocialThread").val(threadId);
            }
        });
    }

    function showProductByCategory(This) {
        $("table#table-product tbody tr").each(function () {
            if ($(this).attr("data-cate") != This.val()) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });
        if ($(".popup-product-item").isOpenEBPopup()) {
            $(".popup-product-item").alignment();
        }
    }

    /// Validate phone
    function validatePhone(This) {
        var phone, ret = false;
        if (typeof This == 'string') {
            phone = This.replace(/\s+/, "");
        } else {
            phone = This.val().replace(/\s+/, "");
        }
        if (phone == "") {
            hideValidatePhone();
            ret = true;
        } else if (!checkStringNumber(phone) || !checkPhoneLength(phone, [10, 11])) {
            showValidatePhone();
            ret = false;
        } else {
            hideValidatePhone();
            ret = true;
        }
        return ret;
    }

    function validatePhoneByString(phone) {
        var phone = This.val().replace(/\s+/, "");
        if (phone == "") {
            hideValidatePhone();
        } else if (!checkStringNumber(phone) || !checkPhoneLength(phone, [10, 11])) {
            showValidatePhone();
        } else {
            hideValidatePhone();
        }
    }

    function validateTypingNumber(event, This) {
        if (!checkTypingNumber(event) || (This.val().length >= 11 && !isAllowTyping(event))) {
            event.preventDefault();
            showValidatePhone();
        } else if (!isAllowTyping(event) && isFuncKeyCode(event)) {
            event.preventDefault();
        } else {
            hideValidatePhone();
        }
    }

    function showValidatePhone() {
        $(".validate-phone").show();
        validate.phone = false;
    }

    function hideValidatePhone() {
        $(".validate-phone").hide();
        validate.phone = true;
    }
    /// End Validate phone

    //===== End Function to execute product
    // Thêm sản phẩm từ danh sách sản phẩm nổi bật
    function pushQuickData(This, typeData) {
        var Code = This.attr("data-code");
        if (This.is(":checked")) {
            var Dom = $("#table-" + typeData).find("input[data-code='" + Code + "']").parent().parent().clone().find("td:first-child").remove().end(),
            quantity = Dom.find(".product-quantity").val(),
            price = Dom.find(".td-product-price").data("price"),
            voucher = Dom.find("input.product-voucher").val();

            // check value            
            price = price.toString().trim() != "" ? parseInt(price) : 0;
            quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
            voucher = voucher.trim() != "" ? parseInt(voucher) : 0;

            Dom.find(".box-money").text(FormatPrice(quantity * price * (100 - voucher) / 100)).show();
            $("#table-item-" + typeData).append(Dom);
            $(".item-" + typeData).show();
        } else {
            $("#table-item-" + typeData).find(".td-product-code[data-code='" + Code + "']").parent().remove();
        }

        TotalMoney();
        getProductIds();
        ExcQuantity();
        UpdateItemOrder($("#table-item-" + typeData).find("tbody"));
    }
    // Xử lý khi chọn checkbox sản phẩm
    function ExcCheckboxItem() {
        $("#EBPopup .td-product-checkbox input[type='checkbox']").unbind("click").bind("click", function () {
            var obj = $(this).parent().parent(),
                boxMoney = obj.find(".box-money"),
                price = obj.find(".td-product-price").data("price"),
                quantity = obj.find("input.product-quantity").val(),
                voucher = obj.find("input.product-voucher").val(),
                checked = $(this).is(":checked"),
                item = obj.attr("data-item"),
                promotion = 0;
            obj.find(".promotion-money input[type='checkbox']:checked").each(function () {
                var value = $(this).attr("data-value").trim();
                promotion += (value != "" ? parseInt(value) : 0);
            });

            // check value
            price = price.toString().trim() != "" ? parseInt(price) : 0;
            quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
            voucher = voucher.trim() != "" ? parseInt(voucher) : 0;


            if (checked) {
                boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100 - promotion)).show();
            } else {
                boxMoney.text("").hide();
            }

        });
    }

    // Số lượng | Quantity
    function ExcQuantity() {
        $("input.product-quantity").bind("change", function () {
            var obj = $(this).parent().parent(),
                quantity = $(this).val(),
                price = obj.find(".td-product-price").data("price"),
                voucher = obj.find("input.product-voucher").val(),
                boxMoney = obj.find(".box-money"),
                promotion = 0;

            obj.find(".promotion-money input[type='checkbox']:checked").each(function () {
                var value = $(this).attr("data-value").trim();
                promotion += (value != "" ? parseInt(value) : 0);
            });

            // check value
            price = price.toString().trim() != "" ? parseInt(price) : 0;
            quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
            voucher = voucher.trim() != "" ? parseInt(voucher) : 0;

            boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100 - promotion)).show();
            TotalMoney();
            getProductIds();
        });
        $("input.product-voucher").bind("change", function () {
            var obj = $(this).parent().parent().parent(),
                quantity = obj.find("input.product-quantity").val(),
                price = obj.find(".td-product-price").data("price"),
                voucher = obj.find("input.product-voucher").val(),
                boxMoney = obj.find(".box-money"),
                promotion = 0;

            obj.find(".promotion-money input[type='checkbox']:checked").each(function () {
                var value = $(this).attr("data-value").trim();
                promotion += (value != "" ? parseInt(value) : 0);
            });

            // check value
            price = price.toString().trim() != "" ? parseInt(price) : 0;
            quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
            voucher = voucher.trim() != "" ? parseInt(voucher) : 0;

            if (promotion == 0) {
                boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100)).show();
            } else {
                obj.find("input.product-voucher").val(0);
            }

            TotalMoney();
            getProductIds();
        });
        $("input.item-promotion").bind("click", function () {
            var obj = $(this).parent().parent().parent().parent().parent(),
                quantity = obj.find("input.product-quantity").val(),
                price = obj.find(".td-product-price").data("price"),
                voucher = obj.find("input.product-voucher").val(),
                boxMoney = obj.find(".box-money"),
                promotion = $(this).attr("data-value"),
                _This = $(this),
                isChecked = $(this).is(":checked");
            obj.find(".promotion-money input[type='checkbox']:checked").prop("checked", false);
            if (isChecked) {
                $(this).prop("checked", true);
                promotion = promotion.trim() != "" ? parseInt(promotion) : 0;
                obj.find("input.product-voucher").val(0);
            } else {
                promotion = 0;
            }

            // check value
            price = price.toString().trim() != "" ? parseInt(price) : 0;
            quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
            voucher = voucher.trim() != "" ? parseInt(voucher) : 0;
            //promotion = promotion.trim() != "" ? parseInt(promotion) : 0;

            if (promotion > 0) {
                boxMoney.text(FormatPrice(quantity * price - promotion)).show();
                obj.find("input.product-voucher").css({ "text-decoration": "line-through" });
            } else {
                boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100)).show();
                obj.find("input.product-voucher").css({ "text-decoration": "none" });
            }
            TotalMoney();
            getProductIds();
        });
    }

    // Xử lý click hoàn tất chọn item từ popup
    function ExcCompletePopup() {
        $("#EBPopup .btn-complete").unbind("click").bind("click", function () {
            var item = $(this).attr("data-item"),
                tableItem = $("table.table-item-" + item + " tbody"),
                objTmp;

            $("#EBPopup .item-product-checkbox input[type='checkbox']:checked").each(function () {
                var Code = $(this).attr("data-code");
                if (!ExcCheckItemIsChose(Code, tableItem)) {
                    objTmp = $(this).parent().parent().clone().find("td:first-child").remove().end();
                    tableItem.append(objTmp);
                }
            });

            TotalMoney();
            getProductIds();
            ExcQuantity();
            UpdateItemOrder(tableItem);
            autoCloseEBPopup(0);
        });
    }

    // Check item đã được chọn
    function ExcCheckItemIsChose(Code, itemClass) {
        var result = false;
        $(itemClass).find(".td-product-code").each(function () {
            var _Code = $(this).text().trim();
            if (_Code == Code)
                result = true;
        });
        return result;
    }

    // Remove item đã được chọn
    function RemoveItem(THIS, name, itemName) {
        // Confirm
        $(".confirm-yn").openEBPopup();

        $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn hủy [ " + name + " ] ?");
        $("#EBPopup .yn-yes").bind("click", function () {
            var Code = THIS.find(".td-product-code").attr("data-code");
            $("#quick-" + itemName).find("input[data-code='" + Code + "']").removeAttr("checked");
            THIS.remove();
            TotalMoney();
            getProductIds();
            UpdateItemDisplay(itemName);
            autoCloseEBPopup(0);
        });
        $("#EBPopup .yn-no").bind("click", function () {
            autoCloseEBPopup(0);
        });
    }

    // Remove item (đã lưu trong phiếu pending) được chọn
    function RemoveItemPending(THIS, itemName, itemId, itemType) {
        // Confirm
        $(".confirm-yn").openEBPopup();

        $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn hủy [ " + itemName + " ] ?");
        $("#EBPopup .yn-yes").bind("click", function () {
            THIS.remove();
            TotalMoney();
            getProductIds();
            UpdateItemDisplay(itemType);
            autoCloseEBPopup(0);
            $.ajax({
                type: "POST",
                url: "/GUI/SystemService/Ajax/Del.aspx/Delele_Item_Pending",
                data: '{itemId : ' + itemId + ', BillId : ' + $("#HDF_BillId").val() + ', itemType : "' + itemType + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var mission = JSON.parse(response.d);
                    if (mission.success) {
                        delSuccess();
                        Row.remove();
                    } else {
                        delFailed();
                    }
                },
                failure: function (response) { alert(response.d); }
            });
        });
        $("#EBPopup .yn-no").bind("click", function () {
            autoCloseEBPopup(0);
        });
    }

    // Update table display
    function UpdateItemDisplay(itemName) {
        var dom = $(".table-item-" + itemName);
        var len = dom.find("tbody tr").length;
        if (len == 0) {
            dom.parent().hide();
        } else {
            UpdateItemOrder(dom.find("tbody"));
        }
    }

    // Update order item
    function UpdateItemOrder(dom) {
        var index = 1;
        dom.find("tr").each(function () {
            $(this).find("td.td-product-index").text(index);
            index++;
        });
    }

    function TotalMoney() {
        var Money = 0;
        $(".fe-service-table-add .box-money:visible").each(function () {
            Money += parseInt($(this).text().replace(/\./gm, ""));
        });
        $("#TotalMoney").val(FormatPrice(Money));
        $("#HDF_TotalMoney").val(Money);
    }

    function showMsgSystem(msg, status) {
        $("#MsgSystem").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);
        setTimeout(function () {
            $("#MsgSystem").fadeTo("slow", 0, function () {
                $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 1);
            });
        }, 5000);
        $('html, body').animate({ scrollTop: 0 }, 'fast');

    }

    function hideMsgSystem() {
        $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 0);
    }

    function getProductIds() {
        var Ids = [];
        var prd = {};
        $("table.table-item-product tbody tr").each(function () {
            prd = {};
            var THIS = $(this);

            var Id = THIS.find("td.td-product-code").attr("data-id"),
                Code = THIS.find("td.td-product-code").text().trim(),
                Name = THIS.find("td.td-product-name").text().trim(),
                Price = THIS.find(".td-product-price").attr("data-price"),
                Quantity = THIS.find("input.product-quantity").val(),
                VoucherPercent = THIS.find("input.product-voucher").val(),
                Promotion = 0;

            THIS.find(".promotion-money input[type='checkbox']:checked").each(function () {
                var value = $(this).attr("data-value").trim();
                Promotion += (value != "" ? parseInt(value) : 0);
            });

            // check value
            Id = Id.toString().trim() != "" ? parseInt(Id) : 0;
            Price = Price.toString().trim() != "" ? parseInt(Price) : 0;
            Quantity = Quantity.trim() != "" ? parseInt(Quantity) : 0;
            VoucherPercent = VoucherPercent.trim() != "" ? parseInt(VoucherPercent) : 0;

            prd.Id = Id;
            prd.Code = Code;
            prd.Name = Name;
            prd.Price = Price;
            prd.Quantity = Quantity;
            prd.VoucherPercent = VoucherPercent;
            prd.Promotion = Promotion;

            Ids.push(prd);

        });
        Ids = JSON.stringify(Ids);
        $("#HDF_ProductIds").val(Ids);
    }
    //===== End Function to execute product

    //============================
    // Suggestion Functions
    //============================
    function Bind_Suggestion() {
        $(".eb-suggestion").bind("keyup", function (e) {
            if (e.keyCode == 40) {
                UpDownListSuggest($(this));
            } else {
                Call_Suggestion($(this));
            }
        });
        $(".eb-suggestion").bind("focus", function () {
            Call_Suggestion($(this));
        });
        $(".eb-suggestion").bind("blur", function () {
            //Exc_To_Reset_Suggestion($(this));
        });
        $(window).bind("click", function (e) {
            if ((!e.target.className.match("eb-select") && e.target.parentElement.className.match("ul-listing-suggestion")) ||
                (!e.target.className.match("eb-select") && !e.target.parentElement.className.match("ul-listing-suggestion"))) {
                //EBSelect_HideBox();
            }
        });
    }

    function UpDownListSuggest(This) {
        var UlSgt = This.parent().find(".ul-listing-suggestion"),
            index = 0,
            LisLen = UlSgt.find(">li").length - 1,
            Value;

        This.blur();
        UlSgt.find(">li.active").removeClass("active");
        UlSgt.find(">li:eq(" + index + ")").addClass("active");

        $(window).unbind("keydown").bind("keydown", function (e) {
            if (e.keyCode == 40) {
                if (index == LisLen) return false;
                UlSgt.find(">li.active").removeClass("active");
                UlSgt.find(">li:eq(" + (++index) + ")").addClass("active");
                return false;
            } else if (e.keyCode == 38) {
                if (index == 0) return false;
                UlSgt.find(">li.active").removeClass("active");
                UlSgt.find(">li:eq(" + (--index) + ")").addClass("active");
                return false;
            } else if (e.keyCode == 13) {
                // Bind data to HDF Field
                var THIS = UlSgt.find(">li.active");
                //var Value = THIS.text().trim();
                var Value = THIS.attr("data-id");
                var dataField = This.attr("data-field");

                BindIdToHDF(THIS, Value, dataField, "#HDF_Suggestion_Code", "#HDF_Suggestion_Field", This);
                EBSelect_HideBox();
            }
        });
    }

    function Exc_To_Reset_Suggestion(This) {
        var value = This.val();
        if (value == "") {
            $(".eb-suggestion").each(function () {
                var THIS = $(this);
                var sgValue = THIS.val();
                if (sgValue != "") {
                    BindIdToHDF(THIS, sgValue, THIS.attr("data-field"), "#HDF_Suggestion_Code", "#HDF_Suggestion_Field", THIS);
                    return false;
                }
            });
        }
    }

    function Call_Suggestion(This) {
        var text = This.val(),
            field = This.attr("data-field");
        Suggestion(This, text, field);
    }

    function Suggestion(This, text, field) {
        var This = This;
        var text = text || "";
        var field = field || "";
        var InputDomId;
        var HDF_Sgt_Code = "#HDF_Suggestion_Code";
        var HDF_Sgt_Field = "#HDF_Suggestion_Field";
        var Callback = This.attr("data-callback");

        if (text == "") return false;

        switch (field) {
            case "customer.name": InputDomId = "#CustomerName"; break;
            case "customer.phone": InputDomId = "#CustomerPhone"; break;
            case "customer.code": InputDomId = "#CustomerCode"; break;
            case "bill.code": InputDomId = "#BillCode"; break;
        }

        $.ajax({
            type: "POST",
            url: "/GUI/SystemService/Ajax/Suggestion.aspx/Suggestion_Customer",
            data: '{field : "' + field + '", text : "' + text + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                var mission = JSON.parse(response.d);
                if (mission.success) {
                    var OBJ = JSON.parse(mission.msg);
                    if (OBJ != null && OBJ.length > 0) {
                        var lis = "";
                        $.each(OBJ, function (i, v) {
                            lis += "<li data-code='" + v.Customer_Code + "' data-id='" + v.CustomerId + "'" +
                                         "onclick=\"BindIdToHDF($(this),'" + v.CustomerId + "','" + field + "','" + HDF_Sgt_Code +
                                         "','" + HDF_Sgt_Field + "','" + InputDomId + "')\" data-callback='" + Callback + "'>" +
                                        v.Value +
                                    "</li>";
                        });
                        This.parent().find("ul.ul-listing-suggestion").empty().append(lis).parent().show();
                        This.parent().find(".eb-select-data").show();
                    } else {
                        This.parent().find(".eb-select-data").hide().find("ul.ul-listing-suggestion").empty();
                    }
                } else {
                    This.parent().find("ul.ul-listing-suggestion").empty();
                    var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                    showMsgSystem(msg, "warning");
                }
            },
            failure: function (response) { alert(response.d); }
        });
    }

    function BindIdToHDF(THIS, Code, Field, HDF_Sgt_Code, HDF_Sgt_Field, Input_DomId) {
        var text = THIS.text().trim();
        $("input.eb-suggestion").val("");
        $(HDF_Sgt_Code).val(Code);
        $(HDF_Sgt_Field).val(Field);
        $(Input_DomId).val(text);
        $(Input_DomId).parent().find(".eb-select-data").hide();
        if (THIS.attr("data-callback") != "") {
            window[THIS.attr("data-callback")].call();
        }
        // Auto post server
        //$("#BtnFakeUP").click();
    }

    function EBSelect_HideBox() {
        $(".eb-select-data").hide();
        $("ul.ul-listing-suggestion li.active").removeClass("active");
    }

    function Callback_UpdateCustomerCode() {
        LoadCustomer($("#HDF_Suggestion_Code").val());
    }

    function CustomerInputOnchange(This) {
        ajaxGetCustomer(This.attr("data-id"));
    }

    //===================
    // Auto select staff
    //===================
    function AutoSelectStaff() {
        $(".auto-select").bind("keyup", function (e) {
            var This = $(this);
            var StaffType = This.attr("data-field");
            var code = parseInt(This.val());
            if (!isNaN(code)) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Ajax/Suggestion.aspx/Suggestion_OneStaff",
                    data: '{code : "' + code + '", SalonId : ' + $("#HDF_SalonId").val() + ', StaffType : "' + StaffType + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            var OBJ = JSON.parse(mission.msg);
                            if (OBJ.length > 0) {
                                var lis = "";
                                This.parent().find(".fake-value").text(This.val() + "- " + OBJ[0].Fullname);
                                $("#" + StaffType).val(OBJ[0].Id);
                            } else {
                                This.parent().find(".fake-value").text("");
                                $("#" + StaffType).val("");
                            }
                        } else {
                            This.parent().find("ul.ul-listing-suggestion").empty();
                            var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                            showMsgSystem(msg, "warning");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            } else {
                This.parent().find(".fake-value").text("");
                $("#" + StaffType).val("");
            }
        });
    }
</script>

</asp:Panel>
</asp:Content>

