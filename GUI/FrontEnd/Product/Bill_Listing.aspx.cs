﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Script.Serialization;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.FrontEnd.ProductSale
{
    public partial class Bill_Listing : System.Web.UI.Page
    {
        private string PageID = "DH_DS";
        protected Paging PAGING = new Paging();
        private bool Perm_Access = false;
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_Develop = false;

        CultureInfo culture = new CultureInfo("vi-VN");
        protected cls_total_report totalReport = new cls_total_report();
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_Develop = permissionModel.GetActionByActionNameAndPageId("Perm_Develop", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();

            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                Bind_Paging();
                bindPayMethod();
                bindBill();
                RemoveLoading();
            }
        }

        private string genSql()
        {
            int integer;
            var sql = "";
            var whereTime = "";
            var wherePayMethod = "";
            var wherePaid = "";
            var timeFrom = new DateTime();
            var timeTo = new DateTime();
            var payMethod = int.TryParse(ddlPayMethod.SelectedValue, out integer) ? integer : 0;
            var paid = int.TryParse(ddlPaid.SelectedValue, out integer) ? integer : 0;
            if (TxtDateTimeFrom.Text != "")
            {
                timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                if (TxtDateTimeTo.Text != "")
                {
                    timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                }
                else
                {
                    timeTo = timeFrom;
                }
                whereTime = " and bill.CreatedDate between '" + string.Format("{0:yyyy/MM/dd}", timeFrom) + "' and '" + string.Format("{0:yyyy/MM/dd}", timeTo) + "'";
            }

            if (payMethod > 0)
            {
                wherePayMethod = " and bill.PayMethodId = " + payMethod;
            }

            if (paid == 1)
            {
                wherePaid = " and bill.Paid = 1";
            }
            else if (paid == 2)
            {
                wherePaid = " and bill.Paid = 0";
            }

            sql = @"select bill.*, customer.Fullname as CustomerName, customer.Phone as CustomerPhone, payMethod.Name as payMethodName
                    from BillService as bill
                    inner join Customer as customer
                    on bill.CustomerId = customer.Id
                    inner join PayMethod as payMethod
                    on bill.PayMethodId = payMethod.Id
                    where bill.IsOnline = 1
                    and bill.IsDelete != 1 " + whereTime + wherePaid + wherePayMethod +
                    " order by bill.Id desc";
            return sql;
        }

        private void bindBill()
        {
            using (var db = new Solution_30shineEntities())
            {
                var sql = genSql();
                if (sql != "")
                {
                    var bills = db.Database.SqlQuery<cls_bill>(sql).ToList();
                    if (bills.Count > 0)
                    {
                        int integer;
                        var loop = 0;
                        var productString = "";
                        var serialize = new JavaScriptSerializer();
                        var productList = new List<ProductBasic>();
                        foreach (var v in bills)
                        {
                            if (v.ProductIds != "")
                            {
                                serialize = new JavaScriptSerializer();
                                productList = serialize.Deserialize<List<ProductBasic>>(v.ProductIds);
                                productString = "";
                                if (productList.Count > 0)
                                {
                                    var i = 0;
                                    var First = "";
                                    foreach (var v2 in productList)
                                    {
                                        // Price lưu ở đây là giá nhập (không phải giá bán)
                                        First = i == 0 ? " class='first'" : "";
                                        productString += "<tr" + First + ">" +
                                                            "<td class='col-xs-7'>" +
                                                                    "<a href='/admin/san-pham/" + v2.Id + ".html' target='_blank'>" +
                                                                    v2.Name +
                                                                    "</a>" +
                                                            "</td>" +
                                                            "<td class='col-xs-2'>" + v2.Quantity + "</td>" +
                                                            "<td class='col-xs-3 no-bdr be-report-price'>" + String.Format("{0:#,###}",v2.Price * v2.Quantity).Replace(',', '.') + "</td>" +
                                                        "</tr>";
                                        i++;
                                    }
                                }
                                else
                                {
                                    productString += "<tr class='first'><td class='no-bdr'>-</td></tr>";
                                }
                                bills[loop].productSet = productString;
                            }

                            if (v.Paid == true)
                            {
                                bills[loop].billStatusName = "Đã thanh toán";
                            }
                            else
                            {
                                bills[loop].billStatusName = "Chưa thanh toán";
                            }
                            loop++;
                            v.FeeCOD = v.FeeCOD != null ? v.FeeCOD.Value : 0;
                            v.FeeExtra = v.FeeExtra != null ? v.FeeExtra.Value : 0;
                            totalReport.totalBill++;
                            totalReport.totalMoney += v.TotalMoney.Value;
                            totalReport.feeCOD += v.FeeCOD.Value;
                            totalReport.feeExtra += v.FeeExtra.Value;
                            if (v.Paid == true)
                            {
                                totalReport.paidMoney += v.TotalMoney.Value + v.FeeCOD.Value + v.FeeExtra.Value;
                            }
                            else
                            {
                                totalReport.oweMoney += v.TotalMoney.Value + v.FeeCOD.Value + v.FeeExtra.Value;
                            }                            
                        }
                    }
                    RptBill.DataSource = bills.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                    RptBill.DataBind();
                }
            }
        }

        private void bindPayMethod()
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = db.PayMethods.Where(w => w.IsDelete != true && w.Publish == true).OrderBy(o => o.Order).ToList();
                var item = new PayMethod();
                item.Name = "Phương thức thanh toán";
                item.Id = 0;
                list.Insert(0, item);
                ddlPayMethod.DataTextField = "Name";
                ddlPayMethod.DataValueField = "Id";
                ddlPayMethod.DataSource = list;
                ddlPayMethod.DataBind();                
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
            bindBill();
            RemoveLoading();
        }

        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                var Count = 0;
                var sql = genSql();
                if (sql != "")
                {
                    Count = db.Database.SqlQuery<cls_bill>(genSql()).Count();
                }                
                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }


        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }

        public class cls_bill : BillService
        {
            public string productSet { get; set; }
            public string payMethodName { get; set; }
            public string billStatusName { get; set; }
        }

        public class cls_total_report
        {
            /// <summary>
            /// Tổng số bill
            /// </summary>
            public int totalBill { get; set; }
            /// <summary>
            /// Tổng doanh thu
            /// </summary>
            public int totalMoney { get; set; }
            /// <summary>
            /// Số tiền đã thanh toán
            /// </summary>
            public int paidMoney { get; set; }
            /// <summary>
            /// Phí COD
            /// </summary>
            public int feeCOD { get; set; }
            /// <summary>
            /// Phí thu thêm
            /// </summary>
            public int feeExtra { get; set; }
            /// <summary>
            /// Công nợ
            /// </summary>
            public int oweMoney { get; set; }
        }
    }
}