﻿using _30shine.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.UIControls
{
    public partial class Header_V2 : System.Web.UI.UserControl
    {
        protected string _User = "";
        protected string Permission = "";
        protected int? TimeLivePassword;
        protected void Page_Load(object sender, EventArgs e)
        {
            _User = Session["User_Name"].ToString();
            if (!IsPostBack)
            {
                var time = Convert.ToDateTime(Session["LastChangePassword"]).AddDays(Convert.ToInt32(Session["TimeLivePassword"])).Date - DateTime.Now.Date;
                TimeLivePassword = time.Days;
                if (time.Days <= 0 && Session["UserID"].ToString() != "0")
                {
                    UIHelpers.Redirect("/doi-mat-khau.html");
                }
            }
        }

        private void SetByPermission()
        {
            Dictionary<string, List<string>> GlbMenu = new Dictionary<string, List<string>>()
            { 
                /// root
                {
                    "root", new List<string>() {
                        "glbCustomer", /// Menu Khách hàng
                        "glbBooking",

                        "glbAddStaff", ///Thêm mới nhân viên
                        "glLevelup",
                        "glLevelup1",
                        "glLevelup2",
                        //"glbAddStaff_V2",///Thêm mới nhân viên V2
                        //"glbCustomerFirstTime", //Menu thống kê khách đến lần đầu chưa booking
                        "glbService", /// Hóa đơn
                        "glbExportGoods", /// Xuất vật tư
                        "glbExportGoods_Salon", // Xuất vật tư - Cho salon
                        "glbExportGoods_Staff", // Xuất vật từ - Cho nhân viên
                        "glbExportGoods_Report_By_Staff", // Thống kê số bill dịch vụ trong lần xuất
                        "glbTimeKeeping", /// Chấm công
                        "glbWorkDay", // Chấm công - Công nhật
                        "glbthongke",
                        "glbAdminContent", /// Quản lý nội dung
                        "glbAdminCommonManagerment", // Quản lý chung
                        "glbAdminService", // Quản lý nội dung - Dịch vụ
                        "glbAdminServiceListing", // Quản lý nội dung - Dịch vụ - Danh sách
                        "glbAdminServiceConfigSalon", // Quản lý nội dung - Dịch vụ - Cấu hình hiển thị theo salon
                        "glbAdminProduct", // Quản lý nội dung - Sản phẩm
                         "Li20", // Quản lý nhóm sản phẩm MN_LK
                        "glbAdminCategory", // Quản lý nội dung - Danh mục
                        "glbAdminSocialThread", // Quản lý nội dung - Nguồn thông tin
                        "glbAdminSalon", // Quản lý nội dung - Salon
                        "glbAdminStaff", // Quản lý nội dung - Nhân sự
                        "mlStaff", // Quản lý nhân sự
                        //"glbAdminAccount", // Quản lý nội dung - Tài khoản
                        "glbAdminStaffType", // Quản lý nội dung - Bộ phận  
                        "glbAdminSkillLevel", // Quản lý nội dung - Bậc kỹ năng
                        "glbAdminConfigSalary", // Quản lý nội dung - Cấu hình lương
                        "glbAdminProcedure", // Quản lý nội dung - Quy trình / Quy định
                        "glbAdminBookingHour", // Quản lý nội dung - Cấu hình khung giờ     
                        "glbAdminBooking", // Quản lý nội dung - Thống kế Booking    
                        "glbAdminBooking_Test", // Quản lý nội dung - Thống kế Booking  
                        "glbAdminBooking_Call", // Gọi đặt lịch               
                        "gblAdminStatisticBooking",//Thống kế booking
                        "gblAdminStatisticBooking_Data", // Thống kê dữ liệu booking
                        "glbAdminBooking_GoiHoTroDatLich", //Danh sách gọi hỗ trợ đặt lịch
                        "glbAdminNotification", // Quản lý nội dung - Notification
                        "glbAdminSales", /// Báo cáo   
                        "glbAdminReportSales", // Báo cáo - Tổng hợp
                        "glbAdminReportProduct", // Báo cáo - Mỹ phẩm
                        "glbAdminReportService", // Báo cáo - Dịch vụ
                        //"glbAdminReportOldPending", // Báo cáo - Pending cũ
                        "glbAdminReportStaff", // Báo cáo - Nhân viên
                        "glbAdminReportSalary", // Báo cáo - Tính lương
                        "glbAdminReport", /// Biểu đồ
                        "glbQuanlity", /// Chất lượng
                        "glbQbyBill", // Chất lượng - Theo hóa đơn
                        "glbQbyServey", // Chất lượng - Theo khảo sát     
                        "glbAdminSlide", // Quản lý slide - API
                        "glbAdminHairMode", // Quản lý kiểu tóc - API
                        "glbBillSale", // Quản lý đơn hàng
                        "glbAdminHairAtt", //Quản lý thuộc tính tóc
                        "glbAdminSkinAtt", //Quản lý thuộc tính da
                        "glbInventory", // Quản lý tồn kho
                        "glbMistake", // Chất lượng - Điểm trừ                        
                        "glbAdminFundOffenItem", // Ngân sách - Khoản thường xuyên
                        "glbAdminFund", // Ngân sách
                        "glbCheckClear", // Chất lượng - vệ sinh salon
                        "glbCheckCSVC", // Chất lượng - CSVC
                        // Stylist4Men
                        "glbS4M",
                        "glbS4MClass", // Lớp học
                        "glbS4MStudent", // Học viên
                        "glbS4MTrainingImage", // Ảnh training khách hàng
                        "glbS4MCheckImageCut", // Check ảnh cắt KCS
                        "glbListBack", //Danh sach chua quay lai sau 1 thang
                        "glbAdminKetQuaKinhDoanh",

                        "glbTuyendungV2",
                        "glbCreatCV",
                        "glbCreateNewCV",
                        "glbReportCV",
                        "glbChangeCV",
                        "glbConfirmSkill",
                        "glbReportConfirm",
                        "glbReportResult",
                        "glbSkillManager",
                        "glbTesterManager",
                       // "glbTuyendungV2",

                        // Thử nghiệm lương doanh số checkin
                        "glbLuongDoanhSoCheckin",
                        "report_checkin_salary",

                        // Quản lý xuất hàng nội bộ
                        "glbDatHangNoiBo",
                        "glbDatHangNoiBo_ChoSalon",
                        "glbDatHangNoiBo_ChoKho",
                        "glbDatHangNoiBo_XuatHoaDon",
                        "glbDatHangNoiBo_DanhSachTraThieu",

                        // Check ảnh ngẫu nhiên
                        "glbThongKeAnh",

                        // Sự kiện
                        "glbSuKien",
                        "glb_Sukien_TeamThiDua",

                        // Nhân sự - Cấu hình Group Team
                        "glbAdminGroupTeam",
                        "glbAdminLevelup",
                        // Marketing
                        "glbMarketing", // Module Marketing
                        "glbMarketing_Config", // Cấu hình
                        "glbMarketing_ItemDaily", // Cấu hình Item nhập hàng ngày
                        "glbMarketing_Import", // Nhập chi phí
                        "glbMarketing_Import_Daily", // Nhập chi phí hàng ngày
                        "glbMarketing_Import_Plan", // Nhập chi phí chiến dịch
                        "glbMarketing_Report", // Báo cáo
                        "glbMarketing_Report_CP",
                        "glbMarketing_Report_NS",

                        // Quản lý dòng tiền
                        "glbAdminMoneyManager",

                        // Quản lý hàng dịch vụ
                        "glbHangDV"
                    }
                },
                /// admin
                {
                    "admin", new List<string>() {
                        "glbCustomer", /// Khách hàng
                        "glbBooking",
                        "glbAddStaff",
                         "glLevelup",
                        "glLevelup1",
                        "glLevelup2",
                        //"glbAddStaff_V2",///Thêm mới nhân viên
                        //"glbCustomerFirstTime", //Menu thống kê khách đến lần đầu chưa booking
                        "glbService", /// Hóa đơn
                        "glbExportGoods", /// Xuất vật tư
                        "glbExportGoods_Salon", // Xuất vật tư - Cho salon
                        "glbExportGoods_Staff", // Xuất vật tư - Cho nhân viên
                        "glbExportGoods_Report_By_Staff", // Thống kê số bill dịch vụ trong lần xuất
                        "glbExportGoods_Report_By_Staff", // Thống kê số bill dịch vụ trong lần xuất
                        "glbTimeKeeping", /// Chấm công
                        "glbWorkDay", // Chấm công - Công nhật
                        "glbthongke",
                        "glbAdminContent", /// Quản lý nội dung
                        "glbAdminCommonManagerment", // Quản lý chung
                        "glbAdminService", // Quản lý nội dung - Dịch vụ
                        "glbAdminServiceListing", // Quản lý nội dung - Dịch vụ - Danh sách
                        "glbAdminServiceConfigSalon", // Quản lý nội dung - Dịch vụ - Cấu hình hiển thị theo salon
                        "glbAdminProduct", // Quản lý nội dung - Sản phẩm
                         "Li20", // Quản lý nhóm sản phẩm MN_LK
                        "glbAdminCategory", // Quản lý nội dung - Danh mục
                        "glbAdminSocialThread", // Quản lý nội dung - Nguồn thông tin
                        "glbAdminSalon", // Quản lý nội dung - Salon
                        "glbAdminStaff", // Quản lý nội dung - Nhân sự
                        "mlStaff", // Quản lý nhân sự
                        "glbAdminAccount", // Quản lý nội dung - Tài khoản
                        "glbAdminStaffType", // Quản lý nội dung - Bộ phận         
                        "glbAdminSkillLevel", // Quản lý nội dung - Bậc kỹ năng  
                        "glbAdminAppMobile", // Quản lý nội dung app mobile                                     
                        "glbAdminConfigSalary", // Cấu hình lương        
                        "glbAdminProcedure", // Quản lý nội dung - Quy trình / Quy định                                                                
                        "glbAdminBookingHour", // Quản lý nội dung - Cấu hình khung giờ
                        "glbAdminBooking", // Quản lý nội dung - Thống kế Booking    
                        "glbAdminBooking_Test", // Quản lý nội dung - Thống kế Booking  
                        "glbAdminBooking_Call", // Gọi đặt lịch
                        "gblAdminStatisticBooking",//Thống kế booking
                        "gblAdminStatisticBooking_Data", // Thống kê dữ liệu booking
                        "glbAdminBooking_GoiHoTroDatLich", //Danh sách gọi hỗ trợ đặt lịch
                        "glbAdminNotification", // Quản lý nội dung - Notification
                        "glbAdminSales", /// Báo cáo
                        "glbAdminReportSales", // Báo cáo - Tổng hợp
                        "glbAdminReportProduct", // Báo cáo - Mỹ phẩm
                        "glbAdminReportService", // Báo cáo - Dịch vụ
                        //"glbAdminReportOldPending", // Báo cáo - Pending cũ
                        "glbAdminReportStaff", // Báo cáo - Nhân viên
                        "glbAdminReportSalary", // Báo cáo - Tính lương               
                        "glbAdminReport", /// Biểu đồ
                        "glbQuanlity", /// Chất lượng
                        "glbQbyBill", // Chất lượng - Theo hóa đơn
                        "glbQbyServey", // Chất lượng - Theo khảo sát        
                        "glbAdminSlide", // Quản lý slide - API
                        "glbAdminHairMode", // Quản lý kiểu tóc - API
                        "glbBillSale", // Quản lý đơn hàng                        
                        "glbAdminHairAtt", //Quản lý thuộc tính tóc
                        "glbAdminSkinAtt", //Quản lý thuộc tính da
                        "glbInventory", // Quản lý tồn kho
                        "glbMistake", // Chất lượng - Điểm trừ
                        "glbAdminFundOffenItem", // Ngân sách - Khoản thường xuyên
                        "glbAdminFund", // Ngân sách
                        "glbCheckClear", // Chất lượng - vệ sinh salon
                        "glbCheckCSVC", // Chất lượng - CSVC

                        // Stylist4Men
                        "glbS4M",
                        "glbS4MClass", // Lớp học
                        "glbS4MStudent", // Học viên
                        "glbS4MTrainingImage", // Ảnh training khách hàng
                        "glbS4MCheckImageCut", // Check ảnh cắt KCS
                         "glbListBack", //Danh sach chua quay lai sau 1 thang
                         "glbAdminKetQuaKinhDoanh",

                         "glbTuyendungV2",
                        "glbCreatCV",
                        "glbCreateNewCV",
                        "glbReportCV",
                        "glbChangeCV",
                        "glbConfirmSkill",
                        "glbReportConfirm",
                        "glbReportResult",
                        "glbSkillManager",
                        "glbTesterManager",
                       // "glbTuyendungV2",

                        // Thử nghiệm lương doanh số checkin
                        "glbLuongDoanhSoCheckin",
                        "report_checkin_salary",

                        // Quản lý xuất hàng nội bộ
                        "glbDatHangNoiBo",
                        "glbDatHangNoiBo_ChoSalon",
                        "glbDatHangNoiBo_ChoKho",
                        "glbDatHangNoiBo_XuatHoaDon",
                        "glbDatHangNoiBo_DanhSachTraThieu",
                        "Li18",
                        "Li19",
                        // Check ảnh ngẫu nhiên
                        "glbThongKeAnh",

                        // Sự kiện
                        "glbSuKien",
                        "glb_Sukien_TeamThiDua",

                        // Nhân sự - Cấu hình Group Team
                        "glbAdminGroupTeam",
                        "glbAdminLevelup",
                        // Marketing
                        "glbMarketing", // Module Marketing
                        "glbMarketing_Config", // Cấu hình
                        "glbMarketing_ItemDaily", // Cấu hình Item nhập hàng ngày
                        "glbMarketing_Import", // Nhập chi phí
                        "glbMarketing_Import_Daily", // Nhập chi phí hàng ngày
                        "glbMarketing_Import_Plan", // Nhập chi phí chiến dịch
                        "glbMarketing_Report", // Báo cáo
                        "glbMarketing_Report_CP",
                        "glbMarketing_Report_NS",

                        // Quản lý dòng tiền
                        "glbAdminMoneyManager",

                        // Quản lý hàng dịch vụ
                        "glbHangDV"
                    }
                },
                /// mode1
                {
                    "mode1", new List<string>() {
                        "glbCustomer", /// Khách hàng
                        "glbBooking",
                        "glbAddStaff",
                         "glLevelup",
                        "glLevelup1",
                        "glLevelup2",
                        //"glbAddStaff_V2",///Thêm mới nhân viên
                        "glbService", /// Hóa đơn
                        "glbTimeKeeping", /// Chấm công
                        "glbWorkDay", // Chấm công - Công nhật
                        "glbthongke",
                        "glbAdminContent", /// Quản lý nội dung
                        "glbAdminService", // Quản lý nội dung - Dịch vụ
                        "glbAdminProduct", // Quản lý nội dung - Sản phẩm
                         "Li20", // Quản lý nhóm sản phẩm MN_LK
                        "glbAdminStaff", // Quản lý nội dung - Nhân sự
                        "glbAdminStaffType", // Quản lý nội dung - Bộ phận     
                        "glbAdminSocialThread", // Quản lý nội dung - Nguồn thông tin
                        "glbAdminSalon", // Quản lý nội dung - Salon
                        "glbAdminSales", /// Báo cáo       
                        "glbAdminReportSales", // Báo cáo - Tổng hợp
                        "glbAdminReportProduct", // Báo cáo - Mỹ phẩm
                        "glbAdminReportService", // Báo cáo - Dịch vụ
                        //"glbAdminReportOldPending", // Báo cáo - Pending cũ
                        "glbAdminReportStaff", // Báo cáo - Nhân viên
                        "glbAdminReportSalary", // Báo cáo - Tính lương   
                        "glbQuanlity", /// Chất lượng
                        "glbQbyBill", // Chất lượng - Theo hóa đơn
                        "glbQbyServey", // Chất lượng - Theo khảo sát
                        "glbMistake", // Chất lượng - Điểm trừ
                        "glbCheckClear", // Chất lượng - vệ sinh salon
                        "glbCheckCSVC", // Chất lượng - CSVC
                         "glbListBack", //Danh sach chua quay lai sau 1 thang

                        // Marketing
                        "glbMarketing", // Module Marketing
                        "glbMarketing_Config", // Cấu hình
                        "glbMarketing_ItemDaily", // Cấu hình Item nhập hàng ngày
                        "glbMarketing_Import", // Nhập chi phí
                        "glbMarketing_Import_Daily", // Nhập chi phí hàng ngày
                        "glbMarketing_Import_Plan", // Nhập chi phí chiến dịch
                        "glbMarketing_Report", // Báo cáo
                        "glbMarketing_Report_CP",
                        "glbMarketing_Report_NS"
                    }
                },
                /// accountant (kế toán)
                {
                    "accountant", new List<string>() {
                        "glbAdminSales", /// Báo cáo
                        //"glbAdminReportProduct", // Báo cáo - Mỹ phẩm
                        //"glbAdminReportService", // Báo cáo - Dịch vụ
                        //"glbExportGoods", /// Xuất vật tư
                        "glbExportGoods_Salon", // Xuất vật tư - Cho salon
                        "glbExportGoods_Staff", // Xuất vật tư - Cho nhân viên
                        "glbAdminContent", //Quản lý nội dung
                        "glbAdminStaff", //Danh sách nhân viên
                        //"glbInventory", // Quản lý tồn kho
                        "glbAdminReportSalary", // Báo cáo - Tính lương   
                        // Quản lý dòng tiền
                        "glbAdminMoneyManager"
                    }
                },
                /// salonmanager (trưởng cửa hàng)
                {
                    "salonmanager", new List<string>() {
                        "glbBooking",
                        "glbAddStaff",
                         "glLevelup",
                        "glLevelup1",
                        "glLevelup2",
                        //"glbAddStaff_V2",///Thêm mới nhân viên
                        "glbCustomer", /// Khách hàng
                        "glbService", /// Hóa đơn
                        "glbExportGoods", /// Xuất vật tư
                        "glbExportGoods_Salon", // Xuất vật tư - Cho salon
                        "glbExportGoods_Staff", // Xuất vật tư - Cho nhân viên
                        "glbAdminContent", /// Quản lý nội dung
                        "glbAdminCommonManagerment", // Quản lý chung
                        "glbAdminStaff", // Quản lý nội dung - Nhân sự
                        "glbAdminAppMobile", // Quản lý nội dung app mobile
                        //"glbAdminBookingHour", // Quản lý nội dung - Cấu hình khung giờ     
                        //"glbAdminBooking", // Quản lý nội dung - Thống kế Booking    
                        "glbAdminBooking_Call", // Gọi đặt lịch
                        "glbAdminBooking_GoiHoTroDatLich", //Danh sách gọi hỗ trợ đặt lịch
                        "glbTimeKeeping", /// Chấm công
                        "glbWorkDay", // Chấm công - Công nhật
                        "glbthongke",
                        /*"glbPartTime",*/
                        "glbAdminSales", /// Báo cáo  
                        "glbAdminReportSales", // Báo cáo - Tổng hợp
                        "glbAdminReportProduct", // Báo cáo - Mỹ phẩm
                        "glbAdminReportService", // Báo cáo - Dịch vụ
                        //"glbAdminReportOldPending", // Báo cáo - Pending cũ
                        "glbAdminReportStaff", // Báo cáo - Nhân viên
                        "glbAdminReportSalary", // Báo cáo - Tính lương    
                        "glbQuanlity", /// Chất lượng
                        "glbQbyBill", // Chất lượng - Theo hóa đơn
                        "glbQbyServey", // Chất lượng - Theo khảo sát
                        "glbGuide", // Quy trình công việc
                        "glbInventory", // Quản lý tồn kho,
                        "glbMistake", // Chất lượng - Điểm trừ
                        "glbCheckClear", // Chất lượng - vệ sinh salon
                        "glbCheckCSVC", // Chất lượng - CSVC
                        "glbAdminContent",//booking
                        "glbAdminBooking",//thống kê booking
                         "glbListBack", //Danh sach chua quay lai sau 1 thang

                        // Quản lý xuất hàng nội bộ
                        "glbDatHangNoiBo",
                        "glbDatHangNoiBo_ChoSalon",
                        "glbDatHangNoiBo_XuatHoaDon",
                        "glbDatHangNoiBo_DanhSachTraThieu",
                        "Li18",
                        "Li19",

                        "glbThongKeAnh",   //thống kê chất lượng ảnh
                        "gblAdminStatisticBooking", //thống kê booking
                        "glbAdminBooking_Test",
                        // Quản lý hàng dịch vụ
                        "glbHangDV",

                        //Tuyển dụng
                          "glbTuyendungV2",
                        //"glbCreatCV",
                        //"glbCreateNewCV",
                        //"glbReportCV",
                        //"glbConfirmSkill",
                        "glbReportConfirm",
                        //"glbReportResult",
                        //"glbSkillManager",
                        //"glbTesterManager",
                        
                        //Cấu hình
                        "glbAdminContent",
                        "glbAdminGroupTeam",
                    }


                },
                /// reception (lễ tân)
                {
                    "reception", new List<string>() {

                        "glbBooking",
                        "glbCustomer", /// Khách hàng
                        "glbService", /// Hóa đơn
                        /*"glbProfile",*/
                        "glbAdminSales", /// Báo cáo  
                        "glbAdminReportProduct", // Báo cáo - Mỹ phẩm
                        "glbAdminReportService", // Báo cáo - Dịch vụ
                        //"glbAdminReportOldPending", // Báo cáo - Pending cũ
                        "glbAdminContent",//booking
                        "glbAdminBooking",//listting booking
                        "glbListBack", //Danh sach chua quay lai sau 1 thang
                        "glbLuongDoanhSoCheckin"
                    }
                },
                /// checkin (Nhân viên check-in)
                {
                    "checkin", new List<string>() {
                        "glbBooking",
                        "glbCustomer", /// Khách hàng
                        "glbService", /// Hóa đơn
                        /*"glbProfile",*/                                                
                        //"glbTimeKeeping", /// Chấm công
                        //"glbExportGoods", /// Xuất vật tư
                        //"glbExportGoods_Staff", // Xuất vật tư - Cho nhân viên
                        "glbAdminSales", /// Báo cáo  
                        "glbAdminReportProduct", // Báo cáo - Mỹ phẩm
                        "glbAdminReportService", // Báo cáo - Dịch vụ
                        //"glbAdminReportOldPending" ,// Báo cáo - Pending cũ
                        //"glbAdminContent",//booking
                        //"glbAdminBooking",//Listting booking
                        "glbAdminBooking_Call", // Gọi đặt lịch
                        "glbAdminBooking_GoiHoTroDatLich", //Danh sách gọi hỗ trợ đặt lịch
                        "glbListBack", //Danh sach chua quay lai sau 1 thang
                        "glbLuongDoanhSoCheckin",
                        "report_checkin_salary",

                        // Quản lý xuất hàng nội bộ
                        "glbDatHangNoiBo",
                        "glbDatHangNoiBo_ChoSalon",
                        "glbDatHangNoiBo_XuatHoaDon",
                        "glbDatHangNoiBo_DanhSachTraThieu",
                        "Li18",
                        "Li19",
                        // Sự kiện
                        "glbSuKien",
                        "glb_Sukien_TeamThiDua",

                        // Quản lý hàng dịch vụ
                        "glbHangDV"
                    }
                },
                /// checkout (Nhân viên check-out)
                {
                    "checkout", new List<string>() {
                        "glbBooking",
                        "glbCustomer", /// Khách hàng
                        "glbService", /// Hóa đơn
                        /*"glbProfile",*/ 
                        //"glbTimeKeeping", /// Chấm công        
                        //"glbExportGoods", /// Xuất vật tư
                        //"glbExportGoods_Staff", // Xuất vật tư - Cho nhân viên
                        "glbAdminSales", /// Báo cáo  
                        "glbAdminReportProduct", // Báo cáo - Mỹ phẩm
                        "glbAdminReportService", // Báo cáo - Dịch vụ
                        //"glbAdminReportOldPending", // Báo cáo - Pending cũ
                        "glbInventory", // Quản lý tồn kho,
                        //"glbAdminContent",//booking
                        //"glbAdminBooking",//listting booking
                        "glbAdminBooking_Call", // Gọi đặt lịch
                        "glbAdminBooking_GoiHoTroDatLich", //Danh sách gọi hỗ trợ đặt lịch
                         "glbListBack", //Danh sach chua quay lai sau 1 thang

                         // Quản lý xuất hàng nội bộ
                        "glbDatHangNoiBo",
                        "glbDatHangNoiBo_ChoSalon",
                        "glbDatHangNoiBo_XuatHoaDon",
                        "glbDatHangNoiBo_DanhSachTraThieu",
                        "Li18",
                        "Li19",
                        // Sự kiện
                        "glbSuKien",
                        "glb_Sukien_TeamThiDua",

                        // Quản lý hàng dịch vụ
                        "glbHangDV"
                    }
                },
                /// staff (toàn bộ nhân vien)
                {
                    "staff", new List<string>() {
                        "glbProfile",
                        "glbXemAnhLichSu"
                    }
                },
                /// survey (Người làm khảo sát)
                {
                    "servey", new List<string>() {
                        "glbServey" /// Khảo sát
                    }
                },
                /// Admin store online
                {
                    "onlinestore", new List<string>() {
                        "glbCustomer", /// Menu Khách hàng
                        "glbBillSale" // Quản lý đơn hàng
                    }
                },


                /// Stylist4Men manager
                {
                    "s4mmanager", new List<string>() {
                        // Stylist4Men
                        "glbS4M",
                        "glbS4MClass", // Lớp học
                        "glbS4MStudent", // Học viên
                        "glbS4MTrainingImage" // Ảnh training khách hàng
                    }
                },

                {
                    "employer_step1", new List<string>()
                    {
                        // Tuyển dụng
                        "glbTuyenDung",
                        "glbTuyenDung_Step1"

                    }
                },

                {
                    "employer_step2", new List<string>()
                    {
                        // Tuyển dụng
                        "glbTuyenDung",
                        //"glbTuyenDung_Step2"
                    }
                },

                {
                    "inventory_manager", new List<string>()
                    {
                        // Đơn hàng
                        "glbBillSale", // Quản lý đơn hàng

                        // Tồn kho
                        "glbInventory", // Quản lý tồn kho

                        // Báo cáo
                        "glbAdminSales", /// Báo cáo  
                        "glbAdminReportProduct", // Báo cáo - Mỹ phẩm
                        "glbAdminReportService", //Báo cáo - dịch vụ
                        // Quản lý xuất hàng nội bộ
                        "glbDatHangNoiBo",
                        "glbDatHangNoiBo_ChoKho"
                    }
                },

                {
                    "inventory_manager_2", new List<string>()
                    {
                        // Đơn hàng
                        "glbBillSale", // Quản lý đơn hàng

                        // Tồn kho
                        "glbInventory", // Quản lý tồn kho

                        // Báo cáo
                        "glbAdminSales", /// Báo cáo  
                        "glbAdminReportProduct", // Báo cáo - Mỹ phẩm
                        "glbAdminReportService", //Báo cáo - dịch vụ
                        // Quản lý xuất hàng nội bộ
                        "glbDatHangNoiBo",
                        "glbDatHangNoiBo_ChoKho",

                        // Quản lý dòng tiền
                        "glbAdminContent",
                        "glbAdminMoneyManager",
                        "glbAdminFundOffenItem", // Danh mục thu chi
                        "glbAdminFund" // Dòng tiền
                    }
                },

                // Quyền xem thông tin booking - Cho nhóm marketing
                {
                    "view_booking", new List<string>()
                    {
                        "glbBooking",
                        "glbAdminBooking_Test",
                        "gblAdminStatisticBooking",
                        "glbAdminBooking_Call", // Gọi đặt lịch    
                    }
                }
            };

            Permission = Session["User_Permission"].ToString();
            var MenuShow = GlbMenu[Permission];
            if (MenuShow.Count > 0)
            {
                foreach (var v in MenuShow)
                {
                    FindControl(v).Visible = true;
                }
            }
        }
    }
}