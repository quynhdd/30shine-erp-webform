﻿using System;
using System.Collections.Generic;

namespace _30shine.GUI.Controls
{
    public partial class Header : System.Web.UI.UserControl
    {
        protected string _User;
        protected string Permission = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            _User = Session["User_Name"].ToString();
            if (!IsPostBack)
            {
                SetByPermission();
            }
        }

        private void SetByPermission()
        {
            Dictionary<string, List<string>> GlbMenu = new Dictionary<string, List<string>>()
            { 
                /// root
                {
                    "root", new List<string>() {
                        "glbCustomer", /// Menu Khách hàng
                        //"glbCustomerFirstTime", //Menu thống kê khách đến lần đầu chưa booking
                        "glbService", /// Hóa đơn
                        "glbExportGoods", /// Xuất vật tư
                        "glbExportGoods_Salon", // Xuất vật tư - Cho salon
                        "glbExportGoods_Staff", // Xuất vật từ - Cho nhân viên
                        "glbTimeKeeping", /// Chấm công
                        "glbWorkDay", // Chấm công - Công nhật
                        "glbthongke",//thông kê
                        "glbAdminContent", /// Quản lý nội dung
                        "glbAdminService", // Quản lý nội dung - Dịch vụ
                        "glbAdminProduct", // Quản lý nội dung - Sản phẩm
                        "glbAdminCategory", // Quản lý nội dung - Danh mục
                        "glbAdminSocialThread", // Quản lý nội dung - Nguồn thông tin                        
                        "glbAdminSalon", // Quản lý nội dung - Salon
                        "glbAdminStaff", // Quản lý nội dung - Nhân sự
                        //"glbAdminAccount", // Quản lý nội dung - Tài khoản
                        "glbAdminStaffType", // Quản lý nội dung - Bộ phận  
                        "glbAdminSkillLevel", // Quản lý nội dung - Bậc kỹ năng
                        "glbAdminConfigSalary", // Quản lý nội dung - Cấu hình lương
                        "glbAdminProcedure", // Quản lý nội dung - Quy trình / Quy định
                        "glbAdminBookingHour", // Quản lý nội dung - Cấu hình khung giờ     
                        "glbAdminBooking", // Quản lý nội dung - Thống kế Booking    
                        "glbAdminBooking_Test", // Quản lý nội dung - Thống kế Booking  
                        "gblAdminStatisticBooking",//Thống kế booking
                        "glbAdminNotification", // Quản lý nội dung - Notification
                        "glbAdminSales", /// Báo cáo   
                        "glbAdminReportSales", // Báo cáo - Tổng hợp
                        "glbAdminReportProduct", // Báo cáo - Mỹ phẩm
                        "glbAdminReportService", // Báo cáo - Dịch vụ
                        //"glbAdminReportOldPending", // Báo cáo - Pending cũ
                        "glbAdminReportStaff", // Báo cáo - Nhân viên
                        "glbAdminReportSalary", // Báo cáo - Tính lương
                        "glbAdminReport", /// Biểu đồ
                        "glbQuanlity", /// Chất lượng
                        "glbQbyBill", // Chất lượng - Theo hóa đơn
                        "glbQbyServey", // Chất lượng - Theo khảo sát     
                        "glbAdminSlide", // Quản lý slide - API
                        "glbAdminHairMode", // Quản lý kiểu tóc - API
                        "glbBillSale", // Quản lý đơn hàng
                        "glbAdminHairAtt", //Quản lý thuộc tính tóc
                        "glbAdminSkinAtt", //Quản lý thuộc tính da
                        "glbInventory", // Quản lý tồn kho
                        "glbMistake", // Chất lượng - Điểm trừ                        
                        "glbAdminFundOffenItem", // Ngân sách - Khoản thường xuyên
                        "glbAdminFund", // Ngân sách
                        "glbCheckClear", // Chất lượng - vệ sinh salon
                        "glbCheckCSVC", // Chất lượng - CSVC
                        // Stylist4Men
                        "glbS4M",
                        "glbS4MClass", // Lớp học
                        "glbS4MStudent", // Học viên
                        "glbS4MTrainingImage", // Ảnh training khách hàng
                        "glbListBack", //Danh sach chua quay lai sau 1 thang
                        "glbAdminKetQuaKinhDoanh",
                        
                        // Tuyển dụng
                        "glbTuyenDung",
                        "glbTuyenDung_Step1",
                        //"glbTuyenDung_Step2",
                        "glbTuyenDung_Step3",

                        "glbTuyenDung_DaDuyet",
                        "glbTuyenDung_Daotao",//danh sách dào tạo


                        // Thử nghiệm lương doanh số checkin
                        "glbLuongDoanhSoCheckin",

                        // Quản lý xuất hàng nội bộ
                        "glbDatHangNoiBo",
                        "glbDatHangNoiBo_ChoSalon",
                        "glbDatHangNoiBo_ChoKho",
                        "glbDatHangNoiBo_XuatHoaDon",
                        "glbDatHangNoiBo_DanhSachTraThieu",

                        // Check ảnh ngẫu nhiên
                        "glbThongKeAnh",

                        // Sự kiện
                        "glbSuKien",
                        "glb_Sukien_TeamThiDua"
                    }
                },
                /// admin
                {
                    "admin", new List<string>() {
                        "glbCustomer", /// Khách hàng
                        //"glbCustomerFirstTime", //Menu thống kê khách đến lần đầu chưa booking
                        "glbService", /// Hóa đơn
                        "glbExportGoods", /// Xuất vật tư
                        "glbExportGoods_Salon", // Xuất vật tư - Cho salon
                        "glbExportGoods_Staff", // Xuất vật tư - Cho nhân viên
                        "glbTimeKeeping", /// Chấm công
                        "glbWorkDay", // Chấm công - Công nhật
                        "glbthongke", //thống kê-công nhật
                        "glbAdminContent", /// Quản lý nội dung
                        "glbAdminService", // Quản lý nội dung - Dịch vụ
                        "glbAdminProduct", // Quản lý nội dung - Sản phẩm
                        "glbAdminCategory", // Quản lý nội dung - Danh mục
                        "glbAdminSocialThread", // Quản lý nội dung - Nguồn thông tin
                        "glbAdminSalon", // Quản lý nội dung - Salon
                        "glbAdminStaff", // Quản lý nội dung - Nhân sự
                        "glbAdminAccount", // Quản lý nội dung - Tài khoản
                        "glbAdminStaffType", // Quản lý nội dung - Bộ phận         
                        "glbAdminSkillLevel", // Quản lý nội dung - Bậc kỹ năng                                       
                        "glbAdminConfigSalary", // Cấu hình lương        
                        "glbAdminProcedure", // Quản lý nội dung - Quy trình / Quy định                                                                
                        "glbAdminBookingHour", // Quản lý nội dung - Cấu hình khung giờ
                        "glbAdminBooking", // Quản lý nội dung - Thống kế Booking    
                        "glbAdminBooking_Test", // Quản lý nội dung - Thống kế Booking  
                        "gblAdminStatisticBooking",//Thống kế booking
                        "glbAdminNotification", // Quản lý nội dung - Notification
                        "glbAdminSales", /// Báo cáo
                        "glbAdminReportSales", // Báo cáo - Tổng hợp
                        "glbAdminReportProduct", // Báo cáo - Mỹ phẩm
                        "glbAdminReportService", // Báo cáo - Dịch vụ
                        //"glbAdminReportOldPending", // Báo cáo - Pending cũ
                        "glbAdminReportStaff", // Báo cáo - Nhân viên
                        "glbAdminReportSalary", // Báo cáo - Tính lương               
                        "glbAdminReport", /// Biểu đồ
                        "glbQuanlity", /// Chất lượng
                        "glbQbyBill", // Chất lượng - Theo hóa đơn
                        "glbQbyServey", // Chất lượng - Theo khảo sát        
                        "glbAdminSlide", // Quản lý slide - API
                        "glbAdminHairMode", // Quản lý kiểu tóc - API
                        "glbBillSale", // Quản lý đơn hàng                        
                        "glbAdminHairAtt", //Quản lý thuộc tính tóc
                        "glbAdminSkinAtt", //Quản lý thuộc tính da
                        "glbInventory", // Quản lý tồn kho
                        "glbMistake", // Chất lượng - Điểm trừ
                        "glbAdminFundOffenItem", // Ngân sách - Khoản thường xuyên
                        "glbAdminFund", // Ngân sách
                        "glbCheckClear", // Chất lượng - vệ sinh salon
                        "glbCheckCSVC", // Chất lượng - CSVC

                        // Stylist4Men
                        "glbS4M",
                        "glbS4MClass", // Lớp học
                        "glbS4MStudent", // Học viên
                        "glbS4MTrainingImage", // Ảnh training khách hàng
                         "glbListBack", //Danh sach chua quay lai sau 1 thang
                         "glbAdminKetQuaKinhDoanh",

                         // Tuyển dụng
                        "glbTuyenDung",
                        "glbTuyenDung_Step1",
                        //"glbTuyenDung_Step2",
                        "glbTuyenDung_Step3",

                        "glbTuyenDung_DaDuyet",


                        "glbTuyenDung_Daotao",//danh sách dào tạo


                        // Thử nghiệm lương doanh số checkin
                        "glbLuongDoanhSoCheckin",

                        // Quản lý xuất hàng nội bộ
                        "glbDatHangNoiBo",
                        "glbDatHangNoiBo_ChoSalon",
                        "glbDatHangNoiBo_ChoKho",
                        "glbDatHangNoiBo_XuatHoaDon",
                        "glbDatHangNoiBo_DanhSachTraThieu",

                        // Check ảnh ngẫu nhiên
                        "glbThongKeAnh",

                        // Sự kiện
                        "glbSuKien",
                        "glb_Sukien_TeamThiDua"
                    }
                },
                /// mode1
                {
                    "mode1", new List<string>() {
                        "glbCustomer", /// Khách hàng
                        "glbService", /// Hóa đơn
                        "glbTimeKeeping", /// Chấm công
                        "glbWorkDay", // Chấm công - Công nhật
                        "glbthongke",//thống kê - công nhật
                        "glbAdminContent", /// Quản lý nội dung
                        "glbAdminService", // Quản lý nội dung - Dịch vụ
                        "glbAdminProduct", // Quản lý nội dung - Sản phẩm
                        "glbAdminStaff", // Quản lý nội dung - Nhân sự
                        "glbAdminStaffType", // Quản lý nội dung - Bộ phận     
                        "glbAdminSocialThread", // Quản lý nội dung - Nguồn thông tin
                        "glbAdminSalon", // Quản lý nội dung - Salon
                        "glbAdminSales", /// Báo cáo       
                        "glbAdminReportSales", // Báo cáo - Tổng hợp
                        "glbAdminReportProduct", // Báo cáo - Mỹ phẩm
                        "glbAdminReportService", // Báo cáo - Dịch vụ
                        //"glbAdminReportOldPending", // Báo cáo - Pending cũ
                        "glbAdminReportStaff", // Báo cáo - Nhân viên
                        "glbAdminReportSalary", // Báo cáo - Tính lương   
                        "glbQuanlity", /// Chất lượng
                        "glbQbyBill", // Chất lượng - Theo hóa đơn
                        "glbQbyServey", // Chất lượng - Theo khảo sát
                        "glbMistake", // Chất lượng - Điểm trừ
                        "glbCheckClear", // Chất lượng - vệ sinh salon
                        "glbCheckCSVC", // Chất lượng - CSVC
                         "glbListBack" //Danh sach chua quay lai sau 1 thang
                    }
                },
                /// accountant (kế toán)
                {
                    "accountant", new List<string>() {
                        "glbAdminSales", /// Báo cáo
                        "glbAdminReportProduct", // Báo cáo - Mỹ phẩm
                        "glbAdminReportService", // Báo cáo - Dịch vụ
                        "glbExportGoods", /// Xuất vật tư
                        "glbExportGoods_Salon", // Xuất vật tư - Cho salon
                        "glbExportGoods_Staff", // Xuất vật tư - Cho nhân viên
                        "glbInventory", // Quản lý tồn kho
                        "glbAdminReportSalary" // Báo cáo - Tính lương   
                    }
                },
                /// salonmanager (trưởng cửa hàng)
                {
                    "salonmanager", new List<string>() {
                        "glbCustomer", /// Khách hàng
                        "glbService", /// Hóa đơn
                        "glbExportGoods", /// Xuất vật tư
                        "glbExportGoods_Salon", // Xuất vật tư - Cho salon
                        "glbExportGoods_Staff", // Xuất vật tư - Cho nhân viên
                        "glbAdminContent", /// Quản lý nội dung
                        "glbAdminStaff", // Quản lý nội dung - Nhân sự
                        "glbAdminBookingHour", // Quản lý nội dung - Cấu hình khung giờ     
                        //"glbAdminBooking", // Quản lý nội dung - Thống kế Booking    
                        "glbTimeKeeping", /// Chấm công
                        "glbWorkDay", // Chấm công - Công nhật
                        "glbthongke",//Thống kê công nhật
                        /*"glbPartTime",*/
                        "glbAdminSales", /// Báo cáo  
                        "glbAdminReportSales", // Báo cáo - Tổng hợp
                        "glbAdminReportProduct", // Báo cáo - Mỹ phẩm
                        "glbAdminReportService", // Báo cáo - Dịch vụ
                        //"glbAdminReportOldPending", // Báo cáo - Pending cũ
                        "glbAdminReportStaff", // Báo cáo - Nhân viên
                        "glbAdminReportSalary", // Báo cáo - Tính lương    
                        "glbQuanlity", /// Chất lượng
                        "glbQbyBill", // Chất lượng - Theo hóa đơn
                        "glbQbyServey", // Chất lượng - Theo khảo sát
                        "glbGuide", // Quy trình công việc
                        "glbInventory", // Quản lý tồn kho,
                        "glbMistake", // Chất lượng - Điểm trừ
                        "glbCheckClear", // Chất lượng - vệ sinh salon
                        "glbCheckCSVC", // Chất lượng - CSVC
                        "glbAdminContent",//booking
                        "glbAdminBooking",//litting booking
                         "glbListBack", //Danh sach chua quay lai sau 1 thang

                        // Quản lý xuất hàng nội bộ
                        "glbDatHangNoiBo",
                        "glbDatHangNoiBo_ChoSalon",
                        "glbDatHangNoiBo_XuatHoaDon",
                        "glbDatHangNoiBo_DanhSachTraThieu"
                    }


                },
                /// reception (lễ tân)
                {
                    "reception", new List<string>() {
                        "glbCustomer", /// Khách hàng
                        "glbService", /// Hóa đơn
                        /*"glbProfile",*/
                        "glbAdminSales", /// Báo cáo  
                        "glbAdminReportProduct", // Báo cáo - Mỹ phẩm
                        "glbAdminReportService", // Báo cáo - Dịch vụ
                        //"glbAdminReportOldPending", // Báo cáo - Pending cũ
                        "glbAdminContent",//booking
                        "glbAdminBooking",//listting booking
                        "glbListBack", //Danh sach chua quay lai sau 1 thang
                        "glbLuongDoanhSoCheckin"
                    }
                },
                /// checkin (Nhân viên check-in)
                {
                    "checkin", new List<string>() {
                        "glbCustomer", /// Khách hàng
                        "glbService", /// Hóa đơn
                        /*"glbProfile",*/                                                
                        "glbTimeKeeping", /// Chấm công
                        //"glbExportGoods", /// Xuất vật tư
                        //"glbExportGoods_Staff", // Xuất vật tư - Cho nhân viên
                        "glbAdminSales", /// Báo cáo  
                        "glbAdminReportProduct", // Báo cáo - Mỹ phẩm
                        "glbAdminReportService", // Báo cáo - Dịch vụ
                        //"glbAdminReportOldPending" ,// Báo cáo - Pending cũ
                        "glbAdminContent",//booking
                        "glbAdminBooking",//Listting booking
                        "glbListBack", //Danh sach chua quay lai sau 1 thang
                        "glbLuongDoanhSoCheckin",

                        // Quản lý xuất hàng nội bộ
                        "glbDatHangNoiBo",
                        "glbDatHangNoiBo_ChoSalon",
                        "glbDatHangNoiBo_XuatHoaDon",
                        "glbDatHangNoiBo_DanhSachTraThieu",

                        // Sự kiện
                        "glbSuKien",
                        "glb_Sukien_TeamThiDua"
                    }
                },
                /// checkout (Nhân viên check-out)
                {
                    "checkout", new List<string>() {
                        "glbCustomer", /// Khách hàng
                        "glbService", /// Hóa đơn
                        /*"glbProfile",*/ 
                        "glbTimeKeeping", /// Chấm công        
                        //"glbExportGoods", /// Xuất vật tư
                        //"glbExportGoods_Staff", // Xuất vật tư - Cho nhân viên
                        "glbAdminSales", /// Báo cáo  
                        "glbAdminReportProduct", // Báo cáo - Mỹ phẩm
                        "glbAdminReportService", // Báo cáo - Dịch vụ
                        //"glbAdminReportOldPending", // Báo cáo - Pending cũ
                        "glbInventory", // Quản lý tồn kho,
                        "glbAdminContent",//booking
                        "glbAdminBooking",//listting booking
                         "glbListBack", //Danh sach chua quay lai sau 1 thang

                         // Quản lý xuất hàng nội bộ
                        "glbDatHangNoiBo",
                        "glbDatHangNoiBo_ChoSalon",
                        "glbDatHangNoiBo_XuatHoaDon",
                        "glbDatHangNoiBo_DanhSachTraThieu",

                        // Sự kiện
                        "glbSuKien",
                        "glb_Sukien_TeamThiDua"
                    }
                },
                /// staff (toàn bộ nhân vien)
                {
                    "staff", new List<string>() {
                        "glbProfile",
                        "glbXemAnhLichSu"
                    }
                },
                /// survey (Người làm khảo sát)
                {
                    "servey", new List<string>() {
                        "glbServey" /// Khảo sát
                    }
                },
                /// Admin store online
                {
                    "onlinestore", new List<string>() {
                        "glbCustomer", /// Menu Khách hàng
                        "glbBillSale" // Quản lý đơn hàng
                    }
                },


                /// Stylist4Men manager
                {
                    "s4mmanager", new List<string>() {
                        // Stylist4Men
                        "glbS4M",
                        "glbS4MClass", // Lớp học
                        "glbS4MStudent", // Học viên
                        "glbS4MTrainingImage" // Ảnh training khách hàng
                    }
                },

                {
                    "employer_step1", new List<string>()
                    {
                        // Tuyển dụng
                        "glbTuyenDung",
                        "glbTuyenDung_Step1"

                    }
                },

                {
                    "employer_step2", new List<string>()
                    {
                        // Tuyển dụng
                        "glbTuyenDung",
                        //"glbTuyenDung_Step2"
                    }
                },

                {
                    "inventory_manager", new List<string>()
                    {
                        // Đơn hàng
                        "glbBillSale", // Quản lý đơn hàng

                        // Tồn kho
                        "glbInventory", // Quản lý tồn kho

                        // Báo cáo
                        "glbAdminSales", /// Báo cáo  
                        "glbAdminReportProduct", // Báo cáo - Mỹ phẩm

                        // Quản lý xuất hàng nội bộ
                        "glbDatHangNoiBo",
                        "glbDatHangNoiBo_ChoKho"
                    }
                },
            };

            Permission = Session["User_Permission"].ToString();
            var MenuShow = GlbMenu[Permission];
            if (MenuShow.Count > 0)
            {
                foreach (var v in MenuShow)
                {
                    FindControl(v).Visible = true;
                }
            }
        }
    }
}