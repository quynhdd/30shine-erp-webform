﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HeaderV3.ascx.cs" Inherits="_30shine.GUI.UIControls.HeaderV3" %>



<script src="/Assets/js/Menu_mobile.js"></script>
<link href="/Assets/css/menu_mobile.css?12379349" rel="stylesheet" />

<style>
    .box-menu {
        box-shadow: 2px 2px 2px 2px #b8b8b8;
        min-height: 100px;
        max-height: 200px;
        min-width: 65px;
        height: 150px;
    }

    .text-box-submenu {
        min-height: 100px;
        text-align: center;
    }

    @media (min-width: 320px) {

        .c-menu--slide-left, .c-menu--slide-right, .c-menu--push-left, .c-menu--push-right {
            width: 37%;
        }
    }

    .ul-sub-menu {
        padding-left: 0px;
    }


    .global-menu ul {
        list-style: none;
    }

        .global-menu ul#glbMenu > li {
            float: left;
            position: relative;
            background: #dfdfdf;
            margin-right: 10px;
        }

        .global-menu ul#glbMenu li {
            margin-top: 10px;
            margin-right: 0px !important;
        }

        .global-menu ul#glbMenu > li > a {
            /*padding: 0 10px;*/
            font-size: 18px;
            /*padding: 5px 20px;*/
            font-family: Roboto Condensed Regular;
            font-size: 14px;
            float: left;
            margin-right: 0px;
        }

        .global-menu ul#glbMenu > li > a {
            padding: 10px 10px;
        }

        .global-menu ul#glbMenu > li:hover ul.ul-sub-menu {
            display: block;
        }

        .global-menu ul#glbMenu ul.ul-sub-menu {
            position: absolute;
            top: 30px;
            left: 0;
            z-index: 20000;
            border: 1px solid #dfdfdf;
            background: #fff;
            display: none;
            width: auto !important;
            white-space: nowrap;
        }

            .global-menu ul#glbMenu ul.ul-sub-menu li:first-child {
                border-top: none;
            }

            .global-menu ul#glbMenu ul.ul-sub-menu li {
                float: left;
                width: 100%;
                margin-top: 0;
                padding: 5px 10px;
                background: #e7e7e7;
                border-top: 1px solid #bababa;
                position: relative;
            }

    .global-menu .ul-sub2-menu {
        padding-left: 5px;
        position: absolute;
        top: 0;
        left: 100%;
        margin: 0 auto;
        text-align: left;
        white-space: nowrap;
        width: auto;
        display: none;
    }

    .ul-sub-menu li:hover ul {
        display: block;
    }

    .c-mask.is-active {
        width: auto !important;
    }

    .password-notification:hover {
        cursor: pointer;
    }
</style>

<div class="">
    <div class="container-fluid global-menu" id="o-wrapper" style="background: #000000;">
        <!-- Menu desktop -->
        <%--<strong class="text-head">Phần mềm quản lý hair salon</strong>--%>

        <div class="row" id="menu-desktop">
            <div class="col-1">
                <div style="width: 75px;">
                    <img src="/Assets/images/ic_logo_white.png" class="w-100" />
                </div>
            </div>
            <div class="col-xl-8 col-lg-7 col-md-6">
                <ul class="ul-global-menu w-100 h-100 pl-0" id="glbMenu" runat="server" clientidmode="Static">
                    <%-- <li>
                            
                        </li>--%>
                    <% Response.Write(Session["Menu"]); %>
                    <%--<% Response.WriteFile("/Config/" + Session["User_Permission"] + ".html");  %>--%>
                </ul>
            </div>
            <div class="col-xl-3 col-lg-4 col-md-5">
                <ul style="float: right; margin-top: 10px; margin-bottom: 0px;">
                    <li style="float: right; top: 0; right: 15px;"
                        id="login_detail">
                        <a href="javascript://"><i class="fa fa-envelope" style="color: #ffffff;"></i></a>
                        <a href="javascript://"><i class="fa fa-bell" style="color: #ffffff;"></i></a>
                        <p class="hello-user" style="color: #ffffff; display: inline-block; margin-top: 10px; margin-right: 15px;">Xin chào <%=_User %></p>
                        <div class="avatar-wrap" style="display: inline-block; position: relative;">
                            <a href="javascript://" class="avatar" id="Avatar">
                                <img class="avatar-img" alt="" title="" src="/Assets/images/avatar.man.white.png" style="width: 20px; height: 21px; position: relative; top: -2px;" />
                            </a>
                            <div class="header-user-wp" id="HeaderUser" style="position: absolute; z-index: 2000; display: none; background: #5d5d5d; margin-left: -65px; right: 7px; margin-right: -20px; margin-top: 5px; padding: 5px 10px;">
                                <ul class="user-login-panel p-0">
                                    <li><a href="javascript://">Sửa thông tin</a></li>
                                    <%if (Session["UserID"].ToString() != "0" && !Convert.ToBoolean(Session["IsLogin"]))
                                        {%>
                                    <li><a href="/doi-mat-khau.html" target="_blank">Đổi mật khẩu</a></li>
                                    <%} %>
                                    <li><a href="/dang-xuat.html">Thoát</a></li>
                                </ul>
                                <div class="triangle"></div>
                            </div>
                        </div>
                    </li>
                    <%if (TimeLivePassword != null && TimeLivePassword <= 10 && Session["UserID"].ToString() != "0" && !Convert.ToBoolean(Session["IsLogin"]))
                        {%>
                    <li style="float: right; position: absolute; top: 55px; right: 50px; background-color: transparent !important;">
                        <p style="color: red; font-size: 15px; position: relative; z-index: 1999;">Còn <%=TimeLivePassword %> ngày nữa là mật khẩu hết hạn. Đổi mật khẩu tại <a class="password-notification" target="_blank" style="color: #3366BB; text-decoration: underline;" href="/doi-mat-khau.html">đây</a></p>
                    </li>
                    <%} %>
                </ul>
            </div>
        </div>
        <!-- Menu desktop -->

        <!-- Menu mobile -->
        <div class="row menu-mobile" style="position: relative; display: none;">
            <a href="/" class="logo-m">
                <img src="/Assets/images/ic_logo_white.png" style="margin: 10px 10px 10px 0; width: 75px;" />
            </a>
            <div class="toogle-menu-m" id="open-left-panel" style="display: inline-block;">
                <img src="/Assets/images/toggle.m.png" style="margin-top: 10px;">
            </div>

        </div>
        <div id="panel-menu-m">
            <nav id="c-menu--push-left" class="c-menu c-menu--push-left" style="display: none; overflow: scroll;">
                <ul class="global-menu-m" style="padding-bottom: 10px;">
                    <li>
                        <div class="row">
                            <div class="col-md-6">
                                <img src="/Assets/images/logo/logo.jpg" style="float: left; width: 100px;" />
                            </div>
                            <div class="col-md-6">
                                <button class="c-menu__close">
                                    <img src="/Assets/images/close-icon-menu.png" style="position: relative; top: 30px; width: 34px; height: 30px; z-index: 10" />
                                </button>
                            </div>
                        </div>
                    </li>
                </ul>
                <ul class="menu-childrent" style="display: none;">
                    <li class="header-menu">
                        <div class="row">
                            <div class="col-md-6">
                                <img src="/Assets/images/logo/logo.jpg" style="float: left; width: 100px;" />
                            </div>
                            <div class="col-md-6">
                                <button class="c-menu__close">
                                    <img src="/Assets/images/close-icon-menu.png" style="position: relative; top: 30px; width: 34px; height: 30px;" />
                                </button>
                            </div>
                        </div>
                    </li>

                </ul>
            </nav>
            <!-- /c-menu push-left -->
        </div>

        <div id="c-mask" class="c-mask"></div>
        <!-- Menu mobile -->
    </div>
</div>
<script>
    $(document).ready(function () {
        $.ajax({
            url: "/GUI/SystemService/WebService/MenuService.asmx/GetMenuMobile",
            datatype: "JSON",
            contentType: "application/json; charset:UTF-8",
            type: "POST",
            success: function (data) {
                var jsData = JSON.parse(data.d);
                var li = $("<li><div class='row' style='margin-bottom: 10px;'></div></li>");
                for (var j = 0; j < jsData.length; j++) {
                    var div_1 = '<div class="col-md-5" style="width:50%;float: left;">';
                    div_1 += '<div class="box-menu parent-' + jsData[j].menuClass + '" onclick="menu_click($(this),\'' + String(jsData[j].menuClass) + '\')">';
                    div_1 += '<img src="' + jsData[j].menuIcon + '" class="sub-icon" style="margin-top: 20px;"/>';
                    div_1 += '<p class="sub-text">' + jsData[j].menuName + '</p>';
                    div_1 += '<p style="display:none;">' + jsData[j].menuId + '</p>';
                    div_1 += '</div>';
                    div_1 += '</div>';
                    li.append(div_1);
                    //get child menu

                    var menuChild = $("<li class='menu-" + jsData[j].menuClass + "' style='display:none;'></li>");

                    //var back='<div class="row" style="margin-bottom: 10px;">';


                    //menuChild.append(back);
                    //back += '</div>';
                    var row = $("<div class='row' style='margin-bottom: 10px;'></div>");
                    var back = '<div class="col-md-5" style="width:50%;float: left;">';
                    back += '<div class="box-menu back" onclick="menu_click($(this),\'' + String(jsData[j].menuClass) + '\')" style="margin-top: 20px;">';
                    back += '<img src="/Assets/images/icon_menu_mobile/back.png" class="sub-icon" />';
                    back += '<p class="sub-text">Quay lại</p>';
                    back += '</div>';
                    row.append(back);

                    for (var i = 0; i < jsData[j].lstMenu.length; i++) {
                        var child = '<div class="col-md-5" style="width:50%;float: left;">';
                        child += '<div class="box-menu text-box-submenu ' + jsData[j].lstMenu[i].childClass + '">';
                        child += '<a href="' + jsData[j].lstMenu[i].childLink + '" class="sub-text">' + jsData[j].lstMenu[i].childName + '</a>';
                        child += '<p style="display:none;">' + jsData[j].lstMenu[i].childId + '</p>';
                        child += '</div>';
                        child += '</div>';
                        child += '</div>';
                        row.append(child);
                    }
                    $(".menu-childrent").append(menuChild.append(row));
                    //back += '</div>';

                }
                $(".global-menu-m").append(li);
            }
        })
    })
</script>

<script type="text/ecmascript">
    //End Build Menu Mobile
    jQuery(document).ready(function () {
        // set active
        $("#glbMenu li").bind("click", function () {
            $("#glbMenu li.active").removeClass("active");
            $(this).addClass("active");
        });

        //show header login panel
        $("#Avatar").bind("click", function () {
            $("#HeaderUser").toggle();
        });
        $(window).bind("click", function (e) {
            if (e.target.className !== "avatar-img") {
                $("#HeaderUser").hide();
            }
        });
        window.onload = function () {
            var heightArray = [];
            //alert($(".box-menu").length);
            for (var i = 0; i < $(".box-menu").length; i++) {
                var height = $(".box-menu").eq(i).height();
                heightArray.push(height);
            }

            var maxHeight = Math.max.apply(null, heightArray);
            $(".box-menu").css("height", maxHeight);
        };

        //Xử lý phân quyền menu mobile tạm thời
        trickMenu();
    });

    function menu_click(This, MenuClass) {
        if (!$(This).hasClass("back")) {
            $(".global-menu-m").css("display", "none");
            $(".menu-childrent").css("display", "block");
            $(".menu-childrent").find("li[class^=\"menu-\"]").css("display", "none");
            if ($(This).hasClass("parent-" + MenuClass + "")) {
                $(".menu-" + MenuClass + "").css("display", "block");
            }
        }
        else {
            $(".global-menu-m").css("display", "block");
            $(".menu-childrent").css("display", "none");
        }
    }

    /*
    * Xử lý phân quyền menu mobile tạm thời
    */
    var adminNames = ["Xin chào Admin02", "Xin chào Admin", "Xin chào Root"];
    var listOff = [];
    function trickMenu() {
        var adminName = $(".hello-user").text();
        if (adminNames.indexOf(adminName) == -1) {
            $("[data-perm='admin']").hide();
        }
    }

</script>

<script>
    jQuery(document).ready(function () {
        var pushRight = new Menu({
            wrapper: '#o-wrapper',
            type: 'push-left',
            menuOpenerClass: '.c-button',
            maskId: '#c-mask'
        });

        var pushRightBtn = document.querySelector('#open-left-panel');

        pushRightBtn.addEventListener('click', function (e) {
            e.preventDefault;
            pushRight.open();
        });

        $(".li-brands").hover(function () {
            $('.menu-sub').css({ 'display': 'block' });
        });

        // click out of search box
        $('html').mousemove(function () {
            $(".menu-sub").hide();
        });
        $('.menu-sub,.li-brands').mousemove(function (event) {
            event.stopPropagation();
        });
        $("#open-left-panel").click(function () {
            $('#panel-menu-m').css('z-index', '100000');
            $('.hotline-m').css('z-index', '0');
        });
    });

</script>
<script>
    function detectDevice() {
        var device = { tablet: false, mobile: false, desktop: false };
        if (navigator.userAgent.match(/Tablet|iPad/)) {
            device.tablet = true;
        } else if (navigator.userAgent.match(/Mobile/)) {
            device.mobile = true;
        } else {
            device.desktop = true;
        }
        return device;
    }
</script>

