﻿using _30shine.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.UIControls
{
    public partial class HeaderV3 : System.Web.UI.UserControl
    {
        protected string _User = "";
        protected string Permission = "";
        protected int? TimeLivePassword;
        protected void Page_Load(object sender, EventArgs e)
        {
            _User = Session["User_Name"].ToString();
            if (!IsPostBack)
            {
                var time = Convert.ToDateTime(Session["LastChangePassword"]).AddDays(Convert.ToInt32(Session["TimeLivePassword"])).Date - DateTime.Now.Date;
                TimeLivePassword = time.Days;
                if (time.Days <= 0 && Session["UserID"].ToString() != "0")
                {
                    UIHelpers.Redirect("/doi-mat-khau.html");
                }
            }
        }
    }
}