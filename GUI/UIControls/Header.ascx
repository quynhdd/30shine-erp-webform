﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Header.ascx.cs" Inherits="_30shine.GUI.Controls.Header" %>

<div class="wp header">
    <div class="top">
        <ul class="header-user-login">
            <li>
                <p class="hello-user">Xin chào <%=_User %></p>
                <div class="avatar-wrap">
                    <a href="javascript://" class="avatar" id="Avatar">
                        <img class="avatar-img" alt="" title="" src="/Assets/images/avatar.man.white.png" />
                    </a>
                    <div class="header-user-wp" id="HeaderUser">
                        <ul class="user-login-panel">
                            <li><a href="javascript://">Sửa thông tin</a></li>
                            <li><a href="/dang-xuat.html">Thoát</a></li>
                        </ul>
                        <div class="triangle"></div>
                    </div>
                </div>
            </li>
            <li>
                <a href="javascript://"></a>
            </li>
        </ul>
    </div>
</div>

<div class="wp global-menu">
    <div class="container">
        <div class="row">
            <strong class="text-head">Phần mềm quản lý hair salon</strong>
        </div>
        <div class="row">
            <ul class="ul-global-menu" id="glbMenu" runat="server" clientidmode="Static">
                <li id="glbCustomer" runat="server" visible="false" clientidmode="Static">
                    <a href="/khach-hang/danh-sach.html">Khách hàng</a>
                    <ul class="ul-sub-menu">
                        <li id="Li12" runat="server" visible="true" clientidmode="Static">
                            <a href="/khach-hang/them-moi.html">Thêm mới</a>
                        </li>
                        <li id="Li13" runat="server" visible="true" clientidmode="Static">
                            <a href="/khach-hang/danh-sach.html">Danh sách</a>
                        </li>
                        <li id="L13" runat="server" visible="true" clientidmode="Static">
                            <a href="/khach-hang/hoa-chat">Khách dùng hóa chất</a>
                        </li>
                        <li id="Li10" runat="server" visible="true" clientidmode="Static">
                            <a href="/khach-hang/dac-biet">Khách đặc biệt</a>
                        </li>
                    </ul>    
                </li>
                <li id="glbService" runat="server" visible="false" clientidmode="Static">
                    <a href="/dich-vu/danh-sach.html">Hóa đơn</a>
                    <ul class="ul-sub-menu">
                        <li id="Li5" runat="server" visible="true" clientidmode="Static">
                            <a href="/dich-vu/checkin.html">Checkin</a>
                        </li>
                        <li id="Li6" runat="server" visible="true" clientidmode="Static">
                            <a href="/dich-vu/pending.html">Pending</a>
                        </li>
                        <li id="Li8" runat="server" visible="true" clientidmode="Static">
                            <a href="/dich-vu/danh-sach.html">Danh sách</a>
                        </li>
                    </ul>    
                </li>
                <li id="glbBillSale" runat="server" visible="false" clientidmode="Static">
                    <a href="/don-hang/danh-sach.html">Đơn hàng</a>
                </li>
                <li id="glbInventory" runat="server" visible="false" clientidmode="Static">
                    <a href="/ton-kho/danh-sach.html">Tồn kho</a>
                </li>
                <li id="glbExportGoods" runat="server" visible="false" clientidmode="Static">
                    <a href="javascript:void(0);">Xuất vật tư</a>
                    <ul class="ul-sub-menu">
                        <li id="glbExportGoods_Salon" runat="server" visible="false" clientidmode="Static">
                            <a href="/my-pham/xuat-salon/xuat-moi.html">Cho salon</a>
                        </li>
                        <li id="glbExportGoods_Staff" runat="server" visible="false" clientidmode="Static">
                            <a href="/my-pham/xuat-nv/xuat-moi.html">Cho nhân viên</a>
                        </li>
                    </ul>
                </li>
                <li id="glbTimeKeeping" runat="server" visible="false" clientidmode="Static">
                    <a href="javascript:void(0);">Chấm công</a>
                    <ul class="ul-sub-menu">
                        <li id="glbWorkDay" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/cong-nhat/diem-danh.html">Ngày công</a>
                        </li>
                        <li id="glbthongke" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/cong-nhat/thong-ke-nhien-vien-di-lam-cua-salon.html">Thống kê salon</a>
                        </li>
                    </ul>
                </li>
                <li id="glbAdminContent" runat="server" visible="false" clientidmode="Static">
                    <a href="javascript:void(0);">Quản lý nội dung</a>
                    <ul class="ul-sub-menu">
                        <%--<li>
                            <a href="javascript:void(0)"></a>
                            <ul class="ul-sub2-menu">
                                
                            </ul>
                        </li>--%>
                        <li>
                            <a href="javascript:void(0)">Quản lý chung</a>
                            <ul class="ul-sub2-menu">
                                <li id="glbAdminCategory" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/danh-muc/danh-sach.html">Danh mục</a>
                                </li>
                                <li id="glbAdminSocialThread" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/nguon-thong-tin.html">Nguồn thông tin</a>
                                </li>
                            </ul>
                        </li>
                        <li id="glbAdminService" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/dich-vu.html">Dịch vụ</a>
                        </li>
                        <li id="glbAdminProduct" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/san-pham.html">Sản phẩm</a>
                        </li>

                        <li id="glbAdminSalon" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/salon.html">Salon</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Nhân sự</a>
                            <ul class="ul-sub2-menu">
                                <li id="glbAdminStaff" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/nhan-vien.html">Nhân viên</a>
                                </li>
                                <li id="glbAdminStaffType" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/kieu-nhan-vien.html">Bộ phận</a>
                                </li>
                                <li id="glbAdminSkillLevel" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/bac-ky-nang/danh-sach.html">Bậc kỹ năng</a>
                                </li>
                                <li id="glbAdminAccount" runat="server" visible="false" clientidmode="Static" style="display: none;">
                                    <a href="/admin/account/danh-sach.html">Tài khoản đăng nhập</a>
                                </li>
                                <li id="glbAdminConfigSalary" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/cau-hinh-luong.html">Cấu hình lương</a>
                                </li>
                                <li id="glbAdminProcedure" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/quy-trinh/danh-sach.html">Quy trình / Quy định</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Booking</a>
                            <ul class="ul-sub2-menu">
                                <li id="glbAdminBookingHour" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/booking/hour.html">Cấu hình khung giờ</a>
                                </li>
                                <li id="glbAdminBooking" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/listing-booking.html">Danh sách Booking</a>
                                </li>
                                <li id="glbAdminBooking_Test" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/listing-booking-test.html">Danh sách Booking - Test</a>
                                </li>
                                <li id="gblAdminStatisticBooking" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/thong-ke-booking.html">Thống kê Booking</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Nội dung app mobile</a>
                            <ul class="ul-sub2-menu">
                                <li id="glbAdminSlide" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/slide/danh-sach.html">Slide</a>
                                </li>
                                <li id="glbAdminHairMode" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/kieu-toc/danh-sach.html">Kiểu tóc</a>
                                </li>
                                <li id="glbAdminHairAtt" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/loai-toc/danh-sach.html">Thuộc tính tóc</a>
                                </li>
                                <li id="glbAdminSkinAtt" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/loai-da/danh-sach.html">Thuộc tính da</a>
                                </li>
                                <li id="glbAdminNotification" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/notification/send.html">Notification</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:void(0)">Dòng tiền</a>
                            <ul class="ul-sub2-menu">
                                <li id="glbAdminFundOffenItem" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/ngan-sach/khoan-thuong-xuyen/danh-sach.html">Danh mục thu chi</a>
                                </li>
                                <li id="glbAdminFund" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/ngan-sach/thu-chi/danh-sach.html">Dòng tiền</a>
                                </li>
                            </ul>
                        </li>
                        <li id="glbListBack" runat="server" visible="false" clientidmode="Static" style="display:none;">
                            <a href="/admin/danh-sach-chua-quay-lai-30-ngay.html">DS chưa quay lại sau 1 tháng</a>
                        </li>
                    </ul>
                </li>
               
                <li id="glbAdminSales" runat="server" visible="false" clientidmode="Static">
                   
                    <a href="javascript://">Báo cáo</a>
                    <ul class="ul-sub-menu">
                        <li id="glbAdminReportSales" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/bao-cao/hoa-don.html">Tổng hợp</a>
                        </li>
                        <li id="glbAdminReportProduct" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/bao-cao/doanh-so-my-pham.html">Mỹ phẩm</a>
                        </li>
                        <li id="glbAdminReportService" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/bao-cao/doanh-so-dich-vu.html">Dịch vụ</a>
                        </li>
                        <%--<li id="glbAdminReportOldPending" runat="server" visible="false" clientidmode="Static">
                            <a href="/dich-vu/pending-cu.html">Pending cũ</a>
                        </li>--%>
                        <li id="glbAdminReportStaff" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/bao-cao/nhan-vien.html">Nhân viên</a>
                        </li>
                        <li id="glbAdminReportSalary" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/bao-cao/salary-v3.html">Tính lương</a>
                        </li>
                        <li id="glbAdminKetQuaKinhDoanh" runat="server" visible="false" clientidmode="Static">
                            <a href="javascript:void(0)">Kết quả kinh doanh</a>
                            <ul class="ul-sub2-menu">
                                <li id="Li3" runat="server" clientidmode="Static">
                                    <a href="/admin/bao-cao/ket-qua-kinh-doanh/nhap-lieu.html">Nhập dữ liệu</a>
                                </li>
                                <li id="Li4" runat="server" clientidmode="Static">
                                    <a href="/admin/bao-cao/ket-qua-kinh-doanh.html">Tóm tắt báo cáo</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li id="glbAdminReport" runat="server" visible="false" clientidmode="Static" style="display:none;">
                    <a href="/admin/bao-cao/bieu-do.html">Biểu đồ</a>
                </li>
                <li id="glbQuanlity" runat="server" visible="false" clientidmode="Static">
                    <a href="javascript:void(0);">Chất lượng</a>
                    <ul class="ul-sub-menu">
                        <li id="glbQbyBill" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/chat-luong/theo-bill.html?st=1">Theo hóa đơn</a>
                        </li>
                        <li id="glbQbyServey" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/chat-luong/theo-khao-sat.html">Theo khảo sát</a>
                        </li>
                        <li id="glbMistake" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/diem-tru/danh-sach.html">Thưởng/Phạt</a>
                        </li>
                        <li id="Li1" runat="server" visible="true" clientidmode="Static">
                            <a href="/admin/chat-luong/list-check3s.html">List Check 3S</a>
                        </li>
                        <li id="glbCheckClear" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/chat-luong/check3s.html">Check 3S</a>
                        </li>
                        <li id="Li2" runat="server" visible="true" clientidmode="Static">
                            <a href="/admin/chat-luong/checkcsvc_l.html">List Check CSVC</a>
                        </li>
                        <li id="glbCheckCSVC" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/chat-luong/checkcsvc.html">Check CSVC</a>
                        </li>
                        <li id="glbThongKeAnh" runat="server" visible="false" clientidmode="Static">
                            <a href="/thong-ke-anh.html">Check ảnh của Stylist</a>
                        </li>
                        <li id="glbXemAnhLichSu" runat="server" visible="false" clientidmode="Static">
                            <a href="/anh-ngau-nhien.html">Xem ảnh lịch sử</a>
                        </li>
                    </ul>
                </li>
                <li id="glbProfile" runat="server" visible="false" clientidmode="Static">
                    <a href="/nhan-vien/<%=Session["User_Id"] %>.html">Thông tin cá nhân</a>
                </li>
                <li id="glbServey" runat="server" visible="false" clientidmode="Static">
                    <a href="/khao-sat/nhan-vien.html">Khảo sát</a>
                </li>
                <li id="glbGuide" runat="server" visible="false" clientidmode="Static">
                    <a href="/assets/js/pdf.js/web/viewer.html?file=/Public/Media/Upload/Images/UserGuide/SalonManager.pdf">Quy trình công việc</a>
                </li>
                <li id="glbS4M" runat="server" visible="false" clientidmode="Static">
                    <a href="javascript:void(0);">Stylist4Men</a>
                    <ul class="ul-sub-menu">
                        <li id="glbS4MClass" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/s4m/lop-hoc/danh-sach.html">Lớp học</a>
                        </li>
                        <li id="glbS4MStudent" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/s4m/hoc-vien/danh-sach.html">Học viên</a>
                        </li>
                        <li id="glbS4MTrainingImage" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/s4m/anh-training.html">Ảnh traning</a>
                        </li>
                    </ul>
                </li>

                <li id="glbLuongDoanhSoCheckin" runat="server" visible="false" clientidmode="Static">
                    <a href="/checkin/luong-doanh-so.html">Lương doanh số Checkin</a>
                    <%--<ul class="ul-sub-menu">
                        <li id="Li6" runat="server" visible="false" clientidmode="Static" style="display: none;">
                            <a href="/admin/cong-nhat/diem-danh.html">Ngày công</a>
                        </li>
                    </ul>--%>
                </li>

                <li id="glbTuyenDung" runat="server" visible="false" clientidmode="Static">
                    <a href="javascript:void(0);">Tuyển dụng</a>
                    <ul class="ul-sub-menu">
                        <li id="glbTuyenDung_Step1" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/tuyen-dung/step-1/them-moi">Bước 1 - Thêm thông tin ứng viên</a>
                        </li>
                        <%--<li id="glbTuyenDung_Step2" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/tuyen-dung/step-2/danh-sach">Bước 2 - Thêm ảnh/video</a>
                        </li>--%>
                        <li id="glbTuyenDung_Step3" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/tuyen-dung/step-3/danh-sach">Bước 2 - Duyệt</a>
                        </li>
                        <li id="glbTuyenDung_DaDuyet" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/tuyen-dung/step-3/da-duyet">Danh sách đã duyệt</a>                            
                        </li>
                        <li id="glbTuyenDung_Daotao" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/tuyen-dung/step-4/danh-sach-dao-tao">Danh sách đào tạo S4M</a>                            
                        </li>
                        
                    </ul>
                </li>

                <li id="glbDatHangNoiBo" runat="server" visible="false" clientidmode="Static">
                    <a href="javascript:void(0);">Đặt hàng nội bộ</a>
                    <ul class="ul-sub-menu">
                        <li id="glbDatHangNoiBo_ChoSalon" runat="server" visible="false" clientidmode="Static">
                            <a href="javascript:void(0);">Dành cho salon</a>
                            <ul class="ul-sub2-menu">
                                <li id="Li7" runat="server" clientidmode="Static">
                                    <a href="/dat-hang-kho/salon/them-moi">Lập đơn mới</a>
                                </li>
                                <li id="Li9" runat="server" clientidmode="Static">
                                    <a href="/dat-hang-kho/salon/danh-sach">Danh sách đơn hàng</a>
                                </li>
                            </ul>
                        </li>
                        <li id="glbDatHangNoiBo_ChoKho" runat="server" visible="false" clientidmode="Static">
                            <a href="javascript:void(0);">Dành cho kho</a>
                            <ul class="ul-sub2-menu">
                                <li id="Li11" runat="server" clientidmode="Static">
                                    <a href="/dat-hang-kho/kho/danh-sach">Danh sách đơn hàng</a>
                                </li>
                            </ul>
                        </li>
                        <li id="glbDatHangNoiBo_DanhSachTraThieu" runat="server" visible="true" clientidmode="Static">
                            <a href="/dat-hang-kho/danh-sach-tra-thieu">Danh sách hàng trả thiếu</a>
                        </li>
                        <li id="glbDatHangNoiBo_XuatHoaDon" runat="server" hidden clientidmode="Static">
                            <a href="/dat-hang-kho/kho/xuat-hoa-don">Xuất hóa đơn</a>
                        </li>
                    </ul>
                </li>                

                <li id="glbSuKien" runat="server" visible="false" clientidmode="Static">
                    <a href="javascript:void(0);">Sự kiện</a>
                    <ul class="ul-sub-menu">
                        <li id="glb_Sukien_TeamThiDua" runat="server" visible="false" clientidmode="Static">
                            <a href="/sukien/team-thi-dua">Team thi đua</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>

<script type="text/ecmascript">
    jQuery(document).ready(function () {
        // set active
        $("#glbMenu li").bind("click", function () {
            $("#glbMenu li.active").removeClass("active");
            $(this).addClass("active");
        });

        //show header login panel
        $("#Avatar").bind("click", function () {
            $("#HeaderUser").toggle();
        });
        $(window).bind("click", function (e) {
            if (e.target.className !== "avatar-img") {
                $("#HeaderUser").hide();
            }
        });
    });
</script>

