﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NavPanel.ascx.cs" Inherits="_30shine.GUI.UIControls.NavPanel" %>

<ul class="header-left-menu" id="MenurLeftHeade">
    <li id="MLHToggle"><a href="javascript://"><i class="fa fa-align-justify"></i></a></li>
    <%--<li id="MLHEmail"><a href="javascript://"><i class="fa fa-envelope-o"></i></a></li>
    <li id="MLHBell"><a href="javascript://"><i class="fa fa-bell-o"></i></a></li>--%>
</ul> 

<div class="nav-container is-hide" id="NavPanel">    
    <div class="scroll-wrapper">
        <!-- BEGIN - scrollbar-padder -->
        <div class="scrollbar-padder">
            <!-- BEGIN - sidebar-header -->
            <div class="sidebar-header">
                <a href="javascript:void(0)" class="pull-left visible-xs" 
                        data-toggle-class="phone-menu-visible" data-target=".content-wrapper">
                    <i class="fa fa-bars fa-fw"></i>
                </a>
                <a data-ui-sref="app.dashboard.dashboard-1" class="ls-sm">
                    <span>30Shine Solution</span> 
                </a>            
            </div>
            <!-- END - sidebar-header -->
            <!-- BEGIN - navigation-items -->
            <ul id="navigation-items" class="nav">
                
                <li class="menu-header"><span>Thành phần</span></li>
                <li><a href="javascript://" class="waves-effect"><i class="fa fa-th-large"></i><span>Dashboard</span> </a></li>

                <li class="menu-header"><span>Nhập liệu</span></li>                
                <li runat="server" id="navCustomer" Visible="false" ClientIDMode="Static"><a href="/khach-hang/danh-sach.html" class="waves-effect"><i class="fa fa-users"></i><span>Khách hàng</span> </a></li>
                <li runat="server" id="navService" Visible="false" ClientIDMode="Static"><a href="/dich-vu/danh-sach.html" class="waves-effect"><i class="fa fa-sun-o"></i><span>Hóa đơn</span> </a></li>
                <li runat="server" id="navBillSale" Visible="false" ClientIDMode="Static"><a href="/don-hang/danh-sach.html" class="waves-effect"><i class="fa fa-sun-o"></i><span>Đơn hàng</span> </a></li>
                <li runat="server" id="navInventory" Visible="false" ClientIDMode="Static"><a href="/ton-kho/danh-sach.html" class="waves-effect"><i class="fa fa-sun-o"></i><span>Tồn kho</span> </a></li>
                <li class="menu-header"><span>Quản trị nội dung</span></li>

                <%--<li class="dcjq-parent-li">
                    <a href="javascript:void(0)" class="waves-effect dcjq-parent"><i class="icon-screen-desktop"></i><span>UI Elements</span> <span class="dcjq-icon"></span></a><ul class="sub-menu" style="display: none;">
                        <li><a href="ui.buttons.html"><i class="fa fa-angle-right"></i><span>Buttons</span> </a></li>
                        <li><a href="ui.components.html"><i class="fa fa-angle-right"></i><span>Components</span> </a></li>
                    </ul>
                </li>--%>
                        
                <li runat="server" id="navAdminService" Visible="false" ClientIDMode="Static"><a href="/admin/dich-vu.html" class="waves-effect"><i class="fa fa-sun-o"></i><span>Dịch vụ</span> </a></li>
                <li runat="server" id="navAdminProduct" Visible="false" ClientIDMode="Static"><a href="/admin/san-pham.html" class="waves-effect"><i class="fa fa-suitcase"></i><span>Sản phẩm</span> </a></li>
                <li runat="server" id="navAdminCategory" Visible="false" ClientIDMode="Static"><a href="/admin/danh-muc.html" class="waves-effect"><i class="fa fa-suitcase"></i><span>Danh mục</span> </a></li>
                <li runat="server" id="navAdminSocialThread" Visible="false" ClientIDMode="Static"><a href="/admin/nguon-thong-tin.html" class="waves-effect"><i class="fa fa-stumbleupon"></i><span>Nguồn thông tin</span> </a></li>
                <li runat="server" id="navAdminSalon" Visible="false" ClientIDMode="Static"><a href="/admin/salon.html" class="waves-effect"><i class="fa fa-university"></i><span>Salon</span> </a></li>
                <li runat="server" id="navAdminStaff" Visible="false" ClientIDMode="Static"><a href="/admin/nhan-vien.html" class="waves-effect"><i class="fa fa-user"></i><span>Nhân viên</span> </a></li>
                <li runat="server" id="navAdminTypeStaff" Visible="false" ClientIDMode="Static"><a href="/admin/kieu-nhan-vien.html" class="waves-effect"><i class="fa fa-user"></i><span>Bộ phận</span> </a></li>
                <li runat="server" id="navAdminNotification" Visible="false" ClientIDMode="Static"><a href="/admin/notification/sender.html" class="waves-effect"><i class="fa fa-user"></i><span>Notification</span> </a></li>

                <li runat="server" id="navAdminAccount" Visible="false" ClientIDMode="Static" style="display:none;"><a href="/admin/account/danh-sach.html" class="waves-effect"><i class="fa fa-user"></i><span>Tài khoản đăng nhập</span> </a></li>
                <li runat="server" id="navProfile" Visible="false" ClientIDMode="Static"><a href="/nhan-vien/<%=Session["User_Id"] %>.html" class="waves-effect"><i class="fa fa-sun-o"></i><span>Thông tin cá nhân</span> </a></li>
                <li runat="server" id="navServey" Visible="false" ClientIDMode="Static"><a href="/khao-sat/nhan-vien.html" class="waves-effect"><i class="fa fa-sun-o"></i><span>Khảo sát</span> </a></li>
                <li class="menu-header"><span>Export dữ liệu</span></li>
                <li runat="server" id="navAdminReport" Visible="false" ClientIDMode="Static">
                    <% if(Permission == "reception" || Permission == "checkin" || Permission == "checkout"){%>
                        <a href="/admin/bao-cao/doanh-so.html" class="waves-effect"><i class="fa fa-retweet"></i><span>Báo cáo</span> </a>
                    <% }else{ %>
                        <a href="/admin/bao-cao/hoa-don.html" class="waves-effect"><i class="fa fa-retweet"></i><span>Báo cáo</span> </a>
                    <% } %>
                </li>
            </ul>
            <!-- END - navigation-items -->       
        </div>
        <!-- BEGIN - scrollbar-padder -->
    </div>    
</div>

<script type="text/ecmascript">
    var NavPanel, MLHToggle, MenurLeftHeade, NavSpanText,
        BodyWrapper, SiteWrapper,
        NavFullWidth = 200,
        NavSmallWidth = 36,
        ToggleIsclick = false,
        IsRemapWidth = false,
        IsNavPanelHide = false;

    jQuery(document).ready(function ($) {
        // Set variable
        NavPanel = $("#NavPanel"),
        MLHToggle = $("#MLHToggle"),
        MenurLeftHeade = $("#MenurLeftHeade"),
        NavSpanText = $(".nav-container .scrollbar-padder #navigation-items > li span"),
        NavSpanTextActive = $(".nav-container .scrollbar-padder #navigation-items > li a.active"),
        BodyWrapper = $("#BodyWrapper"),
        SiteWrapper = $("#SiteWrapper");        
        
        //================================
        // Init check Toggle NavPanel
        //================================         
        IsRemapWidth = getCookie("Nav_IsLarge");
        if (IsRemapWidth == null) {            
            setCookie("Nav_IsLarge", false);
            setNavWidth(NavSmallWidth, false, false);
            setPaddingBodyWrapper(NavSmallWidth, false);
            //bindMouseEnter();
        } else {
            if (IsRemapWidth == "true") {
                setNavWidth(NavFullWidth, true, false);
                setPaddingBodyWrapper(NavFullWidth, false);
                //unBinMouseEnter();
                NavPanel.removeClass("is-hide");
                ToggleIsclick = true;
            } else if(IsRemapWidth == "false") {
                setNavWidth(NavSmallWidth, false, false);
                setPaddingBodyWrapper(NavSmallWidth, false);
                //bindMouseEnter();
            }
        }

        //================================
        // Toggle NavPanel
        //================================        
        MLHToggle.bind("click", function () {
            ToggleNavPanel();
            ToggleIsclick = ToggleIsclick == true ? false : true;
            if (ToggleIsclick) {
                //unBinMouseEnter();
            }
            else {
                //bindMouseEnter();
            }
        });

        //================================
        // Set active nav item
        //================================  
        //var path = window.location.pathname;
        //NavSpanTextActive.removeClass("active");
        //$(".nav-container .scrollbar-padder #navigation-items > li a[href='"+ path +"']").addClass("active");
        setTimeout(function(){
            var cmp = $("ul#glbMenu li.active a").text();
            cmp = cmp == "" ? "Dashboard" : cmp;
            $(".nav-container .scrollbar-padder #navigation-items > li span:contains('"+cmp+"')").parent().addClass("active");
        }, 200);

    });

    function ToggleNavPanel() {        
        var isHide = NavPanel.hasClass("is-hide");
        if (isHide) {
            MenurLeftHeade.animate({ left: NavFullWidth });
            NavPanel.animate({ width: NavFullWidth }, function () {
                setPaddingBodyWrapper(NavFullWidth, true);
            });
            NavPanel.removeClass("is-hide");
            NavSpanText.fadeTo("slow", 1);
            UpdateCookie_IsNavLarge(true);
        } else {
            MenurLeftHeade.animate({ left: NavSmallWidth });
            NavPanel.animate({ width: NavSmallWidth }, function () {
                setPaddingBodyWrapper(NavSmallWidth, true);
            });
            NavPanel.addClass("is-hide");
            NavSpanText.fadeTo("slow", 0);
            UpdateCookie_IsNavLarge(false);
        }
    }

    function bindMouseEnter(remapwidth) {
        NavPanel.unbind("mouseenter").bind("mouseenter", function () {
            $(this).unbind("mouseleave");
            MenurLeftHeade.animate({ left: NavFullWidth });
            NavPanel.animate({ width: NavFullWidth }, function () {
                $(this).bind("mouseleave", function () {
                    MenurLeftHeade.animate({ left: NavSmallWidth });
                    NavPanel.animate({ width: NavSmallWidth });
                    NavPanel.addClass("is-hide");
                    NavSpanText.fadeTo("slow", 0);
                });
            });
            NavPanel.removeClass("is-hide");
            NavSpanText.fadeTo("slow", 1);
        });        
    }

    function unBinMouseEnter() {
        NavPanel.unbind("mouseenter");
        NavPanel.unbind("mouseleave");
    }

    function setPaddingBodyWrapper(NavWidth, IsAnimate) {
        var IsAnimate = IsAnimate;
        var NavWidth = NavWidth || NavPanel.width();
        if (IsAnimate) {
            BodyWrapper.animate({ paddingLeft: NavWidth });
        } else {
            BodyWrapper.css({"padding-left" : NavWidth + "px"});
        }
    }

    function setNavWidth(NavWidth, ShowText, IsAnimate) {
        var IsAnimate = IsAnimate;
        var NavWidth = NavWidth || NavPanel.width();
        var ShowText = ShowText || false;
        
        if (IsAnimate) {
            NavPanel.animate({ width: NavWidth });
            MenurLeftHeade.animate({ left: NavWidth });
        } else {
            NavPanel.css({ "width": NavWidth + "px" });
            MenurLeftHeade.css({ "left": NavWidth + "px" });
        }

        if (ShowText) {
            NavSpanText.animate({opacity : 1});
        } else {
            NavSpanText.animate({ opacity: 0 });
        }
    }

    var docCookies = {
        getItem: function (sKey) {
            if (!sKey) { return null; }
            return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;
        },
        setItem: function (sKey, sValue, vEnd, sPath, sDomain, bSecure) {
            if (!sKey || /^(?:expires|max\-age|path|domain|secure)$/i.test(sKey)) { return false; }
            var sExpires = "";
            if (vEnd) {
                switch (vEnd.constructor) {
                    case Number:
                        sExpires = vEnd === Infinity ? "; expires=Fri, 31 Dec 9999 23:59:59 GMT" : "; max-age=" + vEnd;
                        break;
                    case String:
                        sExpires = "; expires=" + vEnd;
                        break;
                    case Date:
                        sExpires = "; expires=" + vEnd.toUTCString();
                        break;
                }
            }
            document.cookie = encodeURIComponent(sKey) + "=" + encodeURIComponent(sValue) + sExpires + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "") + (bSecure ? "; secure" : "");
            return true;
        },
        removeItem: function (sKey, sPath, sDomain) {
            if (!this.hasItem(sKey)) { return false; }
            document.cookie = encodeURIComponent(sKey) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT" + (sDomain ? "; domain=" + sDomain : "") + (sPath ? "; path=" + sPath : "");
            return true;
        },
        hasItem: function (sKey) {
            if (!sKey) { return false; }
            return (new RegExp("(?:^|;\\s*)" + encodeURIComponent(sKey).replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=")).test(document.cookie);
        },
        keys: function () {
            var aKeys = document.cookie.replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, "").split(/\s*(?:\=[^;]*)?;\s*/);
            for (var nLen = aKeys.length, nIdx = 0; nIdx < nLen; nIdx++) { aKeys[nIdx] = decodeURIComponent(aKeys[nIdx]); }
            return aKeys;
        }
    };

    function setCookie(cname, cvalue, exdays) {
        var exdays = exdays || 365;
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        docCookies.setItem(cname, cvalue, exdays, "/", "solution.30shine.net");
    }

    function getCookie(cname) {
        return docCookies.getItem(cname);
    }

    function UpdateCookie_IsNavLarge(val) {
        docCookies.removeItem("Nav_IsLarge", "/", "solution.30shine.net");
        setCookie("Nav_IsLarge", val, 365);
    }
    
</script>
