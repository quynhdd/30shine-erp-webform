﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Header_V2.ascx.cs" Inherits="_30shine.GUI.UIControls.Header_V2" %>

<link href="/Assets/css/menu_mobile.css?12379349" rel="stylesheet" />
<script src="/Assets/js/Menu_mobile.js"></script>
<style>
    /*ul#glbMenu li ul.ul-sub-menu li:hover>ul.ul-sub-menu,ul#glbMenu li ul.ul-sub-menu li ul.ul-sub-menu li:hover>ul.ul-sub-menu{
        display:block !important;
    }
    ul#glbMenu li ul.ul-sub-menu li ul.ul-sub-menu,ul#glbMenu li ul.ul-sub-menu li ul.ul-sub-menu li ul.ul-sub-menu{
            position: absolute;
             top: -1px; 
             left: 100%; 
             margin: 0 auto; 
             text-align: left; 
             white-space: nowrap; 
             width: auto; 
             border: 1px solid #dfdfdf; 
             display:none !important;

    }*/
    .box-menu {
        box-shadow: 2px 2px 2px 2px #b8b8b8;
        min-height: 100px;
        max-height: 200px;
        min-width: 65px;
        height: 150px;
    }

    .text-box-submenu {
        min-height: 100px;
        text-align: center;
    }

    @media (min-width: 320px) {

        .c-menu--slide-left, .c-menu--slide-right, .c-menu--push-left, .c-menu--push-right {
            width: 50%;
            /* min-width: 350px; */
        }
    }
</style>
<div class="wp global-menu " style="margin-top: 0px;" id="o-wrapper">
    <div class="container" style="background: #000000;">
        <!-- Menu desktop -->
        <div class="row">
            <strong class="text-head">Phần mềm quản lý hair salon</strong>
        </div>
        <%--<div class="row menu-desktop">
            <ul class="ul-global-menu" id="glbMenu" runat="server" clientidmode="Static">
                <li>
                    <img src="/Assets/images/ic_logo_white.png" style="margin-right: 10px; margin-top: -5px; width: 75px;" />
                </li>
                <li id="glbCustomer" runat="server" visible="false" clientidmode="Static">
                    <a href="/khach-hang/danh-sach.html">Khách hàng</a>
                    <ul class="ul-sub-menu">
                        <li id="Li15" runat="server" visible="true" clientidmode="Static">
                            <a href="/khach-hang/tim-kiem.html">Tìm kiếm</a>
                        </li>
                        <li id="Li12" runat="server" visible="true" clientidmode="Static">
                            <a href="/khach-hang/them-moi.html">Thêm mới</a>
                        </li>
                        <li id="Li13" runat="server" visible="true" clientidmode="Static">
                            <a href="/khach-hang/danh-sach.html">Danh sách</a>
                        </li>
                        <li id="L13" runat="server" visible="true" clientidmode="Static">
                            <a href="/khach-hang/hoa-chat">CSKH</a>
                        </li>
                        <li id="Li10" runat="server" visible="true" clientidmode="Static">
                            <a href="/khach-hang/dac-biet">Đặc biệt</a>
                        </li>
                        <li id="Li14" runat="server" visible="true" clientidmode="Static">
                            <a href="/khach-hang/VIP">VIP</a>
                        </li>
                    </ul>
                </li>
                <li id="glbBooking" runat="server" visible="false" clientidmode="Static">
                    <a href="javascript:void(0)">Booking</a>
                    <ul class="ul-sub-menu">
                        <li id="glbAdminBookingHour" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/booking/hour.html">Cấu hình khung giờ</a>
                        </li>
                        <li id="glbAdminBooking" runat="server" visible="false" clientidmode="Static" style="display: none;">
                            <a href="/admin/listing-booking.html">Danh sách Booking</a>
                        </li>
                        <li id="glbAdminBooking_Test" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/listing-booking-test.html">Danh sách Booking</a>
                        </li>
                        <li id="glbAdminBooking_Call" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/listing-booking-old-customer.html">Danh sách gọi nhắc lịch</a>
                        </li>
                        <li id="gblAdminStatisticBooking" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/thong-ke-booking.html">Thống kê Booking</a>
                        </li>
                        <li id="gblAdminStatisticBooking_Data" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/thong-ke-booking.html">Thống kê dữ liệu booking</a>
                        </li>
                        <li id="glbAdminBooking_GoiHoTroDatLich" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/booking/goi-ho-tro-dat-lich.html">Danh sách gọi hỗ trợ đặt lịch</a>
                        </li>
                    </ul>
                </li>
                <li id="glbService" runat="server" visible="false" clientidmode="Static">
                    <a href="/dich-vu/danh-sach.html">Hóa đơn</a>
                    <ul class="ul-sub-menu">
                        <li id="Li5" runat="server" visible="true" clientidmode="Static">
                            <a href="/timeline">Checkin</a>
                        </li>
                        <li id="Li6" runat="server" visible="true" clientidmode="Static">
                            <a href="/dich-vu/pending.html">Pending</a>
                        </li>
                        <li id="Li8" runat="server" visible="true" clientidmode="Static">
                            <a href="/dich-vu/danh-sach.html">Danh sách</a>
                        </li>
                    </ul>
                </li>
                <li id="glbAdminSales" runat="server" visible="false" clientidmode="Static">

                    <a href="javascript://">Báo cáo</a>
                    <ul class="ul-sub-menu">
                        <li id="glbAdminReportSales" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/bao-cao/hoa-don.html">Vận hành</a>
                        </li>
                        <li id="glbAdminReportProduct" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/bao-cao/doanh-so-my-pham.html">Mỹ phẩm</a>
                        </li>
                        <li id="glbAdminReportService" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/bao-cao/doanh-so-dich-vu.html">Dịch vụ</a>
                        </li>
                        <li id="glbAdminReportStaff" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/nhan-vien/nang-suat-stylist.html">Năng suất Stylist</a>
                        </li>
                        <li id="glbAdminReportSalary" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/bao-cao/salary-v3.html">Tính lương</a>
                        </li>
                        <li id="glbAdminKetQuaKinhDoanh" runat="server" visible="false" clientidmode="Static">
                            <a href="javascript:void(0)">KQKD</a>
                            <ul class="ul-sub2-menu">
                                <li id="Li3" runat="server" clientidmode="Static">
                                    <a href="/admin/bao-cao/ket-qua-kinh-doanh/nhap-lieu.html">Nhập dữ liệu</a>
                                </li>
                                <li id="Li4" runat="server" clientidmode="Static">
                                    <a href="/admin/bao-cao/ket-qua-kinh-doanh.html">Tóm tắt báo cáo</a>
                                </li>
                            </ul>
                        </li>
                        <li id="report_checkin_salary" runat="server" visible="false" clientidmode="Static">
                            <a href="/checkin/luong-doanh-so.html">Doanh số checkin</a>
                        </li>
                    </ul>
                </li>
                <li id="glbTimeKeeping" runat="server" visible="false" clientidmode="Static">
                    <a href="javascript:void(0);">Chấm công</a>
                    <ul class="ul-sub-menu">
                        <li id="glbWorkDay" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/cong-nhat/diem-danh.html">Ngày công</a>
                        </li>
                        <li id="glbthongke" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/thong-ke-cham-cong.html">Thống kê salon</a>
                        </li>
                        <li id="glbAdminStaff" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/nhan-vien.html">DS Nhân viên</a>
                        </li>
                        <li id="glbAddStaff" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/nhan-vien/them-moi.html">Thêm mới NV</a>
                        </li>
                        
                         
                          <li id="glLevelup2" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/bao-cao-nang-bac.html">Báo cáo nâng bậc</a>
                              <ul class="ul-sub2-menu">
                                    <li id="glLevelup" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/danh-sach-xet-nang-bac.html">Danh sách xét nâng bậc</a>
                        </li>
                                  <li id="glLevelup1" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/danh-sach-duyet-nang-bac.html">Danh sách duyệt nâng bậc</a>
                        </li>
                              </ul>
                        </li>
                    </ul>
                </li>
                <li id="glbQuanlity" runat="server" visible="false" clientidmode="Static">
                    <a href="javascript:void(0);">Chất lượng</a>
                    <ul class="ul-sub-menu">
                        <li id="glbQbyBill" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/chat-luong/theo-bill.html?st=1">Điểm hài lòng</a>
                        </li>
                        <li id="glbQbyServey" runat="server" visible="false" clientidmode="Static" style="display: none;">
                            <a href="/admin/chat-luong/theo-khao-sat.html">Theo khảo sát</a>
                        </li>
                        <li id="glbMistake" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/diem-tru/danh-sach.html">Thưởng/Phạt</a>
                        </li>
                        <li id="Li1" runat="server" visible="true" clientidmode="Static" style="display: none;">
                            <a href="/admin/chat-luong/list-check3s.html">List Check 3S</a>
                        </li>
                        <li id="glbCheckClear" runat="server" visible="false" clientidmode="Static" style="display: none;">
                            <a href="/admin/chat-luong/check3s.html">Check 3S</a>
                        </li>
                        <li id="Li2" runat="server" visible="true" clientidmode="Static" style="display: none;">
                            <a href="/admin/chat-luong/checkcsvc_l.html">List Check CSVC</a>
                        </li>
                        <li id="glbCheckCSVC" runat="server" visible="false" clientidmode="Static" style="display: none;">
                            <a href="/admin/chat-luong/checkcsvc.html">Check CSVC</a>
                        </li>
                        <li id="glbThongKeAnh" runat="server" visible="false" clientidmode="Static">
                            <a href="/thong-ke-anh.html">KCS</a>
                        </li>
                        <li id="glbXemAnhLichSu" runat="server" visible="false" clientidmode="Static">
                            <a href="/anh-ngau-nhien.html">Xem ảnh lịch sử</a>
                        </li>
                    </ul>
                </li>
                

                <li id="glbTuyendungV2" runat="server" visible="false" clientidmode="Static">
                    <a href="javascript:void(0);">Tuyển dụng</a>
                    <ul class="ul-sub-menu">
                        <li id="glbCreatCV" runat="server" visible="false" clientidmode="Static">
                            <a href="javascript:void(0);">Lập hồ sơ</a>
                            <ul class="ul-sub2-menu">
                                <li id="glbCreateNewCV" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/tuyen-dung/lap-ho-so/tao-moi-ho-so.html">Tạo mới hồ sơ</a>
                                </li>
                                <li id="glbReportCV" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/tuyen-dung/lap-ho-so/danh-sach-ho-so.html">Danh sách hồ sơ mới</a>
                                </li>
                                <li id="glbChangeCV" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/tuyen-dung/lap-ho-so/danh-sach-chuyen-ho-so.html">Chuyển hồ sơ từ S4M</a>
                                </li>
                            </ul>
                        </li>
                          <li id="glbConfirmSkill" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/tuyen-dung/danh-sach-da-test-ky-nang.html">Danh sách chờ duyệt</a>
                        </li>
                        
                        
                        <li id="glbReportConfirm" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/tuyen-dung/danh-sach-da-duyet.html">DS đã duyệt</a>
                        </li>
                         <li id="glbReportResult" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/tuyen-dung/bao-cao-danh-gia.html">Báo cáo đánh giá</a>
                        </li>
                       
                        <li id="glbSkillManager" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/tuyen-dung/quan-tri-ky-nang.html">Quản trị kỹ năng</a>
                        </li>
                        <li id="glbTesterManager" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/tuyen-dung/danh-sach-nguoi-test.html">Quản lý Tester</a>
                        </li>
                    </ul>
                </li>

                
                <li id="glbS4M" runat="server" visible="false" clientidmode="Static">
                    <a href="javascript:void(0);">S4M</a>
                    <ul class="ul-sub-menu">
                        <li id="glbS4MClass" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/s4m/lop-hoc/danh-sach.html">Lớp học</a>
                        </li>
                        <li id="glbS4MStudent" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/s4m/hoc-vien/danh-sach.html">Học viên</a>
                        </li>
                        <li id="glbS4MTrainingImage" runat="server" visible="false" clientidmode="Static" style="display: none;">
                            <a href="/admin/s4m/anh-training.html">Ảnh traning</a>
                        </li>
                        <li id="glbS4MSalon" runat="server" clientidmode="Static" style="display: none;">
                            <a href="/admin/s4m/salon.html">Salon</a>
                        </li>
                        <li id="glbS4MCheckImageCut" runat="server" visible="false" clientidmode="Static">
                            <a href="/s4m/thong-ke-anh.html">KCS</a>
                        </li>
                    </ul>
                </li>
                <li id="glbBillSale" runat="server" visible="false" clientidmode="Static" style="display: none;">
                    <a href="/don-hang/danh-sach.html">Đơn hàng</a>
                </li>
                <li id="glbInventory" runat="server" visible="false" clientidmode="Static">
                    <a href="/ton-kho/danh-sach.html">MP bán</a>
                </li>
                <li id="glbHangDV" runat="server" visible="true" clientidmode="Static">
                    <a href="javascript:void(0);">Hàng DV</a>
                    <ul class="ul-sub-menu">
                        <li id="glbExportGoods" runat="server" visible="false" clientidmode="Static">
                            <a href="javascript:void(0);">Xuất VT</a>
                            <ul class="ul-sub2-menu">
                                <li id="glbExportGoods_Salon" runat="server" visible="false" clientidmode="Static" style="display: none;">
                                    <a href="/my-pham/xuat-salon/xuat-moi.html">Cho salon</a>
                                </li>
                                <li id="glbExportGoods_Staff" runat="server" visible="false" clientidmode="Static">
                                    <a href="/my-pham/xuat-nv/xuat-moi.html">Cho nhân viên</a>
                                </li>
                                <li id="glbExportGoods_Report_By_Staff" runat="server" visible="false" clientidmode="Static">
                                    <a href="/my-pham/thong-ke/so-bill-dich-vu.html">Thống kê dịch vụ</a>
                                </li>
                            </ul>
                        </li>
                        <li id="glbDatHangNoiBo" runat="server" visible="false" clientidmode="Static">
                            <a href="javascript:void(0);">Hàng hóa</a>
                            <ul class="ul-sub2-menu">
                                <li id="glbDatHangNoiBo_ChoSalon" runat="server" visible="false" clientidmode="Static">
                                    <a href="javascript:void(0);">Salon đặt hàng</a>
                                    <ul class="ul-sub2-menu">
                                        <li id="Li7" runat="server" clientidmode="Static">
                                            <a href="/dat-hang-kho/salon/them-moi">Lập đơn mới</a>
                                        </li>
                                        <li id="Li9" runat="server" clientidmode="Static">
                                            <a href="/dat-hang-kho/salon/danh-sach">Danh sách đơn hàng</a>
                                        </li>
                                    </ul>
                                </li>
                                <li id="glbDatHangNoiBo_ChoKho" runat="server" visible="false" clientidmode="Static">
                                    <a href="javascript:void(0);">Kho trả hàng</a>
                                    <ul class="ul-sub2-menu">
                                        <li id="Li11" runat="server" clientidmode="Static">
                                            <a href="/dat-hang-kho/kho/danh-sach">Danh sách đơn hàng</a>
                                        </li>
                                           <li id="Li17" runat="server" clientidmode="Static">
                                    <a href="/admin/kho-tra-hang/thong-ke-xuat-vat-tu.html">Thống kê xuất</a>
                                </li>
                                    </ul>
                                </li>
                                <li id="glbDatHangNoiBo_DanhSachTraThieu" runat="server" visible="true" clientidmode="Static">
                                    <a href="/dat-hang-kho/danh-sach-tra-thieu">DS trả thiếu</a>
                                </li>
                                <li id="glbDatHangNoiBo_XuatHoaDon" runat="server" hidden clientidmode="Static">
                                    <a href="/dat-hang-kho/kho/xuat-hoa-don">Xuất hóa đơn</a>
                                </li>
                                  <li id="Li18" runat="server"  clientidmode="Static">
                                    <a href="/admin/hang-hoa/danh-sach-san-pham-ton.html">Danh sách sp tồn</a>
                                </li>                                                           
                                 <li id="Li19" runat="server"  clientidmode="Static">
                                    <a href="/admin/hang-hoa/thong-ke-muc-dung-vat-tu.html">Mức dùng vật tư</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li id="glbAdminReport" runat="server" visible="false" clientidmode="Static" style="display: none;">
                    <a href="/admin/bao-cao/bieu-do.html">Biểu đồ</a>
                </li>

                <li id="glbProfile" runat="server" visible="false" clientidmode="Static">
                    <a href="/nhan-vien/<%=Session["User_Id"] %>.html">Thông tin cá nhân</a>
                </li>
                <li id="glbServey" runat="server" visible="false" clientidmode="Static">
                    <a href="/khao-sat/nhan-vien.html">Khảo sát</a>
                </li>
                <li id="glbGuide" runat="server" visible="false" clientidmode="Static">
                    <a href="/assets/js/pdf.js/web/viewer.html?file=/Public/Media/Upload/Images/UserGuide/SalonManager.pdf">Quy trình công việc</a>
                </li>


                <li id="glbLuongDoanhSoCheckin" runat="server" visible="false" clientidmode="Static" style="display: none;">
                    <a href="/checkin/luong-doanh-so.html">Lương doanh số Checkin</a>
                </li>

                <li id="glbSuKien" runat="server" visible="true" clientidmode="Static" style="display: none;">
                    <a href="javascript:void(0);">Sự kiện</a>
                    <ul class="ul-sub-menu">
                        <li id="glb_Sukien_TeamThiDua" runat="server" visible="false" clientidmode="Static">
                            <a href="/sukien/team-thi-dua">Team thi đua</a>
                        </li>
                    </ul>
                </li>
                <li id="glbMarketing" runat="server" visible="false" clientidmode="Static">
                    <a href="javascript:void(0);">Marketing</a>
                    <ul class="ul-sub-menu">
                        <li id="glbMarketing_Config" runat="server" visible="false" clientidmode="Static">
                            <a href="javascript:void(0)">Cấu hình</a>
                            <ul class="ul-sub2-menu">
                                <li id="glbMarketing_ItemDaily" runat="server" clientidmode="Static">
                                    <a href="/admin/marketing/item-chi-phi-hang-ngay">Item chi phí hàng ngày</a>
                                </li>
                            </ul>
                        </li>
                        <li id="glbMarketing_Import" runat="server" visible="false" clientidmode="Static">
                            <a href="javascript:void(0)">Nhập chi phí</a>
                            <ul class="ul-sub2-menu">
                                <li id="glbMarketing_Import_Daily" runat="server" clientidmode="Static">
                                    <a href="/admin/marketing/chi-phi-hang-ngay">Chi phí hàng ngày</a>
                                </li>
                                <li id="glbMarketing_Import_Plan" runat="server" clientidmode="Static">
                                    <a href="/admin/marketing/chi-phi-chien-dich">Chiến dịch</a>
                                </li>
                            </ul>
                        </li>
                        <li id="glbMarketing_Report" runat="server" visible="false" clientidmode="Static">
                            <a href="javascript:void(0)">Báo cáo</a>
                            <ul class="ul-sub2-menu">
                                <li id="glbMarketing_Report_CP" runat="server" clientidmode="Static">
                                    <a href="/admin/marketing/bao-cao/chi-phi">Chi phí marketing</a>
                                </li>
                                <li id="glbMarketing_Report_NS" runat="server" clientidmode="Static">
                                    <a href="/admin/marketing/bao-cao/ngan-sach">Ngân sách</a>
                                </li>
                            </ul>
                        </li>
                    </ul>

                </li>
                <li id="glbAdminContent" runat="server" visible="false" clientidmode="Static">
                    <a href="javascript:void(0);">Quản lý</a>
                    <ul class="ul-sub-menu">
                        <li id="glbAdminCommonManagerment" runat="server" visible="false" clientidmode="static">
                            <a href="javascript:void(0)">Quản lý chung</a>
                            <ul class="ul-sub2-menu">
                                <li id="glbAdminCategory" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/danh-muc/danh-sach.html">Danh mục</a>
                                </li>
                                <li id="glbAdminSocialThread" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/nguon-thong-tin.html">Nguồn thông tin</a>
                                </li>
                                <li id="Li16" runat="server" clientidmode="Static">
                                    <a href="/admin/membership/cau-hinh-uu-dai-xep-hang">Cấu hình membership</a>
                                </li>
                            </ul>
                        </li>
                        <li id="glbAdminService" runat="server" visible="false" clientidmode="Static">
                            <a href="javascript:void(0)">Dịch vụ</a>
                            <ul class="ul-sub2-menu">
                                <li id="glbAdminServiceListing" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/dich-vu.html">Danh sách dịch vụ</a>
                                </li>
                                <li id="glbAdminServiceConfigSalon" runat="server" clientidmode="Static">
                                    <a href="/admin/salon-service/salon-dich-vu.html">Cấu hình dịch vụ - salon</a>
                                </li>
                            </ul>
                        </li>
                        <li id="glbAdminProduct" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/san-pham.html">Sản phẩm</a>
                        </li>
                         <li id="Li20" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/groupproduct/danh-sach.html">Nhóm sản phẩm</a>
                        </li>
                        <li id="glbAdminSalon" runat="server" visible="false" clientidmode="Static">
                            <a href="/admin/salon.html">Salon</a>
                        </li>
                        <li id="mlStaff" runat="server" visible="false" clientidmode="Static">
                            <a href="javascript:void(0)">Nhân sự</a>
                            <ul class="ul-sub2-menu">
                                <li id="glbAdminStaffType" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/kieu-nhan-vien.html">Bộ phận</a>
                                </li>
                                <li id="glbAdminSkillLevel" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/bac-ky-nang/danh-sach.html">Bậc kỹ năng</a>
                                </li>
                                <li id="glbAdminAccount" runat="server" visible="false" clientidmode="Static" style="display: none;">
                                    <a href="/admin/account/danh-sach.html">Tài khoản đăng nhập</a>
                                </li>
                                <li id="glbAdminConfigSalary" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/cau-hinh-luong.html">Cấu hình lương</a>
                                </li>
                                <li id="glbAdminProcedure" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/quy-trinh/danh-sach.html">Quy trình / Quy định</a>
                                </li>
                                <li id="glbAdminGroupTeam" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/nhan-vien/group-team.html">Cấu hình Group - Team</a>
                                </li>
                                <li id="glbAdminLevelup" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/dieu-kien-len-bac.html">Điều kiện lên bậc</a>
                                </li>
   
                            </ul>
                        </li>

                        <li id="glbAdminAppMobile" runat="server" visible="false" clientidmode="static">
                            <a href="javascript:void(0)">App Mobile</a>
                            <ul class="ul-sub2-menu">
                                <li id="glbAdminSlide" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/slide/danh-sach.html">Slide</a>
                                </li>
                                <li id="glbAdminHairMode" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/kieu-toc/danh-sach.html">Kiểu tóc</a>
                                </li>
                                <li id="glbAdminHairAtt" runat="server" visible="false" clientidmode="Static" style="display: none;">
                                    <a href="/admin/loai-toc/danh-sach.html">Thuộc tính tóc</a>
                                </li>
                                <li id="glbAdminSkinAtt" runat="server" visible="false" clientidmode="Static" style="display: none;">
                                    <a href="/admin/loai-da/danh-sach.html">Thuộc tính da</a>
                                </li>
                                <li id="glbAdminNotification" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/notification/send.html">Notification</a>
                                </li>
                            </ul>
                        </li>
                        <li id="glbAdminMoneyManager" runat="server" visible="false" clientidmode="Static">
                            <a href="javascript:void(0)">Dòng tiền</a>
                            <ul class="ul-sub2-menu">
                                <li id="glbAdminFundOffenItem" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/ngan-sach/khoan-thuong-xuyen/danh-sach.html">Danh mục thu chi</a>
                                </li>
                                <li id="glbAdminFund" runat="server" visible="false" clientidmode="Static">
                                    <a href="/admin/ngan-sach/thu-chi/danh-sach.html">Dòng tiền</a>
                                </li>
                            </ul>
                        </li>
                        <li id="glbListBack" runat="server" visible="false" clientidmode="Static" style="display: none;">
                            <a href="/admin/danh-sach-chua-quay-lai-30-ngay.html">DS chưa quay lại sau 1 tháng</a>
                        </li>
                    </ul>
                </li>

                <li style="float: right; position: absolute; top: 0; right: 15px;"
                    id="login_detail">
                    <a href="javascript://"><i class="fa fa-envelope" style="color: #ffffff;"></i></a>
                    <a href="javascript://"><i class="fa fa-bell" style="color: #ffffff;"></i></a>
                    <p class="hello-user" style="color: #ffffff; display: inline-block; margin-top: 10px; margin-right: 15px;">Xin chào <%=_User %></p>
                    <div class="avatar-wrap" style="display: inline-block; position: relative;">
                        <a href="javascript://" class="avatar" id="Avatar">
                            <img class="avatar-img" alt="" title="" src="/Assets/images/avatar.man.white.png" style="width: 20px; height: 21px; position: relative; top: -2px;" />
                        </a>
                        <div class="header-user-wp" id="HeaderUser" style="position: absolute; z-index: 2000; display: none; background: #5d5d5d; margin-left: -65px; right: 7px; margin-right: -20px; margin-top: 5px; padding: 5px 10px;">
                            <ul class="user-login-panel">
                                <li><a href="javascript://">Sửa thông tin</a></li>
                                <li><a href="/dang-xuat.html">Thoát</a></li>
                            </ul>
                            <div class="triangle"></div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>--%>

        <div class="row menu-desktop">
            <ul class="ul-global-menu" id="glbMenu" runat="server" clientidmode="Static">
                <li>
                    <img src="/Assets/images/ic_logo_white.png" style="margin-right: 10px; margin-top: -5px; width: 75px;" />
                </li>
                <% Response.Write(Session["Menu"]); %>
                <%--<% Response.WriteFile("/Config/" + Session["User_Permission"] + ".html");  %>--%>
                <li style="float: right; position: absolute; top: 0; right: 15px;"
                    id="login_detail">
                    <a href="javascript://"><i class="fa fa-envelope" style="color: #ffffff;"></i></a>
                    <a href="javascript://"><i class="fa fa-bell" style="color: #ffffff;"></i></a>
                    <p class="hello-user" style="color: #ffffff; display: inline-block; margin-top: 10px; margin-right: 15px;">Xin chào <%=_User %></p>
                    <div class="avatar-wrap" style="display: inline-block; position: relative;">
                        <a href="javascript://" class="avatar" id="Avatar">
                            <img class="avatar-img" alt="" title="" src="/Assets/images/avatar.man.white.png" style="width: 20px; height: 21px; position: relative; top: -2px;" />
                        </a>
                        <div class="header-user-wp" id="HeaderUser" style="position: absolute; z-index: 2000; display: none; background: #5d5d5d; margin-left: -65px; right: 7px; margin-right: -20px; margin-top: 5px; padding: 5px 10px;">
                            <ul class="user-login-panel">
                                <li><a href="javascript://">Sửa thông tin</a></li>
                                <%if (Session["UserID"].ToString() != "0" && !Convert.ToBoolean(Session["IsLogin"]))
                                    {%>
                                <li><a href="/doi-mat-khau.html" target="_blank">Đổi mật khẩu</a></li>
                                <%} %>
                                <li><a href="/dang-xuat.html">Thoát</a></li>
                            </ul>
                            <div class="triangle"></div>
                        </div>
                    </div>
                </li>
                <%if (TimeLivePassword != null && TimeLivePassword <= 10 && Session["UserID"].ToString() != "0" && !Convert.ToBoolean(Session["IsLogin"]))
                    {%>
                <li style="float: right; position: absolute; top: 55px; right: 50px; background-color: transparent !important;">
                    <p style="color: red; font-size: 15px; position: relative; z-index: 1999;">Còn <%=TimeLivePassword %> ngày nữa là mật khẩu hết hạn. Đổi mật khẩu tại <a class="password-notification" target="_blank" style="color: #3366BB; text-decoration: underline;" href="/doi-mat-khau.html">đây</a></p>
                </li>
                <%} %>
            </ul>
        </div>
        <!-- Menu desktop -->

        <!-- Menu mobile -->
        <div class="row menu-mobile" style="position: relative; display: none;">
            <a href="/" class="logo-m">
                <img src="/Assets/images/ic_logo_white.png" style="margin: 10px 10px 10px 0; width: 75px;" />
            </a>
            <div class="toogle-menu-m" id="open-left-panel" style="display: inline-block;">
                <img src="/Assets/images/toggle.m.png" style="margin-top: 10px;">
            </div>

        </div>
        <div id="panel-menu-m">
            <nav id="c-menu--push-left" class="c-menu c-menu--push-left" style="display: none; overflow: scroll;">
                <ul class="global-menu-m" style="padding-bottom: 10px;">
                    <li>
                        <div class="row">
                            <div class="col-xs-6">
                                <img src="/Assets/images/logo/logo.jpg" style="float: left; width: 100px;" />
                            </div>
                            <div class="col-xs-6">
                                <button class="c-menu__close">
                                    <img src="/Assets/images/close-icon-menu.png" style="position: relative; top: 30px; width: 34px; height: 30px;" />
                                </button>
                            </div>
                        </div>
                    </li>
                </ul>
                <ul class="menu-childrent" style="display: none;">
                    <li class="header-menu">
                        <div class="row">
                            <div class="col-xs-6">
                                <img src="/Assets/images/logo/logo.jpg" style="float: left; width: 100px;" />
                            </div>
                            <div class="col-xs-6">
                                <button class="c-menu__close">
                                    <img src="/Assets/images/close-icon-menu.png" style="position: relative; top: 30px; width: 34px; height: 30px;" />
                                </button>
                            </div>
                        </div>
                    </li>

                </ul>
            </nav>
            <!-- /c-menu push-left -->
        </div>

        <div id="c-mask" class="c-mask"></div>
        <!-- Menu mobile -->

    </div>
</div>
<%--Build Menu Desktop--%>
<script>
    //function buildMenu(parent, items) {
    //    $.each(items, function (u,v) {
    //        var li = $("<li><a href=" + v.Url_Rewrite + ">" + v.mName + "</a></li>");
    //        li.appendTo(parent);
    //        if (v.lstMenu.length>0) {
    //            var ul = $("<ul class='ul-sub-menu'></ul>");
    //            ul.appendTo(li);
    //            buildMenu(ul, v.lstMenu);
    //        }
    //    })
    //}
    //$(document).ready(function () {
    //    $.ajax({
    //        url: "/GUI/SystemService/WebService/MenuService.asmx/ListMenu",
    //        datatype: "JSON",
    //        contentType: "application/json; charset:UTF-8",
    //        type: "POST",
    //        success: function (data) {
    //            jsonData = JSON.parse(data.d);
    //            buildMenu($("#glbMenu"), jsonData);
    //        }
    //    })
    //})
</script>
<%-- End build Menu Desktop --%>

<%-- Build Menu Mobile --%>
<script>
    $(document).ready(function () {
        $.ajax({
            url: "/GUI/SystemService/WebService/MenuService.asmx/GetMenuMobile",
            datatype: "JSON",
            contentType: "application/json; charset:UTF-8",
            type: "POST",
            success: function (data) {
                var jsData = JSON.parse(data.d);
                var li = $("<li><div class='row' style='margin-bottom: 10px;'></div></li>");
                for (var j = 0; j < jsData.length; j++) {
                    var div_1 = '<div class="col-xs-6">';
                    div_1 += '<div class="box-menu parent-' + jsData[j].menuClass + '" onclick="menu_click($(this),\'' + String(jsData[j].menuClass) + '\')">';
                    div_1 += '<img src="' + jsData[j].menuIcon + '" class="sub-icon" />';
                    div_1 += '<p class="sub-text">' + jsData[j].menuName + '</p>';
                    div_1 += '<p style="display:none;">' + jsData[j].menuId + '</p>';
                    div_1 += '</div>';
                    div_1 += '</div>';
                    li.append(div_1);
                    //get child menu

                    var menuChild = $("<li class='menu-" + jsData[j].menuClass + "' style='display:none;'></li>");

                    //var back='<div class="row" style="margin-bottom: 10px;">';


                    //menuChild.append(back);
                    //back += '</div>';
                    var row = $("<div class='row' style='margin-bottom: 10px;'></div>");
                    var back = '<div class="col-xs-6">';
                    back += '<div class="box-menu back" onclick="menu_click($(this),\'' + String(jsData[j].menuClass) + '\')" style="margin-top: 10px;">';
                    back += '<img src="/Assets/images/icon_menu_mobile/back.png" class="sub-icon" />';
                    back += '<p class="sub-text">Quay lại</p>';
                    back += '</div>';
                    row.append(back);

                    for (var i = 0; i < jsData[j].lstMenu.length; i++) {
                        var child = '<div class="col-xs-6">';
                        child += '<div class="box-menu text-box-submenu ' + jsData[j].lstMenu[i].childClass + '">';
                        child += '<a href="' + jsData[j].lstMenu[i].childLink + '" class="sub-text">' + jsData[j].lstMenu[i].childName + '</a>';
                        child += '<p style="display:none;">' + jsData[j].lstMenu[i].childId + '</p>';
                        child += '</div>';
                        child += '</div>';
                        child += '</div>';
                        row.append(child);
                    }
                    $(".menu-childrent").append(menuChild.append(row));
                    //back += '</div>';

                }
                $(".global-menu-m").append(li);
            }
        })
    })
</script>
<%-- End Build Menu Mobile --%>
<script type="text/ecmascript">
    jQuery(document).ready(function () {
        // set active
        $("#glbMenu li").bind("click", function () {
            $("#glbMenu li.active").removeClass("active");
            $(this).addClass("active");
        });

        //show header login panel
        $("#Avatar").bind("click", function () {
            $("#HeaderUser").toggle();
        });
        $(window).bind("click", function (e) {
            if (e.target.className !== "avatar-img") {
                $("#HeaderUser").hide();
            }
        });
        window.onload = function () {
            var heightArray = [];
            //alert($(".box-menu").length);
            for (var i = 0; i < $(".box-menu").length; i++) {
                var height = $(".box-menu").eq(i).height();
                heightArray.push(height);
            }

            var maxHeight = Math.max.apply(null, heightArray);
            $(".box-menu").css("height", maxHeight);
        };

        //Xử lý phân quyền menu mobile tạm thời
        trickMenu();
    });

    function menu_click(This, MenuClass) {
        if (!$(This).hasClass("back")) {
            $(".global-menu-m").css("display", "none");
            $(".menu-childrent").css("display", "block");
            $(".menu-childrent").find("li[class^=\"menu-\"]").css("display", "none");
            if ($(This).hasClass("parent-" + MenuClass + "")) {
                $(".menu-" + MenuClass + "").css("display", "block");
            }
        }
        else {
            $(".global-menu-m").css("display", "block");
            $(".menu-childrent").css("display", "none");
        }
    }

    /*
    * Xử lý phân quyền menu mobile tạm thời
    */
    var adminNames = ["Xin chào Admin02", "Xin chào Admin", "Xin chào Root"];
    var listOff = [];
    function trickMenu() {
        var adminName = $(".hello-user").text();
        if (adminNames.indexOf(adminName) == -1) {
            $("[data-perm='admin']").hide();
        }
    }
</script>

<script>
    jQuery(document).ready(function () {
        var pushRight = new Menu({
            wrapper: '#o-wrapper',
            type: 'push-left',
            menuOpenerClass: '.c-button',
            maskId: '#c-mask'
        });

        var pushRightBtn = document.querySelector('#open-left-panel');

        pushRightBtn.addEventListener('click', function (e) {
            e.preventDefault;
            pushRight.open();
        });

        $(".li-brands").hover(function () {
            $('.menu-sub').css({ 'display': 'block' });
        });

        // click out of search box
        $('html').mousemove(function () {
            $(".menu-sub").hide();
        });
        $('.menu-sub,.li-brands').mousemove(function (event) {
            event.stopPropagation();
        });
        $("#open-left-panel").click(function () {
            $('#panel-menu-m').css('z-index', '1');
            $('.hotline-m').css('z-index', '0');
        });
    });

</script>
<script>
    function detectDevice() {
        var device = { tablet: false, mobile: false, desktop: false };
        if (navigator.userAgent.match(/Tablet|iPad/)) {
            device.tablet = true;
        } else if (navigator.userAgent.match(/Mobile/)) {
            device.mobile = true;
        } else {
            device.desktop = true;
        }
        return device;
    }
</script>

