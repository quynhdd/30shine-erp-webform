﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.UIControls
{
    public partial class NavPanel : System.Web.UI.UserControl
    {
        protected string Permission = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SetNavByPermission();
            }
        }

        private void SetNavByPermission()
        {
            Dictionary<string, List<string>> GlbMenu = new Dictionary<string, List<string>>()
            { 
                {
                    "root", new List<string>() {"navCustomer", "navService", "navAdminService", "navAdminProduct", "navAdminStaff", "navAdminAccount",
                                                    "navAdminTypeStaff", "navAdminSocialThread", "navAdminSalon", "navAdminReport", "navAdminCategory", "navBillSale", "navInventory", "navAdminNotification"
                                                }
                },
                {
                    "admin", new List<string>() {"navCustomer", "navService", "navAdminService", "navAdminProduct", "navAdminStaff", "navAdminAccount",
                                                    "navAdminTypeStaff", "navAdminSocialThread", "navAdminSalon", "navAdminReport", "navAdminCategory", "navBillSale", "navInventory", "navAdminNotification"
                                                }
                },                
                {
                    "mode1", new List<string>() {"navCustomer", "navService", "navAdminService", "navAdminProduct", "navAdminStaff", 
                                                    "navAdminTypeStaff", "navAdminSocialThread", "navAdminSalon", "navAdminReport"
                                                }
                },
                {
                    "accountant", new List<string>() { "navAdminReport" }
                },
                {
                    "salonmanager", new List<string>() {"navCustomer", "navService", "navAdminStaff", "navAdminReport"}
                },
                {
                    "reception", new List<string>() {"navCustomer", "navService", /*"navProfile",*/ "navAdminReport"}
                },
                {
                    "checkin", new List<string>() {"navCustomer", "navService", /*"navProfile",*/ "navAdminReport"}
                },
                {
                    "checkout", new List<string>() {"navCustomer", "navService", /*"navProfile",*/ "navAdminReport", "navInventory" }
                },
                {
                    "staff", new List<string>() {"navProfile"}
                },
                {
                    "servey", new List<string>() {"navServey"}
                },
                /// Admin store online
                {
                    "onlinestore", new List<string>() {
                        "navCustomer",
                        "navBillSale" // Quản lý đơn hàng
                    }
                },
                {
                    "employer_step1", new List<string>()
                    {
                        // Tuyển dụng
                    }
                },

                {
                    "employer_step2", new List<string>()
                    {
                        // Tuyển dụng
                    }
                },

                {
                    "inventory_manager", new List<string>()
                    {
                        // Quản lý kho
                    }
                }
            };

            Permission = Session["User_Permission"].ToString();
            var MenuShow = GlbMenu[Permission];
            if (MenuShow.Count > 0)
            {
                foreach (var v in MenuShow)
                {
                    FindControl(v).Visible = true;
                }
            }
        }
    }
}