﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Footer.ascx.cs" Inherits="_30shine.GUI.Controls.Footer" %>
<style>
    .footer { padding: 30px 0; background: #f5f5f5; border-top: 1px solid #ddd; position: relative; z-index: 100; }
</style>
<div class="wp footer">
    <div class="container">
        <div class="row">
            <p><i class="fa fa-copyright"></i> 30Shine. All Rights Reserved.</p>
        </div>
    </div>
</div>