﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Header_V2.ascx.cs" Inherits="_30shine.GUI.UIControls.Header_V2" %>

<link href="/Assets/css/menu_mobile.css?12379349" rel="stylesheet" />
<script src="/Assets/js/Menu_mobile.js"></script>
<style>
    ul#glbMenu li ul.ul-sub-menu li:hover>ul.ul-sub-menu,ul#glbMenu li ul.ul-sub-menu li ul.ul-sub-menu li:hover>ul.ul-sub-menu{
        display:block !important;
    }
    ul#glbMenu li ul.ul-sub-menu li ul.ul-sub-menu,ul#glbMenu li ul.ul-sub-menu li ul.ul-sub-menu li ul.ul-sub-menu{
            position: absolute;
             top: -1px; 
             left: 100%; 
             margin: 0 auto; 
             text-align: left; 
             white-space: nowrap; 
             width: auto; 
             border: 1px solid #dfdfdf; 
             display:none !important;

    }
    .box-menu {
    box-shadow: 2px 2px 2px 2px #b8b8b8;
    min-height: 100px;
    max-height: 200px;
    min-width: 65px;
    height: 150px;
}
    .text-box-submenu{
        min-height:100px;
        text-align:center;
    }
    @media (min-width: 320px) {

        .c-menu--slide-left, .c-menu--slide-right, .c-menu--push-left, .c-menu--push-right {
            width: 50%;
            /* min-width: 350px; */
        }
    }
</style>
<div class="wp global-menu " style="margin-top: 0px;" id="o-wrapper">
    <div class="container" style="background: #000000;">
        <!-- Menu desktop -->
        <div class="row">
            <strong class="text-head">Phần mềm quản lý hair salon</strong>
        </div>
        <div class="row menu-desktop">
            <ul class="ul-global-menu" id="glbMenu" runat="server" clientidmode="Static">
                <li>
                    <img src="/Assets/images/ic_logo_white.png" style="margin-right: 10px; margin-top: -5px; width: 75px;" />
                </li>
               

                <li style="float: right; position: absolute; top: 0; right: 15px;"
                    id="login_detail">
                    <a href="javascript://"><i class="fa fa-envelope" style="color: #ffffff;"></i></a>
                    <a href="javascript://"><i class="fa fa-bell" style="color: #ffffff;"></i></a>
                    <p class="hello-user" style="color: #ffffff; display: inline-block; margin-top: 10px; margin-right: 15px;">Xin chào <%=_User %></p>
                    <div class="avatar-wrap" style="display: inline-block; position: relative;">
                        <a href="javascript://" class="avatar" id="Avatar">
                            <img class="avatar-img" alt="" title="" src="/Assets/images/avatar.man.white.png" style="width: 20px; height: 21px; position: relative; top: -2px;" />
                        </a>
                        <div class="header-user-wp" id="HeaderUser" style="position: absolute; z-index: 2000; display: none; background: #5d5d5d; margin-left: -65px; right: 7px; margin-right: -20px; margin-top: 5px; padding: 5px 10px;">
                            <ul class="user-login-panel">
                                <li><a href="javascript://">Sửa thông tin</a></li>
                                <li><a href="/dang-xuat.html">Thoát</a></li>
                            </ul>
                            <div class="triangle"></div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

        <!-- Menu desktop -->

        <!-- Menu mobile -->
        <div class="row menu-mobile" style="position: relative; display: none;">
            <a href="/" class="logo-m">
                <img src="/Assets/images/ic_logo_white.png" style="margin: 10px 10px 10px 0; width: 75px;" />
            </a>
            <div class="toogle-menu-m" id="open-left-panel" style="display: inline-block;">
                <img src="/Assets/images/toggle.m.png" style="margin-top: 10px;">
            </div>

        </div>
        <div id="panel-menu-m">
            <nav id="c-menu--push-left" class="c-menu c-menu--push-left" style="display: none; overflow: scroll;">
                <ul class="global-menu-m" style="padding-bottom: 10px;">
                    <li>
                        <div class="row">
                            <div class="col-xs-6">
                                <img src="/Assets/images/logo/logo.jpg" style="float: left; width: 100px;" />
                            </div>
                            <div class="col-xs-6">
                                <button class="c-menu__close">
                                    <img src="/Assets/images/close-icon-menu.png" style="position: relative; top: 30px; width: 34px; height: 30px;" />
                                </button>
                            </div>
                        </div>
                    </li> 
                   


                </ul>
                <ul class ="menu-childrent"  style="display:none;">
                    <li class="header-menu">
                        <div class="row">
                            <div class="col-xs-6">
                                <img src="/Assets/images/logo/logo.jpg" style="float: left; width: 100px;" />
                            </div>
                            <div class="col-xs-6">
                                <button class="c-menu__close">
                                    <img src="/Assets/images/close-icon-menu.png" style="position: relative; top: 30px; width: 34px; height: 30px;" />
                                </button>
                            </div>
                        </div>
                    </li>
                    
                </ul>
            </nav>
            <!-- /c-menu push-left -->
        </div>

        <div id="c-mask" class="c-mask"></div>
        <!-- Menu mobile -->

    </div>
</div>
<%--Build Menu Desktop--%>
<script>
    function buildMenu(parent, items) {
        $.each(items, function (u,v) {
            var li = $("<li><a href=" + v.Url_Rewrite + ">" + v.mName + "</a></li>");
            li.appendTo(parent);
            if (v.lstMenu.length>0) {
                var ul = $("<ul class='ul-sub-menu'></ul>");
                ul.appendTo(li);
                buildMenu(ul, v.lstMenu);
            }
        })
    }
    $(document).ready(function () {
        $.ajax({
            url: "/GUI/SystemService/WebService/MenuService.asmx/ListMenu",
            datatype: "JSON",
            contentType: "application/json; charset:UTF-8",
            type: "POST",
            success: function (data) {
                jsonData = JSON.parse(data.d);
                buildMenu($("#glbMenu"), jsonData);
            }
        })
    })
</script>
<%-- End build Menu Desktop --%>

<%-- Build Menu Mobile --%>
<script>
    $(document).ready(function () {
        $.ajax({
            url: "/GUI/SystemService/WebService/MenuService.asmx/getMenu_Mobile",
            datatype: "JSON",
            contentType: "application/json; charset:UTF-8",
            type: "POST",
            success: function (data) {
                var jsData = JSON.parse(data.d);
                var li = $("<li><div class='row' style='margin-bottom: 10px;'></div></li>");
                for (var j = 0; j <= jsData.length-1; j ++) {
                    //console.log(jsData[j]);
                   var div_1 ='<div class="col-xs-6">';
                   div_1 += '<div class="box-menu parent-' + jsData[j].cssTag + '" onclick="menu_click($(this))">';
                   div_1 += '<img src="' + jsData[j].mIcon + '" class="sub-icon" />';
                   div_1 += '<p class="sub-text">' + jsData[j].mName + '</p>';
                   div_1 += '</div>';
                   div_1 += '</div>';
                   li.append(div_1);
                    //get child menu

                   var menuChild = $("<li class='menu-" + jsData[j].cssTag + "' style='display:none;'></li>");
                   
                    //var back='<div class="row" style="margin-bottom: 10px;">';
                    
                   
                   //menuChild.append(back);
                    //back += '</div>';
                   var row = $("<div class='row' style='margin-bottom: 10px;'></div>");
                   var back = '<div class="col-xs-6">';
                   back += '<div class="box-menu back" onclick="menu_click($(this))" style="margin-top: 10px;">';
                   back += '<img src="/Assets/images/icon_menu_mobile/back.png" class="sub-icon" />';
                   back += '<p class="sub-text">Quay lại</p>';
                   back += '</div>';
                   row.append(back);
                  
                    for (var i = 0; i < jsData[j].lstMenu.length - 1; i++) {
                        //console.log(jsData[j].lstMenu[i]);
                        
                        
                        var child = '<div class="col-xs-6">';
                        child += '<div class="box-menu text-box-submenu ' + jsData[j].lstMenu[i].cssTag + '">';
                        child += '<a href="' + jsData[j].lstMenu[i].Url_Rewrite + '" class="sub-text">' + jsData[j].lstMenu[i].mName + '</a>';
                        child += '</div>';
                        child += '</div>';
                        child += '</div>';
                        row.append(child);
                    }
                    $(".menu-childrent").append(menuChild.append(row));
                    //back += '</div>';
                    
                }
                $(".global-menu-m").append(li);
            }
        })
    })
</script>
<%-- End Build Menu Mobile --%>
<script type="text/ecmascript">
    jQuery(document).ready(function () {
        // set active
        $("#glbMenu li").bind("click", function () {
            $("#glbMenu li.active").removeClass("active");
            $(this).addClass("active");
        });

        //show header login panel
        $("#Avatar").bind("click", function () {
            $("#HeaderUser").toggle();
        });
        $(window).bind("click", function (e) {
            if (e.target.className !== "avatar-img") {
                $("#HeaderUser").hide();
            }
        });
        window.onload = function () {
            var heightArray = [];
            //alert($(".box-menu").length);
            for (var i = 0; i < $(".box-menu").length; i++) {
                var height = $(".box-menu").eq(i).height();
                heightArray.push(height);
            }
            
            var maxHeight = Math.max.apply(null, heightArray);
            $(".box-menu").css("height", maxHeight);
        };

        //Xử lý phân quyền menu mobile tạm thời
        trickMenu();
    });

    function menu_click(This) {
        if (!$(This).hasClass("back")) {
            $(".global-menu-m").css("display", "none");
            $(".menu-childrent").css("display", "block");
            $(".menu-childrent").find("li[class^=\"menu-\"]").css("display", "none");
            if ($(This).hasClass("parent-customer")) {
                $(".menu-customer").css("display", "block");
            }
            else if ($(This).hasClass("parent-booking")) {
                $(".menu-booking").css("display", "block");
            }
            else if ($(This).hasClass("parent-bill")) {
                $(".menu-bill").css("display", "block");
            }
            else if ($(This).hasClass("parent-report")) {
                $(".menu-report").css("display", "block");
            }
            else if ($(This).hasClass("parent-KQKD")) {
                $(".menu-childrent").find("li[class^=\"menu-\"]").css("display", "none");
                $(".menu-KQKD").css("display", "block");
            }
            else if ($(This).hasClass("back-report")) {
                $(".menu-report").css("display", "block");
                $(".menu-KQKD").css("display", "none");
            }
            else if ($(This).hasClass("parent-time")) {
                $(".menu-time").css("display", "block");
            }
            else if ($(This).hasClass("parent-quanlity")) {
                $(".menu-quanlity").css("display", "block");
            }
            else if ($(This).hasClass("parent-recruitment")) {
                $(".menu-recruitment").css("display", "block");
            }
            else if ($(This).hasClass("parent-roles")) {
                $(".menu-roles").css("display", "block");
            }
            else if ($(This).hasClass("parent-s4m")) {
                $(".menu-s4m").css("display", "block");
            }
            else if ($(This).hasClass("parent-product")) {
                $(".menu-product").css("display", "block");
            }
            else if ($(This).hasClass("parent-XuatVT")) {
                $(".menu-childrent").find("li[class^=\"menu-\"]").css("display", "none");
                $(".menu-XuatVT").css("display", "block");
            }
            else if ($(This).hasClass("back-MP")) {
                $(".menu-product").css("display", "block");
                $(".menu-XuatVT").css("display", "none");
                $(".menu-HH").css("display", "none");
            }

            else if ($(This).hasClass("parent-HH")) {
                $(".menu-childrent").find("li[class^=\"menu-\"]").css("display", "none");
                $(".menu-HH").css("display", "block");
            }
            else if ($(This).hasClass("parent-orderSalon")) {
                $(".menu-childrent").find("li[class^=\"menu-\"]").css("display", "none");
                $(".menu-orderSalon").css("display", "block");
            }
            else if ($(This).hasClass("back-SL")) {
                $(".menu-HH").css("display", "block");
                $(".menu-orderSalon").css("display", "none");
                $(".menu-khotra").css("display", "none");
            }
            else if ($(This).hasClass("parent-khotra")) {
                $(".menu-childrent").find("li[class^=\"menu-\"]").css("display", "none");
                $(".menu-khotra").css("display", "block");
            }

            else if ($(This).hasClass("parent-marketing")) {
                $(".menu-marketing").css("display", "block");
            }
            else if ($(This).hasClass("parent-CH")) {
                $(".menu-childrent").find("li[class^=\"menu-\"]").css("display", "none");
                $(".menu-CH").css("display", "block");
            }
            else if ($(This).hasClass("parent-CP")) {
                $(".menu-childrent").find("li[class^=\"menu-\"]").css("display", "none");
                $(".menu-CP").css("display", "block");
            }
            else if ($(This).hasClass("parent-BC")) {
                $(".menu-childrent").find("li[class^=\"menu-\"]").css("display", "none");
                $(".menu-BC").css("display", "block");
            }
            else if ($(This).hasClass("back-MKT")) {
                $(".menu-marketing").css("display", "block");
                $(".menu-CH").css("display", "none");
                $(".menu-CP").css("display", "none");
                $(".menu-BC").css("display", "none");
            }

            else if ($(This).hasClass("parent-setting")) {
                $(".menu-setting").css("display", "block");
            }
            else if ($(This).hasClass("parent-QLC")) {
                $(".menu-childrent").find("li[class^=\"menu-\"]").css("display", "none");
                $(".menu-QLC").css("display", "block");
            }
            else if ($(This).hasClass("parent-DV")) {
                $(".menu-childrent").find("li[class^=\"menu-\"]").css("display", "none");
                $(".menu-DV").css("display", "block");
            }
            else if ($(This).hasClass("parent-NS")) {
                $(".menu-childrent").find("li[class^=\"menu-\"]").css("display", "none");
                $(".menu-NS").css("display", "block");
            }
            else if ($(This).hasClass("parent-app")) {
                $(".menu-childrent").find("li[class^=\"menu-\"]").css("display", "none");
                $(".menu-app").css("display", "block");
            }
            else if ($(This).hasClass("parent-money")) {
                $(".menu-childrent").find("li[class^=\"menu-\"]").css("display", "none");
                $(".menu-money").css("display", "block");
            }
            else if ($(This).hasClass("back-set")) {
                $(".menu-setting").css("display", "block");
                $(".menu-QLC").css("display", "none");
                $(".menu-DV").css("display", "none");
                $(".menu-NS").css("display", "none");
                $(".menu-app").css("display", "none");
                $(".menu-money").css("display", "none");
            }
        }
        else {
            $(".global-menu-m").css("display", "block");
            $(".menu-childrent").css("display", "none");
        }
    }

    /*
    * Xử lý phân quyền menu mobile tạm thời
    */
    var adminNames = ["Xin chào Admin02", "Xin chào Admin", "Xin chào Root"];
    var listOff = [];
    function trickMenu()
    {
        var adminName = $(".hello-user").text();
        if (adminNames.indexOf(adminName) == -1)
        {
            $("[data-perm='admin']").hide();
        }
    }
</script>

<script>
    jQuery(document).ready(function () {
        var pushRight = new Menu({
            wrapper: '#o-wrapper',
            type: 'push-left',
            menuOpenerClass: '.c-button',
            maskId: '#c-mask'
        });

        var pushRightBtn = document.querySelector('#open-left-panel');

        pushRightBtn.addEventListener('click', function (e) {
            e.preventDefault;
            pushRight.open();
        });

        $(".li-brands").hover(function () {
            $('.menu-sub').css({ 'display': 'block' });
        });

        // click out of search box
        $('html').mousemove(function () {
            $(".menu-sub").hide();
        });
        $('.menu-sub,.li-brands').mousemove(function (event) {
            event.stopPropagation();
        });
        $("#open-left-panel").click(function () {
            $('#panel-menu-m').css('z-index', '1');
            $('.hotline-m').css('z-index', '0');
        });
    });
   
</script>
<script>
    function detectDevice() {
        var device = { tablet: false, mobile: false, desktop: false };
        if (navigator.userAgent.match(/Tablet|iPad/)) {
            device.tablet = true;
        } else if (navigator.userAgent.match(/Mobile/)) {
            device.mobile = true;
        } else {
            device.desktop = true;
        }
        return device;
    }
</script>

