﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;
using System.Linq.Expressions;
using LinqKit;
using System.Globalization;
using System.Data.Entity;
using _30shine.Helpers;
using Newtonsoft.Json;

namespace _30shine.GUI.SystemService.Ajax
{
    public partial class ReportService : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        //CÁC HÀM BÁO CÁO VIẾT VỚI NGHIỆP VỤ TRƯỚC THÁNG 3/2016, HIỆN TẠI KHÔNG CÒN DÙNG
        //
        /*[WebMethod]
        public static string ReportBill(int salonId, string from, string to, string viewTime)
        {
            DataBill data = new DataBill();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            CultureInfo culture = new CultureInfo("vi-VN");
            //DateTime _FromDate = Convert.ToDateTime(from, culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
            DateTime _FromDate = Convert.ToDateTime(from, culture);
            DateTime _ToDate = _FromDate;
            if (to != "")
            {
                _ToDate = Convert.ToDateTime(to, culture).AddDays(1);
            }
            var timeFrom = String.Format("{0:MM/dd/yyyy}", _FromDate);
            var timeTo = String.Format("{0:MM/dd/yyyy}", _ToDate);

            if (timeFrom != "" && timeTo != "")
            {
                List<DataCountByDate> billServiceTotal = new List<DataCountByDate>();
                //List<DataCountByDate> billServiceCustomerOld = new List<DataCountByDate>();
                //List<DataCountByDate> billServiceCustomerNew = new List<DataCountByDate>();
                List<DataCountByDate> billCosmeticTotal = new List<DataCountByDate>();
                List<DataCountByDate> billCosmeticOnline = new List<DataCountByDate>();
                List<DataCountByDate> billCosmeticSalon = new List<DataCountByDate>();



                billServiceTotal = GetBillN(salonId, "BillServiceTotal", timeFrom, timeTo, viewTime);
                //if (viewTime == "day")
                //{
                //    billServiceCustomerOld = GetBillServiceCustomerOld(salonId, timeFrom, timeTo, billServiceTotal, viewTime);
                //    billServiceCustomerNew = GetBillServiceCustomerNew(salonId, timeFrom, timeTo, billServiceTotal, billServiceCustomerOld, viewTime);
                //}
                //else
                //{
                //    billServiceCustomerOld = GetBillServiceCustomerN(salonId, "old", timeFrom, timeTo, viewTime);
                //    billServiceCustomerNew = GetBillServiceCustomerN(salonId, "new", timeFrom, timeTo, viewTime);
                //}

                billCosmeticOnline = GetBillN(salonId, "BillCosmeticOnline", timeFrom, timeTo, viewTime);
                billCosmeticSalon = GetBillN(salonId, "BillCosmeticSalon", timeFrom, timeTo, viewTime);
                billCosmeticTotal = GetBillN(salonId, "BillCosmeticTotal", timeFrom, timeTo, viewTime);

                using (var db = new Solution_30shineEntities())
                {
                    var salon = "";
                    var salonName = db.Tbl_Salon.Where(w => w.Id == salonId).Select(s => s.Name).FirstOrDefault();
                    if (salonName != null)
                        salon = "tại salon " + salonName;
                    else salon = "tại các Salon";

                    data.Salon = salon;
                    data.Unit = "Hóa đơn";
                    data.Title = "Thống kê hóa đơn " + salon + " (đơn vị " + data.Unit + ")";

                    data.BillServiceTotal = serializer.Serialize(billServiceTotal);
                    //data.BillServiceCustomerOld = serializer.Serialize(billServiceCustomerOld);
                    //data.BillServiceCustomerNew = serializer.Serialize(billServiceCustomerNew);
                    data.BillCosmeticTotal = serializer.Serialize(billCosmeticTotal);
                    data.BillCosmeticSalon = serializer.Serialize(billCosmeticSalon);
                    data.BillCosmeticOnline = serializer.Serialize(billCosmeticOnline);

                    data.Number = billServiceTotal.Count;
                }
            }
            return serializer.Serialize(data);
        }


        [WebMethod]
        public static string ReportCustomer(int salonId, string from, string to, string viewTime)
        {
            DataBill data = new DataBill();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            CultureInfo culture = new CultureInfo("vi-VN");
            //DateTime _FromDate = Convert.ToDateTime(from, culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
            DateTime _FromDate = Convert.ToDateTime(from, culture);
            DateTime _ToDate = _FromDate;
            if (to != "")
            {
                _ToDate = Convert.ToDateTime(to, culture).AddDays(1);
            }
            var timeFrom = String.Format("{0:MM/dd/yyyy}", _FromDate);
            var timeTo = String.Format("{0:MM/dd/yyyy}", _ToDate);

            if (timeFrom != "" && timeTo != "")
            {
                List<DataCountByDate> billServiceTotal = new List<DataCountByDate>();
                List<DataCountByDate> billServiceCustomerOld = new List<DataCountByDate>();
                List<DataCountByDate> billServiceCustomerNew = new List<DataCountByDate>();



                billServiceTotal = GetBillN(salonId, "BillServiceTotal", timeFrom, timeTo, viewTime);
                if (viewTime == "day")
                {
                    billServiceCustomerOld = GetBillServiceCustomerOld(salonId, timeFrom, timeTo, billServiceTotal, viewTime);
                    billServiceCustomerNew = GetBillServiceCustomerNew(salonId, timeFrom, timeTo, billServiceTotal, billServiceCustomerOld, viewTime);
                }
                else
                {
                    billServiceCustomerOld = GetBillServiceCustomerN(salonId, "old", timeFrom, timeTo, viewTime);
                    billServiceCustomerNew = GetBillServiceCustomerN(salonId, "new", timeFrom, timeTo, viewTime);
                }

                using (var db = new Solution_30shineEntities())
                {
                    var salon = "";
                    var salonName = db.Tbl_Salon.Where(w => w.Id == salonId).Select(s => s.Name).FirstOrDefault();
                    if (salonName != null)
                        salon = "tại salon " + salonName;
                    else salon = "tại các Salon";

                    data.Salon = salon;
                    data.Unit = "Khách hàng";
                    data.Title = "Thống kê khách hàng cũ và khách hàng mới " + salon + " (đơn vị " + data.Unit + ")";

                    data.BillServiceTotal = serializer.Serialize(billServiceTotal);
                    data.BillServiceCustomerOld = serializer.Serialize(billServiceCustomerOld);
                    data.BillServiceCustomerNew = serializer.Serialize(billServiceCustomerNew);


                    data.Number = billServiceTotal.Count;
                }
            }
            return serializer.Serialize(data);
        }



        [WebMethod]
        public static string ReportCustomerFamiliar(int salonId, string from, string to, string viewTime, string staffType, string topIds)
        {
            using (var db = new Solution_30shineEntities())
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                CultureInfo culture = new CultureInfo("vi-VN");
                DateTime _FromDate = Convert.ToDateTime(from, culture);
                DateTime _ToDate = _FromDate;
                if (to != "")
                {
                    _ToDate = Convert.ToDateTime(to, culture).AddDays(1);
                }
                int totalDay = (_ToDate - _FromDate).Days;
                var timeFrom = String.Format("{0:MM/dd/yyyy}", _FromDate);
                var timeTo = String.Format("{0:MM/dd/yyyy}", _ToDate);
                // var andCreatedTime = "(CreatedDate between '" + timeFrom + "' and '" + timeTo + "')";

                var andSalonId = "";
                if (salonId == 0)
                    andSalonId = "SalonId>0";
                else
                {
                    andSalonId = "SalonId='" + salonId + "'";
                }
                var selectTime = "";
                var groupBy = "";
                var orderBy = "";

                switch (viewTime)
                {
                    case "day":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month],m.[Week],m.[Day]";
                        break;
                    case "week":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month],m.[Week]";

                        break;
                    case "month":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month]";

                        break;
                    case "quarter":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter]";

                        break;
                    case "year":
                        selectTime = groupBy = orderBy = " m.[Year]";
                        break;
                }


                //var type = 1;
                var listCustomerFamiliar = new List<CustomerOfStaff>();
                string[] staffIds = topIds.Split(',');

                var staff = "";
                var staffName = "";
                if (staffType == "1" || staffType == "")
                {
                    staff = "Staff_Hairdresser_Id";
                    staffName = "Stylist";
                }
                else if (staffType == "2")
                {
                    staff = "Staff_HairMassage_Id";
                    staffName = "Skinner";
                }
                var listStaff = new List<int>();
                for (int i = 0; i < staffIds.Length; ++i)
                {
                    var id = Convert.ToInt32(staffIds[i]);
                    listStaff.Add(id);
                }
                //var listCustomerOfStaff = GetCustomerOfStaff(salonId, from, to, viewTime, staffType, listStaff);


                var listCustomerCode = new List<DataCountByDate>();
                foreach (string staffId in staffIds)
                {
                    var customerOfStaff = new CustomerOfStaff();

                    var sqlCustomerOfStaff = @"declare @dtFrom datetime, @dtTo datetime
                                        set @dtFrom = '" + timeFrom + "'" +
                                      " set @dtTo = '" + timeTo + "'" +
                                      @" select
                                        DATEPART(YEAR, [DateTime]) as [Year]
                                        ,DATEPART(QUARTER, [DateTime]) as [Quarter]
                                        ,DATEPART(MONTH, [DateTime]) as[Month]
                                        ,DATEPART(WEEK, [DateTime]) as[Week]
                                        ,DATEPART(DAY, [DateTime]) as [Day]
                                        from(
                                        select
                                            dateadd(day, number, @dtFrom) as [DateTime]
                                        from
                                            (select distinct number from master.dbo.spt_values
                                             where name is null
                                            ) as n
                                        where dateadd(day, number, @dtFrom) < @dtTo
                                        )a";

                    var listCustomerOfStaff = db.Database.SqlQuery<DataCountByDate>(sqlCustomerOfStaff).ToList();

                    var id = Convert.ToInt32(staffId);
                    var sql0 = @"select 
                             CustomerCode as [Code]
                                ,DATEPART(YEAR, CreatedDate) AS [Year]
                                    ,DATEPART(QUARTER, CreatedDate) as [Quarter] 
                                    ,DATEPART(MONTH, CreatedDate) AS[Month] 
                                    ,DATEPART(WEEK, CreatedDate) AS[Week] 
                                    ,DATEPART(DAY, CreatedDate) as [Day]
                                    from 
                                    BillService where 
                                    (CreatedDate between @dtFrom and @dtTo) 
                                    and IsDelete !=1 and ServiceIds is not null and ServiceIds!='' and Pending!=1
                                    and " + andSalonId +
                                    @"and Staff_Hairdresser_Id=@staffId and CustomerCode!='' and CustomerCode is not null 
                                group by 
                                    DATEPART(YEAR, CreatedDate) 
                                    ,DATEPART(QUARTER, CreatedDate)
                                 ,DATEPART(MONTH, CreatedDate)
                                 ,DATEPART(WEEK, CreatedDate) 
                                 ,DATEPART(DAY, CreatedDate)	
                                 ,CustomerCode";


                    var sqlListCustomerCode = @"declare @dtFrom datetime, @dtTo datetime, @staffId int
                            set @dtFrom = '" + timeFrom + "'" +
                       " set @dtTo = '" + timeTo + "'" +
                       " set @staffId='" + id + "'" +
                       " select Coalesce(m.[Code],'0') as [Code], " + selectTime +
                       @" from 
                            (
                            select a1.*, b.[Code] as [Code]
                            from
                            (
                            select 
                             DATEPART(YEAR, [DateTime]) as [Year] 
                             ,DATEPART(QUARTER, [DateTime]) as [Quarter] 
                             ,DATEPART(MONTH, [DateTime]) as[Month]
                             ,DATEPART(WEEK, [DateTime]) as[Week] 
                             ,DATEPART(DAY, [DateTime]) as [Day] 
                            from 
                            (
                             select 
                              dateadd(day, number, @dtFrom) as [DateTime]
                             from 
                              (select distinct number from master.dbo.spt_values
                               where name is null
                              ) as n
                             where dateadd(day, number, @dtFrom) < @dtTo 
                            ) as a
                            ) as a1
                            left join 
                            (" + sql0 +
                       @") as b 
                            on a1.[Year]=b.[Year] and a1.[Quarter] = b.[Quarter] and a1.[Month] = b.[Month] and a1.[Week] = b.[Week] and a1.[Day] = b.[Day]
                            )m
                            group by " + groupBy +
                       " ,m.[Code] order by " +
                       orderBy;

                    listCustomerCode = db.Database.SqlQuery<DataCountByDate>(sqlListCustomerCode).ToList();

                    customerOfStaff.Id = id;
                    customerOfStaff.Name = db.Staffs.Where(w => w.Id == id).Select(s => s.Fullname).FirstOrDefault().ToString();
                    customerOfStaff.CountByDate = serializer.Serialize(ReLabelDate(listCustomerOfStaff, salonId, timeFrom, timeTo, viewTime));
                    //var dayOff = 0;
                    // var list = new List<DataCountByDate>();
                    var listCountByDate = new List<DataCountByDate>();
                    for (int i = 0; i < listCustomerOfStaff.Count; ++i)
                    {
                        var countCustomerFamiliar = 0;
                        var year = listCustomerOfStaff[i].Year;
                        var month = listCustomerOfStaff[i].Month;
                        var day = listCustomerOfStaff[i].Day;
                        for (int j = 0; j < listCustomerCode.Count; ++j)
                        {
                            var obj = listCustomerCode[j];

                            if (year == obj.Year && month == obj.Month && day == obj.Day)
                            {
                                var timeLine = obj.Year + "/" + obj.Month + "/" + obj.Day;
                                var sqlCountCustomerOld = @"select 
                                                 Count(CustomerCode) as [Count]
                                                    ,DATEPART(YEAR, CreatedDate) AS [Year]
                                                     ,DATEPART(QUARTER, CreatedDate) as [Quarter] 
                                                     ,DATEPART(MONTH, CreatedDate) AS[Month] 
                                                     ,DATEPART(WEEK, CreatedDate) AS[Week] 
                                                     ,DATEPART(DAY, CreatedDate) as [Day]
                                                     from 
                                                     BillService where 
                                                     (CreatedDate < '" + timeLine +
                                                             @"') and IsDelete !=1 and ServiceIds is not null and ServiceIds!='' and Pending!=1
                                                     and " + andSalonId +
                                                             @" and Staff_Hairdresser_Id=" + id +
                                                     @" and CustomerCode='" + obj.Code +
                                                            @"' group by     
                                                     DATEPART(YEAR, CreatedDate) 
                                                     ,DATEPART(QUARTER, CreatedDate)
                                                  ,DATEPART(MONTH, CreatedDate)
                                                  ,DATEPART(WEEK, CreatedDate) 
                                                  ,DATEPART(DAY, CreatedDate)	
                                                 ,CustomerCode";

                                var listCountCustomer = db.Database.SqlQuery<DataCountByDate>(sqlCountCustomerOld).ToList();
                                var count = listCountCustomer.Count;

                                if (count > 0)
                                {
                                    // check check customer cut by stylist in time range between two cut
                                    var thisDay = timeLine;
                                    var countSequence = CountSequence(listCountCustomer, id, obj.Code, salonId, thisDay);
                                    if (countSequence == 0)
                                        countCustomerFamiliar++;

                                }
                            }
                        }
                        var countByDate = new DataCountByDate();
                        countByDate.Count = countCustomerFamiliar;
                        countByDate.Year = year;
                        countByDate.Month = month;
                        countByDate.Day = day;
                        listCountByDate.Add(countByDate);
                        //customerOfStaff.CustomerFamiliar = serializer.Serialize(countByDate);
                    }
                    customerOfStaff.CountByDate = serializer.Serialize(ReLabelDate(listCountByDate, salonId, timeFrom, timeTo, viewTime));
                    listCustomerFamiliar.Add(customerOfStaff);
                }


                var data = new DataTotal();

                data.CustomerOfStaff = serializer.Serialize(listCustomerFamiliar);
                var salon = "";
                var salonName = db.Tbl_Salon.Where(w => w.Id == salonId).Select(s => s.Name).FirstOrDefault();
                if (salonName != null)
                    salon = "tại salon " + salonName;
                else salon = "tại các Salon";
                var timeView = "";
                if (viewTime == "day")
                    timeView = "ngày";
                else if (viewTime == "week")
                    timeView = "tuần";
                else if (viewTime == "quarter")
                    timeView = "quý";
                else if (viewTime == "year")
                    timeView = "năm";
                else if (viewTime == "month")
                    timeView = "tháng";



                data.Unit = "khách";
                data.Title = "Thống kê khách quen của " + staffName + " theo " + timeView + " " + salon + " (đơn vị " + data.Unit + ")";
                data.Number = totalDay;
                return serializer.Serialize(data);
            }
        }








        private static int CountSequence(List<DataCountByDate> list, int staffId, string CustomerCode, int salonId, string thisDay)
        {
            using (var db = new Solution_30shineEntities())
            {
                var count = 0;
                var andSalonId = "";
                if (salonId == 0)
                    andSalonId = "SalonId>0";
                else
                {
                    andSalonId = "SalonId='" + salonId + "'";
                }
                //get time range= prev->to this
                //reverse list
                list.Reverse();

                for (int i = 0; i < list.Count; ++i)
                {
                    if (i < list.Count - 1)
                    {
                        var obj2 = list[i];
                        var obj1 = list[i + 1];
                        var timeFrom = "";
                        var timeTo = "";
                        var sql = "";

                        if (i == 0)
                        {
                            timeFrom = list[0].Year + "/" + list[0].Month + "/" + list[0].Day;
                            timeTo = thisDay;

                            sql = @"select 
                                    Staff_Hairdresser_Id as [Id]
                                    ,DATEPART(YEAR, CreatedDate) AS [Year]
                                        ,DATEPART(QUARTER, CreatedDate) as [Quarter] 
                                        ,DATEPART(MONTH, CreatedDate) AS[Month] 
                                        ,DATEPART(WEEK, CreatedDate) AS[Week] 
                                        ,DATEPART(DAY, CreatedDate) as [Day]
                                        from 
                                        BillService where 
                                        (CreatedDate between '" + timeFrom + "' and '" + timeTo + @"') and IsDelete !=1 and ServiceIds is not null and ServiceIds!='' and Pending!=1
                                        and " + andSalonId + " and Staff_Hairdresser_Id!= '" + staffId +
                                        @"' and CustomerCode='" + CustomerCode + "'" +
                                        @" order by     
                                        DATEPART(YEAR, CreatedDate) 
                                        ,DATEPART(QUARTER, CreatedDate)
                                        ,DATEPART(MONTH, CreatedDate)
                                        ,DATEPART(WEEK, CreatedDate) 
                                        ,DATEPART(DAY, CreatedDate)";

                            var listCustomerOfOtherStylist0 = db.Database.SqlQuery<DataCountByDate>(sql).ToList();
                            count += listCustomerOfOtherStylist0.Count;
                        }

                        obj2 = list[i];
                        obj1 = list[i + 1];

                        timeFrom = obj1.Year + "/" + obj1.Month + "/" + obj1.Day;
                        timeTo = obj2.Year + "/" + obj2.Month + "/" + obj2.Day;

                        sql = @"select 
                        Staff_Hairdresser_Id as [Id]
                        ,DATEPART(YEAR, CreatedDate) AS [Year]
                         ,DATEPART(QUARTER, CreatedDate) as [Quarter] 
                         ,DATEPART(MONTH, CreatedDate) AS[Month] 
                         ,DATEPART(WEEK, CreatedDate) AS[Week] 
                         ,DATEPART(DAY, CreatedDate) as [Day]
                         from 
                         BillService where 
                         (CreatedDate between '" + timeFrom + "' and '" + timeTo + @"') and IsDelete !=1 and ServiceIds is not null and ServiceIds!='' and Pending!=1
                         and " + andSalonId + " and Staff_Hairdresser_Id!= '" + staffId +
                            @"' and CustomerCode='" + CustomerCode + "'" +
                            @" order by     
                         DATEPART(YEAR, CreatedDate) 
                         ,DATEPART(QUARTER, CreatedDate)
                         ,DATEPART(MONTH, CreatedDate)
                         ,DATEPART(WEEK, CreatedDate) 
                         ,DATEPART(DAY, CreatedDate)";

                        var listCustomerOfOtherStylist = db.Database.SqlQuery<DataCountByDate>(sql).ToList();
                        count += listCustomerOfOtherStylist.Count;
                    }

                }
                return count;
            }
        }
        public static List<DataCountByDate> GetBillN(int salonId, string type, string from, string to, string viewTime)
        {
            using (var db = new Solution_30shineEntities())
            {
                var selectTime = "";
                var andType = "";
                var andSalonId = "";
                var orderBy = "";
                var groupBy = "";

                if (salonId == 0)
                    andSalonId = "SalonId>0";
                else
                {
                    andSalonId = "SalonId='" + salonId + "'";
                }

                switch (type)
                {
                    case "BillServiceTotal":
                        andType = "(ServiceIds is not null and ServiceIds!='')";
                        break;


                    case "BillCosmeticTotal":
                        andType = "(ProductIds is not null and ProductIds!='')";
                        break;

                    case "BillCosmeticOnline":
                        andType = "(ProductIds is not null and ProductIds!='') and (ServiceIds is null or ServiceIds='')";
                        break;

                    case "BillCosmeticSalon":
                        andType = "(ProductIds is not null and ProductIds!='') and (ServiceIds is not null and ServiceIds!='')";
                        break;
                }





                switch (viewTime)
                {
                    case "day":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month],m.[Week],m.[Day]";
                        break;
                    case "week":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month],m.[Week]";
                        break;
                    case "month":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month]";
                        break;
                    case "quarter":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter]";
                        break;
                    case "year":
                        selectTime = groupBy = orderBy = " m.[Year]";
                        break;
                }
                var sql0 = @"select COUNT(*) as [Count]
                      ,DATEPART(YEAR, CreatedDate) as [Year]
                      ,DATEPART(QUARTER, CreatedDate) as [Quarter] 
                      ,DATEPART(MONTH, CreatedDate) as [Month]
                      ,DATEPART(WEEK, CreatedDate) as [Week] 
                      ,DATEPART(DAY, CreatedDate) as [Day] 
                            from BillService where IsDelete!=1 and Pending != 1 and " + andType +
                               @" and (CreatedDate between @dtFrom and @dtTo) and " + andSalonId +
                               @" group by
                      DATEPART(YEAR, CreatedDate)
                      ,DATEPART(QUARTER, CreatedDate)
                      ,DATEPART(MONTH, CreatedDate) 
                      ,DATEPART(WEEK, CreatedDate)
                      ,DATEPART(DAY, CreatedDate)";

                var sql = @"declare @dtFrom datetime, @dtTo datetime
                            set @dtFrom = '" + from + "'" +
                         " set @dtTo = '" + to + "'" +
                         " select SUM(m.[Count]) as [Count], " + selectTime +
                         @" from 
                            (
                            select a1.*, Coalesce(b.[Count], 0) as [Count]
                            from
                            (
                            select 
                             DATEPART(YEAR, [DateTime]) as [Year] 
                             ,DATEPART(QUARTER, [DateTime]) as [Quarter] 
                             ,DATEPART(MONTH, [DateTime]) as[Month]
                             ,DATEPART(WEEK, [DateTime]) as[Week] 
                             ,DATEPART(DAY, [DateTime]) as [Day] 
                            from 
                            (
                             select 
                              dateadd(day, number, @dtFrom) as [DateTime]
                             from 
                              (select distinct number from master.dbo.spt_values
                               where name is null
                              ) as n
                             where dateadd(day, number, @dtFrom) < @dtTo 
                            ) as a
                            ) as a1
                            left join 
                            (" + sql0 +
                         @") as b 
                            on a1.[Year]=b.[Year] and a1.[Quarter] = b.[Quarter] and a1.[Month] = b.[Month] and a1.[Week] = b.[Week] and a1.[Day] = b.[Day]
                            )m
                            group by " + groupBy +
                         " order by " +
                         orderBy;

                var listFilter = db.Database.SqlQuery<DataCountByDate>(sql).ToList();
                var listReLabel = ReLabelDate(listFilter, salonId, from, to, viewTime);

                return listReLabel;
            }

        }

        public static List<DataCountByDate> GetBill(int salonId, string type, string from, string to, string viewTime)
        {
            using (var db = new Solution_30shineEntities())
            {
                var sql = "";
                var andBetweenDate = "(CreatedDate between '" + from + "' and '" + to + "')";
                var andSalonId = "";
                if (salonId == 0)
                    andSalonId = "SalonId>0";
                else
                {
                    andSalonId = "SalonId='" + salonId + "'";
                }
                switch (type)
                {
                    case "BillServiceTotal":
                        sql = @"select 
                            CAST(CreatedDate as Date) as [Date],
                            COUNT(*) as [Count]
                            from BillService
                            where IsDelete != 1 and Pending !=1 and " + andSalonId + " and " + andBetweenDate + " and ServiceIds is not null and ServiceIds!='' group by  CAST(CreatedDate as DATE) order by [Date]";
                        break;


                    case "BillCosmeticTotal":
                        sql = @"select 
                            CAST(CreatedDate as Date) as [Date],
                            COUNT(*) as [Count]
                            from BillService
                            where IsDelete != 1 and Pending != 1 and " + andSalonId + " and " + andBetweenDate + " and (ProductIds is not null and ProductIds!='') group by CAST(CreatedDate as DATE) order by [Date]";
                        break;

                    case "BillCosmeticOnline":
                        sql = @"select 
                            CAST(CreatedDate as Date) as [Date],
                            COUNT(*) as [Count]
                            from BillService
                            where IsDelete != 1 and Pending != 1 and " + andSalonId + " and " + andBetweenDate + " and (ProductIds is not null and ProductIds!='') and (ServiceIds is null or ServiceIds='') group by CAST(CreatedDate as DATE) order by [Date]";
                        break;

                    case "BillCosmeticSalon":
                        sql = @"select 
                            CAST(CreatedDate as Date) as [Date],
                            COUNT(*) as [Count]
                            from BillService
                            where IsDelete != 1 and Pending != 1 and " + andSalonId + " and " + andBetweenDate + " and (ProductIds is not null and ProductIds!='') and (ServiceIds is not null and ServiceIds!='') group by CAST(CreatedDate as DATE) order by [Date]";
                        break;
                }

                var listFilter = db.Database.SqlQuery<DataCountByDate>(sql).ToList();

                var listReLabel = ReLabelDate(listFilter, salonId, from, to, viewTime);

                return listReLabel;
            }

        }

        [WebMethod]
        public static string ReportSale(int salonId, string from, string to, string viewTime)
        {
            var data = new DataSale();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            CultureInfo culture = new CultureInfo("vi-VN");
            //DateTime _FromDate = Convert.ToDateTime(from, culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
            DateTime _FromDate = Convert.ToDateTime(from, culture);
            DateTime _ToDate = _FromDate;
            if (to != "")
            {
                _ToDate = Convert.ToDateTime(to, culture).AddDays(1);
            }
            var timeFrom = String.Format("{0:MM/dd/yyyy}", _FromDate);
            var timeTo = String.Format("{0:MM/dd/yyyy}", _ToDate);

            if (timeFrom != "" && timeTo != "")
            {
                List<DataCountByDate> saleService = new List<DataCountByDate>();
                List<DataCountByDate> saleCosmetic = new List<DataCountByDate>();
                List<DataCountByDate> saleCosmeticOnline = new List<DataCountByDate>();
                List<DataCountByDate> saleCosmeticSalon = new List<DataCountByDate>();


                saleService = GetSaleN(salonId, "SaleService", timeFrom, timeTo, viewTime, true);
                saleCosmetic = GetSaleN(salonId, "SaleCosmetic", timeFrom, timeTo, viewTime, true);
                saleCosmeticOnline = GetSaleN(salonId, "SaleCosmeticOnline", timeFrom, timeTo, viewTime, true);
                saleCosmeticSalon = GetSaleN(salonId, "SaleCosmeticSalon", timeFrom, timeTo, viewTime, true);


                using (var db = new Solution_30shineEntities())
                {
                    var salon = "";
                    var salonName = db.Tbl_Salon.Where(w => w.Id == salonId).Select(s => s.Name).FirstOrDefault();
                    if (salonName != null)
                        salon = "tại salon " + salonName;
                    else salon = "tại các Salon";

                    data.Salon = salon;
                    if (viewTime == "day")
                        data.Unit = "Nghìn đồng";
                    else if (viewTime == "month" || viewTime == "week" || viewTime == "quarter" || viewTime == "year")
                        data.Unit = "Triệu đồng";

                    data.Title = "Doanh thu " + salon + " (đơn vị " + data.Unit + ")";

                    data.SaleService = serializer.Serialize(saleService);
                    data.SaleCosmetic = serializer.Serialize(saleCosmetic);
                    data.SaleCosmeticOnline = serializer.Serialize(saleCosmeticOnline);
                    data.SaleCosmeticSalon = serializer.Serialize(saleCosmeticSalon);

                    data.Number = saleService.Count;
                }
            }
            return serializer.Serialize(data);
        }



        public static List<DataCountByDate> GetSaleN(int salonId, string type, string from, string to, string viewTime, bool reLabel)
        {
            using (var db = new Solution_30shineEntities())
            {

                var selectSum = "";
                var selectTime = "";
                var fromTable = "";
                var andType = "";
                var andSalonId = "";
                var orderBy = "";
                var groupBy = "";

                if (salonId == 0)
                    andSalonId = "SalonId>0";
                else
                {
                    andSalonId = "SalonId='" + salonId + "'";
                }

                switch (viewTime)
                {
                    case "day":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month],m.[Week],m.[Day]";
                        break;
                    case "week":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month],m.[Week]";

                        break;
                    case "month":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month]";

                        break;
                    case "quarter":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter]";

                        break;
                    case "year":
                        selectTime = groupBy = orderBy = " m.[Year]";
                        break;
                }

                var sql0 = "";
                switch (type)
                {
                    case "SaleService":
                        fromTable = "FlowService";
                        selectSum = "Coalesce(SUM( Price * Quantity * ( Cast(100 - VoucherPercent as float)/Cast(100 as float))),0) as [Sum] ";
                        // andType = " IsDelete!=1";

                        sql0 = @"Select " + selectSum +
                               @",DATEPART(YEAR, CreatedDate) as [Year]
                             ,DATEPART(QUARTER, CreatedDate) as [Quarter] 
                             ,DATEPART(MONTH, CreatedDate) as[Month] 
                             ,DATEPART(WEEK, CreatedDate) as[Week]
                             ,DATEPART(DAY, CreatedDate) as [Day] 
                            from " + fromTable +
                           @" where (CreatedDate between @dtFrom and @dtTo) 
                            and IsDelete!=1 and " + andSalonId +
                           @" group by
                             DATEPART(YEAR, CreatedDate) 
                             ,DATEPART(QUARTER, CreatedDate) 
                             ,DATEPART(MONTH, CreatedDate) 
                             ,DATEPART(WEEK, CreatedDate) 
                             ,DATEPART(DAY, CreatedDate)";

                        break;


                    case "SaleCosmetic":
                        fromTable = "FlowProduct";
                        selectSum = "Coalesce(SUM( Price * Quantity * ( Cast(100 - VoucherPercent as float)/Cast(100 as float)) - Coalesce(PromotionMoney, 0)),0) as [Sum] ";
                        //andType = " IsDelete!=1";

                        sql0 = @"Select " + selectSum +
                              @",DATEPART(YEAR, CreatedDate) as [Year]
                             ,DATEPART(QUARTER, CreatedDate) as [Quarter] 
                             ,DATEPART(MONTH, CreatedDate) as[Month] 
                             ,DATEPART(WEEK, CreatedDate) as[Week]
                             ,DATEPART(DAY, CreatedDate) as [Day] 
                            from " + fromTable +
                          @" where (CreatedDate between @dtFrom and @dtTo) 
                            and IsDelete!=1 and " + andSalonId +
                          @" group by
                             DATEPART(YEAR, CreatedDate) 
                             ,DATEPART(QUARTER, CreatedDate) 
                             ,DATEPART(MONTH, CreatedDate) 
                             ,DATEPART(WEEK, CreatedDate) 
                             ,DATEPART(DAY, CreatedDate)";

                        break;

                    case "SaleCosmeticOnline":
                        fromTable = "BillService";
                        selectSum = "Coalesce(( Price * Quantity * ( Cast(100 - VoucherPercent as float)/Cast(100 as float)) - Coalesce(PromotionMoney, 0)),0) as [Sum] ";
                        andType = "IsDelete!=1 and (ServiceIds is null or ServiceIds='') and ProductIds is not null and ProductIds!=''";

                        sql0 = @"select
                              SUM(d.[Sum]) as [Sum]
                             ,c.[Year]
                             ,c.[Quarter]
                             ,c.[Month]
                             ,c.[Week]
                             ,c.[Day]
                            from
                            (
                            select Id
                              ,DATEPART(YEAR, CreatedDate) as [Year] 
                              ,DATEPART(QUARTER, CreatedDate) as [Quarter] 
                              ,DATEPART(MONTH, CreatedDate) as [Month]
                              ,DATEPART(WEEK, CreatedDate) as [Week] 
                              ,DATEPART(DAY, CreatedDate) as [Day] 
                             from BillService 
                             where (CreatedDate between @dtFrom and @dtTo)
                             and " + andType + " and " + andSalonId +
                                 @") as c
                             inner join 
                             (select " + selectSum +
                                 @",BillId	                            
                             from FlowProduct ) as d 
                             on c.Id=d.BillId
                             group by 
                               c.[Year]
                               ,c.[Quarter]
                               ,c.[Month]
                               ,c.[Week]
                               ,c.[Day]";
                        break;
                    case "SaleCosmeticSalon":
                        fromTable = "BillService";
                        selectSum = "Coalesce(( Price * Quantity * ( Cast(100 - VoucherPercent as float)/Cast(100 as float)) - Coalesce(PromotionMoney, 0)),0) as [Sum] ";
                        andType = "IsDelete!=1 and ServiceIds is not null and ServiceIds!='' and ProductIds is not null and ProductIds!=''";

                        sql0 = @"select
                              SUM(d.[Sum]) as [Sum]
                             ,c.[Year]
                             ,c.[Quarter]
                             ,c.[Month]
                             ,c.[Week]
                             ,c.[Day]
                            from
                            (
                            select Id
                              ,DATEPART(YEAR, CreatedDate) as [Year] 
                              ,DATEPART(QUARTER, CreatedDate) as [Quarter] 
                              ,DATEPART(MONTH, CreatedDate) as [Month]
                              ,DATEPART(WEEK, CreatedDate) as [Week] 
                              ,DATEPART(DAY, CreatedDate) as [Day] 
                             from BillService 
                             where (CreatedDate between @dtFrom and @dtTo)
                             and " + andType + " and " + andSalonId +
                                @") as c
                             inner join 
                             (select " + selectSum +
                                @",BillId	                            
                             from FlowProduct ) as d 
                             on c.Id=d.BillId
                             group by 
                               c.[Year]
                               ,c.[Quarter]
                               ,c.[Month]
                               ,c.[Week]
                               ,c.[Day]";
                        break;
                }

                var sql = @"declare @dtFrom datetime, @dtTo datetime
                            set @dtFrom = '" + from + "'" +
                            " set @dtTo = '" + to + "'" +
                            " select SUM(m.[Sum]) as [Sum], " + selectTime +
                            @" from 
                            (
                            select a1.*, Coalesce(b.[Sum], 0) as [Sum]
                            from
                            (
                            select 
                             DATEPART(YEAR, [DateTime]) as [Year] 
                             ,DATEPART(QUARTER, [DateTime]) as [Quarter] 
                             ,DATEPART(MONTH, [DateTime]) as[Month]
                             ,DATEPART(WEEK, [DateTime]) as[Week] 
                             ,DATEPART(DAY, [DateTime]) as [Day] 
                            from 
                            (
                             select 
                              dateadd(day, number, @dtFrom) as [DateTime]
                             from 
                              (select distinct number from master.dbo.spt_values
                               where name is null
                              ) as n
                             where dateadd(day, number, @dtFrom) < @dtTo 
                            ) as a
                            ) as a1
                            left join 
                            (" + sql0 +
                            @") as b 
                            on a1.[Year]=b.[Year] and a1.[Quarter] = b.[Quarter] and a1.[Month] = b.[Month] and a1.[Week] = b.[Week] and a1.[Day] = b.[Day]
                            )m
                            group by " + groupBy +
                            " order by " +
                            orderBy;

                var listFilter = db.Database.SqlQuery<DataCountByDate>(sql).ToList();

                if (reLabel == false)
                    return listFilter;
                var listReLabel = ReLabelDate(listFilter, salonId, from, to, viewTime);
                return listReLabel;
            }
        }

        public static List<DataCountByDate> GetSale(int salonId, string type, string from, string to, string viewTime)
        {
            using (var db = new Solution_30shineEntities())
            {
                var sql = "";
                var andBetweenDate = "(CreatedDate between '" + from + "' and '" + to + "')";
                var andSalonId = "";
                if (salonId == 0)
                    andSalonId = "SalonId>0";
                else
                {
                    andSalonId = "SalonId='" + salonId + "'";
                }
                switch (type)
                {

                    case "SaleService":
                        sql = @"Select CAST(CreatedDate as date) as [Date]
                            ,SUM(Price*Quantity) as [Sum]
                             from FlowService            
                             where " + andBetweenDate + " and IsDelete!=1 and " + andSalonId + " group by CAST(CreatedDate as date) order by [Date]";
                        break;

                    case "SaleCosmetic":
                        sql = @"Select CAST(CreatedDate as date) as [Date]
                            ,SUM(Price*Quantity) as [Sum]
                             from FlowProduct            
                             where " + andBetweenDate + " and IsDelete!=1 and " + andSalonId + " group by CAST(CreatedDate as date) order by [Date]";
                        break;

                    case "SaleCosmeticOnline":
                        sql = @"Select CAST(CreatedDate as date) as [Date]
                                ,SUM(TotalMoney) as [Sum]
                                 from BillService            
                                 where " + andBetweenDate + " and IsDelete!=1 and (ServiceIds is null or ServiceIds='') and ProductIds is not null and ProductIds!='' and " + andSalonId + " group by CAST(CreatedDate as date)order by [Date]";
                        break;

                    case "SaleCosmeticSalon":
                        sql = @" Select CAST(CreatedDate as date) as [Date]
                                ,SUM(TotalMoney) as [Sum]
                                 from BillService            
                                 where " + andBetweenDate + " and IsDelete!=1 and ServiceIds is not null and ServiceIds!='' and ProductIds is not null and ProductIds!='' and " + andSalonId + " group by CAST(CreatedDate as date) order by [Date]";
                        break;


                }

                var listFilter = db.Database.SqlQuery<DataCountByDate>(sql).ToList();

                var listReLabel = ReLabelDate(listFilter, salonId, from, to, viewTime);

                return listReLabel;
            }

        }



        public static List<DataCountByDate> GetBillServiceCustomerN(int salonId, string type, string from, string to, string viewTime)
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = new List<DataCountByDate>();
                var selectTime = "";
                var selectTimeB = "";
                var selectTimeE = "";
                var groupBy = "";
                var groupByB = "";
                var groupByE = "";
                var orderByE = "";
                var andSalonId = "";
                var having = "";


                if (salonId == 0)
                    andSalonId = "SalonId>0";
                else
                    andSalonId = "SalonId='" + salonId + "'";

                var totalCount = "";
                if (type == "old")
                {
                    having = "COUNT(*)>0";
                    totalCount = "Total_Count >1";
                }
                else if (type == "new")
                {
                    having = "COUNT(*)=1";
                    totalCount = "Total_Count=1";
                }



                switch (viewTime)
                {
                    case "day":
                        selectTime = "DATEPART(YEAR, CreatedDate) AS [Year] ,DATEPART(QUARTER, CreatedDate) as [Quarter] ,DATEPART(MONTH, CreatedDate) AS[Month] ,DATEPART(WEEK, CreatedDate) AS[Week] ,DATEPART(DAY, CreatedDate) as [Day]";
                        selectTimeB = "b.Year, b.Quarter, b.Month, b.Week, b.Day";
                        groupBy = "DATEPART(YEAR, CreatedDate) ,DATEPART(QUARTER, CreatedDate) ,DATEPART(MONTH, CreatedDate) ,DATEPART(WEEK, CreatedDate) ,DATEPART(DAY, CreatedDate)";
                        groupByE = "e.[Year], e.[Quarter], e.[Month],e.[Week], e.[Day]";

                        // orderBy = "  Year ,Quarter ,Month ,Week ,Day;";
                        break;
                    case "week":
                        selectTime = "DATEPART(YEAR, CreatedDate) AS [Year] ,DATEPART(QUARTER, CreatedDate) as [Quarter] ,DATEPART(MONTH, CreatedDate) AS[Month] ,DATEPART(WEEK, CreatedDate) AS [Week]";
                        selectTimeB = "b.Year, b.Quarter, b.Month, b.Week";
                        groupByE = "e.[Year], e.[Quarter], e.[Month],e.[Week]";
                        groupBy = "DATEPART(YEAR, CreatedDate) ,DATEPART(QUARTER, CreatedDate) ,DATEPART(MONTH, CreatedDate) ,DATEPART(WEEK, CreatedDate)";

                        //get datetime of begin date and end date



                        // orderBy = "  Year ,Quarter ,Month ,Week";
                        break;
                    case "month":
                        selectTime = "DATEPART(YEAR, CreatedDate) AS [Year] ,DATEPART(QUARTER, CreatedDate) as [Quarter] ,DATEPART(MONTH, CreatedDate) AS[Month]";
                        selectTimeB = "b.Year, b.Quarter, b.Month";
                        groupByE = "e.[Year], e.[Quarter], e.[Month]";
                        groupBy = "DATEPART(YEAR, CreatedDate) ,DATEPART(QUARTER, CreatedDate) ,DATEPART(MONTH, CreatedDate)";

                        // orderBy = "  Year ,Quarter ,Month;";
                        break;
                    case "quarter":
                        selectTime = "DATEPART(YEAR, CreatedDate) AS [Year] ,DATEPART(QUARTER, CreatedDate) as [Quarter]";
                        selectTimeB = "b.Year, b.Quarter";
                        groupByE = "e.[Year], e.[Quarter]";
                        groupBy = "DATEPART(YEAR, CreatedDate) ,DATEPART(QUARTER, CreatedDate)";

                        //orderBy = "  Year ,Quarter;";
                        break;
                    case "year":
                        selectTime = "DATEPART(YEAR, CreatedDate) AS [Year]";
                        selectTimeB = "b.Year";
                        groupBy = "DATEPART(YEAR, CreatedDate)";

                        //orderBy = "  Year;";
                        break;
                }
                groupByB = selectTimeB;
                selectTimeE = orderByE = groupByE;

                var listTimeRange = GetTimeRange(salonId, from, to, viewTime);

                for (int i = 0; i < listTimeRange.Count; ++i)
                {
                    var timeFrom = String.Format("{0:MM/dd/yyy}", listTimeRange[i].timeFrom);
                    var timeTo = String.Format("{0:MM/dd/yyy}", listTimeRange[i].timeTo);
                    var andBetweenDate = "(CreatedDate between '" + timeFrom + "' and '" + timeTo + "')";

                    var sql = @"
                            select Count(e.CustomerCode) as [Count], " +
                                  selectTimeE +
                                " from ( select b.CustomerCode, " +
                                     selectTimeB +
                                    " ,COUNT(c.CustomerCode) as Total_Count" +
                                    " from BillService as c inner join (select CustomerCode, COUNT(*) as [Count], " +
                                       selectTime +
                                        " from BillService where " +
                                       andBetweenDate +
                                        " and IsDelete != 1 and Pending != 1 and " +
                                       andSalonId +
                                        " group by " +
                                       groupBy +
                                        " ,CustomerCode ) as b on c.CustomerCode = b.CustomerCode where c.IsDelete != 1 and c.Pending != 1 and CreatedDate > '2015/8/1' and CreatedDate < '" + timeTo + "' and " + andSalonId + " group by " +
                                  groupByB +
                                " ,b.CustomerCode having " + having + ") as e where " + totalCount + " group by " +
                                groupByE +
                               " order by " +
                              orderByE;
                    var obj = db.Database.SqlQuery<DataCountByDate>(sql).FirstOrDefault();
                    list.Add(obj);
                }
                var listReLabel = ReLabelDate(list, salonId, from, to, viewTime);
                return listReLabel;
            }
        }






        public static List<DataCountByDate> GetBillServiceCustomerOld(int salonId, string from, string to, List<DataCountByDate> billServiceTotal, string viewTime)
        {
            using (var db = new Solution_30shineEntities())
            {
                var listReLabel = new List<DataCountByDate>();

                var andSalonId = "";
                if (salonId == 0)
                    andSalonId = "SalonId>0";
                else
                {
                    andSalonId = "SalonId='" + salonId + "'";
                }

                var list = new List<DataCountByDate>();
                foreach (DataCountByDate obj in billServiceTotal)
                {
                    var date = new DateTime(obj.Year, obj.Month, obj.Day);
                    var timeFrom = String.Format("{0:MM/dd/yyyy}", date);
                    var timeTo = String.Format("{0:MM/dd/yyyy}", date.AddDays(1));
                    if (timeFrom != "" && timeTo != "")
                    {
                        var obj2 = new DataCountByDate();
                        if (obj.Count != 0)
                        {
                            //get total price service in table flowService
                            var andBetweenDate = "(CreatedDate between '" + timeFrom + "' and '" + timeTo + "')";
                            var sql = @"select CAST(CreatedDate as Date)  as [Date],
                                 COUNT(*) as [Count]
                                from BillService as d
                                inner join
                                (
                                 select CustomerCode
                                 from
                                 (
                                  select CustomerCode,
                                  (
                                   select COUNT(*) from BillService 
                                   where CustomerCode = b.CustomerCode and IsDelete != 1 and Pending != 1
                                   --and ServiceIds != '' and ServiceIds is not null
                                   and CreatedDate < '" + timeTo + "') as times from BillService as b where IsDelete != 1 and Pending != 1 and ServiceIds != '' and ServiceIds is not null and " + andBetweenDate + " and CustomerCode != '' and " + andSalonId + "group by CustomerCode) as a where times > 1) as c on d.CustomerCode = c.CustomerCode where d.IsDelete != 1 and d.Pending != 1 and ServiceIds != '' and ServiceIds is not null and " + andBetweenDate + " and " + andSalonId + " group by CAST(CreatedDate as Date)";

                            obj2 = db.Database.SqlQuery<DataCountByDate>(sql).FirstOrDefault();

                        }
                        else
                        {
                            obj2.Date = date;
                            obj2.Count = 0;
                        }
                        list.Add(obj2);

                    }
                    listReLabel = ReLabelDate(list, salonId, timeFrom, timeTo, viewTime);
                }

                return listReLabel;
                //return list;
            }
        }

        public static List<DataCountByDate> GetBillServiceCustomerNew(int salonId, string from, string to, List<DataCountByDate> billServiceTotal, List<DataCountByDate> billServiceCustomerOld, string viewTime)
        {
            using (var db = new Solution_30shineEntities())
            {
                var listReLabel = new List<DataCountByDate>();

                var list = new List<DataCountByDate>();
                for (int i = 0; i < billServiceCustomerOld.Count; ++i)
                {
                    var obj2 = new DataCountByDate();
                    obj2.Date = billServiceCustomerOld[i].Date;
                    obj2.Count = billServiceTotal[i].Count - billServiceCustomerOld[i].Count;
                    list.Add(obj2);
                }
                listReLabel = ReLabelDate(list, salonId, from, to, viewTime);
                return listReLabel;
                // return list;
            }

        }



        [WebMethod]
        public static string ReportSaleAvg(int salonId, string from, string to, string viewTime)
        {
            DataSale data = new DataSale();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {

                List<DataCountByDate> list = new List<DataCountByDate>();
                list = GetAvgSale(salonId, from, to, viewTime);

                var salon = "";
                var salonName = db.Tbl_Salon.Where(w => w.Id == salonId).Select(s => s.Name).FirstOrDefault();
                if (salonName != null)
                    salon = "tại salon " + salonName;
                else salon = "tại các Salon";

                data.Salon = salon;
                data.Unit = "Đồng";
                data.Title = "Giá trị trung bình mỗi hóa đơn dịch vụ " + salon + " (đơn vị Đồng)";


                data.Number = list.Count;

                data.Avg = serializer.Serialize(list);

            }
            return serializer.Serialize(data);
        }



        public static List<DataCountByDate> GetAvgSale(int salonId, string from, string to, string viewTime)
        {
            using (var db = new Solution_30shineEntities())
            {
                var listReLabel = new List<DataCountByDate>();
                CultureInfo culture = new CultureInfo("vi-VN");
                DateTime _FromDate = Convert.ToDateTime(from, culture);
                DateTime _ToDate = _FromDate;
                if (to != "")
                {
                    _ToDate = Convert.ToDateTime(to, culture).AddDays(1);
                }
                var timeFrom = String.Format("{0:MM/dd/yyyy}", _FromDate);
                var timeTo = String.Format("{0:MM/dd/yyyy}", _ToDate);

                if (timeFrom != "" && timeTo != "")
                {

                    var billServiceTotal = GetBillN(salonId, "BillServiceTotal", timeFrom, timeTo, viewTime);
                    //get total price service in table flowService
                    var saleService = GetSaleN(salonId, "SaleService", timeFrom, timeTo, viewTime, false);
                    //var andBetweenDate = "(CreatedDate between '" + timeFrom + "' and '" + timeTo + "')";
                    //var sql = @"select CAST(CreatedDate as Date) as [Date]
                    //        ,SUM(Price*Quantity) as [Count]
                    //         from FlowService
                    //     where IsDelete!=1 and " + andBetweenDate + " group by  CAST(CreatedDate as DATE) order by [Date]";
                    //var listPrice = db.Database.SqlQuery<DataCountByDate>(sql).ToList();

                    var listJoin = new List<DataCountByDate>();
                    for (int i = 0; i < billServiceTotal.Count; ++i)
                    {
                        var obj = new DataCountByDate();
                        obj.Day = billServiceTotal[i].Day;
                        obj.Week = billServiceTotal[i].Week;
                        obj.Month = billServiceTotal[i].Month;
                        obj.Quarter = billServiceTotal[i].Quarter;
                        obj.Year = billServiceTotal[i].Year;
                        obj.Count = billServiceTotal[i].Count;
                        obj.Sum = saleService[i].Sum;
                        if (obj.Count != 0)
                            obj.Avg = Math.Round(obj.Sum / obj.Count, 0);
                        else if (obj.Count == 0)
                            obj.Avg = 0;
                        listJoin.Add(obj);
                    }
                    //listReLabel = listJoin;
                    listReLabel = ReLabelDate(listJoin, salonId, timeFrom, timeTo, viewTime);
                }
                return listReLabel;
            }
        }

        [WebMethod]
        public static string ReportPercentCustomer(int salonId, string from, string to, string viewTime)
        {
            DataCustomer data = new DataCustomer();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            CultureInfo culture = new CultureInfo("vi-VN");
            DateTime _FromDate = Convert.ToDateTime(from, culture);
            DateTime _ToDate = _FromDate;
            if (to != "")
            {
                _ToDate = Convert.ToDateTime(to, culture).AddDays(1);
            }
            var timeFrom = String.Format("{0:MM/dd/yyyy}", _FromDate);
            var timeTo = String.Format("{0:MM/dd/yyyy}", _ToDate);
            using (var db = new Solution_30shineEntities())
            {
                var list = GetCustomerPercent(salonId, timeFrom, timeTo, viewTime);

                data.Unit = "%";
                data.Title = "Tỉ lệ khách hàng quay lại (đơn vị %)";
                var salon = db.Tbl_Salon.Where(w => w.Id == salonId).Select(s => s.Name).FirstOrDefault();
                if (salon != null)
                    data.Salon = salon;

                data.Percent = serializer.Serialize(list);
                data.Number = list.Count;
            }
            return serializer.Serialize(data);
        }



        private static List<DataCountByDate> GetCustomerPercent(int salonId, string timeFrom, string timeTo, string viewTime)
        {
            var listReLabel = new List<DataCountByDate>();
            var billServiceTotal = GetBill(salonId, "BillServiceTotal", timeFrom, timeTo, viewTime);
            var billServiceCustomerOld = GetBillServiceCustomerOld(salonId, timeFrom, timeTo, billServiceTotal, viewTime);
            var list = new List<DataCountByDate>();
            for (int i = 0; i < billServiceTotal.Count; ++i)
            {
                var obj = new DataCountByDate();
                obj.Date = billServiceTotal[i].Date;
                obj.Day = billServiceTotal[i].Day;
                obj.Month = billServiceTotal[i].Month;
                obj.Year = billServiceTotal[i].Year;

                obj.Percent = (int)Math.Round((double)(100 * billServiceCustomerOld[i].Count) / billServiceTotal[i].Count);
                list.Add(obj);
            }
            listReLabel = ReLabelDate(list, salonId, timeFrom, timeTo, viewTime);
            return listReLabel;
        }


        //
        [WebMethod]
        public static string ReportStaff(int salonId, string from, string to, string viewTime)
        {
            var data = new DataTotal();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            CultureInfo culture = new CultureInfo("vi-VN");
            DateTime _FromDate = Convert.ToDateTime(from, culture);
            DateTime _ToDate = _FromDate;
            if (to != "")
            {
                _ToDate = Convert.ToDateTime(to, culture).AddDays(1);
            }
            var timeFrom = String.Format("{0:MM/dd/yyyy}", _FromDate);
            var timeTo = String.Format("{0:MM/dd/yyyy}", _ToDate);

            var sql = "";
            var andBetweenDate = "(CreatedDate between '" + timeFrom + "' and '" + timeTo + "')";
            var selectTime = "";
            var groupBy = "";
            var orderBy = "";
            switch (viewTime)
            {
                case "day":
                    selectTime = "DATEPART(YEAR, CreatedDate) AS [Year] ,DATEPART(QUARTER, CreatedDate) as [Quarter] ,DATEPART(MONTH, CreatedDate) AS[Month] ,DATEPART(WEEK, CreatedDate) AS[Week] ,DATEPART(DAY, CreatedDate) as [Day]";
                    groupBy = "DATEPART(YEAR, CreatedDate) ,DATEPART(QUARTER, CreatedDate) ,DATEPART(MONTH, CreatedDate) ,DATEPART(WEEK, CreatedDate) ,DATEPART(DAY, CreatedDate)";
                    orderBy = "[Year] ,[Quarter] ,[Month] ,[Week] ,[Day]";
                    break;
                case "week":
                    selectTime = "DATEPART(YEAR, CreatedDate) AS [Year] ,DATEPART(QUARTER, CreatedDate) as [Quarter] ,DATEPART(MONTH, CreatedDate) AS[Month] ,DATEPART(WEEK, CreatedDate) AS[Week]";
                    groupBy = "DATEPART(YEAR, CreatedDate) ,DATEPART(QUARTER, CreatedDate) ,DATEPART(MONTH, CreatedDate) ,DATEPART(WEEK, CreatedDate)";
                    orderBy = "[Year] ,[Quarter] ,[Month] ,[Week]";
                    break;
                case "month":
                    selectTime = "DATEPART(YEAR, CreatedDate) AS [Year] ,DATEPART(QUARTER, CreatedDate) as [Quarter] ,DATEPART(MONTH, CreatedDate) AS[Month]";
                    groupBy = "DATEPART(YEAR, CreatedDate) ,DATEPART(QUARTER, CreatedDate) ,DATEPART(MONTH, CreatedDate)";
                    orderBy = "[Year] ,[Quarter] ,[Month]";
                    break;
                case "quarter":
                    selectTime = "DATEPART(YEAR, CreatedDate) AS [Year] ,DATEPART(QUARTER, CreatedDate) as [Quarter]";
                    groupBy = "DATEPART(YEAR, CreatedDate) ,DATEPART(QUARTER, CreatedDate)";
                    orderBy = "[Year] ,[Quarter]";
                    break;
                case "year":
                    selectTime = "DATEPART(YEAR, CreatedDate) AS [Year]";
                    groupBy = "DATEPART(YEAR, CreatedDate)";
                    orderBy = "[Year]";
                    break;

            }

            using (var db = new Solution_30shineEntities())
            {
                if (salonId > 0)
                    sql = @"SELECT Stylist as [Stylist],
                            Skinner as [Skinner]," +
                           selectTime +
                            "from FlowStaff where SalonId=" + salonId + " and [Stylist] is not null and [Skinner] is not null and " + andBetweenDate + " and " + groupBy + " order by " +
                            orderBy;
                else
                    sql = @"SELECT SUM(Stylist) as [Stylist],
                            SUM(Skinner) as [Skinner]," +
                           selectTime +
                            " from FlowStaff where " + andBetweenDate + " and [Skinner] is not null " +
                            " group by " + groupBy +
                            " order by " +
                           orderBy;

                var flowStaffs = db.Database.SqlQuery<DataCountStaff>(sql).ToList();

                var billServiceTotal = GetBillN(salonId, "BillServiceTotal", timeFrom, timeTo, viewTime);

                var list = new List<DataCountByDate>();
                var list2 = new List<DataCountByDate>();

                if (flowStaffs.Count > 0 && billServiceTotal.Count > 0)
                {
                    var count = billServiceTotal.Count;
                    if (flowStaffs.Count < billServiceTotal.Count)
                        count = flowStaffs.Count;

                    for (int i = 0; i < count; ++i)
                    {
                        var obj = new DataCountByDate();
                        var obj2 = new DataCountByDate();
                        obj.Year = billServiceTotal[i].Year;
                        obj2.Year = billServiceTotal[i].Year;
                        obj.Quarter = billServiceTotal[i].Quarter;
                        obj2.Quarter = billServiceTotal[i].Quarter;
                        obj.Month = billServiceTotal[i].Month;
                        obj2.Month = billServiceTotal[i].Month;
                        obj.Week = billServiceTotal[i].Week;
                        obj2.Week = billServiceTotal[i].Week;
                        obj.Day = billServiceTotal[i].Day;
                        obj2.Day = billServiceTotal[i].Day;

                        var stylist = flowStaffs[i].Stylist;
                        if (stylist != 0)
                        {
                            //var Percent = Math.Round((billServiceTotal[i].Count) / (double)stylist);
                            var Percent = billServiceTotal[i].Count / stylist;
                            obj.Percent = Math.Round(Percent, 1);
                        }
                        var skinner = flowStaffs[i].Skinner;
                        if (skinner != 0)
                        {
                            //var Percent2 = Math.Round((double)(billServiceTotal[i].Count) / (double)skinner);
                            var Percent2 = billServiceTotal[i].Count / skinner;
                            obj2.Percent = Math.Round(Percent2, 1);
                        }

                        list.Add(obj);
                        list2.Add(obj2);

                    }
                }
                var listProductivityStylelist = ReLabelDate(list, salonId, timeFrom, timeTo, viewTime);
                var listProductivitySkinner = ReLabelDate(list2, salonId, timeFrom, timeTo, viewTime);
                var salon = "";
                var salonName = db.Tbl_Salon.Where(w => w.Id == salonId).Select(s => s.Name).FirstOrDefault();
                if (salonName != null)
                    salon = "tại salon " + salonName;
                else salon = "tại các Salon";
                var unit = "";
                if (viewTime == "day")
                    unit = "Ngày";
                else if (viewTime == "week")
                    unit = "Tuần";
                else if (viewTime == "quarter")
                    unit = "Quý";
                else if (viewTime == "year")
                    unit = "Năm";
                else if (viewTime == "month")
                    unit = "Tháng";

                data.Unit = "đơn vị Hóa đơn/Nhân viên/" + unit;

                data.Salon = salon;
                data.Title = "Năng suất nhân viên " + salon + " (" + data.Unit + ")";
                data.StylelistProductivity = serializer.Serialize(listProductivityStylelist);
                data.SkinnerProductivity = serializer.Serialize(listProductivitySkinner);
                data.Number = billServiceTotal.Count;
            }
            return serializer.Serialize(data);
        }


        [WebMethod]
        public static string ReportCustomerOfStaff(int salonId, string from, string to, string viewTime, string staffType, string topIds)
        {
            using (var db = new Solution_30shineEntities())
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                CultureInfo culture = new CultureInfo("vi-VN");
                DateTime _FromDate = Convert.ToDateTime(from, culture);
                DateTime _ToDate = _FromDate;
                if (to != "")
                {
                    _ToDate = Convert.ToDateTime(to, culture).AddDays(1);
                }
                int totalDay = (_ToDate - _FromDate).Days;
                var timeFrom = String.Format("{0:MM/dd/yyyy}", _FromDate);
                var timeTo = String.Format("{0:MM/dd/yyyy}", _ToDate);
                // var andCreatedTime = "(CreatedDate between '" + timeFrom + "' and '" + timeTo + "')";

                var andSalonId = "";
                if (salonId == 0)
                    andSalonId = "SalonId>0";
                else
                {
                    andSalonId = "SalonId='" + salonId + "'";
                }
                var selectTime = "";
                var groupBy = "";
                var orderBy = "";

                switch (viewTime)
                {
                    case "day":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month],m.[Week],m.[Day]";
                        break;
                    case "week":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month],m.[Week]";

                        break;
                    case "month":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month]";

                        break;
                    case "quarter":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter]";

                        break;
                    case "year":
                        selectTime = groupBy = orderBy = " m.[Year]";
                        break;
                }


                //var type = 1;
                var listBillOfStaff = new List<CustomerOfStaff>();
                string[] staffIds = topIds.Split(',');

                var staff = "";
                var staffName = "";
                if (staffType == "1" || staffType == "")
                {
                    staff = "Staff_Hairdresser_Id";
                    staffName = "Stylist";
                }
                else if (staffType == "2")
                {
                    staff = "Staff_HairMassage_Id";
                    staffName = "Skinner";
                }
                var list = new List<DataCountByDate>();
                foreach (string staffId in staffIds)
                {
                    var id = Convert.ToInt32(staffId);
                    var customerOfStaff = new CustomerOfStaff();
                    var sql0 = @"select COUNT(*) as [Count] 
                                ,DATEPART(YEAR, CreatedDate) AS [Year]
                                 ,DATEPART(QUARTER, CreatedDate) as [Quarter] 
                                 ,DATEPART(MONTH, CreatedDate) AS[Month] 
                                 ,DATEPART(WEEK, CreatedDate) AS[Week] 
                                 ,DATEPART(DAY, CreatedDate) as [Day]
                                 from 
                                 BillService where 
                                 (CreatedDate between @dtFrom and @dtTo) 
                                 and IsDelete !=1 and ServiceIds is not null and ServiceIds!='' and " + andSalonId +
                                 " and " + staff +
                                 @"=@staffId group by 
                                 DATEPART(YEAR, CreatedDate) 
                                 ,DATEPART(QUARTER, CreatedDate)
                                  ,DATEPART(MONTH, CreatedDate)
                                   ,DATEPART(WEEK, CreatedDate) 
                                   ,DATEPART(DAY, CreatedDate) ";


                    var sql = @"declare @dtFrom datetime, @dtTo datetime, @staffId int
                            set @dtFrom = '" + timeFrom + "'" +
                       " set @dtTo = '" + timeTo + "'" +
                       " set @staffId='" + id + "'" +
                       " select SUM(m.[Count]) as [Count], " + selectTime +
                       @" from 
                            (
                            select a1.*, Coalesce(b.[Count], 0) as [Count]
                            from
                            (
                            select 
                             DATEPART(YEAR, [DateTime]) as [Year] 
                             ,DATEPART(QUARTER, [DateTime]) as [Quarter] 
                             ,DATEPART(MONTH, [DateTime]) as[Month]
                             ,DATEPART(WEEK, [DateTime]) as[Week] 
                             ,DATEPART(DAY, [DateTime]) as [Day] 
                            from 
                            (
                             select 
                              dateadd(day, number, @dtFrom) as [DateTime]
                             from 
                              (select distinct number from master.dbo.spt_values
                               where name is null
                              ) as n
                             where dateadd(day, number, @dtFrom) < @dtTo 
                            ) as a
                            ) as a1
                            left join 
                            (" + sql0 +
                       @") as b 
                            on a1.[Year]=b.[Year] and a1.[Quarter] = b.[Quarter] and a1.[Month] = b.[Month] and a1.[Week] = b.[Week] and a1.[Day] = b.[Day]
                            )m
                            group by " + groupBy +
                       " order by " +
                       orderBy;


                    list = db.Database.SqlQuery<DataCountByDate>(sql).ToList();
                    customerOfStaff.Id = id;
                    customerOfStaff.Name = db.Staffs.Where(w => w.Id == id).Select(s => s.Fullname).FirstOrDefault().ToString();
                    customerOfStaff.CountByDate = serializer.Serialize(ReLabelDate(list, salonId, timeFrom, timeTo, viewTime));
                    var dayOff = 0;
                    foreach (DataCountByDate obj in list)
                    {
                        if (obj.Count < 3)
                            dayOff++;
                    }
                    customerOfStaff.DayOff = dayOff;
                    customerOfStaff.DayWork = totalDay - dayOff;
                    listBillOfStaff.Add(customerOfStaff);
                }


                var data = new DataTotal();
                data.CustomerOfStaff = serializer.Serialize(listBillOfStaff);

                var salon = "";
                var salonName = db.Tbl_Salon.Where(w => w.Id == salonId).Select(s => s.Name).FirstOrDefault();
                if (salonName != null)
                    salon = "tại salon " + salonName;
                else salon = "tại các Salon";

                var timeView = "";
                if (viewTime == "day")
                    timeView = "ngày";
                else if (viewTime == "week")
                    timeView = "tuần";
                else if (viewTime == "quarter")
                    timeView = "quý";
                else if (viewTime == "year")
                    timeView = "năm";
                else if (viewTime == "month")
                    timeView = "tháng";



                data.Unit = "Hóa đơn";
                data.Title = "Thống kê số hóa đơn của " + staffName + " theo " + timeView + " " + salon + " (đơn vị " + data.Unit + ")";
                data.Number = list.Count;
                return serializer.Serialize(data);
            }
        }

        public static List<CustomerOfStaff> GetCustomerOfStaff(int salonId, string from, string to, string viewTime, string staffType, List<int> staffIds)
        {
            using (var db = new Solution_30shineEntities())
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                CultureInfo culture = new CultureInfo("vi-VN");
                DateTime _FromDate = Convert.ToDateTime(from, culture);
                DateTime _ToDate = _FromDate;
                if (to != "")
                {
                    _ToDate = Convert.ToDateTime(to, culture).AddDays(1);
                }
                int totalDay = (_ToDate - _FromDate).Days;
                var timeFrom = String.Format("{0:MM/dd/yyyy}", _FromDate);
                var timeTo = String.Format("{0:MM/dd/yyyy}", _ToDate);
                // var andCreatedTime = "(CreatedDate between '" + timeFrom + "' and '" + timeTo + "')";

                var andSalonId = "";
                if (salonId == 0)
                    andSalonId = "SalonId>0";
                else
                {
                    andSalonId = "SalonId='" + salonId + "'";
                }
                var selectTime = "";
                var groupBy = "";
                var orderBy = "";

                switch (viewTime)
                {
                    case "day":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month],m.[Week],m.[Day]";
                        break;
                    case "week":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month],m.[Week]";

                        break;
                    case "month":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month]";

                        break;
                    case "quarter":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter]";

                        break;
                    case "year":
                        selectTime = groupBy = orderBy = " m.[Year]";
                        break;
                }


                //var type = 1;
                var listBillOfStaff = new List<CustomerOfStaff>();
                //string[] staffIds = Ids.Split(',');

                var staff = "";
                // var staffName = "";
                if (staffType == "1" || staffType == "")
                {
                    staff = "Staff_Hairdresser_Id";
                    //  staffName = "Stylist";
                }
                else if (staffType == "2")
                {
                    staff = "Staff_HairMassage_Id";
                    //  staffName = "Skinner";
                }
                var list = new List<DataCountByDate>();
                foreach (int staffId in staffIds)
                {
                    var id = Convert.ToInt32(staffId);
                    var customerOfStaff = new CustomerOfStaff();
                    var sql0 = @"select COUNT(*) as [Count] 
                                ,DATEPART(YEAR, CreatedDate) AS [Year]
                                 ,DATEPART(QUARTER, CreatedDate) as [Quarter] 
                                 ,DATEPART(MONTH, CreatedDate) AS[Month] 
                                 ,DATEPART(WEEK, CreatedDate) AS[Week] 
                                 ,DATEPART(DAY, CreatedDate) as [Day]
                                 from 
                                 BillService where 
                                 (CreatedDate between @dtFrom and @dtTo) 
                                 and IsDelete !=1 and ServiceIds is not null and ServiceIds!='' and " + andSalonId +
                                 " and " + staff +
                                 @"=@staffId group by 
                                 DATEPART(YEAR, CreatedDate) 
                                 ,DATEPART(QUARTER, CreatedDate)
                                  ,DATEPART(MONTH, CreatedDate)
                                   ,DATEPART(WEEK, CreatedDate) 
                                   ,DATEPART(DAY, CreatedDate) ";


                    var sql = @"declare @dtFrom datetime, @dtTo datetime, @staffId int
                            set @dtFrom = '" + timeFrom + "'" +
                       " set @dtTo = '" + timeTo + "'" +
                       " set @staffId='" + id + "'" +
                       " select SUM(m.[Count]) as [Count], " + selectTime +
                       @" from 
                            (
                            select a1.*, Coalesce(b.[Count], 0) as [Count]
                            from
                            (
                            select 
                             DATEPART(YEAR, [DateTime]) as [Year] 
                             ,DATEPART(QUARTER, [DateTime]) as [Quarter] 
                             ,DATEPART(MONTH, [DateTime]) as[Month]
                             ,DATEPART(WEEK, [DateTime]) as[Week] 
                             ,DATEPART(DAY, [DateTime]) as [Day] 
                            from 
                            (
                             select 
                              dateadd(day, number, @dtFrom) as [DateTime]
                             from 
                              (select distinct number from master.dbo.spt_values
                               where name is null
                              ) as n
                             where dateadd(day, number, @dtFrom) < @dtTo 
                            ) as a
                            ) as a1
                            left join 
                            (" + sql0 +
                       @") as b 
                            on a1.[Year]=b.[Year] and a1.[Quarter] = b.[Quarter] and a1.[Month] = b.[Month] and a1.[Week] = b.[Week] and a1.[Day] = b.[Day]
                            )m
                            group by " + groupBy +
                       " order by " +
                       orderBy;


                    list = db.Database.SqlQuery<DataCountByDate>(sql).ToList();
                    customerOfStaff.Id = id;
                    customerOfStaff.Name = db.Staffs.Where(w => w.Id == id).Select(s => s.Fullname).FirstOrDefault().ToString();
                    customerOfStaff.CountByDate = serializer.Serialize(ReLabelDate(list, salonId, timeFrom, timeTo, viewTime));
                    var dayOff = 0;
                    foreach (DataCountByDate obj in list)
                    {
                        if (obj.Count == 0)
                            dayOff++;
                    }
                    customerOfStaff.DayOff = dayOff;
                    customerOfStaff.DayWork = totalDay - dayOff;
                    listBillOfStaff.Add(customerOfStaff);
                }

                return listBillOfStaff;
            }
        }


        [WebMethod]
        public static string ReportCustomerAvgOfStaff(int salonId, string from, string to, string viewTime, string staffType)
        {
            using (var db = new Solution_30shineEntities())
            {

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                CultureInfo culture = new CultureInfo("vi-VN");
                DateTime _FromDate = Convert.ToDateTime(from, culture);
                DateTime _ToDate = _FromDate;
                if (to != "")
                {
                    _ToDate = Convert.ToDateTime(to, culture).AddDays(1);
                }
                int totalDay = (_ToDate - _FromDate).Days;
                var timeFrom = String.Format("{0:MM/dd/yyyy}", _FromDate);
                var timeTo = String.Format("{0:MM/dd/yyyy}", _ToDate);
                var andBetween = "(CreatedDate between '" + timeFrom + "' and '" + timeTo + "')";

                var andSalonId = "";
                if (salonId == 0)
                    andSalonId = "SalonId>0";
                else
                {
                    andSalonId = "SalonId='" + salonId + "'";
                }

                var staff = "";
                var staffName = "";

                if (staffType == "1" || staffType == "")
                {
                    staffType = "1";
                    staff = "Staff_Hairdresser_Id";
                    staffName = "Stylist";
                }
                else if (staffType == "2")
                {
                    staff = "Staff_HairMassage_Id";
                    staffName = "Skinner";
                }


                var sql = @"
                        select
                        a.Count as [Count]
                        ,a." + staff +
                        " as [Id] ,b.FullName as [Name] from( select COUNT(*) as [Count] ," + staff +
                        " from BillService where " + andBetween +
                        " and IsDelete !=1 and ServiceIds is not null and ServiceIds!='' and " + andSalonId + " group by " + staff +
                        ") as a inner join (select Fullname,Id from Staff where IsDelete !=1) as b on a." + staff + "=b.Id order by [Count] desc";


                var list = db.Database.SqlQuery<CustomerOfStaff>(sql).ToList();
                var listAvg = new List<CustomerOfStaff>();

                var sqlIdStaff = @"select Id from Staff where IsDelete!=1 and type=" + staffType;

                var staffIds = db.Database.SqlQuery<int>(sqlIdStaff).ToList();

                var listCustomerOfstaff = GetCustomerOfStaff(salonId, from, to, viewTime, staffType, staffIds);
                var listSort = new List<Sort>();
                foreach (CustomerOfStaff obj in list)
                {
                    foreach (CustomerOfStaff obj2 in listCustomerOfstaff)
                    {
                        if (totalDay > 0)
                        {
                            if (obj2.Id == obj.Id)
                            {
                                double avg = 0;
                                var dayOff = obj2.DayOff;
                                if (dayOff < totalDay)
                                    avg = Math.Round((double)((double)obj.Count / (double)(totalDay - dayOff)), 1);
                                else if (dayOff == 0)
                                    avg = Math.Round((double)((double)obj.Count / (double)totalDay), 1);
                                //else if (dayOff == totalDay)
                                obj.Avg = avg;
                                obj.DayOff = dayOff;
                                obj.DayWork = obj2.DayWork;

                                var sort = new Sort();
                                sort.Id = obj.Id;
                                sort.Value1 = avg;
                                listSort.Add(sort);
                            }
                        }
                        else if (totalDay == 0)
                        {
                            obj.Avg = obj.Count;
                            totalDay = 1;
                        }
                    }
                    listAvg.Add(obj);
                }
                listSort = listSort.OrderByDescending(o => o.Value1).ToList();

                var sortListAvg = new List<CustomerOfStaff>();
                foreach (Sort obj3 in listSort)
                {
                    foreach (CustomerOfStaff obj4 in listAvg)
                    {
                        if (obj3.Id == obj4.Id)
                        {
                            sortListAvg.Add(obj4);
                        }
                    }

                }
                var data = new DataTotal();
                data.CustomerAvgOfStaff = serializer.Serialize(sortListAvg);
                data.Unit = "hóa đơn/" + staffName + "/ngày làm";
                data.Title = "Thống kê trung bình số hóa đơn của " + staffName + " (" + data.Unit + ")";
                data.Number = totalDay;
                return serializer.Serialize(data);



            }
        }

                /// <summary>
        /// Báo cáo chất lượng dịch vụ 
        /// </summary>
        /// <param name="salonId"></param>
        /// <param name="staffType"></param>
        /// <param name="staffId"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="viewTime"></param>
        /// <returns></returns>
        [WebMethod]
        private static List<KeyVal> GetQuantityServey(int salonId, string staffType, string staffId, string from, string to, string viewTime)
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = new List<KeyVal>();

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                CultureInfo culture = new CultureInfo("vi-VN");
                //DateTime _FromDate = Convert.ToDateTime(from, culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                DateTime _FromDate = Convert.ToDateTime(from, culture);
                DateTime _ToDate = _FromDate;
                if (to != "")
                {
                    _ToDate = Convert.ToDateTime(to, culture).AddDays(1);
                }
                var timeFrom = String.Format("{0:MM/dd/yyyy}", _FromDate);
                var timeTo = String.Format("{0:MM/dd/yyyy}", _ToDate);

                var andSalonId = "";
                if (salonId == 0)
                    andSalonId = "SalonId>0";
                else
                {
                    andSalonId = "SalonId='" + salonId + "'";
                }

                var selectTime = "";
                var groupBy = "";
                var orderBy = "";
                switch (viewTime)
                {
                    case "day":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month],m.[Week],m.[Day]";
                        break;
                    case "week":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month],m.[Week]";

                        break;
                    case "month":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month]";

                        break;
                    case "quarter":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter]";

                        break;
                    case "year":
                        selectTime = groupBy = orderBy = " m.[Year]";
                        break;
                }
                var staff = "";
                var andStaffId = "";
                if (staffType == "1" || staffType == "")
                {
                    staff = "StylistId";

                }
                else if (staffType == "2")
                {
                    staff = "SkinnerId";
                }
                if (!String.IsNullOrEmpty(staffId))
                    andStaffId = staff + "='" + staffId + "' and ";

                var listThread = db.SocialThreads.Where(w => w.IsDelete != 1 && w.stType == 2).ToList();
                if (listThread != null && listThread.Count > 0)
                {
                    foreach (SocialThread thread in listThread)
                    {
                        var threadId = thread.Id;
                        var threadName = thread.Name;

                        var sql = @"declare @dtFrom datetime, @dtTo datetime, @staffId int, @threadId int
                            set @dtFrom = '" + timeFrom + "'" +
                                 @" set @dtTo = '" + timeTo + "'" +
                                 @" set @threadId =" + threadId +
                                  @" select Coalesce(m.[Count],'0') as [Count]," + selectTime +
                                @"from 
                            (
                            select a1.*, b.[Count] as [Count] 
                            from
                            (
                            select 
	                            DATEPART(YEAR, [DateTime]) as [Year] 
	                            ,DATEPART(QUARTER, [DateTime]) as [Quarter] 
	                            ,DATEPART(MONTH, [DateTime]) as[Month]
	                            ,DATEPART(WEEK, [DateTime]) as[Week] 
	                            ,DATEPART(DAY, [DateTime]) as [Day] 
                            from 
                            (
	                            select 
		                            dateadd(day, number, @dtFrom) as [DateTime]
	                            from 
		                            (select distinct number from master.dbo.spt_values
			                            where name is null
		                            ) as n
	                            where dateadd(day, number, @dtFrom) < @dtTo 
                            ) as a
                            ) as a1
                            left join 
                            (select 
                                                       
	                             Count(Point) as [Count]
	                             ,Thread as [Thread]
                                ,DATEPART(YEAR, CreatedDate) AS [Year]
                                    ,DATEPART(QUARTER, CreatedDate) as [Quarter] 
                                    ,DATEPART(MONTH, CreatedDate) AS[Month] 
                                    ,DATEPART(WEEK, CreatedDate) AS[Week] 
                                    ,DATEPART(DAY, CreatedDate) as [Day]
                                    from 
                                    Tbl_Staff_Survey where (CreatedDate between @dtFrom and @dtTo) 
                                    and IsDelete !=1 and " + andStaffId + andSalonId + " and Thread=@threadId" +
                                  @" group by 
                                    DATEPART(YEAR, CreatedDate) 
                                    ,DATEPART(QUARTER, CreatedDate)
	                                ,DATEPART(MONTH, CreatedDate)
	                                ,DATEPART(WEEK, CreatedDate) 
	                                ,DATEPART(DAY, CreatedDate)	
	                                ,Point
	                                ,Thread) as b 	                               	                              	                               
                            on a1.[Year]=b.[Year] and a1.[Quarter] = b.[Quarter] and a1.[Month] = b.[Month] and a1.[Week] = b.[Week] and a1.[Day] = b.[Day]
                            )m
                            group by m.[Count]," + groupBy;

                        var list0 = db.Database.SqlQuery<DataCountByDate>(sql).ToList();
                        var listRe = new List<DataCountByDate>();
                        if (list0 != null && list0.Count > 0)
                            listRe = ReLabelDate(list0, salonId, timeFrom, timeTo, viewTime);

                        var obj = new KeyVal();
                        obj.Name = threadName;
                        obj.Val = serializer.Serialize(listRe);
                        list.Add(obj);
                    }
                }
                return list;
            }
        }
        */

        //HIỆN TẠI ĐANG DÙNG NGHIỆP VỤ THIẾT KẾ TỪ THÁNG 3/2016
        //CÁC HÀM SERVICE

        /// <summary>
        /// Báo cáo tổng 
        /// Sử dụng nhiều hàm private trong Service để lấy kết quả báo cáo đơn
        /// 
        /// </summary>
        /// <param name="salonId">kiểu, salon id</param>
        /// <param name="from">thời gian từ </param>
        /// <param name="to">thời gian đến</param>
        /// <param name="viewTime">kiểu xem: day (ngày), month (tháng), year (năm) </param>
        /// <param name="staffType">kiểu nhân viên, 1 hoặc 2</param>
        /// <param name="staffId">mã nhân viên, dùng trong trường hợp xem báo cáo cho nhân viên cụ thể</param>
        /// <returns>trả về json của đối tượng DataTotal:
        /// thông tin báo cáo tổng hợp được định nghĩa trong đối tượng DataTotal
        /// </returns>
        [WebMethod]
        public static string ReportTotal(int salonId, string from, string to, string viewTime, string staffType, string staffId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var data = new DataTotal();
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                CultureInfo culture = new CultureInfo("vi-VN");
                DateTime _FromDate = Convert.ToDateTime(from, culture);
                DateTime _ToDate = _FromDate;
                if (to != "")
                {
                    _ToDate = Convert.ToDateTime(to, culture).AddDays(1);
                }
                var timeFrom = String.Format("{0:MM/dd/yyyy}", _FromDate);
                var timeTo = String.Format("{0:MM/dd/yyyy}", _ToDate);

                var andSalonId = "";
                if (salonId == 0)
                    andSalonId = "SalonId>0";
                else
                {
                    andSalonId = "SalonId='" + salonId + "'";
                }

                //sử dụng các hàm lấy ra các dữ liệu báo cáo đơn, gán kết quả cho đối tượng báo cáo tổng DataTotal
                var sales = new List<DataCountByDate>();
                var bills = new List<DataCountByDate>();

                var listSale = GetSaleOfStaff(andSalonId, staffType, staffId, timeFrom, timeTo, viewTime);
                if (listSale != null)
                    sales = ReLabelDate(listSale, salonId, timeFrom, timeTo, viewTime);
                var listBill = GetBillOfStaff(andSalonId, staffType, staffId, timeFrom, timeTo, viewTime);
                if (listBill != null)
                    bills = ReLabelDate(listBill, salonId, timeFrom, timeTo, viewTime);
                var listStaffWork = CountStaffWorkBySalon(andSalonId, staffType, timeFrom, timeTo, viewTime);
                if (listStaffWork != null)
                    data.StaffWork = serializer.Serialize(listStaffWork);

                data.Sale = serializer.Serialize(sales);
                data.Bill = serializer.Serialize(bills);
                data.Service = ReportServiceByCategory(salonId, staffType, staffId, from, to, viewTime);
                data.BillByHour = ReportBillByHours(salonId, staffType, staffId, from, to, viewTime);
                data.Customer = GetCustomerFamiliar(salonId, staffType, staffId, from, to, viewTime);

                //các đánh giá dịch vụ đặc biệt và dịch vụ thường
                var typeBillSpecial = "special";
                var typeBillNormal = "normal";

                var listRatingTotal = GetRatingPointTotal(andSalonId, staffType, staffId, "special", timeFrom, timeTo, viewTime);
                var ratingService = ReLabelDate(listRatingTotal, salonId, timeFrom, timeTo, viewTime);

                var billNotRating = GetBillRating(salonId, staffType, staffId, timeFrom, timeTo, viewTime, "", 0);

                var billNormal0 = GetBillRating(salonId, staffType, staffId, timeFrom, timeTo, viewTime, typeBillNormal, 1);
                var billNormal1 = GetBillRating(salonId, staffType, staffId, timeFrom, timeTo, viewTime, typeBillNormal, 2);
                var billNormal2 = GetBillRating(salonId, staffType, staffId, timeFrom, timeTo, viewTime, typeBillNormal, 3);

                var billSpecial0 = GetBillRating(salonId, staffType, staffId, timeFrom, timeTo, viewTime, typeBillSpecial, 1);
                var billSpecial1 = GetBillRating(salonId, staffType, staffId, timeFrom, timeTo, viewTime, typeBillSpecial, 2);
                var billSpecial2 = GetBillRating(salonId, staffType, staffId, timeFrom, timeTo, viewTime, typeBillSpecial, 3);


                data.BillNormal0 = billNormal0.ToString();
                data.BillNormal1 = billNormal1.ToString();
                data.BillNormal2 = billNormal2.ToString();
                data.BillSpecial0 = billSpecial0.ToString();
                data.BillSpecial1 = billSpecial1.ToString();
                data.BillSpecial2 = billSpecial2.ToString();
                data.BillNotRating = billNotRating.ToString();
                // đánh giá tổng, dùng cho biểu đồ
                data.RatingTotal = serializer.Serialize(ratingService);

                //khoảng cách số ngày báo cáo
                int totalDay = (_ToDate - _FromDate).Days;
                data.Number = totalDay;

                //lấy tên salon
                var salon = "";
                var salonName = db.Tbl_Salon.Where(w => w.Id == salonId).Select(s => s.Name).FirstOrDefault();

                if (salonName != null)
                    salon = salonName;
                else salon = "Các Salon";
                data.Salon = salon;

                //đơn vi của giá trị báo cáo
                if (viewTime == "day")
                    data.Unit1 = "Triệu đồng";
                else if (viewTime == "month" || viewTime == "week" || viewTime == "quarter" || viewTime == "year")
                    data.Unit1 = "Triệu đồng";
                data.Unit2 = "Hóa đơn";
                data.Unit3 = "Hóa đơn";
                data.Unit4 = "Hóa đơn";
                data.Unit5 = "%";
                data.Unit6 = "Số lượng";
                data.Unit7 = "Điểm";

                //tiêu đề mỗi báo cáo nhỏ trong báo cáo tổng, đặt tiêu đề phù hợp cho việc báo cáo theo salon hoặc theo nhân viên
                //nếu là báo cáo cho nhân viên xác định, lấy thông tin của nhân viên
                if (!String.IsNullOrEmpty(staffId))
                {
                    data.Title = "Thống kê cho Stylist";
                    var id = Convert.ToInt32(staffId);
                    data.Id = id;
                    var staff = db.Staffs.FirstOrDefault(w => w.Id == id && w.IsDelete != 1);
                    if (staff != null)
                    {
                        data.Name = staff.Fullname;
                        data.Phone = staff.Phone;
                        data.Level = staff.SkillLevel + "";
                        var sqlSalonName = @"select b.Name from
                                            (Select * from Staff where Id=" + staffId + @") as a
                                            inner join 
                                            (Select * from Tbl_Salon ) as b
                                            on a.SalonId=b.Id";
                        string salonOfStaff = db.Database.SqlQuery<string>(sqlSalonName).FirstOrDefault();
                        if (salonOfStaff != null)
                            data.Salon = salonOfStaff;
                        data.Avartar = staff.Avatar;
                    }

                    //số ngày làm việc
                    var dataTotal = GetDayWork(salonId, from, to, viewTime, staffType, id);
                    data.DayWork = dataTotal.DayWork;

                    if (staffId != "")
                        data.Title4 = "Loại khách hàng " + salonName;
                    else
                        data.Title4 = "Loại khách hàng";

                    data.Title5 = "Tỷ lệ dịch vụ";
                }
                else
                {
                    data.Id = salonId;
                    data.DayWork = totalDay;
                    data.Title = "Thống kê cho Salon";
                    data.Title4 = "Loại khách hàng ";
                }

                data.Title1 = "Doanh thu ";
                data.Title2 = "Doanh số dịch vụ";
                data.Title3 = "Tổng Hóa đơn theo giờ trong " + data.DayWork + " ngày làm";
                data.Title5 = "Tỷ lệ dịch vụ";
                data.Title6 = "Chất lượng dịch vụ ";
                data.Title7 = "Điểm Hài Lòng ";

                //trả ra kế quả json báo cáo tổng hợp
                return serializer.Serialize(data);
            }
        }
        /// <summary>
        /// Lấy khoảng cách thời gian 
        /// </summary>
        /// <param name="salonId"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="viewTime"></param>
        /// <returns></returns>


        /// <summary>
        /// Báo cáo hóa đơn theo giờ trong ngày
        /// </summary>
        /// <param name="salonId">mã salon</param>
        /// <param name="staffType">kiểu nhân viên</param>
        /// <param name="staffId">mã nhân viên</param>
        /// <param name="from">thời gian từ</param>
        /// <param name="to">thời gian đến</param>
        /// <param name="viewTime">kiểu xem</param>
        /// <returns>Trả về json đối tượng DataBillByHours chứa thông tin hóa đơn theo giờ</returns>
        [WebMethod]
        public static string ReportBillByHours(int salonId, string staffType, string staffId, string from, string to, string viewTime)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var data = new DataBillByHours();
            using (var db = new Solution_30shineEntities())
            {
                string where = "";
                var sql = "";

                CultureInfo culture = new CultureInfo("vi-VN");
                DateTime _FromDate = Convert.ToDateTime(from, culture);
                DateTime _ToDate = _FromDate;
                if (to != "")
                {
                    _ToDate = Convert.ToDateTime(to, culture).AddDays(1);
                }
                var timeFrom = String.Format("{0:MM/dd/yyyy}", _FromDate);
                var timeTo = String.Format("{0:MM/dd/yyyy}", _ToDate);

                if (timeFrom != "" && timeTo != "")
                {
                    where += " and (a.CreatedDate between '" + timeFrom + "' and '" + timeTo + "') ";
                }
                if (salonId > 0)
                {
                    where += " and (a.SalonId = " + salonId + ") ";
                }


                var staff = "";
                var staffName = "";
                var andStaffId = "";
                if (staffType == "1" || staffType == "")
                {
                    staff = "Staff_Hairdresser_Id";
                    staffName = "Stylist";
                }
                else if (staffType == "2")
                {
                    staff = "Staff_HairMassage_Id";
                    staffName = "Skinner";
                }
                if (!String.IsNullOrEmpty(staffId))
                    andStaffId = staff + "='" + staffId + "' and ";

                sql = @"select COUNT(d.Id) as bills,DATEPART(YEAR, d.CreatedDate) as [year],DATEPART(MONTH, d.CreatedDate) as [month],DATEPART(DAY, d.CreatedDate) as [day], DATEPART(HOUR, d.CreatedDate) as [hour]
                        from
                        (
                        select a.Id, a.CustomerCode, b.Fullname, a.CreatedDate 
                        from BillService as a
                        join Customer as b
                        on a.CustomerCode = b.Customer_Code
                        where " + andStaffId +
                        @"--(a.CustomerCode != '' and a.CustomerCode is not null) 
                        --and 
                        a.IsDelete != 1 and b.IsDelete != 1
                        and a.Pending != 1
                        and (a.ServiceIds is not null and a.ServiceIds != '')" +
                    where +
                    @") as d
                        group by 
                        DATEPART(YEAR, d.CreatedDate), 
                        DATEPART(MONTH, d.CreatedDate),DATEPART(DAY, d.CreatedDate), DATEPART(HOUR, d.CreatedDate)
                        order by
                        DATEPART(YEAR, d.CreatedDate) desc, 
                        DATEPART(MONTH, d.CreatedDate) desc,
                        DATEPART(DAY, d.CreatedDate) desc, 
                        DATEPART(HOUR, d.CreatedDate) desc";

                var lst = db.Database.SqlQuery<DateInfo>(sql).ToList();
                var lst2 = new List<DateInfo>();
                var hourFrame = new HourInfo();
                var hourTotal = new HourInfo();
                var hourFrameLST = new List<HourInfo>();

                for (var i = 0; i < lst.Count; i++)
                {

                    hourFrame = new HourInfo();
                    lst2 = lst.Where(w => w.year == _FromDate.Year && w.month == _FromDate.Month && w.day == _FromDate.Day).ToList();
                    if (lst2.Count > 0)
                    {
                        if (_FromDate < _ToDate)
                        {
                            foreach (var v in lst2)
                            {
                                switch (v.hour)
                                {
                                    case 22:
                                        hourFrame.h_22 = v.bills;
                                        hourTotal.h_22 += v.bills;
                                        break;
                                    case 21:
                                        hourFrame.h_21 = v.bills;
                                        hourTotal.h_21 += v.bills;
                                        break;
                                    case 20:
                                        hourFrame.h_20 = v.bills;
                                        hourTotal.h_20 += v.bills;
                                        break;
                                    case 19:
                                        hourFrame.h_19 = v.bills;
                                        hourTotal.h_19 += v.bills;
                                        break;
                                    case 18:
                                        hourFrame.h_18 = v.bills;
                                        hourTotal.h_18 += v.bills;
                                        break;
                                    case 17:
                                        hourFrame.h_17 = v.bills;
                                        hourTotal.h_17 += v.bills;
                                        break;
                                    case 16:
                                        hourFrame.h_16 = v.bills;
                                        hourTotal.h_16 += v.bills;
                                        break;
                                    case 15:
                                        hourFrame.h_15 = v.bills;
                                        hourTotal.h_15 += v.bills;
                                        break;
                                    case 14:
                                        hourFrame.h_14 = v.bills;
                                        hourTotal.h_14 += v.bills;
                                        break;
                                    case 13:
                                        hourFrame.h_13 = v.bills;
                                        hourTotal.h_13 += v.bills;
                                        break;
                                    case 12:
                                        hourFrame.h_12 = v.bills;
                                        hourTotal.h_12 += v.bills;
                                        break;
                                    case 11:
                                        hourFrame.h_11 = v.bills;
                                        hourTotal.h_11 += v.bills;
                                        break;
                                    case 10:
                                        hourFrame.h_10 = v.bills;
                                        hourTotal.h_10 += v.bills;
                                        break;
                                    case 9:
                                        hourFrame.h_9 = v.bills;
                                        hourTotal.h_9 += v.bills;
                                        break;
                                    case 8:
                                        hourFrame.h_8 = v.bills;
                                        hourTotal.h_8 += v.bills;
                                        break;
                                }

                            }
                        }

                        hourFrame.CreadtedDate = _FromDate.Day.ToString() + "/" + _FromDate.Month.ToString() + "/" + _FromDate.Year.ToString();
                        hourFrameLST.Add(hourFrame);
                        _FromDate = _FromDate.AddDays(1);
                    }
                }
                var salon = "";
                var salonName = db.Tbl_Salon.Where(w => w.Id == salonId).Select(s => s.Name).FirstOrDefault();
                if (salonName != null)
                    salon = "tại salon " + salonName;
                else salon = "tại các Salon";

                data.Unit = "Hóa đơn";
                data.Title = "Thống kê hóa đơn theo giờ " + salon + " (đơn vị " + data.Unit + ")";
                data.Hours = serializer.Serialize(hourTotal);

            }

            return serializer.Serialize(data);
        }

        /// <summary>
        /// Báo cáo dịch vụ theo phân loại dịch vụ
        /// </summary>
        /// <param name="salonId"></param>
        /// <param name="staffType"></param>
        /// <param name="staffId"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="viewTime"></param>
        /// <returns>Trả về json đối tượng DataService chứa thông tin thống kê theo loại dịch vụ</returns>
        [WebMethod]
        public static string ReportServiceByCategory(int salonId, string staffType, string staffId, string from, string to, string viewTime)
        {
            using (var db = new Solution_30shineEntities())
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                CultureInfo culture = new CultureInfo("vi-VN");
                DateTime _FromDate = Convert.ToDateTime(from, culture);
                DateTime _ToDate = _FromDate;
                if (to != "")
                {
                    _ToDate = Convert.ToDateTime(to, culture).AddDays(1);
                }
                var timeFrom = String.Format("{0:MM/dd/yyyy}", _FromDate);
                var timeTo = String.Format("{0:MM/dd/yyyy}", _ToDate);
                var andSalonId = "";
                if (salonId == 0)
                    andSalonId = "SalonId>0";
                else
                {
                    andSalonId = "SalonId='" + salonId + "'";
                }

                var staff = "";
                var staffName = "";
                var andStaffId = "";
                if (staffType == "1" || staffType == "")
                {
                    staff = "Staff_Hairdresser_Id";
                    staffName = "Stylist";
                }
                else if (staffType == "2")
                {
                    staff = "Staff_HairMassage_Id";
                    staffName = "Skinner";
                }
                if (!String.IsNullOrEmpty(staffId))
                    andStaffId = staff + "='" + staffId + "' and ";

                // lấy các dịch vụ theo phân loại: ShineMaster, ShineMaster,ShineQuick,CutStyle,HairRemove,ComboU,ComboC
                var shineMaster = GetServiceByCategory(andSalonId, andStaffId, timeFrom, timeTo, "ShineMaster");
                var shineCombo = GetServiceByCategory(andSalonId, andStaffId, timeFrom, timeTo, "ShineCombo");
                var shineQuick = GetServiceByCategory(andSalonId, andStaffId, timeFrom, timeTo, "ShineQuick");
                var cutStyle = GetServiceByCategory(andSalonId, andStaffId, timeFrom, timeTo, "CutStyle");
                var hairRemove = GetServiceByCategory(andSalonId, andStaffId, timeFrom, timeTo, "HairRemove");
                var comboU = GetServiceByCategory(andSalonId, andStaffId, timeFrom, timeTo, "ComboU");
                var comboC = GetServiceByCategory(andSalonId, andStaffId, timeFrom, timeTo, "ComboC");

                var data = new DataService();
                data.ShineMaster = serializer.Serialize(shineMaster);
                data.ShineCombo = serializer.Serialize(shineCombo);
                data.ShineQuick = serializer.Serialize(shineQuick);
                data.CutStyle = serializer.Serialize(cutStyle);
                data.HairRemove = serializer.Serialize(hairRemove);
                data.ComboU = serializer.Serialize(comboU);
                data.ComboC = serializer.Serialize(comboC);

                var salon = "tại các Salon";
                var salonName = db.Tbl_Salon.Where(w => w.Id == salonId).Select(s => s.Name).FirstOrDefault();
                if (salonName != null)
                    salon = "tại salon " + salonName;


                data.Unit = "%";
                data.Title = "Biểu đồ tỉ lệ dịch vụ " + salon + "(đơn vị " + data.Unit + ")";

                return serializer.Serialize(data);
            }
        }

        /// <summary>
        /// thông kê khách quen 
        /// nếu chỉ có 2 lần khách hàng đến: kiểm tra 1 là lần đang xét, lần 2 là lần trước đó
        /// nếu trên 2 lần khách hàng đến: kiểm tra trước đó có trên 2 lần khách hàng đến
        /// </summary>
        /// <param name="salonId"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="viewTime"></param>
        /// <param name="staffType"></param>
        /// <param name="topIds"></param>
        /// <returns>json đối tượng DataTotal chứa thông tin thống kê </returns>
        [WebMethod]
        public static string ReportCustomerFamiliar2(int salonId, string from, string to, string viewTime, string staffType, string topIds)
        {
            using (var db = new Solution_30shineEntities())
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                CultureInfo culture = new CultureInfo("vi-VN");
                DateTime _FromDate = Convert.ToDateTime(from, culture);
                DateTime _ToDate = _FromDate;

                //kiểm tra thời gian from to, salonId, viewTime, staffType, topsId để viết câu sql truy vấn dữ liệu trong bảng BillService
                //chuyển đổi định dạng MM/dd/yyyy cho thời gian nhập vào 
                if (to != "")
                {
                    _ToDate = Convert.ToDateTime(to, culture).AddDays(1);
                }
                int totalDay = (_ToDate - _FromDate).Days;
                var timeFrom = String.Format("{0:MM/dd/yyyy}", _FromDate);
                var timeTo = String.Format("{0:MM/dd/yyyy}", _ToDate);

                //kiểm tra mã salon
                var andSalonId = "";
                if (salonId == 0)
                    andSalonId = "SalonId>0";
                else
                {
                    andSalonId = "SalonId='" + salonId + "'";
                }
                var selectTime = "";
                var groupBy = "";
                var orderBy = "";

                // kiểm tra kiểu xem
                switch (viewTime)
                {
                    case "day":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month],m.[Week],m.[Day]";
                        break;
                    case "week":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month],m.[Week]";

                        break;
                    case "month":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month]";

                        break;
                    case "quarter":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter]";

                        break;
                    case "year":
                        selectTime = groupBy = orderBy = " m.[Year]";
                        break;
                }

                var listCustomerFamiliar = new List<CustomerOfStaff>();
                string[] staffIds = topIds.Split(',');

                var staff = "";
                var staffName = "";
                if (staffType == "1" || staffType == "")
                {
                    staff = "Staff_Hairdresser_Id";
                    staffName = "Stylist";
                }
                else if (staffType == "2")
                {
                    staff = "Staff_HairMassage_Id";
                    staffName = "Skinner";
                }
                var listStaff = new List<int>();
                for (int i = 0; i < staffIds.Length; ++i)
                {
                    var id = Convert.ToInt32(staffIds[i]);
                    listStaff.Add(id);
                }

                var listCustomerCode = new List<DataCountByDate>();
                foreach (string staffId in staffIds)
                {

                    var andStaffId = "";
                    if (staffType == "1" || staffType == "")
                    {
                        staff = "Staff_Hairdresser_Id";
                        staffName = "Stylist";
                    }
                    else if (staffType == "2")
                    {
                        staff = "Staff_HairMassage_Id";
                        staffName = "Skinner";
                    }
                    if (!String.IsNullOrEmpty(staffId))
                        andStaffId = staff + "='" + staffId + "' and ";


                    var customerOfStaff = new CustomerOfStaff();

                    // sql time range
                    var sqlTimeRange = @"declare @dtFrom datetime, @dtTo datetime
                                        set @dtFrom = '" + timeFrom + "'" +
                                      " set @dtTo = '" + timeTo + "'" +
                                      @" select
                                        DATEPART(YEAR, [DateTime]) as [Year]
                                        ,DATEPART(QUARTER, [DateTime]) as [Quarter]
                                        ,DATEPART(MONTH, [DateTime]) as[Month]
                                        ,DATEPART(WEEK, [DateTime]) as[Week]
                                        ,DATEPART(DAY, [DateTime]) as [Day]
                                        from(
                                        select
                                            dateadd(day, number, @dtFrom) as [DateTime]
                                        from
                                            (select distinct number from master.dbo.spt_values
                                             where name is null
                                            ) as n
                                        where dateadd(day, number, @dtFrom) < @dtTo
                                        )a";
                    // list time range
                    var listTimeRange = db.Database.SqlQuery<DataCountByDate>(sqlTimeRange).ToList();

                    var id = Convert.ToInt32(staffId);
                    var sql0 = @"select 
                             CustomerCode as [Code]
                                ,DATEPART(YEAR, CreatedDate) AS [Year]
                                    ,DATEPART(QUARTER, CreatedDate) as [Quarter] 
                                    ,DATEPART(MONTH, CreatedDate) AS[Month] 
                                    ,DATEPART(WEEK, CreatedDate) AS[Week] 
                                    ,DATEPART(DAY, CreatedDate) as [Day]
                                    from 
                                    BillService where (CreatedDate between @dtFrom and @dtTo) 
                                    and IsDelete !=1 and ServiceIds is not null and ServiceIds!='' and Pending!=1
                                    and " + andSalonId +
                                    @" and Staff_Hairdresser_Id=@staffId and CustomerCode!='' and CustomerCode is not null 
                                group by 
                                    DATEPART(YEAR, CreatedDate) 
                                    ,DATEPART(QUARTER, CreatedDate)
                                 ,DATEPART(MONTH, CreatedDate)
                                 ,DATEPART(WEEK, CreatedDate) 
                                 ,DATEPART(DAY, CreatedDate)	
                                 ,CustomerCode";


                    var sqlListCustomerCode = @"declare @dtFrom datetime, @dtTo datetime, @staffId int
                            set @dtFrom = '" + timeFrom + "'" +
                       " set @dtTo = '" + timeTo + "'" +
                       " set @staffId='" + id + "'" +
                       " select Coalesce(m.[Code],'0') as [Code], " + selectTime +
                       @" from 
                            (
                            select a1.*, b.[Code] as [Code]
                            from
                            (
                            select 
                             DATEPART(YEAR, [DateTime]) as [Year] 
                             ,DATEPART(QUARTER, [DateTime]) as [Quarter] 
                             ,DATEPART(MONTH, [DateTime]) as[Month]
                             ,DATEPART(WEEK, [DateTime]) as[Week] 
                             ,DATEPART(DAY, [DateTime]) as [Day] 
                            from 
                            (
                             select 
                              dateadd(day, number, @dtFrom) as [DateTime]
                             from 
                              (select distinct number from master.dbo.spt_values
                               where name is null
                              ) as n
                             where dateadd(day, number, @dtFrom) < @dtTo 
                            ) as a
                            ) as a1
                            left join 
                            (" + sql0 +
                       @") as b 
                            on a1.[Year]=b.[Year] and a1.[Quarter] = b.[Quarter] and a1.[Month] = b.[Month] and a1.[Week] = b.[Week] and a1.[Day] = b.[Day]
                            )m
                            group by " + groupBy +
                       " ,m.[Code] order by " +
                       orderBy;

                    listCustomerCode = db.Database.SqlQuery<DataCountByDate>(sqlListCustomerCode).ToList();

                    customerOfStaff.Id = id;
                    customerOfStaff.Name = db.Staffs.Where(w => w.Id == id).Select(s => s.Fullname).FirstOrDefault().ToString();

                    var listCountByDate = new List<DataCountByDate>();
                    for (int i = 0; i < listTimeRange.Count; ++i)
                    {
                        var countCustomerInDay = 0;
                        var countCustomerFamiliar = 0;
                        var countCustomerNew = 0;
                        var year = listTimeRange[i].Year;
                        var month = listTimeRange[i].Month;
                        var day = listTimeRange[i].Day;

                        for (int j = 0; j < listCustomerCode.Count; ++j)
                        {
                            var obj = listCustomerCode[j];

                            if (year == obj.Year && month == obj.Month && day == obj.Day)
                            {
                                countCustomerInDay++;
                                var timeLine = obj.Year + "/" + obj.Month + "/" + obj.Day;

                                var sqlCountCustomerGoAgain = @"select 
                                                 Count(CustomerCode) as [Count]
                                                    ,DATEPART(YEAR, CreatedDate) AS [Year]
                                                     ,DATEPART(QUARTER, CreatedDate) as [Quarter] 
                                                     ,DATEPART(MONTH, CreatedDate) AS[Month] 
                                                     ,DATEPART(WEEK, CreatedDate) AS[Week] 
                                                     ,DATEPART(DAY, CreatedDate) as [Day]
                                                     from 
                                                     BillService where 
                                                     (CreatedDate < '" + timeLine +
                                                             @"') and IsDelete !=1 and ServiceIds is not null and ServiceIds!='' and Pending!=1
                                                     and " + andSalonId +
                                                     @" and CustomerCode='" + obj.Code +
                                                            @"' group by     
                                                     DATEPART(YEAR, CreatedDate) 
                                                     ,DATEPART(QUARTER, CreatedDate)
                                                  ,DATEPART(MONTH, CreatedDate)
                                                  ,DATEPART(WEEK, CreatedDate) 
                                                  ,DATEPART(DAY, CreatedDate)	
                                                 ,CustomerCode";

                                var listCountCustomerGoAgain = db.Database.SqlQuery<DataCountByDate>(sqlCountCustomerGoAgain).ToList();

                                if (listCountCustomerGoAgain.Count > 0)
                                {
                                    var sqlCountCustomerOld = @"select 
                                                 Count(CustomerCode) as [Count]
                                                    ,DATEPART(YEAR, CreatedDate) AS [Year]
                                                     ,DATEPART(QUARTER, CreatedDate) as [Quarter] 
                                                     ,DATEPART(MONTH, CreatedDate) AS[Month] 
                                                     ,DATEPART(WEEK, CreatedDate) AS[Week] 
                                                     ,DATEPART(DAY, CreatedDate) as [Day]
                                                     from 
                                                     BillService where 
                                                     (CreatedDate < '" + timeLine +
                                                                     @"') and IsDelete !=1 and ServiceIds is not null and ServiceIds!='' and Pending!=1
                                                     and " + andSalonId +
                                                                     @" and Staff_Hairdresser_Id=" + id +
                                                             @" and CustomerCode='" + obj.Code +
                                                                    @"' group by     
                                                     DATEPART(YEAR, CreatedDate) 
                                                     ,DATEPART(QUARTER, CreatedDate)
                                                  ,DATEPART(MONTH, CreatedDate)
                                                  ,DATEPART(WEEK, CreatedDate) 
                                                  ,DATEPART(DAY, CreatedDate)	
                                                 ,CustomerCode";

                                    var listCountCustomerOld = db.Database.SqlQuery<DataCountByDate>(sqlCountCustomerOld).ToList();

                                    if (listCountCustomerOld != null && listCountCustomerOld.Count > 0)
                                    {
                                        var count = listCountCustomerOld.Count;
                                        if (listCountCustomerGoAgain.Count == 1)
                                        {
                                            if (count > 0)
                                            {
                                                countCustomerFamiliar++;
                                            }
                                        }
                                        else if (listCountCustomerGoAgain.Count > 1)
                                        {
                                            if (count > 1)
                                            {
                                                countCustomerFamiliar++;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    countCustomerNew++;
                                }

                            }
                        }
                        var countByDate = new DataCountByDate();

                        countByDate.Count = countCustomerFamiliar;
                        countByDate.Count2 = countCustomerNew;
                        //customer old
                        countByDate.Count3 = countCustomerInDay - countCustomerFamiliar - countCustomerNew;
                        countByDate.Year = year;
                        countByDate.Month = month;
                        countByDate.Day = day;
                        listCountByDate.Add(countByDate);
                        //customerOfStaff.CustomerFamiliar = serializer.Serialize(countByDate);
                    }
                    customerOfStaff.CountByDate = serializer.Serialize(ReLabelDate(listCountByDate, salonId, timeFrom, timeTo, viewTime));

                    listCustomerFamiliar.Add(customerOfStaff);
                }


                var data = new DataTotal();

                data.CustomerOfStaff = serializer.Serialize(listCustomerFamiliar);
                var salon = "";
                var salonName = db.Tbl_Salon.Where(w => w.Id == salonId).Select(s => s.Name).FirstOrDefault();
                if (salonName != null)
                    salon = "tại salon " + salonName;
                else salon = "tại các Salon";
                var timeView = "";
                if (viewTime == "day")
                    timeView = "ngày";
                else if (viewTime == "week")
                    timeView = "tuần";
                else if (viewTime == "quarter")
                    timeView = "quý";
                else if (viewTime == "year")
                    timeView = "năm";
                else if (viewTime == "month")
                    timeView = "tháng";



                data.Unit = "khách";
                data.Title = "Thống kê khách quen của " + staffName + " theo " + timeView + " " + salon + " (đơn vị " + data.Unit + ")";
                data.Number = totalDay;
                return serializer.Serialize(data);
            }
        }

        [WebMethod]
        public static string GetDateTimeRange(int salonId, string from, string to, string viewTime)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var listTimeRange = GetTimeRange(salonId, from, to, viewTime);
            return serializer.Serialize(listTimeRange);

        }

        /// <summary>
        /// Thống kê khách hàng quen, khách hàng cũ của nhân viên
        /// </summary>
        /// <param name="salonId"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="viewTime"></param>
        /// <param name="staffType"></param>
        /// <param name="staffIds"></param>
        /// <returns></returns>
        [WebMethod]
        public static string GetCustomerFamiliarForStaff(int salonId, string from, string to, string viewTime, string staffType, List<int> staffIds)
        {
            using (var db = new Solution_30shineEntities())
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                CultureInfo culture = new CultureInfo("vi-VN");
                DateTime _FromDate = Convert.ToDateTime(from, culture);
                DateTime _ToDate = _FromDate;
                if (to != "")
                {
                    _ToDate = Convert.ToDateTime(to, culture).AddDays(1);
                }
                int totalDay = (_ToDate - _FromDate).Days;
                var timeFrom = String.Format("{0:MM/dd/yyyy}", _FromDate);
                var timeTo = String.Format("{0:MM/dd/yyyy}", _ToDate);

                //kiểm tra các thông tin đầu vào để viết câu truy vấn với điều kiện, sắp xếp và lựa chọn.
                var andSalonId = "";
                if (salonId == 0)
                    andSalonId = "SalonId>0";
                else
                {
                    andSalonId = "SalonId='" + salonId + "'";
                }

                var selectTime = "";
                var groupBy = "";
                var orderBy = "";

                switch (viewTime)
                {
                    case "day":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month],m.[Week],m.[Day]";
                        break;
                    case "week":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month],m.[Week]";
                        break;
                    case "month":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month]";
                        break;
                    case "quarter":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter]";
                        break;
                    case "year":
                        selectTime = groupBy = orderBy = " m.[Year]";
                        break;
                }

                var listCustomerFamiliar = new List<CustomerOfStaff>();

                var staff = "";
                var staffName = "";
                if (staffType == "1" || staffType == "")
                {
                    staff = "Staff_Hairdresser_Id";
                    staffName = "Stylist";
                }
                else if (staffType == "2")
                {
                    staff = "Staff_HairMassage_Id";
                    staffName = "Skinner";
                }
                var listStaff = new List<int>();
                for (int i = 0; i < staffIds.Count; ++i)
                {
                    var id = Convert.ToInt32(staffIds[i]);
                    listStaff.Add(id);
                }

                // câu truy vấn lấy khoảng thời gian để kiểm tra loại khách
                var sqlTimeRange = @"declare @dtFrom datetime, @dtTo datetime
                                        set @dtFrom = '" + timeFrom + "'" +
                                  " set @dtTo = '" + timeTo + "'" +
                                  @" select
                                        DATEPART(YEAR, [DateTime]) as [Year]
                                        ,DATEPART(QUARTER, [DateTime]) as [Quarter]
                                        ,DATEPART(MONTH, [DateTime]) as[Month]
                                        ,DATEPART(WEEK, [DateTime]) as[Week]
                                        ,DATEPART(DAY, [DateTime]) as [Day]
                                        from(
                                        select
                                            dateadd(day, number, @dtFrom) as [DateTime]
                                        from
                                            (select distinct number from master.dbo.spt_values
                                             where name is null
                                            ) as n
                                        where dateadd(day, number, @dtFrom) < @dtTo
                                        )a";
                // lấy ra danh sách khách trong khoảng thời gian kiểm tra
                var listTimeRange = db.Database.SqlQuery<DataCountByDate>(sqlTimeRange).ToList();
                var listCustomerCode = new List<DataCountByDate>();

                // kiểm tra từng nhân viên xem thuộc loại khách gì
                foreach (int staffId in staffIds)
                {

                    var andStaffId = "";
                    if (staffType == "1" || staffType == "")
                    {
                        staff = "Staff_Hairdresser_Id";
                        staffName = "Stylist";
                    }
                    else if (staffType == "2")
                    {
                        staff = "Staff_HairMassage_Id";
                        staffName = "Skinner";
                    }
                    // if (!String.IsNullOrEmpty(staffId))
                    andStaffId = staff + "='" + staffId + "' and ";

                    // sql0 truy vấn tất cả các bill của mã khách hàng đang được kiểm tra
                    var customerOfStaff = new CustomerOfStaff();
                    var id = staffId;
                    var sql0 = @"select 
                             CustomerCode as [Code]
                                ,DATEPART(YEAR, CreatedDate) AS [Year]
                                    ,DATEPART(QUARTER, CreatedDate) as [Quarter] 
                                    ,DATEPART(MONTH, CreatedDate) AS[Month] 
                                    ,DATEPART(WEEK, CreatedDate) AS[Week] 
                                    ,DATEPART(DAY, CreatedDate) as [Day]
                                    from 
                                    BillService where (CreatedDate between @dtFrom and @dtTo) 
                                    and IsDelete !=1 and ServiceIds is not null and ServiceIds!='' and Pending!=1
                                    and " + andSalonId +
                                    @" and Staff_Hairdresser_Id=@staffId and CustomerCode!='' and CustomerCode is not null 
                                group by 
                                    DATEPART(YEAR, CreatedDate) 
                                    ,DATEPART(QUARTER, CreatedDate)
                                 ,DATEPART(MONTH, CreatedDate)
                                 ,DATEPART(WEEK, CreatedDate) 
                                 ,DATEPART(DAY, CreatedDate)	
                                 ,CustomerCode";

                    //sqlListCustomerCode truy vấn tất cả các mã khách hàng đang được kiểm tra
                    //kết hợp với bảng thời gian hệ thống để tránh trường hợp mã khách hàng không xuất hiện vào ngày kiểm tra thì vẫn có dòng thống kê = 0 
                    var sqlListCustomerCode = @"declare @dtFrom datetime, @dtTo datetime, @staffId int
                            set @dtFrom = '" + timeFrom + "'" +
                       " set @dtTo = '" + timeTo + "'" +
                       " set @staffId='" + id + "'" +
                       " select Coalesce(m.[Code],'0') as [Code], " + selectTime +
                       @" from 
                            (
                            select a1.*, b.[Code] as [Code]
                            from
                            (
                            select 
                             DATEPART(YEAR, [DateTime]) as [Year] 
                             ,DATEPART(QUARTER, [DateTime]) as [Quarter] 
                             ,DATEPART(MONTH, [DateTime]) as[Month]
                             ,DATEPART(WEEK, [DateTime]) as[Week] 
                             ,DATEPART(DAY, [DateTime]) as [Day] 
                            from 
                            (
                             select 
                              dateadd(day, number, @dtFrom) as [DateTime]
                             from 
                              (select distinct number from master.dbo.spt_values
                               where name is null
                              ) as n
                             where dateadd(day, number, @dtFrom) < @dtTo 
                            ) as a
                            ) as a1
                            left join 
                            (" + sql0 +
                       @") as b 
                            on a1.[Year]=b.[Year] and a1.[Quarter] = b.[Quarter] and a1.[Month] = b.[Month] and a1.[Week] = b.[Week] and a1.[Day] = b.[Day]
                            )m
                            group by " + groupBy +
                       " ,m.[Code] order by " +
                       orderBy;

                    listCustomerCode = db.Database.SqlQuery<DataCountByDate>(sqlListCustomerCode).ToList();

                    customerOfStaff.Id = id;
                    customerOfStaff.Name = db.Staffs.Where(w => w.Id == id).Select(s => s.Fullname).FirstOrDefault().ToString();

                    // kiểm tra từng khách hàng trong khoảng thời gian
                    var listCountByDate = new List<DataCountByDate>();
                    for (int i = 0; i < listTimeRange.Count; ++i)
                    {
                        var countCustomerInDay = 0;
                        var countCustomerFamiliar = 0;
                        var countCustomerNew = 0;
                        var year = listTimeRange[i].Year;
                        var month = listTimeRange[i].Month;
                        var day = listTimeRange[i].Day;

                        //kiểm tra từng mã khách hàng
                        for (int j = 0; j < listCustomerCode.Count; ++j)
                        {
                            var obj = listCustomerCode[j];

                            //nếu trùng thời gian kiểm tra thì tiếp tục truy vấn khách quen
                            if (year == obj.Year && month == obj.Month && day == obj.Day)
                            {
                                countCustomerInDay++;
                                var timeLine = obj.Year + "/" + obj.Month + "/" + obj.Day;

                                //câu truy vấn khách quen
                                // định nghĩa khách quen dựa vào số lần khách quay trở lại được cắt bởi 1 stylist xác đinh, tính từ mốc thời gian kiểm tra, khách quen là: 
                                //1. khách quay trở lại trước mốc thời gian kiểm tra chỉ 1 lần nhưng lần đó phải là cắt stylist đang xét
                                //2. khách quay trở lại trước mốc thời gian kiểm tra trên 2 lần nhưng các lần đó phải có trên 1 lần là cắt bởi stylist đang xét 
                                var sqlCountCustomerGoAgain = @"select 
                                                 Count(CustomerCode) as [Count]
                                                    ,DATEPART(YEAR, CreatedDate) AS [Year]
                                                     ,DATEPART(QUARTER, CreatedDate) as [Quarter] 
                                                     ,DATEPART(MONTH, CreatedDate) AS[Month] 
                                                     ,DATEPART(WEEK, CreatedDate) AS[Week] 
                                                     ,DATEPART(DAY, CreatedDate) as [Day]
                                                     from 
                                                     BillService where 
                                                     (CreatedDate < '" + timeLine +
                                                             @"') and IsDelete !=1 and ServiceIds is not null and ServiceIds!='' and Pending!=1
                                                     and " + andSalonId +
                                                     @" and CustomerCode='" + obj.Code +
                                                            @"' group by     
                                                     DATEPART(YEAR, CreatedDate) 
                                                     ,DATEPART(QUARTER, CreatedDate)
                                                  ,DATEPART(MONTH, CreatedDate)
                                                  ,DATEPART(WEEK, CreatedDate) 
                                                  ,DATEPART(DAY, CreatedDate)	
                                                 ,CustomerCode";

                                var listCountCustomerGoAgain = db.Database.SqlQuery<DataCountByDate>(sqlCountCustomerGoAgain).ToList();

                                // nếu khách hàng đó có quay lại thì kiểm tra tiếp xem có phải là khách cũ                              
                                // nếu không phải khách quen thì là khách mới
                                if (listCountCustomerGoAgain.Count > 0)
                                {
                                    // truy vấn khách cũ của stylist xác định
                                    var sqlCountCustomerOld = @"select 
                                                 Count(CustomerCode) as [Count]
                                                    ,DATEPART(YEAR, CreatedDate) AS [Year]
                                                     ,DATEPART(QUARTER, CreatedDate) as [Quarter] 
                                                     ,DATEPART(MONTH, CreatedDate) AS[Month] 
                                                     ,DATEPART(WEEK, CreatedDate) AS[Week] 
                                                     ,DATEPART(DAY, CreatedDate) as [Day]
                                                     from 
                                                     BillService where 
                                                     (CreatedDate < '" + timeLine +
                                                                     @"') and IsDelete !=1 and ServiceIds is not null and ServiceIds!='' and Pending!=1
                                                     and " + andSalonId +
                                                                     @" and Staff_Hairdresser_Id=" + id +
                                                             @" and CustomerCode='" + obj.Code +
                                                                    @"' group by     
                                                     DATEPART(YEAR, CreatedDate) 
                                                     ,DATEPART(QUARTER, CreatedDate)
                                                  ,DATEPART(MONTH, CreatedDate)
                                                  ,DATEPART(WEEK, CreatedDate) 
                                                  ,DATEPART(DAY, CreatedDate)	
                                                 ,CustomerCode";

                                    var listCountCustomerOld = db.Database.SqlQuery<DataCountByDate>(sqlCountCustomerOld).ToList();

                                    // kiểm tra liệu có phải là khách quen của stylist
                                    if (listCountCustomerOld != null && listCountCustomerOld.Count > 0)
                                    {
                                        var count = listCountCustomerOld.Count;
                                        // nếu khách quay lại chỉ 1 lần
                                        // kiểm tra tiếp nếu là khách cũ thì đếm thêm vào loại khách cũ (khách cũ như định nghĩa ở trên)
                                        if (listCountCustomerGoAgain.Count == 1)
                                        {
                                            if (count > 0)
                                            {
                                                countCustomerFamiliar++;
                                            }
                                        }
                                        else if (listCountCustomerGoAgain.Count > 1)
                                        {
                                            if (count > 1)
                                            {
                                                countCustomerFamiliar++;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    countCustomerNew++;
                                }

                            }
                        }

                        var countByDate = new DataCountByDate();
                        countByDate.Count = countCustomerFamiliar;
                        countByDate.Count2 = countCustomerNew;
                        //khách cũ
                        countByDate.Count3 = countCustomerInDay - countCustomerFamiliar - countCustomerNew;
                        countByDate.Year = year;
                        countByDate.Month = month;
                        countByDate.Day = day;
                        listCountByDate.Add(countByDate);
                        //customerOfStaff.CustomerFamiliar = serializer.Serialize(countByDate);                      
                    }

                    // đổi nhãn ngày tháng rồi nén thành json thông tin thống kê loại khách của nhân viên
                    customerOfStaff.CountByDate = serializer.Serialize(ReLabelDate(listCountByDate, salonId, timeFrom, timeTo, viewTime));

                    listCustomerFamiliar.Add(customerOfStaff);
                }

                //json thông tin khách quen
                var data = new DataTotal();
                data.CustomerOfStaff = serializer.Serialize(listCustomerFamiliar);

                //salon
                var salon = "";
                var salonName = db.Tbl_Salon.Where(w => w.Id == salonId).Select(s => s.Name).FirstOrDefault();
                if (salonName != null)
                    salon = "tại salon " + salonName;
                else salon = "tại các Salon";
                var timeView = "";

                //kiểu xem
                if (viewTime == "day")
                    timeView = "ngày";
                else if (viewTime == "week")
                    timeView = "tuần";
                else if (viewTime == "quarter")
                    timeView = "quý";
                else if (viewTime == "year")
                    timeView = "năm";
                else if (viewTime == "month")
                    timeView = "tháng";

                // đơn vị và tiêu đề
                data.Unit = "Bill";
                if (staffIds.Count > 1)
                    data.Title = "thống kê khách " + salonName;
                else
                    data.Title = "Thống kê khách của " + staffName + " theo " + timeView + " " + salon + " (đơn vị " + data.Unit + ")";
                // số ngày thống kê
                data.Number = totalDay;

                return serializer.Serialize(data);
            }
        }



        /// <summary>
        /// Lấy danh sách nhân viên
        /// </summary>
        /// <param name="salonId"></param>
        /// <param name="staffType"></param>
        /// <returns>json danh sách đối tượng KeyVal chưa thông tin cơ bản của nhân viên </returns>       
        [WebMethod]
        public static string GetStaffBySalon(int salonId, string staffType)
        {
            using (var db = new Solution_30shineEntities())
            {
                var serializer = new JavaScriptSerializer();
                var Msg = new Msg();
                var andSalonId = "";
                if (salonId == 0)
                    andSalonId = "SalonId>0";
                else
                {
                    andSalonId = "SalonId='" + salonId + "'";
                }

                var sql = @"select Id, Fullname as [Name] from staff where  IsDelete!=1 and " + andSalonId + " and Type=" + staffType + " order by Fullname";
                var list = db.Database.SqlQuery<KeyVal>(sql).ToList();
                if (list.Count > 0)
                {
                    Msg.success = true;
                    Msg.msg = serializer.Serialize(list);
                }
                else
                {
                    Msg.success = false;
                }
                return serializer.Serialize(Msg);
            }
        }
        /// <summary>
        /// Lấy thông tin salon
        /// tìm theo mã nhân viên
        /// </summary>
        /// <param name="staffId"></param>
        /// <returns>json đối tượng KeyVal chứa thông tin cơ bản của salon</returns>
        [WebMethod]
        public static string GetSalonOfStaff(string staffId)
        {
            using (var db = new Solution_30shineEntities())
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                CultureInfo culture = new CultureInfo("vi-VN");

                var sqlSalonName = @"select b.Name, b.Id from
                                            (Select * from Staff where Id=" + staffId + @") as a
                                            inner join 
                                            (Select * from Tbl_Salon ) as b
                                            on a.SalonId=b.Id";
                var obj = db.Database.SqlQuery<KeyVal>(sqlSalonName).FirstOrDefault();
                // if (obj != null)
                return serializer.Serialize(obj);
            }
        }

        //CÁC HÀM PRIVATE
        /// <summary>
        /// Thống kê khách quen theo nhân viên hoặc theo salon:
        /// 1. thống kê khách quen của 1 nhân viên
        /// 2. thống kê khách quen của các nhân viên trong salon
        /// </summary>
        /// <param name="salonId"></param>
        /// <param name="staffType"></param>
        /// <param name="staffId"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="viewTime"></param>
        /// <returns>json chứa thông tin khách quen
        /// </returns>
        private static string GetCustomerFamiliar(int salonId, string staffType, string staffId, string from, string to, string viewTime)
        {
            using (var db = new Solution_30shineEntities())
            {

                var result = "";

                var andSalonId = "";
                if (salonId == 0)
                    andSalonId = "Id>0";
                else
                {
                    andSalonId = "Id='" + salonId + "'";
                }
                // nếu là 1 nhân viên
                // không phải là 1 nhân viên xác định thì là thống kê của salon 
                if (!String.IsNullOrEmpty(staffId))
                    result = ReportCustomerFamiliar2(salonId, from, to, viewTime, staffType, staffId);
                else
                {
                    var sqlStaffBySalon = @" declare @type int                                     
                                            set @type=" + staffType +
                                            @" select a.Id, a.Fullname as[Name] from 
                                        (select * from Staff where IsDelete!=1 and Type=@type ) as a
                                        inner join
                                        (select * from Tbl_Salon where " + andSalonId + @" and IsDelete!=1) as b
                                        on a.SalonId = b.Id
                                        ";
                    var listStaff = db.Database.SqlQuery<KeyVal>(sqlStaffBySalon).ToList();
                    var staffIds = new List<int>();
                    foreach (KeyVal obj in listStaff)
                    {
                        staffIds.Add(obj.Id);
                    }
                    result = GetCustomerFamiliarForStaff(salonId, from, to, viewTime, staffType, staffIds);
                }

                return result;
            }
        }

        /// <summary>
        /// Thống kê bill dịch vụ theo phân loại
        /// </summary>
        /// <param name="andSalonId"></param>
        /// <param name="andStaffId"></param>
        /// <param name="timeFrom"></param>
        /// <param name="timeTo"></param>
        /// <param name="category"></param>
        /// <returns>số lượng bill</returns>
        private static int GetServiceByCategory(string andSalonId, string andStaffId, string timeFrom, string timeTo, string category)
        {
            using (var db = new Solution_30shineEntities())
            {
                var count = 0;
                var dataCount = new DataCount();
                var where = "";

                //7 nhóm dịch vụ phân theo mã dịch vụ              
                switch (category)
                {
                    case "ShineMaster":
                        where = "b.Code = 'DV02' or b.Code = 'DV20' or b.Code = 'spasdf'";
                        break;

                    case "ShineCombo":
                        where = "b.Code = 'dvchl1' or b.Code = 'DV01' or b.Code = 'sp8272'";
                        break;

                    case "ShineQuick":
                        where = "b.Code = 'DVMP' or b.Code = 'sp99' or b.Code = 'sp678' ";
                        break;

                    case "CutStyle":
                        where = "b.Code = 'DV03'";
                        break;

                    case "HairRemove":
                        where = "b.Code = 'DV12' or b.Code = 'DV6'";
                        break;

                    case "ComboU":
                        where = "b.Code = 'DV5'";
                        break;

                    case "ComboC":
                        where = "b.Code = 'DV3'";
                        break;
                }

                var sql = "";
                var sqlDV6 = "";
                var sqlDV12 = "";

                if (category != "HairRemove")
                {
                    sql = @"declare @dtFrom datetime, @dtTo datetime
                             set @dtFrom='" + timeFrom + "'" +
                                @" set @dtTo='" + timeTo + "'" +
                                @" select Coalesce(SUM(m.Count),0) as [Count]
                      from(
                      select 
                      b.Code 
                      ,Count(b.Code) as [Count]		
                      from
                      (select a0.*
                      from
                       (select * from FlowService where IsDelete!=1 and CreatedDate between @dtFrom and @dtTo) as a0
                        inner join 
                       (select * from BillService where " + andStaffId +
                                @" IsDelete!=1 and CreatedDate between @dtFrom and @dtTo and " + andSalonId +
                                    @") as a1
                       on a0.BillId=a1.Id
                       )as a
                      inner join 		
                      (select * from Service Where IsDelete!=1)as b 
                      on a.ServiceId=b.Id 
                      where " + where +
                                @" group by b.Code
                      )as m";

                    dataCount = db.Database.SqlQuery<DataCount>(sql).FirstOrDefault();

                    if (dataCount != null)
                    {
                        count = dataCount.Count;
                    }
                }
                else if (category == "HairRemove")
                {
                    sqlDV12 = @"declare @dtFrom datetime, @dtTo datetime
                             set @dtFrom='" + timeFrom + "'" +
                                @" set @dtTo='" + timeTo + "'" +
                                @" select 
                         -- b.Code 
                         Count(b.Code)*2 as [Count]		
                         from
                         (select a0.*
                          from
                          (select * from FlowService where IsDelete!=1 and CreatedDate between @dtFrom and @dtTo)as a0
                           inner join 
                          (select * from BillService where " + andStaffId +
                                @" IsDelete!=1 and CreatedDate between @dtFrom and @dtTo and " + andSalonId +
                                @") as a1
                          on a0.BillId=a1.Id
                          )as a
                          inner join 		
                         (select * from Service Where IsDelete!=1)as b 
                         on a.ServiceId=b.Id 
                         where b.Code='DV12'
                         group by b.Code";

                    sqlDV6 = @"declare @dtFrom datetime, @dtTo datetime
                             set @dtFrom='" + timeFrom + "'" +
                                @" set @dtTo='" + timeTo + "'" +
                             @" select 
                         --b.Code 
                         Count(b.Code) as [Count]		
                         from
                         (select a0.*
                          from
                          (select * from FlowService where IsDelete!=1 and CreatedDate between @dtFrom and @dtTo)as a0
                           inner join 
                          (select * from BillService where " + andStaffId +
                                @" IsDelete!=1 and CreatedDate between @dtFrom and @dtTo and " + andSalonId +
                                @") as a1
                          on a0.BillId=a1.Id
                          )as a
                          inner join 		
                         (select * from Service Where IsDelete!=1)as b 
                         on a.ServiceId=b.Id 
                         where b.Code='DV6'
                         group by b.Code";

                    var dataCountDV12 = db.Database.SqlQuery<DataCount>(sqlDV12).FirstOrDefault();
                    var dataCountDV6 = db.Database.SqlQuery<DataCount>(sqlDV6).FirstOrDefault();
                    var countDV12 = 0;
                    var countDV6 = 0;

                    if (dataCountDV12 != null)
                        countDV12 = dataCountDV12.Count;
                    if (dataCountDV6 != null)
                        countDV6 = dataCountDV6.Count;
                    count = countDV12 + countDV6;
                }
                return count;
            }
        }

        /// <summary>
        /// Thống kê doanh số dịch vụ theo nhân viên
        /// </summary>
        /// <param name="andSalonId"></param>
        /// <param name="staffType"></param>
        /// <param name="staffId"></param>
        /// <param name="timeFrom"></param>
        /// <param name="timeTo"></param>
        /// <param name="viewTime"></param>
        /// <returns>danh sách đối tượng DataCountByDate chứa thông tin thống kê </returns>
        private static List<DataCountByDate> GetSaleOfStaff(string andSalonId, string staffType, string staffId, string timeFrom, string timeTo, string viewTime)
        {
            using (var db = new Solution_30shineEntities())
            {
                // kiểm tra tham số để viết câu ghép sql lựa chọn, điều kiện, sắp xếp
                var list = new List<DataCountByDate>();
                var selectTime = "";
                var orderBy = "";
                var groupBy = "";

                switch (viewTime)
                {
                    case "day":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month],m.[Week],m.[Day]";
                        break;
                    case "week":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month],m.[Week]";

                        break;
                    case "month":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month]";

                        break;
                    case "quarter":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter]";

                        break;
                    case "year":
                        selectTime = groupBy = orderBy = " m.[Year]";
                        break;
                }

                var staff = "";
                var andStaffId = "";
                if (staffType == "1" || staffType == "")
                {
                    staff = "Staff_Hairdresser_Id";

                }
                else if (staffType == "2")
                {
                    staff = "Staff_HairMassage_Id";
                }
                if (!String.IsNullOrEmpty(staffId))
                    andStaffId = staff + "='" + staffId + "' and ";

                var sql = @"declare @dtFrom datetime, @dtTo datetime
                        set @dtFrom = '" + timeFrom + "'" +
                        @"set @dtTo = '" + timeTo + "'" +
                        @"select SUM(m.[Sum]) as [Sum],  m.[Year],m.[Quarter],m.[Month],m.[Week],m.[Day] from 
                        (
                        select a1.*, Coalesce(b.[Sum], 0) as [Sum]
                        from
                        (
                        select 
                            DATEPART(YEAR, [DateTime]) as [Year] 
                            ,DATEPART(QUARTER, [DateTime]) as [Quarter] 
                            ,DATEPART(MONTH, [DateTime]) as[Month]
                            ,DATEPART(WEEK, [DateTime]) as[Week] 
                            ,DATEPART(DAY, [DateTime]) as [Day] 
                        from 
                        (
                            select 
                                dateadd(day, number, @dtFrom) as [DateTime]
                            from 
                                (select distinct number from master.dbo.spt_values
                                    where name is null
                                ) as n
                            where dateadd(day, number, @dtFrom) < @dtTo 
                        ) as a
                        ) as a1
                        left join 
                        (select
                             SUM(d.[Sum]) as [Sum]
                            ,c.[Year]
                            ,c.[Quarter]
                            ,c.[Month]
                            ,c.[Week]
                            ,c.[Day]
                        from
                        (
                        select Id
		                        ,Staff_Hairdresser_Id
                                ,DATEPART(YEAR, CreatedDate) as [Year] 
                                ,DATEPART(QUARTER, CreatedDate) as [Quarter] 
                                ,DATEPART(MONTH, CreatedDate) as [Month]
                                ,DATEPART(WEEK, CreatedDate) as [Week] 
                                ,DATEPART(DAY, CreatedDate) as [Day] 
                            from BillService 
                            where (CreatedDate between @dtFrom and @dtTo) 
                            and " + andStaffId +
                            @" IsDelete!=1 and ServiceIds is not null and ServiceIds!=''
                            -- and ProductIds is not null and ProductIds!=''
                            and " + andSalonId + @") as c
                            inner join 
                            (select Coalesce(SUM( Price * Quantity * ( Cast(100 - VoucherPercent as float)/Cast(100 as float))),0) as [Sum] ,DATEPART(YEAR, CreatedDate) as [Year]
                            ,DATEPART(QUARTER, CreatedDate) as [Quarter] 
                            ,DATEPART(MONTH, CreatedDate) as[Month] 
                            ,DATEPART(WEEK, CreatedDate) as[Week]
                            ,DATEPART(DAY, CreatedDate) as [Day]  
                            ,BillId	                            
                            from FlowService  where (CreatedDate between @dtFrom and @dtTo) 
                        and IsDelete!=1 and " + andSalonId + @" group by
                            DATEPART(YEAR, CreatedDate) 
                            ,DATEPART(QUARTER, CreatedDate) 
                            ,DATEPART(MONTH, CreatedDate) 
                            ,DATEPART(WEEK, CreatedDate) 
                            ,DATEPART(DAY, CreatedDate)
                            ,BillId
                            ) as d 
                            on c.Id=d.BillId
                            group by 
                                    c.[Year]
                                    ,c.[Quarter]
                                    ,c.[Month]
                                    ,c.[Week]
                                    ,c.[Day]) as b 
                        on a1.[Year]=b.[Year] and a1.[Quarter] = b.[Quarter] and a1.[Month] = b.[Month] and a1.[Week] = b.[Week] and a1.[Day] = b.[Day]
                        )m
                        group by  " + groupBy +
                         @" order by  " + orderBy;

                list = db.Database.SqlQuery<DataCountByDate>(sql).ToList();
                return list;
            }
        }

        /// <summary>
        /// Thống kê hóa đơn theo nhân viên
        /// </summary>
        /// <param name="andSalonId"></param>
        /// <param name="staffType"></param>
        /// <param name="staffId"></param>
        /// <param name="timeFrom"></param>
        /// <param name="timeTo"></param>
        /// <param name="viewTime"></param>
        /// <returns>Danh sách đối tượng DataCountByDate chứa thông tin thống kê </returns>
        private static List<DataCountByDate> GetBillOfStaff(string andSalonId, string staffType, string staffId, string timeFrom, string timeTo, string viewTime)
        {
            using (var db = new Solution_30shineEntities())
            {
                var selectTime = "";
                var groupBy = "";
                var orderBy = "";

                switch (viewTime)
                {
                    case "day":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month],m.[Week],m.[Day]";
                        break;
                    case "week":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month],m.[Week]";
                        break;
                    case "month":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month]";
                        break;
                    case "quarter":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter]";
                        break;
                    case "year":
                        selectTime = groupBy = orderBy = " m.[Year]";
                        break;
                }

                var staff = "";
                var andStaffId = "";
                if (staffType == "1" || staffType == "")
                {
                    staff = "Staff_Hairdresser_Id";

                }
                else if (staffType == "2")
                {
                    staff = "Staff_HairMassage_Id";
                }
                if (!String.IsNullOrEmpty(staffId))
                    andStaffId = staff + "='" + staffId + "' and Staff_Hairdresser_Id>0 and ";


                var sql = @"declare @dtFrom datetime,@dtTo datetime, @staffId int
                        set @dtFrom = '" + timeFrom + "'" +
                        @" set @dtTo = '" + timeTo + "'" +
                        @" set @staffId='" + staffId + "'" +
                        @" select SUM(m.[Count]) as [Count], " + selectTime +
                        @" from 
                        (
                        select a1.*, Coalesce(b.[Count], 0) as [Count]
                        from
                        (
                        select 
                            DATEPART(YEAR, [DateTime]) as [Year] 
                            ,DATEPART(QUARTER, [DateTime]) as [Quarter] 
                            ,DATEPART(MONTH, [DateTime]) as[Month]
                            ,DATEPART(WEEK, [DateTime]) as[Week] 
                            ,DATEPART(DAY, [DateTime]) as [Day] 
                        from 
                        (
                            select 
                                dateadd(day, number, @dtFrom) as [DateTime]
                            from 
                                (select distinct number from master.dbo.spt_values
                                    where name is null
                                ) as n
                            where dateadd(day, number, @dtFrom) < @dtTo 
                        ) as a
                        ) as a1
                        left join 
                        (select COUNT(*) as [Count]
                        ,DATEPART(YEAR, CreatedDate) as [Year]
                        ,DATEPART(QUARTER, CreatedDate) as [Quarter] 
                        ,DATEPART(MONTH, CreatedDate) as [Month]
                        ,DATEPART(WEEK, CreatedDate) as [Week] 
                        ,DATEPART(DAY, CreatedDate) as [Day] 
                        from BillService 
                        where IsDelete!=1 and Pending != 1 and (ServiceIds is not null and ServiceIds!='') and 
                        (CreatedDate between @dtFrom and @dtTo) and " + andStaffId + andSalonId +
                        @" group by
                        DATEPART(YEAR, CreatedDate)
                        ,DATEPART(QUARTER, CreatedDate)
                        ,DATEPART(MONTH, CreatedDate) 
                        ,DATEPART(WEEK, CreatedDate)
                        ,DATEPART(DAY, CreatedDate)) as b 
                        on a1.[Year]=b.[Year] and a1.[Quarter] = b.[Quarter] and a1.[Month] = b.[Month] and a1.[Week] = b.[Week] and a1.[Day] = b.[Day]
                        )m
                        group by " + groupBy +
                        @" order by " + orderBy;

                var list = db.Database.SqlQuery<DataCountByDate>(sql).ToList();
                return list;
            }
        }

        /// <summary>
        /// Thống kê số ngày làm việc của nhân viên
        /// </summary>
        /// <param name="salonId"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="viewTime"></param>
        /// <param name="staffType"></param>
        /// <param name="id"></param>
        /// <returns>Đối tượng DataTotal chứa thông tin thống kê</returns>
        private static DataTotal GetDayWork(int salonId, string from, string to, string viewTime, string staffType, int id)
        {
            using (var db = new Solution_30shineEntities())
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var dataTotal = new DataTotal();

                CultureInfo culture = new CultureInfo("vi-VN");
                DateTime _FromDate = Convert.ToDateTime(from, culture);
                DateTime _ToDate = _FromDate;
                if (to != "")
                {
                    _ToDate = Convert.ToDateTime(to, culture).AddDays(1);
                }
                int totalDay = (_ToDate - _FromDate).Days;
                var timeFrom = String.Format("{0:MM/dd/yyyy}", _FromDate);
                var timeTo = String.Format("{0:MM/dd/yyyy}", _ToDate);
                var staff = "";
                var staffName = "";


                if (staffType == "1" || staffType == "")
                {
                    staff = "Staff_Hairdresser_Id";
                    staffName = "Stylist";
                }
                else if (staffType == "2")
                {
                    staff = "Staff_HairMassage_Id";
                    staffName = "Skinner";
                }


                var andSalonId = "";
                if (salonId == 0)
                    andSalonId = "SalonId>0";
                else
                {
                    andSalonId = "SalonId='" + salonId + "'";
                }

                var selectTime = "";
                var groupBy = "";
                var orderBy = "";

                switch (viewTime)
                {
                    case "day":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month],m.[Week],m.[Day]";
                        break;
                    case "week":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month],m.[Week]";

                        break;
                    case "month":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month]";

                        break;
                    case "quarter":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter]";

                        break;
                    case "year":
                        selectTime = groupBy = orderBy = " m.[Year]";
                        break;
                }

                // truy vấn số bill theo ngày của nhân viên
                var sql0 = @"select COUNT(*) as [Count] 
                                ,DATEPART(YEAR, CreatedDate) AS [Year]
                                 ,DATEPART(QUARTER, CreatedDate) as [Quarter] 
                                 ,DATEPART(MONTH, CreatedDate) AS[Month] 
                                 ,DATEPART(WEEK, CreatedDate) AS[Week] 
                                 ,DATEPART(DAY, CreatedDate) as [Day]
                                 from 
                                 BillService where 
                                 (CreatedDate between @dtFrom and @dtTo) 
                                 and IsDelete !=1 and ServiceIds is not null and ServiceIds!='' and " + andSalonId +
                             " and " + staff +
                             @"=@staffId group by 
                                 DATEPART(YEAR, CreatedDate) 
                                 ,DATEPART(QUARTER, CreatedDate)
                                  ,DATEPART(MONTH, CreatedDate)
                                   ,DATEPART(WEEK, CreatedDate) 
                                   ,DATEPART(DAY, CreatedDate) ";


                // truy vấn join với bản thời gian của hệ thống để tránh tình huống dữ liệu hóa đơn không được tạo trong thời gian cần đếm
                // nếu dữ liệu không được tạo trong thời gian ngày hôm đó thì join sẽ tạo ra dòng dữ liệu có giá trị rỗng
                var sql = @"declare @dtFrom datetime, @dtTo datetime, @staffId int
                            set @dtFrom = '" + timeFrom + "'" +
                   " set @dtTo = '" + timeTo + "'" +
                   " set @staffId='" + id + "'" +
                   " select SUM(m.[Count]) as [Count], " + selectTime +
                   @" from 
                            (
                            select a1.*, Coalesce(b.[Count], 0) as [Count]
                            from
                            (
                            select 
	                            DATEPART(YEAR, [DateTime]) as [Year] 
	                            ,DATEPART(QUARTER, [DateTime]) as [Quarter] 
	                            ,DATEPART(MONTH, [DateTime]) as[Month]
	                            ,DATEPART(WEEK, [DateTime]) as[Week] 
	                            ,DATEPART(DAY, [DateTime]) as [Day] 
                            from 
                            (
	                            select 
		                            dateadd(day, number, @dtFrom) as [DateTime]
	                            from 
		                            (select distinct number from master.dbo.spt_values
			                            where name is null
		                            ) as n
	                            where dateadd(day, number, @dtFrom) < @dtTo 
                            ) as a
                            ) as a1
                            left join 
                            (" + sql0 +
                   @") as b 
                            on a1.[Year]=b.[Year] and a1.[Quarter] = b.[Quarter] and a1.[Month] = b.[Month] and a1.[Week] = b.[Week] and a1.[Day] = b.[Day]
                            )m
                            group by " + groupBy +
                   " order by " +
                   orderBy;


                var list = db.Database.SqlQuery<DataCountByDate>(sql).ToList();

                var dayOff = 0;
                // số bill > 3 mới được tính là nhân viên đi làm ngày hôm đó
                // ngược lại nếu số bill<3 coi như không tính công cho nhân viên
                foreach (DataCountByDate obj in list)
                {
                    if (obj.Count < 3)
                        dayOff++;
                }
                dataTotal.DayOff = dayOff;
                dataTotal.DayWork = totalDay - dayOff;

                return dataTotal;
            }
        }

        /// <summary>
        /// Thống kê nhân viên trong Salon
        /// </summary>
        /// <param name="andSalonId">câu ghép điều kiện mã salon</param>
        /// <param name="staffType">kiểu nhân viên</param>
        /// <param name="timeFrom">thời gian từ</param>
        /// <param name="timeTo">thời gian đến</param>
        /// <param name="viewTime">kiểu xem</param>
        /// <returns>Danh sách đối tượng DataCountByDate chứa thông tin thống kê</returns>
        private static List<DataCountByDate> CountStaffWorkBySalon(string andSalonId, string staffType, string timeFrom, string timeTo, string viewTime)
        {
            using (var db = new Solution_30shineEntities())
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                var staff = "";
                //var staffName = "";


                if (staffType == "1" || staffType == "")
                {
                    staff = "Staff_Hairdresser_Id";
                    // staffName = "Stylist";
                }
                else if (staffType == "2")
                {
                    staff = "Staff_HairMassage_Id";
                    // staffName = "Skinner";
                }

                var sql = @"declare @dtFrom datetime
                        , @dtTo datetime
                        set @dtFrom = '" + timeFrom + "'" +
                        @" set @dtTo = '" + timeTo + "'" +
                        @" select 
                        Count(p.[Code]) as [Count],
                        p.[Year], p.[Quarter],p.[Month],p.[Week], p.[Day]
                         from
                        (
                        select
                        Coalesce(m.[Code],'0') 
                        as [Code], m.[Year],m.[Quarter],m.[Month],m.[Week],m.[Day] from 
                        (
                        select a1.*, b.[Code] as [Code]
                        from
                        (
                        select 
                        DATEPART(YEAR, [DateTime]) as [Year] 
                        ,DATEPART(QUARTER, [DateTime]) as [Quarter] 
                        ,DATEPART(MONTH, [DateTime]) as[Month]
                        ,DATEPART(WEEK, [DateTime]) as[Week] 
                        ,DATEPART(DAY, [DateTime]) as [Day] 
                        from 
                        (
                        select 
                        dateadd(day, number, @dtFrom) as [DateTime]
                        from 
                        (select distinct number from master.dbo.spt_values
                        where name is null
                        ) as n
                        where dateadd(day, number, @dtFrom) < @dtTo 
                        ) as a
                        ) as a1
                        left join 
                        (select 
                        " + staff + @" as [Code]
                        ,DATEPART(YEAR, CreatedDate) AS [Year]
                        ,DATEPART(QUARTER, CreatedDate) as [Quarter] 
                        ,DATEPART(MONTH, CreatedDate) AS[Month] 
                        ,DATEPART(WEEK, CreatedDate) AS[Week] 
                        ,DATEPART(DAY, CreatedDate) as [Day]
                        from 
                        BillService where (CreatedDate between @dtFrom and @dtTo) 
                        and IsDelete !=1 and ServiceIds is not null and ServiceIds!='' and Pending!=1 and " + staff +
                        @">0 and " + andSalonId +
                        @" group by 
                        DATEPART(YEAR, CreatedDate) 
                        ,DATEPART(QUARTER, CreatedDate)
                        ,DATEPART(MONTH, CreatedDate)
                        ,DATEPART(WEEK, CreatedDate) 
                        ,DATEPART(DAY, CreatedDate)	
                        ," + staff +
                        @") as b 
                        on a1.[Year]=b.[Year] and a1.[Quarter] = b.[Quarter] and a1.[Month] = b.[Month] and a1.[Week] = b.[Week] and a1.[Day] = b.[Day]
                        )m
                        group by  m.[Year],m.[Quarter],m.[Month],m.[Week],m.[Day] ,m.[Code] 
                        ) as p
                        group by p.[Year], p.[Quarter],p.[Month],p.[Week], p.[Day]
                        order by p.[Year], p.[Quarter],p.[Month],p.[Week], p.[Day]
                        ";

                var list = db.Database.SqlQuery<DataCountByDate>(sql).ToList();
                return list;
            }
        }

        /// <summary>
        /// Thống kê đánh giá dịch vụ
        /// dịch vụ đặc biết và dịch vụ thường
        /// </summary>
        /// <param name="salonId">mã salon</param>
        /// <param name="staffType">kiểu nhân viên</param>
        /// <param name="staffId">mã nhân viên</param>
        /// <param name="timeFrom">thời gian từ</param>
        /// <param name="timeTo">thời gian đến</param>
        /// <param name="viewTime">kiểu xem</param>
        /// <returns>json đối tượng DataRating chứa thông tin thống kê</returns>
        public static string GetRatingService(int salonId, string staffType, string staffId, string timeFrom, string timeTo, string viewTime)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var data = new DataRating();
            var andSalonId = "";
            if (salonId == 0)
                andSalonId = "SalonId>0";
            else
            {
                andSalonId = "SalonId='" + salonId + "'";
            }
            var typeBillSpecial = "special";
            var typeBillNormal = "normal";

            var listRatingTotal = GetRatingPointTotal(andSalonId, staffType, staffId, "special", timeFrom, timeTo, viewTime);
            var ratingService = ReLabelDate(listRatingTotal, salonId, timeFrom, timeTo, viewTime);


            var billNormal0 = GetBillRating(salonId, staffType, staffId, timeFrom, timeTo, viewTime, typeBillNormal, 1);
            var billNormal1 = GetBillRating(salonId, staffType, staffId, timeFrom, timeTo, viewTime, typeBillNormal, 2);
            var billNormal2 = GetBillRating(salonId, staffType, staffId, timeFrom, timeTo, viewTime, typeBillNormal, 3);
            var billSpecial0 = GetBillRating(salonId, staffType, staffId, timeFrom, timeTo, viewTime, typeBillSpecial, 1);
            var billSpecial1 = GetBillRating(salonId, staffType, staffId, timeFrom, timeTo, viewTime, typeBillSpecial, 2);
            var billSpecial2 = GetBillRating(salonId, staffType, staffId, timeFrom, timeTo, viewTime, typeBillSpecial, 3);


            data.RatingTotal = serializer.Serialize(ratingService);

            data.BillNormal0 = billNormal0.ToString();
            data.BillNormal1 = billNormal0.ToString();
            data.BillNormal2 = billNormal0.ToString();
            data.BillSpecial0 = billNormal0.ToString();
            data.BillSpecial1 = billSpecial0.ToString();
            data.BillSpecial2 = billSpecial0.ToString();

            return serializer.Serialize(data);
        }

        /// <summary>
        /// Thống kê số bill theo điểm đánh giá và phân loại bill
        /// ;Điểm đánh giá: không hài lòng, hài lòng, tuyệt vời
        /// ;Phân loại bill: đặc biệt, thường, chưa đánh giá 
        /// </summary>
        /// <param name="salonId">id salon</param>
        /// <param name="staffType">kiểu nhân viên</param>
        /// <param name="staffId">id nhân viên</param>
        /// <param name="timeFrom">thời gian từ</param>
        /// <param name="timeTo">thời gian đến</param>
        /// <param name="viewTime">kiểu xem</param>
        /// <param name="typeBill">kiểu bill(special, normal, '')</param>
        /// <param name="conventionPoint">điểm đánh giá (0,1,3)</param>
        /// <returns>số bill</returns>
        private static int GetBillRating(int salonId, string staffType, string staffId, string timeFrom, string timeTo, string viewTime, string typeBill, int conventionPoint)
        {
            using (var db = new Solution_30shineEntities())
            {
                //convention point
                //0: khong hai long
                //1: hai long
                //3: tuyet voi

                var staff = "";
                var andStaffId = "";
                if (staffType == "1" || staffType == "")
                {
                    staff = "e.Staff_Hairdresser_Id";

                }
                else if (staffType == "2")
                {
                    staff = "e.Staff_HairMassage_Id";
                }

                var andSalonId = "";
                if (salonId == 0)
                    andSalonId = " e.SalonId>0";
                else
                {
                    andSalonId = " e.SalonId='" + salonId + "'";
                }
                if (!String.IsNullOrEmpty(staffId))
                    andStaffId = staff + "='" + staffId + "' and ";

                // câu ghép truy vấn định nghĩa tham số truy vấn
                var decrale = @"declare @coefficient float
                            declare @coefficient_spc float;
                            declare @dtFrom nvarchar(50);
                            declare @dtTo nvarchar(50);
                            declare @conventionPoint int
                            set @coefficient = 1;
                            set @coefficient_spc = 1.2;
                            set @dtFrom = '" + timeFrom + "'" +
                            @" set @dtTo = '" + timeTo + "'" +
                            @" set @conventionPoint=" + conventionPoint;


                var sql = "";
                // kiểu bill thường hoặc bill đặc biệt
                if (typeBill == "normal")
                {
                    //truy vấn bill thường là các bill có mã dịch vụ là 14,16,17,24 hoặc không phải là khách quen
                    sql = decrale + @" select COUNT(*) as [Count]
                         from(
                         select g.BillId
                         from
                         (
                          select d.*
                          from BillService as e
                          left join FlowService as d
                          on e.Id = d.BillId
                          where
                          e.IsDelete != 1 and e.Pending != 1
                          and e.ServiceIds != '' and e.ServiceIds is not null
                          and e.CreatedDate between @dtFrom and @dtTo and " + andStaffId + andSalonId +
                          @" and e.Mark = @conventionPoint
                          --and d.ServiceId not in (14, 16, 17, 24)
                          and e.IsCusFamiliar = 0
                          and d.IsDelete != 1
                         ) as g
                         EXCEPT
                         select d.BillId
                         from BillService as e
                         left join FlowService as d
                         on e.Id = d.BillId
                         where
                         e.IsDelete != 1 and e.Pending != 1
                         and e.ServiceIds != '' and e.ServiceIds is not null
                         and e.CreatedDate between @dtFrom and @dtTo
                         and " + andStaffId + andSalonId + @" and e.Mark = @conventionPoint
                         and d.ServiceId in (14, 16, 17, 24) 
                         and e.IsCusFamiliar = 0
                         and d.IsDelete != 1
                         group by d.BillId
                         )m
                        ";
                }
                else if (typeBill == "special")
                {
                    // các bill không phải là bill thường thì là bill đặc biệt
                    sql = decrale + @" select
                         --m.BillId
                        Count(m.BillId) as [Count]
                        from
                        (
                        select d.BillId
                        --Count(d.CreatedDate) as [Count]
                        from BillService as e
                        left join FlowService as d
                        on e.Id = d.BillId
                        where
                        e.IsDelete != 1 and e.Pending != 1
                        and e.ServiceIds != '' and e.ServiceIds is not null
                        and e.CreatedDate between @dtFrom and @dtTo
                        and " + andStaffId + andSalonId + @" and e.Mark = @conventionPoint
                         and(d.ServiceId  in (14, 16, 17, 24) or e.IsCusFamiliar = 1)
                        group by d.BillId
                        )
                        as m";
                }
                // các bill chưa đánh giá
                else
                {
                    sql = decrale + @" select
                         --m.BillId
                        Count(m.BillId) as [Count]
                        from
                        (
                        select d.BillId
                        --Count(d.CreatedDate) as [Count]
                        from BillService as e
                        left join FlowService as d
                        on e.Id = d.BillId
                        where
                        e.IsDelete != 1 and e.Pending != 1
                        and e.ServiceIds != '' and e.ServiceIds is not null
                        and e.CreatedDate between @dtFrom and @dtTo
                        and " + andStaffId + andSalonId + @" and e.Mark = @conventionPoint                      
                        group by d.BillId
                        )
                        as m";
                }

                var obj = db.Database.SqlQuery<DataCount>(sql).FirstOrDefault();

                return obj.Count;
            }
        }

        /// <summary>
        /// Thống kê điểm đánh giá
        /// </summary>
        /// <param name="andSalonId">id salon</param>
        /// <param name="staffType">kiểu nhân viên</param>
        /// <param name="staffId">id nhân viên</param>
        /// <param name="conventionPoint">điểm đánh giá</param>
        /// <param name="typeBill">kiểu hóa đơn</param>
        /// <param name="timeFrom">thời gian từ</param>
        /// <param name="timeTo">thời gian đến</param>
        /// <param name="viewTime">kiểu xem</param>
        /// <returns>danh sách đối tượng DataCountByDate chứa thông tin thông kê</returns>
        private static List<DataCountByDate> GetRatingMark(string andSalonId, string staffType, string staffId, int conventionPoint, string typeBill, string timeFrom, string timeTo, string viewTime)
        {
            using (var db = new Solution_30shineEntities())
            {
                var selectTime = "";
                var groupBy = "";
                var orderBy = "";

                switch (viewTime)
                {
                    case "day":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month],m.[Week],m.[Day]";
                        break;
                    case "week":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month],m.[Week]";
                        break;
                    case "month":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month]";
                        break;
                    case "quarter":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter]";
                        break;
                    case "year":
                        selectTime = groupBy = orderBy = " m.[Year]";
                        break;
                }

                var staff = "";
                var andStaffId = "";
                if (staffType == "1" || staffType == "")
                {
                    staff = "Staff_Hairdresser_Id";

                }
                else if (staffType == "2")
                {
                    staff = "Staff_HairMassage_Id";
                }

                var andTypeBill = "";
                if (typeBill == "normal")
                {
                    andTypeBill = "(d.ServiceId  not in (14,16,17,24) or a.IsCusFamiliar is null)";
                }
                else if (typeBill == "special")
                {
                    andTypeBill = "(d.ServiceId  in (14,16,17,24) or a.IsCusFamiliar=1)";
                }

                if (!String.IsNullOrEmpty(staffId))
                    andStaffId = staff + "='" + staffId + "' and ";


                var sql = @"declare @coefficient float;
                            declare @coefficient_spc float;
                            declare @dtFrom nvarchar(50);
                            declare @dtTo nvarchar(50);
                            declare @salonId int;
                            declare @staffId int;
                            declare @conventionPoint int;

                            set @coefficient = 1;
                            set @coefficient_spc = 1.2;
                            set @dtFrom = '" + timeFrom + "'" +
                        @" set @dtTo = '" + timeTo + "'" +
                        @" set @staffId='" + staffId + "'" +
                        @" set @conventionPoint=" + conventionPoint +
                        @" select m.[Count]," + selectTime +
                        @" from
                            (
                            select a1.*, Coalesce(b.[Count], 0) as [Count]
                            from
                            (
                            select 
                            DATEPART(YEAR, [DateTime]) as [Year] 
                            ,DATEPART(QUARTER, [DateTime]) as [Quarter] 
                            ,DATEPART(MONTH, [DateTime]) as[Month]
                            ,DATEPART(WEEK, [DateTime]) as[Week] 
                            ,DATEPART(DAY, [DateTime]) as [Day] 
                            from 
                            (
                            select 
                            dateadd(day, number, @dtFrom) as [DateTime]
                            from 
                            (select distinct number from master.dbo.spt_values
                                where name is null
                            ) as n
                            where dateadd(day, number, @dtFrom) < @dtTo 
                            ) as a
                            ) as a1
                            left join 
                            (
                            select 
                            DATEPART(YEAR, a.CreatedDate) as [Year]
                            , DATEPART(QUARTER, a.CreatedDate) as [Quarter]
                            , DATEPART(MONTH, a.CreatedDate) as [Month]
                            , DATEPART(WEEK, a.CreatedDate) as [Week]
                            , DATEPART(DAY, a.CreatedDate) as [Day]
                            ,Count(a.Mark) as [Count]
       
                            from 
                            (
                            select * from BillService 
                            where SalonId>0 and " + andStaffId +
                            @"  Mark = ( select ConventionPoint from Rating_ConfigPoint where ConventionPoint=@conventionPoint and Hint = 1 ) 
                            ) as a
                            left join FlowService as d
                            on a.Id = d.BillId and " + andTypeBill +
                        @" where a.IsDelete != 1 and a.Pending != 1
                            and a.ServiceIds != '' and a.ServiceIds is not null
                            and a.CreatedDate between @dtFrom and @dtTo
                            and a.Staff_Hairdresser_Id > 0

                            group by  
                            DATEPART(YEAR, a.CreatedDate)
                            , DATEPART(QUARTER, a.CreatedDate)
                            , DATEPART(MONTH, a.CreatedDate)
                            , DATEPART(WEEK, a.CreatedDate)
                            , DATEPART(DAY, a.CreatedDate) 
                            )as b
                            on a1.[Year]=b.[Year] and a1.[Quarter] = b.[Quarter] and a1.[Month] = b.[Month] and a1.[Week] = b.[Week] and a1.[Day] = b.[Day]
                            )m
                            order by
                           " + orderBy +
                        " asc"
                        ;

                var list = db.Database.SqlQuery<DataCountByDate>(sql).ToList();
                return list;
            }
        }

        /// <summary>
        /// Thống kê tống điểm đánh giá
        /// </summary>
        /// <param name="andSalonId">id salon</param>
        /// <param name="staffType">kiểu nhân viên</param>
        /// <param name="staffId">id nhân viên</param>
        /// <param name="conventionPoint">điểm đánh giá</param>
        /// <param name="typeBill">kiểu hóa đơn</param>
        /// <param name="timeFrom">thời gian từ</param>
        /// <param name="timeTo">thời gian đến</param>
        /// <param name="viewTime">kiểu xem</param>
        /// <returns>Danh sách đối tượng DataCountByDate chứa thông tin thông kê</returns>
        private static List<DataCountByDate> GetRatingPointTotal(string andSalonId, string staffType, string staffId, string typeBill, string timeFrom, string timeTo, string viewTime)
        {
            using (var db = new Solution_30shineEntities())
            {
                var selectTime = "";
                var groupBy = "";
                var orderBy = "";

                switch (viewTime)
                {
                    case "day":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month],m.[Week],m.[Day]";
                        break;
                    case "week":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month],m.[Week]";
                        break;
                    case "month":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter],m.[Month]";
                        break;
                    case "quarter":
                        selectTime = groupBy = orderBy = " m.[Year],m.[Quarter]";
                        break;
                    case "year":
                        selectTime = groupBy = orderBy = " m.[Year]";
                        break;
                }

                var staff = "";
                var andStaffId = "";
                if (staffType == "1" || staffType == "")
                {
                    staff = "Staff_Hairdresser_Id";

                }
                else if (staffType == "2")
                {
                    staff = "Staff_HairMassage_Id";
                }

                var andTypeBill = "";
                if (typeBill == "normal")
                {
                    andTypeBill = "(d.ServiceId  not in (14,16,17,24) or a.IsCusFamiliar=0)";
                }
                else if (typeBill == "special")
                {
                    andTypeBill = "(d.ServiceId  in (14,16,17,24) or a.IsCusFamiliar=1)";
                }

                if (!String.IsNullOrEmpty(staffId))
                    andStaffId = staff + "='" + staffId + "' and ";


                var sql = @"declare @coefficient float;
                        declare @coefficient_spc float;
                        declare @dtFrom nvarchar(50);
                        declare @dtTo nvarchar(50);
                        declare @staffId int

                        set @coefficient = 1;
                        set @coefficient_spc = 1.2;
                        set @dtFrom = '" + timeFrom + "'" +
                        @" set @dtTo = '" + timeTo + "'" +

                        @" select 
                        m.[Sum] as [Count]," + selectTime +
                        @" from
                        (
                        select a1.*
                        ,Coalesce(SUM(b.ConventionPoint),0) as [Sum]
                        --, Coalesce(b.[Count], 0) as [Count]
                        from
                        (
                        select 
                            DATEPART(YEAR, [DateTime]) as [Year] 
                            ,DATEPART(QUARTER, [DateTime]) as [Quarter] 
                            ,DATEPART(MONTH, [DateTime]) as[Month]
                            ,DATEPART(WEEK, [DateTime]) as[Week] 
                            ,DATEPART(DAY, [DateTime]) as [Day] 
                        from 
                        (
                            select 
                                dateadd(day, number, @dtFrom) as [DateTime]
                            from 
                                (select distinct number from master.dbo.spt_values
                                    where name is null
                                ) as n
                            where dateadd(day, number, @dtFrom) < @dtTo 
                        ) as a
                        ) as a1
                        left join 
                        (
                                select c.*,
                                        (Case
                                            when (c.IsCusFamiliar = 1 or c.ServiceId is not null ) then c.ConventionPoint * @coefficient_spc
                                            when ( c.IsCusFamiliar = 0 and c.ServiceId is null ) then c.ConventionPoint * @coefficient
                                        end) as point,
                                        (Case
                                            when (c.IsCusFamiliar = 1 or c.ServiceId is not null ) then 1
                                            when ( c.IsCusFamiliar = 0 and c.ServiceId is null ) then 0
                                        end) as IsSpecial,
                                        (Case
                                            when (c.IsCusFamiliar = 1 or c.ServiceId is not null ) then 0
                                            when ( c.IsCusFamiliar = 0 and c.ServiceId is null ) then 1
                                        end) as IsNormal
                                from
                                (
                                    select     
                                        DATEPART(YEAR, a.CreatedDate) as [Year]
				                        , DATEPART(QUARTER, a.CreatedDate) as [Quarter]
				                        , DATEPART(MONTH, a.CreatedDate) as [Month]
				                        , DATEPART(WEEK, a.CreatedDate) as [Week]
				                        , DATEPART(DAY, a.CreatedDate) as [Day]
				                        ,Count(a.Mark) as [Count]
                                        ,a.Id
                                        , a.Mark
                                        , a.IsCusFamiliar
                                        , b.ConventionPoint
                                        ,d.ServiceId
                                        ,ROW_NUMBER() OVER(PARTITION BY a.Id ORDER BY a.Staff_Hairdresser_Id) AS Row
                                    from 
                                    (select * from BillService where " + andStaffId + " IsDelete!=1" +
                                    @" ) as a
                                    left join Rating_ConfigPoint as b
                                    on a.Mark = b.RealPoint and b.Hint = 1
                                    left join FlowService as d
                                    on a.Id = d.BillId and " + andTypeBill +
                                    @" where a.IsDelete != 1 and a.Pending != 1
                                    and a.ServiceIds != '' and a.ServiceIds is not null
                                    and a.CreatedDate between @dtFrom and @dtTo 
                                    and a.Staff_Hairdresser_Id > 0
            
                                    group by 
                                       DATEPART(YEAR, a.CreatedDate) 
				                        , DATEPART(QUARTER, a.CreatedDate)
				                        , DATEPART(MONTH, a.CreatedDate)
				                        , DATEPART(WEEK, a.CreatedDate)
				                        , DATEPART(DAY, a.CreatedDate)
				                        ,a.Id
                                        , a.Staff_Hairdresser_Id
                                        , a.Mark
                                        , a.IsCusFamiliar
                                        , b.ConventionPoint
                                        ,d.ServiceId
                                ) as c
                                where c.Row = 1
                            )as b
                         on a1.[Year]=b.[Year] and a1.[Quarter] = b.[Quarter] and a1.[Month] = b.[Month] and a1.[Week] = b.[Week] and a1.[Day] = b.[Day]
                         group by
                          a1.[Year]
                        , a1.[Quarter]
                        , a1.[Month]
                        , a1.[Week]
                        , a1.[Day]
                        )m
                        group by " + groupBy +
                        @" ,m.[Sum]
                        order by " + orderBy +
                        "asc";


                var list = db.Database.SqlQuery<DataCountByDate>(sql).ToList();
                return list;
            }
        }

        /// <summary>
        /// Đổi nhãn thời gian
        /// và nhãn giá trị thống kê 
        /// </summary>
        /// <param name="list">danh sách đối tượng DataCountByDate cần đổi nhãn</param>
        /// <param name="salonId">id salon</param>
        /// <param name="timeFrom">thời gian từ</param>
        /// <param name="timeTo">thời gian đến</param>
        /// <param name="viewTime">kiểu xem</param>
        /// <returns>Danh sách đối tượng DataCountByDate</returns>
        private static List<DataCountByDate> ReLabelDate(List<DataCountByDate> list, int salonId, string timeFrom, string timeTo, string viewTime)
        {
            // escape là khoảng trống
            // nếu danh sách lớn hơn 87 thì hiển thị cách nhau 10 khoảng trống
            // lớn hơn 28 thì hiển thị cách nhau 4 khoảng trống
            // lớn hơn 15 thì hiển thị cách nhau 2 khoảng trống

            int escape = 1;
            if (list.Count > 87)
                escape = 10;
            else if (list.Count > 28)
                escape = 4;
            else if (list.Count > 15)
                escape = 2;


            var listRe = new List<DataCountByDate>();
            var listTimeRange = GetTimeRange(salonId, timeFrom, timeTo, viewTime);

            for (int i = 0; i < list.Count; ++i)
            {
                var obj = new DataCountByDate();
                var obji = list[i];

                // obj.Date = obji.Date;
                obj.Year = obji.Year;
                obj.Quarter = obji.Quarter;
                obj.Month = obji.Month;
                obj.Week = obji.Week;
                obj.Day = obji.Day;

                obj.Count = obji.Count;
                obj.Count2 = obji.Count2;
                obj.Count3 = obji.Count3;
                // obj.Sum = obji.Sum;
                obj.Avg = obji.Avg;
                obj.Percent = obji.Percent;

                var timeRangeFrom = "";
                var timeRangeTo = "";


                if (i < listTimeRange.Count)
                {
                    //đổi lại các nhãn thành định dạng ngày/tháng/năm 
                    timeRangeFrom = String.Format("{0:dd/MM/yyy}", listTimeRange[i].timeFrom);
                    timeRangeTo = String.Format("{0:dd/MM/yyy}", listTimeRange[i].timeTo);
                }

                //kiểm tra ngày tháng và đổi định dạng
                if (viewTime == "day")
                {
                    obj.Date = obji.Date;
                    // vì nếu phải cắt các đối tượng, các đối tượng trống thì sẽ mất ngày tháng năm
                    // nên cần kiểm tra điều kiện thời gian của đối tượng trước khi thay định dạng và đặt thêm âm lịch
                    //nếu phần tử đầy đủ ngày tháng năm
                    if (obji.Year != 0)
                    {
                        //đặt ngày tháng năm âm lịch
                        var date = new DateTime(obji.Year, obji.Month, obji.Day);
                        LunarDate lularDate = date.ToLunarDate();
                        obj.LularDate = lularDate.ToString();
                        var lularDate2 = new DateTime(lularDate.Year, lularDate.Month, lularDate.Day);
                        // đổi nhãn ngày/tháng/năm nếu khoảng thời gian chọn là giao giữa 2 năm
                        // nếu chọn khoảng thời gian trong cùng 1 năm thì chỉ hiển thị ngày/tháng
                        if (lularDate.Year != date.Year)
                            obj.DateRe = String.Format("{0:dd/MM/yy}", date) + "<br>" + "(" + String.Format("{0:dd/MM/yy}", lularDate2) + " AL)";
                        else
                            obj.DateRe = String.Format("{0:dd/MM}", date) + "<br>" + "(" + lularDate.Day + "/" + lularDate.Month + " AL)";
                    }
                    //nếu đối tượng có ngày/tháng
                    else if (obji.Year == 0 && obji.Date != null)
                    {
                        //đặt ngày tháng âm lịch
                        LunarDate lularDate = obji.Date.ToLunarDate();
                        obj.LularDate = lularDate.ToString();
                        var lularDate2 = new DateTime(lularDate.Year, lularDate.Month, lularDate.Day);
                        // đổi nhãn ngày/tháng/năm nếu khoảng thời gian chọn là giao giữa 2 năm
                        // nếu chọn khoảng thời gian trong cùng 1 năm thì chỉ hiển thị ngày/tháng
                        if (lularDate.Year == obji.Year)
                            obj.DateRe = String.Format("{0:dd/MM/yy}", obji.Date) + "<br>" + "(" + lularDate.Day + "/" + lularDate.Month + " AL)";
                        else
                            obj.DateRe = String.Format("{0:dd/MM/yy}", obji.Date) + "<br>" + "(" + String.Format("{0:dd/MM/yy}", lularDate2) + " AL)";
                    }

                   //đổi nhãn tổng số, giảm đi 1000000, dành cho thống kê đơn vị triệu của doanh thu
                    obj.Sum = obji.Sum / 1000000;
                }

                // nếu kiểu xem theo tuần
                else if (viewTime == "week")
                {

                    obj.DateRe = "Tuần " + obj.Week + "/" + obj.Year + "<br>" + "(" + timeRangeFrom + " - " + timeRangeTo + ")";
                    //đổi nhãn giảm đi 1000 000
                    obj.Sum = obji.Sum / 1000000;
                }
                else if (viewTime == "month")
                {
                    obj.DateRe = "Tháng " + obj.Month + "/" + obj.Year + "<br>" + "(" + timeRangeFrom + " - " + timeRangeTo + ")";
                    //đổi nhãn giảm đi 1000 000
                    obj.Sum = obji.Sum / 1000000;
                }
                else if (viewTime == "quarter")
                {
                    obj.DateRe = "Quý " + obj.Quarter + "/" + obj.Year + "<br>" + "(" + timeRangeFrom + " - " + timeRangeTo + ")";
                    //đổi nhãn giảm đi 1000 000
                    obj.Sum = obji.Sum / 1000000;
                }
                else if (viewTime == "year")
                {
                    obj.DateRe = "Năm " + obj.Year + "<br>" + "(" + timeRangeFrom + " - " + timeRangeTo + ")";
                    //đổi nhãn giảm đi 1000 000
                    obj.Sum = obji.Sum / 1000000;
                }

                //đổi nhãn trống nếu quá nhiều phần tử
                if (escape > 1)
                {
                    if (i % escape != 0)
                        obj.DateRe = "";

                    listRe.Add(obj);
                }
                
                else
                {
                    listRe.Add(obj);
                }
            }
            //trả kết quả là danh sách đã nhãn đã tối ưu cho hiển thị
            return listRe;
        }

        /// <summary>
        /// Lấy khoảng thời gian(thời gian bắt đầu và thời gian kết thúc) theo kiểu xem
        /// Kiểu xem là ngày, tháng, quý, năm
        /// </summary>
        /// <param name="salonId">id salon</param>
        /// <param name="from">thời gian từ</param>
        /// <param name="to">thời gian đến</param>
        /// <param name="viewTime">kiểu xem</param>
        /// <returns>Danh sách DateTimeRange</returns>
        public static List<DateTimeRange> GetTimeRange(int salonId, string from, string to, string viewTime)
        {
            using (var db = new Solution_30shineEntities())
            {

                var andSalonId = "";
                if (salonId == 0)
                    andSalonId = "SalonId>0";
                else
                    andSalonId = "SalonId='" + salonId + "'";

                var andBetweenDate = "(CreatedDate between '" + from + "' and '" + to + "')";
           
                var sql = @"select
                                    COUNT(*) as [count]
                                    ,DATEPART(YEAR, CreatedDate) as [Year]
                                    ,DATEPART(QUARTER,CreatedDate) as [Quarter]
                                    ,DATEPART(MONTH, CreatedDate) as [Month]
                                    ,DATEPART(WEEK, CreatedDate) as [Week]
                                    ,DATEPART(DAY, CreatedDate) as [Day]
                                    from BillService                                  
                                    Where IsDelete!=1 and Pending!=1 and " + andSalonId +
                                " and " + andBetweenDate +
                                "group by DATEPART(YEAR, CreatedDate),DATEPART(QUARTER,CreatedDate),DATEPART(MONTH, CreatedDate),DATEPART(WEEK, CreatedDate),DATEPART(DAY, CreatedDate) order by [Year],[Quarter],[Month],[Week],[Day]";

                var list = db.Database.SqlQuery<DataCountByDate>(sql).ToList();
                var listTimeRange = new List<DateTimeRange>();

                // các biến con trỏ kiểm tra khi chạy vòng lặp
                var i = 0;
                var check = 0;
                int last = 0;
                int next = 0;
                var timeRange = new DateTimeRange();
                //chạy vòng lặp danh sách và kiểm tra kiểu xem,
                // lấy ra phần tử đầu và phần tử cuối theo kiểu xem week, month, quarter, year, 
                while (i < list.Count)
                {
                    if (viewTime == "week")
                    {
                        last = list[i].Week;
                        next = last;
                        if (i < list.Count - 1)
                            next = list[i + 1].Week;
                    }
                    else if (viewTime == "month")
                    {
                        last = list[i].Month;
                        next = last;
                        if (i < list.Count - 1)
                            next = list[i + 1].Month;
                    }
                    else if (viewTime == "quarter")
                    {
                        last = list[i].Quarter;
                        next = last;
                        if (i < list.Count - 1)
                            next = list[i + 1].Quarter;
                    }
                    else if (viewTime == "year")
                    {
                        last = list[i].Year;
                        next = last;
                        if (i < list.Count - 1)
                            next = list[i + 1].Year;
                    }
                    //
                    if (i == 0)
                    {
                        check++;
                        var beginTime = new DateTime(list[i].Year, list[i].Month, list[i].Day);
                        timeRange.timeFrom = beginTime;
                    }
                    // if new week
                    if (last != next)
                    {
                        check++;
                        //var first = next;
                        if (check == 1 && i != 0)
                        {
                            timeRange = new DateTimeRange();
                            var beginTime = new DateTime(list[i + 1].Year, list[i + 1].Month, list[i + 1].Day);
                            timeRange.timeFrom = beginTime;
                        }
                        if (check == 2)
                        {
                            var endTime = new DateTime(list[i + 1].Year, list[i + 1].Month, list[i + 1].Day);
                            timeRange.timeTo = endTime;
                            listTimeRange.Add(timeRange);
                            //timeRange= new DateTimeRange();
                            i--;
                            check = 0;
                        }


                    }
                    i++;
                    //end datetime  
                    if (i == list.Count - 1)
                    {
                        var endTime = new DateTime(list[i].Year, list[i].Month, list[i].Day);
                        timeRange.timeTo = endTime;
                        listTimeRange.Add(timeRange);
                    }
                }

                return listTimeRange;
            }
        }

        /// <summary>
        /// Thông tin thống kê đánh giá
        /// </summary>
        public class DataRating
        {
            public string RatingTotal { get; set; }
            public string BillNormal0 { get; set; }
            public string BillNormal1 { get; set; }
            public string BillNormal2 { get; set; }
            public string BillSpecial0 { get; set; }
            public string BillSpecial1 { get; set; }
            public string BillSpecial2 { get; set; }
        }

        /// <summary>
        /// Thông tin thống kê hóa đơn
        /// </summary>
        public class DataBill
        {
            public string Salon { get; set; }
            public string Title { get; set; }
            public string Unit { get; set; }
            public string BillServiceCustomerOld { get; set; }
            public string BillServiceCustomerNew { get; set; }
            public string BillServiceTotal { get; set; }
            public string BillCosmeticOnline { get; set; }
            public string BillCosmeticSalon { get; set; }
            public string BillCosmeticTotal { get; set; }
            public string BillSpecialPoint { get; set; }
            public string BillNormalPoint { get; set; }
            public int Number { get; set; }
        }
        /// <summary>
        /// Thông tin thông kê phân loại doanh số
        /// </summary>
        public class DataSale
        {
            public string Salon { get; set; }
            public string Title { get; set; }
            public string Unit { get; set; }
            public string SaleService { get; set; }
            public string SaleCosmetic { get; set; }
            public string SaleCosmeticOnline { get; set; }
            public string SaleCosmeticSalon { get; set; }
            public string Avg { get; set; }
            public int Number { get; set; }
        }

        /// <summary>
        /// Thông tin thông kê loại khách hàng
        /// </summary>
        public class DataCustomer
        {
            public string Salon { get; set; }
            public string Title { get; set; }
            public string Unit { get; set; }
            public string Percent { get; set; }
            public string CustomerNew { get; set; }
            public string CustomerOld { get; set; }
            public string CustomerFamiliar { get; set; }
            public int Number { get; set; }
        }

        /// <summary>
        /// Thông tin thống kê loại nhân viên
        /// </summary>
        public class DataCountStaff
        {
            public DateTime Date { get; set; }
            public int Year { get; set; }
            public int Quarter { get; set; }
            public int Month { get; set; }
            public int Week { get; set; }
            public int Day { get; set; }
            public double Skinner { get; set; }
            public double Stylist { get; set; }
        }
        /// <summary>
        /// Thông tin về nhân viên
        /// Dùng trong thống kê quan hệ khách hàng với nhân viên
        /// </summary>
        public class CustomerOfStaff
        {
            public int Count { get; set; }
            public double Avg { get; set; }
            public int Id { get; set; }
            public string Name { get; set; }
            public int DayOff { get; set; }
            public int DayWork { get; set; }
            public string CustomerFamiliar { get; set; }
            public string CountByDate { get; set; }
        }


        public class DataTotal
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Salon { get; set; }
            public string Phone { get; set; }
            public string Level { get; set; }
            public int DayWork { get; set; }
            public int DayOff { get; set; }
            public string Avartar { get; set; }
            public string StaffWork { get; set; }
            public string Sale { get; set; }
            public string Bill { get; set; }
            public string Service { get; set; }
            public string Customer { get; set; }
            public string BillByHour { get; set; }
            public string QuatityServey { get; set; }

            public string RatingService { get; set; }

            //các tiêu tiêu đề
            public string Title { get; set; }
            //title revenue
            public string Title1 { get; set; }
            //title sales
            public string Title2 { get; set; }
            //title bill by hour
            public string Title3 { get; set; }
            //title category customer
            public string Title4 { get; set; }
            //title category service
            public string Title5 { get; set; }
            //quantity servey
            public string Title6 { get; set; }
            //rating service
            public string Title7 { get; set; }

            public string Unit { get; set; }
            // number money
            public string Unit1 { get; set; }
            //nunmber bill
            public string Unit2 { get; set; }
            //number bills/hour
            public string Unit3 { get; set; }
            //number customer
            public string Unit4 { get; set; }
            //number service
            public string Unit5 { get; set; }
            //number quantiy servey
            public string Unit6 { get; set; }

            //rating service
            public string Unit7 { get; set; }

            public int Number { get; set; }

            //các thuộc tính cho đánh giá hóa đơn
            public string BillNotRating { get; set; }
            public string RatingTotal { get; set; }
            public string BillNormal0 { get; set; }
            public string BillNormal1 { get; set; }
            public string BillNormal2 { get; set; }

            public string BillSpecial0 { get; set; }
            public string BillSpecial1 { get; set; }
            public string BillSpecial2 { get; set; }


            //các thuộc tính báo cáo trước 3/2016(hiện tại không còn dùng)
            public string CustomerAvgOfStaff { get; set; }
            public string CustomerOfStaff { get; set; }
            public string SkinnerProductivity { get; set; }
            public string StylelistProductivity { get; set; }
        }

        /// <summary>
        /// Đối tượng lưu trữ các thông tin thống kê theo thời gian
        /// Thông tin thống kê bao gồm: thời gian, sô liệu thống kê, thông tin về chủ thể cần thống kê
        /// </summary>
        public class DataCountByDate
        {
            public int Id { get; set; }
            public string Code { get; set; }
            public DateTime Date { get; set; }
            public int Year { get; set; }
            public int Quarter { get; set; }
            public int Month { get; set; }
            public int Week { get; set; }
            public int Day { get; set; }
            public string DateRe { get; set; }
            public string LularDate { get; set; }
            public int Count { get; set; }
            public int Count2 { get; set; }
            public int Count3 { get; set; }

            public double Sum { get; set; }
            public double Avg { get; set; }
            public double Percent { get; set; }
            public int Number { get; set; }
            public int Thread { get; set; }
        }

        /// <summary>
        /// Thông tin thống kê hóa đơn
        /// </summary>
        public class BillInfo
        {
            public int Visit { get; set; }
            public int Total { get; set; }
            public int Number { get; set; }
        }

        /// <summary>
        /// Thông tin hóa đơn theo giờ
        /// </summary>
        public class DataBillByHours
        {
            public int SalonId { get; set; }
            public string Title { get; set; }
            public string Unit { get; set; }
            public string Count { get; set; }
            public string Number { get; set; }
            public string Hours { get; set; }
        }
        /// <summary>
        /// Thông tin thống kê số lượng theo khoảng thời gian
        /// </summary>
        public class DataCountRange
        {
            public int SalonId { get; set; }
            public string Title { get; set; }
            public string Unit { get; set; }
            public string Count { get; set; }
            public string Number { get; set; }
            public string TimeRange { get; set; }
        }

        /// <summary>
        /// Thông tin về thời gian
        /// </summary>
        public class DateInfo
        {
            public int bills { get; set; }
            public int year { get; set; }
            public int month { get; set; }
            public int day { get; set; }
            public int hour { get; set; }
        }

        /// <summary>
        /// Thông tin khoảng thời gian, bao gồm thời gian bắt đầu và kết thúc của khoảng thời gian
        /// </summary>
        public class DateTimeRange
        {
            public DateTime timeFrom { get; set; }
            public DateTime timeTo { get; set; }
        }
        /// <summary>
        /// Thông tin giờ thời gian 
        /// </summary>
        public class HourInfo
        {
            public string CreadtedDate { get; set; }
            public int h_8 { get; set; }
            public int h_9 { get; set; }
            public int h_10 { get; set; }
            public int h_11 { get; set; }
            public int h_12 { get; set; }
            public int h_13 { get; set; }
            public int h_14 { get; set; }
            public int h_15 { get; set; }
            public int h_16 { get; set; }
            public int h_17 { get; set; }
            public int h_18 { get; set; }
            public int h_19 { get; set; }
            public int h_20 { get; set; }
            public int h_21 { get; set; }
            public int h_22 { get; set; }
        }

        /// <summary>
        /// Thông tin sắp xếp 
        /// </summary>
        public class Sort
        {
            public double Value1 { get; set; }
            public int Value2 { get; set; }
            public int Id { get; set; }
        }

        /// <summary>
        /// Thông tin thông kê đơn giản chỉ bao gồm một thuộc tính Count.
        /// </summary>
        public class DataCount
        {
            public int Count { get; set; }
        }

        /// <summary>
        /// Thông tin về phân loại dịch vụ
        /// </summary>
        public class DataService
        {
            public string Salon { get; set; }
            public string Title { get; set; }
            public string Unit { get; set; }
            public int Number { get; set; }
            public string ShineMaster { get; set; }
            public string ShineCombo { get; set; }
            public string ShineQuick { get; set; }
            public string CutStyle { get; set; }
            public string HairRemove { get; set; }
            public string ComboU { get; set; }
            public string ComboC { get; set; }
        }
        /// <summary>
        /// Thông tin dạng khóa-giá trị, dùng để mô tả chung cho các đối tượng có đặc trưng khóa va giá trị
        /// </summary>
        public class KeyVal
        {
            public int Id { get; set; }
            public string Key { get; set; }
            public string Name { get; set; }
            public string Val { get; set; }
        }


    }
}