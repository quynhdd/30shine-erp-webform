﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity.Migrations;
using System.IO;
using System.Web.Helpers;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Web.Script.Serialization;
using System.Web.Services;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Text;

namespace _30shine.GUI.SystemService.Ajax
{
    public partial class UploadImage : System.Web.UI.Page
    {
        protected UIHelpers _UIHelpers = new UIHelpers();
        public static System.Web.UI.Page _Page = new System.Web.UI.Page();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static string Add_User_Hair(string ImageBase64)
        {
            var Msg = new Msg();
            var serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var base64Data = Regex.Match(ImageBase64, @"data:image/(?<type>.+?),(?<data>.+)").Groups["data"].Value;
                var binData = Convert.FromBase64String(base64Data);

                using (var stream = new MemoryStream(binData))
                {
                    var Img = new Bitmap(stream);
                    var ImgName = Guid.NewGuid().ToString() + ".jpg";
                    var ImgPath = _Page.Server.MapPath("~") + "Public/Media/Upload/Images/User.Hair/" + ImgName;
                    Img.Save(ImgPath);
                    Msg.success = true;
                    Msg.msg = Encoding.UTF8.GetByteCount(ImageBase64).ToString();
                    return serializer.Serialize(Msg);
                }
            }
        }

        [WebMethod]
        public string Delete_User_Hair(string ImageName)
        {
            var Msg = new Msg();
            var serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                // code remove file here
                Msg.success = true;
                return serializer.Serialize(Msg);
            }
        }

        [WebMethod]
        public static string Crop_User_Image(string ImageBase64, string ImgPath, string Code)
        {
            var Msg = new Msg();
            var serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var base64Data = Regex.Match(ImageBase64, @"data:image/(?<type>.+?),(?<data>.+)").Groups["data"].Value;
                var binData = Convert.FromBase64String(base64Data);

                using (var stream = new MemoryStream(binData))
                {
                    var Img = new Bitmap(stream);
                    var ImgPath2 = ChangeFileName(ImgPath, "_crop_");
                    Img.Save(_Page.Server.MapPath("~") + ImgPath2);

                    // Insert to database
                    var OBJ = new Tbl_Media();
                    var obj = db.Tbl_Media.FirstOrDefault(w => w.Value == ImgPath);
                    
                    OBJ.Key = Code;
                    OBJ.Value = ImgPath2;
                    OBJ.Type = 1;                    
                    OBJ.IsDelete = 0;
                    if (obj != null)
                    {
                        OBJ.CreatedDate = obj.CreatedDate;
                    }
                    else
                    {
                        OBJ.CreatedDate = DateTime.Now;
                    }

                    db.Tbl_Media.Add(OBJ);
                    var exc = db.SaveChanges();

                    if (exc > 0)
                    {
                        Msg.success = true;
                        Msg.msg = ImgPath2;
                    }
                    else
                    {
                        Msg.success = false;
                    }
                    
                    return serializer.Serialize(Msg);
                }
            }
        }

        public static string ChangeFileName(string fileName, string chain)
        {
            string temp = "";
            string type = "";
            int chainPos = fileName.IndexOf(chain);
            if (chainPos == -1)
            {
                for (var i = fileName.Length - 1; i >= 0; i--)
                {
                    if (fileName[i] == '.')
                    {
                        temp = fileName.Substring(0, i) + chain + ConvertToUnixTimestamp(DateTime.Now).ToString();
                        break;
                    }
                    else
                    {
                        type = fileName[i] + type;
                    }
                }
            }
            else
            {
                for (var i = fileName.Length - 1; i >= 0; i--)
                {
                    if (fileName[i] != '.')
                    {
                        type = fileName[i] + type;
                    }
                    else
                    {
                        break;
                    }
                }
                temp = fileName.Substring(0, chainPos) + chain + ConvertToUnixTimestamp(DateTime.Now).ToString();
            }

            return temp + "." + type;
        }

        public static double ConvertToUnixTimestamp(DateTime date)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            TimeSpan diff = date - origin;
            return Math.Floor(diff.TotalSeconds);
        }
    }
}