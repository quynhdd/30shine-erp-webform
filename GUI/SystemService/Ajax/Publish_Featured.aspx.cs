﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;

namespace _30shine.GUI.SystemService.Ajax
{
    public partial class Publish_Featured : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static string Publish(int Id, string Table, bool isChecked)
        {
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                switch (Table)
                {
                    case "Product": var product = db.Products.SingleOrDefault(s => s.Id == Id && s.IsDelete != 1);
                        product.Publish = isChecked == true ? Convert.ToByte(1) : Convert.ToByte(0);
                        db.Products.AddOrUpdate(product);
                        break;
                    case "Service": var service = db.Services.SingleOrDefault(s => s.Id == Id && s.IsDelete != 1);
                        service.Publish = isChecked == true ? Convert.ToByte(1) : Convert.ToByte(0);
                        db.Services.AddOrUpdate(service);
                        break;
                    case "Staff": var staff = db.Staffs.SingleOrDefault(s => s.Id == Id && s.IsDelete != 1);
                        staff.Active = isChecked == true ? Convert.ToByte(1) : Convert.ToByte(0);
                        db.Staffs.AddOrUpdate(staff);
                        break;
                    case "Category":
                        var category = db.Tbl_Category.SingleOrDefault(s => s.Id == Id && s.IsDelete != 1);
                        category.Publish = isChecked;
                        db.Tbl_Category.AddOrUpdate(category);
                        break;
                    default: break;
                }                                
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        [WebMethod]
        public static string Featured(int Id, string Table, bool isChecked)
        {
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                switch (Table)
                {
                    case "Product": var product = db.Products.SingleOrDefault(s => s.Id == Id && s.IsDelete != 1);
                        product.Status = isChecked == true ? Convert.ToByte(1) : Convert.ToByte(0);
                        db.Products.AddOrUpdate(product);
                        break;
                    case "Service": var service = db.Services.SingleOrDefault(s => s.Id == Id && s.IsDelete != 1);
                        service.Status = isChecked == true ? Convert.ToByte(1) : Convert.ToByte(0);
                        db.Services.AddOrUpdate(service);
                        break;

                }
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        [WebMethod]
        public static string IsFree(int Id, string Table, bool isChecked)
        {
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                switch (Table)
                {
                    //case "Product": var product = db.Products.SingleOrDefault(s => s.Id == Id && s.IsDelete != 1);
                    //    product.Status = isChecked == true ? Convert.ToByte(1) : Convert.ToByte(0);
                    //    db.Products.AddOrUpdate(product);
                    //    break;
                    case "Service": var service = db.Services.SingleOrDefault(s => s.Id == Id && s.IsDelete != 1);
                        service.IsFreeService = isChecked == true ? Convert.ToByte(1) : Convert.ToByte(0);
                        db.Services.AddOrUpdate(service);
                        break;

                }
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }
    }
}