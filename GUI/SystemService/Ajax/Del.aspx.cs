﻿using _30shine.Helpers;
using _30shine.Helpers.Http;
using _30shine.MODEL.CustomClass;
using _30shine.MODEL.ENTITY.EDMX;
using Libraries;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using Libraries;
using Newtonsoft.Json;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.SystemService.Ajax
{
    public partial class Del : System.Web.UI.Page
    {
        private static Del instance;

        /// <summary>
        /// get instance, phục vụ trường hợp có hàm statistic, sử dụng instance có thẻ gọi các method không phải static
        /// </summary>
        /// <returns></returns>
        public static Del getInstance()
        {
            if (!(Del.instance is Del))
            {
                Del.instance = new Del();
            }

            return Del.instance;
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static string Delele_Mistake(int idMistake, int idFormMistake)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                Staff_Mistake OBJ = new Staff_Mistake();
                FlowSalary objFlowSalary = new FlowSalary();
                try
                {
                    OBJ = db.Staff_Mistake.SingleOrDefault(s => s.Id == idMistake && s.IsDelete != true);
                    if (OBJ != null)
                    {
                        OBJ.IsDelete = true;
                    }

                    objFlowSalary = db.FlowSalaries.Where(o => o.staffId == OBJ.StaffId && o.sDate == OBJ.CreateDate).FirstOrDefault();
                    if (objFlowSalary != null)
                    {
                        switch (idFormMistake)
                        {
                            case 1:
                                objFlowSalary.Mistake_Point = objFlowSalary.Mistake_Point - (OBJ.Point);
                                break;
                            case 3:
                                objFlowSalary.MistakeTemporary = objFlowSalary.MistakeTemporary - OBJ.Point;
                                break;
                            case 4:
                                objFlowSalary.MistakeOrther = objFlowSalary.MistakeOrther - OBJ.Point;
                                break;
                            case 5:
                                objFlowSalary.MistakeInsurrance = objFlowSalary.MistakeInsurrance - OBJ.Point;
                                break;
                            case 7:
                                objFlowSalary.MistakeExtra = objFlowSalary.MistakeExtra - OBJ.Point;
                                break;
                        }
                    }
                    var exc = db.SaveChanges();

                    if (exc > 0)
                    {
                        Msg.success = true;
                    }
                    else
                    {
                        Msg.success = false;
                    }

                }
                catch (Exception ex) { }

            }
            return serializer.Serialize(Msg);
        }

        [WebMethod]
        public static string Delele_SkinAtt_Product(int Code)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var OBJ = db.Skin_Attribute_Product.SingleOrDefault(s => s.Id == Code && s.IsDelete != true);
                OBJ.IsDelete = true;
                db.Skin_Attribute_Product.AddOrUpdate(OBJ);
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        [WebMethod]
        public static string Delele_HairAtt_Product(int Code)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var OBJ = db.Hair_Attribute_Product.SingleOrDefault(s => s.Id == Code && s.IsDelete != true);
                OBJ.IsDelete = true;
                db.Hair_Attribute_Product.AddOrUpdate(OBJ);
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        [WebMethod]
        public static string Delele_SkinAtt(int Code)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var OBJ = db.Skin_Attribute.SingleOrDefault(s => s.Id == Code && s.IsDelete != true);
                OBJ.IsDelete = true;
                db.Skin_Attribute.AddOrUpdate(OBJ);
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        [WebMethod]
        public static string Delele_HairAtt(int Code)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var OBJ = db.Hair_Attribute.SingleOrDefault(s => s.Id == Code && s.IsDelete != true);
                OBJ.IsDelete = true;
                db.Hair_Attribute.AddOrUpdate(OBJ);
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        [WebMethod]
        public static string Delele_Custumer(string Code)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var _Customer = db.Customers.SingleOrDefault(s => s.Customer_Code == Code && s.IsDelete != 1);
                _Customer.IsDelete = 1;
                db.Customers.AddOrUpdate(_Customer);
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        [WebMethod]
        public static string Delele_SocialThread(string Code)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var _Code = Convert.ToInt32(Code);
                var OBJ = db.SocialThreads.SingleOrDefault(s => s.Id == _Code && s.IsDelete != 1);
                OBJ.IsDelete = 1;
                db.SocialThreads.AddOrUpdate(OBJ);
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        [WebMethod]
        public static string Delele_Staff(string Code)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var _Code = Convert.ToInt32(Code);
                var OBJ = db.Staffs.FirstOrDefault(s => s.Id == _Code && s.IsDelete != 1);
                
                var user = db.Users.FirstOrDefault(s => s.StaffId == _Code && s.IsDelete == false);

                if (OBJ != null && user != null)
                {
                    IAuthenticationModel authenticationModel = new AuthenticationModel();

                    authenticationModel.DeleteOrDisableMapUserStaff(OBJ, true, 1);

                    authenticationModel.AdminDeleteUser(OBJ.Email);

                    OBJ.IsDelete = 1;
                    db.Staffs.AddOrUpdate(OBJ);

                    user.IsActive = false;
                    user.IsDelete = true;
                    db.Users.AddOrUpdate(user);

                    var exc = db.SaveChanges();
                    if (exc > 0)
                    {
                        Msg.success = true;
                    }
                    else
                    {
                        Msg.success = false;
                    }
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        [WebMethod]
        public static string Delele_StaffFluctuations(string Code)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var _Code = Convert.ToInt32(Code);
                var OBJ = db.Staff_Fluctuations.SingleOrDefault(s => s.Id == _Code && s.IsDetele != true);
                OBJ.IsDetele = true;
                db.Staff_Fluctuations.AddOrUpdate(OBJ);
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        [WebMethod]
        public static string Delele_TuyenDungUV(string Code)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var _Code = Convert.ToInt32(Code);
                var OBJ = db.TuyenDung_UngVien.SingleOrDefault(s => s.Id == _Code && s.IsDelete != true);
                OBJ.IsDelete = true;
                db.TuyenDung_UngVien.AddOrUpdate(OBJ);
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }
        [WebMethod]
        public static string Delele_Product(string Code)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var OBJ = db.Products.SingleOrDefault(s => s.Code == Code && s.IsDelete != 1);
                OBJ.IsDelete = 1;
                db.Products.AddOrUpdate(OBJ);
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        [WebMethod]
        public static string Delele_Service(string Code)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var OBJ = db.Services.SingleOrDefault(s => s.Code == Code && s.IsDelete != 1);
                OBJ.IsDelete = 1;
                db.Services.AddOrUpdate(OBJ);
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        [WebMethod]
        public static string Delele_Bill(int Id)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var _Id = Convert.ToInt32(Id);
                    var OBJ = db.BillServices.SingleOrDefault(s => s.Id == Id && s.IsDelete == 0);
                    BillService OBJStatic = OBJ;
                    if (OBJStatic != null)
                    {
                        ReflectionExtension.CopyObjectValue(OBJ, OBJStatic);
                    }
                    OBJ.IsDelete = 1;
                    //Check xem có sửa lần 2 không?
                    if (OBJ.CompleteBillTime != null && OBJ.Pending == 0)
                    {
                        var del = new Request().RunDeleteAsyncV1(AppConstants.URL_API_CHECKOUT + "/api/salary?billId=" + OBJ.Id).Result;

                        //Update Customer
                        var objCus = db.Customers.FirstOrDefault(a => a.Id == OBJ.CustomerId);
                        if (objCus != null)
                        {
                            if (objCus.TotalBillservice > 0)
                            {
                                objCus.IsOld = true;
                                objCus.Veryfied = true;
                            }
                            objCus.TotalBillservice = objCus.TotalBillservice - 1;
                            db.Customers.AddOrUpdate(objCus);
                        }
                        //Update CustomerUsedNumber
                        db.Database.ExecuteSqlCommand("UPDATE dbo.BillService SET CustomerUsedNumber = CustomerUsedNumber - 1 WHERE Id > '" + OBJ.Id + "' And Pending = 0 AND IsDelete = 0 and CustomerUsedNumber IS NOT NULL AND CustomerId = '" + OBJ.CustomerId + "'");
                    }
                    if (OBJ.ServiceIds != "" && OBJ.ServiceIds != null)
                    {
                        var ServiceList = serializer.Deserialize<List<ProductBasic>>(OBJ.ServiceIds).ToList();
                        foreach (var item in ServiceList)
                        {
                            FlowService service = new FlowService();
                            service.Quantity = item.Quantity;
                            service.Price = item.Price;
                            service.VoucherPercent = item.VoucherPercent;
                            service.ServiceId = item.Id;
                            // Cap nhat staticSalaryServices
                            if (OBJ.Staff_Hairdresser_Id != null && OBJ.Staff_Hairdresser_Id > 0)
                            {
                                DateTime workDate = OBJ.CreatedDate.Value.Date;
                                var statisticRecord = db.StatisticSalaryServices.FirstOrDefault(w =>
                                    w.SalonId == OBJ.SalonId && w.StaffId == OBJ.Staff_Hairdresser_Id &&
                                    w.ServiceIds == service.ServiceId && w.WorkDate == workDate);
                                if (statisticRecord != null)
                                {
                                    statisticRecord.Total -= service.Quantity;
                                    statisticRecord.TotalMoney -=
                                        service.Quantity * service.Price * (100 - service.VoucherPercent) / 100;
                                    statisticRecord.ModifiedTime = DateTime.Now;
                                    db.StatisticSalaryServices.AddOrUpdate(statisticRecord);
                                    //db.SaveChanges();
                                }
                            }
                            if (OBJ.Staff_HairMassage_Id != null && OBJ.Staff_HairMassage_Id > 0)
                            {
                                DateTime workDate = OBJ.CreatedDate.Value.Date;
                                var statisticRecord = db.StatisticSalaryServices.FirstOrDefault(w =>
                                    w.SalonId == OBJ.SalonId && w.StaffId == OBJ.Staff_HairMassage_Id &&
                                    w.ServiceIds == service.ServiceId && w.WorkDate == workDate);
                                if (statisticRecord != null)
                                {
                                    statisticRecord.Total -= service.Quantity;
                                    statisticRecord.TotalMoney -=
                                        service.Quantity * service.Price * (100 - service.VoucherPercent) / 100;
                                    statisticRecord.ModifiedTime = DateTime.Now;
                                    db.StatisticSalaryServices.AddOrUpdate(statisticRecord);
                                }
                            }
                        }
                        //db.SaveChanges();
                        //DeleteStatisticService(OBJ, service);
                    }
                    if (OBJ.ProductIds != "" && OBJ.ProductIds != null)
                    {
                        var ProductList = serializer.Deserialize<List<ProductBasic>>(OBJ.ProductIds).ToList();
                        foreach (var item in ProductList)
                        {
                            FlowProduct product = new FlowProduct();
                            product.Quantity = item.Quantity;
                            product.Price = item.Price;
                            product.VoucherPercent = item.VoucherPercent;
                            product.ProductId = item.Id;
                            // cap nhat xoa flowProduct
                            DateTime workDate = OBJ.CreatedDate.Value.Date;
                            var statisticRecord = db.StatisticSalaryProducts.FirstOrDefault(w =>
                                w.SalonId == OBJ.SalonId && w.StaffId == OBJ.SellerId &&
                                w.ProductIds == product.ProductId && w.WorkDate == workDate);
                            if (statisticRecord != null)
                            {
                                statisticRecord.Total -= product.Quantity;
                                statisticRecord.TotalMoney -= product.Quantity * product.Price *
                                                              (100 - product.VoucherPercent) / 100;
                                statisticRecord.ModifiedTime = DateTime.Now;
                                db.StatisticSalaryProducts.AddOrUpdate(statisticRecord);
                                //db.SaveChanges();
                            }
                            //DeleteStatisticProduct(OBJ, product);
                        }
                    }

                    var Flow_Product = db.FlowProducts.Where(w => w.BillId == _Id).ToList();
                    if (Flow_Product.Count > 0)
                    {
                        for (var i = 0; i < Flow_Product.Count; i++)
                        {
                            Flow_Product[i].IsDelete = 1;
                            db.FlowProducts.AddOrUpdate(Flow_Product[i]);
                        }
                    }

                    var Flow_Service = db.FlowServices.Where(w => w.BillId == _Id).ToList();
                    if (Flow_Service.Count > 0)
                    {
                        for (var i = 0; i < Flow_Service.Count; i++)
                        {
                            Flow_Service[i].IsDelete = 1;
                            db.FlowServices.AddOrUpdate(Flow_Service[i]);
                        }
                    }
                    // update booking
                    var objBooking = db.Bookings.FirstOrDefault(a => a.Id == OBJ.BookingId);
                    var objBookingTemp = new SM_BookingTemp();
                    if (objBooking != null)
                    {
                        objBooking.IsDelete = 1;
                        objBookingTemp = db.SM_BookingTemp.FirstOrDefault(a => a.Id == objBooking.BookingTempId);
                        if (objBookingTemp != null)
                        {
                            objBookingTemp.IsDelete = 1;
                            //db.SM_BookingTemp.AddOrUpdate(objBookingTemp);
                        }
                        //db.Bookings.AddOrUpdate(objBooking);
                    }
                    db.BillServices.AddOrUpdate(OBJ);
                    // Cap nhat xoa so sao
                    var objRatingDetail = db.RatingDetails.Where(r => r.BillId == OBJ.Id).FirstOrDefault();
                    if (objRatingDetail != null)
                    {
                        objRatingDetail.Isdelete = true;
                        db.RatingDetails.AddOrUpdate(objRatingDetail);
                    }
                    //cap nhat his
                    //var billHis = db.BillServiceHis.SingleOrDefault(s => s.Id == Id && s.IsDelete == 0);
                    BillServiceHi billHis = new BillServiceHi();
                    ReflectionExtension.CopyObjectValue(OBJ, billHis);
                    if (billHis != null)
                    {
                        billHis.IsDelete = 1;
                        db.BillServiceHis.AddOrUpdate(billHis);
                    }

                    // Cập nhật xóa số sao
                    var exc = db.SaveChanges();

                    if (exc >= 0)
                    {
                        //Xoa ban ghi MktCampaignBill (xóa thông tin chiến dịch makerting có trong bill)
                        Task taskCampaign = Task.Run(async () =>
                        {
                            await new Request().RunDeleteAsyncV1(Libraries.AppConstants.URL_API_BILL_SERVICE + "/api/mkt-campaign-bill?BillId=" + OBJ.Id + "&CustomerId=" + OBJ.CustomerId);
                        });
                        //================
                        //Cap nhat tru StaticRatingWaitTime
                        Task StaticRatingWaitTime = Task.Run(async () =>
                        {
                            await new Request().RunDeleteAsyncV1(Libraries.AppConstants.URL_API_CHECKOUT + "/api/static-rating-waittime?billId=" + OBJ.Id);
                        });

                        //xoa membership neu co
                        Task updateMembership = Task.Run(async () =>
                        {
                            await new Request().RunPutAsync(Libraries.AppConstants.URL_API_CHECKOUT + "/api/customer", new
                            {
                                StaffId = OBJ.Staff_Hairdresser_Id ?? 0,
                                SalonId = OBJ.SalonId ?? 0,
                                CustomerId = OBJ.CustomerId,
                                TotalMoneyBillNew = 0,
                                TotalMoneyBillOld = 0,
                                CustomerUsedNumber = 0,
                                BillCreatedDate = OBJ.CreatedDate,
                                ServiceIds = "",
                                ProductIds = "",
                                BillId = OBJ.Id,
                                Pending = 0
                            });
                        });
                        //xoa inventory
                        if (OBJ.Pending == 0)
                        {
                            var data = new
                            {
                                BillId = OBJ.Id,
                                StylistId = 0,
                                StylistIdOld = OBJ.Staff_Hairdresser_Id ?? 0,
                                SkinnerId = 0,
                                SkinnerIdOld = OBJ.Staff_HairMassage_Id ?? 0,
                                SalonId = OBJ.SalonId ?? 0,
                                JsonProductIds = "",
                                JsonProductIdsOld = OBJ.ProductIds,
                                JsonServiceIds = "",
                                JsonServiceIdsOld = OBJ.ServiceIds,
                                Pending = OBJ.Pending,

                            };
                            //cap nhat salon
                            var updateInventoryResult = new Request().RunPutAsync(Libraries.AppConstants.URL_API_INVENTORY + "/api/iv-inventory-current/checkoutSalon", data);
                            if (!updateInventoryResult.Result.IsSuccessStatusCode)
                            {
                                new Helpers.PushNotiErrorToSlack().PushDefault(" MAIN: update inventory salon fail, BillId= " + Library.Function.JavaScript.Serialize(Id),
                                                          ".Delele_Bill, input: " + JsonConvert.SerializeObject(data), "tuanns");
                            }

                            //cap nhat staff
                            updateInventoryResult = new Request().RunPutAsync(Libraries.AppConstants.URL_API_INVENTORY + "/api/iv-inventory-current/checkoutStaff", data);
                            if (!updateInventoryResult.Result.IsSuccessStatusCode)
                            {
                                new Helpers.PushNotiErrorToSlack().PushDefault(" MAIN: update inventory staff fail, BillId= " + Library.Function.JavaScript.Serialize(Id),
                                                          ".Delele_Bill, input: " + JsonConvert.SerializeObject(data), "tuanns");
                            }
                            //dong bo dynamo
                            var billData = new
                            {
                                ListBillId = Id
                            };
                            var updateDynamoResult = new Request().RunPostAsync(Libraries.AppConstants.URL_API_CHECKOUT + "/api/SyncDynamo/ReSync", billData);
                            if (!updateDynamoResult.Result.IsSuccessStatusCode)
                            {
                                new Helpers.PushNotiErrorToSlack().PushDefault(" MAIN: update dynamo fail, BillId= " + Library.Function.JavaScript.Serialize(Id),
                                                          ".Delele_Bill, input: " + JsonConvert.SerializeObject(data), "tuanns");
                            }
                        }
                        Msg.success = true;
                    }
                    else
                    {
                        Msg.success = false;
                    }
                }
            }
            catch (Exception e)
            {
                // Push thông báo cho mọi người(by slack) khi có lỗi xảy ra
                new Helpers.PushNotiErrorToSlack().PushDefault(e.Message + ", MAIN: " + e.StackTrace + ", BillId= " + Library.Function.JavaScript.Serialize(Id),
                                                        ".Delele_Bill", "phucdn");
            }
            return serializer.Serialize(Msg);
        }
        /// NhatAV
        /// <summary>
        /// UpdateStatistic_Delete_Bill
        /// </summary>
        /// <param name="Staff_ID"></param>
        /// <param name="ComPleteTime"></param>        
        public static void UpdateStatistic_Delete_Bill(int? Staff_ID, DateTime? ComPleteTime)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    // lay  obj Xuat vat tu gan nhat nhan vien cu
                    var ObjExportGoodsOld = db.ExportGoods.Where(a => a.IsDelete == 0 && a.RecipientId == Staff_ID).
                                                                OrderByDescending(a => a.CreatedDate).OrderByDescending(a => a.Id).
                                                                Select(a => new { a.Id }).FirstOrDefault();
                    // Lay danh sach Tatistic Xuat vat tu
                    if (ObjExportGoodsOld != null)
                    {
                        var LstSatisticXuatVatTuOld = db.Statistics_XuatVatTu.Where(a => a.IsDelete == false &&
                                                                                       a.ExportGoods_ID == ObjExportGoodsOld.Id &&
                                                                                       a.Date_XuatVatTu < ComPleteTime).ToList();
                        var _CountLSTOld = LstSatisticXuatVatTuOld.Count;
                        if (_CountLSTOld > 0)
                        {
                            for (int i = 0; i < _CountLSTOld; i++)
                            {
                                LstSatisticXuatVatTuOld[i].TotalBill_XuatVatTu = LstSatisticXuatVatTuOld[i].TotalBill_XuatVatTu - 1;
                                db.SaveChanges();
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Delete StatisticSalarySevice
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="flowService"></param>
        public static void DeleteStatisticService(BillService bill, FlowService flowService)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    if (bill.Staff_Hairdresser_Id != null && bill.Staff_Hairdresser_Id > 0)
                    {
                        DateTime workDate = bill.CreatedDate.Value.Date;
                        var statisticRecord = db.StatisticSalaryServices.FirstOrDefault(w =>
                            w.SalonId == bill.SalonId && w.StaffId == bill.Staff_Hairdresser_Id &&
                            w.ServiceIds == flowService.ServiceId && w.WorkDate == workDate);
                        if (statisticRecord != null)
                        {
                            statisticRecord.Total -= flowService.Quantity;
                            statisticRecord.TotalMoney -=
                                flowService.Quantity * flowService.Price * (100 - flowService.VoucherPercent) / 100;
                            statisticRecord.ModifiedTime = DateTime.Now;
                            db.StatisticSalaryServices.AddOrUpdate(statisticRecord);
                            db.SaveChanges();
                        }
                    }

                    if (bill.Staff_HairMassage_Id != null && bill.Staff_HairMassage_Id > 0)
                    {
                        DateTime workDate = bill.CreatedDate.Value.Date;
                        var statisticRecord = db.StatisticSalaryServices.FirstOrDefault(w =>
                            w.SalonId == bill.SalonId && w.StaffId == bill.Staff_HairMassage_Id &&
                            w.ServiceIds == flowService.ServiceId && w.WorkDate == workDate);
                        if (statisticRecord != null)
                        {
                            statisticRecord.Total -= flowService.Quantity;
                            statisticRecord.TotalMoney -=
                                flowService.Quantity * flowService.Price * (100 - flowService.VoucherPercent) / 100;
                            statisticRecord.ModifiedTime = DateTime.Now;
                            db.StatisticSalaryServices.AddOrUpdate(statisticRecord);
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //throw;
            }
        }

        /// <summary>
        /// Delete StatisticSalaryProduct
        /// </summary>
        /// <param name="bill"></param>
        /// <param name="flowProduct"></param>
        public static void DeleteStatisticProduct(BillService bill, FlowProduct flowProduct)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    DateTime workDate = bill.CreatedDate.Value.Date;
                    var statisticRecord = db.StatisticSalaryProducts.FirstOrDefault(w =>
                        w.SalonId == bill.SalonId && w.StaffId == bill.SellerId &&
                        w.ProductIds == flowProduct.ProductId && w.WorkDate == workDate);
                    if (statisticRecord != null)
                    {
                        statisticRecord.Total -= flowProduct.Quantity;
                        statisticRecord.TotalMoney -= flowProduct.Quantity * flowProduct.Price *
                                                      (100 - flowProduct.VoucherPercent) / 100;
                        statisticRecord.ModifiedTime = DateTime.Now;
                        db.StatisticSalaryProducts.AddOrUpdate(statisticRecord);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                //throw;
            }
        }

        [WebMethod]
        public static string Delele_Bill_Product_Online(int Id)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var _Id = Convert.ToInt32(Id);
                // var OBJ = db.BillServices.SingleOrDefault(s => s.Id == _Code && s.IsDelete != 1);
                var sql = @" select * from BillService where Id='" + _Id + "'";
                var OBJ = db.Database.SqlQuery<BillService>(sql).FirstOrDefault();
                OBJ.IsDelete = 1;
                db.BillServices.AddOrUpdate(OBJ);

                var Flow_Product = db.FlowProducts.Where(w => w.BillId == _Id).ToList();
                if (Flow_Product.Count > 0)
                {
                    for (var i = 0; i < Flow_Product.Count; i++)
                    {
                        Flow_Product[i].IsDelete = 1;
                        db.FlowProducts.AddOrUpdate(Flow_Product[i]);
                    }
                }

                var Flow_Service = db.FlowServices.Where(w => w.BillId == _Id).ToList();
                if (Flow_Service.Count > 0)
                {
                    for (var i = 0; i < Flow_Service.Count; i++)
                    {
                        Flow_Service[i].IsDelete = 1;
                        db.FlowServices.AddOrUpdate(Flow_Service[i]);
                    }
                }

                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }

            }
            return serializer.Serialize(Msg);
        }

        [WebMethod]
        public static string Delele_StaffServey(int Code)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var OBJ = db.Tbl_Staff_Survey.SingleOrDefault(s => s.Id == Code && s.IsDelete != 1);
                OBJ.IsDelete = 1;
                db.Tbl_Staff_Survey.AddOrUpdate(OBJ);
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        [WebMethod]
        public static string Delele_Item_Pending(int itemId, int BillId, string itemType)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var exc = true;
            using (var db = new Solution_30shineEntities())
            {
                var OBJ = db.BillServices.SingleOrDefault(s => s.Id == BillId && s.IsDelete != 1);
                if (OBJ != null)
                {
                    if (itemType == "service")
                    {
                        var itemList = serializer.Deserialize<List<ProductBasic>>(OBJ.ServiceIds);
                        if (itemList != null)
                        {
                            var index = itemList.FindIndex(fi => fi.Id == itemId);
                            if (index != -1)
                            {
                                itemList.RemoveAt(index);
                            }
                        }
                        var itemListString = serializer.Serialize(itemList);
                        if (itemListString != null)
                        {
                            OBJ.ServiceIds = itemListString;
                            db.BillServices.AddOrUpdate(OBJ);
                            exc = db.SaveChanges() > 0 ? true : false;
                        }
                        else
                        {
                            OBJ.ServiceIds = "";
                            db.BillServices.AddOrUpdate(OBJ);
                            exc = db.SaveChanges() > 0 ? true : false;
                        }

                        var item = db.FlowServices.FirstOrDefault(w => w.BillId == BillId && w.ServiceId == itemId);
                        if (item != null)
                        {
                            item.IsDelete = 1;
                            db.FlowServices.AddOrUpdate(item);
                            exc = db.SaveChanges() > 0 ? true : false;
                        }
                    }

                    if (itemType == "product")
                    {
                        var itemList = serializer.Deserialize<List<ProductBasic>>(OBJ.ProductIds);
                        if (itemList != null)
                        {
                            var index = itemList.FindIndex(fi => fi.Id == itemId);
                            if (index != -1)
                            {
                                itemList.RemoveAt(index);
                            }
                        }
                        var itemListString = serializer.Serialize(itemList);
                        if (itemListString != null)
                        {
                            OBJ.ProductIds = itemListString;
                            db.BillServices.AddOrUpdate(OBJ);
                            exc = db.SaveChanges() > 0 ? true : false;
                        }
                        else
                        {
                            OBJ.ServiceIds = "";
                            db.BillServices.AddOrUpdate(OBJ);
                            exc = db.SaveChanges() > 0 ? true : false;
                        }

                        var item = db.FlowProducts.FirstOrDefault(w => w.BillId == BillId && w.ProductId == itemId);
                        if (item != null)
                        {
                            item.IsDelete = 1;
                            db.FlowProducts.AddOrUpdate(item);
                            exc = db.SaveChanges() > 0 ? true : false;
                        }
                    }
                }

                if (exc)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        [WebMethod]
        public static string Delele_ExportGoods(int Code)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var error = 0;
                var OBJ = db.ExportGoods.FirstOrDefault(s => s.Id == Code && s.IsDelete != 1);
                if (OBJ != null)
                {
                    OBJ.IsDelete = 1;
                    db.ExportGoods.AddOrUpdate(OBJ);

                    error += db.SaveChanges() > 0 ? 0 : 1;
                    // update FlowGoods
                    if (error == 0)
                    {
                        var flow = db.FlowGoods.Where(w => w.ExportGoodsId == Code && w.IsDelete != 1).ToList();
                        if (flow.Count > 0)
                        {
                            foreach (var v in flow)
                            {
                                v.IsDelete = 1;
                                db.FlowGoods.AddOrUpdate(v);
                                error += db.SaveChanges() > 0 ? 0 : 1;
                            }
                        }
                        var export = db.Statistics_XuatVatTu.Where(w => w.ExportGoods_ID == Code && w.IsDelete == false).ToList();
                        if (export.Count > 0)
                        {
                            foreach (var v in export)
                            {
                                v.IsDelete = true;
                                db.Statistics_XuatVatTu.AddOrUpdate(v);
                                db.SaveChanges();
                            }
                        }
                    }
                }

                if (error == 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        [WebMethod]
        public static string Delele_Inventory_Import(int Code)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var error = 0;
                var OBJ = db.Inventory_Import.FirstOrDefault(s => s.Id == Code);
                if (OBJ != null)
                {
                    OBJ.IsDelete = true;
                    db.Inventory_Import.AddOrUpdate(OBJ);
                    error += db.SaveChanges() > 0 ? 0 : 1;
                    // update Inventory_Flow
                    if (error == 0)
                    {
                        var flow = db.Inventory_Flow.Where(w => w.InvenId == Code && w.IsDelete != true).ToList();
                        if (flow.Count > 0)
                        {
                            foreach (var v in flow)
                            {
                                v.IsDelete = true;
                                db.Inventory_Flow.AddOrUpdate(v);
                                error += db.SaveChanges() > 0 ? 0 : 1;
                            }
                        }
                    }
                    // Cập nhật tồn kho
                    Library.Function.updateInventoryByItemUpdate(db, OBJ.ImportDate.Value.Date, OBJ.SalonId.Value, OBJ.Id, OBJ.ImportType.Value, true);
                }

                if (error == 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        [WebMethod]
        public static string Delele_Category(int Code)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var OBJ = db.Tbl_Category.SingleOrDefault(s => s.Id == Code && s.IsDelete != 1);
                OBJ.IsDelete = 1;
                db.Tbl_Category.AddOrUpdate(OBJ);
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        [WebMethod]
        public static string Delele_SkillLevel(int Code)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var OBJ = db.Tbl_SkillLevel.SingleOrDefault(s => s.Id == Code && s.IsDelete != 1);
                OBJ.IsDelete = 1;
                db.Tbl_SkillLevel.AddOrUpdate(OBJ);
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }


        [WebMethod]
        public static string Delele_Skill(int Code)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var OBJ = db.TuyenDung_Skill.SingleOrDefault(s => s.Id == Code && s.IsDelete != true);
                OBJ.IsDelete = true;
                db.TuyenDung_Skill.AddOrUpdate(OBJ);
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        [WebMethod]
        public static string Delele_Tester(int Code)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var _OBJ = db.TuyenDung_NguoiTest.SingleOrDefault(s => s.Id == Code && s.IsDelete == false);
                _OBJ.IsDelete = true;
                db.TuyenDung_NguoiTest.AddOrUpdate(_OBJ);
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }
        /// <summary>
        /// Xóa khoản thu/chi thường xuyên | Ngân sách
        /// </summary>
        /// <param name="Code"></param>
        /// <returns></returns>
        [WebMethod]
        public static string Delele_Fund_OffenItem(int Code)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var OBJ = db.Fund_OffenItem.FirstOrDefault(s => s.Id == Code);
                OBJ.IsDelete = true;
                db.Fund_OffenItem.AddOrUpdate(OBJ);
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        /// <summary>
        /// Xóa dòng nhập thu chi
        /// </summary>
        /// <param name="Code"></param>
        /// <returns></returns>
        [WebMethod]
        public static string Delele_Fund_Import(int Code)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var OBJ = db.Fund_Import.FirstOrDefault(s => s.Id == Code);
                OBJ.IsDelete = true;
                db.Fund_Import.AddOrUpdate(OBJ);
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        /// <summary>
        /// Xóa bản ghi lớp học
        /// </summary>
        /// <param name="Code"></param>
        /// <returns></returns>
        [WebMethod]
        public static string Delele_S4M_Class(int Code)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var OBJ = db.Staffs.FirstOrDefault(s => s.Id == Code);
                if (OBJ != null)
                {
                    OBJ.IsDelete = 1;
                    db.Staffs.AddOrUpdate(OBJ);
                    var exc = db.SaveChanges();

                    if (exc > 0)
                    {
                        Msg.success = true;
                    }
                    else
                    {
                        Msg.success = false;
                    }
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        /// <summary>
        /// Xóa bản ghi học viên
        /// </summary>
        /// <param name="Code"></param>
        /// <returns></returns>
        [WebMethod]
        public static string Delele_S4M_Student(int Code)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var OBJ = db.Staffs.FirstOrDefault(s => s.Id == Code);
                if (OBJ != null)
                {
                    OBJ.IsDelete = 1;
                    db.Staffs.AddOrUpdate(OBJ);
                    var exc = db.SaveChanges();

                    if (exc > 0)
                    {
                        Msg.success = true;
                    }
                    else
                    {
                        Msg.success = false;
                    }
                }
                else
                {
                    Msg.success = false;
                    Msg.msg = "Lỗi. Không tồn tại bản ghi";
                }
            }
            return serializer.Serialize(Msg);
        }

        [WebMethod]
        public static string Delele_Bill_QLKho(int Id)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var _Id = Convert.ToInt32(Id);
                var OBJ = db.QLKho_SalonOrder.SingleOrDefault(s => s.Id == Id && s.IsDelete != true);
                //var sql = @" select * from BillService where Id='" + _Id + "'";
                //var OBJ = db.Database.SqlQuery<BillService>(sql).FirstOrDefault();
                OBJ.IsDelete = true;
                db.QLKho_SalonOrder.AddOrUpdate(OBJ);

                var QLKho_SalonOrder_Flow = db.QLKho_SalonOrder_Flow.Where(w => w.OrderId == _Id).ToList();
                if (QLKho_SalonOrder_Flow.Count > 0)
                {
                    for (var i = 0; i < QLKho_SalonOrder_Flow.Count; i++)
                    {
                        QLKho_SalonOrder_Flow[i].IsDelete = true;
                        db.QLKho_SalonOrder_Flow.AddOrUpdate(QLKho_SalonOrder_Flow[i]);
                    }
                }

                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

    }
    struct Msg
    {
        public bool success { get; set; }
        public string msg { get; set; }
    }
}