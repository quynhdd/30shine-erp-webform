﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Data.Linq.SqlClient;

namespace _30shine.GUI.SystemService.Ajax
{
    public partial class TimeWaitting : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public int GetWaitTime(BillService bs)
        {
            var WaitTime = 0;
            try
            {
                var db = new Solution_30shineEntities();
                var book = db.Bookings.FirstOrDefault(f => f.Id == bs.BookingId);
                if (book != null && book.IsBookOnline == true)
                {
                    DateTime? pvgTime = bs.InProcedureTime;
                    DateTime? checkinTime = bs.CreatedDate;
                    DateTime bookHour = new DateTime();
                    var hour = db.BookHours.FirstOrDefault(w => w.Id == book.HourId);

                    if (bs.CreatedDate == null)
                    {
                        throw new Exception("Loi thoi gian checkin");
                    }

                    if (bs.InProcedureTime == null)
                    {
                        throw new Exception("Loi thoi gian goi");
                    }

                    if (book.CreatedDate == null)
                    {
                        throw new Exception("Loi thoi gian checkin");
                    }

                    if (hour != null && hour.HourFrame != null)
                    {
                        bookHour = book.CreatedDate.Value.Date.Add(hour.HourFrame.Value);
                    }
                    else
                    {
                        throw new Exception("Loi gio book.");
                    }

                    if (checkinTime >= bookHour)
                    {
                        WaitTime = ((DateTime)pvgTime - (DateTime)checkinTime).Minutes;

                    }
                    else
                    {
                        WaitTime = ((DateTime)pvgTime - (DateTime)bookHour).Minutes;

                    }
                }

            }
            catch (Exception)
            {
                throw new Exception("Error");
            }

            return WaitTime;
        }

    }
}