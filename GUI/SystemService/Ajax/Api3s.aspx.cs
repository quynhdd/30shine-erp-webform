﻿using _30shine.MODEL;
using _30shine.MODEL.ENTITY.EDMX;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Services;

namespace _30shine.GUI.SystemService.Ajax
{
    public partial class Api3s : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static List<StaffView> GetStaff(int id)
        {
            using (var db = new Solution_30shineEntities())
            {
                var sql = @"SELECT id, 
                            fullName
                            FROM STAFF
                            WHERE SalonId =" + id;
                var list = db.Database.SqlQuery<StaffView>(sql).ToList();
                return list;
            }
        }

        [WebMethod]
        public static List<ItemCheckView> GetItemCheck(int id)
        {
            using (var db = new Solution_30shineEntities())
            {
                var query = @"SELECT id, name FROM [ERP.ItemCheck] WHERE category=" + id + " ORDER BY Id";
                var list = db.Database.SqlQuery<ItemCheckView>(query).ToList();
                return list;
            }
        }

        [WebMethod]
        public static void SaveCheck3S(string obj)
        {
            CheckList3SView _LstCheckItem = (CheckList3SView)JsonConvert.DeserializeObject(obj, typeof(CheckList3SView));

            using (var db = new Solution_30shineEntities())
            {
                //insert list item
                var datetime = DateTime.Now;
                var list = _LstCheckItem.list;
                var chItem = new ERP_Check3S();
                int integer;
                foreach (CheckItem3SView o in list)
                {
                    if (!o.isOk)
                    {
                        chItem = new ERP_Check3S();
                        chItem.ItemId = o.itemId;
                        chItem.Comment = o.comment;
                        chItem.Image = o.image;
                        chItem.StaffId = o.staffId;
                        chItem.IsOk = o.isOk;
                        chItem.SalonId = _LstCheckItem.salonId;
                        chItem.DateTime = datetime;
                        chItem.Description = _LstCheckItem.description;
                        chItem.CreatorId = _LstCheckItem.creatorId;

                        db.ERP_Check3S.Add(chItem);
                        db.SaveChanges();
                    }
                }

            }
        }

        [WebMethod]
        public static void SaveCheckCSVC(string obj)
        {
            CheckListCSVCView _LstCheckItem = (CheckListCSVCView)JsonConvert.DeserializeObject(obj, typeof(CheckListCSVCView));
            using (var db = new Solution_30shineEntities())
            {
                //insert list item
                var datetime = DateTime.Now;
                var list = _LstCheckItem.list;
                foreach (CheckItemCSVCView o in list)
                {
                    if (!o.isOk)
                    {
                        var chItem = new ERP_CheckCSVC();
                        chItem.ItemId = o.itemId;
                        chItem.Comment = o.comment;
                        chItem.Image = o.image;
                        chItem.StaffId = o.staffId;
                        chItem.IsOk = o.isOk;
                        chItem.IsReported = o.isReported;
                        chItem.SalonId = _LstCheckItem.salonId;
                        chItem.DateTime = datetime;
                        chItem.Description = _LstCheckItem.description;
                        chItem.CreatorId = _LstCheckItem.creatorId;
                        chItem.Status = 1;//1. Do, 2.Doing, 3.Done
                        db.ERP_CheckCSVC.Add(chItem);
                    }
                }
                db.SaveChanges();
            }
        }

    }
}