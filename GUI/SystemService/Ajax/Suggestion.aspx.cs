﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Web.Script.Serialization;
using System.Web.Services;
using LinqKit;
using System.Data.Entity.Migrations;
using System.Globalization;

namespace _30shine.GUI.SystemService.Ajax
{
    public partial class Suggestion : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        [WebMethod]
        public static string Suggestion_Customer(string field, string text)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var serializer = new JavaScriptSerializer();
                var Msg = new Msg();
                int integer;
                int value = int.TryParse(text, out integer) ? integer : 0;
                switch (field)
                {
                    case "customer.name":
                        var OBJ = db.Customers.Where(w => w.Fullname.Contains(text) && w.IsDelete != 1).OrderBy(o => o.Id)
                                .Select(s => new { Value = s.Fullname, s.Customer_Code, CustomerId = s.Id }).Take(5).ToList();
                        Msg.msg = serializer.Serialize(OBJ);
                        break;
                    case "customer.phone":
                        OBJ = db.Customers.Where(w => w.Phone.Contains(text) && w.IsDelete != 1).OrderBy(o => o.Id)
                                .Select(s => new { Value = s.Phone, s.Customer_Code, CustomerId = s.Id }).Take(5).ToList();
                        Msg.msg = serializer.Serialize(OBJ);
                        break;
                    case "customer.code":
                        if (value == 0)
                        {
                            OBJ = db.Customers.Where(w => w.Phone.Contains(text) && w.IsDelete != 1).OrderBy(o => o.Id)
                                                .Select(s => new { Value = s.Customer_Code, s.Customer_Code, CustomerId = s.Id }).Take(5).ToList();
                        }
                        else
                        {
                            OBJ = db.Customers.Where(w => w.Phone.Contains(text) && w.IsDelete != 1).OrderBy(o => o.Id)
                                                .Select(s => new { Value = s.Phone, s.Customer_Code, CustomerId = s.Id }).Take(5).ToList();
                        }
                        Msg.msg = serializer.Serialize(OBJ);
                        break;
                }
                Msg.success = true;

                return serializer.Serialize(Msg);
            }
        }

        [WebMethod]
        public static string Suggestion_Staff(string field, string text)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var serializer = new JavaScriptSerializer();
                var Msg = new Msg();
                switch (field)
                {
                    case "customer.name":
                        var OBJ = db.Staffs.Where(w => w.Fullname.Contains(text) && w.IsDelete != 1 && w.Active == 1 && (w.Permission != "admin" && w.Permission != "accountant")).OrderBy(o => o.Id)
                                .Select(s => new { Value = s.Fullname, s.Id }).Take(5).ToList();
                        Msg.msg = serializer.Serialize(OBJ);
                        break;
                    case "customer.phone":
                        OBJ = db.Staffs.Where(w => w.Phone.Contains(text) && w.IsDelete != 1 && w.Active == 1 && (w.Permission != "admin" && w.Permission != "accountant")).OrderBy(o => o.Id)
                                .Select(s => new { Value = s.Phone, s.Id }).Take(5).ToList();
                        Msg.msg = serializer.Serialize(OBJ);
                        break;
                    case "customer.code":
                        OBJ = db.Staffs.Where(w => w.OrderCode.Contains(text) && w.IsDelete != 1 && w.Active == 1 && (w.Permission != "admin" && w.Permission != "accountant")).OrderBy(o => o.Id)
                                .Select(s => new { Value = s.Fullname, s.Id }).Take(5).ToList();
                        Msg.msg = serializer.Serialize(OBJ);
                        break;
                }
                Msg.success = true;

                return serializer.Serialize(Msg);
            }
        }

        [WebMethod]
        public static string Suggestion_OneStaff(string code, int SalonId, string StaffType)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var Where = PredicateBuilder.True<Staff>();
                var serializer = new JavaScriptSerializer();
                var Msg = new Msg();
                Where = Where.And(w => w.OrderCode == code && w.IsDelete != 1 && w.Active == 1);
                if (SalonId > 0)
                {
                    var salon = db.Tbl_Salon.FirstOrDefault(w=>w.Id == SalonId);
                    if (salon != null && salon.IsSalonHoiQuan != null && salon.IsSalonHoiQuan == true)
                    {
                        Where = Where.And(w=>w.SalonId == SalonId);
                    }
                }
                switch (StaffType)
                {
                    case "Hairdresser":
                        Where = Where.And(w => w.Type == 1);
                        break;
                    case "HairMassage":
                        Where = Where.And(w => w.Type == 2);
                        break;
                    case "Reception":
                        Where = Where.And(w => w.Type == 5);
                        break;
                    default: break;
                }
                var OBJ = db.Staffs.AsExpandable().Where(Where).OrderBy(o => o.Id)
                                                .Select(s => new { s.Fullname, s.Id }).Take(5).ToList();
                Msg.msg = serializer.Serialize(OBJ);
                Msg.success = true;

                return serializer.Serialize(Msg);
            }
        }

        [WebMethod]
        public static string Suggestion_Staff_ByName(string field, string name, int staffType)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var serializer = new JavaScriptSerializer();
                var Msg = new Msg();
                switch (field)
                {
                    case "stylist.name":
                        var OBJ = db.Staffs.Where(w => w.Fullname.Contains(name) && w.IsDelete != 1 && w.Active == 1 && (w.Permission != "admin" && w.Permission != "accountant") && w.Type == staffType).OrderBy(o => o.Id).Select(s => new { Value = s.Fullname, s.Id, s.SalonId, s.SkillLevel }).Take(5).ToList();
                        Msg.msg = serializer.Serialize(OBJ);
                        break;
                    case "skinner.name":
                        OBJ = db.Staffs.Where(w => w.Fullname.Contains(name) && w.IsDelete != 1 && w.Active == 1 && (w.Permission != "admin" && w.Permission != "accountant") && w.Type == staffType).OrderBy(o => o.Id).Select(s => new { Value = s.Fullname, s.Id, s.SalonId, s.SkillLevel }).Take(5).ToList();
                        Msg.msg = serializer.Serialize(OBJ);
                        break;
                    case "staff.name":
                        OBJ = db.Staffs.Where(w => w.Fullname.Contains(name) && w.IsDelete != 1 && w.Active == 1 && (w.Permission != "admin" && w.Permission != "accountant")).OrderBy(o => o.Id).Select(s => new { Value = s.Fullname, s.Id, s.SalonId, s.SkillLevel }).Take(5).ToList();
                        Msg.msg = serializer.Serialize(OBJ);
                        break;
                    default: break;
                }
                Msg.success = true;

                return serializer.Serialize(Msg);
            }
        }

        [WebMethod]
        public static string Update_BillTeamId(int billId, int teamId)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var serializer = new JavaScriptSerializer();
                var Msg = new Msg();
                var bill = db.BillServices.FirstOrDefault(w => w.Id == billId);
                if (bill != null)
                {
                    bill.TeamId = teamId;
                    db.BillServices.AddOrUpdate(bill);
                    db.SaveChanges();
                }
                Msg.success = true;

                return serializer.Serialize(Msg);
            }
        }

        [WebMethod]
        public static string Update_AppointmentTeamId(int billId, int teamId)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var serializer = new JavaScriptSerializer();
                var Msg = new Msg();
                var bill = db.Appointments.FirstOrDefault(w => w.Id == billId);
                if (bill != null)
                {
                    bill.TeamId = teamId;
                    db.Appointments.AddOrUpdate(bill);
                    db.SaveChanges();
                }
                Msg.success = true;

                return serializer.Serialize(Msg);
            }
        }

        [WebMethod]
        public static string GetCustomer(int CustomerId)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var _Customer = db.Customers.Where(w => w.Id == CustomerId).Select(s => new { s.Id, s.Fullname, s.Customer_Code, s.Phone, s.CityId, s.DistrictId, s.Address, s.Info_Flow }).FirstOrDefault();

                if (_Customer != null)
                {
                    _Msg.success = true;
                    _Msg.msg = serializer.Serialize(_Customer);
                }
                else
                {
                    _Msg.success = false;
                }
            }
            return serializer.Serialize(_Msg);
        }

        [WebMethod]
        public static string GetCustomerByPhone(string phone)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var _Customer = db.Customers.Where(w => w.Phone == phone && w.IsDelete != 1).Select(s => new { s.Id, s.Fullname, s.Customer_Code, s.Phone, s.CityId, s.DistrictId, s.Address, s.Info_Flow }).FirstOrDefault();

                if (_Customer != null)
                {
                    _Msg.success = true;
                    _Msg.msg = serializer.Serialize(_Customer);
                }
                else
                {
                    _Msg.success = false;
                }
            }
            return serializer.Serialize(_Msg);
        }

        [WebMethod]
        public static string CheckDuplicateBill(int CustomerId)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var today = Convert.ToDateTime(DateTime.Now.ToString("d/M/yyyy"), culture);
                var todayL = today.AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                var todayR = today.AddDays(1);
                var _Bill = db.BillServices.FirstOrDefault(w => w.CreatedDate > todayL && w.CreatedDate < todayR && w.CustomerId == CustomerId && w.IsDelete != 1 && w.Pending != 1);

                if (_Bill != null)
                {
                    _Msg.success = true;
                    _Msg.msg = serializer.Serialize(_Bill);
                }
                else
                {
                    _Msg.success = false;
                }
            }
            return serializer.Serialize(_Msg);
        }
    }
}