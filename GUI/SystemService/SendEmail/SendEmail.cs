﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace Library
{
    public class SendEmail
    {
        public SendEmail()
        {
        }

        private string SENDER_EMAIL = "service.30shine@gmail.com";
        private string SENDER_PASSWORD = "v8Yy2MdfXH9s";

        public void SendMail(string ReceiveEmail, string subject, string body)
        {
            //Reading sender Email credential from web.config file
            string HostAdd = "smtp.gmail.com";
            string ToEmail = ReceiveEmail;

            //creating the object of MailMessage
            MailMessage mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(SENDER_EMAIL); //From Email Id
            mailMessage.Subject = subject; //Subject of Email
            mailMessage.Body = body; //body or message of Email
            mailMessage.IsBodyHtml = true;
            //Adding Multiple recipient email id logic
            string[] Multi = ToEmail.Split(','); //spiliting input Email id string with comma(,)
            foreach (string Multiemailid in Multi)
            {
                mailMessage.To.Add(new MailAddress(Multiemailid)); //adding multi reciver's Email Id
            }
            SmtpClient smtp = new SmtpClient(); // creating object of smptpclient
            smtp.Host = HostAdd; //host of emailaddress for example smtp.gmail.com etc

            //network and security related credentials
            smtp.EnableSsl = true;
            NetworkCredential NetworkCred = new NetworkCredential();
            NetworkCred.UserName = mailMessage.From.Address;
            NetworkCred.Password = SENDER_PASSWORD;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;
            smtp.Send(mailMessage); //sending Email
        }
    }
}