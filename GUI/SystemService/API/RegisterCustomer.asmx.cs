﻿using _30shine.GUI.SystemService.Ajax;
using _30shine.MODEL.ENTITY.EDMX;
using Project.SystemService.SendSMS;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace _30shine.GUI.SystemService.API
{
    /// <summary>
    /// Summary description for RegisterCustomer
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class RegisterCustomer : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]  //Kiếm tra xem tài khoản đã có chưa 
        public object LoginWithFacebook(string _FbId)
        {
            var messasge = new cls_message();
            var user = new UserProfile();
            using (var db = new Solution_30shineEntities())
            {
                var obj = db.Customers.FirstOrDefault(a => a.FacebookId == _FbId && a.Publish == 1);
                if (obj != null)
                {
                    user.ID = obj.Id;
                    messasge.success = true;
                    messasge.message = "success";
                }
                else
                {
                    messasge.success = false;
                    messasge.message = "Chưa có tài khoản";
                }
            }

            messasge.data = user;
            return messasge;
        }

        [WebMethod]   // chưa có thì đăng ký mới còn có rồi thì update
        public string DangKyTK(string _Email, string _HoTen, string _DienThoai, string FacebookId, string Picture, string GoogleId)
        {
            string str = "";
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    Random random = new Random();
                    var randomNumber1 = random.Next(0, 9);
                    var randomNumber2 = random.Next(0, 9);
                    var randomNumber3 = random.Next(0, 9);
                    var randomNumber4 = random.Next(0, 9);
                    var code = randomNumber1.ToString() + randomNumber2.ToString() + randomNumber3.ToString() + randomNumber4.ToString();

                    var check = db.Customers.FirstOrDefault(a => (a.FacebookId == FacebookId || a.GoogleId == GoogleId) || a.Phone == _DienThoai);
                    var objND = new Customer();
                    if (check != null)
                    {
                        objND = db.Customers.Single(a => a.Id == check.Id);
                        objND.ModifiedDate = DateTime.Now;
                    }
                    else
                    {
                        objND.TotalPoint = 0;
                        objND.Publish = 0;
                        objND.IsDelete = 0;
                        objND.CreatedDate = DateTime.Now;
                    }

                    objND.Fullname = _HoTen;
                    objND.CheckCode = code;
                    objND.Email = _Email;
                    objND.Phone = _DienThoai;
                    if (FacebookId != "")
                        objND.FacebookId = FacebookId;
                    else if (GoogleId != "")
                        objND.GoogleId = GoogleId;
                    objND.Avatar = Picture;
                    db.Customers.AddOrUpdate(objND);
                    db.SaveChanges();

                    SendSMSLib.sendSMS("Mã xác nhận tài khoản 30Shine của anh " + objND.Fullname + " là " + code + "", objND.Phone);
                    str = "success";
                }
            }
            catch
            {
                str = "error";
            }
            return str;
        }

        //public static int CustomerId = 0;

        [WebMethod] //Kiếm tra xem code đã đúng chưa
        public object CheckCode(string Phone, string Code)
        {
            
            var messasge = new cls_message();
            var user = new UserProfile();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var check = db.Customers.FirstOrDefault(a => a.Phone == Phone && a.CheckCode == Code);
                    if (check != null)
                    {
                        check.Publish = 1;
                        db.Customers.AddOrUpdate(check);
                        db.SaveChanges();
                        user.ID = check.Id;
                        messasge.success = true;
                        messasge.message = "success";
                    }
                    else
                    { 
                        messasge.success = false;
                        messasge.message = "checkError";
                    }
                }
            }
            catch
            {
                messasge.message = "error";
            }
            messasge.data = user;
            return messasge;
        }

        [WebMethod] //Lấy thông tin khách hàng
        public object GetInfoCustomer(int CustomerId)
        {
            JavaScriptSerializer serialize = new JavaScriptSerializer();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var customer = db.Customers.FirstOrDefault(x => x.Id == CustomerId);
                    if (customer != null)
                    {
                        var point = db.Store_Customer_History_Point(CustomerId).Sum(p => p.Point);

                        //lay rank
                        var rank = db.Customer_Rating.FirstOrDefault(x => x.PointStart <= point && x.PointEnd >= point);
                        var rankTop = db.Customer_Rating.Where(p => p.Id > rank.Id).FirstOrDefault();
                        var listData = db.Customers.Select(p => new { p.Id, p.Fullname, TotalPoint = point, rank.Name, p.Avatar, rankTop = rankTop.Name, Need = (rank.PointEnd + 1) - point }).FirstOrDefault(p => p.Id == CustomerId);
                        return serialize.Serialize(listData);
                    }
                    else
                    {
                        return serialize.Serialize(null);
                    }
                }
            }
            catch (Exception ex)
            {
                var str = ex.Message;
                return serialize.Serialize(null);
            }

        }

        [WebMethod]  // lấy lịch sử tích điểm của khách hàng
        public object GetHistoryPoint(int CustomerId)
        {
            using (var db = new Solution_30shineEntities())
            {
                JavaScriptSerializer serialize = new JavaScriptSerializer();
                if (CustomerId != 0)
                {
                    var ListData = new List<ItemData>();
                    var itemData = new ItemData();

                    var list = db.Store_Customer_History_Point(CustomerId).ToList();
                    if (list.Count() > 0)
                    {
                        foreach (var item in list)
                        {
                            itemData = new ItemData();
                            itemData.CreatedDate = item.CreatedDate;
                            itemData.CustomerName = item.CustomerName;
                            itemData.Point = item.Point.Value;
                            itemData.StaffName = item.StaffName;
                            itemData.Salon = item.Salon;
                            itemData.Images = item.Images;
                            var listImg = new List<string>();
                            var listImgTemp = new List<string>();

                            if (itemData.Images != null)
                            {
                                listImg = itemData.Images.Split(',').ToList();

                                var Len = itemData.Images.Length;
                                if (Len > 0)
                                {
                                    foreach (var v2 in listImg)
                                    {
                                        var match = Regex.Match(v2, @"api.30shine.com");
                                        //if (!match.Success)
                                        //{
                                        //    listImgTemp.Add("https://30shine.com/" + v2);
                                        //}
                                        //else
                                        //{
                                        //    listImgTemp.Add(v2);
                                        //}
                                        listImgTemp.Add(v2);
                                    }
                                }
                            }

                            itemData.ListImg = listImgTemp;
                            ListData.Add(itemData);
                        }

                    }
                    return serialize.Serialize(ListData);
                }
                else
                {
                    return serialize.Serialize(null);
                }
            }

        }

        [WebMethod] //Đăng xuất
        public string Logout()
        {
            Session["CustomerId"] = 0;
            return "success";
        }

        [WebMethod] // Kiếm tra Id của google
        public object CheckGoogleId(string GoogleId)
        {
            var messasge = new cls_message();
                
            using (var db = new Solution_30shineEntities())
            {
                var user = new UserProfile();
                var obj = db.Customers.FirstOrDefault(a => a.GoogleId == GoogleId && a.Publish == 1);

                if (obj != null)
                {
                    user.ID = obj.Id;
                    messasge.success = true;
                    messasge.message = "success";
                }
                else
                {
                    messasge.success = false;
                    messasge.message = "Chưa có tài khoản";
                }
                messasge.data = user;
            }
            return messasge;
        }

        [WebMethod]  //Ưu đãi đặc biệt cho khách
        public object GetPromotionByCustomer(int CustomerId)
        {
            try
            {
                var db = new Solution_30shineEntities();
               var point = db.Store_Customer_History_Point(CustomerId).Sum(p => p.Point);

                //lay rank
                var rank = db.Customer_Rating.FirstOrDefault(x => x.PointStart <= point && x.PointEnd >= point && x.IsDelete == false && x.Publish == true);
                var lst = db.Store_Customer_Listing_UuDai(rank.Id).ToList();
                return lst;
            }
            catch (Exception ex)
            {
                var str = ex.Message;
                return null;
            }
        }


        public class ItemData
        {
            public string CustomerName { get; set; }
            public string CreatedDate { get; set; }
            public int Point { get; set; }
            public string StaffName { get; set; }
            public string Salon { get; set; }
            public string Images { get; set; }
            public List<string> ListImg { get; set; }
            // public List<string> ListRanting { get; set; }
        }

        public class UserProfile
        {
            public int ID { get; set; }
            public string FacebookID { get; set; }
            public string GoogleID { get; set; }
            public string UserName { get; set; }
            public string Avatar { get; set; }
            public string Email { get; set; }
            public string Phone { get; set; }
        }

        public class cls_message
        {
            public bool success { get; set; }
            public string message { get; set; }
            public object data { get; set; }
        }
    }
}
