﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _30shine.GUI.SystemService.API.VanHanh
{
    public class ContentResult
    {
        public string ContentType { get; set; }
        public string Content { get; set; }
        public string ContentUrl { get; set; }
        public string ImageUrl { get; set; }
        public bool Success { get; set; }
        public object Data { get; set; }
    }
}