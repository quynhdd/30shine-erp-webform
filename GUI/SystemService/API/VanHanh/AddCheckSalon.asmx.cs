﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Services;
using System.Net;
using System.IO;
using System.Net.Http;
using System.Text;
using Project.Model.Structure;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Web.Script.Serialization;
using System.Drawing;
using System.Configuration;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;
using _30shine.MODEL.ENTITY.EDMX;

namespace _30shine.GUI.SystemService.API.VanHanh
{
    /// <summary>
    /// Summary description for AddCheckSalon
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class AddCheckSalon : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {

            string str = "";

            return str;
        }
        /// <summary>
        /// up load anh
        /// </summary>
        public static Boolean checkUrl = true;
        public static string fileurl = "";
        public static string fileSavePath = "";
        [WebMethod]
        public ContentResult UploadFile()
        {
            ContentResult contentResult = new ContentResult();
            var msg = new CommonClass.cls_message();
            if (HttpContext.Current.Request.Files.AllKeys.Any())
            {
                var httpPostedFile = HttpContext.Current.Request.Files["file"];
                Guid guidPathFile;
                //Tên thư mục
                guidPathFile = Guid.NewGuid();
                if (checkUrl)
                {
                    //linK Url
                    fileurl = "~/Assets/images/VanHanh/" + guidPathFile.ToString();
                    checkUrl = false;
                }
                bool folderExists = Directory.Exists(HttpContext.Current.Server.MapPath(fileurl));
                if (!folderExists)
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(fileurl));
                //Dường dẫn thư mục
                fileSavePath = Path.Combine(HttpContext.Current.Server.MapPath(fileurl),
                   httpPostedFile.FileName);
                httpPostedFile.SaveAs(fileSavePath);

                if (File.Exists(fileSavePath))
                {
                    contentResult.Success = true;
                    contentResult.ContentType = "Messager";
                    contentResult.Content = fileurl;
                    contentResult.ContentUrl = fileSavePath;
                    contentResult.ImageUrl = guidPathFile.ToString();
                    return contentResult;
                }
                else
                {
                    contentResult.Success = false;
                    return contentResult;
                }
            }
            else
            {
                contentResult.Success = false;
                return contentResult;
            }
        }
        [WebMethod]
        public object test()
        {
            var msg = new CommonClass.cls_message();
            var a = "deo thic tra loi";

            return a;
        }
        /// <summary>
        /// Lưu check list
        /// </summary>
        /// <param name="SalonId"></param>
        /// <param name="Note"></param>
        /// <returns></returns>
        [WebMethod]
        public object SaveChecklistItemSalon(string SalonId, string Note)
        {
            using (var db = new Solution_30shineEntities())
            {
                var Id = Convert.ToInt32(SalonId);
                var msg = new CommonClass.cls_message();
                if (Id > 0)
                {
                    var flow = new VanHanh_Flow();
                    flow.SalonId = Id;
                    flow.Note = Note;
                    flow.ImageUrl = fileurl;
                    flow.imageFoder = fileSavePath;
                    db.VanHanh_Flow.AddOrUpdate(flow);
                    db.SaveChanges();
                    msg.success = true;
                    msg.message = "Thành công";
                    msg.data = flow;

                }
                else
                {
                    msg.success = false;
                    msg.message = "Lỗi !.Vui lòng liên hệ với nhà phát triển để được giải đáp";
                }
                fileSavePath = "";
                fileurl = "";
                // checkUrl = false;
                return msg;

            }
        }
        /// <summary>
        /// load salon
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public object LoadSalon()
        {
            using (var db = new Solution_30shineEntities())
            {
                var msg = new CommonClass.cls_message();
                var obj = db.Tbl_Salon.Where(w => w.IsDelete == 0 && w.Publish == true).ToList();
                if (obj != null)
                {
                    msg.success = true;
                    msg.message = "Oke";
                    msg.data = obj;

                }
                else
                {
                    msg.success = false;
                    msg.message = "Lỗi !. vui lòng lên hệ với nhà phat triển";
                }
                return msg;
            }
        }
    }
}
