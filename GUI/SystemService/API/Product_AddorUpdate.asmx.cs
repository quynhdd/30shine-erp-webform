﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using _30shine.MODEL.ENTITY.EDMX;

namespace _30shine.GUI.SystemService.API
{
    /// <summary>
    /// Summary description for Product_AddorUpdate
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Product_AddorUpdate : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public string AddorUpdate(int Id, int brandId, int categoryId, int price, int oldPrice, int discount, string volume, string modelName, string productName)
        {
            //
            return "ok";
        }
    }
}
