﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using System.Web.Script.Serialization;
using _30shine.Helpers;
using System.Web.Script.Services;
using System.Security.Cryptography;
using System.Text;

namespace _30shine.GUI.SystemService.Webservice
{
    /// <summary>
    /// Summary description for wsRating
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]



    ///****SERVICE ĐÁNH GIÁ DỊCH VỤ 30SHINE *****/
    /// 3/2016
    /// CÁC HÀM
    /// getRating(salonId, lgId) cập nhật giá trị rating từ phía web client 
    /// RequestRating(salonId, staffId, ratingMark) cập nhật giá trị rating từ phía app android
    /// LoginRating(accountName, password) kiểm tra đăng nhập trên app android, đăng nhập theo quyền tài khoản, trả về json đối tượng Config
    /// Config là đối tượng lưu trữ thông tin nhân viên đăng nhập, salonId đánh giá để lưu vào cấu hình app anhdroid sau khi đăng nhập
    /// MÔ TẢ CHI TIẾT TRONG CÁC HÀM
    [ScriptService]
    public class wsRating : System.Web.Services.WebService
    {
        /// <summary>
        /// Lấy giá trị rating database
        /// </summary>
        /// <returns>Msg, đối tượng chứa thông tin kết quả</returns>
        [WebMethod]
        public Msg getRating(int salonId, int lgId)
        {
            using (var db = new Solution_30shineEntities())
            {
                /// 1. Lấy giá trị rating tương ứng với salon
                /// 2. Trả giá trị về web brower

                var serializer = new JavaScriptSerializer();
                var Msg = new Msg();

                var rating = db.Rating_Temp.Where(w => w.IsDelete != 1 && w.SalonId == salonId && w.AccountId == lgId).FirstOrDefault();
                if (rating != null)
                {
                    Msg.msg = rating.RatingValue.ToString();
                }
                Msg.success = true;

                return Msg;
            }
        }

        /// <summary>
        /// Đánh giá dịch vụ
        /// </summary>
        /// <param name="salonId">id salon</param>
        /// <param name="staffId">id nhân viên</param>
        /// <param name="ratingMark">điểm đánh giá</param>
        /// <returns>string, thông báo kết quả</returns>
        [WebMethod]
        public string RequestRating(string salonId, string staffId, string ratingMark)
        {
            using (var db = new Solution_30shineEntities())
            {
                //client app android gửi salonId, staffId, ratingMark
                //tìm đối tượng trong bảng Rating_Temp có SalonId=salonId và AccountId=staffId
                //cập nhật giá trị RaingValue mới cho đối tượng Rating_Temp

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                Msg msg = new Msg();

                int salonIdInt = Convert.ToInt32(salonId);
                int ratingMarkInt = Convert.ToInt32(ratingMark);
                int staffIdInt = Convert.ToInt32(staffId);


                var sql = @"select * from Rating_Temp where IsDelete!=1 and SalonId=" + salonIdInt + " and AccountId =" + staffIdInt;
                var obj = db.Database.SqlQuery<Rating_Temp>(sql).FirstOrDefault();

                var result = sql;
                if (obj != null)
                {
                    obj.AccountId = staffIdInt;
                    obj.RatingValue = ratingMarkInt;
                    obj.SalonId = salonIdInt;
                    obj.Status = 1;
                    if (obj.RatingValue == 0)
                        obj.CreatedDate = DateTime.Now;
                    obj.ModifiedDate = DateTime.Now;

                    db.Rating_Temp.AddOrUpdate(obj);
                    var exc = db.SaveChanges();
                    if (exc > 0)
                    {
                        msg.success = true;
                        result += " Đã Đánh giá ";
                    }
                    else
                    {
                        msg.success = false;
                        result += " Đã có lỗi xảy ra ";
                    }
                }
                else
                {
                    result += " Không tìm thấy salon hoặc máy nhân viên mở Bill. ";
                }
                msg.msg = result;
                return serializer.Serialize(msg);
            }
        }


        /// <summary>
        /// Đăng nhập
        /// </summary>
        /// <param name="accountName">tài khoản</param>
        /// <param name="password">mật khẩu</param>
        /// <returns>string, thông báo kết quả</returns>
        [WebMethod]
        public string LoginRating(string accountName, string password)
        {
            using (var db = new Solution_30shineEntities())
            {
                //Đăng nhập bằng tài khoản đã phân quyền của lễ tân checkin và lễ tân checkout với điều kiện isAppLogin=1 để phân biệt với tài khoản khác không được phép đăng nhập cho việc cấu hình đánh giá dịch vụ
                // trả về json đối tượng Config 
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                Msg msg = new Msg();

                string PasswordGen;

                using (MD5 md5Hash = MD5.Create())
                {
                    PasswordGen = GenPassword(md5Hash, password);
                }

                var sql = @"select * from Staff where IsDelete!=1 and Email='" + accountName + "' and Password='" + PasswordGen + "' and isAppLogin=1";
                var staff = db.Database.SqlQuery<Staff>(sql).FirstOrDefault();

                var result = "";
                result = sql;
                if (staff != null)
                {
                    var obj = new Config();
                    obj.salonId = staff.SalonId.ToString();
                    var salonName = db.Tbl_Salon.Where(w => w.Id == staff.SalonId).Select(s => s.Name).FirstOrDefault();
                    if (salonName != null)
                        obj.salonName = salonName;

                    obj.staffId = staff.Id.ToString();
                    obj.staffName = staff.Fullname;

                    msg.success = true;
                    msg.msg = serializer.Serialize(obj);
                }
                else
                {

                }
                return serializer.Serialize(msg);
            }
        }

        [WebMethod]
        public Msg RequestRatingVer1(int BillId)
        {
            //JavaScriptSerializer serializer = new JavaScriptSerializer();
            Msg msg = new Msg();

            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var data = db.BillServices.Where(o => o.Id == BillId).FirstOrDefault();
                    if (data != null)
                    {
                        msg.data = data.Mark.ToString();
                        msg.success = true;
                    }
                    else
                    { msg.success = false; }
                }
            }
            catch (Exception)
            {

            }
            return msg;
        }
        [WebMethod]
        public Msg RequestRatingVer2(int BillId)
        {
            Msg msg = new Msg();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var data = db.RatingDetails.Where(o => o.BillId == BillId && o.isAuto == false).FirstOrDefault();
                    if (data != null)
                    {
                        var isComplete = false;
                        if ((data.RatingReason1 == true || data.RatingReason2 == true ||
                           data.RatingReason3 == true || data.RatingReason6 == true || data.RattingReason7 == true)
                           && data.StepNumber >= 2)
                        {
                            isComplete = true;
                        }

                        msg.success = true;
                        msg.data = new
                        {
                            StarNumber = data.StarNumber ?? 0,
                            IsComplete = isComplete
                        };
                    }
                    else
                    { msg.success = false; }
                }
            }
            catch (Exception)
            {

            }
            return msg;
        }

        [WebMethod(EnableSession = true)]
        public string TestService(int id, string name)
        {
            return "lala";
        }


        static string GenPassword(MD5 md5Hash, string input)
        {
            var scrPush = "uaefsfkoiu&@(^%=";
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input + scrPush));
            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }

        /// Config là đối tượng lưu trữ thông tin nhân viên đăng nhập, salonId đánh giá để lưu vào cấu hình app anhdroid sau khi đăng nhập
        public class Config
        {
            public String salonId { get; set; }
            public String staffId { get; set; }
            public String salonName { get; set; }
            public String staffName { get; set; }
            public String permission { get; set; }
            public String status { get; set; }
        }

    }
}
