﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using System.Web.Script.Serialization;
using _30shine.Helpers;
using System.Web.Script.Services;
using System.Security.Cryptography;
using System.Text;
using LinqKit;


namespace _30shine.GUI.SystemService.Webservice
{
    /// <summary>
    /// Summary description for BookingService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]

    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class BookingService : System.Web.Services.WebService
    {

        /// <summary>
        /// lấy các bill đang ở trang thái pending trong ngày hiện tại
        /// </summary>
        /// <param name="salonId">salon id</param>
        /// <returns>trả về danh sách đối tượng BillServicePending</returns>
        [WebMethod]
        public string GetBillPending(int salonId)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var today = String.Format("{0:yyyy/MM/dd}", DateTime.Today);
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var sql = @"select a.Id, CONVERT(nvarchar,CAST(a.CreatedDate AS TIME), 100) as [CreatedTime]
                    , s.Fullname as HairdresserName
                    , s2.Fullname as HairMassageName, s3.Fullname as CosmeticName
                    , c.Fullname as CustomerName, c.Phone as CustomerPhone, t.Color as TeamColor
                    from BillService as a
                    left join Staff as s
                    on a.Staff_Hairdresser_Id = s.Id
                    left join Staff as s2
                    on a.Staff_HairMassage_Id = s2.Id
                    left join Staff as s3
                    on a.SellerId = s3.Id
                    left join Customer as c
                    on a.CustomerCode = c.Customer_Code
                    left join TeamService as t
                    on a.TeamId = t.Id
                    where a.IsDelete != 1 and a.Pending = 1
                    and a.CreatedDate >='" + today + "'";
                if (salonId > 0)
                    sql += " and a.SalonId=" + salonId;
                else
                    sql += " and a.SalonId>0";

                var list = new List<BillServicePending>();
                list = db.Database.SqlQuery<BillServicePending>(sql).ToList();
                return serializer.Serialize(list);
            }        
        }

        /// <summary>
        /// lấy các bill đang ở trang thái pending trong ngày hiện tại theo team
        /// </summary>
        /// <param name="salonId">salon id</param>
        /// <returns>trả về danh sách đối tượng BillServicePending</returns>
        /// 

        [WebMethod]
        public string GetBillPendingOfTeam(int salonId, int teamId)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var today = String.Format("{0:yyyy/MM/dd}", DateTime.Today);
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var sql = @"select a.Id, CONVERT(nvarchar,CAST(a.CreatedDate AS TIME), 100) as [CreatedTime], a.BillCode
                    , s.Fullname as HairdresserName
                    , s2.Fullname as HairMassageName, s3.Fullname as CosmeticName
                    , c.Fullname as CustomerName, c.Phone as CustomerPhone, t.Color as TeamColor, t.Id as TeamId
                    ,COALESCE(sv.Name,'') as ServiceName   
                    from BillService as a
                    left join Staff as s
                    on a.Staff_Hairdresser_Id = s.Id
                    left join Staff as s2
                    on a.Staff_HairMassage_Id = s2.Id
                    left join Staff as s3
                    on a.SellerId = s3.Id
                    left join Customer as c
                    on a.CustomerCode = c.Customer_Code
                    left join TeamService as t
                    on a.TeamId = t.Id
                    left join FlowService f 
                    on a.Id=f.BillId
                    left join Service as sv
                    on f.ServiceId=sv.Id

                    where a.IsDelete != 1 and a.TeamId=" + teamId +
                        @" and a.Pending = 1
                    and a.CreatedDate >='" + today + "'";
                if (salonId > 0)
                    sql += " and a.SalonId=" + salonId;
                else
                    sql += " and a.SalonId>0";

                var list = new List<BillServicePendingByFloor>();
                list = db.Database.SqlQuery<BillServicePendingByFloor>(sql).ToList();

                if (list.Count > 0)
                {
                    for (var i = 0; i < list.Count; i++)
                    {
                        list[i].Order = UIHelpers.getBillOrder(list[i].BillCode, 4);
                    }
                }
                return serializer.Serialize(list);
            }
        }

        /// <summary>
        /// cập nhật bill đợi thành trạng thái pending
        /// </summary>
        /// <param name="id">bill id</param>
        /// <param name="salonId">salon id</param>
        /// <returns>json thông báo trạng thái:
        /// 1. Hiện tại đang có 4 bill của 1 team đang pending
        /// 2. Cập nhật bill pending thành công
        /// 3. Đã có lỗi xảy ra khi cập nhật dữ liệu
        /// </returns>
        [WebMethod]
        public string SetBillPending(int id, int salonId)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var Msg = new Msg();
                //var sqlonId = Session["SalonId"];
                var obj = db.BillServices.FirstOrDefault(w => w.Id == id);

                if (obj != null)
                {
                    // kiểm tra bill có team id đã đầy 4 bill chưa
                    var check = CheckPendingMax(Convert.ToInt32(obj.TeamId), salonId);
                    if (check == false)
                    {
                        obj.Pending = 1;
                        obj.IsBooked = false;
                        obj.IsBooking = false;
                        obj.CreatedDate = DateTime.Now;
                        db.BillServices.AddOrUpdate(obj);
                        obj.IsDelete = 0;
                        db.SaveChanges();
                        Msg.success = true;
                        Msg.msg = obj.PDFBillCode;
                    }
                    else
                    {
                        Msg.success = false;
                        Msg.msg = "Đã đầy 4 bill team màu này Pending";
                    }
                }
                else
                {
                    Msg.success = false;
                    Msg.msg = "Đã có lỗi khi kiểm tra bill";
                }
                return serializer.Serialize(Msg);
            }
        }



        /// <summary>
        /// kiểm tra có bill có team id đã đầy 4 bill chưa 
        /// </summary>
        /// <param name="teamId">team id</param>
        /// <param name="salonId">salon id</param>
        /// <returns>nếu đầy 4 bill peding thì trả về true, không thì trả về false</returns>
        [WebMethod(EnableSession = true)]
        public bool CheckPendingMax(int teamId, int salonId)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var today = String.Format("{0:yyyy/MM/dd}", DateTime.Today);
                var sql = @"select a.*, s.Fullname as HairdresserName, s2.Fullname as HairMassageName, s3.Fullname as CosmeticName, 
	                    c.Fullname as CustomerName, c.Phone as CustomerPhone, t.Color as TeamColor 
                    from BillService as a
                    left join Staff as s
                    on a.Staff_Hairdresser_Id = s.Id
                    left join Staff as s2
                    on a.Staff_HairMassage_Id = s2.Id
                    left join Staff as s3
                    on a.SellerId = s3.Id
                    left join Customer as c
                    on a.CustomerCode = c.Customer_Code
                    left join TeamService as t
                    on a.TeamId = t.Id
                    where a.IsDelete != 1 
					and t.Id=" + teamId +
                        @"and a.Pending=1 and (a.Status < 3 or a.Status is null)
                    --and a.IsBooking=1 and a.IsBooked=0
                    and a.CreatedDate >= '" + today + "'";
                if (salonId > 0)
                    sql += " and a.SalonId=" + salonId;
                else
                    sql += " and a.SalonId>0";

                var list = db.Database.SqlQuery<BillServicePending>(sql).ToList();
                if (list != null && list.Count < 4)
                    return false;
                else return true;
            }  
        }


        /// <summary>
        /// kiểm tra bill pending theo team id
        /// </summary>
        /// <param name="salonId">salon id</param>
        /// <param name="teamId">team id</param>
        /// <returns>dánh sách BillServicePending</returns>
        [WebMethod(EnableSession = true)]
        public string GetBillPendingByTeam(int salonId, int teamId)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var today = String.Format("{0:yyyy/MM/dd}", DateTime.Today);
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var sql = @"select a.Id, CONVERT(nvarchar,CAST(a.CreatedDate AS TIME), 100) as [CreatedTime]
                    , s.Fullname as HairdresserName
                    , s2.Fullname as HairMassageName, s3.Fullname as CosmeticName
                    , c.Fullname as CustomerName, c.Phone as CustomerPhone, t.Color as TeamColor
                    from BillService as a
                    left join Staff as s
                    on a.Staff_Hairdresser_Id = s.Id
                    left join Staff as s2
                    on a.Staff_HairMassage_Id = s2.Id
                    left join Staff as s3
                    on a.SellerId = s3.Id
                    left join Customer as c
                    on a.CustomerCode = c.Customer_Code
                    left join TeamService as t
                    on a.TeamId = t.Id
                    where a.IsDelete != 1 and a.Pending = 1
                    and  a.TeamId='" + teamId + "'";
                //and a.CreatedDate >='" + today + "'";
                if (salonId > 0)
                    sql += " and a.SalonId=" + salonId;
                else
                    sql = " and a.SalonId>0";

                var list = new List<BillServicePending>();

                return serializer.Serialize(list);
            }
        }

        public string GetStaff(int salonId, int teamId, int staffType)
        {
            using (var db = new Solution_30shineEntities())
            {
                salonId = 0;
                var today = String.Format("{0:yyyy/MM/dd}", DateTime.Today);
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var sql = @"select Id, FullName, Type, OrderCode from Staff where Type='" + staffType + "' and IsDelete!=1 ";
                if (salonId > 0)
                    sql += " and SalonId=" + salonId;
                else
                    sql += " and SalonId>0";
                sql += " order by OrderCode";
                var list = new List<StaffTmp>();
                list = db.Database.SqlQuery<StaffTmp>(sql).ToList();
                return serializer.Serialize(list);
            }
        }

        [WebMethod(EnableSession = true)]
        public string getBillStatus(string ids)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<string> listId = serializer.Deserialize<List<string>>(ids);
            List<cls_billStatus> listBillReturn = new List<cls_billStatus>();
            cls_billStatus billReturn = new cls_billStatus();
            int id;
            using (var db = new Solution_30shineEntities())
            {
                if (listId.Count > 0)
                {
                    foreach (var v in listId)
                    {
                        billReturn = new cls_billStatus();
                        id = Convert.ToInt32(v);
                        billReturn.Id = id;
                        var bill = db.BillServices.FirstOrDefault(w => w.Id == id);
                        if (bill != null)
                        {
                            billReturn.StatusValue = Convert.ToInt32(bill.Status);
                            switch (bill.Status)
                            {
                                case 1:
                                    billReturn.StatusText = "Gội";
                                    billReturn.StatusClass = "status-step1";
                                    billReturn.Time = "Từ " + String.Format("{0:HH}", bill.ShampooTime0) + "h" + String.Format("{0:mm}", bill.ShampooTime0);
                                    break;
                                case 2:
                                    billReturn.StatusText = "Cắt";
                                    billReturn.StatusClass = "status-step2";
                                    billReturn.Time = "Từ " + String.Format("{0:HH}", bill.HairCutTime0) + "h" + String.Format("{0:mm}", bill.HairCutTime0);
                                    break;
                                case 3:
                                    billReturn.StatusText = "Chờ check-out";
                                    billReturn.StatusClass = "status-complete";
                                    billReturn.Time = ""; // + String.Format("{0:HH}", bill.HairCutTime1) + "h" + String.Format("{0:mm}", bill.HairCutTime1);
                                    break;
                            }
                        }
                        listBillReturn.Add(billReturn);
                    }
                    Msg.success = true;
                    Msg.msg = serializer.Serialize(listBillReturn);
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        /// <summary>
        /// Kiểm tra thời gian hẹn
        /// Trong bảng hẹn Appointment, tìm đối tượng có SalonId và AppointmentTime=addTime 
        /// Nếu đúng thời điểm trước thời gian hẹn một khoảng = addTime thì tìm được đối tượng, cập nhật trạng thái IsNotify=true để đánh dấu là đã thông báo 
        /// Không đúng thời cần thông báo thì không tìm thấy đối tượng cần thông báo
        /// </summary>
        /// <param name="salonId">salon id</param>
        /// <param name="addTime"> thời gian hẹn, tính bằng phút</param>
        /// <returns>
        /// Kiểm tra đối tượng Appointment:
        /// 1. Nếu đến thời gian trước hẹn một khoảng = addTime: trả về json đối tượng Appointment2 (chứa thông tin về cuộc hẹn: khách, stylist, thời gian hẹn) 
        /// 2. Nếu chưa đến thời gian hẹn: trả về json thông báo chưa đến thời gian hẹn
        /// </returns>
        /// 

        [WebMethod(EnableSession = true)]
        public string CheckAppointmentTime(int salonId, int addTime)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                var Msg = new Msg();
                JavaScriptSerializer serializer = new JavaScriptSerializer();

                var timeNow = string.Format("{0:HH:mm}", DateTime.Now);
                var today = string.Format("{0:yyyy/MM/dd}", DateTime.Now);
                var sql = @"declare @StartTime time(0) = '" + timeNow + "';" +
                            @" declare @MinutesToAdd int = " + addTime + ";" +
                            @" declare @Time time(0);

                        --select DATEADD(minute, @MinutesToAdd, @StartTime);
                        set @Time=DATEADD(minute, @MinutesToAdd, @StartTime);

                        select a.*, c.Fullname as CustomerName, s.Fullname as HairdresserName from Appointment a
                        left join Customer c on a.CustomerCode=c.Customer_Code
                        left join Staff s on a.StylistId=s.Id
                            where AppointmentTime>='" + today + "' and cast(AppointmentTime AS time) = @Time and a.IsPending is null and a.IsCancel is null and a.IsNotify is null";

                if (salonId > 0)
                    sql += " and a.SalonId=" + salonId;
                else
                    sql += " and a.SalonId>0";

                var obj = db.Database.SqlQuery<Appointment2>(sql).FirstOrDefault();
                if (obj == null)
                {
                    Msg.success = false;
                    Msg.msg = "";
                }
                else
                {
                    obj.IsNotify = true;
                    db.Appointments.AddOrUpdate(obj);
                    db.SaveChanges();

                    Msg.success = true;
                    Msg.msg = serializer.Serialize(obj);
                }
                return serializer.Serialize(Msg);
            }
        }



        /// <summary>
        /// Cập nhật hủy bỏ đặt lịch hẹn
        /// </summary>
        /// <param name="id">id của cuộc hẹn (Appointment)</param>
        /// <param name="salonId">salon id</param>
        /// <returns>trả về true nếu tìm thấy đối tượng và cập nhật thành công, ngược lại trả về false</returns>
        public string SetAppointmentCancel(int id, int salonId)
        {
            using (var db = new Solution_30shineEntities())
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var Msg = new Msg();
                var obj = db.Appointments.FirstOrDefault(w => w.Id == id);

                if (obj != null)
                {
                    obj.IsCancel = true;
                    db.Appointments.AddOrUpdate(obj);
                    db.SaveChanges();
                    Msg.success = true;
                    Msg.msg = "Hủy bỏ đặt lịch";
                }
                else
                {
                    Msg.success = false;
                    Msg.msg = "Đã có lỗi khi kiểm tra cuộc hẹn";
                }
                return serializer.Serialize(Msg);
            }
        }


        /// <summary>
        /// Cập nhật cuộc hẹn thành hóa đơn Pending
        /// Cập nhật trạng thái đã pending của phiếu hẹn
        /// Tạo mới hóa đơn với thông tin lấy từ phiếu hẹn
        /// </summary>
        /// <param name="id">id phiếu hẹn</param>
        /// <param name="salonId">salon id</param>
        /// <returns>
        /// Trả về json thông tin cập nhật:
        /// 1. Cập nhật phiếu hẹn và thêm mới bill thành công thì trả về PDFBillCode
        /// 2. Cập nhật thất bại thì thông báo lỗi</returns>
        [WebMethod]
        public string SetAppointmentPending(int id, int salonId, int checkinId)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            using (var db = new Solution_30shineEntities())
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var Msg = new Msg();
                var obj = db.Appointments.FirstOrDefault(w => w.Id == id);

                if (obj != null)
                {
                    var BillCode = GenBillCode(salonId, "HD", 4);
                    var PDFname = BillCode + "_" + UIHelpers.GetUniqueKey(6, 6);
                    var bill = new BillService();
                    var error = 0;
                    obj.IsPending = true;

                    bill.IsBooked = false;
                    bill.IsBooking = false;
                    bill.Pending = 1;
                    bill.CreatedDate = DateTime.Now;
                    bill.BillCode = BillCode;
                    bill.CustomerCode = obj.CustomerCode;
                    bill.Staff_Hairdresser_Id = obj.StylistId;
                    bill.SalonId = obj.SalonId;
                    bill.PDFBillCode = PDFname;
                    bill.TeamId = obj.TeamId;
                    bill.ServiceIds = obj.ServiceIds;
                    bill.ReceptionId = checkinId;
                    bill.IsDelete = 0;

                    db.Appointments.AddOrUpdate(obj);
                    db.BillServices.AddOrUpdate(bill);
                    error += db.SaveChanges() > 0 ? 0 : 1;
                    if (error == 0)
                    {
                        // insert service to flow
                        var objService = new FlowService();
                        //var str = "[{\"Id\":\"19\",\"Price\":340000,\"Quantity\":\"1\"}]";
                        var ServiceList = serializer.Deserialize<List<ProductBasic>>(obj.ServiceIds);
                        if (ServiceList != null)
                        {
                            foreach (var v in ServiceList)
                            {
                                objService = new FlowService();
                                objService.BillId = obj.Id;
                                objService.ServiceId = v.Id;
                                objService.Price = v.Price;
                                objService.Quantity = v.Quantity;
                                objService.VoucherPercent = Convert.ToInt32(v.VoucherPercent);
                                objService.CreatedDate = DateTime.Now;
                                objService.IsDelete = 0;
                                objService.SalonId = obj.SalonId;

                                db.FlowServices.Add(objService);
                                error += db.SaveChanges() > 0 ? 0 : 1;
                            }
                        }
                    }
                    if (error == 0)
                    {
                        // print bill
                        PrintBillPDF.GenPDF(false, bill);
                        Msg.success = true;
                        Msg.msg = bill.PDFBillCode;
                    }
                }
                else
                {
                    Msg.success = false;
                    Msg.msg = "Đã có lỗi khi kiểm tra bill";
                }
                return serializer.Serialize(Msg);
            }
        }        

        /// <summary>
        /// Generate bill code
        /// </summary>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public static string GenBillCode(int salonId, string prefix, int template = 4)
        {
            using (var db = new Solution_30shineEntities())
            {
                string code = "";
                string temp = "";
                int integer;
                //var SalonId = HttpContext.Current.Session["SalonId"] != null ? (int.TryParse(HttpContext.Current.Session["SalonId"].ToString(), out integer) ? integer : 0) : 0;
                var Where = PredicateBuilder.True<BillService>();
                DateTime _FromDate = DateTime.Today;
                DateTime _ToDate = DateTime.Today.AddDays(1);
                Where = Where.And(w => w.IsDelete != 1 && w.CreatedDate >= _FromDate && w.CreatedDate < _ToDate);
                if (salonId > 0)
                {
                    Where = Where.And(w => w.SalonId == salonId);
                }
                var LastBill = db.BillServices.AsExpandable().Where(Where).OrderByDescending(w => w.Id).Take(1).ToList();
                if (LastBill.Count > 0)
                {
                    if (LastBill[0].BillCode != null && LastBill[0].BillCode != "")
                        temp = (int.TryParse(LastBill[0].BillCode.Substring(LastBill[0].BillCode.Length - template, template), out integer) ? ++integer : 0).ToString();
                    else
                        temp = "1";
                }
                else
                {
                    temp = "1";
                }

                int len = temp.Length;
                for (var i = 0; i < template; i++)
                {
                    if (len > 0)
                    {
                        code = temp[--len].ToString() + code;
                    }
                    else
                    {
                        code = "0" + code;
                    }
                }

                return prefix + String.Format("{0:ddMMyy}", DateTime.Now) + code;
            }
        }

        [WebMethod]
        public string loadBillFromBooking(int salonId)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var Msg = new Msg();
            var list = new List<cls_booking>();
            var sql = "";
            string whereSalon = "";

            if (salonId > 0)
            {
                whereSalon = " and booking.SalonId = " + salonId;
            }
            else
            {
                whereSalon = " and booking.SalonId > 0";
            }
            sql = @"select booking.Id, booking.CustomerName, booking.CustomerPhone, booking.IsMakeBill, bookhour.[Hour], bookhour.[HourFrame] ,ISNULL(s.Fullname , ' ') as StylistName
                from BooKing 
                inner join BookHour as bookhour
                on booking.HourId = bookhour.Id and booking.HourId > 0 
                left join Staff as s
                on s.Id = booking.StylistId
                where booking.IsDelete != 1 /*and (booking.IsMakeBill is null or booking.IsMakeBill != 1)*/ " + whereSalon +
                    @" and (booking.DatedBook >= '" + string.Format("{0:yyyy/MM/dd}", DateTime.Now) + "' and booking.DatedBook < '" + string.Format("{0: yyyy/MM/dd}", DateTime.Now.AddDays(1)) + "')" +
                    " order by bookhour.[HourFrame] asc, booking.Id asc";

            if (sql != null)
            {
                using (var db = new Solution_30shineEntities())
                {
                    list = db.Database.SqlQuery<cls_booking>(sql).ToList();
                }
            }

            Msg.success = true;
            Msg.msg = serializer.Serialize(list);

            return serializer.Serialize(Msg);
        }

        [WebMethod]
        public string loadBillFromBooking_V2(int salonId, int startBookingID)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var Msg = new Msg();
            var list = new List<cls_booking>();
            var sql = "";
            string whereSalon = "";

            if (salonId > 0)
            {
                whereSalon = " and booking.SalonId = " + salonId;
            }
            else
            {
                whereSalon = " and booking.SalonId > 0";
            }
            sql = @"select booking.Id, booking.CustomerName, booking.CustomerPhone, booking.IsMakeBill, bookhour.[Hour], bookhour.[HourFrame] ,ISNULL(s.Fullname , ' ') as StylistName
                from BooKing 
                inner join BookHour as bookhour
                on booking.HourId = bookhour.Id and booking.HourId > 0 
                left join Staff as s
                on s.Id = booking.StylistId
                where booking.IsDelete != 1 and (IsLimited is null or IsLimited != 1) /*and (booking.IsMakeBill is null or booking.IsMakeBill != 1)*/ " + whereSalon +
                    @" and (booking.DatedBook >= '" + string.Format("{0:yyyy/MM/dd}", DateTime.Now) + "' and booking.DatedBook < '" + string.Format("{0: yyyy/MM/dd}", DateTime.Now.AddDays(1)) + "')" +
                    @" and booking.Id > " + startBookingID +
                    " order by bookhour.[HourFrame] asc, booking.Id asc";

            if (sql != null)
            {
                using (var db = new Solution_30shineEntities())
                {
                    list = db.Database.SqlQuery<cls_booking>(sql).ToList();
                }
            }

            Msg.success = true;
            Msg.msg = serializer.Serialize(list);

            return serializer.Serialize(Msg);
        }

        [WebMethod]
        public string updateStatusForBooking(int id, bool status)
        {
            // Verify host from request
            _30shine.Helpers.SecurityLib.VerifyHostFromRequest();

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var Msg = new Msg();
            using (var db = new Solution_30shineEntities())
            {
                var error = 0;
                var obj = db.Bookings.FirstOrDefault(w => w.Id == id);
                if (obj != null)
                {
                    obj.IsMakeBill = status;
                    db.Bookings.AddOrUpdate(obj);
                    error += db.SaveChanges() > 0 ? 0 : 1;
                }
                if (error == 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }

            return serializer.Serialize(Msg);
        }
    }



    /*CÁC ĐỐI TƯỢNG
    BillServicePending : chứa thông tin về Stylist, Skinner, khách hành, mỹ phẩm(nếu có), team, thời gian tạo.
    StaffTmp: chứa thông tin về nhân viên 
    Appointment2: kế thừa Appointment và thêm một số thuộc tính miêu tả khách hàng, thời gian thông báo, sắp xếp
    */

    public class BillServicePendingByFloor : BillServicePending
    {
        public int Floor { get; set; }
        public int TeamId { get; set; }
        public string ServiceName { get; set; }
        public int Order { get; set; }
    }

    public class BillServicePending
    {
        public int Id { get; set; }
        public string HairdresserName { get; set; }
        public string HairMassageName { get; set; }
        public string CosmeticName { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public string CreatedTime { get; set; }
        public string TeamColor { get; set; }

        public string BillCode { get; set; }

    }

    public class StaffTmp
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public int Type { get; set; }
        public string OrderCode { get; set; }
    }

    public class cls_billStatus
    {
        public int Id { get; set; }
        public string Time { get; set; }
        public string StatusText { get; set; }
        public int StatusValue { get; set; }
        public string StatusClass { get; set; }
    }

    public class Appointment2 : Appointment
    {
        public string HairdresserName { get; set; }
        public string SellerName { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public int Order { get; set; }
        public DateTime NotifyTime { get; set; }
    }

    public class cls_booking
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public string Hour { get; set; }
        public TimeSpan HourFrame { get; set; }

        public string StylistName { get; set; }
        public bool? IsMakeBill { get; set; }
    }
}

