﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using System.Web.Script.Serialization;
using _30shine.Helpers;
using System.Web.Script.Services;
using System.Security.Cryptography;
using System.Text;
using LinqKit;
using _30shine.MODEL.CustomClass;
using System.IO;
using System.Threading.Tasks;

namespace _30shine.GUI.SystemService.Webservice
{
    /// <summary>
    /// Summary description for WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class wsBillService : System.Web.Services.WebService
    {
        public string PATH_ASYNC_CHECKOUT_BILL_LOG = @"30ShineSystemLog\erp.30shine.com\" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + "_async_checkout_bill_statistic.txt";
        public string PATH_ASYNC_CHECKOUT_BILL_LOG_EXCEPTION = @"30ShineSystemLog\erp.30shine.com\" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Day.ToString() + "_async_checkout_bill_statistic_exception.txt";

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public void ThongKeVatTuNewBill(Data_ThongkeVattu data)
        {
            try
            {
                _ThongKeVatTuNewBill(data.StylistId, data.CompletedTime);
                _ThongKeVatTuNewBill(data.SkinnerId, data.CompletedTime);
                Library.Function.WriteToFile("ThongKeVatTuNewBill " + Library.Format.getDateTimeString(DateTime.Now, "yyyy/MM/dd HH:mm:ss") + " succeed: " + Library.Function.JavaScript.Serialize(data), PATH_ASYNC_CHECKOUT_BILL_LOG);
            }
            catch (Exception ex)
            {
                Library.Function.WriteToFile("ThongKeVatTuNewBill " + DateTime.Now + " failed: " + (Library.Function.JavaScript.Serialize(data)) + " ex " + ex.StackTrace, PATH_ASYNC_CHECKOUT_BILL_LOG);
                Library.Function.WriteToFile("ThongKeVatTuNewBill Exception " + DateTime.Now + ": " + (Library.Function.JavaScript.Serialize(data)) + " ex " + ex.StackTrace, PATH_ASYNC_CHECKOUT_BILL_LOG_EXCEPTION);
                //throw ex;
            }
        }

        protected void _ThongKeVatTuNewBill(int? staffId, DateTime? completedTime)
        {
            using (var db = new Solution_30shineEntities())
            {
                // lay  obj Xuat vat tu gan nhat
                var ObjExportGoods = db.ExportGoods.Where(a => a.IsDelete == 0 && a.RecipientId == staffId).
                                                            OrderByDescending(a => a.CreatedDate).OrderByDescending(a => a.Id).
                                                            Select(a => new { a.Id }).FirstOrDefault();
                // Lay danh sach Tatistic Xuat vat tu
                if (ObjExportGoods != null)
                {
                    var LstSatisticXuatVatTu = db.Statistics_XuatVatTu.Where(a => a.IsDelete == false
                                                                       && a.ExportGoods_ID == ObjExportGoods.Id
                                                                       && a.Date_XuatVatTu < completedTime).ToList();
                    var _CountLST = LstSatisticXuatVatTu.Count;
                    if (_CountLST > 0)
                    {
                        for (int i = 0; i < _CountLST; i++)
                        {
                            LstSatisticXuatVatTu[i].TotalBill_XuatVatTu = LstSatisticXuatVatTu[i].TotalBill_XuatVatTu + 1;
                            db.SaveChanges();
                        }
                    }
                }
            }
        }

        [WebMethod]
        public void ThongKeVatTuUpdateBill(Data_ThongkeVattu data)
        {
            try
            {
                _ThongKeVatTuUpdateBill(data.StylistOldId, data.StylistId, data.CompletedTime);
                _ThongKeVatTuUpdateBill(data.SkinnerOldId, data.SkinnerId, data.CompletedTime);
                Library.Function.WriteToFile("ThongKeVatTuUpdateBill " + DateTime.Now + " succeed: " + Library.Function.JavaScript.Serialize(data), PATH_ASYNC_CHECKOUT_BILL_LOG);
            }
            catch (Exception ex)
            {
                Library.Function.WriteToFile("ThongKeVatTuUpdateBill " + DateTime.Now + " failed: " + (Library.Function.JavaScript.Serialize(data)) + " ex " + ex.StackTrace, PATH_ASYNC_CHECKOUT_BILL_LOG);
                Library.Function.WriteToFile("ThongKeVatTuUpdateBill Exception " + DateTime.Now + ": " + (Library.Function.JavaScript.Serialize(data)) + " ex " + ex.StackTrace, PATH_ASYNC_CHECKOUT_BILL_LOG_EXCEPTION);
                //throw ex;
            }
        }

        protected void _ThongKeVatTuUpdateBill(int? Staff_Old_ID, int? Staff_New_ID, DateTime? ComPleteTime)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    if (Staff_Old_ID != Staff_New_ID)
                    {

                        // lay  obj Xuat vat tu gan nhat nhan vien cu
                        var ObjExportGoodsOld = db.ExportGoods.Where(a => a.IsDelete == 0 && a.RecipientId == Staff_Old_ID).
                                                                    OrderByDescending(a => a.CreatedDate).OrderByDescending(a => a.Id).
                                                                    Select(a => new { a.Id }).FirstOrDefault();
                        // Lay danh sach Tatistic Xuat vat tu
                        if (ObjExportGoodsOld != null)
                        {
                            var LstSatisticXuatVatTuOld = db.Statistics_XuatVatTu.Where(a => a.IsDelete == false &&
                                                                                           a.ExportGoods_ID == ObjExportGoodsOld.Id &&
                                                                                           a.Date_XuatVatTu < ComPleteTime).ToList();
                            var _CountLSTOld = LstSatisticXuatVatTuOld.Count;
                            if (_CountLSTOld > 0)
                            {
                                for (int i = 0; i < _CountLSTOld; i++)
                                {
                                    LstSatisticXuatVatTuOld[i].TotalBill_XuatVatTu = LstSatisticXuatVatTuOld[i].TotalBill_XuatVatTu - 1;
                                    db.SaveChanges();
                                }
                            }
                        }
                        // lay  obj Xuat vat tu gan nhat nhan vien moi
                        var ObjExportGoodsNew = db.ExportGoods.Where(a => a.IsDelete == 0 && a.RecipientId == Staff_New_ID).
                                                                    OrderByDescending(a => a.CreatedDate).OrderByDescending(a => a.Id).
                                                                    Select(a => new { a.Id }).FirstOrDefault();
                        // Lay danh sach Statistic Xuat vat tu
                        if (ObjExportGoodsNew != null)
                        {
                            var LstSatisticXuatVatTuNew = db.Statistics_XuatVatTu.Where(a => a.IsDelete == false
                                                                    && a.ExportGoods_ID == ObjExportGoodsNew.Id
                                                                    && a.Date_XuatVatTu < ComPleteTime).ToList();
                            var _CountLSTNew = LstSatisticXuatVatTuNew.Count;
                            if (_CountLSTNew > 0)
                            {
                                for (int i = 0; i < _CountLSTNew; i++)
                                {
                                    LstSatisticXuatVatTuNew[i].TotalBill_XuatVatTu = LstSatisticXuatVatTuNew[i].TotalBill_XuatVatTu + 1;
                                    db.SaveChanges();
                                }
                            }
                        }

                    }
                }


            }
            catch (Exception ex)
            {
                //throw ex;
            }
        }

        [WebMethod]
        public void UpdateTotalBillCustomer(DataCustomer DataCustomer)
        {
            using(var db = new Solution_30shineEntities())
            {
                db.Database.ExecuteSqlCommand("UPDATE dbo.Customer SET TotalBillservice = ISNULL(TotalBillservice,0) + 1 WHERE Id = " + DataCustomer.Id + " AND IsDelete = 0");
            }
        }

        [WebMethod]
        public void StatisticSalary(Data_StatisticSalary data)
        {
            try
            {
                SalaryLib.Instance.updateFlowSalaryByBill(data.Bill, data.IsVoucher);
                Library.Function.WriteToFile("StatisticSalary " + DateTime.Now + " succeed: " + Library.Function.JavaScript.Serialize(
                    new
                    {
                        BillId = data.Bill.Id,
                        StylistId = data.Bill.Staff_Hairdresser_Id,
                        SkinnerId = data.Bill.Staff_HairMassage_Id,
                        CheckinId = data.Bill.ReceptionId,
                        CheckoutId = data.Bill.CheckoutId,
                        Sellerid = data.Bill.ServiceIds,
                        ListServices = data.Bill.ServiceIds,
                        ListProduct = data.Bill.ProductIds,
                        CreatedTime = data.Bill.CreatedDate
                    }), PATH_ASYNC_CHECKOUT_BILL_LOG);
            }
            catch (Exception ex)
            {
                Library.Function.WriteToFile("StatisticSalary " + DateTime.Now + " failed: " + (Library.Function.JavaScript.Serialize(
                    new {
                            BillId = data.Bill.Id,
                            StylistId = data.Bill.Staff_Hairdresser_Id,
                            SkinnerId = data.Bill.Staff_HairMassage_Id,
                            CheckinId = data.Bill.ReceptionId,
                            CheckoutId = data.Bill.CheckoutId,
                            Sellerid = data.Bill.ServiceIds,
                            ListServices = data.Bill.ServiceIds,
                            ListProduct = data.Bill.ProductIds,
                            CreatedTime = data.Bill.CreatedDate
                    })) + " ex " + ex.StackTrace, PATH_ASYNC_CHECKOUT_BILL_LOG);
                Library.Function.WriteToFile("StatisticSalary Exception " + DateTime.Now + ": " + (Library.Function.JavaScript.Serialize(
                    new
                    {
                        BillId = data.Bill.Id,
                        StylistId = data.Bill.Staff_Hairdresser_Id,
                        SkinnerId = data.Bill.Staff_HairMassage_Id,
                        CheckinId = data.Bill.ReceptionId,
                        CheckoutId = data.Bill.CheckoutId,
                        Sellerid = data.Bill.ServiceIds,
                        ListServices = data.Bill.ServiceIds,
                        ListProduct = data.Bill.ProductIds,
                        CreatedTime = data.Bill.CreatedDate
                    })) + " ex " + ex.StackTrace, PATH_ASYNC_CHECKOUT_BILL_LOG_EXCEPTION);
            }
        }

        protected string Serialize(dynamic obj)
        {
            JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
            return jsonSerializer.Serialize(obj);
        }

        [WebMethod]
        public void Task1(Data_ThongkeVattu data)
        {
            try
            {
                var task = Task.Run(async delegate
                {
                    await Task.Delay(3000);
                    return DateTime.Now;
                });
                //task.Wait();
                Library.Function.WriteToFile("---------------------------------------------", PATH_ASYNC_CHECKOUT_BILL_LOG);
                Library.Function.WriteToFile("Task call " + Library.Format.getDateTimeString(DateTime.Now, "yyyy/MM/dd HH:mm:ss"), PATH_ASYNC_CHECKOUT_BILL_LOG);
                Library.Function.WriteToFile("Task finish " + Library.Format.getDateTimeString(task.Result, "yyyy/MM/dd HH:mm:ss") + " succeed: " + Library.Function.JavaScript.Serialize(data), PATH_ASYNC_CHECKOUT_BILL_LOG);
            }
            catch (Exception ex)
            {
                Library.Function.WriteToFile("Task exception " + Library.Format.getDateTimeString(DateTime.Now, "yyyy/MM/dd HH:mm:ss") + " failed : " + ex.StackTrace, PATH_ASYNC_CHECKOUT_BILL_LOG_EXCEPTION);
            }
        }
    }
}
