﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using static _30shine.GUI.FrontEnd.UIService.TimeLine_Beta_V2;

namespace _30shine.GUI.SystemService.Webservice
{
    /// <summary>
    /// Summary description for Timeline
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class Timeline : System.Web.Services.WebService
    {

        [WebMethod]
        public object EstimateTimeShampoo(string SalonUid)
        {
            try
            {
                var db = new Solution_30shineEntities();
                Guid? GuidSalinUid = Guid.Parse(SalonUid);
                var IdSalon = db.Tbl_Salon.Where(a => a.Uid == GuidSalinUid).Select(a => a.Id).FirstOrDefault();
                var data = db.Store_Estimate_TimeMassage(IdSalon).ToList();
                List<cls_EstimateTimeMassage> listCls = new List<cls_EstimateTimeMassage>();
                foreach (var item in data)
                {
                    cls_EstimateTimeMassage cls = new cls_EstimateTimeMassage();
                    cls.danggoiTg = (item.DANGGOI_TIME_CONLAI != null ? item.DANGGOI_TIME_CONLAI.Value : 0) + (item.CHUAGOI != null ? item.CHUAGOI.Value : 0) * 12;
                    cls.chuagoiSl = item.CHUAGOI != null ? item.CHUAGOI.Value : 0;
                    cls.danggoiSl = item.DANGGOI_SL != null ? item.DANGGOI_SL.Value : 0;
                    cls.skinnerName = item.SkinnerName;
                    cls.hoachat = item.HOACHAT != null ? item.HOACHAT.Value : 0;
                    listCls.Add(cls);
                }
                listCls = listCls.OrderBy(o => o.chuagoiSl).ThenBy(o => o.danggoiTg).ThenBy(o => o.danggoiSl).ToList();
                return new JavaScriptSerializer().Serialize(listCls);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }


        [WebMethod]
        public object CountSpecialRequire(string SalonUid)
        {
            int salonId = 0;
            Guid? GuidSalinUid = Guid.Parse(SalonUid);
            Solution_30shineEntities db = new Solution_30shineEntities();
            salonId = db.Tbl_Salon.Where(a => a.Uid == GuidSalinUid).Select(a => a.Id).FirstOrDefault();
            var count = db.Tracking_Cus_Special_Requirements_Store(DateTime.Now.Date, salonId).Where(s => s.SalonNote == null || s.SalonNote == "").Count();
            return new JavaScriptSerializer().Serialize(count);
        }

        [WebMethod]
        public string MigrateBilltemp(int salonId)
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
            string msg = "";
            DateTime today = DateTime.Now.Date;

            var listBill = db.BillServices.Where(w=>w.IsDelete == 0 && w.SalonId == salonId && w.CreatedDate > today).ToList();
            var listBillTemp = db.SM_BillTemp.Where(w => w.IsDelete == 0 && w.SalonId == salonId && w.CreatedDate > today).ToList();
            var itemBillTemp = new SM_BillTemp();
            int index = -1;
            if (listBill.Count == 0)
            {
                msg = "No data.";
            }
            if (listBill.Count > 0)
            {
                foreach (var v in listBill)
                {
                    //if (v.Id != 1427264)
                    //{
                    //    continue;
                    //}
                    index = listBillTemp.FindIndex(w=>w.BillServiceID == v.Id);
                    if (index != -1)
                    {
                        continue;
                    }

                    itemBillTemp = new SM_BillTemp();
                    itemBillTemp.BillCode = v.BillCode;
                    itemBillTemp.BillServiceID = v.Id;
                    itemBillTemp.BillStatusId = v.BillStatusId;
                    itemBillTemp.CompleteBillTime = v.CompleteBillTime;
                    itemBillTemp.CreatedDate = v.CreatedDate;
                    itemBillTemp.CustomerId = v.CustomerId;
                    itemBillTemp.FeeCOD = v.FeeCOD;
                    itemBillTemp.FeeExtra = v.FeeExtra;
                    itemBillTemp.HCItem = v.HCItem;
                    itemBillTemp.Images = v.Images;
                    itemBillTemp.InProcedureTime = v.InProcedureTime;
                    itemBillTemp.InProcedureTimeModifed = v.InProcedureTimeModifed;
                    itemBillTemp.IsDelete = v.IsDelete;
                    itemBillTemp.IsOnline = v.IsOnline;
                    itemBillTemp.IsPayByCard = v.IsPayByCard;
                    itemBillTemp.IsX2 = v.IsX2;
                    itemBillTemp.Mark = v.Mark;
                    itemBillTemp.MigrateStatus = v.MigrateStatus;
                    itemBillTemp.ModifiedDate = v.ModifiedDate;
                    itemBillTemp.Note = v.Note;
                    itemBillTemp.Paid = v.Paid;
                    itemBillTemp.PayMethodId = v.PayMethodId;
                    itemBillTemp.PDFBillCode = v.BillCode;
                    itemBillTemp.ProductIds = v.ProductIds;
                    itemBillTemp.ReceptionId = v.ReceptionId;
                    itemBillTemp.SalonId = v.SalonId;
                    itemBillTemp.SellerId = v.SellerId;
                    itemBillTemp.ServiceIds = v.ServiceIds;
                    itemBillTemp.Staff_Hairdresser_Id = v.Staff_Hairdresser_Id;
                    itemBillTemp.Staff_HairMassage_Id = v.Staff_HairMassage_Id;
                    itemBillTemp.Status = v.Status;
                    itemBillTemp.TeamId = v.TeamId;
                    itemBillTemp.TotalMoney = v.TotalMoney;
                    itemBillTemp.VIPCard = v.VIPCard;
                    itemBillTemp.VIPCardGive = v.VIPCardGive;
                    itemBillTemp.VIPCardUse = v.VIPCardUse;
                    var booking = db.Bookings.FirstOrDefault(w=>w.Id == v.BookingId);
                    if (booking != null && booking.BookingTempId != null)
                    {
                        itemBillTemp.BookingId = booking.BookingTempId;
                    }
                    db.SM_BillTemp.AddOrUpdate(itemBillTemp);
                    db.SaveChanges();

                    // Update [SM_BillTemp_FlowService]
                    insertFlowServiceTemp(itemBillTemp);
                }
                msg = "Done.";
            }

            return msg;
        }

        /// <summary>
        /// Insert SM_BillTemp_FlowService
        /// </summary>
        /// <param name="billTemp"></param>
        /// <returns>0:success|1:error</returns>
        public int insertFlowServiceTemp(SM_BillTemp billTemp)
        {
            try
            {
                var error = 0;
                var db = new Solution_30shineEntities();
                var listServices = new JavaScriptSerializer().Deserialize<List<ProductBasic>>(billTemp.ServiceIds).ToList();
                if (listServices.Count > 0)
                {
                    foreach (var v in listServices)
                    {
                        var flowServiceTemp = new SM_BillTemp_FlowService();
                        flowServiceTemp.BillId = v.Id;
                        flowServiceTemp.ServiceId = v.Id;
                        flowServiceTemp.Price = v.Price;
                        flowServiceTemp.Quantity = v.Quantity;
                        try
                        {
                            flowServiceTemp.VoucherPercent = Convert.ToInt32(v.VoucherPercent);
                        }
                        catch
                        {
                            flowServiceTemp.VoucherPercent = 0;
                        }
                        var coefficientRating = db.Services.FirstOrDefault(w => w.Id == v.Id);
                        if (coefficientRating != null && coefficientRating.CoefficientRating != null)
                        {
                            flowServiceTemp.CoefficientRating = coefficientRating.CoefficientRating;
                        }
                        else
                        {
                            flowServiceTemp.CoefficientRating = 0;
                        }
                        flowServiceTemp.CreatedDate = DateTime.Now;
                        flowServiceTemp.IsDelete = 0;
                        flowServiceTemp.SalonId = billTemp.SalonId;
                        flowServiceTemp.SellerId = billTemp.SellerId;

                        db.SM_BillTemp_FlowService.AddOrUpdate(flowServiceTemp);
                    }
                    db.SaveChanges();
                }
                return 0;
            }
            catch (Exception ex)
            {
                return 1;
            }
        }

    }
}
