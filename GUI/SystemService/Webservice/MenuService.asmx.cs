﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;

namespace _30shine.GUI.SystemService.Webservice
{
    /// <summary>
    /// Summary description for MenuService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [ScriptService]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class MenuService : System.Web.Services.WebService
    {
        private List<cls_Menu> _listMenu = new List<cls_Menu>();

        [WebMethod(EnableSession = true)]

        public object ListMenu()
        {
            try
            {


                var permissionID = HttpContext.Current.Session["User_Permission_ID"].ToString();
                var permission = HttpContext.Current.Session["User_Permission"].ToString();
                List<cls_Menu> listMenu = new List<cls_Menu>();
                Solution_30shineEntities db = new Solution_30shineEntities();
                if (permission == "root")
                {

                    _listMenu = db.Database.SqlQuery<cls_Menu>("select * from Tbl_Permission_Menu where mParentID is not null and mPublish != 0 and mID NOT IN (206)").ToList();
                }
                else
                {
                    Int32[] p = permissionID.Split(',').Select(str => Convert.ToInt32(str)).ToArray();
                    var _perm = String.Join(",", p);

                    _listMenu = db.Database.SqlQuery<cls_Menu>("with myTemp(id,maxlenght) as (select mID, MAX(LEN(aID)) from Tbl_Permission_Map where pID in (" + _perm + ") group by mID) select menu.* from Tbl_Permission_Map m inner join myTemp t on m.mID = t.id inner join Tbl_Permission_Menu menu on menu.mID = m.mID where pID in (" + _perm + ") and LEN(m.aID) = t.maxlenght and menu.mParentID is not null").ToList();
                }

                foreach (var item in _listMenu)
                {
                    cls_Menu clsMenu = new cls_Menu();
                    clsMenu.mID = item.mID;
                    clsMenu.mName = item.mName;
                    clsMenu.Url_Rewrite = item.Url_Rewrite;
                    clsMenu.mParentID = (int)item.mParentID;
                    listMenu.Add(clsMenu);
                }
                List<cls_Menu> menuTree = getMenuTree(listMenu, 0);
                JavaScriptSerializer js = new JavaScriptSerializer();
                //try
                //{
                //    if (File.Exists(HttpContext.Current.Server.MapPath("/Config/" + permission + ".txt")))
                //    {
                //        File.Delete(HttpContext.Current.Server.MapPath("/Config/" + permission + ".txt"));
                //        using (StreamWriter file = new StreamWriter(HttpContext.Current.Server.MapPath("/Config/" + permission + ".txt"), true))
                //        {

                //            file.WriteLine(new JavaScriptSerializer().Serialize(menuTree)); // Write the file.
                //            file.Close();
                //        }
                //    }
                //    else
                //    {
                //        using (StreamWriter file = new StreamWriter(HttpContext.Current.Server.MapPath("/Config/" + permission + ".txt"), true))
                //        {
                //            file.WriteLine(new JavaScriptSerializer().Serialize(menuTree)); // Write the file.
                //            file.Close();
                //        }
                //    }
                //}
                //catch (Exception ex)
                //{

                //    throw new Exception(ex.Message);
                //}
                return js.Serialize(menuTree);

            }
            catch (Exception ex)
            {

                throw new Exception(ex.ToString());
            }
        }

        #region
        [WebMethod(EnableSession = true)]
        public object getMenu_Mobile()
        {
            //string permission = HttpContext.Current.Session["User_Permission"].ToString();
            int staffId = Convert.ToInt32(HttpContext.Current.Session["User_Id"].ToString());
            Solution_30shineEntities db = new Solution_30shineEntities();
            var lsParentMenu = (from m in db.Tbl_Permission_Menu
                                join map in db.Tbl_Permission_Map_V2 on m.mID equals map.mID
                                join role in db.Tbl_Permission_Staff_Roles on map.pID equals role.PermissionId
                                where m.mParentID == 0 && role.StaffId == staffId && map.mapPublish == true
                                select m
                                ).ToList();
            List<cls_Menu> listMenu = new List<cls_Menu>();
            foreach (var item in lsParentMenu.Distinct().ToList())
            {
                cls_Menu cls = new cls_Menu();
                cls.mID = item.mID;
                cls.mName = item.mName;
                cls.cssTag = item.mClassTag;
                cls.mIcon = item.mIcon;
                cls.lstMenu = db.Database.SqlQuery<cls_Menu>("WITH temp(mID, mName,Url_Rewrite,CssTag,isPublish,mParentID,MenuShow, alevel) " +
                                                             "as ( Select m.mID, m.mName, m.Url_Rewrite, ISNULL(m.mClassTag,0)," +
                                                             " (CASE when m.mID in (select mID from Tbl_Permission_Map_V2" +
                                                             " where pID IN (SELECT PermissionId from Tbl_Permission_Staff_Roles WHERE StaffId = " + staffId + " ))  then 1 else 0 end)," +
                                                             " m.mParentID,m.MenuShow, 0 as aLevel From Tbl_Permission_Menu m Where m.mID = " + item.mID + " " +
                                                             "Union All " +
                                                             "Select b.mID, b.mName,b.Url_Rewrite,ISNULL(b.mClassTag,0)," +
                                                             "(CASE when b.mID in (select mID from Tbl_Permission_Map_V2 where pID IN (SELECT PermissionId from Tbl_Permission_Staff_Roles WHERE StaffId = " + staffId + " )) then 1 else 0 end), " +
                                                             "b.mParentID,b.MenuShow, a.alevel + 1 From temp as a, Tbl_Permission_Menu as b Where a.mID = b.mParentID) " +
                                                             "Select * From temp where mParentID != 0 AND MenuShow = 1").ToList();
                listMenu.Add(cls);
            }
            return new JavaScriptSerializer().Serialize(listMenu.Distinct().ToList());
        }
        #endregion


        public string ConvertToUnsign3(string str)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = str.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty)
                        .Replace('\u0111', 'd').Replace('\u0110', 'D');
        }

        /// <summary>
        /// Get menu mobile
        /// </summary>
        [WebMethod(EnableSession = true)]
        public object GetMenuMobile()
        {
            List<Menu> menu = new List<Menu>();
            using (Solution_30shineEntities db = new Solution_30shineEntities())
            {
                //get menu parents
                int integer;
                int staffId = int.TryParse(HttpContext.Current.Session["User_Id"].ToString(), out integer) ? integer : 0;
                if (staffId == 0)
                {
                    List<PermissionMenu> permMenu = (
                                                    from m in db.PermissionMenus.AsNoTracking()
                                                    where
                                                        m.Pid == 0
                                                        && m.Pid != null
                                                        && m.IsDelete == false
                                                        && m.IsActive == true
                                                    select m
                                                ).ToList();
                    for (int i = 0; i < permMenu.Count; i++)
                    {
                        Menu menuRecord = new Menu();
                        menuRecord.menuId = permMenu[i].Id;
                        menuRecord.menuName = permMenu[i].Name;
                        menuRecord.menuClass = permMenu[i].ClassTag;
                        menuRecord.menuIcon = permMenu[i].IconImage;
                        //get menu children
                        menuRecord.lstMenu = GetChildMenusRoot(permMenu[i].Id);
                        menu.Add(menuRecord);
                    }
                }
                else
                {
                    List<Menu> permMenu = getAllmenu(staffId);
                    List<Menu> permMenuParent = getchildrenMenu(0, permMenu);
                    for (int i = 0; i < permMenuParent.Count; i++)
                    {
                        Menu menuRecord = new Menu();
                        menuRecord.menuId = permMenuParent[i].menuId;
                        menuRecord.menuName = permMenuParent[i].menuName;
                        menuRecord.menuClass = permMenuParent[i].menuClass;
                        menuRecord.menuIcon = permMenuParent[i].menuIcon;
                        //get menu children
                        menuRecord.lstMenu = GetChildMenusStaff(staffId, permMenuParent[i].menuId);
                        menu.Add(menuRecord);
                    }
                }
            }
            return new JavaScriptSerializer().Serialize(menu.ToList());
        }


        /// <summary>
        ///  //Get chirlden menu
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="allMenu"></param>
        /// <returns></returns>
        public List<Menu> getchildrenMenu(int parentId, List<Menu> allMenu)

        {
            List<Menu> list = (from a in allMenu
                                          where a.menuParentId == parentId
                                          select a).ToList();
            return list;
        }

        public List<clsChildMenu> GetChildMenusRoot(int menuPar)
        {
            List<clsChildMenu> list = new List<clsChildMenu>();
            using (var db = new Solution_30shineEntities())
            {
                string sql = @"DECLARE @menuParentId INT
							    SET @menuParentId = '"+menuPar+@"';
							    BEGIN
                                WITH 
							    menuPar AS
                                    (
									    SELECT 
										    menu.Id AS menuId,menu.Pid AS menuParentId,
										    menu.Name AS menuName, menu.Link AS menuLink, menu.Path AS menuPath
									    FROM dbo.PermissionMenu  AS menu
									    WHERE	
										    menu.IsDelete = 0 
										    AND menu.IsActive = 1
										    AND menu.Pid = 0
										    AND menu.Id = @menuParentId
									    GROUP BY menu.Id,menu.Pid,menu.Name,menu.Link,menu.[Path]
                                    ),
                                menuFilter AS 
                                    (
	                                    SELECT menuFilter.* FROM dbo.PermissionMenu AS menuFilter
	                                    INNER JOIN menuPar ON menuPar.menuId = menuFilter.Pid
                                    ),
                                menuChild AS 
                                    (
                                        SELECT menuChildren.* FROM dbo.PermissionMenu AS menuChildren
	                                    INNER JOIN menuFilter ON menuChildren.Pid = menuFilter.Id	                                        
									    WHERE menuChildren.Pid IS NOT NULL AND menuChildren.Pid > 0
                                    )
                                    SELECT menuChild.Id as childId, menuChild.Name as childName, menuChild.Link as childLink, ISNULL(menuChild.ClassTag, '') AS childClass, ISNULL(menuChild.IconImage, '') AS childIcon FROM menuChild where menuChild.IsDelete = 0 and menuChild.IsActive = 1 
                                UNION
                                SELECT menu.Id as childId, menu.Name as childName, menu.Link as childLink, ISNULL(menu.ClassTag, '') AS childClass, ISNULL(menu.IconImage, '') AS childIcon FROM dbo.PermissionMenu AS menu
                                INNER JOIN menuChild ON menuChild.Id = menu.Pid
                                WHERE ((menu.Pid > 0) and (menu.Pid IS NOT NULL)) AND menu.Name IS NOT NULL AND menu.Name != '' and menu.IsDelete = 0 and menu.IsActive = 1
							    UNION
							    SELECT menu.Id as childId, menu.Name as childName, menu.Link as childLink, ISNULL(menu.ClassTag, '') AS childClass, ISNULL(menu.IconImage, '') AS childIcon FROM dbo.PermissionMenu AS menu
                                INNER JOIN menuPar ON menuPar.menuId = menu.Pid
                                WHERE ((menu.Pid > 0) and (menu.Pid IS NOT NULL)) AND menu.Name IS NOT NULL AND menu.Name != '' and menu.IsDelete = 0 and menu.IsActive = 1
							    ORDER BY childId ASC
	                            END";
                list = db.Database.SqlQuery<clsChildMenu>(sql).ToList();
                return list;
            }
        }

        public List<clsChildMenu> GetChildMenusStaff(int staffId, int menuPar)
        {
            List<clsChildMenu> list = new List<clsChildMenu>();
            using (var db = new Solution_30shineEntities())
            {
                string sql = @" DECLARE 
								@staffId INT,
								@menuParentId INT
								SET @staffId = '" + staffId + @"';
								SET @menuParentId = '" + menuPar + @"';
								BEGIN
                                WITH 
								menuPar AS
                                    (
										SELECT 
											menu.Id AS menuId,menu.Pid AS menuParentId,
											menu.Name AS menuName, menu.Link AS menuLink, menu.ClassTag AS menuClass, menu.IconImage AS menuIcon
										FROM dbo.PermissionMenu  AS menu
											INNER JOIN dbo.PermissionMenuAction AS perAction ON menu.Id = perAction.PageId
											INNER JOIN dbo.PermissionStaff AS perStaff ON perAction.PermissionId = perStaff.PermissionId
										WHERE	menu.IsDelete = 0 AND 
											menu.IsActive = 1 AND 
											perAction.IsDelete = 0 AND 
											perStaff.StaffId = @staffId AND 
											perStaff.IsDelete = 0 AND 
											perStaff.IsActive = 1 AND 
											perAction.IsActive = 1
											AND menu.Pid = 0
											AND menu.Id = @menuParentId
										GROUP BY menu.Id,menu.Pid,menu.Name,menu.Link, menu.ClassTag, menu.IconImage
                                    ),
                                menuFilter AS 
                                    (
	                                    SELECT menuFilter.* FROM dbo.PermissionMenu AS menuFilter
	                                    INNER JOIN menuPar ON menuPar.menuId = menuFilter.Pid
                                    ),
                                menuChild AS 
                                    (
                                        SELECT menuChildren.* FROM dbo.PermissionMenu AS menuChildren
	                                    INNER JOIN menuFilter ON menuChildren.Pid = menuFilter.Id	                                        
										WHERE menuChildren.Pid IS NOT NULL AND menuChildren.Pid > 0
                                    ),
								menuChild2 AS
                                    (
										SELECT menuChild.Id as childId, menuChild.Name as childName, menuChild.Link as childLink, ISNULL(menuChild.ClassTag, '') AS childClass, ISNULL(menuChild.IconImage, '') AS childIcon FROM menuChild where menuChild.IsDelete = 0 and menuChild.IsActive = 1 
										UNION
										SELECT menu.Id as childId, menu.Name as childName, menu.Link as childLink, ISNULL(menu.ClassTag, '') AS childClass, ISNULL(menu.IconImage, '') AS childIcon FROM dbo.PermissionMenu AS menu
										INNER JOIN menuChild ON menuChild.Id = menu.Pid
										WHERE ((menu.Pid > 0) and (menu.Pid IS NOT NULL)) AND menu.Name IS NOT NULL AND menu.Name != '' and menu.IsDelete = 0 and menu.IsActive = 1
										UNION
										SELECT menu.Id as childId, menu.Name as childName, menu.Link as childLink, ISNULL(menu.ClassTag, '') AS childClass, ISNULL(menu.IconImage, '') AS childIcon FROM dbo.PermissionMenu AS menu
										INNER JOIN menuPar ON menuPar.menuId = menu.Pid
										WHERE ((menu.Pid > 0) and (menu.Pid IS NOT NULL)) AND menu.Name IS NOT NULL AND menu.Name != '' and menu.IsDelete = 0 and menu.IsActive = 1
									)
									SELECT 
											menu.childId AS childId,
											menu.childName AS childName, menu.childLink AS childLink, menu.childClass AS childClass, menu.childIcon AS childIcon
										FROM menuChild2  AS menu
											INNER JOIN dbo.PermissionMenuAction AS perAction ON menu.childId = perAction.PageId
											INNER JOIN dbo.PermissionStaff AS perStaff ON perAction.PermissionId = perStaff.PermissionId
										WHERE	
											perAction.IsDelete = 0 AND 
											perStaff.StaffId = @staffId AND 
											perStaff.IsDelete = 0 AND 
											perStaff.IsActive = 1 AND 
											perAction.IsActive = 1
										GROUP BY menu.childId,menu.childName,menu.childLink, menu.childClass, menu.childIcon
	                            END";
                list = db.Database.SqlQuery<clsChildMenu>(sql).ToList();
                return list;
            }
        } 

        /// <summary>
        /// //Function cắt chuỗi rewite sang pageId
        /// </summary>
        /// <param name="staffId"></param>
        /// <returns></returns>
        public List<Menu> getAllmenu(int staffId)
        {
            List<Menu> list = new List<Menu>();
            using (var db = new Solution_30shineEntities())
            {
                string sql = @"DECLARE @staffId INT
                                SET @staffId = '" + staffId + @"';
                                SELECT menu.Id AS menuId,menu.Pid AS menuParentId,menu.Name AS menuName
	                                  ,menu.Link AS menuLink, ISNULL(menu.ClassTag,'') AS menuClass, ISNULL(menu.IconImage,'') AS menuIcon 
                                FROM dbo.PermissionMenu  AS menu
	                                INNER JOIN dbo.PermissionMenuAction AS perAction ON menu.Id = perAction.PageId
	                                INNER JOIN dbo.PermissionStaff AS perStaff ON perAction.PermissionId = perStaff.PermissionId
                                WHERE	
                                    menu.IsDelete = 0
							        AND menu.IsActive = 1 
								    AND perAction.IsDelete = 0 
								    AND perStaff.StaffId = @staffId 
								    AND perStaff.IsDelete = 0 
								    AND perStaff.IsActive = 1 
								    AND perAction.IsActive = 1
								    AND menu.Pid IS NOT NULL
								    AND menu.Name != '' AND menu.Name IS NOT NULL
                                GROUP BY menu.Id,menu.Pid,menu.Name,menu.Link,menu.ClassTag,menu.IconImage";
                list = db.Database.SqlQuery<Menu>(sql).ToList();
                return list;
            }
        } 

        private List<cls_Menu> getMenuTree(List<cls_Menu> list, int? parentID)
        {
            return list.Where(x => x.mParentID == parentID).Select(x => new cls_Menu
            {
                mID = x.mID,
                mName = x.mName,
                mParentID = x.mParentID,
                Url_Rewrite = x.Url_Rewrite,
                mLevel = x.mLevel,
                lstMenu = getMenuTree(list, x.mID)
            }).ToList();
        }
        public class cls_Menu
        {
            public int mID { get; set; }
            public string mName { get; set; }
            public string Url_Rewrite { get; set; }
            public string cssTag { get; set; }
            public int isPublish { get; set; }
            public string mIcon { get; set; }
            public int alevel { get; set; }
            public int mLevel { get; set; }
            public int mParentID { get; set; }
            public List<cls_Menu> lstMenu;

        }
        
        public class Menu
        {
            public int menuId { get; set; }
            public int menuParentId { get; set; }
            public string menuName { get; set; }
            public string menuPath { get; set; }
            public string menuLink { get; set; }
            public string menuClass { get; set; }
            public string menuIcon { get; set; }
            public List<clsChildMenu> lstMenu { get; set; }
        }
        public class clsChildMenu
        {
            public int childId { get; set; }
            public string childName { get; set; }
            public string childLink { get; set; }
            public string childClass { get; set; }
            public string childIcon { get; set; }
        }
    }
}
