﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;

namespace _30shine.GUI.SystemService.Webservice
{
    /// <summary>
    /// Summary description for Permission
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    [ScriptService]

    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Permission : System.Web.Services.WebService
    {
        #region get Permission
        [WebMethod]
        public void GetPem(int iDisplayLength, int iDisplayStart, int iSortCol_0,
            string sSortDir_0, string sSearch)
        {
            try
            {
                int displayLength = iDisplayLength;
                int displayStart = iDisplayStart;
                int sortCol = iSortCol_0;
                string sortDir = sSortDir_0;
                string search = sSearch;

                //string cs = ConfigurationManager.ConnectionStrings["Solution_30shineEntities"].ConnectionString;

                int filteredCount = 0;

                List<cls_Permission> clsPerm = new List<cls_Permission>();
                Solution_30shineEntities db = new Solution_30shineEntities();

                var cls = db.Store_Permission(displayLength, displayStart, sortCol, sortDir, string.IsNullOrEmpty(search) ? null : search).ToList();
                foreach (var item in cls)
                {
                    cls_Permission clsPe = new cls_Permission();
                    filteredCount = Convert.ToInt32(item.TotalCount);
                    clsPe.Id = item.pID;
                    clsPe.PermissionName = item.pName;
                    //clsPe.Des = item.pCreatedDate.ToString();
                    clsPe.Publish = Convert.ToInt32(item.pPublish);
                    if (clsPe.Publish == 1)
                    {
                        clsPe.checkBox = "<input type='checkbox' Id='checkPublish' data-id =" + item.pID + " checked ='checked'>";
                    }
                    else
                    {
                        clsPe.checkBox = "<input type='checkbox' Id='checkPublish' data-id =" + item.pID + ">";
                    }
                    //clsPe.Action = "<a class='btn btn-info btn-xs btn-Edit' href='/admin/phan-quyen/chinh-sua-nhom-quyen/" + item.pID + ".html' style='margin-right:10px'><i class='fa fa-pencil-square-o' aria-hidden='true'></i>&nbsp&nbspSửa</a>";
                    clsPe.Action += "<button type='button' class='btn btn-success btn-xs btn-Edit-Default' pID='" + item.pID + "' pName ='" + item.pName + "' style='margin-right:10px'><i class='fa fa-list-alt' aria-hidden='true'></i>&nbsp&nbspSửa DefaultPage</button>";
                    clsPe.Action += "<a class='btn btn-warning btn-xs btn-Edit-Menu' href='/admin/phan-quyen/chinh-sua-phan-quyen-v2/" + item.pID + ".html' style='margin-right:10px'><i class='fa fa-pencil-square' aria-hidden='true'></i>&nbsp&nbspSửa phân quyền v2</a>";
                    clsPe.Action += "<a class='btn btn-warning btn-xs btn-Edit-Menu' href='/admin/phan-quyen/chinh-sua-phan-quyen/" + item.pID + ".html' style='margin-right:10px'><i class='fa fa-pencil-square' aria-hidden='true'></i>&nbsp&nbspSửa phân quyền</a>";

                    clsPerm.Add(clsPe);
                }
                var result = new
                {
                    iTotalRecords = GetPemTotalCount(),
                    iTotalDisplayRecords = filteredCount,
                    aaData = clsPerm
                };
                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Write(js.Serialize(result));
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }

        private int GetPemTotalCount()
        {
            int totalPem = 0;
            Solution_30shineEntities db = new Solution_30shineEntities();
            totalPem = db.Tbl_Permission.Count();
            return totalPem;
        }
        public class cls_Permission
        {
            public int Id { get; set; }
            public string PermissionName { get; set; }
            public string Title { get; set; }
            public string CreatedDate { get; set; }
            public int Publish { get; set; }
            public string checkBox { get; set; }
            public string Action { get; set; }
        }
        #endregion

        #region get Url
        [WebMethod]
        public void GetUrl(int iDisplayLength, int iDisplayStart, int iSortCol_0,
            string sSortDir_0, string sSearch)
        {
            try
            {
                int displayLength = iDisplayLength;
                int displayStart = iDisplayStart;
                int sortCol = iSortCol_0;
                string sortDir = sSortDir_0;
                string search = sSearch;
                int filteredCount = 0;
                List<cls_Url> listUrl = new List<cls_Url>();
                Solution_30shineEntities db = new Solution_30shineEntities();
                var cls = db.Store_Permission_Menu(displayLength, displayStart, sortCol, sortDir, string.IsNullOrEmpty(search) ? null : search).ToList();
                foreach (var item in cls)
                {
                    cls_Url clsUrl = new cls_Url();
                    filteredCount = Convert.ToInt32(item.TotalCount);
                    clsUrl.Id = item.mID;
                    clsUrl.FrontName = item.mName;
                    clsUrl.RewriteUrl = item.Url_Rewrite;
                    //clsUrl.CreatedDate = item.CreatedDate.ToString();
                    clsUrl.Publish = Convert.ToInt32(item.mPublish);
                    clsUrl.ShowMenu = Convert.ToInt32(item.MenuShow);
                    if (clsUrl.Publish == 1)
                    {
                        clsUrl.checkBox = "<input type='checkbox' class='checkPublish' data-id =" + item.mID + " checked ='checked'>";
                    }
                    else
                    {
                        clsUrl.checkBox = "<input type='checkbox' class='checkPublish' data-id =" + item.mID + ">";
                    }
                    if (clsUrl.ShowMenu == 1)
                    {
                        clsUrl.showHideMenu = "<input type='checkbox' class='show-hide-menu' data-id =" + item.mID + " checked ='checked'>";
                    }
                    else
                    {
                        clsUrl.showHideMenu = "<input type='checkbox' class='show-hide-menu' data-id =" + item.mID + ">";
                    }
                    clsUrl.Action = " <a class='btn btn-info btn-xs btn-Edit' href='/admin/phan-quyen/sua-menu/" + item.mID + ".html' style='margin-right:10px'>Sửa</a>";
                    //< a class='btn btn-info btn-xs btn-Edit' id='_click' href='/admin/phan-quyen/them-moi-menu-con/" + item.mID + ".html' style='margin-right:10px' data-id =" + item.mID + ">Thêm menu con</a>
                    listUrl.Add(clsUrl);
                }
                var result = new
                {
                    iTotalRecords = GetPemTotalCount(),
                    iTotalDisplayRecords = filteredCount,
                    aaData = listUrl
                };
                JavaScriptSerializer js = new JavaScriptSerializer();
                Context.Response.Write(js.Serialize(result));
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }

        private int GetUrlTotalCount()
        {
            int totalUrl = 0;
            Solution_30shineEntities db = new Solution_30shineEntities();
            totalUrl = db.Tbl_Permission_Menu.Count();
            return totalUrl;
        }
        public class cls_Url
        {
            public int Id { get; set; }
            public string FrontName { get; set; }
            public string RewriteUrl { get; set; }
            public string MainUrl { get; set; }
            public int ParentId { get; set; }
            public int ShowMenu { get; set; }
            public int Publish { get; set; }
            public string showHideMenu { get; set; }
            public string checkBox { get; set; }
            public string Action { get; set; }
            public List<cls_Url> _listUrl { get; set; }
        }
        #endregion

        #region change default page service
        [WebMethod]

        public object GetLinkForChangeDefaultPage(int pID)
        {
            try
            {
                Solution_30shineEntities db = new Solution_30shineEntities();
                List<cls_Url> lstUrl = new List<cls_Url>();

                var parentUrl = db.Tbl_Permission_Menu.Where(m => m.mParentID == 0).ToList();
                foreach (var item in parentUrl)
                {
                    cls_Url url = new cls_Url();
                    url.Id = item.mID;
                    url.FrontName = item.mName;
                    var childUrl = db.Store_Permission_Get_Menu(item.mID, pID).Where(p => p.isPublish == 1).ToList();
                    url._listUrl = new List<cls_Url>();
                    foreach (var _item in childUrl)
                    {
                        cls_Url _url = new cls_Url();
                        _url.Id = (int)_item.id;
                        _url.FrontName = _item.name;
                        _url.RewriteUrl = _item.link;
                        //_url.Id = _item.id;
                        url._listUrl.Add(_url);
                    }
                    lstUrl.Add(url);
                }
                return new JavaScriptSerializer().Serialize(lstUrl);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }
        [WebMethod]
        public string ChangeDefaultPage(int mID, string mName, string url, int pID, string pName)
        {
            try
            {
                Solution_30shineEntities db = new Solution_30shineEntities();
                var checkExist = db.Tbl_Permission_Map.FirstOrDefault(map => map.mID == mID);
                if (checkExist != null)
                {
                    AddOrUpdateDefaultPage(mID, url, pID);
                }
                else
                {
                    Tbl_Permission_Map map = new Tbl_Permission_Map();
                    map.mID = mID;
                    map.mName = mName;
                    map.pID = pID;
                    map.pName = pName;
                    map.mapPublish = true;
                    db.Tbl_Permission_Map.Add(map);
                    db.SaveChanges();
                    AddOrUpdateDefaultPage(mID, url, pID);
                }
                return "<span>Thay đổi Page mặc định cho nhóm quyền thành công</span>";
            }
            catch (Exception ex)
            {

                return "<span>Có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển</span>";
            }

        }
        private void AddOrUpdateDefaultPage(int a, string b, int c)
        {
            try
            {
                Solution_30shineEntities db = new Solution_30shineEntities();
                var checkDefault = db.Tbl_Permission_DefaultPage.FirstOrDefault(d => d.pID == c);
                if (checkDefault != null)
                {
                    checkDefault.MenuID = a;
                    checkDefault.MenuLink = b;
                    checkDefault.pID = c;
                    db.Tbl_Permission_DefaultPage.AddOrUpdate(checkDefault);
                    db.SaveChanges();
                }
                else
                {
                    Tbl_Permission_DefaultPage defaultPage = new Tbl_Permission_DefaultPage();
                    defaultPage.MenuID = a;
                    defaultPage.MenuLink = b;
                    defaultPage.pID = c;
                    db.Tbl_Permission_DefaultPage.Add(defaultPage);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }
        #endregion
    }
}
