﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace Project.SystemService.SendSMS
{
    public class SendSMSLib
    {
        Solution_30shineEntities db = new Solution_30shineEntities();
        // Constructor
        public SendSMSLib()
        {
            //
        }

        public static object sendSMS(string smsContent, string phones)
        {
            string userName = "30shine";
            string password = "123@";
            string smsType = "CSKH";
            string senderName = "30Shine";
            bool isUnicode = false;
            bool isFlash = false;
            try
            {
                var msg = new Library.Class.cls_message();
                String sResponseFromServer = "Opps! Something went wrong. Please try again.";
                WebRequest tRequest;
                tRequest = WebRequest.Create("http://123.30.59.159/smsapi/Service.asmx/SendSms");
                tRequest.Method = "post";
                tRequest.ContentType = "application/x-www-form-urlencoded";

                var postData = "userName=" + userName + "&password=" + password + "&smsType=" + smsType + "&senderName=" + senderName + "&smsContent=" + smsContent + "&isUnicode=" + isUnicode + "&isFlash=" + isFlash + "&phones=" + phones;

                Console.WriteLine(postData);
                Byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                tRequest.ContentLength = byteArray.Length;

                Stream dataStream = tRequest.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();

                WebResponse tResponse = tRequest.GetResponse();

                dataStream = tResponse.GetResponseStream();

                StreamReader tReader = new StreamReader(dataStream);

                sResponseFromServer = tReader.ReadToEnd();
                msg.status = "success";

                tReader.Close();
                dataStream.Close();
                tResponse.Close();

                msg.message = sResponseFromServer;
                return msg;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}