﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.Helpers;

namespace _30shine.GUI.Security.SCControls
{
    public partial class PermLogin : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckLogin();
        }

        private void CheckLogin()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                //MsgParam.Add(new KeyValuePair<string, string>("msg_login_status", "warning"));
                //MsgParam.Add(new KeyValuePair<string, string>("msg_login_message", "Yêu cầu đăng nhập!"));
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                try
                {
                    //Add refresh header to refresh the page 60 seconds before session timeout
                    Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) - 60));
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
    }
}