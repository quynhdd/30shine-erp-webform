﻿using _30shine.Helpers;
using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.Interface;
using static _30shine.MODEL.CustomClass.AccountModelClass;
using Helpers;
using Google.Authenticator;
using System.Web.Services;
using System.Net.Mail;
using System.Net;

namespace _30shine.GUI.Security
{
    public partial class Login_Test : System.Web.UI.Page
    {

        private const string key = "4790F55F-096A-45A2-A34D-6A89DD7B1BA0";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CheckIsLogin();
            }
        }

        protected void Btn_Login(object sender, EventArgs e)
        {
            IPermissionModel permission = new PermissionModel();
            var Email = username.Value;
            var Password = password.Value;
            string PasswordGen;
            PasswordGen = Helpers.SecurityLib.Encrypt(Password);
            using (var db = new Solution_30shineEntities())
            {
                var Root = "root";
                var RootPass = "59a0ce49efb6214e6d37ea5444c5b8ec";
                //var SupperPass = "dh9Eve8Zqdr8Mgx9sbs3";
                if (Email == Root && RootPass == PasswordGen)
                {
                    Session["User_Id"] = 0;
                    Session["User_Code"] = "ROOT";
                    Session["User_Name"] = "Root";
                    Session["User_Email"] = "no-reply@30shine.com";
                    Session["User_SN_day"] = "";
                    Session["User_SN_month"] = "";
                    Session["User_SN_year"] = "";
                    Session["User_Permission"] = "root";
                    Session["User_Permission_ID"] = "0";
                    Session["SalonId"] = 0;
                    Session["Menu"] = permission.GetMenuV3(0);
                    var accountSession = new AccountSession();
                    accountSession.UserId = 0;
                    accountSession.UserCode = "ROOT";
                    accountSession.UserName = "Root";
                    accountSession.UserPhone = "";
                    accountSession.UserEmail = "no-reply@30shine.com";
                    accountSession.UserBirthDay = "";
                    accountSession.UserSalonId = 0;
                    accountSession.UserSalonName = "";
                    accountSession.UserDepartmentId = 0;
                    accountSession.UserDepartmentName = "";
                    accountSession.UserSkillLevelId = 0;
                    accountSession.UserSkillLevelName = "";
                    accountSession.UserPermission = "root";
                    accountSession.UserPermissionId = "0";
                    accountSession.UserMenu = Session["Menu"].ToString();
                    AccountModel.Instance.SetAccountInfor(accountSession);

                    UIHelpers.Redirect("/");
                }
                else
                {
                    using (MD5 md5Hash = MD5.Create())
                    {
                        PasswordGen = GenPassword(md5Hash, Password);
                    }
                    if (Email != "")
                    {
                        //var User = db.Staffs.FirstOrDefault(w => w.Email == Email && (w.Password == PasswordGen) && w.Active == 1);
                        var User = (from u in db.Users
                                    join s in db.Staffs on u.StaffId equals s.Id
                                    where u.Username == Email && u.Password == PasswordGen && u.IsActive == true
                                    select new { u, s }
                                    ).FirstOrDefault();
                        if (User != null)
                        {
                            try
                            {
                                //authentication
                                Session["UserID"] = User.u.UserId;
                                Session["UrlImage"] = "";//GenerateQRCode(User.u.UserId, User.u.Username);
                                Session["Valid2FA"] = User.u.Valid2FA;
                                //
                                Session["User_Id"] = User.s.Id;
                                Session["User_Code"] = User.s.Code;
                                Session["User_Name"] = User.s.Fullname;
                                Session["User_Email"] = User.s.Email;
                                Session["User_SN_day"] = User.s.SN_day;
                                Session["User_SN_month"] = User.s.SN_month;
                                Session["User_SN_year"] = User.s.SN_year;
                                Session["Menu"] = permission.GetMenuV3(User.s.Id);
                                Session["User_Permission"] = permission.GetPermisionNames(User.s.Id);
                                Session["SalonId"] = User.s.SalonId;
                                var accountSession = new AccountSession();
                                accountSession.UserId = User.s.Id;
                                accountSession.UserCode = User.s.Code;
                                accountSession.UserName = User.s.Fullname;
                                accountSession.UserPhone = User.s.Phone;
                                accountSession.UserEmail = User.s.Email;
                                accountSession.UserBirthDay = "";
                                accountSession.UserSalonId = User.s.SalonId ?? 0;
                                accountSession.UserDepartmentId = User.s.Type ?? 0;
                                accountSession.UserDepartmentName = "";
                                accountSession.UserSkillLevelId = User.s.SkillLevel ?? 0;
                                accountSession.UserSkillLevelName = "";
                                accountSession.UserPermission = User.s.Permission?.Trim() ?? "";
                                accountSession.UserPermissionId = User.s.PermissionID?.Trim() ?? "0";
                                accountSession.UserMenu = Session["Menu"]?.ToString() ?? "";
                                AccountModel.Instance.SetAccountInfor(accountSession);

                                UIHelpers.Redirect("/xac-nhan-otp.html");
                            }
                            catch (Exception ex)
                            {
                                throw new Exception(ex.Message);
                            }
                        }

                        else
                        {

                            var Team = db.TeamServices.FirstOrDefault(w => w.Account == Email && (w.Password == PasswordGen));
                            if (Team != null)
                            {
                                if (Team.IsLogin == null || Team.IsLogin == false)
                                {
                                    //set cookies 30 days
                                    Team.IsLogin = true;
                                    db.TeamServices.AddOrUpdate(Team);
                                    db.SaveChanges();

                                    Response.Cookies["TeamAccount"].Value = Team.Account;
                                    Response.Cookies["TeamAccount"].Expires = DateTime.Now.AddDays(30);

                                    UIHelpers.Redirect("/team");
                                }
                                else
                                {
                                    var MsgParam = new List<KeyValuePair<string, string>>();
                                    MsgParam.Add(new KeyValuePair<string, string>("msg_login_status", "warning"));
                                    MsgParam.Add(new KeyValuePair<string, string>("msg_login_message", "Tài khoản này đang ở trạng thái đăng nhập."));
                                    UIHelpers.Redirect("/dang-nhap.html", MsgParam);
                                }
                            }
                            else
                            {
                                var MsgParam = new List<KeyValuePair<string, string>>();
                                MsgParam.Add(new KeyValuePair<string, string>("msg_login_status", "warning"));
                                MsgParam.Add(new KeyValuePair<string, string>("msg_login_message", "Email hoặc mật khẩu không đúng."));
                                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
                            }
                        }
                    }
                    else
                    {
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_login_status", "warning"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_login_message", "Email hoặc mật khẩu không đúng."));
                        UIHelpers.Redirect("/dang-nhap.html", MsgParam);
                    }
                }

                Session.Timeout = (24 * 60);
            }
            permission.DisposeConnectString();
        }

        public string GenPassword(MD5 md5Hash, string input)
        {
            var scrPush = "uaefsfkoiu&@(^%=";
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input + scrPush));
            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }

        /// <summary>
        /// Verify password
        /// </summary>
        /// <param name="md5Hash"></param>
        /// <param name="input"></param>
        /// <param name="hash"></param>
        /// <returns></returns>
        //public bool VerifyPassword(MD5 md5Hash, string input, string hash)
        //{
        //    string hashOfInput = GenPassword(md5Hash, input);
        //    StringComparer comparer = StringComparer.OrdinalIgnoreCase;

        //    if (0 == comparer.Compare(hashOfInput, hash))
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        /// <summary>
        /// Check is login
        /// </summary>
        private void CheckIsLogin()
        {
            if (Session["User_Id"] != null)
            {
                Response.Redirect("/");
            }
        }

        /// <summary>
        /// Gen pass
        /// </summary>
        /// <param name="pass"></param>
        /// <returns></returns>
        //private string GenPass(string pass)
        //{
        //    using (MD5 md5Hash = MD5.Create())
        //    {
        //        var PasswordGen = GenPassword(md5Hash, pass);
        //        return PasswordGen;
        //    }
        //}



        //private string GenerateQRCode(string userId,string username)
        //{
        //    var info = username + userId;
        //    //2fa setup
        //    TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
        //    string UserUniqueKey = info+key;
        //    Session["UserUniqueKey"] = UserUniqueKey;
        //    var setupInfo = tfa.GenerateSetupCode("2FA SetUp", info, UserUniqueKey, 300, 300);
        //    string urlImg = setupInfo.QrCodeSetupImageUrl;
        //    //string keyValid = setupInfo.ManualEntryKey;
        //    //var result = new { urlImg, keyValid };
        //    return urlImg;
        //}

        [WebMethod]
        public static string SendEmail(string username, string email)
        {
            Solution_30shineEntities db = new Solution_30shineEntities();

            var user = (from a in db.Users
                        join b in db.Staffs on a.StaffId equals b.Id
                        where a.Username == username && a.Email == email
                        select new
                        {
                            a,b
                        }
                        ).FirstOrDefault();

            if (user == null)
            {
                return "Tên tài khoản hoặc email liên kết không chính xác";
            }
            var key = "57438957&*&$#ahwAHFE";
            var token = DateTime.Now.ToString() + key + user.a.UserId;
            var emailBody = @"<html>
                                <head>
                                </head>
                                <body>
                                <p> Xin chào<b> "+user.b.Fullname+@"</b></p>
                                   <p style = 'line-height: 24px;'>
                                    &nbsp Chúng tôi vừa nhận được yêu cầu thay đổi mật khẩu từ tài khoản liên kết với email này.<br/>
                                    Nếu là bạn vui lòng click vào <a href='"+ HttpContext.Current.Request.Url.Host + "/doi-mat-khau/"+ sha256(token) + @".html'>đây</a>.<br/>
                                    Nếu không phải bạn vui lòng bỏ qua email này.<br/>
                                    Đây là email tự động từ hệ thống vui lòng không trả lời thư này.<br/>
                                    Trân trọng!<br/>
                                    30shine IT Team
                                  </p>
                                  </body>
                              </html>";
            using (MailMessage mm = new MailMessage("no-reply@30shine.com", email))
            {

                mm.Subject = "Email yêu cầu thay đổi mật khẩu";
                mm.Body = emailBody;

                mm.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential("phupv@30shine.com", "162896451@tt");
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(mm);
                user.a.OtpChangePass = sha256(token);
                db.Users.AddOrUpdate(user.a);
                db.SaveChanges();
                return "Send email success";
            }
        }
        static string sha256(string randomString)
        {
            var crypt = new SHA256Managed();
            string hash = String.Empty;
            byte[] crypto = crypt.ComputeHash(Encoding.ASCII.GetBytes(randomString));
            foreach (byte theByte in crypto)
            {
                hash += theByte.ToString("x2");
            }
            return hash;
        }
    }
}
