﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="LoginAuthentication.aspx.cs" Inherits="_30shine.GUI.Security.Login.LoginAuthentication" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>30Shine ERP</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <link href="/Assets/css/font.css" rel="stylesheet" />
    <link href="/Assets/css/Login_V2.css?v=2" rel="stylesheet" />
    <link href="/Assets/css/bootstrap/bootstrap.css" rel="stylesheet" />
    <link href="/Assets/css/bootstrap/bootstrap-theme.css" rel="stylesheet" />
    <link href="/Assets/izitoast/css/iziToast.min.css" rel="stylesheet" />
    <script src="/Assets/izitoast/js/iziToast.min.js"></script>
    <script src="/Assets/js/Common/Common.js"></script>
    <style>
        body {
            background-color: #31343d;
            font-family: 'Roboto Regular';
        }

        .radio label, .checkbox label, .form-control {
            font-family: 'Roboto Regular';
            color: #636770;
        }

        @media(max-width:768px) {
            body {
                background-color: #3b3d46;
            }

            .fix3 {
                font-size: 20px !important;
                color: #d2d2d0 !important;
                margin: 0 !important;
            }
        }

        .checkbox input[type="checkbox"] {
            margin-left: 0px;
            margin-right: 8px;
        }

        .fix3 {
            font-size: 23px !important;
            color: #d2d2d0 !important;
            margin: 0 !important;
        }

        .form-horizontal .form-group {
            margin-right: 0px;
            margin-left: 0px;
        }

        .fixlb {
            margin-bottom: 10px !important;
        }

        .btn-default {
            border-color: #636770;
        }

        .form-forget {
            margin-bottom: 10px;
        }

        .modal-content {
            background-color: #414345;
        }

        .modal-title {
            color: #e4e5e6;
        }

        .modal-header {
            border-bottom: 1px solid #333;
        }

        .modal-footer {
            border-top: 1px solid #333;
        }

        .modal-body label {
            color: #e4e5e6;
        }
    </style>
</head>
<body>
    <form runat="server" id="login" enctype="multipart/form-data">
        <div>
            <div class="all">
                <div class="fix1 container">
                    <div class="logo-wrap">
                        <img class="img-logo" src="/Assets/images/ic_logo_white.png" />
                    </div>
                    <div class="bg">
                        <div class="form-horizontal">
                            <div class="form-group fix2">
                                <div class=" viet fixcol col-sm-10">
                                    <input type="text" id="username" placeholder="Tên đăng nhập ..." spellcheck="false" class="fixform form-control" runat="server" autocomplete="off" />
                                </div>
                            </div>
                            <div class="form-group fix2">
                                <div class="viet fixcol col-sm-10">
                                    <input type="password" class="fixform1 form-control" id="password" placeholder="Mật khẩu ..." runat="server" autocomplete="off" />
                                </div>
                            </div>

                            <div class="lb fix2">
                                <div class="fixlb form-group fix2">
                                    <div class="fixcol col-sm-offset-2 col-sm-10">
                                        <div class="fixcheckbox checkbox">
                                            <label>
                                                <a href="#" style="color: #b9b9b9;" onclick="ShowModal()">Quên mật khẩu?</a>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group fix2">
                                <div class="fixcol viet col-sm-offset-2 col-sm-10">
                                    <asp:Button ID="submit" CssClass="fixbtn fix4 btn btn-default fix3" runat="server" Text="ĐĂNG NHẬP" OnClick="Btn_Login" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ndung">
                        <p class="nd" style="font-size: 14px;">30Shine tin tưởng, nỗ lực mỗi ngày để kết nối bàn tay tài hoa của người Việt cùng quy trình khoa học 30 phút nhằm đem đến cho phái mạnh toàn cầu kiểu tóc đẹp trai, làn da khỏe mạnh cuốn hút phái đẹp; tinh thần thư giãn để bứt phá trong công việc</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Quên mật khẩu?</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-forget">
                            <label for="username">Tài khoản của bạn:</label>
                            <input type="text" runat="server" id="UserNameSend" name="username" class="form-control username fixform" placeholder="example" autocomplete="off" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="BtnComplete" CssClass="fixbtn fix4 btn btn-default fix3" runat="server" OnClick="BtnForgotPassword" Text="Xác nhận" />
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script src="/Assets/js/jquery.v1.11.1.js"></script>
    <script src="/Assets/js/bootstrap/bootstrap.min.js"></script>
    <script type="text/javascript">
        function ShowModal() {
            $("#myModal").modal();
        }
        jQuery(document).ready(function ($) {
            // auto focus on input[name="username"]
            $("input[name='username']").focus();
        });

        $('#UserNameSend').keypress(function (e) {
            var key = e.which;
            if (key == 13)  // the enter key code
            {
                $("#BtnComplete").click();
                return false;
            }
        });
    </script>
</body>
</html>
