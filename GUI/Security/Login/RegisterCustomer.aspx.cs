﻿using _30shine.Helpers;
using _30shine.MODEL.ENTITY.EDMX;
using Project.Model.Structure;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using static Library.Class;

namespace _30shine.GUI.Security.Login
{
    public partial class RegisterCustomer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["code"] != "")
                {
                    GetFacebookUserData(Request.QueryString["code"]);
                }
            }

        }

        protected void GetFacebookUserData(string code)
        {
            try
            {
                // Exchange the code for an access token
                Uri targetUri = new Uri("https://graph.facebook.com/oauth/access_token?client_id=" + ConfigurationManager.AppSettings["FacebookAppId"] + "&client_secret=" + ConfigurationManager.AppSettings["FacebookAppSecret"] + "&redirect_uri=http://" + Request.ServerVariables["SERVER_NAME"] /*+ ":" + Request.ServerVariables["SERVER_PORT"]*/ + "/khach-hang/dang-ki&code=" + code);
                HttpWebRequest at = (HttpWebRequest)HttpWebRequest.Create(targetUri);

                System.IO.StreamReader str = new System.IO.StreamReader(at.GetResponse().GetResponseStream());
                string token = str.ReadToEnd().ToString().Replace("access_token", "").Replace("{", "").Replace("}", "").Replace("\"", "");

                // Split the access token and expiration from the single string
                string[] combined = token.Split(',');
                string accessToken = combined[0].Replace(":", "");

                // Exchange the code for an extended access token
                Uri eatTargetUri = new Uri("https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=" + ConfigurationManager.AppSettings["FacebookAppId"] + "&client_secret=" + ConfigurationManager.AppSettings["FacebookAppSecret"] + "&fb_exchange_token=" + accessToken);
                HttpWebRequest eat = (HttpWebRequest)HttpWebRequest.Create(eatTargetUri);

                StreamReader eatStr = new StreamReader(eat.GetResponse().GetResponseStream());
                string eatToken = eatStr.ReadToEnd().ToString().Replace("access_token", "").Replace("{", "").Replace("}", "").Replace("\"", "");

                // Split the access token and expiration from the single string
                string[] eatWords = eatToken.Split(',');
                string extendedAccessToken = eatWords[0].Replace(":", "");

                // Request the Facebook user information
                Uri targetUserUri = new Uri("https://graph.facebook.com/me?fields=first_name,last_name,gender,locale,link,email,birthday&access_token=" + extendedAccessToken);
                HttpWebRequest user = (HttpWebRequest)HttpWebRequest.Create(targetUserUri);

                // Read the returned JSON object response
                StreamReader userInfo = new StreamReader(user.GetResponse().GetResponseStream());
                string jsonResponse = string.Empty;
                jsonResponse = userInfo.ReadToEnd();

                // Deserialize and convert the JSON object to the Facebook.User object type
                JavaScriptSerializer sr = new JavaScriptSerializer();
                string jsondata = jsonResponse;
                Facebook.User converted = sr.Deserialize<Facebook.User>(jsondata);

                //// Write the user data to a List
                //List<Facebook.User> currentUser = new List<Facebook.User>();
                //currentUser.Add(converted);
                  
                Btn_Login(converted.id, converted);
            }
            catch { }
            // Return the current Facebook user
            //return currentUser;
        }

        protected void Btn_Login(string FacebookId, Facebook.User converted)
        {
            using (var db = new Solution_30shineEntities())
            {
                if (FacebookId != "")
                {
                    var User = db.Customers.FirstOrDefault(w => w.FacebookId == FacebookId);

                    if (User != null)
                    {
                        Session["User_Id"] = User.Id;
                        Session["User_Code"] = User.Customer_Code;
                        Session["User_Name"] = User.Fullname;
                        Session["User_Email"] = User.Email;
                        Session["User_SN_day"] = User.SN_day;
                        Session["User_SN_month"] = User.SN_month;
                        Session["User_SN_year"] = User.SN_year;
                        Session["User_Permission"] = "root";
                        Session["SalonId"] = User.SalonId;

                        UIHelpers.Redirect("/");
                    }
                    else
                    {
                        txtFullName.Value = (converted.first_name + " " + converted.last_name);
                        txtEmail.Value = converted.email;
                        HDF_FacebokId.Value = converted.id;
                    }
                }
            }
            Session.Timeout = (24 * 60);
        }

        [WebMethod]
        public static object ResgisterCus(string FacebookId, string fullname, string phone, string email, string pass)
        {
            using (var db = new Solution_30shineEntities())
            {
                var msg = new cls_message();
                try
                {
                    var checkPhone = db.Customers.Where(p => p.Phone.Trim() == phone.Trim()).ToList();
                    if (checkPhone.Count() > 0)
                    {
                        var customer = db.Customers.Single(p => p.Phone == phone);
                        customer.Fullname = fullname;
                        customer.Email = email;
                        customer.Password = MD5Encrypt(pass);
                        customer.FacebookId = FacebookId;
                        db.Customers.AddOrUpdate(customer);
                        db.SaveChanges();
                    }
                    else
                    {
                        var cus = new Customer();
                        cus.Fullname = fullname;
                        cus.Phone = phone;
                        cus.Email = email;
                        cus.Password = MD5Encrypt(pass);
                        cus.FacebookId = FacebookId;
                        db.Customers.AddOrUpdate(cus);
                        db.SaveChanges();
                    }
                    msg.success = true;
                    msg.message = "Dữ liệu đã được lưu!";
                }
                catch (Exception ex)
                {
                    msg.success = false;
                    msg.message = ex.Message;
                }

                return msg;
            }
        }

        public static string MD5Encrypt(string input)
        {
            string result = "";
            MD5 md = MD5.Create();
            byte[] hash;
            ASCIIEncoding enc = new ASCIIEncoding();
            byte[] buffer = enc.GetBytes(input);
            hash = md.ComputeHash(buffer);
            foreach (byte b in hash)
            {
                result += b.ToString("x2");
            }
            return result;
        }
    }
}