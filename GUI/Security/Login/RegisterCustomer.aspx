﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegisterCustomer.aspx.cs" Inherits="_30shine.GUI.Security.Login.RegisterCustomer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/Assets/css/bootstrap/bootstrap.css" rel="stylesheet" />
    <link href="/Assets/css/bootstrap/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    <div class="container" style="margin-top: 50px;">
        <div class="row">
            <form class="form-horizontal">
                <fieldset>

                    <!-- Form Name -->
                    <legend>Thông tin đăng ký</legend>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Họ và tên</label>
                        <div class="col-md-4">
                            <input id="txtFullName" runat="server" clientidmode="static" placeholder="Nhập họ và tên" class="form-control input-md" required="" type="text" />
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Số điện thoại</label>
                        <div class="col-md-4">
                            <input id="txtPhone" runat="server" clientidmode="static" placeholder="Ví dụ: 091743498" class="form-control input-md" required="" type="text" />
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Email</label>
                        <div class="col-md-4">
                            <input id="txtEmail" runat="server" clientidmode="static" placeholder="Ví dụ: abc@gmai.com" class="form-control input-md" required="" type="text" />
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Mật khẩu</label>
                        <div class="col-md-4">
                            <input id="txtPassword" name="textinput" placeholder="Nhập mật khẩu" class="form-control input-md" required="" type="password" />
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Nhập lại mật khẩu</label>
                        <div class="col-md-4">
                            <input id="txtConfigPass" name="textinput" placeholder="Nhập lại mật khẩu" class="form-control input-md" required="" type="password" />
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <!-- Button -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="singlebutton"></label>
                        <div class="col-md-4">
                            <button id="btnSubmit" name="singlebutton" class="btn btn-primary" onclick="saveCustomer()">Lưu</button>
                        </div>
                    </div>

                </fieldset>
            </form>
            <input id="HDF_FacebokId" runat="server" clientidmode="static" type="hidden" />
        </div>
    </div>
    <script src="/Assets/js/jquery.v1.11.1.js"></script>
    <script src="/Assets/js/common.js"></script>
    <script type="text/javascript">
        function saveCustomer() {
            var FacebookId = $("#HDF_FacebokId").val();
            var fullname = $("#txtFullName").val();
            var phone = $("#txtPhone").val();
            var email = $("#txtEmail").val();
            var pass = $("#txtPassword").val();
            var passRe = $("#txtConfigPass").val();
            if (pass != passRe) {
                alert("Mật khẩu và nhập lại mật khẩu không khớp");
                return;
            }
                        
            $.ajax({
                type: "POST",
                url: "/GUI/Security/Login/RegisterCustomer.aspx/ResgisterCus",
                data: '{FacebookId:"' + FacebookId + '", fullname:"' + fullname + '", phone:"' + phone
                    + '", email:"' + email + '", pass:"' + pass + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    console.log(response.d.data);
                    alert(response.d.message);
                },
                failure: function (response) { alert(response.d); }
            });
        }
    </script>
</body>
</html>
