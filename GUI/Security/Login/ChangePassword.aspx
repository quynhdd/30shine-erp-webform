﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChangePassword.aspx.cs" Inherits="_30shine.GUI.Security.Login.ChangePassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/Assets/css/font.css" rel="stylesheet" />
    <link href="/Assets/css/Login_V2.css?v=2" rel="stylesheet" />
    <link href="/Assets/css/bootstrap/bootstrap.css" rel="stylesheet" />
    <link href="/Assets/css/bootstrap/bootstrap-theme.css" rel="stylesheet" />
    <script src="/Assets/js/jquery.v1.11.1.js"></script>
    <link href="/Assets/izitoast/css/iziToast.min.css" rel="stylesheet" />
    <script src="/Assets/izitoast/js/iziToast.min.js"></script>
    <script src="/Assets/js/Common/Common.js"></script>
    <style>
        body {
            background: #31343d;
            font-family: 'Roboto Regular';
        }

        label {
            color: #fff;
            font-size: 14px;
            float: left;
            margin-left: 15px;
            margin-bottom: -5px;
            font-weight: 100;
        }

        #formChangePassword {
            margin: 0 auto;
            width: 300px;
            height: 450px;
            margin-top: 100px;
        }

        #QRCode {
            width: 300px;
            height: 300px;
            margin: 0 auto;
            margin-bottom: 20px;
        }

        .form-change-pass {
            width: 100%;
            line-height: 40px;
            border: 1px solid #ccc !important;
            border-radius: 5px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            margin-top: 10px;
            margin-bottom: 20px;
            height: 40px;
            font-size: 17px;
        }

        #BtnSubmit {
            width: 100%;
            border: 2px solid #ccc !important;
            padding: 10px 20px;
            float: right;
            border-radius: 5px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            background: #31343d;
            color: #fff;
            font-weight: 400;
            text-shadow: 0 1px 0 #ccc;
            font-size: 18px;
        }

        .fix1 {
            margin-top: 0;
        }

        .ndung {
            margin: 20px 50px 0 50px;
            padding-bottom: 60px;
        }

        .nd {
            margin-bottom: 0;
        }

        .form-group {
            margin-bottom: 0;
        }

        #Password {
            padding-right: 35px;
        }

        #ComeBackLogin:hover {
            cursor: pointer;
        }
    </style>
</head>
<body>
    <form id="formChangePassword" runat="server">
        <div>
            <div class="all">
                <div class="fix1 container">
                    <div class="logo-wrap">
                        <img class="img-logo" src="/Assets/images/ic_logo_white.png" />
                    </div>
                    <div class="bg">
                        <div runat="server" id="PasswordOld" class="form-group fix2">
                            <label for="ValidCode">Mật khẩu cũ:</label>
                            <div class=" viet fixcol col-sm-10">
                                <asp:TextBox runat="server" ClientIDMode="Static" CssClass="form-control form-change-pass fixform1" TextMode="Password" ID="OldPassword" placeholder="Nhập mật khẩu cũ của bạn" autocomplete="off"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group fix2">
                            <label for="ValidCode">Mật khẩu mới:</label>
                            <div class=" viet fixcol col-sm-10">
                                <asp:TextBox runat="server" ClientIDMode="Static" CssClass="form-control form-change-pass fixform1" TextMode="Password" ID="Password" placeholder="Mật khẩu tổi thiếu 8 ký tự, chữ thường, in hoa, số, đặc biệt" autocomplete="off"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group fix2">
                            <label for="ValidCode">Xác nhận mật khẩu:</label>
                            <div class="viet fixcol col-sm-10">
                                <asp:TextBox runat="server" ClientIDMode="Static" CssClass="form-control form-change-pass fixform1" TextMode="Password" ID="ConfirmPassword" placeholder="Xác nhận mật khẩu mới" autocomplete="off"></asp:TextBox>
                            </div>
                        </div>
                        <p id="ComeBackLogin" runat="server" style="float: right; color: whitesmoke; text-decoration: underline; margin-top: 10px; margin-right: 15px;">Trở lại trang đăng nhập?</p>
                        <asp:Button ID="Comeback" OnClick="ComeBackLoginPage" CssClass="hidden" runat="server" Style="float: right; color: whitesmoke; text-decoration: underline; margin-top: 10px;" Text="Trở lại trang đầu?" />
                        <div class="form-group fix2" style="margin-top: 10px;">
                            <div class="fixcol viet col-sm-offset-2 col-sm-10">
                                <asp:Button ID="BtnSubmit" CssClass="fixbtn fix4 btn btn-default fix3" runat="server" Text="ĐỒNG Ý" OnClick="BtnSubmit_Click" />
                            </div>
                        </div>
                    </div>
                    <div class="ndung">
                        <p class="nd" style="font-size: 14px;">30Shine tin tưởng, nỗ lực mỗi ngày để kết nối bàn tay tài hoa của người Việt cùng quy trình khoa học 30 phút nhằm đem đến cho phái mạnh toàn cầu kiểu tóc đẹp trai, làn da khỏe mạnh cuốn hút phái đẹp; tinh thần thư giãn để bứt phá trong công việc</p>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script type="text/javascript">
        $("#ComeBackLogin").click(function () {
            $("#Comeback").click();
        });

        $('#OldPassword').keypress(function (e) {
            var key = e.which;
            if (key == 13)  // the enter key code
            {
                $("#BtnSubmit").click();
                return false;
            }
        });

        $('#Password').keypress(function (e) {
            var key = e.which;
            if (key == 13)  // the enter key code
            {
                $("#BtnSubmit").click();
                return false;
            }
        });

        $('#ConfirmPassword').keypress(function (e) {
            var key = e.which;
            if (key == 13)  // the enter key code
            {
                $("#BtnSubmit").click();
                return false;
            }
        });
    </script>
</body>
</html>
