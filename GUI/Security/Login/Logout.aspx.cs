﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;

namespace _30shine.GUI.Security
{
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            SetLogOut();
            ClearAllCookies();
            UnsetSessionUser();
        }

        private void UnsetSessionUser()
        {
            HttpContext.Current.Session.Abandon();
            string redirect = Request.QueryString["redirect"];
            if(redirect == null)
            {
                UIHelpers.Redirect("/dang-nhap.html");
            } else UIHelpers.Redirect("/dang-nhap.html" + "?redirect=" + redirect);

        }

        private void ClearAllCookies()
        {
            string[] myCookies = Request.Cookies.AllKeys;
            foreach (string cookie in myCookies)
            {
                Response.Cookies[cookie].Expires = DateTime.Now.AddDays(-1);
            }
        }

        private void SetLogOut()
        {
            using (var db = new Solution_30shineEntities())
            {
                if (Request.Cookies["TeamAccount"] != null)
                {
                    var TeamAccount = Request.Cookies["TeamAccount"].Value;
                    var Team = db.TeamServices.FirstOrDefault(w => w.Account == TeamAccount);
                    if (Team != null)
                    {
                        Team.IsLogin = false;
                        db.TeamServices.AddOrUpdate(Team);
                        db.SaveChanges();
                    }
                }
            }
        }
    }
}