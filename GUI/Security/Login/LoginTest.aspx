﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMasterVersion1.Master" AutoEventWireup="true" CodeBehind="LoginTest.aspx.cs" Inherits="_30shine.GUI.Security.Login.LoginTest" %>

<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <link href="/Assets/css/font.css" rel="stylesheet" />
        <link href="/Assets/css/Login_V2.css?v=2" rel="stylesheet" />
        <link href="/Assets/css/bootstrap/bootstrap.css" rel="stylesheet" />
        <link href="/Assets/css/bootstrap/bootstrap-theme.css" rel="stylesheet" />
        <style>
            body {
                background-color: #31343d;
                font-family: 'Roboto Regular';
            }

            .radio label, .checkbox label, .form-control {
                font-family: 'Roboto Regular';
                color: #636770;
            }

            @media(max-width:768px) {
                body {
                    background-color: #3b3d46;
                }

                .fix3 {
                    font-size: 20px !important;
                    color: #d2d2d0 !important;
                    margin: 0 !important;
                }
            }

            .checkbox input[type="checkbox"] {
                margin-left: 0px;
                margin-right: 8px;
            }

            .fix3 {
                font-size: 23px !important;
                color: #d2d2d0 !important;
                margin: 0 !important;
            }

            .form-horizontal .form-group {
                margin-right: 0px;
                margin-left: 0px;
            }

            .fixlb {
                margin-bottom: 10px !important;
            }

            .btn-default {
                border-color: #636770;
            }

            .form-forget {
                margin-bottom: 10px;
            }
        </style>
        <form runat="server" id="login" enctype="multipart/form-data">
            <div>
                <div class="all">
                    <div class="fix1 container">
                        <div class="logo-wrap">
                            <img class="img-logo" src="/Assets/images/ic_logo_white.png" />
                        </div>
                        <div class="bg">
                            <div class="form-horizontal">
                                <div class="form-group fix2">
                                    <div class=" viet fixcol col-sm-10">
                                        <input type="text" id="username" placeholder="Tên đăng nhập ..." spellcheck="false" class="fixform form-control" runat="server" autocomplete="off" />
                                    </div>
                                </div>
                                <div class="form-group fix2">
                                    <div class="viet fixcol col-sm-10">
                                        <input type="password" class="fixform1 form-control" id="password" placeholder="Mật khẩu ..." runat="server" autocomplete="off" />
                                    </div>
                                </div>

                                <div class="lb fix2">
                                    <div class="fixlb form-group fix2">
                                        <div class="fixcol col-sm-offset-2 col-sm-10">
                                            <div class="fixcheckbox checkbox">
                                                <label>
                                                    <a href="#" style="color: #b9b9b9;" onclick="ShowModal()">Quên mật khẩu?</a></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group fix2">
                                    <div class="fixcol viet col-sm-offset-2 col-sm-10">
                                        <asp:Button ID="submit" CssClass="fixbtn fix4 btn btn-default fix3" runat="server" Text="ĐĂNG NHẬP" OnClick="Btn_Login" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="ndung">
                            <p class="nd" style="font-size: 14px;">30Shine tin tưởng, nỗ lực mỗi ngày để kết nối bàn tay tài hoa của người Việt cùng quy trình khoa học 30 phút nhằm đem đến cho phái mạnh toàn cầu kiểu tóc đẹp trai, làn da khỏe mạnh cuốn hút phái đẹp; tinh thần thư giãn để bứt phá trong công việc</p>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">XÁC THỰC EMAIL TÀI KHOẢN</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-forget">
                            <label for="username">Tài khoản của bạn:</label>
                            <input type="text" name="username" class="form-control username" placeholder="example" />

                        </div>
                        <div class="form-forget">
                            <label for="email">Email của bạn:</label>
                            <input type="text" name="email" class="form-control email" placeholder="example@30shine.com" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" onclick="SendEmail()">Xác nhận</button>
                    </div>
                </div>

            </div>
        </div>

        <script src="/Assets/js/jquery.v1.11.1.js"></script>
        <script src="/Assets/js/bootstrap/bootstrap.min.js"></script>
        <script src="/Assets/js/common.js"></script>
        <script type="text/javascript">
            function ShowModal() {
                $("#myModal").modal();
            }
            jQuery(document).ready(function ($) {
                // auto focus on input[name="username"]
                $("input[name='username']").focus();

                // add class input active
                $(".input-field input").bind({
                    focus: function () {
                        $(this).addClass("active");
                    },
                    blur: function () {
                        $(this).removeClass("active");
                    }
                });

                // add class checkbox active
                var _isActive;
                $("#checkBoxRemember").bind({
                    click: function () {
                        _isActive = $(this).hasClass("active") ? true : false;
                        if (_isActive) {
                            $(this).removeClass("active");
                        } else {
                            $(this).addClass("active");
                        }
                    },
                    focus: function () {
                        $(this).bind("keypress", function () {
                            _isActive = $(this).hasClass("active") ? true : false;
                            if (_isActive) {
                                $(this).removeClass("active");
                            } else {
                                $(this).addClass("active");
                            }
                        });
                    }
                });

                //============================
                // Show Message
                //============================ 
                var qs = getQueryStrings();
                showMsgSystem(qs["msg_login_message"], qs["msg_login_status"]);

            });
            function SendEmail() {
                var username = $(".username").val();
                var email = $(".email").val();
                $.ajax({
                    url: "/GUI/Security/Login/Login_Test.aspx/SendEmail",
                    data: "{username:'" + username + "',email:'" + email + "'}",
                    dataType: "json",
                    contentType: "application/json;charset:utf-8",
                    type: "POST",
                    success: function (response) {
                        var jsData = JSON.parse(response.d);
                        console.log(jsData);
                    }

                })
            }
        </script>
    </asp:Panel>
</asp:Content>
