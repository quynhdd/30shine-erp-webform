﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using Amazon.CognitoIdentityProvider;
using Amazon.CognitoIdentityProvider.Model;
using Amazon.Extensions.CognitoAuthentication;
using Google.Authenticator;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Configuration;
using static _30shine.MODEL.CustomClass.AccountModelClass;

namespace _30shine.GUI.Security.Login
{
    public partial class LoginAuthentication : System.Web.UI.Page
    {
        private IPermissionModel permission = new PermissionModel();
        private IAuthenticationModel model = new AuthenticationModel();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CheckIsLogin();
                if (Session["ChangePassword"] != null && Convert.ToBoolean(Session["ChangePassword"]))
                {
                    Session["ChangePassword"] = false;

                    Library.Function.TriggerJsMsgSystem(this, "Đổi mật khẩu thành công, vui lòng đăng nhập lại để tiếp tục!", 2, 5000);
                }
                if (Session["ForgotPassword"] != null && Convert.ToBoolean(Session["ForgotPassword"]))
                {
                    Session["ForgotPassword"] = false;

                    Library.Function.TriggerJsMsgSystem(this, $"Yêu cầu thay đổi mật khẩu đã được gửi tới số điện thoại khôi phục của bạn. Vui lòng quay về trang đăng nhập để thực hiện tiếp tục!", 2, 10000);
                }
            }
        }

        protected void Btn_Login(object sender, EventArgs e)
        {
            var UserName = username.Value;
            var Password = password.Value;
            if (String.IsNullOrEmpty(UserName) || String.IsNullOrEmpty(Password))
            {
                Library.Function.TriggerJsMsgSystem(this, "Vui lòng nhập đầy đủ thông tin tài khoản và mật khẩu!", 3);
                return;
            }
            else
            {
                string PasswordGen = Helpers.SecurityLib.Encrypt(Password);
                using (var db = new Solution_30shineEntities())
                {
                    if ("root".Equals(UserName) && "59a0ce49efb6214e6d37ea5444c5b8ec".Equals(PasswordGen))
                    {
                        SetSessionRoot();
                    }
                    else
                    {

                        SetSessionUser(UserName, Password);
                    }
                    permission.DisposeConnectString();
                    Session.Timeout = 24 * 60;

                    if (Request.Cookies["AccessToken"] != null)
                    {
                        var expireTime = DateTime.Now.AddMinutes(24 * 60);
                        Request.Cookies["AccessToken"].Expires = expireTime;
                        Request.Cookies["RefreshToken"].Expires = expireTime;
                        Request.Cookies["IdToken"].Expires = expireTime;
                        Request.Cookies["UserID"].Expires = expireTime;
                    }
                }
            }
        }

        /// <summary>
        /// set session for ROOT
        /// </summary>
        public void SetSessionRoot()
        {
            try
            {
                //authentication
                Session["UserID"] = "0";
                Session["UrlImage"] = "";
                Session["Valid2FA"] = true;
                Session["IsValid2FA"] = true;
                //end

                Session["User_Account"] = "root";
                Session["User_Id"] = 0;
                Session["User_Code"] = "ROOT";
                Session["User_Name"] = "Root";
                Session["User_Email"] = "no-reply@30shine.com";
                Session["User_SN_day"] = "";
                Session["User_SN_month"] = "";
                Session["User_SN_year"] = "";
                Session["User_Permission"] = "root";
                Session["User_Permission_ID"] = "0";
                Session["SalonId"] = 0;
                Session["Menu"] = permission.GetMenuV3(0);
                SetAccountSession(Session["User_Code"].ToString());
                RedirectToOriginPage();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void RedirectToOriginPage()
        {
            string redirect = Request.QueryString["redirect"];
            if (redirect == null) redirect = "/";
            UIHelpers.Redirect(redirect);
        }

        /// <summary>
        /// set session for user
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="Password"></param>
        public void SetSessionUser(string UserName, string Password)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    MD5 md5 = MD5.Create();

                    //CheckExistAccount(UserName, Password);

                    try
                    {
                        var staff = db.Staffs.FirstOrDefault(w => w.Email == UserName.Trim() && w.IsDelete == 0 && w.Active == 1);
                        if (staff != null)
                        {
                            var tmp = Library.Function.GenMD5(MD5.Create(), Password);
                            if (Library.Function.GenMD5(MD5.Create(), Password) == staff.Password)
                            {
                                if (Password.Length < 6)
                                {
                                    model.SignUp(UserName, Libraries.AppConstants.PASSWORD_DEFAULT);

                                    model.AdminConfirmSignUp(UserName);

                                    Library.Function.TriggerJsMsgSystem(this, "abc@123 là mật khẩu mới của bạn</br>Do đăng nhập lần đầu và mật khẩu của bạn không đủ điều kiện đăng nhập!!!", 3, 10000);
                                    return;
                                }
                                else
                                {
                                    model.SignUp(UserName, Password);

                                    model.AdminConfirmSignUp(UserName);
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    { }

                    var authResponse = model.GetAccessTokenUserForLogin(UserName, Password);

                    if (authResponse.AuthenticationResult != null && !String.IsNullOrEmpty(authResponse.AuthenticationResult.AccessToken))
                    {
                        var user = model.GetUser(UserName);

                        if (user != null)
                        {
                            //authentication
                            Session["UserID"] = user.Item1.UserId;
                            Session["UniqueKey"] = Library.Function.GenMD5(md5, user.Item1.UserId + Libraries.AppConstants.KEY);
                            Session["UrlImage"] = Library.Function.GenerateQRCode(user.Item1.Username, Session["UniqueKey"].ToString());
                            Session["Valid2FA"] = user.Item1.Valid2FA;
                            Session["RequiredValid2FA"] = user.Item1.RequiredValid2FA;
                            //end
                            Session["User_Account"] = user.Item1.Username;
                            Session["User_Id"] = user.Item1.StaffId;
                            Session["User_Code"] = user.Item2.OrderCode;
                            Session["User_Name"] = user.Item2.Fullname;
                            Session["User_Email"] = user.Item2.Email;
                            Session["User_Phone"] = user.Item1.Phone;
                            Session["User_SN_day"] = user.Item2.SN_day;
                            Session["User_SN_month"] = user.Item2.SN_month;
                            Session["User_SN_year"] = user.Item2.SN_year;
                            Session["User_Department"] = user.Item2.Type ?? 0;
                            Session["User_Level"] = user.Item2.SkillLevel ?? 0;
                            Session["SalonId"] = user.Item2.SalonId;
                            Session["Menu"] = permission.GetMenuV3(user.Item1.StaffId.Value);
                            Session["IsActive"] = user.Item1.IsActive;
                            Session["FirstLogin"] = user.Item1.FirstLogin;
                            Session["LastChangePassword"] = user.Item1.LastChangePassword ?? DateTime.Now;
                            Session["TimeLivePassword"] = user.Item1.TimeLivePassword;
                            Session["IsLogin"] = user.Item2.isAccountLogin == 1 ? true : false;
                            Session["User_Permission"] = permission.GetPermisionNames(user.Item2.Id);

                            // access token
                            Response.Cookies["AccessToken"].Value = authResponse.AuthenticationResult.AccessToken;
                            // refresh token
                            Response.Cookies["RefreshToken"].Value = authResponse.AuthenticationResult.RefreshToken;
                            // Id token
                            Response.Cookies["IdToken"].Value = authResponse.AuthenticationResult.IdToken;
                            // userId
                            Response.Cookies["UserID"].Value = user.Item1.UserId;
                            //end

                            SetAccountSession("USER");
                            if (Password.Equals(Libraries.AppConstants.PASSWORD_DEFAULT))
                            {
                                Session["FirstLogin"] = true;
                                if (Session["RequiredValid2FA"] != null && !(bool)Session["RequiredValid2FA"])
                                {
                                    if (Session["FirstLogin"] != null && (bool)Session["FirstLogin"])
                                    {
                                        UIHelpers.Redirect("/doi-mat-khau.html");
                                    }
                                    else
                                    {
                                        Session["IsValid2FA"] = true;
                                    }
                                    RedirectToOriginPage();
                                }
                                else
                                {
                                    UIHelpers.Redirect("/xac-nhan-otp.html");
                                }
                            }
                            else
                            {
                                if (Session["RequiredValid2FA"] != null && !(bool)Session["RequiredValid2FA"])
                                {
                                    if (Session["FirstLogin"] != null && (bool)Session["FirstLogin"])
                                    {
                                        UIHelpers.Redirect("/doi-mat-khau.html");
                                    }
                                    else
                                    {
                                        Session["IsValid2FA"] = true;
                                    }
                                    RedirectToOriginPage();
                                }
                                else
                                {
                                    UIHelpers.Redirect("/xac-nhan-otp.html");
                                }
                            }
                        }
                    }
                    else
                    {
                        Library.Function.TriggerJsMsgSystem(this, "Có lỗi xảy ra vui lòng liên hệ nhà phát triển!", 4);
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                Library.Function.TriggerJsMsgSystem(this, Library.Function.CustomExceptionAWS(e), 4);
                return;
            }
        }

        protected void BtnForgotPassword(object sender, EventArgs e)
        {
            try
            {
                var name = UserNameSend.Value;

                if (String.IsNullOrEmpty(name))
                {
                    Library.Function.TriggerJsMsgSystem(this, "Vui lòng nhập tài khoản.", 3);
                    return;
                }

                using (var db = new Solution_30shineEntities())
                {
                    using (var client = new HttpClient())
                    {
                        MD5 md5 = MD5.Create();

                        var user = model.GetUser(name);

                        if (user == null)
                        {
                            Library.Function.TriggerJsMsgSystem(this, "Tài khoản không tồn tại hoặc bị vô hiệu hoá!", 3);
                            return;
                        }

                        if (!String.IsNullOrEmpty(user.Item1.Email))
                        {
                            var token = DateTime.Now.ToString() + Libraries.AppConstants.KEY + user.Item1.UserId;

                            var list = new List<string>() { user.Item1.Email };

                            var send = new SendEmailInput()
                            {
                                to = list,
                                subject = "Yêu cầu thay đổi mật khẩu!",
                                message = $@"<html>
                                                <head>
                                                </head>
                                                <body>
                                                <p> Xin chào<b> {user.Item1.Username} </b></p>
                                                   <p style = 'line-height: 24px;'>
                                                     Chúng tôi vừa nhận được yêu cầu thay đổi mật khẩu từ tài khoản liên kết với email này.<br/>
                                                    Nếu đúng là bạn vui lòng click vào <a href='{Request.Url.Scheme}{Uri.SchemeDelimiter}{Request.Url.Authority}/doi-mat-khau/{Library.Function.GenMD5(MD5.Create(), token)}.html'>đây</a>.<br/>
                                                    Nếu không phải bạn vui lòng bỏ qua E-mail này.<br/>
                                                    Đây là email tự động từ hệ thống vui lòng không trả lời thư này.<br/>
                                                    Trân trọng!<br/>
                                                    30shine IT Team
                                                  </p>
                                                  </body>
                                            </html>"
                            };

                            ServicePointManager.Expect100Continue = true;
                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                            client.BaseAddress = new Uri(Libraries.AppConstants.URL_API_PUSH_NOTICE);
                            client.DefaultRequestHeaders.Accept.Clear();
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            var response = client.PostAsJsonAsync("/api/pushNotice/email", send).Result;
                            if (response.IsSuccessStatusCode)
                            {
                                Session["User_Account"] = user.Item1.Username;
                                UserNameSend.Value = String.Empty;
                                db.Database.ExecuteSqlCommand($@"UPDATE dbo.Users SET Valid2FA = 0 WHERE Username = '{name}'");
                                Library.Function.TriggerJsMsgSystem(this, $"Yêu cầu thay đổi mật khẩu của bạn đã được gửi tới E-mail: {user.Item1.Email}.", 2, 7000);
                                return;
                            }
                            else
                            {
                                Library.Function.TriggerJsMsgSystem(this, $"Thao tác thực hiện không thành công. Vui lòng liên hệ nhà phát triển!", 4, 7000);
                                return;
                            }
                        }
                        else if (!String.IsNullOrEmpty(user.Item1.Phone))
                        {
                            Session["User_Account"] = user.Item1.Username;
                            Session["User_Phone"] = user.Item1.Phone;
                            Session["ForgotPassword"] = true;
                            Session["UniqueKey"] = Library.Function.GenMD5(md5, user.Item1.UserId + Libraries.AppConstants.KEY);
                            Session["UrlImage"] = Library.Function.GenerateQRCode(user.Item1.Username, Session["UniqueKey"].ToString());
                            Session["Valid2FA"] = false;
                            UserNameSend.Value = String.Empty;
                            db.Database.ExecuteSqlCommand($@"UPDATE dbo.Users SET Valid2FA = 0 WHERE Username = '{name}'");
                            UIHelpers.Redirect("/xac-nhan-otp.html");
                        }
                        else
                        {
                            Library.Function.TriggerJsMsgSystem(this, $"Thông tin tài khoản không chính xác vui lòng liên hệ bộ phận Vận Hành!", 4, 7000);
                            return;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Library.Function.TriggerJsMsgSystem(this, Library.Function.CustomExceptionAWS(ex), 4);
                return;
            }
        }

        public bool CheckExistAccount(string UserName, string Password)
        {
            bool check = false;
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var staff = db.Staffs.FirstOrDefault(w => w.Email == UserName.Trim());
                    if (staff != null)
                    {
                        if (Library.Function.GenMD5(MD5.Create(), Password) == staff.Password)
                        {
                            model.SignUp(UserName, Password);

                            model.AdminConfirmSignUp(UserName);

                            check = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                check = true;
            }
            return check;
        }

        /// <summary>
        /// set account session
        /// </summary>
        /// <param name="key"></param>
        /// <param name="user"></param>
        protected void SetAccountSession(string key = "ROOT")
        {
            var accountSession = new AccountSession()
            {
                UserId = "ROOT".Equals(key) ? 0 : (int)Session["User_Id"],
                UserAccount = "ROOT".Equals(key) ? "root" : Session["User_Account"].ToString(),
                UserCode = "ROOT".Equals(key) ? "ROOT" : Session["User_Code"].ToString(),
                UserName = "ROOT".Equals(key) ? "Root" : Session["User_Name"].ToString(),
                UserPhone = "ROOT".Equals(key) ? "" : Session["User_Id"].ToString(),
                UserEmail = "ROOT".Equals(key) ? "no-reply@30shine.com" : Session["User_Email"].ToString(),
                UserBirthDay = "",
                UserSalonId = "ROOT".Equals(key) ? 0 : (int)Session["SalonId"],
                UserSalonName = "",
                UserDepartmentId = "ROOT".Equals(key) ? 0 : (int)Session["User_Department"],
                UserDepartmentName = "",
                UserSkillLevelId = "ROOT".Equals(key) ? 0 : (int)Session["User_Level"],
                UserSkillLevelName = "",
                UserPermission = "ROOT".Equals(key) ? "root" : Session["User_Permission"].ToString(),
                UserPermissionId = "ROOT".Equals(key) ? "0" : "",
                UserMenu = Session["Menu"].ToString()
            };

            AccountModel.Instance.SetAccountInfor(accountSession);

        }

        /// <summary>
        /// Check is login
        /// </summary>
        private void CheckIsLogin()
        {
            if (Session["UserID"] != null && Session["IsValid2FA"] != null && Convert.ToBoolean(Session["IsValid2FA"]))
            {
                RedirectToOriginPage();
            }
        }

        public class UserResponse
        {
            public string UserId { get; set; }
            public int? SalonId { get; set; }
            public string UrlImage { get; set; }
            public bool? Valid2FA { get; set; }
            public int StaffId { get; set; }
            public string Fullname { get; set; }
            public string UserCode { get; set; }
            public string UserEmail { get; set; }
            public string UserPhone { get; set; }
            public int UserDepartmentId { get; set; }
            public int UserSkillLevelId { get; set; }
            public string UserPermission { get; set; }
            public string UserPermissionId { get; set; }
            public int? SNDay { get; set; }
            public int? SNMonth { get; set; }
            public int? SNYear { get; set; }
            public object UserToken { get; set; }
            public string UniqueKey { get; set; }
            public bool? IsActive { get; set; }
            public bool? FirstLogin { get; set; }
            public DateTime? LastChangePassword { get; set; }
            public int? TimeLivePassword { get; set; }
            public bool RequiredValid2FA { get; set; }
        }

        public class UserInput
        {
            public string username { get; set; }
            public string password { get; set; }
        }

        public class SendEmailInput
        {
            public List<string> to { get; set; }
            public string subject { get; set; }
            public string message { get; set; }
        }
    }
}
