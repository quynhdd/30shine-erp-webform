﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using Amazon.CognitoIdentityProvider;
using Amazon.CognitoIdentityProvider.Model;
using Google.Authenticator;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.Security.Login
{
    public partial class Authen2FA : System.Web.UI.Page
    {
        protected static int Index;
        private IAuthenticationModel model = new AuthenticationModel();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserID"] == null)
            {
                if (Session["ForgotPassword"] == null || !(bool)Session["ForgotPassword"])
                {
                    UIHelpers.Redirect("/dang-nhap.html");
                }
            }
            if (!IsPostBack)
            {
                QRCode.ImageUrl = Session["UrlImage"].ToString();
                if (!(bool)Session["Valid2FA"])
                {
                    QRCode.Visible = true;
                    QRCodeImg.Visible = true;
                    TextAuthenticator.Visible = true;
                }
                else
                {
                    QRCode.Visible = false;
                    QRCodeImg.Visible = false;
                    TextAuthenticator.Visible = false;
                }
            }
        }

        protected void Verify2FA(object sender, EventArgs e)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var code = ValidCode.Text;
                    var username = Session["User_Account"].ToString();
                    var userphone = Session["User_Phone"].ToString();
                    if (String.IsNullOrEmpty(code))
                    {
                        Library.Function.TriggerJsMsgSystem(this, "Vui lòng nhập OTP!", 3);
                        ValidCode.Focus();
                        return;
                    }
                    if (!Regex.IsMatch(code, Library.Function.GetRegexOnlyNumber().ToString()))
                    {
                        Library.Function.TriggerJsMsgSystem(this, "OTP chỉ được nhập số và độ dài phải bằng 6 ký tự!", 3);
                        ValidCode.Focus();
                        return;
                    }
                    TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
                    string UniqueKey = Session["UniqueKey"].ToString();
                    var expire = new TimeSpan(0, 0, 30);
                    bool isValid = tfa.ValidateTwoFactorPIN(UniqueKey, code, expire);
                    if (isValid)
                    {
                        if (!(bool)Session["Valid2FA"])
                        {
                            db.Database.ExecuteSqlCommand($@"UPDATE dbo.Users SET Valid2FA = 1 WHERE Username = '{username}'");
                        }

                        if (Convert.ToBoolean(Session["ForgotPassword"]))
                        {
                            using (var client = new HttpClient())
                            {
                                var passRandom = Library.Function.RandomString(8);

                                var send = new SendPhoneInput()
                                {
                                    phone_number = userphone,
                                    message = $"Mat khau tam thoi cua ban la: {passRandom}"
                                };

                                ServicePointManager.Expect100Continue = true;
                                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                                client.BaseAddress = new Uri(Libraries.AppConstants.URL_API_PUSH_NOTICE);
                                client.DefaultRequestHeaders.Accept.Clear();
                                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                                var result = client.PostAsJsonAsync("/api/pushNotice/smsVMG", send).Result;
                                if (result.IsSuccessStatusCode)
                                {
                                    //lay thong tin user
                                    var isExistCog = model.CheckUserExistCognito(username);
                                    //
                                    if (isExistCog)
                                    {
                                        model.AdminDeleteUser(username);
                                    }

                                    model.SignUp(username, passRandom);

                                    model.AdminConfirmSignUp(username);

                                    var user = model.GetUser(username);

                                    if (user != null)
                                    {
                                        user.Item1.FirstLogin = true;
                                        user.Item1.ModifiedDate = DateTime.Now;
                                        db.Users.AddOrUpdate(user.Item1);
                                        db.SaveChanges();
                                    }

                                    Session["ForgotPassword"] = true;

                                    UIHelpers.Redirect("/dang-nhap.html");
                                }
                                else
                                {
                                    Library.Function.TriggerJsMsgSystem(this, $"Thao tác thực hiện không thành công. Vui lòng liên hệ nhà phát triển!", 4, 7000);
                                    return;
                                }
                            }
                        }

                        Session["IsValid2FA"] = true;
                        if (Convert.ToBoolean(Session["FirstLogin"]))
                        {
                            UIHelpers.Redirect("/doi-mat-khau.html");
                        }
                        else
                        {
                            var time = Convert.ToDateTime(Session["LastChangePassword"]).AddDays(Convert.ToInt32(Session["TimeLivePassword"])).Date - DateTime.Now.Date;
                            if (time.Days <= 0)
                            {
                                UIHelpers.Redirect("/doi-mat-khau.html");
                            }
                            else
                            {
                                UIHelpers.Redirect("/");
                            }
                        }
                    }
                    else
                    {
                        Index++;
                        Library.Function.TriggerJsMsgSystem(this, "Mã không chính xác. Vui lòng kiểm tra lại hoặc liên hệ nhà phát triển!", 4, 5000);
                        if (Index is 5)
                        {
                            HttpContext.Current.Session.Abandon();
                            UIHelpers.Redirect("/dang-nhap.html");
                        }
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                Library.Function.TriggerJsMsgSystem(this, Library.Function.CustomExceptionAWS(ex), 4, 7000);
                return;
            }
        }

        protected void ForgotCodeAuth(object sender, EventArgs e)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var username = Session["User_Account"].ToString();
                    var userphone = Session["User_Phone"].ToString();

                    var user = model.GetUser(username);

                    if (user == null)
                    {
                        Library.Function.TriggerJsMsgSystem(this, "Có lỗi xảy ra vui lòng liên hệ nhóm phát triển!", 3);
                        return;
                    }

                    var code = Library.Function.RandomString(6);

                    if (!String.IsNullOrEmpty(user.Item1.Email))
                    {
                        var list = new List<string>() { user.Item1.Email };

                        var send = new SendEmailInput()
                        {
                            to = list,
                            subject = "Yêu cầu quét lại QR Code!",
                            message = $@"<html>
                                                <head>
                                                </head>
                                                <body>
                                                <p> Xin chào<b> {user.Item1.Username} </b></p>
                                                   <p style = 'line-height: 24px;'>
                                                    Chúng tôi vừa nhận được yêu cầu thay đổi mã QR Code từ tài khoản liên kết với email này.<br/>
                                                    Mã để thay đổi của bạn là: <p style='font-size:20px'>{code}</p><br/>
                                                    Nếu không phải bạn vui lòng bỏ qua E-mail này.<br/>
                                                    Đây là E-mail tự động từ hệ thống vui lòng không trả lời thư này.<br/>
                                                    Trân trọng!<br/>
                                                    30shine IT Team
                                                  </p>
                                                  </body>
                                            </html>"
                        };

                        using (var client = new HttpClient())
                        {
                            ServicePointManager.Expect100Continue = true;
                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                            client.BaseAddress = new Uri(Libraries.AppConstants.URL_API_PUSH_NOTICE);
                            client.DefaultRequestHeaders.Accept.Clear();
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            var response = client.PostAsJsonAsync("/api/pushNotice/email", send).Result;
                            if (response.IsSuccessStatusCode)
                            {
                                Library.Function.TriggerJsMsgSystem(this, $"Yêu cầu thay đổi mã QR Code của bạn đã được gửi tới E-mail: {user.Item1.Email}.", 2, 7000);
                            }
                            else
                            {
                                Library.Function.TriggerJsMsgSystem(this, $"Thao tác thực hiện không thành công. Vui lòng liên hệ nhà phát triển!", 4, 7000);
                                return;
                            }
                        }
                    }
                    else if (!String.IsNullOrEmpty(user.Item1.Phone))
                    {
                        var send = new SendPhoneInput()
                        {
                            phone_number = userphone,
                            message = $"Ma thay doi QR Code cua ban la: {code}"
                        };
                        using (var client = new HttpClient())
                        {
                            ServicePointManager.Expect100Continue = true;
                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                            client.BaseAddress = new Uri(Libraries.AppConstants.URL_API_PUSH_NOTICE);
                            client.DefaultRequestHeaders.Accept.Clear();
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            var result = client.PostAsJsonAsync("/api/pushNotice/smsVMG", send).Result;
                            if (result.IsSuccessStatusCode)
                            {
                                Library.Function.TriggerJsMsgSystem(this, $"Yêu cầu thay đổi mã qr code đã được gửi tới số điện thoại của bạn!", 2, 10000);
                            }
                            else
                            {
                                Library.Function.TriggerJsMsgSystem(this, $"Thao tác thực hiện không thành công. Vui lòng liên hệ nhà phát triển!", 4, 7000);
                                return;
                            }
                        }
                    }
                    else
                    {
                        Library.Function.TriggerJsMsgSystem(this, $"Thông tin tài khoản không chính xác vui lòng liên hệ bộ phận Vận Hành!", 4, 7000);
                        return;
                    }

                    model.AdminUpdateUserAttribute(username, code);

                    TriggerJsOpenModal(this);
                }
            }
            catch (Exception ex)
            {
                Library.Function.TriggerJsMsgSystem(this, Library.Function.CustomExceptionAWS(ex), 4);
                return;
            }
        }

        protected void BtnForgotCodeAuth(object sender, EventArgs e)
        {
            try
            {
                Amazon.RegionEndpoint Region = Amazon.RegionEndpoint.APSoutheast1;
                var username = Session["User_Account"].ToString();
                var chkCode = CodeSend.Value;
                if (String.IsNullOrEmpty(username))
                {
                    Library.Function.TriggerJsMsgSystem(this, "Vui lòng nhập tài khoản.", 3);
                    return;
                }

                var code = model.AdminGetAttribute(username);

                if (code == chkCode)
                {
                    using (var db = new Solution_30shineEntities())
                    {
                        if (Convert.ToBoolean(Session["Valid2FA"]))
                        {
                            db.Database.ExecuteSqlCommand($@"UPDATE dbo.Users SET Valid2FA = 0 WHERE Username = '{username}'");
                            Session["Valid2FA"] = false;
                            Library.Function.TriggerJsMsgSystem(this, "Lấy lại QR CODE thành công.", 3);
                            Response.Redirect(Request.RawUrl);
                        }
                        else
                        {
                            Library.Function.TriggerJsMsgSystem(this, "Thao tác thực hiện không thành công. Vui lòng kiểm tra lại!", 3);
                        }
                    }
                }
                else
                {
                    Library.Function.TriggerJsMsgSystem(this, "Mã xác nhận không chính xác, vui lòng nhập lại!", 3);
                }
            }
            catch (Exception ex)
            {
                Library.Function.TriggerJsMsgSystem(this, Library.Function.CustomExceptionAWS(ex), 4);
                return;
            }
        }

        protected void ComeBackLoginPage(object sender, EventArgs e)
        {
            HttpContext.Current.Session.Abandon();
            ClearAllCookies();
            UIHelpers.Redirect("/dang-nhap.html");
        }

        private void ClearAllCookies()
        {
            string[] myCookies = Request.Cookies.AllKeys;
            foreach (string cookie in myCookies)
            {
                Response.Cookies[cookie].Expires = DateTime.Now.AddDays(-1);
            }
        }

        /// <summary>
        /// Trả thông báo về client 
        /// </summary>
        /// <param name="_OBJ"></param>
        /// <param name="msg"></param>
        /// <param name="status"></param>        
        public static void TriggerJsOpenModal(Page page)
        {
            ScriptManager.RegisterStartupScript(page, page.GetType(), "Open Modal", "ShowModal();", true);
        }

        /// <summary>
        /// class param otp
        /// </summary>
        public class ParamOtp
        {
            public string Code { get; set; }
            public string UniqueKey { get; set; }
        }

        public class SendEmailInput
        {
            public List<string> to { get; set; }
            public string subject { get; set; }
            public string message { get; set; }
        }
    }

    public class SendPhoneInput
    {
        public string phone_number { get; set; }
        public string message { get; set; }
    }
}