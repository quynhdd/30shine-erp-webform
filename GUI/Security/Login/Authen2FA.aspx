﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Authen2FA.aspx.cs" Inherits="_30shine.GUI.Security.Login.Authen2FA" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="/Assets/css/font.css" rel="stylesheet" />
    <link href="/Assets/css/Login_V2.css?v=2" rel="stylesheet" />
    <link href="/Assets/css/bootstrap/bootstrap.css" rel="stylesheet" />
    <link href="/Assets/css/bootstrap/bootstrap-theme.css" rel="stylesheet" />
    <link href="/Assets/izitoast/css/iziToast.min.css" rel="stylesheet" />
    <script src="/Assets/izitoast/js/iziToast.min.js"></script>
    <script src="/Assets/js/jquery.v1.11.1.js"></script>
    <script src="/Assets/js/bootstrap/bootstrap.min.js"></script>
    <script src="/Assets/js/Common/Common.js"></script>
    <style>
        body {
            background: #31343d;
            font-family: 'Roboto Regular';
        }

        label {
            color: #fff;
            font-size: 15px;
            text-transform: uppercase;
            font-weight: 100;
            width: 100%;
        }

        #formOTP {
            margin: 0 auto;
        }

            #formOTP div {
                padding-bottom: 20px;
            }

        #QRCode {
            width: 250px;
            height: 250px;
            margin: 0 auto;
            margin-bottom: 20px;
        }

        #ValidCode {
            width: 100%;
            line-height: 40px;
            border: 1px solid #ccc !important;
            border-radius: 5px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            margin-top: 10px;
            margin-bottom: 20px;
            height: 40px;
            font-size: 17px;
        }

        #BtnSubmit {
            width: 100%;
            border: 2px solid #ccc !important;
            padding: 10px 20px;
            float: right;
            border-radius: 5px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            background: #31343d;
            color: #fff;
            font-weight: 400;
            text-shadow: 0 1px 0 #ccc;
            font-size: 18px;
        }

        body {
            background-color: #31343d;
            font-family: 'Roboto Regular';
        }

        .box-qr-code {
            width: 100%;
        }

            .box-qr-code span {
                color: wheat;
                line-height: 2;
            }

        .fix1 {
            margin-top: 0;
        }

        .ndung {
            margin: 20px 50px 0 50px;
        }

        .nd {
            margin-bottom: 0;
        }

        .img-logo {
            padding: 30px 0px 40px 0px;
        }

        #ComeBackLogin:hover {
            cursor: pointer;
        }

        #ForgotCodeAuthentication:hover {
            cursor: pointer;
        }

        .btn-default {
            border-color: #636770;
        }

        .form-forget {
            margin-bottom: 10px;
        }

        .modal-content {
            background-color: #414345;
        }

        .modal-title {
            color: #e4e5e6;
        }

        .modal-header {
            border-bottom: 1px solid #333;
        }

        .modal-footer {
            border-top: 1px solid #333;
        }

        .modal-body label {
            color: #e4e5e6;
        }
    </style>
</head>
<body>
    <form id="formOTP" runat="server">
        <div>
            <div class="all">
                <div class="fix1 container">
                    <div class="logo-wrap">
                        <img class="img-logo" src="/Assets/images/ic_logo_white.png" />
                    </div>
                    <div class="bg">
                        <div class="form-horizontal">
                            <div>
                                <span runat="server" id="TextAuthenticator" class="box-qr-code">
                                    <span>Mở ứng dụng Google Authenticator để quét mã dưới đây.</span>
                                </span>
                                <span runat="server" id="QRCodeImg" class="box-qr-code">
                                    <asp:Image runat="server" ClientIDMode="Static" ID="QRCode" />
                                </span>
                                <label for="ValidCode">Nhập mã Authenticator</label>
                                <asp:TextBox runat="server" ClientIDMode="Static" CssClass="form-control fixform1" ID="ValidCode" placeholder="Nhập mã trên điện thoại của bạn" autocomplete="off"></asp:TextBox>
                                <asp:Button ID="BtnSubmit" CssClass="fixbtn fix4 btn btn-default fix3" runat="server" OnClick="Verify2FA" Text="Hoàn tất" />
                                <p id="ForgotCodeAuthentication" runat="server" style="float: left; color: whitesmoke; text-decoration: underline; margin-top: 10px;">Quên mã Authenticator?</p>
                                <asp:Button ID="ForgotCode" OnClick="ForgotCodeAuth" CssClass="hidden" runat="server" Style="float: right; color: whitesmoke; text-decoration: underline; margin-top: 10px;" Text="Trở lại trang đầu?" />
                                <p id="ComeBackLogin" runat="server" style="float: right; color: whitesmoke; text-decoration: underline; margin-top: 10px;">Trở lại trang đăng nhập?</p>
                                <asp:Button ID="Comeback" OnClick="ComeBackLoginPage" CssClass="hidden" runat="server" Style="float: right; color: whitesmoke; text-decoration: underline; margin-top: 10px;" Text="Trở lại trang đầu?" />
                            </div>
                        </div>
                    </div>
                    <div class="ndung">
                        <p class="nd" style="font-size: 14px;">30Shine tin tưởng, nỗ lực mỗi ngày để kết nối bàn tay tài hoa của người Việt cùng quy trình khoa học 30 phút nhằm đem đến cho phái mạnh toàn cầu kiểu tóc đẹp trai, làn da khỏe mạnh cuốn hút phái đẹp; tinh thần thư giãn để bứt phá trong công việc</p>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Quên mã Authenticator?</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-forget">
                            <label for="username">Mã của bạn:</label>
                            <input type="text" runat="server" id="CodeSend" name="code" class="form-control username fixform" placeholder="example" autocomplete="off" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button ID="BtnComplete" CssClass="fixbtn fix4 btn btn-default fix3" runat="server" OnClick="BtnForgotCodeAuth" Text="Xác nhận" />
                    </div>
                </div>
            </div>
        </div>
    </form>
    <script type="text/javascript">
        $("#ComeBackLogin").click(function () {
            $("#Comeback").click();
        });

        $("#ForgotCodeAuthentication").click(function () {
            $("#ForgotCode").click();
        });

        $('#CodeSend').keypress(function (e) {
            var key = e.which;
            if (key == 13)  // the enter key code
            {
                $("#BtnComplete").click();
                return false;
            }
        });

        $('#ValidCode').keypress(function (e) {
            var key = e.which;
            if (key == 13)  // the enter key code
            {
                $("#BtnSubmit").click();
                return false;
            }
        });
    </script>
</body>
</html>
