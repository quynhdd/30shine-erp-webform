﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using Amazon.CognitoIdentityProvider;
using Amazon.CognitoIdentityProvider.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.Security.Login
{
    public partial class ChangePassword : System.Web.UI.Page
    {
        private IAuthenticationModel model = new AuthenticationModel();

        public int Retry;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["Code"] == null)
            {
                if (Session["User_Id"] == null)
                {
                    UIHelpers.Redirect("/dang-nhap.html");
                }
                if (!IsPostBack)
                {
                    if (Session["UserID"].ToString() != "0")
                    {
                        if (Session["UserID"].ToString() != "0" && !(bool)Session["FirstLogin"])
                        {
                            PasswordOld.Visible = true;
                            var time = Convert.ToDateTime(Session["LastChangePassword"]).AddDays(Convert.ToInt32(Session["TimeLivePassword"])) - DateTime.Now;
                            if (time.Days <= 1)
                            {
                                Library.Function.TriggerJsMsgSystem(this, "Mật khẩu đã hết hạn, yêu cầu thay mật khẩu mới!", 3, 7000);
                                return;
                            }
                        }
                        else
                        {
                            PasswordOld.Visible = false;
                            Library.Function.TriggerJsMsgSystem(this, "Yêu cầu thay đổi mật khẩu khi lần đầu đăng nhập, tìm lại mật khẩu hoặc nhân viên cùng sở hữu tài khoản nghỉ việc!", 1, 10000);
                            return;
                        }
                    }
                    else
                    {
                        UIHelpers.Redirect("/");
                    }
                }
            }
            else
            {
                PasswordOld.Visible = false;
            }
        }

        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                string PasswordGen = "";
                var password = Password.Text;
                // check ma hoa MD5 update vao table Staff
                if (!string.IsNullOrEmpty(password))
                {
                    using (MD5 md5Hash = MD5.Create())
                    {
                        PasswordGen = model.GenPassword(md5Hash, password);
                    }
                }
                var confirmPassword = ConfirmPassword.Text;
                var oldPassword = "";
                var username = Session["User_Account"] != null ? Session["User_Account"].ToString() : "";
                if (String.IsNullOrEmpty(username))
                {
                    Library.Function.TriggerJsMsgSystem(this, "Thao tác này không hợp lệ!", 3);
                    return;
                }
                if (String.IsNullOrEmpty(password) || String.IsNullOrEmpty(confirmPassword))
                {
                    Library.Function.TriggerJsMsgSystem(this, "Vui lòng nhập mật khẩu và xác nhận mật khẩu!", 3);
                    return;
                }
                else if (!Regex.IsMatch(password, Library.Function.GetRegexPassword().ToString()))
                {
                    Library.Function.TriggerJsMsgSystem(this, "Yêu cầu mật khẩu: ( Tối thiểu 8 ký tự, có 1 ký tự hoa, 1 ký tự số, 1 ký tự đặc biệt và ký tự thường)", 3, 7000);
                    return;
                }
                else if (password != confirmPassword)
                {
                    Library.Function.TriggerJsMsgSystem(this, "Xác nhận mật khẩu không trùng khớp", 3);
                    return;
                }
                if (password.Equals(Libraries.AppConstants.PASSWORD_DEFAULT))
                {
                    Library.Function.TriggerJsMsgSystem(this, "Yêu cầu mật khẩu không được trùng với mật khẩu mặc định!", 3);
                    return;
                }
                var Code = Request.QueryString["Code"] != null ? Request.QueryString["Code"].ToString() : "";
                if (String.IsNullOrEmpty(Code))
                {
                    if (!(bool)Session["FirstLogin"])
                    {
                        oldPassword = OldPassword.Text;
                        if (String.IsNullOrEmpty(oldPassword))
                        {
                            Library.Function.TriggerJsMsgSystem(this, "Vui lòng nhập mật khẩu cũ!", 3);
                            return;
                        }
                        if (oldPassword.Equals(password))
                        {
                            Library.Function.TriggerJsMsgSystem(this, "Mật khẩu mới không được trùng với mật khẩu cũ!", 3);
                            return;
                        }
                    }

                    if ((!(bool)Session["FirstLogin"] && Session["ForgotPassword"] == null) || (!(bool)Session["FirstLogin"] && !(bool)Session["ForgotPassword"]))
                    {
                        oldPassword = OldPassword.Text;
                        if (String.IsNullOrEmpty(oldPassword))
                        {
                            Library.Function.TriggerJsMsgSystem(this, "Vui lòng nhập mật khẩu cũ!", 3);
                            return;
                        }
                        if (oldPassword.Equals(password))
                        {
                            Library.Function.TriggerJsMsgSystem(this, "Mật khẩu mới không được trùng với mật khẩu cũ!", 3);
                            return;
                        }
                    }

                    var token = Request.Cookies["AccessToken"].Value;

                    if (!String.IsNullOrEmpty(oldPassword))
                    {
                        model.ChangePassword(token, oldPassword, password);
                    }
                    else
                    {
                        model.AdminDeleteUser(username);

                        model.SignUp(username, password);

                        model.AdminConfirmSignUp(username);
                    }

                    Session["FirstLogin"] = false;
                    Session["IsValid2FA"] = true;
                    Session["LastChangePassword"] = DateTime.Now;

                    var user = model.GetUser(username);


                    if (user != null)
                    {
                        if (user.Item1 != null)
                        {
                            using (var db = new Solution_30shineEntities())
                            {
                                user.Item1.FirstLogin = false;
                                user.Item1.LastChangePassword = DateTime.Now;
                                user.Item1.ModifiedDate = DateTime.Now;
                                db.Users.AddOrUpdate(user.Item1);
                                db.SaveChanges();
                            }
                        }
                        if (user.Item2 != null)
                        {
                            using (var db = new Solution_30shineEntities())
                            {
                                user.Item2.Password = PasswordGen;
                                db.Staffs.AddOrUpdate(user.Item2);
                                db.SaveChanges();
                            }
                        }
                    }
                }
                else
                {
                    model.AdminDeleteUser(username);

                    model.SignUp(username, password);

                    model.AdminConfirmSignUp(username);

                    Session["FirstLogin"] = false;
                    Session["IsValid2FA"] = true;
                    Session["LastChangePassword"] = DateTime.Now;

                    var user = model.GetUser(username);

                    if (user != null)
                    {
                        if (user.Item1 != null)
                        {
                            using (var db = new Solution_30shineEntities())
                            {
                                user.Item1.FirstLogin = false;
                                user.Item1.LastChangePassword = DateTime.Now;
                                user.Item1.ModifiedDate = DateTime.Now;
                                db.Users.AddOrUpdate(user.Item1);
                                db.SaveChanges();
                            }
                        }
                        if (user.Item2 != null)
                        {
                            using (var db = new Solution_30shineEntities())
                            {
                                user.Item2.Password = PasswordGen;
                                db.Staffs.AddOrUpdate(user.Item2);
                                db.SaveChanges();
                            }
                        }
                    }
                }

                if (Convert.ToBoolean(Session["IsLogin"]))
                {
                    using (var client = new HttpClient())
                    {
                        using (var db = new Solution_30shineEntities())
                        {
                            var userphone = Session["User_Phone"].ToString();
                            var salonId = int.TryParse(Session["SalonId"].ToString(), out var integer) ? integer : 0;
                            var salon = db.Tbl_Salon.FirstOrDefault(f => f.Id == salonId);
                            var send = new SendPhoneInput()
                            {
                                phone_number = userphone,
                                message = $"Tài khoản {username} của salon: {salon.Name} đã được thay đổi mật khẩu thành: {password}"
                            };
                            ServicePointManager.Expect100Continue = true;
                            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                            client.BaseAddress = new Uri(Libraries.AppConstants.URL_API_PUSH_NOTICE);
                            client.DefaultRequestHeaders.Accept.Clear();
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            var result = client.PostAsJsonAsync("/api/pushNotice/smsVMG", send).Result;
                            if (result.IsSuccessStatusCode)
                            {
                                //something
                            }
                        }
                    }
                }

                Session["ChangePassword"] = true;

                UIHelpers.Redirect("/");
            }
            catch (Exception ex)
            {
                Library.Function.TriggerJsMsgSystem(this, Library.Function.CustomExceptionAWS(ex, "change_password"), 4);
                return;
            }
        }

        protected void ComeBackLoginPage(object sender, EventArgs e)
        {
            HttpContext.Current.Session.Abandon();
            ClearAllCookies();
            UIHelpers.Redirect("/dang-nhap.html");
        }

        private void ClearAllCookies()
        {
            string[] myCookies = Request.Cookies.AllKeys;
            foreach (string cookie in myCookies)
            {
                Response.Cookies[cookie].Expires = DateTime.Now.AddDays(-1);
            }
        }
    }
}