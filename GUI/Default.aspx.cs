﻿using _30shine.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using _30shine.MODEL.ENTITY.EDMX;
namespace _30shine.GUI
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                int interger = 0;
                Solution_30shineEntities db = new Solution_30shineEntities();
                var StaffId = int.TryParse(Session["User_Id"].ToString().Trim(), out interger) ? interger : 0;
                if (StaffId == 0)
                {
                    Response.Redirect("/admin/bao-cao/hoa-don.html", false);
                }
                else
                {
                    var defaultPage = (from a in db.PermissionMenus
                                       join b in db.PermissionDefaultPages on a.Id equals b.PageId
                                       join c in db.PermissionStaffs on b.PermissionId equals c.PermissionId
                                       where a.IsDelete == false &&
                                             a.IsActive == true &&
                                             b.IsDelete == false &&
                                             c.IsDelete == false &&
                                             c.IsActive == true &&
                                             c.StaffId == StaffId
                                       select new
                                       {
                                           MenuLink = a.Link
                                       }).FirstOrDefault();
                    if (defaultPage != null)
                    {
                        Response.Redirect(defaultPage.MenuLink, false);
                    }
                    else
                    {
                        Response.Write("Chưa được cài đặt trang chủ mặc định!");

                    }
                }



            }
        }

    }
}