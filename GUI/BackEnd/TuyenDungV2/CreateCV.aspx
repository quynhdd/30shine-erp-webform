﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateCV.aspx.cs" Inherits="_30shine.GUI.BackEnd.TuyenDungV2.CreateCV" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ProductAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <link href="/Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="/Assets/js/select2/select2.min.js"></script>
        <style>
            .table-wp {
                background: #f5f5f5;
                float: left;
                width: 100%;
                padding: 15px;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                -webkit-box-shadow: 0px 0px 3px 0px #ddd;
                -moz-box-shadow: 0px 0px 3px 0px #ddd;
                box-shadow: 0px 0px 3px 0px #ddd;
            }

            .tr-skill {
                display: none;
            }

            .tr-show {
                display: table-row;
            }

            .customer-add .table-add tr td.right .fb-cover-error {
                top: 0;
                right: auto;
            }

            #success-alert {
                top: 100px;
                right: 10px;
                position: fixed;
                width: 20% !important;
                z-index: 2000;
            }

            .div_span span {
                margin-left: 2px !important;
            }
        </style>
        <div class="alert alert-danger" id="success-alert">
            <%--<button type="button" class="close" data-dismiss="alert">x</button>--%>
            <strong>Cảnh báo: </strong>
            <span id="msg-alert"></span>
        </div>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Tuyển dụng &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/admin/tuyen-dung/lap-ho-so/danh-sach-ho-so.html">Danh sách ứng viên</a></li>
                        <li class="li-add active"><a href="#">Thêm mới ứng viên </a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add admin-product-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="table-wp">
                    <table class="table-add admin-product-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Nhập thông tin ứng viên<i style="color: red">&nbsp;&nbsp Các trường đánh dấu * là bắt buộc</i></strong></td>
                                <td></td>
                            </tr>

                            <tr class="tr-margin" style="height: 20px;">
                                
                                
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>ID Học viên(Nếu chuyển từ S4M)</span></td>
                                <td class="col-xs-9 right">
                                     <div class="field-wp">
                                         <div class="div_col">
                                           <asp:TextBox ID="txtS4M_Id" CssClass="form-control" runat="server" ClientIDMode="Static"  placeholder="Nhập ID học viên"></asp:TextBox>
                                        </div>
                                        <div class="div_col">
                                        <span class="field-wp">
                                        <asp:Button ID="Button1" CssClass="btn-send" runat="server" Text="Lấy dữ liệu" ClientIDMode="Static" OnClientClick="if(!ValidationS4MTextbox()) return false;" OnClick="GetdataS4M"></asp:Button>
                                        </span>
                                        </div>
                                        
                                    </div>
                                </td>
                            </tr>

                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Họ tên /Số điện thoại<strong style="color: red"> &nbsp*</strong></span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <asp:TextBox ID="FullName" CssClass="form-control" runat="server" ClientIDMode="Static" placeholder="Họ tên"></asp:TextBox>
                                        </div>
                                        <div class="div_col">
                                            <asp:TextBox ID="Phone" CssClass="form-control" class="number-only" runat="server" ClientIDMode="Static" placeholder="Số điện thoại"></asp:TextBox>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Salon/Bộ phận<strong style="color: red"> &nbsp*</strong></span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <asp:DropDownList runat="server" CssClass="form-control select" Width="100%" ID="ddlSalon" ClientIDMode="Static"></asp:DropDownList>
                                        </div>
                                        <div class="div_col">
                                            <asp:DropDownList runat="server" CssClass="form-control select" ID="ddlDepartment" Width="100%" onchange="choseDepartment($(this));" ClientIDMode="Static"></asp:DropDownList>
                                            <%--<asp:RequiredFieldValidator ID="rfvDepartment" runat="server" ControlToValidate="ddlDepartment"
                                                InitialValue="0" ErrorMessage="Vui lòng chọn bộ phận!!!"></asp:RequiredFieldValidator>--%>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-3 left"><span>Ngày sinh/Số CMT</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <asp:TextBox ID="NgaySinh" runat="server" CssClass="form-control txtdatetime" ClientIDMode="Static" placeholder="Ngày sinh"></asp:TextBox>
                                        </div>
                                        <div class="div_col">
                                            <asp:TextBox ID="CMT" CssClass="form-control" runat="server" class="number-only" ClientIDMode="Static" placeholder="Số CMT"></asp:TextBox>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Nơi cấp/Ngày cấp CMT</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <asp:TextBox ID="IssuedByCMT" runat="server" CssClass="form-control" ClientIDMode="Static" placeholder="Nơi cấp"></asp:TextBox>
                                        </div>
                                        <div class="div_col">
                                            <asp:TextBox ID="DateByCMT" CssClass="form-control txtdatetime" runat="server" ClientIDMode="Static" placeholder="Ngày cấp"></asp:TextBox>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Khu vực/Nguồn</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <asp:DropDownList runat="server" CssClass="form-control select" Width="100%" ID="ddlTinhThanh" ClientIDMode="Static"></asp:DropDownList>
                                        </div>
                                        <div class="div_col">
                                            <asp:DropDownList runat="server" CssClass="form-control select" Width="100%" ID="ddlNguonLaoDong" ClientIDMode="Static"></asp:DropDownList>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Link Facebook/Ngoại hình</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <asp:TextBox ID="txtFaceBook" ClientIDMode="Static" Width="100%" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                        <div class="div_col">
                                            <asp:DropDownList ID="ddlFigure" ClientIDMode="Static" CssClass="form-control select" Width="100%" runat="server">
                                                <asp:ListItem Text="Chọn điểm ngoại hình" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="1 Điểm" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="2 Điểm" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="3 Điểm" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="4 Điểm" Value="4"></asp:ListItem>
                                                <asp:ListItem Text="5 Điểm" Value="5"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Test văn hóa</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <asp:TextBox ID="txtPoint" ClientIDMode="Static" Width="49%" CssClass="form-control" runat="server" Style="margin-right: 10px" placeholder="Số Điểm"></asp:TextBox>
                                            <asp:TextBox ID="txtTotalPoint" ClientIDMode="Static" Width="48%" CssClass="form-control" runat="server" placeholder="Tổng Điểm"></asp:TextBox>
                                        </div>
                                        <div class="div_col">
                                            <asp:DropDownList ID="ddlTestResult" ClientIDMode="Static" CssClass="form-control select" Width="60%" runat="server">

                                                <asp:ListItem Text="Đạt" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Không đạt" Value="0"></asp:ListItem>

                                            </asp:DropDownList>
                                            <asp:CheckBox runat="server" ID="cblTestAgain" ClientIDMode="Static" Width="10%" TextAlign="Right" /><u style="line-height: 32px">Check lần 2</u>
                                        </div>

                                    </div>

                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Giới tính/Người giới thiệu</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <div class="gender">
                                                <a id="male" class="cls_gender <%=clsgender1 %>" data-id="1" onclick="clickGender($(this))">Nam</a>
                                                <a id="female" class="cls_gender  <%=clsgender2 %>" data-id="2" onclick="clickGender($(this))">Nữ</a>
                                            </div>
                                        </div>
                                        <div class="div_col">
                                            <asp:DropDownList runat="server" CssClass="form-control select" Width="50%" ID="ddlNguoiGioiThieu" ClientIDMode="Static"></asp:DropDownList>
                                            <div class="div_span">
                                                <asp:DropDownList runat="server" CssClass="form-control select" Width="49%" ID="ddlRelation" ClientIDMode="Static"></asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr class="tr-upload">
                                <td class="col-xs-3 left"><span>Ảnh CMT</span></td>
                                <td class="col-xs-7 right">
                                    <div class="wrap btn-upload-wp HDF_IMG_CMT1" style="margin-bottom: 5px; width: auto;">
                                        <div id="Btn_UploadImg" class="btn-upload-img" onclick="popupImageIframe('HDF_IMG_CMT1')">Ảnh CMT 1</div>
                                        <asp:FileUpload ID="UploadFile" ClientIDMode="Static" Style="display: none;"
                                            CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                        <div class="wrap listing-img-upload">
                                            <% if (OBJ != null && OBJ.CMTimg1 != null && OBJ.CMTimg1 != "")
                                                { %>
                                            <div class="thumb-wp">
                                                <img class="thumb" alt="" title="" src="<%=OBJ.CMTimg1 %>"
                                                    data-img="<%=OBJ.CMTimg1 %>" />
                                                <span class="delete-thumb" onclick="deleteThumbnail($(this), 'HDF_IMG_CMT1')"></span>
                                            </div>
                                            <% } %>
                                        </div>
                                    </div>

                                    <div class="wrap btn-upload-wp HDF_IMG_CMT2" style="margin-bottom: 5px; margin-left: 10px; width: auto;">
                                        <div id="Btn_UploadImg2" class="btn-upload-img" onclick="popupImageIframe('HDF_IMG_CMT2')">Ảnh CMT 2</div>
                                        <asp:FileUpload ID="FileUpload2" ClientIDMode="Static" Style="display: none;"
                                            CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                        <div class="wrap listing-img-upload">
                                            <% if (OBJ != null && OBJ.CMTimg2 != null && OBJ.CMTimg2 != "")
                                                { %>
                                            <div class="thumb-wp">
                                                <img class="thumb" alt="" title="" src="<%=OBJ.CMTimg2 %>"
                                                    data-img="<%=OBJ.CMTimg2 %>" />
                                                <span class="delete-thumb" onclick="deleteThumbnail($(this), 'HDF_IMG_CMT2')"></span>
                                            </div>
                                            <% } %>
                                        </div>
                                    </div>

                                    <div class="wrap btn-upload-wp HDF_IMG_MAIN" style="margin-bottom: 5px; width: auto; margin-left: 10px;">
                                        <div id="Btn_UploadImg3" class="btn-upload-img" onclick="popupImageIframe('HDF_IMG_MAIN')">Ảnh chân dung</div>
                                        <asp:FileUpload ID="FileUpload1" ClientIDMode="Static" Style="display: none;"
                                            CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                        <div class="wrap listing-img-upload">
                                            <% if (OBJ != null && OBJ.MainImg != null && OBJ.MainImg != "")
                                                { %>
                                            <div class="thumb-wp">
                                                <img class="thumb" alt="" title="" src="<%=OBJ.MainImg %>"
                                                    data-img="<%=OBJ.MainImg %>" />
                                                <span class="delete-thumb" onclick="deleteThumbnail($(this), 'HDF_IMG_MAIN')"></span>
                                            </div>
                                            <% } %>
                                        </div>
                                    </div>

                                    <div class="wrap btn-upload-wp HDF_IMG_MAIN_2" style="margin-bottom: 5px; width: auto; margin-left: 10px;">
                                        <div id="Btn_UploadImg8" class="btn-upload-img" onclick="popupImageIframe('HDF_IMG_MAIN_2')">Ảnh toàn thân</div>
                                        <asp:FileUpload ID="FileUpload8" ClientIDMode="Static" Style="display: none;"
                                            CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                        <div class="wrap listing-img-upload">
                                            <% if (OBJ != null && OBJ.MainImg2 != null && OBJ.MainImg2 != "")
                                                { %>
                                            <div class="thumb-wp">
                                                <img class="thumb" alt="" title="" src="<%=OBJ.MainImg2 %>"
                                                    data-img="<%=OBJ.MainImg2 %>" />
                                                <span class="delete-thumb" onclick="deleteThumbnail($(this), 'HDF_IMG_MAIN')"></span>
                                            </div>
                                            <% } %>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr class="tr-link">
                                <td class="col-xs-2 left"><span>Link test gội </span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="txtVideoLink" runat="server" ClientIDMode="Static" placeholder="Link ..."></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-description">
                                <td class="col-xs-2 left"><span>Địa chỉ thường trú/Ghi chú</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox TextMode="MultiLine" Rows="2" Style="width: 49%; margin-right: 10px;" ID="Address" runat="server" ClientIDMode="Static"></asp:TextBox>
                                        <asp:TextBox TextMode="MultiLine" Rows="2" Style="width: 49%; margin-top: 10px" ID="Step1Note" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-send">
                                <td class="col-xs-3 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <asp:Button ID="Send" CssClass="btn-send" runat="server" Text="Nhận hồ sơ" ClientIDMode="Static" OnClientClick="if(!DoValidation()) return false;" OnClick="ExcAddOrUpdate"></asp:Button>

                                        <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>
                                    </span>
                                    <span class="field-wp" style="margin-left: 20px;">
                                        <asp:Button ID="Cancel" CssClass="btn-send" runat="server" Text="Không nhận" ClientIDMode="Static" OnClientClick="if(!CancelCV()) return false;" OnClick="CancelCV"></asp:Button>
                                    </span>
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="HDF_Images" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="HDF_IMG_CMT1" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="HDF_IMG_CMT2" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="HDF_IMG_MAIN" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="HDF_IMG_MAIN_2" ClientIDMode="Static" />
                        </tbody>
                    </table>
                </div>
            </div>
            <%-- end Add --%>
        </div>

        <asp:HiddenField ID="HDF_Gender" ClientIDMode="Static" runat="server" Value="1" />
        <script type="text/javascript">
            $(".tr-link").hide();
            $('select').select2();
            /// Click set hDF GioiTinh
            function clickGender(This) {
                if (This.hasClass("active")) {
                }
                else {
                    $(".cls_gender").removeClass("active");
                    This.addClass("active");
                    var IdGender = $(".cls_gender.active ").attr("data-id");
                    $("#HDF_Gender").val(IdGender);
                }
            }
            function ValidationS4MTextbox() {
                var valid = true;
                //debugger;
                var errorString = 'Vui lòng nhập ';
                if ($("#txtS4M_Id").val() == null || $("#txtS4M_Id").val() == '') {
                    valid = false;
                    errorString += " học viên Id";
                    $("#txtS4M_Id").css("border-color", "pink");
                }
                if (!valid) {
                    $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                        $("#success-alert").slideUp(2000);
                    });
                    $("#msg-alert").text(errorString);
                    $("#txtS4M_Id").focus();
                }
                return valid;
            }
            /// Click set hDF GioiTinh
            function clickStatusHS(This) {
                if (This.hasClass("active")) {
                }
                else {

                    $(".cls_hoso ").removeClass("active");
                    This.addClass("active");
                    var IdStatus = $(".cls_hoso.active ").attr("data-id");
                    //console.log(IdStatus);
                    $("#HDF_Status").val(IdStatus);
                }
            }
        </script>
        <script>
            jQuery(document).ready(function () {
                $("#success-alert").hide();
                $("#glbTuyendungV2").addClass("active");
                $("#glbCreatCV").addClass("active");
                $("#glbCreateNewCV").addClass("active");
                //date picker
                $('.txtdatetime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        //$input.val($input.val().split(' ')[0]);
                    }
                });
                //excThumbWidth(120, 120);

                choseDepartment(null);
            });

            jQuery(document).ready(function () {
                
                $("#Phone").keypress(function (e) {
                    if (String.fromCharCode(e.keyCode).match(/[^0-9]/g)) {
                        $("#success-alert").fadeTo(2000, 800).slideUp(800, function () {
                            $("#success-alert").slideUp(800);
                        });
                        $("#msg-alert").text("Chỉ được nhập số vào trường này");
                        $(this).css("border-color", "pink");
                        return false;
                        $(this).focus();
                    }
                    else {
                        $(this).css("border-color", "transparent");
                    }
                });

                $("#CMT").keypress(function (e) {
                    if (String.fromCharCode(e.keyCode).match(/[^0-9]/g)) {
                        $("#success-alert").fadeTo(2000, 800).slideUp(800, function () {
                            $("#success-alert").slideUp(800);
                        });
                        $("#msg-alert").text("Chỉ được nhập số vào trường này");
                        $(this).css("border-color", "pink");
                        return false;
                        $(this).focus();
                    }
                    else {
                        $(this).css("border-color", "transparent");
                    }
                });
                excThumbWidth(120, 120);

                // Mặc định xử lý hiển thị phần bộ phận
                choseDepartment($("#ddlDepartment"));
            });

            function choseDepartment(This) {
                try {
                    var id = parseInt(This.val());
                    if (!isNaN(id)) {
                        if (id == 2) {
                            $(".tr-link").show();
                        }
                        else {
                            $(".tr-link").hide();
                        }
                    }
                    else {
                        $(".tr-skill").removeClass('tr-show');
                    }
                }
                catch (err) { }
            }

            function CancelCV() {
                // Do calls to validation services here
                var valid = true;
                //debugger;
                var errorString = 'Vui lòng nhập đầy đủ';
                var salon = $("#ddlSalon").val();
                var fullname = $("#FullName").val();
                var cmt = $("#CMT").val();
                var issuedCMT = $("#IssuedByCMT").val();
                var dateCMT = $("#DateByCMT").val();
                var phone = $("#Phone").val();
                var departmentId = $("#ddlDepartment").val();
                if (fullname == "") {
                    valid = false;
                    errorString += " [Họ Tên]";
                    $("#FullName").css("border-color", "pink");
                }
                if (phone == "") {
                    valid = false;
                    errorString += " [Số Điện Thoại]";
                    $("#Phone").css("border-color", "pink");
                }
                if (cmt == "") {
                    valid = false;
                    errorString += " [Chứng minh thư]";
                    $("#CMT").css("border-color", "pink");
                }
                if (departmentId == 0) {
                    valid = false;
                    errorString += " [Chọn bộ phận]";
                    $("#ddlDepartment").css("border-color", "pink");
                }
                if (salon == "" || salon == 0) {
                    valid = false;
                    errorString += " [Chọn salon]";
                    $("#ddlSalon").css("border-color", "pink");
                }
                if (cmt == "") {
                    valid = false;
                    errorString += " [Chứng minh thư]";
                    $("#CMT").css("border-color", "pink");
                }
                if (issuedCMT == "") {
                    valid = false;
                    errorString += " [Nơi cấp CMT]";
                    $("#IssuedByCMT").css("border-color", "pink");
                }
                if (dateCMT == "") {
                    valid = false;
                    errorString += " [Ngày cấp CMT]";
                    $("#DateByCMT").css("border-color", "pink");
                }
                if (!valid) {
                    $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                        $("#success-alert").slideUp(2000);
                    });
                    $("#msg-alert").text(errorString);

                }
                else {
                    $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                        $("#success-alert").slideUp(2000);
                    });
                    $("#msg-alert").text("Hồ sơ ứng viên đã được lưu trữ");
                }
                return valid;
            }
            function DoValidation() {
                // Do calls to validation services here
                var valid = true;
                var errorString = 'Vui lòng nhập đầy đủ';
                var salon = $("#ddlSalon").val();
                var fullname = $("#FullName").val();
                var phone = $("#Phone").val();
                var cmt = $("#CMT").val();
                var departmentId = $("#ddlDepartment").val();
                var issuedCMT = $("#IssuedByCMT").val();
                var dateCMT = $("#DateByCMT").val();
                if (fullname == "") {
                    valid = false;
                    errorString += " [Họ Tên]";
                    $("#FullName").css("border-color", "pink");
                }
                if (phone == "") {
                    valid = false;
                    errorString += " [Số Điện Thoại]";
                    $("#Phone").css("border-color", "pink");
                }
                if (departmentId == 0) {
                    valid = false;
                    errorString += " [Chọn bộ phận]";
                    $("#ddlDepartment").css("border-color", "pink");
                }
                if (cmt == "") {
                    valid = false;
                    errorString += " [Chứng minh thư]";
                    $("#CMT").css("border-color", "pink");
                }
                if (salon == "" || salon == 0) {
                    valid = false;
                    errorString += " [Chọn salon]";
                    $("#ddlSalon").css("border-color", "pink");
                }
                if (issuedCMT == "") {
                    valid = false;
                    errorString += " [Nơi cấp CMT]";
                    $("#IssuedByCMT").css("border-color", "pink");
                }
                if (dateCMT == "") {
                    valid = false;
                    errorString += " [Ngày cấp CMT]";
                    $("#DateByCMT").css("border-color", "pink");
                }
                if (!valid) {
                    $("#success-alert").fadeTo(2000, 1000).slideUp(1000, function () {
                        $("#success-alert").slideUp(1000);
                    });
                    $("#msg-alert").text(errorString);

                }
                else {
                    $("#success-alert").fadeTo(2000, 1000).slideUp(1000, function () {
                        $("#success-alert").slideUp(1000);
                    });
                    $("#msg-alert").text("Lưu thông tin ứng viên thành công");
                    //window.location("/admin/tuyen-dung/lap-ho-so/danh-sach-ho-so.html");
                }
                return valid;
            }
        </script>

        <!-- Popup plugin image -->
        <script type="text/javascript">
            function popupImageIframe(StoreImgField) {
                var Width = 960;
                var Height = 560;
                var ImgList = [];
                var iframe = $("<div class='iframe-image-upload' style='width:" + Width + "px; height:" + Height + "px; float:left; display:none;'><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + StoreImgField + "&callback=getThumbFromIframe&imgWidth=800&imgHeight=600&multi_img=false&Folder=Common' style='width:100%; height: 100%;'></iframe></div");

                $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
            }

            window.getThumbFromIframe = function (Imgs, StoreImgField) {
                var imgs = "";
                var thumb = "";
                if (Imgs.length > 0) {
                    thumb = '<div class="thumb-wp">' +
                        '<img class="thumb" alt="" title="" src="' + Imgs[0] + '"' +
                        'data-img="" />' +
                        '<span class="delete-thumb" onclick="deleteThumbnail($(this), \'' + StoreImgField + '\')"></span>' +
                        '</div>';
                    $("." + StoreImgField).find(".listing-img-upload").empty().append(thumb);
                    excThumbWidth(120, 120);
                    $("#" + StoreImgField).val(Imgs[0]);
                }
                autoCloseEBPopup();
            }

            function deleteThumbnail(This, StoreImgField) {
                var imgs = "";
                var lst = This.parent().parent();
                This.parent().remove();
                lst.find("img.thumb").each(function () {
                    imgs += $(this).attr("src") + ",";
                });
                $("#" + StoreImgField).val(imgs);
            }
        </script>
        <!-- Popup plugin image -->

    </asp:Panel>
    <style>
        .field-wp .div_col {
            width: 49%;
            float: left;
        }

            .field-wp .div_col:first-child {
                margin-right: 10px;
            }

        .customer-add .table-add tr.tr-field-ahalf .div_col input, .customer-add .table-add tr.tr-field-ahalf .div_col textarea, .customer-add .table-add tr.tr-field-ahalf .div_col select {
            width: 100%;
        }

        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: inherit;
        }

        .span_note {
            color: red;
            width: inherit;
        }

        .cls_gender {
            width: 120px;
            float: left;
            height: 36px;
            background: #dfdfdf;
            margin-left: 10px;
            text-align: center;
            line-height: 36px;
            cursor: pointer;
        }

            .cls_gender:first-child {
                margin-left: 0px;
            }

            .cls_gender.active {
                background: #fcd72a;
            }

        .cls_hoso {
            width: 120px;
            float: left;
            height: 36px;
            background: #dfdfdf;
            margin-left: 10px;
            text-align: center;
            line-height: 36px;
            cursor: pointer;
        }

            .cls_hoso:first-child {
                margin-left: 0px;
            }

            .cls_hoso.active {
                background: #fcd72a;
            }
    </style>

</asp:Content>
