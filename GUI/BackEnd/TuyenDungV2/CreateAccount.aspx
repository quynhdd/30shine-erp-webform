﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TemplateMaster/SiteMaster.Master" CodeBehind="CreateAccount.aspx.cs" Inherits="_30shine.GUI.BackEnd.TuyenDungV2.CreateAccount" %>

<asp:Content ID="StaffAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <style>
            .staff-servey-mark span {
                height: 28px !important;
                line-height: 28px !important;
            }

            .staff-servey-mark td input[type="radio"] {
                top: 4px !important;
            }
        </style>
        <link href="/Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="/Assets/js/select2/select2.min.js"></script>
        <link href="/Assets/js/fancybox/jquery.fancybox.min.css" rel="stylesheet" />
        <script src="/Assets/js/fancybox/jquery.fancybox.min.js"></script>
        <style>
            #radioApprove tr {
                display: inline-table;
                padding-right: 15px;
                cursor: pointer;
            }

                #radioApprove tr td input {
                    margin-left: 15px;
                    cursor: pointer;
                }

                #radioApprove tr td label {
                    margin-left: 5px;
                    position: relative;
                    top: -1px;
                    cursor: pointer;
                }

            .listing-img-upload .thumb-wp {
                border: none;
            }
            /*.listing-img-upload .thumb-wp img { position: relative; width: auto !important; height: auto !important; max-width: 100%; }*/
            .ddl {
                width: 100%;
            }

            .table-wp {
                background: #f5f5f5;
                float: left;
                width: 100%;
                padding: 15px;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                -webkit-box-shadow: 0px 0px 3px 0px #ddd;
                -moz-box-shadow: 0px 0px 3px 0px #ddd;
                box-shadow: 0px 0px 3px 0px #ddd;
            }

            .btn {
                border-radius: 5px;
                margin-left: 10px;
                margin-right: 10px;
                height: 70px;
            }

                .btn:hover {
                    background-color: orangered;
                    color: white;
                }

            .send {
                box-shadow: 2px 2px #b0b0b0;
                float: left;
            }

            .senddd {
                box-shadow: 2px 2px #b0b0b0;
                float: left;
                width: 100px;
                height: 50px;
            }

                .senddd:hover {
                    background-color: #2b8a05;
                    color: white;
                }

            .cancel {
                border-radius: 5px;
            }

            .canceldd {
                float: right;
            }

            .cancel:hover {
                background-color: #50b347;
                color: white;
            }

            .table-wp {
                background: #f5f5f5;
                float: left;
                width: 100%;
                padding: 15px;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                -webkit-box-shadow: 0px 0px 3px 0px #ddd;
                -moz-box-shadow: 0px 0px 3px 0px #ddd;
                box-shadow: 0px 0px 3px 0px #ddd;
            }

            .tr-skill {
                display: none;
            }

            .tr-show {
                display: table-row;
            }

            .customer-add .table-add tr td.right .fb-cover-error {
                top: 0;
                right: auto;
            }

            .gallery {
                display: inline-block;
                margin-top: 20px;
            }

            .thumbnailImage {
                margin-bottom: 6px;
                width: 100%;
            }

            .modal-image {
                display: none;
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */ /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
            }

            /* Modal Content (image) */
            .modal-content-image {
                margin: auto;
                margin-top: 30px;
                display: block;
                width: 50%;
                max-width: 500px;
                height: auto;
                max-height: 600px;
            }

            /* Caption of Modal Image */
            #caption-image {
                margin: auto;
                display: block;
                width: 60%;
                max-width: 700px;
                text-align: center;
                color: #ccc;
                padding: 10px 0;
                height: 150px;
                margin-top: 10px;
            }

            /* Add Animation */
            .modal-content-image, #caption-image {
                -webkit-animation-name: zoom;
                -webkit-animation-duration: 0.6s;
                animation-name: zoom;
                animation-duration: 0.6s;
            }

            @-webkit-keyframes zoom {
                from {
                    -webkit-transform: scale(0);
                }

                to {
                    -webkit-transform: scale(1);
                }
            }

            .btn-send {
                /*padding:0px 30px;*/
                background: black !important;
                color: white;
            }

                .btn-send:hover {
                    color: yellow !important;
                }

            @keyframes zoom {
                from {
                    transform: scale(0.1);
                }

                to {
                    transform: scale(1);
                }
            }

            /* The Close Button */
            .close-image {
                position: absolute;
                top: 15px;
                right: 35px;
                color: #f1f1f1;
                font-size: 40px;
                font-weight: bold;
                transition: 0.3s;
            }

                .close-image:hover,
                .close-image:focus {
                    color: #bbb;
                    text-decoration: none;
                    cursor: pointer;
                }

            .btn-send:hover {
                background: #50b347;
                color: #ffffff;
            }

            .btn-send {
                width: auto !important;
                height: 30px;
                line-height: 30px;
                float: left;
                margin-right: 20px;
                margin-top: 20px;
                background: #ddd;
                text-align: center;
                cursor: pointer;
                border-radius: 2px;
            }

            .one {
                background: #ddd;
                border-radius: 5px;
                width: auto;
                padding-top: 5px;
                padding-bottom: 5px;
                height: 30px;
                line-height: 30px;
            }

                .one:hover {
                    background: #50b347;
                    color: #ffffff;
                }
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý Nhân sự &nbsp;&#187; </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <!-- System Message -->
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <!-- END System Message -->

                <div class="table-wp">
                    <table class="table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Thông tin nhân viên</strong></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="myModalimage" style="z-index: 2000;" class="modal modal-image">
                                        <img class="modal-content-image" id="img01">
                                        <div id="captionimage"></div>
                                    </div>
                                </td>
                            </tr>
                            <tr id="TrSalon" runat="server">
                                <td class="col-xs-2 left"><span>Salon</span></td>
                                <td class="col-xs-3 right"><span class="field-wp">
                                    <asp:DropDownList ID="ddlSalon" runat="server" ClientIDMode="Static" Enabled="false" Style="width: 250px;">
                                    </asp:DropDownList>
                                    <% if (OBJ != null && OBJ.MainImg != null && OBJ.MainImg != "")
                                        { %>
                                    <div class="gallery" style="float: left; cursor: pointer; z-index: 200; position: absolute; top: -19px; width: auto; padding-left: 20px;">

                                        <img class="example-image form-control" id="myImg1" alt="" title="" src="<%=OBJ.MainImg %>"
                                            data-img="<%=OBJ.MainImg %>" style="width: 300px; height: auto; max-height: 265px; left: 0px; max-width: 100%;" />
                                        <%--<% if (OBJ != null && OBJ.MainImg != null && OBJ.MainImg != "")
                                        { %>
                                    <div class="gallery" style="float: left; cursor: pointer; z-index: 200; position: absolute; top: -19px; width: auto; padding-left: 20px;">

                                        <img class="example-image form-control" id="myImg1" alt="" title="" src="<%=OBJ.MainImg %>"
                                            data-img="<%=OBJ.MainImg %>" style="width: 300px; height: auto; max-height: 265px; left: 0px; max-width: 100%;" />
                                    </div> --%>
                                        <% } %></td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Phân quyền</span></td>
                                <td class="col-xs-3 right">
                                    <span class="field-wp">
                                        <asp:DropDownList ID="PermissionList" runat="server" Enabled="false" ClientIDMode="Static" Style="width: 250px;"></asp:DropDownList>

                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-2 left"><span>Bộ phận</span></td>
                                <td class="col-xs-3 right">
                                    <span class="field-wp">
                                        <asp:DropDownList ID="TypeStaff" Enabled="false" runat="server" ClientIDMode="Static" Style="width: 250px;"></asp:DropDownList>

                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Họ và tên</span></td>
                                <td class="col-xs-3 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="FullName" runat="server" ClientIDMode="Static" Style="width: 250px;"></asp:TextBox>
                                    </span>

                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Số CMND</span></td>
                                <td class="col-xs-3 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="StaffID" runat="server" ClientIDMode="Static" Style="width: 250px;"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="Phone" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập số điện thoại!"></asp:RequiredFieldValidator>--%>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-2 left"><span>Ngày bắt đầu</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="DateJoin" CssClass="txtDateTime" runat="server" ClientIDMode="Static" Style="width: 250px;"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Ngày cấp/Nơi cấp CMND</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="ProviderDateCMT"  CssClass="txtDateTime" runat="server" ClientIDMode="Static" Style="width: 49%;margin-right:16px;"></asp:TextBox>
                                        <asp:TextBox ID="ProviderLocaleCMT" runat="server" ClientIDMode="Static" Style="width: 49%;"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Số điện thoại</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="Phone" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-2 left"><span>E-mail</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="Email" runat="server" ClientIDMode="Static" placeholder="example@gmail.com"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>

                            <tr style="height: 90px;">
                                <td class="col-xs-2 left"><span>Tên đăng nhập</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="UserAccount" runat="server" ClientIDMode="Static" placeholder="example"></asp:TextBox>
                                        <asp:RequiredFieldValidator runat="server" id="RequiredFieldUserAccount" ControlToValidate="UserAccount" ErrorMessage="<i style='color:red'>Vui lòng nhập Tên đăng nhập!!!</i>" />
                                    </span> 
                                </td>
                            </tr>

                            <tr class="tr-birthday">
                                <td class="col-xs-2 left"><span>Sinh nhật</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="Day" runat="server" ClientIDMode="Static" placeholder="Ngày"></asp:TextBox>
                                        <asp:TextBox ID="Month" runat="server" ClientIDMode="Static" placeholder="Tháng"></asp:TextBox>
                                        <asp:TextBox ID="Year" runat="server" ClientIDMode="Static" placeholder="Năm"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-2 left"><span>Giới tính</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:DropDownList ID="Gender" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-2 left"><span>Địa chỉ</span></td>
                                <td class="col-xs-9 right">

                                    <div class="city">
                                        <asp:DropDownList ID="ddlCity" runat="server" CssClass="form-control select" ClientIDMode="Static">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvCity" runat="server" ControlToValidate="ddlCity"
                                            InitialValue="0" ErrorMessage="<i style='color:red'>Vui lòng chọn tỉnh thành!!!</i>" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    </div>

                                    <%--<asp:TextBox ID="Address" runat="server" placeholder="Địa chỉ" ClientIDMode="Static"></asp:TextBox>--%>
                                </td>
                            </tr>

                            <tr class="tr-upload">
                                <td class="col-xs-3 left"><span>Ảnh avatar</span></td>
                                <td class="col-xs-7 right">
                                    <div class="wrap btn-upload-wp HDF_MainImg" style="margin-bottom: 5px;">
                                        <div id="Btn_UploadImg" class="btn-upload-img" onclick="popupImageIframe('HDF_MainImg')">Chọn ảnh đăng</div>
                                        <asp:FileUpload ID="UploadFile" ClientIDMode="Static" Style="display: none;"
                                            CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                        <div class="wrap listing-img-upload">
                                            <% if (HasImages)
                                                { %>
                                            <% for (var i = 0; i < ListImagesUrl.Count; i++)
                                                { %>
                                            <div class="thumb-wp">
                                                <img class="thumb" alt="" title="" src="<%=ListImagesUrl[i] %>"
                                                    data-img="<%=ListImagesUrl[i] %>" />
                                                <span class="delete-thumb" onclick="deleteThumbnail($(this), 'HDF_MainImg')"></span>
                                            </div>
                                            <% } %>
                                            <% } %>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf" style="height: 65px;">
                                <td class="col-xs-2 left"><span>Nguồn lao động</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp" id="everything">
                                        <asp:DropDownList runat="server" CssClass="form-control select" Width="100%" ID="ddlNguonLaoDong" ClientIDMode="Static"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvType" runat="server" ControlToValidate="ddlNguonLaoDong"
                                            InitialValue="0" ErrorMessage="<i style='color:red'>Vui lòng chọn nguồn lao động!!!</i>" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-address">
                                <td class="col-xs-2 left"><span>Địa chỉ thường trú</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox TextMode="MultiLine" Rows="2" Style="width: 100%;" ID="Address" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-send">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <asp:Button ID="Send" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="AddStaff"></asp:Button>

                                        <asp:Button ID="btnCancel" CssClass="btn-send" runat="server" Style="margin-left: 20px !important" Text="Không nhận" CausesValidation="false" ClientIDMode="Static" OnClick="CancelCV"></asp:Button>
                                        <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>
                                    </span>
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
                            <asp:HiddenField ID="HDF_MainImg" runat="server" ClientIDMode="Static" />

                        </tbody>
                    </table>
                </div>
            </div>
            <%-- end Add --%>
        </div>

        <script>
            jQuery(document).ready(function () {
                $("#glbAdminContent").addClass("active");
                $("#glbAdminStaff").addClass("active");

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        //$input.val($input.val().split(' ')[0]);
                    },
                    scrollMonth: false,
                    scrollTime: false,
                    scrollInput: false
                });
            });
            //show image
            var modal = document.getElementById('myModalimage');
            // Get the image and insert it inside the modal - use its "alt" text as a caption
            var img = document.getElementById('myImg1');
            var img1 = document.getElementById('myImg2');
            var img2 = document.getElementById('myImg3');
            var img3 = document.getElementById('myImg4');
            var modalImg = document.getElementById("img01");
            var captionText = document.getElementById("captionimage");

            try {
                img.onclick = function () {

                    modal.style.display = "block";
                    modalImg.src = this.src;
                    captionText.innerHTML = this.alt;
                }
                img1.onclick = function () {
                    modal.style.display = "block";
                    modalImg.src = this.src;
                    captionText.innerHTML = this.alt;
                }
                img2.onclick = function () {
                    modal.style.display = "block";
                    modalImg.src = this.src;
                    captionText.innerHTML = this.alt;
                }
                img3.onclick = function () {
                    modal.style.display = "block";
                    modalImg.src = this.src;
                    captionText.innerHTML = this.alt;
                }
                // When the user clicks on <span> (x), close the modal
                modal.onclick = function () {
                    console.log("a");
                    modal.style.display = "none";
                }
            } catch (err) { }


            $('#myModalimage').click(function () {
                $("#myModalimage").css("display", "none")
            });
            $('.modal-content-image').click(function (event) {
                event.stopPropagation();
            });


        </script>
        <!-- Popup plugin image hay là chưa gọi hàm getIdLevel nên nó k chạy  -->
        <script type="text/javascript">
            function popupImageIframe1(StoreImgField) {
                var Width = 960;
                var Height = 560;
                var ImgList = [];
                var iframe = $("<div class='iframe-image-upload' style='width:" + Width + "px; height:" + Height + "px; float:left; display:none;'><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + StoreImgField + "&callback=getThumbFromIframe&imgWidth=800&imgHeight=600&multi_img=false&Folder=Common' style='width:100%; height: 100%;'></iframe></div");

                $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
            }
            function popupImageIframe(StoreImgField) {
                var Width = 960;
                var Height = 560;
                var ImgList = [];
                var iframe = $("<div class='iframe-image-upload' style='width:" + Width + "px; height:" + Height + "px; float:left; display:none;'><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + StoreImgField + "&callback=getThumbFromIframe&imgWidth=800&imgHeight=600&multi_img=false&Folder=Common' style='width:100%; height: 100%;'></iframe></div");

                $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
            }

            window.getThumbFromIframe = function (Imgs, StoreImgField) {
                var imgs = "";
                var thumb = "";
                if (Imgs.length > 0) {
                    thumb = '<div class="thumb-wp">' +
                        '<img class="thumb" alt="" title="" src="' + Imgs[0] + '"' +
                        'data-img="" />' +
                        '<span class="delete-thumb" onclick="deleteThumbnail($(this), \'' + StoreImgField + '\')"></span>' +
                        '</div>';
                    $("." + StoreImgField).find(".listing-img-upload").empty().append(thumb);
                    //excThumbWidth(120, 120);
                    $("#" + StoreImgField).val(Imgs[0]);
                }
                autoCloseEBPopup();
            }

            function deleteThumbnail(This, StoreImgField) {
                var imgs = "";
                var lst = This.parent().parent();
                This.parent().remove();
                lst.find("img.thumb").each(function () {
                    imgs += $(this).attr("src") + ",";
                });
                $("#" + StoreImgField).val(imgs);
            }

        </script>
        <!-- Popup plugin image -->

    </asp:Panel>
</asp:Content>
