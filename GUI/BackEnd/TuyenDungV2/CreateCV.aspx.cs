﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Script.Serialization;
using System.Globalization;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using System.Web.Services;

namespace _30shine.GUI.BackEnd.TuyenDungV2
{
    public partial class CreateCV : System.Web.UI.Page
    {
        private string PageID = "";
        protected TuyenDung_UngVien OBJ;
        protected string _Code;
        protected int orderId;
        DateTime datetime;
        protected bool _IsUpdate = false;
        public UIHelpers _UIHelpers = new UIHelpers();
        protected cls_media Img = new cls_media();
        protected List<cls_media> listImg = new List<cls_media>();
        Solution_30shineEntities db = new Solution_30shineEntities();
        private bool Perm_Access = false;
        private int integer;
        protected string clsgender1 = "";
        protected string clsgender2 = "";
        protected string clsStatus1 = "";
        protected string clsStatus2 = "";
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                bindDepartment();
                Bind_Nguon();
                Bind_Staff();
                BindRelation();
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_Access);
                Bind_TinhThanh();
                GetDataFromOrder();
                if (IsUpdate())
                {
                    Bind_OBJ();
                }
                else
                {
                    clsgender1 = "active";
                    clsStatus1 = "active";
                }

            }
        }

        protected void ExcAddOrUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
            {
                Update();
            }
            else
            {
                Add();
            }
        }
        protected void GetdataS4M(object sender, EventArgs e)
        {
            var staffId = Convert.ToInt32(txtS4M_Id.Text);
            //IStylist4MenStudentModel stylist4Men = new Stylist4MenStudentModel();
            var data = db.Stylist4Men_Student.FirstOrDefault(w=>w.Id == staffId);
            if (data != null)
            {
                OBJ = new TuyenDung_UngVien();
                FullName.Text = data.Fullname;
                Phone.Text = data.Phone;
                //ddlDepartment.SelectedValue = data.Type != null ? data.Type.ToString() : "0";
                ddlNguonLaoDong.SelectedValue = "2";
                ddlTinhThanh.SelectedValue = data.CityId != null ? data.CityId.ToString() : "0";
                if (data.SN_day != null && data.SN_month != null && data.SN_year != null)
                {
                    NgaySinh.Text = data.SN_day + "/" + data.SN_month + "/" + data.SN_year;
                }
                CMT.Text = data.NumberCMT;
                Address.Text = data.Address;
                Step1Note.Text = data.Note;
                if (data.ImageCMT1 != null)
                {
                    OBJ.CMTimg1 = data.ImageCMT1;
                    HDF_IMG_CMT1.Value = data.ImageCMT1;
                }
                if (data.ImageCMT2 != null)
                {
                    OBJ.CMTimg2 = data.ImageCMT2;
                    HDF_IMG_CMT2.Value = data.ImageCMT2;
                }
                
                
                
                
                if (data.Gender == 1)
                {
                    clsgender1 = "active";
                }
                else if (data.Gender == 2) clsgender2 = "active";
                else clsgender1 = "active";
            }
            else
            {
                var msg = "Không tìm thấy dữ liệu của học viên này";
                var status = "warning";
                UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
            }

        }
        protected void CancelCV(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var obj = new TuyenDung_UngVien();
                obj.FullName = FullName.Text;
                obj.Phone = Phone.Text;
                obj.CMT = CMT.Text;
                try
                {
                    obj.Point_Figure = Convert.ToInt32(ddlFigure.SelectedItem.Value);
                }
                catch
                {
                    obj.Point_Figure = 0;
                }

                try
                {
                    obj.Parse_VH = Convert.ToBoolean(Convert.ToInt32(ddlTestResult.SelectedValue));
                }
                catch
                {
                    obj.Parse_VH = false;
                }

                obj.UngVienStatusId = 2;
                if (txtPoint.Text != "" && txtTotalPoint.Text != "")
                {
                    obj.Test_VH = txtPoint.Text + "/" + txtTotalPoint.Text;
                }

                if (cblTestAgain.Checked)
                {
                    obj.TestAgain = true;
                }
                else
                {
                    obj.TestAgain = false;
                }
                if (HDF_IMG_CMT1.Value != "")
                {
                    obj.CMTimg1 = HDF_IMG_CMT1.Value;
                }
                if (HDF_IMG_CMT2.Value != "")
                {
                    obj.CMTimg2 = HDF_IMG_CMT2.Value;
                }
                if (HDF_IMG_MAIN.Value != "")
                {
                    obj.MainImg = HDF_IMG_MAIN.Value;
                }
                if (HDF_IMG_MAIN_2.Value != "")
                {
                    //obj.MainImg2 = HDF_IMG_MAIN_2.Value;
                }
                obj.LinkFaceBook = txtFaceBook.Text;
                obj.DepartmentId = int.TryParse(ddlDepartment.SelectedValue, out integer) ? integer : 0;
                try
                {
                    obj.NgaySinh = Convert.ToDateTime(NgaySinh.Text, new CultureInfo("vi-VN"));
                }
                catch (Exception ex)
                {
                    //
                }

                obj.salonId = int.Parse(ddlSalon.SelectedValue);
                obj.GioiTinhId = int.Parse(HDF_Gender.Value);
                obj.NguoiGioiThieuId = int.Parse(ddlNguoiGioiThieu.SelectedValue);
                obj.NguonTuyenDungId = int.Parse(ddlNguonLaoDong.SelectedValue);
                obj.Step2Note = Step1Note.Text;
                obj.Step1Time = DateTime.Now;
                obj.IsDelete = false;

                // Validate
                var Error = false;

                if (!Error)
                {
                    db.TuyenDung_UngVien.Add(obj);
                    var exc = db.SaveChanges();

                    if (exc > 0)
                    {

                        UIHelpers.Redirect("/admin/tuyen-dung/lap-ho-so/danh-sach-ho-so.html" + obj.Id);
                    }
                    else
                    {
                        var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
            }
        }

        private void Bind_Staff()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Staffs.OrderBy(p => p.Fullname).ToList();
                var item = new Staff();
                item.Fullname = "Chọn người giới thiệu";
                item.Id = 0;
                lst.Insert(0, item);
                ddlNguoiGioiThieu.DataTextField = "Fullname";
                ddlNguoiGioiThieu.DataValueField = "Id";
                ddlNguoiGioiThieu.SelectedIndex = 0;

                ddlNguoiGioiThieu.DataSource = lst;
                ddlNguoiGioiThieu.DataBind();
            }
        }
        private void Bind_Nguon()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.TuyenDung_Nguon.OrderBy(p => p.Name).ToList();
                var item = new TuyenDung_Nguon();
                item.Name = "Chọn nguồn lao động";
                item.Id = 0;
                lst.Insert(0, item);
                ddlNguonLaoDong.DataTextField = "Name";
                ddlNguonLaoDong.DataValueField = "Id";
                ddlNguonLaoDong.SelectedIndex = 0;

                ddlNguonLaoDong.DataSource = lst;
                ddlNguonLaoDong.DataBind();
            }
        }
        private void GetDataFromOrder()
        {
            if (Request.QueryString["orderId"] != null)
            {
                orderId = Convert.ToInt32(Request.QueryString["orderId"].ToString());
                var db = new Solution_30shineEntities();
                var order = db.OrderRecruitingStaffs.Where(w => w.Id == orderId).FirstOrDefault();
                ddlSalon.SelectedValue = order.SalonId.ToString();
                ddlDepartment.SelectedValue = order.DepartmentId.ToString();
                ddlTinhThanh.SelectedValue = order.RegionId.ToString();
            }
            

        }
        /// <summary>
        /// Bind tình trạng hôn nhân
        /// </summary>
        private void BindRelation()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Tbl_Status.Where(w => w.ParentId == 25).OrderBy(p => p.Name).ToList();
                var item = new Tbl_Status();
                item.Name = "Chọn tình trạng hôn nhân";
                item.Id = 0;
                lst.Insert(0, item);
                ddlRelation.DataTextField = "Name";
                ddlRelation.DataValueField = "Value";
                ddlRelation.SelectedIndex = 0;
                ddlRelation.DataSource = lst;
                ddlRelation.DataBind();
            }
        }
        private void Bind_TinhThanh()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.TinhThanhs.Where(a => a.TrangThai == 1).OrderBy(p => p.ThuTu).ToList();
                var item = new TinhThanh();
                item.TenTinhThanh = "Chọn Tỉnh Thành";
                item.ID = 0;
                lst.Insert(0, item);
                ddlTinhThanh.DataTextField = "TenTinhThanh";
                ddlTinhThanh.DataValueField = "ID";
                ddlTinhThanh.SelectedIndex = 0;

                ddlTinhThanh.DataSource = lst;
                ddlTinhThanh.DataBind();
            }
        }
        [WebMethod]
        public static string Bind_S4MStudent(int s4m_id)
        {
            IStylist4MenStudentModel stylist4Men = new Stylist4MenStudentModel();
            var student = stylist4Men.GetById(s4m_id);
            return new JavaScriptSerializer().Serialize(student);
            //IssuedByCMT.Text = student.
        }
        private void Add()
        {
            using (var db = new Solution_30shineEntities())
            {
                var obj = new TuyenDung_UngVien();
                obj.FullName = FullName.Text;
                obj.Phone = Phone.Text;
                obj.VideoLink = txtVideoLink.Text;
                obj.CMT = CMT.Text;
                try
                {
                    obj.TinhThanhId = Convert.ToInt32(ddlTinhThanh.SelectedValue);
                }
                catch
                {
                    obj.TinhThanhId = 0;
                }

                try
                {
                    obj.Parse_VH = Convert.ToBoolean(Convert.ToInt32(ddlTestResult.SelectedValue));
                }
                catch
                {
                    obj.Parse_VH = false;
                }

                if (txtPoint.Text != "" && txtTotalPoint.Text != "")
                {
                    obj.Test_VH = txtPoint.Text + "/" + txtTotalPoint.Text;
                }

                if (cblTestAgain.Checked)
                {
                    obj.TestAgain = true;
                }
                else
                {
                    obj.TestAgain = false;
                }
                if (ddlDepartment.SelectedValue == "1")
                {
                    obj.UngVienStatusId = 6;
                }
                else
                {
                    obj.UngVienStatusId = 1;
                }
                //obj.Point_Figure = Convert.ToInt32(ddlFigure.SelectedItem.Value);
                //obj.UngVienStatusId = 6;
                if (HDF_IMG_CMT1.Value != "")
                {
                    obj.CMTimg1 = HDF_IMG_CMT1.Value;
                }
                if (HDF_IMG_CMT2.Value != "")
                {
                    obj.CMTimg2 = HDF_IMG_CMT2.Value;
                }
                if (HDF_IMG_MAIN.Value != "")
                {
                    obj.MainImg = HDF_IMG_MAIN.Value;
                }
                if (HDF_IMG_MAIN_2.Value != "")
                {
                    //obj.MainImg2 = HDF_IMG_MAIN_2.Value;
                }
                obj.LinkFaceBook = txtFaceBook.Text;
                obj.DepartmentId = int.TryParse(ddlDepartment.SelectedValue, out integer) ? integer : 0;

                try
                {
                    obj.NgaySinh = Convert.ToDateTime(NgaySinh.Text, new CultureInfo("vi-VN"));
                }
                catch (Exception ex)
                {
                    //
                }
                obj.salonId = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                obj.GioiTinhId = int.TryParse(HDF_Gender.Value, out integer) ? integer : 0;
                obj.NguoiGioiThieuId = int.TryParse(ddlNguoiGioiThieu.SelectedValue, out integer) ? integer : 0;
                obj.NguonTuyenDungId = int.TryParse(ddlNguonLaoDong.SelectedValue, out integer) ? integer : 0;
                obj.StatusRelationId = int.TryParse(ddlRelation.SelectedValue, out integer) ? integer : 0;
                obj.ProvidedDate = Convert.ToDateTime(DateByCMT.Text, new CultureInfo("vi-VN"));
                obj.ProvidedLocale = IssuedByCMT.Text;
                obj.Address = Address.Text;
                obj.Step2Note = Step1Note.Text;
                obj.Step1Time = DateTime.Now;
                obj.IsDelete = false;
                obj.RecruimentId = Request.QueryString["orderId"] == null ? 0 : Convert.ToInt32(Request.QueryString["orderId"].ToString());
                // Validate
                var Error = false;

                if (!Error)
                {
                    db.TuyenDung_UngVien.Add(obj);
                    var exc = db.SaveChanges();

                    if (exc > 0)
                    {
                        //update OrderRecruitmentStaff
                        if (Request.QueryString["orderId"] != null)
                        {
                            var order = Convert.ToInt32(Request.QueryString["orderId"]);
                            var recruitment = db.OrderRecruitingStaffs.Where(w => w.Id == order).FirstOrDefault();
                            switch (recruitment.DepartmentId)
                            {
                                case 1:
                                //case 2:
                                    recruitment.Status = 2;
                                    break;
                                //case 2:
                                //    recruitment.Status = 4;
                                //    break;
                                default:
                                    recruitment.Status = 4;
                                    break;
                            }
                            recruitment.ModifiedDate = DateTime.Now;
                            recruitment.CandidateId = obj.Id;
                            db.OrderRecruitingStaffs.AddOrUpdate(recruitment);
                            db.SaveChanges();
                        }
                        if (ddlDepartment.SelectedValue == "1")
                        {
                            UIHelpers.Redirect("/admin/tuyen-dung/lap-ho-so/danh-sach-ho-so.html");
                        }
                        else
                        {
                            UIHelpers.Redirect("/admin/tuyen-dung/danh-sach-da-test-ky-nang.html");
                        }
                        //var MsgParam = new List<KeyValuePair<string, string>>();
                        //MsgParam.Add(new KeyValuePair<string, string>("msg_add_status", "success"));
                        //MsgParam.Add(new KeyValuePair<string, string>("msg_add_message", "Cập nhật thành công!"));

                    }
                    else
                    {
                        var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
            }
        }

        private void Update()
        {
            using (var db = new Solution_30shineEntities())
            {
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.TuyenDung_UngVien.FirstOrDefault(w => w.Id == Id);

                    if (!OBJ.Equals(null))
                    {
                        //OBJ.Point_Figure = Convert.ToInt32(ddlFigure.SelectedItem.Value);
                        OBJ.FullName = FullName.Text;
                        OBJ.Phone = Phone.Text;
                        OBJ.CMT = CMT.Text;
                        if (ddlDepartment.SelectedValue == "1")
                        {
                            OBJ.UngVienStatusId = 6;
                        }
                        else
                        {
                            OBJ.UngVienStatusId = 1;
                        }
                        OBJ.VideoLink = txtVideoLink.Text;
                        OBJ.TinhThanhId = Convert.ToInt32(ddlTinhThanh.SelectedValue);
                        OBJ.LinkFaceBook = txtFaceBook.Text;
                        OBJ.CMTimg1 = HDF_IMG_CMT1.Value;
                        OBJ.CMTimg2 = HDF_IMG_CMT2.Value;
                        OBJ.MainImg = HDF_IMG_MAIN.Value;
                        OBJ.MainImg = HDF_IMG_MAIN_2.Value;
                        OBJ.salonId = int.Parse(ddlSalon.SelectedValue);
                        OBJ.GioiTinhId = int.Parse(HDF_Gender.Value);
                        OBJ.DepartmentId = int.TryParse(ddlDepartment.SelectedValue, out integer) ? integer : 0;

                        OBJ.NgaySinh = DateTime.TryParse(NgaySinh.Text, out datetime) ? datetime : Convert.ToDateTime(null);
                        //OBJ.KyNangHoaChat = KyNangSuDungHoaChat.Text;
                        OBJ.Step2Note = Step1Note.Text;
                        OBJ.Step1ModifiedTime = DateTime.Now;

                        db.TuyenDung_UngVien.AddOrUpdate(OBJ);
                        var exc = db.SaveChanges();
                        if (exc > 0)
                        {
                            //var MsgParam = new List<KeyValuePair<string, string>>();
                            //MsgParam.Add(new KeyValuePair<string, string>("msg_add_status", "success"));
                            //MsgParam.Add(new KeyValuePair<string, string>("msg_add_message", "Cập nhật thành công!"));
                            UIHelpers.Redirect("/admin/tuyen-dung/lap-ho-so/danh-sach-ho-so.html");
                        }
                        else
                        {
                            var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                            var status = "warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                        }
                    }
                    else
                    {
                        var msg = "Lỗi! Không tìm thấy bản ghi.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);

                    }
                }
                else
                {
                    MsgSystem.Text = "Lỗi! Không tìm thấy bản ghi.";
                    MsgSystem.CssClass = "msg-system warning";
                }

                Context.RewritePath("+", "", "");
            }
        }
        private bool Bind_OBJ()
        {
            using (var db = new Solution_30shineEntities())
            {
                var serialize = new JavaScriptSerializer();
                var ExistOBJ = true;
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.TuyenDung_UngVien.FirstOrDefault(w => w.Id == Id);

                    if (!OBJ.Equals(null))
                    {

                        if (Convert.ToInt32(OBJ.UngVienStatusId) == 1)
                        {
                            clsStatus1 = "active";
                        }
                        if (Convert.ToInt32(OBJ.UngVienStatusId) == 2)
                        {
                            clsStatus2 = "active";
                        }
                        HDF_Gender.Value = OBJ.GioiTinhId.ToString();
                        if (Convert.ToInt32(OBJ.GioiTinhId) == 1)
                        {
                            clsgender1 = "active";
                        }
                        if (Convert.ToInt32(OBJ.GioiTinhId) == 2)
                        {
                            clsgender2 = "active";
                        }
                        FullName.Text = OBJ.FullName;
                        Phone.Text = OBJ.Phone;
                        CMT.Text = OBJ.CMT;
                        txtFaceBook.Text = OBJ.LinkFaceBook;
                        //VideoLink.Text = OBJ.VideoLink;
                        HDF_IMG_CMT1.Value = OBJ.CMTimg1;
                        HDF_IMG_CMT2.Value = OBJ.CMTimg2;
                        HDF_IMG_MAIN.Value = OBJ.MainImg;
                        //HDF_IMG_MAIN_2.Value = OBJ.MainImg2;
                        Step1Note.Text = OBJ.Step2Note;
                        NgaySinh.Text = OBJ.NgaySinh.ToString();
                        //ddlGoiTinh.SelectedValue = OBJ.GioiTinhId.ToString();
                        ddlSalon.SelectedValue = OBJ.salonId.ToString();
                        ddlTinhThanh.SelectedValue = OBJ.TinhThanhId.ToString();
                        //KyNangSuDungHoaChat.Text = OBJ.KyNangHoaChat;
                        ddlNguoiGioiThieu.SelectedValue = OBJ.NguoiGioiThieuId.ToString();
                        ddlNguonLaoDong.SelectedValue = OBJ.NguonTuyenDungId.ToString();
                        try
                        {
                            var itemSelected = ddlDepartment.Items.FindByValue(OBJ.DepartmentId.ToString());
                            if (itemSelected != null)
                            {
                                ddlDepartment.SelectedItem.Selected = false;
                                itemSelected.Selected = true;
                            }
                        }
                        catch { }
                    }
                    else
                    {
                        ExistOBJ = false;
                        var msg = "Lỗi! Bản ghi không tồn tại";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
                else
                {
                    ExistOBJ = false;
                    var msg = "Lỗi! Bản ghi không tồn tại";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }
                return ExistOBJ;
            }
        }
        private void bindDepartment()
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).ToList();
                var item = new Staff_Type();
                item.Name = "Chọn bộ phận";
                item.Id = 0;
                list.Insert(0, item);
                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "Id";
                ddlDepartment.DataSource = list;
                ddlDepartment.DataBind();
            }
        }
        public void ReloadPage()
        {
            HttpRequest Request = HttpContext.Current.Request;
            string host = "http://" + Request.Url.Host.ToString();
            string rawUrl = Request.RawUrl.ToString();
            string Url = host + rawUrl;
            Response.Redirect(Url);
        }
        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }
        public class cls_media
        {
            public string url { get; set; }
            public string title { get; set; }
            public string description { get; set; }
        }
    }
}