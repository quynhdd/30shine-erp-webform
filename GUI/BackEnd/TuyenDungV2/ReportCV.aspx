﻿<%@ Page Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="ReportCV.aspx.cs" Inherits="_30shine.GUI.BackEnd.TuyenDungV2.ReportCV" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <%--<uc1:ReportMenu runat="server" id="ReportMenu" />--%>

        <style>
            .table-listing .uv-avatar { width: 120px; }
            #load{
                background:url(/Assets/images/icon-keo.png) center no-repeat !important;

            }
            #load:hover{
                background:url(/Assets/images/icon-keo-hover.png) center no-repeat !important;

            }
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Tuyển dụng &nbsp;&#187; </li>
                        <li class="li-listing active"><a href="/admin/tuyen-dung/lap-ho-so/danh-sach-ho-so.html"> Danh sách ứng viên</a></li>
                        <li class="li-add active"><a href="/admin/tuyen-dung/lap-ho-so/tao-moi-ho-so.html">Thêm mới ứng viên</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report be-report-timekeeping">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
                <div class="row">
                    <div class="filter-item">
                        <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="time-wp">
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeFrom" placeholder="Từ ngày" Style="margin-left: 10px!important"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ValidateTime" ControlToValidate="TxtDateTimeFrom" runat="server" CssClass="fb-cover-error" Text="Vui lòng chọn thời gian!"></asp:RequiredFieldValidator>
                        </div>
                        <strong class="st-head" style="margin-left: 10px;">
                            <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                        </strong>
                        <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeTo" placeholder="Đến ngày" Style="margin-left: 10px!important"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                        <br />

                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlDepartment" runat="server" ClientIDMode="Static" Style="width: 220px;"></asp:DropDownList>
                    </div>
                    <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <a href="javascript:void(0);" onclick="location.href=location.href" class="st-head btn-viewdata">Reset Filter</a>

                </div>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>Danh sách ứng viên mới</strong>
                </div>
   
             
                        <div class="row table-wp">
                            <table class="table" id="tableNewStaff">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Ứng viên ID</th>
                                        <th>Họ tên</th>
                                        <th>Số điện thoại</th>
                                        <th>Bộ phận</th>
                                        <th>Trạng thái</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%                                        
                                        for (int i = 0; i < uv.Count; i++)
                                        {
                                            %>
                                        <tr>
                                            <td><%= i+1 %></td>
                                            <td><%= uv[i].Id %></td>
                                            <td><a href="/admin/tuyen-dung/lap-ho-so/chinh-sua-ho-so/<%= uv[i].Id %>.html" title="Chỉnh sửa hồ sơ ứng viên"><%= uv[i].Fullname %></a></td>
                                            <td><%= uv[i].Phone %></td>
                                            <td><%= uv[i].Department %></td>
                                            <td><%= uv[i].Status %></td>
                                            <td>
                                                <% if (uv[i].Status == "Chưa duyệt")
                                                    {
                                                    %>
		                                             <a class="btn btn-warning btn-xs confirmCV" href="/admin/tuyen-dung/tao-tai-khoan-nhan-vien/<%= uv[i].Id %>.html" ><i class="fa fa-check" aria-hidden="true"></i> &nbsp Duyệt hồ sơ</a>
	                                                <%    
                                                    }
                                                    else
                                                    {
                                                        %>
                                                    <a class="btn btn-warning btn-xs" href="/admin/tuyen-dung/danh-gia-ky-nang/<%= uv[i].Id %>.html"  title="Nhập kỹ năng"><i class="fa fa-check" aria-hidden="true"></i> &nbsp Duyệt kỹ năng</a>
                                                    <%} %>

                                            </td>
                                        </tr>
                                    <%
                                        }
                                         %>
                                </tbody>
                            </table>
                        </div>
                        
     
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <%--<asp:Button ID="BtnFakeExcel" ClientIDMode="Static" runat="server" OnClick="Exc_ExportExcel" style="display:none;" />--%>
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />

                <!-- Hidden Field-->
                <asp:HiddenField ID="HDF_TypeStaff" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_Staff" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_ExcelType" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_ExportType" ClientIDMode="Static" runat="server" />
                <!-- END Hidden Field-->
            </div>
            <%-- END Listing --%>
        </div>

        <link href="/Assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <script src="/Assets/js/jquery.mCustomScrollbar.js"></script>
        <script type="text/ecmascript">

            //$(document).on("click", ".confirmCV", function () {
            //    var uID = $(this).attr("uID");
            //    $.ajax({
            //        type: "POST",
            //        url: "/GUI/BackEnd/TuyenDungV2/ReportCV.aspx/updateStatus",
            //        data: '{uID : "' + $(this).attr("uID") + '"}',
            //        contentType: "application/json; charset=utf-8",
            //        dataType: "json",
            //        success: function () {
            //            window.location.replace("/admin/tuyen-dung/tao-tai-khoan-nhan-vien/" + uID + ".html");
            //        }
            //    })
            //})
            jQuery(document).ready(function () {
                
                $("#tableNewStaff").DataTable({
                    "bSort": false,
            
                    "language": {
                        "lengthMenu": " Bản ghi / trang _MENU_",
                        "zeroRecords": "Không tồn tại bản ghi",
                        "info": "Hiển thị trang _PAGE_ của _PAGES_",
                        "infoEmpty": "Không có dữ liệu",
                        "infoFiltered": "(Lọc kết quả từ _MAX_ tổng số bản ghi)",
                        "search": "Tìm kiếm",
                        "paginate": {
                            "first": "Đầu",
                            "last": "Cuối",
                            "next": "+",
                            "previous": "-"
                        },
                    },
                    "bLengthChange": false,
                    "bFilter":false,
                });
                //$('#ViewData').click();
                // Add active menu
                $("#glbTuyenDungV2").addClass("active");
                $("#glbCreatCV").addClass("active");
                $("#glbReportCV").addClass("active");
                $("li.be-report-li").addClass("active");

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });

                //============================
                // Bind Staff
                //============================
                BindStaffFilter();

                // View data yesterday
                $(".tag-date-today").click();

                // Xử lý kiểm tra biến filter
                var filter = localStorage.getItem("filterObject");
                var filterObject = {};
                console.log(filter);
                if (filter != null) {
                    try {
                        filterObject = JSON.parse(filter);
                        $("#TxtDateTimeFrom").val(filterObject.timeFrom);
                        $("#TxtDateTimeTo").val(filterObject.timeTo);
                        $("#ddlDepartment").val(filterObject.departmentId);
                        localStorage.removeItem("filterObject");
                        $("#ViewData").click();
                    }
                    catch (err) {
                        console.log(err.message);
                    }
                }


            });

            function assignFilter(This) {
                $('#load').click();
                console.log("sdsa")

                filterObject = {};
                filterObject.timeFrom = $("#TxtDateTimeFrom").val();
                filterObject.timeTo = $("#TxtDateTimeTo").val();
                filterObject.departmentId = $("#ddlDepartment").val();
                localStorage.setItem("filterObject", JSON.stringify(filterObject));
            }

            function showTrPrice() {
                $(".tr-price").show();
            }

            function BindStaffFilter() {
                $(".eb-select").bind("focus", function () {
                    EBSelect_ShowBox($(this).parent().find(".eb-select-data"));
                });
                $(window).bind("click", function (e) {
                    console.log(e);
                    if ((!e.target.className.match("eb-select") && e.target.parentElement.className.match("ul-listing-staff")) ||
                        (!e.target.className.match("eb-select") && !e.target.parentElement.className.match("ul-listing-staff"))
                    && !e.target.className.match("mCSB_dragger_bar")) {
                        EBSelect_HideBox();
                    }
                });
                //============================
                // Scroll Staff Filter
                //============================
                $('.eb-select-data').each(function () {
                    if ($(this).height() > 230) {
                        $(this).mCustomScrollbar({
                            theme: "dark-2",
                            scrollInertia: 100
                        });
                    }
                });
            }

            function BindIdToHDF(THIS, id, HDF_DomId, Input_DomId) {
                var text = THIS.innerText.trim(),
                    HDF_Dom = document.getElementById(HDF_DomId),
                    InputText = document.getElementById(Input_DomId);
                HDF_Dom.value = id;
                InputText.value = text;
                InputText.setAttribute("data-value", id);
                ajaxGetStaffsByType(id);
                if (HDF_DomId == "HDF_TypeStaff") {
                    $("#StaffName").val("");
                    $("#HDF_Staff").val("");
                }
            }

            function ajaxGetStaffsByType(type) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Report/TimeKeeping.aspx/Load_Staff_ByType",
                    data: '{type : "' + type + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            var staffs = JSON.parse(mission.msg);
                            if (staffs.length > 0) {
                                var tmp = "";
                                $.each(staffs, function (i, v) {
                                    tmp += '<li data-code="' + v.Code + '" data-id="' + v.Id + '"' +
                                            'onclick="BindIdToHDF(this,' + v.Id + ',\'HDF_Staff\',\'StaffName\')">' +
                                            v.Fullname + '</li>';
                                });
                                $("#UlListStaff").empty().append(tmp);
                            }

                        } else {
                            var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                            showMsgSystem(msg, "warning");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function EBSelect_ShowBox(Dom) {
                EBSelect_HideBox();
                Dom.show();
            }

            function EBSelect_HideBox() {
                $(".eb-select-data").hide();
            }

            function EBSelect_BindData() {

            }

            function ReplaceZezoValue() {
                $("table.table-listing td").each(function () {
                    if ($(this).text().trim() == "0") {
                        $(this).text("-");
                    }
                });
            }

            function viewDataByDate(This, time) {
                $(".tag-date.active").removeClass("active");
                This.addClass("active");
                $("#TxtDateTimeTo").val("");
                $("#TxtDateTimeFrom").val(time);
                $("#ViewData").click();
            }
            // gan linh the <tr>
<<<<<<< HEAD:GUI/BackEnd/TuyenDungV2/ReportCV.aspx
            //function redirect(This) {
            //    //alert($(This).attr("data-id")); return;
            //    window.location = '/admin/tuyen-dung/danh-sach-test/' + $(This).attr("data-id") + '.html';
            //}
=======
            function redirect(This) {
                //alert($(This).attr("data-id")); return;
                window.location = '/admin/tuyen-dung/step-3/' + $(This).attr("data-id") + '.html';
            }
>>>>>>> origin/develop.ThongkeVattu:GUI/BackEnd/TuyenDung/Step3_DanhSach.aspx
        </script>
    </asp:Panel>
</asp:Content>
