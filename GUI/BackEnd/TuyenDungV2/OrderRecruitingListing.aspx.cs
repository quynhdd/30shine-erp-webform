﻿using _30shine.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Linq.Expressions;
using ExportToExcel;
using System.Data.SqlClient;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using System.Net.Http;
using System.Data;
using _30shine.Helpers.Http;
using System.Web.Services;
using System.Data.Entity.Migrations;
using System.Data.Objects;

namespace _30shine.GUI.BackEnd.TuyenDungV2
{
    public partial class OrderRecruitingListing : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        public CultureInfo culture = new CultureInfo("vi-VN");
        public bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected bool isRoot = false;
        protected int departmentId;
        public double? salaryTotal = 0;
        protected string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
        protected string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
        private Solution_30shineEntities db = new Solution_30shineEntities();
        private IOrderRecruitingStaffModel orderStaff = new OrderRecruitingStaffModel();
        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //SetPermission();
            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = FirstDayOfMonth;
                TxtDateTimeTo.Text = Today;
                Library.Function.bindSalon_NotHoiQuan(new List<DropDownList> { Salon }, true);
                bindDepartment();
                BindStatus();
                ddlFillter.SelectedValue = "2";
            }
            RemoveLoading();
        }
        /// <summary>
        /// set permission
        /// </summary>
        public void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        /// <summary>
        /// ExecuteByPermission
        /// </summary>
        private void ExecuteByPermission()
        {

            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        /// <summary>
        /// Load department
        /// </summary>
        private void bindDepartment()
        {
            using (var db = new Solution_30shineEntities())
            {
                List<int> type = new List<int>() { 19, 18, 15, 14, 13, 11 };
                var department = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true && !type.Contains(w.Id)).ToList();
                var first = new Staff_Type();
                first.Id = 0;
                first.Name = "Chọn tất cả bộ phận";
                department.Insert(0, first);
                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "Id";
                ddlDepartment.DataSource = department;
                ddlDepartment.DataBind();
                ddlDepartment.SelectedIndex = 0;
            }
        }
        private void BindStatus()
        {
            using (var db = new Solution_30shineEntities())
            {
                
                var status = db.Tbl_Config.Where(w=>w.Key == "recruitment_status" && w.IsDelete == 0).ToList();
                var first = new Tbl_Config();
                first.Value = 0.ToString();
                first.Label = "Chọn tất cả trạng thái";
                status.Insert(0, first);
                ddlStatus.DataTextField = "Label";
                ddlStatus.DataValueField = "Value";
                ddlStatus.DataSource = status;
                ddlStatus.DataBind();
                ddlStatus.SelectedIndex = 0;
            }
        }
        protected void _BtnClick(object sender, EventArgs e)
        {
            BindData();
            RemoveLoading();
        }

        private Tbl_Config GetConfig(string value, string key)
        {
            var config = db.Tbl_Config.Where(w => w.Value == value && w.Key == key).FirstOrDefault();
            return config;
        }
        protected void BindData()
        {
            var timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text,culture);
            var timeTo = Convert.ToDateTime(TxtDateTimeTo.Text,culture).AddDays(1);
            var salonId = Convert.ToInt32(Salon.SelectedValue);
            var departmentId = Convert.ToInt32(ddlDepartment.SelectedValue);
            var typeFillter = Convert.ToInt32(ddlFillter.SelectedValue);
            var status = Convert.ToInt32(ddlStatus.SelectedValue);
            var fillter = new List<OrderRecruitingStaff>();
            switch (typeFillter)
            {
                case 1:
                    switch (salonId)
                    {
                        case 0:
                            switch (departmentId)
                            {
                                case 0:
                                    switch (status)
                                    {
                                        case 0:
                                            fillter = orderStaff.GetList(w => w.Needed >= timeFrom && w.Needed <= timeTo).ToList();
                                            break;
                                        default:
                                            fillter = orderStaff.GetList(w => w.Needed >= timeFrom && w.Needed <= timeTo && w.Status == status).ToList();
                                            break;
                                    }
                                    break;
                                default:
                                    switch (status)
                                    {
                                        case 0:
                                            fillter = orderStaff.GetList(w => w.Needed >= timeFrom && w.Needed <= timeTo && w.DepartmentId == departmentId).ToList();
                                            break;
                                        default:
                                            fillter = orderStaff.GetList(w => w.Needed >= timeFrom && w.Needed <= timeTo && w.Status == status && w.DepartmentId == departmentId).ToList();
                                            break;
                                    }
                                    break;
                            }
                            break;
                        default:
                            switch (departmentId)
                            {
                                case 0:
                                    switch (status)
                                    {
                                        case 0:
                                            fillter = orderStaff.GetList(w => w.Needed >= timeFrom && w.Needed <= timeTo && w.SalonId == salonId).ToList();
                                            break;
                                        default:
                                            fillter = orderStaff.GetList(w => w.Needed >= timeFrom && w.Needed <= timeTo && w.Status == status && w.SalonId == salonId).ToList();
                                            break;
                                    }
                                    break;
                                default:
                                    switch (status)
                                    {
                                        case 0:
                                            fillter = orderStaff.GetList(w => w.Needed >= timeFrom && w.Needed <= timeTo && w.DepartmentId == departmentId && w.SalonId == salonId).ToList();
                                            break;
                                        default:
                                            fillter = orderStaff.GetList(w => w.Needed >= timeFrom && w.Needed <= timeTo && w.Status == status && w.DepartmentId == departmentId && w.SalonId == salonId).ToList();
                                            break;
                                    }
                                    break;
                            }
                            break;
                    }
                    break;
                case 2:
                    switch (salonId)
                    {
                        case 0:
                            switch (departmentId)
                            {
                                case 0:
                                    switch (status)
                                    {
                                        case 0:
                                            fillter = orderStaff.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate <= timeTo).ToList();
                                            break;
                                        default:
                                            fillter = orderStaff.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate <= timeTo && w.Status == status).ToList();
                                            break;
                                    }
                                    break;
                                default:
                                    switch (status)
                                    {
                                        case 0:
                                            fillter = orderStaff.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate <= timeTo && w.DepartmentId == departmentId).ToList();
                                            break;
                                        default:
                                            fillter = orderStaff.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate <= timeTo && w.Status == status && w.DepartmentId == departmentId).ToList();
                                            break;
                                    }
                                    break;
                            }
                            break;
                        default:
                            switch (departmentId)
                            {
                                case 0:
                                    switch (status)
                                    {
                                        case 0:
                                            fillter = orderStaff.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate <= timeTo && w.SalonId == salonId).ToList();
                                            break;
                                        default:
                                            fillter = orderStaff.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate <= timeTo && w.Status == status && w.SalonId == salonId).ToList();
                                            break;
                                    }
                                    break;
                                default:
                                    switch (status)
                                    {
                                        case 0:
                                            fillter = orderStaff.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate <= timeTo && w.DepartmentId == departmentId && w.SalonId == salonId).ToList();
                                            break;
                                        default:
                                            fillter = orderStaff.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate <= timeTo && w.Status == status && w.DepartmentId == departmentId && w.SalonId == salonId).ToList();
                                            break;
                                    }
                                    break;
                            }
                            break;
                    }
                    break;
                case 3:
                    switch (salonId)
                    {
                        case 0:
                            switch (departmentId)
                            {
                                case 0:
                                    switch (status)
                                    {
                                        case 0:
                                            fillter = orderStaff.GetList(w => w.Completed >= timeFrom && w.Completed <= timeTo).ToList();
                                            break;
                                        default:
                                            fillter = orderStaff.GetList(w => w.Completed >= timeFrom && w.Completed <= timeTo && w.Status == status).ToList();
                                            break;
                                    }
                                    break;
                                default:
                                    switch (status)
                                    {
                                        case 0:
                                            fillter = orderStaff.GetList(w => w.Completed >= timeFrom && w.Completed <= timeTo && w.DepartmentId == departmentId).ToList();
                                            break;
                                        default:
                                            fillter = orderStaff.GetList(w => w.Completed >= timeFrom && w.Completed <= timeTo && w.Status == status && w.DepartmentId == departmentId).ToList();
                                            break;
                                    }
                                    break;
                            }
                            break;
                        default:
                            switch (departmentId)
                            {
                                case 0:
                                    switch (status)
                                    {
                                        case 0:
                                            fillter = orderStaff.GetList(w => w.Completed >= timeFrom && w.Completed <= timeTo && w.SalonId == salonId).ToList();
                                            break;
                                        default:
                                            fillter = orderStaff.GetList(w => w.Completed >= timeFrom && w.Completed <= timeTo && w.Status == status && w.SalonId == salonId).ToList();
                                            break;
                                    }
                                    break;
                                default:
                                    switch (status)
                                    {
                                        case 0:
                                            fillter = orderStaff.GetList(w => w.Completed >= timeFrom && w.Completed <= timeTo && w.DepartmentId == departmentId && w.SalonId == salonId).ToList();
                                            break;
                                        default:
                                            fillter = orderStaff.GetList(w => w.Completed >= timeFrom && w.Completed <= timeTo && w.Status == status && w.DepartmentId == departmentId && w.SalonId == salonId).ToList();
                                            break;
                                    }
                                    break;
                            }
                            break;
                    }
                    break;
                case 4:
                    switch (salonId)
                    {
                        case 0:
                            switch (departmentId)
                            {
                                case 0:
                                    switch (status)
                                    {
                                        case 0:
                                            fillter = orderStaff.GetList(w => w.StaffDate >= timeFrom && w.StaffDate <= timeTo).ToList();
                                            break;
                                        default:
                                            fillter = orderStaff.GetList(w => w.StaffDate >= timeFrom && w.StaffDate <= timeTo && w.Status == status).ToList();
                                            break;
                                    }
                                    break;
                                default:
                                    switch (status)
                                    {
                                        case 0:
                                            fillter = orderStaff.GetList(w => w.StaffDate >= timeFrom && w.StaffDate <= timeTo && w.DepartmentId == departmentId).ToList();
                                            break;
                                        default:
                                            fillter = orderStaff.GetList(w => w.StaffDate >= timeFrom && w.StaffDate <= timeTo && w.Status == status && w.DepartmentId == departmentId).ToList();
                                            break;
                                    }
                                    break;
                            }
                            break;
                        default:
                            switch (departmentId)
                            {
                                case 0:
                                    switch (status)
                                    {
                                        case 0:
                                            fillter = orderStaff.GetList(w => w.StaffDate >= timeFrom && w.StaffDate <= timeTo && w.SalonId == salonId).ToList();
                                            break;
                                        default:
                                            fillter = orderStaff.GetList(w => w.StaffDate >= timeFrom && w.StaffDate <= timeTo && w.Status == status && w.SalonId == salonId).ToList();
                                            break;
                                    }
                                    break;
                                default:
                                    switch (status)
                                    {
                                        case 0:
                                            fillter = orderStaff.GetList(w => w.StaffDate >= timeFrom && w.StaffDate <= timeTo && w.DepartmentId == departmentId && w.SalonId == salonId).ToList();
                                            break;
                                        default:
                                            fillter = orderStaff.GetList(w => w.StaffDate >= timeFrom && w.StaffDate <= timeTo && w.Status == status && w.DepartmentId == departmentId && w.SalonId == salonId).ToList();
                                            break;
                                    }
                                    break;
                            }
                            break;
                    }
                    break;
                default:
                    break;
            }
            #region if-else
            //if (salonId == 0 && departmentId == 0 && typeFillter == 0)
            //{
            //    fillter = orderStaff.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate <= timeTo).ToList();
            //}
            //else if (salonId !=0 && departmentId == 0 && typeFillter == 0)
            //{
            //    fillter = orderStaff.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate <= timeTo && w.SalonId == salonId).ToList();
            //}
            //else if (salonId != 0 && departmentId != 0 && typeFillter == 0)
            //{
            //    fillter = orderStaff.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate <= timeTo && w.SalonId == salonId && w.DepartmentId == departmentId).ToList();
            //}
            //else if (salonId != 0 && departmentId != 0 && typeFillter != 0)
            //{
            //    switch (typeFillter)
            //    {
            //        case 1:
            //            fillter = orderStaff.GetList(w => w.Needed >= timeFrom && w.Needed <= timeTo && w.SalonId == salonId && w.DepartmentId == departmentId).ToList();
            //            break;
            //        case 2:
            //            fillter = orderStaff.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate <= timeTo && w.SalonId == salonId && w.DepartmentId == departmentId).ToList();
            //            break;
            //        case 3:
            //            fillter = orderStaff.GetList(w => w.Completed >= timeFrom && w.Completed <= timeTo && w.SalonId == salonId && w.DepartmentId == departmentId).ToList();
            //            break;
            //        case 4:
            //            fillter = orderStaff.GetList(w => w.StaffDate >= timeFrom && w.StaffDate <= timeTo && w.SalonId == salonId && w.DepartmentId == departmentId).ToList();
            //            break;
            //        default:
            //            break;
            //    }
            //}
            //else if (salonId == 0 && departmentId != 0 && typeFillter == 0)
            //{
            //    fillter = orderStaff.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate <= timeTo && w.DepartmentId == departmentId).ToList();
            //}
            //else if (salonId == 0 && departmentId != 0 && typeFillter != 0)
            //{
            //    switch (typeFillter)
            //    {
            //        case 1:
            //            fillter = orderStaff.GetList(w => w.Needed >= timeFrom && w.Needed <= timeTo && w.DepartmentId == departmentId).ToList();
            //            break;
            //        case 2:
            //            fillter = orderStaff.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate <= timeTo && w.DepartmentId == departmentId).ToList();
            //            break;
            //        case 3:
            //            fillter = orderStaff.GetList(w => w.Completed >= timeFrom && w.Completed <= timeTo && w.DepartmentId == departmentId).ToList();
            //            break;
            //        case 4:
            //            fillter = orderStaff.GetList(w => w.StaffDate >= timeFrom && w.StaffDate <= timeTo && w.DepartmentId == departmentId).ToList();
            //            break;
            //        default:
            //            break;
            //    }

            //}
            //else if (salonId == 0 && departmentId == 0 && typeFillter != 0)
            //{
            //    switch (typeFillter)
            //    {
            //        case 1:
            //            fillter = orderStaff.GetList(w => w.Needed >= timeFrom && w.Needed <= timeTo).ToList();
            //            break;
            //        case 2:
            //            fillter = orderStaff.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate <= timeTo).ToList();
            //            break;
            //        case 3:
            //            fillter = orderStaff.GetList(w => w.Completed >= timeFrom && w.Completed <= timeTo).ToList();
            //            break;
            //        case 4:
            //            fillter = orderStaff.GetList(w => w.StaffDate >= timeFrom && w.StaffDate <= timeTo).ToList();
            //            break;
            //        default:
            //            break;
            //    }
            //}
            #endregion
            var data = (from a in fillter
                        join b in db.Staffs on a.CreatorId equals b.Id
                        join c in db.Tbl_Salon on a.SalonId equals c.Id
                        join d in db.TinhThanhs on a.RegionId equals d.ID
                        join e in db.Staff_Type on a.DepartmentId equals e.Id

                        select new
                        {
                            OrderId = a.Id,
                            RegionId = d.TenTinhThanh,
                            CreatorName = a.CreatorId + " | " + b.Fullname,
                            SalonName = c.ShortName,
                            DepartmentId = a.DepartmentId,
                            Department = e.Name,
                            Quantity = a.Quantity,
                            CreatedDate = a.CreatedDate,
                            Type = GetConfig(a.Type.ToString(), "recruitment_type").Label,
                            NeedDate = a.Needed,
                            Description = a.Description,
                            RevokeId = a.ReasonRevoke,
                            CandidateId = a.CandidateId,
                            CompleteDate = a.Completed,
                            _Status = a.Status,
                            HavingTry = a.HavingTry,
                            Action = GetConfig(a.Status.ToString(), "recruitment_status").Value == "1" ? "<a href='/admin/tuyen-dung/lap-ho-so/tao-moi-ho-so/"+a.Id+".html'>Tạo hồ sơ</a>" :
                                     GetConfig(a.Status.ToString(), "recruitment_status").Value == "2" ? "<a href='/admin/tuyen-dung/danh-gia-ky-nang/"+a.CandidateId+".html'>Test kỹ năng</a>" :
                                     GetConfig(a.Status.ToString(), "recruitment_status").Value == "3" ? "<a href='/admin/tuyen-dung/xac-nhan-thu-viec/" + a.CandidateId + ".html'>Duyệt HS</a>" :
                                     GetConfig(a.Status.ToString(), "recruitment_status").Value == "4" ? "<a href='/admin/tuyen-dung/tao-tai-khoan-nhan-vien/"+a.CandidateId+".html'>Tạo mã NV</a>" :
                                     GetConfig(a.Status.ToString(), "recruitment_status").Value == "5" ? "Hoàn thành" :
                                     "Đã hủy &nbsp<i class='fa fa-info-circle tip'><span>" + a.Description + "</span></i>",
                            Status = GetConfig(a.Status.ToString(), "recruitment_status").Value == "1" ? "<span class='alert alert-danger'>" + GetConfig(a.Status.ToString(), "recruitment_status").Label + "</span>" :
                                     GetConfig(a.Status.ToString(), "recruitment_status").Value == "2" ? "<span class='alert alert-warning'>" + GetConfig(a.Status.ToString(), "recruitment_status").Label + "</span>" :
                                     GetConfig(a.Status.ToString(), "recruitment_status").Value == "3" ? "<span class='alert alert-info'>" + GetConfig(a.Status.ToString(), "recruitment_status").Label + "</span>" :
                                     GetConfig(a.Status.ToString(), "recruitment_status").Value == "4" ? "<span class='alert alert-success'>" + GetConfig(a.Status.ToString(), "recruitment_status").Label + "</span>" :
                                     GetConfig(a.Status.ToString(), "recruitment_status").Value == "5" ? "<span class='alert alert-success'>" + GetConfig(a.Status.ToString(), "recruitment_status").Label + "</span>" :
                                     "<span style='color:red'>" + GetConfig(a.Status.ToString(), "recruitment_status").Label + "</span>",
                            StaffId = a.StaffId,
                            StaffDate = a.StaffDate
                        }
                        ).OrderBy(w => w.NeedDate).ToList();

            Bind_Paging(data.Count());
            rptSalary.DataSource = data.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
            rptSalary.DataBind();
        }
        /// <summary>
        /// Paging
        /// </summary>
        /// <param name="TotalRow"></param>
        protected void Bind_Paging(int TotalRow)
        {
            try
            {
                // init Paging value
                int integer = 1;
                PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
                PAGING._PageNumber = !IsPostBack ? 1 : (int.TryParse(HDF_Page.Value, out integer) ? integer : 1);
                PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
                PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
                PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
                PAGING._Paging = PAGING.Make_Paging();

                RptPaging.DataSource = PAGING._Paging.ListPage;
                RptPaging.DataBind();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        
        [WebMethod]
        public static int UpdateHavingTry(int orderId, int status)
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
            var data = db.OrderRecruitingStaffs.Where(w => w.Id == orderId).FirstOrDefault();
            if (data == null)
            {
                return 0;
            }
            data.HavingTry = Convert.ToBoolean(status);
            data.ModifiedDate = DateTime.Now;
            db.OrderRecruitingStaffs.AddOrUpdate(data);
            db.SaveChanges();
            // write log 1: update status, 2: update ..., 3: update having try
            OrderRecruitingStaffLog log = new OrderRecruitingStaffLog();
            log.CreatedTime = DateTime.Now;
            log.OrderId = data.Id;
            log.TypeId = 3;
            log.Value = status.ToString();
            log.StaffEditId = Convert.ToInt32(HttpContext.Current.Session["User_Id"].ToString());
            db.OrderRecruitingStaffLogs.Add(log);
            db.SaveChanges();
            return 1;
        }
        
        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }
        [WebMethod]
        public static int DeleteOrder(int orderId,int revokeId, string reason)
        {

            Solution_30shineEntities db = new Solution_30shineEntities();
            var data = db.OrderRecruitingStaffs.Where(w => w.Id == orderId).FirstOrDefault();
            if (data == null)
            {
                return 0;
            }
            //data.IsDelete = true;
            data.Description = reason;
            data.ReasonRevoke = revokeId;
            data.Status = 6;
            db.OrderRecruitingStaffs.AddOrUpdate(data);
            db.SaveChanges();
            return 1;
        }
        [WebMethod]
        public static string UpdateStaffId(int staffId, int orderId, int departmentId)
        {
            var db = new Solution_30shineEntities();
            var response = new object();
            var order = db.OrderRecruitingStaffs.FirstOrDefault(w => w.Id == orderId);
            var checkExistStaff = db.OrderRecruitingStaffs.Where(w => w.StaffId == staffId).FirstOrDefault();
            var checkDepartment = db.Staffs.Where(w => w.Id == staffId && w.Type == departmentId).FirstOrDefault();
            //if (checkExistStaff != null)
            //{
            //    response = new
            //    {
            //        status = 1,
            //        msg = "Mã NV này đã được gán cho order khác. Bạn vẫn muốn nhập mã NV này?"
            //    };
            //    return new JavaScriptSerializer().Serialize(response);
            //}
            if (checkDepartment == null)
            {
                response = new
                {
                    status = 2,
                    msg = "Mã NV không tồn tại hoặc bộ phận không giống với yêu cầu. Vui lòng kiểm tra lại"
                };
                return new JavaScriptSerializer().Serialize(response);
            }
            order.StaffId = staffId;
            order.StaffDate = DateTime.Now;
            order.Status = 5;
            order.HavingTry = true;

            db.OrderRecruitingStaffs.AddOrUpdate(order);
            var rs = db.SaveChanges();
            if (rs == 0)
            {
                response = new
                {
                    status = 3,
                    msg = "Lỗi hệ thống. Vui lòng liên hệ nhóm phát triển"
                };
                return new JavaScriptSerializer().Serialize(response);
            }
            response = new
            {
                status = 4,
                msg = "Cập nhật thành công"
            };
            return new JavaScriptSerializer().Serialize(response);
        }
        [WebMethod]
        public static string GetLogs(int orderId)
        {
            // write log 1: update status, 2: update ..., 3: update having try
            var db = new Solution_30shineEntities();
            var logs = (
                from a in db.OrderRecruitingStaffLogs
                join b in db.Staffs on a.StaffEditId equals b.Id
                where a.OrderId.Value == orderId
                select new
                {
                    OrderId = a.OrderId,
                    CreateStaffId = b.Id,
                    CreateStaffName = b.Fullname,
                    TypeLog = a.TypeId == 1 ? "Trạng thái đơn hàng" : a.TypeId == 3 ? "Xác nhận có nhân sự" : "",
                    Value = (a.TypeId == 3 && a.Value == "1") ? "Đã có" : "Chưa có",
                    CreatedDate = a.CreatedTime
                }
                ).Take(10).OrderByDescending(w=>w.CreatedDate).ToList();
            return new JavaScriptSerializer().Serialize(logs);
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
    }
}