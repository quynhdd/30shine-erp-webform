﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Script.Serialization;
using System.Data;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.TuyenDungV2
{
    public partial class ConfirmTryWork : System.Web.UI.Page
    {
        private string PageID = "TD_XNTV";
        protected TuyenDung_UngVien OBJ;
        protected string _Code;
        protected bool _IsUpdate = false;
        public UIHelpers _UIHelpers = new UIHelpers();
        protected bool HasImages = false;
        private bool Perm_Access = false;
        private bool Perm_AllPermission = false;
        protected List<string> ListImagesUrl = new List<string>();

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_AllPermission = permissionModel.GetActionByActionNameAndPageId("Perm_AllPermission", PageID, permission);
                //////Perm_Add = permissionModel.GetActionByActionNameAndPageId("Perm_Add", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_AllPermission = permissionModel.CheckPermisionByAction("Perm_AllPermission", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_AllPermission);
                Bind_department();
                BindSkillNameLevel();
                BindTester();
                BindSource();
                BindRelation();
            }
            if (IsUpdate())
            {
                Bind_OBJ();
            }
        }
        //#region[BindSalon]
        //public void Bind_salon()
        //{
        //    using (var db = new Solution_30shineEntities())
        //    {
        //        var LST = db.Tbl_Salon.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Order).ToList();

        //        ddlSalon.DataTextField = "Name";
        //        ddlSalon.DataValueField = "Id";
        //        ddlSalon.SelectedIndex = 0;
        //        ddlSalon.DataSource = LST;
        //        ddlSalon.DataBind();
        //    }
        //}
        //#endregion
        #region[Deparment]
        public void Bind_department()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Order).ToList();
                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "Id";
                ddlDepartment.SelectedIndex = 0;
                ddlDepartment.DataSource = LST;
                ddlDepartment.DataBind();
            }
        }
        #endregion
        
        #region[BindObj]
        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }
        private bool Bind_OBJ()
        {
            using (var db = new Solution_30shineEntities())
            {
                var serialize = new JavaScriptSerializer();
                var ExistOBJ = true;
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.TuyenDung_UngVien.FirstOrDefault(w => w.Id == Id);
                    if (!OBJ.Equals(null))
                    {
                        var staff = db.Staffs.FirstOrDefault(o => o.Id == OBJ.StaffId);
                        var candidateEvl = db.TuyenDung_DanhGia.FirstOrDefault(o => o.UngVienID == OBJ.Id);
                        if (staff != null)
                        {
                            if (staff.Id > 0)
                            {
                                ID.Text = staff.Id.ToString();
                                Account.Text = staff.Email;
                            }
                        }
                        FullName.Text = OBJ.FullName;
                        Phone.Text = OBJ.Phone;
                        ddlSalon.SelectedValue = OBJ.salonId.ToString() == "" ? "0" : OBJ.salonId.ToString();
                        ddlDepartment.SelectedValue = OBJ.DepartmentId.ToString() == "" ? "0" : OBJ.DepartmentId.ToString();
                        if (candidateEvl != null)
                        {
                            ddlTester.SelectedValue = candidateEvl.UserTestID.ToString() == "" ? "0" : candidateEvl.UserTestID.ToString();
                        }
                        Relation.SelectedValue = OBJ.StatusRelationId.ToString() == "" ? "0" : OBJ.StatusRelationId.ToString();
                        Source.SelectedValue = OBJ.NguonTuyenDungId.ToString() == "" ? "0" : OBJ.NguonTuyenDungId.ToString();
                        CMT.Text = OBJ.CMT;
                        CMTByDate.Text = String.Format("{0:MM/dd/yyyy}", OBJ.ProvidedDate);
                        CMTProviderLocale.Text = OBJ.ProvidedLocale;
                        HDF_MainImg.Value = OBJ.MainImg;
                        HiddenFieldUVId.Value = OBJ.Id.ToString();
                        TestVH.Text = OBJ.Test_VH;
                        PassVH.Text = OBJ.Parse_VH == true ? "Đạt" : "Không đạt";
                        TestVHAgain.Text = OBJ.TestAgain == true ? "Test 2 lần" : "Chưa test 2 lần";
                        TestCut.Text = OBJ.TestCut;
                        PassCut.Text = OBJ.PassCut == true ? "Đạt" : "Không đạt";
                        TestCutAgain.Text = OBJ.TestCutAgain == true ? "Test 2 lần" : "Chưa test 2 lần";
                        TestChemistry.Text = OBJ.TestChemistry;
                        PassChemistry.Text = OBJ.PassChemistry == true ? "Đạt" : "Không đạt";
                        TestChemistryAgain.Text = OBJ.TestChemistryAgain == true ? "Test 2 lần" : "Chưa test 2 lần";
                        Step3Note.Text =  OBJ.Step2Note;
                    }
                    else
                    {
                        ExistOBJ = false;
                        var msg = "Lỗi! Bản ghi không tồn tại";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
                else
                {
                    ExistOBJ = false;
                    var msg = "Lỗi! Bản ghi không tồn tại";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }
                return ExistOBJ;
            }
        }

        //TODO bind ddlUserTest
        private void BindTester()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.TuyenDung_NguoiTest.OrderBy(p => p.FullName).ToList();
                var item = new TuyenDung_NguoiTest();
                item.FullName = "Chọn người kiểm tra";
                item.Id = 0;
                lst.Insert(0, item);
                ddlTester.DataTextField = "Fullname";
                ddlTester.DataValueField = "Id";
                ddlTester.SelectedIndex = 0;
                ddlTester.DataSource = lst;
                ddlTester.DataBind();
            }
        }

        //TODO bind ddlUserTest
        private void BindSource()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.TuyenDung_Nguon.OrderBy(p => p.Name).ToList();
                var item = new TuyenDung_Nguon();
                item.Name = "Chọn nguồn tuyển dụng";
                item.Id = 0;
                lst.Insert(0, item);
                Source.DataTextField = "Name";
                Source.DataValueField = "Id";
                Source.SelectedIndex = 0;
                Source.DataSource = lst;
                Source.DataBind();
            }
        }

        //TODO bind ddlUserTest
        private void BindRelation()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Tbl_Status.Where(w => w.ParentId == 25).OrderBy(p => p.Name).ToList();
                var item = new Tbl_Status();
                item.Name = "Chọn tình trạng hôn nhân";
                item.Value = 0;
                lst.Insert(0, item);
                Relation.DataTextField = "Name";
                Relation.DataValueField = "Value";
                Relation.SelectedIndex = 0;
                Relation.DataSource = lst;
                Relation.DataBind();
            }
        }


        #endregion
        #region[BidnSkillNameAndLevel]
        /// <summary>
        /// Lấy về tên các kỹ năng 
        /// </summary>
        private static string chuoi = "";
        private void BindSkillNameLevel()
        {
            _Code = Request.QueryString["Code"];
            int Id;

            using (var db = new Solution_30shineEntities())
            {
                if (int.TryParse(_Code, out Id))
                {
                    var listId = db.TuyenDung_DanhGia.Where(w => w.UngVienID == Id).ToList();
                    if (listId.Count > 0)
                    {
                        for (int i = 0; i < listId.Count; i++)
                        {
                            chuoi = listId[i].MapSkillLevelID;
                        }
                    }
                }
                if (chuoi != "")
                {
                    var listSkillName = db.Database.SqlQuery<cls_skillname>($"select map.Id as IdSkill , skill.SkillName from TuyenDung_SkillLevel_Map as map join TuyenDung_Skill as skill on map.SkillID = skill.Id where map.IsDelete = 0 and map.Id in ({chuoi})").ToList();
                    if (listSkillName.Count > 0)
                    {
                        rptSkillName.DataSource = listSkillName;
                        rptSkillName.DataBind();
                    }
                }
            }
        }

        /// <summary>
        /// Bind người test kỹ năng
        /// </summary>
        private void BindUserTest(TuyenDung_UngVien obj)
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = db.TuyenDung_NguoiTest.Where(t => t.IsDelete == false).ToList();
                if (list.Count > 0)
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        if (list.FindIndex(w => w.Id == obj.TesterID) != -1)
                        {
                            ddlTester.Items.Add(new ListItem(list[i].FullName, list[i].Id.ToString()));
                        }
                    }
                    ddlTester.DataBind();
                }
            }
        }

        /// <summary>
        /// Sự kiện IteamDataBound lấy dữ liệu đổ vào Dropdownlist
        /// </summary>
        protected void rptSkillName_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                var dataItem = (cls_skillname)e.Item.DataItem;
                if (dataItem != null)
                {
                    DropDownList rpt = e.Item.FindControl("drlLevelName") as DropDownList;
                    using (var db = new Solution_30shineEntities())
                    {
                        var listLevel = db.Database.SqlQuery<cls_SkillLevel>("select lev.Id, CONCAT((lev.LevelName), ' --- ', (map.[Description])) as levelName from TuyenDung_SkillLevel_Map as map join TuyenDung_SkillLevel as lev on map.LevelID = lev.Id where map.IsDelete = 0 and map.Id in (" + dataItem.IdSkill + ")").ToList();
                        rpt.Items.Clear();
                        rpt.DataTextField = "levelName";
                        rpt.DataValueField = "Id";
                        rpt.DataSource = listLevel;
                        rpt.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public class cls_skillname
        {
            public int IdSkill { get; set; }
            public string skillName { get; set; }
        }
        public class cls_SkillLevel
        {
            public int Id { get; set; }
            public string levelName { get; set; }
            public string description { get; set; }
        }

        #endregion
        #region[Duyetthuviec,Daotao,Khongnhan]
        /// <summary>
        /// Duyetthuviec,Daotao,Khongnhan
        /// </summary>
        protected void btnDuyet_Click(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                int Id = int.TryParse(HiddenFieldUVId.Value, out integer) ? integer : 0;

                OBJ = db.TuyenDung_UngVien.FirstOrDefault(w => w.Id == Id);
                var _obj = db.TuyenDung_DanhGia.FirstOrDefault(w => w.UngVienID == Id);
                if (!OBJ.Equals(null))
                {
                    OBJ.UngVienStatusId = 4;
                    _obj.IsDelete = true;
                    _obj.IsStatus = true;
                    _obj.ModifiedTime = DateTime.Now;
                    OBJ.StepEndNote = Step3Note.Text;
                    _obj.Note = Step3Note.Text;
                    if (OBJ.StepEndTime == null)
                    {
                        OBJ.StepEndTime = DateTime.Now;
                    }
                    else
                    {
                        OBJ.StepEndModifiedTime = DateTime.Now;
                    }
                    db.TuyenDung_UngVien.AddOrUpdate(OBJ);

                    //cập nhật thông tin ứng viên
                    var staff = db.Staffs.FirstOrDefault(o => o.Id == OBJ.StaffId);
                    if (staff != null)
                    {
                        staff.SalonId = OBJ.salonId;
                        staff.IsHoiQuan = false;
                        staff.ModifiedDate = DateTime.Now;
                        db.Staffs.AddOrUpdate(staff);
                    }
                    //update OrderRecruitment table status
                    var order = db.OrderRecruitingStaffs.FirstOrDefault(w => w.CandidateId == OBJ.Id);
                    if (order != null)
                    {
                        order.Status = 4;
                        order.ModifiedDate = DateTime.Now;
                        db.OrderRecruitingStaffs.AddOrUpdate(order);
                    }
                    //db.TuyenDung_DanhGia.AddOrUpdate(_obj);
                    var exc = db.SaveChanges();
                    if (exc > 0)
                    {
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_add_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_add_message", "Cập nhật thành công!"));
                        //UIHelpers.Redirect("/admin/tuyen-dung/danh-sach-test.html");
                        UIHelpers.Redirect("/admin/tuyen-dung/tao-tai-khoan-nhan-vien/" + _Code + ".html");

                    }
                    else
                    {
                        var msg = "Vui lòng nhập số lần!";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
                else
                {
                    var msg = "Lỗi! Không tìm thấy bản ghi.";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);

                }
                Context.RewritePath("+", "", "");
            }
        }



        protected void btnKhongDuyet_Click(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                int Id = int.Parse(HiddenFieldUVId.Value);
                OBJ = db.TuyenDung_UngVien.FirstOrDefault(w => w.Id == Id);
                var _obj = db.TuyenDung_DanhGia.FirstOrDefault(w => w.UngVienID == Id);
                if (!OBJ.Equals(null))
                {
                    OBJ.UngVienStatusId = 5;
                    _obj.IsDelete = true;
                    _obj.IsStatus = true;
                    _obj.ModifiedTime = DateTime.Now;
                    _obj.Note = Step3Note.Text;
                    OBJ.StepEndNote = Step3Note.Text;
                    if (OBJ.StepEndTime == null)
                    {
                        OBJ.StepEndTime = DateTime.Now;
                    }
                    else
                    {
                        OBJ.StepEndModifiedTime = DateTime.Now;
                    }

                    db.TuyenDung_UngVien.AddOrUpdate(OBJ);
                    db.TuyenDung_DanhGia.AddOrUpdate(_obj);
                    var exc = db.SaveChanges();
                    if (exc > 0)
                    {
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_add_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_add_message", "Cập nhật thành công!"));
                        UIHelpers.Redirect("/admin/tuyen-dung/danh-sach-da-test-ky-nang.html");
                    }
                    else
                    {
                        var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
                else
                {
                    var msg = "Lỗi! Không tìm thấy bản ghi.";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);

                }
                Context.RewritePath("+", "", "");
            }
        }
        #endregion
    }
}