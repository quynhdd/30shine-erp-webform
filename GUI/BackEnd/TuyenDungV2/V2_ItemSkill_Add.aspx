﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="V2_ItemSkill_Add.aspx.cs" Inherits="_30shine.GUI.BackEnd.TuyenDungV2.V2_ItemSkill_Add" %>

<asp:Content ID="ProductAdd" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý bộ  phận &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/admin/tuyen-dung/quan-tri-ky-nang.html">Danh sách</a></li>
                        <% if (_IsUpdate)
                            { %>
                        <li class="li-add"><a href="/admin/tuyen-dung/quan-tri-ky-nang/them-moi.html">Thêm mới</a></li>
                        <li class="li-edit active"><a href="/admin/tuyen-dung/quan-tri-ky-nang/<%= _Code %>.html">Cập nhật</a></li>
                        <% }
                            else
                            { %>
                        <li class="li-add active"><a href="/admin/tuyen-dung/quan-tri-ky-nang/them-moi.html">Thêm mới</a></li>
                        <% } %>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="table-wp">
                    <table class="table-add admin-product-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Thông tin kỹ năng</strong></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Tên kỹ năng<i style="color:red">&nbsp*</i></span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="Name" runat="server" ClientIDMode="Static"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="NameValidate" ControlToValidate="Name" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập tên kỹ năng!"></asp:RequiredFieldValidator>
                                    </span>

                                </td>
                            </tr>
                            <tr class="tr-description">
                                <td class="col-xs-2 left"><span>Mô tả</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox TextMode="MultiLine" Rows="5" ID="Description" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-2 left"><span>Gán bậc kỹ năng<i style="color:red">&nbsp*</i></span></td>
                                <td class="col-xs-9 right">
                                    <%--<span class="field-wp" id="skillLevelList" style="width: 50%">--%>
                                    <asp:Repeater runat="server" ID="rptSkillLevel" ClientIDMode="Static">
                                        <ItemTemplate>
                                            <tr>
                                                <td class="col-xs-2 left">
                                                    <label class="lbl-cus-no-infor" style="margin-right: 5px; cursor: pointer; font-weight: normal; font-size: 13px; padding: 2px 10px; background: #ddd; line-height: 30px; float: right;">
                                                        <%if (_IsUpdate)
                                                            { %>
                                                        <input type="checkbox" class="ip-skill-level" value="<%# Eval("Id") %>"
                                                            <%# Convert.ToBoolean(Eval("isChecked")) == true ? "checked='checked'" : "" %> style="position: relative; top: 2px;" />
                                                        <%# Eval("LevelName") %>
                                                        <%}
                                                            else
                                                            { %>
                                                        <input onclick="checkboxStatus($(this),<%# Eval("Id") %>)" type="checkbox" class="ip-skill-level" value="<%# Eval("Id") %>"
                                                            <%# Convert.ToBoolean(Eval("isChecked")) == true ? "checked='checked'" : "" %> style="position: relative; top: 2px;" />
                                                        <%# Eval("LevelName") %>
                                                        <%  }%>
                                                    </label>
                                                </td>
                                                <td class="col-xs-6 right">
                                                    <input type="text" data-level-id="<%# Eval("Id") %>" value="<%# Eval("Descr") %>" placeholder="Nhập mô tả" />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <%--  </span>--%>
                                </td>
                            </tr>
                            <tr class="tr-product-category">
                                <td class="col-xs-2 left"><span>Publish</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:CheckBox ID="Publish" Checked="true" runat="server" />
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-send">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <asp:Button ID="Send" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClientClick="getSkillLevel()" OnClick="ExcAddOrUpdate"></asp:Button>
                                    </span>
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="HDF_SkillLevelArray" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="HDF_Description" ClientIDMode="Static" />
                        </tbody>
                    </table>
                </div>
            </div>
            <%--End Add --%>
        </div>
        <script>
            var item = function () {
                this.id = 0,
                this.des = ''
            };
            function getSkillLevel() {
                var skillLevelArray = [];
                $("input.ip-skill-level:checked").each(function () {
                    var id = parseInt($(this).val());
                    id = !isNaN(id) ? id : 0;
                    var des = $("input[data-level-id='" + id + "']").val();
                    var itemLevel = new item();
                    itemLevel.id = id;
                    itemLevel.des = des;
                    skillLevelArray.push(itemLevel);
                });
                $("#HDF_SkillLevelArray").val(JSON.stringify(skillLevelArray));
            }       
        </script>
        <script>
            var skillId = <%= Convert.ToInt32(_Code) %>;
            var skillName = JSON.stringify($("#Name").val());
            $("#glbTuyendungV2").addClass("active");
            //$("#glbTuyendungV2").addClass("active");
            var paramClass = function () {
                this.Id = 0;
                this.isDelete = "";
                this.Description = "";
            }
            var paramObject = new paramClass();
            var arrParam = [];
            function checkboxStatus(This, Id) {
                paramObject.Id = Id;
                paramObject.isDelete = This.prop("checked");
                
                //addLoading();
                //$.ajax({
                //    type: 'POST',
                //    url: '/GUI/BackEnd/TuyenDungV2/V2_ItemSkill_Add.aspx/checkIsDelete',
                //    data: "{ Id : "+paramObject.Id+", isDelete : "+paramObject.isDelete+", skillId: "+skillId+"}",
                //    contentType: 'application/json',
                //}).success(function (response) {                   
                //    removeLoading();
                //});
            }
        </script>
    </asp:Panel>
</asp:Content>
