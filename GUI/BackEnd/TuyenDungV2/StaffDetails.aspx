﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TemplateMaster/SiteMaster.Master" CodeBehind="StaffDetails.aspx.cs" Inherits="_30shine.GUI.BackEnd.TuyenDungV2.StaffDetails" %>

<asp:Content ID="ProductAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <style>
            #radioApprove tr { display: inline-table; padding-right: 15px; cursor: pointer; }
            #radioApprove tr td input { margin-left: 15px; cursor: pointer; }
            #radioApprove tr td label { margin-left: 5px; position: relative; top: -1px; cursor: pointer; }

            .listing-img-upload .thumb-wp { width: auto; height: auto; }
            .listing-img-upload .thumb-wp img { position: relative; width: auto !important; height: auto !important; max-width: 100%; }

            .table-wp { background: #f5f5f5; float: left; width: 100%; padding: 15px; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; -webkit-box-shadow: 0px 0px 3px 0px #ddd; -moz-box-shadow: 0px 0px 3px 0px #ddd; box-shadow: 0px 0px 3px 0px #ddd; }
            .btn { border-radius: 5px; padding: 20px; }
            .cancel { border-radius: 5px; }
          

            .customer-add .table-add tr td.right .fb-cover-error {
                top: 0;
                right: auto;
            }

            .gallery {
                display: inline-block;
                margin-top: 20px;
            }

            .thumbnailImage {
                margin-bottom: 6px;
                width: 100%;
            }
             .modal-image {
                display: none;
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */ /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
            }

            /* Modal Content (image) */
            .modal-content-image {
                margin: auto;
                margin-top: 30px;
                display: block;
                width: 50%;
                max-width: 500px;
                height: auto;
                max-height: 600px;
            }

            /* Caption of Modal Image */
            #caption-image {
                margin: auto;
                display: block;
                width: 60%;
                max-width: 700px;
                text-align: center;
                color: #ccc;
                padding: 10px 0;
                height: 150px;
                margin-top: 10px;
            }

            /* Add Animation */
            .modal-content-image, #caption-image {
                -webkit-animation-name: zoom;
                -webkit-animation-duration: 0.6s;
                animation-name: zoom;
                animation-duration: 0.6s;
            }

            @-webkit-keyframes zoom {
                from {
                    -webkit-transform: scale(0);
                }

                to {
                    -webkit-transform: scale(1);
                }
            }

            @keyframes zoom {
                from {
                    transform: scale(0.1);
                }

                to {
                    transform: scale(1);
                }
            }

        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Tuyển dụng &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/admin/tuyen-dung/lap-ho-so/danh-sach-ho-so.html">Danh sách ứng viên</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add admin-product-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="table-wp">
                    <table class="table-add admin-product-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Chi tiết ứng viên</strong></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="myModalimage" style="z-index: 2000;" class="modal modal-image">
                                        <img class="modal-content-image" id="img01">
                                        <div id="captionimage"></div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Tên ứng viên</span></td>
                                <td class="col-xs-3 right">
                                    <asp:TextBox ID="FullName" runat="server" ClientIDMode="Static" Enabled="false" placeholder="Họ tên" Style="width: 220px;"></asp:TextBox>
                                    <% if (OBJ != null && OBJ.MainImg != null && OBJ.MainImg != "")
                                        { %>
                                    <div class="gallery" style="float: left; cursor: pointer; z-index: 200; position: absolute; top: -14px; width: auto; padding-left: 20px;">

                                        <img class="example-image form-control" id="myImg1" alt="" title="" src="<%=OBJ.MainImg %>"
                                            data-img="<%=OBJ.MainImg %>" style="width:120px; height: auto; max-height: 150px; left: 0px; max-width: 100%;" />
                                    </div>
                                    <% } %> 
                                    
                                </td>

                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Số ĐT</span></td>
                                <td class="col-xs-3 right">
                              
                                        <asp:TextBox ID="Phone" runat="server" ClientIDMode="Static" Enabled="false" placeholder="Số điện thoại" Style="width: 220px"></asp:TextBox>
                          

                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Số CMT/Thẻ căn cước</span></td>
                                <td class="col-xs-3 right">
                         
                                        <asp:TextBox ID="CMT" runat="server" ClientIDMode="Static" Style="width: 220px;" placeholder="Số CMT" Enabled="false"></asp:TextBox>
                         

                                </td>
                            </tr>

                             <tr class=" tr-upload tr-skill tr-skill-img" >
                                <td class="col-xs-3 left"><span>CMT</span></td>
                                <td class="col-xs-9 right image">
                                    <div class="wrap btn-upload-wp HDF_IMG_CMT1" style="margin-bottom: 5px; width: auto;">
                                        <div class="wrap listing-img-upload image">
                                            <% if (OBJ != null && OBJ.CMTimg1 != null && OBJ.CMTimg1 != "")
    { %>
                                            <div class="gallery">
                                                <img class="img-rounded myImg form-control" alt="" id="myImg3" title="Image 3" src="<%=OBJ.CMTimg1 %>"
                                                    data-img="<%=OBJ.CMTimg1 %>" style="width: 220px; max-height: 200px; height: auto; left: 0px; margin-right: 20px" />
                                            </div>
                                            <% } %>
                                            <% if (OBJ != null && OBJ.CMTimg2 != null && OBJ.CMTimg2 != "")
    { %>
                                            <div class="gallery">
                                                <img class="img-rounded myImg form-control" alt="" id="myImg4" title="Image 4" src="<%=OBJ.CMTimg2 %>"
                                                    data-img="<%=OBJ.CMTimg2 %>" style="width: 220px; height: auto; max-height: 200px" />
                                            </div>
                                            <% } %>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Bộ phận</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox runat="server" ID="bophan" Enabled="false"></asp:TextBox>
                                    </span>

                                </td>
                            </tr>

                           <%-- Modal show image --%>
                            
                            <tr class=" tr-upload tr-skill tr-skill-img" <% if (OBJ.DepartmentId != 1)
                                { 
                                        %>
                                    style="display:none"
                                <%
                                        }
                                     %>>
                                <td class="col-xs-3 left"><span>Ảnh kỹ năng</span></td>
                                <td class="col-xs-9 right image">
                                    <div class="wrap btn-upload-wp HDF_IMG_SKILL" style="margin-bottom: 5px; width: auto;">
                                        <div class="wrap listing-img-upload image">
                                            <% if (OBJ != null && OBJ.ImgSkill1 != null && OBJ.ImgSkill1 != "")
                                                { %>
                                            <div class="gallery">
                                                <img class="img-rounded myImg form-control" alt="" id="myImg5" title="Ảnh kỹ năng 1" src="<%=OBJ.ImgSkill1 %>"
                                                    data-img="<%=OBJ.ImgSkill1 %>" style="width: 220px; max-height: 200px; height: auto; left: 0px; margin-right: 20px" />
                                            </div>
                                            <% } %>
                                            <% if (OBJ != null && OBJ.ImgSkill2 != null && OBJ.ImgSkill2 != "")
                                                { %>
                                            <div class="gallery">
                                                <img class="img-rounded myImg form-control" alt="" id="myImg2" title="Ảnh kỹ năng 2" src="<%=OBJ.ImgSkill2 %>"
                                                    data-img="<%=OBJ.ImgSkill2 %>" style="width: 220px; height: auto; max-height: 200px" />
                                            </div>
                                            <% } %>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf" <% if (OBJ.DepartmentId != 1)
                                { 
                                        %>
                                    style="display:none"
                                <%
                                        }
                                     %>>
                                <td class="col-xs-3 left"><span>Kỹ năng</span></td>
                                <td>
                                <asp:Repeater runat="server" ID="rptSkillName" ClientIDMode="Static" OnItemDataBound="rptSkillName_ItemDataBound">
                                    <ItemTemplate>
                                        <tr>
                                            <td class="col-xs-3 right"></td>
                                            <td class="col-xs-9 right">
                                                <span style="width: 230px">
                                                    <label id="lblSkill" class="lbl-cus-no-infor" value="<%#Eval("IdSkill") %>" style="margin-bottom: 8px; cursor: pointer; font-weight: normal; font-size: 13px; padding: 2px 10px; background: #ddd; line-height: 33px; float: left;">
                                                        <%#Eval("SkillName") %>
                                                    </label>
                                                </span>
                                                <span style="width: 354px">
                                                    <asp:DropDownList runat="server" ID="drlLevelName" Enabled="false"></asp:DropDownList>
                                                </span>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                    </td>
                            </tr>
                            <% if (OBJ.DepartmentId == 2)
                                { 
                                        %>
                               <tr >
                                        <td class="col-xs-3 left"><span>Link Video</span></td>
                                        <td class="col-xs-3 right">
                         
                                                <asp:TextBox ID="txtVideoLink" runat="server" ClientIDMode="Static" Style="width: 220px;" placeholder="Link Video" Enabled="false"></asp:TextBox>
                         

                                        </td>
                                    </tr>
                                <%
                                        }
                                     %>
                            
                            <tr class="tr-description">
                                <td class="col-xs-2 left"><span>Ghi chú Lập hồ sơ</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox TextMode="MultiLine" Rows="2" ID="Step2Note" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-description">
                                <td class="col-xs-2 left"><span>Ghi chú Duyệt hồ sơ</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox TextMode="MultiLine" Rows="2" ID="StepEndNote" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-description">
                                <td class="col-xs-2 left"><span>Trạng thái</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="Trangthai" runat="server" ClientIDMode="Static" Enabled="false"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-send">
                                <td class="col-xs-3 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <a Class="btn-send" href="/admin/tuyen-dung/danh-sach-da-duyet.html">Quay lại</a>
                                        <%--<asp:Label runat="server" CssClass="btn-send" href="javascript:history(-1)" >Quay lại</asp:Label>--%>
                                    </span>
                                </td>
                            </tr>

                            <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="HiddenFieldUVId" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="HDF_Images" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="HDF_IMG_CMT1" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="HDF_IMG_CMT2" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="HDF_IMG_MAIN" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="HDF_IMG_SKILL1" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="HDF_IMG_SKILL2" ClientIDMode="Static" />
                        </tbody>
                    </table>
                </div>
            </div>
            <%-- end Add --%>
        </div>
        <script>

            jQuery(document).ready(function () {

                $("#glbAdminContent").addClass("active");
                $("#glbAdminCategory").addClass("active");
                //date picker
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        //$input.val($input.val().split(' ')[0]);
                    }
                });
                //excThumbWidth(120, 120);
            });
        </script>
        <script>
  //show image
            var modal = document.getElementById('myModalimage');
            // Get the image and insert it inside the modal - use its "alt" text as a caption
            var img = document.getElementById('myImg1');
            var img1 = document.getElementById('myImg2');
            var img2 = document.getElementById('myImg3');
            var img3 = document.getElementById('myImg4');
            var img4 = document.getElementById('myImg5');
            var modalImg = document.getElementById("img01");
            var captionText = document.getElementById("captionimage");

            try {
                img.onclick = function () {

                    modal.style.display = "block";
                    modalImg.src = this.src;
                    captionText.innerHTML = this.alt;
                }
                img1.onclick = function () {
                    modal.style.display = "block";
                    modalImg.src = this.src;
                    captionText.innerHTML = this.alt;
                }
                img2.onclick = function () {
                    modal.style.display = "block";
                    modalImg.src = this.src;
                    captionText.innerHTML = this.alt;
                }
                img3.onclick = function () {
                    modal.style.display = "block";
                    modalImg.src = this.src;
                    captionText.innerHTML = this.alt;
                }
                img4.onclick = function () {
                    modal.style.display = "block";
                    modalImg.src = this.src;
                    captionText.innerHTML = this.alt;
                }
                // When the user clicks on <span> (x), close the modal
                modal.onclick = function () {
                    console.log("a");
                    modal.style.display = "none";
                }
            } catch (err) { }


            $('#myModalimage').click(function () {
                $("#myModalimage").css("display", "none")
            });
            $('.modal-content-image').click(function (event) {
                event.stopPropagation();
            });


        </script>
        <!-- Popup plugin image hay là chưa gọi hàm getIdLevel nên nó k chạy  -->
        <script type="text/javascript">
            function popupImageIframe1(StoreImgField) {
                var Width = 960;
                var Height = 560;
                var ImgList = [];
                var iframe = $("<div class='iframe-image-upload' style='width:" + Width + "px; height:" + Height + "px; float:left; display:none;'><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + StoreImgField + "&callback=getThumbFromIframe&imgWidth=800&imgHeight=600&multi_img=false&Folder=Common' style='width:100%; height: 100%;'></iframe></div");

                $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
            }
            function popupImageIframe(StoreImgField) {
                var Width = 960;
                var Height = 560;
                var ImgList = [];
                var iframe = $("<div class='iframe-image-upload' style='width:" + Width + "px; height:" + Height + "px; float:left; display:none;'><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + StoreImgField + "&callback=getThumbFromIframe&imgWidth=800&imgHeight=600&multi_img=false&Folder=Common' style='width:100%; height: 100%;'></iframe></div");

                $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
            }

            window.getThumbFromIframe = function (Imgs, StoreImgField) {
                var imgs = "";
                var thumb = "";
                if (Imgs.length > 0) {
                    thumb = '<div class="thumb-wp">' +
                                '<img class="thumb" alt="" title="" src="' + Imgs[0] + '"' +
                                    'data-img="" />' +
                                '<span class="delete-thumb" onclick="deleteThumbnail($(this), \'' + StoreImgField + '\')"></span>' +
                            '</div>';
                    $("." + StoreImgField).find(".listing-img-upload").empty().append(thumb);
                    //excThumbWidth(120, 120);
                    $("#" + StoreImgField).val(Imgs[0]);
                }
                autoCloseEBPopup();
            }

            function deleteThumbnail(This, StoreImgField) {
                var imgs = "";
                var lst = This.parent().parent();
                This.parent().remove();
                lst.find("img.thumb").each(function () {
                    imgs += $(this).attr("src") + ",";
                });
                $("#" + StoreImgField).val(imgs);
            }
            </script>
    </asp:Panel>
</asp:Content>
