﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TemplateMaster/SiteMaster.Master" CodeBehind="ReportTester.aspx.cs" Inherits="_30shine.GUI.BackEnd.TuyenDungV2.ReportTester" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <title>Quản lý nhân sự | Danh sách</title>
    
    <style>

        .skill-table { display: table; width: 100%; }
        .skill-table .item-row { display: table-row; border-bottom:1px solid #ddd; }
        .skill-table .item-cell { display: table-cell; border-bottom: 1px solid #ddd; padding-top: 5px; padding-bottom: 5px; }
        .skill-table .item-cell > span { float: left; width: 100%; text-align: left; }
        .skill-table .item-row .item-cell .checkbox { float: left; padding-top: 0 !important; padding-bottom: 0 !important; margin-top: 6px !important; margin-bottom: 0px !important; margin-right: 9px; text-align: left; background: #ddd; padding: 2px 10px !important; }
        .skill-table .item-row .item-cell .checkbox input[type='checkbox'] { margin-left: 0!important; margin-right: 3px!important; }
        .skill-table .item-cell-order { width: 30px!important; }
        .skill-table .item-cell-skill { width: 120px!important; }
        .skill-table .item-cell-levels {  }

        .modal { z-index: 10000; }
        .be-report .row-filter { z-index: 0; }
        @media(min-width: 768px) {
            .modal-content,
            .modal-dialog { width: 750px!important; margin: auto; }
        }        
    </style>
</asp:Content>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

<div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Tuyển dụng &nbsp;&#187; </li>
                        <li class="li-listing"><a href="#">Danh sách người test</a></li>
                        <li class="li-add active"><a href="/admin/tuyen-dung/them-moi-nguoi-test.html"> Thêm mới </a></li>
                    </ul>
                </div>
            </div>
        </div>

<div class="wp customer-add customer-listing be-report">
    <%-- Listing --%>                
    <div class="wp960 content-wp">
      
        
                <div class="table-wp">
                    <table class="table display" id="tableTester"  style="width:100%">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>ID</th>
                                <th>Họ và tên</th>
                                <th>Email</th>
                                <th>Số ĐT</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <% 
                                int i = 0;
                                foreach (var item in nv)
                                {
                                    %>
                                    <tr>
                                         <td><%= i++ %></td>
                                        <td><%= item.Id %></td>
                                        <td><%= item.Fullname %></td>
                                        <td><%= item.Email %></td>
                                        
                                        <td><%= item.Telephone %></td>
                                        <td><a class="btn btn-info btn-xs" href="/admin/tuyen-dung/chinh-sua-nguoi-test/<%= item.Id %>.html">Sửa</a>
                                            <a class="btn btn-warning btn-xs" onclick="del(this.parentNode.parentNode.parentNode,'<%= item.Id %>','<%= item.Fullname %>')" href="javascript://" title="Xóa"><i class="fa fa-trash-o" aria-hidden="true"></i>&nbsp Xóa</a>
                                        </td>
                                    </tr>
                            <%
                                } %>
    
                        </tbody>
                    </table>
                </div>

               
        <%--<asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" style="display:none;" />--%>
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
        <!-- Hidden Field-->
        <asp:HiddenField ID="HDF_Suggestion_Code" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="HDF_Suggestion_Field" ClientIDMode="Static" runat="server" />
        <!-- END Hidden Field-->
    </div>    
    <%-- END Listing --%>
</div>

   
<script>
    
    jQuery(document).ready(function () {
        $("#glbTuyendungV2").addClass("active");
        $("#glbReportResult").addClass("active");
        $("#tableTester").DataTable({
            "bSort": false,
            
            'aoColumns': [
                  { bSearchable: true, bSortable: false },
                  { bSearchable: true, bSortable: false },
                  { bSearchable: true, bSortable: false },
                  { bSearchable: true, bSortable: false },
                  { bSearchable: true, bSortable: false },
                  { bSearchable: true, bSortable: false },
           
            ],
            "language": {
                "lengthMenu": " Bản ghi / trang _MENU_",
                "zeroRecords": "Không tồn tại bản ghi",
                "info": "Hiển thị trang _PAGE_ của _PAGES_",
                //"infoEmpty": "Không có dữ liệu",
                "infoFiltered": "(Lọc kết quả từ _MAX_ tổng số bản ghi)",
                "search": "Tìm kiếm",
                "paginate": {
                    "first": "Đầu",
                    "last": "Cuối",
                    "next": "+",
                    "previous": "-"
                },
            },
            "bLengthChange": true,
            "bFilter": true,
        });
    })
   
        function del(This, code, name) {
            var code = code || null,
                name = name || null,
                Row = This;
            if (!code) return false;
            // show EBPopup
            $(".confirm-yn").openEBPopup();
            $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");
            $("#EBPopup .yn-yes").bind("click", function () {
                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Ajax/Del.aspx/Delele_Tester",
                    data: '{Code : "' + code + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {

                            delSuccess();
                            Row.remove();
                            location.reload();
                        } else {
                            delFailed();
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            });
            $("#EBPopup .yn-no").bind("click", function () {
                autoCloseEBPopup(0);
            });
        }
   
        
   
    
</script>

</asp:Panel>
</asp:Content>
