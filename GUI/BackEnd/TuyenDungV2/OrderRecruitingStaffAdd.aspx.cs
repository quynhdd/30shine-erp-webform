﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.TuyenDungV2
{
    public partial class OrderRecruitingStaffAdd : System.Web.UI.Page
    {
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected static bool ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        private static OrderRecruitingStaffAdd instance;
        protected static string day = "";
        protected  bool validOrder = false;
        protected int totalOrder = 0;
        protected int totalOrderStylist = 0;
        protected  int numberOrder = 0;
        protected int numberStylist = 0;
        /// <summary>
        /// Get WorkDay_Listing instance
        /// </summary>
        /// <returns></returns>
        public static OrderRecruitingStaffAdd getInstance()
        {
            if (!(OrderRecruitingStaffAdd.instance is OrderRecruitingStaffAdd))
            {
                return new OrderRecruitingStaffAdd();
            }
            else
            {
                return OrderRecruitingStaffAdd.instance;
            }
        }

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                ViewAllData = Perm_ViewAllData;
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        public int GetIds()
        {
            return int.TryParse(Session["User_Id"].ToString(), out var integer) ? integer : 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                //day = DateTime.Now.DayOfWeek.ToString();
                
                CheckOrderValid(DateTime.Now.DayOfWeek.ToString());
                NumberOrder();
                BindStaffType();
                BindRegion();
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
            }
        }

        /// <summary>
        /// Bind Department to ddl
        /// </summary>
        protected void BindStaffType()
        {
            using (var db = new Solution_30shineEntities())
            {
                List<int> type = new List<int>() { 19, 18, 15, 14, 13, 11 };
                var types = db.Staff_Type.Where(w => w.IsDelete == 0 && w.Publish == true && !type.Contains(w.Id)).OrderBy(o => o.Id).ToList();
                var item = new Staff_Type();
                item.Id = 0;
                item.Name = "Chọn bộ phận";
                types.Insert(0, item);
                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "Id";
                ddlDepartment.DataSource = types;
                ddlDepartment.DataBind();
            }
        }
        private void NumberOrder()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var lastDate = DateTime.Now.AddDays(-7);
                    var dOW = DateTime.Now.DayOfWeek;
                    int od = 0;
                    int odS = 0;
                    var percentStylist = db.Tbl_Config.FirstOrDefault(a => a.Key == "number_order_recruitment_stylist" && a.IsDelete == 0).Value;
                    
                    var _numberOrder = db.Tbl_Config.FirstOrDefault(a => a.Key == "number_order_recruitment" && a.IsDelete == 0).Value;
                    var _numberStylist = Convert.ToInt32(_numberOrder) * Convert.ToDouble(percentStylist) / 100;
                    totalOrder = Convert.ToInt32(_numberOrder);
                    totalOrderStylist = Convert.ToInt32(_numberStylist);

                    var orderToday = db.OrderRecruitingStaffs.Where(w => EntityFunctions.TruncateTime(w.CreatedDate) == EntityFunctions.TruncateTime(DateTime.Now) && w.IsDelete == false && w.ReasonRevoke == 0).ToList().Count;
                    var orderStylistToday = db.OrderRecruitingStaffs.Where(w => EntityFunctions.TruncateTime(w.CreatedDate) == EntityFunctions.TruncateTime(DateTime.Now) && w.IsDelete == false && w.DepartmentId == 1 & w.ReasonRevoke == 0).ToList().Count;
                    var listOrder =
                        db.OrderRecruitingStaffs.Where(w =>EntityFunctions.TruncateTime(w.CreatedDate)< EntityFunctions.TruncateTime(DateTime.Now)
                                    && EntityFunctions.TruncateTime(w.CreatedDate) >= EntityFunctions.TruncateTime(lastDate)
                                    && w.Status != 5 && w.ReasonRevoke == 0
                                    && w.IsDelete == false).ToList();
                    foreach (var item in listOrder)
                    {
                        if (item.CreatedDate.Value.DayOfWeek == dOW)
                        {
                            od += 1;
                            if (item.DepartmentId == 1)
                            {
                                odS += 1;
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                   // var number = listOrder.Count;
                    numberOrder = Convert.ToInt32(_numberOrder) - od - orderToday;
                    numberStylist = (int)_numberStylist - odS - orderStylistToday;
                }
            }
            catch (Exception e)
            {

                throw;
            }
        }
        /// <summary>
        /// Bind Region to ddl
        /// </summary>
        protected void BindRegion()
        {
            using (var db = new Solution_30shineEntities())
            {
                //chỉ lấy bộ phận stylist, skinner
                var types = db.TinhThanhs.OrderBy(o => o.ID).ToList();
                var item = new TinhThanh();
                item.ID = 0;
                item.TenTinhThanh = "Chọn khu vực";
                types.Insert(0, item);
                ddlRegion.DataTextField = "TenTinhThanh";
                ddlRegion.DataValueField = "Id";
                ddlRegion.DataSource = types;
                ddlRegion.DataBind();
            }
        }

        /// <summary>
        /// Bind Department to ddl
        /// </summary>
        [WebMethod]
        public static List<Tbl_Salon> BindSalonByRegion(int RegionId)
        {
            using (var db = new Solution_30shineEntities())
            {
                //chỉ lấy bộ phận stylist, skinner
                if (RegionId <= 0)
                {
                    return new List<Tbl_Salon>();
                }
                var listSalon = new List<Tbl_Salon>();
                var SalonId = Convert.ToInt32(HttpContext.Current.Session["SalonId"]);
                var UserId = Convert.ToInt32(HttpContext.Current.Session["User_Id"]);
                if (ViewAllData)
                {
                    listSalon = db.Tbl_Salon.Where(w => w.IsDelete == 0 && w.Publish == true && ((w.CityId == RegionId) || (RegionId == 0))).OrderByDescending(o => o.CityId).ToList();
                }
                else
                {
                    listSalon = (
                                        from p in db.PermissionSalonAreas.AsNoTracking()
                                        join s in db.Tbl_Salon.AsNoTracking() on p.SalonId equals s.Id
                                        where
                                        p.IsDelete == false && p.StaffId == UserId
                                        && s.Publish == true && s.IsDelete == 0 && ((s.CityId == RegionId) || (RegionId == 0))
                                        select s
                                   ).OrderByDescending(o => o.CityId).ToList();
                    if (listSalon.Count <= 0)
                    {
                        listSalon = db.Tbl_Salon.Where(w => w.Id == SalonId).ToList();
                    }
                }
                return listSalon;
            }
        }

        /// <summary>
        /// Bind Type Request
        /// </summary>
        [WebMethod]
        public static List<Tbl_Config> BindTypeRequest()
        {
            using (var db = new Solution_30shineEntities())
            {
                return db.Tbl_Config.Where(w => w.IsDelete == 0 && w.Status == 1 && w.Key == "recruitment_type").OrderBy(o => o.Id).ToList();
            }
        }

        /// <summary>
        /// insert
        /// </summary>
        [WebMethod(EnableSession = true)]
        public static bool CompleteListing(List<OrderRecruitmentStaffSub> list)
        {
            try
            {
                var db = new Solution_30shineEntities();
                var staffId = Convert.ToInt32(HttpContext.Current.Session["User_Id"]);
                var lastDate = DateTime.Now.AddDays(-7);
                var valid = false;
                var orderDay = db.Tbl_Config.FirstOrDefault(w => w.Key == "order_recruitment_day" && w.IsDelete == 0).Value;
                var permission = db.PermissionStaffs.Where(a => a.IsDelete == false && a.IsActive == true && a.StaffId == staffId).ToList();
                var permissionOrder = db.Tbl_Config.FirstOrDefault(w => w.Key == "permission_order_recruitment" && w.IsDelete == 0).Value;
                var _permissionOrder = permissionOrder.Split(',').ToList();
                var validPerm = false;
                foreach (var item in permission)
                {
                    validPerm = _permissionOrder.Contains(item.PermissionId.ToString());

                }
                if (DateTime.Now.DayOfWeek.ToString() == orderDay && validPerm)
                {
                        valid = true;
                }
                
                if (valid)
                {
                    var creatorId = int.TryParse(HttpContext.Current.Session["User_Id"].ToString(), out var integer) ? integer : 0;
                    int od = 0;
                    int odS = 0;
                    int numberOrder = 0;
                    int numberStylist = 0;
                    var percentStylist = db.Tbl_Config.FirstOrDefault(a => a.Key == "number_order_recruitment_stylist" && a.IsDelete == 0).Value;
                    var _numberOrder = db.Tbl_Config.FirstOrDefault(a => a.Key == "number_order_recruitment" && a.IsDelete == 0).Value;
                    var _numberStylist = Convert.ToInt32(_numberOrder) * Convert.ToDouble(percentStylist) / 100;


                    var orderToday = db.OrderRecruitingStaffs.Where(w => EntityFunctions.TruncateTime(w.CreatedDate) == EntityFunctions.TruncateTime(DateTime.Now) && w.IsDelete == false && w.ReasonRevoke == 0).ToList().Count;
                    var orderStylistToday = db.OrderRecruitingStaffs.Where(w => EntityFunctions.TruncateTime(w.CreatedDate) == EntityFunctions.TruncateTime(DateTime.Now) && w.IsDelete == false && w.DepartmentId == 1 && w.ReasonRevoke == 0).ToList().Count;
                    var listOrder =
                        db.OrderRecruitingStaffs.Where(w => EntityFunctions.TruncateTime(w.CreatedDate) < EntityFunctions.TruncateTime(DateTime.Now)
                                    && EntityFunctions.TruncateTime(w.CreatedDate) >= EntityFunctions.TruncateTime(lastDate)
                                    && w.Status != 5 && w.ReasonRevoke == 0
                                    && w.IsDelete == false).ToList();
                    foreach (var item in listOrder)
                    {
                        if (item.CreatedDate.Value.DayOfWeek == DateTime.Now.DayOfWeek)
                        {
                            od += 1;
                            if (item.DepartmentId == 1)
                            {
                                odS += 1;
                            }
                        }
                        else
                        {
                            continue;
                        }
                    }
                    // var number = listOrder.Count;
                    numberOrder = Convert.ToInt32(_numberOrder) - od - orderToday;
                    numberStylist = (int)_numberStylist - odS - orderStylistToday;
                    //counting number order
                    if (numberOrder <= 0)
                    {
                        return false;
                    }
                    IOrderRecruitingStaffModel OrderRecruitingStaffModel = new OrderRecruitingStaffModel();
                    if (list.Count <= 0)
                    {
                        return false;
                    }
                    var departmentId = list.FirstOrDefault().DepartmentId;
                    if (numberStylist <= 0 && departmentId == 1)
                    {
                        return false;
                    }
                    OrderRecruitingStaffModel.Add(list, creatorId);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void CheckOrderValid(string dayOfWeek)
        {
            try
            {
                var db = new Solution_30shineEntities();
                var staffId = Convert.ToInt32(Session["User_Id"]);
                var orderDay = db.Tbl_Config.FirstOrDefault(w => w.Key == "order_recruitment_day" && w.IsDelete == 0).Value;
                var permission = db.PermissionStaffs.Where(a => a.IsDelete == false && a.IsActive == true && a.StaffId == staffId).ToList();
                var permissionOrder = db.Tbl_Config.FirstOrDefault(w => w.Key == "permission_order_recruitment" && w.IsDelete == 0).Value;
                var _permissionOrder = permissionOrder.Split(',').ToList();
                var validPerm = false;
                foreach (var item in permission)
                {
                    validPerm = _permissionOrder.Contains(item.PermissionId.ToString());

                }
                
                
                if (dayOfWeek == orderDay && validPerm)
                {
                    validOrder = true;
                }
                else validOrder = false;
            }
            catch (Exception e)
            {

                throw e;
            }
        }
    }
    
    public partial class OrderRecruitmentStaffSub
    {
        public int? RegionId { get; set; }
        public int? SalonId { get; set; }
        public int? DepartmentId { get; set; }
        public int? Type { get; set; }
        public int? Quantity { get; set; }
        public string Needed { get; set; }
    }
    public class OrderRecruitmentStaffSubTest
    {
        public int? RegionId { get; set; }
        public int? SalonId { get; set; }
        public int? DepartmentId { get; set; }
        public int? Type { get; set; }
        public int? Quantity { get; set; }
        public string Needed { get; set; }
    }

}