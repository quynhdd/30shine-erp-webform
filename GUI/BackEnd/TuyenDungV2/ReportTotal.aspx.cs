﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Linq.Expressions;
using ExportToExcel;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.TuyenDungV2
{
    public partial class ReportTotal : System.Web.UI.Page
    {
        private string PageID = "TD_DS_DD";
        protected Paging PAGING = new Paging();

        protected string THead = "";
        protected string StrProductPrice = "";
        public CultureInfo culture = new CultureInfo("vi-VN");
        private DateTime timeFrom;
        private DateTime timeTo;
        private int departmentId;
        private int statusId;
        private int statusinteger;
        private int integer;
        protected string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
        protected string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);

        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

        protected bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected string Permission = "";
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = FirstDayOfMonth;
                TxtDateTimeTo.Text = Today;
                bindDepartment();
                bindStatus();
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(this.GetType(), "test", "<script>$('#ViewData').click();</script>");
                //Exc_Filter();
            }
            RemoveLoading();
        }

        private void bindDepartment()
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).ToList();
                var item = new Staff_Type();
                item.Name = "Chọn bộ phận";
                item.Id = 0;
                list.Insert(0, item);
                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "Id";
                ddlDepartment.DataSource = list;
                ddlDepartment.DataBind();
            }
        }
        private void bindStatus()
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = db.TuyenDung_Status.Where(a => a.Id != 1 && a.IsDelete == false).ToList();
                var item = new TuyenDung_Status();
                item.StatusName = "Trạng thái";
                item.Id = 0;
                list.Insert(0, item);
                ddlStatus.DataTextField = "StatusName";
                ddlStatus.DataValueField = "Id";
                ddlStatus.DataSource = list;
                ddlStatus.DataBind();
            }
        }

        private void bindData()
        {
            using (var db = new Solution_30shineEntities())
            {
                try
                {
                    if (TxtDateTimeFrom.Text != "")
                    {
                        timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                        if (TxtDateTimeTo.Text != "")
                        {
                            timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                        }
                        else
                        {
                            timeTo = timeFrom.AddDays(1);
                        }
                        departmentId = int.TryParse(ddlDepartment.SelectedValue, out integer) ? integer : 0;
                        statusId = int.TryParse(ddlStatus.SelectedValue, out integer) ? integer : 0;
                        var lstData = (from td in db.TuyenDung_UngVien
                                       join st in db.Staff_Type on td.DepartmentId equals st.Id into temp
                                       from st in temp.DefaultIfEmpty()
                                       join tds in db.TuyenDung_Status on td.UngVienStatusId equals tds.Id into temp1
                                       from tds in temp1.DefaultIfEmpty()
                                       join salon in db.Tbl_Salon on td.salonId equals salon.Id into temp2
                                       from salon in temp2.DefaultIfEmpty()
                                       where
                                       ((td.UngVienStatusId == statusId) || (statusId == 0))
                                       && ((td.DepartmentId == departmentId) || (departmentId == 0)) 
                                       && ((td.Step1Time >= timeFrom) && (td.Step1Time <= timeTo))
                                       && td.IsDelete == false
                                       select new
                                       {
                                           Id = td.Id,
                                           StaffId = td.StaffId,
                                           SalonId = salon.Id,
                                           SalonName = salon.Name,
                                           FullName = td.FullName,
                                           Phone = td.Phone,
                                           DepartmentId = st.Id,
                                           DepartmentName = st.Name,
                                           StatusName = tds.StatusName
                                       }).OrderBy(w => w.StaffId).ThenBy(w => w.SalonId).ThenBy(w => w.DepartmentId).ToList();
                            //var data = db.Store_TuyenDung_Step3_daduyet_Listing(String.Format("{0:yyyy/MM/dd}", timeFrom), String.Format("{0:yyyy/MM/dd}", timeTo), departmentId, statusId).ToList();
                            Bind_Paging(lstData.Count);
                            rptDanhsach.DataSource = lstData.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                            rptDanhsach.DataBind();
                    }
                }
                catch { }
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            bindData();
            RemoveLoading();
        }

        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        
    }
}