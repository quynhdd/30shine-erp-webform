﻿<%@ Page Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="ConfirmTryWork.aspx.cs" Inherits="_30shine.GUI.BackEnd.TuyenDungV2.ConfirmTryWork" %>

<asp:Content ID="ProductAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <link href="/Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="/Assets/js/select2/select2.min.js"></script>
        <link href="/Assets/js/fancybox/jquery.fancybox.min.css" rel="stylesheet" />
        <script src="/Assets/js/fancybox/jquery.fancybox.min.js"></script>
        <style>
            .btn-viewdata {
                background: black;
                color: white;
            }

            .margin-right {
                margin-right: 12px;
            }

            #success-alert {
                top: 100px;
                right: 10px;
                position: fixed;
                width: 20% !important;
                z-index: 2000;
            }

            .btn-viewdata:hover {
                color: yellow;
            }

            #radioApprove tr {
                display: inline-table;
                padding-right: 15px;
                cursor: pointer;
            }

                #radioApprove tr td input {
                    margin-left: 15px;
                    cursor: pointer;
                }

                #radioApprove tr td label {
                    margin-left: 5px;
                    position: relative;
                    top: -1px;
                    cursor: pointer;
                }

            .listing-img-upload .thumb-wp {
                border: none;
            }
            /*.listing-img-upload .thumb-wp img { position: relative; width: auto !important; height: auto !important; max-width: 100%; }*/
            .ddl {
                width: 100%;
            }

            .table-wp {
                background: #f5f5f5;
                float: left;
                width: 100%;
                padding: 15px;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                -webkit-box-shadow: 0px 0px 3px 0px #ddd;
                -moz-box-shadow: 0px 0px 3px 0px #ddd;
                box-shadow: 0px 0px 3px 0px #ddd;
            }

            .btn {
                border-radius: 5px;
                margin-left: 10px;
                margin-right: 10px;
                height: 70px;
            }

                .btn:hover {
                    background-color: orangered;
                    color: white;
                }

            .send {
                box-shadow: 2px 2px #b0b0b0;
                float: left;
            }

            .senddd {
                box-shadow: 2px 2px #b0b0b0;
                float: left;
                width: 100px;
                height: 50px;
            }

                .senddd:hover {
                    background-color: #2b8a05;
                    color: white;
                }

            .cancel {
                border-radius: 5px;
            }

            .canceldd {
                float: right;
            }

            .cancel:hover {
                background-color: #50b347;
                color: white;
            }

            .table-wp {
                background: #f5f5f5;
                float: left;
                width: 100%;
                padding: 15px;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                -webkit-box-shadow: 0px 0px 3px 0px #ddd;
                -moz-box-shadow: 0px 0px 3px 0px #ddd;
                box-shadow: 0px 0px 3px 0px #ddd;
            }

            .tr-skill {
                display: none;
            }

            .tr-show {
                display: table-row;
            }

            .customer-add .table-add tr td.right .fb-cover-error {
                top: 0;
                right: auto;
            }

            .gallery {
                display: inline-block;
                margin-top: 20px;
            }

            .thumbnailImage {
                margin-bottom: 6px;
                width: 100%;
            }

            .modal-image {
                display: none;
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */ /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
            }

            /* Modal Content (image) */
            .modal-content-image {
                margin: auto;
                margin-top: 30px;
                display: block;
                width: 50%;
                max-width: 500px;
                height: auto;
                max-height: 600px;
            }

            /* Caption of Modal Image */
            #caption-image {
                margin: auto;
                display: block;
                width: 60%;
                max-width: 700px;
                text-align: center;
                color: #ccc;
                padding: 10px 0;
                height: 150px;
                margin-top: 10px;
            }

            /* Add Animation */
            .modal-content-image, #caption-image {
                -webkit-animation-name: zoom;
                -webkit-animation-duration: 0.6s;
                animation-name: zoom;
                animation-duration: 0.6s;
            }

            @-webkit-keyframes zoom {
                from {
                    -webkit-transform: scale(0);
                }

                to {
                    -webkit-transform: scale(1);
                }
            }

            @keyframes zoom {
                from {
                    transform: scale(0.1);
                }

                to {
                    transform: scale(1);
                }
            }

            /* The Close Button */
            .close-image {
                position: absolute;
                top: 15px;
                right: 35px;
                color: #f1f1f1;
                font-size: 40px;
                font-weight: bold;
                transition: 0.3s;
            }

                .close-image:hover,
                .close-image:focus {
                    color: #bbb;
                    text-decoration: none;
                    cursor: pointer;
                }

            .btn-send:hover {
                background: #50b347;
                color: #ffffff;
            }

            .btn-send {
                width: auto !important;
                height: 30px;
                line-height: 30px;
                float: left;
                margin-right: 20px;
                margin-top: 20px;
                background: #ddd;
                text-align: center;
                cursor: pointer;
                border-radius: 2px;
            }

            .one {
                background: #ddd;
                border-radius: 5px;
                width: auto;
                padding-top: 5px;
                padding-bottom: 5px;
                height: 30px;
                line-height: 30px;
            }

                .one:hover {
                    background: #50b347;
                    color: #ffffff;
                }

            .col-3 {
                width: 30% !important
            }
        </style>
        <div class="alert alert-danger" id="success-alert">
            <%--<button type="button" class="close" data-dismiss="alert">x</button>--%>
            <strong>Cảnh báo: </strong>
            <span id="msg-alert"></span>
        </div>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Tuyển dụng &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/admin/tuyen-dung/lap-ho-so/danh-sach-ho-so.html">Danh sách ứng viên</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add admin-product-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="table-wp">
                    <table class="table-add admin-product-table-add">
                        <tbody>
                            <tr class="title-head ">
                                <td><strong>Các kỹ năng của ứng viên</strong></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="myModalimage" style="z-index: 2000;" class="modal modal-image">
                                        <img class="modal-content-image" id="img01" />
                                        <div id="captionimage"></div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>ID Nhân Viên</span></td>
                                <td class="col-xs-3 right">
                                    <asp:TextBox ID="ID" CssClass="form-control" runat="server" ClientIDMode="Static" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Tài khoản</span></td>
                                <td class="col-xs-3 right">
                                    <asp:TextBox ID="Account" CssClass="form-control" runat="server" ClientIDMode="Static" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Họ tên</span></td>
                                <td class="col-xs-3 right">
                                    <asp:TextBox ID="FullName" CssClass="form-control" runat="server" ClientIDMode="Static" Enabled="false" placeholder="Họ tên" Style="width: 220px;"></asp:TextBox>
                                    <% if (OBJ != null && OBJ.MainImg != null && OBJ.MainImg != "")
                                        { %>
                                    <div class="gallery" style="float: left; cursor: pointer; z-index: 200; position: absolute; top: -14px; width: auto; padding-left: 20px;">

                                        <img class="example-image form-control" id="myImg1" alt="" title="" src="<%=OBJ.MainImg %>"
                                            data-img="<%=OBJ.MainImg %>" style="width: 100%; height: auto; max-height: 450px; left: 0px; max-width: 100%;" />
                                    </div>
                                    <% } %> 
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Salon</span></td>
                                <td class="col-xs-3 right ">
                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlSalon" Style="width: 220px;" ClientIDMode="Static" Enabled="false"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Bộ phận</span></td>
                                <td class="col-xs-3 right ">
                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlDepartment" Style="width: 220px;" ClientIDMode="Static" Enabled="false"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Số điện thoại</span></td>
                                <td class="col-xs-3 right"><span>
                                    <asp:TextBox ID="Phone" runat="server" CssClass="form-control" ClientIDMode="Static" Enabled="false" Style="width: 220px;"></asp:TextBox>
                                </span></td>
                            </tr>
                             <tr>
                                <td class="col-xs-3 left"><span>Số CMT</span></td>
                                <td class="col-xs-3 right ">
                                    <asp:TextBox ID="CMT" CssClass="form-control" runat="server" ClientIDMode="Static" Style="width: 220px;" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                              <tr>
                                <td class="col-xs-3 left"><span>Ngày cấp</span></td>
                                <td class="col-xs-3 right ">
                                    <asp:TextBox ID="CMTByDate" CssClass="form-control" runat="server" ClientIDMode="Static" Style="width: 220px;" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                              <tr>
                                <td class="col-xs-3 left"><span>Nơi cấp CMT</span></td>
                                <td class="col-xs-3 right ">
                                    <asp:TextBox ID="CMTProviderLocale" CssClass="form-control" runat="server" ClientIDMode="Static" Style="width: 220px;" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Nguồn LĐ</span></td>
                                <td class="col-xs-3 right">
                                    <span>
                                        <asp:DropDownList runat="server" CssClass="form-control" ID="Source" Style="width: 220px;" ClientIDMode="Static" Enabled="false"></asp:DropDownList>
                                    </span>

                                </td>
                            </tr>
                             <tr>
                                <td class="col-xs-3 left"><span>Tình trạng hôn nhân</span></td>
                                <td class="col-xs-3 right ">
                                    <asp:DropDownList runat="server" CssClass="form-control" ID="Relation" Style="width: 220px;" ClientIDMode="Static" Enabled="false"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Người kiểm tra kỹ năng</span></td>
                                <td class="col-xs-3 right ">
                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlTester" Style="width: 220px;" ClientIDMode="Static" Enabled="false"></asp:DropDownList>
                                </td>
                            </tr>
                            <%-- Modal show image --%>

                            <tr class=" tr-upload tr-skill tr-skill-img">
                                <td class="col-xs-3 left"><span>Ảnh kỹ năng</span></td>
                                <td class="col-xs-9 right image">
                                    <div class="wrap btn-upload-wp HDF_IMG_SKILL" style="margin-bottom: 5px; width: auto;">
                                        <div class="wrap listing-img-upload image">
                                            <% if (OBJ != null && OBJ.ImgSkill1 != null && OBJ.ImgSkill1 != "")
                                                { %>
                                            <div class="gallery">
                                                <img class="img-rounded myImg form-control" alt="" id="myImg2" title="Image 3" src="<%=OBJ.ImgSkill1 %>"
                                                    data-img="<%=OBJ.ImgSkill1 %>" style="width: 250px; max-height: 200px; height: auto; left: 0px; margin-right: 9px;" />
                                            </div>
                                            <% } %>
                                            <% if (OBJ != null && OBJ.ImgSkill2 != null && OBJ.ImgSkill2 != "")
                                                { %>
                                            <div class="gallery">
                                                <img class="img-rounded myImg form-control" alt="" id="myImg3" title="Image 4" src="<%=OBJ.ImgSkill2 %>"
                                                    data-img="<%=OBJ.ImgSkill2 %>" style="width: 250px; height: auto; max-height: 200px" />
                                            </div>
                                            <% } %>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr class="tr-description">
                                <td class="col-xs-2 left"><span>Test văn hóa</span></td>
                                <td class="col-xs-9 right " colspan="2">
                                    <span class="field-wp">
                                        <span>
                                            <asp:TextBox ID="TestVH" CssClass="form-control col-3 margin-right" runat="server" ClientIDMode="Static" Style="width: 220px;" placeholder="Test VH" Enabled="false"></asp:TextBox>
                                            <asp:TextBox ID="PassVH" CssClass="form-control col-3 margin-right" runat="server" ClientIDMode="Static" Style="width: 220px;" Enabled="false"></asp:TextBox>
                                            <asp:TextBox ID="TestVHAgain" CssClass="form-control col-3" runat="server" ClientIDMode="Static" Style="width: 220px;" Enabled="false"></asp:TextBox>
                                        </span>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-description">
                                <td class="col-xs-2 left"><span>Test kỹ năng cắt</span></td>
                                <td class="col-xs-9 right " colspan="2">
                                    <span class="field-wp">
                                        <span>
                                            <asp:TextBox ID="TestCut" CssClass="form-control col-3 margin-right" runat="server" ClientIDMode="Static" Style="width: 220px;" placeholder="Test kỹ năng cắt" Enabled="false"></asp:TextBox>
                                            <asp:TextBox ID="PassCut" CssClass="form-control col-3 margin-right" runat="server" ClientIDMode="Static" Style="width: 220px;" Enabled="false"></asp:TextBox>
                                            <asp:TextBox ID="TestCutAgain" CssClass="form-control col-3" runat="server" ClientIDMode="Static" Style="width: 220px;" Enabled="false"></asp:TextBox>
                                        </span>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-description">
                                <td class="col-xs-2 left"><span>Test kỹ năng hóa chất</span></td>
                                <td class="col-xs-9 right " colspan="2">
                                    <span class="field-wp">
                                        <span>
                                            <asp:TextBox ID="TestChemistry" CssClass="form-control col-3 margin-right" runat="server" ClientIDMode="Static" Style="width: 220px;" placeholder="Test kỹ năng hóa chất" Enabled="false"></asp:TextBox>
                                            <asp:TextBox ID="PassChemistry" CssClass="form-control col-3 margin-right" runat="server" ClientIDMode="Static" Style="width: 220px;" Enabled="false"></asp:TextBox>
                                            <asp:TextBox ID="TestChemistryAgain" CssClass="form-control col-3" runat="server" ClientIDMode="Static" Style="width: 220px;" Enabled="false"></asp:TextBox>
                                        </span>
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Kỹ năng</span></td>
                                <td>
                                    <asp:Repeater runat="server" ID="rptSkillName" ClientIDMode="Static" OnItemDataBound="rptSkillName_ItemDataBound">
                                        <ItemTemplate>
                                            <tr>
                                                <td class="col-xs-3 right"></td>
                                                <td class="col-xs-9 right">
                                                    <span style="width: 230px">
                                                        <label id="lblSkill" class="lbl-cus-no-infor" value="<%#Eval("IdSkill") %>" style="margin-bottom: 8px; cursor: pointer; font-weight: normal; font-size: 13px; padding: 2px 10px; background: #ddd; line-height: 33px; float: left;">
                                                            <%#Eval("SkillName") %>
                                                        </label>
                                                    </span>
                                                    <span style="width: 354px">
                                                        <asp:DropDownList runat="server" ID="drlLevelName" Enabled="false"></asp:DropDownList>
                                                    </span>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="3" style="width: 10px"></td>
                            </tr>
                            <tr class="tr-description">
                                <td class="col-xs-2 left"><span>Ghi chú</span></td>
                                <td class="col-xs-9 right " colspan="2">
                                    <span class="field-wp">
                                        <asp:TextBox TextMode="MultiLine" CssClass="form-control" Rows="2" ID="Step3Note" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <td></td>
                                <td class="text-center" colspan="2">
                                    <asp:Button ID="btnDuyet" CssClass="st-head btn-viewdata" Height="30px" Width="100px" runat="server" Text="Duyệt thử việc" ClientIDMode="Static" OnClick="btnDuyet_Click"></asp:Button>

                                    <asp:Button ID="btnKhongDuyet" CssClass="st-head btn-viewdata" Height="30px" Width="100px" runat="server" Text="Không nhận" ClientIDMode="Static" OnClientClick="if(!DoValidation()) return false;" OnClick="btnKhongDuyet_Click"></asp:Button>
                                </td>
                            </tr>
                            <tr class="tr-send">
                                <td class="col-xs-3 left"></td>
                                <td class="col-xs-9 right no-border"></td>
                            </tr>
                            <asp:HiddenField runat="server" ID="HiddenFieldUVId" ClientIDMode="Static" />
                            <asp:HiddenField ID="HDF_MainImg" runat="server" ClientIDMode="Static" />
                        </tbody>
                    </table>
                </div>
            </div>
            <%-- end Add --%>
        </div>
        <script type="text/javascript">
            $("#success-alert").hide();
            $('select').select2();
        </script>
        <script>
            function DoValidation() {
                // Do calls to validation services here
                var valid = true;
                var errorString = '';
                var note = $("#Step3Note").val();



                if (note == "" || note == null) {
                    valid = false;
                    errorString += "Vui lòng nhập lý do không nhận ứng viên";
                    $("#Step3Note").css("border-color", "pink");
                    $("#Step3Note").focus();
                }


                if (!valid) {
                    $("#success-alert").fadeTo(2000, 1000).slideUp(1000, function () {
                        $("#success-alert").slideUp(1000);
                    });
                    $("#msg-alert").text(errorString);

                }

                return valid;
            }
            function choseDepartment(This) {
                var id = parseInt($("#ddlDepartment").val());
                //console.log(id);
                if (!isNaN(id)) {
                    if (id == 1) {
                        $(".tr-skill").removeClass('tr-show');
                        $(".tr-skill-img").addClass("tr-show");
                        $(".tr-skill-btn").addClass("tr-show");
                        $(".tr-skill-text").addClass("tr-show");
                    }
                    else if (id == 2) {
                        $(".tr-skill").removeClass('tr-show');
                        $(".tr-skill-video").addClass("tr-show");
                        $(".tr-skill-text").addClass("tr-show");
                    }
                    else if (id == 0) {
                        $(".tr-skill").removeClass('tr-show');
                    }
                    else {
                        $(".tr-skill").removeClass('tr-show');
                    }
                }
                else {
                    $(".tr-skill").removeClass('tr-show');
                }
            }
            jQuery(document).ready(function () {

                $("#glbAdminContent").addClass("active");
                $("#glbAdminCategory").addClass("active");
                //date picker
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        //$input.val($input.val().split(' ')[0]);
                    }
                });
                //excThumbWidth(120, 120);

                choseDepartment(null);
            });
            //show image
            var modal = document.getElementById('myModalimage');
            // Get the image and insert it inside the modal - use its "alt" text as a caption
            var img = document.getElementById('myImg1');
            var img1 = document.getElementById('myImg2');
            var img2 = document.getElementById('myImg3');
            var img3 = document.getElementById('myImg4');
            var modalImg = document.getElementById("img01");
            var captionText = document.getElementById("captionimage");

            try {
                img.onclick = function () {

                    modal.style.display = "block";
                    modalImg.src = this.src;
                    captionText.innerHTML = this.alt;
                }
                img1.onclick = function () {
                    modal.style.display = "block";
                    modalImg.src = this.src;
                    captionText.innerHTML = this.alt;
                }
                img2.onclick = function () {
                    modal.style.display = "block";
                    modalImg.src = this.src;
                    captionText.innerHTML = this.alt;
                }
                img3.onclick = function () {
                    modal.style.display = "block";
                    modalImg.src = this.src;
                    captionText.innerHTML = this.alt;
                }
                // When the user clicks on <span> (x), close the modal
                modal.onclick = function () {
                    console.log("a");
                    modal.style.display = "none";
                }
            } catch (err) { }


            $('#myModalimage').click(function () {
                $("#myModalimage").css("display", "none")
            });
            $('.modal-content-image').click(function (event) {
                event.stopPropagation();
            });


        </script>
        <!-- Popup plugin image hay là chưa gọi hàm getIdLevel nên nó k chạy  -->
        <script type="text/javascript">
            function popupImageIframe1(StoreImgField) {
                var Width = 960;
                var Height = 560;
                var ImgList = [];
                var iframe = $("<div class='iframe-image-upload' style='width:" + Width + "px; height:" + Height + "px; float:left; display:none;'><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + StoreImgField + "&callback=getThumbFromIframe&imgWidth=800&imgHeight=600&multi_img=false&Folder=Common' style='width:100%; height: 100%;'></iframe></div");

                $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
            }
            function popupImageIframe(StoreImgField) {
                var Width = 960;
                var Height = 560;
                var ImgList = [];
                var iframe = $("<div class='iframe-image-upload' style='width:" + Width + "px; height:" + Height + "px; float:left; display:none;'><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + StoreImgField + "&callback=getThumbFromIframe&imgWidth=800&imgHeight=600&multi_img=false&Folder=Common' style='width:100%; height: 100%;'></iframe></div");

                $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
            }

            window.getThumbFromIframe = function (Imgs, StoreImgField) {
                var imgs = "";
                var thumb = "";
                if (Imgs.length > 0) {
                    thumb = '<div class="thumb-wp">' +
                        '<img class="thumb" alt="" title="" src="' + Imgs[0] + '"' +
                        'data-img="" />' +
                        '<span class="delete-thumb" onclick="deleteThumbnail($(this), \'' + StoreImgField + '\')"></span>' +
                        '</div>';
                    $("." + StoreImgField).find(".listing-img-upload").empty().append(thumb);
                    //excThumbWidth(120, 120);
                    $("#" + StoreImgField).val(Imgs[0]);
                }
                autoCloseEBPopup();
            }

            function deleteThumbnail(This, StoreImgField) {
                var imgs = "";
                var lst = This.parent().parent();
                This.parent().remove();
                lst.find("img.thumb").each(function () {
                    imgs += $(this).attr("src") + ",";
                });
                $("#" + StoreImgField).val(imgs);
            }

        </script>
    </asp:Panel>
</asp:Content>

