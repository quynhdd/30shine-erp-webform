﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Linq.Expressions;
using ExportToExcel;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.TuyenDungV2
{
    public partial class ReportCV : System.Web.UI.Page
    {
        private string PageID = "TD_DS_HS";

        protected List<UngVien> uv = new List<UngVien>();

        protected string THead = "";
        protected string StrProductPrice = "";
        public CultureInfo culture = new CultureInfo("vi-VN");
        private DateTime timeFrom;
        private DateTime timeTo;
        public int departmentId;
        private int integer;
        protected string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
        protected string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);

        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

        protected bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected string Permission = "";
        public string _DepartmentId = "";
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            //bindData();
            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = FirstDayOfMonth;
                TxtDateTimeTo.Text = Today;
                bindDepartment();
                bindData();
            }
            else
            {
                // Page.ClientScript.RegisterStartupScript(this.GetType(), "test", "<script>$('#ViewData').click();</script>");
                //Exc_Filter();
            }
            RemoveLoading();
        }

        [WebMethod]
        public static void updateStatus(int uID)
        {
            try
            {
                Solution_30shineEntities db = new Solution_30shineEntities();
                var obj = db.TuyenDung_UngVien.Where(u => u.Id == uID).FirstOrDefault();
                obj.UngVienStatusId = 4;
                db.TuyenDung_UngVien.AddOrUpdate(obj);
                db.SaveChanges();
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }
        private void bindDepartment()
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).ToList();
                var item = new Staff_Type();
                item.Name = "Chọn bộ phận";
                item.Id = 0;
                list.Insert(0, item);
                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "Id";
                ddlDepartment.DataSource = list;
                ddlDepartment.DataBind();
            }
        }

        private void bindData()
        {
            using (var db = new Solution_30shineEntities())
            {
                try
                {
                    if (TxtDateTimeFrom.Text != "")
                    {
                        timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                        if (TxtDateTimeTo.Text != "")
                        {
                            timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                        }
                        else
                        {
                            timeTo = timeFrom.AddDays(1);
                        }
                    }
                    departmentId = int.TryParse(ddlDepartment.SelectedValue, out integer) ? integer : 0;

                    uv = db.Database.SqlQuery<UngVien>(string.Format("exec [Store_TuyenDung_Step1_Listing] '{0}', '{1}', {2}", String.Format("{0:yyyy/MM/dd}", timeFrom), String.Format("{0:yyyy/MM/dd}", timeTo), departmentId)).ToList();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            bindData();
            //RemoveLoading();
        }




        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        
        public class UngVien
        {
            public int Id { get; set; }

            public string Fullname { get; set; }
            public string Phone { get; set; }
            public string Status { get; set; }
            public string Department { get; set; }

        }
    }
}