﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.TuyenDungV2
{
    public partial class CreateTester : System.Web.UI.Page
    {
        private string PageID = "";
        protected string _Code;
        protected TuyenDung_NguoiTest OBJ;
        protected bool _IsUpdate = false;
        protected bool Perm_Access = false;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //if (Request.QueryString["Code"] != null)
                //{
                //    PageID = "TD_TESTER_EDIT";
                //}
                //else
                //{
                //    PageID = "TD_TESTER_ADD";
                //}
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                ////Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                if (IsUpdate())
                {
                    bindOBJ();
                }
            }
           

        }

        protected bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }

        protected void bindOBJ()
        {
            var __Code = Convert.ToInt32(Request.QueryString["Code"]);
            Solution_30shineEntities db = new Solution_30shineEntities();
            OBJ = db.TuyenDung_NguoiTest.FirstOrDefault(t => t.Id == __Code);
            Fullname.Text = OBJ.FullName.Trim();
            //txtStaffID.Text = OBJ.StaffId.ToString().Trim();
            Phone.Text = OBJ.Telephone.Trim();
            Email.Text = OBJ.Email.Trim();
        }

        protected void Add()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _obj = new TuyenDung_NguoiTest();
                _obj.FullName = Fullname.Text;
                _obj.Email = Email.Text;
                _obj.Telephone = Phone.Text;
                _obj.IsDelete = false;
                //_obj.StaffId = Convert.ToInt32(txtStaffID.Text);
                db.TuyenDung_NguoiTest.Add(_obj);
                db.SaveChanges();
                if (db.SaveChanges() > 0)
                {
                    UIHelpers.Redirect("/admin/tuyen-dung/danh-sach-user-test.html");
                }
                else
                {
                    var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }
            }
        }
        protected void Update()
        {
            try
            {


                using (var db = new Solution_30shineEntities())
                {
                    var __Code = Convert.ToInt32(Request.QueryString["Code"]);
                    var _OBJ = db.TuyenDung_NguoiTest.FirstOrDefault(t => t.Id == __Code);
                    _OBJ.FullName = Fullname.Text;
                    //_OBJ.StaffId = Convert.ToInt32(txtStaffID.Text);
                    _OBJ.Email = Email.Text;
                    _OBJ.Telephone = Phone.Text;
                    db.TuyenDung_NguoiTest.AddOrUpdate(_OBJ);
                    db.SaveChanges();
                    if (db.SaveChanges() > 0)
                    {
                        UIHelpers.Redirect("/admin/tuyen-dung/danh-sach-user-test.html");
                    }
                    else
                    {
                        //var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                        //var status = "warning";
                        //UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        protected void AddorUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
            {
                Update();
            }
            else
            {
                Add();
            }
        }

        #region get info staff by staffID
        [WebMethod]
        public static string get_Info_Staff(int staffID)
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
            var result = db.Staffs.FirstOrDefault(s => s.Id == staffID);

            return new JavaScriptSerializer().Serialize(result);

        }
        #endregion
    }
}