﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Data.Linq;
using System.Web.Services;
using System.Data.Entity.Migrations;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.TuyenDungV2
{

    public partial class V2_ItemSkill_List : System.Web.UI.Page
    {
        private string PageID = "TD_KN";
       // protected Paging PAGING = new Paging();
        protected List<Skill> skill = new List<Skill>();
        private bool Perm_Access = false;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                ////Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                BindList();
                //Bind_Paging();
            }
        }
        #region[Bind_Danhsach]
        private void BindList()
        {
            using (var db = new Solution_30shineEntities())
            {
                skill = db.Database.SqlQuery<Skill>("select Id as Id, SkillName as skillName, Description as Description, Publish as Publish from TuyenDung_Skill where isDelete != 1").ToList();
                //    (from s in db.TuyenDung_Skill where s.IsDelete == false select new Skill {
                //    Id = s.Id,
                //    skillName = s.SkillName,
                //    Description = s.Description,
                //    Publish = Convert.ToBoolean(s.Publish),
                //}).ToList();

            }
        }
        #endregion
        #region[Paging]
        //protected void Bind_Paging()
        //{
        //    PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
        //    PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
        //    PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
        //    PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
        //    PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
        //    PAGING._Paging = PAGING.Make_Paging();

        //    RptPaging.DataSource = PAGING._Paging.ListPage;
        //    RptPaging.DataBind();
        //}
        //protected int Get_TotalPage()
        //{
        //    using (var db = new Solution_30shineEntities())
        //    {
        //        var Count = db.TuyenDung_Skill.Count(w => w.IsDelete == false);
        //        int TotalRow = Count - PAGING._TopNewsNum;
        //        int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
        //        return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        //    }
        //}
        #endregion
        

        [WebMethod]
        public static void PublishSkill(int skillID, int isPublish)
        {
            try
            {
                Solution_30shineEntities db = new Solution_30shineEntities();

                var obj = db.TuyenDung_Skill.FirstOrDefault(s => s.Id == skillID);
                obj.Publish = Convert.ToBoolean(isPublish);
                db.TuyenDung_Skill.AddOrUpdate(obj);
                db.SaveChanges();
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }
        public class Skill
        {
            public int Id { get; set; }
            public string skillName { get; set; }
            public string Description { get; set; }
            public bool Publish { get; set; }
        }
    }
}