﻿    <%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TemplateMaster/SiteMaster.Master" CodeBehind="ReportConfirm.aspx.cs" Inherits="_30shine.GUI.BackEnd.TuyenDungV2.ReportConfirm" %>

    <asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
        <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
        <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
            <style>
                .table-listing .uv-avatar {
                    width: 120px;
                }
            </style>
            <style>
                #radioApprove tr {
                    display: inline-table;
                    padding-right: 15px;
                    cursor: pointer;
                }

                #success-alert {
                    top: 100px;
                    right: 10px;
                    position: fixed;
                    width: 20% !important;
                    z-index: 2000;
                }

                #radioApprove tr td input {
                    margin-left: 15px;
                    cursor: pointer;
                }

                #radioApprove tr td label {
                    margin-left: 5px;
                    position: relative;
                    top: -1px;
                    cursor: pointer;
                }

                .listing-img-upload .thumb-wp {
                    border: none;
                }
                /*.listing-img-upload .thumb-wp img { position: relative; width: auto !important; height: auto !important; max-width: 100%; }*/
                .ddl {
                    width: 100%;
                }


                .customer-add .table-add tr td.right .fb-cover-error {
                    top: 0;
                    right: auto;
                }

                .gallery {
                    display: inline-block;
                    margin-top: 20px;
                }

                .thumbnailImage {
                    margin-bottom: 6px;
                    width: 100%;
                }

                .modal-image {
                    display: none;
                    position: fixed; /* Stay in place */
                    z-index: 1; /* Sit on top */ /* Location of the box */
                    left: 0;
                    top: 0;
                    width: 100%; /* Full width */
                    height: 100%; /* Full height */
                    overflow: auto; /* Enable scroll if needed */
                    background-color: rgb(0,0,0); /* Fallback color */
                    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
                }

                /* Modal Content (image) */
                .modal-content-image {
                    margin: auto;
                    margin-top: 30px;
                    display: block;
                    width: 50%;
                    max-width: 500px;
                    height: auto;
                    max-height: 600px;
                }

                /* Caption of Modal Image */
                #caption-image {
                    margin: auto;
                    display: block;
                    width: 60%;
                    max-width: 700px;
                    text-align: center;
                    color: #ccc;
                    padding: 10px 0;
                    height: 150px;
                    margin-top: 10px;
                }

                /* Add Animation */
                .modal-content-image, #caption-image {
                    -webkit-animation-name: zoom;
                    -webkit-animation-duration: 0.6s;
                    animation-name: zoom;
                    animation-duration: 0.6s;
                }

                @-webkit-keyframes zoom {
                    from {
                        -webkit-transform: scale(0);
                    }

                    to {
                        -webkit-transform: scale(1);
                    }
                }

                .btn-send {
                    /*padding:0px 30px;*/
                    background: black !important;
                    color: white;
                }

                    .btn-send:hover {
                        color: yellow !important;
                    }

                @keyframes zoom {
                    from {
                        transform: scale(0.1);
                    }

                    to {
                        transform: scale(1);
                    }
                }

                /* The Close Button */
                .close-image {
                    position: absolute;
                    top: 15px;
                    right: 35px;
                    color: #f1f1f1;
                    font-size: 40px;
                    font-weight: bold;
                    transition: 0.3s;
                }

                    .close-image:hover,
                    .close-image:focus {
                        color: #bbb;
                        text-decoration: none;
                        cursor: pointer;
                    }
            </style>
            <div class="wp sub-menu">
                <div class="wp960">
                    <div class="wp content-wp">
                        <ul class="ul-sub-menu" id="subMenu">
                            <li>Tuyển dụng &nbsp;&#187; </li>
                            <li class="li-listing active"><a href="/admin/tuyen-dung/lap-ho-so/danh-sach-ho-so.html">Danh sách ứng viên</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="wp customer-add customer-listing be-report be-report-timekeeping">
                <div class="wp960 content-wp">
                    <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
                    <div class="row">
                        <div class="filter-item">
                            <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                            <div class="time-wp">
                                <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeFrom" placeholder="Từ ngày" Style="margin-left: 10px!important"
                                    ClientIDMode="Static" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="ValidateTime" ControlToValidate="TxtDateTimeFrom" runat="server" CssClass="fb-cover-error" Text="Vui lòng chọn thời gian!"></asp:RequiredFieldValidator>
                            </div>
                            <strong class="st-head" style="margin-left: 10px;">
                                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                            </strong>
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeTo" placeholder="Đến ngày" Style="margin-left: 10px!important"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                            <br />

                        </div>
                        <%--                  <div class="filter-item">
                            <asp:DropDownList ID="ddlDepartment" runat="server" ClientIDMode="Static" Style="width: 220px;"></asp:DropDownList>
                        </div>--%>
                        <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                            onclick="excPaging(1)" runat="server">
                            Xem dữ liệu
                        </asp:Panel>
                        <a href="javascript:void(0);" onclick="location.href=location.href" class="st-head btn-viewdata">Reset Filter</a>

                    </div>
                    <div class="row">
                        <strong class="st-head"><i class="fa fa-file-text"></i>Danh sách ứng viên đã test kỹ năng</strong>
                    </div>
                
               
                            <div class="row table-wp">
                                <table class="table" id="tableUngVienDaTest">
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>ID Ứng viên</th>
                                            <th>Họ tên</th>
                                            <th>Số điện thoại</th>
                                            <th>Bộ phận</th>
                                            <th>Trạng thái</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <% 

                                            for (int i = 0; i < uvdt.Count; i++)
                                            {
                                                %>
                                                <tr>
                                                    <td><%= i+1%></td>
                                                    <td><%= uvdt[i].Id %></td>
                                                    <td><%= uvdt[i].Fullname %></td>
                                                    <td><%= uvdt[i].Phone %></td>
                                                    <td><%= uvdt[i].Department %></td>
                                                    <td><%
                                                            if (uvdt[i].Status == 0)
                                                            {
                                                                %>
                                                        Đã test xong kỹ năng, chờ duyệt
                                                        <%
                                                            }
                                                            else
                                                            {
                                                                %>
                                                        Chờ duyệt hồ sơ
                                                        <%
                                                            }

                                                             %></td>
                                                    <td>
                                                        <%
                                                            if (uvdt[i].Status == 0)
                                                            {
                                                                %>
                                                        <a  class="btn btn-danger btn-xs viewDetails" href="/admin/tuyen-dung/xac-nhan-thu-viec/<%= uvdt[i].Id %>.html"><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp Xem chi tiết</a>
                                                        <%
                                                                    %>
                                                          <a class="elm del-btn btn btn-danger btn-xs viewDetails" onclick="del(this.parentNode.parentNode.parentNode.parentNode,'<%= uvdt[i].Id %>', '<%= uvdt[i].Fullname %>')" href="javascript://" ><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp Xóa</a>
                                                        <%
                                                            }
                                                            else
                                                            {
                                                                    %>
                                                        <button type="button" uID ="<%=uvdt[i].Id %>""  class="btn btn-info btn-xs confirmCV" ><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp Duyệt hồ sơ</button>
                                                         <a class="elm del-btn btn btn-danger btn-xs viewDetails" onclick="del(this.parentNode.parentNode.parentNode.parentNode,'<%= uvdt[i].Id %>', '<%= uvdt[i].Fullname %>')" href="javascript://" ><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp Xóa</a>
                                                        <%
                                                            }
                                                                %>
                                                    </td>
                                                </tr>

                                            <%
                                                }
                                                 %>                                                              
                                    </tbody>
                                </table>
                            </div>
             
                    <script>
                        $(document).on("click", ".confirmCV", function () {
                            $("#myModal").modal();
                            $("#uID").val($(this).attr("uID"));
                            $("#HiddenFieldUVId").val($(this).attr("uID"));
                            $.ajax({
                                url: "/GUI/BackEnd/TuyenDungV2/ReportConfirm.aspx/billOBJ",
                                data: '{uID:' + $(this).attr("uID") + '}',
                                contentType: 'application/json; charset:UTF-8',
                                type: 'POST',
                                datatype: 'json',
                                success: function (data) {
                                    var jsData = JSON.parse(data.d);
                                    console.log(jsData);
                                    if (jsData != null) {
                                        $("#txtFullName").val(jsData.FullName);
                                        $("#txtNguonLD").val(jsData.NguonTD);
                                        $("#txtPhone").val(jsData.Phone);
                                        $("#txtPoint").val(jsData.Point);
                                        if (jsData.Avatar != null) {
                                            $(".gallery").show();
                                            $("#myImg1").attr("src", jsData.Avatar);
                                        }
                                        else {
                                            $(".gallery").hide();
                                        }
                                        if (jsData.SalonName != null) {
                                            $("#ddlSalon").append("<option value=" + jsData.SalonID + ">" + jsData.SalonName + "</option>");
                                        }
                                        if (jsData.BoPhan != null) {
                                            $("#ddlDepartment").append("<option value=" + jsData.BoPhanID + ">" + jsData.BoPhan + "</option>");
                                        }
                                        if (jsData.TestResult == true) {
                                            $("#ddlTestResult").append("<option value='1'>Đạt</option>");
                                        }
                                        else {
                                            $("#ddlTestResult").append("<option value='0'>Không đạt</option>");
                                        }
                                        if (jsData.TestAgain == true) {
                                            $("#cblTestAgain").attr("checked", "true");
                                        }
                                        else {
                                            $("#cblTestAgain").removeAttr("checked");
                                        }
                                        $("#uIDs").val(jsData.ID);

                                    }
                                    else {
                                        alert("Không có dữ liệu");
                                    }

                                }
                            })
                        })
                    </script>
    <div id="myModal" class="modal fade" role="dialog">
          <input type="text" id="uID" style="display:none;" />
              <div class="modal-dialog">
               <!-- Modal content-->
                 <div class="modal-content" style="width: 750px;">
                   <div class="modal-body">
                     <div class="table-wp">
                       <table class="table-add admin-product-table-add">
                        <tbody>
                            <tr class="title-head ">
                                <td><strong>Các kỹ năng của ứng viên</strong></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="hidden" id="uIDs" />
                
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Họ tên</span></td>
                                <td class="col-xs-3 right">
                                    <asp:TextBox ID="txtFullName" CssClass="form-control" runat="server" ClientIDMode="Static" Enabled="false" placeholder="Họ tên" Style="width: 220px;"></asp:TextBox>
                                    <div class="gallery" style="float: left; cursor: pointer; z-index: 200; position: absolute; top: -19px; width: auto; padding-left: 20px;">

                                        <img class="example-image form-control" id="myImg1" alt="" title="" src="#"
                                            data-img="#" style="width: 300px; height: auto; max-height: 265px; left: 0px; max-width: 100%;" />
                                    </div>
                                </td>

                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Salon</span></td>
                                <td class="col-xs-3 right ">
                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlSalon" Style="width: 220px;" ClientIDMode="Static" Enabled="false"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Bộ phận</span></td>
                                <td class="col-xs-3 right ">
                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlDepartment" Style="width: 220px;" ClientIDMode="Static" Enabled="false"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Số điện thoại</span></td>
                                <td class="col-xs-3 right"><span>
                                    <asp:TextBox ID="txtPhone" runat="server" CssClass="form-control" ClientIDMode="Static" Enabled="false" Style="width: 220px;"></asp:TextBox>
                                </span></td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Nguồn LĐ</span></td>
                                <td class="col-xs-9 right">
                                    <span>
                                        <asp:TextBox ID="txtNguonLD" CssClass="form-control" runat="server" ClientIDMode="Static" Style="width:220px" placeholder="Nguồn Lao Động" Enabled="false"></asp:TextBox>
                                    </span>

                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Link Video</span></td>
                                <td class="col-xs-9 right"><span>
                                    <asp:TextBox ID="txtVideoLink" runat="server" CssClass="form-control" ClientIDMode="Static" Enabled="false" Style="width:220px"></asp:TextBox>
                                </span></td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Test văn hóa</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                           <asp:TextBox ID="txtPoint" ClientIDMode="Static" Width="30%" CssClass="form-control" runat="server" style="margin-right:5px" placeholder="Số Điểm" Enabled="false"></asp:TextBox>
                                            <%-- <asp:TextBox ID="txtTotalPoint" ClientIDMode="Static" Width="20%" CssClass="form-control" runat="server" placeholder="Tổng Điểm"></asp:TextBox>--%>
                                        </div>
                                        <div class="div_col">
                                            <asp:DropDownList ID="ddlTestResult" ClientIDMode="Static" CssClass="form-control select" Enabled ="false" Width="30%" runat="server">
                                               
                                                <asp:ListItem Text="Đạt" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Không đạt" Value="0"></asp:ListItem>
                                                
                                            </asp:DropDownList>
                                            <asp:CheckBox runat="server" Enabled="false" ID="cblTestAgain" ClientIDMode="Static" Width="5%" style="margin-left:10px" TextAlign="Right"/><u style="line-height:32px">Check lần 2</u>
                                        </div>
                                        
                                    </div>
           
                                </td>
                            </tr>
                           

                            <tr>
                                <td colspan="3" style="width: 10px"></td>
                            </tr>
                         

                            <tr>
                                <td></td>
                                <td class="text-center" colspan="2">
                                    <%--<asp:Button ID="btnDuyet" CssClass="btn-send" Height="30px" Width="100px" runat="server" OnClientClick="if(!validDropdown()) return false;"  Text="Lưu lại" ClientIDMode="Static" OnClick="btnDuyet_Click"></asp:Button>--%>
                                    <button type="button" class="btn btn-send" id="redirect" style="height:30px;width:100px;background-color:black;color:white">Tạo tài khoản</button>
                                    <button type="button" id="closeModal" style="height:30px;width:100px;background-color:black;color:white" class="btn btn-send">Đóng</button>
                                </td>
                            </tr>
                            
                            <tr class="tr-send">
                                <td class="col-xs-3 left"></td>
                                <td class="col-xs-9 right no-border"></td>
                            </tr>
                            <asp:HiddenField runat="server" ID="HiddenFieldUVId" ClientIDMode="Static" />
                            <asp:HiddenField ID="HDF_MainImg" runat="server" ClientIDMode="Static" />
                            <asp:HiddenField ID="HDF_SkillImg_1" runat="server" ClientIDMode="Static" />
                            <asp:HiddenField ID="HDF_SkillImg_2" runat="server" ClientIDMode="Static" />
                        </tbody>
                    </table>
                  </div>
                </div>
                <div class="modal-footer">
                </div>
        </div>
    </div>
</div> 
</div>
                </div>
              <script>
                  $(document).on("click", "#redirect", function () {
                      //alert($("#uIDs").val());
                      var uIDss = $("#uIDs").val();
                      document.location = "/admin/tuyen-dung/tao-tai-khoan-nhan-vien/" + uIDss + ".html";
                  })
                  $(document).on("click", "#closeModal", function () {
                      $("#myModal").modal("hide");
                  })
              </script>
                   
                    <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                    <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />

   
            <link href="/Assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
            <script src="/Assets/js/jquery.mCustomScrollbar.js"></script>
            <script type="text/ecmascript">

                jQuery(document).ready(function () {

                    $("#glbTuyendungV2").addClass("active");
                    $("#glbConfirmSkill").addClass("active");
                    $("#tableUngVienDaTest").DataTable({
                        "bSort": false,

                        "language": {
                            "lengthMenu": " Bản ghi / trang _MENU_",
                            "zeroRecords": "Không tồn tại bản ghi",
                            "info": "Hiển thị trang _PAGE_ của _PAGES_",
                            "infoEmpty": "Không có dữ liệu",
                            "infoFiltered": "(Lọc kết quả từ _MAX_ tổng số bản ghi)",
                            "search": "Tìm kiếm",
                            "paginate": {
                                "first": "Đầu",
                                "last": "Cuối",
                                "next": "+",
                                "previous": "-"
                            },
                        },
                        "bLengthChange": false,
                        "bFilter": false,
                    });
                    $("#glbTuyenDung").addClass("active");
                    $("#glbTuyenDung_Step3").addClass("active");
                    $("li.be-report-li").addClass("active");

                    //============================
                    // Datepicker
                    //============================
                    $('.txtDateTime').datetimepicker({
                        dayOfWeekStart: 1,
                        lang: 'vi',
                        startDate: '2014/10/10',
                        format: 'd/m/Y',
                        dateonly: true,
                        showHour: false,
                        showMinute: false,
                        timepicker: false,
                        onChangeDateTime: function (dp, $input) { }
                    });

                    //============================
                    // Bind Staff
                    //============================
                    BindStaffFilter();

                    // View data yesterday
                    $(".tag-date-today").click();
                    // Xử lý kiểm tra biến filter
                    var filter = localStorage.getItem("filterObject");
                    var filterObject = {};
                    console.log(filter);
                    if (filter != null) {
                        try {
                            filterObject = JSON.parse(filter);
                            $("#TxtDateTimeFrom").val(filterObject.timeFrom);
                            $("#TxtDateTimeTo").val(filterObject.timeTo);
                            $("#ddlDepartment").val(filterObject.departmentId);
                            localStorage.removeItem("filterObject");
                            $("#ViewData").click();
                        }
                        catch (err) {
                            console.log(err.message);
                        }
                    }
                });
                function assignFilter(This) {
                    $('#load').click();
                    console.log("sdsa")

                    filterObject = {};
                    filterObject.timeFrom = $("#TxtDateTimeFrom").val();
                    filterObject.timeTo = $("#TxtDateTimeTo").val();
                    filterObject.departmentId = $("#ddlDepartment").val();
                    localStorage.setItem("filterObject", JSON.stringify(filterObject));
                }

                function showTrPrice() {
                    $(".tr-price").show();
                }

                function BindStaffFilter() {
                    $(".eb-select").bind("focus", function () {
                        EBSelect_ShowBox($(this).parent().find(".eb-select-data"));
                    });
                    $(window).bind("click", function (e) {
                        console.log(e);
                        if ((!e.target.className.match("eb-select") && e.target.parentElement.className.match("ul-listing-staff")) ||
                            (!e.target.className.match("eb-select") && !e.target.parentElement.className.match("ul-listing-staff"))
                            && !e.target.className.match("mCSB_dragger_bar")) {
                            EBSelect_HideBox();
                        }
                    });
                    //============================
                    // Scroll Staff Filter
                    //============================
                    $('.eb-select-data').each(function () {
                        if ($(this).height() > 230) {
                            $(this).mCustomScrollbar({
                                theme: "dark-2",
                                scrollInertia: 100
                            });
                        }
                    });
                }
                function BindIdToHDF(THIS, id, HDF_DomId, Input_DomId) {
                    var text = THIS.innerText.trim(),
                        HDF_Dom = document.getElementById(HDF_DomId),
                        InputText = document.getElementById(Input_DomId);
                    HDF_Dom.value = id;
                    InputText.value = text;
                    InputText.setAttribute("data-value", id);
                    ajaxGetStaffsByType(id);
                    if (HDF_DomId == "HDF_TypeStaff") {
                        $("#StaffName").val("");
                        $("#HDF_Staff").val("");
                    }
                }
                function EBSelect_ShowBox(Dom) {
                    EBSelect_HideBox();
                    Dom.show();
                }

                function EBSelect_HideBox() {
                    $(".eb-select-data").hide();
                }

                function ReplaceZezoValue() {
                    $("table.table-listing td").each(function () {
                        if ($(this).text().trim() == "0") {
                            $(this).text("-");
                        }
                    });
                }

                function viewDataByDate(This, time) {
                    $(".tag-date.active").removeClass("active");
                    This.addClass("active");
                    $("#TxtDateTimeTo").val("");
                    $("#TxtDateTimeFrom").val(time);
                    $("#ViewData").click();
                }
                function del(This, code, name) {
                    var code = code || null,
                        name = name || null,
                        Row = This;
                    if (!code) return false;

                    // show EBPopup
                    $(".confirm-yn").openEBPopup();
                    $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

                    $("#EBPopup .yn-yes").bind("click", function () {
                        $.ajax({
                            type: "POST",
                            url: "/GUI/SystemService/Ajax/Del.aspx/Delele_TuyenDungUV",
                            data: '{Code : "' + code + '"}',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json", success: function (response) {
                                var mission = JSON.parse(response.d);
                                if (mission.success) {
                                    delSuccess();
                                    Row.remove();
                                } else {
                                    delFailed();
                                }
                            },
                            failure: function (response) { alert(response.d); }
                        });
                    });
                    $("#EBPopup .yn-no").bind("click", function () {
                        autoCloseEBPopup(0);
                    });
                }
            </script>
        </asp:Panel>
    </asp:Content>
