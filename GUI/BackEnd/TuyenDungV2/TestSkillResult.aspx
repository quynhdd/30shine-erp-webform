﻿<%@ Page Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="TestSkillResult.aspx.cs" Inherits="_30shine.GUI.BackEnd.TuyenDungV2.TestSkillResult" %>

<asp:Content ID="ProductAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <link href="/Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="/Assets/js/select2/select2.min.js"></script>
        <link href="/Assets/js/fancybox/jquery.fancybox.min.css" rel="stylesheet" />
        <script src="/Assets/js/fancybox/jquery.fancybox.min.js"></script>
        <style>
            #radioApprove tr {
                display: inline-table;
                padding-right: 15px;
                cursor: pointer;
            }
             #success-alert{
                top:100px;
                right:10px;
                position:fixed;
                width:20% !important;
                z-index:2000;
            }
                #radioApprove tr td input {
                    margin-left: 15px;
                    cursor: pointer;
                }

                #radioApprove tr td label {
                    margin-left: 5px;
                    position: relative;
                    top: -1px;
                    cursor: pointer;
                }

            .listing-img-upload .thumb-wp {
                border: none;
            }
            /*.listing-img-upload .thumb-wp img { position: relative; width: auto !important; height: auto !important; max-width: 100%; }*/
            .ddl {
                width: 100%;
            }

            .table-wp {
                background: #f5f5f5;
                float: left;
                width: 100%;
                padding: 15px;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                -webkit-box-shadow: 0px 0px 3px 0px #ddd;
                -moz-box-shadow: 0px 0px 3px 0px #ddd;
                box-shadow: 0px 0px 3px 0px #ddd;
            }

            .btn {
                border-radius: 5px;
                margin-left: 10px;
                margin-right: 10px;
                height: 70px;
            }

                .btn:hover {
                    background-color: orangered;
                    color: white;
                }

            .send {
                box-shadow: 2px 2px #b0b0b0;
                float: left;
            }

            .senddd {
                box-shadow: 2px 2px #b0b0b0;
                float: left;
                width: 100px;
                height: 50px;
            }

                .senddd:hover {
                    background-color: #2b8a05;
                    color: white;
                }

            .cancel {
                border-radius: 5px;
            }

            .canceldd {
                float: right;
            }

            .cancel:hover {
                background-color: #50b347;
                color: white;
            }

            .table-wp {
                background: #f5f5f5;
                float: left;
                width: 100%;
                padding: 15px;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                -webkit-box-shadow: 0px 0px 3px 0px #ddd;
                -moz-box-shadow: 0px 0px 3px 0px #ddd;
                box-shadow: 0px 0px 3px 0px #ddd;
            }

            .tr-skill {
                display: none;
            }

            .tr-show {
                display: table-row;
            }

            .customer-add .table-add tr td.right .fb-cover-error {
                top: 0;
                right: auto;
            }

            .gallery {
                display: inline-block;
                margin-top: 20px;
            }

            .thumbnailImage {
                margin-bottom: 6px;
                width: 100%;
            }

            .modal-image {
                display: none;
                position: fixed; /* Stay in place */
                z-index: 1; /* Sit on top */ /* Location of the box */
                left: 0;
                top: 0;
                width: 100%; /* Full width */
                height: 100%; /* Full height */
                overflow: auto; /* Enable scroll if needed */
                background-color: rgb(0,0,0); /* Fallback color */
                background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
            }

            /* Modal Content (image) */
            .modal-content-image {
                margin: auto;
                margin-top: 30px;
                display: block;
                width: 50%;
                max-width: 500px;
                height: auto;
                max-height: 600px;
            }

            /* Caption of Modal Image */
            #caption-image {
                margin: auto;
                display: block;
                width: 60%;
                max-width: 700px;
                text-align: center;
                color: #ccc;
                padding: 10px 0;
                height: 150px;
                margin-top: 10px;
            }

            /* Add Animation */
            .modal-content-image, #caption-image {
                -webkit-animation-name: zoom;
                -webkit-animation-duration: 0.6s;
                animation-name: zoom;
                animation-duration: 0.6s;
            }

            @-webkit-keyframes zoom {
                from {
                    -webkit-transform: scale(0);
                }

                to {
                    -webkit-transform: scale(1);
                }
            }
            .btn-send{
                padding:0px 30px;
                background:black !important;
                color:white;
            }
            .btn-send:hover{
                color:yellow !important;
            }
            @keyframes zoom {
                from {
                    transform: scale(0.1);
                }

                to {
                    transform: scale(1);
                }
            }

            /* The Close Button */
            .close-image {
                position: absolute;
                top: 15px;
                right: 35px;
                color: #f1f1f1;
                font-size: 40px;
                font-weight: bold;
                transition: 0.3s;
            }

                .close-image:hover,
                .close-image:focus {
                    color: #bbb;
                    text-decoration: none;
                    cursor: pointer;
                }

            .btn-send:hover {
                background: #50b347;
                color: #ffffff;
            }

            .btn-send {
                width: auto !important;
                height: 30px;
                line-height: 30px;
                float: left;
                margin-right: 20px;
                margin-top: 20px;
                background: #ddd;
                text-align: center;
                cursor: pointer;
                border-radius: 2px;
            }

            .one {
                background: #ddd;
                border-radius: 5px;
                width: auto;
                padding-top: 5px;
                padding-bottom: 5px;
                height: 30px;
                line-height: 30px;
            }

                .one:hover {
                    background: #50b347;
                    color: #ffffff;
                }
        </style>
        <div class="alert alert-danger" id="success-alert">
            <%--<button type="button" class="close" data-dismiss="alert">x</button>--%>
            <strong>Cảnh báo: </strong>
            <span id="msg-alert"></span>
        </div>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Tuyển dụng &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/admin/tuyen-dung/lap-ho-so/danh-sach-ho-so.html">Danh sách ứng viên</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add admin-product-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="table-wp">
                    <table class="table-add admin-product-table-add">
                        <tbody>
                            <tr class="title-head ">
                                <td><strong>Các kỹ năng của ứng viên</strong></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="myModalimage" style="z-index: 2000;" class="modal modal-image">
                                        <img class="modal-content-image" id="img01">
                                        <div id="captionimage"></div>
                                    </div>
                                </td>
                            </tr>
                            <tr>

                                <td class="col-xs-3 left"><span>Họ tên</span></td>
                                <td class="col-xs-3 right">
                                    <asp:TextBox ID="FullName" CssClass="form-control" runat="server" ClientIDMode="Static" Enabled="false" placeholder="Họ tên" Style="width: 220px;"></asp:TextBox>
                                    <% if (OBJ != null && OBJ.MainImg != null && OBJ.MainImg != "")
                                        { %>
                                    <div class="gallery" style="float: left; cursor: pointer; z-index: 200; position: absolute; top: -19px; width: auto; padding-left: 20px;">

                                        <img class="example-image form-control" id="myImg1" alt="" title="" src="<%=OBJ.MainImg %>"
                                            data-img="<%=OBJ.MainImg %>" style="width: 300px; height: auto; max-height: 265px; left: 0px; max-width: 100%;" />
                                    </div>
                                    <% } %> 
                                </td>

                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Salon</span></td>
                                <td class="col-xs-3 right ">
                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlSalon" Style="width: 220px;" ClientIDMode="Static" Enabled="false"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Bộ phận</span></td>
                                <td class="col-xs-3 right ">
                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlDepartment" Style="width: 220px;" ClientIDMode="Static" Enabled="false"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Số điện thoại</span></td>
                                <td class="col-xs-3 right"><span>
                                    <asp:TextBox ID="Phone" runat="server" CssClass="form-control" ClientIDMode="Static" Enabled="false" Style="width: 220px;"></asp:TextBox>
                                </span></td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Nguồn LĐ</span></td>
                                <td class="col-xs-3 right">
                                    <span>
                                        <asp:TextBox ID="CMT" CssClass="form-control" runat="server" ClientIDMode="Static" Style="width: 220px;" placeholder="Số CMT" Enabled="false"></asp:TextBox>
                                    </span>

                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span style="margin-top: -35px">Người kiểm tra kỹ năng</span></td>
                                <td class="col-xs-3 right ">
                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlUserTest" Style="width: 220px;" ClientIDMode="Static" SelectionMode="Multiple" ></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvType" runat="server" ControlToValidate="ddlUserTest" InitialValue="0" ErrorMessage="<i style='color:red'>Vui lòng chọn người test!!!</span>" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <%-- Modal show image --%>
                            <tr class="tr-upload">
                                <td class="col-xs-3 left"><span>Ảnh kỹ năng</span></td>
                                <td class="col-xs-7 right">
                                    <div class="wrap btn-upload-wp HDF_SkillImg_1" style="margin-bottom: 5px; width: auto;">
                                        <div id="Btn_UploadImg" class="btn-upload-img" onclick="popupImageIframe('HDF_SkillImg_1')">Ảnh cắt</div>
                                        <asp:FileUpload ID="UploadFile" ClientIDMode="Static" Style="display: none;"
                                            CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                        <div class="wrap listing-img-upload">
                                            <% if (OBJ != null && OBJ.ImgSkill1 != null && OBJ.ImgSkill1 != "")
                                                { %>
                                            <div class="thumb-wp">
                                                <img class="thumb" alt="" title="" src="<%=OBJ.ImgSkill1 %>"
                                                    data-img="<%=OBJ.ImgSkill1 %>" />
                                                <span class="delete-thumb" onclick="deleteThumbnail($(this), 'HDF_SkillImg_1')"></span>
                                            </div>
                                            <% } %>
                                        </div>
                                    </div>

                                    <div class="wrap btn-upload-wp HDF_SkillImg_2" style="margin-bottom: 5px; margin-left: 10px; width: auto;">
                                        <div id="Btn_UploadImg2" class="btn-upload-img" onclick="popupImageIframe('HDF_SkillImg_2')">Ảnh hóa chất</div>
                                        <asp:FileUpload ID="FileUpload2" ClientIDMode="Static" Style="display: none;"
                                            CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                        <div class="wrap listing-img-upload">
                                            <% if (OBJ != null && OBJ.ImgSkill2 != null && OBJ.ImgSkill2 != "")
                                                { %>
                                            <div class="thumb-wp">
                                                <img class="thumb" alt="" title="" src="<%=OBJ.ImgSkill2 %>"
                                                    data-img="<%=OBJ.ImgSkill2 %>" />
                                                <span class="delete-thumb" onclick="deleteThumbnail($(this), 'HDF_SkillImg_2')"></span>
                                            </div>
                                            <% } %>
                                        </div>
                                    </div>
                                    </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Kỹ năng</span></td>
                                <td class="col-xs-7 right">

                                <asp:Repeater runat="server" ID="rptSkillName" ClientIDMode="Static" OnItemDataBound="rptSkillName_ItemDataBound">
                                    <ItemTemplate>
                                        <tr>
                                            <td class="col-xs-3 right"></td>
                                            <td class="col-xs-9 right">
                                                <span style="width: 230px">
                                                    <label id="lblSkill" class="lbl-cus-no-infor" value="<%#Eval("IdSkill") %>" style="margin-bottom: 8px; cursor: pointer; font-weight: normal; font-size: 13px; padding: 2px 10px; background: #ddd; line-height: 33px; float: left;">
                                                        <%#Eval("SkillName") %>
                                                    </label>
                                                </span>
                                                <span style="width: 354px">
                                                    <asp:DropDownList runat="server" ID="drlLevelName" class="levelName" ClientIDMode="Static" ValidationGroup ="g"></asp:DropDownList>
                                                   <%-- <asp:RequiredFieldValidator ID="rfvType11" runat="server" ControlToValidate="drlLevelName" InitialValue="0" ErrorMessage="<i style='color:red'>Vui lòng đánh giá kỹ năng ứng viên!!!</span>" SetFocusOnError="true" ValidationGroup="g"></asp:RequiredFieldValidator>--%>
                                                </span>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>

                                </td>
                            </tr>

                            <tr>
                                <td colspan="3" style="width: 10px"></td>
                            </tr>
                            <%--<tr class="tr-description">
                                <td class="col-xs-2 left"><span>Ghi chú</span></td>
                                <td class="col-xs-9 right " colspan="2">
                                    <span class="field-wp">
                                        <asp:TextBox TextMode="MultiLine" CssClass="form-control" Rows="2" ID="Step2Note" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>--%>

                            <tr>
                                <td></td>
                                <td class="text-center" colspan="2">
                                    <asp:Button ID="btnDuyet" CssClass="btn-send" Height="30px" Width="100px" runat="server" OnClientClick="if(!validDropdown()) return false;"  Text="Lưu lại" ClientIDMode="Static"  OnClick="btnDuyet_Click"></asp:Button>
                                </td>
                            </tr>
                            <tr class="tr-send">
                                <td class="col-xs-3 left"></td>
                                <td class="col-xs-9 right no-border"></td>
                            </tr>
                            <asp:HiddenField runat="server" ID="HiddenFieldUVId" ClientIDMode="Static" />
                            <asp:HiddenField ID="HDF_MainImg" runat="server" ClientIDMode="Static" />
                            <asp:HiddenField ID="HDF_SkillImg_1" runat="server" ClientIDMode="Static" />
                            <asp:HiddenField ID="HDF_SkillImg_2" runat="server" ClientIDMode="Static" />
                        </tbody>
                    </table>
                </div>
            </div>
            <%-- end Add --%>
        </div>
        <script type="text/javascript">
            function DoValidation() {
                // Do calls to validation services here
                var valid = true;
                var errorString = 'Vui lòng nhập đầy đủ';
                var img1 = $("#HDF_SkillImg_1").val();
                var img2 = $("#HDF_SkillImg_2").val();
               // var phone = $("#Phone").val();
                //var departmentId = $("#ddlDepartment").val();


                if (img1 == "" || img2 == "") {
                    valid = false;
                    errorString += " [Ảnh kỹ năng]";
                    //$("#ddlDepartment").css("border-color", "pink");
                }

               
                if (!valid) {
                    $("#success-alert").fadeTo(2000, 1000).slideUp(1000, function () {
                        $("#success-alert").slideUp(1000);
                    });
                    $("#msg-alert").text(errorString);

                }
              
                return valid;
            }
            $("#success-alert").hide();
            $('select').select2();
            function validDropdown(){
                var valid = true;
                var errorString = '';
                var img1 = $("#HDF_SkillImg_1").val();
                var img2 = $("#HDF_SkillImg_2").val();
                // var phone = $("#Phone").val();
                //var departmentId = $("#ddlDepartment").val();


                if (img1 == "" || img2 == "") {
                    valid = false;
                    errorString = " Ảnh kỹ năng không được để trống";
                    //$("#ddlDepartment").css("border-color", "pink");
                }
                if ($(".levelName").val()== 0) {
                    valid = false;
                    errorString = 'Vui lòng đánh giá kỹ năng của ứng viên';
                }

                if (!valid) {
                    $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                        $("#success-alert").slideUp(2000);
                    });
                    $("#msg-alert").text(errorString);

                }
                return valid;
            }
            
        </script>
        <script>
            function choseDepartment(This) {
                var id = parseInt($("#ddlDepartment").val());
                //console.log(id);
                if (!isNaN(id)) {
                    if (id == 1) {
                        $(".tr-skill").removeClass('tr-show');
                        $(".tr-skill-img").addClass("tr-show");
                        $(".tr-skill-btn").addClass("tr-show");
                        $(".tr-skill-text").addClass("tr-show");
                    }
                    else if (id == 2) {
                        $(".tr-skill").removeClass('tr-show');
                        $(".tr-skill-video").addClass("tr-show");
                        $(".tr-skill-text").addClass("tr-show");
                    }
                    else if (id == 0) {
                        $(".tr-skill").removeClass('tr-show');
                    }
                    else {
                        $(".tr-skill").removeClass('tr-show');
                    }
                }
                else {
                    $(".tr-skill").removeClass('tr-show');
                }
            }
            jQuery(document).ready(function () {

                $("#glbAdminContent").addClass("active");
                $("#glbAdminCategory").addClass("active");
                //date picker
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        //$input.val($input.val().split(' ')[0]);
                    }
                });
                //excThumbWidth(120, 120);

                choseDepartment(null);
            });
            //show image
            var modal = document.getElementById('myModalimage');
            // Get the image and insert it inside the modal - use its "alt" text as a caption
            var img = document.getElementById('myImg1');
            var img1 = document.getElementById('myImg2');
            var img2 = document.getElementById('myImg3');
            var img3 = document.getElementById('myImg4');
            var modalImg = document.getElementById("img01");
            var captionText = document.getElementById("captionimage");

            try {
                img.onclick = function () {

                    modal.style.display = "block";
                    modalImg.src = this.src;
                    captionText.innerHTML = this.alt;
                }
                img1.onclick = function () {
                    modal.style.display = "block";
                    modalImg.src = this.src;
                    captionText.innerHTML = this.alt;
                }
                img2.onclick = function () {
                    modal.style.display = "block";
                    modalImg.src = this.src;
                    captionText.innerHTML = this.alt;
                }
                img3.onclick = function () {
                    modal.style.display = "block";
                    modalImg.src = this.src;
                    captionText.innerHTML = this.alt;
                }
                // When the user clicks on <span> (x), close the modal
                modal.onclick = function () {
                    console.log("a");
                    modal.style.display = "none";
                }
            } catch (err) { }


            $('#myModalimage').click(function () {
                $("#myModalimage").css("display", "none")
            });
            $('.modal-content-image').click(function (event) {
                event.stopPropagation();
            });


        </script>
        <!-- Popup plugin image hay là chưa gọi hàm getIdLevel nên nó k chạy  -->
        <script type="text/javascript">
            function popupImageIframe1(StoreImgField) {
                var Width = 960;
                var Height = 560;
                var ImgList = [];
                var iframe = $("<div class='iframe-image-upload' style='width:" + Width + "px; height:" + Height + "px; float:left; display:none;'><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + StoreImgField + "&callback=getThumbFromIframe&imgWidth=800&imgHeight=600&multi_img=false&Folder=Common' style='width:100%; height: 100%;'></iframe></div");

                $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
            }
            function popupImageIframe(StoreImgField) {
                var Width = 960;
                var Height = 560;
                var ImgList = [];
                var iframe = $("<div class='iframe-image-upload' style='width:" + Width + "px; height:" + Height + "px; float:left; display:none;'><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + StoreImgField + "&callback=getThumbFromIframe&imgWidth=800&imgHeight=600&multi_img=false&Folder=Common' style='width:100%; height: 100%;'></iframe></div");

                $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
            }

            window.getThumbFromIframe = function (Imgs, StoreImgField) {
                var imgs = "";
                var thumb = "";
                if (Imgs.length > 0) {
                    thumb = '<div class="thumb-wp">' +
                                '<img class="thumb" alt="" title="" src="' + Imgs[0] + '"' +
                                    'data-img="" />' +
                                '<span class="delete-thumb" onclick="deleteThumbnail($(this), \'' + StoreImgField + '\')"></span>' +
                            '</div>';
                    $("." + StoreImgField).find(".listing-img-upload").empty().append(thumb);
                    //excThumbWidth(120, 120);
                    $("#" + StoreImgField).val(Imgs[0]);
                }
                autoCloseEBPopup();
            }

            function deleteThumbnail(This, StoreImgField) {
                var imgs = "";
                var lst = This.parent().parent();
                This.parent().remove();
                lst.find("img.thumb").each(function () {
                    imgs += $(this).attr("src") + ",";
                });
                $("#" + StoreImgField).val(imgs);
            }

        </script>
    </asp:Panel>
</asp:Content>
