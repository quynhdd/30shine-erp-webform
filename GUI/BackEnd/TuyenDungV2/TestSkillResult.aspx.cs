﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Script.Serialization;
using System.Data;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.TuyenDungV2
{
    public partial class TestSkillResult : System.Web.UI.Page
    {
        private string PageID = "TD_DGKN";
        protected TuyenDung_UngVien OBJ;
        protected string _Code;
        protected bool _IsUpdate = false;
        public UIHelpers _UIHelpers = new UIHelpers();
        protected bool HasImages = false;
        private bool Perm_Access = false;
        private bool Perm_AllPermission = false;
        private static string idLevel = "";// khai báo lấy về Id của Level
        protected List<string> ListImagesUrl = new List<string>();

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_AllPermission = permissionModel.GetActionByActionNameAndPageId("Perm_AllPermission", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_AllPermission = permissionModel.CheckPermisionByAction("Perm_AllPermission", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            
            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_Access);
                Bind_department();
                BindSkillNameLevel();
                BindUserTest();
         
            }
            if (IsUpdate())
            {
                Bind_OBJ();
            }

        }
        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }
        #region[Deparment]
        public void Bind_department()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Order).ToList();
                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "Id";
                ddlDepartment.SelectedIndex = 0;
                ddlDepartment.DataSource = LST;
                ddlDepartment.DataBind();
            }
        }
        #endregion
        #region[BindUsertest]
        private void BindUserTest()
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = db.TuyenDung_NguoiTest.Where(t => t.IsDelete == false).ToList();
                if (list.Count > 0)
                {
                    ddlUserTest.Items.Clear();
                    ddlUserTest.Items.Add(new ListItem("--- Chọn người kiểm tra ---", "0"));
                    for (int i = 0; i < list.Count; i++)
                    {
                        ddlUserTest.Items.Add(new ListItem(list[i].FullName, list[i].Id.ToString()));
                    }
                    ddlUserTest.DataBind();
                }
            }
        }
        #endregion
        
        #region[BindObj]
        private bool Bind_OBJ()
        {
            using (var db = new Solution_30shineEntities())
            {
                var serialize = new JavaScriptSerializer();
                var ExistOBJ = true;
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.TuyenDung_UngVien.FirstOrDefault(w => w.Id == Id);
                    if (!OBJ.Equals(null))
                    {
                        FullName.Text = OBJ.FullName;
                        Phone.Text = OBJ.Phone;
                        if (OBJ.NguonTuyenDungId == 1)
                        {
                            CMT.Text = "Tuyển ngoài";

                        }
                        else
                        {
                            CMT.Text = "S4M";
                        }
                        ddlSalon.SelectedValue = OBJ.salonId.ToString();
                        ddlDepartment.SelectedValue = OBJ.DepartmentId.ToString();
                        HDF_MainImg.Value = OBJ.MainImg;
                        HiddenFieldUVId.Value = OBJ.Id.ToString();
                       // Step2Note.Text = OBJ.Step2Note;

                    }
                    else
                    {
                        ExistOBJ = false;
                        var msg = "Lỗi! Bản ghi không tồn tại";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
                else
                {
                    ExistOBJ = false;
                    var msg = "Lỗi! Bản ghi không tồn tại";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }
                return ExistOBJ;
            }
        }
       
        #endregion
        #region[BidnSkillNameAndLevel]
        /// <summary>
        /// Lấy về tên các kỹ năng 
        /// </summary>
        private void BindSkillNameLevel()
        {
            using (var db = new Solution_30shineEntities())
            {
                var listSkillName = db.Database.SqlQuery<cls_skillname>("select s.Id as IdSkill, s.SkillName from TuyenDung_SkillLevel_Map as map inner join TuyenDung_Skill as s  on map.SkillID = s.Id where s.IsDelete != 1 and s.Publish !=0 Group by s.Id, s.SkillName").ToList();
                if (listSkillName.Count > 0)
                {
                    rptSkillName.DataSource = listSkillName;
                    rptSkillName.DataBind();
                }
            }
        }
        /// <summary>
        /// Sự kiện IteamDataBound lấy dữ liệu đổ vào Dropdownlist
        /// </summary>
        protected void rptSkillName_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                var dataItem = (cls_skillname)e.Item.DataItem;
                if (dataItem != null)
                {
                    DropDownList rpt = e.Item.FindControl("drlLevelName") as DropDownList;
                    using (var db = new Solution_30shineEntities())
                    {
                        var listLevel = db.Database.SqlQuery<cls_SkillLevel>("select lev.Id, CONCAT(( s.LevelName),' --- ',(lev.[Description] )) as levelName from TuyenDung_SkillLevel_Map as lev inner join TuyenDung_SkillLevel as s on s.Id = lev.LevelID where lev.SkillID in (" + dataItem.IdSkill + ")").ToList();
                        rpt.Items.Clear();
                        ListItem item = new ListItem("Đánh giá cho kỹ năng", "0");
                        rpt.Items.Insert(0, item);
                        foreach (var v in listLevel)
                        {
                            rpt.Items.Add(new ListItem(v.levelName, v.Id.ToString()));
                        }
                        //rpt.DataTextField = "levelName";
                        //rpt.DataValueField = "Id";
                        //rpt.DataSource = listLevel;
                        rpt.SelectedIndex = 0;
                        rpt.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public class cls_skillname
        {
            public int IdSkill { get; set; }
            public string skillName { get; set; }
        }
        public class cls_SkillLevel
        {
            public int Id { get; set; }
            public string levelName { get; set; }
            public string description { get; set; }
        }
        #endregion
        #region[Lưu các kỹ năng lại vào bảng TuyenDung_Danhgia]
        /// <summary>
        /// Lưu các kỹ năng lại vào bảng TuyenDung_Danhgia
        /// </summary>
        protected void btnDuyet_Click(object sender, EventArgs e)
        {
            foreach (RepeaterItem i in rptSkillName.Items)
            {
                DropDownList ddl = (DropDownList)i.FindControl("drlLevelName");
                idLevel += "," + ddl.SelectedValue + "";
            }
            using (var db = new Solution_30shineEntities())
            {

                int Id = int.Parse(HiddenFieldUVId.Value);
                OBJ = db.TuyenDung_UngVien.FirstOrDefault(w => w.Id == Id);
                OBJ.UngVienStatusId = 7;
                try
                {
                    OBJ.TesterID = Convert.ToInt32(ddlUserTest.SelectedValue);
                }
                catch
                {
                    OBJ.TesterID = 0;
                }
                OBJ.ImgSkill1 = HDF_SkillImg_1.Value;
                OBJ.ImgSkill2 = HDF_SkillImg_2.Value;
               // OBJ.Step2Note = Step2Note.Text;
                var _obj = new TuyenDung_DanhGia();
                _obj.UngVienID = Id;
                _obj.UserTestID = Convert.ToInt32(ddlUserTest.SelectedValue);
                _obj.IsDelete = false;
                _obj.CreatedDate = DateTime.Now;
                _obj.IsDelete = false;
                _obj.IsStatus = false;
                //_obj.Note = Step2Note.Text;
                
                _obj.ImgSkill1 = HDF_SkillImg_1.Value;
                _obj.ImgSkill2 = HDF_SkillImg_2.Value;
                _obj.MapSkillLevelID = idLevel.Remove(0, 1);
                db.TuyenDung_DanhGia.Add(_obj);
                db.TuyenDung_UngVien.AddOrUpdate(OBJ);
                var exc = db.SaveChanges();
                if (exc > 0)
                {
                    //update status in OrderRecruitingStaffs table
                    var checkOrder = db.OrderRecruitingStaffs.Where(w => w.CandidateId == OBJ.Id).FirstOrDefault();
                    if (checkOrder != null)
                    {
                        checkOrder.Status = 3;
                        checkOrder.ModifiedDate = DateTime.Now;
                        db.OrderRecruitingStaffs.AddOrUpdate(checkOrder);
                        db.SaveChanges();
                    }
                    idLevel = "";
                    var MsgParam = new List<KeyValuePair<string, string>>();
                    MsgParam.Add(new KeyValuePair<string, string>("msg_add_status", "success"));
                    MsgParam.Add(new KeyValuePair<string, string>("msg_add_message", "Cập nhật thành công!"));
                    UIHelpers.Redirect("/admin/tuyen-dung/danh-sach-da-test-ky-nang.html");
                }
                else
                {
                    var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }
            }

        }
        #endregion

    }
}