﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using LinqKit;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.TuyenDungV2
{
    public partial class Report_Of_Time : System.Web.UI.Page
    {
        private string PageID = "TD_BC_DG";
        protected Paging PAGING = new Paging();
        private Expression<Func<Staff, bool>> Where = PredicateBuilder.True<Staff>();
        protected List<StaffNV> nv = new List<StaffNV>();
        protected bool Perm_Access = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_Edit = false;
        protected bool Perm_Delete = false;
        protected bool Perm_ShowSalon = false;
        protected bool Perm_Add = false;
        protected int SalonId;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                Perm_Add = permissionModel.CheckPermisionByAction("Perm_Add", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        public static Library.SendEmail sendMail = new Library.SendEmail();

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
            }
            else
            {
                //
            }
        }



        protected void _BtnClick(object sender, EventArgs e)
        {
       
            Bind_Staff();
            RemoveLoading();

        }

        
        private void Bind_Staff()
        {

            try
            {

                var _time = Convert.ToInt32(ddlTimeCheck.SelectedItem.Value);
                if (_time != 0)
                {

                    Solution_30shineEntities db = new Solution_30shineEntities();
                    nv = db.Database.SqlQuery<StaffNV>("exec Store_Get_Staff_By_Time {0},{1}",_time,Convert.ToInt32(ddlSalon.SelectedItem.Value)).ToList();
                }
            }

            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

      
        }

        public static string getDateJoin(int staffID)
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
            var date = db.Staffs.FirstOrDefault(s => s.Id == staffID);
            var _Date = date.DateJoin;
            string day = _Date.Value.Day.ToString();
            string month = _Date.Value.Month.ToString();
            string year = _Date.Value.Year.ToString();
            return year+"/"+month+"/"+day;
        }


        [WebMethod]

        public static string get_Total_Result(int staffID, int time)
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
 
            var list = db.Store_Report_By_Time_V2(getDateJoin(staffID),time, staffID).ToList();
            return new JavaScriptSerializer().Serialize(list);
        }

        [WebMethod]
    
        public static string get_Total_Service(int staffID, int time)
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
          
            var list = db.Store_Staff_TotalMoney_BillService(getDateJoin(staffID), time, staffID).ToList();
            return new JavaScriptSerializer().Serialize(list);
        }

        [WebMethod]
        public static string getHairStyle(int staffID, int time)
        {
            Solution_30shineEntities db = new Solution_30shineEntities();

            var _list = db.Store_Staff_TotalHairStyle(getDateJoin(staffID), time, staffID).ToList();
            return new JavaScriptSerializer().Serialize(_list);
        }

        [WebMethod]
        public static string totalMisstake(int staffID, int time)
        {
            Solution_30shineEntities db = new Solution_30shineEntities();

            var __list = db.Store_Total_Misstake(getDateJoin(staffID), time, staffID);
            return new JavaScriptSerializer().Serialize(__list);
        }

        [WebMethod]
        public static void Ratting(int staffID,string note)
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
           
            var _s = db.Staffs.Where(s => s.Id == staffID).FirstOrDefault();
          
            _s.Note = note;
            db.Staffs.AddOrUpdate(_s);
            db.SaveChanges();
   
        }
        [WebMethod]
        public static void Endwork(int staffID)
        {
            Solution_30shineEntities db = new Solution_30shineEntities();

            var _s = db.Staffs.Where(s => s.Id == staffID).FirstOrDefault();

            _s.Active = 0;
            db.Staffs.AddOrUpdate(_s);
            db.SaveChanges();

        }
        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        

        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }

        public class StaffNV
        {
            public int Id { get; set; }
            public string Fullname { get; set; }
            public string SalonName { get; set; }
            public string Note { get; set; }
            public DateTime DateJoin { get; set; } 
        }
        public class cls_staffL : Staff
        {
            public string salonName { get; set; }
            public string departmentName { get; set; }
            public string skillLevelName { get; set; }
        }

        public class total_Price
        {
            public int quantity { get; set; }
            public string serviceName { get; set; }
            public double total { get; set; }
        }
        /// <summary>
        /// Class phục vụ set điểm kỹ năng cho nhân viên
        /// </summary>
        public class cls_skill_level
        {
            public int SkillId { get; set; }
            public string SkillName { get; set; }
            public List<cls_skill_level_item> SkillLevels { get; set; }
            public int Order { get; set; }
        }

        /// <summary>
        /// Class phục vụ set điểm kỹ năng cho nhân viên
        /// </summary>
        public class cls_skill_level_item
        {
            public int LevelId { get; set; }
            public string LevelName { get; set; }
            public int Value { get; set; }
            public int Point { get; set; }
            public bool IsChecked { get; set; }
        }
    }
}