﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TemplateMaster/SiteMaster.Master" CodeBehind="CreateTester.aspx.cs" Inherits="_30shine.GUI.BackEnd.TuyenDungV2.CreateTester" %>

<asp:Content ID="ProductAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
       
        <link href="/Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="/Assets/js/select2/select2.min.js"></script>
        <style>
            .table-wp { background: #f5f5f5; float: left; width: 100%; padding: 15px; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; -webkit-box-shadow: 0px 0px 3px 0px #ddd; -moz-box-shadow: 0px 0px 3px 0px #ddd; box-shadow: 0px 0px 3px 0px #ddd; }

            .tr-skill { display: none; }
            .tr-show { display: table-row; }
            .customer-add .table-add tr td.right .fb-cover-error { top: 0; right: auto; }
            #success-alert{
                top:100px;
                right:10px;
                position:fixed;
                width:20% !important;
                z-index:2000;
            }
        </style>
        <div class="alert alert-danger" id="success-alert">
            <%--<button type="button" class="close" data-dismiss="alert">x</button>--%>
            <strong>Cảnh báo: </strong>
            <span id="msg-alert"></span>
        </div>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Tuyển dụng &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/admin/tuyen-dung/danh-sach-nguoi-test.html">Danh sách người test</a></li>
                        <li class="li-add active"><a href="/admin/tuyen-dung/them-moi-nguoi-test.html"> Thêm mới </a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add admin-product-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="table-wp">
                    <table class="table-add admin-product-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Nhập thông tin của Tester<i style="color:red">&nbsp;&nbsp Các trường đánh dấu * là bắt buộc</i></strong></td>
                                <td></td>
                            </tr>
                            <tr class="tr-margin" style="height: 20px;"></tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>ID Nhân viên <strong style="color:red"> &nbsp*</strong></span></td>
                                <td class="col-xs-9 right">
                                     <div class="field-wp">
                                        <div class="div_col">
                                        <asp:TextBox ID="txtStaffID" CssClass="form-control" class="number-only" runat="server" ClientIDMode="Static" placeholder="ID Nhân viên"></asp:TextBox>
                                        </div>
                                         <div class="div_col">
                                         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Fullname"
                                                InitialValue="" ErrorMessage="<i style='color:red'>Vui lòng nhập ID nhân viên!!!</i>"  SetFocusOnError="true"></asp:RequiredFieldValidator>
                                        </div>
                                         </div>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Họ tên</span></td>
                                <td class="col-xs-9 right">
                                     <div class="field-wp">
                                        <div class="div_col">
                                        <asp:TextBox ID="Fullname" CssClass="form-control"  runat="server" ClientIDMode="Static" placeholder="Họ tên"></asp:TextBox>
                                        </div>
                                         <div class="div_col">
                                         <%--<asp:RequiredFieldValidator ID="rfvFullname" runat="server" ControlToValidate="Fullname"
                                                InitialValue="" ErrorMessage="<i style='color:red'>Vui lòng nhập tên người test!!!</i>"  SetFocusOnError="true"></asp:RequiredFieldValidator>--%>
                                        </div>
                                         </div>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Email</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                     <asp:TextBox ID="Email" CssClass="form-control" class="number-only" runat="server"  ClientIDMode="Static" placeholder="Email"></asp:TextBox>
                                            </div>
                                         <div class="div_col">
                                    <%--<asp:RequiredFieldValidator ID="rfdEmail" runat="server" ControlToValidate="Email"
                                                InitialValue="" ErrorMessage="<i style='color:red'>Vui lòng nhập email!!!</i>"  SetFocusOnError="true"></asp:RequiredFieldValidator>--%>
                                             </div>
                                         </div>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                
                                <td class="col-xs-3 left"><span>Số ĐT</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                     <asp:TextBox ID="Phone" CssClass="form-control" class="number-only" runat="server"  ClientIDMode="Static" placeholder="Phone"></asp:TextBox>
                                     </div>
                                         <div class="div_col">
                                    <%--<asp:RequiredFieldValidator ID="rfdPhone" runat="server" ControlToValidate="Email"
                                                InitialValue="" ErrorMessage="<i style='color:red'>Vui lòng nhập số ĐT!!!</i>" SetFocusOnError="true"></asp:RequiredFieldValidator>--%>
                                              </div>
                                         </div>
                                </td>
                            </tr>
                            <tr class="tr-send">
                                <td class="col-xs-3 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <asp:Button ID="Send" CssClass="btn-send" runat="server" Text="Xác nhận" ClientIDMode="Static"  OnClick="AddorUpdate"></asp:Button>
                                    </span>
                 
                                </td>
                            </tr>
         
                        </tbody>
                    </table>
                </div>
            </div>
            <%-- end Add --%>
        </div>

        
 
        <script>
            jQuery(document).ready(function () {
                $("#success-alert").hide();
                $("#glbTuyendungV2").addClass("active");
                $("#glbCreatCV").addClass("active");
                $("#glbCreateNewCV").addClass("active");
                //date picker
                $('.txtdatetime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        
                    }
                });

            });
            $(document).on("keypress", "#txtStaffID", function (e) {
                if (e.keyCode === 13) {
                    e.preventDefault(); // Ensure it is only this code that rusn

                    var c = confirm("Bạn chắc chắn muốn thêm nhân viên " + $("#txtStaffID").val() + " làm người test không?");

                    if (c==true) {
                        $.ajax({
                            datatype: "json",
                            type: "POST",
                            url:'/GUI/BackEnd/TuyenDungV2/CreateTester.aspx/get_Info_Staff',
                            contentType: "application/json; charset=UTF-8",
                            data: '{staffID : ' + $("#txtStaffID").val() + '}',
                            success: function (data) {
                                var Jsondata = JSON.parse(data.d);
                                if (Jsondata != null) {
                                    
                                    $("#FullName").val(Jsondata.Fullname);
                                        $("#Phone").val(Jsondata.Phone);
                                        $("#Email").val(Jsondata.Email);
                                    //console.log(Jsondata.Phone);
                                }
                                else {
                                    alert("Không tìm thấy dữ liệu của nhân viên " + $("#txtStaffID").val());

                                }
                            }
                        })
                    }
                }
            })
           
        </script>

        

    </asp:Panel>
    <style>
        .field-wp .div_col { width: 49%; float: left; }
            .field-wp .div_col:first-child { margin-right: 10px; }
        .customer-add .table-add tr.tr-field-ahalf .div_col input, .customer-add .table-add tr.tr-field-ahalf .div_col textarea, .customer-add .table-add tr.tr-field-ahalf .div_col select { width: 100%; }
        .select2-container--default .select2-selection--single .select2-selection__rendered { line-height: inherit; }
        .span_note { color: red; width: inherit; }
        .cls_gender { width: 120px; float: left; height: 36px; background: #dfdfdf; margin-left: 10px; text-align: center; line-height: 36px; cursor: pointer; }
            .cls_gender:first-child { margin-left: 0px; }
            .cls_gender.active { background: #fcd72a; }

        .cls_hoso { width: 120px; float: left; height: 36px; background: #dfdfdf; margin-left: 10px; text-align: center; line-height: 36px; cursor: pointer; }
            .cls_hoso:first-child { margin-left: 0px; }
            .cls_hoso.active { background: #fcd72a; }
        /*.customer-add .table-add td span{width:100% !important;}*/
       /*@media (max-width: 768px) { 
       .customer-add .content-wp {padding:0px;}
       #glbMenu {width:85%;}
       }*/
    </style>
   
</asp:Content>
