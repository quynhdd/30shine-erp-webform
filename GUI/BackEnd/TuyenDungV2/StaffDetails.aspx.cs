﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Script.Serialization;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.TuyenDungV2
{
    public partial class StaffDetails : System.Web.UI.Page
    {
        private string PageID = "TD_DETAIL";
        protected TuyenDung_UngVien OBJ;
        protected string _Code;
        protected bool _IsUpdate = false;
        public UIHelpers _UIHelpers = new UIHelpers();
        protected cls_media Img = new cls_media();
        protected List<cls_media> listImg = new List<cls_media>();
        protected bool HasImages = false;
        private bool Perm_AllPermission = false;
        private bool Perm_Access = false;
        private bool _boolean;
        protected List<string> ListImagesUrl = new List<string>();
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_AllPermission = permissionModel.GetActionByActionNameAndPageId("Perm_AllPermission", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_AllPermission = permissionModel.CheckPermisionByAction("Perm_AllPermission", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                if (IsUpdate())
                {
                    Bind_OBJ();
                    if (OBJ.DepartmentId ==1)
                    {
                        BindSkillNameLevel();
                    }

                }
                else
                {
                    //
                }

            }
        }


        private void Add()
        {
            //           
        }
        private bool Bind_OBJ()
        {
            using (var db = new Solution_30shineEntities())
            {
                var serialize = new JavaScriptSerializer();
                var ExistOBJ = true;

                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.TuyenDung_UngVien.FirstOrDefault(w => w.Id == Id);
                    if (!OBJ.Equals(null))
                    {
                        HDF_IMG_CMT1.Value = OBJ.CMTimg1;
                        HDF_IMG_SKILL1.Value = OBJ.ImgSkill1;
                        HDF_IMG_SKILL2.Value = OBJ.ImgSkill2;
                        //VideoLink.Text = OBJ.VideoLink;
                    }
                    else
                    {
                        ExistOBJ = false;
                        var msg = "Lỗi! Bản ghi không tồn tại";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                    var obj = db.Store_TuyenDung_Step3_ChiTietdaduyet_Listing(Id);
                    foreach (var item in obj)
                    {
                        txtVideoLink.Text = item.VideoLink;
                        FullName.Text = item.FullName;
                        Phone.Text = item.Phone;
                        CMT.Text = item.CMT;
                        bophan.Text = item.DepartmentName;
                        Step2Note.Text = item.Step2Note;
                        StepEndNote.Text = item.StepEndNote;
                        Trangthai.Text = item.StatusName;
                    }

                }
                else
                {
                    ExistOBJ = false;
                    var msg = "Lỗi! Bản ghi không tồn tại";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }

                return ExistOBJ;
            }
        }

        public void ReloadPage()
        {
            HttpRequest Request = HttpContext.Current.Request;
            string host = "http://" + Request.Url.Host.ToString();
            string rawUrl = Request.RawUrl.ToString();
            string Url = host + rawUrl;
            Response.Redirect(Url);
        }

        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }
        #region lấy dữ liệu skill level
        private static string chuoi = "";
        private void BindSkillNameLevel()
        {
            _Code = Request.QueryString["Code"];
            int Id;

            using (var db = new Solution_30shineEntities())
            {
                if (int.TryParse(_Code, out Id))
                {
                    var listId = db.TuyenDung_DanhGia.Where(w => w.UngVienID == Id).ToList();
                    if (listId.Count > 0)
                    {
                        for (int i = 0; i < listId.Count; i++)
                        {
                            chuoi = listId[i].MapSkillLevelID;
                        }
                    }
                }
                var listSkillName = db.Database.SqlQuery<cls_skillname>("select map.Id as IdSkill , skill.SkillName from TuyenDung_SkillLevel_Map as map join TuyenDung_Skill as skill on map.SkillID = skill.Id where map.IsDelete = 0 and map.Id in (" + chuoi + ")").ToList();
                if (listSkillName.Count > 0)
                {
                    rptSkillName.DataSource = listSkillName;
                    rptSkillName.DataBind();
                }
            }
        }
        /// <summary>
        /// Sự kiện IteamDataBound lấy dữ liệu đổ vào Dropdownlist
        /// </summary>
        protected void rptSkillName_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                var dataItem = (cls_skillname)e.Item.DataItem;
                if (dataItem != null)
                {
                    DropDownList rpt = e.Item.FindControl("drlLevelName") as DropDownList;
                    using (var db = new Solution_30shineEntities())
                    {
                        var listLevel = db.Database.SqlQuery<cls_SkillLevel>("select lev.Id, CONCAT((lev.LevelName), ' --- ', (map.[Description])) as levelName from TuyenDung_SkillLevel_Map as map join TuyenDung_SkillLevel as lev on map.LevelID = lev.Id where map.IsDelete = 0 and map.Id in (" + dataItem.IdSkill + ")").ToList();
                        rpt.Items.Clear();
                        rpt.DataTextField = "levelName";
                        rpt.DataValueField = "Id";
                        rpt.DataSource = listLevel;
                        rpt.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public class cls_skillname
        {
            public int IdSkill { get; set; }
            public string skillName { get; set; }
        }
        public class cls_SkillLevel
        {
            public int Id { get; set; }
            public string levelName { get; set; }
            public string description { get; set; }
        }
#endregion
        
        public class cls_media
        {
            public string url { get; set; }
            public string title { get; set; }
            public string description { get; set; }
        }
    }
}