﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TemplateMaster/SiteMaster.Master" CodeBehind="ReportTotal.aspx.cs" Inherits="_30shine.GUI.BackEnd.TuyenDungV2.ReportTotal" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <%--<uc1:ReportMenu runat="server" id="ReportMenu" />--%>

        <style>
            .table-listing .uv-avatar { width: 120px; }
            .fb-cover-error{
                 top:100px !important;
                right:10px !important;
                position:fixed !important;
                width:20% !important;
                z-index:2000;
            }
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Tuyển dụng &nbsp;&#187; </li>
                        <li class="li-listing active"><a href="/admin/tuyen-dung/lap-ho-so/danh-sach-ho-so.html">Danh sách ứng viên</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report be-report-timekeeping">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
                <div class="row">
                    <div class="filter-item">
                        <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="time-wp">
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeFrom" placeholder="Từ ngày" Style="margin-left: 10px!important"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ValidateTime" ControlToValidate="TxtDateTimeFrom" runat="server" CssClass="fb-cover-error" Text="Vui lòng chọn thời gian!"></asp:RequiredFieldValidator>
                        </div>
                        <strong class="st-head" style="margin-left: 10px;">
                            <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                        </strong>
                        <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeTo" placeholder="Đến ngày" Style="margin-left: 10px!important"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                        <br />
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlDepartment" runat="server" ClientIDMode="Static" Style="width: 220px;"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlStatus" runat="server" ClientIDMode="Static" Style="width: 220px;">

                        </asp:DropDownList>
                    </div>
                    <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <a href="javascript:void(0);" onclick="location.href=location.href" class="st-head btn-viewdata">Reset Filter</a>
                </div>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>Danh sách phê duyệt</strong>
                </div>
                <!-- Row Table Filter -->
                <div class="table-func-panel" style="margin-top: -33px;">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="row table-wp">
                            <table class="table-add table-listing">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Họ tên</th>
                                        <th>Salon</th>
                                        <th>Số điện thoại</th>
                                        <th>Bộ phận</th>
                                        <th>Trạng thái</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptDanhsach" runat="server">
                                        <ItemTemplate>
                                            <tr onclick="redirect($(this));" data-id ="<%# Eval("Id") %>">
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td><%# Eval("FullName") %></td>
                                                <td><%# Eval("SalonName") %></td>
                                                <td><%# Eval("Phone") %></td>
                                                <td><%# Eval("DepartmentName") %></td>
                                                <td class="map-edit">
                                                    <%# Eval("StatusName")%>
                                                    <div class="edit-wp">
                                                        <a class="elm edit-btn" href="/admin/tuyen-dung/chi-tiet-ung-vien/<%# Eval("Id") %>.html" title="Sửa" onclick="assignFilter()"></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />

                <!-- Hidden Field-->
                <asp:HiddenField ID="HDF_TypeStaff" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_Staff" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_ExcelType" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_ExportType" ClientIDMode="Static" runat="server" />
                <!-- END Hidden Field-->
            </div>
            <%-- END Listing --%>
        </div>

        <link href="/Assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <script src="/Assets/js/jquery.mCustomScrollbar.js"></script>
        <script type="text/ecmascript">

            function redirect(This) {
                //alert($(This).attr("data-id")); return;
                window.location = '/admin/tuyen-dung/chi-tiet-ung-vien/' + $(This).attr("data-id") + '.html';
            }
            jQuery(document).ready(function () {
                //$('#ViewData').click();
                // Add active menu
                $("#glbTuyendungV2").addClass("active");
                $("#glbReportConfirm").addClass("active");
               

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });

                //============================
                // Bind Staff
                //============================
                BindStaffFilter();

                // View data yesterday
                $(".tag-date-today").click();

                // Xử lý kiểm tra biến filter
                var filter = localStorage.getItem("filterObject");
                var filterObject = {};
                console.log(filter);
                if (filter != null) {
                    try {
                        filterObject = JSON.parse(filter);
                        $("#TxtDateTimeFrom").val(filterObject.timeFrom);
                        $("#TxtDateTimeTo").val(filterObject.timeTo);
                        $("#ddlDepartment").val(filterObject.departmentId);
                        localStorage.removeItem("filterObject");
                        $("#ViewData").click();
                    }
                    catch (err) {
                        console.log(err.message);
                    }
                }


            });

            function assignFilter() {
                filterObject = {};
                filterObject.timeFrom = $("#TxtDateTimeFrom").val();
                filterObject.timeTo = $("#TxtDateTimeTo").val();
                filterObject.departmentId = $("#ddlDepartment").val();
                localStorage.setItem("filterObject", JSON.stringify(filterObject));
            }

            function showTrPrice() {
                $(".tr-price").show();
            }

            function BindStaffFilter() {
                $(".eb-select").bind("focus", function () {
                    EBSelect_ShowBox($(this).parent().find(".eb-select-data"));
                });
                $(window).bind("click", function (e) {
                    console.log(e);
                    if ((!e.target.className.match("eb-select") && e.target.parentElement.className.match("ul-listing-staff")) ||
                        (!e.target.className.match("eb-select") && !e.target.parentElement.className.match("ul-listing-staff"))
                    && !e.target.className.match("mCSB_dragger_bar")) {
                        EBSelect_HideBox();
                    }
                });
                //============================
                // Scroll Staff Filter
                //============================
                $('.eb-select-data').each(function () {
                    if ($(this).height() > 230) {
                        $(this).mCustomScrollbar({
                            theme: "dark-2",
                            scrollInertia: 100
                        });
                    }
                });
            }

            function BindIdToHDF(THIS, id, HDF_DomId, Input_DomId) {
                var text = THIS.innerText.trim(),
                    HDF_Dom = document.getElementById(HDF_DomId),
                    InputText = document.getElementById(Input_DomId);
                HDF_Dom.value = id;
                InputText.value = text;
                InputText.setAttribute("data-value", id);
                ajaxGetStaffsByType(id);
                if (HDF_DomId == "HDF_TypeStaff") {
                    $("#StaffName").val("");
                    $("#HDF_Staff").val("");
                }
            }

            function ajaxGetStaffsByType(type) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Report/TimeKeeping.aspx/Load_Staff_ByType",
                    data: '{type : "' + type + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            var staffs = JSON.parse(mission.msg);
                            if (staffs.length > 0) {
                                var tmp = "";
                                $.each(staffs, function (i, v) {
                                    tmp += '<li data-code="' + v.Code + '" data-id="' + v.Id + '"' +
                                            'onclick="BindIdToHDF(this,' + v.Id + ',\'HDF_Staff\',\'StaffName\')">' +
                                            v.Fullname + '</li>';
                                });
                                $("#UlListStaff").empty().append(tmp);
                            }

                        } else {
                            var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                            showMsgSystem(msg, "warning");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function EBSelect_ShowBox(Dom) {
                EBSelect_HideBox();
                Dom.show();
            }

            function EBSelect_HideBox() {
                $(".eb-select-data").hide();
            }

            function EBSelect_BindData() {

            }

            function ReplaceZezoValue() {
                $("table.table-listing td").each(function () {
                    if ($(this).text().trim() == "0") {
                        $(this).text("-");
                    }
                });
            }

            function viewDataByDate(This, time) {
                $(".tag-date.active").removeClass("active");
                This.addClass("active");
                $("#TxtDateTimeTo").val("");
                $("#TxtDateTimeFrom").val(time);
                $("#ViewData").click();
            }

            //function GetStatus(This, UngVienStatusId) {
            //    // set trạng thái active cho button khi click
            //    $.ajax({
            //        url: "/GUI/FrontEnd/PreviewImage/Preview_Random_Salon_Images.aspx/updateStatus",
            //        type: "post",
            //        data: '{UngVienStatusId:' + UngVienStatusId + '}',
            //        contentType: "application/json; charset=utf-8",
            //        dataType: "json",
            //        success: function (data) {
            //            console.log(data.d);
            //            var msg = JSON.parse(data.d);
            //            if (msg.success) {
            //                var index = findIndexByBillID(billID);
            //                if (index != null) {
            //                    _ListPreviewImg[index].ImageStatusId = statusID;
            //                }
            //            }
            //        },
            //        error: function (data) {
            //            alert(response.responseText);
            //        }
            //    });
            //}

        </script>

    </asp:Panel>
</asp:Content>
