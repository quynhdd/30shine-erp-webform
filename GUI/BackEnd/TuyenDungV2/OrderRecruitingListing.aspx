﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderRecruitingListing.aspx.cs" MasterPageFile="~/TemplateMaster/SiteMaster.Master" Inherits="_30shine.GUI.BackEnd.TuyenDungV2.OrderRecruitingListing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            table.table-report-department {
            }

            table.table-report-department {
                border-collapse: collapse;
            }

            table.table-report-department, .table-report-department th, .table-report-department td {
                border: 1px solid #bababa;
            }

            .table-report-department th {
                font-weight: normal;
                font-family: Roboto Condensed Bold;
                padding: 5px 10px;
            }

            .table-report-department td {
                padding: 5px 10px;
            }

            .tip {
                /*border-bottom: 1px dashed;*/
                text-decoration: none
            }

                .tip:hover {
                    cursor: pointer;
                    position: relative
                }

                .tip span {
                    display: none
                }

                .tip:hover span {
                    border: #c0c0c0 1px solid;
                    border-radius: 5px;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    padding: 5px 20px 5px 5px;
                    display: block;
                    z-index: 100;
                    background: #c1d9ff repeat-x;
                    left: 0px;
                    margin: 10px;
                    width: auto !important;
                    min-width: 100px;
                    position: absolute;
                    bottom: 0px;
                    text-decoration: none;
                    color: #252734;
                }

            .alert {
                margin-bottom: 0 !important;
            }

            .btn-submit-staff {
                width: 100px;
            }
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Báo cáo &nbsp;&#187; </li>
                        <li class="be-report-li"><a href="/admin/tuyen-dung/them-moi-yeu-cau.html"><i class="fa fa-th-large"></i>THÊM MỚI YÊU CẦU TUYỂN DỤNG</a></li>
                        <li class="be-report-li"><a href="#"><i class="fa fa-th-large"></i><b>THỐNG KÊ YÊU CẦU TUYỂN DỤNG</b></a></li>
                        <li class="be-report-li"><a href="/admin/tuyen-dung/tien-do-hoan-thanh-yeu-cau.html"><i class="fa fa-th-large"></i>TIẾN ĐỘ HOÀN THÀNH YÊU CẦU TUYỂN DỤNG</a></li>

                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing be-report be-report-timekeeping">
            <%-- Listing --%>
            <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                    <div class="time-wp">
                        <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="ValidateTime" ControlToValidate="TxtDateTimeFrom" runat="server" CssClass="fb-cover-error" Text="Vui lòng chọn thời gian!"></asp:RequiredFieldValidator>
                    </div>
                    <strong class="st-head" style="margin-left: 10px;">
                        <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                    </strong>
                    <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeTo" placeholder="Đến ngày"
                        ClientIDMode="Static" runat="server"></asp:TextBox>

                    <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" Style="z-index: 99999 !important" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <asp:Panel ID="resetFillter" CssClass="st-head btn-viewdata" ClientIDMode="Static" runat="server">
                        Reset Filter
                    </asp:Panel>
                </div>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-filter"></i>Lọc kết quả</strong>
                    <asp:UpdatePanel runat="server" ID="UPsalon">
                        <ContentTemplate>
                            <div class="filter-item">
                                <asp:DropDownList ID="Salon" runat="server" CssClass="form-control select" ClientIDMode="Static" Style="width: 190px;" AutoPostBack="true"></asp:DropDownList>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel runat="server" ID="UPStaff">
                        <ContentTemplate>
                            <div class="filter-item">
                                <asp:DropDownList ID="ddlDepartment" CssClass="form-control" runat="server" ClientIDMode="Static" Style="width: 190px;" AutoPostBack="true"></asp:DropDownList>
                            </div>
                            <div class="filter-item">
                                <asp:DropDownList ID="ddlFillter" CssClass="form-control" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 149px;">
                                    <%--<asp:ListItem Text="Ngày cần" Value="1"></asp:ListItem>--%>
                                    <asp:ListItem Text="Ngày tạo yêu cầu" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Ngày quy định có" Value="3"></asp:ListItem>
                                    <asp:ListItem Text="Ngày có mã nhân sự" Value="4"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="filter-item">
                                <asp:DropDownList ID="ddlStatus" CssClass="form-control" runat="server" ClientIDMode="Static" AutoPostBack="true" Style="margin: 12px 0; width: 149px;">
                                </asp:DropDownList>
                            </div>
                        </ContentTemplate>
                        <Triggers></Triggers>
                    </asp:UpdatePanel>
                </div>
                <!-- Row Table Filter -->
                <div class="table-func-panel" style="margin-top: -33px;">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <!-- End Row Table Filter -->
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="row table-wp">
                            <table class="table-add table-listing table-listing-salary">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Khu vực</th>
                                        <th>Người tạo</th>
                                        <th>Salon</th>
                                        <th>Bộ phận</th>
                                        <th>Số lượng</th>
                                        <th>Ngày tạo yêu cầu</th>
                                        <th>Loại yêu cầu</th>
                                        <%--<th>Ngày cần</th>--%>
                                        <th>Ngày quy định có</th>
                                        <th>Hành động</th>
                                        <th>Trạng thái</th>
                                        <th>Mã nhân sự</th>
                                        <th>Ngày tạo mã</th>
                                        <th>Đã có nhân sự</th>
                                        <th>Hủy yêu cầu</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <asp:Repeater ID="rptSalary" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td data-id="<%# Eval("OrderId") %>" onclick="ShowLogsOrder($(this))" class="click-show"><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td data-id="<%# Eval("OrderId") %>" onclick="ShowLogsOrder($(this))" class="click-show"><%# Eval("RegionId") %></td>
                                                <td data-id="<%# Eval("OrderId") %>" onclick="ShowLogsOrder($(this))" class="click-show"><%# Eval("CreatorName") %></td>
                                                <td data-id="<%# Eval("OrderId") %>" onclick="ShowLogsOrder($(this))" class="click-show"><%# Eval("SalonName") %></td>
                                                <td data-id="<%# Eval("OrderId") %>" onclick="ShowLogsOrder($(this))" class="click-show"><%# Eval("Department") %></td>
                                                <td data-id="<%# Eval("OrderId") %>" onclick="ShowLogsOrder($(this))" class="click-show"><%# Eval("Quantity") %></td>
                                                <td data-id="<%# Eval("OrderId") %>" onclick="ShowLogsOrder($(this))" class="click-show"><%# Eval("CreatedDate") %></td>
                                                <td data-id="<%# Eval("OrderId") %>" onclick="ShowLogsOrder($(this))" class="click-show"><%# Eval("Type") %></td>
                                                <%--<td data-id="<%# Eval("OrderId") %>" onclick="ShowLogsOrder($(this))" class="click-show"><%# Eval("NeedDate") %></td>--%>
                                                <td data-id="<%# Eval("OrderId") %>" onclick="ShowLogsOrder($(this))" class="click-show"><%# Eval("CompleteDate") %></td>
                                                <td class="action"><span><%# Eval("Action") %></span></td>
                                                <td class="status"><%# Eval("Status") %></td>
                                                <td class="td-staff-id" style="min-width: 205px;"><%# Eval("StaffId") == null && Eval("RevokeId").ToString() == "0" ? "<input type='text' class='form-control staff-id-textbox' style='width: 70px;margin-right: 10px;' placeholder='Nhập mã nv(nếu có)'/><button type='button' data-id='"+Eval("OrderId")+"' data-type='"+Eval("DepartmentId")+"' onclick='UpdateStaffId($(this))' class='form-control btn-info btn-submit-staff'>Xác nhận</button>" : Eval("StaffId") %></td>
                                                <td class="td-staff-date"><%# Eval("StaffDate") %></td>
                                                <td>
                                                    <input type="checkbox" class="is-have" <%# Eval("RevokeId").ToString() != "0" ? "disabled='disabled'" : "" %> data-id="<%# Eval("OrderId") %>" onchange="OnchangeCheckbox($(this))" <%# (Convert.ToInt32(Eval("HavingTry")) == 0 ? "" : "checked='checked'") %>></td>
                                                <td>
                                                    <select style="width: 90% !important" class="form-control cancel-recruitment" <%# (Convert.ToInt32(Eval("RevokeId")) != 0 || Convert.ToInt32(Eval("_Status")) != 1) ? "disabled='disabled'" : "" %> onchange="on_change($(this))">
                                                        <option value="0" <%# Convert.ToInt32(Eval("RevokeId")) == 0 ? "selected='selected'" : "" %>>--Lý do hủy--</option>
                                                        <option value="1" <%# Convert.ToInt32(Eval("RevokeId")) == 1 ? "selected='selected'" : "" %>>Tạo sai</option>
                                                        <option value="2" <%# Convert.ToInt32(Eval("RevokeId")) == 2 ? "selected='selected'" : "" %>>SL không tuyển nữa</option>
                                                        <option value="3" <%# Convert.ToInt32(Eval("RevokeId")) == 3 ? "selected='selected'" : "" %>>NS từ chối yêu cầu</option>
                                                    </select>
                                                    <div class="reason-div" style="display: none">
                                                        <br />
                                                        <textarea class="form-control txtReason" rows="3" style="width: 90%; margin-left: 11px; margin-top: 5px;" placeholder="Vui lòng nhập lý do hủy"></textarea>
                                                        <br />
                                                        <button type="button" class="form-control btn-submit btn-danger" style="width: 50%; margin-right: 11px; margin-top: 5px !important; float: right;" onclick="DeleteOrder($(this))" data-id="<%# Eval("OrderId") %>">Xác nhận</button>
                                                    </div>
                                                </td>

                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>

                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>

                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />

                <!-- Hidden Field-->
                <asp:HiddenField ID="HDF_TypeStaff" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_Staff" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_ExcelType" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_ExportType" ClientIDMode="Static" runat="server" />
                <!-- END Hidden Field-->
            </div>
            <%-- END Listing --%>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="show-modal-logs" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <input type="hidden" id="orderId" />
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h3 style="text-align: center; font-size: 20px;"><b>Lịch sử cập nhật yêu cầu</b></h3>
                    </div>
                    <div class="modal-body">
                        <table class="table table-listing table-logs">
                            <thead>
                                <tr>

                                    <%--<th>Order ID</th>--%>
                                    <th>Người cập nhật</th>
                                    <th>Loại cập nhật</th>
                                    <th>Trạng thái cập nhật</th>
                                    <th>Thời gian</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
        <style>
            .be-report table.table-listing.table-listing-salary td {
                padding: 7px 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                margin-top: 4px !important;
            }

                .select2-container--default .select2-selection--single .select2-selection__arrow {
                    margin-top: 5px;
                }
        </style>
        <link href="/Assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <script src="/Assets/js/jquery.mCustomScrollbar.js"></script>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <style>
            .select2-container {
                width: 190px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/ecmascript">
            jQuery(document).ready(function () {
                $('.select').select2();
                // Add active menu
                $("#glbAdminSales").addClass("active");
                $("#glbAdminReportSalary").addClass("active");
                $("li#LuongNhanVien").addClass("active");

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });
            });

            function DeleteOrder(This) {
                var row = This.closest("tr");
                var txt_reason = row.find(".txtReason").val();
                var orderId = This.attr("data-id");
                var reasonText = row.find(".cancel-recruitment option:selected").text() + ": " + txt_reason;
                var reasonId = row.find(".cancel-recruitment").val();
                if (txt_reason == "" || txt_reason == null) {
                    ShowMessage("Lỗi", "Lý do hủy không dược để trống", 4);
                    row.find(".txtReason").focus();
                    row.find(".txtReason").css("border-color", "red");
                    return false;
                }
                $.ajax({
                    url: "/GUI/BackEnd/TuyenDungV2/OrderRecruitingListing.aspx/DeleteOrder",
                    type: "POST",
                    data: "{orderId:" + orderId + ",revokeId:" + reasonId + ",reason:'" + reasonText + "'}",
                    dataType: "json",
                    contentType: "application/json;charset:utf-8",
                    success: function (response) {
                        if (response.d == 0) {
                            ShowMessage("Lỗi", "Bản ghi không tồn tại. Vui lòng kiểm tra lại", 4);
                        }
                        else ShowMessage("Thành công", "Huỷ thành công!", 2);
                        row.find(".action").html("Đã hủy &nbsp<i class='fa fa-info-circle tip'><span>" + reasonText + "</span></i>");
                        row.find(".status").html("<span style='color:red'>Đã hủy</span>");
                        row.find(".cancel-recruitment").attr("disabled", "disabled");
                        row.find(".reason-div").css("display", "none");
                        row.find(".staff-id-textbox").css("display", "none");
                        row.find(".btn-submit-staff").css("display", "none");
                        row.find(".is-have").css("display", "none");
                        //row.remove();
                    }
                })
            }
            function on_change(This) {
                if (This.val() != 0) {
                    This.closest("tr").find(".reason-div").css("display", "");
                }
                else This.closest("tr").find(".reason-div").css("display", "none");
            }
            function OnchangeCheckbox(This) {
                var checked = 0;
                var orderId = This.attr("data-id");
                if (This.is(':checked')) {
                    checked = 1;
                }
                else checked = 0;
                $.ajax({
                    url: "/GUI/BackEnd/TuyenDungV2/OrderRecruitingListing.aspx/UpdateHavingTry",
                    type: "POST",
                    data: "{orderId:" + orderId + ",status:" + checked + "}",
                    dataType: "json",
                    contentType: "application/json;charset:utf-8",
                    success: function (response) {
                        if (response.d == 0) {
                            ShowMessage("Lỗi", "Bản ghi không tồn tại. Vui lòng kiểm tra lại", 4);
                        }
                        else ShowMessage("Thành công", "Update trạng thái đã có nhân sự thành công", 2);
                    }
                })
            }
            function UpdateStaffId(This) {
                var tr = This.closest("tr");
                var staffId = tr.find(".staff-id-textbox").val();
                var orderID = This.attr("data-id");
                var departmentId = This.attr("data-type");
                if (staffId == "" || staffId == null) {
                    ShowMessage("Thông báo", "Vui lòng nhập mã NV", 4);
                    tr.find(".staff-id-textbox").focus();
                    tr.find(".staff-id-textbox").css("border-color", "red");
                    return false;
                }
                var con = confirm("Bạn chắc chắn muốn nhập mã nhân viên? Hành động này sẽ hoàn thành luôn yêu cầu tuyển dụng này");
                if (con) {
                    $.ajax({
                        url: "/GUI/BackEnd/TuyenDungV2/OrderRecruitingListing.aspx/UpdateStaffId",
                        data: "{staffId:" + staffId + ",orderId:" + orderID + ",departmentId:" + departmentId + "}",
                        type: "POST",
                        dataType: "json",
                        contentType: "application/json;charset:utf-8",
                        success: function (response) {
                            var jsonData = JSON.parse(response.d);
                            switch (parseInt(jsonData.status)) {
                                case 1:
                                    tr.find(".staff-id-textbox").focus();
                                    tr.find(".staff-id-textbox").css("border-color", "red");
                                    ShowMessage("Thông báo", jsonData.msg, 3);
                                    break;
                                case 2:
                                    tr.find(".staff-id-textbox").focus();
                                    tr.find(".staff-id-textbox").css("border-color", "red");
                                    ShowMessage("Thông báo", jsonData.msg, 3);
                                    break;
                                case 3:
                                    ShowMessage("Thông báo", jsonData.msg, 4);
                                    break;
                                default:
                                    tr.find(".form-control.cancel-recruitment").attr("disabled", "disabled");
                                    tr.find(".td-staff-date").text(new Date().format("yyyy/MM/dd HH:mm:ss"));
                                    tr.find("td.action").text("Hoàn thành");
                                    tr.find(".status").html("<span class='alert alert-success'>Hoàn thành</span>");
                                    tr.find(".is-have").attr('checked', 'checked');
                                    tr.find(".td-staff-id").text(staffId);
                                    ShowMessage("Thông báo", jsonData.msg, 1);
                                    break;
                            }

                        }
                    })
                }
                else return false;
            }
            function ShowLogsOrder(This) {
                $("#orderId").val(This.attr("data-id"));
                $("#show-modal-logs").modal();
                $.ajax({
                    url: "/GUI/BackEnd/TuyenDungV2/OrderRecruitingListing.aspx/GetLogs",
                    data: "{orderId:" + parseInt($("#orderId").val()) + "}",
                    type: "POST",
                    dataType: "json",
                    contentType: "application/json;charset:utf-8",
                    success: function (response) {
                        var jsData = JSON.parse(response.d);
                        $(".table-logs tbody").empty();
                        var tr = "";
                        for (var i = 0; i < jsData.length; i++) {
                            tr += "<tr>";
                            //tr += "<td>" + jsData[i].OrderId + "</td>";
                            tr += "<td>" + jsData[i].CreateStaffId + " | " + jsData[i].CreateStaffName + "</td>";
                            tr += "<td>" + jsData[i].TypeLog + "</td>";
                            tr += "<td>" + jsData[i].Value + "</td>";
                            tr += "<td>" + ToJavaScriptDate(jsData[i].CreatedDate) + "</td>";
                            tr += "</tr>";
                        }
                        $(".table-logs tbody").append(tr);
                    }
                })
            }

            function ToJavaScriptDate(value) {
                var pattern = /Date\(([^)]+)\)/;
                var results = pattern.exec(value);
                var dt = new Date(parseFloat(results[1]));
                return (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear() + " " + dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
            }
        </script>
    </asp:Panel>
</asp:Content>
