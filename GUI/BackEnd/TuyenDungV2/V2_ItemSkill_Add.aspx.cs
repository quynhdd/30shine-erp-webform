﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity.Migrations;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.TuyenDungV2
{
    public partial class V2_ItemSkill_Add : System.Web.UI.Page
    {
        private string PageID = "";
        private bool Perm_Access = false;
        protected string _Code;
        //static int cd = Convert.ToInt32(Request.QueryString["Code"]);
        protected bool _IsUpdate = false;
        private Solution_30shineEntities db = new Solution_30shineEntities();
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //if (Request.QueryString["Code"] != null)
                //{
                //    PageID = "TD_KN_ADD";
                //}
                //else
                //{
                //    PageID = "TD_KN_EDIT";
                //}
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                ////Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            _Code = Request.QueryString["Code"];
            SetPermission();
            if (!IsPostBack)
            {
                BindSkillLevel();
                if (IsUpdate())
                {
                    BindUpdate();
                }
            }
        }
        #region[Bind bậc kỹ năng]
        private void BindSkillLevel()
        {
            using (var db = new Solution_30shineEntities())
            {
                var listSkill = db.Database.SqlQuery<cls_skillLevel>("select* from TuyenDung_SkillLevel where IsDelete = 'False' and Publish = 'True'").ToList();
                int _Id = Convert.ToInt32(_Code);
                var skillevel = db.TuyenDung_SkillLevel_Map.Where(w => w.SkillID == _Id && w.IsDelete == false).ToList();
                var loop = 0;
                foreach (var v in listSkill)
                {
                    var index = skillevel.FindIndex(w => w.LevelID == v.Id);
                    if (index != -1)
                    {
                        listSkill[loop].isChecked = true;
                        listSkill[loop].Descr = skillevel[index].Description;
                    }
                    else
                    {
                        listSkill[loop].isChecked = false;
                    }
                    loop++;
                }
                rptSkillLevel.DataSource = listSkill;
                rptSkillLevel.DataBind();
            }
        }
        #endregion
        #region[Insert]
        private void Add()
        {
            using (var db = new Solution_30shineEntities())
            {
                var obj = new TuyenDung_Skill();
                obj.SkillName = Name.Text;
                obj.Description = Description.Text;
                obj.Publish = Publish.Checked;
                obj.IsDelete = false;
                obj.CreatedTime = DateTime.Now;
                var erro = false;
                if (!erro)
                {
                    db.TuyenDung_Skill.Add(obj);
                    var exc = db.SaveChanges();
                    int SkillId = obj.Id;
                    int LevelId = 0;
                    string Des = "";
                    var serializer = new JavaScriptSerializer();
                    var _listLevel = new List<cls_employee_skill_level>();
                    var item = new cls_employee_skill_level();
                    if (HDF_SkillLevelArray.Value != "")
                    {
                        _listLevel = serializer.Deserialize<List<cls_employee_skill_level>>(HDF_SkillLevelArray.Value).ToList();
                    }
                    if (_listLevel.Count > 0)
                    {
                        for (int i = 0; i < _listLevel.Count; i++)
                        {
                            LevelId = _listLevel[i].id;
                            Des = _listLevel[i].des;
                            AddSkillLevelMap(SkillId, LevelId, Des);
                        }
                    }
                    if (exc > 0)
                    {
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                        UIHelpers.Redirect("/admin/tuyen-dung/quan-tri-ky-nang.html", MsgParam);
                    }
                    else
                    {
                        var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
            }
        }
        #endregion     
        #region[Update]
        private bool IsUpdate()
        {
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }
        private void BindUpdate()
        {
            using (var db = new Solution_30shineEntities())
            {
                int _Id = Convert.ToInt32(_Code);
                var list = db.TuyenDung_Skill.Where(w => w.Id == _Id && w.IsDelete == false).FirstOrDefault();
                if (!list.Equals(null))
                {
                    Name.Text = list.SkillName;
                    Description.Text = list.Description;
                }
            }
        }

        /// <summary>
        /// Cập nhật
        /// </summary>
        private void Update()
        {
            using (var db = new Solution_30shineEntities())
            {
                int _Id = Convert.ToInt32(_Code);
                var obj = db.TuyenDung_Skill.FirstOrDefault(w => w.Id == _Id);
                obj.SkillName = Name.Text;
                obj.Description = Description.Text;
                obj.Publish = Publish.Checked;
                obj.IsDelete = false;
                obj.ModifiedTime = DateTime.Now;
                var erro = false;
                if (!erro)
                {
                    db.TuyenDung_Skill.AddOrUpdate(obj);
                    var exc = db.SaveChanges();
                    int SkillId = _Id;
                    var serializer = new JavaScriptSerializer();
                    var _listLevel = new List<cls_employee_skill_level>();
                    var item = new cls_employee_skill_level();
                    if (HDF_SkillLevelArray.Value != "")
                    {
                        _listLevel = serializer.Deserialize<List<cls_employee_skill_level>>(HDF_SkillLevelArray.Value).ToList();
                    }

                    // Update TuyenDung_SkillLevel_Map
                    this.updateSkillLevelMap(obj, _listLevel);

                    if (exc > 0)
                    {
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                        UIHelpers.Redirect("/admin/tuyen-dung/quan-tri-ky-nang.html", MsgParam);
                    }
                    else
                    {
                        var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
            }
        }

        /// <summary>
        /// Insert/Update/Delete TuyenDung_SkillLevel_Map
        /// </summary>
        /// <param name="skill"></param>
        /// <param name="listLevelNew"></param>
        public void updateSkillLevelMap(TuyenDung_Skill skill, List<cls_employee_skill_level> listLevelNew)
        {
            try
            {
                var listLevelOld = this.db.TuyenDung_SkillLevel_Map.Where(w=>w.IsDelete == false && w.SkillID == skill.Id).ToList();
                var item = new TuyenDung_SkillLevel_Map();
                var index = -1;
                //1. Kiểm tra Insert/Update
                if (listLevelNew.Count > 0)
                {
                    foreach (var v in listLevelNew)
                    {
                        index = listLevelOld.FindIndex(w=>w.SkillID == skill.Id && w.LevelID == v.id);
                        if (index == -1)
                        {
                            item = new TuyenDung_SkillLevel_Map();
                            item.SkillID = skill.Id;
                            item.LevelID = v.id;
                            item.Description = v.des;
                            item.IsDelete = false;
                            item.Publish = true;
                            item.CreatedTime = DateTime.Now;                            
                        }
                        else
                        {
                            item = listLevelOld[index];
                            item.SkillID = skill.Id;
                            item.Description = v.des;
                            item.LevelID = v.id;
                            item.ModifiedTime = DateTime.Now;
                        }
                        this.db.TuyenDung_SkillLevel_Map.AddOrUpdate(item);
                        this.db.SaveChanges();
                    }
                }

                //2. Kiểm tra delete
                if (listLevelOld.Count > 0)
                {
                    foreach (var v in listLevelOld)
                    {
                        index = listLevelNew.FindIndex(w=>w.id == v.LevelID);
                        if (index == -1)
                        {
                            item = v;
                            item.IsDelete = true;
                            item.ModifiedTime = DateTime.Now;

                            this.db.TuyenDung_SkillLevel_Map.AddOrUpdate(item);
                            this.db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        #endregion
        #region[ButtonAddorUpdate]
        protected void ExcAddOrUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
            {
                Update();
            }
            else
            {
                Add();
            }
        }
        #endregion
        #region[Update,InsertSkillLevelMap]
        private void AddSkillLevelMap(int SkillId, int LevelId, string txtDecription)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var obj = new TuyenDung_SkillLevel_Map();
                    obj.SkillID = SkillId;
                    obj.LevelID = LevelId;
                    obj.Description = txtDecription;
                    obj.CreatedTime = DateTime.Now;
                    obj.IsDelete = false;
                    db.TuyenDung_SkillLevel_Map.Add(obj);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        private void UpdateSkillLevelMap(int SkillId, int LevelId, string txtDecription)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var _obj = db.TuyenDung_SkillLevel_Map.Where(w => w.SkillID == SkillId && w.LevelID == LevelId).ToList();
                    if (_obj.Count > 0)
                    {
                        foreach (var v in _obj)
                        {
                            v.Description = txtDecription;
                            v.ModifiedTime = DateTime.Now;
                            v.IsDelete = false;
                            db.TuyenDung_SkillLevel_Map.AddOrUpdate(v);
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        var obj = new TuyenDung_SkillLevel_Map();
                        obj.SkillID = SkillId;
                        obj.LevelID = LevelId;
                        obj.Description = txtDecription;
                        obj.CreatedTime = DateTime.Now;
                        obj.IsDelete = false;
                        db.TuyenDung_SkillLevel_Map.Add(obj);
                        db.SaveChanges();
                    }
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
        
        #region[Cls_class]
        public class cls_employee_skill_level
        {
            public int id { get; set; }
            public string des { get; set; }
        }
        public class cls_skillLevel : TuyenDung_SkillLevel
        {
            public bool isChecked { get; set; }
            public string Descr { get; set; }
        }
        #endregion
        

      
    }
}