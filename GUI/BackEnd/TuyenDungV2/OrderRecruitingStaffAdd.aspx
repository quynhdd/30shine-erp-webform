﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMasterVersion1.Master" AutoEventWireup="true" CodeBehind="OrderRecruitingStaffAdd.aspx.cs" Inherits="_30shine.GUI.BackEnd.TuyenDungV2.OrderRecruitingStaffAdd" %>

<asp:Content ID="ContentMain" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel ID="ContentWrap" runat="server" ClientIDMode="Static">
        <style>
            th, td {
                text-align: center;
            }

            body, html {
                height: 91%;
            }

            .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
                color: #fff;
                background-color: black;
            }

            .shadow-sm {
                margin-bottom: 15px;
                min-height: 700px;
            }

            .form-box-left {
                height: 31px;
                width: 147px;
            }

            .ul-box-left .li-box-left {
                margin-bottom: 8px;
                font-family: Roboto Condensed Regular !important;
                font-size: 14px !important;
            }

            .btn-group {
                margin-top: 21px !important;
            }

            .fa-times {
                font-size: 17px;
                color: red;
            }

                .fa-times:hover {
                    cursor: pointer;
                }

            .td-resize {
                width: 50px;
                text-align: center;
            }

            .btn-outline-secondary {
                color: #000000;
                border-color: #000000;
            }

                .btn-outline-secondary:hover {
                    background: #000000;
                }

            .table td, .table th {
                vertical-align: middle;
            }

            .quantity-left {
                text-align: center;
            }

            .max-box {
                max-height: 500px;
                overflow-y: scroll;
            }

            .thead-dark {
                font-family: Roboto Condensed Bold;
            }

            .select-list-item {
                width: 100%;
                font-size: 14px;
                height: calc(2.25rem + 2px);
                line-height: 1.5;
                color: #495057;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #ced4da;
                border-radius: .25rem;
                transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
            }

                .select-list-item option {
                    font-size: 14px;
                }
        </style>
        <section>
            <div class="container-fluid sub-menu">
                <div class="row">
                    <div class="col-md-12 pt-3">
                        <ul>
                            <li><a href="#">THÊM MỚI YÊU CẦU TUYỂN DỤNG&nbsp;&#187;</a></li>
                            <li><a href="/admin/tuyen-dung/thong-ke-yeu-cau.html"><i class="fa fa-list-ul"></i>&nbsp THỐNG KÊ YÊU CẦU TUYỂN DỤNG</a></li>
                            <li><a href="/admin/tuyen-dung/tien-do-hoan-thanh-yeu-cau.html"><i class="fa fa-list-ul"></i>&nbsp TIẾN ĐỘ HOÀN THÀNH YÊU CẦU TUYỂN DỤNG</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section style="margin-top: 10px;">
            <form runat="server">
                <div class="container-fluid sub-menu">
                    <div class="card-title">
                        <table class="table table-bordered" style="margin: 15px;width: 98.5%;">
                            <tr>
                                <th><h3>Tổng order còn lại</h3></th>
                                <td><h3 class="total-number-order" style="color:red"><%=numberOrder %></h3></td>
                                <th><h3>Tổng order stylist còn lại</h3></th>
                                <td><h3 class="total-order-stylist" style="color:red"><%=numberStylist %></h3></td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-lg-5 float-left">
                        <div class="card mb-6 shadow-sm">
                            <div class="card-body">
                                <div class="d-flex justify-content-between align-items-center">
                                    <ul class="ul-box-left">
                                        <li class="li-box-left">
                                            <span>
                                                <span>Khu vực <span style="color: red">*</span></span>
                                                <asp:DropDownList runat="server" ID="ddlRegion" CssClass="form-control select form-box-left" ClientIDMode="Static" onchange="GetDropDownData($(this));"></asp:DropDownList>
                                            </span>
                                        </li>
                                        <li class="li-box-left">
                                            <span>
                                                <span>Salon <span style="color: red">*</span></span>
                                                <asp:DropDownList runat="server" ID="ddlSalon" CssClass="form-control select form-box-left" ClientIDMode="Static"></asp:DropDownList>
                                            </span>
                                        </li>
                                        <li class="li-box-left">
                                            <span>
                                                <span>Bộ phận <span style="color: red">*</span></span>
                                                <asp:DropDownList runat="server" ID="ddlDepartment" CssClass="form-control select form-box-left" ClientIDMode="Static"></asp:DropDownList>
                                            </span>
                                        </li>
                                        <li class="li-box-left">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-sm btn-outline-secondary" onclick="AddItemInList();" <%= validOrder ? "" : "disabled='disabled'" %>><i class="fas fa-plus-circle"></i>&nbsp;Thêm vào danh sách</button>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="row max-box">
                                    <table class="table table-bordered table-striped">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th scope="col" class="td-resize">STT</th>
                                                <th scope="col">Khu vực</th>
                                                <th scope="col">Salon</th>
                                                <th scope="col">Bộ phận</th>
                                                <th scope="col" style="width: 20%">Số lượng</th>
                                                <th scope="col" class="td-resize"></th>
                                            </tr>
                                        </thead>
                                        <tbody id="body-item">
                                            <%--<tr>
                                                <th scope="row" class="td-resize">1</th>
                                                <td data-region="">Mark</td>
                                                <td data-salon="">Otto</td>
                                                <td data-department="">@mdo</td>
                                                <td>
                                                    <div class="input-group input-group-sm">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">Ví dụ: 10</span>
                                                        </div>
                                                        <input type="text" class="form-control quantity-left" aria-label="Small" maxlength="2" onchange="onlyNo($(this))" />
                                                    </div>
                                                </td>
                                                <td class="td-resize"><i class="fas fa-times"></i></td>
                                            </tr>--%>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="row">
                                    <div class="btn-group" style="margin-left: 15px;">
                                        <button type="button" id="completed-list" class="btn btn-sm btn-outline-secondary" onclick="ConfirmList();"><i class="fas fa-check" style="color: green"></i>&nbsp;Xác nhận danh sách</button>
                                    </div>
                                    <div class="btn-group" style="margin-left: 15px;">
                                        <button type="button" id="cancel-list" class="btn btn-sm btn-outline-secondary" onclick="iscancel('table tbody#body-item', 'left')"><i class="far fa-times-circle" style="color: red"></i>&nbsp;Huỷ bỏ danh sách</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-1 float-left" style="top: 320px; left: 45px;">
                        <i class="fas fa-arrow-circle-right fa-2x"></i>
                    </div>
                    <div class="col-lg-6 float-right">
                        <div class="card mb-6 shadow-sm">
                            <div class="card-body">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="max-box w-100" style="max-height: 600px; overflow: scroll;">
                                        <select class="select-list-item" style="display: none;"></select>
                                        <table class="table table-bordered table-striped" style="min-width: 700px;">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th scope="col" class="td-resize">STT</th>
                                                    <th scope="col">Khu vực</th>
                                                    <th scope="col">Salon</th>
                                                    <th scope="col">Bộ phận</th>
                                                    <th scope="col">Số lượng</th>
                                                    <th scope="col">Loại yêu cầu</th>
                                                    <%--<th scope="col">Ngày cần</th>--%>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody id="body-content">
                                                <%--<tr>
                                                    <th scope="row" class="td-resize"></th>
                                                    <td data-region="">Mark</td>
                                                    <td data-salon="">Otto</td>
                                                    <td data-department="">@mdo</td>
                                                    <td>1</td>
                                                    <td></td>
                                                    <td>
                                                        <asp:TextBox CssClass="datetime-picker form-control" placeholder="Ngày cần..." ClientIDMode="Static" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td class="td-resize"><span class="isdelete" onclick="DeleteItem($(this), 'table tbody#body-content');"><i class="fas fa-times"></i></span></td>
                                                </tr>--%>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                                <div class="w-100">
                                    <div class="btn-group" style="margin-left: 15px;">
                                        <button type="button" id="completed-list-confirm" class="btn btn-sm btn-outline-secondary" onclick="CompleteListingItemSaveToDb()"><i class="fas fa-check" style="color: green"></i>&nbsp;Hoàn tất danh sách</button>
                                    </div>
                                    <div class="btn-group" style="margin-left: 15px;">
                                        <button type="button" id="cancel-list-confirm" class="btn btn-sm btn-outline-secondary" onclick="iscancel('table tbody#body-content', 'right')"><i class="far fa-times-circle" style="color: red"></i>&nbsp;Huỷ bỏ danh sách</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!-- Page loading -->
            <div class="page-loading">
                <p>Vui lòng đợi trong giây lát...</p>
            </div>
        </section>
        <script type="text/javascript">
            var index = 1;
            var SelectRequest;
            jQuery(document).ready(function () {
                $('#completed-list').hide();
                $('#completed-list-confirm').hide();
                $('#cancel-list').hide();
                $('#cancel-list-confirm').hide();
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/TuyenDungV2/OrderRecruitingStaffAdd.aspx/BindTypeRequest",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $('.select-list-item').append($("<option />").val('0').text('Chọn yêu cầu'));
                        $.each(data.d, function () {
                            $('.select-list-item').append($("<option />").val(this.Value).text(this.Label));
                        });
                        SelectRequest = $('.select-list-item');
                    },
                    failure: function () {
                        ShowMessage('', 'Có lỗi xảy ra, vui lòng liên hệ nhà phát triển!', 4);
                    }
                });
            });

            function GetDropDownData(This) {
                let $region = This.val();
                if ($region === undefined || $region === null || $region === '') {
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/TuyenDungV2/OrderRecruitingStaffAdd.aspx/BindSalonByRegion",
                    data: '{RegionId: ' + $region + ' }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $("#ddlSalon").html('').val('0');
                        $("#select2-ddlSalon-container").val('0').text('Chọn salon');
                        $("#ddlSalon").append($("<option />").val(0).text('Chọn salon'));
                        $.each(data.d, function () {
                            $("#ddlSalon").append($("<option />").val(this.Id).text(this.Name));
                        });
                    },
                    failure: function () {
                        ShowMessage('', 'Có lỗi xảy ra, vui lòng liên hệ nhà phát triển!', 4);
                    }
                });
            }

            function CheckParameter(param) {
                if (param === undefined || param === null || param === '' || param === '0') {
                    return false
                }
                return true;
            }

            ///only number
            function onlyNo(This) {
                if (!CheckParameter(This.val())) {
                    return;
                }
                This.val(This.val().replace(/\D/g, ''));
                if (!CheckParameter(This.val())) {
                    ShowMessage('', 'Chỉ được phép nhập số!', 3);
                }
            }

            function AddItemInList() {
                var tr = $("table tbody#body-item tr");
                if (tr.length >0) {
                    ShowMessage("Thông báo", "Số order không vượt quá 1 order/ 1 lần.", 4);
                    return false;
                }
                let str = '',
                    $region = $('#ddlRegion :selected'),
                    $salon = $('#ddlSalon :selected'),
                    $department = $('#ddlDepartment :selected');
                if (!CheckParameter($region.val())) {
                    ShowMessage('', 'Bạn chưa chọn khu vực!', 3);
                    return;
                }
                else if (!CheckParameter($salon.val())) {
                    ShowMessage('', 'Bạn chưa chọn salon!', 3);
                    return;
                }
                if ($department.val() === '0') {
                    $('#ddlDepartment option').each(function () {
                        if (CheckParameter(this.value)) {
                            //if (!CheckExistItemInList($salon.val(), this.value)) {
                            //    ShowMessage('', 'Dữ liệu đã tồn tại trong danh sách vui lòng cập nhật số lượng!', 3);
                            //    return false;
                            //}
                            str += `<tr>
                                        <th class="th-index" scope="row" class="td-resize"></th>
                                        <td class="td-region" data-region="${$region.val()}">${$region.text()}</td>
                                        <td class="td-salon" data-salon="${$salon.val()}">${$salon.text()}</td>
                                        <td class="td-department" data-department="${this.value}">${this.text}</td>
                                        <td>
                                            <div class="input-group input-group-sm">
                                               
                                                <input type="text" class="form-control quantity-left" aria-label="Small" maxlength="2" value="1" disabled="disabled" onchange="onlyNo($(this))" />
                                            </div>
                                        </td>
                                        <td class="td-resize"><span class="isdelete" onclick="DeleteItem($(this),'table tbody#body-item');"><i class="fas fa-times"></i></span></td>
                                    </tr>`;
                        }
                    });
                    if (str !== '') {
                        $('#body-item').append(str);
                        $('#completed-list').show();
                        $('#cancel-list').show();
                        UpdateItemOrder('table tbody#body-item');
                        ShowMessage('', 'Thêm dữ liệu vào danh sách thành công!', 2);
                    }
                    return;
                }
                //if (!CheckExistItemInList($salon.val(), $department.val())) {
                //    ShowMessage('', 'Dữ liệu đã tồn tại trong danh sách vui lòng cập nhật số lượng!', 3);
                //    return;
                //}
                str = `<tr>
                            <th class="th-index" scope="row" class="td-resize"></th>
                            <td class="td-region" data-region="${$region.val()}">${$region.text()}</td>
                            <td class="td-salon" data-salon="${$salon.val()}">${$salon.text()}</td>
                            <td class="td-department" data-department="${$department.val()}">${$department.text()}</td>
                            <td>
                                <div class="input-group input-group-sm">
                                   
                                    <input type="text" class="form-control quantity-left" aria-label="Small" maxlength="2" value="1" disabled="disabled" onchange="onlyNo($(this))" />
                                </div>
                            </td>
                            <td class="td-resize"><span class="isdelete" onclick="DeleteItem($(this),'table tbody#body-item');"><i class="fas fa-times"></i></span></td>
                        </tr>`;
                if (str !== '') {
                    $('#body-item').append(str);
                    $('#completed-list').show();
                    $('#cancel-list').show();
                    UpdateItemOrder('table tbody#body-item');
                    ShowMessage('', 'Thêm dữ liệu vào danh sách thành công!', 2);
                }
            }

            function CheckExistItemInList(salonId, departmentId) {
                var check = true;
                $("table tbody#body-item tr").each(function () {
                    let $salonId = $(this).find("td.td-salon").attr("data-salon"),
                        $departmentId = $(this).find("td.td-department").attr("data-department");
                    if ($salonId !== undefined && $departmentId !== undefined && $salonId === salonId && $departmentId === departmentId) {
                        check = false;
                        return false;
                    }
                    else {
                        check = true;
                    }
                });
                return check;
            }

            function DeleteItem(This, dom) {
                This.parent().parent().remove();
                if ($("table tbody#body-item tr").length === 0) {
                    $('#completed-list').hide();
                    $('#cancel-list').hide();
                }
                if ($("table tbody#body-content tr").length === 0) {
                    $('#completed-list-confirm').hide();
                    $('#cancel-list-confirm').hide();
                }
                ShowMessage('', 'Xoá thành công!', 2);
                UpdateItemOrder(dom);
            }

            // Update order item
            function UpdateItemOrder(dom) {
                var i = 1;
                $(`${dom}`).find("tr").each(function () {
                    $(this).find("th.th-index").text(i);
                    i++;
                });
            }

            function ConfirmList() {
                let success = true,
                    str = '';
                $("table tbody#body-item tr").each(function () {
                    let $region = $(this).find("td.td-region"),
                        $salon = $(this).find("td.td-salon"),
                        $department = $(this).find("td.td-department"),
                        $quantity = $(this).find(".quantity-left").val();
                    if (!CheckParameter($quantity)) {
                        ShowMessage('', 'Bạn chưa nhập số lượng cần tuyển!', 3);
                        $(this).find(".quantity-left").focus();
                        success = false;
                        return success;
                    }
                    for (var i = 0; i < parseInt($quantity); i++) {
                        str += `<tr>
                                <th scope="row" class="td-resize th-index"></th>
                                <td class="td-complete-region" data-region='${$region.attr(`data-region`)}'>${$region.text()}</td>
                                <td class="td-complete-salon" data-salon='${$salon.attr(`data-salon`)}'>${$salon.text()}</td>
                                <td class="td-complete-department" data-department='${$department.attr(`data-department`)}'>${$department.text()}</td>
                                <td class="td-complete-quantity" data-quantity='1'>1</td>
                                <td class="td-complete-select">
                                </td>
                                <td class="td-resize"><span class="isdelete" onclick="DeleteItem($(this), 'table tbody#body-content');"><i class="fas fa-times"></i></span></td>
                            </tr>`;
                    }
                });
                if (success && str !== '') {
                    var dom = $('table tbody#body-content');
                    dom.append(str);
                    $('#completed-list').hide();
                    $('#cancel-list').hide();
                    $('#completed-list-confirm').show();
                    $('#cancel-list-confirm').show();
                    $('table tbody#body-content tr td.td-complete-select .select-list-item').remove();
                    $('table tbody#body-content tr td.td-complete-select').append(SelectRequest);
                    $('table tbody#body-content tr td.td-complete-select').find('.select-list-item').show();
                    $("table tbody#body-item").html('');
                    $('.select-list-item').val('0');
                    ShowMessage('', 'Xác nhận danh sách thành công!', 2);
                    UpdateItemOrder('table tbody#body-content');
                    Datetimepicker();
                }
            }

            function iscancel(dom, msg) {
                if (msg === 'left') {
                    $('#completed-list').hide();
                    $('#cancel-list').hide();
                }
                else {
                    $('#completed-list-confirm').hide();
                    $('#cancel-list-confirm').hide();
                }
                $(`${dom}`).html('');
            }

            function Datetimepicker() {
                $('.datetime-picker').datetimepicker({
                    dayOfWeekStart: 1,
                    startDate: new Date(),
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    scrollMonth: false,
                    scrollInput: false,
                    onChangeDateTime: function (dp, $input) { }
                });
            }

            function CompleteListingItemSaveToDb() {
                var tr = $("table tbody#body-content tr");
                if (!CheckNumberOrder(tr)) {
                    return false;
                }

                let success = true,
                    Ids = [],
                    prd = {};
                $("table tbody#body-content tr").each(function () {
                    prd = {};
                    let $region = $(this).find("td.td-complete-region").attr('data-region'),
                        $salon = $(this).find("td.td-complete-salon").attr('data-salon'),
                        $department = $(this).find("td.td-complete-department").attr('data-department'),
                        $quantity = $(this).find("td.td-complete-quantity").attr('data-quantity'),
                        $select = $(this).find(".select-list-item").val(),
                        $date = $(this).find(".datetime-picker").val();
                    if (!CheckParameter($select)) {
                        ShowMessage('', 'Bạn chưa chọn loại yêu cầu!', 3);
                        $(this).find(".select-list-item").focus();
                        success = false;
                        return false;
                    }
                    // check value
                    region = $region.toString().trim() !== "" ? parseInt($region) : 0;
                    salon = $salon.toString().trim() !== "" ? parseInt($salon) : 0;
                    department = $department.trim() !== "" ? parseInt($department) : 0;
                    quantity = $quantity.trim() !== "" ? parseInt($quantity) : 0;
                    select = $select.trim() !== "" ? parseInt($select) : 0;
                    //date = $date.trim();

                    prd.RegionId = region;
                    prd.SalonId = salon;
                    prd.DepartmentId = department;
                    prd.Quantity = quantity;
                    prd.Type = select;
                    //prd.Needed = date;
                    Ids.push(prd);
                });
                
                if (success && Ids.length > 0) {
                    var totalOrder = parseInt($(".total-number-order").text());
                    var totalOrderStylist = parseInt($(".total-order-stylist").text());
                    startLoading();
                    var data = JSON.stringify({ list: Ids });
                    console.log(department);
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/TuyenDungV2/OrderRecruitingStaffAdd.aspx/CompleteListing",
                        data: data,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            if (data.d) {
                                $("table tbody#body-content").html('');
                                $('#completed-list-confirm').hide();
                                $('#cancel-list-confirm').hide();
                                $(".total-number-order").empty().append(totalOrder - 1);
                                if (department === 1) {
                                    $(".total-order-stylist").empty().append(totalOrderStylist - 1);
                                }
                                
                                ShowMessage('', 'Hoàn tất thành công!', 2);
                            }
                            else {
                                ShowMessage('Thất bại', ' Số lượng order đã vượt quá <%=totalOrder%> order hoặc số lượng order stylist đã vượt quá <%=totalOrderStylist%>. Vui lòng kiểm tra lại', 4,5000);
                            }
                        },
                        complete: function (data) {
                            finishLoading();
                        },
                        failure: function () {
                            ShowMessage('', 'Có lỗi xảy ra, vui lòng liên hệ nhà phát triển!', 4,5000);
                        }
                    });
                }
            }
            function CheckNumberOrder(dom) {
                
                var check = false;
                if (dom.length > 1) {
                    ShowMessage("Thông báo", "Mỗi lần chỉ được tạo 1 order. Vui lòng kiểm tra lại.", 3);
                    check = false;
                }
                else {
                    check = true;
                }
                return check;
            }
        </script>
    </asp:Panel>
</asp:Content>

