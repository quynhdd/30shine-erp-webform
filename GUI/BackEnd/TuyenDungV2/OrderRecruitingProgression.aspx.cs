﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.TuyenDungV2
{
    public partial class OrderRecruitingProgression : System.Web.UI.Page
    {
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        public int GetIds()
        {
            return int.TryParse(Session["User_Id"].ToString(), out var integer) ? integer : 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                BindStaffType();
                BindRegion();
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
            }
        }

        /// <summary>
        /// Bind Region to ddl
        /// </summary>
        protected void BindRegion()
        {
            using (var db = new Solution_30shineEntities())
            {
                //chỉ lấy bộ phận stylist, skinner
                var types = db.TinhThanhs.OrderBy(o => o.ID).ToList();
                var item = new TinhThanh();
                item.ID = 0;
                item.TenTinhThanh = "Chọn khu vực";
                types.Insert(0, item);
                ddlRegion.DataTextField = "TenTinhThanh";
                ddlRegion.DataValueField = "Id";
                ddlRegion.DataSource = types;
                ddlRegion.DataBind();
            }
        }

        /// <summary>
        /// Bind Department to ddl
        /// </summary>
        protected void BindStaffType()
        {
            using (var db = new Solution_30shineEntities())
            {
                List<int> type = new List<int>() { 19, 18, 15, 14, 13, 11 };
                var types = db.Staff_Type.Where(w => w.IsDelete == 0 && w.Publish == true && !type.Contains(w.Id)).OrderBy(o => o.Id).ToList();
                var item = new Staff_Type();
                item.Id = 0;
                item.Name = "Chọn bộ phận";
                types.Insert(0, item);
                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "Id";
                ddlDepartment.DataSource = types;
                ddlDepartment.DataBind();
            }
        }

        /// <summary>
        /// Bind Department to ddl
        /// </summary>
        [WebMethod]
        public static List<Tbl_Salon> BindSalonByRegion(int RegionId)
        {
            using (var db = new Solution_30shineEntities())
            {
                //chỉ lấy bộ phận stylist, skinner
                if (RegionId <= 0)
                {
                    return new List<Tbl_Salon>();
                }
                return db.Tbl_Salon.Where(w => w.IsDelete == 0 && w.Publish == true && w.CityId == RegionId).OrderBy(o => o.Id).ToList();
            }
        }

        [WebMethod]
        public static ResponseData GetDataProgressCompleted(string timeFrom, string timeTo, List<int> salon, int department, int type)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    DateTime TimeFrom = new DateTime();
                    DateTime TimeTo = new DateTime();
                    var response = new ResponseData();
                    var staffType = new Staff_Type();
                    var list = new List<OrderRecruitingStaff>();
                    if (String.IsNullOrEmpty(timeFrom) || salon.Count <= 0 || department < 0)
                    {
                        response.status = false;
                        response.message = "Data does not exist!";
                        return response;
                    }
                    TimeFrom = Convert.ToDateTime(timeFrom, new CultureInfo("vi-VN"));
                    if (String.IsNullOrEmpty(timeTo))
                    {
                        TimeTo = TimeFrom.AddDays(1);
                    }
                    else
                    {
                        TimeTo = Convert.ToDateTime(timeTo, new CultureInfo("vi-VN"));
                        TimeTo = TimeTo.AddDays(1);
                    }

                    //ngày tạo
                    if (type == 1)
                    {
                        list = db.OrderRecruitingStaffs.Where(w => w.CreatedDate >= TimeFrom && w.CreatedDate < TimeTo &&
                                                              w.IsDelete == false && salon.Contains(w.SalonId.Value) &&
                                                              ((w.DepartmentId == department) || (department == 0))
                                                             ).ToList();
                    }
                    //ngày cần
                    else if (type == 2)
                    {
                        list = db.OrderRecruitingStaffs.Where(w => w.Needed >= TimeFrom && w.Needed <= TimeTo &&
                                                              w.IsDelete == false && salon.Contains(w.SalonId ?? 0) &&
                                                              ((w.DepartmentId == department) || (department == 0))
                                                             ).ToList();
                    }
                    //ngày quy định có
                    else if (type == 3)
                    {
                        list = db.OrderRecruitingStaffs.Where(w => w.Completed >= TimeFrom && w.Completed <= TimeTo &&
                                                              w.IsDelete == false && salon.Contains(w.SalonId ?? 0) &&
                                                              ((w.DepartmentId == department) || (department == 0))
                                                             ).ToList();
                    }
                    if (list.Count > 0)
                    {
                        var staffTypes = db.Staff_Type.Where(w => ((w.Id == department) || (department == 0)) && w.IsDelete == 0 && w.Publish == true).ToList();
                        staffType.Id = 0;
                        staffType.Name = "Tổng";
                        staffTypes.Insert(0, staffType);

                        response.status = true;
                        response.message = "Lấy dữ liệu thành công!";
                        //all
                        var dataAll = staffTypes.Where(w => w.Id == 0).Select(c => new ProgressCompleteData
                        {
                            department = c.Name,
                            quantity = list.Sum(w => w.Quantity),
                            quantityStandards = list.Where(w => w.ReasonRevoke == 0 && w.StaffDate <= w.Completed && w.Status == 5).Sum(w => w.Quantity) +
                                                list.Where(w => w.ReasonRevoke > 0).Sum(w => w.Quantity),
                            percentStandards = Convert.ToDouble(list.Where(w => w.ReasonRevoke == 0 && w.Status == 5).Sum(w => w.Quantity)) > 0 ?
                                               Convert.ToDouble(list.Where(w => w.ReasonRevoke == 0 && w.StaffDate <= w.Completed).Sum(w => w.Quantity)) /
                                               Convert.ToDouble(list.Where(w => w.ReasonRevoke == 0 && w.Status == 5).Sum(w => w.Quantity)) * 100 : 0,
                            averageTime = Convert.ToDouble(list.Where(w => w.ReasonRevoke == 0 && w.Status == 5).Sum(w => w.Quantity)) > 0 ?
                                          Convert.ToDouble(list.Where(w => w.ReasonRevoke == 0 && w.StaffId != null && w.StaffId > 0 && w.StaffDate != null).Sum(w => (w.StaffDate.Value.Date - w.CreatedDate.Value.Date).TotalDays)) /
                                          Convert.ToDouble(list.Where(w => w.ReasonRevoke == 0 && w.Status == 5).Sum(w => w.Quantity)) : 0
                        }).FirstOrDefault();

                        //by department
                        response.data = staffTypes.Where(w => w.Id > 0).Select(c => new ProgressCompleteData
                        {
                            department = c.Name,
                            quantity = list.Where(w => w.DepartmentId == c.Id).Sum(w => w.Quantity),
                            quantityStandards = list.Where(w => w.DepartmentId == c.Id && w.ReasonRevoke == 0 && w.StaffDate <= w.Completed && w.Status == 5).Sum(w => w.Quantity) +
                                                list.Where(w => w.DepartmentId == c.Id && w.ReasonRevoke > 0).Sum(w => w.Quantity),
                            percentStandards = Convert.ToDouble(list.Where(w => w.DepartmentId == c.Id && w.ReasonRevoke == 0 && w.Status == 5).Sum(w => w.Quantity)) > 0 ?
                                               Convert.ToDouble(list.Where(w => w.DepartmentId == c.Id && w.ReasonRevoke == 0 && w.StaffDate <= w.Completed).Sum(w => w.Quantity)) /
                                               Convert.ToDouble(list.Where(w => w.DepartmentId == c.Id && w.ReasonRevoke == 0 && w.Status == 5).Sum(w => w.Quantity)) * 100
                                               : 0,
                            averageTime = Convert.ToDouble(list.Where(w => w.DepartmentId == c.Id && w.ReasonRevoke == 0 && w.Status == 5).Sum(w => w.Quantity)) > 0 ?
                                          Convert.ToDouble(list.Where(w => w.DepartmentId == c.Id && w.ReasonRevoke == 0 && w.StaffId != null && w.StaffId > 0 && w.StaffDate != null).Sum(w => (w.StaffDate.Value.Date - w.CreatedDate.Value.Date).TotalDays)) /
                                          Convert.ToDouble(list.Where(w => w.DepartmentId == c.Id && w.ReasonRevoke == 0 && w.Status == 5).Sum(w => w.Quantity)) : 0
                        }).ToList();
                        response.data.Insert(0, dataAll);
                    }
                    else
                    {
                        response.status = false;
                        response.message = "Không có dữ liệu!";
                    }
                    return response;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class ResponseData
        {
            public bool status { get; set; }
            public string message { get; set; }
            public List<ProgressCompleteData> data { get; set; }
        }

        public class ProgressCompleteData
        {
            public string department { get; set; }
            public int? quantity { get; set; }
            public int? quantityStandards { get; set; }
            public double? percentStandards { get; set; }
            public double? averageTime { get; set; }
        }
    }
}