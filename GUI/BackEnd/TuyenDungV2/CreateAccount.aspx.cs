﻿using _30shine.Helpers;
using _30shine.MODEL.BO;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.TuyenDungV2
{
    public partial class CreateAccount : System.Web.UI.Page
    {
        private string PageID = "TD_TTKNV";
        protected TuyenDung_UngVien OBJ;
        public UIHelpers _UIHelpers = new UIHelpers();
        protected int _Code ;
        private bool Perm_Access = false;
        private bool Perm_AllPermission = false;

        public bool HasImages = false;
        public List<string> ListImagesUrl = new List<string>();
        public string[] ListImagesName;
        private Solution_30shineEntities db = new Solution_30shineEntities();
        public StaffModel staffModel = new StaffModel();
        public EmploymentApplicantModel eApplicantModel = new EmploymentApplicantModel();
        IPermissionModel permissionModel = new PermissionModel();
        private IAuthenticationModel authenticationModel = new AuthenticationModel();
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_AllPermission = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                //Bind_SkillLevel();
                Bind_TypeStaff();
                //Bind_Gender();
                BindData();
                FillTinhThanh();
                //FillQuanHuyen();
                Bind_Nguon();
                Bind_Permission();
                bindOBJ();
            }
        }

        protected void CancelCV(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                int Id = int.Parse(Request.QueryString["Code"]);
                OBJ = db.TuyenDung_UngVien.FirstOrDefault(w => w.Id == Id);
           
                    OBJ.UngVienStatusId = 5;
                    db.TuyenDung_UngVien.AddOrUpdate(OBJ);

                    var exc = db.SaveChanges();
                    if (exc > 0)
                    {
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_add_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_add_message", "Cập nhật thành công!"));
                        UIHelpers.Redirect("/admin/tuyen-dung/danh-sach-da-test-ky-nang.html");
                    }
                    else
                    {
                        var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
               
            }
        }
        private void Bind_Nguon()
        {
            _Code = Convert.ToInt32(Request.QueryString["Code"]);
            using (var db = new Solution_30shineEntities())
            {
                var lst = (from tn in db.TuyenDung_Nguon join tu in db.TuyenDung_UngVien on tn.Id equals tu.NguonTuyenDungId where tu.Id == _Code select tn).FirstOrDefault();
                if (lst != null)
                {
                    ddlNguonLaoDong.Items.Add(new ListItem(lst.Name, lst.Id.ToString()));
                    
                }
                else
                {
                    var nguon = db.TuyenDung_Nguon.ToList();
                    ListItem item = new ListItem("Chọn nguồn lao động", "0");
                    ddlNguonLaoDong.Items.Insert(0, item);
                    foreach (var v in nguon)
                    {
                        ddlNguonLaoDong.Items.Add(new ListItem(v.Name, v.Id.ToString()));
                    }

                }
                ddlNguonLaoDong.DataBind();
            }
        }

        private void Bind_Permission()
        {
            PermissionList.DataTextField = "Permission";
            PermissionList.DataValueField = "Value";
            var key = 0;

            var item = new ListItem("Nhân viên", "0");
            PermissionList.Items.Insert(key++, item);
            if (Perm_Access)
            {
                item = new ListItem("Lễ tân", "1");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Check-in", "7");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Check-out", "8");
                PermissionList.Items.Insert(key++, item);
                // Hide Lễ tân
                PermissionList.Items.FindByValue("1").Enabled = false;
            }
            if (Perm_AllPermission)
            {
                item = new ListItem("Trưởng cửa hàng", "2");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Kế toán", "3");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Mode1", "4");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Admin", "5");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Khảo sát", "6");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Online store", "9");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Quản lý kho", "10");
                PermissionList.Items.Insert(key++, item);
            }
            PermissionList.SelectedIndex = 0;
        }

        protected bool bindOBJ()
        {
            OBJ = db.TuyenDung_UngVien.Where(w => w.Id == _Code).FirstOrDefault();
            var ExistOBJ = true;
            HDF_MainImg.Value = OBJ.MainImg;
            return ExistOBJ;

        }

        private void BindData()
        {
            _Code = Convert.ToInt32(Request.QueryString["Code"]);
            using (var db = new Solution_30shineEntities())
            {
                OBJ = db.TuyenDung_UngVien.Where(w => w.Id == _Code).FirstOrDefault();
                if (OBJ != null)
                {
                    FullName.Text = OBJ.FullName;
                    Phone.Text = OBJ.Phone;
                    StaffID.Text = OBJ.CMT;
                    ProviderDateCMT.Text = String.Format("{0:dd/MM/yyyy}",OBJ.ProvidedDate);
                    ProviderLocaleCMT.Text = OBJ.ProvidedLocale;
                    Address.Text = OBJ.Address;
                    //Show email đối với ứng viên từ salon Hội quán
                    if (OBJ.StaffId != null)
                    {
                        var staffObj = staffModel.GetStaffById(OBJ.StaffId.Value);
                        if (staffObj != null)
                        {
                            Email.Text = staffObj.Email;
                        }
                    }                    


                    if (OBJ.NgaySinh != null)
                    {
                        Day.Text = OBJ.NgaySinh.Value.Day.ToString();
                        Month.Text = OBJ.NgaySinh.Value.Month.ToString();
                        Year.Text = OBJ.NgaySinh.Value.Year.ToString();
                    }
                    else
                    {
                        Day.Text = "";
                        Month.Text = "";
                        Year.Text = "";
                    }
                    if (OBJ.GioiTinhId == 1)
                    {

                        Gender.Items.Add(new ListItem("Nam", "1"));
                        Gender.DataBind();

                    }
                    else if (OBJ.GioiTinhId == 2)
                    {
                        Gender.Items.Add(new ListItem("Nữ", "2"));
                        Gender.DataBind();
                    }
                    else if (OBJ.GioiTinhId == 3)
                    {
                        Gender.Items.Add(new ListItem("Khác", "3"));
                        Gender.DataBind();
                    }
                    else
                    {
                        Bind_Gender();
                    }

                    var sl = Convert.ToInt32(OBJ.salonId);
                    var _salonID = db.Tbl_Salon.Where(s => s.Id == sl).FirstOrDefault();
                    if (_salonID != null)
                    {
                        ddlSalon.Items.Add(new ListItem(_salonID.Name, _salonID.Id.ToString()));
                    }
                    ddlSalon.DataBind();
                }                
            }
        }

        // Fill tỉnh thành
        protected void FillTinhThanh()
        {
            _Code = Convert.ToInt32(Request.QueryString["Code"]);
            using (var db = new Solution_30shineEntities())
            {
                var lst = (from t in db.TinhThanhs join tu in db.TuyenDung_UngVien on t.ID equals tu.TinhThanhId where tu.Id == _Code select t).FirstOrDefault();

                if (lst != null)
                {
                    ddlCity.Items.Add(new ListItem(lst.TenTinhThanh, lst.ID.ToString()));
                    //ddlCity.DataBind();
                }
                else
                {
                    var t = db.TinhThanhs.OrderBy(tt=>tt.ThuTu).ToList();
                    ListItem item = new ListItem("Chọn tỉnh thành", "0");
                    ddlCity.Items.Insert(0, item);
                    foreach (var v in t)
                    {
                        ddlCity.Items.Add(new ListItem(v.TenTinhThanh, v.ID.ToString()));
                    }
                    
                    
                }
                ddlCity.DataBind();
            }
        }

        private void Bind_Gender()
        {
            Gender.DataTextField = "TypeStaff";
            Gender.DataValueField = "Id";
            ListItem item = new ListItem("Chọn giới tính", "0");
            Gender.Items.Insert(0, item);
            item = new ListItem("Nam", "1");
            Gender.Items.Insert(1, item);
            item = new ListItem("Nữ", "2");
            Gender.Items.Insert(2, item);
            item = new ListItem("Khác", "3");
            Gender.Items.Insert(3, item);
        }
        public void Bind_TypeStaff()
        {
            _Code = Convert.ToInt32(Request.QueryString["Code"]);
            using (var db = new Solution_30shineEntities())
            {
                var LST = (from st in db.Staff_Type join tu in db.TuyenDung_UngVien on st.Id equals tu.DepartmentId where tu.Id == _Code select st).FirstOrDefault();
                if (LST != null)
                {
                    TypeStaff.Items.Add(new ListItem(LST.Name, LST.Id.ToString()));
                }
                TypeStaff.DataBind();
            }
        }        

        /// <summary>
        /// Check Email
        /// </summary>
        /// Nguyễn Mạnh Dũng
        /// 11/08/2017
        /// <returns></returns>
        public bool CheckIsExists_Emails(Staff objEntity)
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
            var result = db.Staffs.Where(q => q.Email.Equals(objEntity.Email)).Count();
            if (result > 0)
                return true;
            else
                return false;
        }

        public bool CheckIsExists_Phone(Staff objEntity)
        {
            Solution_30shineEntities db = new Solution_30shineEntities();
            var result = db.Staffs.Where(q => q.Phone.Equals(objEntity.Phone)).Count();
            if (result > 0)
                return true;
            else
                return false;
        }
        
        /// <summary>
        /// Thêm Mới
        /// </summary>
        /// Nguyễn Mạnh Dũng
        /// 11/08/2017
        /// <returns></returns>
        protected void AddStaff(object sender, EventArgs e)
        {
            _Code = Convert.ToInt32(Request.QueryString["Code"]);
            using (var db = new Solution_30shineEntities())
            {
                var obj = new Staff();

                // Kiểm tra trường hợp chuyển nhân sự từ S4M (salon Hội Quán) sang hệ thống salon chính
                var applicantObj = eApplicantModel.GetApplicantById(_Code);
                if (applicantObj != null && applicantObj.StaffId != null)
                {
                    obj = staffModel.GetStaffById(applicantObj.StaffId.Value);
                    if (obj == null)
                    {
                        obj = new Staff();
                        obj.Active = 1;
                        obj.Password = "a05b49b7dc71773bc7ff4ef877824ffe";
                        obj.isAccountLogin = 0;
                        obj.SalaryByPerson = false;
                        obj.RequireEnroll = true;
                        obj.CreatedDate = DateTime.Now;
                        obj.IsDelete = 0;
                    }
                    else
                    {
                        // Set mặc định Level A cho nhân sự từ S4M (salon Hội Quán) sang hệ thống salon chính
                        obj.SkillLevel = 4;
                        obj.ModifiedDate = DateTime.Now;
                    }
                }
                else
                {
                    obj = new Staff();
                    obj.Active = 1;
                    obj.Password = "a05b49b7dc71773bc7ff4ef877824ffe";
                    obj.isAccountLogin = 0;
                    obj.SalaryByPerson = false;
                    obj.RequireEnroll = true;
                    obj.CreatedDate = DateTime.Now;
                    obj.IsDelete = 0;
                }
                int integer;
                //bool _boolean;
                DateTime datetime;

                obj.Fullname = FullName.Text;
                obj.StaffID = StaffID.Text;
                obj.DateJoin = DateTime.TryParse(DateJoin.Text, out datetime) ? datetime : Convert.ToDateTime(null);
                obj.Phone = Phone.Text;
                obj.Email = UserAccount.Text;
                obj.SourceId = Convert.ToInt32(ddlNguonLaoDong.SelectedValue);
                if (HDF_MainImg.Value != null || HDF_MainImg.Value != "")
                {
                    obj.Avatar = HDF_MainImg.Value;
                }
                else
                {
                    obj.Avatar = applicantObj.MainImg;
                }
                obj.CMTimg1 = applicantObj.CMTimg1;
                obj.CMTimg2 = applicantObj.CMTimg2;
                obj.UngvienID = applicantObj.Id;
                obj.Note = applicantObj.Step2Note;
                if (TypeStaff.SelectedValue != "1")
                {
                    var update = db.TuyenDung_UngVien.FirstOrDefault(u => u.Id == _Code);
                    update.UngVienStatusId = 4;
                    db.TuyenDung_UngVien.AddOrUpdate(update);
                    db.SaveChanges();
                }
                if (Day.Text.Length > 0)
                {
                    obj.SN_day = int.TryParse(Day.Text, out integer) ? integer : 0;
                }
                if (Month.Text.Length > 0)
                {
                    obj.SN_month = int.TryParse(Month.Text, out integer) ? integer : 0;
                }
                if (Year.Text.Length > 0)
                {
                    obj.SN_year = int.TryParse(Year.Text, out integer) ? integer : 0;
                }

                // Set bậc kỹ năng [SkillLevel]
                if (!(obj.SkillLevel > 0))
                {
                    //Nếu là nguồn S4M thì level A
                    if (ddlNguonLaoDong.SelectedValue == "2" && TypeStaff.SelectedValue == "1")
                    {
                        obj.SkillLevel = 4;
                    }
                    else if (ddlNguonLaoDong.SelectedValue != "2" && TypeStaff.SelectedValue == "1")
                    {
                        obj.SkillLevel = 3;

                    }
                    else
                    {
                        if (TypeStaff.SelectedValue == "2" || TypeStaff.SelectedValue == "9")
                        {
                            obj.SkillLevel = 3;
                        }
                        else if (TypeStaff.SelectedValue == "4" || TypeStaff.SelectedValue == "5" || TypeStaff.SelectedValue == "6")
                        {
                            obj.SkillLevel = 9;
                        }
                    }
                }
                obj.DateJoin = Library.Format.getDateTimeFromString(DateJoin.Text);
                obj.NgayTinhThamNien = Library.Format.getDateTimeFromString(DateJoin.Text);
                obj.CityId = Convert.ToInt32(ddlCity.SelectedValue);
                obj.Gender = Convert.ToByte(Gender.SelectedValue);                
                obj.Type = Convert.ToInt32(TypeStaff.SelectedValue);
                obj.Address = Address.Text;
                obj.IDProviderLocale = ProviderLocaleCMT.Text;
                obj.IDProvidedDate = Library.Format.getDateTimeFromString(ProviderDateCMT.Text);
                int Error = 0;
                int ErrorPerm = 0;
                obj.SalonId = Convert.ToInt32(ddlSalon.SelectedValue);

                string[] PermLST = new string[] { "staff", "reception", "salonmanager", "accountant", "mode1", "admin", "servey", "checkin", "checkout", "onlinestore", "inventory_manager", "inventory_manager_2" };
                int index = PermissionList.SelectedValue != "" ? Convert.ToInt32(PermissionList.SelectedValue) : 0;
                if (index > PermLST.Length || (!Perm_AllPermission && index > 1))
                {
                    Error++;
                    ErrorPerm++;
                }
                else
                {
                    obj.Permission = PermLST[index];
                    var permission = permissionModel.GetPermissionByName(obj.Permission);
                    if (permission != null)
                    {
                        obj.PermissionID = permission.pID.ToString();
                    }
                }

                if (ErrorPerm > 0)
                {
                    var msg = "Giá trị phân quyền không đúng.";
                    var status = "msg-system warning";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + msg + "','" + status + "')", true);
                }

                // Validate
                // Check trùng số điện thoại
                if (obj.Phone != "")
                {
                    if (staffModel.IssetPhone(obj.Phone, obj))
                    {
                        var msg = "Số điện thoại đã tồn tại. Bạn vui lòng nhập số điện thoại khác.";
                        var status = "msg-system warning";
                        TriggerJsMsgSystemV1(Page, msg, status, 3000);
                        Error++;
                    }
                }

                // Check trùng tên đăng nhập
                if (obj.Email != "")
                {
                    if (staffModel.IssetEmail(obj.Email, obj))
                    {
                        var msg = "Tên đăng nhập đã tồn tại. Bạn vui lòng nhập tên đăng nhập khác.";
                        var status = "msg-system warning";
                        TriggerJsMsgSystemV1(Page, msg, status, 3000);
                        Error++;
                    }
                }

                // Check trùng email
                if (!String.IsNullOrEmpty(Email.Text))
                {
                    var user = authenticationModel.GetByEmail(Email.Text ?? "");
                    if (user != null)
                    {
                        var msg = "Email đã tồn tại. Bạn vui lòng nhập email khác.";
                        var status = "msg-system warning";
                        TriggerJsMsgSystemV1(Page, msg, status, 3000);
                        Error++;
                    }
                }

                if (Error == 0)
                {
                    if (obj.Id > 0)
                    {
                        obj = staffModel.Update(obj);
                    }
                    else
                    {
                        obj = staffModel.Add(obj);

                        //create user
                        var user = authenticationModel.AddUser(obj.Id, UserAccount.Text, obj.isAccountLogin ?? 0, Email.Text, Phone.Text);

                        authenticationModel.SignUp(UserAccount.Text, Libraries.AppConstants.PASSWORD_DEFAULT);

                        authenticationModel.AdminConfirmSignUp(UserAccount.Text);

                        authenticationModel.AddMapUserStaff(user, obj);

                        var order = db.OrderRecruitingStaffs.FirstOrDefault(w => w.CandidateId == _Code);
                        var staff = db.Staffs.FirstOrDefault(w => w.UngvienID == _Code);
                        if (order != null)
                        {
                            order.Status = 5;
                            order.ModifiedDate = DateTime.Now;
                            order.StaffDate = DateTime.Now;
                            order.StaffId = staff.Id;
                            db.OrderRecruitingStaffs.AddOrUpdate(order);
                            db.SaveChanges();
                        }
                    }
                    // Log skill level10
                    if (TypeStaff.SelectedValue == "4")
                    {
                        SalaryLib.Instance.initSalaryByStaffId(obj.Id, obj.CreatedDate.Value.Date, Convert.ToInt32(obj.SalonId));
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                        UIHelpers.Redirect("/admin/nhan-vien.html", MsgParam);
                    }
                    else
                    {
                        StaffLib.AddLogSkillLevel(obj.Id, Convert.ToInt32(obj.SkillLevel ?? 0), db);
                        // Init flow salary
                        SalaryLib.Instance.initSalaryByStaffId(obj.Id, obj.CreatedDate.Value.Date, Convert.ToInt32(obj.SalonId));

                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                        UIHelpers.Redirect("/admin/nhan-vien.html", MsgParam);
                    }
                    
                }
            }
        }
        public void TriggerJsMsgSystemV1(Page _OBJ, string msg, string status, int duration)
        {
            ScriptManager.RegisterStartupScript(_OBJ, _OBJ.GetType(), "Message System",
                                    "showMsgSystem('" + msg + "','" + status + "'," + duration + ");", true);
        }
    }
}