﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Script.Serialization;
using System.Globalization;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.TuyenDungV2
{
    public partial class ChangeCV : System.Web.UI.Page
    {
        private string PageID = "TD_XNTV";

        protected TuyenDung_UngVien OBJ;
        protected string _Code;
        protected bool _IsUpdate = false;
        public UIHelpers _UIHelpers = new UIHelpers();
        protected cls_media Img = new cls_media();
        protected List<cls_media> listImg = new List<cls_media>();
        private CultureInfo coltureInfo = new CultureInfo("vi-VN");

        private bool Perm_Access = false;
        private int integer;
        protected string clsgender1 = "";
        protected string clsgender2 = "";
        protected string clsStatus1 = "";
        protected string clsStatus2 = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            this.SetPermission();

            if (!IsPostBack)
            {
                BindDepartment();
                BindResource();
                BindPresenter();
                BindTester();
                BindHQ();
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_Access);
                BindProvince();
                BindTestSkills();
                BindGender();
                if (IsUpdate())
                {
                    BindModels();
                }
                else
                {
                    clsgender1 = "active";
                    clsStatus1 = "active";
                }

            }
        }

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        protected void ExcAddOrUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
            {
                Update();
            }
            else
            {
                Add();
            }
        }

        /// <summary>
        /// Chuyển thông tin của staff từ nhan viên hội quán sang salon 30shine
        /// Thêm mới thông tin tuyển dụng ứng viên
        /// </summary>
        private void Add()
        {

            try
            {
                //cập nhật các đối tượng chuyển hồ sơ rồi chuyển tới trang danh sách đã test kỹ năng
                updateModels(true);
                UIHelpers.Redirect("/admin/tuyen-dung/danh-sach-da-test-ky-nang.html");
            }
            // nếu phát sinh lỗi thì thông báo lỗi
            catch (Exception ex)
            {
                var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                var status = "warning";
                UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
            }

        }

        /// <summary>
        /// Cập nhật thông tin
        /// </summary>
        private void Update()
        {
            try
            {
                //cập nhật các đối tượng chuyển hồ sơ rồi chuyển tới trang danh sách đã test kỹ năng
                updateModels(false);
                UIHelpers.Redirect("/admin/tuyen-dung/danh-sach-da-test-ky-nang.html");
            }
            // nếu phát sinh lỗi thì thông báo lỗi
            catch (Exception ex)
            {
                var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                var status = "warning";
                UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
            }
        }


        /// <summary>
        /// Hàm lưu lại các đối tượng nhân viên, thông tin đánh giá ứng viên
        /// </summary>
        /// <param name="staffId"></param>
        protected bool updateModels(bool isCreat)
        {
            //TODO: bảng TuyenDung_DanhGia và bảng TuyenDung_UngVien có những trường đánh giá bị trùng nhau => có cần thiết không?
            using (var db = new Solution_30shineEntities())
            {
                try
                {
                    string idLevel = "";
                    var now = DateTime.Now;
                    var obj = new TuyenDung_UngVien();
                    var obj2 = new TuyenDung_DanhGia();
                    var staffId = Convert.ToInt32(ddlStaffHQ.SelectedValue);
                    var staff = db.Staffs.FirstOrDefault(o => o.Id == staffId);
                    if (staff == null)
                    {
                        return false;
                    }

                    if (isCreat)
                    {
                        obj = new TuyenDung_UngVien();
                        obj2 = new TuyenDung_DanhGia();
                        staff.CreatedDate = now;
                        obj2.CreatedDate = now;
                    }
                    else
                    {
                        obj = db.TuyenDung_UngVien.FirstOrDefault(o => o.StaffId == staffId);
                        if (obj == null)
                            obj = new TuyenDung_UngVien();
                        obj2 = db.TuyenDung_DanhGia.FirstOrDefault(o => o.UserTestID == staffId);
                        if (obj2 == null)
                            obj2 = new TuyenDung_DanhGia();
                        staff.ModifiedDate = now;
                        obj2.ModifiedTime = now;
                    }

                    //cập nhật thông tin nhân viên
                    //staff.Email = Account.Text;
                    //staff.Fullname = FullName.Text;
                    //staff.Phone = Phone.Text;
                    //staff.SalonId = Convert.ToInt32(ddlSalon.SelectedValue);
                    //staff.IsHoiQuan = false;
                    //staff.StaffID = CMT.Text;
                    //staff.BirthDay = Convert.ToDateTime(DOB.Text, new CultureInfo("vi-VN"));
                    //staff.Gender = Convert.ToByte(Convert.ToInt32(ddlGender.SelectedValue));

                    //cập nhật thông tin tuyển dụng ứng viên
                    obj.StaffId = staffId;
                    obj.UngVienStatusId = 7;
                    obj.ImgSkill1 = HDF_SkillImg_1.Value;
                    obj.ImgSkill2 = HDF_SkillImg_2.Value;
                    obj.FullName = FullName.Text;
                    obj.Phone = Phone.Text;
                    obj.VideoLink = txtVideoLink.Text;
                    obj.CMT = CMT.Text;
                    obj.TinhThanhId = Convert.ToInt32(ddlProvince.SelectedValue);
                    obj.LinkFaceBook = linkFaceBook.Text;
                    obj.DepartmentId = int.TryParse(ddlDepartment.SelectedValue, out integer) ? integer : 0;
                    try
                    {
                        obj.NgaySinh = Library.Format.getDateTimeFromString(DOB.Text);
                    }
                    catch { }
                    obj.salonId = int.Parse(ddlSalon.SelectedValue);
                    obj.GioiTinhId = int.Parse(HDF_Gender.Value);
                    obj.NguoiGioiThieuId = int.Parse(ddlPresenter.SelectedValue);
                    obj.NguonTuyenDungId = int.Parse(ddlResource.SelectedValue);
                    obj.Step2Note = Note.Text;
                    obj.Step1Time = DateTime.Now;
                    obj.IsDelete = false;
                    //obj.KyNangHoaChat = !string.IsNullOrEmpty(txtPoint2.Text) ? txtPoint2.Text + "/" + txtTotalPoint2.Text : null;
                    obj.Test_VH = !string.IsNullOrEmpty(txtPoint.Text) ? txtPoint.Text + "/" + txtTotalPoint.Text : null;
                    obj.Parse_VH = Convert.ToBoolean(Convert.ToInt32(ddlTestResult.SelectedValue));
                    obj.TestAgain = cblTestAgain.Checked ? true : false;
                    obj.TestCut = !string.IsNullOrEmpty(txtPoint2.Text) ? txtPoint2.Text + "/" + txtTotalPoint2.Text : null;
                    obj.PassCut = Convert.ToBoolean(Convert.ToInt32(ddlTestResult2.SelectedValue));
                    obj.TestCutAgain = cblTestAgain2.Checked ? true : false;
                    obj.TestChemistry = !string.IsNullOrEmpty(txtPoint3.Text) ? txtPoint3.Text + "/" + txtTotalPoint3.Text : null;
                    obj.PassChemistry = Convert.ToBoolean(Convert.ToInt32(ddlTestResult3.SelectedValue));
                    obj.TestChemistryAgain = cblTestAgain3.Checked ? true : false;

                    //TODO: confirm lại điều kiện và trạng thái duyệt
                    if (ddlDepartment.SelectedValue == "1")
                    {
                        //status này không có trong bảng TuyenDung_Status
                        obj.UngVienStatusId = 7;
                    }
                    else
                    {
                        //status trạng thái chưa duyệt
                        obj.UngVienStatusId = 1;
                    }
                    //obj.Point_Figure = Convert.ToInt32(ddlFigure.SelectedItem.Value);

                    //Lưu các ảnh
                    if (HDF_IMG_CMT1.Value != "")
                    {
                        obj.CMTimg1 = HDF_IMG_CMT1.Value;
                        staff.CMTimg1 = HDF_IMG_CMT1.Value;
                    }
                    if (HDF_IMG_CMT2.Value != "")
                    {
                        obj.CMTimg2 = HDF_IMG_CMT2.Value;
                        staff.CMTimg2 = HDF_IMG_CMT2.Value;
                    }
                    if (HDF_IMG_MAIN.Value != "")
                    {
                        obj.MainImg = HDF_IMG_MAIN.Value;
                    }
                    if (HDF_IMG_MAIN_2.Value != "")
                    {
                        obj.MainImg2 = HDF_IMG_MAIN_2.Value;
                    }

                    db.TuyenDung_UngVien.AddOrUpdate(obj);
                    db.SaveChanges();

                    //cập nhật thông tin tuyển dụng đánh giá
                    //lưu đánh giá kỹ năng vào bảng TuyenDung_Danhgia và TuyenDung_UngVien
                    foreach (RepeaterItem i in rptTestSkills.Items)
                    {
                        DropDownList ddl = (DropDownList)i.FindControl("drlLevelName");
                        idLevel += "," + ddl.SelectedValue + "";
                    }
                    obj2.UngVienID = obj.Id;
                    obj2.UserTestID = Convert.ToInt32(ddlTester.SelectedValue);
                    obj2.IsDelete = false;
                    obj2.IsStatus = false;
                    obj2.KyNangHoaChat = !string.IsNullOrEmpty(txtPoint2.Text) ? txtPoint2.Text + "/" + txtTotalPoint2.Text : null;
                    obj2.Note = Note.Text;
                    obj2.ImgSkill1 = HDF_SkillImg_1.Value;
                    obj2.ImgSkill2 = HDF_SkillImg_2.Value;
                    obj2.MapSkillLevelID = idLevel.Remove(0, 1);

                    //db.Staffs.AddOrUpdate(staff);
                    db.TuyenDung_DanhGia.AddOrUpdate(obj2);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    throw e;
                }
                return true;
            }
        }

        private bool BindModels()
        {
            using (var db = new Solution_30shineEntities())
            {
                var serialize = new JavaScriptSerializer();
                var ExistOBJ = true;
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    var obj = db.TuyenDung_DanhGia.FirstOrDefault(w => w.Id == Id);
                    var obj2 = db.TuyenDung_UngVien.FirstOrDefault(w => w.Id == Id);
                    var staffId = obj.UngVienID;
                    var staff = db.Staffs.FirstOrDefault(o => o.Id == staffId);
                    if (staff == null)
                    {
                        return false;
                    }
                    if (!obj2.Equals(null))
                    {

                        if (Convert.ToInt32(obj2.UngVienStatusId) == 1)
                        {
                            clsStatus1 = "active";
                        }
                        if (Convert.ToInt32(obj2.UngVienStatusId) == 2)
                        {
                            clsStatus2 = "active";
                        }
                        FullName.Text = obj2.FullName;
                        Phone.Text = obj2.Phone;
                        CMT.Text = obj2.CMT;
                        linkFaceBook.Text = obj2.LinkFaceBook;
                        //VideoLink.Text = OBJ.VideoLink;
                        HDF_IMG_CMT1.Value = obj2.CMTimg1;
                        HDF_IMG_CMT2.Value = obj2.CMTimg2;
                        HDF_IMG_MAIN.Value = obj2.MainImg;
                        //HDF_IMG_MAIN_2.Value = OBJ.MainImg2;
                        Note.Text = obj2.Step2Note;
                        DOB.Text = obj2.NgaySinh.ToString();
                        //ddlGoiTinh.SelectedValue = OBJ.GioiTinhId.ToString();
                        ddlSalon.SelectedValue = obj2.salonId.ToString();
                        ddlProvince.SelectedValue = obj2.TinhThanhId.ToString();
                        //KyNangSuDungHoaChat.Text = OBJ.KyNangHoaChat;
                        //ddlGender = Convert.ToInt32(staff.Gender);
                        ddlPresenter.SelectedValue = obj2.NguoiGioiThieuId.ToString();
                        ddlResource.SelectedValue = obj2.NguonTuyenDungId.ToString();
                        try
                        {
                            var itemSelected = ddlDepartment.Items.FindByValue(obj2.DepartmentId.ToString());
                            if (itemSelected != null)
                            {
                                ddlDepartment.SelectedItem.Selected = false;
                                itemSelected.Selected = true;
                            }
                        }
                        catch { }
                    }
                    else
                    {
                        ExistOBJ = false;
                        var msg = "Lỗi! Bản ghi không tồn tại";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
                else
                {
                    ExistOBJ = false;
                    var msg = "Lỗi! Bản ghi không tồn tại";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }

                return ExistOBJ;
            }
        }

        /// <summary>
        /// Bind lại danh sách nhân viên khi thay đổi bộ phận
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void bindStylistByHQ(object sender, EventArgs e)
        {
            //ScriptManager.RegisterStartupScript(this, GetType(), "Script", "excPaging(1);", true);
            bindStylistHQ();
        }

        /// <summary>
        /// Bind Stylist
        /// </summary>
        private void bindStylistHQ()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var salon = Convert.ToInt32(ddlHQ.SelectedValue);
                    var staff = new List<Staff>();
                    var whereSalon = "";
                    var sql = "";
                    if (salon > 0)
                    {
                        whereSalon = " and SalonId = " + salon;
                    }
                    sql = @"select * from Staff 
                        where IsDelete != 1 and Active = 1 
                        and (isAccountLogin != 1 or isAccountLogin is null) and [Type] = 1" + whereSalon;
                    staff = db.Staffs.SqlQuery(sql).ToList();

                    var first = new Staff();
                    first.Id = 0;
                    first.Fullname = "Chọn Stylist";
                    staff.Insert(0, first);

                    ddlStaffHQ.DataTextField = "Fullname";
                    ddlStaffHQ.DataValueField = "Id";
                    ddlStaffHQ.DataSource = staff;
                    ddlStaffHQ.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// bind những thông tin của nhân viên chọn từ HQ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void bindInfoStaffHQ(object sender, EventArgs e)
        {
            int staffId = Convert.ToInt32(ddlStaffHQ.SelectedValue);
            setInforStaff(staffId);
        }

        protected void setInforStaff(int id)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var staff = db.Staffs.Where(s => s.Id == id).FirstOrDefault();
                    Account.Text = staff.Email;
                    Id.Text = staff.Id.ToString();
                    Phone.Text = staff.Phone.ToString();
                    FullName.Text = staff.Fullname;
                    DOB.Text = string.Format("{0:dd_MM_yyyy_hh_mm_ss}", staff.BirthDay);
                    CMT.Text = staff.StaffID;
                    if (staff.NguoiGioiThieuID != null)
                        ddlPresenter.SelectedIndex = (int)staff.NguoiGioiThieuID;
                    ddlGender.SelectedIndex = (int)staff.Gender;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void CancelCV(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var obj = new TuyenDung_UngVien();
                obj.FullName = FullName.Text;
                obj.Phone = Phone.Text;
                obj.CMT = CMT.Text;
                obj.Point_Figure = Convert.ToInt32(ddlFigure.SelectedItem.Value);
                obj.UngVienStatusId = 2;
                if (txtPoint.Text != "" && txtTotalPoint.Text != "")
                {
                    obj.Test_VH = txtPoint.Text + "/" + txtTotalPoint.Text;
                }

                obj.Parse_VH = Convert.ToBoolean(ddlTestResult.SelectedValue);
                if (cblTestAgain.Checked)
                {
                    obj.TestAgain = true;
                }
                else
                {
                    obj.TestAgain = false;
                }
                if (HDF_IMG_CMT1.Value != "")
                {
                    obj.CMTimg1 = HDF_IMG_CMT1.Value;
                }
                if (HDF_IMG_CMT2.Value != "")
                {
                    obj.CMTimg2 = HDF_IMG_CMT2.Value;
                }
                if (HDF_IMG_MAIN.Value != "")
                {
                    obj.MainImg = HDF_IMG_MAIN.Value;
                }
                if (HDF_IMG_MAIN_2.Value != "")
                {
                    //obj.MainImg2 = HDF_IMG_MAIN_2.Value;
                }
                obj.LinkFaceBook = linkFaceBook.Text;
                obj.DepartmentId = int.TryParse(ddlDepartment.SelectedValue, out integer) ? integer : 0;
                try
                {
                    obj.NgaySinh = string.IsNullOrEmpty(DOB.Text) == false ? Convert.ToDateTime(DOB.Text, coltureInfo) : Convert.ToDateTime(null, coltureInfo);
                }
                catch (Exception ex)
                {
                    //
                }

                obj.salonId = int.Parse(ddlSalon.SelectedValue);
                obj.GioiTinhId = int.Parse(HDF_Gender.Value);
                obj.NguoiGioiThieuId = int.Parse(ddlPresenter.SelectedValue);
                obj.NguonTuyenDungId = int.Parse(ddlResource.SelectedValue);
                obj.Step2Note = Note.Text;
                obj.Step1Time = DateTime.Now;
                obj.IsDelete = false;

                // Validate
                var Error = false;

                if (!Error)
                {
                    db.TuyenDung_UngVien.Add(obj);
                    var exc = db.SaveChanges();

                    if (exc > 0)
                    {

                        UIHelpers.Redirect("/admin/tuyen-dung/lap-ho-so/danh-sach-ho-so.html" + obj.Id);
                    }
                    else
                    {
                        var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
            }
        }

        private void BindHQ()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Tbl_Salon.Where(s => s.IsSalonHoiQuan == true && s.IsDelete == 0).OrderBy(p => p.Name).ToList();
                var item = new Tbl_Salon();
                item.Name = "Chọn Salon";
                item.Id = 0;
                lst.Insert(0, item);
                ddlHQ.DataTextField = "Name";
                ddlHQ.DataValueField = "Id";
                ddlHQ.SelectedIndex = 0;

                ddlHQ.DataSource = lst;
                ddlHQ.DataBind();
            }
        }

        private void BindPresenter()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Staffs.OrderBy(p => p.Fullname).ToList();
                var item = new Staff();
                item.Fullname = "Chọn người giới thiệu";
                item.Id = 0;
                lst.Insert(0, item);
                ddlPresenter.DataTextField = "Fullname";
                ddlPresenter.DataValueField = "Id";
                ddlPresenter.SelectedIndex = 0;
                ddlPresenter.DataSource = lst;
                ddlPresenter.DataBind();
            }
        }

        private void BindTester()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.TuyenDung_NguoiTest.OrderBy(p => p.FullName).ToList();
                var item = new TuyenDung_NguoiTest();
                item.FullName = "Chọn người kiểm tra";
                item.Id = 0;
                lst.Insert(0, item);
                ddlTester.DataTextField = "Fullname";
                ddlTester.DataValueField = "Id";
                ddlTester.SelectedIndex = 0;
                ddlTester.DataSource = lst;
                ddlTester.DataBind();
            }
        }

        private void BindResource()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.TuyenDung_Nguon.OrderBy(p => p.Name).ToList();
                var item = new TuyenDung_Nguon();
                item.Name = "Chọn nguồn lao động";
                item.Id = 0;
                lst.Insert(0, item);
                ddlResource.DataTextField = "Name";
                ddlResource.DataValueField = "Id";
                ddlResource.SelectedIndex = 4;

                ddlResource.DataSource = lst;
                ddlResource.DataBind();
            }
        }

        private void BindGender()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = new List<Gender>();
                lst.Add(new Gender(0, "Lựa chọn giới tính"));
                lst.Add(new Gender(1, "Nam"));
                lst.Add(new Gender(2, "Nữ"));
                ddlGender.DataTextField = "Name";
                ddlGender.DataValueField = "Id";
                ddlGender.SelectedIndex = 0;
                ddlGender.DataSource = lst;
                ddlGender.DataBind();
            }
        }

        private void BindProvince()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.TinhThanhs.Where(a => a.TrangThai == 1).OrderBy(p => p.ThuTu).ToList();
                var item = new TinhThanh();
                item.TenTinhThanh = "Chọn Tỉnh Thành";
                item.ID = 0;
                lst.Insert(0, item);
                ddlProvince.DataTextField = "TenTinhThanh";
                ddlProvince.DataValueField = "ID";
                ddlProvince.SelectedIndex = 0;

                ddlProvince.DataSource = lst;
                ddlProvince.DataBind();
            }
        }

        /// <summary>
        /// Lấy về tên các kỹ năng 
        /// </summary>
        private void BindTestSkills()
        {
            using (var db = new Solution_30shineEntities())
            {
                //var skills = db.TuyenDung_Skill.Where(s => s.IsDelete == false && s.Publish == true).ToList();
                var sql = "select s.Id as IdSkill, s.SkillName from TuyenDung_SkillLevel_Map as map inner join TuyenDung_Skill as s  on map.SkillID = s.Id where s.IsDelete != 1 and s.Publish !=0 Group by s.Id, s.SkillName";
                var skills = db.Database.SqlQuery<cls_skillname>(sql).ToList();
                if (skills.Count > 0)
                {
                    rptTestSkills.DataSource = skills;
                    rptTestSkills.DataBind();
                }
            }
        }

        /// <summary>
        /// Sự kiện IteamDataBound lấy dữ liệu đổ vào Dropdownlist
        /// </summary>
        protected void rptTestSkills_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var dataItem = (cls_skillname)e.Item.DataItem;
                    if (dataItem != null)
                    {
                        DropDownList rpt = e.Item.FindControl("drlLevelName") as DropDownList;
                        var listLevel = db.Database.SqlQuery<cls_SkillLevel>("select lev.Id, CONCAT(( s.LevelName),' --- ',(lev.[Description] )) as levelName from TuyenDung_SkillLevel_Map as lev inner join TuyenDung_SkillLevel as s on s.Id = lev.LevelID where lev.SkillID in (" + dataItem.IdSkill + ")").ToList();
                        rpt.Items.Clear();
                        ListItem item = new ListItem("Đánh giá cho kỹ năng", "0");
                        rpt.Items.Insert(0, item);
                        foreach (var v in listLevel)
                        {
                            rpt.Items.Add(new ListItem(v.levelName, v.Id.ToString()));
                        }
                        rpt.SelectedIndex = 0;
                        rpt.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        private void BindDepartment()
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).ToList();
                var item = new Staff_Type();
                item.Name = "Chọn bộ phận";
                item.Id = 0;
                list.Insert(0, item);

                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "Id";
                ddlDepartment.DataSource = list;
                ddlDepartment.SelectedIndex = 1;
                ddlDepartment.DataBind();
            }
        }

        public void ReloadPage()
        {
            HttpRequest Request = HttpContext.Current.Request;
            string host = "http://" + Request.Url.Host.ToString();
            string rawUrl = Request.RawUrl.ToString();
            string Url = host + rawUrl;
            Response.Redirect(Url);
        }

        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }

        ///// <summary>
        ///// Check permission
        ///// </summary>
        //private void CheckPermission()
        //{
        //    if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
        //    {
        //        var MsgParam = new List<KeyValuePair<string, string>>();
        //        UIHelpers.Redirect("/dang-nhap.html", MsgParam);
        //    }
        //    else
        //    {
        //        string[] Allow = new string[] { "root", "admin", "employer_step1" };
        //        var Permission = Session["User_Permission"].ToString().Trim();
        //        if (Array.IndexOf(Allow, Permission) != -1)
        //        {
        //            Perm_Access = true;
        //        }
        //    }

        //    // Call execute function
        //    ExecuteByPermission();
        //}

        //private void ExecuteByPermission()
        //{
        //    if (!Perm_Access)
        //    {
        //        ContentWrap.Visible = false;
        //        NotAllowAccess.Visible = true;
        //    }
        //}

        public class cls_SkillLevel
        {
            public int Id { get; set; }
            public string levelName { get; set; }
            public string description { get; set; }
        }

        public class cls_media
        {
            public string url { get; set; }
            public string title { get; set; }
            public string description { get; set; }
        }
        public class cls_skillname
        {
            public int IdSkill { get; set; }
            public string skillName { get; set; }
        }

        public class Gender
        {
            public int Id { get; set; }
            public string Name { get; set; }

            public Gender(int id, string name)
            {
                this.Id = id;
                this.Name = name;
            }
        }

    }
}