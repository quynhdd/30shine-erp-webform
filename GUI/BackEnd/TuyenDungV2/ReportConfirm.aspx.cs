﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Services;
using System.Web.Script.Serialization;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.TuyenDungV2
{
    public partial class ReportConfirm : System.Web.UI.Page
    {
        private string PageID = "TD_DS_CD";
        protected List<UngVienDaTest> uvdt = new List<UngVienDaTest>();
        protected TuyenDung_UngVien OBJ;
        private DateTime timeFrom;
        private DateTime timeTo;
        protected string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
        protected string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
        protected bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected string Permission = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = FirstDayOfMonth;
                TxtDateTimeTo.Text = Today;
                BindData();
                //BillOBJ();
            }
        }
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        [WebMethod]
        public static string billOBJ(int uID)
        {
            Solution_30shineEntities db = new Solution_30shineEntities();

            var uv = db.Store_Show_Info_Uv(uID).FirstOrDefault();
            //var uv = db.TuyenDung_UngVien.FirstOrDefault(t => t.Id == uID);

            return new JavaScriptSerializer().Serialize(uv);
        }
        private DateTime getTimeFrom()
        {
            try
            {
                var timeFrom = Library.Format.getDateTimeFromString(TxtDateTimeFrom.Text);
                if (timeFrom != null)
                {
                    timeFrom = timeFrom.Value;
                }
                else
                {
                    throw new Exception("Lỗi thời gian bắt đầu.");
                }
                return timeFrom.Value.Date;
            }
            catch (Exception)
            {

                throw new Exception("Lỗi thời gian băt đầu.");
            }
        }

        #region[BinData]
        private void BindData()
        {
            using (var db = new Solution_30shineEntities())
                try
                {
                    var timeFrom = Library.Format.getDateTimeFromString(TxtDateTimeFrom.Text);
                    if (timeFrom == null)
                    {
                        throw new Exception("Lỗi ngày bắt đầu.");
                    }
                    var timeTo = Library.Format.getDateTimeFromString(TxtDateTimeTo.Text);
                    if (timeTo != null)
                    {
                        timeTo = timeTo.Value.AddDays(1);
                    }
                    else
                    {
                        timeTo = timeFrom.Value.AddDays(1);
                    }
                    string sql = @"BEGIN		
		                        select uv.Id, uv.FullName,uv.Phone,department.Name as Department,CAST(dg.IsStatus as int) as [Status]
		                        from TuyenDung_DanhGia as dg
		                        left join TuyenDung_UngVien as uv on uv.Id=dg.UngVienID
		                        left join Staff_Type as department on department.Id = uv.DepartmentId
		                        where dg.IsDelete=0 and dg.IsStatus=0  and uv.IsDelete=0
                                union all
                                select uv.Id, uv.FullName,uv.Phone,department.Name as Department,CAST(UngVienStatusId as int) as [Status]
		                        from TuyenDung_UngVien as uv 
		                        left join Staff_Type as department on department.Id = uv.DepartmentId
		                        where uv.UngVienStatusId =1 and uv.IsDelete=0
                                 END";
                    //uvdt = db.Database.SqlQuery<UngVienDaTest>(string.Format("exec [Store_TuyenDung_V2_Listing] '{0}', '{1}'", String.Format("{0:yyyy/MM/dd}", timeFrom), String.Format("{0:yyyy/MM/dd}", timeTo))).ToList();
                    uvdt = db.Database.SqlQuery<UngVienDaTest>(sql).ToList();
                    //uvdt = (from a in db.TuyenDung_DanhGia
                    //        join b in db.TuyenDung_UngVien on a.UngVienID equals b.Id into c
                    //        from d in c.DefaultIfEmpty()
                    //        join departmend in db.Staff_Type on d.DepartmentId equals departmend.Id into f
                    //        from e in f.DefaultIfEmpty()
                    //        where
                    //        a.IsStatus == false
                    //        && a.IsDelete == false
                    //        && d.IsDelete == false
                    //        select new UngVienDaTest
                    //        {
                    //            Id = d.Id,
                    //            Fullname = d.FullName,
                    //            Phone = d.Phone,
                    //            Status = a.IsStatus,
                    //            Department = e.Name
                    //        }).ToList();

                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            RemoveLoading();
            BindData();
        }
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
        #endregion
        #region[Paging]

        #endregion

        public string GetStatus(string IsStatus)
        {
            if (IsStatus == "False")
            { return " <b>Đã test xong kỹ năng!</b>"; }
            else
            {
                return "";
            }
        }
        public class cls_InfoUV
        {
            public int Id { get; set; }
            public string Fullname { get; set; }
            public string Phone { get; set; }
            public string Point { get; set; }
            public bool TestResult { get; set; }
            public bool TestAgain { get; set; }
            public string Avatar { get; set; }
            public string SalonName { get; set; }
            public string NguonUngVien { get; set; }
            public string Bophan { get; set; }

        }
        public class UngVienDaTest
        {
            //public int Id { get; set; }
            public int Id { get; set; }
            public string Fullname { get; set; }
            public string Phone { get; set; }
            public string Department { get; set; }
            public int? Status { get; set; }
        }
    }
}