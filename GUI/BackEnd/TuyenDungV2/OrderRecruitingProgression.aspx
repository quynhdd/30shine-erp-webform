﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMasterVersion1.Master" AutoEventWireup="true" CodeBehind="OrderRecruitingProgression.aspx.cs" Inherits="_30shine.GUI.BackEnd.TuyenDungV2.OrderRecruitingProgression" %>

<asp:Content ID="ContentMain" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel ID="ContentWrap" runat="server" ClientIDMode="Static">
        <style>
            th, td {
                text-align: center;
            }

            body, html {
                height: 91%;
            }

            .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
                color: #fff;
                background-color: black;
            }

            .shadow-sm {
                margin-bottom: 15px;
                min-height: 700px;
            }

            .form-box-left {
                height: 31px;
                width: 185px;
            }

            .ul-box-left .li-box-left {
                margin-bottom: 8px;
                font-family: Roboto Condensed Regular !important;
                font-size: 14px !important;
            }

            .btn-group {
                margin-top: 21px !important;
            }

            .fa-times {
                font-size: 17px;
                color: red;
            }

                .fa-times:hover {
                    cursor: pointer;
                }

            .td-resize {
                width: 50px;
                text-align: center;
            }

            .btn-outline-secondary {
                color: #000000;
                border-color: #000000;
            }

                .btn-outline-secondary:hover {
                    background: #000000;
                }

            .table td, .table th {
                vertical-align: middle;
            }

            .quantity-left {
                text-align: center;
            }

            .thead-dark {
                font-family: Roboto Condensed Bold;
            }

            .select-list-item {
                width: 100%;
                font-size: 14px;
                height: calc(2.25rem + 2px);
                line-height: 1.5;
                color: #495057;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #ced4da;
                border-radius: .25rem;
                transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
            }

                .select-list-item option {
                    font-size: 14px;
                }

            #timefrom, #timeto, #selectdate {
                height: 31px;
            }

            .table .thead-dark th {
                color: #fff;
                background-color: #333;
                border-color: white;
            }

            .form-control {
                font-size: 14px;
                padding: 0 0.75rem;
            }
        </style>
        <section>
            <div class="container-fluid sub-menu">
                <div class="row">
                    <div class="col-md-12 pt-3">
                        <ul>
                            <li>BÁO CÁO&nbsp;&#187;</li>
                            <li><a href="/admin/tuyen-dung/them-moi-yeu-cau.html"><i class="fas fa-plus-square"></i>&nbsp THÊM MỚI YÊU CẦU TUYỂN DỤNG</a></li>
                            <li><a href="/admin/tuyen-dung/thong-ke-yeu-cau.html"><i class="fa fa-list-ul"></i>&nbsp THỐNG KÊ YÊU CẦU TUYỂN DỤNG</a></li>
                            <li><a href="#"><b><i class="fa fa-list-ul"></i>&nbsp TIẾN ĐỘ HOÀN THÀNH YÊU CẦU TUYỂN DỤNG</b></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section style="margin-top: 10px;">
            <form runat="server">
                <div class="container-fluid sub-menu">
                    <div class="col-lg-12 float-left">
                        <div class="card mb-6 shadow-sm">
                            <div class="card-body">
                                <div class="d-flex justify-content-between align-items-center">
                                    <ul class="ul-box-left">
                                        <li class="li-box-left">
                                            <span>
                                                <span>Từ ngày <span style="color: red">*</span></span>
                                                <input type="text" id="timefrom" placeholder="Vd: 01/11/2018" class="datetime-picker form-control" style="width:185px"/>
                                            </span>
                                        </li>
                                        <li class="li-box-left">
                                            <span>
                                                <span>Đến ngày <span style="color: red">*</span></span>
                                                <input type="text" id="timeto" placeholder="Vd: 30/11/2018" class="datetime-picker form-control" style="width:185px"/>
                                            </span>
                                        </li>
                                        <li class="li-box-left">
                                            <span>
                                                <span>Khu vực <span style="color: red">*</span></span>
                                                <asp:DropDownList runat="server" ID="ddlRegion" CssClass="form-control select form-box-left" ClientIDMode="Static" onchange="GetDropDownData($(this));"></asp:DropDownList>
                                            </span>
                                        </li>
                                        <li class="li-box-left">
                                            <span>
                                                <span>Salon <span style="color: red">*</span></span>
                                                <asp:DropDownList runat="server" ID="ddlSalon" CssClass="form-control select form-box-left" ClientIDMode="Static"></asp:DropDownList>
                                            </span>
                                        </li>
                                        <li class="li-box-left">
                                            <span>
                                                <span>Bộ phận <span style="color: red">*</span></span>
                                                <asp:DropDownList runat="server" ID="ddlDepartment" CssClass="form-control select form-box-left" ClientIDMode="Static"></asp:DropDownList>
                                            </span>
                                        </li>
                                        <li class="li-box-left">
                                            <span>
                                                <span>Tìm kiếm ngày <span style="color: red">*</span></span>
                                                <select id="selectdate" class="form-control" style="width:185px">
                                                    <option value="3">Ngày quy định có</option>
                                                    <option value="1">Ngày tạo yêu cầu</option>
                                                    <%--<option value="2">Ngày cần</option>--%>
                                                </select>
                                            </span>
                                        </li>
                                        <li class="li-box-left">
                                            <div class="btn-group" style="width: 110px">
                                                <button type="button" class="btn btn-sm btn-outline-secondary" style="width: 100%" onclick="GetCompleteListing()">Xem dữ liệu</button>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="row max-box">
                                    <table id="tablecontent" class="table table-bordered table-striped">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th scope="col" class="td-resize">STT</th>
                                                <th scope="col" width="18%">Bộ phận</th>
                                                <th scope="col">Số lượng</th>
                                                <th scope="col">Số lượng đáp ứng tiêu chuẩn</th>
                                                <th scope="col">Tỉ lệ đáp ứng tiêu chuẩn thời gian (%)</th>
                                                <th scope="col">Thời gian trung bình đáp ứng (ngày)</th>
                                            </tr>
                                        </thead>
                                        <tbody id="bodycontent">
                                            <%--<tr>
                                                <th scope="col">STT</th>
                                                <td>Bộ phận</td>
                                                <td>Số lượng</td>
                                                <td>Số lượng đáp ứng tiêu chuẩn</td>
                                                <td>Tỉ lệ đáp ứng tiêu chuẩn thời gian</td>
                                                <td>Thời gian trung bình đáp ứng</td>
                                            </tr>--%>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!-- Page loading -->
            <div class="page-loading">
                <p>Vui lòng đợi trong giây lát...</p>
            </div>
        </section>
        <script type="text/javascript">
            var SalonIds = [];
            function CheckParameter(param) {
                if (param === undefined || param === null || param === '' || param === '0') {
                    return false
                }
                return true;
            }

            function GetDropDownData(This) {
                let $region = This.val();
                if (!CheckParameter($region)) {
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/TuyenDungV2/OrderRecruitingStaffAdd.aspx/BindSalonByRegion",
                    data: '{RegionId: ' + $region + ' }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $("#ddlSalon").html('').val('0');
                        $("#select2-ddlSalon-container").val('0').text('Chọn salon');
                        $("#ddlSalon").append($("<option />").val(0).text('Chọn salon'));
                        $.each(data.d, function () {
                            $("#ddlSalon").append($("<option />").val(this.Id).text(this.Name));
                        });
                    },
                    failure: function () {
                        ShowMessage('', 'Có lỗi xảy ra, vui lòng liên hệ nhà phát triển!', 4);
                    }
                });
            }

            function GetSalonIds() {
                let Ids = [];
                $('#ddlSalon option').each(function () {
                    if (CheckParameter(this.value)) {
                        let salonId = parseInt(this.value);
                        Ids.push(salonId);
                    }
                });
                SalonIds = Ids;
            }

            function GetCompleteListing() {
                let $success = true,
                    $dateFrom = $('#timefrom').val(),
                    $dateTo = $('#timeto').val(),
                    $department = $('#ddlDepartment :selected').val(),
                    $salon = $('#ddlSalon :selected').val(),
                    $type = $('#selectdate').val();
                GetSalonIds();
                if (!CheckParameter($dateFrom)) {
                    ShowMessage('', 'Bạn chưa chọn thời gian bắt đầu!', 3);
                    $success = false;
                }
                if ($salon === '0') {
                    if (SalonIds.length === 0) {
                        ShowMessage('', 'Khu vực này chưa có salon!', 4);
                        return;
                    }
                }
                else {
                    SalonIds = [];
                    SalonIds.push(parseInt($salon));
                }
                if ($success && SalonIds.length > 0) {
                    startLoading();
                    $department = parseInt($department);
                    $type = parseInt($type);
                    var data = JSON.stringify({ timeFrom: $dateFrom, timeTo: $dateTo, salon: SalonIds, department: $department, type: $type });
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/TuyenDungV2/OrderRecruitingProgression.aspx/GetDataProgressCompleted",
                        data: data,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            $('#bodycontent').html('');
                            let response = data.d,
                                str = ''
                            index = 0;
                            if (response.status) {
                                ShowMessage('', response.message, 2);
                                for (var i = 0; i < response.data.length; i++) {
                                    index++;
                                    str += `<tr>
                                               <th scope="col">${index}</th>
                                               <td>${response.data[i].department}</td>
                                               <td>${response.data[i].quantity}</td>
                                               <td>${response.data[i].quantityStandards}</td>
                                               <td>${response.data[i].percentStandards.toString().substring(0, 4)}%</td>
                                               <td>${response.data[i].averageTime.toString().substring(0, 4)}</td>
                                           </tr>`;
                                }
                                $('#bodycontent').append(str);
                            }
                            else {
                                ShowMessage('', response.message, 4);
                            }
                            finishLoading();
                        },
                        failure: function () {
                            ShowMessage('', 'Có lỗi xảy ra, vui lòng liên hệ nhà phát triển!', 4);
                        }
                    });
                }
            }
        </script>
    </asp:Panel>
</asp:Content>
