﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TemplateMaster/SiteMaster.Master" CodeBehind="Report_Of_Time.aspx.cs" Inherits="_30shine.GUI.BackEnd.TuyenDungV2.Report_Of_Time" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <title>Quản lý nhân sự | Danh sách</title>

    <style>
        .skill-table
        {
            display: table;
            width: 100%;
        }

        .skill-table .item-row
        {
            display: table-row;
            border-bottom: 1px solid #ddd;
        }

        .skill-table .item-cell
        {
            display: table-cell;
            border-bottom: 1px solid #ddd;
            padding-top: 5px;
            padding-bottom: 5px;
        }

        .skill-table .item-cell > span
        {
            float: left;
            width: 100%;
            text-align: left;
        }

        .skill-table .item-row .item-cell .checkbox
        {
            float: left;
            padding-top: 0 !important;
            padding-bottom: 0 !important;
            margin-top: 6px !important;
            margin-bottom: 0px !important;
            margin-right: 9px;
            text-align: left;
            background: #ddd;
            padding: 2px 10px !important;
        }

        .skill-table .item-row .item-cell .checkbox input[type='checkbox']
        {
            margin-left: 0 !important;
            margin-right: 3px !important;
        }

        .skill-table .item-cell-order
        {
            width: 30px !important;
        }

        .skill-table .item-cell-skill
        {
            width: 120px !important;
        }

        .skill-table .item-cell-levels
        {
        }

        .modal
        {
            z-index: 10000;
        }

        .be-report .row-filter
        {
            z-index: 0;
        }

        @media(min-width: 768px)
        {
            .modal-content,
            .modal-dialog
            {
                width: 750px !important;
                margin: auto;
            }
        }
    </style>
</asp:Content>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quy trình tuyển dụng <%--&nbsp;&#187;--%> </li>
                        <%--<li class="li-listing li-listing1"><a href="/admin/nhan-vien/danh-sach.html">Danh sách</a></li>--%>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <asp:Label ID="Label1" runat="server" Text="Label" Visible="false"></asp:Label>
                <div class="row row-filter">
                    <strong class="st-head"><i class="fa fa-filter"></i>Lọc kết quả</strong>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlSalon" runat="server" Style="margin-right: 12px !important; width: 150px;" ClientIDMode="Static">
                        </asp:DropDownList>
                        <asp:DropDownList ID="ddlTimeCheck" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 150px;">
                            <asp:ListItem Text="Chọn thời gian Check" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Sau 3 ngày" Value="3"></asp:ListItem>
                            <asp:ListItem Text="Sau 7 ngày" Value="7"></asp:ListItem>
                            <asp:ListItem Text="Sau 30 ngày" Value="30"></asp:ListItem>
                        </asp:DropDownList>
                    </div>

                    <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <a href="#" class="st-head btn-viewdata">Reset Filter</a>
                </div>
                <!-- End Filter -->

                <div class="table-wp">
                    <table class="table display" id="table1" style="width: 100%">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>ID</th>
                                <th>Họ và tên</th>
                                <th>Salon</th>
                                <th>Ngày bắt đầu</th>
                                <th>Nhận xét</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <% for (int i = 0; i < nv.Count; i++)
                                {
                            %>
                            <tr>
                                <td><%= i+1 %></td>
                                <td><%= nv[i].Id %></td>
                                <td><%= nv[i].Fullname %></td>
                                <td><%= nv[i].SalonName %></td>
                                <td><%= nv[i].DateJoin.ToString("dd/MM/yyyy") %></td>
                                <td><%= nv[i].Note %></td>
                                <td>
                                    <button type="button" class="btn btn-info btn-xs details" staffid="<%= nv[i].Id %>">Chi tiết</button></td>
                            </tr>
                            <%
                                } %>
                        </tbody>
                    </table>
                </div>


                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                <!-- Hidden Field-->
                <asp:HiddenField ID="HDF_Suggestion_Code" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_Suggestion_Field" ClientIDMode="Static" runat="server" />
                <!-- END Hidden Field-->
            </div>
            <%-- END Listing --%>
        </div>

        <%--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#systemModal">Open Modal</button>--%>

        <!-- Modal -->
        <div id="systemModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#home">Thông số tổng quát</a></li>
                            <li><a data-toggle="tab" href="#service">Chi tiết dịch vụ</a></li>
                            <li><a data-toggle="tab" href="#misstake">Tổng lỗi nhân viên</a></li>
                            <li><a data-toggle="tab" href="#hairstyle">Độ đa dạng kiểu tóc</a></li>
                        </ul>
                    </div>
                    <div class="modal-body">
                        <div class=" tab-content">
                            <div id="home" class="tab-pane fade in active">
                                <table class="skill-table display" id="totalResult">
                                </table>
                            </div>
                            <div id="service" class="tab-pane fade">
                                <table class="skill-table display" id="totalService">
                                </table>
                            </div>
                            <div id="misstake" class="tab-pane fade">
                                <table class="skill-table display" id="totalMisstake">
                                </table>
                            </div>
                            <div id="hairstyle" class="tab-pane fade">
                                <table class="skill-table display" id="totalHairStyle">
                                </table>
                            </div>
                        </div>
                        <div class="form-group" id="Note">
                            <label for="comment">Lý do</label>
                            <textarea class="form-control" rows="5" id="noteEnd"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <% if (Session["User_Name"].ToString() == "Root")
	                        {
		                         %>
                        <button type="button" class="btn btn-default confirm" id="Confirm_Ratting">Xác nhận</button>
                        <button type="button" class="btn btn-default" id="Ratting">Đánh giá</button>
                        <%    
	                        }
                            else if (Session["User_Name"].ToString() != "Root" && Perm_Add && Perm_Edit)
                            {
                        %>
                        <button type="button" class="btn btn-default confirm" id="Confirm_Ratting">Xác nhận</button>
                        <button type="button" class="btn btn-default" id="Ratting">Đánh giá</button>
                        <%    
                            } %>
                        <% 
                            if (Session["User_Name"].ToString() == "Root")
                            {
                             %>
     
                            <button type="button" class="btn btn-default" id="EndWork">Buộc thôi việc</button>
                            <% 
                            }
                            else if (Session["User_Name"].ToString() != "Root" && Perm_Edit && Perm_Delete)
                            {
                        %>
     
                        <button type="button" class="btn btn-default" id="EndWork">Buộc thôi việc</button>
                        <%    
                            } %>


                        <button type="button" class="btn btn-default" id="closeModal">Đóng</button>
                    </div>
                </div>
            </div>
        </div>

        <script>
            jQuery(document).ready(function () {
                $("#glbTuyendungV2").addClass("active");
                $("#glbReportResult").addClass("active");
                $("#totalResult").DataTable({
                    "bInfo": false,
                    "bLengthChange": false,
                    "bFilter": false,
                    'aoColumns': [
                         { bSearchable: false, bSortable: false }
                    ],
                });
                $("#totalMisstake").DataTable({
                    "bInfo": false,
                    "bLengthChange": false,
                    "bFilter": false,
                    'aoColumns': [
                         { bSearchable: false, bSortable: false }
                    ],
                });
                $("#totalService").DataTable({
                    "bInfo": false,
                    "bLengthChange": false,
                    "bFilter": false,
                    'aoColumns': [
                         { bSearchable: false, bSortable: false }
                    ],
                });
                $("#totalHairStyle").DataTable({
                    "bInfo": false,
                    "bLengthChange": false,
                    "bFilter": false,
                    'aoColumns': [
                         { bSearchable: false, bSortable: false }
                    ],
                });
                $("#table1").DataTable({
                    "bSort": false,

                    'aoColumns': [
                          { bSearchable: false, bSortable: false },
                          { bSearchable: false, bSortable: false },
                          { bSearchable: false, bSortable: false },
                          { bSearchable: false, bSortable: false },
                          { bSearchable: false, bSortable: false },
                          { bSearchable: false, bSortable: false },
                          { bSearchable: false, bSortable: false }
                    ],
                    "language": {
                        "lengthMenu": " Bản ghi / trang _MENU_",
                        "zeroRecords": "Không tồn tại bản ghi",
                        "info": "Hiển thị trang _PAGE_ của _PAGES_",
                        //"infoEmpty": "Không có dữ liệu",
                        "infoFiltered": "(Lọc kết quả từ _MAX_ tổng số bản ghi)",
                        "search": "Tìm kiếm",
                        "paginate": {
                            "first": "Đầu",
                            "last": "Cuối",
                            "next": "+",
                            "previous": "-"
                        },
                    },
                    "bLengthChange": false,
                    "bFilter": false,
                });

                // Add active menu
                //$("#glbTuyendungV2").addClass("active");
                //$("#glbTuyenDung").addClass("active");
                $("#subMenu .li-listing1").addClass("active");
                $(document).on("click", "#closeModal", function () {
                    $("#Note").hide();
                    $("#EndWork").show();
                    $("#Ratting").show();
                    $("#systemModal").modal("hide");
                    $("table tbody tr").css("background-color", "white");
                })
                $(document).on("click", ".details", function () {
                    $("#systemModal").modal();
                    $(this).closest("tr").css("background-color", "green");

                    var staffID = $(this).attr("staffID");
                    var time = $("#ddlTimeCheck").val();


                    // total report
                    $.ajax({
                        type: "POST",
                        datatype: "json",
                        url: "/GUI/BackEnd/TuyenDungV2/Report_Of_Time.aspx/get_Total_Result",
                        data: '{staffID : ' + staffID + ', time : ' + time + '}',
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var jsonData = $.parseJSON(data.d);
                            var itemRows = '<thead>' +
                                                   '<tr class="item-row item-row-head">' +
                                                        '<th class="item-cell item-cell-skill" style="display:none"><span>Tổng doanh thu</span></th>' +
                                                        '<th class="item-cell item-cell-skill"><span>Tổng giờ làm</span></th>' +
                                                       '<th class="item-cell item-cell-skill"><span>Năng suất TB</span></th>' +
                                                       '<th class="item-cell item-cell-skill"><span>Lỗi KCS</span></th>' +
                                                       '<th class="item-cell item-cell-skill"><span>Điểm SCSC TB</span></th>' +
                                                       //'<th class="item-cell item-cell-skill"><span>KCS</span></th>' +
                                                       '<th class="item-cell item-cell-skill"><span>Tốc độ cắt TB</span></th>' +
                                                       '<th class="item-cell item-cell-skill"><span>Tổng điểm hài lòng</span></th>' +
                                                   '</tr>' +

                                            '</thead>' +
                                            '<tbody>';
                            for (var i = 0; i < jsonData.length; i++) {

                                itemRows += '<tr class="item-row">' +
                                    '<td class="item-cell item-cell-skill" style="display:none"><span>' + jsonData[i].TotalMoney.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '</span></td>' +
                                    '<td class="item-cell item-cell-skill"><span>' + jsonData[i].TotalHours + '</span></td>' +
                                    '<td class="item-cell item-cell-skill"><span>' + (Math.round(jsonData[i].TotalMoney / jsonData[i].TotalHours)).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '</span></td>' +
                                    //'<td class="item-cell item-cell-skill" border="1"><span>' + ((jsonData[i].TotalBillOK / jsonData[i].TotalBill) * 100) + '%</span></td>' +
                                    '<td class="item-cell item-cell-skill">' + jsonData[i].Total_SCSC_Error + '</td>' +
                                    '<td class="item-cell item-cell-skill">' + jsonData[i].PointSCSC_TB + '</td>' +
                                    '<td class="item-cell item-cell-skill">' + jsonData[i].AVGTimeCut + '</td>' +
                                    '<td class="item-cell item-cell-skill">' + jsonData[i].TotalPoint + '</td>' +
                                '</tr>';
                            }
                            itemRows += '</tbody>'
                            $("#systemModal .modal-body #totalResult").html(itemRows);

                        }
                    })

                    // total money from services
                    $.ajax({
                        type: "POST",
                        datatype: "json",
                        url: "/GUI/BackEnd/TuyenDungV2/Report_Of_Time.aspx/get_Total_Service",
                        data: '{staffID : ' + staffID + ', time : ' + time + '}',
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var jsonData = $.parseJSON(data.d);

                            var itemRows = '<thead>' +
                                                   '<tr class="item-row item-row-head">' +
                                                       '<th class="item-cell item-cell-skill"><span>Tên dịch vụ</span></th>' +
                                                       '<th class="item-cell item-cell-skill"><span>Số lượng</span></th>' +
                                                       //'<th class="item-cell item-cell-skill"><span>Tổng tiền</span></th>' +
                                                   '</tr>' +

                                            '</thead>' +
                                            '<tbody>';
                            for (var i = 0; i < jsonData.length; i++) {

                                itemRows += '<tr class="item-row">' +
                                    '<td class="item-cell item-cell-skill"><span>' + jsonData[i].Service_Name + '</span></td>' +
                                    '<td class="item-cell item-cell-skill"><span>' + jsonData[i].QUANTITY + '</span></td>' +
                        //'<td class="item-cell item-cell-skill">' + jsonData[i].Total_Money.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") +'</td>' +
                                '</tr>';
                            }
                            itemRows += '</tbody>'
                            $("#systemModal .modal-body #totalService").html(itemRows);

                        }
                    })

                    //total hair style
                    $.ajax({
                        type: "POST",
                        datatype: "json",
                        url: "/GUI/BackEnd/TuyenDungV2/Report_Of_Time.aspx/getHairStyle",
                        data: '{staffID : ' + staffID + ', time : ' + time + '}',
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var _jsonData = $.parseJSON(data.d);

                            var _itemRows = '<thead>' +
                                                   '<tr class="item-row item-row-head">' +
                                                       '<th class="item-cell item-cell-skill"><span>Kiểu tóc</span></th>' +
                                                       '<th class="item-cell item-cell-skill"><span>Số lượng</span></th>' +

                                                   '</tr>' +

                                            '</thead>' +
                                            '<tbody>';
                            for (var i = 0; i < _jsonData.length; i++) {

                                _itemRows += '<tr class="item-row">' +
                                    '<td class="item-cell item-cell-skill"><span>' + _jsonData[i].Hair_Name + '</span></td>' +
                                    '<td class="item-cell item-cell-skill"><span>' + _jsonData[i].Total_HairStyle + '</span></td>' +
                                '</tr>';
                            }
                            _itemRows += '</tbody>'
                            $("#systemModal .modal-body #totalHairStyle").html(_itemRows);

                        }
                    })

                    //total misstake         
                    $.ajax({
                        type: "POST",
                        datatype: "json",
                        url: "/GUI/BackEnd/TuyenDungV2/Report_Of_Time.aspx/totalMisstake",
                        data: '{staffID : ' + staffID + ', time : ' + time + '}',
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            var __jsonData = $.parseJSON(data.d);

                            var __itemRows = '<thead>' +
                                                   '<tr class="item-row item-row-head">' +
                                                     '<th class="item-cell item-cell-skill"><span>Loại lỗi</span></th>' +
                                                       //'<th class="item-cell item-cell-skill"><span>Hình thức phạt</span></th>' +
                                                       '<th class="item-cell item-cell-skill"><span>Số lần phạt</span></th>' +
                                                       '<th class="item-cell item-cell-skill"><span>Số lần nhắc nhở</span></th>' +
                                                       //'<th class="item-cell item-cell-skill"><span>Tổng tiền phạt</span></th>' +

                                                   '</tr>' +

                                            '</thead>' +
                                            '<tbody>';
                            for (var i = 0; i < __jsonData.length; i++) {

                                __itemRows += '<tr class="item-row">' +
                                    //'<td class="item-cell item-cell-skill"><span>' + __jsonData[i].MISSTAKE_NAME + '</span></td>' +
                                    '<td class="item-cell item-cell-skill">' + __jsonData[i].Formality + '</td>' +
                                    '<td class="item-cell item-cell-skill"><span>' + __jsonData[i].MISSTAKE_COUNT + '</span></td>' +
                                    '<td class="item-cell item-cell-skill">' + __jsonData[i].SoLanNhacNho + '</td>' +
                                    //'<td class="item-cell item-cell-skill"><span>' + __jsonData[i].TOTAL_MONEY.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '</span></td>' +
                                '</tr>';
                            }
                            __itemRows += '</tbody>'
                            $("#systemModal .modal-body #totalMisstake").html(__itemRows);

                        }
                    })
                    $("#Note").hide();
                    $(".confirm").hide();
                    //end work button
                    $(document).on("click", "#Ratting", function () {
                        $("#Note").show();
                        $(".confirm").show();
                        $("#Ratting").hide();



                    })

                    $(document).on("click", "#Confirm_Ratting", function () {
                        if ($("#noteEnd").val() == "") {
                            alert("Vui lòng đánh giá nhân viên!!!");
                            $("#noteEnd").focus();
                        }
                        else {
                            $.ajax({
                                type: "POST",
                                datatype: "json",


                                url: "/GUI/BackEnd/TuyenDungV2/Report_Of_Time.aspx/Ratting",

                                data: '{staffID : ' + staffID + ',note : ' + JSON.stringify($("#noteEnd").val()) + '}',
                                contentType: "application/json; charset=utf-8",
                                success: function (data) {
                                    console.log("Success");
                                    $("#Note").hide();
                                    location.reload();
                                }
                            })
                        }

                    })

                    $(document).on("click", "#EndWork", function () {
                        var c = confirm("Bạn chắc chắn muốn cho nhân viên này nghỉ việc không?");
                        if (c == true) {
                            $.ajax({
                                type: "POST",
                                datatype: "json",
                                url: "/GUI/BackEnd/TuyenDungV2/Report_Of_Time.aspx/Endwork",
                                data: '{staffID : ' + staffID + '}',
                                contentType: "application/json; charset=utf-8",
                                success: function (data) {
                                    location.reload();

                                }
                            })
                        }
                    })
                })

            });

        </script>
    </asp:Panel>
</asp:Content>
