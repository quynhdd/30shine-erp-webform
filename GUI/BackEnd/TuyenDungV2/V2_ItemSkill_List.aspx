﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="V2_ItemSkill_List.aspx.cs" Inherits="_30shine.GUI.BackEnd.TuyenDungV2.V2_ItemSkill_List" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý kỹ năng &nbsp;&#187; </li>
                        <li class="li-listing active"><a href="/admin/tuyen-dung/quan-tri-ky-nang.html">Danh sách</a></li>
                        <li class="li-add"><a href="/admin/tuyen-dung/quan-tri-ky-nang/them-moi.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                
       
                        <div class="table-wp">
                            <table class="table" id="tableSkill">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>ID</th>
                                        <th>Kỹ năng</th>
                                        <th>Mô tả</th>
                                        <th>Publish</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                    <% for (int i = 0; i < skill.Count; i++)
                                        {
                                            %>
                                    <tr>
                                        <td><%= i+1 %></td>
                                        <td><%= skill[i].Id %></td>
                                        <td><%= skill[i].skillName %></td>
                                        <td><%= skill[i].Description %></td>
                                        <td><input type="checkbox" class="checkPublish" skillID="<%= skill[i].Id %>"
                                            <%= Convert.ToInt32(skill[i].Publish) == 1 ? "Checked=\"True\"" : "" %> />

                                            
                                        </td>
                                         <td>
                                             <a class="btn btn-info btn-xs" href="/admin/tuyen-dung/quan-tri-ky-nang/<%= skill[i].Id %>.html" ><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp Sửa</a>
                                                       <a class="btn btn-warning btn-xs" onclick="del(this.parentNode.parentNode.parentNode,'<%= skill[i].Id %>', '<%= skill[i].skillName %>')" href="javascript://" title="Xóa"><i class="fa fa-trash-o" aria-hidden="true"></i>&nbsp Xóa</a>

                                         </td>
                                    </tr>
                                    <%
                                        } %>
                                </tbody>
                            </table>
                        </div>

           
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
            </div>
            <%-- END Listing --%>
        </div>
        <script>
            $(document).ready(function () {
                $("#glbTuyendungV2").addClass("active");
                $("#glbSkillManager").addClass("active");
                $("#tableSkill").DataTable({
                    "bSort": false,
                    "pageLength": 10,
                    "language": {
                        "lengthMenu": " Bản ghi / trang _MENU_",
                        "zeroRecords": "Không tồn tại bản ghi",
                        "info": "Hiển thị trang _PAGE_ trên _PAGES_",
                        "infoEmpty": "Không có dữ liệu",
                        "infoFiltered": "(Lọc kết quả từ _MAX_ tổng số bản ghi)",
                        "search": "Tìm kiếm",
                        "paginate": {
                            "first": "Đầu",
                            "last": "Cuối",
                            "next": "+",
                            "previous": "-"
                        },
                    },
                    "bLengthChange": true,
                    "bFilter": true,
                });
            })
            function del(This, code, name) {
                var code = code || null,
                    name = name || null,
                    Row = This;
                if (!code) return false;
                // show EBPopup
                $(".confirm-yn").openEBPopup();
                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");
                $("#EBPopup .yn-yes").bind("click", function () {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/SystemService/Ajax/Del.aspx/Delele_Skill",
                        data: '{Code : "' + code + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            var mission = JSON.parse(response.d);
                            if (mission.success) {

                                delSuccess();
                                Row.remove();
                                location.reload();
                            } else {
                                delFailed();
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    });
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
            }
            $(document).on("change", ".checkPublish", function (e) {
                var skillID = $(this).attr("skillID");
                var isPublish;
                if (this.checked) {
                    isPublish = 1;
                }
                else isPublish = 0;
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=UTF-8",
                    data:'{skillID:'+skillID+',isPublish:'+isPublish+'}',
                    datatype: "JSON",
                    url: "/GUI/BackEnd/TuyenDungV2/V2_ItemSkill_List.aspx/PublishSkill",
                    success: function (data) {

                    }
                })
                e.preventDefault();
            })

        </script>
    </asp:Panel>
</asp:Content>
