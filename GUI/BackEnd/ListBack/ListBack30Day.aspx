﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="ListBack30Day.aspx.cs" Inherits="_30shine.GUI.BackEnd.ListBack.ListBack30Day" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Danh sách chưa quay lại </title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .customer-listing table.table-listing tbody tr:hover .edit-wp.display_permission { display: none !important; }
            .display_permission { display: none; }
            .edit-wp { position: absolute; right: 3px; top: 24%; background: #e7e7e7; display: none; }
                .edit-wp .elm { width: 17px; height: 20px; float: left; display: block; background: url(/Assets/images/icon.delete.small.active.png?v=2); margin-right: 30px; }
                    .edit-wp .elm:hover { background: url(/Assets/images/icon.delete.small.png?v=2); }
            .wp_time_booking { width: 100%; float: left; padding-left: 55px; margin: 5px 0px 15px; }
                .wp_time_booking .a_time_booking { float: left; height: 26px; line-height: 26px; padding: 0px 15px; background: #dfdfdf; margin-right: 10px; cursor: pointer; position: relative; font-size: 13px; -webkit-border-radius: 15px; -moz-border-radius: 15px; border-radius: 15px; }
                    .wp_time_booking .a_time_booking:hover, .wp_time_booking .a_time_booking.active { background: #fcd344; color: #000; }
                    .wp_time_booking .a_time_booking .span_time_booking { position: absolute; top: 24px; left: 0; width: 100%; float: left; text-align: center; font-size: 13px; }
        </style>
        <%--<uc1:ReportMenu runat="server" id="ReportMenu" />--%>

     <%--   <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Danh sách &nbsp;&#187; </li>
                        <li class="be-report-li"><a href="javascript:void(0);"><i class="fa fa-th-large"></i>Booking</a></li>
                    </ul>
                </div>
            </div>
        </div>--%>

        <div class="wp customer-add customer-listing be-report be-report-timekeeping">
            <%-- Listing --%>
            <div class="wp960 content-wp">

                <div class="row">
                    <div class="filter-item">
                        <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="time-wp">
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="txtDate" placeholder="Từ ngày" Style="margin-left: 10px!important"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ValidateTime" ControlToValidate="txtDate" runat="server" CssClass="fb-cover-error" Text="Vui lòng chọn thời gian!"></asp:RequiredFieldValidator>
                        </div>
                        <strong class="st-head" style="margin-left: 10px;">
                            <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                        </strong>
                        <br />
                    </div>
                    <div class="filter-item">
                        <strong class="st-head" style="margin-right: 5px;">Salon : </strong>
                        <asp:DropDownList ID="ddlSalon" CssClass="select form-control" runat="server" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                    </div>

                    <%--<asp:Button ID="btnViews" runat="server" CssClass="st-head btn-viewdata" Text="Xem dữ liệu"  OnClientClick="excPaging(1)" />--%>
                    <a id="ViewData" onclick="excPaging(1)" class="st-head btn-viewdata " href="javascript:void(0)">Xem dữ liệu
                    </a>
                    <%--OnClick="btnViews_Click"--%>
                    <%--<a class="st-head btn-viewdata">Xem dữ liệu</a>--%>
                    <a href="/admin/listing-booking.html" class="st-head btn-viewdata">Reset Filter</a>

                </div>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>Danh sách khách chưa quay lại</strong>
                </div>
                <div class="row">
                    <p class="" style="border:1px dashed #808080; padding:5px 7px; float:left; max-width:100%;"> Nội dung nhắn tin : 
                    <strong>    A Hùng ơi, khách quen 30Shine mà lâu rồi chưa thấy a qua. A rep "OK" để e đặt chỗ a hnay hoặc mai cắt tóc đẹp trai a nhé !</strong>
                    </p>
                </div>
                <div class="wp customer-add customer-listing be-report">
                    <%-- Listing --%>
                    <div class="wp960 content-wp">
                        <!-- Filter -->
                        <!-- End Filter -->
                        <!-- Row Table Filter -->
                        <div class="table-func-panel">
                            <div class="table-func-elm">
                                <span>Số hàng / Page : </span>
                                <div class="table-func-input-wp">
                                    <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                                    <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                                    <ul class="ul-opt-segment">
                                        <li data-value="10">10</li>
                                        <li data-value="20">20</li>
                                        <li data-value="30">30</li>
                                        <li data-value="40">40</li>
                                        <li data-value="50">50</li>
                                        <li data-value="1000000">Tất cả</li>
                                    </ul>
                                    <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                                </div>
                            </div>
                        </div>
                        <!-- End Row Table Filter -->
                        <asp:ScriptManager runat="server" ID="ScriptManager1"></asp:ScriptManager>
                        <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                            <ContentTemplate>
                                <div class="table-wp">
                                    <table class="table-add table-listing">
                                        <thead>
                                            <tr>
                                                <th>STT</th>
                                               
                                                <th>Tên khách hàng</th>
                                                <th>Sđt</th>
                                                <th>Đã nhắn tin</th>
                                                <th>Ghi chú</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="RptListBack" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                     
                                                        <td><%#Eval("CusName") %></td>
                                                        <td><%#Eval("CusPhone") %></td>
                                                        <td>
                                                            <input type="checkbox" <%=setreadonly%> class="myCheck_permission" data-name="<%#Eval("CusName") %>" data-phone="<%#Eval("CusPhone") %>" data-id="<%#Eval("Id") %>" onclick="checkCall($(this))" <%# Convert.ToInt32(Eval("Call")) == 1 ? "Checked=\"True\"" : "" %> />
                                                        </td>
                                                        <td>
                                                            <textarea class="" <%=setreadonly%>  id="txtNote" data-name="<%#Eval("CusName") %>" data-phone="<%#Eval("CusPhone") %>" data-id="<%#Eval("Id") %>" onchange="ChangeNote($(this))" cols="20" rows="2"><%#Eval("Note") %></textarea></td>

                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </div>

                                <!-- Paging -->
                                <div class="site-paging-wp">
                                    <% if (PAGING.TotalPage > 1)
                                        { %>
                                    <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                        <% if (PAGING._Paging.Prev != 0)
                                            { %>
                                        <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                        <% } %>
                                        <asp:Repeater ID="RptPaging" runat="server">
                                            <ItemTemplate>
                                                <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                                    <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                                    <%# Eval("PageNum") %>
                                                </a>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                            { %>
                                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                        <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                        <% } %>
                                    </asp:Panel>
                                    <% } %>
                                </div>
                                <!-- End Paging -->
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                    </div>
                    <%-- END Listing --%>
                </div>
            </div>
            <%-- END Listing --%>
            <div class="notification-box">Thành công</div>
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
                margin-top: 5px !important;
            }
            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/ecmascript">
            jQuery(document).ready(function () {
                // Add active menu
                $("#glbAdminContent").addClass("active");

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });
            });
            // Check Call
            function checkCall(This) {
                var Id = This.data("id");
                var CusName = This.data("name");
                var CusPhone = This.data("phone");
                if (Id != null && Id != "" && Id != 0) {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/ListBack/ListBack30Day.aspx/UpdatCheckCall",
                        data: '{Id : "' + Id + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            var mission = JSON.parse(response.d);
                            if (mission.success) {
                             
                                showNotification("Lưu dữ liệu thành công!");
                            } else {
                                //alert("Có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển.");
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    })
                }

            }
            //Change Note Call
            function ChangeNote(This) {
                var Id = This.data("id");
                var CusName = This.data("name");
                var CusPhone = This.data("phone");
                var Note = This.val();
                var ctnNote = Note.trim();
                //console.log(Id + CusName + CusPhone + ctnNote);
                if (Id != null && Id != "" && Id != 0 && Note.trim() != "" && Node != null) {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/ListBack/ListBack30Day.aspx/UpdatNote",
                        data: '{Id : "' + Id + '" , ctnNote :"'+ctnNote+'" }',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            var mission = JSON.parse(response.d);
                            if (mission.success) {            
                              
                                showNotification("Lưu dữ liệu thành công!");
                            } else {
                                //alert("Có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển.");
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    })
                }
                else {
                    alert("Mời bạn nhập nội dung ghi chú");
                }
            }

            /// show notification
            function showNotification(content) {
                $(".notification-box").text(content).fadeIn(function () {
                    setTimeout(function () {
                        $(".notification-box").fadeOut();
                    }, 800);
                });
            }

            //Test
           
            function disablPermission()
            {
                $(".myCheck_permission").prop("disabled", true);
            }
        </script>

    </asp:Panel>
</asp:Content>
