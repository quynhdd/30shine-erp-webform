﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Script.Serialization;
using ExportToExcel;
using Excel;
using LinqKit;
using System.Web.Services;
using System.Data.Entity.Migrations;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.ListBack
{
    public partial class ListBack30Day : System.Web.UI.Page
    {
        private string PageID = "CUS_NOTRETURN_30";
        protected Paging PAGING = new Paging();
        private bool Perm_Access = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        CultureInfo culture = new CultureInfo("vi-VN");
        public string setreadonly = "";
        protected void Page_Load(object sender, EventArgs e)
        {

            SetPermission();
            if (txtDate.Text != "")
            {
                InsertNotListBack30day();
            }
            if (!IsPostBack)
            {
                txtDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now));
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
                InsertNotListBack30day();
                Bind_Paging();
                BindBackList();
            }
        }

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                ////Perm_ViewAllDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDate", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
      
        //Inset NoteListBack30day
        protected void InsertNotListBack30day()
        {
            using (var db = new Solution_30shineEntities())
            {
                DateTime DateBack = Convert.ToDateTime(txtDate.Text, culture);
                var TopBackList = db.tbl_ListBack30Day.Where(a => a.Publish == true && a.IsDelete == false && a.Date == DateBack).Take(2).OrderBy(a => a.Id).ToList();

                var LstBack30Day = db.Store_Khach_chua_quay_lai_salon_1thangV2(DateBack).ToList();
                int _countListBackToday = TopBackList.Count;
                int _CountLstBack30Day = LstBack30Day.Count;
                if (_countListBackToday == 0 && _CountLstBack30Day > 0)
                {
                    var item = new tbl_ListBack30Day();
                    foreach (var v in LstBack30Day)
                    {
                        item = new tbl_ListBack30Day();
                        item.CusName = v.Fullname;
                        item.CusPhone = v.Phone;
                        item.CusId = v.Id;
                        item.Call = false;
                        item.IsDelete = false;
                        item.Publish = true;
                        item.CreateDate = DateTime.Now;
                        item.Date = DateBack;
                        item.SalonId = v.SalonId;
                        db.tbl_ListBack30Day.Add(item);
                        db.SaveChanges();
                    }
                }
            }
        }
        // Bind ListBack30day where Date
        protected void BindBackList()
        {
            using (var db = new Solution_30shineEntities())
            {
                DateTime DateBack = Convert.ToDateTime(txtDate.Text, culture);
                int _SalonId = Convert.ToInt32(ddlSalon.SelectedValue);
                //var aa = PAGING._Offset;
                //var lstBooking = db.BookingListV1(_StartDate, _EndDate, _SalonId, _IsMakeBill).Skip(PAGING._Offset).Take(PAGING._Segment).ToList(); ;
                var lstBackList = db.tbl_ListBack30Day.Where(a => a.IsDelete == false && a.Publish == true && a.Date == DateBack && a.SalonId == _SalonId).OrderBy(a => a.CusName).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                int Countlist = lstBackList.Count;
                if(Countlist>0)
                {
                    RptListBack.DataSource = lstBackList;
                    RptListBack.DataBind();
                    string User_permission = HttpContext.Current.Session["User_Permission"].ToString();
                    if (User_permission != "root" && User_permission != "admin" && User_permission != "salonmanager")
                    {
                    }
                    else
                    {
                        setreadonly = "readonly";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "script callback", "disablPermission();", true);
                    }
                }
            }
        }
        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();
            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                DateTime DateBack = Convert.ToDateTime(txtDate.Text, culture);
                int _SalonId = Convert.ToInt32(ddlSalon.SelectedValue);
                //int _IsMakeBill = Convert.ToInt32(ddlIsMakeBill.SelectedValue);
                var lstBackList = db.tbl_ListBack30Day.Where(a => a.Publish == true && a.IsDelete == false && a.Date == DateBack && a.SalonId == _SalonId).ToList();
                var Count = lstBackList.Count();
                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }
        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
            BindBackList();
            RemoveLoading();
        }
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
       
        //Update CheckCall
        [WebMethod(EnableSession = true)]
        public static string UpdatCheckCall(int Id)
        {
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                string User_permission = HttpContext.Current.Session["User_Permission"].ToString();
                if (User_permission != "root" && User_permission != "admin" && User_permission != "salonmanager")
                {
                    var objUpdateBackList = db.tbl_ListBack30Day.FirstOrDefault(a => a.Publish == true && a.IsDelete != true && a.Id == Id);
                    if (objUpdateBackList != null)
                    {

                        if (objUpdateBackList.Call == true)
                        {
                            objUpdateBackList.Call = false;
                        }
                        else
                        {
                            objUpdateBackList.Call = true;
                        }
                        objUpdateBackList.ModyfiDate = DateTime.Now;
                        db.tbl_ListBack30Day.AddOrUpdate(objUpdateBackList);
                        db.SaveChanges();
                        Msg.success = true;
                    }
                    else
                    {
                        Msg.success = false;
                    }
                }

            }
            return serializer.Serialize(Msg);
        }
        [WebMethod(EnableSession = true)]
        public static string UpdatNote(int Id, string ctnNote)
        {
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                string User_permission = HttpContext.Current.Session["User_Permission"].ToString();
                if (User_permission != "root" && User_permission != "admin" && User_permission != "salonmanager")
                {
                    var objUpdateBackList = db.tbl_ListBack30Day.FirstOrDefault(a => a.Publish == true && a.IsDelete != true && a.Id == Id);
                    if (objUpdateBackList != null)
                    {
                        objUpdateBackList.Note = ctnNote;
                        objUpdateBackList.ModyfiDate = DateTime.Now;
                        db.tbl_ListBack30Day.AddOrUpdate(objUpdateBackList);
                        db.SaveChanges();
                        Msg.success = true;
                    }
                    else
                    {
                        Msg.success = false;
                    }
                }
            }
            return serializer.Serialize(Msg);
        }
        public class TempListBack30Day
        {
            public int? Id { get; set; }
            public string Fullname { get; set; }
            public string Phone { get; set; }
            public int? SalonId { get; set; }
        }
    }
}