﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Net.Http;
using System.Web.Script.Serialization;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.Interface;

namespace _30shine.GUI.BackEnd.Notification
{
    public partial class CreateNotification : System.Web.UI.Page
    {
        public bool isUpdate = false;
        public int idNotification = 0;
        public bool Perm_ViewAllData = false;
        private bool Perm_Access = false;

        /// <summary>
        /// set permission
        /// </summary>
        public void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        /// <summary>
        /// ExecuteByPermission
        /// </summary>
        private void ExecuteByPermission()
        {

            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                if (IsUpdate())
                {
                    BtnEditNoti.Visible = true;
                    BindPage(idNotification);
                }
                else
                {
                    BtnAddNoti.Visible = true;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private bool IsUpdate()
        {
            var code = Request.QueryString["Code"];
            var test = Request.Url.AbsoluteUri;
            int integer = 0;
            if (code != null)
            {
                idNotification = int.TryParse(code, out integer) ? integer : 0;
                isUpdate = true;
                return true;
            }
            else
            {
                isUpdate = false;
                return false;
            }
        }

        private void BindPage(int id)
        {
            try
            {
                var result = new HttpClient().GetAsync(Libraries.AppConstants.URL_API_NOTIFICATION + "/api/notification/?id=" + id).Result;
                if (result.IsSuccessStatusCode)
                {
                    var data = result.Content.ReadAsStringAsync().Result;
                    if (data != "")
                    {
                        var objNotification = new JavaScriptSerializer().Deserialize<NotificationManagement>(data);
                        if (objNotification != null)
                        {
                            HDF_IdNotification.Value = idNotification.ToString();
                            txtName.Text = objNotification.Name;
                            txtTitle.Text = objNotification.Title;
                            HDF_MainImg.Value = objNotification.Images;
                            chkIsActive.Checked = objNotification.IsPublish;
                            txtUrl.Text = objNotification.Url;
                            txtDescription.Text = objNotification.Descriptions;
                            HDF_NameNotification.Value = objNotification.Name;
                            HDF_Url.Value = objNotification.Url;
                            HDF_CreateTime.Value = objNotification.CreatedTime.ToString();
                        }
                    }
                    else
                    {
                        UIHelpers.TriggerJsMsgSystem_v1(this, "Dữ liệu không có trong hệ thống", "msg-system warning", 15000);
                    }
                }
                else
                {
                    UIHelpers.TriggerJsMsgSystem_v1(this, "Lấy dữ liệu thất bại (" + result.Content.ReadAsStringAsync().Result + ") ", "msg-system warning", 15000);
                }
            }
            catch (Exception ex)
            {
                UIHelpers.TriggerJsMsgSystem_v1(this, "Lỗi: " + ex.Message.Replace("'", "\'"), "msg-system warning", 15000);
            }
        }

        /// <summary>
        /// Add notification
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddNotification(object sender, EventArgs e)
        {
            try
            {
                var name = txtName.Text;
                var title = txtTitle.Text;
                var description = txtDescription.Text;
                var url = txtUrl.Text;
                var image = HDF_MainImg.Value;
                var publish = chkIsActive.Checked;
                var objNotification = new NotificationManagement();
                string error = Validatefield(name, title, url);
                if (!string.IsNullOrEmpty(error))
                {
                    UIHelpers.TriggerJsMsgSystem_v1(this, error, "msg-system warning", 15000);
                    return;
                }
                objNotification.Name = name;
                objNotification.Title = title;
                objNotification.Descriptions = description;
                objNotification.Url = url;
                objNotification.Images = image;
                objNotification.IsPublish = publish;
                var result = new HttpClient().PostAsJsonAsync(Libraries.AppConstants.URL_API_NOTIFICATION + "/api/notification", objNotification).Result;
                if (result.IsSuccessStatusCode)
                {
                    UIHelpers.TriggerJsMsgSystem_v1(this, "Thêm mới thành công!", "msg-system success", 15000);
                }
                else
                {
                    UIHelpers.TriggerJsMsgSystem_v1(this, "Thêm mới thất bại (" + result.Content.ReadAsStringAsync().Result + ") ", "msg-system warning", 15000);
                }
            }
            catch (Exception ex)
            {
                UIHelpers.TriggerJsMsgSystem_v1(this, "Lỗi: " + ex.Message.Replace("'", "\'"), "msg-system warning", 15000);
            }
        }

        /// <summary>
        /// Edit notification
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditNotification(object sender, EventArgs e)
        {
            try
            {
                int integer = 0;
                var id = int.TryParse(HDF_IdNotification.Value, out integer) ? integer : 0;
                var name = txtName.Text;
                var title = txtTitle.Text;
                var description = txtDescription.Text;
                var url = txtUrl.Text;
                var image = HDF_MainImg.Value;
                var publish = chkIsActive.Checked;
                var objNotification = new NotificationManagement();
                string error = Validatefield(name, title, url);
                if (!string.IsNullOrEmpty(error))
                {
                    UIHelpers.TriggerJsMsgSystem_v1(this, error, "msg-system warning", 15000);
                    return;
                }
                objNotification.Id = id;
                objNotification.Name = name;
                objNotification.Title = title;
                objNotification.Descriptions = description;
                objNotification.Url = url;
                objNotification.Images = image;
                objNotification.IsPublish = publish;
                objNotification.CreatedTime = Convert.ToDateTime(HDF_CreateTime.Value);
                objNotification.ModifiedTime = DateTime.Now;
                var result = new HttpClient().PutAsJsonAsync(Libraries.AppConstants.URL_API_NOTIFICATION + "/api/notification", objNotification).Result;
                if (result.IsSuccessStatusCode)
                {
                    UIHelpers.TriggerJsMsgSystem_v1(this, "Cập nhật thành công!", "msg-system success", 15000);
                }
                else
                {
                    UIHelpers.TriggerJsMsgSystem_v1(this, "Cập nhật thất bại (" + result.Content.ReadAsStringAsync().Result + ") ", "msg-system warning", 15000);
                }
            }
            catch (Exception ex)
            {
                UIHelpers.TriggerJsMsgSystem_v1(this, "Lỗi: " + ex.Message.Replace("'", "\'"), "msg-system warning", 15000);
            }
        }

        private string Validatefield(string name, string title, string url)
        {
            string error = "";
            try
            {
                var nameOld = HDF_NameNotification.Value;
                var urlOld = HDF_Url.Value;
                if (string.IsNullOrEmpty(name)) error += "[Bạn chưa nhập tên thông báo] --";
                if (string.IsNullOrEmpty(title)) error += "[Bạn chưa nhập tên hiển thị] --";
                if (string.IsNullOrEmpty(url)) error += "[Bạn chưa nhập url thông báo] -- ";
                if (url != urlOld)
                {
                    if (!string.IsNullOrEmpty(url) && CheckDuplicateUrl(url)) error += "[Url thông báo đã có trong hệ thống] -- ";
                }
                if (nameOld != name)
                {
                    if (!string.IsNullOrEmpty(name) && CheckDuplicateName(name)) error += "[Tên thông báo đã có trong hệ thống] -- ";
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return error;
        }

        private bool CheckDuplicateUrl(string url)
        {
            try
            {
                bool isDuplicate = false;
                var result = new HttpClient().GetAsync(Libraries.AppConstants.URL_API_NOTIFICATION + "/api/notification/check-duplicate-url/?url=" + url.ToString()).Result;
                if (result.IsSuccessStatusCode)
                {
                    return bool.TryParse(result.Content.ReadAsStringAsync().Result, out isDuplicate) ? isDuplicate : false;
                }
                else
                {
                    throw new Exception("Check duplicate noti url error!");
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private bool CheckDuplicateName(string name)
        {
            try
            {
                bool isDuplicate = false;
                var result = new HttpClient().GetAsync(Libraries.AppConstants.URL_API_NOTIFICATION + "/api/notification/check-duplicate-name/?name=" + name.ToString()).Result;
                if (result.IsSuccessStatusCode)
                {
                    return bool.TryParse(result.Content.ReadAsStringAsync().Result, out isDuplicate) ? isDuplicate : false;
                }
                else
                {
                    throw new Exception("Check duplicate noti name error!");
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}