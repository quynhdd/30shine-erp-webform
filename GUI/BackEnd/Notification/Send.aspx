﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Send.aspx.cs" Inherits="_30shine.GUI.BackEnd.Notification.Send" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ProductAdd" ContentPlaceHolderID="CtMain" runat="server">
<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

<style>
    textarea { padding: 10px!important; }
    .sendto-wrap { width: 100%; float: left; }
    .table-add input[type="radio"] { position: relative!important; top: 2px!important; margin-right: 3px!important; }
    .table-add label { font-family: Roboto Condensed Bold!important; font-weight: normal!important; cursor: pointer; }
    .customer-add .content-wp { padding: 0 18%; }
    .noti-status { line-height: 30px!important; padding-left: 10px; font-style: italic; display:none; }
    .noti-success{ color: #50b347; }
    .noti-fail{ color: red; }
</style>

<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">      
                <li>Quản lý Notification &nbsp;&#187; </li>
                <li class="li-listing"><a href="/admin/bac-ky-nang/danh-sach.html">Gửi notification</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add">
    <%-- Add --%>                
    <div class="wp960 content-wp">
        <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
            <div class="table-wp">
                <table class="table-add admin-product-table-add">
                    <tbody>
                        <tr class="title-head">
                            <td><strong>Nội dung gửi</strong></td>
                            <td></td>
                        </tr>
                        <tr class="tr-margin" style="height: 20px;"></tr>

                        <tr>
                            <td class="col-xs-2 left"><span>Mẫu tin nhắn</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <%--<label onclick="bindTextNoti($(this))" style="margin-right: 10px;" data-title="Tin nhắn từ 30Shine" data-text-noti="Anh ơi, sắp sinh nhật anh rồi đấy, qua 30Shine cắt tóc đẹp trai đón tuổi mới nào!" data-text-data="Anh <#customername#> ơi, sắp sinh nhật anh rồi đấy, qua 30Shine cắt tóc đẹp trai đón tuổi mới nào!" data-noti-type="ic_read_message_status_bar"><input type="radio" name="notiForm" />Chúc mừng SN</label>--%>
                                    <label onclick="bindTextNoti($(this))" style="margin-right: 10px;" data-title="Khai trương Salon 702 Đường Láng" data-text-noti="Salon 30Shine 702 Đường Láng mới hoạt động từ ngày 11/08/2016, mời anh qua trải nghiệm dịch vụ!" data-text-data="Salon 30Shine 702 Đường Láng mới hoạt động từ ngày 11/08/2016, mời anh qua trải nghiệm dịch vụ!" data-noti-type="ic_info_status_bar"><input type="radio" name="notiForm" />Khai trương Salon</label>
                                    <label onclick="bindTextNoti($(this))" style="margin-right: 10px;" data-title="Ra mắt sản phẩm mới" data-text-noti="" data-text-data="" data-noti-type="ic_info_status_bar"><input type="radio" name="notiForm" />Ra mắt sản phẩm mới</label>
                                    <%--<label onclick="bindTextNoti($(this))" style="margin-right: 10px;" data-title="Tin nhắn từ 30Shine" data-text-noti="Anh ơi, đã lâu không thấy anh đến salon 30Shine cắt tóc, cuối tuần này anh qua để chúng em chăm sóc mái tóc cho anh nhé!" data-text-data="Anh <#customername#> ơi, đã lâu không thấy anh đến salon 30Shine cắt tóc, cuối tuần này anh qua để chúng em chăm sóc mái tóc cho anh nhé!" data-noti-type="ic_read_message_status_bar"><input type="radio" name="notiForm" />Khách lâu không đến</label>--%>
                                </span>
                            </td>
                        </tr>

                        <tr>
                            <td class="col-xs-2 left"><span>Tiêu đề</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:TextBox ID="Title" runat="server" ClientIDMode="Static" placeholder="VD: Tin nhắn từ 30Shine"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="TitleValidate" ControlToValidate="Title" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập tiêu đề!"></asp:RequiredFieldValidator>
                                </span>
                                            
                            </td>
                        </tr>                   

                        <tr class="tr-description">
                            <td class="col-xs-2 left"><span style="line-height: 18px;">Nội dung <br /> (Hiển thị trên notification feed )</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:TextBox TextMode="MultiLine" Rows="2" ID="TextNoti" runat="server" ClientIDMode="Static" placeholder="VD: Anh ơi, đã lâu rồi không thấy anh đến salon 30Shine cắt tóc..."></asp:TextBox>
                                </span>
                            </td>
                        </tr>

                        <tr class="tr-description">
                            <td class="col-xs-2 left"><span style="line-height: 18px;">Nội dung <br /> (Hiển thị trên app )</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:TextBox TextMode="MultiLine" Rows="3" ID="TextData" runat="server" ClientIDMode="Static" placeholder="VD: Anh <#customername#> ơi, đã lâu rồi không thấy anh đến salon 30Shine cắt tóc..."></asp:TextBox>
                                </span>
                            </td>
                        </tr>

                        <tr class="tr-upload">
                            <td class="col-xs-3 left"><span>Ảnh slide</span></td>
                            <td class="col-xs-7 right">
                                <div class="wrap btn-upload-wp HDF_Images">
                                    <div id="Btn_UploadImg" class="btn-upload-img" onclick="popupImageIframe('HDF_Images')">Thêm ảnh</div>
                                    <asp:FileUpload ID="UploadFile" ClientIDMode="Static" Style="display: none;"
                                        CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                    <div class="wrap listing-img-upload title-version">
                                        <% if (Img.url != null && Img.url != "")
                                            { %>
                                                <div class="thumb-wp">
                                                    <div class="left">
                                                        <img class="thumb" alt="" title="" src="<%= Img.url %>" data-img="" style="width: 120px; height: 80px; left: 0px;"/></div>
                                                    <div class="right">
                                                        <input class="thumb-title" type="text" placeholder="Tiêu đề ảnh" value="<%=Img.title %>"/>
                                                        <textarea class="thumb-des" placeholder="Mô tả"><%=Img.description %></textarea>
                                                        <div class="action">
                                                            <%--<div class="action-item action-add" onclick="popupImageIframe('HDF_Images')"><i class="fa fa-plus-square" aria-hidden="true"></i>Thêm ảnh</div>--%>
                                                            <div class="action-item action-delete" onclick="deleteThumbnail($(this), 'HDF_Images')"><i class="fa fa-trash-o" aria-hidden="true"></i>Xóa</div>
                                                        </div>
                                                    </div>
                                                </div>                                         
                                        <% } %>
                                    </div>
                                </div>                                
                            </td>
                        </tr>

                        <tr>
                            <td></td>
                            <td style="text-decoration: underline;">Gửi tới : </td>
                        </tr>

                        <tr>
                            <td style="padding-top: 0;"></td>
                            <td style="padding-top: 0;">
                                <div class="sendto-wrap">
                                    <label><input type="radio" name="sendTo" value="1" />Tất cả</label>
                                </div>
                                <%--<div class="sendto-wrap">
                                    <label><input type="radio" name="sendTo" value="2" />Khách đã 30 ngày không quay lại</label>
                                </div>--%>
                            </td>
                        </tr>

                        <tr>
                            <td></td>
                            <td style="font-style: italic; color: #808080;"><span style="width: auto; line-height: inherit; margin-right: 5px; text-decoration: underline; color: #808080;">Lưu ý : </span> Thẻ <#customername#> dùng để hiển thị tên khách hàng trên App. Thêm và đặt vào vị trí bạn muốn.</td>
                        </tr>
                    
                        <tr class="tr-send">
                            <td class="col-xs-2 left"></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <span class="btn-send" id="BtnSend" onclick="clientScript()">Gửi đi</span>
                                    <span class="btn-send" id="BtnRefresh" onclick="location.href=location.href" style="margin-left: 10px;">Làm lại</span>
                                    <span class="noti-status noti-success" id="notiStatus">Tin nhắn đã được gửi đi thành công!</span>
                                    <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>
                                </span>
                            </td>
                        </tr>
                        <asp:HiddenField runat="server" ID="UrlKeyField"  ClientIDMode="Static"/>
                        <asp:HiddenField runat="server" ID="ImgCoverField"  ClientIDMode="Static"/>
                        <asp:HiddenField runat="server" ID="HDF_Images"  ClientIDMode="Static"/>
                        <asp:HiddenField runat="server" ID="HDF_NotiType"  ClientIDMode="Static"/>
                    </tbody>
                </table>
            </div>
        </div>
    <%-- end Add --%>
</div>

<script>
    jQuery(document).ready(function () {
        $("#glbAdminContent").addClass("active");
        $("#glbAdminNotification").addClass("active");
        //============================
        // Show System Message
        //============================ 
        var qs = getQueryStrings();
        showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);
    });

    function clientScript() {
        var notiData = { title: "", text: "", text_data: "", sendTo : 0, notiType : ""};
        // Lấy dữ liệu notification
        notiData.title = $("#Title").val();
        notiData.text = $("#TextNoti").val();
        notiData.text_data = $("#TextData").val();
        notiData.image = JSON.stringify(imgLink("HDF_Images"));
        notiData.sendTo = $("input[name='sendTo']:checked").val();
        notiData.notiType = $("#HDF_NotiType").val();
        console.log(notiData);
        // Validate điều kiện
        if (notiData.title == "") {
            // show EBPopup
            $(".confirm-yn").openEBPopup();
            $("#EBPopup .confirm-yn-text").text("Bạn vui lòng nhập tiêu đề.");

            $("#EBPopup .yn-yes").remove();
            $("#EBPopup .yn-no").text("Đóng").css({ "margin": "0 auto", "position": "relative", "top": "10px", "float": "none" }).bind("click", function () {
                autoCloseEBPopup(0);
            });
        } else if(notiData.text == ""){
            // show EBPopup
            $(".confirm-yn").openEBPopup();
            $("#EBPopup .confirm-yn-text").text("Bạn vui lòng nhập nội dung.");

            $("#EBPopup .yn-yes").remove();
            $("#EBPopup .yn-no").text("Đóng").css({ "margin": "0 auto", "position": "relative", "top": "10px", "float": "none" }).bind("click", function () {
                autoCloseEBPopup(0);
            });
        } else if (notiData.sendTo == null) {
            // show EBPopup
            $(".confirm-yn").openEBPopup();
            $("#EBPopup .confirm-yn-text").text("Bạn vui lòng chọn đối tượng gửi.");

            $("#EBPopup .yn-yes").remove();
            $("#EBPopup .yn-no").text("Đóng").css({ "margin": "0 auto", "position": "relative", "top": "10px", "float": "none" }).bind("click", function () {
                autoCloseEBPopup(0);
            });
        } else {
            // show EBPopup
            $(".confirm-yn").openEBPopup();
            $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn gửi thông báo này đến khách hàng?!!");

            $("#EBPopup .yn-yes").bind("click", function () {
                autoCloseEBPopup(0);
                addLoading();
                $.ajax({
                    type: "POST",
                    url: "/admin/notification/send-to-users",
                    data: '{title : "' + notiData.title + '", text_noti : "' + notiData.text + '", text_data : "' + notiData.text_data + '", image : ' + notiData.image + ', notiType : "' + notiData.notiType + '", sendTo : ' + notiData.sendTo + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.d.status != null && response.d.status == "success") {
                            $("#notiStatus").removeClass("noti-fail").addClass("noti-success").text("Tin nhắn đã được gửi đi thành công!").show().fadeOut(5000);
                        } else {
                            $("#notiStatus").removeClass("noti-success").addClass("noti-fail").text("Tin nhắn chưa gửi được đến khách hàng!").show().fadeOut(5000);
                        }
                        removeLoading();
                    },
                    failure: function (response) { alert(response.d); }
                });
            });

            $("#EBPopup .yn-no").bind("click", function () {
                autoCloseEBPopup(0);
            });
            
        }
    }

    function bindTextNoti(This) {
        var title = This.attr("data-title");
        var text_noti = This.attr("data-text-noti");
        var text_data = This.attr("data-text-data");
        var noti_type = This.attr("data-noti-type");
        $("#Title").val(title);
        $("#TextNoti").val(text_noti);
        $("#TextData").val(text_data);
        $("#HDF_NotiType").val(noti_type);
    }
</script>

<!-- Popup plugin image -->
<script type="text/javascript">
    function popupImageIframe(StoreImgField) {
        var Width = 960;
        var Height = 560;
        var ImgList = [];
        var iframe = $("<div class='iframe-image-upload' style='width:" + Width + "px; height:" + Height + "px; float:left; display:none;'><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + StoreImgField + "&callback=getThumbFromIframe&imgWidth=1334&imgHeight=750&multi_img=false&Folder=Common' style='width:100%; height: 100%;'></iframe></div");

        $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
    }

    window.getThumbFromIframe = function (Imgs, StoreImgField) {
        var imgs = "";
        var thumb = "";
        if (Imgs.length > 0) {
            for (var i = 0; i < Imgs.length; i++) {
                thumb += '<div class="thumb-wp">' +
                            '<div class="left">' +
                                    '<img class="thumb" alt="" title="" src="' + Imgs[i] + '" data-img=""/>' +
                            '</div>' +
                            '<div class="right">' +
                                '<input class="thumb-title" type="text" placeholder="Tiêu đề ảnh" />' +
                                '<textarea class="thumb-des" placeholder="Mô tả"></textarea>' +
                                '<div class="action">' +                                            
                                    //'<div class="action-item action-add" onclick="popupImageIframe(\'HDF_Images\')"><i class="fa fa-plus-square" aria-hidden="true"></i>Thêm ảnh</div>' +
                                    '<div class="action-item action-delete" onclick="deleteThumbnail($(this), \'' + StoreImgField + '\')"><i class="fa fa-trash-o" aria-hidden="true"></i>Xóa</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>';
            }
            $("." + StoreImgField).find(".listing-img-upload").empty().append($(thumb));
            excThumbWidth(120, 120);
        }
        autoCloseEBPopup();
    }

    function imgLink(StoreImgField) {
        var imgs = [], img = { url : "", thumb : "", title : "", description : "" }, src, title, description;
        $("." + StoreImgField).find(".thumb-wp").each(function () {
            src = $(this).find("img.thumb").attr("src");
            title = $(this).find("input.thumb-title").val();
            description = $(this).find("textarea.thumb-des").val();
            img = { url: src, thumb: executeThumPath(src), title: title, description: description };
        });
        return img;
    }

    function initImgStore(StoreImgField) {
        $("#" + StoreImgField).val(JSON.stringify(imgLink(StoreImgField)));
    }

    function deleteThumbnail(This, StoreImgField) {
        This.parent().parent().parent().remove();
    }

    function executeThumPath(src) {
        var str = "";
        var fileExtension = "";
        var strLeft = "";
        var strPush = "x350";
        if (src != null && src != "") {
            var loop = src.length - 1;
            for (var i = loop; i >= 0; i--) {
                if (src[i] == ".") {
                    str = src.substring(0, i) + strPush + "." + fileExtension;
                    break;
                } else {
                    fileExtension = src[i] + fileExtension;
                }
            }
        }
        return str;
    }
</script>
<!-- Popup plugin image -->

</asp:Panel>
</asp:Content>

