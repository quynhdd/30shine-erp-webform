﻿using _30shine.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
namespace _30shine.GUI.BackEnd.Notification
{
    public partial class ListNotification : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                bindDdlName();
            }
        }
        /// <summary>
        /// button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _BtnClick(object sender, EventArgs e)
        {
            try
            {
                UIHelpers.RemoveLoading(this);
                BindData();

            }
            catch (Exception ex)
            {
                UIHelpers.TriggerJsMsgSystem_v1(this, "Lỗi: " + ex.Message.Replace("'", "\'"), "msg-system warning", 15000);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="TotalRow"></param>
        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        /// <summary>
        /// Get_TotalPage
        /// </summary>
        /// <param name="TotalRow"></param>
        /// <returns></returns>
        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        /// <summary>
        /// Bind data
        /// </summary>
        private void BindData()
        {
            try
            {
                var name = ddlName.SelectedValue;
                var result = new HttpClient().GetAsync(Libraries.AppConstants.URL_API_NOTIFICATION + "/api/notification/list-noti?name=" + name).Result;
                if (result.IsSuccessStatusCode)
                {
                    var data = result.Content.ReadAsStringAsync().Result;
                    var listObjNotification = new JavaScriptSerializer().Deserialize<List<NotificationManagement>>(data);
                    Bind_Paging(listObjNotification.Count());
                    Rpt.DataSource = listObjNotification.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                    Rpt.DataBind();
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Bind name
        /// </summary>
        private void bindDdlName()
        {
            try
            {
                var result = new HttpClient().GetAsync(Libraries.AppConstants.URL_API_NOTIFICATION + "/api/notification/get-name").Result;
                if (result.IsSuccessStatusCode)
                {
                    var data = result.Content.ReadAsStringAsync().Result;
                    var objNotification = new JavaScriptSerializer().Deserialize<List<string>>(data);
                    if (objNotification != null)
                    {
                        ddlName.DataTextField = "Name";
                        ddlName.DataValueField = "Text";
                        int count = 0;
                        foreach (var v in objNotification)
                        {
                            var item1 = new ListItem(v, v);
                            ddlName.Items.Insert(count, item1);
                            count++;
                        }
                        ListItem item = new ListItem("Chọn tên thông báo", "");
                        ddlName.Items.Insert(0, item);
                        ddlName.SelectedIndex = -1;
                    }
                }
            }
            catch (Exception ex)
            {
                UIHelpers.TriggerJsMsgSystem_v1(this, "Lỗi: " + ex.Message.Replace("'", "\'"), "msg-system warning", 15000);
            }
        }
    }
}