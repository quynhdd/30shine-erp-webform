﻿<%@ Page Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="ListNotification.aspx.cs" Inherits="_30shine.GUI.BackEnd.Notification.ListNotification" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý thông tin thông báo</li>
                        <li class="li-listing"><a href="/admin/notification/app-user-listing.html">Danh sách</a></li>
                        <li class="li-add"><a href="/admin/notification/app-user/them-moi.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <style>
            .img-row {
                display: none;
                position: absolute;
                width: 120px;
                height: 120px;
                left: 100%;
            }

            .img-display.none {
                display: none;
            }

            .img-display:hover .img-row {
                display: flex;
                z-index: 999999;
            }
             .select2-container {
                width: 250px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
                margin-left:10px;
            }
        </style>

        <div class="wp customer-add customer-listing be-report">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- System Message -->
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <!-- Filter -->
                <div class="filter-item">
                    <asp:DropDownList ID="ddlName" CssClass="form-control select" runat="server" ClientIDMode="Static"></asp:DropDownList>
                    <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata right " ClientIDMode="Static" onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                </div>
                <!-- End Filter -->

                <!-- Row Table Filter -->
                <div class="table-func-panel">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="table-wp">
                            <table class="table-add table-listing">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên thông báo</th>
                                        <th>Tên hiển thị</th>
                                        <th>Mô tả</th>
                                        <th>Url thông báo</th>
                                        <th>Ảnh</th>
                                        <th>Kích hoạt</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="Rpt" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td style="width: 5%;"><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td><%# Eval("Name") %></a></td>
                                                <td><%# Eval("Title") %></td>
                                                <td><%# Eval("Descriptions") %></td>
                                                <td><%# Eval("Url") %></td>
                                                <td style="width: 5%;">
                                                    <div class="img-display <%# Eval("Images").ToString() != "" ? "" : "none" %>">
                                                        Hiện ảnh
                                                        <img class="img-row" src="<%# Eval("Images")%>" />
                                                    </div>
                                                </td>
                                                <td style="width: 7%;" class="map-edit">
                                                    <input type="checkbox" disabled="disabled" <%# Convert.ToInt32(Eval("IsPublish")) == 1 ? "Checked=\"True\"" : "" %> class="chkisActive" data-id="<%# Eval("id") %>" id="<%# Eval("id") %>" />
                                                    <div class="edit-wp">
                                                        <a class="elm edit-btn" href="/admin/notification/app-user/<%# Eval("id") %>.html" title="Sửa"></a>
                                                        <a class="elm del-btn" onclick="del(this.parentNode.parentNode.parentNode,'<%# Eval("Id") %>', '<%# Eval("Name") %>')" href="javascript://" title="Xóa"></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>

                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
            </div>
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script type="text/javascript">
                var url_noti = "<%= Libraries.AppConstants.URL_API_NOTIFICATION %>";
                jQuery(document).ready(function () {
                     $('.select').select2();
                });
                //============================
                // Event delete
                //============================
                function del(This, code, name) {
                    var code = code || null,
                        name = name || null,
                        Row = This;
                    if (!code) return false;

                    // show EBPopup
                    $(".confirm-yn").openEBPopup();
                    $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

                    $("#EBPopup .yn-yes").bind("click", function () {
                        addLoading();
                        $.ajax({
                            type: "DELETE",
                            url: url_noti + '/api/notification/?id=' + code,
                            contentType: "application/json; charset=utf-8",
                            dataType: "json", success: function (response) {
                                removeLoading();
                                delSuccess();
                                Row.remove();
                            },
                            failure: function (response) { alert(response.d); }
                        });
                    });
                    $("#EBPopup .yn-no").bind("click", function () {
                        autoCloseEBPopup(0);
                    });
                }
        </script>
    </asp:Panel>
</asp:Content>

