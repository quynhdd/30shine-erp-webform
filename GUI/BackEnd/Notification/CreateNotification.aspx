﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TemplateMaster/SiteMaster.Master" CodeBehind="CreateNotification.aspx.cs" Inherits="_30shine.GUI.BackEnd.Notification.CreateNotification" %>

<asp:Content ID="ServiceAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Tạo thông báo</li>
                        <li class="li-listing"><a href="/admin/notification/app-user-listing.html">Danh sách</a></li>
                        <li class="li-add"><a href="/admin/notification/app-user/them-moi.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <style>
            .btn {
                background-color: black;
                color: white;
            }
        </style>
        <div class="wp customer-add admin-product-add fe-service">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <!-- System Message -->
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="table-wp">
                    <table class="table-add admin-product-table-add fe-service-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Tạo mới thông báo</strong></td>
                                <td></td>
                            </tr>

                            <tr>
                                <td class="col-xs-2 left"><span>Tên thông báo</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="txtName" runat="server" ClientIDMode="Static" placeholder="Nhập tên thông báo"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-2 left"><span>Tên hiển thị</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="txtTitle" runat="server" ClientIDMode="Static" placeholder="Nhập tên hiển thị"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-2 left"><span>Mô tả</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="txtDescription" runat="server" ClientIDMode="Static" placeholder="Nhập mô tả"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Url thông báo</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="txtUrl" runat="server" ClientIDMode="Static" placeholder="Nhập mô tả"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-upload">
                                <td class="col-xs-3 left"><span>Ảnh</span></td>
                                <td class="col-xs-7 right">
                                    <div class="wrap btn-upload-wp HDF_MainImg" style="margin-bottom: 5px; width: auto;">
                                        <div id="Btn_UploadImg" class="btn-upload-img" onclick="popupImageIframe('HDF_MainImg')">Chọn ảnh</div>
                                        <asp:FileUpload ID="UploadFile" ClientIDMode="Static" Style="display: none;"
                                            CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                        <div class="wrap listing-img-upload">
                                            <div id="MainImg" class="thumb-wp">
                                                <img class="thumb" src="#" />
                                                <span class="delete-thumb" onclick="deleteThumbnail($(this), 'HDF_MainImg')"></span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="staff-servey-mark">
                                <td class="col-xs-2 left"><span>Kích hoạt</span></td>
                                <td class="col-xs-2 right">
                                    <asp:CheckBox ID="chkIsActive" runat="server" ClientIDMode="Static" />
                                </td>

                            </tr>
                            <tr class="tr-send">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <asp:Button ID ="BtnAddNoti"  CssClass="btn" OnClick="AddNotification" runat="server" Visible ="false" Enabled="true" Text="Tạo thông báo" ClientIDMode="Static"></asp:Button>
                                        <asp:Button ID ="BtnEditNoti" CssClass="btn" OnClick="EditNotification" runat="server" Visible ="false" Enabled="true" Text="Cập nhật thông báo" ClientIDMode="Static"></asp:Button>
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <asp:HiddenField ID="HDF_MainImg" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_IdNotification" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_NameNotification" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_Url" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_CreateTime" runat="server" ClientIDMode="Static" />
                </div>
            </div>
        </div>
        <script>
            jQuery(document).ready(function () {
                bindImage();
            });

            function bindImage() {
                var mainImg = $("#HDF_MainImg").val();
                mainImg = typeof mainImg == undefined || mainImg == null ? "" : mainImg;
                if (mainImg == "") {
                    $("#MainImg").hide();
                }
                else {
                    $("#MainImg img").attr("src", mainImg);
                }
            }
            function popupImageIframe(StoreImgField) {
                var Width = 960;
                var Height = 560;
                var ImgList = [];
                var iframe = $("<div class='iframe-image-upload' style='width:" + Width + "px; height:" + Height + "px; float:left; display:none;'><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + StoreImgField + "&callback=getThumbFromIframe&imgWidth=800&imgHeight=600&multi_img=false&Folder=NhanVien' style='width:100%; height: 100%;'></iframe></div");

                $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
            }

            window.getThumbFromIframe = function (Imgs, StoreImgField) {
                var imgs = "";
                var thumb = "";
                if (Imgs.length > 0) {
                    thumb = '<div class="thumb-wp">' +
                        '<img class="thumb" alt="" title="" src="' + Imgs[0] + '"' +
                        'data-img="" />' +
                        '<span class="delete-thumb" onclick="deleteThumbnail($(this), \'' + StoreImgField + '\')"></span>' +
                        '</div>';
                    $("." + StoreImgField).find(".listing-img-upload").empty().append(thumb);
                    excThumbWidth(120, 120);
                    $("#" + StoreImgField).val(Imgs[0]);
                }
                autoCloseEBPopup();
            }
            function deleteThumbnail(This, StoreImgField) {
                This.parent().remove();
                $("#" + StoreImgField).val("");
            }
        </script>
    </asp:Panel>
</asp:Content>

