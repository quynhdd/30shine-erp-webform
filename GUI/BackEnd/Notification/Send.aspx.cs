﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.Notification
{
    public partial class Send : System.Web.UI.Page
    {
        protected bool _IsUpdate = false;
        protected Library.Class.cls_media Img = new Library.Class.cls_media();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        private static string SERVER_API_KEY = "AIzaSyAMwSHk7XltP0KgXLhO5WYnsHma7QMIDaA";
        private static string SENDER_ID = "556554538523";//30ShineApp - 556554538523 // VongEBApp - 377236071532

        /// <summary>
        /// Gửi notification tới khách trước sinh nhật 3 ngày (vào 8h sáng) - Test gửi cho toàn bộ device đã đc đăng ký trên db
        /// </summary>
        /// <param name="to"></param>
        /// <param name="priority"></param>
        /// <param name="notification"></param>
        /// <returns></returns>
        [WebMethod]
        public static object SendNotification(string title, string text_noti, string text_data, Library.Class.cls_media image, string notiType, int sendTo)
        {
            using (var db = new Solution_30shineEntities())
            {
                var msg = new Library.Class.cls_message();                
                var listDeviceIds = new List<Library.Class.cls_device_infor>();
                var notification = new Library.Class.cls_notification();
                var data = new List<KeyValuePair<string, string>>();
                var now = DateTime.Now;
                var Notification = new Library.Notification(SERVER_API_KEY, SENDER_ID);

                notification.title = title;
                notification.text = text_noti;
                notification.text_data = text_data;
                notification.icon = notiType;
                notification.tag = String.Format("{0:HH}", now) + "h" + String.Format("{0:mm}", now) + " " + String.Format("{0:dd/MM/yyyy}", now);
                notification.sound = "default";
                notification.image = (image != null && image.url != "") ? /*"http://ql.30shine.com/" +*/ image.url : "";

                data.Add(new KeyValuePair<string, string>("title", notification.title));
                data.Add(new KeyValuePair<string, string>("text", notification.text_data != null && notification.text_data != "" ? notification.text_data : notification.text));
                data.Add(new KeyValuePair<string, string>("icon", notification.icon));
                data.Add(new KeyValuePair<string, string>("send_time", notification.tag));
                data.Add(new KeyValuePair<string, string>("sound", notification.sound));
                data.Add(new KeyValuePair<string, string>("image", notification.image));

                switch (sendTo)
                {
                    case 1: listDeviceIds = getAllDeviceIds(db);
                        break;
                }

                string token = "";
                int count = 0;
                int bound = 1000;

                //listDeviceIds = new List<Library.Class.cls_device_infor>();
                //var vongeb = new Library.Class.cls_device_infor();
                //vongeb.UserId = 2427;
                //vongeb.DeviceRegistrationToken = "csx8D8ojPm0:APA91bFsyQvXL5HYuMa4aUgscMUxEO8jR7Nalf7CikIx6OfvNm-3G8_GhIekDXh1qbXMSWymVGhpt3X2-z28-xbqzdQuN-3r5HnQ2rG5XgxV1qNnRPCXsUYtlzNb9Wt7rNIu4guYn62D";
                //listDeviceIds.Add(vongeb);

                if (listDeviceIds.Count > 0)
                {
                    foreach (var v in listDeviceIds)
                    {
                        token += "\"" + v.DeviceRegistrationToken + "\",";
                        count++;
                        if (count == bound)
                        {
                            token = token.TrimEnd(',');
                            //msg = Notification.SendToMultipleDevice(token, "high", notification, data);
                            token = "";
                            count = 0;
                        }
                    }
                }
                
                // Statistic sended notification
                statisticNoti(db, listDeviceIds);
                return msg;
            }
        }

        /// <summary>
        /// Lấy toàn bộ devices
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        private static List<Library.Class.cls_device_infor> getAllDeviceIds(Solution_30shineEntities db)
        {
            var list = new List<Library.Class.cls_device_infor>();
            var item = new Library.Class.cls_device_infor();
            var devices = db.Api_DeviceManager.Where(w => w.IsDelete != true).ToList();
            if (devices.Count > 0)
            {
                foreach (var v in devices)
                {
                    item = new Library.Class.cls_device_infor();
                    item.UserId = v.UserId.Value;
                    item.DeviceRegistrationToken = v.DeviceRegistrationToken;
                    list.Add(item);
                }
            }
            return list;
        }

        /// <summary>
        /// Lấy các khách hàng đã lâu (days ngày) không quay lại dùng dịch vụ
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        private static List<Library.Class.cls_device_infor> getDeviceIds_CustomerDontBack(Solution_30shineEntities db, int days)
        {
            var timeTo = DateTime.Now;
            var timeFrom = timeTo.AddDays(-days);
            string sql = @"select devices.UserId,devices.DeviceRegistrationToken
                            from
                            (
	                            select CustomerId
	                            from BillService as bill
	                            where bill.IsDelete != 1 and bill.Pending != 1
	                            and ServiceIds != '' and ServiceIds is not null
	                            and CustomerId > 0
	                            and bill.CompleteBillTime between '" + String.Format("{0:yyyy/MM/dd}", timeFrom) + "' and '" + String.Format("{0:yyyy/MM/dd}", timeFrom.AddDays(1)) + @"'
	                            and (
			                            select Count(*) from BillService 
			                            where IsDelete != 1 and Pending != 1 
			                            and ServiceIds != '' and ServiceIds is not null
			                            and CompleteBillTime between '" + String.Format("{0:yyyy/MM/dd}", timeFrom.AddDays(1)) + "' and '" + String.Format("{0:yyyy/MM/dd}", timeTo.AddDays(1)) + @"'
			                            and CustomerId = bill.CustomerId
		                            ) = 0
	                            group by bill.CustomerId
                            ) as a
                            inner join Customer as customer
                            on a.CustomerId = customer.Id
                            inner join Api_DeviceManager as devices
                            on a.CustomerId = devices.UserId and devices.IsDelete != 1";

            return db.Database.SqlQuery<Library.Class.cls_device_infor>(sql).ToList();
        }

        /// <summary>
        /// Ghi lại lần gửi notification cho các devices
        /// </summary>
        /// <param name="db"></param>
        /// <param name="listDevices"></param>
        private static void statisticNoti(Solution_30shineEntities db, List<Library.Class.cls_device_infor> listDevices)
        {
            if (listDevices.Count > 0)
            {
                var item = new Api_NotiSendManager();
                foreach (var v in listDevices)
                {
                    item = new Api_NotiSendManager();
                    item.UserId = v.UserId;
                    item.DeviceRegistrationToken = v.DeviceRegistrationToken;
                    item.SendTime = DateTime.Now;
                    db.Api_NotiSendManager.Add(item);
                    db.SaveChanges();
                }
            }
        }
    }
}