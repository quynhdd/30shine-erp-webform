﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;

namespace _30shine.GUI.BackEnd.Check3S
{
    public partial class CheckCSVC_Done_L : System.Web.UI.Page
    {
        private string PageID = "CL_CHECKCSVC_L_DONE";
        private bool Perm_Access = false;
        private bool Perm_ShowSalon = false;
        private int _StaffId = 0;
        private int _SalonId = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            var staffId = _StaffId;
            var salonId = _SalonId;

            if (!IsPostBack)
            {
                BindListDo();
            }
        }
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ShowSalon = permissionModel.GetActionByActionNameAndPageId("Perm_ShowSalon", PageID, permission);
                ////Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                ////Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                ////Perm_ViewAllDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDate", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                //ContentWrap.Visible = false;
                //NotAllowAccess.Visible = true;
            }
        }
        private void BindListDo()
        {
            using (var db = new Solution_30shineEntities())
            {
                if (_SalonId != 0)
                {
                    var sql = @"SELECT a.*, b.Name as ItemName FROM [ERP.CheckCSVC] a
                            Inner Join [ERP.ItemCheck] b
                            On a.ItemId = b.Id
                            WHERE a.SalonId =" + _SalonId + " And a.Status = 3";
                    var list = db.Database.SqlQuery<cls_checkCSVC>(sql).ToList();
                    rptDo.DataSource = list;
                    rptDo.DataBind();
                }
                else
                {
                    var sql = @"SELECT a.*, b.Name as ItemName FROM [ERP.CheckCSVC] a
                            Inner Join [ERP.ItemCheck] b
                            On a.ItemId = b.Id
                            WHERE a.Status = 3";
                    var list = db.Database.SqlQuery<cls_checkCSVC>(sql).ToList();
                    rptDo.DataSource = list;
                    rptDo.DataBind();
                }


            }
        }

        
    }
}