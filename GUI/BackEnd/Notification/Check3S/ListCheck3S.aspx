﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ListCheck3S.aspx.cs" Inherits="_30shine.GUI.BackEnd.Check3S.ListCheck3S" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>List check 3S</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <%--<meta name="viewport" content="width=device-width, initial-scale=1.0" />--%>
    <!--jquery-->
    <script src="../../../Assets/js/jquery.v1.11.1.js"></script>
    <!-- Bootstrap 3-->
    <link href="../../../Assets/css/bootstrap/bootstrap.min.css" rel="stylesheet" />
    <link href="../../../Assets/css/bootstrap/bootstrap-theme.min.css" rel="stylesheet" />

    <link href="../../../Assets/css/check-list.css" rel="stylesheet" />
    <link href="../../../Assets/css/jquery.datetimepicker.css?v=2" rel="stylesheet" />
    <script src="../../../Assets/js/jquery.datetimepicker.js"></script>
    <script src="../../../Assets/js/bootstrap/bootstrap.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container-fluid">
            <div class="row">
                <div class="title background black">
                    <label><a href="/" title="Trang chủ solution">Home |&nbsp;</a></label>
                    <label>Xem lịch sử Check List 3S</label>
                </div>
                <div class="wp_proviso">
                    <div class="ctn_proviso">
                        <div class="wp_date ">
                            <label class="">Từ ngày</label>
                            <asp:TextBox CssClass="form-control txtDateTime" ID="txtFromDate" runat="server" placeholder="Từ ngày ..."></asp:TextBox>
                        </div>
                        <div class="wp_date ">
                            <label>Tới ngày</label>
                            <asp:TextBox ID="txtToDate" class="form-control txtDateTime" runat="server" placeholder="Đến ngày ..."></asp:TextBox>
                        </div>
                        <div class="wp_full_date ">
                            <label>Salon</label>
                            <asp:DropDownList CssClass="form-control select" ID="ddlSalon" runat="server"></asp:DropDownList>
                        </div>
                        <div class="wp_full_date ">
                            <%--  <asp:Button ID="Views" CssClass="a_btn_list_check_3s btn btn-success" runat="server" Text="Xem" OnClick="Views_Click" />--%>
                            <a class="a_btn_list_check_3s btn btn-success" onclick="clickViews()" href="javascript://">Xem</a>
                        </div>
                    </div>

                    <!--listing check list-->
                    <div id="main-screen" class="">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="info">
                                        <th>#</th>
                                        <th>Item Check3S</th>
                                        <th>Ngày</th>
                                        <%--<th>Tên NViên</th>--%>
                                        <th>Chi tiết</th>
                                    </tr>
                                </thead>
                                <tbody id="table-listing">
                                    <%-- Content --%>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <%-- PopUp --%>
                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Vệ sinh khu <%=NameItem %></h4>
                            </div>
                            <div class="modal-body" id="CheckList3S">
                                <%-- Content --%>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <style>
        /*.container-fluid { padding: 0px; }*/
        .img_check { margin-top: 10px; }
        p { margin-bottom: 0px; }
        .views_detail_vs { padding: 5px 10px; }
        .modal-body { padding: 10px 15px; }
        .modal-header { padding: 10px 5px; }
        .modal-footer { padding: 10px 5px; }
        .wp_proviso { width: 100%; float: left; margin-top: 10px; }
        .wp_date { width: 50%; float: left; padding: 0px 5px; margin-bottom: 10px; }
        .a_btn_list_check_3s { width: 100%; }
        .wp_full_date { width: 100%; float: left; margin-bottom: 10px; padding: 0px 5px; }
    </style>
    <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
    <script src="../../../Assets/js/select2/select2.min.js"></script>
    <script>
        $('.select').select2();
    </script>
    <style>
        .select2-container {
            width: 75% !important;
            margin-top: 5px !important;
        }

        .select2-container--default .select2-selection--single {
            height: 32px !important;
            padding-top: 1px !important;
        }
    </style>
    <script>
        function clickViews() {
            var _FromDate = $("#txtFromDate").val();
            var _ToDate = $("#txtToDate").val();
            var _SalonId = $("#ddlSalon").val();

            if (_FromDate != "" && _ToDate != ""/* && _SalonId != 0*/) {
                console.log("tu====" + _FromDate + '=======' + _ToDate + '++++++++++' + _SalonId);
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Check3S/ListCheck3S.aspx/BindListCheckList3S",
                    data: '{_FromDate : "' + _FromDate + '", _ToDate : "' + _ToDate + '", _SalonId:"' + _SalonId + '" }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        $('#table-listing').html('');
                        $("#table-listing").html('' + response.d + '');
                        //var jsonObj = $.parseJSON(response.d);
                        //console.log(jsonObj);
                        //var _Count = jsonObj.length;

                        //if (_Count > 0) {

                        //    $('#table-listing').html('');
                        //    var i = 1;
                        //    for (var i = 0; i < _Count; i++) {
                        //        console.log("123");
                        //        $("#table-listing").append(' <tr> <td>' + i + '</td> <td>' + jsonObj[i]["NameItemCheck"] + '</td> <td>' + jsonObj[i]["DateCheck"] + '</td> <td>' + jsonObj[i]["NameStaff"] + '</td> <td><a class="views_detail_vs" data-id="' + jsonObj[i]["Id"] + ' " data-toggle="modal" data-target="#myModal" href="javascript://" onclick="clickDetail($(this))">Xem</a></td> </tr>');
                        //    }
                        //    i++;
                        //}
                    },
                    failure: function (response) { alert(response.d); }
                });
            }
        }

        $(document).ready(function () {
            $('.txtDateTime').datetimepicker({
                dayOfWeekStart: 1,
                lang: 'vi',
                startDate: '2014/10/10',
                format: 'd/m/Y',
                dateonly: true,
                showHour: false,
                showMinute: false,
                timepicker: false,
                onChangeDateTime: function (dp, $input) {
                    //$input.val($input.val().split(' ')[0]);
                }
            });

        });
        //
        function clickDetail(This) {
            var _Id = This.attr('data-id');
            var _NameItemCheck = This.attr('data-nameitem');
            console.log("abc" + _Id);
            $.ajax({
                type: "POST",
                url: "/GUI/BackEnd/Check3S/ListCheck3S.aspx/DetailCheckList",
                data: '{_Id : "' + _Id + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    FuntionNameItem(_NameItemCheck);
                    console.log(response.d);                    
                    $('#CheckList3S').html('');
                    $('#CheckList3S').html('' + response.d + '');
                    //var jsonObj = $.parseJSON(response.d);
                    //console.log(jsonObj);

                    //var _Count = jsonObj.length;
                    //if (_Count > 0) {
                    //    $('#myModalLabel').html('Vệ sinh khu')
                    //    $('#CheckList3S').html('');
                    //    for (var i = 0; i < _Count; i++) {
                    //        $('#myModalLabel').html('Vệ sinh khu : ' + jsonObj[i]["NameItemCheck"] + ' ');
                    //        $("#CheckList3S").append(' <p>' + jsonObj[i]["NameStaff"] + ' (bị phạt)</p> <p>Salon : ' + jsonObj[i]["NameSalon"] + '</p> <p>Ngày : ' + jsonObj[i]["DateCheck"] + '</p> <p class="">Người tạo : ' + jsonObj[i]["NameIsCheck"] + '</p> <img src="' + jsonObj[i]["img"] + '" />');
                    //    }
                    //}
                },
                failure: function (response) { alert(response.d); }
            });
            //End Ajax
        }
        function FuntionNameItem(_NameItemCheck)
        {
            $('#myModalLabel').html('Check3S : ' + _NameItemCheck + '');
        }
    </script>
</body>
</html>

