﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="ViewCheck3S.aspx.cs" Inherits="_30shine.GUI.BackEnd.Check3S.ViewCheck3S" %>
<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .table-listing .uv-avatar {
                width: 120px;
            }

            .table-row-detail {
                display: inline-block;
                width: 50% !important;
                min-width: 500px;
                float: none !important;
            }

            .product-QuantityReceived {
                width: 50px !important;
                height: 22px !important;
                line-height: 22px !important;
                text-align: center;
                border-bottom: 1px solid #ddd !important;
                border-top: none !important;
                border-right: none !important;
                border-left: none !important;
                background: none;
            }
        </style>
        <div class='loading-frame hidden'></div>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý Check 3S </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing be-report be-report-timekeeping">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
                <!-- System Message -->
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <!-- END System Message -->
                <div class="row">
                    <div class="filter-item">
                        <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="time-wp">
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeFrom" placeholder="Từ ngày" Style="margin-left: 10px!important"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ValidateTime" ControlToValidate="TxtDateTimeFrom" runat="server" CssClass="fb-cover-error" Text="Vui lòng chọn thời gian!"></asp:RequiredFieldValidator>
                        </div>
                        <strong class="st-head" style="margin-left: 10px;">
                            <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                        </strong>
                        <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeTo" placeholder="Đến ngày" Style="margin-left: 10px!important"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                        <br />
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlSalon" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px;"></asp:DropDownList>
                    </div>
                    <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <a href="javascript:void(0);" onclick="location.href=location.href" class="st-head btn-viewdata">Reset Filter</a>

                </div>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>Danh sách Check 3S</strong>
                </div>
                <!-- Row Table Filter -->
                <div class="table-func-panel" style="margin-top: -33px;">
                    <div class="table-func-elm">

                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="row table-wp">
                            <table id="tblDonHang" class="table-add table-listing">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th class="hidden">ID</th>
                                        <th>Ngày check</th>
                                        <th>Hình ảnh</th>
                                        <th>Tên kiểm tra</th>
                                        <th>Ghi chú</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptDanhsach" runat="server">
                                        <ItemTemplate>
                                            <tr onclick="addTrToTable($(this));" class="parent">
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td class="hidden"><%# Eval("Id") %></td>
                                                <td class="map-edit">
                                                    <%# Eval("DateTime") != null ? String.Format("{0:dd/MM/yyyy}", Eval("DateTime")) + " - " + String.Format("{0:HH:mm}", Eval("DateTime")) : "" %>
                                                    <%--<%# Eval("CreateDate") %></td>--%>
                                                <td>
                                                    <img src='/Assets/images/check-csvc-icon/<%# DataBinder.Eval(Container.DataItem, "Image") %>'  
                                                        alt="No Image" style="height:200px;width:200px;"/>
                                                </td>
                                                <td>
                                                    <img src='/Resource/Images/Check3S/<%# DataBinder.Eval(Container.DataItem, "Icon") %>'  
                                                        alt="No Image" /> &nbsp;&nbsp;&nbsp;&nbsp;
                                                    <%# Eval("CheckItemName") %>

                                                </td>
                                                <td><%# Eval("Comment") %></td>
                                                
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>

               <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />

                <!-- Hidden Field-->
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                <asp:HiddenField ID="HDF_TypeStaff" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_Staff" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_ExcelType" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_ExportType" ClientIDMode="Static" runat="server" />
                <!-- END Hidden Field-->
            </div>
            <%-- END Listing --%>
        </div>

        <link href="/Assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <script src="/Assets/js/jquery.mCustomScrollbar.js"></script>
        <script type="text/ecmascript">
            var listElement = listElement;
            jQuery(document).ready(function () {
                // Add active menu
                $("#glbDatHangNoiBo").addClass("active");
                $("#glbDatHangNoiBo_ChoSalon").addClass("active");
                $("#subMenu li.li-listing").addClass("active");
                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });

                //============================
                // Bind Staff
                //============================
                BindStaffFilter();

                // View data yesterday
                $(".tag-date-today").click();

                var qs = getQueryStrings();
                showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);
            });

            function showMsgSystem(msg, status) {
                $("#MsgSystem").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);
                setTimeout(function () {
                    $("#MsgSystem").fadeTo("slow", 0, function () {
                        $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 1);
                    });
                }, 5000);
            }

            function showTrPrice() {
                $(".tr-price").show();
            }

            function BindStaffFilter() {
                $(".eb-select").bind("focus", function () {
                    EBSelect_ShowBox($(this).parent().find(".eb-select-data"));
                });
                $(window).bind("click", function (e) {
                    console.log(e);
                    if ((!e.target.className.match("eb-select") && e.target.parentElement.className.match("ul-listing-staff")) ||
                        (!e.target.className.match("eb-select") && !e.target.parentElement.className.match("ul-listing-staff"))
                    && !e.target.className.match("mCSB_dragger_bar")) {
                        EBSelect_HideBox();
                    }
                });
                //============================
                // Scroll Staff Filter
                //============================
                $('.eb-select-data').each(function () {
                    if ($(this).height() > 230) {
                        $(this).mCustomScrollbar({
                            theme: "dark-2",
                            scrollInertia: 100
                        });
                    }
                });
                $(".show-item").bind("click", function () {
                    var item = $(this).attr("data-item");
                    var classItem = ".popup-" + item + "-item";

                    $(classItem).openEBPopup();
                    ExcCheckboxItem();
                    ExcQuantity();
                    ExcCompletePopup();

                    $('#EBPopup .listing-product').mCustomScrollbar({
                        theme: "dark-2",
                        scrollInertia: 100
                    });

                    $("#EBPopup .btn-esc").bind("click", function () { autoCloseEBPopup(0); });
                });
            }

            function BindIdToHDF(THIS, id, HDF_DomId, Input_DomId) {
                var text = THIS.innerText.trim(),
                    HDF_Dom = document.getElementById(HDF_DomId),
                    InputText = document.getElementById(Input_DomId);
                HDF_Dom.value = id;
                InputText.value = text;
                InputText.setAttribute("data-value", id);
                ajaxGetStaffsByType(id);
                if (HDF_DomId == "HDF_TypeStaff") {
                    $("#StaffName").val("");
                    $("#HDF_Staff").val("");
                }
            }

            function ajaxGetStaffsByType(type) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Report/TimeKeeping.aspx/Load_Staff_ByType",
                    data: '{type : "' + type + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            var staffs = JSON.parse(mission.msg);
                            if (staffs.length > 0) {
                                var tmp = "";
                                $.each(staffs, function (i, v) {
                                    tmp += '<li data-code="' + v.Code + '" data-id="' + v.Id + '"' +
                                            'onclick="BindIdToHDF(this,' + v.Id + ',\'HDF_Staff\',\'StaffName\')">' +
                                            v.Fullname + '</li>';
                                });
                                $("#UlListStaff").empty().append(tmp);
                            }

                        } else {
                            var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                            showMsgSystem(msg, "warning");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function EBSelect_ShowBox(Dom) {
                EBSelect_HideBox();
                Dom.show();
            }

            function EBSelect_HideBox() {
                $(".eb-select-data").hide();
            }

            function EBSelect_BindData() {

            }

            function ReplaceZezoValue() {
                $("table.table-listing td").each(function () {
                    if ($(this).text().trim() == "0") {
                        $(this).text("-");
                    }
                });
            }

            function viewDataByDate(This, time) {
                $(".tag-date.active").removeClass("active");
                This.addClass("active");
                $("#TxtDateTimeTo").val("");
                $("#TxtDateTimeFrom").val(time);
                $("#ViewData").click();
            }


        </script>
    </asp:Panel>
</asp:Content>



