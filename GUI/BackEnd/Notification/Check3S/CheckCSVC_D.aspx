﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CheckCSVC_D.aspx.cs" Inherits="_30shine.GUI.BackEnd.Check3S.CheckCSVC_D" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Check CSVC Details</title>
    <!--jquery-->
    <script src="../../../Assets/js/jquery.v1.11.1.js"></script>
    <!-- Bootstrap 3-->
    <link href="../../../Assets/css/bootstrap/bootstrap.min.css" rel="stylesheet" />
    <link href="../../../Assets/css/bootstrap/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="../../../Assets/css/check-list.css" rel="stylesheet" />
</head>
<body>
    <div class="check-csvc">
        <div class="title background black">
            <label><a href="/" title="Trang chủ solution">Home |&nbsp</a></label>
            <label>Check List CSVC</label>
        </div>
        <div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 top-tile">
                        Chi Tiết CSVC Báo Hỏng
                    </div>
                    <div class="col-xs-12 col-sm-12 detail">
                        <p>- Hạng mục : <asp:Literal Text="" ID="ltrDanhMuc" runat="server" /></p>
                        <p>- Salon : <asp:Literal Text="" ID="ltrSalon" runat="server" /></p>
                        <p style="display:none;">- Phạt NV : <asp:Literal Text="" ID="ltrNhanVien" runat="server" /></p>
                        <p>- Ngày giờ : <asp:Literal Text="" ID="ltrNgayGio" runat="server" /></p>
                        <p style="display:none;">- Người tạo : <asp:Literal Text="" ID="ltrNguoiTao" runat="server" /></p>
                        <p>- Chi tiết : <asp:Literal Text="" ID="ltrComment" runat="server" /></p>
                        <p>- Hình ảnh : <img src="javascript://" runat="server" id="imgDes" alt="" /></p>
                        <p><label style="float: left; font-weight: normal;">Ngày sửa xong:</label> <input type="text" class="txtDateTime" style="float: left; width: 60%; padding-left: 10px;" /></p>
                        <p class="action">
                            <input type="button" id="btnBack" onclick="window.location = '/admin/chat-luong/checkcsvc_l.html'" value="Quay lại" />
                            <input type="button" id="btnConfirm" value="Xác nhận làm" />
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .detail { background: #ccc; padding: 0;}
        .detail p { margin: 0; padding: 7px 5px; float: left; width: 100%; border-bottom: 1px solid #221d1d;}
        .action { text-align: center;}
        .action input[type=button] { border: none; padding: 5px;}
        #btnConfirm { background: #50b347; border: none;}
        .action input[type=button]:hover { background: #F2D600!important; }
    </style>
    <input type="hidden" value="0" runat="server" id="hdfStaffId" />
    <input type="hidden" value="0" runat="server" id="hdfCSVCId" />
    <link href="../../../Assets/css/jquery.datetimepicker.css" rel="stylesheet" />
    <script src="../../../Assets/js/jquery.datetimepicker.js"></script>
    <script>
        jQuery(document).ready(function () {
            var checkCSVCId = $('#hdfCSVCId').val();
            var staffId = $('#hdfStaffId').val();
            console.log(checkCSVCId + " - " + staffId);
            //============================
            // Datepicker
            //============================
            $('.txtDateTime').datetimepicker({
                dayOfWeekStart: 1,
                lang: 'vi',
                startDate: '2014/10/10',
                format: 'd/m/Y',
                dateonly: true,
                showHour: false,
                showMinute: false,
                timepicker: false,
                onChangeDateTime: function (dp, $input) { }
            });

            $('#btnConfirm').click(function () {
                var date = $('.txtDateTime').val();
                if (date == '') {
                    alert('Bạn chưa chọn ngày sửa xong!');
                    return;
                }
                var frm = confirm("Xác nhận sửa cơ sở vật chất hỏng?")
                if (frm)
                {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/Check3S/CheckCSVC_D.aspx/UpdateStatus",
                        //string _FixDate, int _StaffId, int _Id
                        data: '{_FixDate : "' + date + '", _StaffId : "' + staffId + '", _Id : "' + checkCSVCId + '"}',
                        
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            if (response.d == 'success')
                                window.location = '/admin/chat-luong/checkcsvc_l.html';
                            else
                                alert('Đã có lỗi xảy ra trong quá trình cập nhật, vui lòng thử lại sau!')
                        },
                        failure: function (response) { console.log(response.d); }
                    });
                }
            });
        });
    </script>
</body>
</html>
