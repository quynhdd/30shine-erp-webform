﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity.Migrations;
using Project.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using _30shine.Helpers;
using _30shine.MODEL.ENTITY.EDMX;

namespace _30shine.GUI.BackEnd.Check3S
{
    public partial class ViewCheck3S : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        protected string THead = "";
        protected string StrProductPrice = "";
        public CultureInfo culture = new CultureInfo("vi-VN");
        private DateTime timeFrom;
        private DateTime timeTo;
        protected int salonId;
        private int integer;
        protected string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
        protected string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);

        protected bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected string Permission = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckPermission();

            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Today);
                bindSalon();
                bindData();

            }
            else
            {
                //Exc_Filter();
            }
            RemoveLoading();
        }

        /// <summary>
        /// Bind dữ liệu salon
        /// </summary>
        private void bindSalon()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Salons = db.Tbl_Salon.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Id).ToList();
                var Key = 0;
                var SalonId = Convert.ToInt32(Session["SalonId"]);

                ddlSalon.DataTextField = "Salon";
                ddlSalon.DataValueField = "Id";

                ListItem item = new ListItem();
                if (Perm_ViewAllData)
                {
                    foreach (var v in _Salons)
                    {
                        item = new ListItem(v.Name, v.Id.ToString());
                        ddlSalon.Items.Insert(Key++, item);
                    }
                    ddlSalon.SelectedIndex = 0;
                }
                else
                {
                    foreach (var v in _Salons)
                    {
                        if (v.Id == SalonId)
                        {
                            item = new ListItem(v.Name, v.Id.ToString());
                            ddlSalon.Items.Insert(Key, item);
                            ddlSalon.SelectedIndex = SalonId;
                            ddlSalon.Enabled = false;
                        }
                    }
                }
            }
        }

        private void bindData()
        {
            using (var db = new Solution_30shineEntities())
            {
                try
                {
                    if (TxtDateTimeFrom.Text != "")
                    {
                        timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                        if (TxtDateTimeTo.Text != "")
                        {
                            timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                        }
                        else
                        {
                            timeTo = timeFrom;
                        }
                        salonId = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;

                        var data = db.Store_Check3S_View(timeFrom, timeTo, salonId).ToList();
                        Bind_Paging(data.Count);
                        rptDanhsach.DataSource = data.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                        rptDanhsach.DataBind();
                    }
                }
                catch { }
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            bindData();
            RemoveLoading();
        }

        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["User_Id"] == null)
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "salonmanager", "checkin", "checkout" };
                string[] AllowAllData = new string[] { "root", "admin" };
                string[] AllowViewAllDate = new string[] { "root", "admin" };
                string[] Accountant = new string[] { "accountant" };
                Permission = Session["User_Permission"].ToString().Trim();

                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }

                if (Array.IndexOf(AllowViewAllDate, Permission) != -1)
                {
                    Perm_ViewAllData = true;
                }
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
    }
}