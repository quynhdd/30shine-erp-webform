﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CheckCSVC_Done_L.aspx.cs" Inherits="_30shine.GUI.BackEnd.Check3S.CheckCSVC_Done_L" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Check CSVC Listing</title>
    <!--jquery-->
    <script src="../../../Assets/js/jquery.v1.11.1.js"></script>
    <!-- Bootstrap 3-->
    <link href="../../../Assets/css/bootstrap/bootstrap.min.css" rel="stylesheet" />
    <link href="../../../Assets/css/bootstrap/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="../../../Assets/css/check-list.css" rel="stylesheet" />
</head>
<body>
    <div class="check-csvc">
        <div class="title background black">
            <label><a href="/" title="Trang chủ solution">Home |&nbsp</a></label>
            <label>Check List CSVC</label>
        </div>
        <div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 top-tile">
                        Danh Sách CSVC Báo Hỏng
                    </div>
                    <div class="col-xs-4 col-sm-4 col-lg-4 do flow3s flow-todo">
                        <a href="/admin/chat-luong/checkcsvc_l.html">Cần làm</a>
                        <div class="line-active"></div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-lg-4 doing flow3s flow-doing">
                         <a href="/admin/chat-luong/checkcsvc_doing_l.html">Đang làm</a>
                        <div class="line-active"></div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-lg-4 done flow3s flow-done active">
                        <a href="/admin/chat-luong/checkcsvc_done_l.html">Đã xong</a>
                        <div class="line-active"></div>
                    </div>

                    <asp:Repeater runat="server" ID="rptDo">
                        <ItemTemplate>
                            <div class="flow-item col-xs-12 col-sm-12">
                                <div class="col-xs-4 col-sm-4 col-left">
                                    <%#Eval("ItemName") %>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-center">
                                    <%#string.Format("{0:dd/MM/yyy hh:mm}", Eval("DateTime")) %>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-right">
                                    <a href="/admin/chat-luong/checkcsvc_done_d/<%#Eval("Id") %>.html">Chi tiết</a>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    
                </div>
            </div>
        </div>
    </div>
</body>
</html>