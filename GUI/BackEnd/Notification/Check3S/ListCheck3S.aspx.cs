﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.Check3S
{
    public partial class ListCheck3S : System.Web.UI.Page
    {
        private string PageID = "CL_DS_CHECK3S";
        protected Paging PAGING = new Paging();
        private bool Perm_Access = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        private int _StaffId = 0;
        private int _SalonId = 0;
        public static string NameItem = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            txtFromDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now));
            txtToDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now));
            SetPermission();
            Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
        }
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_ShowSalon = permissionModel.GetActionByActionNameAndPageId("Perm_ShowSalon", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                ////Perm_ViewAllDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDate", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                //ContentWrap.Visible = false;
                //NotAllowAccess.Visible = true;
            }
        }
        CultureInfo culture = new CultureInfo("vi-VN");
        protected void Views_Click(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                //DateTime FromDate = Convert.ToDateTime(txtFromDate.Text, culture);
                //DateTime ToDate = Convert.ToDateTime(txtToDate.Text, culture);
                //int SalonId =Convert.ToInt32( ddlSalon.SelectedValue);
                //var ListtingCheckList3S = (from a in db.Check3S
                //                           join b in db.ItemChecks on a.ItemId equals b.Id
                //                           join c in db.Staffs on a.StaffId equals c.Id
                //                           //where a.DateTime >= FromDate && a.DateTime <= ToDate && a.SalonId == SalonId
                //                           select new TempCheckList3S { Id = a.Id, NameItemCheck = b.Name, DateCheck = a.DateTime, NameStaff = c.Fullname, img = a.Image }).ToList();
                //if (ListtingCheckList3S.Count > 0)
                //{
                //    rtpCheckList3S.DataSource = ListtingCheckList3S;
                //    rtpCheckList3S.DataBind();
                //}

            }
        }
        // //bind Listting checklist 3S
        [WebMethod]
        public static string BindListCheckList3S(string _FromDate, string _ToDate, int _SalonId)
        {
            using (var db = new Solution_30shineEntities())
            {
                string str = "";
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                CultureInfo culture = new CultureInfo("vi-VN");
                DateTime FromDate = Convert.ToDateTime(_FromDate, culture);
                DateTime ToDate = Convert.ToDateTime(_ToDate, culture).AddDays(1);
                int SalonId = _SalonId;
                int stt = 1;
                //List<TempCheckList3S> ListtingCheckList3S = null;
                var ListtingCheckList3S = new List<TempCheckList3S>();
                if (_SalonId > 0)
                {
                    ListtingCheckList3S = (from a in db.ERP_Check3S
                                               join b in db.ERP_ItemCheck on a.ItemId equals b.Id
                                               join c in db.Staffs on a.StaffId equals c.Id
                                               where a.DateTime >= FromDate && a.DateTime < ToDate && a.SalonId == SalonId
                                               select new TempCheckList3S { Id = a.Id, NameItemCheck = b.Name, DateCheck = a.DateTime, NameStaff = c.Fullname, img = a.Image }).ToList();
                }
                else
                {
                    ListtingCheckList3S = (from a in db.ERP_Check3S
                                               join b in db.ERP_ItemCheck on a.ItemId equals b.Id
                                               join c in db.Staffs on a.StaffId equals c.Id
                                               where a.DateTime >= FromDate && a.DateTime <= ToDate
                                               select new TempCheckList3S { Id = a.Id, NameItemCheck = b.Name, DateCheck = a.DateTime, NameStaff = c.Fullname, img = a.Image }).ToList();
                }
                
                if (ListtingCheckList3S.Count > 0)
                {
                    for (int i = 0; i < ListtingCheckList3S.Count; i++)
                    {
                        str += "<tr ><td>"+ stt + "</td><td >"+ ListtingCheckList3S[i].NameItemCheck+ "</td ><td>"+ string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(ListtingCheckList3S[i].DateCheck)) + "</td> <!--<td > "+ ListtingCheckList3S[i].NameStaff+"</td >--> <td ><a class=\"views_detail_vs\" data-id=\""+ ListtingCheckList3S[i].Id+ "\" data-nameitem=\""+ ListtingCheckList3S [i].NameItemCheck+ "\"  data-toggle=\"modal\" data-target=\"#myModal\"  href=\"javascript://\" onclick=\"clickDetail($(this))\"> Xem</a></td> </tr>";
                        stt++;
                    }
                    
                }
                //return serializer.Serialize(ListtingCheckList3S);
                return str;
            }
        }
        // bind detail
        [WebMethod]
        public static string DetailCheckList(int _Id)
        {
            using (var db = new Solution_30shineEntities())
            {
                string str = "";
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                //List<TempDetailCheckList3S> objCheckList = null;
                if (_Id > 0)
                {
                    //var objCheckList = (from a in db.ERP_Check3S
                    //                    join b in db.ERP_ItemCheck on a.ItemId equals b.Id
                    //                    join c in db.Staffs on a.StaffId equals c.Id
                    //                    join d in db.Tbl_Salon on a.SalonId equals d.Id
                    //                    join f in db.Staffs on a.CreatorId equals f.Id
                    //                    where a.Id == _Id
                    //                    select new TempDetailCheckList3S { Id = a.Id, NameItemCheck = b.Name, DateCheck = a.DateTime, NameStaff = c.Fullname, img = a.Image, NameSalon = d.Name, NameIsCheck = f.Fullname }).SingleOrDefault();

                    var sql = @"select a.Id, b.Name as NameItemCheck, a.DateTime DateCheck, c.Fullname NameStaff, a.Image img, d.Name NameSalon, f.Fullname NameIsCheck 
                            from  [ERP.Check3S] a
                            left join [ERP.ItemCheck] b on a.ItemId = b.Id
                            left join Staff c on a.StaffId = c.Id
                            left join Tbl_Salon d on a.SalonId = d.Id
                            left join Staff f on a.CreatorId = f.Id
                            where a.Id = " + _Id;
                    var objCheckList = db.Database.SqlQuery<TempDetailCheckList3S>(sql).SingleOrDefault();


                    if (objCheckList != null)
                    {

                        string _DateCheck = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(objCheckList.DateCheck));
                        str += "<p> Nhân viên : " + objCheckList.NameStaff + " (bị phạt)</p>";
                        str += "<p>Salon: " + objCheckList.NameSalon + "</p>";
                        str += "<p>Ngày phạt: " + _DateCheck + "</p>";
                        str += "<p>Người tạo: " + objCheckList.NameIsCheck + "</p>";
                        str += "<img class=\"img_check\" src=\"" + objCheckList.img + "\"/>";
                    }
                }
                //return serializer.Serialize(str);
                return str;
            }

        }
    

        //format
        public static string FormatDatetime(string _Datetime)
        {
            string strDate = "";
            strDate = string.Format("{0:dd/MM/yyyy hh:mm}", _Datetime);
            return strDate;
        }
        public class TempCheckList3S
        {
            public int Id { get; set; }
            public string NameItemCheck { get; set; }
            public DateTime? DateCheck { get; set; }
            public string NameStaff { get; set; }
            public string img { get; set; }

        }
        public class TempDetailCheckList3S
        {
            public int Id { get; set; }
            public string NameItemCheck { get; set; }
            [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
            public DateTime? DateCheck { get; set; }
            public string NameStaff { get; set; }
            public string img { get; set; }
            public string NameSalon { get; set; }
            public string NameIsCheck { get; set; }
        }
    }
}