﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.Check3S
{
    public partial class CheckCSVC_Done_D : System.Web.UI.Page
    {
        private string PageID = "CL_CHECKCSVC_D_DONE";
        private bool Perm_Access = false;
        private bool Perm_ShowSalon = false;
        private int _StaffId = 0;
        private int _SalonId = 0;
        int _Code = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["Code"]))
                int.TryParse(Request.QueryString["Code"], out _Code);

            SetPermission();

            if (!IsPostBack)
            {
                BindDetail();
            }
        }
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ShowSalon = permissionModel.GetActionByActionNameAndPageId("Perm_ShowSalon", PageID, permission);
                ////Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                ////Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                ////Perm_ViewAllDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDate", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                //ContentWrap.Visible = false;
                //NotAllowAccess.Visible = true;
            }
        }
        protected void BindDetail()
        {
            using (var db = new Solution_30shineEntities())
            {
                if (_Code != 0)
                {
                    var sql = @"SELECT a.*, b.Name as ItemName, c.Fullname as StaffName, d.Fullname as CreatorName, e.Name as SalonName FROM [ERP.CheckCSVC] a
                            left Join [ERP.ItemCheck] b
                            On a.ItemId = b.Id
                            left Join Staff c
                            on a.StaffId = c.Id
                            left Join Staff d
                            on a.CreatorId = d.Id
                            left Join Tbl_Salon e
                            on a.SalonId = e.Id
                            WHERE a.id =" + _Code;
                    var list = db.Database.SqlQuery<cls_checkCSVC>(sql).ToList();
                    if (list.Count > 0)
                    {
                        ltrDanhMuc.Text = list[0].ItemName;
                        ltrSalon.Text = list[0].SalonName;
                        ltrNhanVien.Text = list[0].StaffName;
                        ltrNguoiTao.Text = list[0].CreatorName;
                        ltrNgayGio.Text = string.Format("{0:dd/MM/yyyy hh:mm}", list[0].DateTime);
                        imgDes.Src = list[0].Image;
                        ltrComment.Text = list[0].Comment;
                        ltrFixDate.Text = string.Format("{0:dd/MM/yyyy hh:mm}", list[0].FixDate);
                    }
                }
            }
        }

        
    }
}