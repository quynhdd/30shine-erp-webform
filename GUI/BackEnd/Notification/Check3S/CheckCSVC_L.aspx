﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CheckCSVC_L.aspx.cs" Inherits="_30shine.GUI.BackEnd.Check3S.CheckCSVC_L" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Check CSVC Listing</title>
    <!--jquery-->
    <script src="../../../Assets/js/jquery.v1.11.1.js"></script>
    <!-- Bootstrap 3-->
    <link href="../../../Assets/css/bootstrap/bootstrap.min.css" rel="stylesheet" />
    <link href="../../../Assets/css/bootstrap/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="../../../Assets/css/check-list.css" rel="stylesheet" />
</head>
<body>
    <div class="check-csvc">
        <div class="title background black">
            <label><a href="/" title="Trang chủ solution">Home |&nbsp</a></label>
            <label>Check List CSVC</label>
        </div>
        <div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-lg-12 col-md-12 top-tile">
                        Danh Sách CSVC Báo Hỏng
                    </div>
                    <div class="col-xs-4 col-sm-4 col-lg-4 do flow3s flow-todo active">
                        <a href="/admin/chat-luong/checkcsvc_l.html">Cần làm</a>
                        <div class="line-active"></div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-lg-4 doing flow3s flow-doing">
                         <a href="/admin/chat-luong/checkcsvc_doing_l.html">Đang làm</a>
                        <div class="line-active"></div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-lg-4 done flow3s flow-done">
                        <a href="/admin/chat-luong/checkcsvc_done_l.html">Đã xong</a>
                        <div class="line-active"></div>
                    </div>
                    <%--<div class="flow-item col-xs-12 col-sm-12">
                        <div class="col-xs-4 col-sm-4 col-left">
                            Điều hòa
                        </div>
                        <div class="col-xs-4 col-sm-4 col-center">
                            22/10/2016
                        </div>
                        <div class="col-xs-4 col-sm-4 col-right">
                            <a href="javascript://">Xác nhận</a>
                        </div>
                    </div>
                    <div class="flow-item col-xs-12 col-sm-12">
                        <div class="col-xs-4 col-sm-4 col-left">
                            Điều hòa
                        </div>
                        <div class="col-xs-4 col-sm-4 col-center">
                            22/10/2016
                        </div>
                        <div class="col-xs-4 col-sm-4 col-right">
                            <a href="javascript://">Xác nhận</a>
                        </div>
                    </div>--%>

                    <asp:Repeater runat="server" ID="rptDo">
                        <ItemTemplate>
                            <div class="flow-item col-xs-12 col-sm-12">
                                <div class="col-xs-4 col-sm-4 col-left">
                                    <%#Eval("ItemName") %>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-center">
                                    <%#string.Format("{0:dd/MM/yyy hh:mm}", Eval("DateTime")) %>
                                </div>
                                <div class="col-xs-4 col-sm-4 col-right">
                                    <a href="/admin/chat-luong/checkcsvc_d/<%#Eval("Id") %>.html">Xác nhận</a>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>

                    
                </div>
            </div>
        </div>
    </div>
    <style>
        /*.top-tile { padding: 0; text-align: center; background: #F2D600; padding: 7px 0;}
        .do { padding: 0; text-align: center; background: rgb(0, 174, 204); padding: 7px 0;}
        .doing { padding: 0; text-align: center; background: rgb(0, 174, 204); padding: 7px 0; border-left: 1px solid #221d1d; border-right: 1px solid #221d1d; cursor: pointer;}
        .done { padding: 0; }
        .flow3s { text-align: center; background: rgb(0, 174, 204); padding: 7px 0; cursor: pointer;}
        .flow3s:hover { background: #50b347;}
        .active { background: #50b347;}
        .flow-item { background: #50b347;  margin-top: 1px; padding: 0;}
        .flow-item .col-center { border-left: 1px solid #221d1d; border-right: 1px solid #221d1d; padding: 7px 5px;}
        .flow-item .col-center,.flow-item .col-left { padding: 7px 5px;}
        .flow-item .col-right { padding: 7px 5px; text-align: center;}
        .flow-item .col-right a { text-decoration: underline; color: #000;}
        .flow-item .col-center { text-align: center;}*/
    </style>
</body>
</html>
