﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="NotificationTools.aspx.cs" Inherits="_30shine.GUI.BackEnd.Notification.NotiTools.NotificationTools" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <title>Noti-tool</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="Panel1" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous" />
        <style>
            #wrapper {
                font-family: 'Times New Roman';
                width: 100% !important;
                height: 770px;
            }

            .div-body {
                margin: auto;
                width: 70%;
                padding: 10px;
            }

            .tab-content {
                border: 1px solid #808080;
                height: auto !important;
                min-height: 660px !important;
                width: 100% !important;
                margin-top: 10px;
                overflow-y: scroll;
                background: linear-gradient(silver, white, silver);
            }

            hr {
                color: white;
            }

            .st-head {
                font-size: 35px;
                padding: 10px 0 0 90px;
            }

            .filter-item {
                margin-top: 20px;
                width: 152%;
            }

            .btn-noti {
                padding: 3px 15px;
                font-size: 16px;
                font-weight: 300;
                margin-top: 10px;
                background-color: black;
                color: white;
                margin-bottom: 20px;
            }

                .btn-noti:hover {
                    color: red;
                }

                .btn-noti:before, .btn-noti:after, .btn-noti:active, .btn-noti:focus {
                    color: white !important;
                }

            .select2-container--default .select2-selection--multiple .select2-selection__choice {
                font-size: 16px;
            }

            .select2-container--default .select2-selection--multiple {
                min-height: 35px !important;
                height: auto !important;
            }

            .select2-container--default .select2-selection--single {
                min-height: 35px !important;
                padding-top: 1px !important;
            }

            .select2-container {
                width: 80% !important;
            }

            i {
                margin-bottom: 8px !important;
            }

            .fa-times {
                font-size: 20px;
                font-weight: 1000;
                color: red;
            }

                .fa-times:hover {
                    cursor: pointer;
                }

            .fa-square {
                font-size: 20px;
                font-weight: 1000;
            }

            .fa-square {
                cursor: pointer;
            }

            .i-fa-delete {
                margin-left: 5px;
                display: none;
            }

            .i-fa-select {
                margin-left: 5px;
                display: inline-block;
                -webkit-animation: my 1200ms infinite;
                -moz-animation: my 1200ms infinite;
                -o-animation: my 1200ms infinite;
                animation: my 1200ms infinite;
                font-weight:bold;
                text-decoration:underline !important;
            }

            @-webkit-keyframes my {
                0% {
                    color: white;
                }

                50% {
                    color: #808080;
                }

                100% {
                    color: #000000;
                }
            }

            @-moz-keyframes my {
                0% {
                    color: white;
                }

                50% {
                    color: #808080;
                }

                100% {
                    color: #000000;
                }
            }

            @-o-keyframes my {
                0% {
                    color: white;
                }

                50% {
                    color: #808080;
                }

                100% {
                    color: #000000;
                }
            }

            @keyframes my {
                0% {
                    color: white;
                }

                50% {
                    color: #808080;
                }

                100% {
                    color: #000000;
                }
            }
            .i-fa-select:hover {
                cursor: pointer;
                color: red;
            }
        </style>
        <style>
            .wp_table {
                overflow-y: scroll;
                width: 100%;
                float: left;
            }

            .cls_table {
                padding: 5px;
                margin: auto;
                width: 100%;
            }

            .cls_table, td, th {
                border: 1px solid black;
                border-collapse: collapse;
                padding: 5px;
                min-width: 100px;
            }

                .cls_table, td:first-child, th:first-child {
                    min-width: 30px
                }

            .h2-cls-excel {
                width: 100%;
                float: left;
                text-align: center;
                font-size: 20px;
                padding: 10px;
                text-transform: uppercase;
                margin-top: 10px;
            }

            .h1-title-timport-file-excel {
                width: 100%;
                float: left;
                font-size: 20px;
                text-transform: uppercase;
                margin-bottom: 10px;
            }

            .wp-page {
                width: 100%;
                float: left;
                padding: 20px 10px 0;
            }

            #my_file_input::-webkit-file-upload-button {
                color: red;
            }

            #my_file_input::before {
                color: red;
                background: none;
                border: none;
            }

            .wp-submit {
                width: 100%;
                float: left;
                margin: 15px 0px 20px;
                text-align: right;
            }

            #my_file_input {
                float: left;
            }

            .cls_tempfile {
                float: left;
                text-decoration: underline !important;
                color: #0277bd;
                margin-left: 30px;
                padding-top: 10px;
            }

            .container-sub {
                width: 75%;
                margin: auto;
            }

            th, td {
                text-align: center;
            }

            .wp_import-file-excel {
                float: right;
                padding-top: 20px;
            }

            tbody tr:nth-child(even) {
                background-color: #f2f2f2;
            }

            .btn-complete {
                background-color: black;
                color: white;
            }

                .btn-complete:hover {
                    background-color: black;
                    color: red;
                }

            div.box-main {
                padding-left: 0px !important;
                border-bottom: 1px solid white;
                padding-bottom: 15px;
            }

            .fa-times-circle {
                font-size: 20px;
                font-weight: 1000;
                color: red;
            }

                .fa-times-circle:hover {
                    cursor: pointer;
                }
        </style>
        <div id="wrapper">
            <div class="container">
                <div class="row">
                    <div class="div-body">
                        <ul class="nav nav-pills">
                            <li class="active"><a data-toggle="pill" href="#box-left">Gửi thông báo (Salon - Bộ phận)</a></li>
                            <li><a data-toggle="pill" href="#box-right">Gửi thông báo (Excel)</a></li>
                        </ul>
                        <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                        <div class="tab-content">
                            <div id="box-left" class="tab-pane active">
                                <hr />
                                <h1><strong class="st-head"><i class="fa fa-filter"></i>NOTIFICATION</strong></h1>
                                <hr />
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                    </div>
                                    <div class="col-md-4">
                                        <div class="filter-item">
                                            <i class="fas fa-store-alt"></i>- Danh sách Salon
                                            <asp:DropDownList multiple="multiple" ID="ddlSalon" onchange="funcDisplayDelete($(this));" CssClass="form-control js-example-basic-multiple drop-down-salon" placeholer="hihi" name="salon[]" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                            <span class="i-fa-delete icon-delete-salon"><i class="fas fa-times fa-salon" onclick="funcRemoveDropdown($(this));"></i>- Xoá</span>
                                            <a class="i-fa-select icon-all-salon fa-check-salon" onclick="funcSelectAllDropdown($(this));">Chọn tất cả Salon</a>
                                        </div>
                                        <div class="filter-item">
                                            <i class="fa fa-list"></i>- Danh sách bộ phận
                                            <asp:DropDownList multiple="multiple" ID="ddlDepartment" onchange="funcDisplayDelete($(this));" CssClass="form-control js-example-basic-multiple drop-down-department" name="department[]" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                            <span class="i-fa-delete icon-delete-department"><i class="fas fa-times fa-department" onclick="funcRemoveDropdown($(this));"></i>- Xoá</span>
                                            <a class="i-fa-select icon-all-department fa-check-department" onclick="funcSelectAllDropdown($(this));">Chọn tất cả bộ phận</a>
                                        </div>

                                        <div class="filter-item">
                                            <i class="fas fa-globe-asia"></i>- Danh sách thông báo
                                            <asp:DropDownList ID="ddlNotification" CssClass="form-control select" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                        </div>
                                        <input type="button" onclick="InsertObj();" class="btn btn-sm btn-noti" value="Send notification" />
                                    </div>
                                    <div class="col-md-4">
                                    </div>
                                </div>
                            </div>
                            <div id="box-right" class="tab-pane fade">
                                <div class="flow-item-body">
                                    <div class="wp-page">
                                        <div class="container-sub">
                                            <div class="row">
                                                <div class="col-md-12 box-main">
                                                    <div class="col-md-5">
                                                        <div class="filter-item">
                                                            <i class="fas fa-globe-asia"></i>- Danh sách thông báo
                                                                <asp:DropDownList ID="ddlNotifications" onchange="onchangeNoti();" CssClass="form-control select" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                    </div>
                                                    <div class="col-md-3" style="padding-right: 0px !important">
                                                        <div class="wp_import-file-excel">
                                                            <h1 class="h1-title-timport-file-excel">Send Notification.</h1>
                                                            <input type="file" id="my_file_input" />
                                                            <div id='my_file_output'></div>
                                                            <a href="/TemplateFile/ImportExcelNotification/huong_dan_su_dung_gui_thong_bao_bang_excel.xlsx" class="cls_tempfile">Tải File Excel Import MẪu</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <h2 class="h2-cls-excel">Gửi thông báo</h2>
                                                </div>
                                                <div class="wp_table">
                                                    <table class="cls_table">
                                                        <thead>
                                                            <tr>
                                                                <th>STT</th>
                                                                <th><span>NOTIFICATION NAME</span></th>
                                                                <th><span>USERS ID</span></th>
                                                                <th><span>SLUG KEY</span></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody id="body-content">
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="wp-submit">
                                                    <a href="javascript://" onclick="UploadData();" class="btn btn-complete">Send Notification</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            var URL_NOTI = '<%= Libraries.AppConstants.URL_API_NOTIFICATION %>';
            $(document).ready(function () {
                //var maxheight = $(window).height();
                //console.log('aba', aba);
                $('.tab-content').css('max-height', "300px");
            });
            $(document).ready(function () {
                $('.js-example-basic-multiple').select2();
                $('.select').select2();
            });

            //display icon delete
            function funcDisplayDelete(This) {
                var data = $('#ddlSalon').select2('data');
                // Push each item into an array
                if (This.hasClass("drop-down-salon")) {
                    let salonId = $("#ddlSalon :selected").val();
                    if (salonId != '' && typeof salonId != 'undefined') {
                        $('.icon-delete-salon').css({ "display": "inline-block" });
                        $('.icon-all-salon').css({ "display": "none" });
                    }
                    else {
                        $('.icon-delete-salon').css({ "display": "none" });
                        $('.icon-all-salon').css({ "display": "inline-block" });
                    }
                }
                else if (This.hasClass("drop-down-department")) {
                    let departmentId = $("#ddlDepartment :selected").val();
                    if (departmentId != '' && typeof departmentId != 'undefined') {
                        $('.icon-delete-department').css({ "display": "inline-block" });
                        $('.icon-all-department').css({ "display": "none" });
                    }
                    else {
                        $('.icon-delete-department').css({ "display": "none" });
                        $('.icon-all-department').css({ "display": "inline-block" });
                    }
                }
                var finalResult = data.map(function () {
                    return This.id;
                });
            }

            // select all
            function funcSelectAllDropdown(This) {
                if (This.hasClass("fa-check-salon")) {
                    $("#ddlSalon > option").prop("selected", "selected");
                    $("#ddlSalon").trigger("change");
                    $('.icon-delete-salon').css({ "display": "inline-block" });
                    $('.icon-all-salon').css({ "display": "none" });
                }
                if (This.hasClass("fa-check-department")) {
                    $("#ddlDepartment > option").prop("selected", "selected");
                    $("#ddlDepartment").trigger("change");
                    $('.icon-delete-department').css({ "display": "inline-block" });
                    $('.icon-all-department').css({ "display": "none" });
                }
            }

            //delete all select
            function funcRemoveDropdown(This) {
                let $selectSalon = $('#ddlSalon');
                let $selectDepartment = $('#ddlDepartment');
                if (This.hasClass("fa-salon")) {
                    $selectSalon.val('').trigger('change.select2');
                    $('.icon-delete-salon').css({ "display": "none" });
                    $('.icon-all-salon').css({ "display": "inline-block" });
                }
                else if (This.hasClass("fa-department")) {
                    $selectDepartment.val('').trigger('change.select2');
                    $('.icon-delete-department').css({ "display": "none" });
                    $('.icon-all-department').css({ "display": "inline-block" });
                }
            }

            // func click luu data
            function InsertObj() {
                var salonValues = [];
                var departmentValues = [];
                $("#ddlSalon :selected").each(function () {
                    salonValues.push($(this).val());
                });
                $("#ddlDepartment :selected").each(function () {
                    departmentValues.push($(this).val());
                });
                let strSalon = salonValues.toString();
                let strDepartment = departmentValues.toString();
                let notiId = $("#ddlNotification :selected").val();
                if (strSalon === '' || typeof strSalon === 'undefined') {
                    showMsgSystem('...vui lòng chọn salon...', 'msg-system warning', 3000);
                }
                else if (strDepartment === '' || typeof strDepartment === 'undefined') {
                    showMsgSystem('...vui lòng chọn bộ phận...', 'msg-system warning', 3000);
                }
                else if (notiId === '' || typeof notiId === 'undefined' || notiId === '0') {
                    showMsgSystem('...vui lòng chọn thông báo...', 'msg-system warning', 3000);
                }
                else {
                    startLoading();
                    var dataJson = {
                        SalonId: strSalon,
                        DepartmentId: strDepartment,
                        NotiId: parseInt(notiId)
                    };
                    $.ajax({
                        type: "POST",
                        url: URL_NOTI + '/api/notification-users',
                        data: JSON.stringify(dataJson),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            finishLoading();
                            if (response === 'Success!') {
                                showMsgSystem('Gửi thông báo thành công...', 'msg-system success', 5000);
                                $('#ddlSalon').val('').trigger('change.select2');
                                $('.icon-delete-salon').css({ "display": "none" });
                                $('.icon-all-salon').css({ "display": "inline-block" });
                                $('#ddlDepartment').val('').trigger('change.select2');
                                $('.icon-delete-department').css({ "display": "none" });
                                $('.icon-all-department').css({ "display": "inline-block" });
                                $('#ddlNotification').val('0').trigger('change.select2');
                            }
                            else {
                                showMsgSystem('Gửi thông báo thất bại...', 'msg-system warning', 5000);
                                $('#ddlSalon').val('').trigger('change.select2');
                                $('.icon-delete-salon').css({ "display": "none" });
                                $('.icon-all-salon').css({ "display": "inline-block" });
                                $('#ddlDepartment').val('').trigger('change.select2');
                                $('.icon-delete-department').css({ "display": "none" });
                                $('.icon-all-department').css({ "display": "inline-block" });
                                $('#ddlNotification').val('0').trigger('change.select2');
                            }
                        }
                    });
                }
            }
        </script>
        <script>
            var URL_NOTIFICATION = "<%= Libraries.AppConstants.URL_API_NOTIFICATION %>";
            var objListNoti = {};
            var oFileIn;
            $(document).ready(function () {
                //var maxheight = $(window).height();
                //console.log('aba', aba);
                $('.wp_table').css('max-height', "400px");
            });
            $(function () {
                oFileIn = document.getElementById('my_file_input');
                if (oFileIn.addEventListener) {
                    oFileIn.addEventListener('change', filePicked, false);
                }
            });

            // func get data
            function filePicked(oEvent) {
                let notiId = $("#ddlNotifications :selected").val();
                if (notiId != '' && notiId != '0') {
                    // Get The File From The Input
                    var oFile = oEvent.target.files[0];
                    var sFilename = oFile.name;
                    // Create A File Reader HTML5
                    var reader = new FileReader();
                    // Ready The Event For When A File Gets Selected
                    reader.onload = function (e) {
                        var data = e.target.result;
                        var cfb = XLSX.read(data, { type: 'binary' });
                        // Here is your object
                        var XL_row_object = XLSX.utils.sheet_to_row_object_array(cfb.Sheets["Sheet1"]);
                        var obj = XLSX.utils.sheet_to_row_object_array()
                        objListNoti = XL_row_object;
                        console.log("XL_row_object", XL_row_object);
                        // append html 
                        if (objListNoti.length > 0) {
                            appendData(objListNoti);
                        }
                        else {
                            $("#body-content").html('');
                            objListNoti = {};
                            let msg = 'Vui lòng kiểm tra lại file excel';
                            let status = 'msg-system warning';
                            let duration = 2000;
                            showMsgSystem(msg, status, duration);
                        }
                    };
                    // Tell JS To Start Reading The File.. You could delay this if desired
                    reader.readAsBinaryString(oFile);
                }
                else {
                    let msg = 'Vui lòng chọn thông báo...';
                    let status = 'msg-system warning';
                    let duration = 2000;
                    showMsgSystem(msg, status, duration);
                    $("#my_file_input").val('');
                }
            }
            // func apeend html
            function appendData(objData) {
                var str = "";
                var Total = 0;
                $("#body-content").html('');
                const Count = objData.length;
                var notiName = $("#ddlNotifications option:selected").text();
                var stt = 0;
                if (Count > 0) {
                    for (var i = 0; i < Count; i++) {
                        Total = Total + parseFloat(objData[i].ElectricityAndWaterBill);
                        stt++
                        str += '<tr> ' +
                            //stt
                            '<td>' +
                            stt +
                            ' </td>' +
                            // date
                            '<td>' +
                            notiName +
                            ' </td>' +
                            '<td>' +
                            objData[i].UserId +
                            ' </td>' +
                            '<td>' +
                            objData[i].Slugkey +
                            ' </td>' +
                            '</tr>';
                    }
                    if (str != '') {
                        $("#body-content").append(str);
                    }
                }
            }
            // func click luu data
            function UploadData() {
                debugger;
                if (objListNoti.length > 0) {
                    startLoading();
                    let notiId = $("#ddlNotifications option:selected").val();
                    var dataJson = {
                        NotiId: parseInt(notiId),
                        data: objListNoti
                    };
                    console.log(dataJson);
                    $.ajax({
                        type: "POST",
                        url: URL_NOTIFICATION + '/api/notification-users/import-file-excel',
                        data: JSON.stringify(dataJson),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            finishLoading();
                            if (response === 'Success!') {
                                showMsgSystem('Gửi thông báo thành công...', 'msg-system success', 3000);
                                $("#body-content").html('');
                                objListNoti = {};
                                $("#my_file_input").val('');
                                $('#ddlNotifications').val('0').trigger('change.select2');
                            }
                            else {
                                showMsgSystem('Gửi thông báo thất bại...', 'msg-system warning', 3000);
                                $("#body-content").html('');
                                objListNoti = {};
                                $("#my_file_input").val('');
                                $('#ddlNotifications').val('0').trigger('change.select2');
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var responseText = jqXHR.responseJSON.message;
                            objListNoti = {};
                            showMsgSystem('Có lỗi xảy ra vui lòng liên hệ nhà phát triển...', 'msg-system warning', 3000);
                            $("#body-content").html('');
                            $("#my_file_input").val('');
                            $('#ddlNotifications').val('0').trigger('change.select2');
                            finishLoading();
                        }
                    });
                }
            };

            //onchange noti
            function onchangeNoti() {
                $("#body-content").html('');
                objListNoti = {};
                $("#my_file_input").val('');
            }
        </script>
        <script src="/Assets/js/libs.xlsx/libs-xlsx-0.8.0-jszip.js"></script>
        <script src="/Assets/js/libs.xlsx/libs-xlsx-0.8.0-xlsx.js"></script>
        <!-- Page loading -->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
    </asp:Panel>
</asp:Content>
