﻿using _30shine.Helpers;
using _30shine.Helpers.Http;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using Libraries;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.Notification.NotiTools
{
    public partial class NotificationTools : System.Web.UI.Page
    {
        public CultureInfo culture = new CultureInfo("vi-VN");
        public bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected bool isRoot = false;
        protected int departmentId;
        public double? salaryTotal = 0;
        protected string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
        protected string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);

        /// <summary>
        /// set permission
        /// </summary>
        public void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        /// <summary>
        /// ExecuteByPermission
        /// </summary>
        private void ExecuteByPermission()
        {

            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                Library.Function.bindSalon_NotHoiQuanSpecial(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
                bindDepartment();
                bindNotification();
            }
        }

        /// <summary>
        /// Load department
        /// </summary>
        private void bindDepartment()
        {
            using (var db = new Solution_30shineEntities())
            {
                var department = db.Staff_Type.Where(w => w.IsDelete == 0 && w.Publish == true).ToList();
                var ddls = new List<DropDownList> { ddlDepartment };
                for (var i = 0; i < ddls.Count; i++)
                {
                    ddls[i].DataTextField = "Name";
                    ddls[i].DataValueField = "Id";
                    ddls[i].DataSource = department;
                    ddls[i].DataBind();
                }
            }
        }

        /// <summary>
        /// Load department
        /// </summary>
        private void bindNotification()
        {
            using (var db = new Solution_30shineEntities())
            {
                var noti = db.NotificationManagements.Where(w => w.IsDelete == false && w.IsPublish == true).ToList();
                var record = new NotificationManagement();
                record.Id = 0;
                record.Name = "Chọn thông báo";
                noti.Insert(0, record);
                ddlNotification.DataTextField = "Name";
                ddlNotification.DataValueField = "Id";
                ddlNotification.DataSource = noti;
                ddlNotification.DataBind();
                ddlNotification.SelectedIndex = 0;
                ddlNotifications.DataTextField = "Name";
                ddlNotifications.DataValueField = "Id";
                ddlNotifications.DataSource = noti;
                ddlNotifications.DataBind();
                ddlNotifications.SelectedIndex = 0;
            }
        }

        public partial class InputNotification
        {
            public string salonId { get; set; }
            public string departmentId { get; set; }
            public int notiId { get; set; }
        }
    }
}