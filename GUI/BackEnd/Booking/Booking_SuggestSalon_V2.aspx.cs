﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Web.Services;
using System.Data.Entity.Migrations;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.UIBooking
{
    public partial class Booking_SuggestSalon_V2 : System.Web.UI.Page
    {
        protected string PageID = "BK_DS_GHTDL";
        protected bool Perm_Access = false;
        protected bool Perm_Add = false;
        protected bool Perm_Edit = false;
        protected bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;

        protected Paging PAGING = new Paging();
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
             
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
                bindData();
            }
        }
       
        /// <summary>
        /// Bind danh sách học viên Stylist4Men
        /// </summary>
        // Lấy data
        protected void bindData()
        {
            using (var db = new Solution_30shineEntities())
            {
                DateTime _StartDate = DateTime.Now;
                DateTime _EndDate = _StartDate.AddDays(1);
                int _SalonId = Convert.ToInt32(ddlSalon.SelectedValue);
                var data = db.Store_Booking_SuggestSalon_CustomerListing(String.Format("{0:yyyy/MM/dd}", _StartDate), String.Format("{0:yyyy/MM/dd}", _EndDate), _SalonId).ToList();
                Bind_Paging(data.Count);
                Rpt.DataSource = data.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                Rpt.DataBind();
            }
        }

        /// <summary>
        /// Set trạng thái đã gọi điện cho khách chưa
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="IsCall"></param>
        /// <returns></returns>
        [WebMethod]
        public static object checkIsCall(int Id, bool IsCall)
        {
            using (var db = new Solution_30shineEntities())
            {
                var message = new Library.Class.cls_message();
                var error = 0;
                var record = db.Booking_Suggest.FirstOrDefault(w=>w.Id == Id);
                if (record != null)
                {
                    record.IsCall = IsCall;
                    record.ModifiedTime = DateTime.Now;
                    db.Booking_Suggest.AddOrUpdate(record);
                    error += db.SaveChanges() > 0 ? 0 : 1;

                    if (error == 0)
                    {
                        message.success = true;
                        message.message = "Successed!";
                    }
                    else
                    {
                        message.success = false;
                        message.message = "Failed!";
                    }
                }
                else
                {
                    message.success = false;
                    message.message = "Error! Record not found!";
                }

                return message;
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            bindData();
            RemoveLoading();
        }

        protected void Bind_Paging(int totalRecord)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._Segment = 10000;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(totalRecord) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int totalRecord)
        {
            using (var db = new Solution_30shineEntities())
            {
                int TotalRow = totalRecord - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

       

        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }
    }
}