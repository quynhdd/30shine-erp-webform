﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="BookHour.aspx.cs" Inherits="_30shine.GUI.BackEnd.UIBooking._BookHour" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Cấu hình thời gian đặt lịch
    </title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
         <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
            <script src="../../../Assets/js/select2/select2.min.js"></script>
        <style>
            .table-add {
                margin-top: 25px;
            }

            .input-inline-wp input {
                text-align: center;
            }
            .select2-container{
                 width:210px !important;
            }
            .wp_salonId_ddl{
               padding-top: 5px !important;
            }
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý cấu hình Booking &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/admin/booking/hour.html">Danh sách khung giờ đặt lịch</a></li>
                        <li class="li-add"><a href="/admin/booking/them-moi.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing be-report config-salary">
            <%-- Listing --%>
            <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
            <div class="wp960 content-wp">
                <div class="row">
                    <strong class="st-head" style="margin-bottom: -20px;margin-right: 10px;"><i class="fa fa-file-text"></i>Danh sách cấu hình khung giờ đặt lịch</strong>
                    <div class="wp_salonId_ddl">
                        <asp:DropDownList
                            ID="ddlSalon" ClientIDMode="Static" runat="server" CssClass="form-control select" OnTextChanged="ddlSalon_TextChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="wp960 content-wp">
                <div class="wp customer-add customer-listing">
                    <asp:UpdatePanel ID="UP01" runat="server">
                        <ContentTemplate>
                            <table class="table-add table-listing table-config-salary">
                                <thead>
                                    <tr>
                                        <th>STT
                                        </th>
                                        <th>Tên khung giờ
                                        </th>
                                        <th>Số lượng
                                        </th>
                                        <th>Publish
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptBookHour" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <%#Container.ItemIndex +1%>
                                                </td>

                                                <td>
                                                    <%#Eval("Hour") %>
                                                </td>
                                                <td>
                                                    <div class="input-inline-wp">
                                                        <%--<asp:TextBox ID="txtSlot" runat="server" ClientIDMode="Static" Text='<%#Eval("Slot") %>'>  </asp:TextBox>--%>
                                                        <input id="txtSlot" type="number" min="0" max="100" step="1" value="<%#Eval("Slot") %>"
                                                            onchange="checkSlotBook($(this), <%# Eval("Id") %>, '<%# Eval("Hour") %>')" />
                                                    </div>
                                                </td>
                                                <td class="map-edit">
                                                    <input type="checkbox" <%# Convert.ToInt32(Eval("Publish")) == 1 ? "Checked=\"True\"" : "" %>
                                                        onchange="checkPublish($(this), <%# Eval("Id") %>, '<%# Eval("Hour") %>')" />
                                                    <div class="edit-wp">
                                                        <a class="elm edit-btn" href="/admin/booking/sua-khung-gio-<%#Eval("Id") %>.html" title="Sửa"></a>
                                                        <%-- <a class="elm del-btn" href="javascript://" title="Xóa"></a>--%>
                                                        <a class="elm del-btn" onclick="del(this.parentNode.parentNode.parentNode,'<%# Eval("Id") %>', '<%# Eval("Hour") %>')" href="javascript://" title="Xóa"></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger EventName="TextChanged" ControlID="ddlSalon" />
                        </Triggers>
                    </asp:UpdatePanel>

                </div>
            </div>

            <%-- END Listing --%>
            <!-- Notification box -->
            <div class="notification-box">Thành công</div>
            <script>
                $('.select').select2();
            </script>
            <!--// Notification box -->
            <script>
                //Update Publish BookHour
                function checkPublish(This, HourId, HourName, _chkPublish) {
                    var _chkPublish = This.is(":checked");
                    console.log("Id" + HourId + "_chk" + _chkPublish);
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/Booking/BookHour.aspx/UpdatePublish",
                        data: '{HourId : "' + HourId + '", _chkPublish : "' + _chkPublish + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            var mission = JSON.parse(response.d);
                            if (mission.success) {
                                //alert("Đã điểm danh cho [ " + staffName + " ]");
                                if (_chkPublish == false)
                                    showNotification("Đã ẩn cho khung giờ [ " + HourName + " ]");
                                else
                                    showNotification("Đã hiển thị cho khung giờ [ " + HourName + " ]");
                            } else {
                                alert("Có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển.");
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    });
                }

                //Update Slot Hour
                function checkSlotBook(This, HourId, HourName, Slot) {
                    var _Slot = This.val();
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/Booking/BookHour.aspx/UpdateSlot",
                        data: '{HourId : "' + HourId + '" , Slot : "' + _Slot + '" }',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            var mission = JSON.parse(response.d);
                            if (mission.success) {
                                //showNotification("Đã nhập part-time cho [ " + staffName + " ]");
                                showNotification("Đã cập nhập thành công cho khung giờ:  [" + HourName + "] ");
                            } else {
                                alert("Có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển.");
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    });
                }

                function del(This, IdHour, HourName) {
                    var IdHour = IdHour || null,
                        HourName = HourName || null,
                        Row = This;
                    if (!IdHour) return false;

                    // show EBPopup
                    $(".confirm-yn").openEBPopup();
                    $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa khung giờ [ " + HourName + " ] ?");

                    $("#EBPopup .yn-yes").bind("click", function () {
                        $.ajax({
                            type: "POST",
                            url: "/GUI/BackEnd/Booking/BookHour.aspx/Delele_BookHour",
                            data: '{IdHour : "' + IdHour + '"}',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json", success: function (response) {
                                var mission = JSON.parse(response.d);
                                if (mission.success) {
                                    delSuccess();
                                    Row.remove();
                                } else {
                                    delFailed();
                                }
                            },
                            failure: function (response) { alert(response.d); }
                        });
                    });
                    $("#EBPopup .yn-no").bind("click", function () {
                        autoCloseEBPopup(0);
                    });
                }

                function showNotification(content) {
                    $(".notification-box").text(content).fadeIn(function () {
                        setTimeout(function () {
                            $(".notification-box").fadeOut();
                        }, 2000);
                    });
                }
                //jQuery(document).ready(function () {
                //    // Check Slot BookHour

                //});
            </script>
        </div>
    </asp:Panel>
</asp:Content>
