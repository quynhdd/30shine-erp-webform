﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="AddOrUpdateBooHour.aspx.cs" Inherits="_30shine.GUI.BackEnd.UIBooking.AddOrUpdateBooHour" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
    <script src="../../../Assets/js/select2/select2.min.js"></script>
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .msg-system.success.failed { background: Orange; }
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý - Cấu hình đặt lịch &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/admin/booking/hour.html">Danh sách</a></li>
                        <li class="li-add"><a href="/admin/booking/them-moi.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add admin-product-add admin-service-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <!-- System Message -->
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <!-- END System Message -->

                <div class="table-wp">
                    <table class="table-add admin-product-table-add admin-service-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Thông tin giờ đặt lịch</strong></td>
                                <td></td>
                            </tr>
                            <tr class="tr-margin" style="height: 20px;"></tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Nhập khung giờ</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="txtHour"  CssClass="form-control" Width="70%" runat="server" ClientIDMode="Static" placeholder="8h:30"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="NameValidate" ControlToValidate="txtHour" runat="server" CssClass="fb-cover-error  form-control" Text="Bạn chưa nhập tên giờ!"></asp:RequiredFieldValidator>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Thời gian thực trong ngày</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="txtHourFrame" runat="server"  CssClass="form-control"  Width="70%" ClientIDMode="Static" placeholder="Ví dụ : 08:00:00"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="CodeValidate" ControlToValidate="txtHourFrame" runat="server" CssClass="fb-cover-error  form-control" Text="Bạn chưa nhập thời gian thực trong ngày !"></asp:RequiredFieldValidator>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-2 left"><span>Số lượng chỗ đặt</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="txtSlot" runat="server" CssClass="form-control" ClientIDMode="Static" Width="40%" placeholder="Ví dụ : 2"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="ValidatePrice" ControlToValidate="txtSlot" CssClass="fb-cover-error" Text="Bạn chưa nhập số lượng !" runat="server"></asp:RequiredFieldValidator>
                                        <asp:RangeValidator CssClass="fb-cover-error" runat="server" Type="Integer"
                                            MinimumValue="0" MaximumValue="400" ControlToValidate="txtSlot"
                                            ErrorMessage=" Bạn  phải nhập số từ 0 - 400 " />
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-2 left"><span>Salon</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:DropDownList Style="cursor: pointer;" CssClass="select form-control" Width="40%" ID="ddlSalon" runat="server"></asp:DropDownList>
                                        <asp:RequiredFieldValidator InitialValue="0" CssClass="fb-cover-error"
                                            ID="rfvDDL_Product" Display="Dynamic"
                                            ControlToValidate="ddlSalon"
                                            runat="server" 
                                            ErrorMessage="Bạn chưa chọn Salon"
                                            ForeColor="Red">
                                        </asp:RequiredFieldValidator>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-product-category">
                                <td class="col-xs-2 left"><span>Publish</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:CheckBox ID="chkPublish" Checked="true" runat="server" />
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-send">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <asp:Button ID="btnComplete" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="btnComplete_Click"></asp:Button>
                                        <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>
                                    </span>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>
            <%-- end Add --%>
        </div>
        <script>
            jQuery(document).ready(function ($) {
                $('.select').select2();
                //============================
                // Show System Message
                //============================ 
                var qs = getQueryStrings();
                showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);
            });
        </script>
    </asp:Panel>
</asp:Content>
