﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Custumers_Booking_Check_V2.aspx.cs" Inherits="_30shine.GUI._Booking.Custumers_Booking_Check_V2" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Thống kê số lượng slot</title>
    <style>
        .be-report table.table-listing td.cls_danger_book {
            background-color: #ff5100;
        }

        table.table-total-report-1 {
            border-collapse: collapse;
        }

            table.table-total-report-1,
            table.table-total-report-1 th,
            table.table-total-report-1 td {
                border: 1px solid black;
            }

                table.table-total-report-1 td {
                    padding: 5px;
                    text-align: right;
                }

                    table.table-total-report-1 td:first-child {
                        text-align: left;
                    }

        .billx2 tr,
        .billx2 td {
            background: yellow !important;
        }

        .is-booking {
            background: #bbff99;
        }

        .is-normal {
            background: #ff8080;
        }

        .is-paybycard {
            background: yellow;
        }

        ._p2 {
            float: left;
        }

        .p-note {
            font-family: Roboto Condensed Regular !important;
            margin-left: 7px;
            margin-right: 15px;
            padding-bottom: 5px;
        }

            .p-note span {
                width: 10px;
                height: 10px;
                display: block;
                float: left;
                position: relative;
                top: 5px;
                margin-right: 5px;
                border: 1px solid #000000;
            }

        .customer-listing .table-func-panel {
            margin-top: -25px;
            top: 20px;
        }

        .customer-listing .tag-wp {
            padding-left: 20px;
            margin-top: 8px;
        }

        .select2-container {
            width: 180px !important;
            margin-top: 5px !important;
        }

        .select2-container--default .select2-selection--single {
            height: 32px !important;
            padding-top: 1px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Thống kê </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing be-report be-report-timekeeping">
            <%-- Listing --%>
            <div class="wp960 content-wp">

                <div class="row">
                    <div class="filter-item">
                        <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="time-wp">
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="txtStartDates" placeholder="Từ ngày" Style="margin-left: 10px!important"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ValidateTime" ControlToValidate="txtStartDates" runat="server" CssClass="fb-cover-error" Text="Vui lòng chọn thời gian!"></asp:RequiredFieldValidator>
                        </div>
                        <strong class="st-head" style="margin-left: 10px;">
                            <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                        </strong>
                        <asp:TextBox CssClass="txtDateTime st-head  form-control" ID="txtEndDate" placeholder="Đến ngày" Style="margin-left: 10px!important"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                        <br />
                    </div>
                    <div style="margin-top: -20px;">
                        <% if (Perm_ViewAllData)
                            { %>
                        <br />
                        <% }
                            else
                            { %>
                        <style>
                            .datepicker-wp {
                                display: none;
                            }

                            .customer-listing .tag-wp {
                                padding-left: 0;
                                padding-top: 5px;
                            }
                        </style>
                        <% } %>
                        <div class="filter-item">
                            <asp:DropDownList ID="Salon" CssClass="select form-control" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px;"></asp:DropDownList>
                        </div>
                        <div class="tag-wp" style="margin-left: -30px; display: none;">
                            <p class="tag tag-date" onclick="viewDataByDate($(this), '<%=Day1 %>')">Hôm kia <span class="txt-date"><%=Day1 %></span></p>
                            <p class="tag tag-date" onclick="viewDataByDate($(this), '<%=Day2 %>')">Hôm qua<span class="txt-date"><%=Day2 %></span></p>
                            <p class="tag tag-date tag-date-today" onclick="viewDataByDate($(this), '<%=Day3 %>')">Hôm nay<span class="txt-date"><%=Day3 %></span></p>
                        </div>
                        <a id="ViewData" onclick="excPaging(1)" class="st-head btn-viewdata " href="javascript:void(0)">Xem dữ liệu
                        </a>
                    </div>
                </div>

                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>Danh sách Booking</strong>
                </div>
                <div class="row">
                    <asp:ScriptManager runat="server" ID="ScriptManager2"></asp:ScriptManager>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" ClientIDMode="Static">
                        <ContentTemplate>
                            <% if (Perm_ViewAllData)
                                { %>
                            <div class="row">
                                <table class="table-total-report-1" style="float: left; margin-left: 20px;">
                                    <tbody>
                                        <tr>
                                            <td>Số lượng khách đã gọi</td>
                                            <td><span style="font-family: Roboto Condensed Bold;">
                                                <%= String.Format("{0:#,###}", isCall).Replace(",", ".") %>
                                            </span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="table-total-report-1" style="float: left; margin-left: 30px;">
                                    <tbody>
                                        <tr>
                                            <td>Khách xác nhận đến</td>
                                            <td><span style="font-family: Roboto Condensed Bold;">
                                                <%= String.Format("{0:#,###}", isCome).Replace(",", ".") %>
                                            </span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="table-total-report-1" style="float: left; margin-left: 30px;">
                                    <tbody>
                                        <tr>
                                            <td>Khách hủy và đã SMS</td>
                                            <td><span style="font-family: Roboto Condensed Bold;">
                                                <%= String.Format("{0:#,###}", isCancel).Replace(",", ".") %>
                                            </span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="table-total-report-1" style="float: left; margin-left: 30px;">
                                    <tbody>
                                        <tr>
                                            <td>Không liên lạc được</td>
                                            <td><span style="font-family: Roboto Condensed Bold;">
                                                <%= String.Format("{0:#,###}", isNotContact).Replace(",", ".") %>
                                            </span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="table-total-report-1" style="float: left; margin-left: 30px;">
                                    <tbody>
                                        <tr>
                                            <td>Khách chuyển lịch</td>
                                            <td><span style="font-family: Roboto Condensed Bold;">
                                                <%= String.Format("{0:#,###}", isTurn).Replace(",", ".") %>
                                            </span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <%} %>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div class="wp customer-add customer-listing be-report">
                    <%-- Listing --%>
                    <div class="wp960 content-wp">
                        <!-- Filter -->
                        <!-- End Filter -->
                        <!-- Row Table Filter -->
                        <div class="table-func-panel" style="margin-bottom: 40px;">
                            <div class="table-func-elm">
                                <span>Số hàng / Page : </span>
                                <div class="table-func-input-wp">
                                    <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                                    <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                                    <ul class="ul-opt-segment">
                                        <li data-value="10">10</li>
                                        <li data-value="20">20</li>
                                        <li data-value="30">30</li>
                                        <li data-value="40">40</li>
                                        <li data-value="50">50</li>
                                        <li data-value="1000000">Tất cả</li>
                                    </ul>
                                    <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                                </div>
                            </div>
                        </div>
                        <!-- End Row Table Filter -->
                        <%--<asp:ScriptManager runat="server" ID="ScriptManager1"></asp:ScriptManager>--%>
                        <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                            <ContentTemplate>
                                <div class="table-wp">
                                    <table class="table-add table-listing" id="tablebook">
                                        <thead>
                                            <tr>
                                                <th>STT</th>
                                                <th>Khách hàng</th>
                                                <th>Số điện thoại</th>
                                                <th>Khung giờ</th>
                                                <th>Trạng thái gọi khách</th>
                                                <th>Ghi chú</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="RptBooking" runat="server" OnItemDataBound="RptBooking_ItemDataBound">
                                                <ItemTemplate>
                                                    <tr id="trHeader">
                                                        <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                        <td class="<%#Eval("colorCheck") %>">
                                                            <asp:Label ID="LabelName" runat="server"><%#Eval("CustomerName")%></asp:Label></td>
                                                        <td>
                                                            <asp:Label ID="LabelPhone" runat="server"><%#Eval("CustomerPhone")%></asp:Label></td>
                                                        <td>
                                                            <asp:Label ID="LabelHourFrame" runat="server"><%#Eval("HourFrame")%></asp:Label></td>
                                                        <td style="width: 300px;">
                                                            <input <%#Eval("blockCheck") %> class="chkIsCall" onclick="updateIsCall($(this), <%# Eval("BookID") %>)" data-id="<%# Eval("BookID") %>" type="checkbox" style="float: left;" <%# Convert.ToInt32(Eval("IsCall")) == 1 ? "Checked='checked'" : "" %> />
                                                            <select <%#Eval("blockCheck") %> onchange="updateStatus($(this), <%# Eval("BookID") %>)" class="ddrlclient" style="text-align: center; width: 200px; border-radius: 3px;" id="ddlStatus">
                                                                <option value="0">Trạng thái</option>
                                                                <option value="1" <%# Convert.ToInt32(Eval("Status")) == 1 ? "selected='selected'" : "" %>>Khách xác nhận đến</option>
                                                                <option value="2" <%# Convert.ToInt32(Eval("Status")) == 2 ? "selected='selected'" : "" %>>Khách hủy và đã sms</option>
                                                                <option value="3" <%# Convert.ToInt32(Eval("Status")) == 3 ? "selected='selected'" : "" %>>Không liên lạc được</v>
                                                                <option value="4" <%# Convert.ToInt32(Eval("Status")) == 4 ? "selected='selected'" : "" %>>Khách chuyển lịch</option>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <textarea class="note" onchange="updateNote($(this), <%# Eval("BookID") %>)"><%# Eval("Note") %></textarea>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- Paging -->
                                <div class="site-paging-wp">
                                    <% if (PAGING.TotalPage > 1)
                                        { %>
                                    <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                        <% if (PAGING._Paging.Prev != 0)
                                            { %>
                                        <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                        <% } %>
                                        <asp:Repeater ID="RptPaging" runat="server">
                                            <ItemTemplate>
                                                <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                                    <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                                    <%# Eval("PageNum") %>
                                                </a>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                            { %>
                                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                        <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                        <% } %>
                                    </asp:Panel>
                                    <% } %>
                                </div>
                                <!-- End Paging -->
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                    </div>
                    <%-- END Listing --%>
                </div>
            </div>
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <script type="text/ecmascript">
            jQuery(document).ready(function () {
                $('#ddlStatus').prop('disabled', 'disabled');
                // Add active menu
                $("#glbAdminSales").addClass("active");
                $("#glbAdminReportProduct").addClass("active");
                $("li.be-report-li").addClass("active");

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });

                // Load data default
                $("#ViewData").click();
            });

            function viewDataByDate(This, time) {
                $(".tag-date.active").removeClass("active");
                This.addClass("active");
                $("#TxtDateTimeTo").val("");
                $("#TxtDateTimeFrom").val(time);
                $("#ViewData").click();
            }

            var paramClass = function () {
                this.bookID = 0;
                this.isCall = false;
                this.status = 0;
                this.note = "";
            }
            var paramObject = new paramClass();

            /*
            * Cập nhật trạng thái IsCall : đã gọi điện cho khách
            */
            function updateIsCall(This, bookID) {
                debugger;
                paramObject.bookID = bookID;
                paramObject.isCall = This.prop("checked");
                paramObject.status = This.parent().find("select.ddrlclient").val();
                paramObject.note = This.parent().parent().find("textarea.note").val();
                if (this.isCall)
                    isCall = 1;
                else
                    isCall = 0;
                addLoading();
                $.ajax({
                    type: 'POST',
                    url: '/GUI/BackEnd/Booking/Custumers_Booking_Check_V2.aspx/checkIsCall',
                    data: "{ bookID : " + paramObject.bookID + ", isCall : " + paramObject.isCall + ", status : " + paramObject.status + ", note : '" + paramObject.note + "' }",
                    contentType: 'application/json',
                }).success(function (response) {
                    if (response.d != null) {
                        alert("Cập nhật thành công!");
                    }
                    else {
                        alert("Có lỗi xảy ra . Vui lòng liên hệ với nhóm phát triển !");
                        if (isCall == 1) {
                            $('.chkisDelete[data-id="' + paramObject.bookID + '"]').prop('checked', true);
                        }
                        else {
                            $('.chkisDelete[data-id="' + paramObject.bookID + '"]').prop('checked', false);
                        }
                    }
                    removeLoading();
                });
            }

            /*
            * Cập nhật trạng thái Status : xác nhận phản hồi của khách
            */
            function updateStatus(This, bookID) {
                paramObject.bookID = bookID;
                paramObject.status = This.val();
                paramObject.isCall = This.parent().find("input[type='checkbox']").prop("checked");
                paramObject.note = This.parent().parent().find("textarea.note").val();

                addLoading();
                $.ajax({
                    type: 'POST',
                    url: '/GUI/BackEnd/Booking/Custumers_Booking_Check_V2.aspx/checkIsCall',
                    data: "{ bookID : " + paramObject.bookID + ", isCall : " + paramObject.isCall + ", status : " + paramObject.status + ", note : '" + paramObject.note + "' }",
                    contentType: 'application/json',
                }).success(function (response) {
                    if (response.d != null) {
                        alert("Cập nhật thành công!");
                    }
                    else {
                        alert("Có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển.");
                    }
                    removeLoading();
                });
            }

            /*
            * Cập nhật ghi chú
            */
            function updateNote(This, bookID) {
                paramObject.bookID = bookID;
                paramObject.status = This.parent().parent().find("select.ddrlclient").val();
                paramObject.isCall = This.parent().parent().find("input[type='checkbox']").prop("checked");
                paramObject.note = This.val();
                addLoading();
                $.ajax({
                    type: 'POST',
                    url: '/GUI/BackEnd/Booking/Custumers_Booking_Check_V2.aspx/checkIsCall',
                    data: "{ bookID : " + paramObject.bookID + ", isCall : " + paramObject.isCall + ", status : " + paramObject.status + ",  note : '" + paramObject.note + "' }",
                    contentType: 'application/json',
                }).success(function (response) {
                    if (response.d != null) {
                        alert("Cập nhật thành công!");
                    }
                    else {
                        alert("Có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển.");
                    }
                    removeLoading();
                });
            }

        </script>
    </asp:Panel>
</asp:Content>
