﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using Project.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.UIBooking
{
    public partial class StatisticBookingSlot : System.Web.UI.Page
    {
        private string PageID = "BK_TK";
        private bool Perm_Access = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {

                txtStartDate.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
            }
        }
        /// <summary>
        /// Set Permission for Page
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_ViewCol = permissionModel.GetActionByActionNameAndPageId("Perm_ViewCol", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        /// <summary>
        /// Get Data bindding to RptBooking 
        /// </summary>
        #region[Get Data bindding to RptBooking ]
        private void getData()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {

                    var _startDate = Library.Format.getDateTimeFromString(txtStartDate.Text);
                    if (_startDate == null)
                    {
                        throw new Exception("Vui lòng chọn ngày bắt đầu");
                    }
                    var _endDate = Library.Format.getDateTimeFromString(txtEndDate.Text);
                    if (_endDate == null)
                    {
                        _endDate = _startDate;
                    }

                    int _salonId = Convert.ToInt32(ddlSalon.SelectedValue);
                    var listTotalCheckin = db.Store_Booking_Statistic_Totalcheckin(_startDate, _endDate, _salonId).ToList();
                    var listTotalBook = db.Store_Booking_Statistic_TotalBook(_startDate, _endDate, _salonId).ToList();
                    var listTotalSlot = db.Store_BooKing_Statistic_Slot(_startDate, _endDate, _salonId).ToList();
                    var data = new List<cls_statistic_booking>();
                    var item = new cls_statistic_booking();
                    if (listTotalBook.Count > 0)
                    {
                        var loop = 0;
                        foreach (var v in listTotalBook)
                        {
                            item = new cls_statistic_booking();
                            item.SalonID = v.Id;
                            item.SalonName = v.SalonName;
                            item.TotalBook = v.TotalBook.Value;
                            item.TotalSlot = listTotalSlot[loop].TotalSlot.Value;
                            item.TotalCheckin = listTotalCheckin[loop].TotalCheckin.Value;
                            data.Add(item);
                            loop++;
                        }
                    }
                    RptBooking.DataSource = data;
                    RptBooking.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion
     

        #region[btnClick]
        protected void _BtnClick(object sender, EventArgs e)
        {
            getData();
            ScriptManager.RegisterStartupScript(UPTableListing, this.GetType(), "Format Price", "UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing();", true);
            RemoveLoading();
        }
        #endregion
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
        
        #region[cls_statistic_booking]
        public class cls_statistic_booking
        {
            public int SalonID { get; set; }
            public string SalonName { get; set; }
            public int TotalSlot { get; set; }
            public int TotalBook { get; set; }
            public int TotalCheckin { get; set; }
            public int TotalNotCheckin { get; set; }
        }
        #endregion
    }
}