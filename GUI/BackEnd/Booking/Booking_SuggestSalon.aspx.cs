﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Linq.Expressions;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.UIBooking
{
    public partial class Booking_SuggestSalon : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        protected bool Perm_Access = false;
        protected bool Perm_Add = false;
        protected bool Perm_Edit = false;
        protected bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected string style_css = "";
        protected string style1_css = "";
        protected string DayI = "";
        protected string DayII = "";
        protected string DayIII = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            checkPermission();
            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
                bindData();
            }
        }
        CultureInfo culture = new CultureInfo("vi-VN");
        protected void checkPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //string perName = Session["User_Permission"].ToString();
                //string url = Request.Url.AbsolutePath.ToString();
                //Solution_30shineEntities db = new Solution_30shineEntities();
                //var pem = (from m in db.Tbl_Permission_Map
                //           join mn in db.Tbl_Permission_Menu on m.mID equals mn.mID
                //           where m.pName == perName && mn.Url_Main.EndsWith(url) || mn.Url_Rewrite.EndsWith(url)
                //           select new { m.aID }
                //           ).FirstOrDefault();
                //if (pem != null)
                //{
                //    var pemArray = pem.aID.Split(',');
                //    if (Array.IndexOf(pemArray, "1") > -1)
                //    {
                //        Perm_Access = true;
                //        if (Array.IndexOf(pemArray, "2") > -1)
                //        {
                //            Perm_ViewAllData = true;
                //        }
                //        if (Array.IndexOf(pemArray, "3") > -1)
                //        {
                //            Perm_Add = true;
                //        }
                //        if (Array.IndexOf(pemArray, "4") > -1)
                //        {
                //            Perm_Edit = true;
                //        }
                //        if (Array.IndexOf(pemArray, "5") > -1)
                //        {
                //            Perm_Delete = true;
                //        }
                //    }
                //    else
                //    {
                //        ContentWrap.Visible = false;
                //        NotAllowAccess.Visible = true;
                //    }

                //}
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Add = permissionModel.CheckPermisionByAction("Perm_Add", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();

            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        // Lấy data
        protected void bindData()
        {
            using (var db = new Solution_30shineEntities())
            {
                DateTime _StartDate = DateTime.Now;
                DateTime _EndDate = _StartDate.AddDays(1);
                int _SalonId = Convert.ToInt32(ddlSalon.SelectedValue);
                var data = db.Store_Booking_SuggestSalon_CustomerListing(String.Format("{0:yyyy/MM/dd}", _StartDate), String.Format("{0:yyyy/MM/dd}", _EndDate), _SalonId).ToList();
                Bind_Paging(data.Count);
                RptBooking.DataSource = data.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                RptBooking.DataBind();
            }
        }
        
        //private void CheckPermission()
        //{
        //    if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
        //    {
        //        var MsgParam = new List<KeyValuePair<string, string>>();
        //        UIHelpers.Redirect("/dang-nhap.html", MsgParam);
        //    }
        //    else
        //    {
        //        string[] Allow = new string[] { "root", "admin", "salonmanager", "reception", "checkin", "checkout" };
        //        string[] AllowViewAllData = new string[] { "root", "admin" };
        //        string[] AllowDelete = new string[] { "root", "admin" };
        //        var Permission = Session["User_Permission"].ToString().Trim();
        //        if (Array.IndexOf(Allow, Permission) != -1)
        //        {
        //            Perm_Access = true;
        //        }
        //        if (Array.IndexOf(AllowDelete, Permission) != -1)
        //        {
        //            Perm_Delete = true;
        //        }
        //        if (Array.IndexOf(AllowViewAllData, Permission) != -1)
        //        {
        //            Perm_ViewAllData = true;
        //        }
        //    }

        //    // Call execute function
        //    ExecuteByPermission();
        //}

        //private void ExecuteByPermission()
        //{
        //    if (!Perm_Access)
        //    {
        //        ContentWrap.Visible = false;
        //        NotAllowAccess.Visible = true;
        //    }
        //}
        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._Segment = 10000;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }


        protected int Get_TotalPage(int TotalRow)
        {
            using (var db = new Solution_30shineEntities())
            {
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            bindData();
            RemoveLoading();
        }
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
        [WebMethod(EnableSession = true)]
        public static string Delele_Booking(int Id)
        {
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                int userId = int.TryParse(HttpContext.Current.Session["User_Id"].ToString(), out integer) ? integer : 0;
                var OBJ = db.Bookings.SingleOrDefault(s => s.Id == Id && s.IsDelete != 1);
                OBJ.IsDelete = 1;
                db.Bookings.AddOrUpdate(OBJ);
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

    }
}