﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Http.Results;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace _30shine.GUI._Booking
{
    public partial class Custumers_Booking_Check_V2 : System.Web.UI.Page
    {
        protected string PageID = "BK_DS_GNL";
        protected Paging PAGING = new Paging();
        protected List<List<ProductBasic>> ProductList;
        CultureInfo culture = new CultureInfo("vi-VN");

        private bool Perm_Access = false;
        public bool Perm_ViewAllData = false;
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        private bool Perm_ShowSalon = false;
        protected bool Perm_Add = false;
        protected bool IsAccountant = false;

        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

        private DateTime timeFrom;
        private DateTime timeTo;
        private int integer;
        public Cls_Quantiy cls_quan;

        private static Custumers_Booking_Check_V2 instance;
        private Solution_30shineEntities db = new Solution_30shineEntities();
        public int isCall = 0;
        public int isCome = 0;
        public int isCancel = 0;
        public int isNotContact = 0;
        public int isTurn = 0;
        private int totalRows = 0;

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Add = permissionModel.GetActionByActionNameAndPageId("Perm_Add", PageID, permission);
                //Perm_ShowSalon = permissionModel.GetActionByActionNameAndPageId("Perm_ShowSalon", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Add = permissionModel.CheckPermisionByAction("Perm_Add", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                txtStartDates.Text = Library.Format.getDateTimeString(DateTime.Now, "dd/MM/yyyy");
                if (!Perm_ViewAllData)
                {
                    txtStartDates.Enabled = false;
                    txtEndDate.Visible = false;
                    txtEndDate.Text = Library.Format.getDateTimeString(DateTime.Now, "dd/MM/yyyy");
                }
                else
                {
                    txtStartDates.Enabled = true;
                    txtEndDate.Visible = true;
                }
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
            }
        }

        /// <summary>
        /// Get instance
        /// </summary>
        /// <returns></returns>
        public static Custumers_Booking_Check_V2 getInstance()
        {
            if (!(Custumers_Booking_Check_V2.instance is Custumers_Booking_Check_V2))
            {
                Custumers_Booking_Check_V2.instance = new Custumers_Booking_Check_V2();
            }

            return Custumers_Booking_Check_V2.instance;
        }


        /// <summary>
        /// Get timeFrom
        /// </summary>
        /// <returns></returns>
        private DateTime getTimeFrom()
        {
            try
            {
                var timeFrom = Library.Format.getDateTimeFromString(txtStartDates.Text);
                var timeTo = Library.Format.getDateTimeFromString(txtEndDate.Text);
                if (timeFrom != null)
                {
                    timeFrom = timeFrom.Value;
                }
                else
                {
                    throw new Exception("Lỗi thời gian bắt đầu.");
                }
                return timeFrom.Value.Date;
            }
            catch (Exception)
            {

                throw new Exception("Lỗi thời gian băt đầu.");
            }
        }


        protected void RptBooking_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                var dataItem = (Cls_Customers_Check)e.Item.DataItem;
                if (dataItem != null)
                {
                    Label rpt = e.Item.FindControl("Label1") as Label;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// Listing data
        /// </summary>
        /// <returns></returns>
        private List<Cls_Customers_Check> getListData()
        {
            try
            {
                int PageNumber = 1;
                int RowspPage = PAGING._Segment;
                if (HDF_Page.Value != "" && Convert.ToInt32(HDF_Page.Value) != 0)
                {
                    PageNumber = Convert.ToInt32(HDF_Page.Value);
                }
                if (HDF_OPTSegment.Value != "" && Convert.ToInt32(HDF_OPTSegment.Value) != 0)
                {
                    RowspPage = Convert.ToInt32(HDF_OPTSegment.Value);
                }
                checkWarningTime checkwarning = new checkWarningTime();
                int salonId = Convert.ToInt32(Salon.SelectedValue);
                if (txtEndDate.Text != "")
                {
                    this.timeTo = Convert.ToDateTime(txtEndDate.Text, culture);
                }
                else
                {
                    this.timeTo = Convert.ToDateTime(txtStartDates.Text, culture);
                }
                this.timeFrom = getTimeFrom();
                var listBooking = db.Store_CustomerBookingCheck_V3(this.timeFrom, this.timeTo, salonId, PageNumber, RowspPage).ToList();
                var listBookingCheck = this.getListBookingCheck();
                var listData = new List<Cls_Customers_Check>();
                var item = new Cls_Customers_Check();                
                if (listBooking.Count > 0)
                {
                    foreach (var v in listBooking)
                    {
                        var index = listData.FindIndex(w => w.BookID == v.bookId);
                        if (index == -1)
                        {
                            item = new Cls_Customers_Check();
                            item.BookID = v.bookId;
                            item.CustomerName = v.CustomerName;
                            item.CustomerPhone = v.CustomerPhone;
                            item.HourFrame = v.HourFrame;
                            item.SalonID = v.SalonId;
                            var recordBookingCheck = db.Customer_Booking_Check.FirstOrDefault(w => w.BookingID == item.BookID && w.SalonID == item.SalonID);
                            if (recordBookingCheck != null)
                            {
                                item.IsCall = recordBookingCheck.IsCall;
                                item.Status = recordBookingCheck.Status;
                                item.Note = recordBookingCheck.Note;
                            }
                            else
                            {
                                item.IsCall = false;
                                item.Status = 0;
                                item.Note = "";
                            }                            
                            listData.Add(item);
                            if (totalRows == 0)
                            {
                                try
                                {
                                    totalRows = Convert.ToInt32(listBooking[0].totalRows);
                                }
                                catch
                                {
                                    totalRows = 0;
                                }
                            }
                        }
                        try
                        {

                            DateTime datetimeNow = DateTime.Now;
                            var hourNow = datetimeNow.Hour * 60 + datetimeNow.Minute;
                            double hour = item.HourFrame.Value.TotalMinutes;
                            if (hourNow > hour && item.IsCall == false)
                            {

                                var time = hourNow - hour;
                                if (time > 15)
                                {
                                    item.colorCheck = "cls_danger_book";
                                    if (time > 30)
                                    {
                                        item.blockCheck = "disabled=  " + " disabled " + "  ";
                                    }
                                    else
                                    {
                                        item.blockCheck = "";
                                    }
                                }
                                else
                                {
                                    item.colorCheck = "";
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception(ex.Message);
                        }
                    }
                }
                return listData;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void GetData()
        {
            try
            {
                int salonId = Convert.ToInt32(Salon.SelectedValue);
                if (txtEndDate.Text != "")
                {
                    this.timeTo = Convert.ToDateTime(txtEndDate.Text, culture);
                }
                else
                {
                    this.timeTo = Convert.ToDateTime(txtStartDates.Text, culture);
                }
                this.timeFrom = getTimeFrom();
                var listCallCustomer = db.Store_CustomerBookingCheck_CallCustomer(this.timeFrom, this.timeTo, salonId).FirstOrDefault();
                if (listCallCustomer != null)
                {
                    this.isCall = (int)listCallCustomer.TotalCall;
                    this.isCome = (int)listCallCustomer.TotalCome;
                    this.isCancel = (int)listCallCustomer.TotalCancel;
                    this.isNotContact = (int)listCallCustomer.TotalNotContact;
                    this.isTurn = (int)listCallCustomer.TotalTurn;
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();
            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }
        /// <summary>
        /// Binding data
        /// </summary>
        public void bindData()
        {
            var data = getListData();
            Bind_Paging(this.totalRows);
            RptBooking.DataSource = data;
            RptBooking.DataBind();
        }

        /// <summary>
        /// Lấy danh sách [Customer_Booking_Check] tại salon theo ngày
        /// </summary>
        /// <returns></returns>
        public List<Customer_Booking_Check> getListBookingCheck()
        {
            try
            {
                int salonId = Convert.ToInt32(Salon.SelectedValue);
                return this.db.Customer_Booking_Check.Where(w => w.SalonID == salonId && w.WorkDate == this.timeFrom).ToList();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Check ghi nhận bản ghi [Custumers_Booking_Check_V2]
        /// </summary>
        /// <param name="bookID"></param>
        /// <param name="isCall"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        [WebMethod]
        public static object checkIsCall(int bookID, bool isCall, int status, string note)
        {
            try
            {
                var message = new Library.Class.cls_message();
                Custumers_Booking_Check_V2 instance = Custumers_Booking_Check_V2.getInstance();
                var bookingRecord = instance.db.Bookings.FirstOrDefault(w => w.Id == bookID);
                if (bookingRecord == null)
                {
                    throw new Exception("Lỗi. Không tồn tại bản ghi booking.");
                }

                var customer = instance.getCustomerByPhone(bookingRecord.CustomerPhone);
                if (customer == null)
                {
                    throw new Exception("Lỗi. Không tồn tại khách hàng.");
                }

                instance.insertBookingCheck(bookingRecord.Id, bookingRecord.SalonId.Value, customer.Id, isCall, status, note);
                return bookingRecord;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Lấy bản ghi khách hàng theo số điện thoại
        /// </summary>
        /// <param name="customerPhone"></param>
        /// <returns></returns>
        public Customer getCustomerByPhone(string customerPhone)
        {
            try
            {
                return this.db.Customers.Where(w => w.IsDelete == 0 && w.Phone == customerPhone).OrderByDescending(o => o.Id).FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Insert bản ghi [Customer_Booking_Check]
        /// </summary>
        /// <param name="bookID"></param>
        /// <param name="salonID"></param>
        /// <param name="customerID"></param>
        /// <param name="isCall"></param>
        /// <param name="status"></param>
        public void insertBookingCheck(int bookID, int salonID, int customerID, bool isCall, int status, string note)
        {
            try
            {
                var message = new Library.Class.cls_message();
                var record = this.isSetRecord(bookID, salonID, customerID);
                if (record == null)
                {
                    record = new Customer_Booking_Check();
                    record.BookingID = bookID;
                    record.SalonID = salonID;
                    record.CustomerID = customerID;
                    record.IsCall = isCall;
                    record.Status = status;
                    record.Note = note;
                    record.CreatedTime = DateTime.Now;
                    record.WorkDate = DateTime.Now.Date;
                }
                else
                {
                    record.IsCall = isCall;
                    record.Status = status;
                    record.Note = note;
                    record.ModifiedTime = DateTime.Now;
                }
                this.db.Customer_Booking_Check.AddOrUpdate(record);
                var exc = this.db.SaveChanges();
                if (exc > 0)
                {
                    message.success = true;
                }
                else
                {
                    message.success = false;
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Kiểm tra đã tồn tại bản ghi [Customer_Booking_Check]
        /// </summary>
        /// <param name="bookID"></param>
        /// <param name="salonID"></param>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public Customer_Booking_Check isSetRecord(int bookID, int salonID, int customerID)
        {
            try
            {
                return this.db.Customer_Booking_Check.FirstOrDefault(w => w.BookingID == bookID && w.SalonID == salonID && w.CustomerID == customerID);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }


        protected void _BtnClick(object sender, EventArgs e)
        {
            GetData();
            bindData();
            ScriptManager.RegisterStartupScript(UPTableListing, this.GetType(), "Format Price", "SetActiveTableListing();", true);
            RemoveLoading();
        }
        protected void _click(object sender, EventArgs e)
        {

        }

        public class Cls_Customers_Check
        {
            public int BookID { get; set; }
            public string CustomerName { get; set; }
            public string CustomerPhone { get; set; }
            public Nullable<System.TimeSpan> HourFrame { get; set; }
            public int SalonID { get; set; }
            public bool IsCall { get; set; }
            public int Status { get; set; }
            public string Note { get; set; }
            public string colorCheck { get; set; }
            public string blockCheck { get; set; }
        }
        public class Cls_Quantiy
        {
            public int? IsCall { get; set; }
            public int? KhachDen { get; set; }
            public int? KhachHuy { get; set; }
            public int? KhongLienLac { get; set; }
            public int? ChuyenLich { get; set; }

        }
        public class checkWarningTime
        {
            public int Id { get; set; }
            public TimeSpan Time15 { get; set; }
            public TimeSpan Time30 { get; set; }
            public string CustomersName { get; set; }
        }


    }
}