﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using Project.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.UIBooking
{
    public partial class StatisticBooking : System.Web.UI.Page
    {
        protected string PageID = "BK_TK_DATA";
        protected Paging PAGING = new Paging();
        string[] hourFrameP1 = new string[] { "15h30", "16h00", "16h30", "17h00", "17h30", "18h00", "18h30" }; // Áp dụng cho Khâm Thiên và TĐN
        CultureInfo culture = new CultureInfo("vi-VN");
        private bool Perm_Access = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewCol = false;

        int percentP1;
        int percentP2;

        /// <summary>
        /// Constructor
        /// </summary>
        public StatisticBooking()
        {
            // Lấy giá trị cấu hình từ file Webconfig cho tỷ lệ số slot theo khung giờ
            try
            {
                percentP1 = Convert.ToInt32(ConfigurationManager.AppSettings["TyLe_Slot_TheoKhungGio_1"]);
                percentP2 = Convert.ToInt32(ConfigurationManager.AppSettings["TyLe_Slot_TheoKhungGio_2"]);
            }
            catch
            {
                percentP1 = 0;
                percentP2 = 0;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            SetPermission();
            if (!IsPostBack)
            {
                txtStartDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Today));
                //txtEndDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Today));
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
                //Bind_Paging();
                //BindBooking();
            }
            if (Perm_ViewCol == false)
            {
                th1_perm.Visible = true;
                th2_perm.Visible = true;
                th3_perm.Visible = true;
            }
            else
            {
                th1_perm.Visible = false;
                th2_perm.Visible = false;
                th3_perm.Visible = false;
            }
        }
        /// <summary>
        /// Set Permission for Page
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ViewCol = permissionModel.GetActionByActionNameAndPageId("Perm_ViewCol", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewCol = permissionModel.CheckPermisionByAction("Perm_ViewCol", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        protected void BindBooking()
        {
            using (var db = new Solution_30shineEntities())
            {
                DateTime _StartDate = Convert.ToDateTime(txtStartDate.Text, culture);
                int _SalonId = Convert.ToInt32(ddlSalon.SelectedValue);
                List<TempStatisticBooking> listHour = null;
                //tinh % theo salon
                double _PercentSlotCusNew = 0;
                double _PercentSlotCusOld = 0;
                if (_SalonId > 0)
                {
                    var objSalon = db.Tbl_Salon.FirstOrDefault(a => a.Id == _SalonId);
                    _PercentSlotCusNew = (double)Math.Round((double)(objSalon.PercentCusNew / 100), 1, MidpointRounding.AwayFromZero);
                    _PercentSlotCusOld = (double)Math.Round((double)(objSalon.PercentCusOld / 100), 1, MidpointRounding.AwayFromZero);
                    //Check khach cu khach moi
                    listHour = (from a in db.Store_StatisticBookingWhereDateAndSalon(_SalonId, _StartDate)
                                select new TempStatisticBooking
                                {
                                    Id = a.Id,
                                    SalonId = a.SalonId,
                                    CurrentOrderNew = a.CurrentOrderNew,
                                    CurrentOrder = a.CurrentOrder,
                                    CurrentOrderOld = a.CurrentOrderOld,
                                    Hour = a.Hour,
                                    HourFrame = a.HourFrame,
                                    TotalPower = a.TotalPower,
                                    Name = a.Name,
                                    DateBook = a.DatedBook,
                                    SlotALL = a.SlotALL,
                                    MaxSlotCusNew = (double)Math.Round((double)(a.SlotALL * _PercentSlotCusNew), 0, MidpointRounding.AwayFromZero),
                                    MaxSlotCusOld = (double)Math.Round((double)(a.SlotALL * _PercentSlotCusOld), 0, MidpointRounding.AwayFromZero),
                                }).ToList();
                    int _count = listHour.Count;
                    if (_count > 0)
                    {
                        for (int i = 0; i < _count; i++)
                        {
                            if ((TimeSpan)listHour[i].HourFrame >= TimeSpan.Parse("15:30:00") && (TimeSpan)listHour[i].HourFrame <= TimeSpan.Parse("18:30:00"))
                            {
                                listHour[i].SlotALL = Math.Floor((double)listHour[i].SlotALL * percentP2 / 100);
                                listHour[i].MaxSlotCusNew = Math.Floor((double)listHour[i].MaxSlotCusNew * percentP2 / 100);
                                listHour[i].MaxSlotCusOld = Math.Floor((double)listHour[i].MaxSlotCusOld * percentP2 / 100);



                            }
                            else
                            {
                                listHour[i].SlotALL = Math.Floor((double)listHour[i].SlotALL * percentP1 / 100);
                                listHour[i].MaxSlotCusNew = Math.Floor((double)listHour[i].MaxSlotCusNew * percentP1 / 100);
                                listHour[i].MaxSlotCusOld = Math.Floor((double)listHour[i].MaxSlotCusOld * percentP1 / 100);
                            }
                        }
                        RptBooking.DataSource = listHour;
                        RptBooking.DataBind();
                        RemoveLoading();

                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Không có dữ liệu !');", true);
                        RemoveLoading();
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "alert('Mời bạn chọn Salon !');", true);
                    RemoveLoading();
                }

                //DateTime _EndDate = Convert.ToDateTime(txtEndDate.Text, culture);

            }
        }

       
        //protected void Bind_Paging()
        //{
        //    // init Paging value            
        //    PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
        //    PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
        //    PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
        //    PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
        //    PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
        //    PAGING._Paging = PAGING.Make_Paging();

        //    RptPaging.DataSource = PAGING._Paging.ListPage;
        //    RptPaging.DataBind();
        //}


        //protected int Get_TotalPage()
        //{
        //    using (var db = new Solution_30shineEntities())
        //    {
        //        DateTime _StartDate = Convert.ToDateTime(txtStartDate.Text, culture);
        //        DateTime _EndDate = Convert.ToDateTime(txtEndDate.Text, culture);
        //        int _SalonId = Convert.ToInt32(ddlSalon.SelectedValue);
        //        int _IsMakeBill = Convert.ToInt32(ddlIsMakeBill.SelectedValue);
        //        var lstBooking = db.BookingListV1(_StartDate, _EndDate, _SalonId, _IsMakeBill);
        //        var Count = lstBooking.Count();
        //        int TotalRow = Count - PAGING._TopNewsNum;
        //        int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
        //        return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        //    }
        //}

        protected void _BtnClick(object sender, EventArgs e)
        {
            if (Perm_ViewCol == false)
            {
                th1_perm.Visible = true;
                th2_perm.Visible = true;
                th3_perm.Visible = true;
            }
            else
            {
                th1_perm.Visible = false;
                th2_perm.Visible = false;
                th3_perm.Visible = false;
            }
            //Bind_Paging();
            BindBooking();
            //RemoveLoading();
        }
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
        public class TempStatisticBooking
        {
            public int? Id { get; set; }
            public int? SalonId { get; set; }
            public double? SlotALL { get; set; }
            public int? CurrentOrder { get; set; }
            public int? CurrentOrderNew { get; set; }
            public int? CurrentOrderOld { get; set; }
            public int? TotalPower { get; set; }
            public double? MaxSlotCusNew { get; set; }
            public double? MaxSlotCusOld { get; set; }
            public string Hour { get; set; }
            public string Name { get; set; }
            public TimeSpan? HourFrame { get; set; }
            public DateTime? DateBook { get; set; }

        }

        protected void RptBooking_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            HtmlTableCell td1_perm = (HtmlTableCell)e.Item.FindControl("td1_perm");
            HtmlTableCell td2_perm = (HtmlTableCell)e.Item.FindControl("td2_perm");
            HtmlTableCell td3_perm = (HtmlTableCell)e.Item.FindControl("td3_perm");
            if (Perm_ViewCol == false)
            {
                td1_perm.Visible = true;
                td2_perm.Visible = true;
                td3_perm.Visible = true;
            }
            else
            {
                td1_perm.Visible = false;
                td2_perm.Visible = false;
                td3_perm.Visible = false;
            }
        }
    }

}