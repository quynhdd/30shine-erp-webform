﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Linq.Expressions;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.UIBooking
{
    public partial class ListingBooking_Test : System.Web.UI.Page
    {
        protected string PageID = "BK_DS";
        protected Paging PAGING = new Paging();
        private bool Perm_Access = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDataByDate = false;
        protected bool Perm_ViewAllDataBySalon = false;        
        protected string style_css = "";
        protected string style1_css = "";
        protected string DayI = "";
        protected string DayII = "";
        protected string DayIII = "";
        protected DateTime timeFrom;
        protected DateTime timeTo;
        protected int salonId;
        protected string CustomerPhone;
        protected int? iCount = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                txtStartDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now));
                txtEndDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now));
                //Permission();
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
                GetDateBook();
                BindIsMakeBill();
                BindBooking();
            }
        }
        CultureInfo culture = new CultureInfo("vi-VN");
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ViewAllDataByDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDataByDate", PageID, permission);
                //Perm_ViewAllDataBySalon = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDataBySalon", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllDataByDate = permissionModel.CheckPermisionByAction("Perm_ViewAllDataByDate", pageId, staffId);
                Perm_ViewAllDataBySalon = permissionModel.CheckPermisionByAction("Perm_ViewAllDataBySalon", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected string checkper()
        {
            string str = "";
            int _UserId = Convert.ToInt32(Session["User_Id"]);
            string a = Session["User_Permission"].ToString();
            if (a == "admin" || a == "root")
            {
                str = "";
            }
            if (_UserId == 119 || _UserId == 79)
            {
                str = "";
            }
            else
            {
                str = "display_permission";
            }

            return str;
        }

        //=========================
        protected void GetDateBook()
        {
            string setActiveNight = "";
            string setDayTime = "";
            string str = "";
            DateTime day = DateTime.Now;
            DateTime a = DateTime.Today;
            DayI = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now));
            DayII = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now).AddDays(+1));
            DayIII = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now).AddDays(+2));
            int dayI = ((int)day.DayOfWeek);
            int dayII = ((int)day.AddDays(+1).DayOfWeek);
            int dayIII = ((int)day.AddDays(+2).DayOfWeek);

        }

        //=========================
        protected void BindIsMakeBill()
        {
            ListItem item1 = new ListItem("Tất cả", "3");
            ListItem item2 = new ListItem("Đã đến", "1");
            ListItem item3 = new ListItem("Chưa đến/không đến", "0");
            ddlIsMakeBill.Items.Add(item1);
            ddlIsMakeBill.Items.Add(item2);
            ddlIsMakeBill.Items.Add(item3);
        }


        /// <summary>
        /// Get list data booking
        /// </summary>
        /// <returns></returns>
        protected List<Store_ListingBookingTest_BySalon_V2_Result> GetListData()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    int PageNumber = 1;
                    int RowspPage = PAGING._Segment;
                    if (HDF_Page.Value != "" && Convert.ToInt32(HDF_Page.Value) != 0)
                    {
                        PageNumber = Convert.ToInt32(HDF_Page.Value);
                    }
                    if (HDF_OPTSegment.Value != "" && Convert.ToInt32(HDF_OPTSegment.Value) != 0)
                    {
                        RowspPage = Convert.ToInt32(HDF_OPTSegment.Value);
                    }
                    int integer;
                    this.timeFrom = Convert.ToDateTime(txtStartDate.Text, culture);
                    this.timeTo = Convert.ToDateTime(txtEndDate.Text, culture);
                    this.salonId = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                    this.CustomerPhone = txtCustomerPhone.Text;
                    var lstBooking = db.Store_ListingBookingTest_BySalon_V2(this.timeFrom, this.timeTo, this.salonId, this.CustomerPhone, PageNumber, RowspPage).ToList();
                    if (lstBooking.Count > 0)
                    {
                        Bind_Paging((int)lstBooking[0].totalRows);
                        iCount = lstBooking[0].totalRows;
                    }
                    return lstBooking;
                }
            }
            catch (Exception)
            {

                throw;
            }
        }
        /// <summary>
        /// Bind data to view
        /// </summary>
        protected void BindBooking()
        {
            var data = GetListData();
            //Bind_Paging(data.Count);
            RptBooking.DataSource = data;
            RptBooking.DataBind();
        }


        private void Permission()
        {
            int _UserId = Convert.ToInt32(Session["User_Id"]);
            if (_UserId == 119 || _UserId == 79 || _UserId == 0)
            {
                style1_css = "display_permission";
            }
            else
            {
                style_css = "display_permission";
            }
        }


        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }
        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            BindBooking();
            RemoveLoading();
        }
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Hủy lịch booking
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public static string Delele_Booking(int Id)
        {
            try
            {
                var Msg = new Msg();
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (var db = new Solution_30shineEntities())
                {
                    int integer;
                    int error = 0;
                    int userId = int.TryParse(HttpContext.Current.Session["User_Id"].ToString(), out integer) ? integer : 0;

                    /// Delete trên bảng Booking
                    Booking booking = (from c in db.Bookings
                                       where c.BookingTempId == Id && c.IsDelete == 0
                                       select c).FirstOrDefault();

                    if (booking != null)
                    {
                        var checkBill = db.BillServices.FirstOrDefault(w => w.BookingId == booking.Id && w.IsDelete == 0);
                        if (checkBill != null)
                        {
                            Msg.success = false;
                            Msg.msg = "Lỗi. Booking đã được in bill. Không thể xóa booking này.";
                        }
                        else
                        {
                            booking.IsDelete = 1;
                            booking.ModifiedDate = DateTime.Now;
                            //db.Bookings.AddOrUpdate(booking);
                            error += db.SaveChanges() > 0 ? 0 : 1;

                            /// Delete trên bảng SM_BookingTemp                        
                            if (error == 0)
                            {
                                SM_BookingTemp bookingTemp = (from c in db.SM_BookingTemp
                                                              where c.Id == booking.BookingTempId
                                                              select c).FirstOrDefault();
                                if (bookingTemp != null)
                                {
                                    bookingTemp.IsDelete = 1;
                                    bookingTemp.ModifiedDate = DateTime.Now;
                                    //db.SM_BookingTemp.AddOrUpdate(bookingTemp);
                                    error += db.SaveChanges() > 0 ? 0 : 1;
                                }
                            }

                            if (error == 0)
                            {
                                Msg.success = true;
                                Msg.msg = "Cập nhật thành công!";
                            }
                            else
                            {
                                Msg.success = false;
                                Msg.msg = "Đã có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển!";
                            }
                        }
                        
                    }
                    else
                    {
                        Msg.success = false;
                        Msg.msg = "Lỗi. Bản ghi không tồn tại.";
                    }
                }
                return serializer.Serialize(Msg);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}