﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using LinqKit;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Data.Linq.SqlClient;
using System.Data.Objects;
using _30shine.GUI.SystemService.API;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.Interface;

namespace _30shine.GUI.BackEnd.UIBooking
{
    public partial class AddOrUpdateBooHour : System.Web.UI.Page
    {
        protected string PageID = "";
        int _HourId = 0;
        protected bool Perm_Access = false;
        protected bool Perm_Add = false;
        protected bool Perm_Edit = false;
        protected bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                if (Request.QueryString["Code"]!= null)
                {
                    PageID = "BK_EDIT_HOUR";
                }
                else
                {
                    PageID = "BK_ADDNEW";
                }
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                ////Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!string.IsNullOrEmpty(Request.QueryString["HourId"]))
                int.TryParse(Request.QueryString["HourId"], out _HourId);
            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_Access);
                BindBookhourWhereId();
            }
        }
        
        //Bind bookhour where Id
        protected void BindBookhourWhereId()
        {
            if (_HourId > 0)
            {
                using (var db = new Solution_30shineEntities())
                {
                    var OBJ = new BookHour();
                    OBJ = db.BookHours.FirstOrDefault(a => a.Id == _HourId);
                    txtHour.Text = OBJ.Hour;
                    txtHourFrame.Text = Convert.ToString(OBJ.HourFrame);
                    txtSlot.Text = Convert.ToString(OBJ.Slot);
                    ddlSalon.ClearSelection();
                    ddlSalon.Items.FindByValue(OBJ.SalonId.ToString()).Selected = true;
                   
                    if (OBJ.Publish == true)
                    {
                        chkPublish.Checked = true;
                    }
                    else
                    {
                        chkPublish.Checked = false;
                    }
                }
            }
        }

        //validate HH:mm:ss
        public bool ValidateTime(string time)
        {
            DateTime ignored;
            return DateTime.TryParseExact(time, "HH:mm:ss",
                                          CultureInfo.InvariantCulture,
                                          DateTimeStyles.None,
                                          out ignored);
        }

        protected void btnComplete_Click(object sender, EventArgs e)
        {
            UpdateComplete();
        }
        protected void UpdateComplete()
        {
            using (var db = new Solution_30shineEntities())
            {
                if (ValidateTime(txtHourFrame.Text))
                {
                    var OBJ = new BookHour();
                    int integer;
                    int userId = int.TryParse(HttpContext.Current.Session["User_Id"].ToString(), out integer) ? integer : 0;
                    string _hourFrame = txtHourFrame.Text;
                    TimeSpan ts = TimeSpan.Parse(_hourFrame);
                    if (_HourId > 0)
                    {
                        OBJ = db.BookHours.FirstOrDefault(a => a.Id == _HourId);
                        OBJ.ModifiedDate = DateTime.Now;
                    }
                    else
                    {
                        OBJ.CreatedDate = DateTime.Now;
                    }
                    OBJ.SalonId =Convert.ToInt32(ddlSalon.SelectedValue);
                    OBJ.Hour = txtHour.Text.Trim();
                    OBJ.IsDelete = 0;
                    OBJ.UserId = userId;
                    OBJ.Slot = Convert.ToInt32(txtSlot.Text);

                    OBJ.HourFrame = ts;
                    if (chkPublish.Checked == true)
                    {
                        OBJ.Publish = true;
                    }
                    else
                    {
                        OBJ.Publish = false;
                    }
                    db.BookHours.AddOrUpdate(OBJ);
                    db.SaveChanges();
                    // Thông báo thành công
                    var MsgParam = new List<KeyValuePair<string, string>>();
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                    UIHelpers.Redirect("/admin/booking/sua-khung-gio-" + OBJ.Id + ".html", MsgParam);
                }
                else
                {
                    MsgSystem.Text = "Sai định dạng ! Bạn vui lòng nhập lại ( Thời gian thực trong ngày ) VD  08:00:00";
                    MsgSystem.CssClass = "msg-system success failed";
                }
            }
        }
        
        
        
    }
}