﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Booking_SuggestSalon_V2.aspx.cs" Inherits="_30shine.GUI.BackEnd.UIBooking.Booking_SuggestSalon_V2" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .select2-container {
                width: 180px !important;
                margin-top: 5px !important;
            }
            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Booking &nbsp;&#187;</li>
                        <li class="li-listing active"><a href="javascript:void(0);">Danh sách gọi hỗ trợ đặt lịch</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row row-filter">
                    <strong class="st-head"><i class="fa fa-filter"></i>Lọc kết quả</strong>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlSalon" CssClass="select form-control" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 150px;">
                            <asp:ListItem Text="Chọn salon" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Hội quán cắt tóc 1" Value="15"></asp:ListItem>
                            <asp:ListItem Text="Hội quán cắt tóc 2" Value="16"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <%--<a href="javascript:void(0);" onclick="location.href=location.href;" class="st-head btn-viewdata">Reset Filter</a>--%>
                </div>
                <!-- End Filter -->
                <!-- Row Table Filter -->
                <div class="table-func-panel">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="table-wp">
                            <table class="table-add table-listing">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên khách hàng</th>
                                        <th>Số điện thoại</th>
                                        <th>Giờ vào book</th>
                                        <th>Khách từ salon</th>
                                        <th>Đã gọi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="Rpt" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td><%#Eval("CustomerName") %></td>
                                                <td><%#Eval("CustomerPhone") %></td>
                                                <td><%#string.Format("{0:HH:mm}",Eval("CreatedTime")) %></td>
                                                <td><%# Eval("SalonName") %></td>
                                                <td>
                                                    <input type="checkbox" onclick="checkIsCall($(this), <%# Eval("Id") %>)" <%# Eval("IsCall") != null && Convert.ToBoolean(Eval("IsCall")) == true ? "checked='checked'" : "" %> /></td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>

                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
            </div>
            <%-- END Listing --%>
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <script>
            jQuery(document).ready(function () {
                $("#glbS4M").addClass("active");
                $("#glbS4MStudent").addClass("active");

                var autoClickLoadData = setInterval(autoLoadData, 10000)
            });

            function checkIsCall(This, Id) {
                $.ajax({
                    type: "POST",
                    url: "/admin/booking/goi-ho-tro-dat-lich/da-goi",
                    data: '{Id : ' + Id + ', IsCall : ' + This.prop("checked") + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        if (response.d.success) {
                            //alert("Đã đánh dấu gọi cho khách!");
                        } else {
                            //alert("Hủy đánh dấu gọi cho khách!");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function autoLoadData() {
                $("#ViewDataFilter").click();
            }
        </script>

    </asp:Panel>
</asp:Content>
