﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Linq.Expressions;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.UIBooking
{
    public partial class ListingBooking : System.Web.UI.Page
    {
        protected string PageID ="BK_DS";
        protected Paging PAGING = new Paging();
        private bool Perm_Access = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected string style_css = "";
        protected string style1_css = "";
        protected string DayI = "";
        protected string DayII = "";
        protected string DayIII = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                var staffId = Convert.ToInt32(Session["User_Id"].ToString());
                txtStartDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now));
                txtEndDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now));
                Permission();
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
                GetDateBook();
                BindIsMakeBill();
                Bind_Paging();
                BindBooking();
            }
        }

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        CultureInfo culture = new CultureInfo("vi-VN");
        protected string checkper()
        {
            string str = "";
            int _UserId = Convert.ToInt32(Session["User_Id"]);
            string a = Session["User_Permission"].ToString();
            if(a=="admin")
            {

            }
            if (_UserId == 119 || _UserId == 79 || _UserId == 0)
            {
                str = "";
            }
            else
            {
                str = "display_permission";
            }
           
            return str;
        }

        //=========================
        protected void GetDateBook()
        {
            string setActiveNight = "";
            string setDayTime = "";
            string str = "";
            DateTime day = DateTime.Now;
            DateTime a = DateTime.Today;
             DayI = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now));
             DayII = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now).AddDays(+1));
             DayIII = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now).AddDays(+2));
            int dayI = ((int)day.DayOfWeek);
            int dayII = ((int)day.AddDays(+1).DayOfWeek);
            int dayIII = ((int)day.AddDays(+2).DayOfWeek);        
            
        }
      
        //=========================
        protected void BindIsMakeBill()
        {
            ListItem item1 = new ListItem("Tất cả", "3");
            ListItem item2 = new ListItem("Đã đến", "1");
            ListItem item3 = new ListItem("Chưa đến/không đến", "0");
            ddlIsMakeBill.Items.Add(item1);
            ddlIsMakeBill.Items.Add(item2);
            ddlIsMakeBill.Items.Add(item3);
        }

        protected void BindBooking()
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                DateTime _StartDate = Convert.ToDateTime(txtStartDate.Text, culture);
                DateTime _EndDate = Convert.ToDateTime(txtEndDate.Text, culture);
                int _SalonId = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                int _IsMakeBill = int.TryParse(ddlIsMakeBill.SelectedValue, out integer) ? integer : 0;
                var lstBooking = db.BookingListV1(_StartDate, _EndDate, _SalonId, _IsMakeBill).Skip(PAGING._Offset).Take(PAGING._Segment).ToList(); ;
                RptBooking.DataSource = lstBooking;
                RptBooking.DataBind();
            }
        }

       
        private void Permission()
        {
            int _UserId = Convert.ToInt32(Session["User_Id"]);
            if (_UserId == 119 || _UserId == 79 || _UserId == 0)
            {
                style1_css = "display_permission";
            }
            else
            {
                style_css = "display_permission";
            }
        }
       
        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }


        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                DateTime _StartDate = Convert.ToDateTime(txtStartDate.Text, culture);
                DateTime _EndDate = Convert.ToDateTime(txtEndDate.Text, culture);
                int _SalonId = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                int _IsMakeBill = int.TryParse(ddlIsMakeBill.SelectedValue, out integer) ? integer : 0;
                var lstBooking = db.BookingListV1(_StartDate, _EndDate, _SalonId, _IsMakeBill);
                var Count = lstBooking.Count();
                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
            BindBooking();
            RemoveLoading();
        }
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
        [WebMethod(EnableSession = true)]
        public static string Delele_Booking(int Id)
        {
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                int userId = int.TryParse(HttpContext.Current.Session["User_Id"].ToString(), out integer) ? integer : 0;
                var OBJ = db.Bookings.SingleOrDefault(s => s.Id == Id && s.IsDelete != 1);
                OBJ.IsDelete = 1;
                db.Bookings.AddOrUpdate(OBJ);
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

    }
}