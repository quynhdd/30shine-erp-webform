﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using LinqKit;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Data.Entity.Migrations;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.UIBooking
{
    public partial class _BookHour : System.Web.UI.Page
    {
        protected string PageID = "BK_CH";
        public int STT = 0;
        protected bool Perm_Access = false;
        protected bool Perm_Add = false;
        protected bool Perm_Edit = false;
        protected bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
                ListingBookHour();
            }
        }

        protected void ListingBookHour()
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                var salonId = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                var LstBookHour = (from a in db.BookHours join s in db.Tbl_Salon on a.SalonId equals s.Id where a.IsDelete == 0 orderby a.HourFrame ascending select new { a.Hour, a.HourFrame, a.Slot, a.Publish, s.Name, a.Id, a.SalonId }).ToList();
                if (salonId > 0)
                {
                    LstBookHour = LstBookHour.Where(w => w.SalonId == salonId).ToList();
                }

                if (LstBookHour.Count > 0)
                {
                    rptBookHour.DataSource = LstBookHour;
                    rptBookHour.DataBind();
                }
            }
        }

        protected bool CheckPublish(string _Publish)
        {
            try
            {
                bool _Result = false;
                if (_Publish != "" && _Publish == "True")
                    _Result = true;
                return _Result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString());
            }
        }

        [WebMethod(EnableSession = true)]
        public static string UpdateSlot(int HourId, int Slot)
        {
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                int userId = int.TryParse(HttpContext.Current.Session["User_Id"].ToString(), out integer) ? integer : 0;
                var obj = db.BookHours.FirstOrDefault(w => w.Id == HourId);
                if (obj != null)
                {
                    obj.ModifiedDate = DateTime.Now;
                    obj.UserId = userId;
                    obj.Slot = Slot;
                }
                db.BookHours.AddOrUpdate(obj);
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        [WebMethod(EnableSession = true)]
        public static string UpdatePublish(int HourId, bool _chkPublish)
        {
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                int userId = int.TryParse(HttpContext.Current.Session["User_Id"].ToString(), out integer) ? integer : 0;
                var obj = db.BookHours.FirstOrDefault(w => w.Id == HourId);
                if (obj != null)
                {
                    obj.ModifiedDate = DateTime.Now;
                    obj.UserId = userId;
                    obj.Publish = _chkPublish;
                }
                db.BookHours.AddOrUpdate(obj);
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        [WebMethod(EnableSession = true)]
        public static string Delele_BookHour(int IdHour)
        {
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                int userId = int.TryParse(HttpContext.Current.Session["User_Id"].ToString(), out integer) ? integer : 0;
                var OBJ = db.BookHours.SingleOrDefault(s => s.Id == IdHour && s.IsDelete != 1);
                OBJ.IsDelete = 1;
                db.BookHours.AddOrUpdate(OBJ);
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }
        protected void ddlSalon_TextChanged(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                int _SalonId = Convert.ToInt32(ddlSalon.SelectedValue);
                if (_SalonId == 0)
                {
                    var lstBookHour = (from a in db.BookHours join s in db.Tbl_Salon on a.SalonId equals s.Id where a.IsDelete == 0   orderby a.HourFrame ascending select new { a.Hour, a.HourFrame, a.Slot, a.Publish, s.Name, a.Id }).ToList();
                    if (lstBookHour.Count > 0)
                    {
                        rptBookHour.DataSource = lstBookHour;
                        rptBookHour.DataBind();
                    }
                }
                else if (_SalonId > 0)
                {
                    var lstBookHour = (from a in db.BookHours join s in db.Tbl_Salon on a.SalonId equals s.Id where a.IsDelete == 0  && a.SalonId == _SalonId orderby a.HourFrame ascending select new { a.Hour, a.HourFrame, a.Slot, a.Publish, s.Name, a.Id }).ToList();
                    if (lstBookHour.Count > 0)
                    {
                        rptBookHour.DataSource = lstBookHour;
                        rptBookHour.DataBind();
                    }
                }

            }
        }
    }
    public class clss_BookHour : BookHour
    {
        public string fff { get; set; }
    }
    struct Msg
    {
        public bool success { get; set; }
        public string msg { get; set; }
    }
}