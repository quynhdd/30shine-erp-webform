﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;

namespace _30shine.GUI.BackEnd.Category
{
    public partial class Category_Listing : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind_Paging();
                Bind_Rpt();
                RemoveLoading();
            }
        }

        private string genSql()
        {
            var sql = @"select a.*, Coalesce(b.Name, '') as Pid_Name 
                        from Tbl_Category as a
                        left join Tbl_Category as b
                        on a.[Pid] = b.Id
                        where a.IsDelete != 1";
            return sql;
        }

        private void Bind_Rpt()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Database.SqlQuery<cls_category>(genSql()).ToList().OrderBy(o => o.Id).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();

                Rpt.DataSource = LST;
                Rpt.DataBind();
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
            Bind_Rpt();
            RemoveLoading();
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                var Count = db.Database.SqlQuery<cls_category>(genSql()).Count();
                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }
    }

    public class cls_category: Tbl_Category
    {
        public string Pid_Name { get; set; }
    }
}