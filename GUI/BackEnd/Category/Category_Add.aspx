﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Category_Add.aspx.cs" Inherits="_30shine.GUI.BackEnd.Category.Category_Add" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ProductAdd" ContentPlaceHolderID="CtMain" runat="server">
<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">
                <li>Quản lý danh mục &nbsp;&#187; </li>
                <li class="li-listing"><a href="/admin/danh-muc/danh-sach.html">Danh sách</a></li>
                <li class="li-add active"><a href="/admin/danh-muc/them-moi.html">Thêm mới</a></li>
            </ul>


        </div>
    </div>
</div>

<div class="wp customer-add admin-product-add">
    <%-- Add --%>                
    <div class="wp960 content-wp">
        <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
        <div class="table-wp">
            <table class="table-add admin-product-table-add">
                <tbody>
                    <tr class="title-head">
                        <td><strong>Thông tin danh mục</strong></td>
                        <td></td>
                    </tr>
                    <tr class="tr-margin" style="height: 20px;"></tr>
                    <tr>
                        <td class="col-xs-3 left"><span>Tên danh mục</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="Name" runat="server" ClientIDMode="Static"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="NameValidate" ControlToValidate="Name" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập tên danh mục!"></asp:RequiredFieldValidator>
                            </span>
                                            
                        </td>
                    </tr>

                    <tr class="tr-field-ahalf">
                        <td class="col-xs-3 left"><span>Danh mục cha</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:DropDownList ID="Pid" runat="server" CssClass="select" ClientIDMode="Static"></asp:DropDownList>
                            </span>                                            
                        </td>
                    </tr>
                    
                    <tr class="tr-field-ahalf">
                        <td class="col-xs-3 left"><span>30shinestore Id</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="OnWebId" runat="server" ClientIDMode="Static"></asp:TextBox>
                            </span>                                            
                        </td>
                    </tr>                        

                    <tr class="tr-description">
                        <td class="col-xs-3 left"><span>Mô tả</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox TextMode="MultiLine" Rows="5" ID="Description" runat="server" ClientIDMode="Static"></asp:TextBox>
                            </span>
                        </td>
                    </tr>

                    <tr class="tr-upload">
                        <td class="col-xs-3 left"><span>Ảnh đại diện</span></td>
                        <td class="col-xs-7 right">
                            <div class="wrap btn-upload-wp HDF_Images">
                                <div id="Btn_UploadImg" class="btn-upload-img" onclick="popupImageIframe('HDF_Images')">Thêm ảnh</div>
                                <asp:FileUpload ID="UploadFile" ClientIDMode="Static" Style="display: none;"
                                    CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                <div class="wrap listing-img-upload title-version">
                                    <% if (Thumb.url != null && Thumb.url != "")
                                        { %>
                                            <div class="thumb-wp">
                                                <div class="left">
                                                    <img class="thumb" alt="" title="" src="<%= Thumb.url %>" data-img="" style="width: 120px; height: 80px; left: 0px;"/></div>
                                                <div class="right">
                                                    <input class="thumb-title" type="text" placeholder="Tiêu đề ảnh" value="<%=Thumb.title %>"/>
                                                    <textarea class="thumb-des" placeholder="Mô tả"><%=Thumb.description %></textarea>
                                                    <div class="action">
                                                        <div class="action-item action-add" onclick="popupImageIframe('HDF_Images')"><i class="fa fa-plus-square" aria-hidden="true"></i>Thêm ảnh</div>
                                                        <div class="action-item action-delete" onclick="deleteThumbnail($(this), 'HDF_Images')"><i class="fa fa-trash-o" aria-hidden="true"></i>Xóa</div>
                                                    </div>
                                                </div>
                                            </div>                                         
                                    <% } %>
                                </div>
                            </div>                                
                        </td>
                    </tr>

                    <tr class="tr-field-ahalf">
                        <td class="col-xs-3 left"><span>Thứ tự sắp xếp</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="_Order" runat="server" ClientIDMode="Static"
                                    placeholder="Ví dụ : 1"></asp:TextBox>
                            </span>
                        </td>
                    </tr>

                    <tr class="tr-product-category">
                        <td class="col-xs-3 left"><span>Publish</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:CheckBox ID="Publish" Checked="true" runat="server" />
                            </span>
                        </td>
                    </tr>
                    
                    <tr class="tr-send">
                        <td class="col-xs-3 left"></td>
                        <td class="col-xs-9 right no-border">
                            <span class="field-wp">
                                <asp:Button ID="Send" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="ExcAddOrUpdate" OnClientClick="initImgStore('HDF_Images')"></asp:Button>
                                <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>
                            </span>
                        </td>
                    </tr>
                    <asp:HiddenField runat="server" ID="UrlKeyField"  ClientIDMode="Static"/>
                    <asp:HiddenField runat="server" ID="ImgCoverField"  ClientIDMode="Static"/>
                    <asp:HiddenField runat="server" ID="HDF_Images"  ClientIDMode="Static"/>
                </tbody>
            </table>
        </div>
    </div>
    <%-- end Add --%>
</div>

<script>
    jQuery(document).ready(function () {
        $("#glbAdminContent").addClass("active");
        $("#glbAdminCategory").addClass("active");

        var test = "http://solution.30shine.net/Public/Media/Upload/Images/Common/2016/7/22/1-iuTivF.png";
        console.log(executeThumPath(test));
    });
</script>

<!-- Popup plugin image -->
<script type="text/javascript">
    function popupImageIframe(StoreImgField) {
        var Width = 960;
        var Height = 560;
        var ImgList = [];
        var iframe = $("<div class='iframe-image-upload' style='width:" + Width + "px; height:" + Height + "px; float:left; display:none;'><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + StoreImgField + "&callback=getThumbFromIframe&imgWidth=350&imgHeight=350&multi_img=false&Folder=Common' style='width:100%; height: 100%;'></iframe></div");

        $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
    }

    window.getThumbFromIframe = function (Imgs, StoreImgField) {
        var imgs = "";
        var thumb = "";
        if (Imgs.length > 0) {
            for (var i = 0; i < Imgs.length; i++) {
                thumb += '<div class="thumb-wp">' +
                            '<div class="left">' +
                                    '<img class="thumb" alt="" title="" src="' + Imgs[i] + '" data-img=""/>' +
                            '</div>' +
                            '<div class="right">' +
                                '<input class="thumb-title" type="text" placeholder="Tiêu đề ảnh" />' +
                                '<textarea class="thumb-des" placeholder="Mô tả"></textarea>' +
                                '<div class="action">        ' +                                            
                                    '<div class="action-item action-add" onclick="popupImageIframe(\'HDF_Images\')"><i class="fa fa-plus-square" aria-hidden="true"></i>Thêm ảnh</div>' +
                                    '<div class="action-item action-delete" onclick="deleteThumbnail($(this), \'' + StoreImgField + '\')"><i class="fa fa-trash-o" aria-hidden="true"></i>Xóa</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>';
            }
            $("." + StoreImgField).find(".listing-img-upload").append($(thumb));
            excThumbWidth(120, 120);
        }
        autoCloseEBPopup();
    }

    function imgLink(StoreImgField) {
        var imgs = [], img, src, title, description;
        $("." + StoreImgField).find(".thumb-wp").each(function () {
            src = $(this).find("img.thumb").attr("src");
            title = $(this).find("input.thumb-title").val();
            description = $(this).find("textarea.thumb-des").val();
            img = { url: src, thumb: executeThumPath(src), title: title, description: description };
        });
        return img;
    }

    function initImgStore(StoreImgField) {
        $("#" + StoreImgField).val(JSON.stringify(imgLink(StoreImgField)));
    }

    function deleteThumbnail(This, StoreImgField) {
        This.parent().parent().parent().remove();
    }

    function executeThumPath(src) {
        var str = "";
        var fileExtension = "";
        var strLeft = "";
        var strPush = "x350";
        if (src != null && src != "") {
            var loop = src.length - 1;
            for (var i = loop; i >= 0; i--) {
                if (src[i] == ".") {
                    str = src.substring(0, i) + strPush + "." + fileExtension;
                    break;
                } else {
                    fileExtension = src[i] + fileExtension;
                }
            }
        }
        return str;
    }
</script>
<!-- Popup plugin image -->

</asp:Panel>
</asp:Content>

