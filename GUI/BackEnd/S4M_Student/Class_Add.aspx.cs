﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.Helpers;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.Interface;
using _30shine.MODEL.IO;
using _30shine.MODEL.BO;
using System.Globalization;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace _30shine.GUI.BackEnd.S4M_Student
{
    public partial class Class_Add : System.Web.UI.Page
    {
        private string PageID = "";
        protected bool _IsUpdate = false;
        private bool Perm_Access = false;
        protected bool Perm_ViewAllData = false;
        IStaffModel dbStaff = new StaffModel();
        IS4MClassModel dbS4mClass = new S4MClassModel();
        protected string _Code;
        protected Stylist4Men_Class OBJ;
        CultureInfo culture = new CultureInfo("vi-VN");
        protected void Page_Load(object sender, EventArgs e)
        {
            //DateStart.Text = string.Format("{0:dd/MM/yyyy}",DateTime.Now);
            SetPermission();
            if (!IsPostBack)
            {
                Bind_GVChuNhiem();
                Bind_GVDiemCat();
                Bind_GVHoiQuan();

                if (IsUpdate())
                {
                    Bind_OBJ();
                }

            }
        }

        /// <summary>
        /// Bind Data 
        /// </summary>
        private void Bind_OBJ()
        {

            var obj = dbS4mClass.GetById(int.Parse(_Code));
            if (obj != null)
            {
                Name.Text = obj.Name;
                DateStart.Text = string.Format("{0:dd/MM/yyyy}", obj.StartTime);
                ddlGVChuNhiem.SelectedValue = obj.HomeRoomTeacherId.ToString();

                ddlGVDiemCat.SelectedValue = obj.TeacherPointCutId.ToString();
                ddlGVHoiQuan.SelectedValue = obj.TeacherOfTheClubId.ToString();
            }
        }

        private bool IsUpdate()
        {
            if (Request.QueryString["Code"] == null) { _IsUpdate = false; return false; }

            _Code = Request.QueryString["Code"];
            if (_Code != "")
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;

            }

        }


        protected void AddOrUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
            {
                Update();

            }
            else
            {
                Add();
            }
        }

        /// <summary>
        /// Insert class Author: Như Phúc
        /// </summary>
        private void Add()
        {

            var obj = new Stylist4Men_Class();
            if (DateStart.Text != "" && DateStart != null) obj.StartTime = Convert.ToDateTime(DateStart.Text, culture);
            obj.HomeRoomTeacherId = int.Parse(ddlGVChuNhiem.SelectedValue);
            obj.TeacherOfTheClubId = int.Parse(ddlGVHoiQuan.SelectedValue);
            obj.TeacherPointCutId = int.Parse(ddlGVDiemCat.SelectedValue);
            obj.Name = Name.Text;
            obj.IsDelete = false;
            obj.Publish = true;
            var exc = dbS4mClass.Add(obj);
            if (exc != null)
            {

                var MsgParam = new List<KeyValuePair<string, string>>();
                MsgParam.Add(new KeyValuePair<string, string>("msg_add_status", "success"));
                MsgParam.Add(new KeyValuePair<string, string>("msg_add_message", "Thêm thành công!"));
                UIHelpers.Redirect("/admin/s4m/lop-hoc/danh-sach.html", MsgParam);
            }
            else
            {

                var msg = "Thêm thất bại! Vui lòng liên hệ nhóm phát triển!";
                var status = "warning";
                UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
            }
        }

        /// <summary>
        /// Update class Như Phúc
        /// </summary>
        private void Update()
        {
            var obj = new Stylist4Men_Class();
            obj.Id = int.Parse(_Code);
            obj.Name = Name.Text;
            if (DateStart.Text != "" && DateStart != null) obj.StartTime = Convert.ToDateTime(DateStart.Text, culture);
            obj.HomeRoomTeacherId = int.Parse(ddlGVChuNhiem.SelectedValue);
            obj.TeacherOfTheClubId = int.Parse(ddlGVHoiQuan.SelectedValue);
            obj.TeacherPointCutId = int.Parse(ddlGVDiemCat.SelectedValue);
            var exc = dbS4mClass.Update(obj);

            if (exc > 0)
            {

                var MsgParam = new List<KeyValuePair<string, string>>();
                MsgParam.Add(new KeyValuePair<string, string>("msg_add_status", "success"));
                MsgParam.Add(new KeyValuePair<string, string>("msg_add_message", "Cập nhật thành công!"));
                //UIHelpers.Redirect("/admin/s4m/lop-hoc/" + obj.Id + ".html", MsgParam);
                UIHelpers.Redirect("/admin/s4m/lop-hoc/danh-sach.html", MsgParam);
            }
            else
            {

                var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                var status = "warning";
                UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
            }

        }

        /// <summary>
        /// Bind data to dropdowlist
        /// </summary>
        private void Bind_GVChuNhiem()
        {

            var list = dbStaff.ListGiaoVienS4M(13);
            if (list.Count > 0)
            {
                ddlGVChuNhiem.Items.Add(new ListItem("Chọn giáo viên chủ nhiệm", "0"));
                for (int i = 0; i < list.Count; i++)
                {
                    ddlGVChuNhiem.Items.Add(new ListItem(list[i].Fullname, list[i].Id.ToString()));
                }
                ddlGVChuNhiem.DataBind();
            }
        }
        private void Bind_GVDiemCat()
        {
            var list = dbStaff.ListGiaoVienS4M(14);
            if (list.Count > 0)
            {
                ddlGVDiemCat.Items.Add(new ListItem("Chọn giáo viên điểm cắt", "0"));
                for (int i = 0; i < list.Count; i++)
                {
                    ddlGVDiemCat.Items.Add(new ListItem(list[i].Fullname, list[i].Id.ToString()));
                }
                ddlGVDiemCat.DataBind();
            }
        }
        private void Bind_GVHoiQuan()
        {
            var list = dbStaff.ListGiaoVienS4M(15);
            if (list.Count > 0)
            {
                ddlGVHoiQuan.Items.Add(new ListItem("Chọn giáo viên hội quán", "0"));
                for (int i = 0; i < list.Count; i++)
                {
                    ddlGVHoiQuan.Items.Add(new ListItem(list[i].Fullname, list[i].Id.ToString()));
                }
                ddlGVHoiQuan.DataBind();
            }
        }

        /// <summary>
        ///  set phân quyền 
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //if (Request.QueryString["Code"] != null)
                //{
                //    PageID = "S4M_LH_EDIT";
                //}
                //else
                //{
                //    PageID = "S4M_LH_ADD";
                //}
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
    }
}