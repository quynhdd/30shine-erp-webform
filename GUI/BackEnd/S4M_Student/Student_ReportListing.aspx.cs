﻿using _30shine.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using System.Web.Services;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Web.Script.Serialization;
using DocumentFormat.OpenXml.Office2010.Excel;

namespace _30shine.GUI.BackEnd.S4M_Student
{
    public partial class Student_ReportListing : System.Web.UI.Page
    {
        protected bool HasImages = false;
        protected List<string> ListImagesUrl = new List<string>();
        protected string[] ListImagesName;
        private static readonly CultureInfo culture = new CultureInfo("vi-VN");
        private string PageID = "S4M_Export";
        private bool Perm_Access = false;
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        private bool Perm_ViewAllData = false;
        protected Paging Paging = new Paging();
        protected static Stylist4Men_GraduationScore OBJ;
        protected static Stylist4Men_PointClubs OBJPointClubs;
        private bool Speedless25p = false;
        private bool between2530 = false;
        private bool between3035 = false;
        private bool better40p = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                Library.Function.bind_S4M_Class(new List<DropDownList>() { ddlClass });
                Library.Function.bindSalonHoiQuan(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
            }
        }


        /// <summary>
        /// Bind data to rpt report
        /// </summary>
        /// <returns></returns>
        private void BindData()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {

                    int integer;
                    int classId = int.TryParse(ddlClass.SelectedValue, out integer) ? integer : 0;
                    int isApccept = int.TryParse(ddlIsAccept.SelectedValue, out integer) ? integer : 0;
                    int statusTuition = int.TryParse(ddlstatusTuition.SelectedValue, out integer) ? integer : 0;
                    int salonId = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                    Speedless25p = ddlSpeedCut.SelectedValue == "Speedless25p" ? true : false;
                    between2530 = ddlSpeedCut.SelectedValue == "between2530" ? true : false;
                    between3035 = ddlSpeedCut.SelectedValue == "between3035" ? true : false;
                    better40p = ddlSpeedCut.SelectedValue == "better40p" ? true : false;
                    //var list = db.Store_S4MStudent_GetList(pageNumber, rowspPage, salonId, classId, isApccept, statusTuition).ToList();
                    var listTemp = (from student in db.Stylist4Men_Student
                                    join clas in db.Stylist4Men_Class on student.ClassId equals clas.Id into a
                                    from clas in a.DefaultIfEmpty()
                                    join package in db.Stylist4Men_StudyPackage on student.StudyPackageId equals package.Id
                                    join staff in db.Staffs on student.Id equals staff.StudentId into b
                                    from staff in b.DefaultIfEmpty()
                                    join salon in db.Tbl_Salon on staff.SalonId equals salon.Id into c
                                    from salon in c.DefaultIfEmpty()
                                    join graduation in db.Stylist4Men_GraduationScore on staff.Id equals graduation.StaffId into d
                                    from graduation in d.DefaultIfEmpty()
                                    where student.IsDelete == false &&
                                          student.IsAccept == true &&
                                          (student.SalonId == salonId || salonId == 0) &&
                                          (student.StatusTuition == statusTuition || statusTuition == 2) &&
                                          (student.ClassId == classId || classId == 0) &&
                                          (graduation.StatusId == isApccept || isApccept == 0)

                                    select new studentStatisticTemp
                                    {
                                        StudentId = student.Id,
                                        StaffId = staff.Id,
                                        Fullname = student.Fullname,
                                        Email = student.Email,
                                        Name = clas.Name,
                                        NamePackage = package.NamePackage,
                                        StatusTuition = student.StatusTuition ?? 0,
                                        NameSalon = salon.ShortName,
                                        PointLTCat = student.PointLTCat ?? 0,
                                        PointLTKN = student.PointLTKN ?? 0,
                                        Vote = student.Vote ?? 0,
                                        PointTheoryCut = student.PointTheoryCut ?? 0,
                                        TienDaDong = student.TotalAmountCollected ?? 0,
                                        TotalBill = student.TotalBill ?? 0,
                                        attr = student.TotalBill < 100 ? "disabled" : "",
                                        CongNo = (float)(student.TotalAmountPaid ?? 0) - (float)(student.TotalAmountCollected ?? 0),
                                        TotalBillSalonHoiQuan = 0,
                                        attr1 = "disabled",
                                        StatusId = graduation.StatusId,
                                        IsSalonHoiQuan = salon.IsSalonHoiQuan ?? false

                                    });

                    int totalListTemp = listTemp.ToList().Count();
                    if (totalListTemp > 0)
                    {
                        if (!Speedless25p && !between2530 && !between3035 && !better40p)
                        {
                            Bind_Paging(listTemp.Count());
                            rpt.DataSource = CalulatorInforStudent(listTemp.OrderBy(r => r.Fullname).Skip(Paging._Offset).Take(Paging._Segment).ToList());
                            rpt.DataBind();
                        }
                        else
                        {
                            var temp = CalulatorInforStudent(listTemp.ToList());
                            Bind_Paging(temp.Count);
                            rpt.DataSource = temp.Skip(Paging._Offset).Take(Paging._Segment).ToList();
                            rpt.DataBind();
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                //Push thông báo cho mọi người (by slack) khi có lỗi xảy ra
                new Helpers.PushNotiErrorToSlack().Push("GA30CQZPF",
                                                        ex.Message + ", " + ex.InnerException,
                                                        this.ToString() + ".BindData",
                                                        "phucdn",
                                                        "ERP");

                WaringClient("Đã có lỗi xảy ra vui lòng liên hệ nhà phát triển!");
            }
        }
        /// <summary>
        /// Gửi thông tin cảnh báo về client
        /// </summary>
        /// <param name="message"></param>
        public void WaringClient(string message)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Click", " ShowWarning('" + message + "',0);", true);
        }


        /// <summary>
        /// tính toán thêm thông tin cho học viên như tổng bill 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private List<studentStatisticTemp> CalulatorInforStudent(List<studentStatisticTemp> list)
        {
            var result = new List<studentStatisticTemp>();


            using (var db = new Solution_30shineEntities())
            {
                if (list.Count > 0)
                {
                    foreach (var temp in list)
                    {
                        if (temp.IsSalonHoiQuan)
                        {
                            var listBillClub = (from a in db.BillServices
                                                 join b in db.Tbl_Salon on a.SalonId equals b.Id
                                                 where a.IsDelete == 0 &&
                                                       a.Pending == 0 &&
                                                       a.Staff_Hairdresser_Id == temp.StaffId &&
                                                       b.IsDelete == 0 &&
                                                       b.Publish == true &&
                                                       b.IsSalonHoiQuan == true
                                                 select new objBill
                                                 {
                                                     BillId = a.Id,
                                                     CompleteBill = a.CompleteBillTime,
                                                     UploadImageTime = a.UploadImageTime,
                                                     ServiceIds = a.ServiceIds
                                                 });
                           
                            if (listBillClub.Count() > 0)
                            {
                                // tính tổng đầu nhuộm, uốn
                                int uon = 0;
                                int nhuom = 0;
                                var listServiceIds = listBillClub.Select(r => r.ServiceIds.Trim());
                                if (listServiceIds.Count() > 0)
                                {
                                    foreach (var service in listServiceIds)
                                    {
                                        if (service != null && service != "")
                                        {
                                            var objServices = new JavaScriptSerializer().Deserialize<List<ServiceBasic>>(service.Trim());
                                            if (objServices.Count > 0)
                                            {
                                                foreach (var i in objServices)
                                                {
                                                    if (i.Id == 71)
                                                    {
                                                        uon++;
                                                    }
                                                    else if (i.Id == 72)
                                                    {
                                                        nhuom++;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                temp.Uon = uon;
                                temp.Nhuom = nhuom;
                                var listBillId = listBillClub.Select(r => r.BillId );

                                temp.TotalBillSalonHoiQuan = listBillClub.Count();
                                var listHairStyle = db.Customer_HairMode_Bill.Where(r => listBillId.Contains(r.BillId ?? 0) &&
                                                                                                                r.IsDelete == false &&
                                                                                                                (r.HairStyleId == 15 ||
                                                                                                                r.HairStyleId == 16 ||
                                                                                                                r.HairStyleId == 7))
                                                                                                                .Select(r => new objHairStyle
                                                                                                                {
                                                                                                                    BillId = r.BillId ?? 0,
                                                                                                                    HairStyleId = r.HairStyleId ?? 0,
                                                                                                                    CreateDate = r.CreateDate
                                                                                                                });
                                if (listHairStyle.Count() > 0)
                                {
                                    int speedCutClassic = 0;
                                    int speedCutUnder = 0;
                                    int speedCutSport = 0;

                                    var listClubClassic = listHairStyle.Where(r => r.HairStyleId == 15).OrderByDescending(r => r.CreateDate);
                                    if (listClubClassic.Count() > 0)
                                    {
                                        //Tổng bill classic
                                        temp.TotalClubClassic = listClubClassic.Count();
                                        //Tốc độ cắt lần gần nhất classic
                                        var recordClubClassic = listClubClassic.FirstOrDefault();
                                        var recordBillClassic = listBillClub.Where(r => r.BillId == recordClubClassic.BillId).FirstOrDefault();
                                        speedCutClassic = GetSpeedCut(recordBillClassic, recordClubClassic);
                                        temp.SpeedCutClassicClub = speedCutClassic;
                                    }

                                    var listClubUnder = listHairStyle.Where(r => r.HairStyleId == 16).OrderByDescending(r => r.CreateDate);
                                    if (listClubUnder.Count() > 0)
                                    {
                                        //Tổng bill Under
                                        temp.TotalClubUnder = listClubUnder.Count();
                                        //Tốc độ cắt lần gần nhất Under
                                        var recordClubUnder = listClubUnder.FirstOrDefault();
                                        var recordBillUnder = listBillClub.Where(r => r.BillId == recordClubUnder.BillId).FirstOrDefault();
                                        speedCutUnder = GetSpeedCut(recordBillUnder, recordClubUnder);
                                        temp.SpeedCutUnderClub = speedCutUnder;
                                    }
                                    var listClubSport = listHairStyle.Where(r => r.HairStyleId == 7).OrderByDescending(r => r.CreateDate);
                                    if (listClubSport.Count() > 0)
                                    {
                                        //Tổng bill Sport
                                        temp.TotalClubSport = listClubSport.Count();
                                        //Tốc độ cắt lần gần nhất Sport
                                        var recordClubSport = listClubSport.FirstOrDefault();
                                        var recordBillSport = listBillClub.Where(r => r.BillId == recordClubSport.BillId).FirstOrDefault();
                                        speedCutUnder = GetSpeedCut(recordBillSport, recordClubSport);
                                        temp.SpeedSportClub = speedCutUnder;
                                    }
                                    //Tốc độ cắt trung bình 3 kiểu đầu
                                    float averageSpeed = (speedCutClassic + speedCutUnder + speedCutSport) / 3;
                                    if (Speedless25p)
                                    {
                                        if (averageSpeed < 25)
                                        {
                                            result.Add(temp);
                                        }
                                    }
                                    else if (between2530)
                                    {
                                        if (averageSpeed > 25 && averageSpeed <= 30)
                                        {
                                            result.Add(temp);
                                        }
                                    }
                                    else if (between3035)
                                    {
                                        if (averageSpeed > 30 && averageSpeed <= 35)

                                        {
                                            result.Add(temp);
                                        }
                                    }
                                    else if (better40p)
                                    {
                                        if (averageSpeed > 40)
                                        {
                                            result.Add(temp);
                                        }
                                    }
                                    else
                                    {
                                        result.Add(temp);
                                    }
                                }
                            }
                        }
                        // Nếu là Học viên
                        else if (!Speedless25p && !between2530 & !between3035 & !better40p)
                        {
                            var listBillFree = db.Stylist4Men_BillCutFree.Where(r => r.StudentId == temp.StudentId)
                                                                         .Select(r => new objBill
                                                                         {
                                                                             CreateDate = r.CreatedTime,
                                                                             UploadImageTime = r.UploadImageTime,
                                                                             BillId = r.Id,
                                                                             HairStyleId = r.HairStyleId ?? 0
                                                                         });
                            //var listBillFree = listbillFreeTest.Where(r => r.StudentId == temp.StudentId).ToList();
                            if (listBillFree.Count() > 0)
                            {
                                var recordFreeClassic = listBillFree.Where(r => r.HairStyleId == 15).OrderByDescending(r => r.CreateDate);
                                if (recordFreeClassic.Count() > 0)
                                {
                                    //Tổng bill classic
                                    temp.TotalStudentClassic = recordFreeClassic.Count();
                                    //Tốc độ cắt lần gần nhất classic
                                    var recordStudentClassic = recordFreeClassic.FirstOrDefault();
                                    temp.SpeedCutClassicClub = GetSpeedCut(recordStudentClassic);
                                }
                                //Kiểu tóc Under
                                var recordFreeUnder = listBillFree.Where(r => r.HairStyleId == 16).OrderByDescending(r => r.CreateDate);
                                if (recordFreeUnder.Count() > 0)
                                {
                                    //Tổng bill classic
                                    temp.TotalStudentUnder = recordFreeUnder.Count();
                                    //Tốc độ cắt lần gần nhất classic
                                    var recordStudentUnder = recordFreeUnder.FirstOrDefault();
                                    temp.SpeedCutUnderStudent = GetSpeedCut(recordStudentUnder);
                                }
                                //Kiểu tóc sport
                                var recordFreeSport = listBillFree.Where(r => r.HairStyleId == 7).OrderByDescending(r => r.CreateDate).ToList();
                                if (recordFreeSport.Count > 0)
                                {
                                    //Tổng bill classic
                                    temp.TotalStudentSport = recordFreeSport.Count;
                                    //Tốc độ cắt lần gần nhất classic
                                    var recordStudentSport = recordFreeSport.FirstOrDefault();
                                    temp.SpeedCutClassicClub = GetSpeedCut(recordStudentSport);
                                }
                            }
                            result.Add(temp);
                        }
                    }
                }
                db.Dispose();
            }
            return result;
        }
        /// <summary>
        /// Trả về tốc độ cắt cho hội quán
        /// </summary>
        /// <param name="objBill"></param>
        /// <param name="objHairStyle"></param>
        /// <returns></returns>
        private int GetSpeedCut(objBill objBill, objHairStyle objHairStyle)
        {
            int speedCut = 0;
            // Nếu bill có hóa chất
            if (objBill.CompleteBill != null)
            {
                if (objHairStyle.CreateDate != null)
                {
                    int createHairStyleTime = objHairStyle.CreateDate.Value.Hour * 60 + objHairStyle.CreateDate.Value.Minute;
                    int completeBillTime = objBill.CompleteBill.Value.Hour * 60 + objBill.CompleteBill.Value.Minute;
                    speedCut = ((completeBillTime - createHairStyleTime) <= 0 ? -5 : (completeBillTime - createHairStyleTime)) + 5;
                }
            }
            //nếu không có hóa chất
            else
            {
                if (objHairStyle.CreateDate != null && objBill.UploadImageTime != null)
                {
                    int createHairStyleTime = objHairStyle.CreateDate.Value.Hour * 60 + objHairStyle.CreateDate.Value.Minute;
                    int uploadImageTime = objBill.UploadImageTime.Value.Hour * 60 + objBill.UploadImageTime.Value.Minute;
                    speedCut = uploadImageTime - createHairStyleTime;
                    speedCut = speedCut <= 0 ? 0 : speedCut;
                }
            }
            return speedCut;
        }
        /// <summary>
        /// trả về tốc độ cắt cho học viên
        /// </summary>
        /// <param name="objBill"></param>
        /// <returns></returns>
        private int GetSpeedCut(objBill objBill)
        {
            int speedCut = 0;
            if (objBill.CreateDate != null && objBill.UploadImageTime != null)
            {
                int createHairStyleTime = objBill.CreateDate.Value.Hour * 60 + objBill.CreateDate.Value.Minute;
                int uploadImageTime = objBill.UploadImageTime.Value.Hour * 60 + objBill.UploadImageTime.Value.Minute;
                speedCut = (uploadImageTime - createHairStyleTime) <= 0 ? 0 : (uploadImageTime - createHairStyleTime);
            }
            return speedCut;
        }
        /// <summary>
        /// lấy toàn bộ học viên 
        /// </summary>
        /// <param name="salonId"></param>
        /// <param name="classId"></param>
        /// <param name="isAccept"></param>
        /// <param name="statusTution"></param>
        /// <returns></returns>
        private IEnumerable<studentStatisticTemp> GetAllStudent(int salonId, int classId, int isAccept, int statusTution)
        {
            IEnumerable<studentStatisticTemp> list;
            using (var db = new Solution_30shineEntities())
            {
                list = (from student in db.Stylist4Men_Student
                        join clas in db.Stylist4Men_Class on student.ClassId equals clas.Id into a
                        from clas in a.DefaultIfEmpty()
                        join package in db.Stylist4Men_StudyPackage on student.StudyPackageId equals package.Id
                        join staff in db.Staffs on student.Id equals staff.StudentId into b
                        from staff in b.DefaultIfEmpty()
                        join salon in db.Tbl_Salon on staff.SalonId equals salon.Id into c
                        from salon in c.DefaultIfEmpty()
                        join graduation in db.Stylist4Men_GraduationScore on staff.Id equals graduation.StaffId into d
                        from graduation in d.DefaultIfEmpty()
                        where student.IsDelete == false &&
                              student.IsAccept == true &&
                              (student.SalonId == salonId || salonId == 0) &&
                              (student.StatusTuition == statusTution || statusTution == 2) &&
                              (student.ClassId == classId || classId == 0) &&
                              (graduation.StatusId == isAccept || isAccept == 0)

                        select new studentStatisticTemp
                        {
                            StudentId = student.Id,
                            StaffId = staff.Id,
                            Fullname = student.Fullname,
                            Email = student.Email,
                            Name = clas.Name,
                            NamePackage = package.NamePackage,
                            StatusTuition = student.StatusTuition ?? 0,
                            NameSalon = salon.ShortName,
                            PointLTCat = student.PointLTCat ?? 0,
                            PointLTKN = student.PointLTKN ?? 0,
                            Vote = student.Vote ?? 0,
                            PointTheoryCut = student.PointTheoryCut ?? 0,
                            TienDaDong = student.TotalAmountCollected ?? 0,
                            TotalBill = student.TotalBill ?? 0,
                            attr = student.TotalBill < 100 ? "disabled" : "",
                            CongNo = (float)(student.TotalAmountPaid ?? 0) - (float)(student.TotalAmountCollected ?? 0),
                            TotalBillSalonHoiQuan = 0,
                            attr1 = "disabled",
                            StatusId = graduation.StatusId,
                            IsSalonHoiQuan = salon.IsSalonHoiQuan ?? false

                        });
                //db.Dispose();
            }
            return list;
        }
        /// <summary>
        /// Update point LTCat, LTKN, Vote
        /// </summary>
        /// <param name="LTKN"></param>
        /// <param name="Vote"></param>
        /// <returns></returns>
        [WebMethod]
        public static object UpdatePoint(int? Id, int? LTKN, int? Vote, int? PointTheoryCut)
        {
            try
            {
                var message = new Library.Class.cls_message();
                var exc = 0;
                using (var db = new Solution_30shineEntities())
                {
                    if (Id != null)
                    {
                        var data = new Stylist4Men_Student();
                        data = (from c in db.Stylist4Men_Student
                                where c.Id == Id
                                select c).FirstOrDefault();
                        //data.PointLTCat = LTCat;
                        if (data != null)
                        {
                            data.PointLTKN = LTKN;
                            data.Vote = Vote;
                            data.PointTheoryCut = PointTheoryCut;
                        }
                        exc = db.SaveChanges();
                    }
                    if (exc > 0)
                        message.success = true;
                    else
                        message.success = false;

                }
                return message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Update point tốt nghiệp theo từng học viên
        /// </summary>
        /// <param name="LTCat"></param>
        /// <param name="LTHC"></param>
        /// <param name="THCat"></param>
        /// <param name="THHC"></param>
        /// <returns></returns>
        [WebMethod]
        public static object UpdatePoint30Shine(int? staffId, int LTCat, int LTHC, int THCat, int THHC, int StatusId)
        {
            try
            {
                var message = new Library.Class.cls_message();
                var exc = 0;
                using (var db = new Solution_30shineEntities())
                {
                    if (staffId > 0)
                    {

                        OBJ = (from c in db.Stylist4Men_GraduationScore
                               where c.StaffId == staffId && c.IsDelete == false && c.Publish == true
                               select c).FirstOrDefault();
                        if (OBJ == null)
                        {
                            var obj = new Stylist4Men_GraduationScore();
                            obj.StaffId = staffId;
                            obj.PointTheoryCut = LTCat;
                            obj.PointPracticeCut = THCat;
                            obj.PointTheoryChemistry = LTHC;
                            obj.PointPacticeChemistry = THHC;
                            obj.StatusId = StatusId;
                            obj.CreatedTime = DateTime.Now;
                            obj.IsDelete = false;
                            obj.Publish = true;
                            db.Stylist4Men_GraduationScore.Add(obj);
                        }
                        else
                        {
                            OBJ.StaffId = staffId;
                            OBJ.PointTheoryCut = LTCat;
                            OBJ.PointPracticeCut = THCat;
                            OBJ.PointTheoryChemistry = LTHC;
                            OBJ.PointPacticeChemistry = THHC;
                            OBJ.StatusId = StatusId;
                            OBJ.ModifiedTime = DateTime.Now;
                            OBJ.IsDelete = false;
                            OBJ.Publish = true;
                            db.Stylist4Men_GraduationScore.AddOrUpdate(OBJ);
                        }
                        exc = db.SaveChanges();
                        if (exc > 0)
                            message.success = true;
                        else
                            message.success = false;
                    }
                }
                return message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Update Point tốt nghiệp hội quán
        /// 1. Nếu tài khoản của học viên được tạo ở Salon Hội Quán rồi, khi có StaffId thì thực hiện insert điểm của học viên
        /// 2. Nếu đã tồn tại điểm của học viên có thể thực hiện sửa dựa vào StaffId
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="PointDuongNet"></param>
        /// <param name="PointHinhKhoi"></param>
        /// <param name="PointSaySap"></param>
        /// <param name="PointFade"></param>
        /// <param name="PointTatoo"></param>
        /// <param name="PointUon"></param>
        /// <param name="PointNhuom"></param>
        /// <returns></returns>
        [WebMethod]
        public static object UpdatePointTNHoiQuan(int StaffId, int IdCredit, string Images, int? PointTheory, int? PointVote)
        {
            try
            {

                var message = new Library.Class.cls_message();
                var exc = 0;
                using (var db = new Solution_30shineEntities())
                {
                    if (StaffId > 0 && IdCredit > 0)
                    {
                        OBJPointClubs = (from c in db.Stylist4Men_PointClubs
                                         where c.StaffId == StaffId && c.CreditsId == IdCredit
                                         select c
                                      ).FirstOrDefault();
                        var obj = new Stylist4Men_PointClubs();
                        obj.StaffId = StaffId;
                        obj.CreditsId = IdCredit;
                        if (OBJPointClubs == null)
                        {
                            obj.PointTheory = PointTheory;
                            obj.PointVote = PointVote;
                            obj.Images = Images;
                            obj.IsDelete = false;
                            obj.Publish = true;
                            obj.CreatedTime = DateTime.Now;
                            db.Stylist4Men_PointClubs.Add(obj);
                        }
                        else
                        {
                            OBJPointClubs.PointTheory = PointTheory;
                            OBJPointClubs.PointVote = PointVote;
                            OBJPointClubs.Images = Images;
                            OBJPointClubs.ModifiedTime = DateTime.Now;
                            db.Stylist4Men_PointClubs.AddOrUpdate(OBJPointClubs);
                        }
                        exc = db.SaveChanges();
                        if (exc > 0)
                            message.success = true;
                        else
                            message.success = false;

                    }
                }

                return message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod]
        public static object UpdateImagesStudent(int Id, string image1)
        {
            try
            {
                var message = new Library.Class.cls_message();
                var error = 0;
                using (var db = new Solution_30shineEntities())
                {
                    var obj = new Stylist4Men_Student();
                    if (Id > 0)
                    {

                        obj = (from c in db.Stylist4Men_Student
                               where c.Id == Id
                               select c).FirstOrDefault();
                        obj.Images = image1;
                        error = db.SaveChanges();

                        if (error > 0)
                            message.success = true;
                        else
                            message.success = false;

                    }
                    return message;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Bind data to point + image hội quán theo từng học viên khi lên hội quan
        /// </summary>
        /// <param name="staffId"></param>
        /// <returns></returns>
        [WebMethod]
        public static string BindDataHoiQuanStudent(int staffId)
        {
            try
            {
                var serila = new JavaScriptSerializer();
                using (var db = new Solution_30shineEntities())
                {
                    //var list = db.Stylist4Men_Credits.ToList().OrderBy(o => o.Id);

                    if (staffId > 0)
                    {
                        var list = (from a in db.Stylist4Men_Credits
                                    join b in db.Stylist4Men_PointClubs on a.Id equals b.CreditsId
                                    into c
                                    from d in c.DefaultIfEmpty()
                                    where d.StaffId == staffId
                                    select new
                                    {
                                        Id = a.Id,
                                        Name = a.Name,
                                        PointTheory = d.PointTheory,
                                        PointVote = d.PointVote,
                                        Images = d.Images,
                                        StaffId = d.StaffId,
                                        CreditsId = d.CreditsId

                                    }).ToList().OrderBy(o => o.Id);
                        return serila.Serialize(list);
                    }

                }

                return "";
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Bind ảnh học viên 
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        [WebMethod]
        public static string BindDataImageStudent(int studentId)
        {
            var serila = new JavaScriptSerializer();
            try
            {

                using (var db = new Solution_30shineEntities())
                {
                    var data = new Stylist4Men_Student();
                    if (studentId > 0)
                    {
                        data = db.Stylist4Men_Student.FirstOrDefault(a => a.Id == studentId);
                    }
                    return serila.Serialize(data);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Author: Phúc
        /// Bổ sung bind data từ bảng Credits
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static string InitalBlockPoint()
        {
            var serila = new JavaScriptSerializer();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    //var list = db.Stylist4Men_Credits.Where(o => o.IsDelete == false && o.Publish == true).Select(o => new { Id = o.Id, Name = o.Name }).ToList(); 
                    //List<string> khoi = new List<string>() {"Khoi" };
                    var list = (from a in db.Stylist4Men_Credits
                                where a.IsDelete == false && a.Publish == true
                                select new
                                {
                                    Id = a.Id,
                                    btnId = "",
                                    nameBlock = "",
                                    name = a.Name,
                                    images = "",
                                    pointLT = 0,
                                    pointVote = 0
                                }).ToList().OrderBy(o => o.Id);

                    if (list != null)
                        return serila.Serialize(list);
                    else
                        return "";
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var msgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", msgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            rpt.DataSource = "";
            rpt.DataBind();
            BindData();
            RemoveLoading();
        }
        protected void Bind_Paging(int totalRow)
        {
            Paging._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : Paging._Segment;
            Paging._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            Paging._PageNumber = Paging._PageNumber > 0 ? Paging._PageNumber : 1;
            Paging.TotalPage = !Paging.TotalPage.Equals(null) ? Get_TotalPage(totalRow) : Paging.TotalPage;
            Paging._Offset = (Paging._PageNumber - 1) * Paging._Segment;
            Paging._Paging = Paging.Make_Paging();
            RptPaging.DataSource = Paging._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int totalRow)
        {
            totalRow -= Paging._TopNewsNum;
            int returnTotalPage = Convert.ToInt32(Math.Ceiling((double)totalRow / Paging._Segment));
            return returnTotalPage >= 0 ? returnTotalPage : 0;
        }
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        protected void ddlSalon_SelectedIndexChanged(object sender, EventArgs e)
        {
            Library.Function.bind_S4M_Class(new List<DropDownList>() { ddlClass });

        }

        protected void ddlClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            Library.Function.bindSalonHoiQuan(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
        }
        public class objBill
        {
            public int StudentId { get; set; }
            public int staffId { get; set; }
            public DateTime? CreateDate { get; set; }
            public int BillId { get; set; }
            public DateTime? CompleteBill { get; set; }
            public DateTime? UploadImageTime { get; set; }
            // kiểu tóc áp dụng cho học viên
            public int HairStyleId { get; set; }
            public string ServiceIds { get; set; }
        }
        public class objHairStyle
        {
            public int BillId { get; set; }
            public DateTime? CreateDate { get; set; }
            public int HairStyleId { get; set; }
        }
        public class ServiceBasic
        {
            public int Id { get; set; }
            /// <summary>
            /// Mã sản phẩm
            /// </summary>
            public string Code { get; set; }
            /// <summary>
            /// Tên sản phẩm
            /// </summary>
            public string Name { get; set; }
            /// <summary>
            /// Giá gốc
            /// </summary>
            public int Cost { get; set; }
            /// <summary>
            /// Giá bán
            /// </summary>
            public int Price { get; set; }
            /// <summary>
            /// Số lượng
            /// </summary>
            public int Quantity { get; set; }
            /// <summary>
            /// % khuyến mãi
            /// </summary>
            public int VoucherPercent { get; set; }
        }
        public class studentStatisticTemp
        {
            public int StudentId { get; set; }
            public int? StaffId { get; set; }
            public string Fullname { get; set; }
            public string Email { get; set; }
            public string Name { get; set; }
            public string NamePackage { get; set; }
            public int StatusTuition { get; set; }
            public string NameSalon { get; set; }
            public int PointLTCat { get; set; }
            public int PointLTKN { get; set; }
            public int Vote { get; set; }
            public int PointTheoryCut { get; set; }
            public int TienDaDong { get; set; }
            public int TotalBill { get; set; }
            public string attr { get; set; }
            public float CongNo { get; set; }
            public int TotalBillSalonHoiQuan { get; set; }
            public string attr1 { get; set; }
            public int? StatusId { get; set; }
            public bool IsSalonHoiQuan { get; set; }
            //Tổng số đầu classic cho học viên
            public int TotalStudentClassic { get; set; }
            // Tốc độ cắt lần gần nhất classic
            public int SpeedCutClassicStudent { get; set; }
            //Tổng số đầu Under cho học viên
            public int TotalStudentUnder { get; set; }
            // Tốc độ cắt lần gần nhất under
            public int SpeedCutUnderStudent { get; set; }
            //Tổng số đầu Sport cho học viên
            public int TotalStudentSport { get; set; }
            // Tốc độ cắt lần gần nhát sport
            public int SpeedCutSportStudent { get; set; }
            //Tổng số đầu classic cho hội quán
            public int TotalClubClassic { get; set; }
            // Tốc độ cắt lần gần nhất classic
            public int SpeedCutClassicClub { get; set; }
            //Tổng số đầu Under cho hội quán
            public int TotalClubUnder { get; set; }
            // tốc độ cắt lần gần nhất under
            public int SpeedCutUnderClub { get; set; }
            //Tổng số đầu Sport cho hội quán
            public int TotalClubSport { get; set; }
            // Tốc độ cắt lần gần nhất sport
            public int SpeedSportClub { get; set; }
            public int Uon { get; set; }
            public int Nhuom { get; set; }

        }

    }
}