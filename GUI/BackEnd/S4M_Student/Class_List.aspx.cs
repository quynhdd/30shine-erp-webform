﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.Interface;
using _30shine.Helpers;
using _30shine.MODEL.IO;
using _30shine.MODEL.BO;
using System.Web.Services;
using System.Web.Script.Serialization;

namespace _30shine.GUI.BackEnd.S4M_Student
{
    public partial class Class_List : System.Web.UI.Page
    {

        private string PageID = "S4M_LH";
        private bool Perm_Access = false;
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        static IS4MClassModel dbS4mClass = new S4MClassModel();
        IStaffModel dbStaff = new StaffModel();
        protected Paging PAGING = new Paging();
       
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                Bind_RptClass();

                RemoveLoading();
            }
        }
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                ////Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //////Perm_Add = permissionModel.GetActionByActionNameAndPageId("Perm_Add", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        /// <summary>
        /// Bind data to DropDownlist giáo viên điểm cắt
        /// Author: Như Phúc
        /// <param name="id"></param>
        /// <returns></returns>
        public string Bind_GVDiemCat(int id)
        {
            var list = dbStaff.ListGiaoVienS4M(14);
            if (list.Count > 0)
            {
                var nameGVdiemcat = "";
                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].Id == id)
                    {
                        nameGVdiemcat = list[i].Fullname;
                        return nameGVdiemcat;
                    }
                }
            }
            return "";
        }

        /// <summary>
        /// Bind data to DropDownlist giáo viên chủ nhiệm
        /// Author: Như Phúc
        /// <param name="id"></param>
        /// <returns></returns>
        public string Bind_GVChuNhiem(int id)
        {
            var list = dbStaff.ListGiaoVienS4M(13);
            var nameGVdiemcat = "";
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Id == id)
                {
                    nameGVdiemcat = list[i].Fullname;
                    return nameGVdiemcat;
                }
            }
            return "";
        }

        /// <summary>
        /// Bind data to DropDownlist giáo viên hội quán
        /// Author: Như Phúc
        /// <param name="id"></param>
        /// <returns></returns>
        public string Bind_GVHoiQuan(int id)
        {
            var list = dbStaff.ListGiaoVienS4M(15);
            if (list.Count > 0)
            {
                var nameGVdiemcat = "";
                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].Id == id)
                    {
                        nameGVdiemcat = list[i].Fullname;
                        return nameGVdiemcat;
                    }
                }
            }
            return "";
        }
        protected void _BtnClick(object sender, EventArgs e)
        {

            Bind_RptClass();
            RemoveLoading();
        }

        private void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Bind data to rpt
        /// </summary>
        private void Bind_RptClass()
        {
            var list = dbS4mClass.AllList();
            if (list.Count > 0)
            {
                Bind_Paging(list.Count);
                RptClass.DataSource = list.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                RptClass.DataBind();
            }
        }


        protected void Bind_Paging(int TotalRow)
        {

            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }
    }
}