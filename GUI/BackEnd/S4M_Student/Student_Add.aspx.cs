﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Security.Cryptography;
using System.Text;
using _30shine.MODEL.BO;
using _30shine.MODEL.IO;

namespace _30shine.GUI.BackEnd.S4M_Student
{
    public partial class Student_Add : System.Web.UI.Page
    {
        private bool Perm_Access = false;
        protected bool Perm_ViewAllData = false;
        protected bool HasImages = false;
        protected List<string> ListImagesUrl = new List<string>();
        protected string[] ListImagesName;
        private string PageID = "";
        protected static Stylist4Men_Student OBJ;
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //if (Request.QueryString["Code"] != null)
                //{
                //    PageID = "S4M_HV_EDIT";
                //}
                //else
                //{
                //    PageID = "S4M_HV_ADD";
                //}
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {

            SetPermission();
            if (!IsPostBack)
            {
                FillTinhThanh();
                FillQuanHuyen();
                ClassS4M();
                StudyPackegAll();
                Library.Function.bindSalonHoiQuan(new List<DropDownList> { Salon }, Perm_ViewAllData);
            }
        }

        /// <summary>
        /// Bind data to radio button
        /// </summary>
        /// 
        [WebMethod]
        public static object FillRadio()
        {
            try
            {
                ITblStatusModel db = new TblStatusModel();
                var serilaze = new JavaScriptSerializer();
                var list = db.GetList();
                return serilaze.Serialize(list);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Fill Tỉnh thành
        /// </summary>
        private void FillTinhThanh()
        {
            ITinhThanhModel db = new TinhThanhModel();
            var list = db.GetListTinhThanh();
            City.DataTextField = "TenTinhThanh";
            City.DataValueField = "Id";
            City.SelectedIndex = 0;
            City.DataSource = list;
            City.DataBind();
        }

        /// <summary>
        /// Fill Quan huyen
        /// </summary>`
        protected void FillQuanHuyen()
        {
            ITinhThanhModel db = new TinhThanhModel();
            int _TinhThanhID = Convert.ToInt32(City.SelectedValue);
            if (_TinhThanhID != 0)
            {
                var lst = db.GetListQuanHuyen(_TinhThanhID);
                District.DataTextField = "TenQuanHuyen";
                District.DataValueField = "Id";
                District.SelectedIndex = 0;
                District.DataSource = lst;
                District.DataBind();
            }

        }

        /// <summary>
        /// Bind Dropdowlist quận huyện
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlFTinhThanh_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillQuanHuyen();
        }

        [WebMethod]
        public static object fillDistrict(int id)
        {
            try
            {
                var serializer = new JavaScriptSerializer();
                ITinhThanhModel db = new TinhThanhModel();
                var lst = db.GetListQuanHuyen(id);
                return serializer.Serialize(lst);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Data class
        /// </summary>
        private void ClassS4M()
        {
            IS4MClassModel db = new S4MClassModel();
            var list = db.AllList();
            ddlKhoahoc.Items.Add(new ListItem("--- Chọn lớp học ---", "0"));
            if (list.Count > 0)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    ddlKhoahoc.Items.Add(new ListItem(list[i].Name, list[i].Id.ToString()));
                }
                ddlKhoahoc.DataBind();
            }

        }

        /// <summary>
        /// get data gói học
        /// </summary>
        private void StudyPackegAll()
        {
            IS4MStudyPackageModel db = new S4MStudyPackageModel();
            var list = db.GetListAll();
            ddlGoiHoc.Items.Add(new ListItem("---Chọn gói học---", "0"));
            if (list.Count > 0)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    ddlGoiHoc.Items.Add(new ListItem(list[i].NamePackage, list[i].Id.ToString()));
                }
                ddlGoiHoc.DataBind();
            }
        }

        /// <summary>
        /// Bind data to textbox
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [WebMethod]
        public static object BindData(int id)
        {
            try
            {
                IStylist4MenStudentModel db = new Stylist4MenStudentModel();
                var serializer = new JavaScriptSerializer();
                var OBJ = db.GetById(id);
                return serializer.Serialize(OBJ);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Confirm nhập học!
        /// 1. Insert 1 record new in table Stylist_Student
        /// 2. Insert 1 record new Stylist_tuli( bảng lưu trữ thông tin nộp học phí)
        /// 3. Khi update thực hiện kiểm tra trùng tài khoản trong bảng Staff nếu chưa có thì thực hiện tạo account in table Staff
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [WebMethod]
        public static string AddOrUpdate(int Id, string Name, string NumberCMT, string Email, string HocPhi, string HocPhi1, int Khoahoc, int Goihoc, string ImgCMT1, string ImgCMT2, string Phone, int City, int District, string Address, string Note, int radiobttList, int salonID, int hocphiThem1)
        {
            IStylist4MenStudentModel dbStudent = new Stylist4MenStudentModel();
            IStylist4MenTuitionModel dbTuitionModel = new Stylist4MenTuitionModel();
            IStaffModel dbStaffModel = new StaffModel();
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var obj = new Stylist4Men_Student();
            var objTuilion = new Stylist4Men_Tuition();
            obj.Fullname = Name;
            obj.Email = Email;
            obj.NumberCMT = NumberCMT;
            obj.ImageCMT1 = ImgCMT1;
            obj.ImageCMT2 = ImgCMT2;
            obj.Phone = Phone;
            obj.ClassId = Khoahoc;
            obj.DistrictId = District;
            obj.CityId = City;
            obj.Address = Address;
            obj.LevelOfConcern = radiobttList;
            obj.CreatedTime = DateTime.Now;
            obj.IsAccept = true;
            obj.IsDelete = false;
            obj.Note = Note;
            obj.Type = 7;
            obj.TotalAmountPaid = Convert.ToInt32(HocPhi.Replace(",", ""));
            obj.StudyPackageId = Goihoc;
            var totalAmountCollected = Convert.ToInt32(HocPhi1.Replace(",", ""));
            if (Id == 0)
            {
                obj.SalonId = 0;
                if (obj.TotalAmountPaid == hocphiThem1)
                    obj.StatusTuition = 1;
                else
                    obj.StatusTuition = 0;
                obj.TotalAmountCollected = hocphiThem1;
                obj.PointLTCat = 0;
                obj.PointLTKN = 0;
                obj.PointTheoryCut = 0;
                obj.Vote = 0;
                obj.NumberOfCreateProfile = 1;
                obj.Password = "a05b49b7dc71773bc7ff4ef877824ffe";
                obj.TotalBill = 0;
                obj.Publish = true;
                var error = dbStudent.Add(obj);
                var data = dbStudent.GetRecordNew();
                AddTuition(data.Id, data.TotalAmountCollected);
                if (error != null)
                    Msg.success = true;
                else
                    Msg.success = false;
            }
            else
            {

                obj.Id = Id;
                obj.TotalAmountCollected = totalAmountCollected + hocphiThem1;
                if (obj.TotalAmountPaid == obj.TotalAmountCollected)
                    obj.StatusTuition = 1;
                else
                    obj.StatusTuition = 0;
                obj.SalonId = salonID;
                if (hocphiThem1 > 0)
                    AddTuition(Id, hocphiThem1);
                //var checkEmailStaff = dbStaffModel.IssetEmail(Email);
                var checkEmailStaff = CheckIssetEmailStaff(Email);
                // kiểm tra nếu salon Id >0 thì gọi hàm insert account new cho học viên in table Staff
                if (salonID > 0)
                    if (checkEmailStaff != true)
                    {
                        AddStaffAccount(Id, Name, Phone, Email, Address, City, District, Khoahoc, salonID, ImgCMT1, ImgCMT2, NumberCMT);
                        obj.Publish = false;
                    }
                    else
                    {
                        UpdateSalonId(Id, salonID);
                        obj.Publish = false;
                    }
                else
                {
                    obj.Publish = true;
                }
                var error = dbStudent.Update(obj);
                if (error != null)
                    Msg.success = true;
                else
                    Msg.success = false;
            }

            return serializer.Serialize(Msg);
        }

        /// <summary>
        ///  Create account auto in table Staff cho student khi đủ điều kiện lên Salon hội quán
        /// </summary>
        /// <param name="StudentId"></param>
        /// <param name="fullName"></param>
        /// <param name="email"></param>
        /// <param name="Address"></param>
        /// <param name="cityId"></param>
        /// <param name="districtId"></param>
        /// <param name="classId"></param>
        /// <param name="salonId"></param>
        private static void AddStaffAccount(int studentId, string fullName, string Phone, string email, string address, int cityId, int districtId, int classId, int salonId, string imgCmt1, string imgCmt2, string cmtNumber)
        {
            if (studentId > 0)
            {
                IStaffModel db = new StaffModel();
                var obj = new Staff();
                obj.StudentId = studentId;
                obj.Phone = Phone;
                obj.Fullname = fullName;
                obj.Email = email;
                obj.Address = address;
                obj.CityId = cityId;
                obj.DistrictId = districtId;
                obj.S4MClassId = classId;
                obj.SalonId = salonId;
                obj.CreatedDate = DateTime.Now;
                obj.Password = "a05b49b7dc71773bc7ff4ef877824ffe";
                obj.IsDelete = 0;
                obj.Publish = true;
                obj.Active = 1;
                obj.Type = 1;
                obj.IsHoiQuan = true;
                obj.SkillLevel = 0;
                obj.CMTimg1 = imgCmt1;
                obj.CMTimg2 = imgCmt2;
                obj.StaffID = cmtNumber;
                obj.Type = 1;
                db.Add(obj);
            }
        }

        /// <summary>
        /// Update lại SalonID nếu trường hợp đã tồn tại tài khoản học viên trong Staff
        /// </summary>
        /// <param name="studentId"></param>
        /// <param name="salonId"></param>
        private static void UpdateSalonId(int studentId, int salonId)
        {
            using (var db = new Solution_30shineEntities())
            {
                if (studentId > 0)
                {
                    var obj = new Staff();
                    obj = (from c in db.Staffs
                           where c.StudentId == studentId
                           select c).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.Active = 1;
                        obj.SalonId = salonId;
                        obj.Publish = true;
                        obj.IsDelete = 0;
                        obj.IsHoiQuan = true;
                        obj.ModifiedDate = DateTime.Now;
                        db.SaveChanges();
                    }
                }
            }
        }

        /// <summary>
        /// AddTuition 
        /// </summary>
        /// <param name="studenId"></param>
        /// <param name="TotalAmountCollected"></param>
        private static void AddTuition(int studenId, int? TotalAmountCollected)
        {
            try
            {
                if (studenId > 0)
                {
                    IStylist4MenTuitionModel db = new Stylist4MenTuitionModel();
                    var obj = new Stylist4Men_Tuition();
                    obj.StudentId = studenId;
                    obj.AmountCollected = TotalAmountCollected;
                    obj.PayTheMoney = 1;
                    obj.IsDelete = false;
                    obj.Publish = true;
                    obj.CreaetedTime = DateTime.Now;
                    db.Add(obj);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        /// <summary>
        /// Hủy không nhận
        /// 1. Kiem tra Ung vien nay da duoc nhap hay chua
        ///  - Neu chua ton tai UV, tao ban ghi Tiep can, set gia tri = 1
        ///  - Neu Ung vien da ton tai, thi tang gia tri Tiep can + 1
        /// 2. nếu chưa tồn tại bản ghi thì Insert bản ghi mới 
        /// </summary>     
        [WebMethod]
        public static string AddOrUpdateCancel(string Name, string Phone, int City, int District, string Address, string Note, int radiobttList)
        {
            IStylist4MenStudentModel db = new Stylist4MenStudentModel();
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var obj = new Stylist4Men_Student();
            obj.Phone = Phone;
            if (obj != null)
            {
                //check trùng số điện thoại
                var student = db.IssetPhoneNumer(obj.Phone);
                if (student != null)
                {
                    student.NumberOfCreateProfile += 1;
                    var exc = db.UpdateNumberOfCreateProfile(student);
                    if (exc != null)
                        Msg.success = true;
                    else
                        Msg.success = false;
                }
                else
                {
                    obj.Fullname = Name;
                    obj.DistrictId = District;
                    obj.CityId = City;
                    obj.Address = Address;
                    obj.Note = Note;
                    obj.LevelOfConcern = radiobttList;
                    obj.IsAccept = false;
                    obj.IsDelete = false;
                    obj.Publish = true;
                    obj.SalonId = 0;
                    obj.ClassId = 0;
                    obj.StudyPackageId = 0;
                    obj.NumberOfCreateProfile = 1;
                    // if (Id == 0){
                    obj.CreatedTime = DateTime.Now;
                    var error = db.Add(obj);
                    if (error != null)
                        Msg.success = true;
                    else
                        Msg.success = false;
                    //}
                    //else
                    //{

                    //    obj.ModifiedTime = DateTime.Now;
                    //    obj.Id = Id;
                    //    var error = db.Update(obj);
                    //    if (error != null)
                    //        Msg.success = true;
                    //    else
                    //        Msg.success = false;
                    //}
                }
            }
            return serializer.Serialize(Msg);
        }

        /// <summary>
        /// check isset account
        /// </summary>
        /// <param name="Email"></param>
        /// <returns></returns>
        [WebMethod]
        public static object CheckAccount(string Email)
        {
            try
            {
                var message = new Library.Class.cls_message();
                var dataStudent = CheckIssetEmailStudent(Email);
                var dataStaff = CheckIssetEmailStaff(Email);
                if (dataStaff == true || dataStudent == true)
                    message.success = true;
                else
                    message.success = false;
                return message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Check tài khoản xem đã tồn tại ở Staff chưa
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static bool CheckIssetEmailStaff(string email)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var isset = false;
                    var staffObj = new Staff();
                    staffObj = db.Staffs.FirstOrDefault(w => w.Email == email);
                    if (staffObj != null)
                    {
                        isset = true;
                    }

                    return isset;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Check trùng email bang Stylist Student
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static bool CheckIssetEmailStudent(string email)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var kt = false;
                    var data = db.Stylist4Men_Student.FirstOrDefault(w => w.Email == email);
                    if (data != null)
                    {
                        kt = true;
                    }
                    return kt;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Change password
        /// </summary>
        public static MD5 md5Hash;
        [WebMethod]
        public static string ResetPassWord(int? _Id)
        {
            string str = "";
            using (var db = new Solution_30shineEntities())
            {

                var obj = db.Stylist4Men_Student.Where(x => x.Id == _Id && x.IsDelete == false).FirstOrDefault();
                using (MD5 md5Hash = MD5.Create())
                {
                    obj.Password = GenPassword(md5Hash, Libraries.AppConstants.PASSWORD_DEFAULT);
                    db.SaveChanges();
                }
                str = "success";

            }
            return str;
        }
        public static string GenPassword(MD5 md5Hash, string input)
        {
            var scrPush = "uaefsfkoiu&@(^%=";
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input + scrPush));
            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }


    }
}