﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Student_Listing.aspx.cs" Inherits="_30shine.GUI.BackEnd.S4M_Student.Student_Listing" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý Stylist4Men &nbsp;&#187; Học viên &nbsp;&#187;</li>
                        <li class="li-listing active"><a href="/admin/s4m/hoc-vien/danh-sach.html">Danh sách học viên không nhận</a></li>
                        <li class="li-listing active"><a href="/admin/s4m/hoc-vien/bao-cao-diem-hoc-vien.html">Danh sách học viên được nhận</a></li>
                        <li class="li-add"><a href="/admin/s4m/hoc-vien/them-moi.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <span style="display: none"></span>
                <!-- Filter -->
                <div class="row row-filter">
                    <div class="filter-item" style="margin-left: 0;">
                        <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="datepicker-wp">
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                                ClientIDMode="Static" runat="server"></asp:TextBox>

                            <strong class="st-head" style="margin-left: 10px; margin-right: 10px;">
                                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                            </strong>
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeTo" placeholder="Đến ngày"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlClass" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 150px;"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlLevelOfConcern" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 150px;"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlNumberOfCreateProfile" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 150px;">
                            <asp:ListItem Text="Số lần tiếp cận" Value="0"></asp:ListItem>
                            <asp:ListItem Text="1" Value="1"></asp:ListItem>
                            <asp:ListItem Text="2" Value="2"></asp:ListItem>
                            <asp:ListItem Text="3" Value="3"></asp:ListItem>
                            <asp:ListItem Text="4" Value="4"></asp:ListItem>
                            <asp:ListItem Text="5" Value="5"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <%--       <div class="filter-item">
                        <asp:DropDownList ID="ddlIsAccept" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 150px;">
                            <asp:ListItem Text="Trạng thái" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Nhận" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Không nhận" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                    </div>--%>
                    <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                </div>
                <!-- End Filter -->
                <!-- Row Table Filter -->
                <div class="table-func-panel">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="table-wp">
                            <table class="table-add table-listing">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>ID</th>
                                        <th>Học viên</th>
                                        <th>Số điện thoại</th>
                                        <%--                                        <th>Tài khoản</th>
                                        <th>Lớp học</th>--%>
                                        <th>Mức độ quan tâm</th>
                                        <th>Mức độ tiếp cận</th>
                                        <th>Ghi chú</th>
                                        <%--<th>Kết quả</th>--%>
                                        <th>Chức năng</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="Rpt" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td><%# Eval("Id") %></td>
                                                <td>
                                                    <a href="/admin/s4m/hoc-vien/<%# Eval("Id")%>.html"><%# Eval("Fullname") %></a>
                                                </td>
                                                <td><%# Eval("Phone") %></td>
                                                <%--   <td><%# Eval("Email") %></td>
                                                <td><%# Eval("ClassName") %></td>--%>
                                                <td><%# Eval("NameStatus") %></td>
                                                <td style="width: 100px">
                                                    <input type="text" class="NumberId" value="<%# Eval("NumberOfCreateProfile") %>" onchange="UpdateNumberOf($(this), <%#Eval("Id") %>)" style="width: 70px; text-align: center; height: 30px" />
                                                </td>
                                                <td>
                                                    <input type="text" style="width: 95%; text-align: center; height: 50px" class="note" value="<%# Eval("Note") %>" onchange="UpdateNote($(this), <%#Eval("Id") %>)" />
                                                </td>
                                                <td class="map-edit" style="width: 100px">
                                                    <div class="edit-wp">

                                                        <a class="elm edit-btn" href="/admin/s4m/hoc-vien/<%# Eval("Id") %>.html" title="Sửa"></a>
                                                        <a class="elm del-btn" onclick="del(this.parentNode.parentNode.parentNode,'<%# Eval("Id") %>', '<%# Eval("Fullname") %>')" href="javascript://" title="Xóa"></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>

                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
            </div>
            <%-- END Listing --%>
        </div>

        <script>
            jQuery(document).ready(function () {
                $("#glbS4M").addClass("active");
                $("#glbS4MStudent").addClass("active");
            });
            //============================
            // Datepicker
            //============================
            $('.txtDateTime').datetimepicker({
                dayOfWeekStart: 1,
                lang: 'vi',
                startDate: '2014/10/10',
                format: 'd/m/Y',
                dateonly: true,
                showHour: false,
                showMinute: false,
                timepicker: false,
                onChangeDateTime: function (dp, $input) { },
                scrollMonth: false,
                scrollTime: false,
                scrollInput: false
            });
            //============================
            // Event delete
            //============================
            function del(This, code, name) {
                var code = code || null,
                    name = name || null,
                    Row = This;
                if (!code) return false;

                // show EBPopup
                $(".confirm-yn").openEBPopup();
                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

                $("#EBPopup .yn-yes").bind("click", function () {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/SystemService/Ajax/Del.aspx/Delele_S4M_Student",
                        data: '{Code : "' + code + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            var mission = JSON.parse(response.d);
                            if (mission.success) {
                                delSuccess();
                                Row.remove();
                            } else {
                                delFailed();
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    });
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
            }


            //Update mức độ tiếp cận
            function UpdateNumberOf(This, Id) {
                var Id = Id;
                var Number = This.val();
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    datatype: "JSON",
                    url: "/GUI/BackEnd/S4M_Student/Student_Listing.aspx/UpdateNumber",
                    data: "{Id:" + Id + ",numberId:" + Number + "}",
                }).success(function (response) {
                    if (response.d.success) {
                        alert("Cập nhật thành công!")
                    }
                    else {
                        alert("Có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển!");
                    }
                });
            }
            //Update note
            function UpdateNote(This, Id) {
                var Note = This.val();
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    datatype: "JSON",
                    url: "/GUI/BackEnd/S4M_Student/Student_Listing.aspx/UpdateNote",
                    data: '{Id:' + Id + ',Note:"' + Note + '"}'
                }).success(function (response) {
                    if (response.d.success) {
                        alert("Thay đổi ghi chú thành công!");
                    }
                    else {
                        alert("Có lỗi xảy ra! Vui lòng liên hệ với nhóm phát triển!");
                    }
                })

            }
        </script>

    </asp:Panel>
</asp:Content>
