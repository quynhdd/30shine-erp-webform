﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Student_ReportListing.aspx.cs" Inherits="_30shine.GUI.BackEnd.S4M_Student.Student_ReportListing" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ProductAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .msg-system {
                min-height: 10px;
                margin: 2px 0;
                width: 100%;
                float: left;
                -webkit-border-radius: 6px;
                -moz-border-radius: 6px;
                border-radius: 6px;
            }

                .msg-system.warning {
                    background: orange;
                    padding: 8px 15px;
                    margin: 5px 0;
                }

                .msg-system.success {
                    background: #7ec977;
                    color: white;
                    padding: 8px 15px;
                    margin: 5px 0;
                }

            .btn-upload-img {
                width: 120px;
                height: 36px;
                line-height: 36px;
                text-align: center;
                background: #dfdfdf;
                cursor: pointer;
            }

            .skill-table {
                display: table;
                width: 100%;
            }

                .skill-table .item-row {
                    display: table-row;
                    border-bottom: 1px solid #ddd;
                }

                .skill-table .item-cell {
                    display: table-cell;
                    border: 1px solid #ddd;
                    padding: 5px;
                }

                    .skill-table .item-cell .inputPoint {
                        width: 140px;
                        text-align: center;
                        margin-left: 10px
                    }

                    .skill-table .item-cell > span {
                        float: left;
                        width: 100%;
                        text-align: center;
                    }

                .skill-table .item-row .item-cell .checkbox {
                    float: left;
                    padding-top: 0 !important;
                    padding-bottom: 0 !important;
                    margin-top: 6px !important;
                    margin-bottom: 0px !important;
                    margin-right: 9px;
                    text-align: left;
                    background: #ddd;
                    padding: 2px 10px !important;
                }

                    .skill-table .item-row .item-cell .checkbox input[type='checkbox'] {
                        margin-left: 0 !important;
                        margin-right: 3px !important;
                    }

                .skill-table .item-cell-order {
                    width: 30px !important;
                }

            .modal {
                z-index: 9998;
            }

            .be-report .row-filter {
                z-index: 0;
            }

            @media(min-width: 768px) {
                .modal-content, .modal-dialog {
                    width: 700px !important;
                    margin: auto;
                }
            }

            table.dataTable {
                border-collapse: collapse !important;
            }
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý Stylist4Men &nbsp;&#187; Báo cáo &nbsp;&#187;</li>
                        <li class="li-listing active"><a href="/admin/s4m/hoc-vien/danh-sach.html">Danh sách học viên không nhận</a></li>
                        <li class="li-listing active"><a href="/admin/s4m/hoc-vien/bao-cao-diem-hoc-vien.html">Danh sách học viên được nhận </a></li>
                        <li class="li-add"><a href="/admin/s4m/hoc-vien/them-moi.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report">
            <!-- Listing -->
            <div class="wp960 content-wp">
                <asp:Label ID="LabelMsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <!-- Filter -->
                <div class="row">
                    <span style="display: none"></span>
                    <div class="filter-item3" style="margin-left: 0;">
                        <div class="filter-item3">
                            <span class="field-wp">
                                <asp:DropDownList ID="ddlSalon" runat="server" ClientIDMode="Static" Style="margin-top: 13px" OnSelectedIndexChanged="ddlSalon_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </span>
                        </div>

                        <!--Chọn theo list lớp học khóa-->
                        <!--padding"Trên-Phải-dưới-trái"-->
                        <div class="filter-item3">
                            <span class="field-wp">
                                <asp:DropDownList ID="ddlClass" runat="server" ClientIDMode="Static" Style="margin-top: 13px" OnSelectedIndexChanged="ddlClass_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                            </span>
                        </div>
                        <!-- Trạng thái kết quả-->
                        <div class="filter-item3">
                            <asp:DropDownList ID="ddlIsAccept" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 150px;">
                                <asp:ListItem Text="--- Trạng thái ---" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Chưa có kết quả" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Vào 30Shine" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Trượt" Value="3"></asp:ListItem>
                                <asp:ListItem Text="Thi lại" Value="4"></asp:ListItem>
                                <asp:ListItem Text="Tạm nghỉ" Value="5"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="filter-item3">
                            <asp:DropDownList ID="ddlstatusTuition" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 150px;">
                                <asp:ListItem Text="--- Học phí ---" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Hoàn thành" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Công nợ" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="filter-item3">
                            <asp:DropDownList ID="ddlSpeedCut" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 150px;">
                                <asp:ListItem Text="--- Tốc độ cắt trung bình HQ---" Value="0"></asp:ListItem>
                                <asp:ListItem Text="tốc độ <= 25p" Value="Speedless25p"></asp:ListItem>
                                <asp:ListItem Text="25p < tốc độ <= 30p" Value="between2530"></asp:ListItem>
                                <asp:ListItem Text="30p < tốc độ <= 35p" Value="between3035"></asp:ListItem>
                                <asp:ListItem Text="tốc độ > 40p" Value="better40p"></asp:ListItem>
                            </asp:DropDownList>
                        </div>

                        <span class="field-wp" style="float: left; margin-left: 10px">
                            <%-- <input type="button" onclick="clickConfirm($(this))" class="st-head btn-viewdata" value="Xem dữ liệu" />--%>
                            <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                                onclick="excPaging(1)" runat="server" Style="margin-top: 13px">
                                Xem dữ liệu
                            </asp:Panel>
                        </span>
                    </div>
                </div>
                <!-- Row Table Filter -->
                <div class="table-func-panel">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <!--End Fillter-->
                        <div class="table-wp">
                            <table class="table-add table-listing" id="StudentReport">
                                <thead>
                                    <tr>
                                        <th colspan="8">Thông tin cơ bản</th>
                                        <th colspan="2">Lớp lý thuyết</th>
                                        <th colspan="7">Điểm cắt</th>
                                        <th colspan="9">Hội quán</th>
                                        <%--<th colspan="4">Tốt nghiệp vào 30shine</th>--%>
                                        <th rowspan="2">Kết quả</th>
                                    </tr>
                                    <tr>
                                        <th style="width: 40px;">STT</th>
                                        <th style="width: 40px;">ID</th>
                                        <th style="width: 70px">Tài khoản</th>
                                        <th>Tên</th>
                                        <th>Khóa học</th>
                                        <th>Gói học </th>
                                        <th>Đã đóng</th>
                                        <th>Công nợ</th>
                                        <%--<th>LT Cắt</th>--%>
                                        <th>Điểm thi<br /> lý thuyết</th>
                                        <th>Điểm vote<br /> kỹ năng</th>
                                        <th>Classic/<br />tốc độ</th>
                                        <th>Under/<br />tốc độ</th>
                                        <th>Sport/<br />tốc độ</th>
                                        <th>Số đầu</th>
                                        <th>KCS</th>
                                        <th>LT</th>
                                        <th style="width: 60px">Ảnh kết quả</th>
                                        <th>Tên Salon</th>
                                        <th>Classic/<br />tốc độ</th>
                                        <th>Under/<br />tốc độ</th>
                                        <th>Sport/<br />tốc độ</th>
                                        <th>Uốn</th>
                                        <th>Nhuộm</th>
                                        <th>Số đầu</th>
                                        <th>KCS</th>
                                        <th style="width: 60px">Điểm TN</th>
                                        <%--<th>LT Cắt</th>
                                        <th>LT HC</th>
                                        <th>TH Cắt</th>
                                        <th>TH HC</th>--%>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rpt" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td class="item-total"><%# Container.ItemIndex+1 %></td>
                                                <td class="item-total"><%#Eval("StudentId") %></td>
                                                <td class="item-total"><%#Eval ("Email") %></td>
                                                <td class="item-total">
                                                    <a href="/admin/s4m/hoc-vien/<%#Eval("StudentId") %>.html"><%#Eval("Fullname") %></a>
                                                </td>
                                                <td class="item-total"><%# Eval("Name") %></td>
                                                <td class="item-total"><%# Eval("NamePackage") %></td>
                                                <td class="item-total"><%# String.Format("{0:0,0}",Eval("TienDaDong")) %></td>
                                                <td class="item-total"><%# String.Format("{0:0,0}", Eval("CongNo")) %></td>
                                                <%-- <td class="item-total" style="width: 70px;">
                                                    <input type="text" onkeypress="return isNumber(event)" id="PointLTCat<%# Eval("StudentId") %>" value=' <%# Eval("PointLTCat") %>' onchange="UpdatePoint($(this),<%# Eval("StudentId") %> );" style="width: 40px; text-align: center; height: 25px" /></td>--%>
                                                <td class="item-total" style="width: 70px;">
                                                    <input type="text" onkeypress="return isNumber(event)" id="PointLTKN<%# Eval("StudentId") %>" value='<%# Eval("PointLTKN") %> ' onchange="UpdatePoint($(this), <%# Eval("StudentId") %> );" style="width: 40px; text-align: center; height: 25px" /></td>
                                                <td class="item-total" style="width: 70px;">
                                                    <input type="text" onkeypress="return isNumber(event)" id="PointVote<%# Eval("StudentId") %>" value='<%# Eval("Vote") %> ' onchange="UpdatePoint($(this), <%# Eval("StudentId") %> );" style="width: 40px; text-align: center; height: 25px" /></td>
                                                <td><%# Eval("TotalStudentClassic").ToString() %> / <%# Eval("SpeedCutClassicStudent").ToString() %></td>
                                                <td><%# Eval("TotalStudentUnder").ToString() %> / <%# Eval("SpeedCutUnderStudent").ToString() %></td>
                                                <td><%# Eval("TotalStudentSport").ToString() %> / <%# Eval("SpeedCutSportStudent").ToString() %></td>
                                                <td class="totalbill"><%# Eval("TotalBill") %></td>
                                                <td></td>
                                                <td class="item-total" style="width: 70px;">
                                                    <input type="text" id="PointTheoryCut<%# Eval("StudentId") %>" value='<%# Eval("PointTheoryCut") %>' class="item-total-LT" <%# Eval("attr") %> onchange="UpdatePoint($(this), <%# Eval("StudentId") %>);" style="width: 40px; text-align: center; height: 25px" />
                                                </td>
                                                <td><a class="uploadImage" onclick="BindImageStudent($(this),<%#Eval("StudentId") %>);" data-id="<%#Eval("StudentId") %>" data-name="<%# Eval("Fullname") %>">Ảnh KQ</a></td>
                                                <td><%#Eval("NameSalon") %></td>
                                                <td><%# Eval("TotalClubClassic").ToString() %> / <%# Eval("SpeedCutClassicClub").ToString() %></td>
                                                <td><%# Eval("TotalClubUnder").ToString() %> / <%# Eval("SpeedCutUnderClub").ToString() %></td>
                                                <td><%# Eval("TotalClubSport").ToString() %> / <%# Eval("SpeedSportClub").ToString() %></td>
                                                <td><%# Eval("Uon").ToString() %></td>
                                                <td><%# Eval("Nhuom").ToString() %></td>
                                                <td><%#Eval ("TotalBillSalonHoiQuan") %></td>
                                                <td></td>
                                                <td>
                                                    <a class="insert" onclick="BindImageHoiQuan($(this),<%# Eval("StaffId") %>);" data-id="<%# Eval("StaffId") %>" data-name="<%# Eval("Fullname") %>">Cập nhật</a>
                                                </td>
                                                <%--<td>
                                                    <input type="text" id="LTCat<%# Eval("StaffId") %>" onkeypress="return isNumber(event)" value="<%# Eval("LTCat") %>" style="width: 40px; text-align: center; height: 25px" <%# Eval("attr1") %> />

                                                </td>
                                                <td>
                                                    <input type="text" id="LTHC<%# Eval("StaffId") %>" onkeypress="return isNumber(event)" value="<%# Eval("LTHC") %>" style="width: 40px; text-align: center; height: 25px" <%# Eval("attr1") %> />

                                                </td>
                                                <td>
                                                    <input type="text" id="THCat<%# Eval("StaffId") %>" onkeypress="return isNumber(event)" value="<%# Eval("THCat") %>" style="width: 40px; text-align: center; height: 25px" <%# Eval("attr1") %> />

                                                </td>
                                                <td>
                                                    <input type="text" id="THHC<%# Eval("StaffId") %>" onkeypress="return isNumber(event)" value="<%# Eval("THHC") %>" style="width: 40px; text-align: center; height: 25px" <%# Eval("attr1") %> />

                                                </td>--%>
                                                <td>
                                                    <select id="statusId" style="width: 90px" onchange="UpdatePointTN($(this),<%# Eval("StaffId") %>);">
                                                        <option class="ChuacoKQ<%# Eval("StaffId") %>" <%# Convert.ToInt32(Eval("StatusId")) == 1 ? "selected='selected'" : "" %> value="1">Chưa có KQ</option>
                                                        <option class="30Shine<%# Eval("StaffId") %>" <%# Convert.ToInt32(Eval("StatusId")) == 2 ? "selected='selected'" : "" %> value="2">Vào 30Shine</option>
                                                        <option class="Truot<%# Eval("StaffId") %>" <%# Convert.ToInt32(Eval("StatusId")) == 3 ? "selected='selected'" : "" %> value="3">Trượt</option>
                                                        <option class="ThiLai<%# Eval("StaffId") %>" <%# Convert.ToInt32(Eval("StatusId")) == 4 ? "selected='selected'" : "" %> value="4">Thi lại</option>
                                                        <option class="ThiLai<%# Eval("StaffId") %>" <%# Convert.ToInt32(Eval("StatusId")) == 5 ? "selected='selected'" : "" %> value="5">Tạm nghỉ</option>
                                                    </select>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <div class="site-paging-wp">
                            <% if (Paging.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (Paging._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=Paging._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (Paging._Paging.Next != Paging.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=Paging._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=Paging.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" Text="Click" OnClick="_BtnClick" Style="display: none" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
            </div>
            <!--End Listting-->
        </div>
        <!--Model 1-->
        <div id="systemModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="filter-item1" id="contentdata1" style="margin-left: 20px; padding: 10px">
                        <strong class="st-head" style="margin-right: 10px;"><i class="action-add"></i>Cập nhật ảnh : Học viên</strong>
                        <span class="spanname" style="font-size: 15px;"></span>
                        <div class="tab-content" data-credit-id="8" style="margin-top: 10px; text-align: center">
                            <div class="skill-table display" id="totalImageStudent" style="margin-left: 14px">
                                <div class="wrap btn-upload-wp" style="margin-bottom: 5px; margin-left: 10px; width: auto;">
                                    <div id="Btn_UploadImg8" class="btn-upload-img">Upload Image</div>
                                    <asp:FileUpload ID="FileUpload10" ClientIDMode="Static" Style="display: none;" CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                    <div class="wrap listing-img-upload">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="st-head btn-viewdata" style="height: 32px; width: 65px!important; margin-right: 30px" id="ConfirmModalImageStudent" onclick="clickConfirmModalImageStudent($(this))">Hoàn tất</button>
                                <button type="button" class="btn btn-default" id="closeModal">Đóng</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!--Model 2-->
        <div id="systemModal1" class="modal fade" role="dialog" style="overflow: scroll !important">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div style="margin: 10px;">
                        <strong class="st-head">Học viên</strong>
                        <span class="spanname" style="font-size: 15px;"></span>
                    </div>
                    <div class="filter-item1" id="contentdata" style="margin-left: 40px; padding: 10px">
                    </div>
                    <div class="modal-footer">
                        <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static" Width="200px"></asp:Label>
                        <button type="button" class="st-head btn-viewdata" style="height: 32px; width: 65px!important; margin-right: 30px" id="Confirm" onclick="clickConfirmModal($(this))">Hoàn tất</button>
                        <button type="button" class="btn btn-default" id="closeModal1">Đóng</button>
                    </div>
                </div>

            </div>
        </div>

        <script>
            var id;
            //var currentCredit;
            var creditId;
            //var imagedata = [];
            var PointBlockNoData = [];
            //var dataImages = "";
            jQuery(document).ready(function () {
                $(document).on("click", "body", function () {
                    $('#LabelMsgSystem').text("");
                });

                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { },
                    scrollMonth: false,
                    scrollTime: false,
                    scrollInput: false

                });
                var qs = getQueryStrings();
                showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);
                $("td.item-total input .item-total-LT").hide();

                //$(document).on('click', '.btn-upload-img', function (e) {
                //    e.preventDefault();
                //    var _this = $(e.target);
                //    creditId = + _this.closest('.tab-content').data('credit-id');
                //    popupImageIframe(creditId);

                //});

                (function () {
                    window.s4mData = {};
                    $(document).on('click', '.btn-upload-img', function (e) {
                        e.preventDefault();
                        var _this = $(e.target);
                        creditId = + _this.closest('.tab-content').data('credit-id');
                        if (s4mData[creditId] == null) {
                            s4mData[creditId] = {
                                btnId: e.target.id,
                                image: [],
                                pointLT: 0,
                                pointVote: 0
                            };
                        }
                        popupImageIframe(creditId);
                    });

                    //$(document).on('input propertychange', 'input.inputPoint', function (e) {

                    //    e.preventDefault();
                    //    var _this = $(e.target);
                    //    //var pointType = _this.data('point-type');
                    //    // var pointLT = _this.data('point-tt');
                    //    //var pointVote = _this.data('point-vote');

                    //    creditId = _this.closest('.tab-content').data('credit-id');

                    //    var PointLT = _this.parent().parent().find("#pointTheory").val();
                    //    var PointVote = _this.parent().parent().find("#pointVote").val();
                    //    if (s4mData[creditId] == null) {
                    //        s4mData[creditId] = {
                    //            btnId: e.target.id,
                    //            image: [],
                    //            pointLT: 0,
                    //            pointVote: 0
                    //        };
                    //    }
                    //    currentCredit = s4mData[creditId];
                    //    //currentCredit[pointType] = e.target.value;
                    //    currentCredit.pointLT = PointLT;
                    //    currentCredit.pointVote = PointVote;
                    //    console.log(s4mData);
                    //});
                })();
            });
            function ShowWarning(message) {
                $('#LabelMsgSystem').text(message);
                $('#LabelMsgSystem').css("color", "red");
            }

            // show popup chọn ảnh 
            function popupImageIframe(creditId) {

                var Width = 960;
                var Height = 560;
                var ImgList = [];
                var iframe = $("<div class='iframe-image-upload' style='width:" + Width + "px; height:" + Height + "px; float:left; display:none;'><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + creditId + "&callback=getThumbFromIframe&imgWidth=800&imgHeight=600&multi_img=false&Folder=Common' style='width:100%; height: 100%;'></iframe></div");

                $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
            }

            window.getThumbFromIframe = function (Imgs, creditId) {
                var thumb = "";
                if (Imgs.length > 0) {
                    thumb = '<div class="thumb-wp">' +
                        '<img class="thumb" alt="" title="" src="' + Imgs[0] + '"' +
                        'data-img="' + Imgs[0] + '" />' +
                        '<span class="delete-thumb" onclick="deleteThumbnail($(this), \'' + creditId + '\')"></span>' +
                        '</div>';
                    $("#contentdata1").find("#totalImageStudent").find(".listing-img-upload").append(thumb);
                    $("#contentdata").find("#" + creditId).find("#totalImageStaff").find(".listing-img-upload").append(thumb);

                    excThumbWidth(120, 120);
                }
                autoCloseEBPopup();
            }
            // xóa ảnh
            function deleteThumbnail(This, StoreImgField) {
                var imgs = "";
                var lst = This.parent().parent();
                This.parent().remove();
                lst.find("img.thumb").each(function () {
                    imgs += $(this).attr("src") + ",";
                });
                $("#" + StoreImgField).val(imgs);
            }
            // View popup Modal
            var name;
            $(document).on("click", ".insert", function () {
                $("#systemModal").hide();
                $("#systemModal1").modal();
                name = $(this).attr("data-name");
                id = $(this).attr("data-id");
                $(".spanname").html(name);
            });
            $(document).on("click", ".uploadImage", function () {
                $("#systemModal").modal();
                name = $(this).attr("data-name");
                id = $(this).attr("data-id");
                $(".spanname").html(name);
            });
            $(document).on("click", "#closeModal", function () {
                $("#systemModal").modal("hide");
                $("#systemModal1").modal("hide");
            });
            $(document).on("click", "#closeModal1", function () {
                $("#systemModal1").modal("hide");
                $("#systemModal").modal("hide");
                $("#contentdata").empty();
            });
            $("#subMenu .li-listing1").addClass("active");

            //Update Point lớp lý thuyết
            function UpdatePoint(This, StudentId) {
                var Id = StudentId;
                var pointLTKN = $("#PointLTKN" + StudentId + "").val();
                var pointVote = $("#PointVote" + StudentId + "").val();
                var pointTheoryCut = $("#PointTheoryCut" + StudentId + "").val();
                $.ajax({
                    type: "POST",
                    contentType: "application/json;charset:UTF-8",
                    datatype: "JSON",
                    url: "/GUI/BackEnd/S4M_Student/Student_ReportListing.aspx/UpdatePoint",
                    data: '{Id:' + Id + ', LTKN:' + pointLTKN + ',Vote:' + pointVote + ',PointTheoryCut:' + pointTheoryCut + '}',
                    success: function (response) {
                        if (response.d.success == true) {
                            alert("Cập nhật thành công!");
                        }
                        else {
                            alert("Có lỗi xảy ra! Vui lòng liên hệ với nhà phát triển!");
                        }
                    }
                });
            }

            // Update point tốt nghiệp vào 30Shine
            function UpdatePointTN(This, StaffId) {
                if (StaffId > 0) {
                    var staffId = StaffId;
                    var LTCat = $("#LTCat" + StaffId + "").val();
                    var LTHC = $("#LTHC" + StaffId + "").val();
                    var THCat = $("#THCat" + StaffId + "").val();
                    var THHC = $("#THHC" + StaffId + "").val();
                    var StatusId = This.val();
                    $.ajax({
                        type: "POST",
                        contentType: "application/json;charset:UTF-8",
                        datatype: "JSON",
                        url: "/GUI/BackEnd/S4M_Student/Student_ReportListing.aspx/UpdatePoint30Shine",
                        data: '{staffId:' + staffId + ',LTCat:' + LTCat + ',LTHC:' + LTHC + ',THCat:' + THCat + ',THHC:' + THHC + ',StatusId:' + StatusId + '}',
                        success: function (response) {
                            //console.log(response);
                            if (response.d.success == true) {
                                alert("Cập nhật thành công!");
                            }
                        }
                    });
                }
                else {
                    alert("Học viên này chưa phải là nhân viên Salon hội quán!");
                }
            }

            //Insert điểm hội Quán
            function clickConfirmModal(This) {
                var staffId = id;
                var IdCredit = 0;
                for (i = 0; i < PointBlockNoData.length; i++) {
                    var jsonData = PointBlockNoData[i];
                    IdCredit = jsonData.Id;
                    var PointTheory = $("#contentdata").find("#" + jsonData.Id).find("#pointTheory").val();
                    PointTheory = PointTheory === "" ? null : PointTheory;
                    var PointVote = $("#contentdata").find("#" + jsonData.Id).find("#pointVote").val();
                    PointVote = PointVote === "" ? null : PointVote;
                    var Objimages = $("#contentdata").find("#" + jsonData.Id).find("#totalImageStaff").find(".listing-img-upload").find(".thumb-wp").find(".thumb");
                    var ImagesStrings = "";
                    for (j = 0; j < Objimages.length; j++) {
                        if (ImagesStrings != "")
                            ImagesStrings += ",";
                        ImagesStrings += $(Objimages[j]).attr("src");
                    }
                    if (PointTheory != null || PointVote != null || ImagesStrings != "") {
                        $.ajax({
                            type: "POST",
                            contentType: "application/json;charset:UTF-8",
                            datatype: "JSON",
                            url: "/GUI/BackEnd/S4M_Student/Student_ReportListing.aspx/UpdatePointTNHoiQuan",
                            data: '{StaffId:' + staffId + ',IdCredit:' + IdCredit + ', Images:"' + ImagesStrings + '",PointTheory:' + PointTheory + ',PointVote:' + PointVote + '}',
                            success: function (response) {
                                if (response.d.success == true) {
                                    showMsgSystem("Cập nhập điểm thi thành công!", "success");
                                }
                                else {
                                    showMsgSystem("Có lỗi xảy ra! Vui lòng liên hệ với nhóm phát triển!", "warning");
                                }
                            }
                        });

                    }
                }
            }

            ///Author: Như Phúc tạo ra các khối theo các học phần có sẵn
            function initalBlockPoit() {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/S4M_Student/Student_ReportListing.aspx/InitalBlockPoint",
                    async: false,
                    contentType: "application/json; charset=utf-8",
                    datatype: "JSON",
                    data: '{}',
                    success: function (data) {
                        var jsonData = $.parseJSON(data.d);
                        PointBlockNoData = jsonData;
                        if (jsonData != null) {
                            $("#contentdata").empty();
                            for (var i = 0; i < PointBlockNoData.length; i++) {
                                item =
                                    '<div class="tab-content" data-credit-id="' + PointBlockNoData[i].Id + '" Id="' + PointBlockNoData[i].Id + '">' +
                                    '<p style="margin: 5px 0 0 10px; font-weight: bold">' + PointBlockNoData[i].name + '</p>' +
                                    '<table class="skill-table display" id="TotalPoint" style="margin: 10px 0 10px 25px; width: 540px">' +
                                    '<tbody>' +
                                    '<tr>' +
                                    '<td class="item-cell item-cell-skill" style="width: 200px">Điểm thi lý thuyết: <input type="text" data-point-type="pointLT" id="pointTheory" onkeypress="return isNumber(event);" class="inputPoint" value="" placeholder="Nhập điểm" /></td>' +

                                    '<td class="item-cell item-cell-skill" style="width: 200px">Điểm vote kỹ năng: <input type="text" data-point-type="pointVote" id="pointVote" onkeypress="return isNumber(event);" class="inputPoint" value="" placeholder= "Nhập điểm" /></td > ' +
                                    '</tr>' +
                                    '</tbody >' +
                                    '</table >' +
                                    '<div class="skill-table display" id="totalImageStaff" style="margin-left: 14px">' +
                                    '<div class="wrap btn-upload-wp" style="margin-bottom: 5px; margin-left: 10px; width: auto;">' +
                                    '<div id="Btn_UploadImg' + PointBlockNoData[i].Id + '" class="btn-upload-img">Upload Image</div>' +
                                    '<asp:FileUpload ID="FileUpload1" ClientIDMode="Static" Style="display: none;" CssClass="input-file-upload" runat="server" AllowMultiple="true" />' +
                                    '<div class="wrap listing-img-upload">' +
                                    '</div>' +
                                    '</div>';
                                $("#contentdata").append(item);
                            }

                        }

                    }
                });
            }

            // Bind Ảnh và điểm thi nhân viên hội quán
            function BindImageHoiQuan(This, StaffId) {
                //khoi tạo đối tượng khối;
                if (StaffId > 0) {
                    initalBlockPoit();
                    id = StaffId;
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/S4M_Student/Student_ReportListing.aspx/BindDataHoiQuanStudent",
                        async: false,
                        contentType: "application/json; charset=utf-8",
                        datatype: "JSON",
                        data: '{staffId:' + StaffId + '}',
                        success: function (data) {
                            var jsonData = $.parseJSON(data.d);
                            for (i = 0; i < jsonData.length; i++) {
                                var PointTheory = $("#contentdata").find("#" + jsonData[i].Id).find("#pointTheory").val(jsonData[i].PointTheory);
                                var PointVote = $("#contentdata").find("#" + jsonData[i].Id).find("#pointVote").val(jsonData[i].PointVote);
                                var images = $("#contentdata").find("#" + jsonData[i].Id).find("#totalImageStaff").find(".listing-img-upload");
                                var dataImages = "";
                                var image = jsonData[i].Images.split(',');
                                if (jsonData[i].Images === "") image = "";
                                if (image != "") {
                                    for (j = 0; j < image.length; j++) {
                                        dataImages = '<div class="thumb-wp" >' +
                                            '<img class="thumb" title="" src="' + image[j] + '" data-img="' + image[j] + '" style="width:120px; height:120px; left: 0;" />' +
                                            '<span class="delete-thumb" onclick="deleteThumbnail($(this),' + jsonData[i].Id + ')"></span>' +
                                            '</div>';
                                        images.append(dataImages);
                                    }
                                }

                            }

                        }
                    });
                }
                else {
                    alert("Học viên này chưa là nhân viên Salon hội quán!");
                    $("#contentdata").empty();
                }

            }

            //Insert ảnh học viên
            function clickConfirmModalImageStudent(This) {
                var Id = id;
                var Objimages = $("#totalImageStudent").find(".listing-img-upload").find(".thumb-wp").find(".thumb");
                var ImagesStrings = "";

                for (j = 0; j < Objimages.length; j++) {
                    if (ImagesStrings != "")
                        ImagesStrings += ",";
                    ImagesStrings += $(Objimages[j]).attr("src");
                }
                $.ajax({
                    type: "POST",
                    contentType: "application/json;charset:UTF-8",
                    datatype: "JSON",
                    url: "/GUI/BackEnd/S4M_Student/Student_ReportListing.aspx/UpdateImagesStudent",
                    data: '{Id:' + Id + ',image1:"' + ImagesStrings + '"}',
                    success: function (response) {
                        if (response.d.success == true) {
                            $("#systemModal").modal("hide");
                            alert("Cập nhật ảnh hoàn tất!", "");

                        }
                        else {
                            $("#systemModal").modal("hide");
                            alert("Cập nhật ảnh thất bại! Vui lòng liên hệ với nhà phát triển!", "warning");
                        }
                    }
                });
            }

            // View anh student
            function BindImageStudent(This, StudentId) {
                if (StudentId > 0) {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/S4M_Student/Student_ReportListing.aspx/BindDataImageStudent",
                        contentType: "application/json; charset=utf-8",
                        datatype: "JSON",
                        data: '{studentId:' + StudentId + '}',
                        success: function (data) {
                            var jsonData = $.parseJSON(data.d);
                            var images = $("#totalImageStudent").find(".listing-img-upload");
                            images.empty();
                            if (jsonData != null) {
                                if (jsonData.Images != null) {
                                    var image = jsonData.Images.split(',');
                                    for (j = 0; j < image.length; j++) {
                                        dataImages = '<div class="thumb-wp" >' +
                                            '<img class="thumb" title="" src="' + image[j] + '" data-img="' + image[j] + '" style="width:120px; height:120px; left: 0;" />' +
                                            '<span class="delete-thumb" onclick="deleteThumbnail($(this),' + jsonData.Id + ')"></span>' +
                                            '</div>';
                                        images.append(dataImages);
                                    }
                                }
                            }
                        }
                    });
                }
            }
            //Check nhập kiểu số
            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }

            //show thông báo
            function showMsgSystem(msg, status) {
                $("#MsgSystem").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);
                setTimeout(function () {
                    $("#MsgSystem").fadeTo("slow", 0, function () {
                        $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 1);
                    });
                }, 5000);
            }
        </script>

    </asp:Panel>
</asp:Content>
