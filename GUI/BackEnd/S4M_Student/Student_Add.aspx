﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Student_Add.aspx.cs" Inherits="_30shine.GUI.BackEnd.S4M_Student.Student_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <script src="/Assets/js/S4M.js"></script>
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            table.table-quick-infor td {
                padding-top: 5px;
                padding-bottom: 5px;
            }

            table.table-quick-infor tr {
                border-bottom: 1px solid #DDDDDD;
            }

                table.table-quick-infor tr.no-border-bottom {
                    border-bottom: none;
                }

            table.table-log-skill-level {
                margin-top: 5px;
            }

            table.table-log-skill-level {
                border-collapse: collapse;
            }

                table.table-log-skill-level,
                table.table-log-skill-level th,
                table.table-log-skill-level td {
                    border: 1px solid black;
                    padding: 3px 10px;
                }

                    table.table-log-skill-level th {
                        font-weight: normal;
                        font-family: Roboto Condensed Bold;
                        text-align: center;
                    }

            .content-toggle .error {
                border: 1px solid #ff0000;
            }

            .content-toggle .text-warning {
                display: none;
                color: red;
                padding-right: 5px;
                padding-top: 18px;
                padding-left: 7px;
                font-size: 13px;
            }

            .content-toggle .text-success {
                display: none;
                color: green;
                padding-right: 5px;
                padding-top: 20px;
                padding-left: 7px;
                font-size: 13px;
            }

            .f_btn_reset {
                text-decoration: none !important;
                font-style: inherit !important;
                background: #000;
                color: #FFF;
                padding: 2px 7px 3px;
            }

                .f_btn_reset:hover {
                    color: #ffe400;
                }

            .staff-servey-mark span {
                height: 28px !important;
                line-height: 28px !important;
            }

            .staff-servey-mark td input[type="radio"] {
                top: 4px !important;
            }

            #pem {
                width: 100%;
            }

                #pem span {
                    width: 100%;
                    line-height: 22px;
                    height: 22px;
                }

                    #pem span div.btn-group .multiselect, #pem span div.btn-group, .multiselect-container {
                        width: 100% !important;
                    }

            #PermissionList {
                width: 100% !important;
            }

            .staff-servey-mark td input[type="radio"] {
                top: 4px !important;
            }

            .span-isdelete {
                height: 25px !important;
                padding: 5px !important;
                margin-top: -15px !important;
                line-height: 1.0 !important;
                background-color: #ccc !important;
            }
        </style>
        <style>
            .customer-add .table-add td.left span {
                text-align: left;
            }

            .cssradio label {
                margin-left: 5px;
                float: left;
            }

            .cssradio input {
                margin-left: 10px;
                float: left;
                margin-top: 4px;
            }

            .tr-skill {
                display: none;
            }

            .tr-show {
                display: table-row;
            }

            .customer-add .table-add tr td.right .fb-cover-error {
                top: 0;
                right: auto;
            }

            #success-alert {
                top: 100px;
                right: 10px;
                position: fixed;
                width: 20% !important;
                z-index: 2000;
            }
        </style>
        <div class="alert alert-danger" id="success-alert">
            <span id="msg-alert"></span>
        </div>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý Stylist4Men &nbsp;&#187; Học viên &nbsp;&#187;</li>
                        <li class="li-listing active"><a href="/admin/s4m/hoc-vien/danh-sach.html">Danh sách học viên không nhận</a></li>
                        <li class="li-listing active"><a href="/admin/s4m/hoc-vien/bao-cao-diem-hoc-vien.html">Danh sách học viên được nhận </a></li>
                        <li class="li-add"><a href="/admin/s4m/hoc-vien/them-moi.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>

                <div class="table-wp" style="">
                    <table class="table-add admin-product-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Thông tin đầy đủ của  học viên</strong></td>
                                <td></td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Tên học viên/ Số điện thoại<strong style="color: red"> &nbsp*</strong></span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <input type="text" id="Name" placeholder="Nhập họ tên" onkeypress="fnCheckNameOnKeypress($(this));" />
                                        </div>
                                        <div class="div_col">
                                            <input type="text" id="Phone" style="margin-left: 17px" placeholder="Nhập số điện thoại" onkeypress="return isNumber (event);" onchange="kt();" />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Thành phố/Quận huyện</span></td>
                                <td class="col-xs-9 right">
                                    <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
                                    <asp:UpdatePanel runat="server" ID="UP01">
                                        <ContentTemplate>
                                            <div class="city">
                                                <asp:DropDownList AutoPostBack="true" ID="City" runat="server" CssClass="select"
                                                    ClientIDMode="Static" OnSelectedIndexChanged="ddlFTinhThanh_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="district">
                                                <asp:DropDownList ID="District" runat="server" CssClass="select" ClientIDMode="Static"></asp:DropDownList>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr class="tr-description">
                                <td class="col-xs-2 left"><span>Địa chỉ</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="Address" runat="server" placeholder="Địa chỉ" ClientIDMode="Static"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left">Mức độ quan tâm<strong style="color: red"> &nbsp*</strong></td>
                                <td class="col-xs-9 right cssradio" id="LstradiobttList"></td>
                            </tr>
                            <tr class="tr-upload">
                                <td class="col-xs-2 left">Tài khoản/ Số CMT<strong style="color: red"> &nbsp*</strong></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <input type="text" style="width: 49%" id="Email" onblur="Checkemail()" placeholder="Nhập tên tài khoản" />
                                        </div>
                                        <div class="div_col">
                                            <input type="text" id="CMT" onkeypress="return ValidateKeypress(/\d/,event);" style="width: 49%; margin-left: 10px" placeholder="Nhập số chứng minh thư" />

                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="tr-upload">
                                <td class="col-xs-3 left">Gói học/ Khóa học<strong style="color: red"> &nbsp*</strong></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <asp:DropDownList ID="ddlGoiHoc" runat="server" ClientIDMode="Static" Width="49%"></asp:DropDownList>
                                        </div>
                                        <div class="div_col">
                                            <asp:DropDownList ID="ddlKhoahoc" runat="server" ClientIDMode="Static" Width="49%" Style="margin-left: 10px"></asp:DropDownList>

                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="tr-upload">
                                <td class="col-xs-3 left"><span>Mức học phí/ Đã nộp<strong style="color: red"> &nbsp*</strong></span></td>
                                <td>
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <input type="text" id="HocPhi" placeholder="Mức học phí" onkeypress="return ValidateKeypress(/\d/,event);" onkeyup="reformatText(this)" style="width: 30%" />
                                        </div>
                                        <div class="div_col">
                                            <input type="text" disabled="disabled" id="hocphi2" placeholder="Số tiền học viên đã đóng" style="margin-left: 10px; width: 30%" onkeypress="return ValidateKeypress(/\d/,event);" onkeyup="reformatText(this)" />
                                        </div>
                                        <div class="div_col">
                                            <input type="text" id="hocphi3" onchange="changePrice()" placeholder="Nhập số tiền học viên đóng" style="width: 30%; margin-left: 10px" onkeyup="reformatText(this)" />&nbsp&nbsp <span style="width: 30px; margin-left: 4px; color: red;">VNĐ</span>
                                        </div>

                                    </div>
                                </td>
                            </tr>
                            <tr class="tr-upload">
                                <td class="col-xs-3 left"><span>Salon</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <asp:DropDownList ID="Salon" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="width: 49%">
                                            </asp:DropDownList>
                                        </div>
                                        <%-- Phúc thêm học phí 3 --%>
                                        <div class="div_col">
                                            <input type="text" id="HocphiThem1" style="display: none" />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="tr-upload">
                                <td class="col-xs-3 left"><span>Ảnh CMT</span></td>
                                <td class="col-xs-7 right">
                                    <div class="wrap btn-upload-wp HDF_MainImgCMT1" style="margin-bottom: 5px; margin-left: 10px; width: auto;">
                                        <input class="hide" style="display: none" id="files1" type="file" />
                                        <div class="btn-upload-img choose-image1">Ảnh CMT mặt trước</div>
                                        <output id="result1" />
                                        <div class="wrap listing-img-upload" style="display: none">
                                            <div class="thumb-wp" style="height: 120px; width: 120px">
                                                <img style="width: 100% !important; height: 120px !important;" id="img1" src="" class="thumb" alt="" title="" />
                                                <span class="delete-thumb" onclick="deleteThumbnail($(this), 'HDF_MainImgCMT1')"></span>
                                            </div>
                                        </div>
                                        <%--  <div id="Btn_UploadImg1" class="btn-upload-img form-control" onclick="popupImageIframe('HDF_MainImgCMT1')">Ảnh CMT mặt trước</div>
                                        <asp:FileUpload ID="FileUpload1" ClientIDMode="Static" Style="display: none;"
                                            CssClass="input-file-upload" runat="server" AllowMultiple="true" />--%>
                                        <%--<div class="wrap listing-img-upload">
                                            <% if (HasImages)
                                                { %>
                                            <% for (var i = 0; i < ListImagesUrl.Count; i++)
                                                { %>
                                            <div class="thumb-wp">
                                                <img class="thumb" title="" src="<%=ListImagesUrl[i] %>"
                                                    data-img="<%=ListImagesUrl[i] %>" />
                                                <span class="delete-thumb" onclick="deleteThumbnail($(this), 'HDF_MainImgCMT1')"></span>
                                            </div>
                                            <% } %>
                                            <%} %>
                                            <div class="thumb-wp">
                                                <img class="thumb" id="img1" src=""
                                                    data-img="" style="width: 100%; height: 100%; position: relative" />
                                                <span class="delete-thumb" onclick="deleteThumbnail($(this), 'HDF_MainImgCMT1')"></span>
                                            </div>
                                        </div>--%>
                                    </div>
                                    <div class="wrap btn-upload-wp HDF_MainImgCMT2" style="margin-bottom: 5px; width: auto; margin-left: 10px;">
                                        <input class="hide" style="display: none" id="files2" type="file" />
                                        <div class="btn-upload-img choose-image2">Ảnh CMT mặt sau</div>
                                        <output id="result2" />
                                        <div class="wrap listing-img-upload" style="display: none">
                                            <div class="thumb-wp" style="height: 120px; width: 120px">
                                                <img style="width: 100% !important; height: 120px !important;" id="img2" src="" class="thumb" alt="" title="" />
                                                <span class="delete-thumb" onclick="deleteThumbnail($(this), 'HDF_MainImgCMT1')"></span>
                                            </div>
                                        </div>
                                        <%-- <div id="Btn_UploadImg2" class="btn-upload-img form-control" onclick="popupImageIframe('HDF_MainImgCMT2')">Ảnh CMT mặt sau</div>
                                        <asp:FileUpload ID="FileUpload2" ClientIDMode="Static" Style="display: none;"
                                            CssClass="input-file-upload" runat="server" AllowMultiple="true" />--%>
                                        <%--      <div class="wrap listing-img-upload">
                                            <% if (HasImages)
                                                { %>
                                            <% for (var i = 0; i < ListImagesUrl.Count; i++)
                                                { %>
                                            <div class="thumb-wp">
                                                <img class="thumb" title="" src="<%=ListImagesUrl[i] %>"
                                                    data-img="<%=ListImagesUrl[i] %>" />
                                                <span class="delete-thumb" onclick="deleteThumbnail($(this), 'HDF_MainImgCMT2')"></span>
                                            </div>
                                            <% } %>
                                            <% } %>
                                            <div class="thumb-wp">
                                                <img class="thumb" id="img2" alt="" title="" src=""
                                                    data-img="" style="width: 100%; height: 100%; position: relative" />
                                                <span class="delete-thumb" onclick="deleteThumbnail($(this), 'HDF_MainImgCMT2')"></span>
                                            </div>
                                        </div>--%>
                                    </div>
                                </td>
                            </tr>
                            <tr class="tr-description">
                                <td class="col-xs-2 left"><span>Ghi chú<strong style="color: red"> &nbsp*</strong></span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <textarea id="Note" class="form-control" style="padding: 10px" rows="4"></textarea>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-send">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span id="nhaphoc" class="field-wp">
                                        <input type="button" id="btnAdd" class="btn-send" onclick="AddOrUpdate();" value="Hoàn tất" />
                                    </span>
                                    <span id="nhan" class="field-wp">
                                        <input type="button" id="btnAddConfirm" class="btn-send" onclick="AddConfirm();" value="Nhận" />
                                    </span>
                                    <span id="khongnhan" class="field-wp" style="margin-left: 10px">
                                        <input type="button" id="btnCancel" class="btn-send" onclick="AddCancel();" value="Không nhận" />
                                    </span>
                                </td>
                            </tr>
                            <asp:HiddenField ID="HDF_MainImgCMT1" runat="server" ClientIDMode="Static" />
                            <asp:HiddenField ID="HDF_MainImgCMT2" runat="server" ClientIDMode="Static" />
                        </tbody>
                    </table>
                </div>
            </div>
            <%-- end Add --%>
        </div>

        <%-- Change password --%>
        <div class="wp960 content-wp content-toggle" id="ChangepassWp" style="display: none; padding: 0 20%;">
            <!-- System Message -->
            <asp:Label ID="Label2" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
            <!-- END System Message -->

            <div class="table-wp">
                <table class="table-add">
                    <tbody>
                        <tr class="title-head">
                            <td><strong>Đổi mật khẩu</strong></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="col-xs-3 left"><span>Mật khẩu mới</span></td>
                            <td class="col-xs-8 right"><span class="field-wp">
                                <asp:TextBox ID="TextBox1" runat="server" ClientIDMode="Static" CssClass="perm-input-field form-control"></asp:TextBox>
                            </span></td>
                        </tr>

                        <tr>
                            <td class="col-xs-3 left"><span>Nhập lại mật khẩu mới</span></td>
                            <td class="col-xs-8 right">
                                <span class="field-wp">
                                    <asp:TextBox ID="TextBox2" runat="server" ClientIDMode="Static" CssClass="perm-input-field form-control"></asp:TextBox>

                                </span>

                            </td>
                        </tr>

                        <tr>
                            <td class="col-xs-3 left"><span>Mật khẩu xác nhận</span></td>
                            <td class="col-xs-8 right">
                                <span class="field-wp">
                                    <asp:TextBox ID="TextBox3" runat="server" ClientIDMode="Static" CssClass="perm-input-field form-control"></asp:TextBox>

                                </span>

                            </td>
                        </tr>

                        <tr class="tr-send">
                            <td class="col-xs-3 left"></td>
                            <td class="col-xs-8 right no-border">
                                <span class="field-wp">
                                    <%--     <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="FullName" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập mật khẩu xác nhận!"></asp:RequiredFieldValidator>--%>
                                    <span class="btn-send btn-cancel" style="margin-left: 12px;">Đóng</span>
                                    <div id="Div1" runat="server" class="col-xs-12 col-sm-8 col-md-8 form-item text-message text-warning" clientidmode="Static" style="padding-right: 5px;">
                                        (*) Vui lòng điền đầy đủ thông tin.
                                    </div>
                                    <div id="Div2" runat="server" class="col-xs-12 col-sm-8 col-md-8 form-item text-message text-success" clientidmode="Static" style="padding-right: 5px;">
                                        Đổi mật khẩu thành công.
                                    </div>
                                </span>

                            </td>
                        </tr>
                        <asp:HiddenField runat="server" ID="HiddenField4" ClientIDMode="Static" />
                        <asp:HiddenField runat="server" ID="HiddenField5" ClientIDMode="Static" />
                        <asp:HiddenField ID="HiddenField6" runat="server" ClientIDMode="Static" />
                    </tbody>
                </table>
            </div>
        </div>
        <%-- end change password --%>
        <!-- Staff Information -->
        <div class="wp960 content-wp content-customer-history" id="changepassword" style="float: left">
            <div class="container-fluid">
                <div class="row">
                    <!-- Common Information -->
                    <div class="col-xs-12 line-head" style="width: 250px">
                        <strong class="st-head" style="margin-top: 0px;">
                            <i class="fa fa-file-text"></i>
                            Thông tin cơ bản
                            <%--   <%if (Perm_Edit)
                                    { %>--%>
                            <span class="view-more-infor f_btn_reset" onclick="ResetPass()" style="">Reset Password </span>
                            <%--    <%} %>--%>
                        </strong>
                    </div>
                    <div class="col-xs-12 common-infor" style="float: left;">
                        <table class="table-quick-infor">
                            <tbody>
                                <tr>
                                    <td>Họ tên</td>
                                    <td style="text-align: left; padding-left: 20px;">
                                        <span style="font-family: Roboto Condensed Bold;">
                                            <input type="text" id="ChangeName" class="form-control" />
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Tài khoản</td>
                                    <td style="text-align: left; padding-left: 20px;">
                                        <span style="font-family: Roboto Condensed Bold;">
                                            <input type="text" id="ChangeEmail" class="form-control" />
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Số ĐT</td>
                                    <td style="text-align: left; padding-left: 20px;">
                                        <span style="font-family: Roboto Condensed Bold;">
                                            <input type="text" id="ChangePhone" class="form-control" />
                                        </span>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                    <!--/ Common Information -->

                </div>
            </div>

        </div>
        <!--/ Staff Information -->
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script src="../../../Assets/js/uploadImage.js"></script>
        <script src="/Assets/js/erp.30shine.com/monitor-staff/config.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 100% !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script>                
            jQuery(document).ready(function () {

                fillRadio();
                $("#glbS4M").addClass("active");
                $("#glbS4MStudent").addClass("active");
                if (Id == 0) {
                    $("#nhaphoc").hide();
                    $(".tr-upload").hide();
                    $("#nhan").show();
                    $("#khongnhan").show();
                    $("#success-alert").hide();
                    $("#changepassword").hide();
                }
                else {
                    $("#nhaphoc").show();
                    $(".tr-upload").show();
                    $("#changepassword").show();
                    $("#nhan").hide();
                    $("#khongnhan").hide();
                    $("#success-alert").hide();
                    BindOBJ();
                }

                $("#Name").bind("click", function () {
                    $("#Name").css("border-color", "#ddd");
                });
                $("#Phone").bind("click", function () {
                    $("#Phone").css("border-color", "#ddd");
                });
                $("#Note").bind("click", function () {
                    $("#Note").css("border-color", "#ddd");
                });
                $("#Email").bind("click", function () {
                    $("#Email").css("border-color", "#ddd");
                });
                $("#CMT").bind("click", function () {
                    $("#CMT").css("border-color", "#ddd");
                });
                $("#HocPhi").bind("click", function () {
                    $("#HocPhi").css("border-color", "#ddd");
                });
                $("#hocphi3").bind("click", function () {
                    $("#hocphi3").css("border-color", "#ddd");
                });
                window.onload = function () {
                    //call uploadCMT1
                    uploadCMT1();
                    //Call uploadCMT2
                    uploadCMT2();
                }
            });

            // Bind data to radio mức độ quan tâm
            function fillRadio() {
                $.ajax({
                    type: "POST",
                    contentType: "application/json;charset:UTF-8",
                    datatye: "JSON",
                    url: "/GUI/BackEnd/S4M_Student/Student_Add.aspx/FillRadio",
                    success: function (response) {
                        var jsonData = JSON.parse(response.d)
                        var listitem = "";
                        for (var i = 0; i < jsonData.length; i++) {
                            listitem += "<input class='cls_ConcernId' name=levelConcern id=radiobttList" + jsonData[i].Id + " type=radio value=" + jsonData[i].Id + "  /><label for=radiobttList" + i + ">" + jsonData[i].Name + "</label>";
                        }
                        $('#LstradiobttList').append(listitem);
                    }
                });
            }

            var Id = <%= Convert.ToInt32(Request.QueryString["Code"]) >0 ? Convert.ToInt32(Request.QueryString["Code"]) : 0%>;
            var statusId;
            // bind data to textbox
            function BindOBJ() {
                debugger;
                var listQuanHuyen = "";
                $(".listing-img-upload").css("display", "block");
                $.ajax({
                    type: "POST",
                    contentType: "application/json;charset:UTF-8",
                    datatype: "JSON",
                    url: "/GUI/BackEnd/S4M_Student/Student_Add.aspx/BindData",
                    data: '{id:' + Id + '}',
                    success: function (response) {
                        dataJSON = JSON.parse(response.d);
                        //console.log(dataJSON);
                        statusId = dataJSON.IsAccept;
                        if (statusId == 0) {
                            $("#nhaphoc").hide();
                            $(".tr-upload").hide();
                            $("#nhan").show();
                            $("#khongnhan").show();
                            $("#success-alert").hide();
                            $("#changepassword").hide();
                            $("changepassword").hide();
                        }
                        else { document.getElementById("Email").disabled = true; }
                        $("#Name").val(dataJSON.Fullname);
                        $("#Phone").val(dataJSON.Phone);
                        $("#Address").val(dataJSON.Address);
                        $("#CMT").val(dataJSON.NumberCMT);
                        $("#Email").val(dataJSON.Email);
                        var kq = (dataJSON.TotalAmountPaid);
                        var kq2 = dataJSON.TotalAmountCollected;
                        if (kq != null && kq2 != null) {
                            var HocPhi = kq.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                            $("#HocPhi").val(HocPhi);
                            var hocphi2 = kq2.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                            $("#hocphi2").val(hocphi2);
                        }
                        $("#ddlGoiHoc").val(dataJSON.StudyPackageId);
                        $("#ddlKhoahoc").val(dataJSON.ClassId);
                        $("#img1").attr("src", dataJSON.ImageCMT1);
                        $("#img2").attr("src", dataJSON.ImageCMT2);
                        $("#Note").val(dataJSON.Note);
                        $("#radiobttList" + dataJSON.LevelOfConcern + "").prop("checked", true);
                        $("#City").val(dataJSON.CityId);
                        $("#ChangeName").val(dataJSON.Fullname);
                        $("#ChangeEmail").val(dataJSON.Email);
                        $("#ChangePhone").val(dataJSON.Phone);
                        $("#Salon").val(dataJSON.SalonId);
                        fillDistrict(dataJSON.CityId, dataJSON.DistrictId);

                    }
                });
            }

            var statusId;
            //if (Id!=0){

            //    document.getElementById("HocPhi").disabled = true;                
            //}
            //Reset Password
            function ResetPass() {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/S4M_Student/Student_Add.aspx/ResetPassWord",
                    data: '{_Id: "' + Id + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.d == "success") {
                            setTimeout(function () {
                                setTimeout(function () {
                                    swal("Reset Success!", "Clicked the button to close!", "success")
                                }, 2000);
                                $("#btnFakeClick").css("dipslay", "none");

                            })
                        }
                    }
                });

            }

            //author: Phúc update, set value text box nhập tiền 
            function changePrice() {
                var hocphi3 = $("#hocphi3").val();
                if (hocphi3 == "")
                    hocphi3 = "0";

                hocphi3 = parseInt(hocphi3.replace(/,/g, ""));
                $("#HocphiThem1").val(hocphi3);

                var HocPhiDaDong = $("#hocphi2").val();
                if (HocPhiDaDong == "")
                    HocPhiDaDong = "0";
                HocPhiDaDong = parseInt(HocPhiDaDong.replace(/,/g, ""));
                var SumHocPhi = HocPhiDaDong + hocphi3;
                var HocPhiQuyDinh = parseInt($("#HocPhi").val().replace(/,/g, ""));
                if (hocphi3 != 0) {
                    if (SumHocPhi > HocPhiQuyDinh) {
                        $("#success-alert").fadeTo(2000, 800).slideUp(800, function () {
                            $("#success-alert").slideUp(4000);
                        });
                        $("#msg-alert").text(" Bạn đã nhập thừa tiền  " + (SumHocPhi - HocPhiQuyDinh) + " VNĐ!");
                        $("#hocphi3").css("border-color", "pink");
                        $("#hocphi3").focus();
                        $("#HocPhi").css("border-color", "pink");
                        return false;
                    }
                    else {

                        $("#success-alert").fadeTo(2000, 800).slideUp(800, function () {
                            $("#success-alert").slideUp(800);
                        });
                        $("#msg-alert").text("Bạn đã nộp tổng tiền học phí :" + (SumHocPhi.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")));

                        return true;
                    }
                }

            }

            // Add confirm
            function AddConfirm(This) {
                $("#nhaphoc").show();
                $(".tr-upload").show();
                $("#nhan").hide();
                $("#khongnhan").hide();
                document.getElementById("HocPhi").disabled = false;
                $("#Email").css("border-color", "red");
                $("#Email").focus();

            }
            // Check trùng tài khoản
            function checkIssetAccount() {
                var error = true;
                var email = $("#Email").val();
                var data = '{Email:"' + email + '"}';
                startLoading();
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8", async: false,
                    datatype: "JSON",
                    url: "/GUI/BackEnd/S4M_Student/Student_Add.aspx/CheckAccount",
                    data: data,
                    success: (function (response) {
                        if (response.d.success == true) {
                            error = false;
                        }
                    })
                });
                return error;
            }
            //Onblur check tai khoan trung 
            function Checkemail() {
                if (!checkIssetAccount()) {
                    $("#success-alert").fadeTo(2000, 800).slideUp(800, function () {
                        $("#success-alert").slideUp(800);
                    });
                    $("#msg-alert").text("Tài khoản này đã tồn tại. Vui lòng nhập tài khoản khác!");
                    $("#Email").css("border-color", "pink");
                    $("#Email").focus();

                    return false;
                }
                else return true;
            }
            // show popup chọn ảnh chứng minh thư
            function popupImageIframe(StoreImgField) {
                var Width = 960;
                var Height = 560;
                var ImgList = [];
                var iframe = $("<div class='iframe-image-upload' style='width:" + Width + "px; height:" + Height + "px; float:left; display:none;'><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + StoreImgField + "&callback=getThumbFromIframe&imgWidth=800&imgHeight=600&multi_img=false&Folder=Common' style='width:100%; height: 100%;'></iframe></div");

                $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
            }
            window.getThumbFromIframe = function (Imgs, StoreImgField) {
                var imgs = "";
                var thumb = "";
                if (Imgs.length > 0) {
                    thumb = '<div class="thumb-wp">' +
                        '<img class="thumb" alt="" title="" src="' + Imgs[0] + '"' +
                        'data-img="" />' +
                        '<span class="delete-thumb" onclick="deleteThumbnail($(this), \'' + StoreImgField + '\')"></span>' +
                        '</div>';
                    $("." + StoreImgField).find(".listing-img-upload").empty().append(thumb);
                    excThumbWidth(120, 120);
                    $("#" + StoreImgField).val(Imgs[0]);
                }
                autoCloseEBPopup();
            }
            function deleteThumbnail(This, StoreImgField) {
                var imgs = "";
                var lst = This.parent().parent();
                This.parent().remove();
                lst.find("img.thumb").each(function () {
                    imgs += $(this).attr("src") + ",";
                });
                $("#" + StoreImgField).val(imgs);
            }

            var elms = {
                //submit: 'AddOrUpdate()',
                btnChooseImage1: '.choose-image1',
                inputFile1: '#files1',
                btnChooseImage2: '.choose-image2',
                inputFile2: '#files2'
            }

            //choose image cmt mặt trước
            $(elms.btnChooseImage1).click(function () {
                $(elms.inputFile1).click();
            });

            //choose image cmt mặt sau
            $(elms.btnChooseImage2).click(function () {
                $(elms.inputFile2).click();
            });

            //call submit add image avatar staff
            //$(elms.submit).click(function () {
            function getImage() {
                //get image mặt trước
                let arrImage1 = [];
                $('#result1 img').each(function (i, v) {
                    let base64 = $(v).attr('src');
                    let item = {
                        img_name: "CMTMatTruocStudentS4M" + Date.now(),
                        img_base64: base64
                    };
                    arrImage1.push(item);
                });

                let dataImages1 = {
                    images: arrImage1
                };

                //post to s3 image mặt trước
                if (dataImages1.images.length > 0) {
                    let resultUpload1 = uploadImageStaffToS3(dataImages1);
                    $.each(resultUpload1.images, function (i, v) {
                        let link = v.link;
                        if (link) {
                            $("#HDF_MainImgCMT1").val(link);
                        }
                    });
                }

                //get image mặt sau
                let arrImage2 = [];
                $('#result2 img').each(function (i, v) {
                    let base64 = $(v).attr('src');
                    let item = {
                        img_name: "CMTMatSauStudentS4M" + Date.now(),
                        img_base64: base64
                    };
                    arrImage2.push(item);
                });

                let dataImages2 = {
                    images: arrImage2
                };

                //post to s3 image mặt sau
                if (dataImages2.images.length > 0) {
                    let resultUpload2 = uploadImageStaffToS3(dataImages2);
                    $.each(resultUpload2.images, function (i, v) {
                        let link = v.link;
                        if (link) {
                            $("#HDF_MainImgCMT2").val(link);
                        }
                    });
                }
                //});
            }
        </script>
    </asp:Panel>
</asp:Content>
