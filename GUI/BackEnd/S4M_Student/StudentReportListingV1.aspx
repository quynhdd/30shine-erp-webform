﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TemplateMaster/SiteMasterVersion1.Master" CodeBehind="StudentReportListingV1.aspx.cs" Inherits="_30shine.GUI.BackEnd.S4M_Student.StudentReportListingV1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel ID="ContentWrap" runat="server" ClientIDMode="Static">
        <div class="container-fluid sub-menu">
            <div class="row">
                <div class="col-md-12 pt-3 pb-2">
                    <ul>
                        <li><i class="fas fa-th-large mr-1" aria-hidden="true" style="width: auto !important; height: 1.2em !important"></i><a href="javascript:void(0);">Báo cáo - S4M</a></li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <div class="sub-menu-boder"></div>
                </div>
            </div>
        </div>
        <form runat="server">
            <div class="container-fluid pt-2 pb-2">
                <div class="row">
                    <div class="form-group col-auto">
                        <strong><i class="fa fa-filter"></i>Lọc kết quả &nbsp;&#187; </strong>
                    </div>
                    <div class="col-auto form-group">
                        <label>Salon HQ</label>
                        <br />
                        <select class="select form-control" name="salonId"></select>
                    </div>
                    <div class="col-auto form-group">
                        <label>Lớp học</label>
                        <br />
                        <select class="select form-control" name="classId"></select>
                    </div>
                    <div class="col-auto form-group">
                        <label>Học phí</label>
                        <br />
                        <select class="select form-control" name="statusTuitionId">
                            <option value="-1">--- Chọn học phí ---</option>
                            <option value="1">Hoàn thành</option>
                            <option value="0">Công nợ</option>
                        </select>
                    </div>
                    <div class="col-auto form-group d-flex align-items-end">
                        <div id="ViewDataFilter" onclick="GetData()">
                            <div class="btn-viewdata">Xem dữ liệu</div>
                        </div>
                    </div>
                    <div class="col-md-12 col-xl-12">
                        <div class="sub-menu-boder-low"></div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <table class="table table-sm table-bordered selectable" id="tableListing">
                    <thead>
                        <tr>
                            <th class="align-middle text-center" rowspan="2">STT</th>
                            <th class="align-middle text-center" rowspan="2">Họ và tên</th>
                            <th class="align-middle text-center view-money" colspan="5">Nhân sự và công nợ</th>
                            <th class="align-middle text-center" colspan="4">Tổng đầu đã cắt</th>
                            <th class="align-middle text-center" colspan="3">Thống kê</th>
                        </tr>
                        <tr>
                            <th class="align-middle text-center view-money">Tài khoản</th>
                            <th class="align-middle text-center view-money">Khóa học</th>
                            <th class="align-middle text-center view-money">Gói học</th>
                            <th class="align-middle text-center view-money">Đã đóng</th>
                            <th class="align-middle text-center view-money">Công nợ</th>
                            <th class="align-middle text-center">Under</th>
                            <th class="align-middle text-center">Classic</th>
                            <th class="align-middle text-center">Sport</th>
                            <th class="align-middle text-center">Tổng đầu</th>
                            <th class="align-middle text-center">Số tháng</th>
                            <th class="align-middle text-center" style="cursor: cell" data-toggle="tooltip" title="=(350 / (x đầu đã cắt / số ngày đã cắt)) - tổng ngày đã cắt">Ngày thực hành còn lại</th>
                            <th class="align-middle text-center">Dự kiến ngày tốt nghiệp</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </form>
        <style>
            table tr th {
                font-family: Roboto Condensed Bold;
            }

            .dataTables_scrollBody {
                height: auto !important;
                max-height: 80vh;
            }
        </style>
        <script>
            var permisisionViewAll = <%=Perm_ViewAllData ? 1 : 0%>;
            var table = null;
            var hide = "";
            $(document).ready(function () {
                startLoading();
                if (!permisisionViewAll) {
                    $('.view-money').addClass('d-none');
                    hide = "d-none";
                }
                $('[data-toggle="tooltip"]').tooltip();
                BindClass();
                BindSalonHQ();
                finishLoading();
            });
            function GetData() {
                startLoading();
                var obj = {
                    classId: ~~$('select[name=classId]').val(),
                    salonId: ~~$('select[name=salonId]').val(),
                    statusTuitionId: ~~$('select[name=statusTuitionId]').val(),
                }

                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/S4M_Student/StudentReportListingV1.aspx/GetData",
                    contentType: "application/json; charset=utf-8",
                    datatype: "JSON",
                    data: JSON.stringify(obj),
                    success: function (response) {
                        BindData(response);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        ShowMessage("Cảnh báo", "status:" + jqXHR.status + ',' + textStatus + '<br>- ' + errorThrown, 3, 7000);
                        finishLoading();
                    }
                });

            }
            function BindData(response) {
                if (response) {
                    console.log(response.d);
                    if (table) {
                        $('#tableListing').DataTable().clear();
                        $('#tableListing').DataTable().destroy();
                    }
                    var data = response.d;
                    $(data).each(function (i, v) {
                        v.Stt = i + 1;
                        v.TienDaDong = addCommas(v.TienDaDong);
                        v.CongNo = addCommas(v.CongNo);
                    });
                    var widthWindow = $(document).width();
                    let scrollx = false;
                    let fixedColumn = null;
                    if (widthWindow <= 992) {
                        scrollx = true;
                        fixedColumn = {
                            leftColumns: 2
                        }
                    }

                    table = $('#tableListing').DataTable(
                        {
                            data: data,
                            columns: [
                                { "data": 'Stt' },
                                { "data": 'Fullname' },
                                { "data": 'Email', className: "view-money " + hide },
                                { "data": 'ClassName', className: "view-money " + hide },
                                { "data": 'NamePackage', className: "view-money " + hide },
                                { "data": 'TienDaDong', className: "view-money " + hide },
                                { "data": 'CongNo', className: "view-money " + hide },
                                { "data": 'Under' },
                                { "data": 'Classic' },
                                { "data": 'Sport' },
                                { "data": 'TotalBill' },
                                { "data": 'NumberOfMonth' },
                                { "data": 'TimeOfLearing' },
                                { "data": 'GuessOfDateGraduating' },
                            ],
                            pagingType: "full_numbers",
                            language: {
                                lengthMenu: "Dòng / trang _MENU_",
                                paginate: {
                                    previous: "<",
                                    first: "Đầu",
                                    last: "Cuối",
                                    next: ">"
                                },
                                info: "Dòng _START_ tới _END_ của _TOTAL_ dòng",
                                search: "Nhập tìm kiếm:"
                            },
                            lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "Tất cả"]],
                            fixedHeader: true,
                            fixedColumns: fixedColumn,
                            scrollY: "80vh",
                            scrollX: scrollx,
                        });

                }
                finishLoading();
            }
            function BindClass() {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/S4M_Student/StudentReportListingV1.aspx/GetListClass",
                    contentType: "application/json; charset=utf-8",
                    datatype: "JSON",
                    //data:{},
                    success: function (response) {
                        console.log(response);
                        var option = '<option value="0" selected="selected">Chọn lớp học</option>';
                        $(response.d).each(function (i, v) {
                            option += `<option value="${v.Id}">${v.Name}</option>`;
                        });
                        $('select[name=classId]').empty().append(option);
                        $('.select').select2();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        ShowMessage("Cảnh báo", "status:" + jqXHR.status + ',' + textStatus + '<br>- ' + errorThrown, 3, 7000);

                    }
                });
            }
            function BindSalonHQ() {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/S4M_Student/StudentReportListingV1.aspx/GetSalonHQ",
                    contentType: "application/json; charset=utf-8",
                    datatype: "JSON",
                    data: `{permissionViewAll :${permisisionViewAll}}`,
                    success: function (response) {
                        console.log(response);
                        var option = '<option value="0"  selected="selected">Chọn salon</option>';
                        $(response.d).each(function (i, v) {
                            option += `<option value="${v.Id}">${v.ShortName}</option>`;
                        });
                        $('select[name=salonId]').empty().append(option);
                        $('.select').select2();
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        ShowMessage("Cảnh báo", "status:" + jqXHR.status + ',' + textStatus + '<br>- ' + errorThrown, 3, 7000);
                    }
                });
            }
        </script>
    </asp:Panel>
</asp:Content>
