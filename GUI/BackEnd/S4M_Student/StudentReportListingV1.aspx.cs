﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.S4M_Student
{
    public partial class StudentReportListingV1 : System.Web.UI.Page
    {
        public bool Perm_Access { get; private set; }
        public bool Perm_Delete { get; private set; }
        public bool Perm_ViewAllData { get; private set; }
        public bool Perm_Edit { get; private set; }

        protected void SetPermission()
        {
            if (Session["User_Id"] == null)
            {
                var msgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", msgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {

            }
        }
        [WebMethod]
        public static object GetData(int classId, int salonId, int statusTuitionId)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var listInfoStudent = (from student in db.Stylist4Men_Student
                                           join clas in db.Stylist4Men_Class on student.ClassId equals clas.Id into a
                                           from clas in a.DefaultIfEmpty()
                                           join package in db.Stylist4Men_StudyPackage on student.StudyPackageId equals package.Id
                                           join staff in db.Staffs on student.Id equals staff.StudentId into b
                                           from staff in b.DefaultIfEmpty()
                                           join graduation in db.Stylist4Men_GraduationScore on staff.Id equals graduation.StaffId into d
                                           from graduation in d.DefaultIfEmpty()
                                           join salon in db.Tbl_Salon on staff.SalonId equals salon.Id into c
                                           from salon in c.DefaultIfEmpty()
                                           where student.IsDelete == false &&
                                                 student.IsAccept == true &&
                                                 (student.SalonId == salonId || salonId == 0) &&
                                                 (student.StatusTuition == statusTuitionId || statusTuitionId == -1) &&
                                                 (student.ClassId == classId || classId == 0)

                                           select new studentStatisticTemp
                                           {
                                               StudentId = student.Id,
                                               StaffId = staff.Id,
                                               Fullname = student.Fullname,
                                               Email = student.Email,
                                               ClassName = clas.Name,
                                               NamePackage = package.NamePackage,
                                               StatusTuition = student.StatusTuition ?? 0,
                                               NameSalon = salon.ShortName,
                                               TienDaDong = student.TotalAmountCollected ?? 0,
                                               TotalBill = student.TotalBill ?? 0,
                                               TotalAmountPaid = student.TotalAmountPaid ?? 0,
                                               TotalAmountCollected = student.TotalAmountCollected ?? 0,
                                               IsSalonHoiQuan = salon.IsSalonHoiQuan ?? false,
                                               StartDate = student.CreatedTime,
                                               GraduationStatusId = graduation.StatusId ?? 0,
                                               TotalBillFree = student.TotalBill
                                           }).ToList();

                    var listStudentId = listInfoStudent.Where(r => r.StudentId != null).Select(r => r.StudentId).ToList();
                    var listStaffId = listInfoStudent.Where(r => r.StaffId != null).Select(r => r.StaffId).ToList();
                    var listBillClub = (from a in db.BillServices
                                        join staff in db.Staffs on a.Staff_Hairdresser_Id equals staff.Id
                                        join b in db.Tbl_Salon on staff.SalonId equals b.Id
                                        where a.IsDelete == 0
                                        && a.Pending == 0
                                        && listStaffId.Contains(a.Staff_Hairdresser_Id ?? 0)
                                        && b.Publish == true
                                        && b.IsSalonHoiQuan == true
                                        && b.IsDelete == 0
                                        select new
                                        {
                                            BillId = a.Id,
                                            StaffId = a.Staff_Hairdresser_Id ?? 0
                                        }).GroupBy(r => new { r.StaffId }).Select(r => new
                                        {
                                            StaffId = r.Key.StaffId,
                                            TotalBill = r.Count()
                                        }).ToList();
                    var listBillStudent = (from a in db.Stylist4Men_BillCutFree
                                           where listStudentId.Contains(a.StudentId ?? 0)
                                           select new
                                           {
                                               BillId = a.Id,
                                               StudentId = a.StudentId ?? 0,
                                               HairStyleId = a.HairStyleId ?? 0,
                                               CreateDate = a.CreatedTime
                                           }).ToList();
                    var listStartDate = listBillStudent.GroupBy(r => r.StudentId).Select(r => new
                    {
                        StudentId = r.Key,
                        StartDate = r.OrderBy(g=>g.CreateDate).Select(g=>g.CreateDate).FirstOrDefault()
                    }).ToList();
                    var listStudentGroupHairStyle = listBillStudent
                        .GroupBy(r => new { r.StudentId, r.HairStyleId }).Select(r => new
                        {
                            StudentId = r.Key.StudentId,
                            HairStyle = r.Key.HairStyleId,
                            TotalHairStyle = r.Count()
                        }).ToList();
                    var listHairStyleClub = (from a in db.BillServices
                                             join staff in db.Staffs on a.Staff_Hairdresser_Id equals staff.Id
                                             join b in db.Tbl_Salon on staff.SalonId equals b.Id
                                             join c in db.Customer_HairMode_Bill on a.Id equals c.BillId
                                             where a.IsDelete == 0
                                                && a.Pending == 0
                                                && listStaffId.Contains(a.Staff_Hairdresser_Id ?? 0)
                                                && b.Publish == true
                                                && b.IsSalonHoiQuan == true
                                                && c.IsDelete == false
                                                && (c.HairStyleId == 15 || c.HairStyleId == 16 || c.HairStyleId == 7)
                                             select new
                                             {
                                                 StaffId = a.Staff_Hairdresser_Id ?? 0,
                                                 HairStyleId = c.HairStyleId
                                             }).GroupBy(r => new { r.StaffId, r.HairStyleId }).Select(r => new
                                             {
                                                 StaffId = r.Key.StaffId,
                                                 HairStyleId = r.Key.HairStyleId,
                                                 TotalHairStyle = r.Count()
                                             }).ToList();

                    var result = new List<studentStatisticTemp>();
                    foreach (var item in listInfoStudent)
                    {
                        if(item.StudentId == 1099)
                        {

                        }
                        var record = new studentStatisticTemp();
                        record.Fullname = item.Fullname;
                        record.Email = item.Email;
                        record.ClassName = item.ClassName;
                        record.NamePackage = item.NamePackage;
                        record.TienDaDong = item.TienDaDong;
                        record.CongNo = (float)(item.TotalAmountPaid) - (float)(item.TotalAmountCollected);

                        // Tính tổng bill hội quán.
                        var totalBillClub = listBillClub.Where(r => r.StaffId == item.StaffId).Select(r => r.TotalBill).FirstOrDefault();
                        //var totalBillFree = listBillStudent.Where(r => r.StudentId == item.StudentId).Count();
                        record.TotalBill = totalBillClub + (item.TotalBillFree ?? 0);
                        // Tinh kieu toc classic hội quán
                        record.Classic = listHairStyleClub.Where(r => r.StaffId == item.StaffId && r.HairStyleId == 15).Select(r => r.TotalHairStyle).FirstOrDefault();
                        record.Under = listHairStyleClub.Where(r => r.StaffId == item.StaffId && r.HairStyleId == 16).Select(r => r.TotalHairStyle).FirstOrDefault();
                        record.Sport = listHairStyleClub.Where(r => r.StaffId == item.StaffId && r.HairStyleId == 7).Select(r => r.TotalHairStyle).FirstOrDefault();
                        // Tinh kieu toc diem cat
                        record.Classic += listStudentGroupHairStyle.Where(r => r.StudentId == item.StudentId && r.HairStyle == 15).Select(r => r.TotalHairStyle).FirstOrDefault();
                        record.Under += listStudentGroupHairStyle.Where(r => r.StudentId == item.StudentId && r.HairStyle == 16).Select(r => r.TotalHairStyle).FirstOrDefault();
                        record.Sport += listStudentGroupHairStyle.Where(r => r.StudentId == item.StudentId && r.HairStyle == 7).Select(r => r.TotalHairStyle).FirstOrDefault();

                        var date = listStartDate.Where(r=>r.StudentId == item.StudentId).Select(r=>r.StartDate).FirstOrDefault();
                        // GraduationStatusid = 2 "Trạng thái đã tốt nghiệp"(vào 30shine);
                        if (date != null && item.GraduationStatusId != 2)
                        {
                            var startDate = date.Value;
                            var months = -1;
                            startDate = startDate.Date;
                            while (startDate < DateTime.Now.Date)
                            {
                                months++;
                                startDate = startDate.AddMonths(1);
                            }
                            record.NumberOfMonth = months;
                            var totalDaysCut = (int)DateTime.Now.Subtract(date.Value).TotalDays;
                            record.TimeOfLearing = (int)((350 / ((double)record.TotalBill / totalDaysCut)) - totalDaysCut);
                            record.TimeOfLearing = record.TimeOfLearing < 0 ? 0 : record.TimeOfLearing;
                            record.GuessOfDateGraduating = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(record.TimeOfLearing));
                        }
                        else
                        {
                            record.NumberOfMonth = 0;
                            record.GuessOfDateGraduating = "";
                            
                        }
                        result.Add(record);
                    }
                    return result;

                }

            }
            catch (Exception e)
            {
                throw e;
            }
        }
        [WebMethod]
        public static object GetSalonHQ(int permissionViewAll)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var SalonId = Convert.ToInt32(HttpContext.Current.Session["SalonId"]);
                    var UserId = Convert.ToInt32(HttpContext.Current.Session["User_Id"]);
                    if (permissionViewAll == 1)
                    {
                        var listSalon = db.Tbl_Salon.Where(w => w.IsDelete == 0 && w.Publish == true && w.IsSalonHoiQuan == true).OrderBy(o => o.Order).ToList();
                        return listSalon;
                    }
                    else
                    {
                        var listSalon = (
                                      from p in db.PermissionSalonAreas
                                      join s in db.Tbl_Salon on p.SalonId equals s.Id
                                      where
                                      p.IsDelete == false && p.StaffId == UserId
                                      && s.Publish == true && s.IsDelete == 0 && s.IsSalonHoiQuan == true
                                      select s
                                 ).OrderBy(o => o.Order).ToList();
                        return listSalon;

                    }
                }

            }
            catch (Exception e)
            {
                throw e;
            }
        }
        [WebMethod]
        public static object GetListClass()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var list = db.Stylist4Men_Class.Where(w => w.IsDelete == false && w.Publish == true).OrderBy(o => o.Id).ToList();
                    return list;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public class studentStatisticTemp
        {
            public int? StudentId { get; set; }
            public int? StaffId { get; set; }
            public string Fullname { get; set; }
            public string Email { get; set; }
            public string ClassName { get; set; }
            public string NamePackage { get; set; }
            public int StatusTuition { get; set; }
            public string NameSalon { get; set; }
            public int TienDaDong { get; set; }
            public int TotalBill { get; set; }
            public float CongNo { get; set; }
            public bool IsSalonHoiQuan { get; set; }
            public int NumberOfMonth { get; set; }
            public DateTime? StartDate { get; set; }
            public int Under { get; set; }
            public int Classic { get; set; }
            public int Sport { get; set; }
            public int TimeOfLearing { get; set; }
            public string GuessOfDateGraduating { get; set; }
            public int TotalAmountPaid { get; set; }
            public int TotalAmountCollected { get; set; }
            public  int? GraduationStatusId { get; set; }
            public int? TotalBillFree { get; set; }
        }
    }
}