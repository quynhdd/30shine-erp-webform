﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Class_Add.aspx.cs" Inherits="_30shine.GUI.BackEnd.S4M_Student.Class_Add" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ProductAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            #success-alert {
                top: 100px;
                right: 10px;
                position: fixed;
                width: 20% !important;
                z-index: 2000;
            }

            #MsgSystem {
                color: green;
                font-size: 16px;
            }
        </style>

        <div class="alert alert-danger" id="success-alert" style="color: red;">
            <span id="msg-alert"></span>
        </div>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý Stylist4Men &nbsp;&#187; Lớp học &nbsp;&#187;</li>
                        <li class="li-listing"><a href="/admin/s4m/lop-hoc/danh-sach.html">Danh sách</a></li>
                        <li class="li-add"><a href="/admin/s4m/lop-hoc/them-moi.html">Thêm mới</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <div class="table-wp">
                    <table class="table-add admin-product-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Thông tin lớp học</strong></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Tên khóa</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp ">
                                        <asp:TextBox ID="Name" runat="server" ClientIDMode="Static" placeholder="Nhập Tên khóa" Width="49%"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Ngày tạo khóa</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="DateStart" placeholder="Nhập Ngày tạo khóa" CssClass="txtDateTime" runat="server" ClientIDMode="Static" Width="49%"></asp:TextBox>
                                    </span>
                                    <span id="KTNgayThang" style="color: red;"></span>
                                </td>

                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Tên giáo viên chủ nhiệm</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp" id="GVChuNhiem">
                                        <asp:DropDownList ID="ddlGVChuNhiem" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Tên giáo viên điểm cắt</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp" id="GVDiemCat">
                                        <asp:DropDownList ID="ddlGVDiemCat" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Tên giáo viên hội quán</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp" id="GVHoiQuan">
                                        <asp:DropDownList ID="ddlGVHoiQuan" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-send">
                                <td class="col-xs-3 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <asp:Panel ID="btnAdd" CssClass="btn-send" runat="server" ClientIDMode="Static" onclick="Add()">Hoàn tất</asp:Panel>
                                        <asp:Button ID="btnAdd_hidden" ClientIDMode="Static" runat="server" OnClick="AddOrUpdate" Text="Click" Style="display: none;" />
                                    </span>
                                </td>
                            </tr>

                        </tbody>
                    </table>

                </div>

            </div>
        </div>
        <script>
            jQuery(document).ready(function () {
                $("#KTNgayThang").hide();
                $("#glbS4M").addClass("active");
                $("#glbS4MClass").addClass("active");
                $("#success-alert").hide();
                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        $input.val($input.val().split(' ')[0]);
                    },
                    scrollMonth: false,
                    scrollTime: false,
                    scrollInput: false
                });


                $("#Name").bind("click", function () {
                    $("#Name").css("border-color", "#ddd");
                });
                $("#DateStart").bind("click", function () {
                    $("#DateStart").css("border-color", "#ddd");
                    $("#KTNgayThang").hide();
                });
                $("#ddlGVChuNhiem").bind("click", function () {
                    $("#ddlGVChuNhiem").css("border-color", "#ddd");
                });
                $("#ddlGVDiemCat").bind("click", function () {
                    $("#ddlGVDiemCat").css("border-color", "#ddd");
                });
                $("#ddlGVHoiQuan").bind("click", function () {
                    $("#ddlGVHoiQuan").css("border-color", "#ddd");
                });
            });
            // validate
            function Add() {
                var kt = true;
                var error = 'Vui lòng nhập đầy đủ thông tin!';
                var name = $('#Name').val();
                var dateStart = $('#DateStart').val();
                var gvChunhiem = $('#ddlGVChuNhiem :selected').val();
                var gvDiemCat = $('#ddlGVDiemCat :selected').val();
                var gvHoiQuan = $('#ddlGVHoiQuan :selected').val();
                var notDate = CheckDate($('#DateStart').val());
                if (notDate) {
                    kt = false;
                    $("#KTNgayThang").show();
                    $("#KTNgayThang").text("Phải lớn hơn Ngày-Tháng-Năm hiện tại");
                }

                if (name == "") {
                    kt = false;
                    error += "[Họ tên]";
                    $("#Name").css("border-color", "red");
                }
                if (dateStart == "" || CheckDate($("#DateStart").val())) {
                    kt = false;
                    error += "[Ngày Bắt đầu],";
                    $("#DateStart").css("border-color", "red");
                }

                if (gvChunhiem == 0 || gvChunhiem === undefined) {
                    kt = false;
                    error += "[Giáo viên chủ nhiệm],";
                    $("#ddlGVChuNhiem").css("border-color", "red");
                }
                if (gvDiemCat == 0 || gvDiemCat === undefined) {
                    kt = false;
                    error += "[Giáo viên điểm cắt], ";
                    $("#ddlGVDiemCat").css("border-color", "red");
                }
                if (gvHoiQuan == 0 || gvHoiQuan === undefined) {
                    kt = false;
                    error += "[Giáo viên hội quán]";
                    $("#ddlGVHoiQuan").css("border-color", "red");
                }

                if (!kt) {
                    $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                        $("#success-alert").slideUp(2000);
                    });
                    $("#msg-alert").text(error);
                    $("#MsgSystem").show();
                }

                else {
                    //chạy hàm thêm AddOrUpadte trong codebehind
                    $("#btnAdd_hidden").click();
                }
            }

            //Hàm kiểm tra ngày tháng phải lớn hơn ngày hiện tại--
            function CheckDate(date) {
                var isError = false;
                var string = $("#DateStart").val();
                var int1 = parseInt(string.substr(0, 2));//lấy ngày của đối tượng
                var int2 = parseInt(string.substr(3, 2));//lấy tháng của đối tượng
                var int3 = parseInt(string.substr(6, 4));//lấy năm của đối tượng

                var dt = new Date();
                var dt1 = dt.getDate();//lấy ngày hiện tại
                var dt2 = dt.getMonth();//lấy tháng hiện tại
                dt2 += 1;
                var dt3 = dt.getFullYear();//lấy năm hiện tại
                if (int1 < dt1) { isError = true; }
                if (int2 < dt2) { isError = true; }
                if (int3 < dt3) { isError = true; }

                return isError;
            }
        </script>
    </asp:Panel>
</asp:Content>



