﻿using _30shine.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using System.Web.Services;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using System.Globalization;

namespace _30shine.GUI.BackEnd.S4M_Student
{
    public partial class Student_Listing : System.Web.UI.Page
    {
        private string FirstOfMonth = String.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
        private string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
        CultureInfo culture = new CultureInfo("vi-VN");
        private string PageID = "S4M_HV";
        private bool Perm_Access = false;
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        private bool Perm_ViewAllData = false;
        protected Paging PAGING = new Paging();
        string id;
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = FirstOfMonth;
                TxtDateTimeTo.Text = Today;
                Library.Function.bind_S4M_Class(new List<DropDownList>() { ddlClass });
                Bind_Rpt();
                BindLevelOfConcern();
            }
        }

        /// <summary>
        /// Bind data to dropdownlist mức độ quan tâm
        /// </summary>
        private void BindLevelOfConcern()
        {
            ITblStatusModel db = new TblStatusModel();
            var data = db.GetList();
            ddlLevelOfConcern.Items.Add(new ListItem("Chọn mức độ quan tâm", "0"));
            if (data.Count > 0)
            {
                for (int i = 0; i < data.Count; i++)
                {
                    ddlLevelOfConcern.Items.Add(new ListItem(data[i].Name, data[i].Id.ToString()));
                }
                ddlLevelOfConcern.DataBind();
            }
        }

        /// <summary>
        /// Bind data to Rpt
        /// </summary>
        private void Bind_Rpt()
        {
            try
            {
                IStylist4MenStudentModel db = new Stylist4MenStudentModel();
                int integer;
                var timeFrom = new DateTime();
                var timeTo = new DateTime();
                if (TxtDateTimeFrom.Text != "")
                {
                    timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                    if (TxtDateTimeTo.Text != "")
                    {
                        timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                    }
                    else
                    {
                        timeTo = timeFrom.AddDays(1);
                    }
                    int classId = Convert.ToInt32(ddlClass.SelectedValue);
                    int LevelOfConcernId = int.TryParse(ddlLevelOfConcern.SelectedValue, out integer) ? integer : 0;
                    int NumberOfCreateProfileId = int.TryParse(ddlNumberOfCreateProfile.SelectedValue, out integer) ? integer : 0;
                    var list = db.GetByAllListStudent(timeFrom, timeTo, classId, LevelOfConcernId, NumberOfCreateProfileId);
                    Bind_Paging(list.Count);
                    Rpt.DataSource = list.Skip(PAGING._Offset).Take(PAGING._Segment).OrderByDescending(o => o.Id).OrderByDescending(o => o.Id).ToList();
                    Rpt.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Chuyển trạng thái 
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="IsAccept"></param>
        [WebMethod]
        public static object CheckIsAccept(int Id, bool IsAccept)
        {
            using (var db = new Solution_30shineEntities())
            {
                var message = new Library.Class.cls_message();
                var obj = new Stylist4Men_Student();
                obj = (from c in db.Stylist4Men_Student
                       where c.Id == Id
                       select c).FirstOrDefault();
                if (obj != null)
                {
                    obj.IsAccept = IsAccept;
                    db.SaveChanges();
                    message.success = true;
                }
                return message;
            }
        }

        /// <summary>
        /// Update note 
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="Note"></param>
        /// <returns></returns>
        [WebMethod]
        public static object UpdateNote(int Id, string Note)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var message = new Library.Class.cls_message();
                    var obj = new Stylist4Men_Student();
                    obj = (from c in db.Stylist4Men_Student
                           where c.Id == Id
                           select c).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.Note = Note;
                        db.SaveChanges();
                        message.success = true;
                    }
                    return message;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Rpt();
            RemoveLoading();
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
        protected void Bind_Paging(int totalRecord)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(totalRecord) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int totalRecord)
        {
            int TotalRow = totalRecord - PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        /// <summary>
        /// check permission
        /// </summary>
        /// 
        protected void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }

        /// <summary>
        /// Update Mức độ quan tâm
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="numberId"></param>
        [WebMethod]
        public static object UpdateNumber(int Id, int numberId)
        {
            try
            {
                var message = new Library.Class.cls_message();
                IStylist4MenStudentModel db = new Stylist4MenStudentModel();
                var data = db.GetById(Id);
                if (data != null)
                {
                    data.NumberOfCreateProfile = numberId;
                    db.UpdateNumberOfCreateProfile(data);
                }
                message.success = true;
                return message;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}