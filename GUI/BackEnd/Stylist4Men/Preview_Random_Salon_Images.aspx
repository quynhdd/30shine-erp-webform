﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Preview_Random_Salon_Images.aspx.cs" Inherits="_30shine.GUI.BackEnd.Stylist4Men.PreviewImage.Preview_Random_Salon_Images" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <title>Xem ảnh ngẫu nhiên theo Salon</title>
    <script src="/Assets/js/jquery.v1.11.1.js"></script>
    <link href="../../../Assets/css/bootstrap/bootstrap.css" rel="stylesheet" />
    <link href="/Assets/css/font.css" rel="stylesheet" />
    <link href="/Assets/css/font-awesome.css" rel="stylesheet" />
    <link href="/Assets/css/csvc.css" rel="stylesheet" />
    <link href="../../../Assets/libs/toastr/toastr.css" rel="stylesheet" />

    <script src="../../../Assets/js/bootstrap/bootstrap.js"></script>
    <script src="/Assets/libs/canvasjs-1.9.8/canvasjs.min.js"></script>
    <script src="/Assets/libs/canvasjs-1.9.8/jquery.canvasjs.min.js"></script>
    <script src="/Assets/js/bootstrap/bootstrap-modal.js"></script>
    <link href="/Assets/css/jquery.datetimepicker.css?v=2" rel="stylesheet" />
    <script src="/Assets/js/jquery.datetimepicker.js"></script>
    <script src="../../../Assets/libs/toastr/toastr.js"></script>
    <script src="../../../Assets/js/common.js"></script>
    <style>
        a.disabled {
            color: gray;
        }

        .btn {
            width: 100%;
            background: #ccc;
            padding: 10px 5px;
            text-align: center;
            margin-top: 5px;
            margin-bottom: 10px;
            font-size: 14px;
            white-space: normal;
        }

        .item-status-wrap {
            padding-left: 5px;
            padding-right: 5px;
        }

        .item-status.btn-ok.active {
            background: #4cff00;
        }

        .item-status.btn-xs.active {
            background: red;
        }


        .top-info {
            border: none !important;
        }

        .preview-img {
            background: #fff;
        }

        .heading-title {
            background: #000;
            padding: 10px;
            text-align: center;
            color: #fff;
            text-transform: uppercase;
            font-size: 16px;
        }

        .top-filter .form-control {
            margin-top: 15px;
            margin-bottom: 5px;
        }

        .top-filter {
            padding: 5px;
            border-bottom: 1px solid #f2f2f2;
        }

        .top-info {
            padding: 5px;
            border-bottom: 1px solid #f2f2f2;
        }

            .top-info .bill-info {
                text-align: center;
            }

            .top-info .salon-name {
                text-align: center;
                padding-top: 4px;
            }

            .top-info strong {
                font-weight: bold;
            }

        .top4-img {
            padding: 5px;
            border-bottom: 1px solid #f2f2f2;
        }

            .top4-img .img-item {
                background: #808080;
                padding: 10px;
                text-align: center;
                border-right: 1px solid #fff;
            }

                .top4-img .img-item img {
                    max-width: 60%;
                }

            .top4-img .col1 {
                border-bottom: 1px solid #fff;
                width: 100%;
                float: left;
            }

            .top4-img .col2 {
                width: 100%;
                float: left;
            }

        .img-nav {
            padding: 5px;
            border-bottom: 1px solid #f2f2f2;
            text-align: center;
        }

        .btn-default {
            background-color: #e6e6e6;
        }

        canvas {
        }

        .modal-dialog {
            max-width: 400px;
            margin: 10px auto;
        }

        .modal-body {
            padding: 0;
        }

        .modal-header {
            padding: 5px 15px;
        }

        .modal-footer {
            margin-top: 0;
            padding: 5px 10px;
        }

            .modal-footer i {
                font-size: 46px;
                cursor: pointer;
            }

        .carousel-control {
            top: inherit;
            bottom: 5%;
            padding-top: 10px;
            padding-bottom: 10px;
        }

        @media(max-width: 768px) {
            .modal-dialog {
                width: 100%;
            }
        }

        .page-loading {
            width: 100%;
            height: 100%;
            position: fixed;
            top: 0;
            left: 0;
            z-index: 100000;
            background: rgba(0,0,0, 0.65) url(/Assets/images/load_ring.svg) center no-repeat;
            text-align: center;
            display: none;
        }

            .page-loading p {
                color: #fcd344;
                position: relative;
                top: 50%;
                padding-top: 37px;
                line-height: 22px;
            }

        legend {
            padding: 0 10px;
            margin-bottom: 5px;
            font-size: inherit;
            font-family: Roboto Condensed Regular;
        }

        fieldset {
            padding: 0 5px;
            margin-top: 10px;
        }
    </style>
</head>

<body>
    <div class="wp">
        <div class="preview-img">
            <div class="heading-title">Xem ảnh lịch sử</div>
            <!-- Filter-->
            <div class="top-filter">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <select name="ddlClass" id="ddlClass" class="form-control" onchange="getStudentByClass()">
                    </select>
                </div>
                <!-- Filter-->
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <select name="ddlSalon" id="ddlSalon" class="form-control select" onchange="getStylistBySalon()">
                    </select>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <select name="ddlStylist" id="ddlStylist" class="form-control">
                        <option value="0">Chọn Stylist/Học viên</option>
                    </select>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <input runat="server" name="txtToDate" type="text" value="" id="txtToDate" class="form-control txtDateTime" placeholder="Ngày ..." />
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <input type="submit" id="btnTimKiem" class="form-control btn-default btnSearch" value="Tìm" />
                </div>
            </div>
            <!-- Customer info-->
            <div class="top-info">
                <div class="bill-info">SĐT: &nbsp<strong id="pre-date"></strong></div>
                <div class="salon-name">Bill ID: &nbsp<strong id="pre-id"></strong></div>
                <div class="salon-name">DV: &nbsp<strong id="pre-dv"></strong></div>
                <div class="salon-name" id="status"></div>
            </div>
            <!-- Kiểu tóc-->
            <fieldset>
                <legend><b>Kiểu tóc</b></legend>
                <select class="col-lg-6 col-md-6 col-sm-6 col-xs-6 form-control btnHairList" id="sltHairStyle" style="padding-top: 0; text-align: center">
                    <option value="0">--Chọn kiểu tóc--</option>
                </select>
            </fieldset>
            <!-- Ảnh chụp khách-->
            <div class="top4-img">
                <div class="container">
                    <div class="row">
                        <div class="col1">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 img-item" data-toggle="modal" data-target="#myModal" data-slide-to="0">
                                <img src="https://30shine.com/images/1.jpg" alt="" id="pre-img1" class="image" />
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 img-item" data-toggle="modal" data-target="#myModal" data-slide-to="1">
                                <img src="https://30shine.com/images/2.jpg" alt="" id="pre-img2" class="image" />
                            </div>
                        </div>
                        <div class="col2">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 img-item" data-toggle="modal" data-target="#myModal" data-slide-to="2">
                                <img src="https://30shine.com/images/3.jpg" alt="" id="pre-img3" class="image" />
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 img-item" data-toggle="modal" data-target="#myModal" data-slide-to="3">
                                <img src="https://30shine.com/images/4.jpg" alt="" id="pre-img4" class="image" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Lỗi cắt-->
            <fieldset>
                <legend><b>Lỗi cắt</b></legend>
                <div class="top-info text-center btnList" style="padding-top: 0;"></div>
            </fieldset>
            <%--<!-- Kiểu khuôn mặt-->
            <fieldset>
                <legend><b>Kiểu khuôn mặt</b></legend>
                <div class="top-info text-center btnFaceList" style="padding-top: 0;"></div>
            </fieldset>--%>
            <!-- Ghi chú-->
            <div class="top-info">
                <table style="width: 100%">
                    <tr>
                        <td style="width: 25%; text-align: center;">Ghi chú của Stylist</td>
                        <td style="width: 75%;">
                            <textarea id="txtNoteByStylist" class="form-control" rows="2"></textarea>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="top-info">
                <table style="width: 100%">
                    <tr>
                        <td style="width: 25%; text-align: center;">Ghi chú check lỗi </td>
                        <td style="width: 75%;">
                            <textarea id="txtErrorNote" class="form-control ErrorNote" rows="2"></textarea>
                        </td>
                    </tr>
                </table>
            </div>

            <!-- btn-->
            <div class="img-nav">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-4 col-sm-4">
                            <input type="submit" id="btnPreview" class="form-control btn-default" value="Trước" />
                        </div>
                        <div class="col-xs-4 col-sm-4">
                            &nbsp(Đang xem hóa đơn <strong id="current-index">0</strong>/<strong id="total-index">0</strong>)&nbsp
                        </div>
                        <div class="col-xs-4 col-sm-4">
                            <input type="submit" id="btnNext" class="form-control btn-default" value="Sau" />
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" value="0" id="hdfIndexPage" />
            <input type="hidden" value="0" id="HDF_BillID" />
        </div>
    </div>

    <!-- Pop up-->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog" id="modalDialog">
            <!-- Modal content-->
            <div class="modal-content" id="modalContent">
                <div class="modal-header" id="modalHeader">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Check lỗi cắt</h4>
                </div>
                <div class="modal-body">
                    <div id="myCarousel" class="carousel" data-ride="carousel" data-interval="false">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                            <li data-target="#myCarousel" data-slide-to="3"></li>
                        </ol>
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox" id="canvasWrap">
                            <div class="item active" id="div-image1">
                                <canvas id="pre-img1-can" data-order="1" style="background-repeat: no-repeat; background-size: 100% auto!important;" title="Before"></canvas>
                            </div>
                            <div class="item" id="div-image2">
                                <canvas id="pre-img2-can" data-order="2" style="background-repeat: no-repeat; background-size: 100% auto!important" title="Left"></canvas>
                            </div>
                            <div class="item" id="div-image3">
                                <canvas id="pre-img3-can" data-order="3" style="background-repeat: no-repeat; background-size: 100% auto!important" title="Right"></canvas>
                            </div>
                            <div class="item" id="div-image4">
                                <canvas id="pre-img4-can" data-order="4" style="background-repeat: no-repeat; background-size: 100% auto!important" title="After"></canvas>
                            </div>
                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev" id="prev-Slide">
                            <%--<i class="fa fa-arrow-left" aria-hidden="true"></i>--%>
                            <span class="fa fa-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next" id="next-Slide">
                            <span class="fa fa-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                            <%--<i class="fa fa-arrow-right" aria-hidden="true"></i>--%>
                        </a>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-1"></div>
                        <div class="col-xs-2">
                            <i class="fa fa-plus-circle btnAddCycle" aria-hidden="true" onclick="addCycleToCanvas()" title="Thêm vòng lỗi"></i>
                        </div>
                        <div class="col-xs-2">
                            <i class="fa fa-minus-circle btnUndoCycle" aria-hidden="true" onclick="undoCycle()" title="undo"></i>
                        </div>
                        <div class="col-xs-2">
                            <i class="fa fa-floppy-o btnSaveError" aria-hidden="true" onclick="saveCheckedImages()" title="Lưu ảnh"></i>
                        </div>
                        <div class="col-xs-2">
                            <i class="fa fa-refresh btnReset" id="btnRefresh" aria-hidden="true" title="Sửa lại"></i>
                        </div>
                        <div class="col-xs-2">
                            <i class="fa fa-times-circle" aria-hidden="true" onclick="closeCanvas()" data-dismiss="modal" title="Đóng"></i>
                        </div>
                        <div class="col-xs-1"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
    <script src="../../../Assets/js/select2/select2.min.js"></script>
    <script>
        $('.select').select2();
    </script>
    <style>
        .select2-container {
            width: 100% !important;
            margin-top: 15px !important;
        }

        .select2-container--default .select2-selection--single {
            height: 32px !important;
            padding-top: 1px !important;
        }
    </style>
    <!-- Loading-->
    <div class="page-loading">
        <p>Vui lòng đợi trong giây lát...</p>
    </div>

    <script type="text/ecmascript">
        var _ListPreviewImg = [];
        var objPreviewImg = {};
        var billID;

        //var listElement = <%= listElement_Perm %>;

        $(document).ready(function () {
            startLoading();
            getListSalon();
            GetErrorList();
            GetListHairMode();
            getListClass();
            //GetFaceType();
            cusId = "";
            /* fn tìm kiếm bill ảnh theo điều kiện lọc */
            $('#btnTimKiem').click(function () {
                //initCircles();
                var _ClassId = $("#ddlClass").val();
                var _SalonId = $("#ddlSalon").val();
                var _StylistId = $("#ddlStylist").val();
                var _ToDate = $("#txtToDate").val();

                if (_ToDate == "") {
                    alert("Bạn chưa chọn ngày");
                    return;
                }

                if ($("#ddlClass").val() != "0") {

                    GetBillImage_S4M();
                }
                else if ($("#ddlSalon").val() != "0") {
                    $('#ddlClass option').removeAttr('selected').filter('[value=0]').attr('selected', 'selected');
                    startLoading();
                    $.ajax({
                        type: "POST",
                        url: "/GUI/FrontEnd/PreviewImage/Preview_Random_Salon_Images.aspx/GetRandomBillImage",
                        data: '{_Date: "' + _ToDate + '", _SalonId: ' + _SalonId + ', _StylistId: ' + _StylistId + ', _imageStatus:"" }',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            _ListPreviewImg = [];

                            $.each(response.d, function (key, value) {
                                objPreviewImg = {};
                                objPreviewImg.Id = value.Id;
                                $("#HDF_BillID").val(objPreviewImg.Id);
                                objPreviewImg.BillDate = value.BillDate;
                                objPreviewImg.SalonName = value.SalonName;
                                objPreviewImg.CustomerPhone = value.CustomerPhone;
                                objPreviewImg.CustomerName = value.CustomerName;
                                objPreviewImg.Img1 = value.Img1;
                                objPreviewImg.Img2 = value.Img2;
                                objPreviewImg.Img3 = value.Img3;
                                objPreviewImg.Img4 = value.Img4;
                                objPreviewImg.ImageStatusId = value.ImageStatusId;
                                objPreviewImg.ErrorNote = value.ErrorNote;
                                objPreviewImg.CustomerId = value.CustomerId;
                                objPreviewImg.ServiceIds = value.ServiceIds;
                                _ListPreviewImg.push(objPreviewImg);

                                // Set giá trị mặc định billID;
                                if (key == 0) {
                                    setBillID(value.Id);
                                    bindBillStatus(value.ImageStatusId);
                                }
                            });

                            if (_ListPreviewImg.length > 0) {
                                SetPreviewInfo(0);
                                $('#hdfIndexPage').val("0");
                                $('#total-index').text(_ListPreviewImg.length);
                                $('#current-index').text('1');

                            }

                            finishLoading();
                        },
                        failure: function (response) { console.log(response.d); }
                    });

                }
                var _StylistId = $("#ddlStylist").val();
                var _ToDate = $("#txtToDate").val();
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/PreviewImage/Preview_Random_Salon_Images.aspx/checkStylistFullImage",
                    data: '{date: "' + _ToDate + '",  stylistID: ' + _StylistId + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        //alert(response.d);
                        if (response.d == "Success") {
                            $("#status").html("<span style='color:green'>Stylist đã chụp đủ ảnh</span>");
                        }
                        else {
                            $("#status").html("<span style='color:red'>Stylist chụp thiếu ảnh</span>");
                        }
                    }
                })
            });

            /* fn khi chuyển tiếp sang bill tiếp theo */
            $('#btnNext').click(function () {
                updateErrorNote();
                var totalIndex = parseInt(_ListPreviewImg.length);
                var index = parseInt($('#hdfIndexPage').val());

                if ((totalIndex > 0) && (index < totalIndex - 1)) {
                    SetPreviewInfo(index + 1);
                    $('#hdfIndexPage').val(index + 1);
                    $('#current-index').text((index + 2));
                }

                cusHairId = "";
            });

            /* fn khi quay lại bill trước đó */
            $('#btnPreview').click(function () {
                updateErrorNote();
                var totalIndex = parseInt(_ListPreviewImg.length);
                var index = parseInt($('#hdfIndexPage').val());

                if ((totalIndex > 0) && (index > 0)) {
                    SetPreviewInfo(index - 1);
                    $('#hdfIndexPage').val(index - 1);
                    $('#current-index').text((index));
                }
                cusHairId = "";
            });

            //============================
            // Datepicker
            //============================
            $('.txtDateTime').datetimepicker({
                dayOfWeekStart: 1,
                lang: 'vi',
                startDate: '2014/10/10',
                format: 'd/m/Y',
                dateonly: true,
                showHour: false,
                showMinute: false,
                timepicker: false,
                scrollMonth: false,
                scrollInput: false,
                onChangeDateTime: function (dp, $input) { }
            });

            /* mở popup ảnh chỉ tiết */
            $("#myModal").on('shown', function () {
                initCircles();
                var $activeCanvas = $(".carousel-inner .item.active").find("canvas");
                var canvasId = $activeCanvas.attr("id");
                canvas = document.getElementById(canvasId);
                if (canvas != undefined) {
                    ctx = canvas.getContext("2d");
                }
                for (var j = 0; j < Circles.length; j++) {
                    if (Circles[j].idCustomer == $('#pre-id').text() && Circles[j].key == canvasId) {
                        for (var i = Circles[j].circles.length - 1; i >= 0; i--) {
                            Circles[j].circles[i].draw();
                        }
                    }
                }
            });

            /* quay về slide trước khi show popup */
            $("#prev-Slide").click(function () {
                var $activeCanvas = $(".carousel-inner .item.active").prev().find("canvas");

                var canvasId = $activeCanvas.attr("id");
                canvas = document.getElementById(canvasId);
                if (canvas != undefined) {
                    ctx = canvas.getContext("2d");
                }
                for (var j = 0; j < Circles.length; j++) {
                    if (Circles[j].idCustomer == $('#pre-id').text() && Circles[j].key == canvasId) {
                        for (var i = Circles[j].circles.length - 1; i >= 0; i--) {
                            Circles[j].circles[i].draw();
                        }
                    }
                }
            });

            /* chuyển tiếp silde sau khi show popup */
            $("#next-Slide").click(function () {
                //var $activeCanvas = $(".carousel-inner .item.active").find("canvas").next();
                var $activeCanvas = $(".carousel-inner .item.active").next().find("canvas");

                var canvasId = $activeCanvas.attr("id");
                canvas = document.getElementById(canvasId);
                if (canvas != undefined) {
                    ctx = canvas.getContext("2d");
                }
                for (var j = 0; j < Circles.length; j++) {
                    if (Circles[j].idCustomer == $('#pre-id').text() && Circles[j].key == canvasId) {
                        for (var i = Circles[j].circles.length - 1; i >= 0; i--) {
                            Circles[j].circles[i].draw();
                        }
                    }
                }
            })
        });

        function testObject(jsonStrig) {
            var obj = JSON.parse(jsonStrig);
            var arr = [];
            for (var i = 0; i < obj.length; i++) {
                arr.push(obj[i].Name);
            }
            var result = arr.join();
            $("#pre-dv").text(result);
        }

        /* Khai báo biến */
        var _ListPreviewImg = [];
        var objPreviewImg = {};
        var billID;
        var cusHairId;
        var hairStyleId;
        //var faceTypeId;

        /* Check quyền element */
        function checkElement() {
            for (var j = 0; j < listElement.length; j++) {
                var nameElement = listElement[j].ElementName;
                var enabled = listElement[j].Enable;
                var type = listElement[j].Type;
                if (type == "hidden" && enabled == true) {
                    $("." + nameElement).addClass('hidden');
                }
                else if (type == "disable" && enabled == true && nameElement.substr(0, 3) == "btn") {
                    $("." + nameElement).addClass('disabled');
                }
                else {
                    $("." + nameElement).prop('disabled', enabled);
                }
            }
        }

        /* Get bill theo S4M */
        function GetBillImage_S4M() {
            var _ClassId = $("#ddlClass").val();
            if (_ClassId == "0") {
                alert("Bạn chưa chọn lớp");
                return;
            }
            var _StylistId = $("#ddlStylist").val();
            var _ToDate = $("#txtToDate").val();
            if (_ToDate == "") {
                alert("Bạn chưa chọn ngày");
                return;
            }

            startLoading();
            $.ajax({
                type: "POST",
                url: "/GUI/BackEnd/Stylist4Men/Preview_Salon_Image_onlyView.aspx/GetBillImage_S4M",
                data: '{_Date: "' + _ToDate + '", _ClassId: ' + _ClassId + ', _StylistId: ' + _StylistId + ', _imageStatus:"" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    _ListPreviewImg = [];

                    $.each(response.d, function (key, value) {
                        objPreviewImg = {};
                        objPreviewImg.Id = value.Id;
                        $("#HDF_BillID").val(objPreviewImg.Id);
                        objPreviewImg.BillDate = value.BillDate;
                        objPreviewImg.SalonName = value.SalonName;
                        objPreviewImg.CustomerPhone = value.CustomerPhone;
                        objPreviewImg.CustomerName = value.CustomerName;
                        objPreviewImg.Img1 = value.Img1;
                        objPreviewImg.Img2 = value.Img2;
                        objPreviewImg.Img3 = value.Img3;
                        objPreviewImg.Img4 = value.Img4;
                        objPreviewImg.ImageStatusId = value.ImageStatusId;
                        objPreviewImg.ErrorNote = value.ErrorNote;
                        objPreviewImg.CustomerId = value.CustomerId;
                        objPreviewImg.ServiceIds = value.ServiceIds;
                        _ListPreviewImg.push(objPreviewImg);

                        // Set giá trị mặc định billID;
                        if (key == 0) {
                            setBillID(value.Id);
                            bindBillStatus(value.ImageStatusId);
                        }
                    });

                    if (_ListPreviewImg.length > 0) {
                        SetPreviewInfo(0);
                        $('#hdfIndexPage').val("0");
                        $('#total-index').text(_ListPreviewImg.length);
                        $('#current-index').text('1');

                    }

                    finishLoading();
                },
                failure: function (response) { console.log(response.d); }
            });
        }
        function getStylistBySalon(stylistId) {
            $('#ddlClass option').removeAttr('selected').filter('[value=0]').attr('selected', 'selected');
            var salonId = $("#ddlSalon").val();
            var select = $('#ddlStylist');
            if (salonId != "0") {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Stylist4Men/Preview_Salon_Image_onlyView.aspx/ReturnAllStylistBySalon",
                    data: '{_SalonId: ' + salonId + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {

                        select.html('');
                        select.append('<option selected="selected" value="0">Chọn Stylist/Học viên</option>');
                        $.each(response.d, function (key, value) {
                            select.append('<option value="' + value.Id + '">' + value.Fullname + '</option>');
                        });
                        $('#ddlStylist option').removeAttr('selected').filter('[value=' + stylistId + ']').attr('selected', 'selected');
                        finishLoading();
                    },
                    failure: function (response) { console.log(response.d); }
                });
            } else {
                select.html('');
                select.append('<option selected="selected" value="0">Chọn Chọn Stylist/Học viên</option>');
            }

        }

        /* Get list Salon */
        function getListSalon() {
            $.ajax({
                type: "POST",
                url: "/GUI/FrontEnd/PreviewImage/Preview_Random_Salon_Images.aspx/ReturnAllSalon",
                data: '',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var select = $('#ddlSalon');
                    select.append('<option selected="selected" value="0">Chọn Salon</option>');
                    $.each(response.d, function (key, value) {
                        select.append('<option value="' + value.Id + '">' + value.Name + '</option>');
                    });
                    finishLoading();
                    checkTotalBill();
                },
                failure: function (response) { console.log(response.d); }
            });
        }
        /* Get list lớp đào tạo */
        function getListClass() {
            $.ajax({
                type: "POST",
                url: "/GUI/BackEnd/Stylist4Men/Preview_Salon_Image_onlyView.aspx/ReturnAllClass",
                data: '',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var select = $('#ddlClass');
                    select.append('<option selected="selected" value="0">Chọn lớp đào tạo</option>');
                    $.each(response.d, function (key, value) {
                        select.append('<option value="' + value.Id + '">' + value.Name + '</option>');
                    });
                    finishLoading();
                },
                failure: function (response) { console.log(response.d); }
            });
        }

        /* Get list học viên theo lớp đào tạo */
        function getStudentByClass(stylistId) {
            $('#ddlSalon option').removeAttr('selected').filter('[value=0]').attr('selected', 'selected');
            var classId = $("#ddlClass").val();
            var select = $('#ddlStylist');
            if (classId != "0") {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Stylist4Men/Preview_Salon_Image_onlyView.aspx/ReturnAllStudentByClass",
                    data: '{_ClassId: ' + classId + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        select.html('');
                        select.append('<option selected="selected" value="0">Chọn Stylist/Học viên</option>');
                        $.each(response.d, function (key, value) {
                            select.append('<option value="' + value.Id + '">' + value.Fullname + '</option>');
                        });
                        $('#ddlStylist option').removeAttr('selected').filter('[value=' + stylistId + ']').attr('selected', 'selected');
                        finishLoading();
                    },
                    failure: function (response) { console.log(response.d); }
                });
            } else {
                select.html('');
                select.append('<option selected="selected" value="0">Chọn Stylist/Học viên</option>');
            }
        }
        /* set list lỗi cắt vào btn */
        function GetErrorList() {
            $.ajax({
                type: "POST",
                url: "/GUI/FrontEnd/PreviewImage/Preview_Random_Salon_Images.aspx/GetErrorList",
                data: '',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var btnList = "";
                    $.each(response.d, function (i, v) {
                        btnList += '<div class="col-xs-3 item-status-wrap">' +
                            '<a data-id="' + v.Id + '" href="javascript:void(0);" onclick="updateStatus($(this))" class="btn btn-default btn-xs item-status btnErrorList">' + v.ErrorCut + '</a>' +
                            '</div>';
                    });
                    $(".btnList").append(btnList);
                    var heightArray = [];
                    for (var i = 0; i < $(".btnList").find(".btnErrorList").length; i++) {
                        var height = $(".btnList").find(".btnErrorList").eq(i).height();
                        heightArray.push(height);
                    }
                    var maxHeight = Math.max.apply(null, heightArray);
                    $(".btnList").find(".btn").css("height", maxHeight + 16);
                    finishLoading();
                },
                failure: function (response) { console.log(response.d); }
            });
        }

        /* set list kiểu tóc vào btn */
        function GetListHairMode() {
            $.ajax({
                type: "POST",
                url: "/GUI/FrontEnd/PreviewImage/Preview_Random_Salon_Images.aspx/ReturnAllHairMode",
                data: '',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var btnList = "";
                    //$.each(response.d, function (i, v) {
                    //    btnList += '<div class="col-xs-3 item-status-wrap">' +
                    //            '<a data-id="' + v.Id + '" href="javascript:void(0);" onclick="setActiveOnclickHair($(this))" class="btn btn-default btn-xs btn-ok item-status btnHairMode">' + v.Title + '</a>' +
                    //            '</div>';
                    //});
                    $.each(response.d, function (i, v) {
                        btnList += '<option value="' + v.Id + '" >' + v.Title + '</option>';
                    });
                    $(".btnHairList").append(btnList);
                    //var heightArray = [];
                    //for (var i = 0; i < $(".btnHairList").find(".btnHairMode").length; i++) {
                    //    var height = $(".btnHairList").find(".btnHairMode").eq(i).height();
                    //    heightArray.push(height);
                    //}
                    //var maxHeight = Math.max.apply(null, heightArray);
                    //$(".btnHairList").find(".btn").css("height", maxHeight + 16);
                    finishLoading();
                },
                failure: function (response) { console.log(response.d); }
            });
        }

        /* Set thông tin của bill ảnh : bill, khách hàng, ảnh, lỗi cắt, kiểu cắt, .... */
        function SetPreviewInfo(index) {
            testObject(_ListPreviewImg[index].ServiceIds);
            if (_ListPreviewImg[index].CustomerName == null) {
                $('#pre-salon').text("Không có");
            }
            else {
                $('#pre-salon').text(_ListPreviewImg[index].CustomerName);
            }
            if (_ListPreviewImg[index].CustomerPhone == null) {
                $('#pre-date').text("Không có");
            }
            else {
                if (_ListPreviewImg[index].CustomerPhone.length == 11) {
                    $('#pre-date').text("*******" + _ListPreviewImg[index].CustomerPhone.substr(8, 11));
                }
                else {
                    $('#pre-date').text("******" + _ListPreviewImg[index].CustomerPhone.substr(7, 10));
                }
            }

            $('#pre-id').text(_ListPreviewImg[index].Id);
            $('#phone-id').text(_ListPreviewImg[index].CustomerPhone);
            //$('#pre-salon').text(_ListPreviewImg[index].CustomerName);
            $("#txtErrorNote").val(_ListPreviewImg[index].ErrorNote);
            $("#txtNoteByStylist").val(_ListPreviewImg[index].NoteByStylist);
            $("#pre-customerid").text(_ListPreviewImg[index].CustomerId);

            $('#pre-img1').attr('src', _ListPreviewImg[index].Img1);
            $('#pre-img2').attr('src', _ListPreviewImg[index].Img2);
            $('#pre-img3').attr('src', _ListPreviewImg[index].Img3);
            $('#pre-img4').attr('src', _ListPreviewImg[index].Img4);

            $('#pre-img1-can').css({ 'background-image': "url(" + _ListPreviewImg[index].Img1 + ")" });
            $('#pre-img2-can').css({ 'background-image': "url(" + _ListPreviewImg[index].Img2 + ")" });
            $('#pre-img3-can').css({ 'background-image': "url(" + _ListPreviewImg[index].Img3 + ")" });
            $('#pre-img4-can').css({ 'background-image': "url(" + _ListPreviewImg[index].Img4 + ")" });
            $('#pre-img1-can').attr('data-src', _ListPreviewImg[index].Img1);
            $('#pre-img2-can').attr('data-src', _ListPreviewImg[index].Img2);
            $('#pre-img3-can').attr('data-src', _ListPreviewImg[index].Img3);
            $('#pre-img4-can').attr('data-src', _ListPreviewImg[index].Img4);

            // set giá trị cho billID
            setBillID(_ListPreviewImg[index].Id);
            // reset trạng thái active của item trạng thái
            resetItemStatus();
            // bind lại trạng thái
            bindBillStatus(_ListPreviewImg[index].ImageStatusId);
            getHairStyleById(billID);
            getErrorNote();

            //initCircles();
        }

        /* cập nhật giá trị cho billID */
        function setBillID(id) {
            billID = id;
        }

        /* set giá trị cho list lỗi cắt (đã đc đánh giá hoặc chưa)  */
        function bindBillStatus(statusID) {
            $(".btnErrorList").removeClass("active");
            if (statusID != null && statusID.length > 0) {
                var stt = statusID.toString().split(',');
                for (var i = 0; i < stt.length; i++) {
                    if (stt[i] == "1") {
                        $(".btnErrorList[data-id='" + stt[i] + "']").addClass("btn-ok");
                    }
                    else {
                        $(".btnErrorList[data-id='" + stt[i] + "']").addClass("btn-act");
                    }
                    $(".btnErrorList[data-id='" + stt[i] + "']").addClass("active");
                }
            }
        }

        /* reset các giá trị của list btn  */
        function resetItemStatus() {
            $(".btnErrorList").removeClass("active");
            $(".btnHairMode").removeClass("active");
            //$(".btnFaceType").removeClass("active");
        }

        /* set trạng thái active cho btn lỗi cắt */
        function setActiveOnclick(This) {
            $(".btnErrorList").removeClass("active");
            This.addClass("active");
        }

        /* update lỗi cắt cho bill tương ứng */
        function updateStatus(This) {
            var statusID = new Array();

            if (This.attr("data-id") == 1) {
                $(".btnErrorList").removeClass("active");
                This.addClass("btn-ok");
                This.addClass("active");
                statusID.push("1");
            }
            else {
                $(".btnErrorList").find(".btn-ok").removeClass("active");
                if (This.hasClass("active")) {
                    This.removeClass("active");
                }
                else {
                    This.addClass("btn-act");
                    This.addClass("active");
                }

                for (var i = 0; i < $(".btnErrorList").length; i++) {
                    if ($(".btnErrorList").eq(i).attr("data-id") != 1 && $(".btnErrorList").eq(i).hasClass("active")) {
                        statusID.push($(".btnErrorList").eq(i).attr("data-id"));
                    }
                }
            }

            if (billID > 0 && statusID.length > 0) {
                startLoading();
                $.ajax({
                    url: "/GUI/FrontEnd/PreviewImage/Preview_Random_Salon_Images.aspx/updateStatus",
                    type: "post",
                    data: '{billID:' + billID + ', statusID : "' + statusID + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var msg = JSON.parse(data.d);
                        if (msg.success) {
                            var index = findIndexByBillID(billID);
                            if (index != null) {
                                _ListPreviewImg[index].ImageStatusId = statusID;
                            }
                            //if ($(This).attr("data-id") == 1) {
                            //    updateErrorNote();
                            //    var totalIndex = parseInt(_ListPreviewImg.length);
                            //    var index = parseInt($('#hdfIndexPage').val());

                            //    if ((totalIndex > 0) && (index < totalIndex - 1)) {
                            //        SetPreviewInfo(index + 1);
                            //        $('#hdfIndexPage').val(index + 1);
                            //        $('#current-index').text((index + 2));
                            //    }
                            //}
                        }
                        finishLoading();
                    },
                    error: function (data) {
                        alert(response.d.responseText);
                    }
                });
            }
        }

        /* lấy index theo mã bill */
        function findIndexByBillID(billID) {
            var index = null;
            if (_ListPreviewImg != null) {
                for (var i = 0; i < _ListPreviewImg.length; i++) {
                    if (billID == _ListPreviewImg[i].Id) {
                        index = i;
                        break;
                    }
                }
            }
            return index;
        }

        /* update text ghi chú */
        function updateErrorNote() {
            if (billID > 0) {
                startLoading();
                $.ajax({
                    url: "/GUI/FrontEnd/PreviewImage/Preview_Random_Salon_Images.aspx/updateErrorNote",
                    type: "post",
                    data: '{billID:' + billID + ', errorNote : "' + $("#txtErrorNote").val() + '", noteByStylist: "' + $("#txtNoteByStylist").val() + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        //$("#txtErrorNote").val('');
                        finishLoading();
                    },
                    error: function (data) {
                        //alert(response.d);
                    }
                });
            }
        }

        /* lấy nội dung ghi chú của bill */
        function getErrorNote() {
            if (billID > 0) {
                $.ajax({
                    url: "/GUI/FrontEnd/PreviewImage/Preview_Random_Salon_Images.aspx/GetErrorNoteById",
                    type: "post",
                    data: '{billID:' + billID + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $("#txtErrorNote").val(data.d.NoteByManager);
                        $("#txtNoteByStylist").val(data.d.NoteByStylist);
                    },
                    error: function (data) {

                    }
                });
            }
        }

        /* Hiển thị thông tin của bill có paras truyền từ module thống kê ảnh */
        function checkTotalBill() {
            var params = {};
            if (location.search) {
                var parts = location.search.substring(1).split('&');
                for (var i = 0; i < parts.length; i++) {
                    var nv = parts[i].split('=');
                    if (!nv[0]) continue;
                    params[nv[0]] = nv[1] || true;
                }
            }
            if ([params.time, params.salonId, params.stylistId, params.imageStatus].indexOf(undefined) == -1) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/PreviewImage/Preview_Random_Salon_Images.aspx/GetRandomBillImage",
                    data: '{_Date: "' + params.time + '", _SalonId: ' + params.salonId + ', _StylistId: ' + params.stylistId + ', _imageStatus:' + params.imageStatus + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        _ListPreviewImg = [];

                        $.each(response.d, function (key, value) {
                            objPreviewImg = {};
                            objPreviewImg.Id = value.Id;
                            $("#HDF_BillID").val(objPreviewImg.Id);
                            objPreviewImg.BillDate = value.BillDate;
                            objPreviewImg.SalonName = value.SalonName;
                            objPreviewImg.CustomerPhone = value.CustomerPhone;
                            objPreviewImg.CustomerName = value.CustomerName;
                            objPreviewImg.Img1 = value.Img1;
                            objPreviewImg.Img2 = value.Img2;
                            objPreviewImg.Img3 = value.Img3;
                            objPreviewImg.Img4 = value.Img4;
                            objPreviewImg.ImageStatusId = value.ImageStatusId;
                            objPreviewImg.ErrorNote = value.ErrorNote;
                            _ListPreviewImg.push(objPreviewImg);

                            // Set giá trị mặc định billID;
                            if (key == 0) {
                                setBillID(value.Id);
                                bindBillStatus(value.ImageStatusId);
                            }
                        });

                        if (_ListPreviewImg.length > 0) {
                            SetPreviewInfo(0);
                            $('#hdfIndexPage').val("0");
                            $('#total-index').text(_ListPreviewImg.length);
                            $('#current-index').text('1');
                            $("#txtToDate").val(params.time);
                            $('#ddlSalon option').removeAttr('selected').filter('[value=' + params.salonId + ']').attr('selected', 'selected');
                            getStylistBySalon(params.stylistId);
                        }
                        finishLoading();
                    },
                    failure: function (response) { console.log(response.d); }
                });
            }
        }

        /*author: Phú Phạm - Update on: 05/08/2017 */
        $("#sltHairStyle").on('change', function () {

            $.ajax({
                type: "POST",
                url: "/GUI/FrontEnd/PreviewImage/Preview_Random_Salon_Images.aspx/getHairStyleById",
                data: '{billId:' + billID + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    if (response.d != null) {

                        cusHairId = response.d.Id;
                        hairStyleId = response.d.HairStyleId;
                        cusId = response.d.CustomerId;
                        //faceTypeId = response.d.FaceTypeId;
                    }
                    updateCustomer_HairMode_Bill();
                },
                failure: function (response) { console.log(response.d); }
            });


        })



        /* update kiểu tóc và kiểu khuôn mặt ứng với từng bill */
        function updateCustomer_HairMode_Bill() {
            if (billID > 0) {
                startLoading();

                if (cusHairId == "" || cusHairId == undefined) {
                    cusHairId = 0;
                }
                if (cusId == "" || cusId == undefined || cusId == 'null') {
                    cusId = 0;
                }
                $.ajax({
                    url: "/GUI/FrontEnd/PreviewImage/Preview_Random_Salon_Images.aspx/updateCustomer_HairMode_Bill",
                    type: "post",
                    data: '{cusHairId:' + cusHairId + ', hairStyleId : ' + $("#sltHairStyle").val() + ', CustomerId: "' + cusId + '", billID: ' + billID + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        finishLoading();
                    },
                    error: function (data) {
                    }
                });
            }
        }

        /* get kiểu tóc và kiểu khuôn mặt của khách theo mã bill */
        function getHairStyleById(billID) {
            var _salonId = $("#ddlSalon").val();
            var _classId = $("#ddlClass").val();
            $.ajax({
                type: "POST",
                url: "/GUI/FrontEnd/PreviewImage/Preview_Random_Salon_Images.aspx/getHairStyleById",
                data: '{billId:' + billID + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    if (response.d != null) {

                        cusHairId = response.d.Id;
                        hairStyleId = response.d.HairStyleId;
                        $("#sltHairStyle option[value='" + hairStyleId + "']").attr("selected", "selected");
                        //faceTypeId = response.d.FaceTypeId;
                        //$.ajax({
                        //    type: "POST",
                        //    url: "/GUI/FrontEnd/PreviewImage/Preview_Random_Salon_Images.aspx/Get_HairMode_Title",
                        //    data: '{hairstyleID:' + hairStyleId + '}',
                        //    contentType: "application/json; charset=utf-8",
                        //    dataType: "json",
                        //    success: function (response) {
                        //        $("#current-style").text("("+response.d.Title+")");
                        //    }

                        //})
                    }
                    //bindBillHairStylist(hairStyleId);
                    // hairStyleId = "";
                    //faceTypeId = "";
                },
                failure: function (response) { console.log(response.d); }
            });
        }

        /********************************/
        // Xử lý canvas điểm cắt lỗi
        /********************************/
        var mousePosition;
        var isMouseDown;
        var isTouchStart;

        var focused = { key: 0, state: false };
        var canvasId;
        var $canvas;
        var canvas;
        var ctx;
        var canvasOrder;
        var classCircle = function () {
            this.key = "";
            this.circles = [];
        }
        var classCustomer = function () {
            this.idCustomer = "";
            this.key = "";
            this.circles = [];
        }
        var Circles = [];
        var currentCircle = new classCircle();
        var circles = [];

        function getCircleByKey(key, idCustomer) {

            for (var i = 0; i < Circles.length; i++) {
                if (Circles[i].key === key && Circles[i].idCustomer == idCustomer) {
                    return Circles[i];
                }
            }
        }

        function setCircleByKey(key, array) {
            for (var i = 0; i < Circles.length; i++) {
                if (Circles[i].key === key) {
                    Circles[i].circles = array;
                }
            }
        }

        /* main draw method */
        function draw() {
            //clear canvas
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            for (var i = circles.length - 1; i >= 0; i--) {
                circles[i].draw();
            }

        }

        //circle Object
        function Circle(x, y, r, stroke) {
            this.startingAngle = 0;
            this.endAngle = 2 * Math.PI;
            this.x = x;
            this.y = y;
            this.r = r;
            this.stroke = stroke;
            this.draw = function () {
                ctx.beginPath();
                ctx.arc(this.x, this.y, this.r, 0, this.endAngle, false);
                ctx.lineWidth = 2;
                ctx.strokeStyle = this.stroke;
                ctx.stroke();
            }
        }

        function move(e) {
            var idCustomer = $("#pre-id").text();
            var $activeCanvas = $(".carousel-inner .item.active").find("canvas");
            var canvasId = $activeCanvas.attr("id");
            canvas = document.getElementById($activeCanvas.attr("id"));
            ctx = canvas.getContext("2d");
            circles = getCircleByKey(canvasId, idCustomer).circles;

            if (!isMouseDown) {
                return;
            }
            var rect = canvas.getBoundingClientRect();
            mousePosition = {
                x: Math.round(e.x - rect.left),
                y: Math.round(e.y - rect.top)
            };
            //if any circle is focused
            if (focused.state) {
                circles[focused.key].x = mousePosition.x;
                circles[focused.key].y = mousePosition.y;
                draw();
                return;
            }
            //no circle currently focused check if circle is hovered
            for (var i = 0; i < circles.length; i++) {
                if (intersects(circles[i])) {
                    circles.move(i, 0);
                    focused.state = true;
                    break;
                }
            }
            draw();
        }

        /* Event touch trên mobile */
        function touchMove(e) {

            var idCustomer = $("#pre-id").text();
            var $activeCanvas = $(".carousel-inner .item.active").find("canvas");
            var canvasId = $activeCanvas.attr("id");
            canvas = document.getElementById(canvasId);
            ctx = canvas.getContext("2d");
            circles = getCircleByKey(canvasId, idCustomer).circles;

            if (!e) {
                var e = event;
                e.stopPropagation();
                e.preventDefault();

            }
            var modalDialog = document.getElementById("modalDialog");
            var modalHeader = document.getElementById("modalHeader");

            mousePosition = {
                x: Math.round(e.targetTouches[0].pageX - modalDialog.offsetLeft),
                y: Math.round(e.targetTouches[0].pageY - window.pageYOffset - (modalDialog.offsetTop + modalHeader.offsetHeight))
            };

            //if any circle is focused
            if (focused.state) {
                circles[focused.key].x = mousePosition.x;
                circles[focused.key].y = mousePosition.y;
                draw();
                return;
            }
            for (var i = 0; i < circles.length; i++) {
                if (intersects(circles[i])) {
                    circles.move(i, 0);
                    focused.state = true;
                    break;
                }
            }
            draw();
        }

        //set mousedown state
        function setDraggable(e) {
            var t = e.type;
            if (t === "mousedown") {
                isMouseDown = true;
            } else if (t === "mouseup") {
                isMouseDown = false;
                focused.state = false;
            }
        }

        function setDraggableMobile(e) {
            var t = e.type;
            if (t === "touchstart") {
                isTouchStart = true;
            } else if (t === "touchend") {
                isTouchStart = false;
                focused.state = false;
            }
        }

        function intersects(circle) {
            var areaX = mousePosition.x - circle.x;
            var areaY = mousePosition.y - circle.y;
            return areaX * areaX + areaY * areaY <= circle.r * circle.r;
        }
        Array.prototype.move = function (old_index, new_index) {
            if (new_index >= this.length) {
                var k = new_index - this.length;
                while ((k--) + 1) {
                    this.push(undefined);
                }
            }
            this.splice(new_index, 0, this.splice(old_index, 1)[0]);
        };

        /* Khởi tạo mảng lưu lỗi */
        function initCircles() {
            $(".carousel-inner canvas").each(function () {
                canvas = document.getElementById($(this).attr("id"));
                canvas.width = $(".modal-content").width();
                canvas.height = canvas.width * 4 / 3;

                var circle = new classCustomer();
                circle.idCustomer = $('#pre-id').text();
                circle.key = $(this).attr("id");
                circle.circles = [];
                Circles.push(circle);
            });
        }

        /*  Truy vấn canvas hiện tại */
        function getActiveCanvas() {
            var idCustomer = $("#pre-id").text();
            $canvas = canvasId = $(".carousel-inner .item.active").find("canvas");
            canvasId = $canvas.attr("id");
            canvasOrder = $canvas.attr("data-order");
            canvas = document.getElementById(canvasId);
            ctx = canvas.getContext("2d");
            circles = getCircleByKey(canvasId, idCustomer).circles;
        }

        /* Thêm vòng tròn lỗi */
        function addCycleToCanvas() {
            getActiveCanvas();
            var idCustomer = $("#pre-id").text();
            ctx = canvas.getContext("2d");
            circles = getCircleByKey(canvasId, idCustomer).circles;

            var c1 = new Circle(70, 70, 40, "red");
            c1.draw();

            circles.push(c1);

            canvas.addEventListener('mousemove', move, false);
            canvas.addEventListener('mousedown', setDraggable, false);
            canvas.addEventListener('mouseup', setDraggable, false);

            canvas.addEventListener("touchmove", touchMove, false);
            canvas.addEventListener("touchstart", setDraggableMobile, false);
            canvas.addEventListener("touchend", setDraggableMobile, false);

        }
        /*  Hủy vòng tròn lỗi */
        function undoCycle() {
            getActiveCanvas();
            circles.shift();
            setCircleByKey(canvasId, circles);
            draw();
        }

        function bindCircleFromArray() {
            getActiveCanvas();
            draw();
        }

        /*  Đóng khung canvas */
        function closeCanvas() {
            canvas = null;
            ctx = null;
            //circles = [];
        }

        /* Mở layer loading */
        function startLoading() {
            $(".page-loading").fadeIn();
        }

        /* Đóng layer loading */
        function finishLoading() {
            $(".page-loading").fadeOut();
        }

        /* Lưu ảnh đã check */
        function saveCheckedImages() {
            getActiveCanvas();
            ctx.globalCompositeOperation = "destination-over";
            var dataUrl = "";
            var image = new Image();
            image.setAttribute('crossOrigin', 'anonymous');
            image.src = $canvas.attr("data-src");
            image.onload = function () {
                ctx.drawImage(image, 0, 0, canvas.width, canvas.height);
                dataUrl = canvas.toDataURL("image/jpg").replace(/^data:image\/(png|jpg);base64,/, "");
                startLoading();
                $.ajax({
                    url: "/GUI/FrontEnd/PreviewImage/Preview_Random_Salon_Images.aspx/LoadImage",
                    type: "post",
                    data: '{bID : ' + billID + ', order : ' + $canvas.attr("data-order") + ', base64:' + JSON.stringify(dataUrl) + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        setCircleByKey(canvasId, circles);
                        finishLoading();
                        displayMessageSuccess("Lưu ảnh thành công!");
                    },
                    error: function (data) {
                    }
                });
            };
        }

        /* Load lại ảnh đã checked ra màn hiển thị */
        function refreshCheckedImage() {
            startLoading();
            var $activeCanvas = $(".carousel-inner .item.active").find("canvas");
            var canvasId = $activeCanvas.attr("id");
            $.ajax({
                url: "/GUI/FrontEnd/PreviewImage/Preview_Random_Salon_Images.aspx/RefreshCheckedImage",
                type: "post",
                data: '{bID : ' + billID + ', order : ' + $activeCanvas.attr("data-order") + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $("#" + canvasId).css({ 'background-image': "url(" + data.d + ")" });
                    $("#" + canvasId).attr('data-src', data.d);
                    finishLoading();
                },
                error: function (data) {
                }
            });
        }

        /* Full screen */
        var videoElement = document.getElementById("myModal");

        function toggleFullScreen() {
            if (!document.mozFullScreen && !document.webkitFullScreen) {
                if (videoElement.mozRequestFullScreen) {
                    videoElement.mozRequestFullScreen();
                } else {
                    videoElement.webkitRequestFullScreen();
                }
            } else {
                if (document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else {
                    document.webkitCancelFullScreen();
                }
            }
        }

        document.addEventListener("keydown", function (e) {
            if (e.keyCode == 13) {
                toggleFullScreen();
            }
        }, false);
    </script>
</body>
</html>
