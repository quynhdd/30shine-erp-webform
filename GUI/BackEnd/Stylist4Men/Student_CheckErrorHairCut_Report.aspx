﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Student_CheckErrorHairCut_Report.aspx.cs" Inherits="_30shine.GUI.BackEnd.Stylist4Men.Student_CheckErrorHairCut_Report" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .table-listing .uv-avatar {
                width: 120px;
            }

            .table-row-detail td {
                border: 1px solid black;
                padding: 5px;
            }
            td a:hover{
                color: deepskyblue !important;
            }
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Stylist4Men - Thống kê số lượng ảnh</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report be-report-timekeeping">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <div class="row">
                    <div class="filter-item">
                        <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="time-wp">
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeFrom" placeholder="Từ ngày" Style="margin-left: 10px!important"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ValidateTime" ControlToValidate="TxtDateTimeFrom" runat="server" CssClass="fb-cover-error" Text="Vui lòng chọn thời gian!"></asp:RequiredFieldValidator>
                        </div>
                        <strong class="st-head" style="margin-left: 10px;">
                            <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                        </strong>
                        <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeTo" placeholder="Đến ngày" Style="margin-left: 10px!important"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                        <br />
                    </div>
                    <div class="filter-item">
                        <select name="ddlClass" id="ddlClass" class="form-control" style="margin: 12px 0; width: 190px;" onchange="getStudentByClass()">
                        </select>
                    </div>
                    <div class="filter-item">
                        <select name="ddlStudent" id="ddlStudent" class="form-control" style="margin: 12px 0; width: 190px;">
                            <option value="0">Chọn học viên</option>
                        </select>
                    </div>
                    <%--<asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="getThongKeAnh(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>--%>
                    <a href="javascript:void(0);" onclick="getThongKeAnh()" class="st-head btn-viewdata">Xem dữ liệu</a>
                    <a href="javascript:void(0);" onclick="location.href=location.href" class="st-head btn-viewdata">Reset Filter</a>

                </div>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>Stylist4Men >> Thống kê số lượng check ảnh</strong>
                </div>


                <div class="row">
                    <table class="table-add table-listing" id="tblReport" style="float: left; width: 100%">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Số lần OK</th>
                                <th>Hình khối chưa cân</th>
                                <th>Đường cắt chưa nét</th>
                                <th>Hình khối chưa liên kết</th>
                                <th>Ngọn tóc quá dày</th>
                                <th>Cạo mai gáy lỗi</th>
                                <th>vuốt sáp lỗi</th>
                                <th>Ảnh thiếu/mờ</th>
                                <th>Bill có ảnh</th>
                                <th>Bill đã đánh giá</th>
                                <th>Bill chưa đánh giá</th>
                                <th>Bill chưa có ảnh</th>
                                <th>Tổng bill</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr id="totalReport">
                                <td><b>TỔNG</b></td>
                                <td class="td-SoLanOK">-</td>
                                <td class="td-HinhKhoiChuaChuan">-</td>
                                <td class="td-DuongCatChuaNet">-</td>
                                <td class="td-HinhKhoiChuaLienKet">-</td>
                                <td class="td-NgonTocQuaDay">-</td>
                                <td class="td-CaoMaiGayLoi">-</td>
                                <td class="td-VuotSapLoi">-</td>
                                <td class="td-AnhThieuMo">-</td>
                                <td class="td-BillCoAnh">-</td>
                                <td class="td-BillDaCheck">-</td>
                                <td class="td-BillChuaCheck">-</td>
                                <td class="td-BillChuaCoAnh">-</td>
                                <td class="td-TongBill">-</td>
                            </tr>

                        </tbody>
                    </table>


                </div>
            </div>
            <%-- END Listing --%>
        </div>

        <script src="../../../Assets/libs/canvasjs-1.9.8/canvasjs.min.js"></script>
        <script src="../../../Assets/libs/canvasjs-1.9.8/jquery.canvasjs.min.js"></script>
        <link href="/Assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <script src="/Assets/js/jquery.mCustomScrollbar.js"></script>

        <script type="text/ecmascript">
            var params = {};
            if (location.search) {
                var parts = location.search.substring(1).split('&');
                for (var i = 0; i < parts.length; i++) {
                    var nv = parts[i].split('=');
                    if (!nv[0]) continue;
                    params[nv[0]] = nv[1] || true;
                }
                if ([params.classId, params.studentId].indexOf(undefined) == -1) {
                    getThongKeAnh();
                }
               
            }
            jQuery(document).ready(function () {
                if ([params.classId, params.studentId].indexOf(undefined) != -1) {
                    getListClass();
                }
                
                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });
            });

            function getStudentByClass(studentId) {
                var classId = $("#ddlClass").val();
                var select = $('#ddlStudent');
                if ([params.classId, params.studentId].indexOf(undefined) == -1) {
                    classId = params.classId;
                }
                if (classId != "0") {
                    startLoading();
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/Stylist4Men/Student_CheckErrorHairCut_Report.aspx/ReturnAllStudentByClass",
                        data: '{_ClassId: ' + classId + '}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            select.html('');
                            select.append('<option selected="selected" value="0">Chọn học viên</option>');
                            $.each(response.d, function (key, value) {
                                select.append('<option value="' + value.Id + '">' + value.Fullname + '</option>');
                            });
                            $('#ddlStudent option').removeAttr('selected').filter('[value=' + studentId + ']').attr('selected', 'selected');
                            finishLoading();
                        },
                        failure: function (response) { console.log(response.d); }
                    });
                } else {
                    select.html('');
                    select.append('<option selected="selected" value="0">Chọn học viênt</option>');
                }
            }

            /* Get list lớp đào tạo */
            function getListClass(classId) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Stylist4Men/Student_CheckErrorHairCut_Report.aspx/ReturnAllClass",
                    data: '',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var select = $('#ddlClass');
                        select.append('<option selected="selected" value="0">Chọn lớp đào tạo</option>');
                        $.each(response.d, function (key, value) {
                            select.append('<option value="' + value.Id + '">' + value.Name + '</option>');
                        });
                        $('#ddlClass option').removeAttr('selected').filter('[value=' + classId + ']').attr('selected', 'selected');
                        finishLoading();
                    },
                    failure: function (response) { console.log(response.d); }
                });
            }

            function redirectPage(This, imageStatus) {
                var time = $('#TxtDateTimeFrom').val();
                var classId = "";
                var studentId = 0;
                if ($("#ddlClass").val() == "0") {
                    classId = $(This).attr("data-id");
                    studentId = 0;
                }
                else {
                    classId = $("#ddlClass").val();
                    studentId = $(This).attr("data-id");
                }
                if (time != undefined && time != null) {
                    window.location = '/admin/s4m/check-cat-loi?time=' + time + '&classId=' + classId + '&studentId=' + studentId + '&imageStatus=' + imageStatus;
                }
            }

            function getThongKeAnh() {
                addLoading();
                if ($('#ddlClass').val() != 0) {
                    var classId = $('#ddlClass').val();
                }
                else {
                    var classId = null;
                }

                if ($('#ddlStudent').val() != 0) {
                    var studentId = $('#ddlStudent').val();
                }
                else {
                    var studentId = null;
                }
                               
                var timeFrom = ConvertDateToTypeMMddyyyy($('#TxtDateTimeFrom').val());

                if ($('#TxtDateTimeTo').val() != '') {
                    var timeTo = ConvertDateToTypeMMddyyyy($('#TxtDateTimeTo').val());
                }
                else {
                    var timeTo = ConvertDateToTypeMMddyyyy($('#TxtDateTimeFrom').val());
                }

                if ([params.classId, params.studentId].indexOf(undefined) == -1) {
                    classId = params.classId;
                    studentId = params.studentId;
                    getListClass(params.classId);
                    getStudentByClass(params.studentId);
                }

                var dataJSON = {
                    timeFrom: timeFrom, timeTo: timeTo, classId: classId, studentId: studentId
                }

                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Stylist4Men/Student_CheckErrorHairCut_Report.aspx/ImageReport",
                    data: JSON.stringify(dataJSON),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        $("#tblReport tbody").find(".tr-next").remove();
                        var row = "";
                        var totalOK = 0, totalHinhKhoiChuaChuan = 0, totalDuongCatChuaNet = 0, totalHinhKhoiChuaLK = 0,
                            totalNgonTocQuaDay = 0, totalCaoMaiGayLoi = 0, totalVuotSapLoi = 0, totalAnhThieuMo = 0,
                            totalCoAnh = 0, totalChecked = 0, totalNotCheck = 0, totalNotImg = 0, totalBill = 0;

                        $.each(response.d, function (key, value) {
                            row += '<tr class="tr-next"><td>' + value.Name + '</td>' +
                                    '<td><a href="javascript:void(0);" onclick="redirectPage($(this),1);" data-id="' + value.Id + '">' + value.SoLanOK + '</a></td>' +
                                    '<td><a href="javascript:void(0);" onclick="redirectPage($(this),2);" data-id="' + value.Id + '">' + value.HinhKhoiChuaChuan + '</a></td>' +
                                    '<td><a href="javascript:void(0);" onclick="redirectPage($(this),3);" data-id="' + value.Id + '">' + value.DuongCatChuaNet + '</a></td>' +
                                    '<td><a href="javascript:void(0);" onclick="redirectPage($(this),4);" data-id="' + value.Id + '">' + value.HinhKhoiChuaLienKet + '</a></td>' +
                                    '<td><a href="javascript:void(0);" onclick="redirectPage($(this),5);" data-id="' + value.Id + '">' + value.NgonTocQuaDay + '</a></td>' +
                                    '<td><a href="javascript:void(0);" onclick="redirectPage($(this),6);" data-id="' + value.Id + '">' + value.CaoMaiGayLoi + '</a></td>' +
                                    '<td><a href="javascript:void(0);" onclick="redirectPage($(this),7);" data-id="' + value.Id + '">' + value.VuotSapLoi + '</a></td>' +
                                    '<td><a href="javascript:void(0);" onclick="redirectPage($(this),8);" data-id="' + value.Id + '">' + value.AnhThieuMo + '</a></td>' +
                                    '<td><a href="javascript:void(0);" onclick="redirectPage($(this),9);" data-id="' + value.Id + '">' + value.TongBillCoAnh + '</a></td>' +
                                    '<td><a href="javascript:void(0);" onclick="redirectPage($(this),10);" data-id="' + value.Id + '">' + value.TongBillDaDanhGia + '</a></td>' +
                                    '<td><a href="javascript:void(0);" onclick="redirectPage($(this),11);" data-id="' + value.Id + '">' + value.TongBillChuaDanhGia + '</a></td>' +
                                    '<td>' + value.TongBillChuaCoAnh + '</td>' +
                                    '<td>' + value.TongBill + '</td>' +
                                    '</tr>';
                            totalOK += value.SoLanOK;
                            totalHinhKhoiChuaChuan += value.HinhKhoiChuaChuan;
                            totalDuongCatChuaNet += value.DuongCatChuaNet;
                            totalHinhKhoiChuaLK += value.HinhKhoiChuaLienKet;
                            totalNgonTocQuaDay += value.NgonTocQuaDay;
                            totalCaoMaiGayLoi += value.CaoMaiGayLoi;
                            totalVuotSapLoi += value.VuotSapLoi;
                            totalAnhThieuMo += value.AnhThieuMo;
                            totalCoAnh += value.TongBillCoAnh;
                            totalChecked += value.TongBillDaDanhGia;
                            totalNotCheck += value.TongBillChuaDanhGia;
                            totalNotImg += value.TongBillChuaCoAnh;
                            totalBill += value.TongBill;
                        });

                        $(row).insertAfter("#totalReport");
                        $('.td-SoLanOK').text(totalOK);
                        $('.td-HinhKhoiChuaChuan').text(totalHinhKhoiChuaChuan);
                        $('.td-DuongCatChuaNet').text(totalDuongCatChuaNet);
                        $('.td-HinhKhoiChuaLienKet').text(totalHinhKhoiChuaLK);
                        $('.td-NgonTocQuaDay').text(totalNgonTocQuaDay);
                        $('.td-CaoMaiGayLoi').text(totalCaoMaiGayLoi);
                        $('.td-VuotSapLoi').text(totalVuotSapLoi);
                        $('.td-AnhThieuMo').text(totalAnhThieuMo);
                        $('.td-BillCoAnh').text(totalCoAnh);
                        $('.td-BillDaCheck').text(totalChecked);
                        $('.td-BillChuaCheck').text(totalNotCheck);
                        $('.td-BillChuaCoAnh').text(totalNotImg);
                        $('.td-TongBill').text(totalBill);

                        if (response.d.length <= 1) {
                            $("#totalReport").addClass("hidden");
                        }
                        else {
                            $("#totalReport").removeClass("hidden");
                        }

                        removeLoading();
                    },
                    failure: function (response) { console.log(response.d); }
                });
            }

        </script>
    </asp:Panel>
</asp:Content>






