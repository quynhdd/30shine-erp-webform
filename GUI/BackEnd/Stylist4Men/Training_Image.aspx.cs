﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Stylist4Men
{
    public partial class Training_Image : System.Web.UI.Page
    {
        private string PageID = "S4M_ANHTN";
        private bool Perm_Access = false;
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;

        protected Paging PAGING = new Paging();

        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

        private CultureInfo culture = new CultureInfo("vi-VN");
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermission_V2 permissionModel = new PermissionModel_V2();
                //var staffId = Convert.ToInt32(Session["User_Id"].ToString());
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, staffId);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, staffId);
                ////Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, staffId);
                //////Perm_Add = permissionModel.GetActionByActionNameAndPageId("Perm_Add", PageID, staffId);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, staffId);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                Library.Function.bind_S4M_Class(new List<DropDownList>() { ddlClass });
                Bind_Paging();
                //Bind_Rpt();
                //RemoveLoading();
            }
        }

        private string genSql()
        {
            string sql = "";
            string where_class = "";
            string where_student = "";
            string where_time = "";
            int classId = 0;
            int studentId = 0;
            DateTime timeFrom;
            DateTime timeTo;

            try
            {
                classId = Convert.ToInt32(ddlClass.SelectedValue);
                studentId = Convert.ToInt32(ddlStudent.SelectedValue);
                if (TxtDateTimeFrom.Text != "")
                {
                    timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                    if (TxtDateTimeTo.Text != "")
                    {
                        timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                    }
                    else
                    {
                        timeTo = timeFrom.AddDays(1);
                    }
                    where_time = " and bill.CreatedTime between '" + String.Format("{0:yyyy/MM/dd}", timeFrom) + "' and '" + String.Format("{0:yyyy/MM/dd}", timeTo) + "'";
                }
                if (classId > 0)
                {
                    where_class = " and bill.ClassId = " + classId;
                }
                if (studentId > 0)
                {
                    where_student = " and bill.StudentId = " + studentId;
                }
                sql = @"select *, class.Name as ClassName, student.Fullname as StudentName,
	                        customer.CustomerName, customer.CustomerPhone
                        from Stylist4Men_BillCutFree as bill
                        left join Stylist4Men_Class as class
                        on bill.ClassId = class.Id
                        left join Stylist4Men_Student as student
                        on bill.StudentId = student.Id
                        left join Stylist4Men_Customer as customer
                        on bill.CustomerId = customer.Id
                        where bill.IsDelete != 1 " + where_time + where_class + where_student;                
            }
            catch { }

            return sql;
        }

        private void Bind_Rpt()
        {
            using (var db = new Solution_30shineEntities())
            {
                string sql = genSql();
                if (sql != "")
                {
                    var list = db.Database.SqlQuery<Library.Class.cls_S4M_BillTraning>(sql).OrderBy(o => o.Id).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();

                    Rpt.DataSource = list;
                    Rpt.DataBind();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "load images", "bindImages();", true);
                }                
            }
        }

        protected void ddlSelected_Class(object sender, EventArgs e)
        {
            try
            {
                int classId = Convert.ToInt32(ddlClass.SelectedValue);
                Library.Function.bind_S4M_StudentByClass(new List<DropDownList>() { ddlStudent }, classId);
            }
            catch { }            
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
            Bind_Rpt();
            RemoveLoading();
        }

        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._Segment = 200;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                var Count = 0;
                var sql = genSql();
                if (sql != "")
                {
                    Count = db.Database.SqlQuery<Library.Class.cls_S4M_BillTraning>(sql).Count();
                }                
                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        

        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }
    }
}