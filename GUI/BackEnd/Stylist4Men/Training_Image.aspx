﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Training_Image.aspx.cs" Inherits="_30shine.GUI.BackEnd.Stylist4Men.Training_Image" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

<style type="text/css">
    .be-report table.table-listing td.td-image img{ width: 120px; margin-right: 7px; }
</style>

<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">
                <li>Quản lý Stylist4Men &nbsp;&#187; Ảnh khách hàng<%-- &nbsp;&#187;--%></li>
                <%--<li class="li-listing active"><a href="/admin/s4m/anh-training/danh-sach.html">Danh sách</a></li>
                <li class="li-add"><a href="/admin/s4m/anh-training/them-moi.html">Thêm mới</a></li>--%>
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add customer-listing be-report">
    <%-- Listing --%>                
    <div class="wp960 content-wp">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <!-- Filter -->        
        <div class="row">
            <div class="filter-item" style="margin-left: 0;">
                <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Thời gian</strong>
                <div class="datepicker-wp">
                    <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                        ClientIDMode="Static" runat="server"></asp:TextBox>

                    <strong class="st-head" style="margin-left: 10px; margin-right: 10px;">
                        <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                    </strong>
                    <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeTo" placeholder="Đến ngày"
                        ClientIDMode="Static" runat="server"></asp:TextBox>
                </div>
                <br />
                <div class="tag-wp">
                    <p class="tag tag-date" onclick="viewDataByDate($(this), '<%=Day1 %>')">Hôm kia <span class="txt-date"><%=Day1 %></span></p>
                    <p class="tag tag-date" onclick="viewDataByDate($(this), '<%=Day2 %>')">Hôm qua<span class="txt-date"><%=Day2 %></span></p>
                    <p class="tag tag-date tag-date-today" onclick="viewDataByDate($(this), '<%=Day3 %>')">Hôm nay<span class="txt-date"><%=Day3 %></span></p>
                </div>
            </div>
            <asp:UpdatePanel runat="server" ID="UPStaff">
                <ContentTemplate>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlClass" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 180px;" 
                            OnSelectedIndexChanged="ddlSelected_Class" AutoPostBack="true"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlStudent" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 180px;">
                            <asp:ListItem Text="Chọn học viên" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </ContentTemplate>
                <Triggers></Triggers>
            </asp:UpdatePanel>                   
            <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                onclick="excPaging(1)" runat="server">
                Xem dữ liệu
            </asp:Panel>
            <a href="javascript:void(0);" onclick="location.href=location.href;" class="st-head btn-viewdata">Reset Filter</a>
        </div>
        <!-- End Filter -->
        <!-- Row Table Filter -->
        <div class="table-func-panel">
            <div class="table-func-elm">
                <span>Số hàng / Page : </span>
                <div class="table-func-input-wp">
                    <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                    <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                    <ul class="ul-opt-segment">
                        <li data-value="10">10</li>
                        <li data-value="20">20</li>
                        <li data-value="30">30</li>
                        <li data-value="40">40</li>
                        <li data-value="50">50</li>
                        <li data-value="1000000">Tất cả</li>
                    </ul>
                    <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                </div>                
            </div>            
        </div>
        <!-- End Row Table Filter -->
        <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
            <ContentTemplate>
                <div class="table-wp">
                    <table class="table-add table-listing" id="tableListing">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Ngày</th>
                                <th>ID</th>
                                <th>Lớp học</th>
                                <th>Học viên</th>  
                                <th>Khách hàng</th>
                                <th>Số điện thoại</th>
                                <th>Ảnh</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="Rpt" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                        <td><%# String.Format("{0:dd/MM/yyyy}", Eval("CreatedTime")) %></td>
                                        <td><%# Eval("Id") %></td>
                                        <td>
                                            <a href="/admin/s4m/lop-hoc/<%# Eval("ClassId") %>.html"><%# Eval("ClassName") %></a>                                            
                                        </td>
                                        <td>
                                            <a href="/admin/s4m/hoc-vien/<%# Eval("StudentId") %>.html"><%# Eval("StudentName") %></a>
                                        </td>                                                                                
                                        <td><%# String.Format("{0}",Eval("CustomerName")).Replace("Optional(\"","").Replace("\")", "") %></td>
                                        <td><%# String.Format("{0}", Eval("CustomerPhone")).Replace("Optional(\"","").Replace("\")", "") %></td>
                                        <td data-images="<%# Eval("Images") %>" class="td-image"></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>                    
                        </tbody>
                    </table>
                </div>

                <!-- Paging -->
                <div class="site-paging-wp">
                <% if(PAGING.TotalPage > 1){ %>
                    <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                        <% if (PAGING._Paging.Prev != 0)
                           { %>
                        <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                        <% } %>
                        <asp:Repeater ID="RptPaging" runat="server">
                            <ItemTemplate>
                                <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                    <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>
                                >
                                    <%# Eval("PageNum") %>
                                </a>
                            </ItemTemplate>                    
                        </asp:Repeater>
                        <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                           { %>                
                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                        <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                        <% } %>
                    </asp:Panel>
                <% } %>
                </div>
		        <!-- End Paging -->                
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" style="display:none;" />
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
    </div> 
    <%-- END Listing --%>
</div>

<script>
    jQuery(document).ready(function () {
        $("#glbS4M").addClass("active");
        $("#glbS4MStudent").addClass("active");

        //============================
        // Datepicker
        //============================
        $('.txtDateTime').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'vi',
            startDate: '2014/10/10',
            format: 'd/m/Y',
            dateonly: true,
            showHour: false,
            showMinute: false,
            timepicker: false,
            onChangeDateTime: function (dp, $input) {
                //$input.val($input.val().split(' ')[0]);
            },
            scrollMonth: false,
            scrollTime: false,
            scrollInput: false
        });

        // View data today
        $(".tag-date-today").click();

        // bind images
        bindImages();
    });

    function viewDataByDate(This, time) {
        $(".tag-date.active").removeClass("active");
        This.addClass("active");
        $("#TxtDateTimeTo").val("");
        $("#TxtDateTimeFrom").val(time);
        $("#ViewData").click();
    }

    //============================
    // Event delete
    //============================
    function del(This, code, name) {
        var code = code || null,
            name = name || null,
            Row = This;
        if (!code) return false;

        // show EBPopup
        $(".confirm-yn").openEBPopup();
        $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

        $("#EBPopup .yn-yes").bind("click", function () {
            $.ajax({
                type: "POST",
                url: "/GUI/SystemService/Ajax/Del.aspx/Delele_S4M_Student",
                data: '{Code : "' + code + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var mission = JSON.parse(response.d);
                    if (mission.success) {
                        delSuccess();
                        Row.remove();
                    } else {
                        delFailed();
                    }
                },
                failure: function (response) { alert(response.d); }
            });
        });
        $("#EBPopup .yn-no").bind("click", function () {
            autoCloseEBPopup(0);
        });
    }

    function bindImages() {
        $("table#tableListing tr td.td-image").each(function ()
        {
            var imageStr = $(this).attr("data-images");
            if (imageStr != "")
            {
                var imageList = imageStr.split(",");
                if (imageList.length > 0)
                {
                    var dom = "";
                    for (var i = 0; i < imageList.length; i++)
                    {
                        dom += "<img alt='image" + (i + 1) + "' title='image" + (i + 1) + "' src='" + imageList[i] + "' />";
                    }
                    $(this).append(dom);
                }
            }
        });
    }
</script>

</asp:Panel>
</asp:Content>

