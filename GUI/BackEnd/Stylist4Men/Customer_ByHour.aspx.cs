﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Script.Serialization;
using ExportToExcel;
using Excel;
using LinqKit;
using System.Data.Objects;
using System.Data.Common;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Report
{
    public partial class Customer_ByHour : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        protected SingleReportData _SingleReportData = new SingleReportData();
        protected SingleReportData _SingleReportData_CL = new SingleReportData();
        protected SingleReportData _SingleReportData_KT = new SingleReportData();
        protected SingleReportData _SingleReportData_TDN = new SingleReportData();
        protected List<List<ProductBasic>> ProductList;
        private Expression<Func<BillService, bool>> Where = PredicateBuilder.True<BillService>();

        private string PageID = "BC_BILL_TIME";
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;

        List<_30shine.MODEL.ENTITY.EDMX.Staff> _LstNhanVien;
        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

        private DateTime MinTime = new DateTime();
        private DateTime MaxTime = new DateTime();
        private DateTime TimeFrom = new DateTime();
        private Expression<Func<BillService, bool>> WhereBill = PredicateBuilder.True<BillService>();

        private string sql = "";
        private CultureInfo culture = new CultureInfo("vi-VN");
        private int SalonId;
        protected _Struct2 TotalReport = new _Struct2();
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                ////Perm_ViewAllDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDate", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", PageID, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", PageID, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                GenWhere();
                Bind_Paging();
                Bind_TotalBills();
                Bind_Bills();
            }
            else
            {
                GenWhere();
                RemoveLoading();
            }
        }

        /// <summary>
        /// Bind data for service and product
        /// </summary>
        private void GenWhere()
        {
            if (TxtDateTimeFrom.Text != "")
            {
                MinTime = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                if (TxtDateTimeTo.Text != "")
                {
                    MaxTime = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                }
                else
                {
                    MaxTime = MinTime.AddDays(1);
                }
            }
            else
            {
                MinTime = Convert.ToDateTime("01/08/2015", culture);
                MaxTime = DateTime.Now;
            }

            SalonId = Convert.ToInt32(Salon.SelectedValue);
        }

        public void Bind_Bills()
        {
            TimeFrom = MinTime.AddDays(PAGING._Offset);
            string where = "";
            string timeFrom = "";
            string timeTo = "";
            if (TimeFrom < MaxTime)
            {
                timeFrom = String.Format("{0:MM/dd/yyyy}", TimeFrom);    
            }
            if (TimeFrom.AddDays(PAGING._Segment) > MaxTime)
            {
                timeTo = String.Format("{0:MM/dd/yyyy}", MaxTime);
            }
            else
            {
                timeTo = String.Format("{0:MM/dd/yyyy}", TimeFrom.AddDays(PAGING._Segment));
            }
            if (timeFrom != "" && timeTo != "")
            {
                where += "and (a.CreatedDate between '" + timeFrom + "' and '" + timeTo + "')";
            }
            if (SalonId > 0)
            {
                where += "and (a.SalonId = "+SalonId.ToString()+")";
            }
            
            sql = @"select COUNT(d.Id) as bills,DATEPART(YEAR, d.CreatedDate) as [year],DATEPART(MONTH, d.CreatedDate) as [month],DATEPART(DAY, d.CreatedDate) as [day], DATEPART(HOUR, d.CreatedDate) as [hour]
                        from
                        (
                        select a.Id, a.CustomerCode, b.Fullname, a.CreatedDate 
                        from BillService as a
                        join Customer as b
                        on a.CustomerCode = b.Customer_Code
                        where 
                        --(a.CustomerCode != '' and a.CustomerCode is not null) 
                        --and 
                        a.IsDelete != 1 and b.IsDelete != 1
                        and a.Pending != 1
                        and (a.ServiceIds is not null and a.ServiceIds != '')" +
                    where +
                    @") as d
                        group by 
                        DATEPART(YEAR, d.CreatedDate), 
                        DATEPART(MONTH, d.CreatedDate),DATEPART(DAY, d.CreatedDate), DATEPART(HOUR, d.CreatedDate)
                        order by
                        DATEPART(YEAR, d.CreatedDate) desc, 
                        DATEPART(MONTH, d.CreatedDate) desc,
                        DATEPART(DAY, d.CreatedDate) desc, 
                        DATEPART(HOUR, d.CreatedDate) desc";
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Database.SqlQuery<_Struct1>(sql).ToList();
                var lst2 = new List<_Struct1>();
                var hourFrame = new _Struct2();
                var hourFrameLST = new List<_Struct2>();
                for (var i = 0; i < PAGING._Segment; i++)
                {
                    if (TimeFrom < MaxTime)
                    {
                        hourFrame = new _Struct2();
                        lst2 = lst.Where(w => w.year == TimeFrom.Year && w.month == TimeFrom.Month && w.day == TimeFrom.Day).ToList();
                        if (lst2.Count > 0)
                        {
                            foreach (var v in lst2)
                            {
                                switch (v.hour)
                                {
                                    case 22: hourFrame.h_22 = v.bills;
                                        break;
                                    case 21: hourFrame.h_21 = v.bills;
                                        break;
                                    case 20: hourFrame.h_20 = v.bills;
                                        break;
                                    case 19: hourFrame.h_19 = v.bills;
                                        break;
                                    case 18: hourFrame.h_18 = v.bills;
                                        break;
                                    case 17: hourFrame.h_17 = v.bills;
                                        break;
                                    case 16: hourFrame.h_16 = v.bills;
                                        break;
                                    case 15: hourFrame.h_15 = v.bills;
                                        break;
                                    case 14: hourFrame.h_14 = v.bills;
                                        break;
                                    case 13: hourFrame.h_13 = v.bills;
                                        break;
                                    case 12: hourFrame.h_12 = v.bills;
                                        break;
                                    case 11: hourFrame.h_11 = v.bills;
                                        break;
                                    case 10: hourFrame.h_10 = v.bills;
                                        break;
                                    case 9: hourFrame.h_9 = v.bills;
                                        break;
                                    case 8: hourFrame.h_8 = v.bills;
                                        break;
                                }
                            }
                        }
                        hourFrame.CreadtedDate = TimeFrom.Day.ToString() + "/" + TimeFrom.Month.ToString() + "/" + TimeFrom.Year.ToString();
                        hourFrameLST.Add(hourFrame);
                        TimeFrom = TimeFrom.AddDays(1);
                    }                            
                }

                RptBills.DataSource = hourFrameLST;
                RptBills.DataBind();
            }            
        }

        public void Bind_TotalBills()
        {
            string where = "and (a.CreatedDate between '" + String.Format("{0:MM/dd/yyyy}", MinTime) + "' and '" + String.Format("{0:MM/dd/yyyy}", MaxTime) + "')";
            if (SalonId > 0)
            {
                where += "and (a.SalonId = "+SalonId.ToString()+")";
            }
            
            string sql = @"select COUNT(d.Id) as bills,DATEPART(YEAR, d.CreatedDate) as [year],DATEPART(MONTH, d.CreatedDate) as [month],DATEPART(DAY, d.CreatedDate) as [day], DATEPART(HOUR, d.CreatedDate) as [hour]
                        from
                        (
                        select a.Id, a.CustomerCode, b.Fullname, a.CreatedDate 
                        from BillService as a
                        join Customer as b
                        on a.CustomerCode = b.Customer_Code
                        where 
                        --(a.CustomerCode != '' and a.CustomerCode is not null) 
                        --and 
                        a.IsDelete != 1 and b.IsDelete != 1
                        and a.Pending != 1
                        and (a.ServiceIds is not null and a.ServiceIds != '')" +
                    where +
                    @") as d
                        group by 
                        DATEPART(YEAR, d.CreatedDate), 
                        DATEPART(MONTH, d.CreatedDate),DATEPART(DAY, d.CreatedDate), DATEPART(HOUR, d.CreatedDate)
                        order by
                        DATEPART(YEAR, d.CreatedDate) desc, 
                        DATEPART(MONTH, d.CreatedDate) desc,
                        DATEPART(DAY, d.CreatedDate) desc, 
                        DATEPART(HOUR, d.CreatedDate) desc";
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Database.SqlQuery<_Struct1>(sql).ToList();
                if (lst.Count > 0)
                {
                    foreach (var v in lst)
                    {
                        switch (v.hour)
                        {
                            case 22: TotalReport.h_22 += v.bills;
                                break;
                            case 21: TotalReport.h_21 += v.bills;
                                break;
                            case 20: TotalReport.h_20 += v.bills;
                                break;
                            case 19: TotalReport.h_19 += v.bills;
                                break;
                            case 18: TotalReport.h_18 += v.bills;
                                break;
                            case 17: TotalReport.h_17 += v.bills;
                                break;
                            case 16: TotalReport.h_16 += v.bills;
                                break;
                            case 15: TotalReport.h_15 += v.bills;
                                break;
                            case 14: TotalReport.h_14 += v.bills;
                                break;
                            case 13: TotalReport.h_13 += v.bills;
                                break;
                            case 12: TotalReport.h_12 += v.bills;
                                break;
                            case 11: TotalReport.h_11 += v.bills;
                                break;
                            case 10: TotalReport.h_10 += v.bills;
                                break;
                            case 9: TotalReport.h_9 += v.bills;
                                break;
                            case 8: TotalReport.h_8 += v.bills;
                                break;
                        }
                    }
                }
            }
        }

   

        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
            Bind_TotalBills();
            Bind_Bills();
            ScriptManager.RegisterStartupScript(UPTableListing, this.GetType(), "Format Price", "UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing();", true);
            RemoveLoading();
        }

        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? (HDF_Page.Value != "" ? Convert.ToInt32(HDF_Page.Value) : 1) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                int Count = (int)(MaxTime - MinTime).TotalDays;
                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        
        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }

    }

    public class _Struct1
    {
        public int bills { get; set; }
        public int year { get; set; }
        public int month { get; set; }
        public int day { get; set; }
        public int hour { get; set; }
    }

    public class _Struct2
    {
        public string CreadtedDate { get; set; }
        public int h_8 { get; set; }
        public int h_9 { get; set; }
        public int h_10 { get; set; }
        public int h_11 { get; set; }
        public int h_12 { get; set; }
        public int h_13 { get; set; }
        public int h_14 { get; set; }
        public int h_15 { get; set; }
        public int h_16 { get; set; }
        public int h_17 { get; set; }
        public int h_18 { get; set; }
        public int h_19 { get; set; }
        public int h_20 { get; set; }
        public int h_21 { get; set; }
        public int h_22 { get; set; }
    }
}