﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Student_CheckErrorHairCut_View.aspx.cs" Inherits="_30shine.GUI.BackEnd.Stylist4Men.Student_CheckErrorHairCut_View" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
    <title>Xem lỗi cắt của học viên</title>
    <script src="/Assets/js/jquery.v1.11.1.js"></script>
    <link href="../../../Assets/css/bootstrap/bootstrap.css" rel="stylesheet" />
    <link href="/Assets/css/font.css" rel="stylesheet" />
    <link href="/Assets/css/font-awesome.css" rel="stylesheet" />
    <link href="/Assets/css/csvc.css" rel="stylesheet" />
    <link href="../../../Assets/libs/toastr/toastr.css" rel="stylesheet" />

    <script src="../../../Assets/js/bootstrap/bootstrap.js"></script>
    <script src="/Assets/libs/canvasjs-1.9.8/canvasjs.min.js"></script>
    <script src="/Assets/libs/canvasjs-1.9.8/jquery.canvasjs.min.js"></script>
    <script src="/Assets/js/bootstrap/bootstrap-modal.js"></script>
    <link href="/Assets/css/jquery.datetimepicker.css?v=2" rel="stylesheet" />
    <script src="/Assets/js/jquery.datetimepicker.js"></script>
    <script src="../../../Assets/libs/toastr/toastr.js"></script>
    <script src="../../../Assets/js/common.js"></script>
    <style>
        a.disabled {
            color: gray;
        }

        .btn {
            width: 100%;
            background: #ccc;
            padding: 10px 5px;
            text-align: center;
            margin-top: 5px;
            margin-bottom: 10px;
            font-size: 14px;
            white-space: normal;
        }

        .item-status-wrap {
            padding-left: 5px;
            padding-right: 5px;
        }

        .item-status.btn-ok.active {
            background: #50b347;
        }

        .item-status.btn-act.active {
            background: red;
        }

        .top-info {
            border: none !important;
        }

        .preview-img {
            background: #fff;
        }

        .heading-title {
            background: #000;
            padding: 10px;
            text-align: center;
            color: #fff;
            text-transform: uppercase;
            font-size: 16px;
        }

        .top-filter .form-control {
            margin-bottom: 5px;
        }

        .top-filter {
            padding: 5px;
            border-bottom: 1px solid #f2f2f2;
        }

        .top-info {
            padding: 5px;
            border-bottom: 1px solid #f2f2f2;
        }

            .top-info .bill-info {
                text-align: center;
            }

            .top-info .salon-name {
                text-align: center;
                padding-top: 4px;
            }

            .top-info strong {
                font-weight: bold;
            }

        .top4-img {
            padding: 5px;
            border-bottom: 1px solid #f2f2f2;
        }

            .top4-img .img-item {
                background: #808080;
                padding: 10px;
                text-align: center;
                border-right: 1px solid #fff;
            }

                .top4-img .img-item img {
                    max-width: 100%;
                }

            .top4-img .col1 {
                border-bottom: 1px solid #fff;
                width: 100%;
                float: left;
            }

            .top4-img .col2 {
                width: 100%;
                float: left;
            }

        .img-nav {
            padding: 5px;
            border-bottom: 1px solid #f2f2f2;
            text-align: center;
        }

        .btn-default {
            background-color: #e6e6e6;
        }

        canvas {
        }

        .modal-dialog {
            max-width: 400px;
            margin: 10px auto;
        }

        .modal-body {
            padding: 0;
        }

        .modal-header {
            padding: 5px 15px;
        }

        .modal-footer {
            margin-top: 0;
            padding: 5px 10px;
        }

            .modal-footer i {
                font-size: 46px;
                cursor: pointer;
            }

        .carousel-control {
            top: inherit;
            bottom: 5%;
            padding-top: 10px;
            padding-bottom: 10px;
        }

        @media(max-width: 768px) {
            .modal-dialog {
                width: 100%;
            }
        }

        .page-loading {
            width: 100%;
            height: 100%;
            position: fixed;
            top: 0;
            left: 0;
            z-index: 100000;
            background: rgba(0,0,0, 0.65) url(/Assets/images/load_ring.svg) center no-repeat;
            text-align: center;
            display: none;
        }

            .page-loading p {
                color: #fcd344;
                position: relative;
                top: 50%;
                padding-top: 37px;
                line-height: 22px;
            }
    </style>
</head>
<body>
    <div class="wp">
        <div class="preview-img">
            <div class="heading-title">Stylist4Men - Xem ảnh cắt lỗi</div>
            <!-- Filter-->
            <div class="top-filter">
                <div>
                    <select name="ddlClass" id="ddlClass" class="form-control" onchange="getStudentByClass()">
                    </select>
                </div>
                <div>
                    <select name="ddlStudent" id="ddlStudent" class="form-control">
                        <option value="0">Chọn học viên</option>
                    </select>
                </div>
                <div>
                    <input runat="server" name="txtToDate" type="text" value="" id="txtToDate" class="form-control txtDateTime" placeholder="Ngày ..." />
                    <input type="submit" id="btnTimKiem" class="form-control btn-default btnSearch" value="Tìm" />
                </div>
            </div>
            <!-- Customer info-->
            <div class="top-info">
                <div class="bill-info">Id: &nbsp<strong id="pre-id"></strong> &nbsp - &nbsp  Số điện thoại: &nbsp<strong id="pre-date"></strong></div>
                <div class="salon-name">Tên khách hàng: <strong id="pre-salon"></strong></div>
                <label id="pre-customerid" class="hidden"></label>
            </div>
            
            <!-- Ảnh chụp khách-->
            <div class="top4-img">
                <div class="container">
                    <div class="row">
                        <div class="col1">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 img-item" data-toggle="modal" data-target="#myModal" data-slide-to="0">
                                <img src="https://30shine.com/images/1.jpg" alt="" id="pre-img1" class="image" />
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 img-item" data-toggle="modal" data-target="#myModal" data-slide-to="1">
                                <img src="https://30shine.com/images/2.jpg" alt="" id="pre-img2" class="image" />
                            </div>
                        </div>
                        <div class="col2">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 img-item" data-toggle="modal" data-target="#myModal" data-slide-to="2">
                                <img src="https://30shine.com/images/3.jpg" alt="" id="pre-img3" class="image" />
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 img-item" data-toggle="modal" data-target="#myModal" data-slide-to="3">
                                <img src="https://30shine.com/images/4.jpg" alt="" id="pre-img4" class="image" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Lỗi cắt-->
            
                <div class="top-info text-center btnList" style="padding-top: 0;"></div>
            
           
            <!-- Ghi chú-->
            <div class="top-info">
                <table style="width: 100%">
                    <tr>
                        <td style="width: 25%; text-align: center;">Ghi chú</td>
                        <td style="width: 75%;">
                            <textarea id="txtErrorNote" class="form-control ErrorNote" rows="2" disabled></textarea></td>
                    </tr>
                </table>
            </div>
            <!-- btn-->
            <div class="img-nav">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-4 col-sm-4">
                            <input type="submit" id="btnPreview" class="form-control btn-default" value="Trước" />
                        </div>
                        <div class="col-xs-4 col-sm-4">
                            &nbsp(Đang xem hóa đơn <strong id="current-index">0</strong>/<strong id="total-index">0</strong>)&nbsp
                        </div>
                        <div class="col-xs-4 col-sm-4">
                            <input type="submit" id="btnNext" class="form-control btn-default" value="Sau" />
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" value="0" id="hdfIndexPage" />
            <input type="hidden" value="0" id="HDF_BillID" />
        </div>
    </div>

    <!-- Pop up-->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog" id="modalDialog">
            <!-- Modal content-->
            <div class="modal-content" id="modalContent">
                <div class="modal-header" id="modalHeader">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Check lỗi cắt</h4>
                </div>
                <div class="modal-body">
                    <div id="myCarousel" class="carousel" data-ride="carousel" data-interval="false">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                            <li data-target="#myCarousel" data-slide-to="3"></li>
                        </ol>
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox" id="canvasWrap">
                            <div class="item active" id="div-image1">
                                <%--<canvas id="pre-img1-can" data-order="1" style="background-repeat: no-repeat;" title="Before"></canvas>--%>
                                <img src="" alt="" id="pre-img1-view" data-order="1" />
                            </div>
                            <div class="item" id="div-image2">
                                <img src="" alt="" id="pre-img2-view" data-order="2" />
                            </div>
                            <div class="item" id="div-image3">
                                <img src="" alt="" id="pre-img3-view" data-order="3" />
                            </div>
                            <div class="item" id="div-image4">
                                <img src="" alt="" id="pre-img4-view" data-order="4" />
                            </div>
                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev" id="prev-Slide">
                            <%--<i class="fa fa-arrow-left" aria-hidden="true"></i>--%>
                            <span class="fa fa-chevron-left" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next" id="next-Slide">
                            <span class="fa fa-chevron-right" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                            <%--<i class="fa fa-arrow-right" aria-hidden="true"></i>--%>
                        </a>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-xs-9"></div>
                        <div class="col-xs-3">
                            <i class="fa fa-times-circle" aria-hidden="true" data-dismiss="modal" title="Đóng"></i>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!-- Loading-->
    <div class="page-loading">
        <p>Vui lòng đợi trong giây lát...</p>
    </div>

    <script type="text/javascript">

        $(document).ready(function () {
            startLoading();
            getListClass();
            GetErrorList();

            /* fn tìm kiếm bill ảnh theo điều kiện lọc */
            $('#btnTimKiem').click(function () {
                //initCircles();
                var _ClassId = $("#ddlClass").val();
                if (_ClassId == "0") {
                    alert("Bạn chưa chọn lớp");
                    return;
                }
                var _StudentId = $("#ddlStudent").val();
                var _ToDate = $("#txtToDate").val();
                if (_ToDate == "") {
                    alert("Bạn chưa chọn học viên");
                    return;
                }

                startLoading();
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Stylist4Men/Student_CheckErrorHairCut_View.aspx/GetRandomBillImage",
                    data: '{_Date: "' + _ToDate + '", _ClassId: ' + _ClassId + ', _StudentId: ' + _StudentId + ', _imageStatus:"" }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        _ListPreviewImg = [];
                        $.each(response.d, function (key, value) {
                            objPreviewImg = {};
                            objPreviewImg.Id = value.Id;
                            $("#HDF_BillID").val(objPreviewImg.Id);
                            objPreviewImg.BillDate = value.BillDate;
                            objPreviewImg.ClassName = value.ClassName;
                            objPreviewImg.CustomerPhone = value.CustomerPhone;
                            objPreviewImg.CustomerName = value.CustomerName;
                            objPreviewImg.Img1 = value.Img1;
                            objPreviewImg.Img2 = value.Img2;
                            objPreviewImg.Img3 = value.Img3;
                            objPreviewImg.Img4 = value.Img4;
                            objPreviewImg.ImageStatusId = value.ImageStatusId;
                            objPreviewImg.ErrorNote = value.ErrorNote;
                            objPreviewImg.CustomerId = value.CustomerId;
                            _ListPreviewImg.push(objPreviewImg);

                            // Set giá trị mặc định billID;
                            if (key == 0) {
                                setBillID(value.Id);
                                bindBillStatus(value.ImageStatusId);
                            }
                        });

                        if (_ListPreviewImg.length > 0) {
                            SetPreviewInfo(0);
                            $('#hdfIndexPage').val("0");
                            $('#total-index').text(_ListPreviewImg.length);
                            $('#current-index').text('1');
                        }

                        finishLoading();
                    },
                    failure: function (response) { console.log(response.d); }
                });

            });

            /* fn khi chuyển tiếp sang bill tiếp theo */
            $('#btnNext').click(function () {
                var totalIndex = parseInt(_ListPreviewImg.length);
                var index = parseInt($('#hdfIndexPage').val());

                if ((totalIndex > 0) && (index < totalIndex - 1)) {
                    SetPreviewInfo(index + 1);
                    $('#hdfIndexPage').val(index + 1);
                    $('#current-index').text((index + 2));
                }

                cusHairId = "";
            });

            /* fn khi quay lại bill trước đó */
            $('#btnPreview').click(function () {
                var totalIndex = parseInt(_ListPreviewImg.length);
                var index = parseInt($('#hdfIndexPage').val());

                if ((totalIndex > 0) && (index > 0)) {
                    SetPreviewInfo(index - 1);
                    $('#hdfIndexPage').val(index - 1);
                    $('#current-index').text((index));
                }
                cusHairId = "";
            });

            //============================
            // Datepicker
            //============================
            $('.txtDateTime').datetimepicker({
                dayOfWeekStart: 1,
                lang: 'vi',
                startDate: '2014/10/10',
                format: 'd/m/Y',
                dateonly: true,
                showHour: false,
                showMinute: false,
                timepicker: false,
                scrollMonth: false,
                scrollInput: false,
                onChangeDateTime: function (dp, $input) { }
            });

            /* mở popup ảnh chỉ tiết */
            $("#myModal").on('shown', function () {
                
            });
        });

        /* Khai báo biến */
        var _ListPreviewImg = [];
        var objPreviewImg = {};
        var billID;

        /* Get list học viên theo lớp đào tạo */
        function getStudentByClass(studentId) {
            var classId = $("#ddlClass").val();
            var select = $('#ddlStudent');
            if (classId != "0") {
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Stylist4Men/Student_CheckErrorHairCut_View.aspx/ReturnAllStudentByClass",
                    data: '{_ClassId: ' + classId + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        select.html('');
                        select.append('<option selected="selected" value="0">Chọn học viên</option>');
                        $.each(response.d, function (key, value) {
                            select.append('<option value="' + value.Id + '">' + value.Fullname + '</option>');
                        });
                        $('#ddlStudent option').removeAttr('selected').filter('[value=' + studentId + ']').attr('selected', 'selected');
                        finishLoading();
                    },
                    failure: function (response) { console.log(response.d); }
                });
            } else {
                select.html('');
                select.append('<option selected="selected" value="0">Chọn học viênt</option>');
            }
        }

        /* Get list lớp đào tạo */
        function getListClass() {
            $.ajax({
                type: "POST",
                url: "/GUI/BackEnd/Stylist4Men/Student_CheckErrorHairCut_View.aspx/ReturnAllClass",
                data: '',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var select = $('#ddlClass');
                    select.append('<option selected="selected" value="0">Chọn lớp đào tạo</option>');
                    $.each(response.d, function (key, value) {
                        select.append('<option value="' + value.Id + '">' + value.Name + '</option>');
                    });
                    finishLoading();
                },
                failure: function (response) { console.log(response.d); }
            });
        }

        /* set list lỗi cắt vào btn */
        function GetErrorList() {
            $.ajax({
                type: "POST",
                url: "/GUI/BackEnd/Stylist4Men/Student_CheckErrorHairCut_View.aspx/GetErrorList",
                data: '',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var btnList = "";
                    $.each(response.d, function (i, v) {
                        btnList += '<div class="col-xs-3 item-status-wrap">' +
                                '<a data-id="' + v.Id + '" href="javascript:void(0);" onclick="updateStatus($(this))" class="btn btn-default btn-xs item-status btnErrorList" disabled>' + v.ErrorCut + '</a>' +
                                '</div>';
                    });
                    $(".btnList").append(btnList);
                    var heightArray = [];
                    for (var i = 0; i < $(".btnList").find(".btnErrorList").length; i++) {
                        var height = $(".btnList").find(".btnErrorList").eq(i).height();
                        heightArray.push(height);
                    }
                    var maxHeight = Math.max.apply(null, heightArray);
                    $(".btnList").find(".btn").css("height", maxHeight + 16);
                    finishLoading();
                },
                failure: function (response) { console.log(response.d); }
            });
        }

        /* Set thông tin của bill ảnh : bill, khách hàng, ảnh, lỗi cắt, .... */
        function SetPreviewInfo(index) {
            $('#pre-id').text(_ListPreviewImg[index].Id);
            $('#pre-date').text(_ListPreviewImg[index].CustomerPhone);
            $('#pre-salon').text(_ListPreviewImg[index].CustomerName);
            $("#txtErrorNote").val(_ListPreviewImg[index].ErrorNote);
            $("#pre-customerid").text(_ListPreviewImg[index].CustomerId);

            $('#pre-img1').attr('src', _ListPreviewImg[index].Img1);
            $('#pre-img2').attr('src', _ListPreviewImg[index].Img2);
            $('#pre-img3').attr('src', _ListPreviewImg[index].Img3);
            $('#pre-img4').attr('src', _ListPreviewImg[index].Img4);

            $('#pre-img1-view').attr('src', _ListPreviewImg[index].Img1);
            $('#pre-img2-view').attr('src', _ListPreviewImg[index].Img2);
            $('#pre-img3-view').attr('src', _ListPreviewImg[index].Img3);
            $('#pre-img4-view').attr('src', _ListPreviewImg[index].Img4);

            // set giá trị cho billID
            setBillID(_ListPreviewImg[index].Id);
            // reset trạng thái active của item trạng thái
            resetItemStatus();
            // bind lại trạng thái
            bindBillStatus(_ListPreviewImg[index].ImageStatusId);
            getErrorNote();

            //initCircles();
        }

        /* cập nhật giá trị cho billID */
        function setBillID(id) {
            billID = id;
        }

        /* set giá trị cho list lỗi cắt (đã đc đánh giá hoặc chưa)  */
        function bindBillStatus(statusID) {
            $(".btnErrorList").removeClass("active");
            if (statusID != null && statusID.length > 0) {
                var stt = statusID.toString().split(',');
                for (var i = 0; i < stt.length ; i++) {
                    if (stt[i] == "1") {
                        $(".btnErrorList[data-id='" + stt[i] + "']").addClass("btn-ok");
                    }
                    else {
                        $(".btnErrorList[data-id='" + stt[i] + "']").addClass("btn-act");
                    }
                    $(".btnErrorList[data-id='" + stt[i] + "']").addClass("active");
                }
            }
        }

        /* reset các giá trị của list btn  */
        function resetItemStatus() {
            $(".btnErrorList").removeClass("active");
        }

        /* lấy index theo mã bill */
        function findIndexByBillID(billID) {
            var index = null;
            if (_ListPreviewImg != null) {
                for (var i = 0; i < _ListPreviewImg.length; i++) {
                    if (billID == _ListPreviewImg[i].Id) {
                        index = i;
                        break;
                    }
                }
            }
            return index;
        }

        /* lấy nội dung ghi chú của bill */
        function getErrorNote() {
            if (billID > 0) {
                $.ajax({
                    url: "/GUI/BackEnd/Stylist4Men/Student_CheckErrorHairCut_View.aspx/GetErrorNoteById",
                    type: "post",
                    data: '{billID:' + billID + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $("#txtErrorNote").val(data.d);
                    },
                    error: function (data) {

                    }
                });
            }
        }

        /* Full screen */
        var videoElement = document.getElementById("myModal");

        function toggleFullScreen() {
            if (!document.mozFullScreen && !document.webkitFullScreen) {
                if (videoElement.mozRequestFullScreen) {
                    videoElement.mozRequestFullScreen();
                } else {
                    videoElement.webkitRequestFullScreen();
                }
            } else {
                if (document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else {
                    document.webkitCancelFullScreen();
                }
            }
        }

        document.addEventListener("keydown", function (e) {
            if (e.keyCode == 13) {
                toggleFullScreen();
            }
        }, false);
    </script>
</body>
</html>
