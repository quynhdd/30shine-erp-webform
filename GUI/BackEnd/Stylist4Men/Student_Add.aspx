﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Student_Add.aspx.cs" Inherits="_30shine.GUI.BackEnd.Stylist4Men.Student_Add" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ProductAdd" ContentPlaceHolderID="CtMain" runat="server">
    <script src="/Assets/js/sweetalert.min.js"></script>
    <link href="/Assets/css/sweetalert.css" rel="stylesheet" />
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý Stylist4Men &nbsp;&#187; Học viên &nbsp;&#187;</li>
                        <li class="li-listing"><a href="/admin/s4m/hoc-vien/danh-sach.html">Danh sách</a></li>
                        <% if (_IsUpdate)
                        { %>
                        <li class="li-add"><a href="/admin/s4m/hoc-vien/them-moi.html">Thêm mới</a></li>
                        <li class="li-edit active"><a href="/admin/s4m/hoc-vien/<%= _Code %>.html">Cập nhật</a></li>
                        <% }
                        else
                        { %>
                        <li class="li-add active"><a href="/admin/s4m/hoc-vien/them-moi.html">Thêm mới</a></li>
                        <% } %>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="table-wp">
                    <table class="table-add admin-product-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Thông tin học viên</strong></td>
                                <td></td>
                            </tr>
                            <tr class="tr-margin" style="height: 20px;"></tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Tên học viên</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="Name" runat="server" ClientIDMode="Static"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator 
                                            ID="TitleValidate" 
                                            ControlToValidate="Name" 
                                            runat="server" 
                                            CssClass="fb-cover-error" 
                                            Text="Bạn chưa nhập tên học viên!">
                                        </asp:RequiredFieldValidator>--%>
                                    </span>

                                </td>
                            </tr>

                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Số điện thoại</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="Phone" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    </span>

                                </td>
                            </tr>

                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Tài khoản</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="Email" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    </span>

                                </td>
                            </tr>

                      <%--      <tr class="tr-birthday">
                                <td class="col-xs-2 left"><span>Sinh nhật</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="Day" runat="server" ClientIDMode="Static" placeholder="Ngày"></asp:TextBox>
                                        <asp:TextBox ID="Month" runat="server" ClientIDMode="Static" placeholder="Tháng"></asp:TextBox>
                                        <asp:TextBox ID="Year" runat="server" ClientIDMode="Static" placeholder="Năm"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>--%>

                            <tr class="tr-field-ahalf">
                                <td class="col-xs-2 left"><span>Giới tính</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:DropDownList ID="Gender" runat="server" ClientIDMode="Static">
                                            <asp:ListItem Text="Nam" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Nữ" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Khác" Value="3"></asp:ListItem>
                                        </asp:DropDownList>
                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-2 left"><span>Địa chỉ</span></td>
                                <td class="col-xs-9 right">
                                    <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
                                    <asp:UpdatePanel runat="server" ID="UP01">
                                        <ContentTemplate>
                                            <div class="city">
                                                <asp:DropDownList AutoPostBack="true" ID="City" runat="server" CssClass="select"
                                                    OnSelectedIndexChanged="ddlFTinhThanh_SelectedIndexChanged" ClientIDMode="Static">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="district">
                                                <asp:DropDownList ID="District" runat="server" CssClass="select" ClientIDMode="Static"></asp:DropDownList>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <%--<asp:AsyncPostBackTrigger ControlID="OrderCloseBtn" EventName="Click"/>--%>
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <asp:TextBox ID="Address" runat="server" placeholder="Địa chỉ" ClientIDMode="Static"></asp:TextBox>
                                </td>
                            </tr>

                            <tr class="tr-field-ahalf">
                                <td class="col-xs-2 left"><span>Lớp học</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:DropDownList ID="Class" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-2 left"><span>Salon</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:DropDownList ID="ddlSalon" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-2 left"><span>Bậc kỹ năng</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:DropDownList ID="ddlSkillLevel" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="Phone" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập số điện thoại!"></asp:RequiredFieldValidator>--%>
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-description">
                                <td class="col-xs-3 left"><span>Ghi chú</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox TextMode="MultiLine" Rows="5" ID="Note" runat="server" ClientIDMode="Static" Style="padding: 10px;"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-product-category">
                                <td class="col-xs-3 left"><span>Đã tốt nghiệp</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:CheckBox ID="IsGraduate" Checked="False" runat="server" />
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-product-category">
                                <td class="col-xs-3 left"><span>Được tuyển vào 30Shine</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:CheckBox ID="IsHire" Checked="False" runat="server" />
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-product-category">
                                <td class="col-xs-3 left"><span>Publish</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:CheckBox ID="Publish" Checked="True" runat="server" />
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-send">
                                <td class="col-xs-3 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <asp:Button ID="Send" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="ExcAddOrUpdate" OnClientClick="performCheck();"></asp:Button>
                                        <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>
                                    </span>
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
                        </tbody>
                    </table>
                </div>
            </div>
            <%-- end Add --%>
        </div>

        <script>
            jQuery(document).ready(function () {
                $("#glbS4M").addClass("active");
                $("#glbS4MStudent").addClass("active");
                //============================
                // Show System Message
                //============================ 
                var qs = getQueryStrings();
                showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        //$input.val($input.val().split(' ')[0]);
                    },
                    scrollMonth: false,
                    scrollTime: false,
                    scrollInput: false
                });
            });
            function performCheck() {

                if (Page_ClientValidate()) {
                    addLoading();
                }

            }
            function IsEmail(email) {
                var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (!regex.test(email)) {
                    return false;
                } else {
                    return true;
                }
            }
        </script>

    </asp:Panel>
</asp:Content>







