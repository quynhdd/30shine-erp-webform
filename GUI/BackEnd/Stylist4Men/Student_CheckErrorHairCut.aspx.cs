﻿using _30shine.Helpers;
using _30shine.MODEL.CustomClass;
using _30shine.MODEL.ENTITY.EDMX;
using Project.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Migrations;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace _30shine.GUI.BackEnd.Stylist4Men
{
    public partial class Student_CheckErrorHairCut : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txtToDate.Value = DateTime.Now.ToString("dd/MM/yyyy");
        }

        [WebMethod]
        public static List<Stylist4Men_Class> ReturnAllClass()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Stylist4Men_Class.Where(a => a.IsDelete == false && a.Publish == true).ToList();
                return lst;
            }
        }

        [WebMethod]
        public static List<Stylist4Men_Student> ReturnAllStudentByClass(int _ClassId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Stylist4Men_Student.Where(a => a.IsDelete == false && a.Publish == true && a.ClassId == _ClassId).ToList();
                return lst;
            }
        }

        [WebMethod]
        public static List<S4M_Bill> GetRandomBillImage(string _Date, int _ClassId, int _StudentId, string _imageStatus)
        {
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                DateTime _FilterDate = Convert.ToDateTime(_Date, culture);
                string _FromDate = String.Format("{0:yyyy/MM/dd}", _FilterDate);
                string _ToDate = String.Format("{0:yyyy/MM/dd}", _FilterDate.AddDays(1));

                var sqlCount = @"select count(*) from Stylist4Men_BillCutFree a where a.CreatedTime between '" + _FromDate + "' and '" + _ToDate + "' and a.IsDelete != 1 and a.Images is not null and a.ClassId =" + _ClassId;

                int _TotalBill = db.Database.SqlQuery<int>(sqlCount).First();

                var sql = "";

                if (_StudentId != 0)
                {
                    sql = "select _bill.*, _cus.CustomerName, _cus.CustomerPhone from Stylist4Men_BillCutFree _bill left join Stylist4Men_Customer _cus on _cus.Id = _bill.CustomerId where _bill.CreatedTime between '" + _FromDate + "' and '" + _ToDate + "' and _bill.IsDelete != 1 and _bill.Images is not null and _bill.ClassId = " + _ClassId + " and _bill.StudentId = " + _StudentId;
                }
                else
                {
                    sql = "select _bill.*, _cus.CustomerName, _cus.CustomerPhone from Stylist4Men_BillCutFree _bill left join Stylist4Men_Customer _cus on _cus.Id = _bill.CustomerId where _bill.CreatedTime between '" + _FromDate + "' and '" + _ToDate + "' and _bill.IsDelete != 1 and _bill.Images is not null and _bill.ClassId = " + _ClassId;
                }

                //1-bill không có lỗi cắt || 2-bill có lỗi hình khối || 3-bill lỗi đường cắt  || 4-bill lỗi hình khối chưa liên kết
                //5-bill lỗi ngọn tóc quá dày || 6-bill cạo mai gáy lỗi || 7-bill vuốt sáp lỗi || 8-bill ảnh thiếu/mờ
                //9-bill có ảnh || 10-bill đã đánh giá || 11-bill chưa đánh giá 

                if (_imageStatus == "1" || _imageStatus == "2" || _imageStatus == "3" || _imageStatus == "4" || _imageStatus == "5" || _imageStatus == "6" || _imageStatus == "7" || _imageStatus == "8")
                {
                    sql += " and ImageStatusId like '%" + _imageStatus + "%' ";
                }
                else if (_imageStatus == "10")
                {
                    sql += " and a.ImageStatusId is not null";
                }
                else if (_imageStatus == "11")
                {
                    sql += " and a.ImageStatusId is null";
                }
                var list = db.Database.SqlQuery<custom_BillService>(sql).ToList();

                var objClass = db.Stylist4Men_Class.Find(_ClassId);

                List<S4M_Bill> lstBillView = new List<S4M_Bill>();
                S4M_Bill obj = new S4M_Bill();
                string[] ListImg;

                string _Domain = ConfigurationManager.AppSettings["DomainKey_PreviewImg"];

                foreach (var item in list)
                {
                    ListImg = item.Images.Split(',');
                    obj = new S4M_Bill();
                    obj.Id = item.Id;
                    obj.BillDate = string.Format("{0:dd/MM/yyyy}", item.CreatedTime);
                    obj.CustomerPhone = item.CustomerPhone;
                    obj.CustomerName = item.CustomerName;
                    obj.CustomerId = item.CustomerId;

                    if (item.ImageStatusId != null)
                    {
                        obj.ImageStatusId = item.ImageStatusId;
                    }

                    for (int i = 0; i < ListImg.Length; i++)
                    {
                        if (!(ListImg[i].StartsWith("http")))
                        {
                            ListImg[i] = _Domain + ListImg[i];
                        }
                    }
                    if (ListImg.Count() == 4)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                        obj.Img3 = ListImg[2];
                        obj.Img4 = ListImg[3];
                    }
                    if (ListImg.Count() == 3)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                        obj.Img3 = ListImg[2];
                    }
                    if (ListImg.Count() == 2)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                    }
                    if (ListImg.Count() == 1)
                        obj.Img1 = ListImg[0];

                    obj.ClassName = objClass.Name;
                    obj.ErrorNote = item.ErrorNote;
                    if (item.ImageChecked1 != null)
                    {
                        obj.Img1 = item.ImageChecked1;
                    }
                    if (item.ImageChecked2 != null)
                    {
                        obj.Img2 = item.ImageChecked2;
                    }
                    if (item.ImageChecked3 != null)
                    {
                        obj.Img3 = item.ImageChecked3;
                    }
                    if (item.ImageChecked4 != null)
                    {
                        obj.Img4 = item.ImageChecked4;
                    }

                    lstBillView.Add(obj);
                }

                return lstBillView;
            }
        }
        
        [WebMethod]
        public static string updateStatus(int billID, string statusID)
        {
            var Message = new Library.Class.cls_message();
            var serialize = new JavaScriptSerializer();
            var error = 0;
            try
            {
                var db = new Solution_30shineEntities();
                var RECORD = db.Stylist4Men_BillCutFree.FirstOrDefault(w => w.Id == billID);
                if (RECORD != null)
                {
                    RECORD.ImageStatusId = statusID;
                    RECORD.ModifiedTime = DateTime.Now;
                    db.Stylist4Men_BillCutFree.AddOrUpdate(RECORD);
                    error += db.SaveChanges() > 0 ? 0 : 1;
                    if (error == 0)
                    {
                        Message.success = true;
                        Message.message = "Cập nhật thành công.";
                    }
                    else
                    {
                        Message.success = false;
                        Message.message = "Cập nhật thất bại.";
                    }
                }
                else
                {
                    error = 1;
                    Message.success = false;
                    Message.message = "Bản ghi không tồn tại";
                }
                return serialize.Serialize(Message);
            }
            catch (Exception ex)
            {
                Message.success = false;
                Message.message = ex.Message;
                return serialize.Serialize(Message);
            }
        }

        [WebMethod]
        public static string GetErrorNoteById(int billID)
        {
            var db = new Solution_30shineEntities();
            var RECORD = db.Stylist4Men_BillCutFree.FirstOrDefault(w => w.Id == billID);

            return RECORD.ErrorNote;

        }

        [WebMethod]
        public static string LoadImage(int bID, int order, string base64)
        {
            var Message = new Library.Class.cls_message();
            var serialize = new JavaScriptSerializer();
            var error = 0;
            S4M_Bill obj = new S4M_Bill();
            string[] ListImg;
            string _Domain = ConfigurationManager.AppSettings["DomainKey_PreviewImg"];
            try
            {
                string imgPath = "";
                string ImageName = "";
                string ImgRootPath = "";
                byte[] image64 = Convert.FromBase64String(base64);
                Image image;

                using (MemoryStream ms = new MemoryStream(image64))
                {
                    image = Image.FromStream(ms, true);
                    ImgRootPath = "/Resource/Images/S4M_CheckImages/";
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath("~" + ImgRootPath)))
                    {
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~" + ImgRootPath));
                    }
                    string dirFullPath = HttpContext.Current.Server.MapPath("~" + ImgRootPath);
                    ImageName = "img_" + DateTime.Now.ToString("ddMMyyhhmmss") + ".png";
                    image.Save(dirFullPath + ImageName, ImageFormat.Png);
                }
                var db = new Solution_30shineEntities();
                var RECORD = db.Stylist4Men_BillCutFree.FirstOrDefault(w => w.Id == bID);
                ListImg = RECORD.Images.Split(',');
                //imgPath = "http://api.30shine.com" + ImgRootPath + ImageName;
                imgPath = "http://ql.30shine.com" + ImgRootPath + ImageName;
                //imgPath = "http://erp.30shine.net" + ImgRootPath + ImageName;
                if (RECORD != null)
                {
                    for (int i = 0; i < ListImg.Length; i++)
                    {
                        if (!(ListImg[i].StartsWith("http")))
                        {
                            ListImg[i] = _Domain + ListImg[i];
                        }
                    }

                    if (ListImg.Count() == 4)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                        obj.Img3 = ListImg[2];
                        obj.Img4 = ListImg[3];
                    }
                    if (ListImg.Count() == 3)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                        obj.Img3 = ListImg[2];
                    }
                    if (ListImg.Count() == 2)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                    }
                    if (ListImg.Count() == 1)
                    {
                        obj.Img1 = ListImg[0];
                    }

                    if (order == 1)
                    {
                        RECORD.ImageChecked1 = imgPath;
                    }
                    else if (order == 2)
                    {
                        RECORD.ImageChecked2 = imgPath;
                    }
                    else if (order == 3)
                    {
                        RECORD.ImageChecked3 = imgPath;
                    }
                    else if (order == 4)
                    {
                        RECORD.ImageChecked4 = imgPath;
                    }

                    if (RECORD.ImageChecked1 == null && obj.Img1 != null)
                    {
                        RECORD.ImageChecked1 = obj.Img1;
                    }
                    if (RECORD.ImageChecked2 == null && obj.Img2 != null)
                    {
                        RECORD.ImageChecked2 = obj.Img2;
                    }
                    if (RECORD.ImageChecked3 == null && obj.Img3 != null)
                    {
                        RECORD.ImageChecked3 = obj.Img3;
                    }
                    if (RECORD.ImageChecked4 == null && obj.Img4 != null)
                    {
                        RECORD.ImageChecked4 = obj.Img4;
                    }

                    RECORD.ModifiedTime = DateTime.Now;
                    db.Stylist4Men_BillCutFree.AddOrUpdate(RECORD);
                    error += db.SaveChanges() > 0 ? 0 : 1;
                    if (error == 0)
                    {
                        Message.success = true;
                        Message.message = "Cập nhật thành công.";
                    }
                    else
                    {
                        Message.success = false;
                        Message.message = "Cập nhật thất bại.";
                    }
                }
                else
                {
                    error = 1;
                    Message.success = false;
                    Message.message = "Bản ghi không tồn tại";
                }
                return serialize.Serialize(Message);
            }
            catch (Exception ex)
            {
                Message.success = false;
                Message.message = ex.Message;
                return serialize.Serialize(Message);
            }
        }

        [WebMethod]
        public static string RefreshCheckedImage(int bID, int order)
        {
            string imageOlder = "";
            S4M_Bill obj = new S4M_Bill();
            string[] ListImg;
            string _Domain = ConfigurationManager.AppSettings["DomainKey_PreviewImg"];
            var db = new Solution_30shineEntities();
            var RECORD = db.BillServices.FirstOrDefault(w => w.Id == bID);
            ListImg = RECORD.Images.Split(',');
            if (RECORD != null)
            {
                for (int i = 0; i < ListImg.Length; i++)
                {
                    if (!(ListImg[i].StartsWith("http")))
                    {
                        ListImg[i] = _Domain + ListImg[i];
                    }
                }
                if (ListImg.Count() == 4)
                {
                    obj.Img1 = ListImg[0];
                    obj.Img2 = ListImg[1];
                    obj.Img3 = ListImg[2];
                    obj.Img4 = ListImg[3];
                }
                if (ListImg.Count() == 3)
                {
                    obj.Img1 = ListImg[0];
                    obj.Img2 = ListImg[1];
                    obj.Img3 = ListImg[2];
                }
                if (ListImg.Count() == 2)
                {
                    obj.Img1 = ListImg[0];
                    obj.Img2 = ListImg[1];
                }
                if (ListImg.Count() == 1)
                {
                    obj.Img1 = ListImg[0];
                }
                if (order == 1 && (obj.Img1 != null || obj.Img1 != string.Empty))
                {
                    imageOlder = obj.Img1;
                }
                else if (order == 2 && (obj.Img2 != null || obj.Img2 != string.Empty))
                {
                    imageOlder = obj.Img2;
                }
                else if (order == 3 && (obj.Img3 != null || obj.Img3 != string.Empty))
                {
                    imageOlder = obj.Img3;
                }
                else if (order == 4 && (obj.Img4 != null || obj.Img4 != string.Empty))
                {
                    imageOlder = obj.Img4;
                }

            }
            return imageOlder;
        }

        [WebMethod]
        public static List<ErrorCutHair> GetErrorList()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.ErrorCutHairs.Where(a => a.IsDelete == false).ToList();
                return lst;
            }
        }

        [WebMethod]
        public static string updateErrorNote(int billID, string errorNote)
        {
            var Message = new Library.Class.cls_message();
            var serialize = new JavaScriptSerializer();
            var error = 0;
            try
            {
                var db = new Solution_30shineEntities();
                var RECORD = db.Stylist4Men_BillCutFree.FirstOrDefault(w => w.Id == billID);
                if (RECORD != null)
                {
                    RECORD.ErrorNote = errorNote;
                    RECORD.ModifiedTime = DateTime.Now;
                    db.Stylist4Men_BillCutFree.AddOrUpdate(RECORD);
                    error += db.SaveChanges() > 0 ? 0 : 1;
                    if (error == 0)
                    {
                        Message.success = true;
                        Message.message = "Cập nhật thành công.";
                    }
                    else
                    {
                        Message.success = false;
                        Message.message = "Cập nhật thất bại.";
                    }
                }
                else
                {
                    error = 1;
                    Message.success = false;
                    Message.message = "Bản ghi không tồn tại";
                }
                return serialize.Serialize(Message);
            }
            catch (Exception ex)
            {
                Message.success = false;
                Message.message = ex.Message;
                return serialize.Serialize(Message);
            }
        }
    }
    public class custom_BillService : Stylist4Men_BillCutFree
    {
        public string CustomerPhone { get; set; }
        public string CustomerName { get; set; }
    }

    public class S4M_Bill
    {
        public int Id { get; set; }
        public string BillDate { get; set; }
        public string ClassName { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerName { get; set; }
        public string Img1 { get; set; }
        public string Img2 { get; set; }
        public string Img3 { get; set; }
        public string Img4 { get; set; }
        public string ImageStatusId { get; set; }
        public string ErrorNote { get; set; }
        public string Student { get; set; }
        public int? CustomerId { get; set; }
    }

}