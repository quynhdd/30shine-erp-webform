﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Student_Listing.aspx.cs" Inherits="_30shine.GUI.BackEnd.Stylist4Men.Student_Listing" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý Stylist4Men &nbsp;&#187; Học viên &nbsp;&#187;</li>
                        <li class="li-listing active"><a href="/admin/s4m/hoc-vien/danh-sach.html">Danh sách</a></li>
                        <li class="li-add"><a href="/admin/s4m/hoc-vien/them-moi.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row row-filter">
                    <strong class="st-head"><i class="fa fa-filter"></i>Lọc kết quả</strong>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlClass" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 150px;"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlSalon" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 150px;">
                            <asp:ListItem Text="Chọn salon" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Hội quán cắt tóc 1" Value="15"></asp:ListItem>
                            <asp:ListItem Text="Hội quán cắt tóc 2" Value="16"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <a href="javascript:void(0);" onclick="location.href=location.href;" class="st-head btn-viewdata">Reset Filter</a>
                </div>
                <!-- End Filter -->
                <!-- Row Table Filter -->
                <div class="table-func-panel">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="table-wp">
                            <table class="table-add table-listing">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>ID</th>
                                        <th>Học viên</th>
                                        <th>Số điện thoại</th>
                                        <th>Email (Tài khoản)</th>
                                        <th>Lớp học</th>
                                        <th>Publish</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="Rpt" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td><%# Eval("Id") %></td>
                                                <td>
                                                    <a href="/admin/s4m/hoc-vien/<%# Eval("Id")%>.html"><%# Eval("Fullname") %></a>
                                                </td>
                                                <td><%# Eval("Phone") %></td>
                                                <td><%# Eval("Email") %></td>
                                                <td><%# Eval("ClassName") %></td>
                                                <td class="map-edit">
                                                    <input type="checkbox"
                                                        <%# Convert.ToInt32(Eval("Publish")) == 1 ? "Checked=\"True\"" : "" %> />
                                                    <div class="edit-wp">
                                                        <a class="elm edit-btn" href="/admin/s4m/hoc-vien/<%# Eval("Id") %>.html" title="Sửa"></a>
                                                        <a class="elm del-btn" onclick="del(this.parentNode.parentNode.parentNode,'<%# Eval("Id") %>', '<%# Eval("Fullname") %>')" href="javascript://" title="Xóa"></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>

                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                            { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
            </div>
            <%-- END Listing --%>
        </div>

        <script>
            jQuery(document).ready(function () {
                $("#glbS4M").addClass("active");
                $("#glbS4MStudent").addClass("active");
            });

            //============================
            // Event delete
            //============================
            function del(This, code, name) {
                var code = code || null,
                    name = name || null,
                    Row = This;
                if (!code) return false;

                // show EBPopup
                $(".confirm-yn").openEBPopup();
                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

                $("#EBPopup .yn-yes").bind("click", function () {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/SystemService/Ajax/Del.aspx/Delele_S4M_Student",
                        data: '{Code : "' + code + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            var mission = JSON.parse(response.d);
                            if (mission.success) {
                                delSuccess();
                                Row.remove();
                            } else {
                                delFailed();
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    });
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
            }
        </script>

    </asp:Panel>
</asp:Content>
