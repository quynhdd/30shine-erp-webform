﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreViewS4MSCSC_ImageView.aspx.cs" MasterPageFile="~/TemplateMaster/SCSCMaster.master" Inherits="_30shine.GUI.BackEnd.Stylist4Men.PreViewS4MSCSC_ImageView" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMainSCSC" runat="server">

    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false" Style="text-align: center; padding: 50px 0px; color: red; border-bottom: 1px solid #ddd;"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="preview-img">
            <%--<div class="heading-title">Xem ảnh lịch sử</div>--%>
            <!-- Filter-->
            <div class="top-filter">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <select name="ddlClass" id="ddlClass" class="form-control" onchange="getStudentByClass()">
                    </select>
                </div>
                <!-- Filter-->
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <select name="ddlSalon" id="ddlSalon" class="form-control select" onchange="getStylistBySalon()">
                    </select>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <select name="ddlStylist" id="ddlStylist" class="form-control">
                        <option value="0">Chọn Stylist/Học viên</option>
                    </select>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <input runat="server" name="txtToDate" type="text" value="" id="txtToDate" class="form-control txtDateTime" placeholder="Ngày ..." />
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <input type="button" id="btnTimKiem" class="form-control btn-default btnSearch" value="Tìm" />
                </div>
            </div>

            <!-- Customer info-->
            <div class="top-info">
                <div class="bill-info">SĐT: &nbsp<strong id="pre-date"></strong></div>
                <div class="salon-name">Bill ID: &nbsp<strong id="pre-id"></strong></div>
                <div class="salon-name">DV: &nbsp<strong id="pre-dv"></strong></div>
                <div class="salon-name" id="status"></div>
            </div>

            <!-- Kiểu tóc-->
            <fieldset>
                <select class="col-lg-6 col-md-6 col-sm-6 col-xs-12 form-control btnHairList" id="sltHairStyle" style="padding-top: 0; text-align: center">
                    <option value="0">--Chọn kiểu tóc--</option>
                </select>

            </fieldset>

            <!-- Ảnh chụp khách-->
            <div class="top4-img">
                <div class="container">
                    <div class="row">
                        <div class="col1">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 img-item" data-toggle="modal" data-target="#myModal" data-slide-to="0">
                                <img src="https://30shine.com/images/1.jpg" alt="" id="pre-img1" class="image" />
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 img-item" data-toggle="modal" data-target="#myModal" data-slide-to="1">
                                <img src="https://30shine.com/images/2.jpg" alt="" id="pre-img2" class="image" />
                            </div>
                        </div>
                        <div class="col2">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 img-item" data-toggle="modal" data-target="#myModal" data-slide-to="2">
                                <img src="https://30shine.com/images/3.jpg" alt="" id="pre-img3" class="image" />
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 img-item" data-toggle="modal" data-target="#myModal" data-slide-to="3">
                                <img src="https://30shine.com/images/4.jpg" alt="" id="pre-img4" class="image" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="wp_scsc">
                <div class="wp_title_scsc">
                    <p class="text_title_scsc">SCSC (<span class="span_score">score: &nbsp;</span><span id="TotalPoinSCSC">0</span>/<%=TotalPointCate %>) </p>
                    <p class="triangle-top-left"></p>
                </div>
                <div class="ctn_scsc scsc_shape" id="SHAPE">
                    <div class="wp_left_scsc">
                        <p class="p_cate_parent_scsc">
                            1.SHAPE
                        </p>
                    </div>
                    <ul class="ul_children_scsc " id="uate">
                        <asp:Repeater ID="rpt_Shape" runat="server">
                            <ItemTemplate>
                                <li class="li_children_scsc li_children_scsc<%#Eval("SCSC_Cate_IDCate") %>" data-catid="<%#Eval("SCSC_Cate_IDCate") %>" data-id="<%#Eval("ID_SCSC_Cate") %>" data-point="<%#Eval("SCSC_Cate_Point") %>" onclick="SCSC_CheckError($(this))">
                                    <div class="wp_img">
                                        <img class="scsc_img img_often" src="<%#Eval("SCSC_Cate_Image") %>" />
                                        <img class="scsc_img img_active" src="<%#Eval("SCSC_Cate_Image_Active") %>" />
                                    </div>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
                <div class="ctn_scsc" id="CONNECTIVE">
                    <div class="wp_left_scsc">
                        <p class="p_cate_parent_scsc">
                            2.CONNECTIVE
                        </p>
                    </div>
                    <ul class="ul_children_scsc">
                        <asp:Repeater ID="rpt_Connective" runat="server">
                            <ItemTemplate>
                                <li class="li_children_scsc li_children_scsc<%#Eval("SCSC_Cate_IDCate") %>" data-catid="<%#Eval("SCSC_Cate_IDCate") %>" data-id="<%#Eval("ID_SCSC_Cate") %>" data-point="<%#Eval("SCSC_Cate_Point") %>" onclick="SCSC_CheckError($(this))">
                                    <div class="wp_img">
                                        <img class="scsc_img img_often" src="<%#Eval("SCSC_Cate_Image") %>" />
                                        <img class="scsc_img img_active" src="<%#Eval("SCSC_Cate_Image_Active") %>" />
                                    </div>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
                <div class="ctn_scsc" id="SHARPNESS">
                    <div class="wp_left_scsc">
                        <p class="p_cate_parent_scsc">
                            3.SHARPNESS
                        </p>
                    </div>
                    <ul class="ul_children_scsc">
                        <asp:Repeater ID="rptSharpness" runat="server">
                            <ItemTemplate>
                                <li class="li_children_scsc li_children_scsc<%#Eval("SCSC_Cate_IDCate") %>" data-catid="<%#Eval("SCSC_Cate_IDCate") %>" data-id="<%#Eval("ID_SCSC_Cate") %>" data-point="<%#Eval("SCSC_Cate_Point") %>" onclick="SCSC_CheckError($(this))">
                                    <div class="wp_img">
                                        <img class="scsc_img img_often" src="<%#Eval("SCSC_Cate_Image") %>" />
                                        <img class="scsc_img img_active" src="<%#Eval("SCSC_Cate_Image_Active") %>" />
                                    </div>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
                <div class="ctn_scsc" id="COMPLETION">
                    <div class="wp_left_scsc">
                        <p class="p_cate_parent_scsc">
                            4.COMPLETION
                        </p>
                    </div>
                    <ul class="ul_children_scsc">
                        <asp:Repeater ID="rpt_Completion" runat="server">
                            <ItemTemplate>
                                <li class="li_children_scsc li_children_scsc<%#Eval("SCSC_Cate_IDCate") %>" data-catid="<%#Eval("SCSC_Cate_IDCate") %>" data-id="<%#Eval("ID_SCSC_Cate") %>" data-point="<%#Eval("SCSC_Cate_Point") %>" onclick="SCSC_CheckError($(this))">
                                    <div class="wp_img">
                                        <img class="scsc_img img_often" src="<%#Eval("SCSC_Cate_Image") %>" />
                                        <img class="scsc_img img_active" src="<%#Eval("SCSC_Cate_Image_Active") %>" />
                                    </div>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>

                    </ul>
                </div>
                <!-- Note-->
                <div class="wp_img_error">
                    <div class="wp_btn_img_error">
                        <a class="a_btn_img " id="ImgError" onclick="ErrorImage($(this))">ảnh thiếu  <span class="span_a_btn_img">MỜ/LỆCH </span></a>
                    </div>
                    <div class="wp_note_Error">
                        <textarea id="txtNoteByStylist" cols="20" rows="1" style="margin-bottom: 5px;" class="form-control ErrorNote" placeholder="Ghi chú Stylist"></textarea>
                        <textarea id="txtErrorNote" class="form-control ErrorNote" placeholder="SCSC note" rows="2"></textarea>
                    </div>
                </div>
            </div>

            <!-- btn-->
            <div class="img-nav" style="margin-bottom: 40px;">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-4 col-sm-4">
                            <input type="button" id="btnPreview" class="form-control btn-default" value="Trước" />
                        </div>
                        <div class="col-xs-4 col-sm-4 fix_style_count_item">
                            &nbsp(Đang xem hóa đơn <strong id="current-index">0</strong>/<strong id="total-index">0</strong>)&nbsp
                            <br />
                            &nbsp(Hóa đơn chưa check <strong id="total-billcheck">0</strong>)&nbsp
                        </div>
                        <div class="col-xs-4 col-sm-4">

                            <input type="button" id="btnNext" class="form-control btn-default" value="Sau" />
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" value="0" id="hdfIndexPage" />
            <input type="hidden" value="0" id="HDF_BillID" />
        </div>

        <!-- Pop up-->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog" id="modalDialog">
                <!-- Modal content-->
                <div class="modal-content" id="modalContent">
                    <div class="modal-header" id="modalHeader">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Check lỗi cắt</h4>
                    </div>
                    <div class="modal-body">
                        <div id="myCarousel" class="carousel" data-ride="carousel" data-interval="false">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                <li data-target="#myCarousel" data-slide-to="1"></li>
                                <li data-target="#myCarousel" data-slide-to="2"></li>
                                <li data-target="#myCarousel" data-slide-to="3"></li>
                            </ol>
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox" id="canvasWrap">
                                <div class="item active" id="div-image1">
                                    <canvas id="pre-img1-can" data-order="1" style="background-repeat: no-repeat; background-size: 100% auto!important;" title="Before"></canvas>
                                </div>
                                <div class="item" id="div-image2">
                                    <canvas id="pre-img2-can" data-order="2" style="background-repeat: no-repeat; background-size: 100% auto!important" title="Left"></canvas>
                                </div>
                                <div class="item" id="div-image3">
                                    <canvas id="pre-img3-can" data-order="3" style="background-repeat: no-repeat; background-size: 100% auto!important" title="Right"></canvas>
                                </div>
                                <div class="item" id="div-image4">
                                    <canvas id="pre-img4-can" data-order="4" style="background-repeat: no-repeat; background-size: 100% auto!important" title="After"></canvas>
                                </div>
                            </div>

                            <!-- Left and right controls -->
                            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev" id="prev-Slide">
                                <span class="fa fa-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next" id="next-Slide">
                                <span class="fa fa-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-xs-1"></div>
                            <div class="col-xs-2">
                                <i class="fa fa-plus-circle btnAddCycle" aria-hidden="true" onclick="addCycleToCanvas()" title="Thêm vòng lỗi"></i>
                            </div>
                            <div class="col-xs-2">
                                <i class="fa fa-minus-circle btnUndoCycle" aria-hidden="true" onclick="undoCycle()" title="undo"></i>
                            </div>
                            <div class="col-xs-2">
                                <i class="fa fa-floppy-o btnSaveError" aria-hidden="true" onclick="saveCheckedImages()" title="Lưu ảnh"></i>
                            </div>
                            <div class="col-xs-2">
                                <i class="fa fa-refresh btnReset" aria-hidden="true" onclick="refreshCheckedImage()" title="Sửa lại"></i>
                            </div>
                            <div class="col-xs-2">
                                <i class="fa fa-times-circle" aria-hidden="true" onclick="closeCanvas()" data-dismiss="modal" title="Đóng"></i>
                            </div>
                            <div class="col-xs-1"></div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <%--Hidden Field--%>
        <input id="HDFBill_ID" type="hidden" value="0" />
        <input id="HDF_ID_SCSC" type="hidden" value="0" />
        <input id="HDFErrorImage" type="hidden" value="0" />
        <input id="HDFShapMaxPoint" type="hidden" value="<%=ShapMaxPoint %>" />
        <input id="HDFConnectiveMaxPoint" type="hidden" value="<%=ConnectiveMaxPoint %>" />
        <input id="HDFSharpNessMaxPoint" type="hidden" value="<%=SharpNessMaxPoint %>" />
        <input id="HDFCompletionMaxPoint" type="hidden" value="<%=CompletionMaxPoint %>" />

        <!-- Loading-->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 100% !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
             .preview-img{
            float:none !important;
        }
        </style>
        <script type="text/ecmascript">
            var OBJ = "";
            var _ListPreviewImg = [];
            var objPreviewImg = {};
            var billID;

            var objCount = {};
            var _List = [];
            var totalBill = 0;

            $(document).ready(function () {
                startLoading();
                getListSalon();
                GetListHairMode();
                getListClass();
                cusId = "";

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    scrollMonth: false,
                    scrollInput: false,
                    onChangeDateTime: function (dp, $input) { }
                });

                /* Tìm kiếm */
                $('#btnTimKiem').click(function () {
                    //initCircles();
                    $("#total-billcheck").html("0");
                    var _ClassId = $("#ddlClass").val();
                    var _SalonId = $("#ddlSalon").val();
                    var _StylistId = $("#ddlStylist").val();
                    var _ToDate = $("#CtMainSCSC_txtToDate").val();

                    if (_ToDate == "") {
                        alert("Bạn chưa chọn ngày");
                        return;
                    }

                    if ($("#ddlClass").val() != "0") {

                        GetBillImage_S4M();
                    }
                    else if ($("#ddlSalon").val() != "0") {
                        $('#ddlClass option').removeAttr('selected').filter('[value=0]').attr('selected', 'selected');
                        startLoading();
                        $.ajax({
                            type: "POST",
                            url: "/GUI/BackEnd/Stylist4Men/PreViewS4MSCSC_ImageView.aspx/GetRandomBillImage",
                            data: '{_Date: "' + _ToDate + '", _SalonId: ' + _SalonId + ', _StylistId: ' + _StylistId + ', _imageStatus:"" }',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json", success: function (response) {
                                _ListPreviewImg = [];

                                $.each(response.d, function (key, value) {
                                    objPreviewImg = {};
                                    objPreviewImg.Id = value.Id;
                                    $("#HDF_BillID").val(objPreviewImg.Id);
                                    objPreviewImg.BillDate = value.BillDate;
                                    objPreviewImg.SalonName = value.SalonName;
                                    objPreviewImg.CustomerPhone = value.CustomerPhone;
                                    objPreviewImg.CustomerName = value.CustomerName;
                                    objPreviewImg.Img1 = value.Img1;
                                    objPreviewImg.Img2 = value.Img2;
                                    objPreviewImg.Img3 = value.Img3;
                                    objPreviewImg.Img4 = value.Img4;
                                    objPreviewImg.ImageStatusId = value.ImageStatusId;
                                    objPreviewImg.ErrorNote = value.ErrorNote;
                                    objPreviewImg.CustomerId = value.CustomerId;
                                    objPreviewImg.ServiceIds = value.ServiceIds;
                                    _ListPreviewImg.push(objPreviewImg);

                                    // Set giá trị mặc định billID;
                                    if (key == 0) {
                                        setBillID(value.Id);
                                        bindBillStatus(value.ImageStatusId);
                                    }
                                });

                                if (_ListPreviewImg.length > 0) {
                                    SetPreviewInfo(0);
                                    $('#hdfIndexPage').val("0");
                                    $('#total-index').text(_ListPreviewImg.length);
                                    $('#current-index').text('1');

                                }
                                _List = [];
                                $.each(response.d, function (key, value) {
                                    objCount = {};
                                    if (value.PointSCSC == -1) {
                                        objCount.PointSCSC = value.PointSCSC;
                                        _List.push(obj);
                                    }

                                });
                                _List = [];
                                $.each(response.d, function (key, value) {
                                    objCount = {};
                                    if (value.PointSCSC == -1) {
                                        objCount.PointSCSC = value.PointSCSC;
                                        _List.push(objCount);
                                    }
                                });
                                if (_List.length > 0) {
                                    $("#total-billcheck").text(_List.length);
                                    totalBill = parseInt(_List.length);
                                }
                                finishLoading();
                            },
                            failure: function (response) { console.log(response.d); }
                        });

                    }
                    var _StylistId = $("#ddlStylist").val();
                    var _ToDate = $("#CtMainSCSC_txtToDate").val();
                    startLoading();
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/Stylist4Men/PreViewS4MSCSC_ImageView.aspx/checkStylistFullImage",
                        data: '{date: "' + _ToDate + '",  stylistID: ' + _StylistId + '}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            //alert(response.d);
                            if (response.d == "Success") {
                                $("#status").html("<span style='color:green'>Stylist đã chụp đủ ảnh</span>");
                            }
                            else {
                                $("#status").html("<span style='color:red'>Stylist chụp thiếu ảnh</span>");
                            }
                        }
                    })
                });
                function getCookie(cname) {
                    var name = cname + "=";
                    var decodedCookie = decodeURIComponent(document.cookie);
                    var ca = decodedCookie.split(';');
                    for (var i = 0; i < ca.length; i++) {
                        var c = ca[i];
                        while (c.charAt(0) == ' ') {
                            c = c.substring(1);
                        }
                        if (c.indexOf(name) == 0) {
                            return c.substring(name.length, c.length);
                        }
                    }
                    return "";
                }
                /* fn khi chuyển tiếp sang bill tiếp theo */
                $('#btnNext').click(function () {
                    //updateErrorNote();
                    //InsertOrUpdateSCSC_CheckError();
                    var totalIndex = parseInt(_ListPreviewImg.length);
                    var index = parseInt($('#hdfIndexPage').val());

                    if ((totalIndex > 0) && (index < totalIndex - 1)) {
                        SetPreviewInfo(index + 1);
                        $('#hdfIndexPage').val(index + 1);
                        $('#current-index').text((index + 2));
                    }

                    cusHairId = "";
                });

                /* fn khi quay lại bill trước đó */
                $('#btnPreview').click(function () {
                    // updateErrorNote();
                    //InsertOrUpdateSCSC_CheckError();
                    var totalIndex = parseInt(_ListPreviewImg.length);
                    var index = parseInt($('#hdfIndexPage').val());

                    if ((totalIndex > 0) && (index > 0)) {
                        SetPreviewInfo(index - 1);
                        $('#hdfIndexPage').val(index - 1);
                        $('#current-index').text((index));
                    }
                    cusHairId = "";
                });

                /* mở popup ảnh chỉ tiết */
                $("#myModal").on('shown', function () {
                    initCircles();
                    var $activeCanvas = $(".carousel-inner .item.active").find("canvas");
                    var canvasId = $activeCanvas.attr("id");
                    canvas = document.getElementById(canvasId);
                    if (canvas != undefined) {
                        ctx = canvas.getContext("2d");
                    }
                    for (var j = 0; j < Circles.length; j++) {
                        if (Circles[j].idCustomer == $('#pre-id').text() && Circles[j].key == canvasId) {
                            for (var i = Circles[j].circles.length - 1; i >= 0; i--) {
                                Circles[j].circles[i].draw();
                            }
                        }
                    }
                });

                /* quay về slide trước khi show popup */
                $("#prev-Slide").click(function () {
                    var $activeCanvas = $(".carousel-inner .item.active").prev().find("canvas");

                    var canvasId = $activeCanvas.attr("id");
                    canvas = document.getElementById(canvasId);
                    if (canvas != undefined) {
                        ctx = canvas.getContext("2d");
                    }
                    for (var j = 0; j < Circles.length; j++) {
                        if (Circles[j].idCustomer == $('#pre-id').text() && Circles[j].key == canvasId) {
                            for (var i = Circles[j].circles.length - 1; i >= 0; i--) {
                                Circles[j].circles[i].draw();
                            }
                        }
                    }
                });

                /* chuyển tiếp silde sau khi show popup */
                $("#next-Slide").click(function () {
                    //var $activeCanvas = $(".carousel-inner .item.active").find("canvas").next();
                    var $activeCanvas = $(".carousel-inner .item.active").next().find("canvas");

                    var canvasId = $activeCanvas.attr("id");
                    canvas = document.getElementById(canvasId);
                    if (canvas != undefined) {
                        ctx = canvas.getContext("2d");
                    }
                    for (var j = 0; j < Circles.length; j++) {
                        if (Circles[j].idCustomer == $('#pre-id').text() && Circles[j].key == canvasId) {
                            for (var i = Circles[j].circles.length - 1; i >= 0; i--) {
                                Circles[j].circles[i].draw();
                            }
                        }
                    }
                })

            });
            function testObject(jsonStrig) {
                var obj = JSON.parse(jsonStrig);
                var arr = [];
                for (var i = 0; i < obj.length; i++) {
                    arr.push(obj[i].Name);
                }
                var result = arr.join();
                $("#pre-dv").text(result);
            }
            /* Khai báo biến */
            var _ListPreviewImg = [];
            var objPreviewImg = {};
            var billID;
            var cusHairId;
            var hairStyleId;
            //var faceTypeId;

            /* Check quyền element */
            function checkElement() {
                for (var j = 0; j < listElement.length; j++) {
                    var nameElement = listElement[j].ElementName;
                    var enabled = listElement[j].Enable;
                    var type = listElement[j].Type;
                    if (type == "hidden" && enabled == true) {
                        $("." + nameElement).addClass('hidden');
                    }
                    else if (type == "disable" && enabled == true && nameElement.substr(0, 3) == "btn") {
                        $("." + nameElement).addClass('disabled');
                    }
                    else {
                        $("." + nameElement).prop('disabled', enabled);
                    }
                }
            }

            /* Get list Salon */
            function getListSalon() {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Stylist4Men/PreViewS4MSCSC_ImageView.aspx/ReturnAllSalon",
                    data: '',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var select = $('#ddlSalon');
                        select.append('<option selected="selected" value="0">Chọn Salon</option>');
                        $.each(response.d, function (key, value) {
                            select.append('<option value="' + value.Id + '">' + value.Name + '</option>');
                        });
                        finishLoading();
                        checkTotalBill();
                    },
                    failure: function (response) { console.log(response.d); }
                });
            }

            /* Get list by salon  */
            function getStylistBySalon(stylistId) {
                $('#ddlClass option').removeAttr('selected').filter('[value=0]').attr('selected', 'selected');
                var salonId = $("#ddlSalon").val();
                var select = $('#ddlStylist');
                if (salonId != "0") {
                    startLoading();
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/Stylist4Men/PreViewS4MSCSC_ImageView.aspx/ReturnAllStylistBySalon",
                        data: '{_SalonId: ' + salonId + '}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {

                            select.html('');
                            select.append('<option selected="selected" value="0">Chọn Stylist/Học viên</option>');
                            $.each(response.d, function (key, value) {
                                select.append('<option value="' + value.Id + '">' + value.Fullname + '</option>');
                            });
                            $('#ddlStylist option').removeAttr('selected').filter('[value=' + stylistId + ']').attr('selected', 'selected');
                            finishLoading();
                        },
                        failure: function (response) { console.log(response.d); }
                    });
                } else {
                    select.html('');
                    select.append('<option selected="selected" value="0">Chọn Chọn Stylist/Học viên</option>');
                }

            }

            /* set list kiểu tóc vào btn */
            function GetListHairMode() {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Stylist4Men/PreViewS4MSCSC_ImageView.aspx/ReturnAllHairMode",
                    data: '',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var btnList = "";
                        //$.each(response.d, function (i, v) {
                        //    btnList += '<div class="col-xs-3 item-status-wrap">' +
                        //            '<a data-id="' + v.Id + '" href="javascript:void(0);" onclick="setActiveOnclickHair($(this))" disabled class="btn btn-default btn-xs btn-ok item-status btnHairMode">' + v.Title + '</a>' +
                        //            '</div>';
                        //});
                        $(".btnHairList").append(btnList);
                        //var heightArray = [];
                        //for (var i = 0; i < $(".btnHairList").find(".btnHairMode").length; i++) {
                        //    var height = $(".btnHairList").find(".btnHairMode").eq(i).height();
                        //    heightArray.push(height);
                        //}
                        //var maxHeight = Math.max.apply(null, heightArray);
                        //$(".btnHairList").find(".btn").css("height", maxHeight + 16);
                        //finishLoading();
                    },
                    failure: function (response) { console.log(response.d); }
                });
            }

            /* Get list lớp đào tạo */
            function getListClass() {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Stylist4Men/PreViewS4MSCSC_ImageView.aspx/ReturnAllClass",
                    data: '',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var select = $('#ddlClass');
                        select.append('<option selected="selected" value="0">Chọn lớp đào tạo</option>');
                        $.each(response.d, function (key, value) {
                            select.append('<option value="' + value.Id + '">' + value.Name + '</option>');
                        });
                        finishLoading();
                    },
                    failure: function (response) { console.log(response.d); }
                });
            }

            /* Get list học viên theo lớp đào tạo */
            function getStudentByClass(stylistId) {
                $('#ddlSalon option').removeAttr('selected').filter('[value=0]').attr('selected', 'selected');
                var classId = $("#ddlClass").val();
                var select = $('#ddlStylist');
                if (classId != "0") {
                    startLoading();
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/Stylist4Men/PreViewS4MSCSC_ImageView.aspx/ReturnAllStudentByClass",
                        data: '{_ClassId: ' + classId + '}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            select.html('');
                            select.append('<option selected="selected" value="0">Chọn Stylist/Học viên</option>');
                            $.each(response.d, function (key, value) {
                                select.append('<option value="' + value.Id + '">' + value.Fullname + '</option>');
                            });
                            $('#ddlStylist option').removeAttr('selected').filter('[value=' + stylistId + ']').attr('selected', 'selected');
                            finishLoading();
                        },
                        failure: function (response) { console.log(response.d); }
                    });
                } else {
                    select.html('');
                    select.append('<option selected="selected" value="0">Chọn Stylist/Học viên</option>');
                }
            }

            /*Chọn kiểu tóc */
            $("#sltHairStyle").on('change', function () {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Stylist4Men/PreViewS4MSCSC_ImageView.aspx/getHairStyleById",
                    data: '{billId:' + billID + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.d != null) {

                            cusHairId = response.d.Id;
                            hairStyleId = response.d.HairStyleId;
                            cusId = response.d.CustomerId;
                        }
                        updateCustomer_HairMode_Bill();
                    },
                    failure: function (response) { console.log(response.d); }
                });
            })

            /* update kiểu tóc và kiểu khuôn mặt ứng với từng bill */
            function updateCustomer_HairMode_Bill() {
                if (billID > 0) {
                    startLoading();

                    if (cusHairId == "" || cusHairId == undefined) {
                        cusHairId = 0;
                    }
                    if (cusId == "" || cusId == undefined || cusId == 'null') {
                        cusId = 0;
                    }
                    $.ajax({
                        url: "/GUI/BackEnd/Stylist4Men/PreViewS4MSCSC_ImageView.aspx/updateCustomer_HairMode_Bill",
                        type: "post",
                        data: '{cusHairId:' + cusHairId + ', hairStyleId : ' + $("#sltHairStyle").val() + ', CustomerId: "' + cusId + '", billID: ' + billID + '}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            finishLoading();
                        },
                        error: function (data) {
                        }
                    });
                }
            }

            /* get kiểu tóc và kiểu khuôn mặt của khách theo mã bill */
            function getHairStyleById(billID) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Stylist4Men/PreViewS4MSCSC_ImageView.aspx/getHairStyleById",
                    data: '{billId:' + billID + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        if (response.d != null) {
                            console.log(response);
                            cusHairId = response.d.Id;
                            hairStyleId = response.d.HairStyleId;
                            $("#sltHairStyle").val(hairStyleId);
                        }
                    },
                    failure: function (response) { console.log(response.d); }
                });
            }

            /* Hiển thị thông tin của bill có paras truyền từ module thống kê ảnh */
            function checkTotalBill() {
                var params = {};
                if (location.search) {
                    var parts = location.search.substring(1).split('&');
                    for (var i = 0; i < parts.length; i++) {
                        var nv = parts[i].split('=');
                        if (!nv[0]) continue;
                        params[nv[0]] = nv[1] || true;
                    }
                }
                if ([params.time, params.salonId, params.stylistId, params.imageStatus].indexOf(undefined) == -1) {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/Stylist4Men/PreViewS4MSCSC_ImageView.aspx/GetRandomBillImage",
                        data: '{_Date: "' + params.time + '", _SalonId: ' + params.salonId + ', _StylistId: ' + params.stylistId + ', _imageStatus:' + params.imageStatus + '}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            _ListPreviewImg = [];

                            $.each(response.d, function (key, value) {
                                objPreviewImg = {};
                                objPreviewImg.Id = value.Id;
                                $("#HDF_BillID").val(objPreviewImg.Id);
                                objPreviewImg.BillDate = value.BillDate;
                                objPreviewImg.SalonName = value.SalonName;
                                objPreviewImg.CustomerPhone = value.CustomerPhone;
                                objPreviewImg.CustomerName = value.CustomerName;
                                objPreviewImg.Img1 = value.Img1;
                                objPreviewImg.Img2 = value.Img2;
                                objPreviewImg.Img3 = value.Img3;
                                objPreviewImg.Img4 = value.Img4;
                                objPreviewImg.ImageStatusId = value.ImageStatusId;
                                objPreviewImg.ErrorNote = value.ErrorNote;
                                _ListPreviewImg.push(objPreviewImg);

                                // Set giá trị mặc định billID;
                                if (key == 0) {
                                    setBillID(value.Id);
                                    bindBillStatus(value.ImageStatusId);
                                }
                            });

                            if (_ListPreviewImg.length > 0) {
                                SetPreviewInfo(0);
                                $('#hdfIndexPage').val("0");
                                $('#total-index').text(_ListPreviewImg.length);
                                $('#current-index').text('1');
                                $("#CtMainSCSC_txtToDate").val(params.time);
                                $('#ddlSalon option').removeAttr('selected').filter('[value=' + params.salonId + ']').attr('selected', 'selected');
                                getStylistBySalon(params.stylistId);
                            }
                            finishLoading();
                        },
                        failure: function (response) { console.log(response.d); }
                    });
                }
            }

            /* Get bill theo S4M */
            function GetBillImage_S4M() {
                var _ClassId = $("#ddlClass").val();
                if (_ClassId == "0") {
                    alert("Bạn chưa chọn lớp");
                    return;
                }
                var _StylistId = $("#ddlStylist").val();
                var _ToDate = $("#CtMainSCSC_txtToDate").val();
                if (_ToDate == "") {
                    alert("Bạn chưa chọn ngày");
                    return;
                }

                startLoading();
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Stylist4Men/PreViewS4MSCSC_ImageView.aspx/GetBillImage_S4M",
                    data: '{_Date: "' + _ToDate + '", _ClassId: ' + _ClassId + ', _StylistId: ' + _StylistId + ', _imageStatus:"" }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        _ListPreviewImg = [];

                        $.each(response.d, function (key, value) {
                            objPreviewImg = {};
                            objPreviewImg.Id = value.Id;
                            $("#HDF_BillID").val(objPreviewImg.Id);
                            objPreviewImg.BillDate = value.BillDate;
                            objPreviewImg.SalonName = value.SalonName;
                            objPreviewImg.CustomerPhone = value.CustomerPhone;
                            objPreviewImg.CustomerName = value.CustomerName;
                            objPreviewImg.Img1 = value.Img1;
                            objPreviewImg.Img2 = value.Img2;
                            objPreviewImg.Img3 = value.Img3;
                            objPreviewImg.Img4 = value.Img4;
                            objPreviewImg.ImageStatusId = value.ImageStatusId;
                            objPreviewImg.ErrorNote = value.ErrorNote;
                            objPreviewImg.CustomerId = value.CustomerId;
                            objPreviewImg.ServiceIds = value.ServiceIds;
                            _ListPreviewImg.push(objPreviewImg);

                            // Set giá trị mặc định billID;
                            if (key == 0) {
                                setBillID(value.Id);
                                bindBillStatus(value.ImageStatusId);
                            }
                        });

                        if (_ListPreviewImg.length > 0) {
                            SetPreviewInfo(0);
                            $('#hdfIndexPage').val("0");
                            $('#total-index').text(_ListPreviewImg.length);
                            $('#current-index').text('1');

                        }

                        finishLoading();
                    },
                    failure: function (response) { console.log(response.d); }
                });
            }

            /********************************/
            // Xử lý canvas điểm cắt lỗi
            /********************************/
            var mousePosition;
            var isMouseDown;
            var isTouchStart;

            var focused = { key: 0, state: false };
            var canvasId;
            var $canvas;
            var canvas;
            var ctx;
            var canvasOrder;
            var classCircle = function () {
                this.key = "";
                this.circles = [];
            }
            var classCustomer = function () {
                this.idCustomer = "";
                this.key = "";
                this.circles = [];
            }
            var Circles = [];
            var currentCircle = new classCircle();
            var circles = [];
            function getCircleByKey(key, idCustomer) {

                for (var i = 0; i < Circles.length; i++) {
                    if (Circles[i].key === key && Circles[i].idCustomer == idCustomer) {
                        return Circles[i];
                    }
                }
            }

            /* main draw method */
            function draw() {
                //clear canvas
                ctx.clearRect(0, 0, canvas.width, canvas.height);

                for (var i = circles.length - 1; i >= 0; i--) {
                    circles[i].draw();
                }

            }
            //circle Object
            function Circle(x, y, r, stroke) {
                this.startingAngle = 0;
                this.endAngle = 2 * Math.PI;
                this.x = x;
                this.y = y;
                this.r = r;
                this.stroke = stroke;
                this.draw = function () {
                    ctx.beginPath();
                    ctx.arc(this.x, this.y, this.r, 0, this.endAngle, false);
                    ctx.lineWidth = 2;
                    ctx.strokeStyle = this.stroke;
                    ctx.stroke();
                }
            }

            function move(e) {
                var idCustomer = $("#pre-id").text();
                var $activeCanvas = $(".carousel-inner .item.active").find("canvas");
                var canvasId = $activeCanvas.attr("id");
                canvas = document.getElementById($activeCanvas.attr("id"));
                ctx = canvas.getContext("2d");
                circles = getCircleByKey(canvasId, idCustomer).circles;

                if (!isMouseDown) {
                    return;
                }
                var rect = canvas.getBoundingClientRect();
                mousePosition = {
                    x: Math.round(e.x - rect.left),
                    y: Math.round(e.y - rect.top)
                };
                //if any circle is focused
                if (focused.state) {
                    circles[focused.key].x = mousePosition.x;
                    circles[focused.key].y = mousePosition.y;
                    draw();
                    return;
                }
                //no circle currently focused check if circle is hovered
                for (var i = 0; i < circles.length; i++) {
                    if (intersects(circles[i])) {
                        circles.move(i, 0);
                        focused.state = true;
                        break;
                    }
                }
                draw();
            }

            /* Event touch trên mobile */
            function touchMove(e) {

                var idCustomer = $("#pre-id").text();
                var $activeCanvas = $(".carousel-inner .item.active").find("canvas");
                var canvasId = $activeCanvas.attr("id");
                canvas = document.getElementById(canvasId);
                ctx = canvas.getContext("2d");
                circles = getCircleByKey(canvasId, idCustomer).circles;

                if (!e) {
                    var e = event;
                    e.stopPropagation();
                    e.preventDefault();

                }
                var modalDialog = document.getElementById("modalDialog");
                var modalHeader = document.getElementById("modalHeader");

                mousePosition = {
                    x: Math.round(e.targetTouches[0].pageX - modalDialog.offsetLeft),
                    y: Math.round(e.targetTouches[0].pageY - window.pageYOffset - (modalDialog.offsetTop + modalHeader.offsetHeight))
                };

                //if any circle is focused
                if (focused.state) {
                    circles[focused.key].x = mousePosition.x;
                    circles[focused.key].y = mousePosition.y;
                    draw();
                    return;
                }
                for (var i = 0; i < circles.length; i++) {
                    if (intersects(circles[i])) {
                        circles.move(i, 0);
                        focused.state = true;
                        break;
                    }
                }
                draw();
            }

            //set mousedown state
            function setDraggable(e) {
                var t = e.type;
                if (t === "mousedown") {
                    isMouseDown = true;
                } else if (t === "mouseup") {
                    isMouseDown = false;
                    focused.state = false;
                }
            }

            function setDraggableMobile(e) {
                var t = e.type;
                if (t === "touchstart") {
                    isTouchStart = true;
                } else if (t === "touchend") {
                    isTouchStart = false;
                    focused.state = false;
                }
            }

            function intersects(circle) {
                var areaX = mousePosition.x - circle.x;
                var areaY = mousePosition.y - circle.y;
                return areaX * areaX + areaY * areaY <= circle.r * circle.r;
            }
            Array.prototype.move = function (old_index, new_index) {
                if (new_index >= this.length) {
                    var k = new_index - this.length;
                    while ((k--) + 1) {
                        this.push(undefined);
                    }
                }
                this.splice(new_index, 0, this.splice(old_index, 1)[0]);
            };
            function setCircleByKey(key, array) {
                for (var i = 0; i < Circles.length; i++) {
                    if (Circles[i].key === key) {
                        Circles[i].circles = array;
                    }
                }
            }
            /* Khởi tạo mảng lưu lỗi */
            function initCircles() {
                $(".carousel-inner canvas").each(function () {
                    canvas = document.getElementById($(this).attr("id"));
                    canvas.width = $(".modal-content").width();
                    canvas.height = canvas.width * 4 / 3;

                    var circle = new classCustomer();
                    circle.idCustomer = $('#pre-id').text();
                    circle.key = $(this).attr("id");
                    circle.circles = [];
                    Circles.push(circle);
                });
            }

            /* Chọn scsc */
            function SCSC_CheckError(This) {
                var Bill_ID = $("#HDFBill_ID").val();
                if (Bill_ID == 0) {
                    alert("Chưa có Bill để đánh giá hoặc có lỗi ! Bạn kiểm tra lại nếu không được hãy liên hệ với nhà phát triển !");

                    return false;
                }
                else {
                    if ($("#HDFErrorImage").val() == 0) {
                        var ParentCateId = This.data("catid");

                        if (This.hasClass("active")) {
                        }
                        else {

                            $(".li_children_scsc" + ParentCateId + "  ").removeClass("active");
                            This.addClass("active");
                            //This.removeClass("active");
                            //CalculatorTotalPoint();
                        }
                    }
                    else {
                        alert("Bill Ảnh mờ, lệch được coi là Bill thiếu ảnh không cần đánh giá ! ");
                    }
                }
            }

            // xu lý scsc khi click truoc sau
            function bindataSCSC(Bill_ID) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Stylist4Men/PreViewS4MSCSC_ImageView.aspx/BillDataWhereBillID",
                    data: '{ Bill_ID : ' + Bill_ID + '    }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        //console.log(response.d)
                        OBJ = response.d.data;
                        if (response.d.msg == "success") {
                            //console.log(response.d.data);
                            $("#HDF_ID_SCSC").val(OBJ.ID);
                            $(".li_children_scsc1 ").removeClass('active');
                            $(".li_children_scsc2 ").removeClass('active');
                            $(".li_children_scsc3 ").removeClass('active');
                            $(".li_children_scsc4 ").removeClass('active');
                            $('.li_children_scsc[data-id="' + OBJ.Shape_ID + '"]').addClass('active');
                            $('.li_children_scsc[data-id="' + OBJ.ConnectTive_ID + '"]').addClass('active');
                            $('.li_children_scsc[data-id="' + OBJ.SharpNess_ID + '"]').addClass('active');
                            $('.li_children_scsc[data-id="' + OBJ.ComPlatetion_ID + '"]').addClass('active');
                            $("#TotalPoinSCSC").text(OBJ.PointError);
                            $("#txtErrorNote").val(OBJ.NoteError);
                            if (OBJ.ImageError == 1) {
                                $("#HDFErrorImage").val(1);
                                $("#ImgError").addClass('active');
                            }
                            else {
                                $("#HDFErrorImage").val(0)
                                $("#ImgError").removeClass('active');
                            }
                        }
                        else {
                            ResetSCSC();
                        }

                    },
                    failure: function (response) { console.log(response.d); }
                });
            };

            // Tính tổng điểm
            //function CalculatorTotalPoint() {
            //    var _TotalPointSCSC = 0;
            //    var PointShape = $(".li_children_scsc1.active").data("point");
            //    if (PointShape == undefined) {
            //        PointShape = 0;
            //    }
            //    var PointConnective = $(".li_children_scsc2.active").data("point");
            //    if (PointConnective == undefined) {
            //        PointConnective = 0;
            //    }
            //    var PointSharpNess = $(".li_children_scsc3.active").data("point");
            //    if (PointSharpNess == undefined) {
            //        PointSharpNess = 0;
            //    }
            //    var PointComPletion = $(".li_children_scsc4.active").data("point");
            //    if (PointComPletion == undefined) {
            //        PointComPletion = 0;
            //    }
            //    if (PointShape != 0 && PointConnective != 0 && PointSharpNess != 0 && PointComPletion != 0) {
            //        _TotalPointSCSC = parseInt(PointShape) + parseInt(PointConnective) + parseInt(PointSharpNess) + parseInt(PointComPletion);

            //    }
            //    else {
            //        _TotalPointSCSC = 0;
            //    }
            //    $("#TotalPoinSCSC").text(_TotalPointSCSC);

            //};

            // xu lý scsc khi click truoc sau
            function BindFlowserService(Bill_ID) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Stylist4Men/PreViewS4MSCSC_ImageView.aspx/BindFlowserService",
                    data: '{ Bill_ID : ' + Bill_ID + '    }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        console.log(response.d)
                        var OBJ = response.d.data;

                        var _Count = OBJ.length;
                        $('#Service').html('');
                        if (_Count > 0) {
                            for (var i = 0; i < _Count; i++) {
                                var html = '<p>' + OBJ[i].Name + '. &nbsp;' + '</p>';

                            }
                        }
                        else {
                            $('#Service').html('');
                        }
                        $('#Service').append('<strong>Dịch vụ  : &nbsp; </strong> ' + html);
                    },
                    failure: function (response) { console.log(response.d); }
                });
            };

            /* Set thông tin của bill ảnh : bill, khách hàng, ảnh, lỗi cắt, kiểu cắt, .... */
            function SetPreviewInfo(index) {
                testObject(_ListPreviewImg[index].ServiceIds);
                //console.log(_ListPreviewImg[index]);
                if (_ListPreviewImg[index].CustomerName == null) {
                    $('#pre-cus').text("Không có");
                }
                else {
                    $('#pre-cus').text(_ListPreviewImg[index].CustomerName);
                }
                if (_ListPreviewImg[index].CustomerPhone == null) {
                    $('#pre-date').text("Không có");
                }
                else {
                    if (_ListPreviewImg[index].CustomerPhone.length == 11) {
                        $('#pre-date').text("******" + _ListPreviewImg[index].CustomerPhone.substr(7, 11));
                    }
                    else {
                        $('#pre-date').text("*****" + _ListPreviewImg[index].CustomerPhone.substr(6, 10));
                    }
                }

                $('#pre-id').text(_ListPreviewImg[index].Id);
                $("#HDFBill_ID").val(_ListPreviewImg[index].Id);

                //console.log($("#HDFBill_ID").val());
                $('#phone-id').text(_ListPreviewImg[index].CustomerPhone);
                //$('#pre-salon').text(_ListPreviewImg[index].CustomerName);
                $("#txtErrorNote").val(_ListPreviewImg[index].ErrorNote);
                $("#txtNoteByStylist").val(_ListPreviewImg[index].NoteByStylist);
                $("#pre-customerid").text(_ListPreviewImg[index].CustomerId);

                $('#pre-img1').attr('src', _ListPreviewImg[index].Img1);
                $('#pre-img2').attr('src', _ListPreviewImg[index].Img2);
                $('#pre-img3').attr('src', _ListPreviewImg[index].Img3);
                $('#pre-img4').attr('src', _ListPreviewImg[index].Img4);

                $('#pre-img1-can').css({ 'background-image': "url(" + _ListPreviewImg[index].Img1 + ")" });
                $('#pre-img2-can').css({ 'background-image': "url(" + _ListPreviewImg[index].Img2 + ")" });
                $('#pre-img3-can').css({ 'background-image': "url(" + _ListPreviewImg[index].Img3 + ")" });
                $('#pre-img4-can').css({ 'background-image': "url(" + _ListPreviewImg[index].Img4 + ")" });
                $('#pre-img1-can').attr('data-src', _ListPreviewImg[index].Img1);
                $('#pre-img2-can').attr('data-src', _ListPreviewImg[index].Img2);
                $('#pre-img3-can').attr('data-src', _ListPreviewImg[index].Img3);
                $('#pre-img4-can').attr('data-src', _ListPreviewImg[index].Img4);
                console.log(_ListPreviewImg[index]);

                // Bind Service 
                BindFlowserService(_ListPreviewImg[index].Id);
                // set giá trị cho billID
                setBillID(_ListPreviewImg[index].Id);
                // reset trạng thái active của item trạng thái
                resetItemStatus();
                // bind lại trạng thái
                bindBillStatus(_ListPreviewImg[index].ImageStatusId);
                //getHairStyleById( billID );
                getHairStyleById(_ListPreviewImg[index].Id);
                getErrorNote();

                //Set SCSC
                bindataSCSC(_ListPreviewImg[index].Id);

                //initCircles();
            }

            /* cập nhật giá trị cho billID */
            function setBillID(id) {
                billID = id;
            }
            /* set giá trị cho list lỗi cắt (đã đc đánh giá hoặc chưa)  */
            function bindBillStatus(statusID) {
                $(".btnErrorList").removeClass("active");
                if (statusID != null && statusID.length > 0) {
                    var stt = statusID.toString().split(',');
                    for (var i = 0; i < stt.length; i++) {
                        if (stt[i] == "1") {
                            $(".btnErrorList[data-id='" + stt[i] + "']").addClass("btn-ok");
                        }
                        else {
                            $(".btnErrorList[data-id='" + stt[i] + "']").addClass("btn-act");
                        }
                        $(".btnErrorList[data-id='" + stt[i] + "']").addClass("active");
                    }
                }
            }

            /* reset các giá trị của list btn  */
            function resetItemStatus() {
                $(".btnErrorList").removeClass("active");
                $(".btnHairMode").removeClass("active");
                //$(".btnFaceType").removeClass("active");
            }

            /* set trạng thái active cho btn lỗi cắt */
            function setActiveOnclick(This) {
                $(".btnErrorList").removeClass("active");
                This.addClass("active");
            }

            /* update lỗi cắt cho bill tương ứng */
            function updateStatus(This) {
                var statusID = new Array();

                if (This.attr("data-id") == 1) {
                    $(".btnErrorList").removeClass("active");
                    This.addClass("btn-ok");
                    This.addClass("active");
                    statusID.push("1");
                }
                else {
                    $(".btnErrorList").find(".btn-ok").removeClass("active");
                    if (This.hasClass("active")) {
                        This.removeClass("active");
                    }
                    else {
                        This.addClass("btn-act");
                        This.addClass("active");
                    }

                    for (var i = 0; i < $(".btnErrorList").length; i++) {
                        if ($(".btnErrorList").eq(i).attr("data-id") != 1 && $(".btnErrorList").eq(i).hasClass("active")) {
                            statusID.push($(".btnErrorList").eq(i).attr("data-id"));
                        }
                    }
                }

                if (billID > 0 && statusID.length > 0) {
                    startLoading();
                    $.ajax({
                        url: "/GUI/BackEnd/Stylist4Men/PreViewS4MSCSC_ImageView.aspx/updateStatus",
                        type: "post",
                        data: '{billID:' + billID + ', statusID : "' + statusID + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            var msg = JSON.parse(data.d);
                            if (msg.success) {
                                var index = findIndexByBillID(billID);
                                if (index != null) {
                                    _ListPreviewImg[index].ImageStatusId = statusID;
                                }
                            }
                            finishLoading();
                        },
                        error: function (data) {
                            alert(response.d.responseText);
                        }
                    });
                }
            }

            /* lấy index theo mã bill */
            function findIndexByBillID(billID) {
                var index = null;
                if (_ListPreviewImg != null) {
                    for (var i = 0; i < _ListPreviewImg.length; i++) {
                        if (billID == _ListPreviewImg[i].Id) {
                            index = i;
                            break;
                        }
                    }
                }
                return index;
            }

            /* Get note and Update note SCSC */
            function getErrorNote() {
                if (billID > 0) {
                    $.ajax({
                        url: "/GUI/BackEnd/Stylist4Men/PreViewS4MSCSC_ImageView.aspx/GetErrorNoteById",
                        type: "post",
                        data: '{billID:' + billID + '}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            $("#txtErrorNote").val(data.d.NoteByManager);
                            $("#txtNoteByStylist").val(data.d.NoteByStylist);
                        },
                        error: function (data) {

                        }
                    });
                }
            }
            //function updateErrorNote() {
            //    debugger;
            //    if (billID > 0) {
            //        startLoading();
            //        $.ajax({
            //            url: "/GUI/BackEnd/Stylist4Men/PreViewS4MSCSC_ImageView.aspx/updateErrorNote",
            //            type: "post",
            //            data: '{billID:' + billID + ', errorNote : "' + $("#txtErrorNote").val() + '", noteByStylist: "' + $("#txtNoteByStylist").val() + '"}',
            //            contentType: "application/json; charset=utf-8",
            //            dataType: "json",
            //            success: function (data) {
            //                //$("#txtErrorNote").val('');
            //                finishLoading();
            //            },
            //            error: function (data) {
            //                //alert(response.d);
            //            }
            //        });
            //    }
            //}
            /* Load lại ảnh đã checked ra màn hiển thị */
            function refreshCheckedImage() {
                startLoading();
                var $activeCanvas = $(".carousel-inner .item.active").find("canvas");
                var canvasId = $activeCanvas.attr("id");
                $.ajax({
                    url: "/GUI/BackEnd/Stylist4Men/PreViewS4MSCSC.aspx/RefreshCheckedImage",
                    type: "post",
                    data: '{bID : ' + billID + ', order : ' + $activeCanvas.attr("data-order") + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $("#" + canvasId).css({ 'background-image': "url(" + data.d + ")" });
                        $("#" + canvasId).attr('data-src', data.d);
                        finishLoading();
                    },
                    error: function (data) {
                    }
                });
            }

            /* Full screen */
            var videoElement = document.getElementById("myModal");

            function toggleFullScreen() {
                if (!document.mozFullScreen && !document.webkitFullScreen) {
                    if (videoElement.mozRequestFullScreen) {
                        videoElement.mozRequestFullScreen();
                    } else {
                        videoElement.webkitRequestFullScreen();
                    }
                } else {
                    if (document.mozCancelFullScreen) {
                        document.mozCancelFullScreen();
                    } else {
                        document.webkitCancelFullScreen();
                    }
                }
            }

            document.addEventListener("keydown", function (e) {
                if (e.keyCode == 13) {
                    toggleFullScreen();
                }
            }, false);

            /*  Truy vấn canvas hiện tại */
            function getActiveCanvas() {
                var idCustomer = $("#pre-id").text();
                $canvas = canvasId = $(".carousel-inner .item.active").find("canvas");
                canvasId = $canvas.attr("id");
                canvasOrder = $canvas.attr("data-order");
                canvas = document.getElementById(canvasId);
                ctx = canvas.getContext("2d");
                circles = getCircleByKey(canvasId, idCustomer).circles;
            }

            /*  Đóng khung canvas */
            function closeCanvas() {
                canvas = null;
                ctx = null;
                //circles = [];
            }
            /* Thêm vòng tròn lỗi */
            function addCycleToCanvas() {
                getActiveCanvas();
                var idCustomer = $("#pre-id").text();
                ctx = canvas.getContext("2d");
                circles = getCircleByKey(canvasId, idCustomer).circles;

                var c1 = new Circle(70, 70, 40, "red");
                c1.draw();

                circles.push(c1);

                canvas.addEventListener('mousemove', move, false);
                canvas.addEventListener('mousedown', setDraggable, false);
                canvas.addEventListener('mouseup', setDraggable, false);

                canvas.addEventListener("touchmove", touchMove, false);
                canvas.addEventListener("touchstart", setDraggableMobile, false);
                canvas.addEventListener("touchend", setDraggableMobile, false);

            }
            /*  Hủy vòng tròn lỗi */
            function undoCycle() {
                getActiveCanvas();
                circles.shift();
                setCircleByKey(canvasId, circles);
                draw();
            }

            function bindCircleFromArray() {
                getActiveCanvas();
                draw();
            }

            // Xử lý Ảnh thiếu or mờ 
            //function ErrorImage(This) {
            //    if (This.hasClass("active")) {
            //        console.log("a");
            //        This.removeClass("active");
            //        $("#HDFErrorImage").val(0);
            //    }
            //    else {
            //        console.log("b");
            //        This.addClass("active");
            //        $("#HDFErrorImage").val(1)
            //        $(".li_children_scsc").removeClass("active");
            //        $("#TotalPoinSCSC").text(0);

            //    }
            //}
            // Insert Or Update
            //function InsertOrUpdateSCSC_CheckError() {
            //    var SCSC_PointError = 0;
            //    ///  Tinh bill co  loi hay khong
            //    var PointShape = 0;
            //    var PointConnective = 0;
            //    var PointSharpNess = 0;
            //    var PointComPletion = 0;
            //    //End
            //    var ImageError = $("#HDFErrorImage").val();
            //    var ID_SCSC_Check = $("#HDF_ID_SCSC").val();
            //    var _TotalPoint_SCSC = $("#TotalPoinSCSC").text().replace(/[^\d]+/g, '');
            //    var Bill_ID = $("#HDFBill_ID").val();
            //    if (Bill_ID == 0) {
            //        alert("Chưa có Bill để đánh giá hoặc có lỗi ! Bạn kiểm tra lại nếu không được hãy liên hệ với nhà phát triển !");

            //        return false;
            //    }
            //    var SHAPE_ID = $(".li_children_scsc1.active").data("id");
            //    var CONNECTIVE_ID = $(".li_children_scsc2.active").data("id");
            //    var SHARPNESS_ID = $(".li_children_scsc3.active").data("id");
            //    var COMPLETION_ID = $(".li_children_scsc4.active").data("id");
            //    if (ImageError == 0) {
            //        if (SHAPE_ID == undefined) {
            //            alert("Chưa có đánh giá SHPAE");
            //            return false;
            //        }
            //        else {
            //            PointShape = $(".li_children_scsc1.active").data("point");
            //        }

            //        if (CONNECTIVE_ID == undefined) {
            //            alert("Chưa có đánh giá CONNECTIVE");
            //            return false;
            //        }
            //        else {
            //            PointConnective = $(".li_children_scsc2.active").data("point");
            //        }

            //        if (SHARPNESS_ID == undefined) {
            //            alert("Chưa có đánh giá SHARPNESS");
            //            return false;
            //        }
            //        else {
            //            PointSharpNess = $(".li_children_scsc3.active").data("point");
            //        }

            //        if (COMPLETION_ID == undefined) {
            //            alert("Chưa có đánh giá COMPLETION");
            //            return false;
            //        }
            //        else {
            //            PointComPletion = $(".li_children_scsc4.active").data("point");
            //        }
            //    }
            //    else {
            //        SHAPE_ID = 0;
            //        CONNECTIVE_ID = 0;
            //        SHARPNESS_ID = 0;
            //        COMPLETION_ID = 0;
            //    }

            //    var NoteError = $("#txtErrorNote").val();
            //    if (PointShape != 0 && PointConnective != 0 && PointSharpNess != 0 && PointComPletion != 0) {
            //        SCSC_PointError = parseInt(PointShape) + parseInt(PointConnective) + parseInt(PointSharpNess) + parseInt(PointComPletion);
            //if (totalBill > 0 && OBJ == null) {
            //    totalBill = totalBill - 1;
            //    $('#total-billcheck').text(totalBill);
            //}
            //    }
            //    else {
            //        SCSC_PointError = 0;
            //    }
            //    var MaxPointShap_ID = $("#HDFShapMaxPoint").val();
            //    var MaxPointConnective_ID = $("#HDFConnectiveMaxPoint").val();
            //    var MaxPointSharpNess_ID = $("#HDFSharpNessMaxPoint").val();
            //    var MaxPointCompletion_ID = $("#HDFCompletionMaxPoint").val();

            //    startLoading();
            //    $.ajax({
            //        type: "POST",
            //        url: "/GUI/BackEnd/Stylist4Men/PreViewS4MSCSC_ImageView.aspx/InsertAndUpdateSCSC_CheckError",
            //        data: '{ ID_SCSC_Check : ' + ID_SCSC_Check + ' , Bill_ID : ' + Bill_ID + ' , ImageError : ' + ImageError + ' , NoteError : "' + NoteError + '" , _TotalPoint_SCSC : ' + _TotalPoint_SCSC + ' , SHAPE_ID : ' + SHAPE_ID + ' , CONNECTIVE_ID : ' + CONNECTIVE_ID + ', SHARPNESS_ID : ' + SHARPNESS_ID + ', COMPLETION_ID : ' + COMPLETION_ID + ', SCSC_PointError : ' + SCSC_PointError + ', MaxPointShap_ID : ' + MaxPointShap_ID + ', MaxPointConnective_ID : ' + MaxPointConnective_ID + ' , MaxPointSharpNess_ID : ' + MaxPointSharpNess_ID + ', MaxPointCompletion_ID : ' + MaxPointCompletion_ID + '   }',
            //        contentType: "application/json; charset=utf-8",
            //        dataType: "json", success: function (response) {
            //            if (response.d != null) {

            //                $("#HDF_ID_SCSC").val(response.d.data.ID);
            //                $("#danhgia").css("display", "block")
            //                finishLoading();
            //                setTimeout(function () {
            //                    $("#danhgia").css("display", "none")
            //                }, 1500);


            //        },
            //        failure: function (response) { console.log(response.d); }
            //    });
            //}

            /* Lưu ảnh đã check */
            //function saveCheckedImages() {
            //    getActiveCanvas();
            //    ctx.globalCompositeOperation = "destination-over";
            //    var dataUrl = "";
            //    var image = new Image();
            //    image.setAttribute('crossOrigin', 'anonymous');
            //    image.src = $canvas.attr("data-src");
            //    image.onload = function () {
            //        ctx.drawImage(image, 0, 0, canvas.width, canvas.height);
            //        dataUrl = canvas.toDataURL("image/jpg").replace(/^data:image\/(png|jpg);base64,/, "");
            //        startLoading();
            //        $.ajax({
            //            url: "/GUI/BackEnd/Stylist4Men/PreViewS4MSCSC_ImageView.aspx/LoadImage",
            //            type: "post",
            //            data: '{bID : ' + billID + ', order : ' + $canvas.attr("data-order") + ', base64:' + JSON.stringify(dataUrl) + '}',
            //            contentType: "application/json; charset=utf-8",
            //            dataType: "json",
            //            success: function (data) {
            //                setCircleByKey(canvasId, circles);
            //                finishLoading();
            //                displayMessageSuccess("Lưu ảnh thành công!");
            //            },
            //            error: function (data) {
            //            }
            //        });
            //    };
            //}

            function ResetSCSC() {
                $('.li_children_scsc').removeClass('active');
                $("#HDF_ID_SCSC").val(0);
                $("#TotalPoinSCSC").text(0);
                $("#HDFErrorImage").val(0)
                $("#ImgError").removeClass('active');
                $("#txtErrorNote").val('');
            }
        </script>
    </asp:Panel>
</asp:Content>
