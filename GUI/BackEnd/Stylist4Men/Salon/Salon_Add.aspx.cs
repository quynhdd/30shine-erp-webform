﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Script.Serialization;

namespace _30shine.GUI.BackEnd.Stylist4Men.Salon
{
    public partial class Salon_Add : System.Web.UI.Page
    {
        protected Stylist4Men_Salon objSalon;
        protected string _Code;
        protected bool _IsUpdate = false;
        private bool Perm_Access = false;
        protected List<cls_media> listImg = new List<cls_media>();

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckPermission();

            if (!IsPostBack)
            {
                if (IsUpdate())
                {
                    if (Bind_OBJ())
                    {
                        Bind_City();
                        Bind_District();
                    }
                }
                else
                {
                    Bind_City();
                    Bind_District();
                }   
                
            }            
        }

        protected void ExcAddOrUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
            {
                Update();
            }
            else
            {
                Add();
            }
        }

        private void Add()
        {
            using (var db = new Solution_30shineEntities())
            {
                var obj = new Stylist4Men_Salon();
                var objKQKD = new KetQuaKinhDoanh_Salon();
                obj.Name = Name.Text;
                obj.ShortName = ShortName.Text;
                obj.Description = Description.Text;
                obj.Email = Email.Text;
                obj.Phone = Phone.Text;
                obj.CityId = Convert.ToInt32(City.SelectedValue);
                obj.DistrictId = Convert.ToInt32(District.SelectedValue);
                obj.Address = Address.Text;
                obj.Images = Array.IndexOf(new string[] { "", "[]" }, HDF_Images.Value) == -1 ? HDF_Images.Value : "";
                obj.ManagerName = SalonManagerName.Text.Trim();
                obj.Fanpage = Fanpage.Text.Trim();
                obj.FanpageId = FanpageId.Text.Trim();
                obj.Order = _Order.Text != "" ? Convert.ToInt32(_Order.Text) : 0;
                obj.Publish = Publish.Checked;
                obj.CreatedDate = DateTime.Now;
                obj.IsDelete = 0;
                
                // Validate
                var Error = false;

                if (!Error)
                {
                    db.Stylist4Men_Salon.Add(obj);
                    var exc = db.SaveChanges();

                    if (exc > 0)
                    {
                        var newSalon = db.Database.SqlQuery<Tbl_Salon>("select top 1 * from Stylist4Men_Salon order by Id DESC").SingleOrDefault();
                        objKQKD.SalonId = newSalon.Id;
                        objKQKD.SalonName = newSalon.Name;
                        objKQKD.CreatedTime = DateTime.Now;
                        objKQKD.IsDelete = false;
                        objKQKD.Meta = "salon";
                        objKQKD.Order = newSalon.Id - 1;
                        objKQKD.Publish = newSalon.Publish;
                        objKQKD.ShortName = newSalon.ShortName;
                        db.KetQuaKinhDoanh_Salon.Add(objKQKD);
                        db.SaveChanges();
                        // Thông báo thành công
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                        UIHelpers.Redirect("/admin/s4m/salon/" + obj.Id + ".html", MsgParam);
                    }
                    else
                    {
                        var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);  
                    }
                }
            }
        }

        private void Update()
        {
            using (var db = new Solution_30shineEntities())
            {
                int Id;
                var objKQKD = new KetQuaKinhDoanh_Salon();
                if (int.TryParse(_Code, out Id))
                {
                    objSalon = db.Stylist4Men_Salon.FirstOrDefault(w => w.Id == Id);
                   
                    if (!objSalon.Equals(null))
                    {
                        objSalon.Name = Name.Text;
                        objSalon.ShortName = ShortName.Text;
                        objSalon.Description = Description.Text;
                        objSalon.Email = Email.Text;
                        objSalon.Phone = Phone.Text;
                        objSalon.CityId = Convert.ToInt32(City.SelectedValue);
                        objSalon.DistrictId = Convert.ToInt32(District.SelectedValue);
                        objSalon.Address = Address.Text;
                        objSalon.Images = Array.IndexOf(new string[] { "", "[]" }, HDF_Images.Value) == -1 ? HDF_Images.Value : "";
                        objSalon.ManagerName = SalonManagerName.Text.Trim();
                        objSalon.Fanpage = Fanpage.Text.Trim();
                        objSalon.FanpageId = FanpageId.Text.Trim();
                        objSalon.Order = _Order.Text != "" ? Convert.ToInt32(_Order.Text) : 0;
                        objSalon.Publish = Publish.Checked;
                        objSalon.CreatedDate = DateTime.Now;
                        objSalon.IsDelete = 0;

                        db.Stylist4Men_Salon.AddOrUpdate(objSalon);
                        var exc = db.SaveChanges();
                        if (exc > 0)
                        {
                            objKQKD = db.KetQuaKinhDoanh_Salon.FirstOrDefault(w => w.SalonId == Id);
                            if (objKQKD != null)
                            {
                                objKQKD.SalonId = objSalon.Id;
                                objKQKD.SalonName = objSalon.Name;
                                objKQKD.ModifiedTime = DateTime.Now;
                                objKQKD.Meta = "salon";
                                objKQKD.Order = objSalon.Order;
                                objKQKD.Publish = objSalon.Publish;
                                objKQKD.ShortName = objSalon.ShortName;
                                db.KetQuaKinhDoanh_Salon.AddOrUpdate(objKQKD);
                                db.SaveChanges();
                            }
                            else
                            {
                                objKQKD = new KetQuaKinhDoanh_Salon();
                                objKQKD.SalonId = objSalon.Id;
                                objKQKD.SalonName = objSalon.Name;
                                objKQKD.CreatedTime = DateTime.Now;
                                objKQKD.IsDelete = false;
                                objKQKD.Meta = "salon";
                                objKQKD.Order = objSalon.Order;
                                objKQKD.Publish = objSalon.Publish;
                                objKQKD.ShortName = objSalon.ShortName;
                                db.KetQuaKinhDoanh_Salon.AddOrUpdate(objKQKD);
                                db.SaveChanges();
                            }
                            
                            // Thông báo thành công
                            var MsgParam = new List<KeyValuePair<string, string>>();
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                            UIHelpers.Redirect("/admin/s4m/salon/" + objSalon.Id + ".html", MsgParam);
                        }
                        else
                        {
                            var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                            var status = "warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                        }
                    }
                    else
                    {
                        var msg = "Lỗi! Không tìm thấy danh mục.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);

                    }
                }
                else
                {
                    MsgSystem.Text = "Lỗi! Không tìm thấy danh mục.";
                    MsgSystem.CssClass = "msg-system warning";
                }

                Context.RewritePath("+", "", "");
            }
        }

        private bool Bind_OBJ()
        {
            using (var db = new Solution_30shineEntities())
            {
                var ExistOBJ = true;
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    objSalon = db.Stylist4Men_Salon.Where(w => w.Id == Id).FirstOrDefault();

                    if (!objSalon.Equals(null))
                    {
                        Name.Text = objSalon.Name;
                        ShortName.Text = objSalon.ShortName;
                        Description.Text = objSalon.Description;
                        Phone.Text = objSalon.Phone;
                        Email.Text = objSalon.Email;
                        Address.Text = objSalon.Address;
                        SalonManagerName.Text = objSalon.ManagerName;
                        Fanpage.Text = objSalon.Fanpage;
                        FanpageId.Text = objSalon.FanpageId;
                        _Order.Text = Convert.ToString(objSalon.Order);
                        if (objSalon.Images != null && objSalon.Images != "")
                        {
                            var serialize = new JavaScriptSerializer();
                            listImg = serialize.Deserialize<List<cls_media>>(objSalon.Images).ToList();
                        }

                        if (objSalon.Publish == true)
                        {
                            Publish.Checked = true;
                        }
                        else
                        {
                            Publish.Checked = false;
                        }
                    }
                    else
                    {
                        ExistOBJ = false;
                        var msg = "Lỗi! Kiểu nhân viên không tồn tại";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
                else
                {
                    ExistOBJ = false;
                    var msg = "Lỗi! Kiểu nhân viên không tồn tại";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }

                return ExistOBJ;
            }
        }

        protected void Bind_City()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.TinhThanhs.OrderBy(p => p.ThuTu).ToList();

                City.DataTextField = "TenTinhThanh";
                City.DataValueField = "Id";

                City.DataSource = lst;
                City.DataBind();

                if (objSalon != null && objSalon.CityId != null)
                {
                    var ItemSelected = City.Items.FindByValue(objSalon.CityId.ToString());
                    if (ItemSelected != null)
                        ItemSelected.Selected = true;
                }                
            }
        }

        protected void Bind_District()
        {
            using (var db = new Solution_30shineEntities())
            {
                int Id = Convert.ToInt32(City.SelectedValue);
                if (Id != 0)
                {
                    var lst = db.QuanHuyens.Where(w => w.TinhThanhID == Id).OrderBy(o => o.ThuTu).ToList();

                    District.DataTextField = "TenQuanHuyen";
                    District.DataValueField = "Id";

                    District.DataSource = lst;
                    District.DataBind();
                }

                if (!IsPostBack && objSalon != null && objSalon.DistrictId != null)
                {
                    var ItemSelected = District.Items.FindByValue(objSalon.DistrictId.ToString());
                    if (ItemSelected != null)
                    {
                        ItemSelected.Selected = true;
                    }
                    else
                    {
                        District.SelectedIndex = 0;
                    }
                }
                else
                {
                    District.SelectedIndex = 0;
                }
            }
        }

        protected void Reload_District(object sender, EventArgs e)
        {
            Bind_District();
        }

        public void ReloadPage()
        {
            HttpRequest Request = HttpContext.Current.Request;
            string host = "http://" + Request.Url.Host.ToString();
            string rawUrl = Request.RawUrl.ToString();
            string Url = host + rawUrl;
            Response.Redirect(Url);
        }

        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["User_Id"] == null)
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin" };
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        public class cls_media
        {
            public string url { get; set; }
            public string title { get; set; }
            public string description { get; set; }
        }
    }
}