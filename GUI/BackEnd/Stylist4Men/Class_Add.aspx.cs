﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Globalization;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Stylist4Men
{
    public partial class Class_Add : System.Web.UI.Page
    {
        private string PageID = "";
        protected Stylist4Men_Class OBJ;
        protected string _Code;
        protected bool _IsUpdate = false;
        private bool Perm_Access = false;
        protected bool Perm_ViewAllData = false;

        private CultureInfo culture = new CultureInfo("vi-VN");
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                if (Request.QueryString["Code"] != null)
                {
                    PageID = "S4M_LH_EDIT";
                }
                else
                {
                    PageID = "S4M_LH_ADD";
                }
                IPermissionModel permissionModel = new PermissionModel();
                var permission = Session["User_Permission"].ToString();
                Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                ////Perm_Add = permissionModel.GetActionByActionNameAndPageId("Perm_Add", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList>() { ddlSalon }, Perm_ViewAllData);
                if (IsUpdate())
                {
                    if (Bind_OBJ())
                    {
                        //
                    }
                }
                else
                {
                    //
                }

            }
        }

        protected void ExcAddOrUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
            {
                Update();
            }
            else
            {
                Add();
            }
        }

        private void Add()
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                var obj = new Stylist4Men_Class();
                obj.Name = Name.Text;
                obj.Note = Note.Text;
                try
                {
                    obj.SalonId = Convert.ToInt32(ddlSalon.SelectedValue);
                    obj.StartTime = Convert.ToDateTime(StartTime.Text, culture);
                    obj.GraduateTime = Convert.ToDateTime(GraduateTime.Text, culture);
                }
                catch { }
                obj.Publish = Publish.Checked;
                obj.CreatedTime = DateTime.Now;
                obj.IsDelete = false;

                // Validate
                var Error = false;

                if (!Error)
                {
                    db.Stylist4Men_Class.Add(obj);
                    var exc = db.SaveChanges();

                    if (exc > 0)
                    {
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                        UIHelpers.Redirect("/admin/s4m/lop-hoc/" + obj.Id + ".html", MsgParam);
                    }
                    else
                    {
                        var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
            }
        }

        private void Update()
        {
            using (var db = new Solution_30shineEntities())
            {
                int Id = 0;
                int integer;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Stylist4Men_Class.FirstOrDefault(w => w.Id == Id);

                    if (!OBJ.Equals(null))
                    {
                        OBJ.Name = Name.Text;
                        OBJ.Note = Note.Text;
                        //OBJ.Money = int.TryParse(Money.Text, out integer) ? integer : 0;
                        try
                        {
                            OBJ.SalonId = Convert.ToInt32(ddlSalon.SelectedValue);
                            OBJ.StartTime = Convert.ToDateTime(StartTime.Text, culture);
                            OBJ.GraduateTime = Convert.ToDateTime(GraduateTime.Text, culture);
                        }
                        catch { }
                        OBJ.Publish = Publish.Checked;
                        OBJ.ModifiedTime = DateTime.Now;

                        db.Stylist4Men_Class.AddOrUpdate(OBJ);
                        var exc = db.SaveChanges();
                        if (exc > 0)
                        {
                            var MsgParam = new List<KeyValuePair<string, string>>();
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                            UIHelpers.Redirect("/admin/s4m/lop-hoc/" + OBJ.Id + ".html", MsgParam);
                        }
                        else
                        {
                            var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                            var status = "warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                        }
                    }
                    else
                    {
                        var msg = "Lỗi! Bản ghi không tồn tại.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);

                    }
                }
                else
                {
                    MsgSystem.Text = "Lỗi! Bản ghi không tồn tại.";
                    MsgSystem.CssClass = "msg-system warning";
                }

                Context.RewritePath("+", "", "");
            }
        }

        private bool Bind_OBJ()
        {
            using (var db = new Solution_30shineEntities())
            {
                var ExistOBJ = true;
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Stylist4Men_Class.Where(w => w.Id == Id).FirstOrDefault();

                    if (!OBJ.Equals(null))
                    {
                        Name.Text = OBJ.Name;
                        Note.Text = OBJ.Note;
                        
                        var itemSelected = ddlSalon.Items.FindByValue(OBJ.SalonId.ToString());
                        if (itemSelected != null)
                        {
                            itemSelected.Selected = true;
                        }

                        if (!OBJ.StartTime.Equals(null))
                        {
                            StartTime.Text = String.Format("{0:dd/MM/yyyy}", OBJ.StartTime);
                        }

                        if (!OBJ.GraduateTime.Equals(null))
                        {
                            GraduateTime.Text = String.Format("{0:dd/MM/yyyy}", OBJ.GraduateTime);
                        }

                        if (OBJ.Publish == true)
                        {
                            Publish.Checked = true;
                        }
                        else
                        {
                            Publish.Checked = false;
                        }

                    }
                    else
                    {
                        ExistOBJ = false;
                        var msg = "Lỗi! Bản ghi không tồn tại";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
                else
                {
                    ExistOBJ = false;
                    var msg = "Lỗi! Bản ghi không tồn tại";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }

                return ExistOBJ;
            }
        }

        public void bindSalon()
        {
            using (var db = new Solution_30shineEntities())
            {
                var source = db.Tbl_Salon.Where(w => w.IsDelete != 1 && w.Publish == true).ToList();
                ddlSalon.DataTextField = "Name";
                ddlSalon.DataValueField = "Id";
                ddlSalon.DataSource = source;
                ddlSalon.DataBind();
                if (OBJ != null)
                {
                    var itemSelected = ddlSalon.Items.FindByValue(OBJ.SalonId.ToString());
                    if (itemSelected != null)
                    {
                        itemSelected.Selected = true;
                    }
                }
            }
        }

        public void ReloadPage()
        {
            HttpRequest Request = HttpContext.Current.Request;
            string host = "http://" + Request.Url.Host.ToString();
            string rawUrl = Request.RawUrl.ToString();
            string Url = host + rawUrl;
            Response.Redirect(Url);
        }

        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }

        
    }
}