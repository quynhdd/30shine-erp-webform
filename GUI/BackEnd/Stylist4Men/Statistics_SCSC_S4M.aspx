﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Statistics_SCSC_S4M.aspx.cs" Inherits="_30shine.GUI.BackEnd.Stylist4Men.Statistics_SCSC_S4M" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">

    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
           <style>
            .sub-menu ul.ul-sub-menu li.active a, 
            .sub-menu ul.ul-sub-menu li:hover a
             {color:green;}
            .sub-menu ul.ul-sub-menu li#report:hover a,
            .sub-menu ul.ul-sub-menu li#report:active a
            {color:green !important;}
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>SCSC &nbsp;&#187; </li>
                        <li class="be-report-li"><a href="/s4m/thong-ke-anh-V2.html"><i class="fa fa-th-large"></i>Thống kê số lượng ảnh S4M</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing be-report" onload="abc()">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row">
                    <div class="filter-item" style="margin-left: 0;">
                        <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="datepicker-wp">
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                                ClientIDMode="Static" runat="server"></asp:TextBox>

                            <strong class="st-head" style="margin-left: 10px; margin-right: 10px;">
                                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                            </strong>
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeTo" placeholder="Đến ngày"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlSalon" runat="server" ClientIDMode="Static" CssClass="form-control select" Style="width: 190px;" OnSelectedIndexChanged="Salon_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlStylist" runat="server" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                    </div>

                    <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" Text="Xem dữ liệu"
                        ClientIDMode="Static" onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                </div>
                <!-- End Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <asp:UpdatePanel ID="UPTotal" runat="server" ClientIDMode="Static" style="margin-top: 15px;">
                    <ContentTemplate>
                        <div class="row">
                            <strong class="st-head"><i class="fa fa-file-text"></i>Thống kê SCSC</strong>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-wp">
                                    <table class="table-add table-listing" id="tblAllSalon" style="float: left; width: 100%;">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Tổng bill</th>
                                                <th>% Bill không ảnh</th>
                                                <th>% Bill mờ lệch</th>
                                                <th>Lỗi Shape</th>
                                                <th>Lỗi Connective</th>
                                                <th>Lỗi SharpNess</th>
                                                <th>Lỗi Completion</th>                                            
                                                <th>Bill chưa đánh giá</th>
                                                <th>%Tổng lỗi SCSC</th>
                                                <th>Điểm SCSC TB</th>
                                                <th>% Lỗi KCS</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="rptAllSalon" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><b><%# Eval("ShortName") %></b></td>
                                                        <td class="td-SoLanOK"><%#Eval("TongBill") %> </td>
                                                        <td class="td-HinhKhoiChuaChuan"><%#Eval("Bill_NotImg") %>%</td>
                                                        <td><%# Eval("Percent_BillImg_MoLech") %>%</td>
                                                        <td class="td-HinhKhoiChuaLienKet"><%#Eval("TotalShapeError") %></td>
                                                        <td class="td-NgonTocQuaDay"><%#Eval("TotalConnectiveError") %></td>
                                                        <td class="td-CaoMaiGayLoi"><%#Eval("TotalSharpNessError") %></td>
                                                        <td class="td-VuotSapLoi"><%#Eval("TotalCompletionError") %></td>
                                                        <td class="td-DuongCatChuaNet"><%#Eval("BillChuaDanhGia") %></td>
                                                        <td><%# Eval("Percent_BillError") %>%</td>
                                                        <td><%# Eval("PointSCSC_TB") %></td>
                                                        <td><%# Eval("Percent_ErrorKCS") %>%</td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                            <asp:Repeater ID="rptOnlySalon" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td>
                                                            <b><%# Eval("Fullname") %></b>     </td>
                                                        <td class="td-SoLanOK"><%#Eval("TotalBill") %> </td>
                                                        <td class="td-HinhKhoiChuaChuan"><%#Eval("Bill_NotImg") %>%</td>
                                                        <td><%# Eval("Percent_BillImg_MoLech") %>%</td>
                                                        <td class="td-HinhKhoiChuaLienKet"><%#Eval("TotalBillErrorShape") %></td>
                                                        <td class="td-NgonTocQuaDay"><%#Eval("TotalBillErrorConnective") %></td>
                                                        <td class="td-CaoMaiGayLoi"><%#Eval("TotalBillErrorSharpNess") %></td>
                                                        <td class="td-VuotSapLoi"><%#Eval("TotalBillErrorCompletion") %></td>
                                                        <td class="td-DuongCatChuaNet"><%#Eval("TotalBillChuaDanhGia") %></td>
                                                        <td><%# Eval("Total_SCSC_Error") %>%</td>
                                                        <td><%#Eval("PointSCSC_TB") %></td>
                                                        <td><%# Eval("Percent_BillError") %>%</td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="col-xs-6">
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>

                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
            </div>
            <%-- END Listing --%>
        </div>
         <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 190px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        //$input.val($input.val().split(' ')[0]);
                    }
                });
            });
        </script>
    </asp:Panel>
</asp:Content>
