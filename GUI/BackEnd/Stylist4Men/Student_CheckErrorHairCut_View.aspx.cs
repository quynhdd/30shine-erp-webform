﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.Stylist4Men
{
    public partial class Student_CheckErrorHairCut_View : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txtToDate.Value = DateTime.Now.ToString("dd/MM/yyyy");
        }

        [WebMethod]
        public static List<Stylist4Men_Class> ReturnAllClass()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Stylist4Men_Class.Where(a => a.IsDelete == false && a.Publish == true).ToList();
                return lst;
            }
        }

        [WebMethod]
        public static List<Stylist4Men_Student> ReturnAllStudentByClass(int _ClassId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Stylist4Men_Student.Where(a => a.IsDelete == false && a.Publish == true && a.ClassId == _ClassId).ToList();
                return lst;
            }
        }

        [WebMethod]
        public static List<S4M_Bill> GetRandomBillImage(string _Date, int _ClassId, int _StudentId, string _imageStatus)
        {
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                DateTime _FilterDate = Convert.ToDateTime(_Date, culture);
                string _FromDate = String.Format("{0:yyyy/MM/dd}", _FilterDate);
                string _ToDate = String.Format("{0:yyyy/MM/dd}", _FilterDate.AddDays(1));

                var sqlCount = @"select count(*) from Stylist4Men_BillCutFree a where a.CreatedTime between '" + _FromDate + "' and '" + _ToDate + "' and a.IsDelete != 1 and a.Images is not null and a.ClassId =" + _ClassId;

                int _TotalBill = db.Database.SqlQuery<int>(sqlCount).First();

                var sql = "";

                if (_StudentId != 0)
                {
                    sql = "select _bill.*, _cus.CustomerName, _cus.CustomerPhone from Stylist4Men_BillCutFree _bill left join Stylist4Men_Customer _cus on _cus.Id = _bill.CustomerId where _bill.CreatedTime between '" + _FromDate + "' and '" + _ToDate + "' and _bill.IsDelete != 1 and _bill.Images is not null and _bill.ClassId = " + _ClassId + " and _bill.StudentId = " + _StudentId;
                }
                else
                {
                    sql = "select _bill.*, _cus.CustomerName, _cus.CustomerPhone from Stylist4Men_BillCutFree _bill left join Stylist4Men_Customer _cus on _cus.Id = _bill.CustomerId where _bill.CreatedTime between '" + _FromDate + "' and '" + _ToDate + "' and _bill.IsDelete != 1 and _bill.Images is not null and _bill.ClassId = " + _ClassId;
                }
                
                var list = db.Database.SqlQuery<custom_BillService>(sql).ToList();

                var objClass = db.Stylist4Men_Class.Find(_ClassId);

                List<S4M_Bill> lstBillView = new List<S4M_Bill>();
                S4M_Bill obj = new S4M_Bill();
                string[] ListImg;

                string _Domain = ConfigurationManager.AppSettings["DomainKey_PreviewImg"];

                foreach (var item in list)
                {
                    ListImg = item.Images.Split(',');
                    obj = new S4M_Bill();
                    obj.Id = item.Id;
                    obj.BillDate = string.Format("{0:dd/MM/yyyy}", item.CreatedTime);
                    obj.CustomerPhone = item.CustomerPhone;
                    obj.CustomerName = item.CustomerName;
                    obj.CustomerId = item.CustomerId;

                    if (item.ImageStatusId != null)
                    {
                        obj.ImageStatusId = item.ImageStatusId;
                    }

                    for (int i = 0; i < ListImg.Length; i++)
                    {
                        if (!(ListImg[i].StartsWith("http")))
                        {
                            ListImg[i] = _Domain + ListImg[i];
                        }
                    }
                    if (ListImg.Count() == 4)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                        obj.Img3 = ListImg[2];
                        obj.Img4 = ListImg[3];
                    }
                    if (ListImg.Count() == 3)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                        obj.Img3 = ListImg[2];
                    }
                    if (ListImg.Count() == 2)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                    }
                    if (ListImg.Count() == 1)
                        obj.Img1 = ListImg[0];

                    obj.ClassName = objClass.Name;
                    obj.ErrorNote = item.ErrorNote;
                    if (item.ImageChecked1 != null)
                    {
                        obj.Img1 = item.ImageChecked1;
                    }
                    if (item.ImageChecked2 != null)
                    {
                        obj.Img2 = item.ImageChecked2;
                    }
                    if (item.ImageChecked3 != null)
                    {
                        obj.Img3 = item.ImageChecked3;
                    }
                    if (item.ImageChecked4 != null)
                    {
                        obj.Img4 = item.ImageChecked4;
                    }

                    lstBillView.Add(obj);
                }

                return lstBillView;
            }
        }

        [WebMethod]
        public static string GetErrorNoteById(int billID)
        {
            var db = new Solution_30shineEntities();
            var RECORD = db.Stylist4Men_BillCutFree.FirstOrDefault(w => w.Id == billID);

            return RECORD.ErrorNote;

        }

        [WebMethod]
        public static List<ErrorCutHair> GetErrorList()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.ErrorCutHairs.Where(a => a.IsDelete == false).ToList();
                return lst;
            }
        }
        
        public class custom_BillService : Stylist4Men_BillCutFree
        {
            public string CustomerPhone { get; set; }
            public string CustomerName { get; set; }
        }

        public class S4M_Bill
        {
            public int Id { get; set; }
            public string BillDate { get; set; }
            public string ClassName { get; set; }
            public string CustomerPhone { get; set; }
            public string CustomerName { get; set; }
            public string Img1 { get; set; }
            public string Img2 { get; set; }
            public string Img3 { get; set; }
            public string Img4 { get; set; }
            public string ImageStatusId { get; set; }
            public string ErrorNote { get; set; }
            public string Student { get; set; }
            public int? CustomerId { get; set; }
        }
    }
    

}