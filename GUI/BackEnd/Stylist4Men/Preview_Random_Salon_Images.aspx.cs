﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.CustomClass;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using Project.Helpers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Migrations;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace _30shine.GUI.BackEnd.Stylist4Men.PreviewImage
{
    public partial class Preview_Random_Salon_Images : System.Web.UI.Page
    {
        private string PageID = "S4M_KCS";
        protected string Permission = "";
        protected bool Perm_Access = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_Stylist = false;
        protected string listElement_Perm = string.Empty;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Stylist = permissionModel.GetActionByActionNameAndPageId("Perm_Stylist", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //////Perm_Add = permissionModel.GetActionByActionNameAndPageId("Perm_Add", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Stylist = permissionModel.CheckPermisionByAction("Perm_Stylist", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                //ContentWrap.Visible = false;
                //NotAllowAccess.Visible = true;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            txtToDate.Value = DateTime.Now.ToString("dd/MM/yyyy");
            //PassDataFromServerToClient();

        }
        /// <summary>
        /// Check permission
        /// </summary>
        //private void CheckPermission()
        //{
        //    if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
        //    {
        //        var MsgParam = new List<KeyValuePair<string, string>>();
        //        UIHelpers.Redirect("/dang-nhap.html", MsgParam);
        //    }
        //    else
        //    {
        //        string[] Allow = new string[] { "root", "admin", "salonmanager", "checkin", "checkout", "inventory_manager", "inventory_manager_2" };
        //        string[] AllowViewAllData = new string[] { "root", "admin" };
        //        string[] AllowView = new string[] { "staff" };  //stylist

        //        Permission = Session["User_Permission"].ToString().Trim();
        //        if (Array.IndexOf(Allow, Permission) != -1)
        //        {
        //            Perm_Access = true;
        //        }
        //        if (Array.IndexOf(AllowViewAllData, Permission) != -1)
        //        {
        //            Perm_ViewAllData = true;
        //        }
        //        if (Array.IndexOf(AllowView, Permission) != -1)
        //        {
        //            Perm_Stylist = true;
        //        }
        //    }
        //}
        private void PassDataFromServerToClient()
        {
            List<ElementEnable> list = listElement();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jSon = serializer.Serialize(list);
            listElement_Perm = jSon;
        }

        protected List<ElementEnable> listElement()
        {
            List<ElementEnable> list = new List<ElementEnable>();
            list.Add(new ElementEnable() { ElementName = "btn-ok", Enable = Perm_Stylist, Type = "disable" });
            list.Add(new ElementEnable() { ElementName = "btn-cube", Enable = Perm_Stylist, Type = "disable" });
            list.Add(new ElementEnable() { ElementName = "btn-line", Enable = Perm_Stylist, Type = "disable" });
            list.Add(new ElementEnable() { ElementName = "btnAddCycle", Enable = Perm_Stylist, Type = "hidden" });
            list.Add(new ElementEnable() { ElementName = "btnUndoCycle", Enable = Perm_Stylist, Type = "hidden" });
            list.Add(new ElementEnable() { ElementName = "btnSaveError", Enable = Perm_Stylist, Type = "hidden" });
            list.Add(new ElementEnable() { ElementName = "ErrorNote", Enable = Perm_Stylist, Type = "disable" });
            list.Add(new ElementEnable() { ElementName = "btnReset", Enable = Perm_Stylist, Type = "hidden" });
            return list;
        }
        
        /// <summary>
        /// Lấy danh sách Styist theo Salon
        /// </summary>
        /// <param name="_SalonId"></param>
        /// <returns></returns>
        [WebMethod]
        public static List<Staff> ReturnAllStylistBySalon(int _SalonId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Staffs.Where(a => a.IsDelete == 0 && a.Active == 1 && a.Type == 1 && a.SalonId == _SalonId).ToList();
                return lst;
            }
        }

        /// <summary>
        /// Lấy danh sách lớp học
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static List<Stylist4Men_Class> ReturnAllClass()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Stylist4Men_Class.Where(a => a.IsDelete == false && a.Publish == true).ToList();
                return lst;
            }
        }

        /// <summary>
        /// Lấy toàn bộ Học viên Stylist4Men
        /// </summary>
        /// <param name="_ClassId"></param>
        /// <returns></returns>
        [WebMethod]
        public static List<Staff> ReturnAllStudentByClass(int _ClassId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Staffs.Where(a => a.IsDelete == 0 && a.Active == 1 && a.S4MClassId == _ClassId).ToList();
                return lst;
            }
        }

        #region Get bill theo salon
        [WebMethod]
        public static List<BillServiceView> GetRandomBillImage(string _Date, int _SalonId, int _StylistId, string _imageStatus)
        {
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                DateTime _FilterDate = Convert.ToDateTime(_Date, culture);
                string _FromDate = String.Format("{0:yyyy/MM/dd}", _FilterDate);
                string _ToDate = String.Format("{0:yyyy/MM/dd}", _FilterDate.AddDays(1));

                var sqlCount = @"select count(*) from BillService a where a.CreatedDate between '" + _FromDate + "' and '" + _ToDate + "' and a.IsDelete != 1 and a.Pending = 0 and a.Images is not null and a.SalonId = " + _SalonId;

                int _TotalBill = db.Database.SqlQuery<int>(sqlCount).First();

                var sql = "";

                if (_StylistId != 0)
                {
                    sql = @"select a.*,b.Fullname as CustomerName, b.Phone as CustomerPhone ,b.Id as IdCustomer  from BillService a left join Customer b on a.CustomerId = b.Id where a.Staff_Hairdresser_Id = " + _StylistId + " and a.CreatedDate between '" + _FromDate + "' and '" + _ToDate + "' and a.IsDelete != 1 and a.Pending = 0 and a.Images is not null and a.SalonId = " + _SalonId;
                }
                else
                {
                    sql = @"select a.*,b.Fullname as CustomerName, b.Phone as CustomerPhone ,b.Id as IdCustomer  from BillService a left join Customer b on a.CustomerId = b.Id where a.CreatedDate between '" + _FromDate + "' and '" + _ToDate + "' and a.IsDelete != 1 and a.Pending = 0 and a.Images is not null and a.SalonId = " + _SalonId;
                }

                //1-bill không có lỗi cắt || 2-bill có lỗi hình khối || 3-bill lỗi đường cắt  || 4-bill lỗi hình khối chưa liên kết
                //5-bill lỗi ngọn tóc quá dày || 6-bill cạo mai gáy lỗi || 7-bill vuốt sáp lỗi || 8-bill ảnh thiếu/mờ
                //9-bill có ảnh || 10-bill đã đánh giá || 11-bill chưa đánh giá 

                if (_imageStatus == "1" || _imageStatus == "2" || _imageStatus == "3" || _imageStatus == "4" || _imageStatus == "5" || _imageStatus == "6" || _imageStatus == "7" || _imageStatus == "8")
                {
                    sql += " and ImageStatusId like '%" + _imageStatus + "%' ";
                }
                else if (_imageStatus == "10")
                {
                    sql += " and a.ImageStatusId is not null";
                }
                else if (_imageStatus == "11")
                {
                    sql += " and a.ImageStatusId is null";
                }
                var list = db.Database.SqlQuery<custom_BillService>(sql).ToList();

                var objSalon = db.Tbl_Salon.Find(_SalonId);

                List<BillServiceView> lstBillView = new List<BillServiceView>();
                BillServiceView obj = new BillServiceView();
                string[] ListImg;

                string _Domain = ConfigurationManager.AppSettings["DomainKey_PreviewImg"];

                foreach (var item in list)
                {
                    ListImg = item.Images.Split(',');
                    obj = new BillServiceView();
                    obj.Id = item.Id;
                    obj.BillDate = string.Format("{0:dd/MM/yyyy}", item.CreatedDate);
                    obj.CustomerPhone = item.CustomerPhone;
                    obj.CustomerName = item.CustomerName;
                    obj.CustomerId = item.IdCustomer;
                    obj.ServiceIds = item.ServiceIds;

                    if (item.ImageStatusId != null)
                    {
                        obj.ImageStatusId = item.ImageStatusId;
                    }

                    for (int i = 0; i < ListImg.Length; i++)
                    {
                        if (!(ListImg[i].StartsWith("http")))
                        {
                            ListImg[i] = _Domain + ListImg[i];
                        }
                    }
                    if (ListImg.Count() == 4)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                        obj.Img3 = ListImg[2];
                        obj.Img4 = ListImg[3];
                    }
                    if (ListImg.Count() == 3)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                        obj.Img3 = ListImg[2];
                    }
                    if (ListImg.Count() == 2)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                    }
                    if (ListImg.Count() == 1)
                        obj.Img1 = ListImg[0];

                    obj.SalonName = objSalon.Name;
                    obj.ErrorNote = item.ErrorNote;
                    obj.NoteByStylist = item.NoteByStylist;
                    if (item.ImageChecked1 != null)
                    {
                        obj.Img1 = item.ImageChecked1;
                    }
                    if (item.ImageChecked2 != null)
                    {
                        obj.Img2 = item.ImageChecked2;
                    }
                    if (item.ImageChecked3 != null)
                    {
                        obj.Img3 = item.ImageChecked3;
                    }
                    if (item.ImageChecked4 != null)
                    {
                        obj.Img4 = item.ImageChecked4;
                    }

                    lstBillView.Add(obj);
                }

                return lstBillView;
            }
        }
        #endregion

        #region Get bill theo S4M
        [WebMethod]
        public static List<S4M_Bill> GetBillImage_S4M(string _Date, int _ClassId, int _StylistId, string _imageStatus)
        {
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                DateTime _FilterDate = Convert.ToDateTime(_Date, culture);
                string _FromDate = String.Format("{0:yyyy/MM/dd}", _FilterDate);
                string _ToDate = String.Format("{0:yyyy/MM/dd}", _FilterDate.AddDays(1));

                var sqlCount = @"select count(*) from Stylist4Men_BillCutFree a where a.CreatedTime between '" + _FromDate + "' and '" + _ToDate + "' and a.IsDelete != 1 and a.Images is not null and a.ClassId =" + _ClassId;

                int _TotalBill = db.Database.SqlQuery<int>(sqlCount).First();

                var sql = "";

                if (_StylistId != 0)
                {
                    sql = "select _bill.*, _cus.CustomerName, _cus.CustomerPhone from Stylist4Men_BillCutFree _bill left join Stylist4Men_Customer _cus on _cus.Id = _bill.CustomerId where _bill.CreatedTime between '" + _FromDate + "' and '" + _ToDate + "' and _bill.IsDelete != 1 and _bill.Images is not null and _bill.ClassId = " + _ClassId + " and _bill.StudentId = " + _StylistId;
                }
                else
                {
                    sql = "select _bill.*, _cus.CustomerName, _cus.CustomerPhone from Stylist4Men_BillCutFree _bill left join Stylist4Men_Customer _cus on _cus.Id = _bill.CustomerId where _bill.CreatedTime between '" + _FromDate + "' and '" + _ToDate + "' and _bill.IsDelete != 1 and _bill.Images is not null and _bill.ClassId = " + _ClassId;
                }

                //1-bill không có lỗi cắt || 2-bill có lỗi hình khối || 3-bill lỗi đường cắt  || 4-bill lỗi hình khối chưa liên kết
                //5-bill lỗi ngọn tóc quá dày || 6-bill cạo mai gáy lỗi || 7-bill vuốt sáp lỗi || 8-bill ảnh thiếu/mờ
                //9-bill có ảnh || 10-bill đã đánh giá || 11-bill chưa đánh giá 

                if (_imageStatus == "1" || _imageStatus == "2" || _imageStatus == "3" || _imageStatus == "4" || _imageStatus == "5" || _imageStatus == "6" || _imageStatus == "7" || _imageStatus == "8")
                {
                    sql += " and ImageStatusId like '%" + _imageStatus + "%' ";
                }
                else if (_imageStatus == "10")
                {
                    sql += " and a.ImageStatusIcheckTotalBilld is not null";
                }
                else if (_imageStatus == "11")
                {
                    sql += " and a.ImageStatusId is null";
                }
                var list = db.Database.SqlQuery<S4M_custom_BillService>(sql).ToList();

                var objClass = db.Stylist4Men_Class.Find(_ClassId);

                List<S4M_Bill> lstBillView = new List<S4M_Bill>();
                S4M_Bill obj = new S4M_Bill();
                string[] ListImg;

                string _Domain = ConfigurationManager.AppSettings["DomainKey_PreviewImg"];

                foreach (var item in list)
                {
                    ListImg = item.Images.Split(',');
                    obj = new S4M_Bill();
                    obj.Id = item.Id;
                    obj.NoteByStylist = item.NoteByStylist;
                    obj.BillDate = string.Format("{0:dd/MM/yyyy}", item.CreatedTime);
                    obj.CustomerPhone = item.CustomerPhone;
                    obj.CustomerName = item.CustomerName;
                    obj.CustomerId = item.CustomerId;

                    if (item.ImageStatusId != null)
                    {
                        obj.ImageStatusId = item.ImageStatusId;
                    }

                    for (int i = 0; i < ListImg.Length; i++)
                    {
                        if (!(ListImg[i].StartsWith("http")))
                        {
                            ListImg[i] = _Domain + ListImg[i];
                        }
                    }
                    if (ListImg.Count() == 4)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                        obj.Img3 = ListImg[2];
                        obj.Img4 = ListImg[3];
                    }
                    if (ListImg.Count() == 3)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                        obj.Img3 = ListImg[2];
                    }
                    if (ListImg.Count() == 2)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                    }
                    if (ListImg.Count() == 1)
                        obj.Img1 = ListImg[0];

                    obj.ClassName = objClass.Name;
                    obj.ErrorNote = item.ErrorNote;
                    if (item.ImageChecked1 != null)
                    {
                        obj.Img1 = item.ImageChecked1;
                    }
                    if (item.ImageChecked2 != null)
                    {
                        obj.Img2 = item.ImageChecked2;
                    }
                    if (item.ImageChecked3 != null)
                    {
                        obj.Img3 = item.ImageChecked3;
                    }
                    if (item.ImageChecked4 != null)
                    {
                        obj.Img4 = item.ImageChecked4;
                    }

                    lstBillView.Add(obj);
                }

                return lstBillView;
            }
        }
        #endregion

        [WebMethod]
        public static List<Tbl_Salon> ReturnAllSalon()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Tbl_Salon.Where(a => a.IsDelete == 0 && a.Publish == true && a.IsSalonHoiQuan == true).ToList();
                return lst;
            }
        }

        /// <summary>
        /// Update trạng thái KCS : Lỗi(item)|Đạt
        /// </summary>
        /// <param name="billID"></param>
        /// <param name="statusID"></param>
        /// <param name="_salonId"></param>
        /// <param name="_classId"></param>
        /// <returns></returns>
        [WebMethod]
        public static string updateStatus(int billID, string statusID, int _salonId, int _classId)
        {
            var Message = new Library.Class.cls_message();
            var serialize = new JavaScriptSerializer();
            var error = 0;
            try
            {
                var db = new Solution_30shineEntities();
                if(_salonId > 0)
                {
                    var RECORD = db.BillServices.FirstOrDefault(w => w.Id == billID);
                    if (RECORD != null)
                    {
                        RECORD.ImageStatusId = statusID;
                        RECORD.ModifiedDate = DateTime.Now;
                        db.BillServices.AddOrUpdate(RECORD);
                        error += db.SaveChanges() > 0 ? 0 : 1;
                        if (error == 0)
                        {
                            Message.success = true;
                            Message.message = "Cập nhật thành công.";
                        }
                        else
                        {
                            Message.success = false;
                            Message.message = "Cập nhật thất bại.";
                        }
                    }
                    else
                    {
                        error = 1;
                        Message.success = false;
                        Message.message = "Bản ghi không tồn tại";
                    }
                }
                else if (_classId > 0)
                {
                    var RECORD = db.Stylist4Men_BillCutFree.FirstOrDefault(w => w.Id == billID);
                    if (RECORD != null)
                    {
                        RECORD.ImageStatusId = statusID;
                        RECORD.ModifiedTime = DateTime.Now;
                        db.Stylist4Men_BillCutFree.AddOrUpdate(RECORD);
                        error += db.SaveChanges() > 0 ? 0 : 1;
                        if (error == 0)
                        {
                            Message.success = true;
                            Message.message = "Cập nhật thành công.";
                        }
                        else
                        {
                            Message.success = false;
                            Message.message = "Cập nhật thất bại.";
                        }
                    }
                    else
                    {
                        error = 1;
                        Message.success = false;
                        Message.message = "Bản ghi không tồn tại";
                    }
                }
                
                return serialize.Serialize(Message);
            }
            catch (Exception ex)
            {
                Message.success = false;
                Message.message = ex.Message;
                return serialize.Serialize(Message);
            }
        }

        /// <summary>
        /// Lấy ghi chú check lỗi
        /// </summary>
        /// <param name="billID"></param>
        /// <param name="_salonId"></param>
        /// <param name="_classId"></param>
        /// <returns></returns>
        [WebMethod]
        public static cls_Note GetErrorNoteById(int billID, int _salonId, int _classId)
        {
            cls_Note note = new cls_Note();
            var db = new Solution_30shineEntities();
            if(_salonId > 0)
            {
                var RECORD = db.BillServices.FirstOrDefault(w => w.Id == billID);
                note.NoteByManager = RECORD.ErrorNote;
                note.NoteByStylist = RECORD.NoteByStylist;
            }
            else if (_classId > 0)
            {
                var RECORD = db.Stylist4Men_BillCutFree.FirstOrDefault(w => w.Id == billID);
                note.NoteByManager = RECORD.ErrorNote;
                note.NoteByStylist = RECORD.NoteByStylist;
            }
            return note;
        }

        //[WebMethod]
        //public static List<KCS_FaceType> GetFaceType()
        //{
        //    var db = new Solution_30shineEntities();
        //    var lst = db.KCS_FaceType.Where(a => a.IsDelete != true).ToList();
        //    return lst;
        //}

        #region Lưu ảnh checked của salon
        [WebMethod]
        public static string LoadImage(int bID, int order, string base64)
        {
            var Message = new Library.Class.cls_message();
            var serialize = new JavaScriptSerializer();
            var error = 0;
            BillServiceView obj = new BillServiceView();
            string[] ListImg;
            string _Domain = ConfigurationManager.AppSettings["DomainKey_PreviewImg"];
            try
            {
                string imgPath = "";
                string ImageName = "";
                string ImgRootPath = "";
                byte[] image64 = Convert.FromBase64String(base64);
                Image image;

                using (MemoryStream ms = new MemoryStream(image64))
                {
                    image = Image.FromStream(ms, true);
                    ImgRootPath = "/Resource/Images/CheckImages/";
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath("~" + ImgRootPath)))
                    {
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~" + ImgRootPath));
                    }
                    string dirFullPath = HttpContext.Current.Server.MapPath("~" + ImgRootPath);
                    ImageName = "img_" + DateTime.Now.ToString("ddMMyyhhmmss") + ".png";
                    image.Save(dirFullPath + ImageName, ImageFormat.Png);
                }
                var db = new Solution_30shineEntities();
                var RECORD = db.BillServices.FirstOrDefault(w => w.Id == bID);
                ListImg = RECORD.Images.Split(',');
                //imgPath = "http://api.30shine.com" + ImgRootPath + ImageName;
                imgPath = "http://ql.30shine.com" + ImgRootPath + ImageName;
                if (RECORD != null)
                {
                    for (int i = 0; i < ListImg.Length; i++)
                    {
                        if (!(ListImg[i].StartsWith("http")))
                        {
                            ListImg[i] = _Domain + ListImg[i];
                        }
                    }

                    if (ListImg.Count() == 4)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                        obj.Img3 = ListImg[2];
                        obj.Img4 = ListImg[3];
                    }
                    if (ListImg.Count() == 3)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                        obj.Img3 = ListImg[2];
                    }
                    if (ListImg.Count() == 2)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                    }
                    if (ListImg.Count() == 1)
                    {
                        obj.Img1 = ListImg[0];
                    }

                    if (order == 1)
                    {
                        RECORD.ImageChecked1 = imgPath;
                    }
                    else if (order == 2)
                    {
                        RECORD.ImageChecked2 = imgPath;
                    }
                    else if (order == 3)
                    {
                        RECORD.ImageChecked3 = imgPath;
                    }
                    else if (order == 4)
                    {
                        RECORD.ImageChecked4 = imgPath;
                    }

                    if (RECORD.ImageChecked1 == null && obj.Img1 != null)
                    {
                        RECORD.ImageChecked1 = obj.Img1;
                    }
                    if (RECORD.ImageChecked2 == null && obj.Img2 != null)
                    {
                        RECORD.ImageChecked2 = obj.Img2;
                    }
                    if (RECORD.ImageChecked3 == null && obj.Img3 != null)
                    {
                        RECORD.ImageChecked3 = obj.Img3;
                    }
                    if (RECORD.ImageChecked4 == null && obj.Img4 != null)
                    {
                        RECORD.ImageChecked4 = obj.Img4;
                    }

                    RECORD.ModifiedDate = DateTime.Now;
                    db.BillServices.AddOrUpdate(RECORD);
                    error += db.SaveChanges() > 0 ? 0 : 1;
                    if (error == 0)
                    {
                        Message.success = true;
                        Message.message = "Cập nhật thành công.";
                    }
                    else
                    {
                        Message.success = false;
                        Message.message = "Cập nhật thất bại.";
                    }
                }
                else
                {
                    error = 1;
                    Message.success = false;
                    Message.message = "Bản ghi không tồn tại";
                }
                return serialize.Serialize(Message);
            }
            catch (Exception ex)
            {
                Message.success = false;
                Message.message = ex.Message;
                return serialize.Serialize(Message);
            }
        }
        #endregion


        #region Lưu ảnh checked của S4M
        [WebMethod]
        public static string LoadImage_S4M(int bID, int order, string base64)
        {
            var Message = new Library.Class.cls_message();
            var serialize = new JavaScriptSerializer();
            var error = 0;
            S4M_Bill obj = new S4M_Bill();
            string[] ListImg;
            string _Domain = ConfigurationManager.AppSettings["DomainKey_PreviewImg"];
            try
            {
                string imgPath = "";
                string ImageName = "";
                string ImgRootPath = "";
                byte[] image64 = Convert.FromBase64String(base64);
                Image image;

                using (MemoryStream ms = new MemoryStream(image64))
                {
                    image = Image.FromStream(ms, true);
                    ImgRootPath = "/Resource/Images/S4M_CheckImages/";
                    if (!Directory.Exists(HttpContext.Current.Server.MapPath("~" + ImgRootPath)))
                    {
                        Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~" + ImgRootPath));
                    }
                    string dirFullPath = HttpContext.Current.Server.MapPath("~" + ImgRootPath);
                    ImageName = "img_" + DateTime.Now.ToString("ddMMyyhhmmss") + ".png";
                    image.Save(dirFullPath + ImageName, ImageFormat.Png);
                }
                var db = new Solution_30shineEntities();
                var RECORD = db.Stylist4Men_BillCutFree.FirstOrDefault(w => w.Id == bID);
                ListImg = RECORD.Images.Split(',');
                //imgPath = "http://api.30shine.com" + ImgRootPath + ImageName;
                imgPath = "http://ql.30shine.com" + ImgRootPath + ImageName;
                //imgPath = "http://erp.30shine.net" + ImgRootPath + ImageName;
                if (RECORD != null)
                {
                    for (int i = 0; i < ListImg.Length; i++)
                    {
                        if (!(ListImg[i].StartsWith("http")))
                        {
                            ListImg[i] = _Domain + ListImg[i];
                        }
                    }

                    if (ListImg.Count() == 4)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                        obj.Img3 = ListImg[2];
                        obj.Img4 = ListImg[3];
                    }
                    if (ListImg.Count() == 3)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                        obj.Img3 = ListImg[2];
                    }
                    if (ListImg.Count() == 2)
                    {
                        obj.Img1 = ListImg[0];
                        obj.Img2 = ListImg[1];
                    }
                    if (ListImg.Count() == 1)
                    {
                        obj.Img1 = ListImg[0];
                    }

                    if (order == 1)
                    {
                        RECORD.ImageChecked1 = imgPath;
                    }
                    else if (order == 2)
                    {
                        RECORD.ImageChecked2 = imgPath;
                    }
                    else if (order == 3)
                    {
                        RECORD.ImageChecked3 = imgPath;
                    }
                    else if (order == 4)
                    {
                        RECORD.ImageChecked4 = imgPath;
                    }

                    if (RECORD.ImageChecked1 == null && obj.Img1 != null)
                    {
                        RECORD.ImageChecked1 = obj.Img1;
                    }
                    if (RECORD.ImageChecked2 == null && obj.Img2 != null)
                    {
                        RECORD.ImageChecked2 = obj.Img2;
                    }
                    if (RECORD.ImageChecked3 == null && obj.Img3 != null)
                    {
                        RECORD.ImageChecked3 = obj.Img3;
                    }
                    if (RECORD.ImageChecked4 == null && obj.Img4 != null)
                    {
                        RECORD.ImageChecked4 = obj.Img4;
                    }

                    RECORD.ModifiedTime = DateTime.Now;
                    db.Stylist4Men_BillCutFree.AddOrUpdate(RECORD);
                    error += db.SaveChanges() > 0 ? 0 : 1;
                    if (error == 0)
                    {
                        Message.success = true;
                        Message.message = "Cập nhật thành công.";
                    }
                    else
                    {
                        Message.success = false;
                        Message.message = "Cập nhật thất bại.";
                    }
                }
                else
                {
                    error = 1;
                    Message.success = false;
                    Message.message = "Bản ghi không tồn tại";
                }
                return serialize.Serialize(Message);
            }
            catch (Exception ex)
            {
                Message.success = false;
                Message.message = ex.Message;
                return serialize.Serialize(Message);
            }
        }
        #endregion

        #region Load lại ảnh đã check của salon
        [WebMethod]
        public static string RefreshCheckedImage(int bID, int order)
        {
            string imageOlder = "";
            BillServiceView obj = new BillServiceView();
            string[] ListImg;
            string _Domain = ConfigurationManager.AppSettings["DomainKey_PreviewImg"];
            var db = new Solution_30shineEntities();
            var RECORD = db.BillServices.FirstOrDefault(w => w.Id == bID);
            ListImg = RECORD.Images.Split(',');
            if (RECORD != null)
            {
                for (int i = 0; i < ListImg.Length; i++)
                {
                    if (!(ListImg[i].StartsWith("http")))
                    {
                        ListImg[i] = _Domain + ListImg[i];
                    }
                }
                if (ListImg.Count() == 4)
                {
                    obj.Img1 = ListImg[0];
                    obj.Img2 = ListImg[1];
                    obj.Img3 = ListImg[2];
                    obj.Img4 = ListImg[3];
                }
                if (ListImg.Count() == 3)
                {
                    obj.Img1 = ListImg[0];
                    obj.Img2 = ListImg[1];
                    obj.Img3 = ListImg[2];
                }
                if (ListImg.Count() == 2)
                {
                    obj.Img1 = ListImg[0];
                    obj.Img2 = ListImg[1];
                }
                if (ListImg.Count() == 1)
                {
                    obj.Img1 = ListImg[0];
                }
                if (order == 1 && (obj.Img1 != null || obj.Img1 != string.Empty))
                {
                    imageOlder = obj.Img1;
                }
                else if (order == 2 && (obj.Img2 != null || obj.Img2 != string.Empty))
                {
                    imageOlder = obj.Img2;
                }
                else if (order == 3 && (obj.Img3 != null || obj.Img3 != string.Empty))
                {
                    imageOlder = obj.Img3;
                }
                else if (order == 4 && (obj.Img4 != null || obj.Img4 != string.Empty))
                {
                    imageOlder = obj.Img4;
                }

            }
            return imageOlder;
        }
        #endregion

        #region Load lại ảnh đã check của S4M 
        [WebMethod]
        public static string RefreshCheckedImage_S4M(int bID, int order)
        {
            string imageOlder = "";
            S4M_Bill obj = new S4M_Bill();
            string[] ListImg;
            string _Domain = ConfigurationManager.AppSettings["DomainKey_PreviewImg"];
            var db = new Solution_30shineEntities();
            var RECORD = db.BillServices.FirstOrDefault(w => w.Id == bID);
            ListImg = RECORD.Images.Split(',');
            if (RECORD != null)
            {
                for (int i = 0; i < ListImg.Length; i++)
                {
                    if (!(ListImg[i].StartsWith("http")))
                    {
                        ListImg[i] = _Domain + ListImg[i];
                    }
                }
                if (ListImg.Count() == 4)
                {
                    obj.Img1 = ListImg[0];
                    obj.Img2 = ListImg[1];
                    obj.Img3 = ListImg[2];
                    obj.Img4 = ListImg[3];
                }
                if (ListImg.Count() == 3)
                {
                    obj.Img1 = ListImg[0];
                    obj.Img2 = ListImg[1];
                    obj.Img3 = ListImg[2];
                }
                if (ListImg.Count() == 2)
                {
                    obj.Img1 = ListImg[0];
                    obj.Img2 = ListImg[1];
                }
                if (ListImg.Count() == 1)
                {
                    obj.Img1 = ListImg[0];
                }
                if (order == 1 && (obj.Img1 != null || obj.Img1 != string.Empty))
                {
                    imageOlder = obj.Img1;
                }
                else if (order == 2 && (obj.Img2 != null || obj.Img2 != string.Empty))
                {
                    imageOlder = obj.Img2;
                }
                else if (order == 3 && (obj.Img3 != null || obj.Img3 != string.Empty))
                {
                    imageOlder = obj.Img3;
                }
                else if (order == 4 && (obj.Img4 != null || obj.Img4 != string.Empty))
                {
                    imageOlder = obj.Img4;
                }

            }
            return imageOlder;
        }
        #endregion

        [WebMethod]
        public static List<ErrorCutHair> GetErrorList()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.ErrorCutHairs.Where(a => a.IsDelete == false).ToList();
                return lst;
            }
        }

        /// <summary>
        /// Lấy danh sách kiểu tóc
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static List<Api_HairMode> ReturnAllHairMode()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Api_HairMode.Where(a => a.IsDelete == false && a.Publish == true).ToList();
                return lst;
            }
        }

        /// <summary>
        /// Cập nhật danh sách kiểu tóc
        /// </summary>
        /// <param name="cusHairId"></param>
        /// <param name="hairStyleId"></param>
        /// <param name="CustomerId"></param>
        /// <param name="billID"></param>
        /// <param name="_salonId"></param>
        /// <param name="_classId"></param>
        /// <returns></returns>
        [WebMethod]
        public static string updateCustomer_HairMode_Bill(int cusHairId, int? hairStyleId, int CustomerId, int billID, int _salonId, int _classId)
        {
            var Message = new Library.Class.cls_message();
            var serialize = new JavaScriptSerializer();
            var error = 0;
            var db = new Solution_30shineEntities();
            var RECORD = db.Customer_HairMode_Bill.FirstOrDefault(w => w.Id == cusHairId);
            var obj = new Customer_HairMode_Bill();
            if (RECORD == null)
            {
                obj.BillId = billID;
                obj.CreateDate = DateTime.Now;
                obj.HairStyleId = hairStyleId;
                //obj.FaceTypeId = faceTypeId;
                obj.CustomerId = CustomerId;
                //if(_salonId > 0)
                //{
                //    obj.IsHoiQuan = false;
                //}
                //else if (_classId > 0)
                //{
                //    obj.IsHoiQuan = true;
                //}
                db.Customer_HairMode_Bill.Add(obj);
                error += db.SaveChanges() > 0 ? 0 : 1;
                if (error == 0)
                {
                    Message.success = true;
                    Message.message = "Thêm thành công.";
                }
                else
                {
                    Message.success = false;
                    Message.message = "Thêm thất bại.";
                }
            }
            else
            {
                RECORD.BillId = billID;
                if(hairStyleId != null)
                {
                    RECORD.HairStyleId = hairStyleId;
                }
                //if(faceTypeId != null)
                //{
                //    RECORD.FaceTypeId = faceTypeId;
                //}
                
                RECORD.CustomerId = CustomerId;
                RECORD.ModifiledDate = DateTime.Now;
                db.Customer_HairMode_Bill.AddOrUpdate(RECORD);
                error += db.SaveChanges() > 0 ? 0 : 1;
                if (error == 0)
                {
                    Message.success = true;
                    Message.message = "Cập nhật thành công.";
                }
                else
                {
                    Message.success = false;
                    Message.message = "Cập nhật thất bại.";
                }
            }
            return serialize.Serialize(Message);
            //}
            //catch (Exception ex)
            //{
            //    Message.success = false;
            //    Message.message = ex.Message;
            //    return serialize.Serialize(Message);
            //}
        }
        #region Tính tỉ lệ stylist chụp đủ ảnh
        [WebMethod]
        public static string checkStylistFullImage(int stylistID, DateTime date)
        {
            //return date.ToString();
            try
            {
                Solution_30shineEntities db = new Solution_30shineEntities();
                //var bill = (from bs in db.BillServices where bs.Staff_HairMassage_Id == stylistID && bs.CreatedDate.)
                int bill = db.BillServices.Where(bs => bs.CreatedDate.Value.Day == date.Day
                        && bs.CreatedDate.Value.Month == date.Month && bs.CreatedDate.Value.Year == date.Year
                        && bs.Staff_Hairdresser_Id == stylistID
                        && bs.IsDelete != 1 && bs.Pending != 0
                        ).Count();
                int bill2 = db.BillServices.Where(bs => bs.CreatedDate.Value.Day == date.Day
                        && bs.CreatedDate.Value.Month == date.Month && bs.CreatedDate.Value.Year == date.Year
                        && bs.Staff_Hairdresser_Id == stylistID
                        && bs.IsDelete != 1 && bs.Pending != 0 && bs.Images != null
                        ).Count();
                if (bill2 != 0)
                {
                    float tile = bill / bill2;
                    if (tile >= 0.8)
                    {
                        return "Success";
                    }
                    else
                    {
                        return "Errors";
                    }
                }
                else
                    return "Success";
            }
            catch (Exception ex)
            {

                return ex.ToString();
            }

        }
        #endregion
        /// <summary>
        /// Lấy kiểu tóc theo BillID
        /// </summary>
        /// <param name="billId"></param>
        /// <param name="_salonId"></param>
        /// <param name="_classId"></param>
        /// <returns></returns>
        [WebMethod]
        public static Customer_HairMode_Bill getHairStyleById(int billId, int _salonId, int _classId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = new Customer_HairMode_Bill();
                if (_salonId > 0)
                {
                    lst = db.Customer_HairMode_Bill.Where(a => a.BillId == billId && (a.IsDelete == false || a.IsDelete == null)).SingleOrDefault();
                }
                else if (_classId > 0)
                {
                    lst = db.Customer_HairMode_Bill.Where(a => a.BillId == billId && (a.IsDelete == false || a.IsDelete == null)).SingleOrDefault();
                }
                return lst;
            }
        }

        /// <summary>
        /// Cập nhật ghi chú lỗi cắt
        /// </summary>
        /// <param name="billID"></param>
        /// <param name="errorNote"></param>
        /// <param name="noteByStylist"></param>
        /// <param name="_salonId"></param>
        /// <param name="_classId"></param>
        /// <returns></returns>
        [WebMethod]
        public static string updateErrorNote(int billID, string errorNote, string noteByStylist, int _salonId, int _classId)
        {
            var Message = new Library.Class.cls_message();
            var serialize = new JavaScriptSerializer();
            var error = 0;
            try
            {
                var db = new Solution_30shineEntities();
                if(_salonId > 0)
                {
                    var RECORD = db.BillServices.FirstOrDefault(w => w.Id == billID);
                    if (RECORD != null)
                    {
                        RECORD.ErrorNote = errorNote;
                        //RECORD.NoteByStylist = noteByStylist;
                        RECORD.ModifiedDate = DateTime.Now;
                        db.BillServices.AddOrUpdate(RECORD);
                        error += db.SaveChanges() > 0 ? 0 : 1;
                        if (error == 0)
                        {
                            Message.success = true;
                            Message.message = "Cập nhật thành công.";
                        }
                        else
                        {
                            Message.success = false;
                            Message.message = "Cập nhật thất bại.";
                        }
                    }
                    else
                    {
                        error = 1;
                        Message.success = false;
                        Message.message = "Bản ghi không tồn tại";
                    }
                    //return serialize.Serialize(Message);
                }
                else if (_classId > 0)
                {
                    var RECORD = db.Stylist4Men_BillCutFree.FirstOrDefault(w => w.Id == billID);
                    if (RECORD != null)
                    {
                        RECORD.ErrorNote = errorNote;
                        //RECORD.NoteByStylist = noteByStylist;
                        RECORD.ModifiedTime = DateTime.Now;
                        db.Stylist4Men_BillCutFree.AddOrUpdate(RECORD);
                        error += db.SaveChanges() > 0 ? 0 : 1;
                        if (error == 0)
                        {
                            Message.success = true;
                            Message.message = "Cập nhật thành công.";
                        }
                        else
                        {
                            Message.success = false;
                            Message.message = "Cập nhật thất bại.";
                        }
                    }
                    else
                    {
                        error = 1;
                        Message.success = false;
                        Message.message = "Bản ghi không tồn tại";
                    }
                }
                return serialize.Serialize(Message);

            }
            catch (Exception ex)
            {
                Message.success = false;
                Message.message = ex.Message;
                return serialize.Serialize(Message);
            }
        }
        public class S4M_Bill
        {
            public int Id { get; set; }
            public string BillDate { get; set; }
            public string ClassName { get; set; }
            public string CustomerPhone { get; set; }
            public string CustomerName { get; set; }
            public string Img1 { get; set; }
            public string Img2 { get; set; }
            public string Img3 { get; set; }
            public string Img4 { get; set; }
            public string ImageStatusId { get; set; }
            public string ErrorNote { get; set; }
            public string NoteByStylist { get; set; }
            public string Student { get; set; }
            public int? CustomerId { get; set; }
        }
        public class S4M_custom_BillService : Stylist4Men_BillCutFree
        {
            public string CustomerPhone { get; set; }
            public string CustomerName { get; set; }
        }
    }
    public class custom_BillService : BillService
    {
        public string CustomerPhone { get; set; }
        public string CustomerName { get; set; }
        public int IdCustomer { get; set; }
    }
    public class cls_Note
    {
        public string NoteByManager { get; set; }
        public string NoteByStylist { get; set; }
    }
}

