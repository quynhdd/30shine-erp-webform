﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Globalization;
using _30shine.Helpers;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Stylist4Men
{
    public partial class Statistics_SCSC_S4M : System.Web.UI.Page
    {
        public string PageID = "S4M_Export";
        protected string Permission = "";
        protected bool Perm_ViewAllDataBySalon = false;
        private bool Perm_Access = false;

        public CultureInfo culture = new CultureInfo("vi-VN");
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = DateTime.Now.ToString("dd/MM/yyyy");
                Library.Function.bindSalonHoiQuan(new List<DropDownList> { ddlSalon }, Perm_ViewAllDataBySalon);
                Bind_Staff();
            }
        }

        protected void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var msgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", msgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ViewAllDataBySalon = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDataBySalon", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllDataBySalon = permissionModel.CheckPermisionByAction("Perm_ViewAllDataBySalon", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        protected void Salon_SelectedIndexChanged(object sender, EventArgs e)
        {
            Bind_Staff();
        }
        /// <summary>
        /// Bind Staff
        /// </summary>
        private void Bind_Staff()
        {
            using (var db = new Solution_30shineEntities())
            {
                var salon = Convert.ToInt32(ddlSalon.SelectedValue);
                var staff = new List<Staff>();
                var whereSalon = "";
                var sql = "";

                if (salon > 0)
                {
                    whereSalon = " and SalonId = " + salon;
                }
                sql = @"select * from Staff 
                        where IsDelete != 1 and Active = 1  and Type = 1 and (IsHoiQuan = 0 or IsHoiQuan is null)
                        and (isAccountLogin != 1 or isAccountLogin is null)" + whereSalon;
                staff = db.Staffs.SqlQuery(sql).ToList();

                var first = new Staff();
                first.Id = 0;
                first.Fullname = "Chọn nhân viên";
                staff.Insert(0, first);

                ddlStylist.DataTextField = "Fullname";
                ddlStylist.DataValueField = "Id";
                ddlStylist.DataSource = staff;
                ddlStylist.DataBind();
            }
        }
        /// <summary>
        /// Bind data all salon
        /// </summary>
        private void BindAllSalon()
        {
            using (var db = new Solution_30shineEntities())
            {
                if (TxtDateTimeFrom.Text != "")
                {
                    DateTime _FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                    DateTime _ToDate;
                    if (TxtDateTimeTo.Text != "")
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                    }
                    else
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                    }
                    var list = db.SCSC_TotalAllSalon_Store_S4M_V2(_FromDate, _ToDate).ToList();
                    var item = new SCSC_TotalAllSalon_Store_S4M_V2_Result();
                    var countList = list.Count;
                    item.ShortName = "Tổng";
                    item.TongBill = list.Sum(a => a.TongBill);
                    item.Bill_NotImg = Math.Round((Double)(list.Sum(a => a.Bill_NotImg)) / countList, 2);
                    item.BillChuaDanhGia = list.Sum(a => a.BillChuaDanhGia);
                    item.Percent_BillImg_MoLech = Math.Round((Double)(list.Sum(a => a.Percent_BillImg_MoLech)) / countList, 2);
                    item.PointSCSC_TB = Math.Round((Double)(list.Sum(a => a.PointSCSC_TB)) / countList, 2);
                    item.Percent_BillError = Math.Round((Double)(list.Sum(a => a.Percent_BillError)) / countList, 2);
                    item.Percent_ErrorKCS = Math.Round((Double)(list.Sum(a => a.Percent_ErrorKCS)) / countList, 2);
                    item.TotalShapeError = list.Sum(a => a.TotalShapeError);
                    item.TotalSharpNessError = list.Sum(a => a.TotalSharpNessError);
                    item.TotalConnectiveError = list.Sum(a => a.TotalConnectiveError);
                    item.TotalCompletionError = list.Sum(a => a.TotalCompletionError);
                    list.Insert(0, item);
                    rptAllSalon.DataSource = list;
                    rptAllSalon.DataBind();
                }
            }
        }

        /// <summary>
        /// Bind data theo từng salon
        /// </summary>
        private void BindOnlySalon()
        {
            using (var db = new Solution_30shineEntities())
            {
                if (TxtDateTimeFrom.Text != "")
                {
                    DateTime _FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                    DateTime _ToDate;
                    if (TxtDateTimeTo.Text != "")
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                    }
                    else
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                    }
                    int SalonId = Convert.ToInt32(ddlSalon.SelectedValue);
                    int StylistId = Convert.ToInt32(ddlStylist.SelectedValue);
                    var list = db.SCSC_BindStaffWhereSalon_S4M_Store_V2(_FromDate, _ToDate, SalonId, StylistId).ToList();
                    var item = new SCSC_BindStaffWhereSalon_S4M_Store_V2_Result();
                    var countList = list.Count;
                    item.Fullname = "Tổng";
                    item.TotalBill = list.Sum(a => a.TotalBill);
                    item.Bill_NotImg = Math.Round((Double)(list.Sum(a => a.Bill_NotImg)) / countList, 2);
                    item.TotalBillChuaDanhGia = list.Sum(a => a.TotalBillChuaDanhGia);
                    item.Percent_BillError = Math.Round((Double)(list.Sum(a => a.Percent_BillError)) / countList, 2);
                    item.Percent_BillImg_MoLech = Math.Round((Double)(list.Sum(a => a.Percent_BillImg_MoLech)) / countList, 2);
                    item.PointSCSC_TB = Math.Round((Double)(list.Sum(a => a.PointSCSC_TB)) / countList, 2);
                    item.Total_SCSC_Error = Math.Round((Double)(list.Sum(a => a.Total_SCSC_Error)) / countList, 2);
                    item.TotalBillErrorShape = list.Sum(a => a.TotalBillErrorShape);
                    item.TotalBillErrorSharpNess = list.Sum(a => a.TotalBillErrorSharpNess);
                    item.TotalBillErrorConnective = list.Sum(a => a.TotalBillErrorConnective);
                    item.TotalBillErrorCompletion = list.Sum(a => a.TotalBillErrorCompletion);
                    list.Insert(0, item);
                    rptOnlySalon.DataSource = list;
                    rptOnlySalon.DataBind();
                }
            }
        }
        /// <summary>
        /// Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _BtnClick(object sender, EventArgs e)
        {
            int SalonId = Convert.ToInt32(ddlSalon.SelectedValue);
            if (SalonId == 0)
            {
                BindAllSalon();
                rptOnlySalon.DataSource = "";
                rptOnlySalon.DataBind();
            }
            else
            {
                BindOnlySalon();
                rptAllSalon.DataSource = "";
                rptAllSalon.DataBind();

            }
            RemoveLoading();
        }
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "salonmanager" };
                string[] AllowSalon = new string[] { "root", "admin" };
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }
                if (Array.IndexOf(AllowSalon, Permission) != -1)
                {
                    Perm_ViewAllDataBySalon = true;
                }

            }
            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
    }
}