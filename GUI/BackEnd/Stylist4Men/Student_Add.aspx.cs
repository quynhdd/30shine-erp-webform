﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Globalization;
using System.Net.Mail;
using System.Text.RegularExpressions;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Stylist4Men
{
    public partial class Student_Add : System.Web.UI.Page
    {
        private string PageID = "";
        private static Solution_30shineEntities db;
        public Student_Add()
        {
            db = new Solution_30shineEntities();
        }
        protected Staff OBJ;
        protected string _Code;
        protected bool _IsUpdate = false;
        private bool Perm_Access = false;
        protected bool Perm_ViewAllData = false;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                if (Request.QueryString["Code"] != null)
                {
                    PageID = "S4M_HV_EDIT";
                }
                else
                {
                    PageID = "S4M_HV_ADD";
                }
                IPermissionModel permissionModel = new PermissionModel();
                var permission = Session["User_Permission"].ToString();
                Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                ////Perm_Add = permissionModel.GetActionByActionNameAndPageId("Perm_Add", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        private CultureInfo culture = new CultureInfo("vi-VN");

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                Library.Function.bind_S4M_Class(new List<DropDownList>() { Class });
                Library.Function.bindSalonHoiQuan(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
                Library.Function.Bind_SkillLevel(new List<DropDownList> { ddlSkillLevel });                
                bindTinhThanh();
                bindQuanHuyen();
                if (IsUpdate())
                {
                    if (Bind_OBJ())
                    {
                        //
                    }
                }
                else
                {
                    //
                }

            }
        }

        protected void ExcAddOrUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
            {
                Update();
            }
            else
            {
                Add();
            }
        }
        //Add Student
        #region
        private void Add()
        {
            int integer;
            var obj = new Staff();
            obj.Fullname = Name.Text;
            if (obj.Fullname == "")
            {
                var msg = "Bạn chưa nhập tên học viên";
                showInputFormMsg(msg, "error");
                return;
            }
            obj.Phone = Phone.Text;
            //if (obj.Phone == "" || checkPhone(obj.Phone) == false)
            //{
            //    var msg = "Nhập số điện thoại chỉ có 10 hoặc 11 số";
            //    showInputFormMsg(msg, "error");
            //    return;
            //}
            obj.Email = Email.Text;
            //if (obj.Email == "" || checkMail(obj.Email) == false || IsValidMail(obj.Email) == false)
            //{
            //    var msg = "Nhập đúng định dạng email";
            //    showInputFormMsg(msg, "error");
            //    return;
            //}
            obj.Password = "a05b49b7dc71773bc7ff4ef877824ffe";
            try
            {
                obj.S4MClassId = int.TryParse(Class.SelectedValue, out integer) ? integer : 0;
                //if (obj.S4MClassId == 0)
                //{
                //    var msg = "Bạn chưa chọn lớp cho học viên";
                //    showInputFormMsg(msg, "error");
                //    return;
                //}
                int salonId = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                obj.SalonId = salonId;
                if (salonId > 0)
                {
                    // Học viên trở thành Stylist Hội Quán
                    obj.Type = 1;
                }
                else
                {
                    // Học viên S4M
                    obj.Type = 7;
                }
                //if ( obj.SalonId == 0)
                //{
                //    var msg = "Bạn chưa chọn salon cho học viên";
                //    showInputFormMsg(msg, "error");
                //    return;
                //}
                //obj.SN_day = Convert.ToInt32(Day.Text);
                //obj.SN_month = Convert.ToInt32(Month.Text);
                //obj.SN_year = Convert.ToInt32(Year.Text);
                obj.SkillLevel = int.TryParse(ddlSkillLevel.SelectedValue, out integer) ? integer : 0;
                obj.Gender = Convert.ToByte(Gender.SelectedValue);
                obj.CityId = Convert.ToInt32(City.SelectedValue);
                obj.DistrictId = Convert.ToInt32(District.SelectedValue);
                obj.Address = Address.Text;
                //obj.IsHoiQuan = true;
                obj.Permission = "staff";
                obj.Active = 1;
                obj.RequireEnroll = true;
            }
            catch { }
            obj.Publish = Publish.Checked;
            obj.IsDelete = 0;
            obj.CreatedDate = DateTime.Now;
            //obj.CreatedTime = DateTime.Now;
            //obj.IsDelete = false;

            // Validate
            var Error = false;

            if (!Error)
            {
                db.Staffs.Add(obj);
                var exc = db.SaveChanges();

                obj.OrderCode = obj.Id.ToString();
                db.Staffs.AddOrUpdate(obj);

                if (exc > 0)
                {
                    var MsgParam = new List<KeyValuePair<string, string>>();
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                    UIHelpers.Redirect("/admin/s4m/hoc-vien/" + obj.Id + ".html", MsgParam);
                }
                else
                {
                    var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }
            }

        }
        #endregion
        //Update Student
        #region
        private void Update()
        {
            using (var db = new Solution_30shineEntities())
            {
                int Id = 0;
                int integer;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Staffs.FirstOrDefault(w => w.Id == Id);

                    if (!OBJ.Equals(null))
                    {
                        OBJ.Fullname = Name.Text;
                        if (OBJ.Fullname == " ")
                        {
                            var msg = "Bạn chưa nhập tên học viên";
                            showInputFormMsg(msg, "error");
                            return;
                        }
                        OBJ.Phone = Phone.Text;
                        OBJ.Email = Email.Text;
                        //OBJ.Note = Note.Text;
                        //OBJ.Money = int.TryParse(Money.Text, out integer) ? integer : 0;
                        try
                        {
                            OBJ.S4MClassId = int.TryParse(Class.SelectedValue, out integer) ? integer : 0;
                            //if (OBJ.S4MClassId == 0)
                            //{
                            //    var msg = "Bạn chưa chọn lớp cho học viên";
                            //    showInputFormMsg(msg, "error");
                            //    return;
                            //}
                            int salonId = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                            OBJ.SalonId = salonId;
                            if (salonId > 0)
                            {
                                // Học viên trở thành Stylist Hội Quán
                                OBJ.Type = 1;
                            }
                            else
                            {
                                // Học viên S4M
                                OBJ.Type = 7;
                            }
                            //OBJ.SN_day = int.TryParse(Day.Text, out integer) ? integer : 0;
                            //OBJ.SN_month = Convert.ToInt32(Month.Text);
                            //OBJ.SN_year = Convert.ToInt32(Year.Text);
                            OBJ.SkillLevel = int.TryParse(ddlSkillLevel.SelectedValue, out integer) ? integer : 0;
                            OBJ.Gender = Convert.ToByte(Gender.SelectedValue);
                            OBJ.CityId = Convert.ToInt32(City.SelectedValue);
                            OBJ.DistrictId = Convert.ToInt32(District.SelectedValue);
                            OBJ.Address = Address.Text;                            
                            //OBJ.IsHoiQuan = true;
                            //OBJ.IsGraduate = Convert.ToBoolean(IsGraduate.Checked);
                            //OBJ.IsHire = Convert.ToBoolean(IsHire.Checked);
                        }
                        catch { }
                        OBJ.Publish = Publish.Checked;
                        OBJ.ModifiedDate = DateTime.Now;

                        db.Staffs.AddOrUpdate(OBJ);
                        var exc = db.SaveChanges();
                        if (exc > 0)
                        {
                            var MsgParam = new List<KeyValuePair<string, string>>();
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                            UIHelpers.Redirect("/admin/s4m/hoc-vien/" + OBJ.Id + ".html", MsgParam);
                        }
                        else
                        {
                            var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                            var status = "warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                        }
                    }
                    else
                    {
                        var msg = "Lỗi! Bản ghi không tồn tại.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);

                    }
                }
                else
                {
                    MsgSystem.Text = "Lỗi! Bản ghi không tồn tại.";
                    MsgSystem.CssClass = "msg-system warning";
                }

                Context.RewritePath("+", "", "");
            }
        }
        #endregion

        /// <summary>
        /// Bind du lieu ban ghi
        /// </summary>
        /// <returns></returns>
        private bool Bind_OBJ()
        {
            using (var db = new Solution_30shineEntities())
            {
                var ExistOBJ = true;
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Staffs.Where(w => w.Id == Id).FirstOrDefault();

                    if (!OBJ.Equals(null))
                    {
                        Name.Text = OBJ.Fullname;
                        Phone.Text = OBJ.Phone;
                        Email.Text = OBJ.Email;
                        //Note.Text = OBJ.Note;

                        //try
                        //{
                        //    Day.Text = OBJ.SN_day.ToString();
                        //    Month.Text = OBJ.SN_month.ToString();
                        //    Year.Text = OBJ.SN_year.ToString();
                        //}
                        //catch { }

                        // Lớp học
                        var itemSelected = Class.Items.FindByValue(OBJ.S4MClassId.ToString());
                        if (itemSelected != null)
                        {
                            itemSelected.Selected = true;
                        }

                        // Salon
                        itemSelected = ddlSalon.Items.FindByValue(OBJ.SalonId.ToString());
                        if (itemSelected != null)
                        {
                            itemSelected.Selected = true;
                        }

                        // Bậc kỹ năng
                        itemSelected = ddlSkillLevel.Items.FindByValue(OBJ.SkillLevel.ToString());
                        if (itemSelected != null)
                        {
                            itemSelected.Selected = true;
                        }

                        // Giới tính : Gender
                        itemSelected = Gender.Items.FindByValue(OBJ.Gender.ToString());
                        if (itemSelected != null)
                        {
                            itemSelected.Selected = true;
                        }

                        // Thành phố : City
                        itemSelected = City.Items.FindByValue(OBJ.CityId.ToString());
                        if (itemSelected != null)
                        {
                            City.ClearSelection();
                            itemSelected.Selected = true;
                        }

                        // Quận huyện : District
                        bindQuanHuyen();
                        itemSelected = District.Items.FindByValue(OBJ.DistrictId.ToString());
                        if (itemSelected != null)
                        {
                            District.ClearSelection();
                            itemSelected.Selected = true;
                        }

                        //if (OBJ.IsGraduate == true)
                        //{
                        IsGraduate.Checked = false;
                        //}
                        //if (OBJ.IsHire == true)
                        //{
                        IsHire.Checked = false;
                        //}
                        if (OBJ.Publish == true)
                        {
                            Publish.Checked = true;
                        }
                        else
                        {
                            Publish.Checked = false;
                        }

                    }
                    else
                    {
                        ExistOBJ = false;
                        var msg = "Lỗi! Bản ghi không tồn tại";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
                else
                {
                    ExistOBJ = false;
                    var msg = "Lỗi! Bản ghi không tồn tại";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }

                return ExistOBJ;
            }
        }

        

        /// <summary>
        /// Bind tỉnh thành
        /// </summary>
        protected void bindTinhThanh()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.TinhThanhs.OrderBy(p => p.ThuTu).ToList();

                City.DataTextField = "TenTinhThanh";
                City.DataValueField = "Id";
                //City.SelectedIndex = 0;

                City.DataSource = lst;
                City.DataBind();
            }
        }

        /// <summary>
        /// Bind quận huyện
        /// </summary>
        protected void bindQuanHuyen()
        {
            using (var db = new Solution_30shineEntities())
            {
                int _TinhThanhID = Convert.ToInt32(City.SelectedValue);
                if (_TinhThanhID != 0)
                {
                    var lst = db.QuanHuyens.Where(p => p.TinhThanhID == _TinhThanhID).OrderBy(p => p.ThuTu).ToList();

                    District.DataTextField = "TenQuanHuyen";
                    District.DataValueField = "Id";
                    //District.SelectedIndex = 0;

                    District.DataSource = lst;
                    District.DataBind();
                }
            }
        }

        /// <summary>
        /// Update panel quận huyện theo thành phố
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlFTinhThanh_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindQuanHuyen();
        }

        public void ReloadPage()
        {
            HttpRequest Request = HttpContext.Current.Request;
            string host = "http://" + Request.Url.Host.ToString();
            string rawUrl = Request.RawUrl.ToString();
            string Url = host + rawUrl;
            Response.Redirect(Url);
        }

        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }

        
        private void showInputFormMsg(string msg, string status)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Thông báo lỗi", "swal('', '" + msg + "', '" + status + "')", true);
        }
        public Boolean checkPhone(string phone)
        {
            string res = "";
            res = phone.Substring(0, 1);
            if ((phone.Length < 10 || phone.Length > 11) || res.Equals('0') || checknumber(phone) == false)
            {
                return false;
            }

            return true;
        }
        public bool checknumber(string str)
        {
            foreach (Char c in str)
            {
                if (Char.IsDigit(c))
                    return true;
            }
            return false;

        }
        public bool IsValidMail(string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);

                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
        public bool checkMail(string emailAddress)
        {
            //string patternStrict = @"^( [a-zA-Z0-9_\.\-\+\@ ])$";
            //Regex reStrict = new Regex(patternStrict);
            //bool isStrictMatch = reStrict.IsMatch(emailAddress);
            //return isStrictMatch;
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
               @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
               @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Regex re = new Regex(strRegex);
            if (re.IsMatch(emailAddress))
                return (true);
            else
                return (false);
        }

    }
}