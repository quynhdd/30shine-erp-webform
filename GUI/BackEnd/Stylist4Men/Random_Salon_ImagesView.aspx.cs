﻿using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.Stylist4Men
{
    public partial class Random_Salon_ImagesView : System.Web.UI.Page
    {
        public CultureInfo culture = new CultureInfo("vi-VN");
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = DateTime.Now.ToString("dd/MM/yyyy");
            }
        }

        /// <summary>
        /// Lấy danh sách lớp học
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static List<Stylist4Men_Class> ReturnAllClass()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Stylist4Men_Class.Where(a => a.IsDelete == false && a.Publish == true).ToList();
                return lst;
            }
        }

        /// <summary>
        /// Lấy danh sách học viên
        /// </summary>
        /// <param name="_ClassId"></param>
        /// <returns></returns>
        [WebMethod]
        public static List<Staff> ReturnAllStudentByClass(int _ClassId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Staffs.Where(a => a.IsDelete == 0 && a.Active == 1 && a.S4MClassId == _ClassId).ToList();
                return lst;
            }
        }

        /// <summary>
        /// Báo cáo dữ liệu ảnh
        /// </summary>
        /// <param name="timeFrom"></param>
        /// <param name="timeTo"></param>
        /// <param name="classId"></param>
        /// <param name="studentId"></param>
        /// <returns></returns>
        [WebMethod]
        public static List<ReportBySalon> ImageReport(DateTime timeFrom, DateTime timeTo, int? classId, int? studentId)
        {
            var db = new Solution_30shineEntities();

            List<ReportBySalon> list = new List<ReportBySalon>();

            List<Stylist4Men_Class> listClass = db.Stylist4Men_Class.Where(w => w.IsDelete != true && w.Publish == true).ToList();
            List<Staff> listStudent = db.Staffs.Where(a => a.IsDelete == 0 && a.Active == 1 && a.S4MClassId == classId).ToList();
            List<PreviewImages_Report> listReport = db.PreviewImages_Report.Where(w => w.IsDelete != true).ToList();
            if (classId == null)
            {
                foreach (var item in listClass)
                {
                    object[] paras = {
                            new SqlParameter("@timeFrom",timeFrom),
                            new SqlParameter("@timeTo", timeTo.AddDays(1)),
                            new SqlParameter("@classId", item.Id),
                            new SqlParameter("@studentId", Convert.DBNull),
                    };
                    var report = db.Database.SqlQuery<ReportBySalon>("Store_Stylist4Men_CheckErrorHairCutReport @timeFrom, @timeTo, @classId, @studentId", paras).SingleOrDefault();

                    report.Id = item.Id;
                    report.Name = item.Name;
                    list.Add(report);
                }
            }
            else
            {
                if (studentId == null)
                {
                    foreach (var item in listStudent)
                    {
                        object[] paras = {
                            new SqlParameter("@timeFrom",timeFrom),
                            new SqlParameter("@timeTo", timeTo.AddDays(1)),
                            new SqlParameter("@classId", classId),
                            new SqlParameter("@studentId", item.Id),
                    };
                        var report = db.Database.SqlQuery<ReportBySalon>("Store_Stylist4Men_CheckErrorHairCutReport @timeFrom, @timeTo, @classId, @studentId", paras).SingleOrDefault();
                        report.Id = item.Id;
                        report.Name = item.Fullname;
                        list.Add(report);
                    }
                }
                else
                {
                    var stl = db.Staffs.Where(a => a.Id == studentId).SingleOrDefault();
                    object[] paras = {
                            new SqlParameter("@timeFrom",timeFrom),
                            new SqlParameter("@timeTo", timeTo.AddDays(1)),
                            new SqlParameter("@classId", classId),
                            new SqlParameter("@studentId", studentId)
                    };
                    var report = db.Database.SqlQuery<ReportBySalon>("Store_Stylist4Men_CheckErrorHairCutReport @timeFrom, @timeTo, @classId, @studentId", paras).SingleOrDefault();
                    if (report != null && stl != null)
                    {
                        report.Id = studentId;
                        report.Name = stl.Fullname;
                        list.Add(report);
                    }                    
                }

            }
            return list;
        }

        public class ThongKeAnh
        {
            public int? SoLanOK { get; set; }
            public int? HinhKhoiChuaChuan { get; set; }
            public int? DuongCatChuaNet { get; set; }
            public int? HinhKhoiChuaLienKet { get; set; }
            public int? NgonTocQuaDay { get; set; }
            public int? CaoMaiGayLoi { get; set; }
            public int? VuotSapLoi { get; set; }
            public int? AnhThieuMo { get; set; }
            public int? TongBillCoAnh { get; set; }
            public int? TongBillDaDanhGia { get; set; }
            public int? TongBillChuaDanhGia { get; set; }
            public int? TongBillChuaCoAnh { get; set; }
            public int? TongBill { get; set; }
        }

        public class ReportBySalon : ThongKeAnh
        {
            public int? Id { get; set; }
            public string Name { get; set; }
        }
    }
}