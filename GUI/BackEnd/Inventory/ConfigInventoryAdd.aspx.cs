﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Globalization;
using LinqKit;
using Project.Helpers;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.UI;
using _30shine.Helpers.Http;
using Libraries;
using System.Web.Services;

namespace _30shine.GUI.BackEnd.Inventory
{
    public partial class ConfigInventoryAdd : System.Web.UI.Page
    {
        private bool Perm_Access = false;
        private bool Perm_ViewAllData = false;
        protected int Id;
        protected static string ListProduct;
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {

                Id = getId();
                BindOBJ();
                ListProduct = getProducts();
                getServices();
                getDepartments();
            }
        }

        /// <summary>
        /// get Id của config
        /// </summary>
        /// <returns></returns>
        public int getId()
        {
            return int.TryParse(Request.QueryString["Id"], out var integer) ? integer : 0;
        }

        private void Add()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    int integer;
                    string productIds = ""; ;
                    var MsgParam = new List<KeyValuePair<string, string>>();
                    var serialize = new JavaScriptSerializer();
                    var record = new Origin();
                    record.DepartmentId = int.TryParse(ddlDepartment.SelectedValue, out integer) ? integer : 0;
                    record.ServiceId = int.TryParse(ddlService.SelectedValue, out integer) ? integer : 0;
                    record.TotalNumberService = int.TryParse(txtTotalNumberService.Text, out integer) ? integer : 0;
                    productIds = HDF_ProductIds.Value != "[]" ? HDF_ProductIds.Value : "";
                    if (!String.IsNullOrEmpty(productIds))
                    {
                        record.listProduct = serialize.Deserialize<List<ProductList>>(productIds).ToList();
                        var result = new HttpClient().PostAsJsonAsync(Libraries.AppConstants.URL_API_INVENTORY + "/api/config-quantify-product", record).Result;
                        if (result.IsSuccessStatusCode)
                        {
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Thêm mới thành công!"));
                            UIHelpers.Redirect("/admin/cau-hinh-dinh-luong/danh-sach.html", MsgParam);
                        }
                        else
                        {
                            UIHelpers.TriggerJsMsgSystem_v1(this, "Thêm mới thất bại. (" + result.Content.ReadAsStringAsync().Result + ") ", "msg-system warning", 3000);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                UIHelpers.TriggerJsMsgSystem_v1(this, "Đã xảy ra lỗi: " + ex.Message.Replace("'", "\'"), "msg-system warning", 7000);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        private void Update(int Id)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    if (Id > 0)
                    {
                        string productIds = ""; ;
                        var serialize = new JavaScriptSerializer();
                        var origin = new Origin();
                        var record = db.ConfigQuantifyProducts.FirstOrDefault(w => w.Id == Id);
                        if (record != null)
                        {
                            origin.DepartmentId = record.DepartmentId ?? 0;
                            origin.ServiceId = record.ServiceId ?? 0;
                            origin.TotalNumberService = int.TryParse(txtTotalNumberService.Text, out var integer) ? integer : 0;
                            productIds = HDF_ProductIds.Value != "[]" ? HDF_ProductIds.Value : "";
                            if (!String.IsNullOrEmpty(productIds))
                            {
                                origin.listProduct = serialize.Deserialize<List<ProductList>>(productIds).ToList();
                                var result = new Request().RunPutAsync(Libraries.AppConstants.URL_API_INVENTORY + "/api/config-quantify-product", origin).Result;
                                if (result.IsSuccessStatusCode)
                                {
                                    UIHelpers.TriggerJsMsgSystem_v1(this, "Cập nhật thành công.", "msg-system success", 3000);
                                    BindOBJ();
                                }
                                else
                                {
                                    UIHelpers.TriggerJsMsgSystem_v1(this, "Cập nhật thất bại. (" + result.Content.ReadAsStringAsync().Result + ") ", "msg-system warning", 3000);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                UIHelpers.TriggerJsMsgSystem_v1(this, "Đã xảy ra lỗi: " + ex.Message.Replace("'", "\'"), "msg-system warning", 7000);
            }
        }


        protected void FuncComplete(object sender, EventArgs e)
        {
            Id = getId();
            if (Id > 0)
            {
                Update(Id);
            }
            else
            {
                Add();
            }
        }

        private void BindOBJ()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    if (Id > 0)
                    {
                        var record = db.ConfigQuantifyProducts.FirstOrDefault(w => w.Id == Id);
                        if (record != null)
                        {
                            ddlDepartment.Enabled = false;
                            ddlService.Enabled = false;
                            ddlDepartment.SelectedValue = record.DepartmentId.ToString();
                            ddlService.SelectedValue = record.ServiceId.ToString();
                            txtTotalNumberService.Text = record.TotalNumberService.ToString();
                            var getData = new Request().RunGetAsyncV1(AppConstants.URL_API_INVENTORY + $"/api/config-quantify-product?departmentId={record.DepartmentId ?? 0}&serviceId={record.ServiceId ?? 0}").Result;
                            //checkout success
                            if (getData.IsSuccessStatusCode)
                            {
                                //response data
                                var origin = getData.Content.ReadAsStringAsync().Result;
                                JavaScriptSerializer serializer = new JavaScriptSerializer();
                                var responseData = serializer.Deserialize<List<OutputProduct>>(origin);
                                RptConfigInventory.DataSource = responseData;
                                RptConfigInventory.DataBind();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                UIHelpers.TriggerJsMsgSystem_v1(this, "Đã xảy ra lỗi: " + ex.Message.Replace("'", "\'"), "msg-system warning", 7000);
            }
        }

        /// <summary>
        /// Lấy danh sách product
        /// </summary>
        /// <returns></returns>
        private string getProducts()
        {
            using (var db = new Solution_30shineEntities())
            {
                var serialize = new JavaScriptSerializer();
                var data = db.Products.Where(w => w.IsDelete == 0 && w.Publish == 1).ToList();
                if (data.Any())
                {
                    return serialize.Serialize(data);
                }
                return serialize.Serialize(new List<Product>());
            }
        }

        /// <summary>
        /// Load service
        /// </summary>
        private void getServices()
        {
            using (var db = new Solution_30shineEntities())
            {
                var service = db.Services.Where(w => w.IsDelete == 0 && w.Publish == 1).ToList();
                var ddls = new List<DropDownList> { ddlService };
                for (var i = 0; i < ddls.Count; i++)
                {
                    ddls[i].DataTextField = "Name";
                    ddls[i].DataValueField = "Id";
                    ddls[i].DataSource = service;
                    ddls[i].DataBind();
                }
            }
        }

        /// <summary>
        /// Load department
        /// </summary>
        private void getDepartments()
        {
            using (var db = new Solution_30shineEntities())
            {
                var department = db.Staff_Type.Where(w => w.IsDelete == 0 && w.Publish == true).ToList();
                var ddls = new List<DropDownList> { ddlDepartment };
                for (var i = 0; i < ddls.Count; i++)
                {
                    ddls[i].DataTextField = "Name";
                    ddls[i].DataValueField = "Id";
                    ddls[i].DataSource = department;
                    ddls[i].DataBind();
                }
            }
        }

        public partial class Origin
        {
            public int DepartmentId { get; set; }
            public int ServiceId { get; set; }
            public int TotalNumberService { get; set; }
            public List<ProductList> listProduct { get; set; }
        }

        public partial class ProductList
        {
            public int ProductId { get; set; }
            public double Quantify { get; set; }
            public double TotalNumberService { get; set; }
            public double Volume { get; set; }
        }

        public class OutputProduct
        {
            public int Id { get; set; }
            public int ProductId { get; set; }
            public string Name { get; set; }
            public string Code { get; set; }
            public double Quantify { get; set; }
            public double TotalNumberService { get; set; }
            public double Volume { get; set; }
        }
    }
}