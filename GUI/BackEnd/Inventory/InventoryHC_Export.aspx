﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="InventoryHC_Export.aspx.cs" Inherits="_30shine.GUI.BackEnd.Inventory.InventoryHC_Export" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý hàng tồn kho theo tuần &nbsp;&#187;</li>
                        <li class="li-listing"><a href="javascript:void();">Thống kê mức dùng vật tư</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row">
                    <div class="filter-item">
                        <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="time-wp">
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Từ ngày" Style="margin-left: 10px!important"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                        </div>
                        <strong class="st-head" style="margin-left: 10px;">
                            <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                        </strong>
                        <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeTo" placeholder="Đến ngày" Style="margin-left: 10px!important"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                        <br />
                    </div>
                    <div class="filter-item">

                        <asp:DropDownList ID="ddlSalon" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px;"></asp:DropDownList>
                    </div>
                    <div class="filter-item" style="margin-bottom: 10px">
                        <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" Text="Xem dữ liệu"
                            ClientIDMode="Static" onclick="excPaging(1)" runat="server">
                            Xem dữ liệu
                        </asp:Panel>
                    </div>
                    <!-- End Filter -->
                    <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                    <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                        <ContentTemplate>
                            <div class="row table-wp ">
                                <asp:GridView Border="0" HeaderStyle-BackColor="#cccccc"  CssClass="table-add table-listing report-sales-listing" ID="example" runat="server" PageSize="20" AllowPaging="true" FooterStyle-CssClass="site-paging" OnPageIndexChanging="example_PageIndexChanging">
                                    <HeaderStyle CssClass="trHeader"></HeaderStyle>
                                    <PagerStyle CssClass="site-paging1" HorizontalAlign="Center"   />
                                    <PagerSettings Mode="Numeric" FirstPageText="First" LastPageText="Last" NextPageText="Next"    PreviousPageText ="Prev"/>
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                </div>
                <%-- END Listing --%>
            </div>
        </div>
        <script>
            $('.txtDateTime').datetimepicker({
                dayOfWeekStart: 1,
                lang: 'vi',
                startDate: '2014/10/10',
                format: 'd/m/Y',
                dateonly: true,
                showHour: false,
                showMinute: false,
                timepicker: false,
                onChangeDateTime: function (dp, $input) { }
            });

        </script>
       <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
                margin-top:5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <style>
.site-paging1-wp{ width: 100%; float: left; margin: 35px 0; overflow: hidden; }
.site-paging1 td{border:0;}
.site-paging1 * { float: left; margin:auto !important;}
.site-paging1 a { width: 36px; height: 36px; line-height: 36px; text-align: center; background: #eeeeee; font-family: Roboto Condensed Regular; color: #999999; margin-right: 9px; }
.site-paging1 a.active { background: #50b347; border: 1px solid #50b347; color: #ffffff; }
.site-paging1 a:hover { background: #50b347; border: 1px solid #50b347; color: #ffffff; -webkit-transition: 0.2s all ease; -moz-transition: 0.2s all ease; -o-transition: 0.2s all ease; -ms-transition: 0.2s all ease; transition: 0.2s all ease; }
 .site-paging1 a {background: #000000;color: #ffffff;}
 .customer-listing table.table-listing .site-paging1 td {border:0;margin:auto;}
.site-paging1 a:hover{ background: #000000;color: #ffe400;}
        </style>    
    </asp:Panel>
</asp:Content>
