﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Globalization;
using System.Data;
using System.Linq.Expressions;
using System.Reflection;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Inventory
{
    public partial class InventoryHC_Export : Page
    {
        protected DataTable dt = new DataTable();
        private DateTime timeFrom;
        private DateTime timeTo;
        protected bool Perm_ShowSalon = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;
        private string PageID = "Inven_Export";
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        private bool Perm_ViewAllData = false;
        CultureInfo culture = new CultureInfo("vi-VN");
        protected Paging PAGING = new Paging();
        public List<cls_Export> data = new List<cls_Export>();
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
            }
            RemoveLoading();
        }
        /// <summary>
        /// Bind Data
        /// </summary>
        private void Bind_Data()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    if (TxtDateTimeFrom.Text != "")
                    {
                        timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                        if (TxtDateTimeTo.Text != "")
                        {
                            timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                        }
                        else
                        {
                            timeTo = timeFrom;
                        }
                        int salonId = Convert.ToInt32(ddlSalon.SelectedValue);
                        data = db.Database.SqlQuery<cls_Export>("exec [Store_InventoryHC_Export] {0}, {1}, {2}", timeFrom, timeTo, salonId).ToList();
                        int stt = 1;
                        if (data.Count > 0)
                        {
                            dt.Columns.Add("STT", typeof(int));
                            dt.Columns.Add("SalonName", typeof(string));
                            //dt.Columns.Add("GroupNameProduct", typeof(string));
                            for (int i = 0; i < data.Count; i++)
                            {
                                int dem = -1;
                                for (int j = 0; j < dt.Rows.Count; j++)
                                {
                                    if (dt.Rows[j]["SalonName"].ToString() == data[i].SalonName)
                                    {
                                        dem = j;
                                        break;
                                    }
                                }
                                if (dem == -1)
                                {
                                    if (dt.Columns.Contains(data[i].GroupNameProduct) == false)
                                    {
                                        dt.Columns.Add(data[i].GroupNameProduct, typeof(string));
                                    }
                                    DataRow row = dt.NewRow();
                                    row["SalonName"] = data[i].SalonName;
                                    //row["GroupNameProduct"] = data[i].GroupNameProduct;
                                    row["STT"] = stt++ + PAGING._Offset;
                                    row[data[i].GroupNameProduct] = data[i].Total;
                                    dt.Rows.Add(row);
                                }
                                else
                                {
                                    if (dt.Columns.Contains(data[i].GroupNameProduct) == false)
                                    {
                                        dt.Columns.Add(data[i].GroupNameProduct, typeof(string));
                                        dt.Rows[dem][data[i].GroupNameProduct] = data[i].Total;
                                    }
                                    //else
                                    //{
                                    //    try
                                    //    {
                                    //        dt.Rows[dem][data[i].GroupNameProduct] = Convert.ToInt32(dt.Rows[dem][data[i].GroupNameProduct]) + data[i].Total;
                                    //    }
                                    //    catch
                                    //    {
                                    //        dt.Rows[dem][data[i].GroupNameProduct] = data[i].Total;
                                    //    }
                                    //}
                                }
                            }
                            example.DataSource = dt;
                            example.DataBind();
                        }
                        else
                        {
                            example.DataSource = null;
                            example.DataBind();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Data();
            RemoveLoading();
        }
        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var msgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", msgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //////Perm_Add = permissionModel.GetActionByActionNameAndPageId("Perm_Add", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        public class cls_Export
        {
            public int STT { get; set; }
            public string SalonName { get; set; }
            public string GroupNameProduct { get; set; }
            public int Total { get; set; }
        }
        protected void example_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            example.PageIndex = e.NewPageIndex;
            Bind_Data();
        }

    }
}