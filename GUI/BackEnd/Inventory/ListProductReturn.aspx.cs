﻿using _30shine.Helpers;
using _30shine.Helpers.Http;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using Libraries;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.Inventory
{
    public partial class ListProductReturn : System.Web.UI.Page
    {
        private bool Perm_Edit = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;
        protected Paging PAGING = new Paging();
        CultureInfo culture = new CultureInfo("vi-VN");
        public List<Output_ProductReturn> listOrigins = new List<Output_ProductReturn>();
        public int index = 1;

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ViewAllDate = permissionModel.CheckPermisionByAction("Perm_ViewAllDate", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }

        //page load
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
                Library.Function.bindSalon_NotHoiQuan(new List<DropDownList>() { ddlSalon }, Perm_ViewAllData);
                BindProduct();
            }
        }

        /// <summary>
        /// Bind data ddlProduct
        /// </summary>
        private void BindProduct()
        {
            using (var db = new Solution_30shineEntities())
            {
                var listProduct = db.Products.Where(a => a.IsDelete == 0 && a.Publish == 1).OrderBy(o => o.Id).ToList();
                var item = new Product();
                item.Name = "Chọn tất cả sản phẩm";
                item.Id = 0;
                listProduct.Insert(0, item);
                ddlProduct.DataTextField = "Name";
                ddlProduct.DataValueField = "Id";
                ddlProduct.DataSource = listProduct;
                ddlProduct.DataBind();
            }
        }

        /// <summary>
        /// Get data
        /// </summary>
        private void GetData()
        {
            try
            {
                if (!String.IsNullOrEmpty(TxtDateTimeFrom.Text))
                {
                    int integer;
                    DateTime timeFrom, timeTo;
                    timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                    if (!String.IsNullOrEmpty(TxtDateTimeTo.Text))
                    {
                        timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                    }
                    else
                    {
                        timeTo = timeFrom;
                    }
                    int salonId = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                    int productId = int.TryParse(ddlProduct.SelectedValue, out integer) ? integer : 0;
                    var getData = new Request().RunGetAsyncV1(AppConstants.URL_API_INVENTORY + $"/api/product-return/list-product-return?salonId={salonId}&productId={productId}&timeFrom={timeFrom}&timeTo={timeTo}").Result;
                    if (getData.IsSuccessStatusCode)
                    {
                        var list = getData.Content.ReadAsStringAsync().Result;
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        var reponseData = serializer.Deserialize<List<Output_ProductReturn>>(list);
                        if (reponseData != null)
                        {
                            Bind_Paging(listOrigins.Count);
                            listOrigins = reponseData.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                new Helpers.PushNotiErrorToSlack().Push("GA1LZN9AM", ex.Message + ", MAIN: " + ex.StackTrace + ", " + "ProductReturn",
                                         "ProductReturn" + ".ProductReturn", "QuynhDD",
                                         HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host);
            }
        }

        /// <summary>
        /// btn click
        /// </summary>
        protected void _BtnClick(object sender, EventArgs e)
        {
            GetData();
            RemoveLoading();
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "finishLoading();", true);
        }

        /// <summary>
        /// Paging
        /// </summary>
        /// <param name="TotalRow"></param>
        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();
            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }
    }

    /// <summary>
    /// Output data reposne
    /// </summary>
    public class Output_ProductReturn
    {
        public string createdDate { get; set; }
        public string customerName { get; set; }
        public string salonSellName { get; set; }
        public string salonReceiverName { get; set; }
        public int sellerId { get; set; }
        public string sellerName { get; set; }
        public string receiverName { get; set; }
        public int productId { get; set; }
        public string productName { get; set; }
        public int quantity { get; set; }
        public double priceProduct { get; set; }
        public double productSalary { get; set; }
    }
}