﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Globalization;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Inventory
{
    public partial class InventoryHC_Add : System.Web.UI.Page
    {
        protected bool _IsUpdate = false;
        protected int _Code;
        public string disable;
        private string PageID = "Inven_Add";
        private bool Perm_Access = false;
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        private bool Perm_ViewAllData = false;
        protected bool IsAccountant = false;
        protected string ListProduct;
        protected string permission;
        public static List<_30shine.MODEL.ENTITY.EDMX.Product> _list = new List<_30shine.MODEL.ENTITY.EDMX.Product>();
        protected int indexRow;
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            //CheckTime();
            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                if (Perm_ViewAllData == true)
                {
                    TxtDateTimeFrom.Enabled = true;
                }
                else
                {
                    TxtDateTimeFrom.Enabled = false;
                }
                TxtDateTimeFrom.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Today);
                Bind_RptProduct();
                bindProductCategory();
            }

            ListProduct = GetProduct();
        }
        /// <summary>
        /// get quyền 
        /// </summary>
        /// <returns></returns>
        public bool IsAdmin()
        {
            if (Session["User_Permission"].ToString() == "ADMIN" || Session["User_Permission"].ToString() == "root")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// GetProduct Bind Dropdowlist
        /// </summary>
        private string GetProduct()
        {
            using (var db = new Solution_30shineEntities())
            {
                _list = db.Products.Where(w => (w.IsDelete != 1 && w.Publish == 1 && w.CategoryId == 20) || (w.Id == 378 || w.Id == 63 || w.Id == 44 || w.Id == 58 || w.Id == 83 || w.Id == 440 || w.Id == 32 || w.Id == 446) && w.IsCheckVatTu == true).ToList();
                //_list = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1).ToList();
                var seria = new JavaScriptSerializer();
                return seria.Serialize(_list);
            }
        }

        /// <summary>
        /// check Time and Day ( Insert Product Monday time 7h00-9h00)
        /// </summary>
        public bool IsTimeOfDayBetween(DateTime time, TimeSpan? startTime, TimeSpan? endTime)
        {
            if (endTime == startTime)
            {
                return true;
            }
            else if (endTime < startTime)
            {
                return time.TimeOfDay <= endTime || time.TimeOfDay >= startTime;
            }
            else
            {
                return time.TimeOfDay >= startTime && time.TimeOfDay <= endTime;
            }
        }
        private void CheckTime()
        {
            var input = DateTime.Now;
            int offset = input.DayOfWeek - DayOfWeek.Monday;
            DateTime lastMonday = input.AddDays(-offset);
            DateTime nextDayOfWeek = lastMonday.AddDays(7);
            if (Perm_ViewAllData)
            {
                this.disable = "";
            }
            else
            {
                if (lastMonday.Date == input.Date || nextDayOfWeek.Date == input.Date)
                {
                    DateTime stime = DateTime.Now;
                    var check = IsTimeOfDayBetween(stime, new TimeSpan(7, 0, 0), new TimeSpan(9, 0, 0));
                    if (check == true)
                        this.disable = "";
                    else
                        this.disable = "disabled = " + "disabled" + "";
                }
                else
                    this.disable = "disabled = " + "disabled" + "";
            }
        }

        //Bind category
        private void bindProductCategory()
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = new List<Tbl_Category>();
                var node = new List<Tbl_Category>();
                var c = new Tbl_Category();
                c.Id = 0;
                c.Name = "Chọn danh mục";
                list.Insert(0, c);
                var cateRoot = db.Tbl_Category.Where(w => w.IsDelete != 1 && w.Pid == 1 && w.Publish == true).ToList();
                if (cateRoot.Count > 0)
                {
                    foreach (var v in cateRoot)
                    {
                        node = db.Tbl_Category.Where(w => w.IsDelete != 1 && w.Pid == v.Id).ToList();
                        list.Add(v);
                        if (node.Count > 0)
                        {
                            foreach (var v2 in node)
                            {
                                v2.Name = "----- " + v2.Name;
                                list.Add(v2);
                            }
                        }
                    }
                }
                rptProductCategory.DataSource = list;
                rptProductCategory.DataBind();
            }
        }
        //Bind  product to popup
        private void Bind_RptProduct()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Products = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1 && w.IsCheckVatTu == true).OrderBy(o => o.CategoryId).ToList();
                Rpt_Product_Flow.DataSource = _Products;
                Rpt_Product_Flow.DataBind();
            }
        }
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var msgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", msgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //////Perm_Add = permissionModel.GetActionByActionNameAndPageId("Perm_Add", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }

    }

}