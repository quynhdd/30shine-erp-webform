﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Script.Serialization;
using ExportToExcel;
using Excel;
using LinqKit;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Inventory
{
    public partial class Report : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        protected List<List<ProductBasic>> ProductList;
        private Expression<Func<ExportGood, bool>> Where = PredicateBuilder.True<ExportGood>();
        CultureInfo culture = new CultureInfo("vi-VN");
        private string sql = "";

        private string PageID = "TK_DL_1";
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;
        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

        protected string table = "";
        protected List<Product> products = new List<Product>();
        private JavaScriptSerializer serialize = new JavaScriptSerializer();

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                GenWhere();
                Bind_Paging();
                Bind_DataByDate();
            }
            else
            {
                GenWhere();
            }
        }
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //Perm_ViewAllDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDate", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ViewAllDate = permissionModel.CheckPermisionByAction("Perm_ViewAllDate", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        private void GenWhere()
        {
            DateTime timeFrom = Convert.ToDateTime("2016/8/1", culture);
            DateTime timeTo = new DateTime();
            string whereTime = "";
            string whereSalon = "";
            int SalonId = Convert.ToInt32(Salon.SelectedValue);
            if (TxtDateTimeFrom.Text != "")
            {
                timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                if (TxtDateTimeTo.Text != "")
                {
                    timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                }
                else
                {
                    timeTo = timeFrom;
                }
                whereTime = " and idata.iDate >= '"+ string.Format("{0:yyyy/MM/dd}", timeFrom) + "' and idata.iDate <= '" + string.Format("{0:yyyy/MM/dd}", timeTo) + "'";
            }
            else
            {
                whereTime = " and idata.iDate >= '" + string.Format("{0:yyyy/MM/dd}", timeFrom) + "'";
            }

            if (SalonId > 0)
            {
                whereSalon += " and idata.SalonId = " + SalonId;
            }
            else
            {
                whereSalon += " and idata.SalonId > 0";
            }
            sql = @"select idata.*, salon.Name as salonName
                    from Inventory_Data as idata
                    left join Tbl_Salon as salon
                    on idata.SalonId = salon.Id
                    where idata.IsDelete != 1 " + whereTime + whereSalon;
            // get list product
            using (var db = new Solution_30shineEntities())
            {
                products = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1).OrderBy(o=>o.Id).ToList();
            }
        }

        protected void Bind_DataByDate()
        {
            using (var db = new Solution_30shineEntities())
            {
                var dataReport = new List<Library.Class.cls_inventoryItem>();
                var dataBySalon = new List<Library.Class.cls_inventoryReport>();
                var list = new List<Library.Class.cls_inventoryItem>();
                var item = new Library.Class.cls_inventoryItem();
                var loop = 0;
                var count = 0;
                var listInvenData = db.Database.SqlQuery<Library.Class.cls_inventory_data>(sql).ToList();
                var salons = new List<Tbl_Salon>();
                var salonId = Library.Function.getSalonId(Perm_ViewAllData, Salon.SelectedValue);
                if (Perm_ViewAllData)
                {
                    if (salonId == 0)
                    {
                        salons = db.Tbl_Salon.Where(w => w.IsDelete != 1 && w.Publish == true).ToList();
                    }
                    else
                    {
                        salons = db.Tbl_Salon.Where(w => w.Id == salonId).ToList();
                    }
                }
                else
                {
                    salons = db.Tbl_Salon.Where(w => w.Id == salonId).ToList();
                }
                if (salons.Count > 0)
                {
                    foreach (var v in salons)
                    {
                        var itemBySalon = new Library.Class.cls_inventoryReport();
                        itemBySalon.salonId = v.Id;
                        itemBySalon.salonName = v.Name;
                        var listBySalon = listInvenData.Where(w => w.SalonId == v.Id).OrderBy(o=>o.iDate ).ToList();
                        if (listBySalon.Count > 0)
                        {
                            foreach (var v2 in listBySalon)
                            {
                                if (v2.InvenData != null && v2.InvenData != "")
                                {
                                    list = serialize.Deserialize<List<Library.Class.cls_inventoryItem>>(v2.InvenData);
                                    count = list.Count;
                                    if (count > 0)
                                    {
                                        if (loop == 0)
                                        {
                                            itemBySalon.data = list;
                                        }
                                        else
                                        {
                                            for (var i = 0; i < count; i++)
                                            {
                                                itemBySalon.data[i].importQuantity += list[i].importQuantity;
                                                itemBySalon.data[i].sellQuantity += list[i].sellQuantity;
                                                itemBySalon.data[i].sendBackQuantity += list[i].sendBackQuantity;
                                                itemBySalon.data[i].exportOutQuantity += list[i].exportOutQuantity;
                                                itemBySalon.data[i].restQuantity = list[i].restQuantity;
                                            }
                                        }
                                    }
                                }
                                loop++;
                            }
                            loop = 0;
                        }
                        dataBySalon.Add(itemBySalon);
                    }

                    foreach (var v in dataBySalon)
                    {
                        if (v.data != null && v.data.Count > 0)
                        {
                            if (loop == 0)
                            {
                                dataReport = v.data;
                            }
                            else
                            {
                                for (var i = 0; i < count; i++)
                                {
                                    dataReport[i].restQuantityLastDay += v.data[i].restQuantityLastDay;
                                    dataReport[i].importQuantity += v.data[i].importQuantity;
                                    dataReport[i].sellQuantity += v.data[i].sellQuantity;
                                    dataReport[i].sendBackQuantity += v.data[i].sendBackQuantity;
                                    dataReport[i].exportOutQuantity += v.data[i].exportOutQuantity;
                                    dataReport[i].restQuantity += v.data[i].restQuantity;
                                }
                            }                            
                        }
                        loop++;
                    }

                    // Bind data to repeater
                    RptInventory.DataSource = dataReport;
                    RptInventory.DataBind();
                }
                
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
            Bind_DataByDate();
            ScriptManager.RegisterStartupScript(UPTableListing, this.GetType(), "Format Price", "", true);
            RemoveLoading();
        }

        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? (HDF_Page.Value != "" ? Convert.ToInt32(HDF_Page.Value) : 1) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                var Count = db.Database.SqlQuery<cls_products>(sql).Count();
                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        public string GenTime(int minutes)
        {
            string time = "";
            int hour = minutes / 60;
            int minute = minutes % 60;
            if (hour > 0)
            {
                time += hour + "h";
                time += minute + "p";
            }
            else
            {
                if (minute > 0)
                {
                    time += minute + " p";
                }
                else
                {
                    time += "-";
                }
            }
            return time;
        }
        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }

    }
}