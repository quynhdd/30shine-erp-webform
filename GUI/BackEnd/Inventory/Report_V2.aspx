﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Report_V2.aspx.cs" Inherits="_30shine.GUI.BackEnd.Inventory.Report_V2" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<%@ Register Src="~/GUI/BackEnd/Report/ReportMenu.ascx" TagName="ReportMenu" TagPrefix="uc1" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">

<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">
                <li>Quản lý tồn kho &nbsp;&#187; </li>
                <li class="li-listing li-listing-1" runat="server" id="subMenu_liListing"><a href="/ton-kho/danh-sach.html">Lịch sử nhập</a></li>
                <li class="li-listing li-listing-2" runat="server" id="subMenu_liReport"><a href="/ton-kho/du-lieu.html">Dữ liệu tồn kho</a></li>
                <li class="li-add" runat="server" id="subMenu_liAdd"><a href="/ton-kho/nhap-moi.html">Nhập mới</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add customer-listing be-report">
    <%-- Listing --%>                
    <div class="wp960 content-wp">
        <!-- Filter -->
        <div class="row">
            <div class="filter-item" style="margin-left: 0;">
                <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Thời gian</strong>                
                <div class="datepicker-wp">
                    <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                        ClientIDMode="Static" runat="server"></asp:TextBox>
                    <strong class="st-head" style="margin-left: 10px; margin-right: 10px;">
                        <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                    </strong>
                    <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeTo" placeholder="Đến ngày"
                        ClientIDMode="Static" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="filter-item">
                <asp:DropDownList ID="Salon" runat="server" CssClass="select form-control" ClientIDMode="Static" style="margin: 12px 0; width: 190px;"></asp:DropDownList>
            </div>
            <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" Text="Xem dữ liệu" 
                ClientIDMode="Static" onclick="excPaging(1)" runat="server">Xem dữ liệu</asp:Panel>
            <div class="export-wp drop-down-list-wp">
                <asp:Panel ID="fff" CssClass="st-head btn-viewdata"
                    ClientIDMode="Static" runat="server" onclick="EntryExportExcel('all')">Xuất File Excel</asp:Panel><%--OnClick="Exc_ExportExcel" --%>
                <ul class="ul-drop-down-list" style="display:none;">
                    <li onclick="EntryExportExcel('all')">Tất cả dữ liệu</li>
                    <li onclick="EntryExportExcel('current page')">Trang hiện tại</li>
                </ul>
            </div>            
            <%--<asp:Panel ID="Btn_ExportExcel" CssClass="st-head btn-viewdata"
                ClientIDMode="Static" onclick="excExcel('Export')" runat="server">Xuất File Excel</asp:Panel>--%>
            <%--<asp:Panel ID="Btn_ImportExcel" CssClass="st-head btn-viewdata"
                ClientIDMode="Static" onclick="excExcel('Import')" runat="server">Nhập File Excel</asp:Panel>--%>
        </div> 
        <!-- End Filter -->       
        <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>    
        <div class="row mgt">
            <strong class="st-head"><i class="fa fa-file-text"></i>Dữ liệu tồn kho các salon</strong>
        </div>
        <!-- Row Table Filter -->
        <div class="table-func-panel" style="margin-top: -33px;">
            <div class="table-func-elm">
                <span>Số hàng / Page : </span>
                <div class="table-func-input-wp">
                    <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                    <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                    <ul class="ul-opt-segment">
                        <li data-value="10">10</li>
                        <li data-value="20">20</li>
                        <li data-value="30">30</li>
                        <li data-value="40">40</li>
                        <li data-value="50">50</li>
                        <li data-value="200">200</li>
                        <%--<li data-value="1000000">Tất cả</li>--%>
                    </ul>
                    <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                </div>                
            </div>            
        </div>
        <!-- End Row Table Filter -->
        <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
            <ContentTemplate>            
                <div class="row table-wp">
                    <table class="table-add table-listing report-sales-listing">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>ID</th>
                                <th>Mỹ phẩm</th>
                                <th>Tồn đầu kỳ</th>
                                <th>Nhập từ kho</th>
                                <th>Trả lại kho</th>
                                <th>Bán ra</th>                                
                                <%--<th>Xuất lý do khác</th>--%>
                                <th>Tồn cuối kỳ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="RptInventory" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                        <td><%# Eval("ProductId") %></td>
                                        <td><%# Eval("ProductName") %></td>
                                        <td><%# Eval("InvenBefore") %></td>
                                        <td><%# Eval("ImportInTime") %></td>                                        
                                        <td><%# Eval("ExportInTime") %></td>
                                        <td><%# Eval("SellInTime") %></td>
                                        <%--<td><%# Eval("exportOutQuantity") %></td>--%>
                                        <td><%# Eval("InvenNow") %></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>                    
                        </tbody>
                    </table>
                </div>
                <!-- Paging -->
                <div class="site-paging-wp">
                <% if(PAGING.TotalPage > 1){ %>
                    <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                        <% if (PAGING._Paging.Prev != 0)
                           { %>
                        <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                        <% } %>
                        <asp:Repeater ID="RptPaging" runat="server">
                            <ItemTemplate>
                                <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                    <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>
                                >
                                    <%# Eval("PageNum") %>
                                </a>
                            </ItemTemplate>                    
                        </asp:Repeater>
                        <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                           { %>                
                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                        <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                        <% } %>
                    </asp:Panel>
                <% } %>
                </div>
		        <!-- End Paging -->        
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <%--<asp:UpdatePanel ID="UPExcel" runat="server" ClientIDMode="Static">
            <ContentTemplate></ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="BtnFakeExcel" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>--%>
        <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" style="display:none;" />
        <%--<asp:Button ID="BtnFakeExcel" ClientIDMode="Static" runat="server" OnClick="Exc_ExportExcel" style="display:none;" />--%>
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="IsPaging" />
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_ExcelType" />
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELPage" />
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELSegment" />
        <asp:Button ID="BtnFakeExcel" ClientIDMode="Static" runat="server" OnClick="Exc_ExportExcel" style="display:none;" />    
    </div> 
    <%-- END Listing --%>
</div>
    <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
                margin-top: 5px !important;
            }
            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
<script type="text/javascript">
    jQuery(document).ready(function () {
        // Add active menu
        $("#glbInventory").addClass("active");
        if (location.pathname == "/ton-kho/danh-sach.html") {
            $("#subMenu .li-listing-1").addClass("active");
        } else if (location.pathname == "/ton-kho/du-lieu.html") {
            $("#subMenu .li-listing-2").addClass("active");
        }

        // Price format
        UP_FormatPrice('.be-report-price');

        //============================
        // Datepicker
        //============================
        $('.txtDateTime').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'vi',
            startDate: '2014/10/10',
            format: 'd/m/Y',
            dateonly: true,
            showHour: false,
            showMinute: false,
            timepicker: false,
            onChangeDateTime: function (dp, $input) { },
            scrollMonth: false,
            scrollTime: false,
            scrollInput: false
        });

        // View data today
        $(".tag-date-today").click();
    });
    
    function SubTableFixHeight() {
        $("tbody table.sub-table").each(function () {
            $(this).height($(this).parent().height() + 1);
        });
    }

    function viewDataByDate(This, time) {
        $(".tag-date.active").removeClass("active");
        This.addClass("active");
        $("#TxtDateTimeTo").val("");
        $("#TxtDateTimeFrom").val(time);
        $("#ViewData").click();
    }

    //============================
    // Event delete
    //============================
    function del(This, code, name) {
        var code = code || null,
            name = name || null,
            Row = This;
        if (!code) return false;

        // show EBPopup
        $(".confirm-yn").openEBPopup();
        $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

        $("#EBPopup .yn-yes").bind("click", function () {
            $.ajax({
                type: "POST",
                url: "/GUI/SystemService/Ajax/Del.aspx/Delele_Inventory_Import",
                data: '{Code : "' + code + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var mission = JSON.parse(response.d);
                    if (mission.success) {
                        delSuccess();
                        Row.remove();
                    } else {
                        delFailed();
                    }
                },
                failure: function (response) { alert(response.d); }
            });
        });
        $("#EBPopup .yn-no").bind("click", function () {
            autoCloseEBPopup(0);
        });
    }



</script>

</asp:Panel>
</asp:Content>