﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Script.Serialization;
using ExportToExcel;
using Excel;
using LinqKit;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Inventory
{
    public partial class Listing : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        protected List<List<ProductBasic>> ProductList;
        private Expression<Func<ExportGood, bool>> Where = PredicateBuilder.True<ExportGood>();
        CultureInfo culture = new CultureInfo("vi-VN");
        private string sql = "";

        private string PageID = "MP_B";
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;
        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
        protected string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
        protected string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
        protected string table = "";
        private int integer;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //Perm_ViewAllDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDate", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ViewAllDate = permissionModel.CheckPermisionByAction("Perm_ViewAllDate", pageId, staffId);
                //Perm_ShowElement = permissionModel.CheckPermisionByAction("Perm_ShowElement", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                //Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();

            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (IsAccountant)
            {
                subMenu_liAdd.Visible = false;
            }

            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = FirstDayOfMonth;
                TxtDateTimeTo.Text = Today;
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                //GenWhere();
                //Bind_Paging();
                //Bind_DataByDate();
            }
            //else
            //{
            //    GenWhere();
            //}
        }

        private void GenWhere()
        {
            DateTime timeFrom = Convert.ToDateTime("2016/8/1", culture);
            DateTime timeTo = new DateTime();
            string whereTime = "";
            string whereSalon = "";
            int SalonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
            if (TxtDateTimeFrom.Text != "")
            {
                timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                if (TxtDateTimeTo.Text != "")
                {
                    timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                }
                else
                {
                    timeTo = timeFrom;
                }
                whereTime = " and a.ImportDate between '" + string.Format("{0:yyyy/MM/dd}", timeFrom) + "' and '" + string.Format("{0:yyyy/MM/dd}", timeTo) + "'";
            }
            else
            {
                whereTime = " and a.ImportDate >= '" + string.Format("{0:yyyy/MM/dd}", timeFrom) + "'";
            }
            if (SalonId > 0)
            {
                whereSalon += " and a.SalonId = " + SalonId;
            }
            else
            {
                whereSalon += " and a.SalonId > 0";
            }
            sql = @"select a.*, b.Name as SalonName, c.Fullname as DoName, d.Fullname as RecipientName
                    from Inventory_Import as a
                    left join Tbl_Salon as b
                    on a.SalonId = b.Id
                    left join Staff as c
                    on a.DoId = c.Id
                    left join Staff as d
                    on a.RecipientId = d.Id
                    where a.IsDelete != 1 " +
                    whereTime + whereSalon;
        }

        protected void Bind_DataByDate()
        {
            //call
            GenWhere();
            using (var db = new Solution_30shineEntities())
            {
                var listGoods = new List<ProductBasicV1>();
                var goods = new cls_products();
                var list = db.Database.SqlQuery<cls_products>(sql).ToList();
                var serialize = new JavaScriptSerializer();
                int loop = 0;
                int totalCost = 0;
                if (list.Count > 0)
                {
                    foreach (var v in list)
                    {
                        totalCost = 0;
                        var goodsDetail = "";
                        listGoods = serialize.Deserialize<List<ProductBasicV1>>(v.ProductIds);
                        if (listGoods.Count > 0)
                        {
                            var i = 0;
                            var First = "";
                            foreach (var v2 in listGoods)
                            {
                                // Price lưu ở đây là giá nhập (không phải giá bán)
                                First = i == 0 ? " class='first'" : "";
                                goodsDetail += "<tr" + First + ">" +
                                                    "<td class='col-xs-9'>" +
                                                            "<a href='/admin/san-pham/" + v2.Id + ".html' target='_blank'>" +
                                                            v2.Name +
                                                            "</a>" +
                                                    "</td>" +
                                                    "<td class='col-xs-1 no-bdr'>" + v2.Quantity + "</td>" +
                                                    "<td class='col-xs-2 no-bdr be-report-price' style='display:none;'>" + v2.Price + "</td>" +
                                                "</tr>";
                                i++;
                                // Tính tổng giá nhập
                                totalCost += v2.Price * v2.Quantity ?? 1;
                            }
                            goodsDetail = goodsDetail.TrimEnd(',', ' ');
                        }
                        else
                        {
                            goodsDetail += "<tr class='first'><td class='no-bdr'>-</td></tr>";
                        }
                        list[loop].productsDetail = goodsDetail;
                        list[loop++].totalCost = (double)totalCost;
                    }
                }
                Bind_Paging(list.Count);
                RptGoods.DataSource = list.OrderByDescending(o => o.Id).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                RptGoods.DataBind();
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_DataByDate();
            ScriptManager.RegisterStartupScript(UPTableListing, this.GetType(), "Format Price", "", true);
            RemoveLoading();
        }

        /// <summary>
        /// Paging
        /// </summary>
        /// <param name="TotalRow"></param>
        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        public string GenTime(int minutes)
        {
            string time = "";
            int hour = minutes / 60;
            int minute = minutes % 60;
            if (hour > 0)
            {
                time += hour + "h";
                time += minute + "p";
            }
            else
            {
                if (minute > 0)
                {
                    time += minute + " p";
                }
                else
                {
                    time += "-";
                }
            }
            return time;
        }



        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (!IsAccountant)
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    if (Perm_Edit == true)
                        ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                    if (Perm_Delete == true)
                        ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
                }
            }
        }

    }
    public struct ProductBasicV1
    {
        public int Id { get; set; }
        /// <summary>
        /// Mã sản phẩm
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// Tên sản phẩm
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Giá gốc
        /// </summary>
        public int Cost { get; set; }
        /// <summary>
        /// Giá bán
        /// </summary>
        public int Price { get; set; }
        /// <summary>
        /// Số lượng
        /// </summary>
        public int? Quantity { get; set; }
        /// <summary>
        /// % khuyến mãi
        /// </summary>
        public int VoucherPercent { get; set; }
        /// <summary>
        /// Tiền khuyến mãi
        /// </summary>
        public int Promotion { get; set; }
        public string MapIdProduct { get; set; }
        public bool CheckCombo { get; set; }
        public int? Order { get; set; }
        /// <summary>
        /// Check mức dùng vật tư
        /// </summary>
        public bool IsCheckVatTu { get; set; }
    }

    public class cls_products : Inventory_Import
    {
        /// <summary>
        /// Bảng danh sách các item (vật tư)
        /// </summary>
        public string productsDetail { get; set; }
        /// <summary>
        /// Tên salon
        /// </summary>
        public string SalonName { get; set; }
        /// <summary>
        /// Tên người xuất hàng
        /// </summary>
        public string DoName { get; set; }
        /// <summary>
        /// Tên người nhận hàng
        /// </summary>
        public string RecipientName { get; set; }
        /// <summary>
        /// Tổng giá nhập
        /// </summary>
        public double totalCost { get; set; }
    }
}