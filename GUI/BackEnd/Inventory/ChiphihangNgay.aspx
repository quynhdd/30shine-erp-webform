﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ChiphihangNgay.aspx.cs" Inherits="_30shine.GUI.BackEnd.Inventory.ChiphihangNgay" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý chi tiêu hằng ngày &nbsp;&#187; </li>
                        <li class="li-listing" style="cursor: pointer"><a href="/admin/hang-hoa/danh-sach-chi-phi-hang-ngay.html">Chi phí hàng ngày</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing be-report woktime-listing">
            <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row row-filter">
                    <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Thời gian</strong>
                    <div class="datepicker-wp">
                        <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Chọn ngày"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                        <strong class="st-head" style="margin-left: 10px;">
                            <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                        </strong>
                        <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeTo" placeholder="Đến ngày" Style="margin-left: 10px!important"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                        <br />
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlSalon" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px;"></asp:DropDownList>
                    </div>
                    <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                      <%if (Perm_ViewAllData)
                        {%>
                    <asp:Panel ID="fff" CssClass="st-head btn-viewdata"
                        ClientIDMode="Static" runat="server" onclick="EntryExportExcel('all')">
                        Xuất File Excel
                    </asp:Panel>
                    <%} %>
                </div>
                <div class="row">
                </div>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <% if (Perm_ViewAllData)
                            { %>
                        <div class="row">
                            <table class="table-total-report-1">
                                <tbody>
                                    <tr>
                                        <td><strong class="st-head"><i class="fa fa-file-text"></i>Tổng chi phí:</strong></td>
                                        <td style="text-align: right; padding-left: 20px;">
                                            <span style="font-family: Roboto Condensed Bold;"><%=String.Format("{0:#,###}",totalCost).Replace(',','.') + " VNĐ"%></span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <% } %>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- End Filter -->
                <!-- Row Table Filter -->
                <div class="table-func-panel">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="row table-wp">
                            <table class="table-add table-listing report-sales-listing">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên sản phẩm</th>
                                        <th>Hàng tồn cũ</th>
                                        <th>Hàng nhận đầu ngày</th>
                                        <th>Hàng tồn cuối ngày</th>
                                        <th>Mức dùng hàng ngày</th>
                                        <% if (Perm_ViewAllData == true)
                                            { %>
                                        <th>Giá nhập</th>
                                        <th>Chi phí</th>
                                        <% } %>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptChiphingay" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td><%# Eval("name") %></td>
                                                <td><%# Eval("quantityOld") %></td>
                                                <td><%# Eval("quantityReceived") %></td>
                                                <td><%# Eval("quantityNew") %></td>
                                                <td><%# Eval("quantityLevelOfUse") %></td>
                                                <% if (Perm_ViewAllData == true)
                                                    { %>
                                                <td>
                                                    <%# String.Format("{0:#,####}", Convert.ToInt64(Eval("cost"))).Replace(',', '.') %>
                                                </td>
                                                <td><%# String.Format("{0:#,####}",Convert.ToDouble(Eval("totalMoney"))).Replace(',', '.') %></td>
                                                <% } %>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                        <%-- <asp:AsyncPostBackTrigger ControlID="BtnFakeUPV1" EventName="Click" />--%>
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:Button ID="BtnFakeExcel" ClientIDMode="Static" runat="server" OnClick="Exc_ExportExcel" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELPage" />
                <%--//goi khi an nut xem view--%>
                <%--<asp:Button ID="BtnFakeUPV1" ClientIDMode="Static" OnClick="_BtnClickV1" runat="server" Text="Click" Style="display: none;"/>--%>
            </div>
        </div>

    </asp:Panel>
    <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
    <script src="../../../Assets/js/select2/select2.min.js"></script>
    <script>
        $('.select').select2();
    </script>
    <style>
        .select2-container {
            width: 180px !important;
            margin-top: 5px !important;
        }

        .select2-container--default .select2-selection--single {
            height: 32px !important;
            padding-top: 1px !important;
        }
    </style>
    <script>
        jQuery(document).ready(function () {

            $('.txtDateTime').datetimepicker({
                dayOfWeekStart: 1,
                lang: 'vi',
                startDate: '2014/10/10',
                format: 'd/m/Y',
                dateonly: true,
                showHour: false,
                showMinute: false,
                timepicker: false,
                onChangeDateTime: function (dp, $input) { },
                scrollMonth: false,
                scrollTime: false,
                scrollInput: false
            });
        });
    </script>
</asp:Content>
