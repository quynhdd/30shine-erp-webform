﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Linq.Expressions;
using ExportToExcel;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Inventory
{
    public partial class ReportExportSupplies : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        public bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected string Permission = "";
        private int integer;
        protected CultureInfo culture = new CultureInfo("vi-VN");
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
                TxtDateTimeTo.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Today);
                BindRegion();
                BindSalonWhereRegion();
                BindData();
            }
            else
            {
            }
            RemoveLoading();
        }

        public void BindRegion()
        {
            using (var db = new Solution_30shineEntities())
            {
                //id region
                int[] iRegion = new int[] { 1, 42, 62 };
                IEnumerable<TinhThanh> lstRegion = db.TinhThanhs.Where(w => iRegion.Contains(w.ID)).ToList();
                var Key = 0;
                ddlRegion.DataTextField = "SkillLevel";
                ddlRegion.DataValueField = "Value";
                ListItem item = new ListItem("Chọn khu vực", "0");
                ddlRegion.Items.Insert(Key, item);
                Key++;

                if (lstRegion.Any())
                {
                    foreach (var v in lstRegion)
                    {
                        item = new ListItem(v.TenTinhThanh, v.ID.ToString());
                        ddlRegion.Items.Insert(Key, item);
                        Key++;
                    }
                }
            }
        }

        public List<ExportSupplies> GetData(bool RemovedProductIdWhenExportExcel = false)
        {
            using (var db = new Solution_30shineEntities())
            {
                DateTime TimeFrom = new DateTime();
                DateTime TimeTo = new DateTime();
                List<ExportSupplies> lstExportSupplies = new List<ExportSupplies>();
                if (!String.IsNullOrEmpty(TxtDateTimeFrom.Text))
                {
                    TimeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                    if (String.IsNullOrEmpty(TxtDateTimeTo.Text))
                    {
                        TimeTo = TimeFrom.AddDays(1);
                    }
                    else
                    {
                        TimeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                    }
                    int iRegion = int.TryParse(ddlRegion.SelectedValue, out var integer) ? integer : 0;
                    int iSalon = int.TryParse(ddlSalon.SelectedValue, out var integers) ? integers : 0;
                    int UserId = Convert.ToInt32(HttpContext.Current.Session["User_Id"]);
                    //get list salon
                    string WhereSalon = "";
                    string RemovedId = "";
                    string sql = "";
                    List<int> lstInt = new List<int>();
                    if (Perm_ViewAllData)
                    {
                        if (iSalon > 0)
                        {
                            WhereSalon = "o.SalonId = " + iSalon + " AND ";
                        }
                        else if (iRegion > 0)
                        {
                            lstInt = db.Tbl_Salon.Where(s => s.CityId == iRegion && s.IsDelete == 0 && s.Publish == true).Select(s => s.Id).ToList();
                            if (lstInt.Count > 0)
                            {
                                var result = String.Join(", ", lstInt);
                                WhereSalon = "o.SalonId IN (" + result + @") AND ";
                            }
                        }
                        else
                        {
                            WhereSalon = "o.SalonId > 0 AND";
                        }
                    }
                    else
                    {
                        lstInt = (
                                            from p in db.PermissionSalonAreas.AsNoTracking()
                                            join s in db.Tbl_Salon.AsNoTracking() on p.SalonId equals s.Id
                                            where
                                            p.IsDelete == false && p.StaffId == UserId
                                            && s.Publish == true &&
                                            s.IsDelete == 0
                                            && s.CityId == iRegion
                                            select s.Id
                                       ).ToList();
                        if (lstInt.Count > 0)
                        {
                            var result = String.Join(", ", lstInt);
                            WhereSalon = "o.SalonId IN (" + result + @") AND ";
                        }
                        if (iSalon > 0)
                        {
                            WhereSalon = "o.SalonId = " + iSalon + " AND ";
                        }

                    }
                    if (RemovedProductIdWhenExportExcel)
                    {
                        RemovedId = " AND q.ProductId NOT IN (508,520,521)";
                    }
                    else
                    {
                        RemovedId = " AND q.ProductId NOT IN (508)";
                    }
                    if (WhereSalon != "" && RemovedId != "")
                    {
                        sql = @"SELECT FORMAT(o.ExportTime,'dd/MM/yyyy') AS ReceivedTime, s.ShortName AS SalonName, p.Code, p.Name, ISNULL(SUM(q.QuantityExport),0) AS  QuantityExport
									FROM dbo.QLKho_SalonOrder_Flow AS q
									INNER JOIN dbo.QLKho_SalonOrder AS o ON o.Id = q.OrderId
									INNER JOIN dbo.Tbl_Salon AS s ON s.Id = o.SalonId
									INNER JOIN dbo.Product AS p ON p.Id = q.ProductId
									WHERE 
									" + WhereSalon + @"
									o.IsDelete = 0 AND q.IsDelete = 0 AND o.CheckCosmetic IS NULL
									AND s.Publish = 1 AND s.IsDelete = 0
									AND o.ExportTime IS NOT NULL
									AND o.ExportTime >= '" + TimeFrom + @"' AND o.ExportTime < '" + TimeTo + @"' 
                                    " + RemovedId + @"
									GROUP BY FORMAT(o.ExportTime,'dd/MM/yyyy'), s.ShortName, p.Code, p.Name, s.Id
									ORDER BY FORMAT(o.ExportTime,'dd/MM/yyyy') DESC, s.Id ASC";
                        lstExportSupplies = db.Database.SqlQuery<ExportSupplies>(sql).ToList();
                    }
                }

                return lstExportSupplies;
            }
        }

        public void BindData()
        {
            var data = GetData();
            if (data.Any())
            {
                Bind_Paging(data.Count);
                RptReportExport.DataSource = data.Skip(PAGING._Offset).Take(PAGING._Segment).OrderByDescending(o => o.ReceivedTime);
                RptReportExport.DataBind();
            }
            else
            {
                RptReportExport.DataSource = new List<ExportSupplies>();
                RptReportExport.DataBind();
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            BindData();
            RemoveLoading();
        }

        protected void Salon_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSalonWhereRegion();
        }

        private void BindSalonWhereRegion()
        {
            using (var db = new Solution_30shineEntities())
            {
                int iRegion = int.TryParse(ddlRegion.SelectedValue, out var integer) ? integer : 0;
                int UserId = Convert.ToInt32(HttpContext.Current.Session["User_Id"]);
                int SalonId = Convert.ToInt32(HttpContext.Current.Session["SalonId"]);
                List<Tbl_Salon> listSalon = new List<Tbl_Salon>();
                if (Perm_ViewAllData)
                {
                    if (iRegion > 0)
                    {
                        listSalon = db.Tbl_Salon.Where(w => w.IsDelete == 0 && w.Publish == true && w.CityId == iRegion).OrderByDescending(o => o.CityId).ToList();
                    }
                    else
                    {
                        listSalon = db.Tbl_Salon.Where(w => w.IsDelete == 0 && w.Publish == true).OrderByDescending(o => o.CityId).ToList();
                    }
                    var item = new Tbl_Salon();
                    item.Name = "Chọn tất cả salon";
                    item.Id = 0;
                    listSalon.Insert(0, item);
                }
                else
                {
                    listSalon = (
                                        from p in db.PermissionSalonAreas.AsNoTracking()
                                        join s in db.Tbl_Salon.AsNoTracking() on p.SalonId equals s.Id
                                        where
                                        p.IsDelete == false && p.StaffId == UserId
                                        && s.Publish == true &&
                                        s.IsDelete == 0
                                        && s.CityId == iRegion
                                        select s
                                   ).OrderBy(o => o.CityId).ToList();
                    if (!listSalon.Any())
                    {
                        listSalon = db.Tbl_Salon.Where(w => w.Id == SalonId).ToList();
                    }
                    var item = new Tbl_Salon();
                    item.Name = "Chọn tất cả salon";
                    item.Id = 0;
                    listSalon.Insert(0, item);
                }
                ddlSalon.DataTextField = "Name";
                ddlSalon.DataValueField = "Id";
                ddlSalon.DataSource = listSalon;
                ddlSalon.DataBind();
            }
        }
        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();
            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        /// <summary>
        /// Export Excell for misa
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Exc_ExportExcel(object sender, EventArgs e)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    if (TxtDateTimeFrom.Text.Trim() != "")
                    {
                        IEnumerable<ExportSupplies> lstExports = GetData(true);
                        var _ExcelHeadRow = new List<string>();
                        var RowLST = new List<List<string>>();
                        _ExcelHeadRow.Add("Ngày");
                        _ExcelHeadRow.Add("Salon");
                        _ExcelHeadRow.Add("Mã sản phẩm");
                        _ExcelHeadRow.Add("Sản phẩm");
                        _ExcelHeadRow.Add("Số lượng xuất");
                        if (lstExports.Any())
                        {
                            foreach (var v in lstExports)
                            {
                                var Row = new List<string>();
                                Row.Add(v.ReceivedTime.ToString());
                                Row.Add(v.SalonName);
                                Row.Add(v.Code);
                                Row.Add(v.Name);
                                Row.Add(v.QuantityExport.ToString());
                                RowLST.Add(Row);

                            }
                            var ExcelStorePath = Server.MapPath("~") + "Public/Excel/Hoa.Don/";
                            var FileName = "Bao_cao_xuat_vat_tu_" + string.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
                            var UrlFileName = "http://" + Request.Url.Host + "/Public/Excel/Hoa.Don/" + FileName;
                            ExportXcel(_ExcelHeadRow, RowLST, ExcelStorePath + FileName);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "');UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing(); alert(\"Xuất excel thành công.\")", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                new Helpers.PushNotiErrorToSlack().Push("GA1LZN9AM", ex.Message + ", MAIN: " + ex.StackTrace + ", " + Library.Function.JavaScript.Serialize("Input: " + TxtDateTimeFrom.Text + TxtDateTimeTo.Text + ddlRegion.SelectedValue + ddlSalon.SelectedValue),
                                                       this.ToString() + ".ReportExportSupplies", "dungnm",
                                                       HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_ExcelHeadRow"></param>
        /// <param name="List"></param>
        /// <param name="Path"></param>
        private static void ExportXcel(List<string> _ExcelHeadRow, List<List<string>> List, string Path)
        {
            try
            {
                CreateExcelFile.ExcelHeadRow = _ExcelHeadRow;
                CreateExcelFile.CreateExcel(List, Path);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi. Không tạo được file excel.\r\nException: " + ex.Message);
            }
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
    }
    public partial class ExportSupplies
    {
        public string ReceivedTime { get; set; }
        public string SalonName { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int QuantityExport { get; set; }
    }
}