﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Script.Serialization;
using ExportToExcel;
using Excel;
using LinqKit;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using System.IO;

namespace _30shine.GUI.BackEnd.Inventory
{
    public partial class Report_V2 : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        protected List<List<ProductBasic>> ProductList;
        private Expression<Func<ExportGood, bool>> Where = PredicateBuilder.True<ExportGood>();
        CultureInfo culture = new CultureInfo("vi-VN");
        private string sql = "";

        private string PageID = "TK_DL";
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;

        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

        protected string table = "";
        protected List<Product> products = new List<Product>();
        private JavaScriptSerializer serialize = new JavaScriptSerializer();

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (IsAccountant)
            {
                subMenu_liAdd.Visible = false;
            }

            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                //GenWhere();
                Bind_Paging();
                bindData();
            }
            else
            {
                //GenWhere();
            }
        }
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //Perm_ViewAllDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDate", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ViewAllDate = permissionModel.CheckPermisionByAction("Perm_ViewAllDate", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        private string genSql()
        {
            DateTime dtTimeFrom = Convert.ToDateTime("2016/8/1", culture);
            DateTime dtTimeTo = new DateTime();
            if (TxtDateTimeFrom.Text != "")
            {
                dtTimeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                if (TxtDateTimeTo.Text != "")
                {
                    dtTimeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                }
                else
                {
                    dtTimeTo = dtTimeFrom;
                }
            }
            else
            {
                dtTimeTo = DateTime.Now;
            }

            string whereSalon1 = "";
            string whereSalon2 = "";
            int SalonId = Convert.ToInt32(Salon.SelectedValue);
            if (SalonId > 0)
            {
                whereSalon1 += " and bill.SalonId = " + SalonId;
                whereSalon2 += " and iimport.SalonId = " + SalonId;
            }
            else
            {
                whereSalon1 += " and bill.SalonId > 0";
                whereSalon2 += " and iimport.SalonId > 0";
            }
            string initDate = "2016/8/19";
            string timeBefore = string.Format("{0:yyyy/MM/dd}", dtTimeFrom.AddDays(-1));
            string timeFrom = string.Format("{0:yyyy/MM/dd}", dtTimeFrom);
            string timeTo = string.Format("{0:yyyy/MM/dd}", dtTimeTo);
            string timeTo2 = string.Format("{0:yyyy/MM/dd}", dtTimeTo.AddDays(1));
            string sql = @"select invenflow.*, 
	                        (invenflow.ImportBefore - invenflow.ExportBefore - invenflow.SellBefore) as InvenBefore,
	                        ((invenflow.ImportBefore - invenflow.ExportBefore - invenflow.SellBefore) + invenflow.ImportInTime - invenflow.ExportInTime - invenflow.SellInTime) as InvenNow,
	                        product.Name as ProductName
                        from
                        (
	                        select product.Id as ProductId,
		                        Coalesce(SUM(
			                        case 
				                        when iimport.ImportType = 1 and iimport.ImportDate between '" + initDate + @"' and '" + timeBefore + @"' then Coalesce(iflow.Quantity,0)
			                        end
		                        ),0) as ImportBefore,
		                        Coalesce(SUM(
			                        case 
				                        when iimport.ImportType = 2 and iimport.ImportDate between '" + initDate + @"' and '" + timeBefore + @"' then Coalesce(iflow.Quantity,0)
			                        end
		                        ),0) as ExportBefore,
		                        Coalesce((
			                        select SUM(Coalesce(fproduct.Quantity,0))
			                        from FlowProduct as fproduct
			                        inner join BillService as bill
			                        on fproduct.BillId = bill.Id and fproduct.IsDelete != 1 
			                        where  ((fproduct.CheckCombo is null) or (fproduct.CheckCombo = 0 ))  and bill.IsDelete != 1 and bill.Pending != 1 and fproduct.IsDelete != 1" +
                                        whereSalon1 +
                                        @" and bill.CompleteBillTime between '" + initDate + @"' and '" + timeFrom + @"'
				                        and fproduct.ProductId = product.Id
			                        group by fproduct.ProductId
		                        ),0) SellBefore,
		                        Coalesce(SUM(
			                        case 
				                        when iimport.ImportType = 1 and iimport.ImportDate between '" + timeFrom + "' and '" + timeTo + @"' then Coalesce(iflow.Quantity,0)
			                        end
		                        ),0) as ImportInTime,
		                        Coalesce(SUM(
			                        case 
				                        when iimport.ImportType = 2 and iimport.ImportDate between '" + timeFrom + @"' and '" + timeTo + @"' then Coalesce(iflow.Quantity,0)
			                        end
		                        ),0) as ExportInTime,
		                        Coalesce((
			                        select SUM(Coalesce(fproduct.Quantity,0))
			                        from FlowProduct as fproduct
			                        inner join BillService as bill
			                        on fproduct.BillId = bill.Id and fproduct.IsDelete != 1 
			                        where  
                                             ((fproduct.CheckCombo is null) or(fproduct.CheckCombo = 0))  and

                                            bill.IsDelete != 1 and bill.Pending != 1 and fproduct.IsDelete != 1" +
                                        whereSalon1 +
                                        @" and bill.CompleteBillTime between '" + timeFrom + @"' and '" + timeTo2 + @"'
				                        and fproduct.ProductId = product.Id
			                        group by fproduct.ProductId
		                        ),0) SellInTime
	                        from Product as product
                            left join Inventory_Flow as iflow
                            on product.Id = iflow.ProductId and iflow.IsDelete != 1
	                        left join Inventory_Import as iimport
	                        on iflow.InvenId = iimport.Id" +
                            whereSalon2 +
                            @" where product.IsDelete != 1
                            and ((product.MapIdProduct is null) or (product.MapIdProduct = ''))
                            AND product.Publish = 1
	                        group by product.Id
                        ) as invenflow
                        left join Product as product
                        on invenflow.ProductId = product.Id
                        where invenflow.ImportBefore != 0 or invenflow.ExportBefore != 0 or invenflow.SellBefore != 0 
	                        or invenflow.ImportInTime != 0 or invenflow.ExportInTime != 0 or invenflow.SellInTime != 0
                            AND product.Publish = 1
                        order by product.Name asc";
            return sql;
        }
        //((fproduct.CheckCombo is null) or(fproduct.CheckCombo = 1))  and
        //fproduct.CheckCombo is null or fproduct.CheckCombo = 1  and
        
        private List<Library.Class.cls_inventory_report> getData(bool isPaging)
        {
            var list = new List<Library.Class.cls_inventory_report>();
            using (var db = new Solution_30shineEntities())
            {
                var sql = genSql();
                if (sql != "")
                {
                    if (isPaging)
                        list = db.Database.SqlQuery<Library.Class.cls_inventory_report>(sql).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                    else
                        list = db.Database.SqlQuery<Library.Class.cls_inventory_report>(sql).ToList();
                }
            }
            return list;
        }

        public void bindData()
        {
            RptInventory.DataSource = getData(true);
            RptInventory.DataBind();
        }


        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
            //Bind_DataByDate();
            bindData();
            ScriptManager.RegisterStartupScript(UPTableListing, this.GetType(), "Format Price", "", true);
            RemoveLoading();
        }

        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? (HDF_Page.Value != "" ? Convert.ToInt32(HDF_Page.Value) : 1) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                int Count = 0;
                string sql = genSql();
                if (sql != "")
                {
                    Count = db.Database.SqlQuery<Library.Class.cls_inventory_report>(sql).Count();
                }
                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        public string GenTime(int minutes)
        {
            string time = "";
            int hour = minutes / 60;
            int minute = minutes % 60;
            if (hour > 0)
            {
                time += hour + "h";
                time += minute + "p";
            }
            else
            {
                if (minute > 0)
                {
                    time += minute + " p";
                }
                else
                {
                    time += "-";
                }
            }
            return time;
        }

        protected void Exc_ExportExcel(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {

                var data = getData(false);

                var _ExcelHeadRow = new List<string>();
                var serializer = new JavaScriptSerializer();
                var BillRowLST = new List<List<string>>();
                var BillRow = new List<string>();

                _ExcelHeadRow.Add("Mỹ phẩm");
                _ExcelHeadRow.Add("Tồn đầu kỳ");
                _ExcelHeadRow.Add("Nhập từ kho");
                _ExcelHeadRow.Add("Trả lại kho");
                _ExcelHeadRow.Add("Bán ra");
                _ExcelHeadRow.Add("Tồn cuối kỳ");

                if (data.Count > 0)
                {
                    foreach (var v in data)
                    {
                        BillRow = new List<string>();
                        BillRow.Add(v.ProductName);
                        BillRow.Add(v.InvenBefore.ToString());
                        BillRow.Add(v.ImportInTime.ToString());
                        BillRow.Add(v.ExportInTime.ToString());
                        BillRow.Add(v.SellInTime.ToString());
                        BillRow.Add(v.InvenNow.ToString());
                        BillRowLST.Add(BillRow);
                    }
                }

                // export
                var ExcelStorePath = Server.MapPath("~") + "Public/Excel/Hoa.Don/";
                var FileName = "Hoa_Don_" + string.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
                var UrlFileName = "http://" + Request.Url.Host + "/Public/Excel/Hoa.Don/" + FileName;

                ExportXcel(_ExcelHeadRow, BillRowLST, ExcelStorePath + FileName);
                Bind_Paging();
                ScriptManager.RegisterStartupScript(BtnFakeExcel, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "');UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing();", true);
            }
        }

        private static void ExportXcel(List<string> _ExcelHeadRow, List<List<string>> List, string Path)
        {
            try
            {
                CreateExcelFile.ExcelHeadRow = _ExcelHeadRow;
                CreateExcelFile.CreateExcel(List, Path);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi. Không tạo được file excel.\r\nException: " + ex.Message);
            }
        }

        
        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }

    }
}