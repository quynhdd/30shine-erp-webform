﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Helpers;
using System.Text.RegularExpressions;
using System.Globalization;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Inventory
{
    public partial class Import : System.Web.UI.Page
    {
        private string PageID = "";
        protected bool Perm_ShowSalon = false;
        protected bool Perm_Access = false;
        protected bool Disable = false;

        private List<ProductBasic> ProductList = new List<ProductBasic>();
        protected int SalonId = 0;
        protected int Id;
        protected List<Product> ProductSellLastDay = new List<Product>();
        private int integer;
        private CultureInfo culture = new CultureInfo("vi-VN");

        private Inventory_Import OBJ = new Inventory_Import();

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            Id = Library.Function.getIdFromQueryString();
            if (Id > 0 && !Perm_ShowSalon)
            {
                BtnSend.Visible = false;
                quickProduct.Visible = false;
                Description.Enabled = false;
                Disable = true;
            }

            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ShowSalon);
                Bind_OBJ();
                Bind_RptProduct();
                Bind_RptProductFeatured();
                bindProductCategory();                
            }
        }
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //if (Request.QueryString["Code"] != null)
                //{
                //    PageID = "TK_ADD";
                //}
                //else
                //{
                //    PageID = "TK_EDIT";
                //}
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ShowSalon = permissionModel.GetActionByActionNameAndPageId("Perm_ShowSalon", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                pageId = permissionModel.GetRegexPageIdSpecial().Replace(pageId, ".html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        /// <summary>
        /// get Id đăng nhập và chuyển mỹ phẩm
        /// </summary>
        /// <returns></returns>
        private int getExportUserId()
        {
            if (Session["User_Id"] != null)
            {
                return int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// get Id người nhận mỹ phẩm
        /// </summary>
        /// <returns></returns>
        private int getRecipientId()
        {
            return int.TryParse(Recipient.Value, out integer) ? integer : 0;
        }

        /// <summary>
        /// Cập nhật lô mỹ phẩm
        /// </summary>
        /// <param name="Id"></param>
        private void Update(int Id)
        {
            if (Id > 0)
            {
                using (var db = new Solution_30shineEntities())
                {
                    var serialize = new JavaScriptSerializer();
                    var error = 0;
                    OBJ = db.Inventory_Import.FirstOrDefault(w=>w.Id == Id);
                    if (OBJ != null)
                    {
                        OBJ.SalonId = Library.Function.getSalonId(Perm_ShowSalon, Salon.SelectedValue);
                        OBJ.ModifiedDate = DateTime.Now;
                        OBJ.RecipientId = getRecipientId();
                        OBJ.Note = Description.Text;
                        OBJ.ProductIds = HDF_ProductIds.Value != "[]" ? HDF_ProductIds.Value : "";
                        OBJ.ProductIds = serialize.Serialize(Library.Function.genListFromJson(OBJ.ProductIds));
                        OBJ.ModifiedDate = DateTime.Now;
                        if (TxtDateTimeFrom.Text != "")
                        {
                            OBJ.ImportDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                        }                            
                             
                        db.Inventory_Import.AddOrUpdate(OBJ);
                        error += db.SaveChanges() > 0 ? 0 : 1;
                        if (error == 0)
                        {
                            error += UpdateFlow(OBJ.ProductIds, OBJ.Id);
                            // Cập nhật thống kê tồn kho
                            Library.Function.updateInventoryByItemUpdate(db, OBJ.ImportDate.Value.Date, OBJ.SalonId.Value, OBJ.Id, OBJ.ImportType.Value, false);
                        }
                        if (error == 0)
                        {
                            // Thông báo thành công
                            var MsgParam = new List<KeyValuePair<string, string>>();
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                            MsgParam.Add(new KeyValuePair<string, string>("id", Id.ToString()));
                            UIHelpers.Redirect("/ton-kho/nhap-moi.html", MsgParam);
                        }
                        else
                        {
                            MsgSystem.Text = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                            MsgSystem.CssClass = "msg-system warning";
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Xuất lô mỹ phẩm
        /// </summary>
        private void Add()
        {
            using (var db = new Solution_30shineEntities())
            {
                var obj = new Inventory_Import();
                var serialize = new JavaScriptSerializer();
                var error = 0;
                obj.SalonId = Library.Function.getSalonId(Perm_ShowSalon, Salon.SelectedValue);
                obj.DoId = getExportUserId();
                obj.RecipientId = getRecipientId();
                obj.ProductIds = HDF_ProductIds.Value != "[]" ? HDF_ProductIds.Value : "";
                obj.ProductIds = serialize.Serialize(Library.Function.genListFromJson(obj.ProductIds));
                obj.ImportType = Convert.ToInt32(importType.SelectedValue);
                obj.IsDelete = false;
                obj.CreatedDate = DateTime.Now;
                if (TxtDateTimeFrom.Text != "")
                {
                    obj.ImportDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                }
                else
                {
                    obj.ImportDate = DateTime.Now;
                }                
                obj.Note = Description.Text;
                db.Inventory_Import.Add(obj);
                error += db.SaveChanges() > 0 ? 0 : 1;
                if (error == 0)
                {
                    error += AddFlow(obj.ProductIds, obj.Id);
                    // Cập nhật thống kê tồn kho
                    Library.Function.updateInventoryByItemUpdate(db, obj.ImportDate.Value.Date, obj.SalonId.Value, obj.Id, obj.ImportType.Value, false);
                }

                if (error == 0)
                {
                    // Thông báo thành công
                    var MsgParam = new List<KeyValuePair<string, string>>();
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                    UIHelpers.Redirect("/ton-kho/nhap-moi.html", MsgParam);
                }
                else
                {
                    MsgSystem.Text = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                    MsgSystem.CssClass = "msg-system warning";
                }
            }
        }

        private int AddFlow(string goodsIds, int ExportGoodsId)
        {
            var error = 0;
            if (goodsIds != "")
            {
                using (var db = new Solution_30shineEntities())
                {
                    var obj = new Inventory_Flow();
                    var list = Library.Function.genListFromJson(goodsIds);
                    var product = new Product();
                    if (list.Count > 0)
                    {
                        foreach (var v in list)
                        {
                            product = db.Products.FirstOrDefault(w=>w.Id == v.Id);
                            obj = new Inventory_Flow();
                            obj.InvenId = ExportGoodsId;
                            obj.ProductId = v.Id;
                            obj.Quantity = v.Quantity;
                            obj.Cost = v.Price;
                            if (product != null)
                            {
                                obj.Price = Convert.ToInt32(product.Price);
                            }
                            if (TxtDateTimeFrom.Text != "")
                            {
                                obj.CreatedDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                            }
                            else
                            {
                                obj.CreatedDate = DateTime.Now;
                            }
                            obj.IsDelete = false;
                            db.Inventory_Flow.Add(obj);
                            error += db.SaveChanges() > 0 ? 0 : 1;
                        }
                    }                    
                }
            }
            return error;
        }

        private int UpdateFlow(string productIds, int InvenId)
        {
            var error = 0;
            if (productIds != "")
            {
                using (var db = new Solution_30shineEntities())
                {
                    var obj = new ProductBasic();
                    var list = Library.Function.genListFromJson(productIds);
                    var flow = db.Inventory_Flow.Where(w => w.InvenId == InvenId && w.IsDelete != true).ToList();
                    var index = -1;
                    var product = new Product();
                    // Update/Delete from flow
                    if (flow.Count > 0)
                    {
                        foreach (var v in flow)
                        {
                            product = db.Products.FirstOrDefault(w=>w.Id == v.Id);
                            index = list.FindIndex(w => w.Id == v.ProductId);
                            if (index == -1)
                            {
                                v.IsDelete = true;
                                db.Inventory_Flow.AddOrUpdate(v);
                                error += db.SaveChanges() > 0 ? 0 : 1;
                            }
                            else
                            {
                                obj = list[index];
                                v.Quantity = obj.Quantity;                                                                
                                if (product != null)
                                {
                                    v.Cost = product.Cost;
                                    v.Price = Convert.ToInt32(product.Price);
                                }
                                v.ModifiedDate = DateTime.Now;
                                db.Inventory_Flow.AddOrUpdate(v);
                                error += db.SaveChanges() > 0 ? 0 : 1;
                            }
                        }
                    }
                    // Add to flow
                    if (list.Count > 0)
                    {
                        var invenFlow = new Inventory_Flow();
                        foreach (var v in list)
                        {
                            product = db.Products.FirstOrDefault(w=>w.Id == v.Id);
                            index = flow.FindIndex(w=>w.ProductId == v.Id);
                            if (index == -1)
                            {
                                invenFlow = new Inventory_Flow();
                                invenFlow.InvenId = InvenId;
                                invenFlow.ProductId = v.Id;
                                invenFlow.Quantity = v.Quantity;
                                if (product != null)
                                {
                                    invenFlow.Cost = product.Cost;
                                    invenFlow.Price = product.Price;
                                }                                
                                invenFlow.CreatedDate = DateTime.Now;
                                invenFlow.IsDelete = false;
                                db.Inventory_Flow.Add(invenFlow);
                                db.SaveChanges();
                            }
                        }
                    }
                }
            }
            return error;
        }
        
        protected void Action(object sender, EventArgs e)
        {
            if (Id > 0)
            {
                Update(Id);
            }
            else
            {
                Add();
            }
        }

        private bool Bind_OBJ()
        {
            var isset = false;
            using (var db = new Solution_30shineEntities())
            {
                if (Id > 0)
                {
                    TxtDateTimeFrom.Enabled = false;
                    importType.Enabled = false;
                    Salon.Enabled = false;

                    OBJ = db.Inventory_Import.FirstOrDefault(w => w.Id == Id);
                    if (OBJ != null)
                    {
                        isset = true;
                        if (OBJ.ImportDate != null)
                        {
                            TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", OBJ.ImportDate);
                        }
                        // bind salon selected
                        var ItemSelected = Salon.Items.FindByValue(OBJ.SalonId.ToString());
                        if (ItemSelected != null)
                        {
                            ItemSelected.Selected = true;
                        }
                        else
                        {
                            Salon.SelectedIndex = 0;
                        }
                        // bind người nhận mỹ phẩm
                        if (OBJ.RecipientId > 0)
                        {
                            var staff = db.Staffs.FirstOrDefault(w => w.Id == OBJ.RecipientId);
                            if (staff != null)
                            {
                                FVRecipient.InnerText = staff.OrderCode.ToString() + "- " + staff.Fullname;
                                InputRecipient.Text = staff.OrderCode.ToString();
                                Recipient.Value = staff.Id.ToString();
                            }
                        }
                        // bind danh sách mỹ phẩm
                        if (OBJ.ProductIds != "")
                        {
                            Rpt_Product_Flow.DataSource = Library.Function.genListFromJson(OBJ.ProductIds);
                            Rpt_Product_Flow.DataBind();
                            ListingProductWp.Style.Add("display", "block");
                        }
                        // bind note
                        Description.Text = OBJ.Note;
                    }
                    else
                    {
                        TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Today);
                    }

                    if (!isset)
                    {
                        MsgSystem.Text = "Lỗi. Dữ liệu không tồn tại.";
                        MsgSystem.CssClass = "msg-system warning";
                    }
                }
                else
                {
                    TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Today);
                }             
            }            

            return isset;
        }

        private void Bind_RptProduct()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Products = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1).OrderByDescending(o => o.Id).ToList();

                Rpt_Product.DataSource = _Products;
                Rpt_Product.DataBind();
            }
        }

        private void Bind_RptProductFeatured()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1 && w.Status == 1).OrderByDescending(o => o.Id).ToList();

                Rpt_ProductFeatured.DataSource = lst;
                Rpt_ProductFeatured.DataBind();
            }
        }

        private void bindProductCategory()
        {
            using (var db = new Solution_30shineEntities())
            {
                //var list = db.Tbl_Category.Where(w=>w.Pid == 1 && w.IsDelete != 1 && w.Publish == true).ToList();
                ////var c = new Tbl_Category();
                ////c.Id = 0;
                ////c.Name = "Chọn danh mục";
                ////list.Insert(0, c);
                //rptProductCategory.DataSource = list;
                //rptProductCategory.DataBind();
                var list = new List<Tbl_Category>();
                var node = new List<Tbl_Category>();
                var c = new Tbl_Category();
                c.Id = 0;
                c.Name = "Chọn danh mục";
                list.Insert(0, c);
                var cateRoot = db.Tbl_Category.Where(w => w.IsDelete != 1 && w.Pid == 1 && w.Publish == true && w.Id != 20).ToList();
                if (cateRoot.Count > 0)
                {
                    foreach (var v in cateRoot)
                    {
                        node = db.Tbl_Category.Where(w => w.IsDelete != 1 && w.Pid == v.Id).ToList();
                        list.Add(v);
                        if (node.Count > 0)
                        {
                            foreach (var v2 in node)
                            {
                                v2.Name = "----- " + v2.Name;
                                list.Add(v2);
                            }
                        }
                    }
                }
                rptProductCategory.DataSource = list;
                rptProductCategory.DataBind();
            }
        }

        /// <summary>
        /// Reset Rating Temp Value
        /// </summary>
        private void resetRatingTemp()
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                /// Reset giá trị rating temp
                int accId = Session["User_Id"] != null ? int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0 : 0;
                int salonId = Session["SalonId"] != null ? int.TryParse(Session["SalonId"].ToString(), out integer) ? integer : 0 : 0;
                var ratingTemp = db.Rating_Temp.FirstOrDefault(w => w.SalonId == salonId && w.AccountId == accId);
                if (ratingTemp != null)
                {
                    ratingTemp.RatingValue = 0;
                    db.Rating_Temp.AddOrUpdate(ratingTemp);
                    db.SaveChanges();
                }
            }
        }

        [WebMethod]
        public static string GetCustomer(string CustomerCode)
        {
            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var _Customer = db.Customers.Where(w => w.Customer_Code == CustomerCode).ToList();

                if (_Customer.Count > 0)
                {
                    _Msg.success = true;
                    _Msg.msg = serializer.Serialize(_Customer);
                }
                else
                {
                    _Msg.success = false;
                }
            }
            return serializer.Serialize(_Msg);
        }

        [WebMethod]
        public static string CheckDuplicateBill(string CustomerCode)
        {
            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var today = Convert.ToDateTime(DateTime.Now.ToString("d/M/yyyy"), culture);
                var todayL = today.AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                var todayR = today.AddDays(1);
                var _Bill = db.BillServices.FirstOrDefault(w => w.CreatedDate > todayL && w.CreatedDate < todayR && w.CustomerCode == CustomerCode);

                if (_Bill != null)
                {
                    _Msg.success = true;
                    _Msg.msg = serializer.Serialize(_Bill);
                }
                else
                {
                    _Msg.success = false;
                }
            }
            return serializer.Serialize(_Msg);
        }

        [WebMethod]
        public static object getProductSellLastDay(string date, int salonId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var day = Convert.ToDateTime(date, new CultureInfo("vi-VN"));
                return Library.Function.getListProductSellLastDay(db, day.Date, salonId);
            }
        }

        
    }
}