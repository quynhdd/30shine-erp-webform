﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Globalization;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Inventory
{
    public partial class QLKho_Export : System.Web.UI.Page
    {
        private DateTime timeFrom;
        private DateTime timeTo;
        protected bool Perm_ShowSalon = false;
        protected bool IsAccountant = false;
        private bool Perm_Access = false;
        private string PageID = "Inven_QLKho";
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        private bool Perm_ViewAllData = false;
        CultureInfo culture = new CultureInfo("vi-VN");
        protected Paging PAGING = new Paging();
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
                Bind_Pro();
                TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Today);
            }
        }
        #region[BindProduct]
        private void Bind_Pro()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _list = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1 && w.CategoryId != 95).ToList();
                if (_list.Count > 0)
                {
                    ddlProduct.Items.Clear();
                    ddlProduct.Items.Add(new ListItem("--- Chọn sản phẩm ---", "0"));
                    for (int i = 0; i < _list.Count; i++)
                    {
                        ddlProduct.Items.Add(new ListItem(_list[i].Name, _list[i].Id.ToString()));
                    }
                    ddlProduct.DataBind();
                }
            }
        }
        #endregion
    
        #region[Bindata]
        /// <summary>
        /// Bind data
        /// </summary>
        private void Bind_Data()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    if (TxtDateTimeFrom.Text != "")
                    {
                        timeFrom = Library.Format.getDateTimeFromString(TxtDateTimeFrom.Text).Value;
                        if (TxtDateTimeTo.Text != "")
                        {
                            timeTo = Library.Format.getDateTimeFromString(TxtDateTimeTo.Text).Value.AddDays(1);
                        }
                        else
                        {
                            timeTo = timeFrom.AddDays(1);
                        }
                        int saloId = Convert.ToInt32(ddlSalon.SelectedValue);
                        int productId = Convert.ToInt32(ddlProduct.SelectedValue);
                        var data = db.Store_QLKhoExprot_HC(timeFrom, timeTo, saloId, productId).ToList();
                        Bind_Paging(data.Count);
                        Rpt_Product.DataSource = data.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                        Rpt_Product.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
        #endregion
        #region[ButtonAnd Paging]
        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Data();
            RemoveLoading();
        }
        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
        #endregion
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var msgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", msgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //////Perm_Add = permissionModel.GetActionByActionNameAndPageId("Perm_Add", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
    }
}