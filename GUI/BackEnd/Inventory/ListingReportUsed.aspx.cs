﻿using _30shine.Helpers;
using _30shine.Helpers.Http;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using Libraries;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.Inventory
{
    public partial class ListingReportUsed : System.Web.UI.Page
    {
        private Solution_30shineEntities db;
        private bool Perm_Edit = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;
        protected Paging PAGING = new Paging();
        CultureInfo culture = new CultureInfo("vi-VN");
        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
        public List<Origin> listOrigins = new List<Origin>();
        public int index = 1;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ViewAllDate = permissionModel.CheckPermisionByAction("Perm_ViewAllDate", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
                Library.Function.bindSalon_NotHoiQuan(new List<DropDownList>() { ddlSalon }, Perm_ViewAllData);
                BindDepartment();
                BindService();
            }
        }

        /// <summary>
        /// Bind danh sách bộ phận
        /// </summary>
        protected void BindDepartment()
        {
            using (var db = new Solution_30shineEntities())
            {
                //chỉ lấy bộ phận stylist, skinner
                var lstDepartment = db.Staff_Type.Where(w => w.IsDelete == 0 && w.Publish == true).OrderBy(o => o.Id).ToList();
                var item = new Staff_Type();
                item.Name = "Chọn tất cả bộ phận";
                item.Id = 0;
                lstDepartment.Insert(0, item);
                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "Id";
                ddlDepartment.DataSource = lstDepartment;
                ddlDepartment.DataBind();
            }
        }

        /// <summary>
        /// Bind danh sách dịch vụ
        /// </summary>
        protected void BindService()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lstService = db.Services.Where(w => w.IsDelete == 0 && w.Publish == 1).OrderBy(o => o.Id).ToList();
                var item = new Service();
                item.Name = "Chọn tất cả dịch vụ";
                item.Id = 0;
                lstService.Insert(0, item);
                ddlService.DataTextField = "Name";
                ddlService.DataValueField = "Id";
                ddlService.DataSource = lstService;
                ddlService.DataBind();
            }
        }

        /// <summary>
        /// Get data
        /// </summary>
        public void GetData()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    if (!String.IsNullOrEmpty(TxtDateTimeFrom.Text))
                    {
                        int integer; DateTime timeFrom, timeTo;
                        timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                        if (!String.IsNullOrEmpty(TxtDateTimeTo.Text))
                        {
                            timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                        }
                        else
                        {
                            timeTo = timeFrom;
                        }
                        int salonId = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                        int departmentId = int.TryParse(ddlDepartment.SelectedValue, out integer) ? integer : 0;
                        int serviceId = int.TryParse(ddlService.SelectedValue, out integer) ? integer : 0;
                        var getData = new Request().RunGetAsyncV1(AppConstants.URL_API_INVENTORY + $"/api/product-used-statistic/list-data?salonId={salonId}&deparmentId={departmentId}&serviceId={serviceId}&timeFrom={timeFrom}&timeTo={timeTo}").Result;
                        if (getData.IsSuccessStatusCode)
                        {
                            //response data
                            var origin = getData.Content.ReadAsStringAsync().Result;
                            JavaScriptSerializer serializer = new JavaScriptSerializer();
                            var responseData = serializer.Deserialize<List<Origin>>(origin);
                            if (responseData != null)
                            {
                                Bind_Paging(listOrigins.Count);
                                listOrigins = responseData.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// _BtnClick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _BtnClick(object sender, EventArgs e)
        {
            GetData();
            RemoveLoading();
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "finishLoading();", true);
        }

        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();
            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }
    }

    public class Origin
    {
        public int salonId { get; set; }
        public string salonName { get; set; }
        public int departmentId { get; set; }
        public string department { get; set; }
        public int staffId { get; set; }
        public string fullName { get; set; }
        public int serviceId { get; set; }
        public string serviceName { get; set; }
        public int totalService { get; set; }
        public List<ListProducts> products { get; set; }
    }

    public partial class ListProducts
    {
        public int productId { get; set; }
        public string productName { get; set; }
        public double volume { get; set; }
        public double quantifyDefault { get; set; }
        public double tonDau { get; set; }
        public double exportToStaff { get; set; }
        public double dungTrongNgay { get; set; }
        public double tonCuoi { get; set; }
        public double totalOrder { get; set; }
    }
}