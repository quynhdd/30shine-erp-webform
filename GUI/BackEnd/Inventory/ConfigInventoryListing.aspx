﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="ConfigInventoryListing.aspx.cs" Inherits="_30shine.GUI.BackEnd.Inventory.ConfigInventoryListing" %>

<asp:Content ID="CustomerAdd" ContentPlaceHolderID="CtMain" runat="server">

    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .sub-menu ul.ul-sub-menu li:first-child {
                font-size: 18px;
            }

            .customer-add .history-timeline .line-h {
                left: 65px !important;
                background: #50b347;
                height:467% !important;
            }

            .customer-add .history-timeline .line-v {
                top: 85px !important;
            }

            .customer-add .history-timeline .time-value {
                display: inline-block !important;
                border-radius: 50%;
                width: 130px;
                height: 130px;
            }

                .customer-add .history-timeline .time-value p {
                    text-align: center;
                    line-height: 1.1;
                    margin-top: 57px;
                    padding: 0px 10px;
                    word-wrap: break-word;
                    color: white;
                    font-size: 12px;
                    font-family: sans-serif;
                }

            .customer-add .table-add {
                margin-left: 10px;
            }

            .line-h {
                z-index: 100 !important;
            }

            .history-timeline {
                overflow: scroll !important;
                width: 100% !important;
                float: left !important;
                height: auto !important;
                min-height: 700px !important;
                white-space: nowrap;
                max-height:750px !important;
            }

            .wrap {
                display: inline-block;
                width: 200px;
                height: 100px;
                margin-left: 30em;
            }

            .wrap-main {
                margin-top: 20px;
            }

            .wrap-product {
                display: inline-block;
                width: 500px;
                height: 100px;
                margin-left: 30em;
            }

            .imageapent {
                width: 15px;
                height: 15px;
            }

            .image12 {
                width: 30px;
                height: 30px;
            }

            .degree90 {
                -webkit-transform: rotate(90deg);
                -moz-transform: rotate(90deg);
                -o-transform: rotate(90deg);
                -ms-transform: rotate(90deg);
                transform: rotate(90deg);
                height: 118px !important;
                width: 118px !important;
            }

            .downImage .btn-filter:hover {
                color: #ffd800;
            }

            .select2-container {
                width: 230px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }

            p {
                word-break: break-all !important;
                white-space: normal !important;
            }

            .customer-add .history-timeline table tr {
                background: none;
                background-color: white;
                border: 1px solid white;
            }

            .customer-add .history-timeline .line-v {
                width: 100%;
            }
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Danh sách cấu hình định lượng &nbsp;&#187; Dịch vụ &#187;</li>
                        <li><a href="/admin/cau-hinh-dinh-luong/them-moi.html"><i class="fa fa-list-ul"></i>&nbsp Thêm mới định lượng</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing be-report be-report-timekeeping">
            <!-- Customer History -->
            <div class="wp960 content-wp">
                <div class="row">
                    <strong class="st-head"><i class="fa fa-filter"></i>Lọc kết quả &nbsp;&#187; </strong>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlDepartment" CssClass="form-control select" runat="server" ClientIDMode="Static"></asp:DropDownList>
                    </div>
                    <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" ClientIDMode="Static" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <asp:Button ID="BtnFakeFilter" OnClick="GetData" runat="server"
                        ClientIDMode="Static" Style=""></asp:Button>
                </div>
                <%foreach (var item in ListOutputData)
                    {%>
                <div class="container">
                    <div class="row">
                        <!-- History -->
                        <div class="history-timeline">
                            <table class="cls_table">
                                <thead>
                                    <tr>
                                        <th>
                                            <div class="wrap-main">
                                                <div class="time-value" style="font-family: Roboto Condensed Bold; font-size: 15px;">
                                                    <p>Dịch vụ</p>
                                                </div>
                                                <div class="line-v"></div>
                                            </div>
                                        </th>
                                        <%foreach (var ser in ListOutputService)
                                            {%>
                                            <th>
                                                <div class="wrap">
                                                    <div class="time-value" style="font-family: Roboto Condensed Bold; font-size: 15px;">
                                                        <p class="<%=ser.ServiceId %>""><%=ser.ServiceName %></p>
                                                        <div class="line-h"></div>
                                                    </div>
                                                    <div class="line-v"></div>
                                                </div>
                                            </th>
                                        <%}%>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <%foreach (var ser in ListOutputService)
                                            {%>
                                        <td>
                                          <table class="table-add table-listing" style="margin-top: 70px;margin-left: 16em;">
                                            <tbody>
                                                <tr class="tr-service-color">
                                                    <td>
                                                        <table class="table table-listing-product table-item-product" id="table-item-product">                                               
                                                            <thead>
                                                                <tr>
                                                                    <th>STT</th>
                                                                    <th class="maSPcol">Mã sản phẩm</th>
                                                                    <th>Tên sản phẩm</th>
                                                                    <th style="width: 100px">Trọng lượng</th>
                                                                    <th style="width: 100px">Định lượng</th>
                                                                    <th style="width: 50px"></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <%foreach (var pro in ListOutputProduct.Where(w => w.ServiceId == ser.ServiceId))
                                                                    {%>
                                                                <tr>
                                                                    <td style="text-align: center;" class="td-product-index"><%= pro.Index%></td>
                                                                    <td class="td-product-code maSPcol" style="width: 120px;text-align: center;" data-id="<%=pro.Id %>"><%=pro.Code %></td>
                                                                    <td style="text-align: center;" class="td-product-name"><%=pro.Name %></td>
                                                                    <td style="text-align: center;" class="td-product-volume"><%=pro.Volume %></td>
                                                                    <td style="text-align: center;" class="td-product-quantify"><%=pro.Quantify %></td>
                                                                    <td class="map-edit">
                                                                        <div class="edit-wp">
                                                                            <a class="elm edit-btn" style="margin-top:0px;" href="/admin/cau-hinh-dinh-luong/<%=pro.Id %>.html" title="Sửa"></a>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <%}%>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                          </table>
                                        </td>
                                        <%}%>
                                    </tr>
                                </tbody>
                            </table>
                            <!--/ History -->
                        </div>
                    </div>
                </div>
                <%} %>
                <!--/ Customer History -->
                <asp:HiddenField runat="server" ID="HDF_CustomerId" ClientIDMode="Static" />
            </div>
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
            $("#ViewDataFilter").bind("click", function () {
                debugger;
                $("#BtnFakeFilter").click();
            });
        </script>
    </asp:Panel>
</asp:Content>
