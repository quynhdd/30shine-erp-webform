﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="InventoryHC_Add.aspx.cs" Inherits="_30shine.GUI.BackEnd.Inventory.InventoryHC_Add" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <title>Quản lý kho - Lập đơn đặt hàng</title>
    <link href="/Assets/css/bootstrap/bootstrap-toggle.min.css" rel="stylesheet" />
    <script src="/Assets/js/bootstrap/bootstrap-toggle.min.js"></script>

    <script src="/Assets/libs/magicsuggest-master/magicsuggest.js"></script>
    <link href="/Assets/libs/magicsuggest-master/magicsuggest.css" rel="stylesheet" />

    <style>
        .fe-service .tr-product .table-listing-product tbody input.product-quantity {
            border-bottom: 1px solid #ddd !important;
        }

        .suggestion-wrap {
            width: 550px;
            float: left;
        }

        .ms-sel-ctn input[type='text'] {
            height: auto !important;
            line-height: 24px !important;
            border: none !important;
        }

        .ms-ctn .ms-sel-item {
            line-height: 22px;
            color: #000000;
            font-size: 13px;
        }

        .ms-helper {
            display: none !important;
        }

        .input-quantity-wrap {
            float: left;
            margin-left: 10px;
        }

        .input-quantity {
            width: 90px;
            line-height: 36px;
            padding: 0 10px;
            border: 1px solid #ddd;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }

        .btn-add {
            float: left;
            background: #ddd;
            padding: 9px 15px;
            margin-left: 10px;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            cursor: pointer;
        }

        #success-alert {
            top: 100px;
            right: 10px;
            position: fixed;
            width: 20% !important;
            z-index: 200000;
        }

        .show-item:hover {
            cursor: pointer;
        }

        .table-listing-product {
            width: 70% !important;
        }
    </style>


</asp:Content>
<asp:Content ID="ServiceAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="alert alert-danger" id="success-alert">
            <span id="msg-alert"></span>
        </div>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý hàng tồn kho theo tuần &nbsp;&#187;</li>
                        <li class="be-report-li" id="Tonkho"><a href="/admin/hang-hoa/danh-sach-san-pham-ton.html"><i class="fa fa-th-large"></i>DS hàng tồn kho</a></li>
                        <li class="be-report-li" id="Mucdung"><a href="/admin/hang-hoa/danh-sach-muc-dung-san-pham.html"><i class="fa fa-th-large"></i>DS mức dùng SP</a></li>
                        <li class="li-add"><a href="/admin/hang-hoa/them-moi-vat-tu.html">Nhập hàng tồn</a></li>

                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add admin-product-add fe-service">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <!-- System Message -->
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <!-- END System Message -->

                <div class="table-wp">
                    <table class="table-add admin-product-table-add fe-service-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Nhập số lượng hàng</strong></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr class="tr-field-ahalf" runat="server" id="TrSalon">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right">
                                    <label style="float: left; line-height: 36px; padding-right: 10px; font-weight: normal; font-family: Roboto Condensed Bold;">Ngày</label>
                                    <div class="datepicker-wp">
                                        <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Chọn ngày"
                                            ClientIDMode="Static" runat="server" Style="width: 160px; margin-right: 10px;"></asp:TextBox>
                                    </div>
                                    <div class="salonSelect">
                                        <label style="float: left; line-height: 36px; padding-right: 10px; font-weight: normal; font-family: Roboto Condensed Bold;">Salon</label>
                                        <div class="field-wp" style="float: left; width: auto;">
                                            <asp:DropDownList ID="Salon" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="width: 160px;"></asp:DropDownList>
                                            <asp:RequiredFieldValidator InitialValue="0"
                                                ID="ValidateSalon" Display="Dynamic"
                                                ControlToValidate="Salon"
                                                runat="server" Text="Bạn chưa chọn Salon!"
                                                ErrorMessage="Vui lòng chọn Salon!"
                                                ForeColor="Red"
                                                CssClass="fb-cover-error">
                                            </asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-2 left"><span>Tìm kiếm sản phẩm</span></td>
                                <td class="col-xs-9 right">
                                    <div class="suggestion-wrap">
                                        <input id="ms" <%=this.disable %> class="form-control" type="text" />
                                    </div>
                                    <div class="input-quantity-wrap">
                                        <input type="number" <%=this.disable %> id="inputQuantity" class="input-quantity" placeholder="Số lượng" />
                                    </div>
                                    <div class="btn-add" onclick="addProduct()">Thêm sản phẩm</div>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Danh mục sản phẩm</span></td>
                                <td class="col-xs-9 right">
                                    <div class="show-product show-item" data-item="product"><i class="fa fa-plus-circle" style="margin-right: 5px;"></i>Thêm sản phẩm</div>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf tr-product">
                                <td class="col-xs-2 left"><span></span></td>
                                <td class="col-xs-12 left">
                                    <div class="listing-product item-product" id="ListingProductWp" runat="server" clientidmode="Static">
                                        <table class="table table-listing-product table-item-product" id="table-item-product">
                                            <thead>
                                                <tr>
                                                    <th>STT</th>
                                                    <th class="maSPcol" style="display: none;">Mã sản phẩm</th>
                                                    <th>Tên sản phẩm</th>
                                                    <th>Giá sản phẩm</th>
                                                    <th style="width: 100px;">Số lượng</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="Rpt_Product" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                                            <td class="td-product-code maSPcol" style="display: none;" data-id="<%# Eval("Id") %>"></td>
                                                            <td class="td-product-name" data-cost="<%#Eval("Cost") %>"><%# Eval("Name") %></td>
                                                            <td class="td-product-price" id="box-money" data-price="<%#Eval("Price") %>">
                                                                <%# string.Format("{0:#,0.##}", Eval("Price")) %></td>
                                                            <td class="td-product-quantity map-edit">
                                                                <input type="text" class="product-quantity" value="<%# Eval("Quantity") %>" />
                                                                <div class="edit-wp">
                                                                    <a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),'<%# Eval("Name") %>','product')"
                                                                        href="javascript://" title="Xóa"></a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr class="tr-send">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <input type="button" id="btnAdd" class="btn-send" onclick="addInventory();" value="Hoàn tất" />
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <!-- input hidden -->
                    <asp:HiddenField ID="HDF_ProductIds" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_CheckQuantity" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_ServiceIds" runat="server" ClientIDMode="Static" />
                    <!-- END input hidden -->
                </div>
            </div>
            <%-- end Add --%>
        </div>
        <!-- Danh mục sản phẩm -->
        <div class="popup-product-wp popup-product-item">
            <div class="wp popup-product-head">
                <strong>Danh mục sản phẩm</strong>
            </div>
            <div class="wp popup-product-content">
                <div class="wp popup-product-guide">
                    <%--<div class="left">Hướng dẫn:</div>
            <div class="right">
                <p>- Click vào ô check box để chọn sản phẩm</p>
                <p>- Click vào ô số lượng để thay đổi số lượng sản phẩm</p>
            </div>--%>
                    <label style="font-family: Roboto Condensed Bold; font-weight: normal; font-size: 14px; color: #222222; margin-right: 10px;">Chọn danh mục</label>
                    <select onchange="showProductByCategory($(this))" id="ddlProductCategory" style="color: #222222; font-size: 14px; padding: 0 5px;">
                        <asp:Repeater runat="server" ID="rptProductCategory">
                            <ItemTemplate>
                                <option value="<%# Eval("Id") %>" style="color: #222222; font-size: 14px;"><%# Eval("Name") %></option>
                            </ItemTemplate>
                        </asp:Repeater>
                    </select>
                </div>
                <div class="wp listing-product item-product">
                    <table class="table" id="table-product">
                        <thead>
                            <tr>
                                <th>
                                    <%--Click All--%><input type="checkbox" <%--onchange="checkAllpage(this)"--%> />
                                </th>
                                <th>STT</th>
                                <th>Tên sản phẩm</th>
                                <th>Giá sản phẩm</th>
                                <th style="display: none;">Đơn giá nhập</th>
                                <th class="th-product-quantity">Số lượng</th>
                                <th style="display: none;">Giảm giá</th>
                                <%--<th>Thành tiền</th>--%>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="Rpt_Product_Flow" runat="server">
                                <ItemTemplate>
                                    <tr data-cate="<%# Eval("CategoryId") %>">
                                        <td class="td-product-checkbox item-product-checkbox">
                                            <input type="checkbox" value="<%# Eval("Id") %>"
                                                data-code="<%# Eval("Code") %>" data-name="<%# Eval("Name") %>" />
                                        </td>
                                        <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                        <td class="td-product-name"
                                            <%# Convert.ToInt32(Eval("IsCheckVatTu")) == 1 ? "data-check=\"True\"" : "data-check=\"False\"" %>><%# Eval("Name") %></td>
                                        <td class="td-product-code" style="display: none" data-id="<%# Eval("Id") %>" data-code="<%# Eval("Code") %>"><%# Eval("Code") %></td>
                                        <td class="td-product-price" data-price="<%# Eval("Price") %>"><%# Eval("Price") %></td>
                                        <td class="td-product-quantity map-edit">
                                            <input type="text" class="product-quantity" value="1" />
                                            <div class="edit-wp">
                                                <a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),'<%# Eval("Name") %>','product')"
                                                    href="javascript://" title="Xóa"></a>
                                            </div>
                                        </td>
                                        <td class="td-product-voucher" data-voucher="<%# Eval("VoucherPercent") %>" style="display: none;">
                                            <div class="row">
                                                <input type="text" class="product-voucher" value="<%# Eval("VoucherPercent") %>" style="margin: 0 auto; float: none; text-align: center; width: 50px;" />
                                                %
                                            </div>
                                            <div class="row promotion-money">
                                                <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                                    <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                        <input type="checkbox" class="item-promotion" data-value="35000" data-id="1" style="margin-left: 0;" />
                                                        - 35.000 VNĐ
                                                    </label>

                                                </div>
                                                <br />
                                                <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                                    <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                        <input type="checkbox" class="item-promotion" data-id="2" data-value="50000" style="margin-left: 0;" />
                                                        - 50.000 VNĐ
                                                    </label>

                                                </div>
                                            </div>
                                        </td>
                                        <%--<td class="map-edit">
                                    <div class="box-money"></div>
                                    <div class="edit-wp">
                                        <a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),'<%# Eval("Name") %>','product')"
                                             href="javascript://" title="Xóa"></a>
                                    </div>
                                </td>--%>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
                <div class="wp btn-wp">
                    <div class="popup-product-btn btn-complete" data-item="product">Hoàn tất</div>
                    <div class="popup-product-btn btn-esc">Thoát</div>
                </div>
            </div>
        </div>
        <!-- END Danh mục sản phẩm -->
        <script src="/assets/js/jquery.mCustomScrollbar.js"></script>
        <link href="/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 35px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/javascript">
            // Mở box listing sản phẩm
            $(".show-product").bind("click", function () {
                $(this).parent().parent().find(".listing-product").show();
                $("select#ddlProductCategory").val($("select#ddlProductCategory option:first-child").val());
                showProductByCategory($("select#ddlProductCategory"));
            });

            $(".show-item").bind("click", function () {
                var item = $(this).attr("data-item");
                var classItem = ".popup-" + item + "-item";

                $(classItem).openEBPopup();
                ExcCheckboxItem();
                ExcQuantity();
                ExcCompletePopup();

                $('#EBPopup .listing-product').mCustomScrollbar({
                    theme: "dark-2",
                    scrollInertia: 100
                });

                $("#EBPopup .btn-esc").bind("click", function () { autoCloseEBPopup(0); });
            });

            function showProductByCategory(This) {
                $("table#table-product tbody tr").each(function () {
                    if ($(this).attr("data-cate") != This.val()) {
                        $(this).hide();
                    } else {
                        $(this).show();
                    }
                });
                if ($(".popup-product-item").isOpenEBPopup()) {
                    $(".popup-product-item").alignment();
                }
            }

            // Xử lý khi chọn checkbox sản phẩm
            function ExcCheckboxItem() {
                $("#EBPopup .td-product-checkbox input[type='checkbox']").unbind("click").bind("click", function () {
                    var obj = $(this).parent().parent(),
                        boxMoney = obj.find(".box-money"),
                        price = obj.find(".td-product-price").data("price"),
                        quantity = obj.find("input.product-quantity").val(),
                        voucher = obj.find("input.product-voucher").val(),
                        checked = $(this).is(":checked"),
                        item = obj.attr("data-item"),
                        promotion = 0;
                    obj.find(".promotion-money input[type='checkbox']:checked").each(function () {
                        var value = $(this).attr("data-value").trim();
                        promotion += (value != "" ? parseInt(value) : 0);
                    });

                    // check value
                    price = price.toString().trim() != "" ? parseInt(price) : 0;
                    quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
                    voucher = voucher.trim() != "" ? parseInt(voucher) : 0;


                    if (checked) {
                        boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100 - promotion)).show();
                    } else {
                        boxMoney.text("").hide();
                    }

                });
            }

            // Số lượng | Quantity
            function ExcQuantity() {
                $("input.product-quantity").bind("change", function () {
                    var obj = $(this).parent().parent(),
                        quantity = $(this).val(),
                        price = obj.find(".td-product-price").data("price"),
                        voucher = obj.find("input.product-voucher").val(),
                        boxMoney = obj.find(".box-money"),
                        promotion = 0;

                    obj.find(".promotion-money input[type='checkbox']:checked").each(function () {
                        var value = $(this).attr("data-value").trim();
                        promotion += (value != "" ? parseInt(value) : 0);
                    });

                    // check value
                    price = price.toString().trim() != "" ? parseInt(price) : 0;
                    quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
                    voucher = voucher.trim() != "" ? parseInt(voucher) : 0;

                    boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100 - promotion)).show();
                    TotalMoney();
                    getProductIds();
                    getServiceIds();
                });
                $("input.product-voucher").bind("change", function () {
                    var obj = $(this).parent().parent().parent(),
                        quantity = obj.find("input.product-quantity").val(),
                        price = obj.find(".td-product-price").data("price"),
                        voucher = obj.find("input.product-voucher").val(),
                        boxMoney = obj.find(".box-money"),
                        promotion = 0;

                    obj.find(".promotion-money input[type='checkbox']:checked").each(function () {
                        var value = $(this).attr("data-value").trim();
                        promotion += (value != "" ? parseInt(value) : 0);
                    });

                    // check value
                    price = price.toString().trim() != "" ? parseInt(price) : 0;
                    quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
                    voucher = voucher.trim() != "" ? parseInt(voucher) : 0;

                    if (promotion == 0) {
                        boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100)).show();
                    } else {
                        obj.find("input.product-voucher").val(0);
                    }

                    TotalMoney();
                    getProductIds();
                    getServiceIds();
                });
                $("input.item-promotion").bind("click", function () {
                    var obj = $(this).parent().parent().parent().parent().parent(),
                        quantity = obj.find("input.product-quantity").val(),
                        price = obj.find(".td-product-price").data("price"),
                        voucher = obj.find("input.product-voucher").val(),
                        boxMoney = obj.find(".box-money"),
                        promotion = $(this).attr("data-value"),
                        _This = $(this),
                        isChecked = $(this).is(":checked");
                    obj.find(".promotion-money input[type='checkbox']:checked").prop("checked", false);
                    if (isChecked) {
                        $(this).prop("checked", true);
                        promotion = promotion.trim() != "" ? parseInt(promotion) : 0;
                        obj.find("input.product-voucher").val(0);
                    } else {
                        promotion = 0;
                    }

                    // check value
                    price = price.toString().trim() != "" ? parseInt(price) : 0;
                    quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
                    voucher = voucher.trim() != "" ? parseInt(voucher) : 0;
                    //promotion = promotion.trim() != "" ? parseInt(promotion) : 0;

                    if (promotion > 0) {
                        boxMoney.text(FormatPrice(quantity * price - promotion)).show();
                        obj.find("input.product-voucher").css({ "text-decoration": "line-through" });
                    } else {
                        boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100)).show();
                        obj.find("input.product-voucher").css({ "text-decoration": "none" });
                    }
                    TotalMoney();
                    getProductIds();
                    getServiceIds();
                });
            }

            // Xử lý click hoàn tất chọn item từ popup
            function ExcCompletePopup() {
                $("#EBPopup .btn-complete").unbind("click").bind("click", function () {
                    var item = $(this).attr("data-item"),
                        tableItem = $("table.table-item-" + item + " tbody"),
                        objTmp;

                    $("#EBPopup .item-product-checkbox input[type='checkbox']:checked").each(function () {
                        var Code = $(this).attr("data-code");
                        if (!ExcCheckItemIsChose(Code, tableItem)) {
                            objTmp = $(this).parent().parent().clone().find("td:first-child").remove().end();
                            tableItem.append(objTmp);
                            $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                                $("#success-alert").slideUp(2000);
                            });
                            $("#msg-alert").text("Thêm sản phẩm thành công!!!");
                        }
                        else {
                            $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                                $("#success-alert").slideUp(2000);
                            });
                            $("#msg-alert").text("Có sản phẩm đã tồn tại trong danh sách!!!");
                        }
                    });
                    TotalMoney();
                    getProductIds();
                    getServiceIds();
                    ExcQuantity();
                    UpdateItemOrder(tableItem);
                    $("#ListingProductWp").show();
                    //autoCloseEBPopup(0);
                });
            }
            function getServiceIds() {
                var Ids = [];
                var prd = {};
                $("table.table-item-service tbody tr").each(function () {
                    prd = {};
                    var THIS = $(this),
                        obj = THIS.parent().parent();

                    var Id = THIS.find("td.td-product-code").attr("data-id"),
                        Code = THIS.find("td.td-product-code").text().trim(),
                        Name = THIS.find("td.td-product-name").text().trim(),
                        Price = THIS.find(".td-product-price").attr("data-price"),
                        Quantity = THIS.find("input.product-quantity").val(),
                        VoucherPercent = THIS.find("input.product-voucher").val();

                    // check value
                    Id = Id.toString().trim() != "" ? parseInt(Id) : 0;
                    Price = Price.toString().trim() != "" ? parseInt(Price) : 0;
                    Quantity = Quantity.trim() != "" ? parseInt(Quantity) : 0;
                    VoucherPercent = !isNaN(parseInt(VoucherPercent)) ? parseInt(VoucherPercent) : 0;

                    prd.Id = Id;
                    prd.Code = Code;
                    prd.Name = Name;
                    prd.Price = Price;
                    prd.Quantity = Quantity;
                    prd.VoucherPercent = VoucherPercent;

                    Ids.push(prd);

                });
                Ids = JSON.stringify(Ids);
                $("#HDF_ServiceIds").val(Ids);
            }
            // Check item đã được chọn
            function ExcCheckItemIsChose(Code, itemClass) {
                var result = false;
                $(itemClass).find(".td-product-code").each(function () {
                    var _Code = $(this).text().trim();
                    if (_Code == Code)
                        result = true;
                });
                return result;
            }
            function TotalMoney() {
                var Money = 0;
                $(".fe-service-table-add .box-money:visible").each(function () {
                    Money += parseInt($(this).text().replace(/\./gm, ""));
                });
                $("#TotalMoney").val(FormatPrice(Money));
                $("#HDF_TotalMoney").val(Money);
            }
            //get url
            var URL_API_INVENTORY = "<%= Libraries.AppConstants.URL_API_INVENTORY %>";
            var inventoryFlows;
            $(document).keydown(function (e) {
                if (e.keyCode == '13') {
                    $('.btn-add').click();
                    $('#ms').focus();
                    e.cancelBubble = true;
                    return false;
                }
            });

            //add product 
            function addProduct() {
                var kt = true;
                var error = '';
                quantity = $("#inputQuantity").val();
                if (quantity == "" || quantity <= "0") {
                    kt = false;
                    error += "Nhập số lượng sản phẩm và phải lớn hơn 0! ";
                    $('#inputQuantity').css("border-color", "red");
                }

                // check trùng sản phẩm
                $("table.table-item-product tbody tr").each(function () {
                    var THIS = $(this);
                    var productId = THIS.find("td.td-product-code").attr("data-id");
                    if (productId != "undefined" && itemProduct.Id != null) {
                        if (itemProduct.Id == productId) {
                            kt = false;
                            error += "Sản phẩm đã tồn tại trong danh sách. Vui lòng thêm số lượng!";
                        }
                    }
                });

                // !kt xuất hiện thông báo
                if (!kt) {
                    $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                        $("#success-alert").slideUp(2000);
                    });
                    $("#msg-alert").text(error);
                }

                // add item 
                else {
                    index = $("#table-item-product tbody tr").length + 1;
                    if (itemProduct != null) {
                        if (itemProduct.Code != undefined && quantity > 0) {
                            var tr = '<tr>' +
                                '<td>' + index + '</td>' +
                                '<td class="td-product-code maSPcol" style="display:none;" data-id="' + itemProduct.Id + '">' + itemProduct.Code + '</td>' +
                                '<td class="td-product-name" data-cost="' + itemProduct.Cost + '">' + itemProduct.Name + '</td>' +
                                '<td class="td-product-price" id="box-money" data-price="' + itemProduct.Price + '">' + itemProduct.Price + '</td>' +
                                '<td class="td-product-quantity map-edit">' +
                                '<input class="product-quantity" value="' + quantity + '" type="text" placeholder="Số lượng"/>' +
                                '<div class="edit-wp">' +
                            '<a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),\'<%# Eval("Name") %>\',\'product\')" href="javascript://" title="Xóa"></a>' +
                                '</div>' +
                                '</td>' +
                                '</tr>';
                            $("#table-item-product tbody").append($(tr));
                            $("#ListingProductWp").show();

                            getProductIds();
                            resetInputForm();
                        }
                    }
                }

            }
            //reset inputQuantity
            function resetInputForm() {
                ms.clear();
                $("#inputQuantity").val("");
                $(ms).focus();
                $(".ms-sel-ctn input[type='text']").focus();
            }

            //Get product
            function getProductIds() {
                var Ids = [];
                var prd = {};
                $("table.table-item-product tbody tr").each(function () {
                    prd = {};
                    var THIS = $(this);
                    var productId = THIS.find("td.td-product-code").attr("data-id"),
                        quantity = THIS.find("input.product-quantity").val()
                    // check value
                    productId = productId.toString().trim() != "" ? parseInt(productId) : 0,
                        quantity = quantity.toString().trim != "" ? parseFloat(quantity) : 0;
                    prd.productId = productId;
                    prd.quantity = parseFloat(quantity);
                    Ids.push(prd);
                });

                inventoryFlows = Ids;
            }

            ///call api insert product
            function addInventory() {
                var kt = true;
                var error = 'Vui lòng nhập đầy đủ thông tin!';
                var salonId = $('#Salon').val();
                var staffOrderId = "1";
                var createdTime = $('#TxtDateTimeFrom').val();
                //if (staffOrderId == "root") {
                //    staffOrderId = "0";
                //}
                if (createdTime == "") {
                    kt = false;
                    error += " [Chọn ngày], ";
                    $('#TxtDateTimeFrom').css("border-color", "red");
                }
                if (salonId == "" || salonId == "0") {
                    kt = false;
                    error += " [Chọn salon], ";
                    $('#salonId').css("border-color", "red");
                }
                if (staffOrderId == "") {
                    kt = false;
                    error += " [Chưa có thông tin người nhập hàng tồn], ";
                }
                if (inventoryFlows == "" || inventoryFlows == "[]") {
                    kt = false;
                    error += " [Bạn chưa chọn mỹ phẩm và số lượng!] ";
                }
                if (!kt) {
                    $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                        $("#success-alert").slideUp(2000);
                    });
                    $("#msg-alert").text(error);
                }
                else {
                    addLoading();
                    salonId = salonId.toString().trim() != "" ? parseInt(salonId) : 0;
                    staffOrderId = staffOrderId.toString().trim() != "" ? parseInt(staffOrderId) : 0;
                    var data = JSON.stringify({ salonId, staffOrderId, createdTime, inventoryFlows: inventoryFlows });
                    $.ajax({
                        url: URL_API_INVENTORY + "/api/inventory-manager",
                        type: "POST",
                        contentType: "application/json;charset:UTF-8",
                        dataType: "json",
                        data: data,
                        success: function (data) {
                            var stattus = data.status;
                            if (stattus == 1) {
                                $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                                    $("#success-alert").slideUp(2000);
                                });
                                $("#msg-alert").text("Thêm mới thành công!");
                                window.location.href = "/admin/hang-hoa/danh-sach-san-pham-ton.html";
                                setTimeout(function () {
                                    window.location.href = "/admin/hang-hoa/danh-sach-san-pham-ton.html";
                                }, 1500);
                            }
                        },

                        error: function (xhr) {
                            console.log(xhr)
                            alert('Có lỗi xảy ra! Xin vui lòng liên hệ với nhóm phát triển!');
                        }
                    });
                }
            }

            // Update table display
            function UpdateItemDisplay(itemName) {
                var dom = $(".table-item-" + itemName);
                var len = dom.find("tbody tr").length;
                if (len == 0) {
                    dom.parent().hide();
                } else {
                    UpdateItemOrder(dom.find("tbody"));
                }
            }

            // Update order item
            function UpdateItemOrder(dom) {
                var index = 1;
                dom.find("tr").each(function () {
                    $(this).find("td.td-product-index").text(index);
                    index++;
                });
            }
            // Remove item đã được chọn
            function RemoveItem(THIS, name, itemName) {
                // Confirm
                $(".confirm-yn").openEBPopup();
                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn hủy [ " + name + " ] ?");
                $("#EBPopup .yn-yes").bind("click", function () {
                    var Code = THIS.find(".td-staff-code").attr("data-code");
                    $("#quick-" + itemName).find("input[data-code='" + Code + "']").removeAttr("checked");
                    THIS.remove();
                    getProductIds();
                    UpdateItemDisplay(itemName);
                    autoCloseEBPopup(0);
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
            }
        </script>

    </asp:Panel>
    <script>
        var itemProduct;
        var quantity;
        var index;
        var ms;
        var listProduct;
        ///datetime
        $('.txtDateTime').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'vi',
            startDate: '2014/10/10',
            format: 'd/m/Y',
            dateonly: true,
            showHour: false,
            showMinute: false,
            timepicker: false,
            onChangeDateTime: function (dp, $input) { },
            scrollMonth: false,
            scrollTime: false,
            scrollInput: false
        });

        jQuery(document).ready(function () {
            $("#success-alert").hide();


            ms = $('#ms').magicSuggest({
                maxSelection: 1,
                data: <%=ListProduct %>,
                valueField: 'Id',
                displayField: 'Name',
            });
            $(ms).on('selectionchange', function (e, m) {
                itemProduct = null;
                var listProduct = this.getSelection();
                //console.log(listProduct);
                var listData = ms.getData();
                if (listProduct.length > 0) {
                    for (var i = 0; i < listData.length; i++) {
                        if (listProduct[0].Id == listData[i].Id) {
                            itemProduct = listProduct[0];
                            $("#inputQuantity").focus();
                            break;
                        }
                        else {
                            continue;
                        }
                    }
                    if (itemProduct == null) {
                        showMsgSystem("Vui lòng nhập đúng sản phẩm tồn kho!", "warning");
                        ms.clear();
                        $(".ms-sel-ctn input[type='text']").focus();
                    }
                }
            });
        });
    </script>
</asp:Content>
