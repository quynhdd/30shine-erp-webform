﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="ListProductReturn.aspx.cs" Inherits="_30shine.GUI.BackEnd.Inventory.ListProductReturn" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .table-wp {
                width: 100%;
                float: left;
            }

                .table-wp table th {
                    font-family: Roboto Condensed Bold;
                    font-weight: normal;
                }

            table tr td, table tr th {
                border: 1px solid black;
                border-collapse: collapse;
                text-align: center;
            }

            table tr.active {
                background: #ffe400;
            }

            .be-report .btn-viewdata {
                height: 32px;
                line-height: 32px;
            }

            .customer-listing .tag-wp {
                width: 100%;
            }

            table.table-report {
                display: table;
                width: 100% !important;
            }

                table.table-report tbody {
                    width: 100% !important;
                    display: table;
                }

                table.table-report th {
                    height: 40px;
                    border: 1px solid black;
                    border-collapse: collapse;
                    text-align: center;
                    background-color: #ccc;
                }

            td {
                border-collapse: collapse;
                text-align: center;
                height: 30px;
            }

            .table-sub-report {
                line-height: 2.2;
                border-style: none;
                width: 100% !important;
            }

                .table-sub-report tr {
                    border-style: none;
                }

                    .table-sub-report tr td {
                        border-right-style: none;
                        border-left-style: none;
                        border-top-style: none;
                    }

            .select2-container {
                width: 190px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }

            .table-report tr:nth-child(even) {
                background-color: #f2f2f2
            }

            .table-sub-report tr:nth-child(even) {
                background-color: white;
            }

            .table-sub-report tr:nth-child(odd) {
                background-color: #f2f2f2
            }

            .be-report-li {
                padding: 7px 0px 0px 0px !important;
            }

            .customer-listing .table-func-panel {
                width: 100%;
                float: none !important;
                position: relative;
                height: 0px !important;
                margin-bottom: 0px !important;
            }

            @media only screen and (max-width: 1600) {
                table.table-report tbody tr th:nth-child(1) {
                    min-width: 30px !important;
                }

                table.table-report tbody tr th:nth-child(2), table.table-report tbody tr th:nth-child(4), table.table-report tbody tr th:nth-child(5), table.table-report tbody tr th:nth-child(7) {
                    min-width: 100px !important;
                }
            }

            @media only screen and (max-width: 1280) {
                table.table-report tbody tr th:nth-child(1) {
                    min-width: 0px !important;
                }

                table.table-report tbody tr th:nth-child(2), table.table-report tbody tr th:nth-child(4), table.table-report tbody tr th:nth-child(5), table.table-report tbody tr th:nth-child(7) {
                    min-width: 0px !important;
                }
            }
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Danh sách &nbsp;&#187; </li>
                        <li class="be-report-li"><i class="fa fa-th-large"></i>HÀNG TRẢ ĐỔI TRẢ</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing be-report">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row">
                    <div class="filter-item" style="margin-left: 0;">
                        <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Ngày trả</strong>
                        <div class="datepicker-wp">
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Chọn ngày"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                            <strong class="st-head" style="margin-left: 10px;">
                                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                            </strong>
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeTo" placeholder="Đến ngày" Style="margin-left: 10px!important"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                            <br />
                        </div>
                        <strong class="st-head" style="margin-left: 5px;">Salon nhận </strong>
                        <div class="filter-item">
                            <asp:DropDownList ID="ddlSalon" runat="server" CssClass="select form-control" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                        </div>
                        <div class="filter-item">
                            <asp:DropDownList ID="ddlProduct" runat="server" CssClass="select form-control" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                        </div>
                        <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" Text="Xem dữ liệu"
                            ClientIDMode="Static" onclick="excPagings(1)" runat="server">
                            Xem danh sách
                        </asp:Panel>
                        <a href="javascript://" class="st-head btn-viewdata" onclick="ExportExcel($(this))">Xuất File Excel</a>
                        <a href="/danh-sach/hang-doi-tra.html" class="st-head btn-viewdata">Reset Filter</a>
                    </div>
                </div>
                <!-- End Filter -->
                <!-- Row Table Filter -->
                <div class="table-func-panel">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegments($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegments($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <asp:UpdatePanel ID="UPTotal" runat="server" ClientIDMode="Static" style="margin-top: 15px;">
                    <ContentTemplate>
                        <div class="row">
                            <strong class="st-head"><i class="fa fa-file-text"></i>DANH SÁCH HÀNG TRẢ LẠI</strong>
                        </div>
                        <div class="ctn-table">
                            <div class="table-wp" id="table-wp-productreturn">
                                <table class="table-report">
                                    <tbody>
                                        <tr>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px;">STT</th>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px;">NGÀY TRẢ</th>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px;">TÊN KHÁCH HÀNG</th>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px;">SALON BÁN</th>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px;">ID NV BÁN TRƯỚC</th>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px;">NV BÁN TRƯỚC</th>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px;">SALON NHẬN</th>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px;">NV NHẬN</th>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 30px;">ID SẢN PHẨM</th>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px;">TÊN SẢN PHẨM</th>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px;">SỐ LƯỢNG</th>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px;">GIÁ SẢN PHẨM</th>
                                            <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px;">LƯƠNG NHÂN VIÊN</th>
                                        </tr>
                                        <%foreach (var v in listOrigins)
                                            { %>
                                        <tr>
                                            <td><%= index++ %></td>
                                            <td><%= v.createdDate %></td>
                                            <td><%= v.customerName %></td>
                                            <td><%= v.salonSellName %></td>
                                            <td><%= v.sellerId %></td>
                                            <td><%= v.sellerName %></td>
                                            <td><%= v.salonReceiverName %></td>
                                            <td><%= v.receiverName %></td>
                                            <td><%= v.productId %></td>
                                            <td><%= v.productName %></td>
                                            <td><%= v.quantity %></td>
                                            <td><%=  v.priceProduct %></td>
                                            <td><%=  v.productSalary %></td>
                                        </tr>
                                        <%} %>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPagings(1)">Đầu</a>
                                <a href="javascript://" onclick="excPagings(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPagings(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPagings(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPagings(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="IsPaging" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_ExcelType" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELPage" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELSegment" />
            </div>
        </div>
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script type="text/javascript">
            // load khi load trang
            $(document).ready(function () {
                $("table.table-report").css("height", "auto");
            });
            jQuery(document).ready(function () {
                $('.select').select2();
                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { },
                    scrollMonth: false,
                    scrollTime: false,
                    scrollInput: false
                });
            });

            function viewDataByDate(This, time) {
                This.addClass("active");
                $("#TxtDateTimeTo").val("");
                $("#TxtDateTimeFrom").val(time);
                $("#ViewData").click();
            }

            function excPagings(page) {
                $("#HDF_Page").val(page);
                $("#BtnFakeUP").click();
                startLoading();
            }

            function ShowUlOptSegments(THIS) {
                var UlOptSegment = THIS.parent().find(".ul-opt-segment"),
                    OPTSegment = $("#OPTSegment"),
                    HDF_OPTSegment = $("#HDF_OPTSegment");
                UlOptSegment.toggle();
                UlOptSegment.find(">li").bind("click", function () {
                    var segment = $(this).attr("data-value");
                    var _segment = segment == 1000000 ? "Tất cả" : segment;
                    HDF_OPTSegment.val(segment);
                    OPTSegment.text(_segment);
                    UlOptSegment.hide();
                    excPagings(1);
                });
            }
            // Xuat file excel
            function ExportExcel(This) {
                // function export excel
                funcExportExcelHtml("#table-wp-productreturn", "HangDoiTra");
            }
            // Format price
            function formatPrice(price) {
                var pricef = price.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                return pricef;
            }
        </script>
    </asp:Panel>
</asp:Content>
