﻿using _30shine.Helpers;
using _30shine.Helpers.Http;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mail;
using System.Net;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Net.Http;
using ExportToExcel;

namespace _30shine.GUI.BackEnd.Inventory
{
    public partial class ChiphihangNgay : System.Web.UI.Page
    {
        private string PageID = "_30shine.GUI.BackEnd.Inventory.ChiphihangNgay";
        public bool Perm_ViewAllData = false;
        public double totalCost = 0;
        private bool Perm_Access = false;
        protected Paging PAGING = new Paging();
        private int salonId = 0;
        private List<Chiphingay> data = new List<Chiphingay>();

        /// <summary>
        /// set permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                bindData();
                Library.Function.bindSalon(new List<DropDownList>() { ddlSalon }, Perm_ViewAllData);
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            bindData();
            RemoveLoading();

        }
        /// <summary>
        /// get quyền 
        /// </summary>
        /// <returns></returns>
        public bool IsAdmin()
        {
            if (Session["User_Permission"].ToString() == "ADMIN" || Session["User_Permission"].ToString() == "root")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Paging
        /// </summary>
        /// <param name="TotalRow"></param>
        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }
        private void bindData()
        {
            data = Getlist();
            Bind_Paging(data.Count());
            rptChiphingay.DataSource = data.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
            rptChiphingay.DataBind();
        }

        /// <summary>
        /// Get list daily repost
        /// </summary>
        /// <returns></returns>
        public List<Chiphingay> Getlist()
        {
            if (TxtDateTimeFrom.Text != "")
            {
                if (TxtDateTimeTo.Text == "")
                {
                    TxtDateTimeTo.Text = TxtDateTimeFrom.Text;
                }
                int integer;
                int salonId = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                try
                {
                    var request = new Request();
                    var response = request.RunGetAsyncV1(Libraries.AppConstants.URL_API_INVENTORY +
                                                         "/api/inventory-manager/get-daily-cost?fromDate=" + TxtDateTimeFrom.Text + "&toDate=" + TxtDateTimeTo.Text + "&salonId=" + salonId)
                                       .Result;//.Content.ReadAsStringAsync().Result;
                                               //resultChiphingay data = new JavaScriptSerializer().Deserialize<resultChiphingay>(result);
                    if (response.IsSuccessStatusCode)
                    {
                        data = response.Content.ReadAsAsync<resultChiphingay>().Result.data.ToList();
                        totalCost = data.Sum(a => a.totalMoney);
                    }
                    else
                    {
                        throw new Exception("Đã có lỗi xảy ra");
                    }
                }
                catch (Exception ex)
                {
                    //Library.Function.WriteToFile("Getlist Exception First " + DateTime.Now + ": " + ex.Message, "CHICHINGAY (INVENTORY)");
                    SendMail("nhuphuc1007@gmail.com", "ERROR MAIN: " + this.ToString() + @"\Getlist()", ex.Message);
                }
            }
            return data;
        }
        /// <summary>
        /// Export Excell
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Exc_ExportExcel(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                if (TxtDateTimeFrom.Text.Trim() != "")
                {
                    var _ExcelHeadRow = new List<string>();
                    var RowLST = new List<List<string>>();
                    var Row = new List<string>();
                    data = Getlist();
                    _ExcelHeadRow.Add("Tên sản phẩm");
                    _ExcelHeadRow.Add("Hàng tồn cũ");
                    _ExcelHeadRow.Add("Hàng nhận đầu ngày");
                    _ExcelHeadRow.Add("Hàng tồn cuối ngày");
                    _ExcelHeadRow.Add("Mức dùng hàng ngày");
                    _ExcelHeadRow.Add("Giá nhập");
                    _ExcelHeadRow.Add("Chi phí");
                    if (data.Count > 0)
                    {
                        foreach (var v in data)
                        {
                            Row = new List<string>();
                            Row.Add(v.name);
                            Row.Add(v.quantityOld.ToString());
                            Row.Add(v.quantityReceived.ToString());
                            Row.Add(v.QuantityNew.ToString());
                            Row.Add(v.quantityLevelOfUse.ToString());
                            Row.Add(string.Format("{0:#,####}", v.cost.ToString()));
                            Row.Add(v.totalMoney.ToString());
                            RowLST.Add(Row);
                           
                        }
                        var ExcelStorePath = Server.MapPath("~") + "Public/Excel/Hoa.Don/";
                        var FileName = "Chi_phi_hang_ngay_" + string.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
                        var UrlFileName = "http://" + Request.Url.Host + "/Public/Excel/Hoa.Don/" + FileName;
                        ExportXcel(_ExcelHeadRow, RowLST, ExcelStorePath + FileName);
                        Bind_Paging(int.Parse(HDF_EXCELPage.Value));
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "');UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing(); alert(\"Xuất excel thành công.\")", true);
                    }


                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_ExcelHeadRow"></param>
        /// <param name="List"></param>
        /// <param name="Path"></param>
        private static void ExportXcel(List<string> _ExcelHeadRow, List<List<string>> List, string Path)
        {
            try
            {
                CreateExcelFile.ExcelHeadRow = _ExcelHeadRow;
                CreateExcelFile.CreateExcel(List, Path);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi. Không tạo được file excel.\r\nException: " + ex.Message);
            }
        }
        /// <summary>
        /// Hàm send mail
        /// </summary>
        /// <param name="ReceiveEmail"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        public void SendMail(string ReceiveEmail, string subject, string body)
        {
            string SENDER_EMAIL = "phuc.2110@gmail.com";
            string SENDER_PASSWORD = "tflmxmdmgnyuqzhh";
            //Reading sender Email credential from web.config file
            string HostAdd = "smtp.gmail.com";
            string ToEmail = ReceiveEmail;

            //creating the object of MailMessage
            System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage();
            mailMessage.From = new MailAddress(SENDER_EMAIL); //From Email Id
            mailMessage.Subject = subject; //Subject of Email
            mailMessage.Body = body; //body or message of Email
            mailMessage.IsBodyHtml = true;
            //Adding Multiple recipient email id logic
            string[] Multi = ToEmail.Split(','); //spiliting input Email id string with comma(,)
            foreach (string Multiemailid in Multi)
            {
                mailMessage.To.Add(new MailAddress(Multiemailid)); //adding multi reciver's Email Id
            }
            SmtpClient smtp = new SmtpClient(); // creating object of smptpclient
            smtp.Host = HostAdd; //host of emailaddress for example smtp.gmail.com etc

            //network and security related credentials
            smtp.EnableSsl = true;
            NetworkCredential NetworkCred = new NetworkCredential();
            NetworkCred.UserName = mailMessage.From.Address;
            NetworkCred.Password = SENDER_PASSWORD;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;
            smtp.Send(mailMessage); //sending Email
        }
        public class resultChiphingay
        {
            public int status { get; set; }

            public string message { get; set; }

            public List<Chiphingay> data { get; set; }
        }

        public class Chiphingay
        {
            public int id { get; set; }

            public string name { get; set; }

            public string code { get; set; }

            public int price { get; set; }

            public int cost { get; set; }

            public double quantityOld { get; set; }

            public double QuantityNew { get; set; }

            public double quantityLevelOfUse { get; set; }

            public double quantityReceived { get; set; }
            public double totalMoney { get; set; }
        }

    }
}