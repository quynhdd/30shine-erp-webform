﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Web.Script.Serialization;

namespace _30shine.GUI.BackEnd.Inventory.CosmeticOrder
{
    public partial class Order_Cosmetic : System.Web.UI.Page
    {
        private bool Perm_Access = false;
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        private bool Perm_ViewAllData = false;
        protected string ListProduct;
        public static List<_30shine.MODEL.ENTITY.EDMX.Product> _list = new List<_30shine.MODEL.ENTITY.EDMX.Product>();
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var msgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", msgParam);
            }
            else
            {

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                ListProduct = GetProduct();
                TxtDateTimeFrom.Enabled = false;
                TxtDateTimeFrom.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Today);
            }
        }

        /// <summary>
        /// GetProduct Bind Dropdowlist
        /// </summary>
        private string GetProduct()
        {
            using (var db = new Solution_30shineEntities())
            {
                _list = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1 && w.CategoryId != 20).ToList();

                //_list = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1).ToList();
                var seria = new JavaScriptSerializer();
                return seria.Serialize(_list);
            }
        }
    }
}