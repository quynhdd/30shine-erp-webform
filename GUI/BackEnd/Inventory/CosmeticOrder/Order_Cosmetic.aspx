﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Order_Cosmetic.aspx.cs" Inherits="_30shine.GUI.BackEnd.Inventory.CosmeticOrder.Order_Cosmetic" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Quản lý kho - SalonOrder</title>
    <link href="/Assets/css/bootstrap/bootstrap-toggle.min.css" rel="stylesheet" />
    <script src="/Assets/js/bootstrap/bootstrap-toggle.min.js"></script>

    <script src="/Assets/libs/magicsuggest-master/magicsuggest.js"></script>
    <link href="/Assets/libs/magicsuggest-master/magicsuggest.css" rel="stylesheet" />

    <style>
        .fe-service .tr-product .table-listing-product tbody input.product-quantity {
            border-bottom: 1px solid #ddd !important;
        }

        .suggestion-wrap {
            width: 420px;
            float: left;
        }

        .ms-sel-ctn input[type='text'] {
            height: auto !important;
            line-height: 24px !important;
            border: none !important;
        }

        .ms-ctn .ms-sel-item {
            line-height: 22px;
            color: #000000;
            font-size: 13px;
        }

        .ms-helper {
            display: none !important;
        }

        .input-quantity-wrap {
            float: left;
            margin-left: 10px;
        }

        .input-quantity {
            width: 90px;
            line-height: 36px;
            padding: 0 10px;
            border: 1px solid #ddd;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }

        .btn-add {
            float: left;
            background: #ddd;
            padding: 9px 15px;
            margin-left: 10px;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            cursor: pointer;
        }

        #success-alert {
            top: 100px;
            right: 10px;
            position: fixed;
            width: 20% !important;
            z-index: 2000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="alert alert-danger" id="success-alert">
            <span id="msg-alert"></span>
        </div>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý salon đặt hàng  &nbsp;&#187;</li>
                        <li class="be-report-li" id="Tonkho"><a href="/my-pham/kho-tra-hang/danh-sach.html"><i class="fa fa-th-large"></i>Danh sách hàng salon order</a></li>
                        <li class="li-add"><a href="/my-pham/salon-ban/salon-order.html">Salon order</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add admin-product-add fe-service">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <!-- System Message -->
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <!-- END System Message -->

                <div class="table-wp">
                    <table class="table-add admin-product-table-add fe-service-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Nhập số lượng hàng</strong></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr class="tr-field-ahalf" runat="server" id="TrSalon">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right">
                                    <label style="float: left; line-height: 36px; padding-right: 10px; font-weight: normal; font-family: Roboto Condensed Bold;">Ngày</label>
                                    <div class="datepicker-wp">
                                        <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Chọn ngày"
                                            ClientIDMode="Static" runat="server" Style="width: 160px; margin-right: 10px;"></asp:TextBox>
                                    </div>
                                    <div class="salonSelect">
                                        <label style="float: left; line-height: 36px; padding-right: 10px; font-weight: normal; font-family: Roboto Condensed Bold;">Salon</label>
                                        <div class="field-wp" style="float: left; width: auto;">
                                            <asp:DropDownList ID="Salon" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="width: 160px;"></asp:DropDownList>
                                            <asp:RequiredFieldValidator InitialValue="0"
                                                ID="ValidateSalon" Display="Dynamic"
                                                ControlToValidate="Salon"
                                                runat="server" Text="Bạn chưa chọn Salon!"
                                                ErrorMessage="Vui lòng chọn Salon!"
                                                ForeColor="Red"
                                                CssClass="fb-cover-error">
                                            </asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-2 left"><span>Sản phẩm</span></td>
                                <td class="col-xs-9 right">
                                    <div class="suggestion-wrap">
                                        <input id="ms" class="form-control" type="text" />
                                    </div>
                                    <div class="input-quantity-wrap">
                                        <input type="number" id="inputQuantity" class="input-quantity" placeholder="Số lượng" />
                                    </div>
                                    <div class="btn-add" onclick="addProduct()">Thêm sản phẩm</div>
                                </td>
                            </tr>

                            <tr class="tr-field-ahalf tr-product">
                                <td class="col-xs-2 left"><span>Sản phẩm</span></td>
                                <td class="col-xs-9 right">
                                    <div class="listing-product item-product" id="ListingProductWp" runat="server" clientidmode="Static">
                                        <table class="table table-listing-product table-item-product" id="table-item-product">
                                            <thead>
                                                <tr>
                                                    <th>STT</th>
                                                    <th class="maSPcol" style="display: none;">Mã sản phẩm</th>
                                                    <th>Tên sản phẩm</th>
                                                    <th style="display:none">Giá sản phẩm</th>
                                                    <th style="width: 100px;">Số lượng</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="Rpt_Product" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                                            <td class="td-product-code maSPcol" style="display: none;" data-id="<%# Eval("Id") %>"></td>
                                                            <td class="td-product-name" data-cost="<%#Eval("Cost") %>"><%# Eval("Name") %></td>
                                                            <td class="td-product-price" style="display:none" id="box-money" data-price="<%#Eval("Price") %>">
                                                                <%# string.Format("{0:#,0.##}", Eval("Price")) %></td>
                                                            <td class="td-product-quantity map-edit">
                                                                <input type="text" class="product-quantity" value="<%# Eval("Quantity") %>" />
                                                                <div class="edit-wp">
                                                                    <a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),'<%# Eval("Name") %>','product')"
                                                                        href="javascript://" title="Xóa"></a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr class="tr-send">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <input type="button" id="btnAdd" class="btn-send" onclick="addInventory();" value="Hoàn tất" />
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <!-- input hidden -->
                    <asp:HiddenField ID="HDF_ProductIds" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_CheckQuantity" runat="server" ClientIDMode="Static" />
                    <!-- END input hidden -->
                </div>
            </div>
            <%-- end Add --%>
        </div>

        <script src="/assets/js/jquery.mCustomScrollbar.js"></script>
        <link href="/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 35px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/javascript">

            //get url
            var URL_API_INVENTORY = "<%= Libraries.AppConstants.URL_API_INVENTORY %>";
            var qLKhoSalonOrderFlows;
            $(document).keydown(function (e) {
                if (e.keyCode == '13') {
                    $('.btn-add').click();
                    $('#ms').focus();
                    e.cancelBubble = true;
                    return false;
                }
            });

            //add product 
            function addProduct() {
                var kt = true;
                var error = '';
                quantity = $("#inputQuantity").val();
                if (quantity == "" || quantity <= "0") {
                    kt = false;
                    error += "Nhập số lượng sản phẩm và phải lớn hơn 0!";
                    $('#inputQuantity').css("border-color", "red");
                }

                // check trùng sản phẩm
                $("table.table-item-product tbody tr").each(function () {
                    var THIS = $(this);
                    var productId = THIS.find("td.td-product-code").attr("data-id");
                    if (productId != "undefined" && itemProduct.Id == productId) {
                        kt = false;
                        error += "Sản phẩm đã tồn tại trong danh sách. Vui lòng thêm số lượng!";
                    }
                });

                // !kt xuất hiện thông báo
                if (!kt) {
                    $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                        $("#success-alert").slideUp(2000);
                    });
                    $("#msg-alert").text(error);
                }

                // add item 
                else {
                    index = $("#table-item-product tbody tr").length + 1;
                    if (itemProduct != null) {
                        if (itemProduct.Code != undefined && quantity > 0) {
                            var tr = '<tr>' +
                                '<td>' + index + '</td>' +
                                '<td class="td-product-code maSPcol" style="display:none;" data-id="' + itemProduct.Id + '">' + itemProduct.Code + '</td>' +
                                '<td class="td-product-name" data-cost="' + itemProduct.Cost + '">' + itemProduct.Name + '</td>' +
                                '<td class="td-product-price" style="display:none" id="box-money" data-price="' + itemProduct.Price + '">' + itemProduct.Price + '</td>' +
                                '<td class="td-product-quantity map-edit">' +
                                '<input class="product-quantity" value="' + quantity + '" type="text" placeholder="Số lượng"/>' +
                                '<div class="edit-wp">' +
                            '<a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),\'<%# Eval("Name") %>\',\'product\')" href="javascript://" title="Xóa"></a>' +
                                '</div>' +
                                '</td>' +
                                '</tr>';
                            $("#table-item-product tbody").append($(tr));
                            $("#ListingProductWp").show();

                            getProductIds();
                            resetInputForm();
                        }
                    }
                }

            }
            //reset inputQuantity
            function resetInputForm() {
                ms.clear();
                $("#inputQuantity").val("");
                $(ms).focus();
                $(".ms-sel-ctn input[type='text']").focus();
            }

            //Get product
            function getProductIds() {
                var Ids = [];
                var prd = {};
                $("table.table-item-product tbody tr").each(function () {
                    prd = {};
                    var THIS = $(this);
                    var productId = THIS.find("td.td-product-code").attr("data-id"),
                        quantity = THIS.find("input.product-quantity").val()
                    // check value
                    productId = productId.toString().trim() != "" ? parseInt(productId) : 0,
                        quantity = quantity.toString().trim != "" ? parseFloat(quantity) : 0;
                    prd.productId = productId;
                    prd.quantity = parseFloat(quantity);
                    Ids.push(prd);
                });

                qLKhoSalonOrderFlows = Ids;
            }

            ///call api insert product
            function addInventory() {
                var kt = true;
                var error = 'Vui lòng nhập đầy đủ thông tin!';
                var salonId = $('#Salon').val();
                var orderDate = $('#TxtDateTimeFrom').val();
                if (orderDate == "") {
                    kt = false;
                    error += " [Chọn ngày], ";
                    $('#TxtDateTimeFrom').css("border-color", "red");
                }
                if (salonId == "" || salonId == "0") {
                    kt = false;
                    error += " [Chọn salon], ";
                    $('#salonId').css("border-color", "red");
                }
                if (qLKhoSalonOrderFlows == "" || qLKhoSalonOrderFlows == "[]") {
                    kt = false;
                    error += " [Bạn chưa chọn mỹ phẩm và số lượng!] ";
                }
                if (!kt) {
                    $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                        $("#success-alert").slideUp(2000);
                    });
                    $("#msg-alert").text(error);
                }
                else {
                    addLoading();
                    salonId = salonId.toString().trim() != "" ? parseInt(salonId) : 0;
                    var data = JSON.stringify({ salonId, orderDate, qLKhoSalonOrderFlows: qLKhoSalonOrderFlows });
                    $.ajax({
                        url: URL_API_INVENTORY + "/api/qlkho_salonorder",
                        type: "POST",
                        contentType: "application/json;charset:UTF-8",
                        dataType: "json",
                        data: data,
                        success: function (data) {
                            var stattus = data;
                            if (stattus == "Success!") {
                                removeLoading();
                                $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                                    $("#success-alert").slideUp(2000);
                                });
                                $("#msg-alert").text("Thêm mới thành công!");
                                window.location.href = "/my-pham/kho-tra-hang/danh-sach.html";
                            }
                        },

                        error: function (xhr) {
                            console.log(xhr)
                            alert('Có lỗi xảy ra! Xin vui lòng liên hệ với nhóm phát triển!');
                        }
                    });
                }
            }

            // Update table display
            function UpdateItemDisplay(itemName) {
                var dom = $(".table-item-" + itemName);
                var len = dom.find("tbody tr").length;
                if (len == 0) {
                    dom.parent().hide();
                } else {
                    UpdateItemOrder(dom.find("tbody"));
                }
            }

            // Update order item
            function UpdateItemOrder(dom) {
                var index = 1;
                dom.find("tr").each(function () {
                    $(this).find("td.td-product-index").text(index);
                    index++;
                });
            }

            // Remove item đã được chọn
            function RemoveItem(THIS, name, itemName) {
                // Confirm
                $(".confirm-yn").openEBPopup();
                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn hủy [ " + name + " ] ?");
                $("#EBPopup .yn-yes").bind("click", function () {
                    var Code = THIS.find(".td-staff-code").attr("data-code");
                    $("#quick-" + itemName).find("input[data-code='" + Code + "']").removeAttr("checked");
                    THIS.remove();
                    getProductIds();
                    UpdateItemDisplay(itemName);
                    autoCloseEBPopup(0);
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
            }

        </script>
    </asp:Panel>
    <script>
        var itemProduct;
        var quantity;
        var index;
        var ms;
        var listProduct;
        ///datetime
        $('.txtDateTime').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'vi',
            startDate: '2014/10/10',
            format: 'd/m/Y',
            dateonly: true,
            showHour: false,
            showMinute: false,
            timepicker: false,
            onChangeDateTime: function (dp, $input) { },
            scrollMonth: false,
            scrollTime: false,
            scrollInput: false
        });

        jQuery(document).ready(function () {
            $("#success-alert").hide();


            ms = $('#ms').magicSuggest({
                maxSelection: 1,
                data: <%=ListProduct %>,
                valueField: 'Id',
                displayField: 'Name',
            });
            $(ms).on('selectionchange', function (e, m) {
                itemProduct = null;
                var listProduct = this.getSelection();
                //console.log(listProduct);
                var listData = ms.getData();
                if (listProduct.length > 0) {
                    for (var i = 0; i < listData.length; i++) {
                        if (listProduct[0].Id == listData[i].Id) {
                            itemProduct = listProduct[0];
                            $("#inputQuantity").focus();
                            break;
                        }
                        else {
                            continue;
                        }
                    }
                    if (itemProduct == null) {
                        showMsgSystem("Vui lòng nhập đúng sản phẩm tồn kho!", "warning");
                        ms.clear();
                        $(".ms-sel-ctn input[type='text']").focus();
                    }
                }
            });
        });
    </script>
</asp:Content>
