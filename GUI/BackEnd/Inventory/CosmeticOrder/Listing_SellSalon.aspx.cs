﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Linq.Expressions;
using ExportToExcel;
using Project.Helpers;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Inventory.CosmeticOrder
{
    public partial class Listing_SellSalon : System.Web.UI.Page
    {
        private string PageID = "MP_SellSalon";
        protected Paging PAGING = new Paging();
        protected string THead = "";
        protected string StrProductPrice = "";
        public CultureInfo culture = new CultureInfo("vi-VN");
        private DateTime timeFrom;
        private DateTime timeTo;
        protected int salonId;
        private int integer;
        protected string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
        protected string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);

        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

        private bool Perm_Access = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewDataText = false;
        private static Listing_SellSalon instance;
        protected string Permission = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Today);
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
                bindData();
                PassDataFromServerToClient();

            }
            else
            {
                //Exc_Filter();
            }
            RemoveLoading();
        }
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //Perm_ViewDataText = permissionModel.GetActionByActionNameAndPageId("Perm_ViewDataText", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ViewDataText = permissionModel.CheckPermisionByAction("Perm_ViewDataText", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }


        /// <summary>
        /// Get instance
        /// </summary>
        /// <returns></returns>
        public static Listing_SellSalon getInstance()
        {
            if (!(Listing_SellSalon.instance is Listing_SellSalon))
            {
                Listing_SellSalon.instance = new Listing_SellSalon();
            }

            return Listing_SellSalon.instance;
        }


        /// <summary>
        /// hiển thị đơn hàng theo từ ngày đến ngày
        /// </summary>
        private void bindData()
        {
            using (var db = new Solution_30shineEntities())
            {
                try
                {
                    if (TxtDateTimeFrom.Text != "")
                    {
                        timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                        if (TxtDateTimeTo.Text != "")
                        {
                            timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                        }
                        else
                        {
                            timeTo = timeFrom;
                        }
                        salonId = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;

                        var data = (from a in db.QLKho_SalonOrder.AsNoTracking()
                                    join b in db.QLKho_SalonOrder_Status.AsNoTracking() on a.StatusId equals b.Id
                                    join c in db.Tbl_Salon.AsNoTracking() on a.SalonId equals c.Id
                                    where
                                    a.IsDelete == false
                                    && a.OrderDate >= timeFrom && a.OrderDate <= timeTo
                                    && ((a.SalonId == salonId) || (salonId == 0))
                                    && a.CheckCosmetic != null
                                    select new
                                    {
                                        Id = a.Id,
                                        BillCode = a.BillCode,
                                        CreatedTime = a.CreatedTime,
                                        ModifiedTime = a.ModifiedTime,
                                        IsDelete = a.IsDelete,
                                        SalonId = a.SalonId,
                                        StatusId = a.StatusId,
                                        NoteSalon = a.NoteSalon,
                                        NoteKho = a.NoteKho,
                                        ProductIds = a.ProductIds,
                                        OrderDate = a.OrderDate,
                                        ExportTime = a.ExportTime,
                                        ReceivedTime = a.ReceivedTime,
                                        Order = a.Order,
                                        CheckCosmetic = a.CheckCosmetic,
                                        SalonName = c.Name,
                                        StatusTitle = b.Title,
                                    })
                                  .OrderBy(o => o.Id)
                                  .ToList();
                        Bind_Paging(data.Count);
                        rptDanhsach.DataSource = data.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                        rptDanhsach.DataBind();
                    }
                }
                catch { }
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            bindData();
            RemoveLoading();
        }

        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        private void PassDataFromServerToClient()
        {
            List<ElementEnable> list = listElement();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jSon = serializer.Serialize(list);
            string script = String.Format("<script type=\"text/javascript\">var listElement={0}</script>", jSon);
            if (!this.ClientScript.IsClientScriptBlockRegistered("clientScript"))
            {
                this.ClientScript.RegisterClientScriptBlock(this.GetType(), "clientScript", script, false);
            }
        }

        #region Bind chi tiết đơn đặt hàng 
        /// <summary>
        /// author: duy lê
        /// Bind chi tiết đơn đặt hàng 
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        [WebMethod]
        public static object getAllGoodsById(int orderId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var getGood = db.Store_Cosmetic_DetailById(orderId).ToList();
                if (getGood.Count > 0)
                {
                    getGood = db.Store_Cosmetic_DetailById(orderId).ToList();
                }
                else
                {
                    getAllGoodsByIdV2(orderId);
                }
                return getGood;
            }
        }
        #endregion

        public static List<Store_Cosmetic_QuantityBySalon_Result> getAllGoodsByIdV2(int orderId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var getGood = db.Store_Cosmetic_QuantityBySalon(orderId).ToList();
                return getGood;
            }
        }


        #region Update chi tiết đơn hàng

        /// <summary>
        /// author: duy lê
        /// Update chi tiết đơn hàng
        /// </summary>
        /// <param name="productIds"></param>
        /// <param name="orderId"></param>
        /// <param name="checkStatus"></param>
        /// <returns></returns>
        [WebMethod]
        public static bool UpdateOrderById(string productIds, int orderId, int checkStatus)
        {
            CultureInfo culture = new CultureInfo("vi-VN");
            int idInventory = 0;
            var serialize = new JavaScriptSerializer();
            var objectList = serialize.Deserialize<List<QLKho_SalonOrder_Flow>>(productIds).ToList();
            var error = 0;
            DateTime importFlowDate = new DateTime();
            using (var db = new Solution_30shineEntities())
            {
                QLKho_SalonOrder updateProduct = (from c in db.QLKho_SalonOrder
                                                  where c.Id == orderId
                                                  select c).FirstOrDefault();
                if (updateProduct != null)
                {
                    updateProduct.ProductIds = productIds;
                    db.SaveChanges();
                    var checkImport = db.Inventory_Import.FirstOrDefault(f => f.SalonId == updateProduct.SalonId && f.ImportDate == updateProduct.OrderDate);
                    if (checkImport == null)
                    {
                        checkImport = new Inventory_Import();
                        checkImport.RecipientId = Convert.ToInt32(HttpContext.Current.Session["User_Id"]);
                        checkImport.ProductIds = productIds;
                        checkImport.CreatedDate = DateTime.Now;
                        checkImport.IsDelete = false;
                        checkImport.SalonId = updateProduct.SalonId;
                        //checkImport.ImportDate = updateProduct.OrderDate;
                        checkImport.ImportDate = DateTime.Now;
                        checkImport.ImportType = 1;
                        db.Inventory_Import.Add(checkImport);
                        db.SaveChanges();
                        idInventory = checkImport.Id;
                    }
                    else
                    {
                        checkImport = (from c in db.Inventory_Import
                                       where c.SalonId == updateProduct.SalonId && c.ImportDate == updateProduct.OrderDate
                                       select c).FirstOrDefault();
                        checkImport.ProductIds = productIds;
                        checkImport.RecipientId = Convert.ToInt32(HttpContext.Current.Session["User_Id"]);
                        idInventory = checkImport.Id;
                        db.SaveChanges();
                    }
                    importFlowDate = Convert.ToDateTime(checkImport.ImportDate);
                }
                var flowOrder = db.QLKho_SalonOrder_Flow.FirstOrDefault(w => w.OrderId == orderId && w.IsDelete == false);
                if (flowOrder == null)
                {
                    flowOrder = new QLKho_SalonOrder_Flow();
                    flowOrder.OrderId = orderId;
                    flowOrder.IsDelete = false;
                    db.QLKho_SalonOrder_Flow.Add(flowOrder);
                    db.SaveChanges();
                }
                var obj = new ProductBasic();
                var DS2 = db.QLKho_SalonOrder_Flow.Where(w => w.OrderId == orderId && w.IsDelete != true).ToList();
                var DS1 = Library.Function.genListFromJson(productIds);
                var index = -1;
                var product = new Product();
                var item = new QLKho_SalonOrder_Flow();
                // Kiểm tra cập nhật Item
                if (DS2.Count > 0)
                {
                    foreach (var v in DS2)
                    {
                        var listAdd = Library.Function.genListFromJson(productIds).Where(w => w.Id == v.Id).FirstOrDefault();
                        product = db.Products.FirstOrDefault(w => w.Id == v.ProductId);
                        index = DS1.FindIndex(w => w.Id == v.Id);
                        if (index != -1)
                        {
                            obj = DS1[index];
                            if (checkStatus == 0)
                            {
                                v.QuantityReceived = obj.Quantity;
                            }
                            else if (checkStatus == 1)
                            {
                                v.QuantityOrder = obj.Quantity;
                            }
                            v.ModifiedTime = DateTime.Now;
                            if (product != null)
                            {
                                v.Cost = product.Cost;
                                v.Price = product.Price;
                            }
                            db.QLKho_SalonOrder_Flow.AddOrUpdate(v);
                            error += db.SaveChanges() > 0 ? 0 : 1;
                            if (obj.MapIdProduct != "")
                            {
                                //tách mapid 
                                string map = obj.MapIdProduct;
                                string[] mapid = map.Split(',');
                                for (int i = 0; i < mapid.Length; i++)
                                {
                                    //Insert vào flowproduct sau khi tách mapid
                                    var id = Convert.ToInt32(mapid[i]);
                                    var flowInventory = new Inventory_Flow();
                                    flowInventory.InvenId = idInventory;
                                    flowInventory.ProductId = id;
                                    flowInventory.Quantity = obj.Quantity;
                                    flowInventory.Cost = v.Cost;
                                    flowInventory.Price = v.Price;
                                    flowInventory.CreatedDate = importFlowDate;
                                    flowInventory.IsDelete = false;
                                    db.Inventory_Flow.Add(flowInventory);
                                    db.SaveChanges();
                                }
                            }
                            else
                            {
                                var flowInventory = new Inventory_Flow();
                                flowInventory.InvenId = idInventory;
                                flowInventory.ProductId = v.ProductId;
                                flowInventory.Quantity = obj.Quantity;
                                flowInventory.Cost = v.Cost;
                                flowInventory.Price = v.Price;
                                flowInventory.CreatedDate = importFlowDate;
                                flowInventory.IsDelete = false;
                                db.Inventory_Flow.Add(flowInventory);
                                db.SaveChanges();
                            }
                        }
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// author: duy lê
        /// update status = 3 và khóa trạng thái ô txtbox
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="noteKho"></param>
        /// <param name="noteSalon"></param>
        /// <param name="checkStatus"></param>
        /// <returns></returns>
        [WebMethod]
        public static bool UpdateQLKho_SalonOrder(int orderId, string noteKho, string noteSalon, int checkStatus)
        {
            QLKho_SalonOrder OBJ = new QLKho_SalonOrder();
            if (orderId > 0)
            {
                using (var db = new Solution_30shineEntities())
                {
                    var error = 0;
                    OBJ = db.QLKho_SalonOrder.FirstOrDefault(w => w.Id == orderId);
                    if (OBJ != null)
                    {
                        if (checkStatus == 0)
                        {
                            OBJ.StatusId = 3;
                            OBJ.NoteKho = noteKho;
                            OBJ.NoteSalon = noteSalon;
                            OBJ.ReceivedTime = DateTime.Now;
                        }
                        else if (checkStatus == 1)
                        {
                            OBJ.StatusId = 1;
                        }
                        OBJ.ModifiedTime = DateTime.Now;
                        db.QLKho_SalonOrder.AddOrUpdate(OBJ);
                        error += db.SaveChanges() > 0 ? 0 : 1;

                    }
                }
            }
            return true;
        }
        #endregion
        #region Bind danh sách hàng trả thiếu

        [WebMethod]
        public static object ProductsOweList(int salonId, DateTime currentOrderDate)
        {
            using (var db = new Solution_30shineEntities())
            {
                return db.Store_QLKho_DanhSachTraThieu(null, currentOrderDate, salonId).ToList();
            }
        }
        #endregion

        protected List<ElementEnable> listElement()
        {
            List<ElementEnable> list = new List<ElementEnable>();
            list.Add(new ElementEnable() { ElementName = "NoteKho", Enable = !Perm_ViewAllData, Type = "disable" });
            list.Add(new ElementEnable() { ElementName = "NoteSalon", Enable = !Perm_Access, Type = "disable" });
            list.Add(new ElementEnable() { ElementName = "tdOrderID", Enable = !Perm_ViewAllData, Type = "hidden" });
            list.Add(new ElementEnable() { ElementName = "del_Bill", Enable = !Perm_ViewAllData, Type = "hidden" });
            return list;
        }
    }
}