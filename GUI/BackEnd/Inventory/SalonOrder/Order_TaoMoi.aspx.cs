﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Helpers;
using System.Text.RegularExpressions;
using System.Globalization;
using LinqKit;
using Project.Helpers;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Inventory.SalonOrder
{
    public partial class Order_TaoMoi : System.Web.UI.Page
    {
        private string PageID = "";
        protected bool Perm_ShowSalon = false;
        protected bool Perm_AllSalon = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;

        private List<ProductBasic> ProductList = new List<ProductBasic>();
        private List<ProductBasic> ServiceList = new List<ProductBasic>();
        protected int SalonId = 0;
        private List<int> lstFreeService = new List<int>();
        protected int Id;
        private int integer;
        private CultureInfo culture = new CultureInfo("vi-VN");

        protected QLKho_SalonOrder OBJ;
        protected string ListProduct;
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                if (Request.QueryString["Code"] != null)
                {
                    PageID = "DHK_SL_EDIT";
                }
                else
                {
                    PageID = "DV_SLDH_ADD";
                }
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ShowSalon = permissionModel.GetActionByActionNameAndPageId("Perm_ShowSalon", PageID, permission);
                //Perm_AllSalon = permissionModel.GetActionByActionNameAndPageId("Perm_AllSalon", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                Perm_AllSalon = permissionModel.CheckPermisionByAction("Perm_AllSalon", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (IsAccountant)
            {
                UIHelpers.Redirect("/dat-hang-kho/salon/danh-sach", null);
            }
            else
            {
                Id = getId();
                if (!IsPostBack)
                {
                    Library.Function.bindSalon(new List<DropDownList>() { Salon }, Perm_AllSalon);
                    Bind_OBJ();
                    //Bind_RptProduct();
                    //Bind_RptProductFeatured();
                    //bindProductCategory();
                    PassDataFromServerToClient();
                    TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Today);
                    TxtDateTimeFrom.Enabled = false;
                }
                ListProduct = getProducts();
            }
        }

        /// <summary>
        /// get Id của lô chuyển mỹ phẩm
        /// </summary>
        /// <returns></returns>
        private int getId()
        {
            return int.TryParse(Request.QueryString["Id"], out integer) ? integer : 0;
        }

        /// <summary>
        /// get Id đăng nhập và chuyển mỹ phẩm
        /// </summary>
        /// <returns></returns>
        private int getExportUserId()
        {
            if (Session["User_Id"] != null)
            {
                return int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// get Id người nhận mỹ phẩm
        /// </summary>
        /// <returns></returns>
        private int getRecipientId()
        {
            return int.TryParse(Recipient.Value, out integer) ? integer : 0;
        }

        /// <summary>
        /// Xuất lô mỹ phẩm
        /// </summary>
        private void Add()
        {

            using (var db = new Solution_30shineEntities())
            {
                var serialize = new JavaScriptSerializer();
                var obj = new QLKho_SalonOrder();
                var error = 0;
                obj.SalonId = Convert.ToInt32(Salon.SelectedValue);
                obj.ProductIds = HDF_ProductIds.Value != "[]" ? HDF_ProductIds.Value : "";
                //obj.ProductIds = serialize.Serialize(Library.Function.genListFromJson(obj.ProductIds));
                ProductList = serialize.Deserialize<List<ProductBasic>>(obj.ProductIds);
                foreach (var v in ProductList)
                {
                    if (v.Order == null)
                    {
                        obj.Order = 0;
                    }
                    else
                    {
                        obj.Order = v.Order;
                    }
                }
                obj.IsDelete = false;
                if (TxtDateTimeFrom.Text != "")
                {
                    obj.OrderDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                }
                obj.CreatedTime = DateTime.Now;
                obj.NoteSalon = Note.Text;
                obj.StatusId = 1;
                obj.BillCode = GenBillCode(obj.SalonId.Value, "HDDHK", 4);

                db.QLKho_SalonOrder.Add(obj);
                error += db.SaveChanges() > 0 ? 0 : 1;
                if (error == 0)
                {
                    error += AddFlow(obj.ProductIds, obj.Id);
                }

                if (error == 0)
                {
                    //set session = null khi thêm mới thành công!
                    Session["fromDate"] = null;
                    Session["salonId"] = null;
                    Session["toDate"] = null;
                    // Thông báo thành công
                    var MsgParam = new List<KeyValuePair<string, string>>();
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                    UIHelpers.Redirect("/dat-hang-kho/salon/danh-sach");
                }
                else
                {
                    MsgSystem.Text = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                    MsgSystem.CssClass = "msg-system warning";
                }
            }
        }

        /// <summary>
        /// Cập nhật lô mỹ phẩm
        /// </summary>
        /// <param name="Id"></param>
        private void Update(int Id)
        {
            if (Id > 0)
            {
                using (var db = new Solution_30shineEntities())
                {
                    var error = 0;
                    var serialize = new JavaScriptSerializer();
                    OBJ = db.QLKho_SalonOrder.FirstOrDefault(w => w.Id == Id);
                    if (OBJ != null)
                    {
                        OBJ.SalonId = Convert.ToInt32(Salon.SelectedValue);
                        OBJ.ModifiedTime = DateTime.Now;
                        OBJ.NoteSalon = Note.Text;
                        OBJ.ProductIds = HDF_ProductIds.Value != "[]" ? HDF_ProductIds.Value : "";
                        OBJ.ProductIds = serialize.Serialize(Library.Function.genListFromJson(OBJ.ProductIds));
                        db.QLKho_SalonOrder.AddOrUpdate(OBJ);
                        error += db.SaveChanges() > 0 ? 0 : 1;
                        if (error == 0)
                        {
                            error += UpdateFlow(OBJ.ProductIds, OBJ.Id);
                        }
                        if (error == 0)
                        {
                            // Thông báo thành công
                            var MsgParam = new List<KeyValuePair<string, string>>();
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                            MsgParam.Add(new KeyValuePair<string, string>("id", Id.ToString()));
                            UIHelpers.Redirect("/dat-hang-kho/salon/" + OBJ.Id + ".html", MsgParam);
                        }
                        else
                        {
                            MsgSystem.Text = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                            MsgSystem.CssClass = "msg-system warning";
                        }
                    }
                }
            }
        }

        private int AddFlow(string productIds, int orderId)
        {
            var error = 0;
            if (productIds != "")
            {
                using (var db = new Solution_30shineEntities())
                {
                    var obj = new QLKho_SalonOrder_Flow();
                    var list = Library.Function.genListFromJson(productIds);
                    var product = new Product();
                    if (list.Count > 0)
                    {
                        foreach (var v in list)
                        {
                            product = db.Products.FirstOrDefault(w => w.Id == v.Id);
                            obj = new QLKho_SalonOrder_Flow();
                            obj.OrderId = orderId;
                            obj.ProductId = v.Id;
                            obj.QuantityOrder = v.Quantity;
                            if (product != null)
                            {
                                obj.Cost = product.Cost;
                                obj.Price = product.Price;
                            }
                            obj.CreatedTime = DateTime.Now;
                            obj.IsDelete = false;
                            db.QLKho_SalonOrder_Flow.Add(obj);
                            error += db.SaveChanges() > 0 ? 0 : 1;
                        }
                    }
                }
            }
            return error;
        }

        /// <summary>
        /// Cập nhật trên dòng chi tiết flow
        /// DS1: Danh sách mới được post lên, DS2: Danh sách lấy từ database
        /// 1. Kiểm tra xóa item : Check trong DS2, nếu item nào không có trong DS1 thì XÓA, có thì CẬP NHẬT
        /// 2. Kiểm tra thêm item : Check trong DS1, nếu item nào không có trong DS2 thì THÊM
        /// </summary>
        /// <param name="productIds"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        private int UpdateFlow(string productIds, int orderId)
        {
            var error = 0;
            if (productIds != "")
            {
                using (var db = new Solution_30shineEntities())
                {
                    var obj = new ProductBasic();
                    var DS1 = Library.Function.genListFromJson(productIds);
                    var DS2 = db.QLKho_SalonOrder_Flow.Where(w => w.OrderId == orderId && w.IsDelete != true).ToList();
                    var index = -1;
                    var product = new Product();
                    var item = new QLKho_SalonOrder_Flow();
                    // Kiểm tra xóa Item
                    if (DS2.Count > 0)
                    {
                        foreach (var v in DS2)
                        {
                            product = db.Products.FirstOrDefault(w => w.Id == v.ProductId);
                            index = DS1.FindIndex(w => w.Id == v.ProductId);
                            if (index == -1)
                            {
                                v.IsDelete = true;
                                db.QLKho_SalonOrder_Flow.AddOrUpdate(v);
                                error += db.SaveChanges() > 0 ? 0 : 1;
                            }
                            else
                            {
                                obj = DS1[index];
                                v.QuantityOrder = obj.Quantity;
                                v.ModifiedTime = DateTime.Now;
                                if (product != null)
                                {
                                    v.Cost = product.Cost;
                                    v.Price = product.Price;
                                }
                                db.QLKho_SalonOrder_Flow.AddOrUpdate(v);
                                error += db.SaveChanges() > 0 ? 0 : 1;
                            }
                        }
                    }

                    // Kiểm tra thêm mới Item
                    if (DS1.Count > 0)
                    {
                        foreach (var v in DS1)
                        {
                            product = db.Products.FirstOrDefault(w => w.Id == v.Id);
                            index = DS2.FindIndex(w => w.ProductId == v.Id);
                            if (index == -1)
                            {
                                item = new QLKho_SalonOrder_Flow();
                                item.ProductId = v.Id;
                                item.QuantityOrder = v.Quantity;
                                item.QuantityReceived = 0;
                                item.OrderId = orderId;
                                item.CreatedTime = DateTime.Now;
                                item.IsDelete = false;
                                if (product != null)
                                {
                                    item.Cost = product.Cost;
                                    item.Price = product.Price;
                                }

                                db.QLKho_SalonOrder_Flow.AddOrUpdate(item);
                                error += db.SaveChanges() > 0 ? 0 : 1;
                            }
                        }
                    }
                }
            }
            return error;
        }

        protected void Action(object sender, EventArgs e)
        {
            if (Id > 0)
            {
                Update(Id);
            }
            else
            {
                Add();
            }
        }

        private bool Bind_OBJ()
        {
            var isset = false;
            using (var db = new Solution_30shineEntities())
            {
                if (Id > 0)
                {
                    OBJ = db.QLKho_SalonOrder.FirstOrDefault(w => w.Id == Id);
                    if (OBJ != null)
                    {
                        isset = true;
                        // bind salon selected
                        var ItemSelected = Salon.Items.FindByValue(OBJ.SalonId.ToString());
                        if (ItemSelected != null)
                        {
                            ItemSelected.Selected = true;
                        }
                        else
                        {
                            Salon.SelectedIndex = 0;
                        }

                        // bind danh sách mỹ phẩm
                        if (OBJ.ProductIds != "")
                        {
                            Rpt_Product_Flow.DataSource = Library.Function.genListFromJson(OBJ.ProductIds);
                            Rpt_Product_Flow.DataBind();
                            ListingProductWp.Style.Add("display", "block");
                        }
                        // bind note
                        Note.Text = OBJ.NoteSalon;
                    }
                    if (!isset)
                    {
                        MsgSystem.Text = "Lỗi. Dữ liệu không tồn tại.";
                        MsgSystem.CssClass = "msg-system warning";
                    }
                }
            }

            return isset;
        }

        /// <summary>
        /// Lấy danh sách product
        /// </summary>
        /// <returns></returns>
        private string getProducts()
        {
            using (var db = new Solution_30shineEntities())
            {
                var data = db.Products.Where(w => (w.IsDelete != 1 && w.Publish == 1 && w.CategoryId == 20) || (w.Id == 378 || w.Id == 63 || w.Id == 44 || w.Id == 58 || w.Id == 83 || w.Id == 440 || w.Id == 32 || w.Id == 446)).ToList();
                var serialize = new JavaScriptSerializer();
                return serialize.Serialize(data);
            }
        }

        public string GenBillCode(int salonId, string prefix, int template = 4)
        {
            using (var db = new Solution_30shineEntities())
            {
                string code = "";
                string temp = "";
                int integer;
                var Where = PredicateBuilder.True<QLKho_SalonOrder>();
                DateTime today = DateTime.Today;
                Where = Where.And(w => w.IsDelete != true && w.OrderDate == today);
                if (salonId > 0)
                {
                    Where = Where.And(w => w.SalonId == salonId);
                }
                var LastBill = db.QLKho_SalonOrder.AsExpandable().Where(Where).OrderByDescending(w => w.Id).Take(1).ToList();
                if (LastBill.Count > 0)
                {
                    if (LastBill[0].BillCode != null && LastBill[0].BillCode != "")
                        temp = (int.TryParse(LastBill[0].BillCode.Substring(LastBill[0].BillCode.Length - template, template), out integer) ? ++integer : 0).ToString();
                    else
                        temp = "1";
                }
                else
                {
                    temp = "1";
                }

                int len = temp.Length;
                for (var i = 0; i < template; i++)
                {
                    if (len > 0)
                    {
                        code = temp[--len].ToString() + code;
                    }
                    else
                    {
                        code = "0" + code;
                    }
                }

                return prefix + String.Format("{0:ddMMyy}", DateTime.Now) + code;
            }
        }


        private void PassDataFromServerToClient()
        {
            List<ElementEnable> list = listElement();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jSon = serializer.Serialize(list);
            string script = String.Format("<script type=\"text/javascript\">var listElement={0}</script>", jSon);
            if (!this.ClientScript.IsClientScriptBlockRegistered("clientScript"))
            {
                this.ClientScript.RegisterClientScriptBlock(this.GetType(), "clientScript", script, false);
            }
        }
        protected List<ElementEnable> listElement()
        {
            List<ElementEnable> list = new List<ElementEnable>();
            list.Add(new ElementEnable() { ElementName = "salonSelect", Enable = !Perm_ShowSalon, Type = "hidden" });
            //list.Add(new ElementEnable() { ElementName = "maSPcol", Enable = !Perm_ShowSalon, Type = "hidden" });
            return list;
        }
    }
}