﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Helpers;
using System.Text.RegularExpressions;
using System.Globalization;
using LinqKit;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Inventory.SalonOrder
{
    public partial class Order_KiemHang : System.Web.UI.Page
    {
        protected bool Perm_ShowSalon = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;

        private List<ProductBasic_QLKho> ProductList = new List<ProductBasic_QLKho>();
        private List<ProductBasic_QLKho> ServiceList = new List<ProductBasic_QLKho>();
        protected int SalonId = 0;
        private List<int> lstFreeService = new List<int>();
        protected int Id;
        private int integer;
        private CultureInfo culture = new CultureInfo("vi-VN");

        private QLKho_SalonOrder OBJ = new QLKho_SalonOrder();
        private string PageID = "DHK_SL_KH";
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (IsAccountant)
            {
                UIHelpers.Redirect("/dat-hang-kho/salon/danh-sach", null);
            }
            else
            {
                Id = getId();
                if (!IsPostBack)
                {
                    Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ShowSalon);
                    Bind_OBJ();
                    Bind_RptProduct();
                    Bind_RptProductFeatured();
                    bindProductCategory();
                    TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Today);
                    TxtDateTimeFrom.Enabled = false;
                }
            }
        }
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ShowSalon = permissionModel.GetActionByActionNameAndPageId("Perm_ShowSalon", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        /// <summary>
        /// get Id của lô chuyển mỹ phẩm
        /// </summary>
        /// <returns></returns>
        private int getId()
        {
            return int.TryParse(Request.QueryString["Id"], out integer) ? integer : 0;
        }

        /// <summary>
        /// get Id đăng nhập và chuyển mỹ phẩm
        /// </summary>
        /// <returns></returns>
        private int getExportUserId()
        {
            if (Session["User_Id"] != null)
            {
                return int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// get Id người nhận mỹ phẩm
        /// </summary>
        /// <returns></returns>
        private int getRecipientId()
        {
            return int.TryParse(Recipient.Value, out integer) ? integer : 0;
        }

        /// <summary>
        /// Cập nhật lô mỹ phẩm
        /// </summary>
        /// <param name="Id"></param>
        private void Update(int Id)
        {
            if (Id > 0)
            {
                using (var db = new Solution_30shineEntities())
                {
                    var error = 0;
                    var serialize = new JavaScriptSerializer();
                    OBJ = db.QLKho_SalonOrder.FirstOrDefault(w => w.Id == Id);
                    if (OBJ != null)
                    {
                        OBJ.StatusId = 3;
                        OBJ.ReceivedTime = DateTime.Now;
                        OBJ.ModifiedTime = DateTime.Now;
                        OBJ.NoteSalon = Note.Text;
                        db.QLKho_SalonOrder.AddOrUpdate(OBJ);
                        error += db.SaveChanges() > 0 ? 0 : 1;

                        string productIds = HDF_ProductIds.Value != "[]" ? HDF_ProductIds.Value : "";
                        productIds = serialize.Serialize(Library.Function.genListFromJson(productIds));

                        if (error == 0)
                        {
                            error += UpdateFlow(productIds, OBJ.Id);
                        }
                        if (error == 0)
                        {
                            // Thông báo thành công
                            var MsgParam = new List<KeyValuePair<string, string>>();
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                            MsgParam.Add(new KeyValuePair<string, string>("id", Id.ToString()));
                            UIHelpers.Redirect("/dat-hang-kho/salon/kiem-hang/" + OBJ.Id + ".html", MsgParam);
                        }
                        else
                        {
                            MsgSystem.Text = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                            MsgSystem.CssClass = "msg-system warning";
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Cập nhật trên dòng chi tiết flow
        /// DS1: Danh sách mới được post lên, DS2: Danh sách lấy từ database
        /// 1. Kiểm tra xóa item : Check trong DS2, nếu item nào không có trong DS1 thì XÓA, có thì CẬP NHẬT
        /// 2. Kiểm tra thêm item : Check trong DS1, nếu item nào không có trong DS2 thì THÊM
        /// </summary>
        /// <param name="productIds"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        private int UpdateFlow(string productIds, int orderId)
        {
            var error = 0;
            if (productIds != "")
            {
                using (var db = new Solution_30shineEntities())
                {
                    var obj = new ProductBasic();
                    var DS1 = Library.Function.genListFromJson(productIds);
                    var DS2 = db.QLKho_SalonOrder_Flow.Where(w => w.OrderId == orderId && w.IsDelete != true).ToList();
                    var index = -1;
                    var product = new Product();
                    var item = new QLKho_SalonOrder_Flow();
                    // Kiểm tra cập nhật Item
                    if (DS2.Count > 0)
                    {
                        foreach (var v in DS2)
                        {
                            product = db.Products.FirstOrDefault(w => w.Id == v.ProductId);
                            index = DS1.FindIndex(w => w.Id == v.ProductId);
                            if (index != -1)
                            {
                                obj = DS1[index];
                                v.QuantityReceived = obj.Quantity;
                                v.ModifiedTime = DateTime.Now;
                                db.QLKho_SalonOrder_Flow.AddOrUpdate(v);
                                error += db.SaveChanges() > 0 ? 0 : 1;
                            }
                        }
                    }
                }
            }
            return error;
        }

        protected void Action(object sender, EventArgs e)
        {
            if (Id > 0)
            {
                Update(Id);
            }
            else
            {
                //Add();
            }
        }

        private bool Bind_OBJ()
        {
            var isset = false;
            using (var db = new Solution_30shineEntities())
            {
                if (Id > 0)
                {
                    OBJ = db.QLKho_SalonOrder.FirstOrDefault(w => w.Id == Id);
                    if (OBJ != null)
                    {
                        isset = true;
                        // bind salon selected
                        var ItemSelected = Salon.Items.FindByValue(OBJ.SalonId.ToString());
                        if (ItemSelected != null)
                        {
                            ItemSelected.Selected = true;
                        }
                        else
                        {
                            Salon.SelectedIndex = 0;
                        }

                        // bind danh sách mỹ phẩm
                        if (OBJ.ProductIds != "")
                        {
                            Rpt_Product_Flow.DataSource = db.Store_QLKho_DetailById(OBJ.Id).ToList();
                            Rpt_Product_Flow.DataBind();
                            ListingProductWp.Style.Add("display", "block");
                        }
                        // bind note
                        Note.Text = OBJ.NoteSalon;
                    }
                    if (!isset)
                    {
                        MsgSystem.Text = "Lỗi. Dữ liệu không tồn tại.";
                        MsgSystem.CssClass = "msg-system warning";
                    }
                }
            }

            return isset;
        }


        private void Bind_RptProduct()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Products = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1).OrderByDescending(o => o.Id).ToList();

                Rpt_Product.DataSource = _Products;
                Rpt_Product.DataBind();
            }
        }

        private void Bind_RptProductFeatured()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1 && w.Status == 1).OrderByDescending(o => o.Id).ToList();

                Rpt_ProductFeatured.DataSource = lst;
                Rpt_ProductFeatured.DataBind();
            }
        }

        private void bindProductCategory()
        {
            using (var db = new Solution_30shineEntities())
            {
                //var list = db.Tbl_Category.Where(w=>w.Pid == 1 && w.IsDelete != 1 && w.Publish == true).ToList();
                ////var c = new Tbl_Category();
                ////c.Id = 0;
                ////c.Name = "Chọn danh mục";
                ////list.Insert(0, c);
                //rptProductCategory.DataSource = list;
                //rptProductCategory.DataBind();
                var list = new List<Tbl_Category>();
                var node = new List<Tbl_Category>();
                var c = new Tbl_Category();
                c.Id = 0;
                c.Name = "Chọn danh mục";
                list.Insert(0, c);
                var cateRoot = db.Tbl_Category.Where(w => w.IsDelete != 1 && w.Pid == 1 && w.Publish == true).ToList();
                if (cateRoot.Count > 0)
                {
                    foreach (var v in cateRoot)
                    {
                        node = db.Tbl_Category.Where(w => w.IsDelete != 1 && w.Pid == v.Id).ToList();
                        list.Add(v);
                        if (node.Count > 0)
                        {
                            foreach (var v2 in node)
                            {
                                v2.Name = "----- " + v2.Name;
                                list.Add(v2);
                            }
                        }
                    }
                }
                rptProductCategory.DataSource = list;
                rptProductCategory.DataBind();
            }
        }

        /// <summary>
        /// Reset Rating Temp Value
        /// </summary>
        private void resetRatingTemp()
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                /// Reset giá trị rating temp
                int accId = Session["User_Id"] != null ? int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0 : 0;
                int salonId = Session["SalonId"] != null ? int.TryParse(Session["SalonId"].ToString(), out integer) ? integer : 0 : 0;
                var ratingTemp = db.Rating_Temp.FirstOrDefault(w => w.SalonId == salonId && w.AccountId == accId);
                if (ratingTemp != null)
                {
                    ratingTemp.RatingValue = 0;
                    db.Rating_Temp.AddOrUpdate(ratingTemp);
                    db.SaveChanges();
                }
            }
        }

        [WebMethod]
        public static string GetCustomer(string CustomerCode)
        {
            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var _Customer = db.Customers.Where(w => w.Customer_Code == CustomerCode).ToList();

                if (_Customer.Count > 0)
                {
                    _Msg.success = true;
                    _Msg.msg = serializer.Serialize(_Customer);
                }
                else
                {
                    _Msg.success = false;
                }
            }
            return serializer.Serialize(_Msg);
        }

        [WebMethod]
        public static string CheckDuplicateBill(string CustomerCode)
        {
            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var today = Convert.ToDateTime(DateTime.Now.ToString("d/M/yyyy"), culture);
                var todayL = today.AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                var todayR = today.AddDays(1);
                var _Bill = db.BillServices.FirstOrDefault(w => w.CreatedDate > todayL && w.CreatedDate < todayR && w.CustomerCode == CustomerCode);

                if (_Bill != null)
                {
                    _Msg.success = true;
                    _Msg.msg = serializer.Serialize(_Bill);
                }
                else
                {
                    _Msg.success = false;
                }
            }
            return serializer.Serialize(_Msg);
        }

        /// <summary>
        /// Lấy toàn bộ danh sách vật tư xuất cho salon
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static object getAllGoods()
        {
            using (var db = new Solution_30shineEntities())
            {
                return db.Products.Where(w => w.IsDelete != 1/* && w.Publish == 1*/ && w.CategoryId == 20).Select(s => new { s.Id, s.Code, s.Name, s.Cost, s.Price, Quantity = 1 }).ToList();
            }
        }

        public string GenBillCode(int salonId, string prefix, int template = 4)
        {
            using (var db = new Solution_30shineEntities())
            {
                string code = "";
                string temp = "";
                int integer;
                var Where = PredicateBuilder.True<QLKho_SalonOrder>();
                DateTime today = DateTime.Today;
                Where = Where.And(w => w.IsDelete != true && w.OrderDate == today);
                if (salonId > 0)
                {
                    Where = Where.And(w => w.SalonId == salonId);
                }
                var LastBill = db.QLKho_SalonOrder.AsExpandable().Where(Where).OrderByDescending(w => w.Id).Take(1).ToList();
                if (LastBill.Count > 0)
                {
                    if (LastBill[0].BillCode != null && LastBill[0].BillCode != "")
                        temp = (int.TryParse(LastBill[0].BillCode.Substring(LastBill[0].BillCode.Length - template, template), out integer) ? ++integer : 0).ToString();
                    else
                        temp = "1";
                }
                else
                {
                    temp = "1";
                }

                int len = temp.Length;
                for (var i = 0; i < template; i++)
                {
                    if (len > 0)
                    {
                        code = temp[--len].ToString() + code;
                    }
                    else
                    {
                        code = "0" + code;
                    }
                }

                return prefix + String.Format("{0:ddMMyy}", DateTime.Now) + code;
            }
        }


    }
}