﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Order_XuatHang.aspx.cs" Inherits="_30shine.GUI.BackEnd.Inventory.SalonOrder.Order_XuatHang" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ServiceAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <link href="/Assets/css/bootstrap/bootstrap-toggle.min.css" rel="stylesheet" />
        <script src="/Assets/js/bootstrap/bootstrap-toggle.min.js"></script>

        <style>
            .fe-service .tr-product .table-listing-product tbody input.product-quantity {
                border: 1px solid #ddd !important;
            }
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý đặt hàng nội bộ &nbsp;&#187; Salon đặt hàng kho</li>
                        <li class="li-listing"><a href="/dat-hang-kho/kho/danh-sach">Danh sách</a></li>
                        <li class="li-export"><a href="javascript:void(0);">Xuất hàng</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add admin-product-add fe-service">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <!-- System Message -->
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <!-- END System Message -->

                <div class="table-wp">
                    <table class="table-add admin-product-table-add fe-service-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Lập đơn đặt hàng</strong></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr class="tr-margin" style="height: 20px;"></tr>

                            <tr class="tr-field-ahalf" runat="server" id="TrSalon">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right">
                                    <label style="float: left; line-height: 36px; padding-right: 10px; font-weight: normal; font-family: Roboto Condensed Bold;">Ngày</label>
                                    <div class="datepicker-wp">
                                        <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Chọn ngày"
                                            ClientIDMode="Static" runat="server" Style="width: 160px; margin-right: 10px;"></asp:TextBox>
                                    </div>
                                    <label style="float: left; line-height: 36px; padding-right: 10px; font-weight: normal; font-family: Roboto Condensed Bold;">Salon</label>
                                    <div class="field-wp" style="float: left; width: auto;">
                                        <asp:DropDownList ID="Salon" runat="server" CssClass="form-control select" ClientIDMode="Static" Style="width: 160px;"></asp:DropDownList>
                                        <asp:RequiredFieldValidator InitialValue="0"
                                            ID="ValidateSalon" Display="Dynamic"
                                            ControlToValidate="Salon"
                                            runat="server" Text="Bạn chưa chọn Salon!"
                                            ErrorMessage="Vui lòng chọn Salon!"
                                            ForeColor="Red"
                                            CssClass="fb-cover-error">
                                        </asp:RequiredFieldValidator>
                                    </div>
                                    <%--<label style="float: left; line-height: 36px; padding-left: 10px; padding-right: 10px; font-weight: normal; font-family: Roboto Condensed Bold;">Nguời nhận</label>
                            <div class="filter-item" style="width: 180px;">
                                <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion auto-select" data-field="Recipient" data-value="0"
                                        AutoCompleteType="Disabled" ID="InputRecipient" ClientIDMode="Static" placeholder="Nhập mã số" runat="server"></asp:TextBox>
                                <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                                    <ul class="ul-listing-staff ul-listing-suggestion" id="Ul1"></ul>
                                </div>
                                <div class="fake-value" id="FVRecipient" runat="server" ClientIDMode="Static"></div>
                            </div>--%>
                                </td>
                            </tr>

                            <tr class="tr-field-ahalf tr-product">
                                <td class="col-xs-2 left"><span>Sản phẩm</span></td>
                                <td class="col-xs-9 right">
                                    <div class="row" id="quick-product" style="display: none;">
                                        <asp:Repeater runat="server" ID="Rpt_ProductFeatured">
                                            <ItemTemplate>
                                                <div class="checkbox cus-no-infor">
                                                    <label class="lbl-cus-no-infor">
                                                        <input type="checkbox" data-code="<%# Eval("Code") %>" onclick="pushQuickData($(this), 'product')" />
                                                        <%# Eval("Name") %>
                                                    </label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>

                                    </div>
                                    <div class="listing-product item-product" id="ListingProductWp" runat="server" clientidmode="Static">
                                        <table class="table table-listing-product table-item-product" id="table-item-product">
                                            <thead>
                                                <tr>
                                                    <th>STT</th>
                                                    <th>Tên sản phẩm</th>
                                                    <th style="display: none;">Mã sản phẩm</th>
                                                    <th style="display: none;">Đơn giá nhập</th>
                                                    <th style="width: 100px;">Số lượng hàng đặt</th>
                                                    <th style="width: 100px;">Số lượng xuất thực tế</th>
                                                    <th style="display: none;">Giảm giá</th>
                                                    <%--<th>Thành tiền</th>--%>
                                                    <th style="display: none;"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="Rpt_Product_Flow" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                                            <td class="td-product-name"><%# Eval("Name") %></td>
                                                            <td class="td-product-code" data-id="<%# Eval("ProductId") %>" style="display: none;"></td>
                                                            <td class="td-product-price" data-price="<%# Eval("Cost") %>" style="display: none;"><%# Eval("Cost") %></td>
                                                            <td><%# Eval("QuantityOrder") %></td>
                                                            <td class="td-product-quantity map-edit">
                                                                <input type="text" class="product-quantity" value="<%# Eval("QuantityExport") %>" />
                                                            </td>
                                                            <td class="td-product-voucher" data-voucher="" style="display: none;">
                                                                <div class="row">
                                                                    <input type="text" class="product-voucher" value="" style="margin: 0 auto; float: none; text-align: center; width: 50px;" />
                                                                </div>
                                                                <div class="row promotion-money">
                                                                    <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                                                        <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                                            <input type="checkbox" class="item-promotion" data-value="35000" data-id="1" style="margin-left: 0;" />
                                                                            - 35.000 VNĐ
                                                                        </label>

                                                                    </div>
                                                                    <br />
                                                                    <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                                                        <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                                            <input type="checkbox" class="item-promotion" data-id="2" data-value="50000" style="margin-left: 0;" />
                                                                            - 50.000 VNĐ
                                                                        </label>

                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td class="map-edit" style="display: none;">
                                                                <div class="box-money"></div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>

                            <tr class="tr-description">
                                <td class="col-xs-2 left"><span>Ghi chú</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox TextMode="MultiLine" Rows="5" ID="Note" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>



                            <tr class="tr-send">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <asp:Panel ID="BtnSend" CssClass="btn-send" runat="server" ClientIDMode="Static">Xuất hàng</asp:Panel>
                                        <asp:Button ID="BtnFakeSend" OnClick="Action" runat="server" Text="Hoàn tất"
                                            ClientIDMode="Static" Style="display: none;"></asp:Button>
                                        <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>
                                    </span>
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
                        </tbody>
                    </table>

                    <!-- input hidden -->
                    <asp:HiddenField ID="HDF_CustomerCode" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_ProductIds" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_ServiceIds" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_TotalMoney" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_UserImages" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_UserImagesDelete" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="Recipient" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HairMassage" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="Cosmetic" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_SalonId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_lgId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_BillId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_FreeService" ClientIDMode="Static" runat="server" />
                    <asp:HiddenField ID="HDF_Rating" ClientIDMode="Static" runat="server" />
                    <!-- END input hidden -->
                </div>
            </div>
            <%-- end Add --%>
        </div>

        <!-- Danh mục sản phẩm -->
        <div class="popup-product-wp popup-product-item">
            <div class="wp popup-product-head">
                <strong>Danh mục sản phẩm</strong>
            </div>
            <div class="wp popup-product-content">
                <div class="wp popup-product-guide">
                    <%--<div class="left">Hướng dẫn:</div>
            <div class="right">
                <p>- Click vào ô check box để chọn sản phẩm</p>
                <p>- Click vào ô số lượng để thay đổi số lượng sản phẩm</p>
            </div>--%>
                    <label style="font-family: Roboto Condensed Bold; font-weight: normal; font-size: 14px; color: #222222; margin-right: 10px;">Chọn danh mục</label>
                    <select onchange="showProductByCategory($(this))" id="ddlProductCategory" style="color: #222222; font-size: 14px; padding: 0 5px;">
                        <asp:Repeater runat="server" ID="rptProductCategory">
                            <ItemTemplate>
                                <option value="<%# Eval("Id") %>" style="color: #222222; font-size: 14px;"><%# Eval("Name") %></option>
                            </ItemTemplate>
                        </asp:Repeater>
                    </select>
                </div>
                <div class="wp listing-product item-product">
                    <table class="table" id="table-product">
                        <thead>
                            <tr>
                                <th>
                                    <input type="checkbox" /></th>
                                <th>STT</th>
                                <th>Tên sản phẩm</th>
                                <th style="display: none;">Mã sản phẩm</th>
                                <th style="display: none;">Đơn giá nhập</th>
                                <th class="th-product-quantity">Số lượng</th>
                                <th style="display: none;">Giảm giá</th>
                                <%--<th>Thành tiền</th>--%>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="Rpt_Product" runat="server">
                                <ItemTemplate>
                                    <tr data-cate="<%# Eval("CategoryId") %>">
                                        <td class="td-product-checkbox item-product-checkbox">
                                            <input type="checkbox" value="<%# Eval("Id") %>"
                                                data-code="<%# Eval("Code") %>" data-name="<%# Eval("Name") %>" />
                                        </td>
                                        <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                        <td class="td-product-name"><%# Eval("Name") %></td>
                                        <td class="td-product-code" data-id="<%# Eval("Id") %>" data-code="<%# Eval("Code") %>" style="display: none;"><%# Eval("Code") %></td>
                                        <td class="td-product-price" data-price="<%# Eval("Cost") %>" style="display: none;"><%# Eval("Cost") %></td>
                                        <td class="td-product-quantity map-edit">
                                            <input type="text" class="product-quantity" value="1" />
                                            <div class="edit-wp">
                                                <a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),'<%# Eval("Name") %>','product')"
                                                    href="javascript://" title="Xóa"></a>
                                            </div>
                                        </td>
                                        <td class="td-product-voucher" data-voucher="<%# Eval("VoucherPercent") %>" style="display: none;">
                                            <div class="row">
                                                <input type="text" class="product-voucher" value="<%# Eval("VoucherPercent") %>" style="margin: 0 auto; float: none; text-align: center; width: 50px;" />
                                                %
                                            </div>
                                            <div class="row promotion-money">
                                                <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                                    <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                        <input type="checkbox" class="item-promotion" data-value="35000" data-id="1" style="margin-left: 0;" />
                                                        - 35.000 VNĐ
                                                    </label>

                                                </div>
                                                <br />
                                                <div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">
                                                    <label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">
                                                        <input type="checkbox" class="item-promotion" data-id="2" data-value="50000" style="margin-left: 0;" />
                                                        - 50.000 VNĐ
                                                    </label>

                                                </div>
                                            </div>
                                        </td>
                                        <%--<td class="map-edit">
                                    <div class="box-money"></div>
                                    <div class="edit-wp">
                                        <a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),'<%# Eval("Name") %>','product')"
                                             href="javascript://" title="Xóa"></a>
                                    </div>
                                </td>--%>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
                <div class="wp btn-wp">
                    <div class="popup-product-btn btn-complete" data-item="product">Hoàn tất</div>
                    <div class="popup-product-btn btn-esc">Thoát</div>
                </div>
            </div>
        </div>
        <!-- END Danh mục sản phẩm -->

        <script src="/assets/js/jquery.mCustomScrollbar.js"></script>
        <link href="/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 35px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                // Add active menu
                $("#glbDatHangNoiBo").addClass("active");
                $("#glbDatHangNoiBo_ChoKho").addClass("active");
                $("#subMenu li.li-export").addClass("active");


                // Mở box listing sản phẩm
                $(".show-product").bind("click", function () {
                    $(this).parent().parent().find(".listing-product").show();
                    $("select#ddlProductCategory").val($("select#ddlProductCategory option:first-child").val());
                    showProductByCategory($("select#ddlProductCategory"));
                });

                $(".show-item").bind("click", function () {
                    var item = $(this).attr("data-item");
                    var classItem = ".popup-" + item + "-item";

                    $(classItem).openEBPopup();
                    ExcCheckboxItem();
                    ExcQuantity();
                    ExcCompletePopup();

                    $('#EBPopup .listing-product').mCustomScrollbar({
                        theme: "dark-2",
                        scrollInertia: 100
                    });

                    $("#EBPopup .btn-esc").bind("click", function () { autoCloseEBPopup(0); });
                });

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { },
                    scrollMonth: false,
                    scrollTime: false,
                    scrollInput: false
                });

                // Btn Send
                $("#BtnSend").bind("click", function () {
                    TotalMoney();
                    getProductIds();
                    getServiceIds();
                    ExcQuantity();
                    // Validate : Mã khách hàng || Khách không cho thông tin
                    if ($("#Salon").val() == 0) {
                        showMsgSystem("Bạn chưa chọn salon.", "warning");
                        // Validate : Chọn mỹ phẩm
                    } else if ($("#HDF_ProductIds").val() == "[]") {
                        showMsgSystem("Bạn chưa chọn mỹ phẩm.", "warning");
                    } else {
                        $("#BtnFakeSend").click();
                    }
                });

                // Init execute service, product quantity
                TotalMoney();
                getProductIds();
                getServiceIds();
                ExcQuantity();

                // Auto select staff
                AutoSelectStaff();

                /// Bind gói phụ trợ
                pushFreeService();

                //============================
                // Show System Message
                //============================ 
                var qs = getQueryStrings();
                showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);
            });

            function bindAllGoods() {
                var day = $("#TxtDateTimeFrom").val();
                var salonId = $("#Salon").val();
                var Ids = [], prd = {};
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/GoodsManagerment/ExportProductToSalon.aspx/getAllGoods",
                    data: '{}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        if (response.d.length > 0) {
                            var trs = "";
                            $.each(response.d, function (i, v) {
                                trs += '<tr data-cate="' + v.CategoryId + '" style="">' +
                                    '<td class="td-product-index">' + (i + 1) + '</td>' +
                                    '<td class="td-product-name">' + v.Name + '</td>' +
                                    '<td class="td-product-code" data-id="' + v.Id + '" data-code="' + v.Code + '">' + v.Code + '</td>' +
                                    '<td class="td-product-price" data-price="' + v.Cost + '" style="display:none;">' + v.Cost + '</td>' +
                                    '<td class="td-product-quantity map-edit">' +
                                    '<input type="text" class="product-quantity" value="' + v.Quantity + '">' +
                                    '<div class="edit-wp">' +
                                    '<a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),\'' + v.Name + '\',\'product\')" href="javascript://" title="Xóa"></a>' +
                                    '</div>' +
                                    '</td>' +
                                    '<td class="td-product-voucher" data-voucher="0" style="display:none;">' +
                                    '<div class="row">' +
                                    '<input type="text" class="product-voucher" value="' + v.VoucherPercent + '" style="margin: 0 auto; float: none; text-align: center;width: 50px;"> %' +
                                    '</div>' +
                                    '<div class="row promotion-money">' +
                                    '<div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">' +
                                    '<label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">' +
                                    '<input type="checkbox" class="item-promotion" data-value="35000" data-id="1" style="margin-left: 0;"> - 35.000 VNĐ </label>' +
                                    '</div><br>' +
                                    '<div class="checkbox cus-no-infor" style="margin-left: 0; padding-left: 5px;">' +
                                    '<label class="lbl-cus-no-infor" style="font-family: Roboto Condensed Regular;">' +
                                    '<input type="checkbox" class="item-promotion" data-id="2" data-value="50000" style="margin-left: 0;"> - 50.000 VNĐ </label>' +
                                    '</div>' +
                                    '</div>' +
                                    '</td>' +
                                    '</tr>';
                                prd = {};
                                prd.Id = v.Id;
                                prd.Code = v.Code;
                                prd.Name = v.Name;
                                prd.Price = v.Price;
                                prd.Quantity = v.Quantity;
                                prd.VoucherPercent = v.VoucherPercent != null ? v.VoucherPercent : 0;
                                prd.Promotion = v.Promotion;
                                Ids.push(prd);
                            });
                            $("#HDF_ProductIds").val(JSON.stringify(Ids));
                            $("#table-item-product").empty().append($(trs));
                            $("#ListingProductWp").show();
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function showProductByCategory(This) {
                $("table#table-product tbody tr").each(function () {
                    if ($(this).attr("data-cate") != This.val()) {
                        $(this).hide();
                    } else {
                        $(this).show();
                    }
                });
                if ($(".popup-product-item").isOpenEBPopup()) {
                    $(".popup-product-item").alignment();
                }
            }

            // Xử lý mã khách hàng => bind khách hàng
            function LoadCustomer(code) {
                ajaxGetCustomer(code);
            }

            // Xử lý khi chọn checkbox sản phẩm
            function ExcCheckboxItem() {
                $("#EBPopup .td-product-checkbox input[type='checkbox']").unbind("click").bind("click", function () {
                    var obj = $(this).parent().parent(),
                        boxMoney = obj.find(".box-money"),
                        price = obj.find(".td-product-price").data("price"),
                        quantity = obj.find("input.product-quantity").val(),
                        voucher = obj.find("input.product-voucher").val(),
                        checked = $(this).is(":checked"),
                        item = obj.attr("data-item"),
                        promotion = 0;
                    obj.find(".promotion-money input[type='checkbox']:checked").each(function () {
                        var value = $(this).attr("data-value").trim();
                        promotion += (value != "" ? parseInt(value) : 0);
                    });

                    // check value
                    price = price.toString().trim() != "" ? parseInt(price) : 0;
                    quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
                    voucher = voucher.trim() != "" ? parseInt(voucher) : 0;


                    if (checked) {
                        boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100 - promotion)).show();
                    } else {
                        boxMoney.text("").hide();
                    }

                });
            }

            // Số lượng | Quantity
            function ExcQuantity() {
                $("input.product-quantity").bind("change", function () {
                    var obj = $(this).parent().parent(),
                        quantity = $(this).val(),
                        price = obj.find(".td-product-price").data("price"),
                        voucher = obj.find("input.product-voucher").val(),
                        boxMoney = obj.find(".box-money"),
                        promotion = 0;

                    obj.find(".promotion-money input[type='checkbox']:checked").each(function () {
                        var value = $(this).attr("data-value").trim();
                        promotion += (value != "" ? parseInt(value) : 0);
                    });

                    // check value
                    price = price.toString().trim() != "" ? parseInt(price) : 0;
                    quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
                    voucher = voucher.trim() != "" ? parseInt(voucher) : 0;

                    boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100 - promotion)).show();
                    TotalMoney();
                    getProductIds();
                    getServiceIds();
                });
                $("input.product-voucher").bind("change", function () {
                    var obj = $(this).parent().parent().parent(),
                        quantity = obj.find("input.product-quantity").val(),
                        price = obj.find(".td-product-price").data("price"),
                        voucher = obj.find("input.product-voucher").val(),
                        boxMoney = obj.find(".box-money"),
                        promotion = 0;

                    obj.find(".promotion-money input[type='checkbox']:checked").each(function () {
                        var value = $(this).attr("data-value").trim();
                        promotion += (value != "" ? parseInt(value) : 0);
                    });

                    // check value
                    price = price.toString().trim() != "" ? parseInt(price) : 0;
                    quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
                    voucher = voucher.trim() != "" ? parseInt(voucher) : 0;

                    if (promotion == 0) {
                        boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100)).show();
                    } else {
                        obj.find("input.product-voucher").val(0);
                    }

                    TotalMoney();
                    getProductIds();
                    getServiceIds();
                });
                $("input.item-promotion").bind("click", function () {
                    var obj = $(this).parent().parent().parent().parent().parent(),
                        quantity = obj.find("input.product-quantity").val(),
                        price = obj.find(".td-product-price").data("price"),
                        voucher = obj.find("input.product-voucher").val(),
                        boxMoney = obj.find(".box-money"),
                        promotion = $(this).attr("data-value"),
                        _This = $(this),
                        isChecked = $(this).is(":checked");
                    obj.find(".promotion-money input[type='checkbox']:checked").prop("checked", false);
                    if (isChecked) {
                        $(this).prop("checked", true);
                        promotion = promotion.trim() != "" ? parseInt(promotion) : 0;
                        obj.find("input.product-voucher").val(0);
                    } else {
                        promotion = 0;
                    }

                    // check value
                    price = price.toString().trim() != "" ? parseInt(price) : 0;
                    quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
                    voucher = voucher.trim() != "" ? parseInt(voucher) : 0;
                    //promotion = promotion.trim() != "" ? parseInt(promotion) : 0;

                    if (promotion > 0) {
                        boxMoney.text(FormatPrice(quantity * price - promotion)).show();
                        obj.find("input.product-voucher").css({ "text-decoration": "line-through" });
                    } else {
                        boxMoney.text(FormatPrice(quantity * price * (100 - voucher) / 100)).show();
                        obj.find("input.product-voucher").css({ "text-decoration": "none" });
                    }
                    TotalMoney();
                    getProductIds();
                    getServiceIds();
                });
            }

            // Xử lý click hoàn tất chọn item từ popup
            function ExcCompletePopup() {
                $("#EBPopup .btn-complete").unbind("click").bind("click", function () {
                    var item = $(this).attr("data-item"),
                        tableItem = $("table.table-item-" + item + " tbody"),
                        objTmp;

                    $("#EBPopup .item-product-checkbox input[type='checkbox']:checked").each(function () {
                        var Code = $(this).attr("data-code");
                        if (!ExcCheckItemIsChose(Code, tableItem)) {
                            objTmp = $(this).parent().parent().clone().find("td:first-child").remove().end();
                            tableItem.append(objTmp);
                        }
                    });

                    TotalMoney();
                    getProductIds();
                    getServiceIds();
                    ExcQuantity();
                    UpdateItemOrder(tableItem);
                    autoCloseEBPopup(0);
                });
            }

            // Check item đã được chọn
            function ExcCheckItemIsChose(Code, itemClass) {
                var result = false;
                $(itemClass).find(".td-product-code").each(function () {
                    var _Code = $(this).text().trim();
                    if (_Code == Code)
                        result = true;
                });
                return result;
            }

            // Remove item đã được chọn
            function RemoveItem(THIS, name, itemName) {
                // Confirm
                $(".confirm-yn").openEBPopup();

                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn hủy [ " + name + " ] ?");
                $("#EBPopup .yn-yes").bind("click", function () {
                    var Code = THIS.find(".td-product-code").attr("data-code");
                    $("#quick-" + itemName).find("input[data-code='" + Code + "']").removeAttr("checked");
                    THIS.remove();
                    TotalMoney();
                    getProductIds();
                    getServiceIds();
                    UpdateItemDisplay(itemName);
                    autoCloseEBPopup(0);
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
            }

            // Remove item (đã lưu trong phiếu pending) được chọn
            function RemoveItemPending(THIS, itemName, itemId, itemType) {
                // Confirm
                $(".confirm-yn").openEBPopup();

                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn hủy [ " + itemName + " ] ?");
                $("#EBPopup .yn-yes").bind("click", function () {
                    THIS.remove();
                    TotalMoney();
                    getProductIds();
                    getServiceIds();
                    UpdateItemDisplay(itemType);
                    autoCloseEBPopup(0);
                    $.ajax({
                        type: "POST",
                        url: "/GUI/SystemService/Ajax/Del.aspx/Delele_Item_Pending",
                        data: '{itemId : ' + itemId + ', BillId : ' + $("#HDF_BillId").val() + ', itemType : "' + itemType + '" }',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            var mission = JSON.parse(response.d);
                            if (mission.success) {
                                delSuccess();
                                Row.remove();
                            } else {
                                delFailed();
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    });
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
            }

            // Update table display
            function UpdateItemDisplay(itemName) {
                var dom = $(".table-item-" + itemName);
                var len = dom.find("tbody tr").length;
                if (len == 0) {
                    dom.parent().hide();
                } else {
                    UpdateItemOrder(dom.find("tbody"));
                }
            }

            // Update order item
            function UpdateItemOrder(dom) {
                var index = 1;
                dom.find("tr").each(function () {
                    $(this).find("td.td-product-index").text(index);
                    index++;
                });
            }

            function TotalMoney() {
                var Money = 0;
                $(".fe-service-table-add .box-money:visible").each(function () {
                    Money += parseInt($(this).text().replace(/\./gm, ""));
                });
                $("#TotalMoney").val(FormatPrice(Money));
                $("#HDF_TotalMoney").val(Money);
            }

            function showMsgSystem(msg, status) {
                $("#MsgSystem").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);
                setTimeout(function () {
                    $("#MsgSystem").fadeTo("slow", 0, function () {
                        $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 1);
                    });
                }, 5000);
            }

            function getProductIds() {
                var Ids = [];
                var prd = {};
                $("table.table-item-product tbody tr").each(function () {
                    prd = {};
                    var THIS = $(this);

                    var Id = THIS.find("td.td-product-code").attr("data-id"),
                        Code = THIS.find("td.td-product-code").text().trim(),
                        Name = THIS.find("td.td-product-name").text().trim(),
                        Price = THIS.find(".td-product-price").attr("data-price"),
                        Quantity = THIS.find("input.product-quantity").val(),
                        VoucherPercent = THIS.find("input.product-voucher").val(),
                        Promotion = 0;

                    THIS.find(".promotion-money input[type='checkbox']:checked").each(function () {
                        var value = $(this).attr("data-value").trim();
                        Promotion += (value != "" ? parseInt(value) : 0);
                    });

                    // check value
                    Id = Id.toString().trim() != "" ? parseInt(Id) : 0;
                    Price = Price.toString().trim() != "" ? parseInt(Price) : 0;
                    Quantity = Quantity.trim() != "" ? parseInt(Quantity) : 0;
                    VoucherPercent = !isNaN(parseInt(VoucherPercent)) ? parseInt(VoucherPercent) : 0;

                    prd.Id = Id;
                    prd.Code = Code;
                    prd.Name = Name;
                    prd.Price = Price;
                    prd.Quantity = Quantity;
                    prd.VoucherPercent = VoucherPercent;
                    prd.Promotion = Promotion;

                    Ids.push(prd);

                });
                Ids = JSON.stringify(Ids);
                $("#HDF_ProductIds").val(Ids);
            }

            function getServiceIds() {
                var Ids = [];
                var prd = {};
                $("table.table-item-service tbody tr").each(function () {
                    prd = {};
                    var THIS = $(this),
                        obj = THIS.parent().parent();

                    var Id = THIS.find("td.td-product-code").attr("data-id"),
                        Code = THIS.find("td.td-product-code").text().trim(),
                        Name = THIS.find("td.td-product-name").text().trim(),
                        Price = THIS.find(".td-product-price").attr("data-price"),
                        Quantity = THIS.find("input.product-quantity").val(),
                        VoucherPercent = THIS.find("input.product-voucher").val();

                    // check value
                    Id = Id.toString().trim() != "" ? parseInt(Id) : 0;
                    Price = Price.toString().trim() != "" ? parseInt(Price) : 0;
                    Quantity = Quantity.trim() != "" ? parseInt(Quantity) : 0;
                    VoucherPercent = !isNaN(parseInt(VoucherPercent)) ? parseInt(VoucherPercent) : 0;

                    prd.Id = Id;
                    prd.Code = Code;
                    prd.Name = Name;
                    prd.Price = Price;
                    prd.Quantity = Quantity;
                    prd.VoucherPercent = VoucherPercent;

                    Ids.push(prd);

                });
                Ids = JSON.stringify(Ids);
                $("#HDF_ServiceIds").val(Ids);
            }

            // Get Customer
            function ajaxGetCustomer(CustomerCode) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/FrontEnd/Service/Service_Add.aspx/GetCustomer",
                    data: '{CustomerCode : "' + CustomerCode + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            var customer = JSON.parse(mission.msg);
                            customer = customer[0];
                            $("#CustomerName").val(customer.Fullname);
                            $("#CustomerName").attr("data-code", customer.Customer_Code);
                            $("#HDF_CustomerCode").val(customer.Customer_Code);

                        } else {
                            var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                            showMsgSystem(msg, "warning");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function pushQuickData(This, typeData) {
                var Code = This.attr("data-code");
                if (This.is(":checked")) {
                    var Dom = $("#table-" + typeData).find("input[data-code='" + Code + "']").parent().parent().clone().find("td:first-child").remove().end(),
                        quantity = Dom.find(".product-quantity").val(),
                        price = Dom.find(".td-product-price").data("price"),
                        voucher = Dom.find("input.product-voucher").val();

                    // check value            
                    price = price.toString().trim() != "" ? parseInt(price) : 0;
                    quantity = quantity.trim() != "" ? parseInt(quantity) : 0;
                    voucher = voucher.trim() != "" ? parseInt(voucher) : 0;

                    Dom.find(".box-money").text(FormatPrice(quantity * price * (100 - voucher) / 100)).show();
                    $("#table-item-" + typeData).append(Dom);
                    $(".item-" + typeData).show();
                } else {
                    $("#table-item-" + typeData).find(".td-product-code[data-code='" + Code + "']").parent().remove();
                }

                TotalMoney();
                getProductIds();
                getServiceIds();
                ExcQuantity();
                UpdateItemOrder($("#table-item-" + typeData).find("tbody"));
            }

            function pushFreeService(This, id) {
                var arr = [];
                var sv = {};
                $(".free-service input[type='checkbox']").each(function () {
                    if ($(this).is(":checked")) {
                        arr.push(parseInt($(this).attr('data-id')));
                    }
                });
                $("#HDF_FreeService").val(JSON.stringify(arr));
            }

            //============================
            // Suggestion Functions
            //============================
            function Bind_Suggestion() {
                $(".eb-suggestion").bind("keyup", function (e) {
                    if (e.keyCode == 40) {
                        UpDownListSuggest($(this));
                    } else {
                        Call_Suggestion($(this));
                    }
                });
                $(".eb-suggestion").bind("focus", function () {
                    Call_Suggestion($(this));
                });
                $(".eb-suggestion").bind("blur", function () {
                    //Exc_To_Reset_Suggestion($(this));
                });
                $(window).bind("click", function (e) {
                    if ((!e.target.className.match("eb-select") && e.target.parentElement.className.match("ul-listing-suggestion")) ||
                        (!e.target.className.match("eb-select") && !e.target.parentElement.className.match("ul-listing-suggestion"))) {
                        EBSelect_HideBox();
                    }
                });
            }

            function UpDownListSuggest(This) {
                var UlSgt = This.parent().find(".ul-listing-suggestion"),
                    index = 0,
                    LisLen = UlSgt.find(">li").length - 1,
                    Value;

                This.blur();
                UlSgt.find(">li.active").removeClass("active");
                UlSgt.find(">li:eq(" + index + ")").addClass("active");

                $(window).unbind("keydown").bind("keydown", function (e) {
                    if (e.keyCode == 40) {
                        if (index == LisLen) return false;
                        UlSgt.find(">li.active").removeClass("active");
                        UlSgt.find(">li:eq(" + (++index) + ")").addClass("active");
                        return false;
                    } else if (e.keyCode == 38) {
                        if (index == 0) return false;
                        UlSgt.find(">li.active").removeClass("active");
                        UlSgt.find(">li:eq(" + (--index) + ")").addClass("active");
                        return false;
                    } else if (e.keyCode == 13) {
                        // Bind data to HDF Field
                        var THIS = UlSgt.find(">li.active");
                        //var Value = THIS.text().trim();
                        var Value = THIS.attr("data-code");
                        var dataField = This.attr("data-field");

                        BindIdToHDF(THIS, Value, dataField, "#HDF_Suggestion_Code", "#HDF_Suggestion_Field", This);
                        EBSelect_HideBox();
                    }
                });
            }

            function Exc_To_Reset_Suggestion(This) {
                var value = This.val();
                if (value == "") {
                    $(".eb-suggestion").each(function () {
                        var THIS = $(this);
                        var sgValue = THIS.val();
                        if (sgValue != "") {
                            BindIdToHDF(THIS, sgValue, THIS.attr("data-field"), "#HDF_Suggestion_Code", "#HDF_Suggestion_Field", THIS);
                            return false;
                        }
                    });
                }
            }

            function Call_Suggestion(This) {
                var text = This.val(),
                    field = This.attr("data-field");
                Suggestion(This, text, field);
            }

            function Suggestion(This, text, field) {
                var This = This;
                var text = text || "";
                var field = field || "";
                var InputDomId;
                var HDF_Sgt_Code = "#HDF_Suggestion_Code";
                var HDF_Sgt_Field = "#HDF_Suggestion_Field";

                if (text == "") return false;

                switch (field) {
                    case "customer.name": InputDomId = "#CustomerName"; break;
                    case "customer.phone": InputDomId = "#CustomerPhone"; break;
                    case "customer.code": InputDomId = "#CustomerCode"; break;
                    case "bill.code": InputDomId = "#BillCode"; break;
                }

                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Ajax/Suggestion.aspx/Suggestion_Staff",
                    data: '{field : "' + field + '", text : "' + text + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            var OBJ = JSON.parse(mission.msg);
                            if (OBJ.length > 0) {
                                var lis = "";
                                $.each(OBJ, function (i, v) {
                                    lis += "<li data-code='" + v.Customer_Code + "'" +
                                        "onclick=\"BindIdToHDF($(this),'" + v.Customer_Code + "','" + field + "','" + HDF_Sgt_Code +
                                        "','" + HDF_Sgt_Field + "','" + InputDomId + "')\">" +
                                        v.Value +
                                        "</li>";
                                });
                                This.parent().find("ul.ul-listing-suggestion").empty().append(lis).parent().show();
                                This.parent().find(".eb-select-data").show();
                            } else {
                                This.parent().find(".eb-select-data").hide().find("ul.ul-listing-suggestion").empty();
                            }
                        } else {
                            This.parent().find("ul.ul-listing-suggestion").empty();
                            var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                            showMsgSystem(msg, "warning");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function BindIdToHDF(THIS, Code, Field, HDF_Sgt_Code, HDF_Sgt_Field, Input_DomId) {
                var text = THIS.text().trim();
                $("input.eb-suggestion").val("");
                $(HDF_Sgt_Code).val(Code);
                $(HDF_Sgt_Field).val(Field);
                $(Input_DomId).val(text);
                $(Input_DomId).parent().find(".eb-select-data").hide();

                // Auto post server
                $("#BtnFakeUP").click();
            }

            function EBSelect_HideBox() {
                $(".eb-select-data").hide();
                $("ul.ul-listing-suggestion li.active").removeClass("active");
            }

            //===================
            // Auto select staff
            //===================
            function AutoSelectStaff() {
                $(".auto-select").bind("keyup", function (e) {
                    var This = $(this);
                    var StaffType = This.attr("data-field");
                    var code = parseInt(This.val());
                    if (!isNaN(code)) {
                        $.ajax({
                            type: "POST",
                            url: "/GUI/SystemService/Ajax/Suggestion.aspx/Suggestion_OneStaff",
                            data: '{code : "' + code + '", StaffType : "' + StaffType + '"}',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json", success: function (response) {
                                var mission = JSON.parse(response.d);
                                if (mission.success) {
                                    var OBJ = JSON.parse(mission.msg);
                                    if (OBJ.length > 0) {
                                        var lis = "";
                                        This.parent().find(".fake-value").text(This.val() + "- " + OBJ[0].Fullname);
                                        $("#" + StaffType).val(OBJ[0].Id);
                                    } else {
                                        This.parent().find(".fake-value").text("");
                                        $("#" + StaffType).val("");
                                    }
                                } else {
                                    This.parent().find("ul.ul-listing-suggestion").empty();
                                    var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                                    showMsgSystem(msg, "warning");
                                }
                            },
                            failure: function (response) { alert(response.d); }
                        });
                    } else {
                        This.parent().find(".fake-value").text("");
                        $("#" + StaffType).val("");
                    }
                });
            }

            //===================
            // Function webservice to rating
            //===================
            function requestRating() {

            }
            function getRating() {
                var salonId = $("#HDF_SalonId").val();
                var lgId = $("#HDF_lgId").val();
                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Webservice/wsRating.asmx/getRating",
                    data: "{salonId : " + salonId + ", lgId : " + lgId + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = response.d;
                        if (mission.success) {
                            if (mission.msg == "1") {
                                $("#HDF_Rating").val(1);
                                $(".rating-icon").removeClass("active");
                                $(".rating-icon.icon-sad").addClass("active");
                            } else if (mission.msg == "2") {
                                $("#HDF_Rating").val(2);
                                $(".rating-icon").removeClass("active");
                                $(".rating-icon.icon-normal").addClass("active");
                            } else if (mission.msg == "3") {
                                $("#HDF_Rating").val(3);
                                $(".rating-icon").removeClass("active");
                                $(".rating-icon.icon-happy").addClass("active");
                            }
                            //else if(mission.msg == "0"){
                            //    $("#HDF_Rating").val(0);
                            //    $(".rating-icon").removeClass("active");
                            //}
                        } else {
                            console.log("Get rating error.");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function excRating(This, value) {
                return; // Hủy chức năng này
                $("#HDF_Rating").val(value);
                This.parent().find(".rating-icon").removeClass("active");
                This.addClass("active");
            }

            function bindRating(mark) {
                switch (mark) {
                    case 1: $(".rating-icon").removeClass("active");
                        $(".rating-icon.icon-sad").addClass("active");
                        break;
                    case 2: $(".rating-icon").removeClass("active");
                        $(".rating-icon.icon-normal").addClass("active");
                        break;
                    case 3: $(".rating-icon").removeClass("active");
                        $(".rating-icon.icon-happy").addClass("active");
                        break;
                    default: break;
                }
            }

        </script>
    </asp:Panel>
</asp:Content>


