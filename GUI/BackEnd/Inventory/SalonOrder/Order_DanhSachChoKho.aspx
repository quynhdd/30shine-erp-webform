﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Order_DanhSachChoKho.aspx.cs" Inherits="Project.GUI.BackEnd.Inventory.SalonOrder.Order_DanhSachChoKho" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<%--<%@ Register Src="~/GUI/BackEnd/Report/ReportMenu.ascx" TagName="ReportMenu" TagPrefix="uc1" %>--%>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .table-listing .uv-avatar {
                width: 120px;
            }

            .table-row-detail {
                display: inline-block;
                width: 47% !important;
                min-width: 500px;
                float: none !important;
            }

            .product-QuantityExport {
                width: 50px !important;
                height: 22px !important;
                line-height: 22px !important;
                text-align: center;
                border-bottom: 1px solid #ddd !important;
                border-top: none !important;
                border-right: none !important;
                border-left: none !important;
                background: none;
            }
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý đặt hàng nội bộ &nbsp;&#187; Salon đặt hàng kho</li>
                        <li class="li-listing"><a href="/dat-hang-kho/kho/danh-sach">Danh sách</a></li>
                        <li class="li-listing"><a href="/dat-hang-kho/danh-sach-tra-thieu">DS trả thiếu</a></li>
                        <li class="li-listing"><a href="/dat-hang-kho/kho/bao-cao-xuat-vat-tu.html">Báo cáo xuất VT</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report be-report-timekeeping">
            <%-- Listing --%>
            <label id="lblSalonId" hidden="hidden"><%=salonId%></label>
            <div class="wp960 content-wp">
                <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
                <!-- System Message -->
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <!-- END System Message -->
                <div class="row">
                    <div class="filter-item">
                        <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="time-wp">
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Từ ngày" Style="margin-left: 10px!important"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ValidateTime" ControlToValidate="TxtDateTimeFrom" runat="server" CssClass="fb-cover-error" Text="Vui lòng chọn thời gian!"></asp:RequiredFieldValidator>
                        </div>
                        <strong class="st-head" style="margin-left: 10px;">
                            <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                        </strong>
                        <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeTo" placeholder="Đến ngày" Style="margin-left: 10px!important"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                        <br />
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlSalon" CssClass="select form-control" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px;"></asp:DropDownList>
                    </div>
                    <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>

                </div>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>Danh sách đơn đặt hàng</strong>
                </div>
                <!-- Row Table Filter -->
                <div class="table-func-panel" style="margin-top: -33px;">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="row table-wp">
                            <table class="table-add table-listing" id="tblDonHangKho">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th class="tdOrderID">ID</th>
                                        <th>Salon</th>
                                        <th>Thời gian lên đơn</th>
                                        <th>Thời gian nhận hàng</th>
                                        <th class="tdOrderID">Mã đơn hàng</th>
                                        <th>Trạng thái</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptDanhsach" runat="server">
                                        <ItemTemplate>
                                            <tr onclick="addTrToTable($(this));" class="parent" data-status="<%# Eval("StatusId") %>">
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td class="row-id tdOrderID"><%# Eval("Id") %></td>
                                                <td><%#Eval ("SalonName") %></td>
                                                <td><%# Eval("CreatedTime") != null ? String.Format("{0:dd/MM/yyyy}", Eval("CreatedTime")) + " - " + String.Format("{0:HH:mm}", Eval("CreatedTime")) : "" %></td>
                                                <td><%# Eval("ReceivedTime") != null ? (String.Format("{0:dd/MM/yyyy}", Eval("ReceivedTime")) + " - " + String.Format("{0:HH:mm}", Eval("ReceivedTime"))) : "" %></td>
                                                <td class="tdOrderID"><%# Eval("BillCode") %></td>
                                                <td class="map-edit">
                                                    <%# Eval("StatusTitle") %>
                                                    <div class="edit-wp">
                                                        <a href="javascript:void(0);" class="elm edit-btn" onclick="addTrToTable($(this), <%# Eval("Id") %> )" title="Sửa" data-status="<%# Eval("StatusId") %>"></a>
                                                        <a class="elm del-btn del_Bill" onclick="del($(this),'<%# Eval("Id") %>', '<%# Eval("BillCode") %>')" href="javascript://" title="Xóa"></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />

                <!-- Hidden Field-->
                <asp:HiddenField ID="HDF_TypeStaff" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_Staff" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_ExcelType" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_ExportType" ClientIDMode="Static" runat="server" />
                <!-- END Hidden Field-->
            </div>
            <%-- END Listing --%>
        </div>



        <link href="/Assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <script src="/Assets/js/jquery.mCustomScrollbar.js"></script>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/ecmascript">
            jQuery(document).ready(function () {
                $(".del_Bill").click(function (e, This) {
                    var code = code || null,
                        name = name || null,
                        Row = This;
                    if (!code) return false;

                    // show EBPopup
                    $(".confirm-yn").openEBPopup();
                    $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

                    $("#EBPopup .yn-yes").bind("click", function () {
                        $.ajax({
                            type: "POST",
                            url: "/GUI/SystemService/Ajax/Del.aspx/Delele_Bill_QLKho",
                            data: '{Id : ' + code + '}',
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                var mission = JSON.parse(response.d);
                                if (mission.success) {
                                    delSuccess();
                                    Row.remove();
                                    setTimeout(function () {
                                        $("#ViewData").click();
                                    }, 1000);
                                } else {
                                    delFailed();
                                }
                            },
                            failure: function (response) { alert(response.d); }
                        });
                    });
                    $("#EBPopup .yn-no").bind("click", function () {
                        autoCloseEBPopup(0);
                    });
                    e.stopPropagation();
                })
                // Add active menu
                $("#glbDatHangNoiBo").addClass("active");
                $("#glbDatHangNoiBo_ChoKho").addClass("active");
                $("#subMenu li.li-listing").addClass("active");

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });

                //============================
                // Bind Staff
                //============================
                BindStaffFilter();

                // View data yesterday
                $(".tag-date-today").click();

                var qs = getQueryStrings();
                showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);

                for (var j = 0; j < listElement.length; j++) {
                    var nameElement = listElement[j].ElementName;
                    var enabled = listElement[j].Enable;
                    var type = listElement[j].Type;
                    if (type == "hidden" && enabled == true) {
                        $("." + nameElement).addClass('hidden');
                    }
                    else {
                        $("." + nameElement).prop('disabled', enabled);
                    }
                }
            });

            function showMsgSystem(msg, status) {
                $("#MsgSystem").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);
                setTimeout(function () {
                    $("#MsgSystem").fadeTo("slow", 0, function () {
                        $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 1);
                    });
                }, 5000);
            }

            function showTrPrice() {
                $(".tr-price").show();
            }

            function BindStaffFilter() {
                $(".eb-select").bind("focus", function () {
                    EBSelect_ShowBox($(this).parent().find(".eb-select-data"));
                });
                $(window).bind("click", function (e) {
                    console.log(e);
                    if ((!e.target.className.match("eb-select") && e.target.parentElement.className.match("ul-listing-staff")) ||
                        (!e.target.className.match("eb-select") && !e.target.parentElement.className.match("ul-listing-staff"))
                        && !e.target.className.match("mCSB_dragger_bar")) {
                        EBSelect_HideBox();
                    }
                });
                //============================
                // Scroll Staff Filter
                //============================
                $('.eb-select-data').each(function () {
                    if ($(this).height() > 230) {
                        $(this).mCustomScrollbar({
                            theme: "dark-2",
                            scrollInertia: 100
                        });
                    }
                });
            }

            function BindIdToHDF(THIS, id, HDF_DomId, Input_DomId) {
                var text = THIS.innerText.trim(),
                    HDF_Dom = document.getElementById(HDF_DomId),
                    InputText = document.getElementById(Input_DomId);
                HDF_Dom.value = id;
                InputText.value = text;
                InputText.setAttribute("data-value", id);
                ajaxGetStaffsByType(id);
                if (HDF_DomId == "HDF_TypeStaff") {
                    $("#StaffName").val("");
                    $("#HDF_Staff").val("");
                }
            }

            function ajaxGetStaffsByType(type) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Report/TimeKeeping.aspx/Load_Staff_ByType",
                    data: '{type : "' + type + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            var staffs = JSON.parse(mission.msg);
                            if (staffs.length > 0) {
                                var tmp = "";
                                $.each(staffs, function (i, v) {
                                    tmp += '<li data-code="' + v.Code + '" data-id="' + v.Id + '"' +
                                        'onclick="BindIdToHDF(this,' + v.Id + ',\'HDF_Staff\',\'StaffName\')">' +
                                        v.Fullname + '</li>';
                                });
                                $("#UlListStaff").empty().append(tmp);
                            }

                        } else {
                            var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                            showMsgSystem(msg, "warning");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function EBSelect_ShowBox(Dom) {
                EBSelect_HideBox();
                Dom.show();
            }

            function EBSelect_HideBox() {
                $(".eb-select-data").hide();
            }

            var orderIdCurrent, currentOrderDate;

            function addTrToTable(This, orderId) {
                addLoading();
                var rowIndex = $('#tblDonHangKho tbody tr.parent').index(This);
                var orderId = $(This).find("td:eq(1)").text();
                currentOrderDate = ConvertDateToTypeMMddyyyy($(This).find("td:eq(3)").text().substring(0, 10));
                var newRow = $('<tr class="expand"> <td colspan="7">'
                    + '  <table class="table-row-detail">                       '
                    + '      <tbody>                                                                                                  '
                    + '         <tr><td colspan="7" style="border:none; text-align: left">Thông tin sản phẩm</td></tr>                '
                    + '          <tr class="tr-field-ahalf tr-product">                                                                '
                    + '              <td style="border: none;" colspan="7">                                    '
                    + '                  <div class="listing-product item-product" id="ListingProductWp">                              '
                    + '                      <table class="table table-listing-product table-item-product" id="table-item-product">    '
                    + '                          <thead>                                                                               '
                    + '                              <tr>                                                                              '
                    + '                                  <th>STT</th>                                                                 '
                    + '                                  <th class="tdOrderID">ID</th>                                                                  '
                    + '                                  <th style="display:none;">ProductId</th>                                     '
                    + '                                  <th>Tên sản phẩm</th>                                                        '
                    + '                                  <th class="td-product-cost">Đơn giá</th>                                          '
                    + '                                  <th class="hidden">Price</th>                                         '
                    + '                                  <th style="width: 100px;">Số lượng hàng đặt</th>                              '
                    + '                                  <th style="width: 100px;">Số lượng xuất thực tế</th>                          '
                    + '                                  <th style="width: 100px;" class="td-product-TotalPrice">Thành tiền</th>                          '
                    + '                              </tr>                                                                             '
                    + '                          </thead>                                                                              '
                    + '                          <tbody>                                                                               '
                    + '                                                                                                                '
                    + '                          </tbody>                                                                              '
                    + '                      </table>                                                                                  '
                    + '                  </div>                                                                                        '
                    + '              </td>                                                                                             '
                    + '         </tr>                                                                                                  '
                    + '    <tr class="tr-totalPrice">                                                                                                        '
                    + '        <td style="border: none; text-align:left"></td>                                                         '
                    + '        <td style="border: none;text-align:right"><p style="font-size: 16pt!important">Tổng tiền: <label style="font-size: 16pt!important;" id="txtTongOrder"></label></p> </td>                   '
                    + '    </tr>                                                                                                       '
                    + '    <tr class="tr-totalQuantityOrder">                                                                                                        '
                    + '        <td style="border: none; text-align:left"></td>                                                         '
                    + '        <td style="border: none;text-align:right"><p style="font-size: 16pt!important">Tổng số lượng order: <label style="font-size: 16pt!important;" id="txtTotalQuantityOrder"></label></p> </td>                   '
                    + '    </tr>                                                                                                       '
                    + '    <tr>                                                                                                        '
                    + '        <td style="border: none; text-align:left">Ghi chú của kho</td>                                          '
                    + '        <td style="border: none;text-align:left">Ghi chú của salon</td>                                         '
                    + '    </tr>                                                                                                       '
                    + '    <tr>                                                                                                        '
                    + '        <td style="border: none;">                                                                              '
                    + '               <textarea id="txtNoteKho" class="form-control NoteKho" rows="4" style="margin-right:10px;"></textarea>  '
                    + '        </td>                                                                                                   '
                    + '        <td style="border: none;">                                                                              '
                    + '               <textarea id="txtNoteSalon" class="form-control NoteSalon" rows="4" style="margin-right:10px;"></textarea>  '
                    + '        </td>                                                                                                   '
                    + '    </tr>                                                                                                       '
                    + '          <tr class="tr-send btnXuatHang">                                                                      '
                    + '              <td style="border:none;"></td>                                                                    '
                    + '              <td class="col-xs-6 right no-border"  style="border: none;">                                      '
                    + '                  <span class="field-wp">                                                                       '
                    + '                   <input class="btn btn-sm btn-info btnXuatHang"  type="button" value="Xuất hàng" onclick="SaveOrderById();" /> '
                    + '                 </span>                                                                                        '
                    + '              </td>                                                                                             '
                    + '          </tr>                                                                                                 '
                    + '         <tr><td colspan="7"  class="ProductsOwe" style="border:none; text-align: left">Sản phẩm trả thiếu</td></tr>               '
                    + '          <tr class="tr-field-ahalf tr-product">                                                                '
                    + '              <td class="col-xs-6 right" style="border: none;" colspan="7">                                     '
                    + '                  <div class="listing-product item-product" id="ListingProductWp">                              '
                    + '                      <table class="table table-listing-product table-item-product ProductsOwe" id="table-item-product-Quantityowe">    '
                    + '                          <thead>                                                                               '
                    + '                              <tr>                                                                              '
                    + '                                  <th>STT</th>                                                                  '
                    + '                                  <th class="hidden">ID</th>                                                                   '
                    + '                                  <th class="hidden">Order Id</th>                                                                   '
                    + '                                  <th>Ngày order</th>                                                                   '
                    + '                                  <th style="display:none;">ProductId</th>                                      '
                    + '                                  <th>Tên sản phẩm</th>                                                         '
                    + '                                  <th style="display:none;">Cost</th>                                           '
                    + '                                  <th style="display:none;">Price</th>                                          '
                    + '                                  <th style="width: 100px;">Số lượng hàng đặt</th>                              '
                    + '                                  <th style="width: 100px;">Số lượng kho xuất trả</th>                          '
                    + '                                  <th style="width: 100px;">Số lượng trả thiếu</th>                             '
                    + '                              </tr>                                                                             '
                    + '                          </thead>                                                                              '
                    + '                          <tbody>                 </tbody>                                                      '
                    + '                      </table>                                                                                  '
                    + '                  </div>                                                                                        '
                    + '              </td>                                                                                             '
                    + '         </tr>                                                                                                  '
                    + '      </tbody>                                                                                                   '
                    + '  </table>                                                                                                       '
                    + '    </td></tr>');

                var nextIndex = rowIndex + 1;
                if ($('#tblDonHangKho tbody tr:nth(' + nextIndex + ')').hasClass('expand')) {
                    $('#tblDonHangKho tbody').find('.expand').remove();
                    $('#tblDonHangKho tbody tr').removeClass('test');
                }
                else {
                    for (var i = 0; i < $('#tblDonHangKho tbody tr').length; i++) {
                        $('#tblDonHangKho tbody tr').removeClass('test');
                        $('#tblDonHangKho tbody').find('.expand').remove();
                    }
                    newRow.insertAfter($('#tblDonHangKho tbody tr:nth(' + rowIndex + ')'));
                    $(This).addClass('test');
                }
                LoadDetailOrderById(orderId);
                LoadProductsOwe(This);
            }

            function LoadDetailOrderById(orderId) {
                var Ids = [], prd = {};
                orderIdCurrent = "";
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Inventory/SalonOrder/Order_DanhSachChoKho.aspx/getAllGoodsById",
                    data: '{orderId : ' + orderId + ' }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.d.length > 0) {
                            var trs = "";
                            var tongOrder = 0;
                            var totalQuantityOrder = 0;
                            $.each(response.d, function (i, v) {
                                var quantityExport = v.QuantityExport != null ? parseInt(v.QuantityExport) : "";
                                var thanhTien = 0;
                                var cost = v.Cost;
                                if (v.Cost != null) {
                                    cost = v.Cost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
                                }
                                else {
                                    cost = 0;
                                }
                                if (quantityExport == "") {
                                    thanhTien = 0;
                                }
                                else {
                                    thanhTien = parseInt(cost) * parseInt(quantityExport);
                                }
                                trs += '<tr>' +
                                    '<td class="td-product-STT">' + (i + 1) + '</td>' +
                                    '<td class="td-product-Id tdOrderID">' + v.Id + '</td>' +
                                    '<td class="td-product-ProductId" style="display:none;">' + v.ProductId + '</td>' +
                                    '<td class="td-product-name">' + v.Name + '</td>' +
                                    '<td class="td-product-cost" data-cost="' + v.Cost + '">' + cost + '</td>' +
                                    '<td class="td-product-price hidden">' + v.Price + '</td>' +
                                    '<td class="td-product-QuantityOrder" style="width: 100px;">' + v.QuantityOrder + '</td>' +
                                    '<td class="td-product-QuantityExport" style="width: 100px;"><input type="text" class="product-QuantityExport txtQuantityExport" value="' + quantityExport + '" onkeypress="return isNumber(event)" onchange="changequantity($(this))" /></td>' +
                                    '<td class="td-product-TotalPrice" style="width: 100px;"> ' + thanhTien.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.") + '</td>' +
                                    //'<td class="td-product-TotalPrice" style="width: 100px;"><input class="txtTest" /> </td>' +
                                    '</tr>';
                                totalQuantityOrder += v.QuantityOrder;
                                tongOrder += thanhTien;
                                prd = {};
                                prd.Id = v.Id;
                                prd.OrderId = v.OrderId;
                                prd.ProductId = v.ProductId;
                                prd.Cost = v.Cost;
                                prd.Price = v.Price;
                                prd.IsDelete = v.IsDelete;
                                prd.QuantityReceived = v.QuantityReceived;
                                prd.QuantityOwe = v.QuantityOwe;
                                prd.Name = v.Name;
                                prd.QuantityOrder = v.QuantityOrder;
                                prd.QuantityExport = v.QuantityExport;
                                prd.CreatedTime = v.CreatedTime;
                                prd.NoteKho = v.NoteKho;
                                prd.NoteSalon = v.NoteSalon;
                                Ids.push(prd);
                            });
                        }
                        var total = tongOrder.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
                        $('#txtTongOrder').text(total);
                        $('#txtTotalQuantityOrder').text(totalQuantityOrder);
                        $('#table-item-product tbody').append(trs);
                        orderIdCurrent = Ids[0].OrderId;
                        $('#txtNoteKho').val(Ids[0].NoteKho);
                        $('#txtNoteSalon').val(Ids[0].NoteSalon);
                        //CheckElementEnable();
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }

            function SaveOrderById() {
                var IdList = [];
                var prd = {};
                $("#table-item-product tbody tr").each(function () {
                    prd = {};
                    var THIS = $(this);

                    var Id = THIS.find("td.td-product-Id").text().trim(),
                        ProductId = THIS.find("td.td-product-ProductId").text().trim(),
                        Cost = THIS.find("td.td-product-cost").attr("data-cost").trim(),
                        Name = THIS.find("td.td-product-name").text().trim(),
                        Price = THIS.find("td.td-product-price").text().trim(),
                        QuantityOrder = THIS.find("td.td-product-QuantityOrder").text().trim(),
                        QuantityExport = THIS.find("input.product-QuantityExport").val(),
                        NoteKho = $('#txtNoteKho').val(),
                        NoteSalon = $('#txtNoteSalon').val();

                    // check value
                    ProductId = ProductId.toString().trim() != "" ? parseInt(ProductId) : 0;
                    if (Cost == "null") {
                        Cost = 0;
                    }
                    //QuantityExport = QuantityExport.toString().trim() != "" ? parseInt(QuantityExport) : 0;

                    prd.Id = ProductId;
                    prd.Cost = Cost;
                    prd.Name = Name;
                    prd.Price = Price;
                    prd.QuantityOrder = QuantityOrder;
                    prd.Quantity = QuantityExport;
                    IdList.push(prd);

                });
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Inventory/SalonOrder/Order_DanhSachChoKho.aspx/UpdateOrderById",
                    data: "{productIds : '" + JSON.stringify(IdList) + "' , orderId: '" + orderIdCurrent + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        Update_QLKhoSaLonOrder();
                        showMsgSystem('Xuất hàng thành công!', 'success');
                        setTimeout(function () {
                            $("#ViewData").click();
                        }, 1000);
                    },
                    error: function (error) {
                        showMsgSystem('Xuất hàng thất bại!', 'warning');
                        console.log(error);
                    }
                });
            }

            function Update_QLKhoSaLonOrder() {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Inventory/SalonOrder/Order_DanhSachChoKho.aspx/UpdateQLKho_SalonOrder",
                    data: "{orderId: '" + orderIdCurrent + "', noteKho: '" + $('#txtNoteKho').val() + "', noteSalon: '" + $('#txtNoteSalon').val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }

            function LoadProductsOwe(This) {
                console.log(currentOrderDate);
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Inventory/SalonOrder/Order_DanhSachChoKho.aspx/ProductsOweList",
                    data: '{salonId : "' + $('#lblSalonId').text() + '", currentOrderDate : "' + currentOrderDate + '" }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.d.length > 0) {
                            var trs = "";
                            $.each(response.d, function (i, v) {
                                trs += '<tr>' +
                                    '<td class="td-product-STT">' + (i + 1) + '</td>' +
                                    '<td class="td-product-Id hidden">' + v.Id + '</td>' +
                                    '<td class="td-product-OrderID hidden">' + v.OrderId + '</td>' +
                                    '<td class="td-product-Date">' + ConvertJsonDateToStringFormat(v.CreatedTime) + '</td>' +
                                    '<td class="td-product-ProductId" style="display:none;">' + v.ProductId + '</td>' +
                                    '<td class="td-product-name">' + v.ProductName + '</td>' +
                                    '<td class="td-product-cost" style="display:none;">' + v.Cost + '</td>' +
                                    '<td class="td-product-price" style="display:none;">' + v.Price + '</td>' +
                                    '<td class="td-product-QuantityOrder" style="width: 100px;">' + v.QuantityOrder + '</td>' +
                                    '<td class="td-product-QuantityExport" style="width: 100px;">' + v.QuantityExport + '</td>' +
                                    '<td class="td-product-QuantityOwe" style="width: 100px;">' + v.TraThieu + '</td>' +
                                    '</tr>';
                            });
                        }
                        $('#table-item-product-Quantityowe tbody').append(trs);
                        CheckElementEnable(This);
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }

            function CheckElementEnable(This) {
                var statuscurrent = "";
                statuscurrent = $('.test').find("td:eq(5)").text();

                if ($('#table-item-product-Quantityowe tbody tr').length == 0) {
                    $('.ProductsOwe').remove();
                }

                if ($(This).attr("data-status") == "3") {
                    $('.product-QuantityExport ').prop('disabled', 'true');
                    $('.btnXuatHang').remove();
                }


                for (var j = 0; j < listElement.length; j++) {
                    var nameElement = listElement[j].ElementName;
                    var enabled = listElement[j].Enable;
                    var type = listElement[j].Type;
                    if (type == "hidden" && (enabled == true || $(This).attr("data-status") == "3")) {
                        $("." + nameElement).addClass('hidden');
                    }
                    else if (type == 'disable' && $(This).attr("data-status") == "3") {
                        $("." + nameElement).prop('disabled', true);
                    }
                    else if (type == "visible" && enabled == true) {
                        $("." + nameElement).addClass('hidden');
                    }
                    else {
                        $("." + nameElement).prop('disabled', enabled);
                    }
                }
                removeLoading();
            }

            //============================
            // Event delete
            //============================
            function del(This, code, name, e) {

                var code = code || null,
                    name = name || null,
                    Row = This;
                if (!code) return false;

                // show EBPopup
                $(".confirm-yn").openEBPopup();
                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

                $("#EBPopup .yn-yes").bind("click", function () {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/SystemService/Ajax/Del.aspx/Delele_Bill_QLKho",
                        data: '{Id : ' + code + '}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            var mission = JSON.parse(response.d);
                            if (mission.success) {
                                delSuccess();
                                Row.remove();
                                setTimeout(function () {
                                    $("#ViewData").click();
                                }, 1000);
                            } else {
                                delFailed();
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    });
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
                if (!e) var e = window.event;
                e.cancelBubble = true;
                if (e.stopPropagation) e.stopPropagation();
            }

            // fn onchange cho input số lượng xuất 
            function changequantity(This) {
                This.parent().parent().addClass("currentClass");
                if ($("#table-item-product tbody tr").hasClass("currentClass")) {
                    var cost = $("#table-item-product tbody tr.currentClass .td-product-cost").attr("data-cost");
                    if (cost == "null") {
                        cost = 0;
                    }
                    var quantity = $(This).val();
                    var thanhTien = cost * quantity;
                    $("#table-item-product tbody tr.currentClass .td-product-TotalPrice").text(thanhTien.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));
                    $("#table-item-product tbody tr.currentClass").removeClass("currentClass");
                }
                else {
                    alert("Error!");
                }
                var tongTien = parseInt(thanhTien) + parseInt($("#txtTongOrder").text().toString().replace(/\D/g, ''));
                $("#txtTongOrder").text(tongTien.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1."));
            }
        </script>

    </asp:Panel>
</asp:Content>




