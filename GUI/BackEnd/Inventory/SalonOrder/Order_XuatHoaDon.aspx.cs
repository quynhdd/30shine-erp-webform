﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Linq.Expressions;
using ExportToExcel;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.Inventory.SalonOrder
{
    public partial class Order_XuatHoaDon : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        private string PageID = "DHK_KHO_XHD";
        protected string THead = "";
        protected string StrProductPrice = "";
        public CultureInfo culture = new CultureInfo("vi-VN");
        private DateTime timeFrom;
        private DateTime timeTo;
        private int salonId;
        private int integer;
        protected string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
        protected string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);

        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

        protected bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected string Permission = "";
        protected List<Tbl_Salon> salons;
        protected List<productItem> productItems = new List<productItem>();
        protected string thead;
        protected string trFirst;
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Today);
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
                bindData();
            }
            else
            {
                //Exc_Filter();
            }
            RemoveLoading();
        }

        private List<productItem> getData()
        {
            var data = new List<productItem>();
            using (var db = new Solution_30shineEntities())
            {
                try
                {
                    if (TxtDateTimeFrom.Text != "")
                    {
                        timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);

                        var list = db.Store_QLKho_XuatHoaDon(timeFrom.Date).ToList();
                        if (list.Count > 0)
                        {
                            var item = new productItem();
                            var itemSalon = new productSalonItem();
                            var index = -1;
                            foreach (var v in list)
                            {
                                index = data.FindIndex(w=>w.ProductId == v.ProductId);
                                if (index == -1)
                                {
                                    item = new productItem();
                                    item.ProductId = v.ProductId.Value;
                                    item.ProductName = v.ProductName;
                                    item.SalonData = new List<productSalonItem>();

                                    itemSalon = new productSalonItem();
                                    itemSalon.SalonId = v.SalonId.Value;
                                    itemSalon.SalonName = v.SalonName;
                                    itemSalon.QuantityOrder = v.QuantityOrder.Value;
                                    itemSalon.QuantityExport = v.QuantityExport.Value;
                                    itemSalon.QuantityReceived = v.QuantityReceived.Value;

                                    item.SalonData.Add(itemSalon);
                                    data.Add(item);
                                }
                                else
                                {
                                    item = data[index];

                                    itemSalon = new productSalonItem();
                                    itemSalon.SalonId = v.SalonId.Value;
                                    itemSalon.SalonName = v.SalonName;
                                    itemSalon.QuantityOrder = v.QuantityOrder.Value;
                                    itemSalon.QuantityExport = v.QuantityExport.Value;
                                    itemSalon.QuantityReceived = v.QuantityReceived.Value;

                                    item.SalonData.Add(itemSalon);
                                    data[index] = item;
                                }
                            }

                            // Đặt giá trị mặc định cho các item mà sản phẩm không được xuất cho salon nào đó     
                            thead = @"<thead>
                                            <tr>
                                                <th>STT</th>
                                                <th>Tên SP</th>";
                            trFirst = "<tr><td></td><td></td>";                   
                            salons = db.Tbl_Salon.Where(w=>w.IsDelete !=1 && w.Publish == true).ToList();
                            var temp = data;
                            var loop = 0;
                            if (salons.Count > 0)
                            {
                                foreach (var v in salons)
                                {
                                    thead += "<th colspan='3'>"+v.Name+"</th>";
                                    trFirst += "<td>Yêu cầu</td><td>Thực trả</td><td>Thực nhận</td>";

                                    loop = 0;
                                    foreach (var v2 in temp)
                                    {
                                        index = v2.SalonData.FindIndex(w => w.SalonId == v.Id);
                                        if (index == -1)
                                        {
                                            itemSalon = new productSalonItem();
                                            itemSalon.SalonId = v.Id;
                                            itemSalon.SalonName = v.Name;
                                            itemSalon.QuantityOrder = 0;
                                            itemSalon.QuantityExport = 0;
                                            itemSalon.QuantityReceived = 0;

                                            data[loop].SalonData.Add(itemSalon);
                                        }
                                        loop++;                            
                                    }                      
                                }
                            }
                            thead += "</thead>";
                            trFirst += "</tr>";
                        }
                    }
                }
                catch { }
            }
            return data;
        }

        private void bindData()
        {
            productItems = getData();
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            bindData();
            RemoveLoading();
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        

        public class productItem
        {
            public int ProductId { get; set; }
            public string  ProductName { get; set; }
            public List<productSalonItem> SalonData { get; set; }
        }

        public class productSalonItem
        {
            public int SalonId { get; set; }
            public string SalonName { get; set; }
            public int QuantityOrder { get; set; }
            public int QuantityExport { get; set; }
            public int QuantityReceived { get; set; }
        }
    }
}