﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="CustomerReturnProduct.aspx.cs" Inherits="_30shine.GUI.BackEnd.Inventory.CustomerReturnProduct" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .imageapent {
                width: 15px;
                height: 15px;
            }

            .image12 {
                width: 30px;
                height: 30px;
            }

            .degree90 {
                -webkit-transform: rotate(90deg);
                -moz-transform: rotate(90deg);
                -o-transform: rotate(90deg);
                -ms-transform: rotate(90deg);
                transform: rotate(90deg);
                height: 118px !important;
                width: 118px !important;
            }

            .btn-filter {
                background: #000000;
                color: #ffffff;
                width: 80px !important;
                height: 35px;
                line-height: 30px;
                float: left;
                width: auto;
                text-align: center;
                cursor: pointer;
                -webkit-border-radius: 2px;
                -moz-border-radius: 2px;
                border-radius: 2px;
                margin: 0 5px;
            }

            .downImage .btn-filter:hover {
                color: #ffd800;
            }

            .wp_page {
                width: 80%;
                margin: 0 auto;
            }

            .content-page {
                width: 100%;
                float: left;
                margin: 20px 0;
            }

            .title {
                width: 100%;
                float: left;
                font-size: 16px;
            }

            .raw-inforproduct .title strong, .raw-inforproduct .title {
                font-size: 14px;
            }

            .title strong {
                font-size: 16px;
            }

            .name-product .btn-danger {
                float: right;
            }

            .wp-raw, .wp-raw-infor {
                width: 100%;
                float: left;
            }

            .ctn-raw-data {
                width: 100%;
                float: left;
                border: 1px solid #000;
                padding: 5px 10px;
            }

            .product {
                width: 100%;
                float: left;
                margin-bottom: 5px;
                border-top: 1px solid #000;
                padding-top: 5px;
            }

                .product .btn-success {
                    float: right;
                }

            .input-number {
                width: 50px;
                border: 1px solid #000;
                padding: 0 3px;
                float: right;
                margin-right: 10px;
                font-weight: bold;
                text-align: center;
            }

            .btn-warning {
                float: right;
                margin-top: 10px;
            }

            .wp-from-product {
                width: 100%;
                float: left;
            }

            .raw-data, .name-product {
                width: 100%;
                float: left;
            }

            .raw-data {
                margin-bottom: 5px;
                border-top: 1px solid #000;
                padding-top: 5px;
            }

                .raw-data:first-child {
                    border: none;
                }

            .alert-error {
                color: red;
            }

            .table {
                margin-bottom: 0;
            }
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                       
                        <li>Tìm kiếm khách hàng &nbsp;&#187; </li>
                        <li>
                            <input class="txtFilter form-control" id="phone_number" placeholder="Số điện thoại" />
                        </li>
                        <li>
                            <span class="field-wp">
                                <asp:Button ID="btnFilter" CssClass="btn-send perm-btn-field btn-filter " runat="server" Text="Tìm kiếm" OnClientClick="getCustomerData(); return false;" ClientIDMode="Static"></asp:Button>
                            </span>
                        </li>
                        <li>
                            <i class="alert-error"></i>
                        </li>
                         <li ><a target="_blank" href="https://docs.google.com/document/d/1f8aNKY4n7-Z9XqT3bxzDRfZ_e9WHD6gaR59dpgHPhC8/edit"><span style="font-weight:bold; font-size:15px; color:red; text-decoration:underline">>> HƯỚNG DẪN SỬ DỤNG CHỨC NĂNG <<</span></a></li>
                    </ul>
                </div>
                <div class="wp_page">
                    <div class="content-page">
                        <div class="wp-raw" style="min-height: 680px">
                            <p class="title" style="margin-left: 12px"><strong>Tên khách hàng:</strong> <strong class="customer_name"></strong></p>
                            <strong class="title" style="margin-left: 12px">Danh sách mỹ phẩm khách hàng mua < 20 ngày.</strong>
                            <div class="wp-raw-infor">
                                <div class="raw-inforreturnproduct col-lg-6 col-md-6" style="float: right; min-height: 680px;">
                                    <div class="wp-from-product" style="display: none; margin-top: 17px">
                                        <strong class="title">Danh sách sản phẩm trả</strong>
                                        <div class="ctn-raw-data" style="float: right; margin-top: 2px">
                                            <div style="">
                                                <div class="col-lg-4" style="float: left; padding-left: 0; margin-bottom: 10px;">
                                                    <p class="title">Ngày bán: <strong class="date_sell_return" style="font-size: 14px"></strong></p>
                                                </div>
                                                <div class="col-lg-6" style="float: left; padding-left: 0; margin-bottom: 10px;">
                                                    <p class="title">
                                                        NV nhận hàng:
                                                    <input type="text" class='checkout_id' onchange="getCheckout($(this).val())" style="font-weight: bold; border: 1px solid #808080; width: 50px; padding: 3px; font-size: 12px" placeholder="Mã NV" /><strong class="receiver_name" style="color: #000; font-size: 14px; height: 20px; margin-left: 10px"></strong>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="wp-return-list-product">
                                                <table class="table table-bordered table-striped" id="product_detail">
                                                    <thead>
                                                        <tr>
                                                            <th>Người bán</th>
                                                            <th>Tên SP</th>
                                                            <th>Giá tiền</th>
                                                            <th>% Giảm giá</th>
                                                            <th>Số lượng</th>
                                                            <th>Tiền trả</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-warning btn-sm btn-confirm" onclick="confirmReturn()">Xác Nhận</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="HDFProduct" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="HDFSalonKeeping" runat="server" ClientIDMode="Static" />
        <script>
            // get url
            var URL_API_INVENTORY = "<%= Libraries.AppConstants.URL_API_INVENTORY %>";

            //Check số phone
            function checkPhone(phone) {
                phone = String(phone);
                var x = phone.substring(0, 1);
                if (phone.length < 10 || phone.length > 11 || x != '0' || validatePhone(phone) == false)
                    return false;
                else
                    return true;
            }

            // validate phone
            function validatePhone(txtPhone) {
                var filter = /^[0-9-+]+$/;
                if (filter.test(txtPhone)) {
                    return true;
                }
                else {
                    return false;
                }
            }

            // get bill of customer when find with customer's phone
            function getCustomerData() {
                if ($("#phone_number").val() === '') {
                    ShowMessage('', 'Bạn phải nhập số điện thoại khách hàng', 3);
                    $("#phone_number").css("border-color", "red");
                    $("#phone_number").focus();
                }
                else {
                    $(".raw-inforproduct").remove();
                    $("#product_detail tbody").html('');
                    phone = $("#phone_number").val();
                    $.ajax({
                        url: URL_API_INVENTORY + "/api/product-return?phone=" + phone,
                        type: "GET",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            if (response === '' || response === null || response.length === 0) {
                                ShowMessage('', 'Số điện thoại không chính xác hoặc không có dữ liệu', 3);
                                $("#phone_number").focus();
                                $("#phone_number").css("border-color", "red");
                                $('.alert-error').delay(3000).fadeOut('fast');
                                return false;
                            }
                            startLoading();
                            $(".customer_name").text(response[0].customerName);
                            for (var i = 0; i < response.length; i++) {
                                item =
                                    '<div class="raw-inforproduct col-lg-6 col-md-6"  style="float:left;margin-top:20px">' +
                                    '<p class="title"><strong>' + (i + 1) + '</strong>. Ngày mua hàng:<strong class="date_buy"> ' + response[i].dateOfBill + '</strong> ' +
                                    ' - Salon bán: <strong>' + response[i].salonSellName + '</strong></p > ' +
                                    '<div class="ctn-raw-data">' +
                                    '<table class="table product_table  table-bordered table-striped">' +
                                    '<thead>' +
                                    '<tr>' +
                                    '<th>Người bán</th>' +
                                    '<th>Tên SP</th>' +
                                    '<th>Giá tiền</th>' +
                                    '<th>% Giảm giá</th>' +
                                    '<th>Số lượng</th>' +
                                    '<th>Thành tiền</th>' +
                                    '<th>Action</th>' +
                                    '</tr>' +
                                    '</thead>' +
                                    '<tbody>';
                                for (var j = 0; j < response[i].listProduct.length; j++) {
                                    item += "<tr>" +
                                        "<td class='date_sell' style='display:none'>" + response[i].dateOfBill + "</td>" +
                                        "<td class='bill_id' style='display:none'>" + response[i].listProduct[j].billId + "</td>" +
                                        "<td class='product_id' style='display:none'>" + response[i].listProduct[j].productId + "</td>" +
                                        "<td class='product_forSalary' style='display:none'>" + response[i].listProduct[j].forSalary + "</td>" +
                                        "<td class='salon_id' style='display:none'>" + response[i].salonSellId + "</td>" +
                                        "<td class='seller_id' style='display:none'>" + response[i].listProduct[j].sellerId + "</td>" +
                                        "<td class='product_seller'>" + response[i].listProduct[j].sellerName + "</td>" +
                                        "<td class='product_name'>" + response[i].listProduct[j].name + "</td>" +
                                        "<td class='product_price'>" + response[i].listProduct[j].price + "</td>" +
                                        "<td class='voucher_percent'>" + response[i].listProduct[j].voucherPercent + "</td>" +
                                        "<td class='product_quantity'>" + response[i].listProduct[j].quantity + "</td>" +
                                        "<td class='product_total'>" + (response[i].listProduct[j].price - (response[i].listProduct[j].price * response[i].listProduct[j].voucherPercent / 100)) * response[i].listProduct[j].quantity + "</td>" +
                                        "<td>" +
                                        "<button type='button' class='btn btn-danger btn-xs'" +
                                        "onclick = 'showProduct($(this))'>Trả lại</button > " +
                                        "</td>" +
                                        "</tr>";
                                }
                                '</tbody > ' +
                                    '</table>' +
                                    '</div>' +
                                    '</div>';
                                $(".wp-raw-infor").append(item);
                                finishLoading();
                            }
                            $(".wp-from-product").css("display", "block");
                        }
                    });
                }
            }

            //get data from table send to table with button "Trả lại" click event
            function showProduct(This) {
                $(".btn-confirm").removeAttr("disabled");
                table = $("#product_detail");
                date_sell = This.closest("tr").find(".date_sell").text();
                seller_name = This.closest("tr").find(".product_seller").text();
                bill_id = This.closest("tr").find(".bill_id").text();
                product_id = This.closest("tr").find(".product_id").text();
                for_salary = This.closest("tr").find(".product_forSalary").text();
                seller_id = This.closest("tr").find(".seller_id").text();
                salon_id = This.closest("tr").find(".salon_id").text();
                product_name = This.closest("tr").find(".product_name").text();
                product_price = This.closest("tr").find(".product_price").text();
                product_quantity = This.closest("tr").find(".product_quantity").text();
                voucher_percent = This.closest("tr").find(".voucher_percent").text();
                product_total = (100 - parseInt(voucher_percent)) / 100 * parseInt(product_price) * parseInt(product_quantity);
                if (table.find(".bill_id_return").text() !== '' && parseInt(table.find(".bill_id_return").text()) !== parseInt(bill_id)) {
                    ShowMessage('', 'Vui lòng chọn sản phẩm trong cùng 1 bill để trả lại', 3);
                    return false;
                }
                $(".date_sell_return").text(date_sell);
                $(".seller_name_return").text(seller_name);
                row = "<tr>" +
                    "<td class='bill_id_return' style='display:none'>" + bill_id + "</td>" +
                    "<td class='product_id_return' style='display:none'>" + product_id + "</td>" +
                    "<td class='for_salary_return' style='display:none'>" + for_salary + "</td>" +
                    "<td class='seller_id_return' style='display:none'>" + seller_id + "</td>" +
                    "<td class='salon_id_return' style='display:none'>" + salon_id + "</td>" +
                    "<td class='product_seller_return'>" + seller_name + "</td>" +
                    "<td class='product_name_return'>" + product_name + "</td>" +
                    "<td class='product_price_return'>" + product_price + "</td>" +
                    "<td class='voucher_percent_return'>" + voucher_percent + "</td>" +
                    "<td ><input type='number' class='input-number product_quantity_return' value='1' disabled='disabled' min='1' max='3' /></td>" +
                    "<td class='product_total_return'>" + product_total + "</td>" +
                    "<td><button type='button' class='btn btn-success btn-xs' onclick='remove_row($(this))'>Xóa trả</button></td>" +
                    "</tr>";
                if (table.find('tr').find('.product_id_return').text() === product_id) {
                    var quantity = parseInt(table.find('tr').find(".product_quantity_return").val()) + 1;
                    if (quantity > product_quantity) {
                        ShowMessage('', 'Số lượng trả lại không được vượt quá số lượng khách đặt mua', 3);
                        table.find('tr').find(".product_quantity_return").focus();
                        table.find('tr').find(".product_quantity_return").css('border-color', 'red');
                    }
                    else
                        table.find('tr').find(".product_quantity_return").val(quantity);
                    return false;
                }
                else {
                    $("#product_detail tbody").append(row);
                }
            }

            //get and check exist checkout by checkout_id
            function getCheckout(checkout_id) {
                $.ajax({
                    type: "GET",
                    url: URL_API_INVENTORY + `/api/staff/get-staff-timekeeping?staffId=${checkout_id}`,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response === null || response === '' || response === undefined) {
                            ShowMessage('', 'Mã nhân viên sai hoặc chưa được chấm công tại Salon!', 3);
                            $(".checkout_id").focus();
                            $(".checkout_id").css("border-color", "pink");
                            return false;
                        }
                        else {
                            $(".receiver_name").text(response.staffName);
                            $("#HDFSalonKeeping").val(response.salonId);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        var responseText = jqXHR.responseJSON.message;
                        ShowMessage('', responseText, 4);
                        finishLoading();
                    }
                });
            }

            //remove row when click button "Xóa trả"
            function remove_row(This) {
                table = $("#product_detail tbody");
                This.closest("tr").remove();
            }

            //disable confirm button if table have not data
            $(document).ready(function () {
                if ($("#product_detail tbody").find('tr').length === 0) {
                    $(".btn-confirm").attr("disabled", "disabled");
                }
            });

            // get data from table and send to server with confirm button click event
            function confirmReturn() {
                if ($(".checkout_id").val() === null || $(".checkout_id").val() === '') {
                    $(".receiver_name").text("  Vui lòng nhập mã nhân viên nhận hàng");
                    $(".checkout_id").focus();
                    $(".checkout_id").css("border-color", "pink");
                    return false;
                }
                receiverId = $(".checkout_id").val();
                //defined variable table, array arr, product_array
                table = $("#product_detail tbody");
                var arr = [];
                var product_array = [];
                var product_arrayInventory = [];
                // foreach data from table and insert into array
                table.find("tr").each(function () {
                    id = $(this).find(".product_id_return").text();
                    forSalary = $(this).find(".for_salary_return").text();
                    voucherPercent = $(this).find(".voucher_percent_return").text();
                    price = $(this).find(".product_price_return").text();
                    quantity = $(".product_quantity_return").val();
                    product_array.push({
                        'id': parseInt(id),
                        'forSalary': parseFloat(forSalary),
                        'voucherPercent': parseFloat(voucherPercent),
                        'price': parseFloat(price),
                        'quantity': parseInt(quantity)
                    });
                    //
                    product_arrayInventory.push({
                        'Id': parseInt(id),
                        'Quantity': parseInt(quantity)
                    });
                    //add HDF
                    $("#HDFProduct").val(JSON.stringify(product_arrayInventory));
                });
                billId = (table.find("tr").find(".bill_id_return").text()).substring(0, (table.find("tr").find(".bill_id_return").text()).length / table.find('tr').length);
                sellerId = (table.find("tr").find(".seller_id_return").text()).substring(0, (table.find("tr").find(".seller_id_return").text()).length / table.find('tr').length);
                salonSellId = (table.find("tr").find(".salon_id_return").text()).substring(0, (table.find("tr").find(".salon_id_return").text()).length / table.find('tr').length);
                dateOfBill = $(".date_sell_return").text();
                //salonReceiverId = <%=Convert.ToInt32(Session["SalonId"].ToString())%>
                salonReceiverId = $("#HDFSalonKeeping").val();
                // push all data to array
                arr.push({
                    'billId': parseInt(billId),
                    'salonSellId': parseInt(salonSellId),
                    'salonReceiverId': parseInt(salonReceiverId),
                    'sellerId': parseInt(sellerId),
                    'receiverId': parseInt(receiverId),
                    'dateOfBill': dateOfBill,
                    'listProduct': product_array
                });
                var c = confirm("Bạn chắc chắn muốn trả lại những sản phẩm này về kho?");
                if (c) {
                    startLoading();
                    // call api and send data to server
                    $.ajax({
                        url: URL_API_INVENTORY + "/api/product-return",
                        type: "POST",
                        data: JSON.stringify(arr).slice(1, -1),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            if (response.status === 1) {
                                // call api cập nhật tồn kho Salon
                                updateInventoryProduct(billId, salonReceiverId);
                                $('.date_sell_return').text('');
                                $('.receiver_name').text('');
                                $('.checkout_id').text('');
                                table.find('tr').remove();
                                $(".btn-confirm").attr("disabled", "disabled");
                            }
                            else {
                                ShowMessage('', 'Không cập nhật được số lượng hàng trả lại. Vui lòng liên hệ nhóm phát triển!', 4);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var responseText = jqXHR.responseJSON.message;
                            ShowMessage('', responseText, 4);
                            finishLoading();
                        }
                    });
                }
                else {
                    finishLoading();
                    return false;
                }
            }

            // Cập nhật tồn vật tư kho Salon
            function updateInventoryProduct(BillId, SalonReceiverId) {
                var productArray = $("#HDFProduct").val();
                var data = JSON.stringify({ BillId: parseInt(BillId), SalonId: parseInt(SalonReceiverId), JsonProductIds: productArray });
                console.log(data);
                $.ajax({
                    url: URL_API_INVENTORY + "/api/iv-inventory-current/productReturn",
                    type: "PUT",
                    data: data,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.status === 1) {
                            finishLoading();
                            ShowMessage('', 'Trả hàng thành công, cập nhật số lượng tồn thành công!', 2);
                        }
                        else {
                            ShowMessage('', 'Không cập nhật được tồn kho Salon. Vui lòng liên hệ nhóm phát triển!', 4);
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        var responseText = jqXHR.responseJSON.message;
                        ShowMessage('', responseText, 4);
                        finishLoading();
                    }
                });
            }
        </script>
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
    </asp:Panel>
</asp:Content>
