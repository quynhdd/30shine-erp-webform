﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Globalization;
using LinqKit;
using Project.Helpers;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.UI;
using _30shine.Helpers.Http;
using Libraries;

namespace _30shine.GUI.BackEnd.Inventory
{
    public partial class ConfigInventoryListing : System.Web.UI.Page
    {
        private bool Perm_Access = false;
        private bool Perm_ViewAllData = false;
        protected int Index = 0;
        protected static string ListProduct;
        public List<OutputData> ListOutputData = new List<OutputData>();
        public List<OutputService> ListOutputService = new List<OutputService>();
        public List<OutputProductSub> ListOutputProduct = new List<OutputProductSub>();
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                getDepartments();
                BindOBJ();
            }
        }

        /// <summary>
        /// Load department
        /// </summary>
        private void getDepartments()
        {
            using (var db = new Solution_30shineEntities())
            {
                var department = db.Staff_Type.Where(w => w.IsDelete == 0 && w.Publish == true).ToList();
                var ddls = new List<DropDownList> { ddlDepartment };
                for (var i = 0; i < ddls.Count; i++)
                {
                    ddls[i].DataTextField = "Name";
                    ddls[i].DataValueField = "Id";
                    ddls[i].DataSource = department;
                    ddls[i].DataBind();
                }
            }
        }

        protected void GetData(object sender, EventArgs e)
        {
            BindOBJ();
        }

        public void BindOBJ()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    int departmentId = int.TryParse(ddlDepartment.SelectedValue, out var integer) ? integer : 0;
                    if (departmentId > 0)
                    {
                        var record = new OutputData();
                        var subRepordService = new OutputService();
                        var subRepordProduct = new OutputProductSub();
                        var getData = new Request().RunGetAsyncV1(AppConstants.URL_API_INVENTORY + $"/api/config-quantify-product/get-list?departmentId={departmentId}").Result;
                        //checkout success
                        if (getData.IsSuccessStatusCode)
                        {
                            //response data
                            var origin = getData.Content.ReadAsStringAsync().Result;
                            JavaScriptSerializer serializer = new JavaScriptSerializer();
                            var responseData = serializer.Deserialize<OutputData>(origin);
                            if (responseData != null)
                            {
                                ListOutputData.Add(responseData);
                                foreach (var ser in responseData.data)
                                {
                                    Index = 0;
                                    ListOutputService.Add(ser);
                                    foreach (var pro in ser.product)
                                    {
                                        Index++;
                                        subRepordProduct = new OutputProductSub();
                                        subRepordProduct.Index = Index;
                                        subRepordProduct.Id = pro.Id;
                                        subRepordProduct.ServiceId = ser.ServiceId;
                                        subRepordProduct.ProductId = pro.ProductId;
                                        subRepordProduct.Code = pro.Code;
                                        subRepordProduct.Name = pro.Name;
                                        subRepordProduct.Quantify = pro.Quantify;
                                        subRepordProduct.Volume = pro.Volume;
                                        ListOutputProduct.Add(subRepordProduct);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Output departmentName
        /// </summary>
        public class OutputData
        {
            public int DepartmentId { get; set; }
            public string DepartmentName { get; set; }
            public List<OutputService> data { get; set; }

        }

        /// <summary>
        /// Output servicename
        /// </summary>
        public class OutputService
        {
            public int ServiceId { get; set; }
            public string ServiceName { get; set; }
            public List<OutputProduct> product { get; set; }
        }

        /// <summary>
        /// Output product
        /// </summary>
        public class OutputProduct
        {
            public int Id { get; set; }
            public int ProductId { get; set; }
            public int ServiceId { get; set; }
            public string Name { get; set; }
            public string Code { get; set; }
            public double Volume { get; set; }
            public double Quantify { get; set; }
        }

        /// <summary>
        /// Output product
        /// </summary>
        public class OutputProductSub
        {
            public int Index { get; set; }
            public int Id { get; set; }
            public int ProductId { get; set; }
            public int ServiceId { get; set; }
            public string Name { get; set; }
            public string Code { get; set; }
            public double TotalNumberService { get; set; }
            public double Volume { get; set; }
            public double Quantify { get; set; }
        }
    }
}