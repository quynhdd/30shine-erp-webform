﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="QLKho_Export.aspx.cs" Inherits="_30shine.GUI.BackEnd.Inventory.QLKho_Export" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <link href="/Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="/Assets/js/select2/select2.min.js"></script>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý hàng tồn kho theo tuần &nbsp;&#187;</li>
                        <li>Thống kê xuất vật tư cho Salon</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row">
                    <div class="filter-item">
                        <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="time-wp">
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Từ ngày" Style="margin-left: 10px!important"
                                ClientIDMode="Static" runat="server"></asp:TextBox><br />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" InitialValue="0" ControlToValidate="TxtDateTimeFrom" runat="server" CssClass="fb-cover-error" SetFocusOnError="true" Text="<i style='color:red'>Bạn chưa chọn ngày bắt đầu!</i>"></asp:RequiredFieldValidator>
                        </div>
                        <strong class="st-head" style="margin-left: 10px;">
                            <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                        </strong>
                        <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeTo" placeholder="Đến ngày" Style="margin-left: 10px!important"
                            ClientIDMode="Static" runat="server"></asp:TextBox><br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" InitialValue="0" ControlToValidate="TxtDateTimeTo" runat="server" CssClass="fb-cover-error" SetFocusOnError="true" Text="<i style='color:red'>Bạn chưa chọn ngày kết thúc!</i>"></asp:RequiredFieldValidator>
                    </div>
                    <div class="filter-item" style="margin-top: 8px">
                        <asp:DropDownList ID="ddlSalon" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px;"></asp:DropDownList>
                    </div>
                    <div class="filter-item" style="width: 180px; margin-top: 8px">
                        <asp:DropDownList ID="ddlProduct" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 200px;"></asp:DropDownList>
                        <br />
                        <asp:RequiredFieldValidator ID="NameValidate" InitialValue="0" ControlToValidate="ddlProduct" runat="server" CssClass="fb-cover-error" SetFocusOnError="true" Text="<i style='color:red'>Bạn chưa chọn sản phẩm!</i>"></asp:RequiredFieldValidator>
                    </div>
                    <div class="filter-item">
                        <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" Text="Xem dữ liệu"
                            ClientIDMode="Static" onclick="excPaging(1)" runat="server">
                            Xem dữ liệu
                        </asp:Panel>
                    </div>
                    <!-- End Filter -->
                    <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>

                    <!-- Row Table Filter -->
                    <div class="table-func-panel" style="margin-top: -33px;">
                        <div class="table-func-elm">
                            <span>Số hàng / Page : </span>
                            <div class="table-func-input-wp">
                                <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                                <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                                <ul class="ul-opt-segment">
                                    <li data-value="10">10</li>
                                    <li data-value="20">20</li>
                                    <li data-value="30">30</li>
                                    <li data-value="40">40</li>
                                    <li data-value="50">50</li>
                                    <li data-value="200">200</li>
                                </ul>
                                <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                            </div>
                        </div>
                    </div>
                    <!-- End Row Table Filter -->
                    <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                        <ContentTemplate>
                            <div class="row table-wp">
                                <table class="table-add table-listing report-sales-listing">
                                    <thead>
                                        <tr>
                                            <th>STT</th>
                                            <th>Tên salon</th>
                                            <th>Mã Code - Tên sản phẩm</th>
                                            <th>Số lượng</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater ID="Rpt_Product" runat="server">
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                    <td><%#Eval("SalonName") %></td>
                                                    <td><%#Eval("ProductName") %></td>
                                                    <td><%#Eval("TotalQuantityReceived") %></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </div>
                            <!-- Paging -->
                            <div class="site-paging-wp">
                                <% if (PAGING.TotalPage > 1)
                                    { %>
                                <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                    <% if (PAGING._Paging.Prev != 0)
                                        { %>
                                    <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                    <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                    <% } %>
                                    <asp:Repeater ID="RptPaging" runat="server">
                                        <ItemTemplate>
                                            <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                                <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                                <%# Eval("PageNum") %>
                                            </a>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                        { %>
                                    <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                    <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                    <% } %>
                                </asp:Panel>
                                <% } %>
                            </div>
                            <!-- End Paging -->
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                    <asp:HiddenField runat="server" ClientIDMode="Static" ID="IsPaging" />
                    <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                    <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_ExcelType" />
                    <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELPage" />
                    <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELSegment" />
                </div>
                <%-- END Listing --%>
            </div>
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
                margin-top: -3px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script>
            $('.txtDateTime').datetimepicker({
                dayOfWeekStart: 1,
                lang: 'vi',
                startDate: '2014/10/10',
                format: 'd/m/Y',
                dateonly: true,
                showHour: false,
                showMinute: false,
                timepicker: false,
                onChangeDateTime: function (dp, $input) { }
            });
        </script>
    </asp:Panel>
</asp:Content>
