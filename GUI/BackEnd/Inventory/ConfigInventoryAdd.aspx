﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="ConfigInventoryAdd.aspx.cs" Inherits="_30shine.GUI.BackEnd.Inventory.ConfigInventoryAdd" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <title>Cấu hình định lượng sản phẩm</title>
    <link href="/Assets/css/bootstrap/bootstrap-toggle.min.css" rel="stylesheet" />
    <script src="/Assets/js/bootstrap/bootstrap-toggle.min.js"></script>

    <script src="/Assets/libs/magicsuggest-master/magicsuggest.js"></script>
    <link href="/Assets/libs/magicsuggest-master/magicsuggest.css" rel="stylesheet" />
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <style>
        .customer-add .table-add .title-head strong {
            font-family: sans-serif;
            font-weight: 700;
            font-size: 17px;
        }

        .sub-menu ul.ul-sub-menu li a {
            padding-left: 30px;
        }

        .fe-service .tr-product .table-listing-product tbody input.product-quantify {
            border-bottom: 1px solid #ddd !important;
            width: 100px !important;
        }

        .fe-service .tr-product .table-listing-product tbody input.product-volume {
            border-bottom: 1px solid #ddd !important;
            width: 100px !important;
        }

        .fe-service .tr-product .listing-product {
            display: block !important;
        }

        .suggestion-wrap {
            width: 420px;
            float: left;
        }

        .ms-sel-ctn input[type='text'] {
            height: auto !important;
            line-height: 24px !important;
            border: none !important;
        }

        .ms-ctn .ms-sel-item {
            line-height: 22px;
            color: #000000;
            font-size: 13px;
        }

        .ms-helper {
            display: none !important;
        }

        .input-quantity-wrap {
            float: left;
            margin-left: 10px;
        }

        .input-quantity {
            width: 90px;
            line-height: 36px;
            padding: 0 10px;
            border: 1px solid #ddd;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
        }

        .btn-add {
            float: left;
            background: #ddd;
            padding: 9px 15px;
            margin-left: 10px;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            cursor: pointer;
        }

        .view-ddl {
            height: 36px !important;
            line-height: 36px !important;
            width: 100% !important;
            float: none !important;
        }
    </style>
</asp:Content>
<asp:Content ID="ServiceAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Cấu hình định lượng &nbsp;&#187; Dịch vụ &#187;</li>
                        <li><a href="/admin/cau-hinh-dinh-luong/danh-sach.html"><i class="fa fa-list-ul"></i>&nbsp Danh sách định lượng</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add admin-product-add fe-service">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <!-- System Message -->
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <!-- END System Message -->
                <div class="table-wp">
                    <table class="table-add admin-product-table-add fe-service-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Cấu hình định lượng sản phẩm của dịch vụ</strong></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr class="tr-margin" style="height: 20px;"></tr>
                            <tr class="tr-field-ahalf" runat="server" id="TrSalon">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right">
                                    <div class="department-select" style="height: 0px !important">
                                        <label style="float: left; line-height: 36px; padding-right: 10px; font-family: Roboto Condensed Bold;">Bộ phận&#187;</label>
                                        <div class="field-wp" style="float: left; width: auto;">
                                            <asp:DropDownList ID="ddlDepartment" CssClass="form-control select" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="service-select">
                                        <label style="float: left; line-height: 36px; padding-left: 40px; padding-right: 10px; font-family: Roboto Condensed Bold;">Dịch vụ&#187;</label>
                                        <div class="field-wp" style="float: left; width: auto;">
                                            <asp:DropDownList ID="ddlService" CssClass="form-control select" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="service-select">
                                        <label style="float: left; line-height: 36px; padding-left: 40px; padding-right: 10px; font-family: Roboto Condensed Bold;">Tổng số lần sử dụng dịch vụ&#187;</label>
                                        <div class="field-wp" style="float: left; width: 35px !important; height: 35px !important; text-align: center;">
                                            <asp:TextBox Style="float: left; width: 60px !important; height: 35px !important; text-align: center;" CssClass="st-head form-control" ID="txtTotalNumberService" placeholder="Vd: 30"
                                                ClientIDMode="Static" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Sản phẩm</span></td>
                                <td class="col-xs-9 right">
                                    <div class="suggestion-wrap">
                                        <input id="ms" class="form-control" type="text" />
                                    </div>
                                    <div class="input-quantity-wrap">
                                        <input type="text" id="inputVolume" class="form-control" placeholder="Trọng lượng" onkeypress="return ValidateKeypress(/\d/,event);" style="width: 80px; text-align: center;" maxlength="10" />
                                    </div>
                                    <div class="input-quantity-wrap">
                                        <input type="text" id="inputQuantify" class="form-control" placeholder="Định lượng" onkeypress="return ValidateKeypress(/\d/,event);" style="width: 80px; text-align: center;" maxlength="10" />
                                    </div>
                                    <div class="btn-add" onclick="addProduct()">Thêm sản phẩm</div>
                                </td>
                            </tr>

                            <tr class="tr-field-ahalf tr-product">
                                <td class="col-xs-2 left"><span></span></td>
                                <td class="col-xs-9 right">
                                    <div class="listing-product item-product" id="ListingProductWp" runat="server" clientidmode="Static">
                                        <table class="table table-listing-product table-item-product" id="table-item-product">
                                            <thead>
                                                <tr>
                                                    <th>STT</th>
                                                    <th class="maSPcol">Mã sản phẩm</th>
                                                    <th>Tên sản phẩm</th>
                                                    <th style="width: 100px">Trọng lượng</th>
                                                    <th style="width: 100px">Định lượng</th>
                                                    <th style="width: 50px"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater ID="RptConfigInventory" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="text-align: center;" class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                                            <td class="td-product-code maSPcol" style="width: 120px; text-align: center;" data-id="<%# Eval("ProductId") %>"><%# Eval("Code") %></td>
                                                            <td style="text-align: center;" class="td-product-name"><%# Eval("Name") %></td>
                                                            <td style="text-align: center;" class="td-product-volume">
                                                                <input style="text-align: center;" type="text" class="product-volume" value="<%# Eval("Volume") %>" />
                                                            </td>
                                                            <td style="text-align: center;" class="td-product-quantify">
                                                                <input style="text-align: center;" type="text" class="product-quantify" value="<%# Eval("Quantify") %>" />
                                                            </td>
                                                            <td class="map-edit">
                                                                <div class="edit-wp">
                                                                    <a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),'<%# Eval("Name") %>','product')"
                                                                        href="javascript://" title="Xóa"></a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr class="tr-send">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <asp:Panel ID="BtnSend" CssClass="btn-send" runat="server" ClientIDMode="Static">Hoàn Tất</asp:Panel>
                                        <asp:Button ID="BtnFakeSend" OnClick="FuncComplete" runat="server" Text="Hoàn tất"
                                            ClientIDMode="Static" Style="display: none;"></asp:Button>
                                        <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>
                                    </span>
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
                        </tbody>
                    </table>

                    <!-- input hidden -->
                    <asp:HiddenField ID="HDF_CustomerCode" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_ProductIds" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_ServiceIds" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_UserImages" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_UserImagesDelete" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="Recipient" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HairMassage" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="Cosmetic" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_SalonId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_lgId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_BillId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_FreeService" ClientIDMode="Static" runat="server" />
                    <asp:HiddenField ID="HDF_Rating" ClientIDMode="Static" runat="server" />
                    <!-- END input hidden -->
                </div>
            </div>
            <%-- end Add --%>
        </div>

        <script src="/assets/js/jquery.mCustomScrollbar.js"></script>
        <link href="/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 200px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 35px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/javascript">

            var URL_INVENTORY = "<%= Libraries.AppConstants.URL_API_INVENTORY %>";
            var itemProduct;
            var quantity;
            var index;
            var ms;
            // Btn Send
            $("#BtnSend").bind("click", function () {

                let result = false;
                var Code = "<%= Id %>";
                startLoading();
                let departmentId = $("#ddlDepartment :selected").val();
                let serviceId = $("#ddlService :selected").val();
                let index = 0;

                $("table.table-item-product tbody tr").each(function () {
                    var THIS = $(this);
                    let quantify = THIS.find("input.product-quantify").val();
                    let volume = THIS.find("input.product-volume").val();
                    let name = THIS.find("td.td-product-name").attr("data-name");
                    if (quantify == '') {
                        showMsgSystem('Nhập định lượng cho sản phẩm.', 'msg-system warning', 3000);
                        result = true;
                    }
                    else if (quantify == '0') {
                        showMsgSystem('Định lượng phải lớn hơn 0.', 'msg-system warning', 3000);
                        result = true;
                    }
                    else if (volume == '') {
                        showMsgSystem('Nhập trọng lượng cho sản phẩm.', 'msg-system warning', 3000);
                        result = true;
                    }
                    else if (volume == '0') {
                        showMsgSystem('Trọng lượng phải lớn hơn 0.', 'msg-system warning', 3000);
                        result = true;
                    }
                });
                if (!result) {
                    if (Code != null && Code > 0) {
                        var Ids = [];
                        var prd = {};
                        $("table.table-item-product tbody tr").each(function () {
                            prd = {};
                            var THIS = $(this);
                            var Id = THIS.find("td.td-product-code").attr("data-id"),
                                Quantify = THIS.find("input.product-quantify").val(),
                                Volume = THIS.find("input.product-volume").val(),
                                // check value
                                Id = Id.toString().trim() != "" ? parseInt(Id) : 0;
                            prd.ProductId = Id;
                            prd.Quantify = Quantify;
                            prd.Volume = Volume;
                            Ids.push(prd);
                        });
                        Ids = JSON.stringify(Ids);
                        $("#HDF_ProductIds").val(Ids);
                        console.log($("#HDF_ProductIds").val());
                        $("#BtnFakeSend").click();
                    }
                    else {
                        $("table.table-item-product tbody tr").each(function () {
                            var THIS = $(this);
                            var productId = THIS.find("td.td-product-code").attr("data-id");
                            if (productId != "undefined") {
                                index++;
                            }
                        });
                        if (index > 0) {
                            $.ajax({
                                type: "GET",
                                url: URL_INVENTORY + `/api/config-quantify-product/check-exist?departmentId=${departmentId}&serviceId=${serviceId}`,
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (response) {
                                    finishLoading();
                                    if (response != 1) {
                                        $("#BtnFakeSend").click();
                                    }
                                    else {
                                        swal("Dịch vụ đã cấu hình định lượng, yêu cầu update.", "", "error")
                                            .then((value) => {
                                                window.location.href = '/admin/cau-hinh-dinh-luong/danh-sach.html';
                                            });
                                    }
                                }
                            });
                        }
                        else {
                            finishLoading();
                            showMsgSystem('Yêu cầu chọn sản phẩm.', 'msg-system warning', 3000);
                        }
                    }
                }
                finishLoading();
            });

            jQuery(document).ready(function () {
                // bind data to dropdownlist
                ms = $('#ms').magicSuggest({
                    maxSelection: 1,
                    data: <%=ListProduct %>,
                    valueField: 'Id',
                    displayField: 'Name',
                });

                $(ms).on('selectionchange', function (e, m) {
                    itemProduct = null;
                    var listProduct = this.getSelection();
                    console.log(listProduct);
                    var listData = ms.getData();
                    if (listProduct.length > 0) {
                        for (var i = 0; i < listData.length; i++) {
                            if (listProduct[0].Id == listData[i].Id) {
                                itemProduct = listProduct[0];
                                $("#inputVolume").focus();
                                break;
                            }
                            else {
                                continue;
                            }
                        }
                        if (itemProduct == null) {
                            showMsgSystem("Lễ tân chú ý chọn đúng sản phẩm trong danh sách vật tư dịch vụ.", "warning");
                            ms.clear();
                            $(".ms-sel-ctn input[type='text']").focus();
                        }
                    }
                });
            });

            //get product to form body
            function getDataProduct() {
                var Ids = [];
                var prd = {};
                $("table.table-item-product tbody tr").each(function () {
                    prd = {};
                    var THIS = $(this);
                    var Id = THIS.find("td.td-product-code").attr("data-id"),
                        Quantify = THIS.find("input.product-quantify").val(),
                        Volume = THIS.find("input.product-volume").val(),
                        // check value
                        Id = Id.toString().trim() != "" ? parseInt(Id) : 0;
                    prd.ProductId = Id;
                    prd.Quantify = Quantify;
                    prd.Volume = Volume;
                    Ids.push(prd);
                });
                Ids = JSON.stringify(Ids);
                $("#HDF_ProductIds").val(Ids);
            }

            function addProduct() {
                var result = false;
                var error = '';
                // check trùng sản phẩm
                quantify = $("#inputQuantify").val();
                volume = $("#inputVolume").val();
                if (itemProduct == null) {
                    showMsgSystem('Yêu cầu chọn sản phẩm.', 'msg-system warning', 3000);
                    result = true;
                }
                else if (quantify == '') {
                    showMsgSystem('Nhập định lượng cho sản phẩm.', 'msg-system warning', 3000);
                    result = true;
                }
                else if (quantify == '0') {
                    showMsgSystem('Định lương phải lớn hơn 0.', 'msg-system warning', 3000);
                    result = true;
                }
                else if (volume == '') {
                    showMsgSystem('Nhập trọng lượng cho sản phẩm.', 'msg-system warning', 3000);
                    result = true;
                }
                else if (volume == '0') {
                    showMsgSystem('Trọng lượng phải lớn hơn 0.', 'msg-system warning', 3000);
                    result = true;
                }
                else if (itemProduct != null) {
                    $("table.table-item-product tbody tr").each(function () {
                        var THIS = $(this);
                        var productId = THIS.find("td.td-product-code").attr("data-id");
                        if (productId != "undefined" && itemProduct.Id == productId) {
                            showMsgSystem('Sản phẩm đã tồn tại trong danh sách, yêu cầu thay đổi định lượng hoặc trọng lượng.', 'msg-system warning', 3000);
                            $("#inputQuantify").val('');
                            $("#inputVolume").val('');
                            result = true;
                        }
                    });
                }
                if (!result) {
                    index = $("#table-item-product tbody tr").length + 1;
                    if (itemProduct != null && quantify > 0 && volume > 0) {
                        if (typeof itemProduct.Code != 'undefined') {
                            var tr = '<tr>' +
                                '<td style="text-align: center;">' + index + '</td>' +
                                '<td style="text-align: center;" class="td-product-code maSPcol" data-id="' + itemProduct.Id + '">' + itemProduct.Code + '</td>' +
                                '<td class="td-product-name" style="text-align:center;" data-name="' + itemProduct.Name + '">' + itemProduct.Name + '</td>' +
                                '<td class="td-product-order" data-order="' + itemProduct.Order + '" style="display:none;text-align: center;"> ' + itemProduct.Order + ' </td>' +
                                '<td style="text-align: center;" class="td-product-volume">' +
                                '<input class="product-volume" value="' + volume + '" type="text" onkeypress="return ValidateKeypress(/\\d/,event);" style="text-align:center;" maxlength="10" placeholder="Trọng lượng"/>' +
                                '</td>' +
                                '<td style="text-align: center;" class="td-product-quantify">' +
                                '<input class="product-quantify" value="' + quantify + '" type="text" onkeypress="return ValidateKeypress(/\\d/,event);" style="text-align:center;" maxlength="10" placeholder="Định lượng"/>' +
                                '</td>' +
                                '<td class=" map-edit">' +
                                '<div class="edit-wp">' +
                                    '<a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),\'<%# Eval("Name") %>\',\'product\')" href="javascript://" title="Xóa"></a>' +
                                '</div>' +
                                '</td>' +
                                '</tr>';
                            $("#table-item-product tbody").append($(tr));
                            $("#ListingProductWp").show();
                            getDataProduct();
                            resetInputForm();
                        }
                        else {
                            showMsgSystem("Lễ tân chú ý chọn đúng sản phẩm trong danh sách vật tư dịch vụ.", "warning");
                            ms.clear();
                            $(".ms-sel-ctn input[type='text']").focus();
                        }

                    }
                }
            }


            // Update order item
            function UpdateItemOrder(dom) {
                var index = 1;
                dom.find("tr").each(function () {
                    $(this).find("td.td-product-index").text(index);
                    index++;
                });
            }

            // Remove item đã được chọn
            function RemoveItem(THIS, name, itemName) {
                // Confirm
                $(".confirm-yn").openEBPopup();

                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn hủy [ " + name + " ] ?");
                $("#EBPopup .yn-yes").bind("click", function () {
                    var Code = THIS.find(".td-staff-code").attr("data-code");
                    $("#quick-" + itemName).find("input[data-code='" + Code + "']").removeAttr("checked");
                    THIS.remove();
                    getDataProduct();
                    UpdateItemDisplay(itemName);
                    autoCloseEBPopup(0);
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
            }

            //reset input
            function resetInputForm() {
                ms.clear();
                $("#inputQuantify").val('');
                $("#inputVolume").val('');
                $(ms).focus();
                $(".ms-sel-ctn input[type='text']").focus();
            }

            // Update table display
            function UpdateItemDisplay(itemName) {
                var dom = $(".table-item-" + itemName);
                var len = dom.find("tbody tr").length;
                if (len == 0) {
                    dom.parent().hide();
                } else {
                    UpdateItemOrder(dom.find("tbody"));
                }
            }

            // Validate keypress
            function ValidateKeypress(numcheck, e) {
                var keynum, keychar, numcheck;
                if (window.event) {
                    keynum = e.keyCode;
                }
                else if (e.which) {
                    keynum = e.which;
                }
                if (keynum == 8 || keynum == 127 || keynum == null || keynum == 9 || keynum == 0 || keynum == 13 || keynum == 46) return true;
                keychar = String.fromCharCode(keynum);
                var result = numcheck.test(keychar);
                return result;
            }
        </script>
        <!-- Page loading -->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
    </asp:Panel>
</asp:Content>
