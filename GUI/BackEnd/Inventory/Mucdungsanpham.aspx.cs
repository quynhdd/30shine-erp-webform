﻿using _30shine.Helpers;
using _30shine.Helpers.Http;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.Inventory
{
    public partial class Mucdungsanpham : System.Web.UI.Page
    {
        private string PageID = "_30shine.GUI.BackEnd.Inventory.Mucdungsanpham";
        public bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected Paging PAGING = new Paging();
        public CultureInfo culture = new CultureInfo("vi-VN");

        /// <summary>
        /// set permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        /// <summary>
        /// page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList>() { ddlSalon }, Perm_ViewAllData);
                TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
                TxtDateTimeTo.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
            }
        }

        /// <summary>
        /// btn click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _BtnClick(object sender, EventArgs e)
        {
            getData();
            RemoveLoading();
        }

        /// <summary>
        /// removeloading
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Paging
        /// </summary>
        /// <param name="TotalRow"></param>
        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        /// <summary>
        /// Get data call api
        /// </summary>
        public void getData()
        {
            try
            {
                var filter = new cls_Search();
                filter.salonId = Convert.ToInt32(ddlSalon.SelectedValue);
                if (TxtDateTimeFrom.Text != "")
                {
                    filter.fromDate = TxtDateTimeFrom.Text;
                    if (TxtDateTimeTo.Text != "")
                    {
                        if (TxtDateTimeFrom.Text == TxtDateTimeTo.Text)
                        {
                            filter.toDate = filter.fromDate;
                        }
                        else
                        {
                            filter.toDate = TxtDateTimeTo.Text;
                        }
                    }
                    else
                    {
                        filter.toDate = filter.fromDate;
                    }
                }
                Session["toDate"] = filter.toDate;
                Session["fromDate"] = filter.fromDate;
                Session["salonId"] = filter.salonId;
                var request = new Request();
                var response = request.RunGetAsyncV1(Libraries.AppConstants.URL_API_INVENTORY + "/api/inventory-manager/get-level-of-use?fromDate=" + filter.fromDate + "&toDate=" + filter.toDate + "&salonId=" + filter.salonId + "").Result;
                if (response.IsSuccessStatusCode)
                {
                    var data = response.Content.ReadAsAsync<ResponseData>().Result.data.ToList();
                    if (data != null)
                    {
                        Bind_Paging(data.Count);
                        rptLevelOfUse.DataSource = data.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                        rptLevelOfUse.DataBind();

                    }
                }
            }
            catch (Exception ex)
            {
                //Library.Function.WriteToFile("Getlist Exception First " + DateTime.Now + ": " + ex.Message, "CHICHINGAY (INVENTORY)");
                SendMail("nhuphuc1007@gmail.com", "ERROR MAIN", ex.Message);
            }

        }

        /// <summary>
        /// get quyền 
        /// </summary>
        /// <returns></returns>
        public bool IsAdmin()
        {
            if (Session["User_Permission"].ToString() == "ADMIN" || Session["User_Permission"].ToString() == "root")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///send mail
        /// </summary>
        /// <param name="ReceiveEmail"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        public void SendMail(string ReceiveEmail, string subject, string body)
        {
            string SENDER_EMAIL = "phuc.2110@gmail.com";
            string SENDER_PASSWORD = "tflmxmdmgnyuqzhh";
            //Reading sender Email credential from web.config file
            string HostAdd = "smtp.gmail.com";
            string ToEmail = ReceiveEmail;

            //creating the object of MailMessage
            System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage();
            mailMessage.From = new MailAddress(SENDER_EMAIL); //From Email Id
            mailMessage.Subject = subject; //Subject of Email
            mailMessage.Body = body; //body or message of Email
            mailMessage.IsBodyHtml = true;
            //Adding Multiple recipient email id logic
            string[] Multi = ToEmail.Split(','); //spiliting input Email id string with comma(,)
            foreach (string Multiemailid in Multi)
            {
                mailMessage.To.Add(new MailAddress(Multiemailid)); //adding multi reciver's Email Id
            }
            SmtpClient smtp = new SmtpClient(); // creating object of smptpclient
            smtp.Host = HostAdd; //host of emailaddress for example smtp.gmail.com etc

            //network and security related credentials
            smtp.EnableSsl = true;
            NetworkCredential NetworkCred = new NetworkCredential();
            NetworkCred.UserName = mailMessage.From.Address;
            NetworkCred.Password = SENDER_PASSWORD;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;
            smtp.Send(mailMessage); //sending Email
        }

        /// <summary>
        /// ResponseData
        /// </summary>
        public class ResponseData
        {
            public int status { get; set; }
            public string message { get; set; }
            public List<Origin> data { get; set; }
            public ResponseData()
            {
                data = null;
                status = 0;
                message = "";
            }
        }

        /// <summary>
        /// origin
        /// </summary>
        public class Origin
        {
            public int id { get; set; }
            public string name { get; set; }
            public string code { get; set; }
            public float price { get; set; }
            public float cost { get; set; }
            public float quantity { get; set; }
            public float totalMoney { get; set; }
        }

        /// <summary>
        /// cls search
        /// </summary>
        public class cls_Search
        {
            public int salonId { get; set; }
            public string fromDate { get; set; }
            public string toDate { get; set; }
        }
    }
}