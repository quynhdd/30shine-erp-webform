﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Linq.Expressions;
using LinqKit;

namespace _30shine.GUI.BackEnd.UIStaff
{
    public partial class Account_Listing : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        private Expression<Func<Staff, bool>> Where = PredicateBuilder.True<Staff>();
        private bool Perm_Access = false;
        private bool Perm_AllData = false;
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        private bool Perm_ShowSalon = false;
        protected bool Perm_AllowAddNew = false;
        protected int SalonId;

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckPermission();

            if (!IsPostBack)
            {
                Bind_Salon();
                Bind_StaffType();
                GenWhere();

                Bind_Paging();
                Bind_RptStaff();
                RemoveLoading();
            }
            else
            {
                GenWhere();
            }
        }

        private void GenWhere()
        {
            Where = Where.And(w => w.IsDelete != 1 && w.isAccountLogin == 1);
            int integer;
            int SalonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
            int StaffTypeId = int.TryParse(StaffType.SelectedValue, out integer) ? integer : 0;
            if (SalonId > 0)
            {
                Where = Where.And(w => w.SalonId == SalonId);
            }
            if (StaffTypeId > 0)
            {
                Where = Where.And(w => w.Type == StaffTypeId);
            }
        }

        private void Bind_RptStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Staffs = db.Staffs.AsExpandable().Where(Where).OrderByDescending(o => o.Id).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                RptStaff.DataSource = _Staffs;
                RptStaff.DataBind();
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
            Bind_RptStaff();
            RemoveLoading();
        }

        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                var Count = db.Staffs.AsExpandable().Count(Where);
                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        private void Bind_Salon()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Salons = db.Tbl_Salon.Where(w => w.IsDelete != 1).OrderBy(o => o.Id).ToList();
                var Key = 0;
                SalonId = Convert.ToInt32(Session["SalonId"]);

                Salon.DataTextField = "Salon";
                Salon.DataValueField = "Id";

                if (Perm_AllData)
                {
                    ListItem item = new ListItem("Chọn Salon", "0");
                    Salon.Items.Insert(0, item);

                    foreach (var v in _Salons)
                    {
                        Key++;
                        item = new ListItem(v.Name, v.Id.ToString());
                        Salon.Items.Insert(Key, item);
                    }
                    Salon.SelectedIndex = 0;
                }
                else
                {
                    foreach (var v in _Salons)
                    {
                        if (v.Id == SalonId)
                        {
                            ListItem item = new ListItem(v.Name, v.Id.ToString());
                            Salon.Items.Insert(Key, item);
                            Salon.SelectedIndex = SalonId;
                            Salon.Enabled = false;
                        }

                    }
                }
            }
        }

        private void Bind_StaffType()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Staff_Type.Where(w => w.IsDelete != 1).OrderBy(o => o.Id).ToList();
                var Key = 0;

                StaffType.DataTextField = "StaffType";
                StaffType.DataValueField = "Id";

                ListItem item = new ListItem("Chọn kiểu nhân viên", "0");
                StaffType.Items.Insert(0, item);

                foreach (var v in lst)
                {
                    Key++;
                    item = new ListItem(v.Name, v.Id.ToString());
                    StaffType.Items.Insert(Key, item);
                }
                if (lst.Count > 0)
                {
                    StaffType.SelectedIndex = 1;
                }
                else
                {
                    StaffType.SelectedIndex = 0;
                }
            }
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["User_Id"] == null)
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "mode1", "salonmanager" };
                string[] AllowAllData = new string[] { "root", "admin", "mode1" };
                string[] AllowDelete = new string[] { "root", "admin" };
                string[] AllowSalon = new string[] { "root", "admin" };
                string[] AllowAddNew = new string[] { "root", "admin" };
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }
                if (Array.IndexOf(AllowAllData, Permission) != -1)
                {
                    Perm_AllData = true;
                }
                if (Array.IndexOf(AllowDelete, Permission) != -1)
                {
                    Perm_Delete = true;
                }
                if (Array.IndexOf(AllowSalon, Permission) != -1)
                {
                    Perm_ShowSalon = true;
                }
                if (Array.IndexOf(AllowAddNew, Permission) != -1)
                {
                    Perm_AllowAddNew = true;
                }
                Perm_Edit = true;
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }
    }
} 