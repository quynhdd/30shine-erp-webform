﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Account_Edit.aspx.cs" Inherits="_30shine.GUI.BackEnd.UIStaff.Account_Edit" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="StaffAdd" ContentPlaceHolderID="CtMain" runat="server">
<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

<div class="wp sub-menu" runat="server" id="ActionMenu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">
                <li>Quản lý Tài khoản &nbsp;&#187; </li>
                <li class="li-listing"><a href="/admin/account/danh-sach.html">Danh sách</a></li>
                <li class="li-add"><a href="/admin/account/them-moi.html">Thêm mới</a></li>
                <li class="li-edit"><a href="javascript://">Chỉnh sửa</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add customer-listing be-report be-report-timekeeping">
    <%-- Add --%>
    <div class="wp960 content-wp" id="StaffInfoWrap" style="display:none; padding: 0 20%;">
        <!-- System Message -->
        <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
        <!-- END System Message -->

        <div class="table-wp">
            <table class="table-add">
                <tbody>
                    <tr class="title-head">
                        <td><strong>Thông tin tài khoản</strong></td>
                        <td></td>
                    </tr>
                    <tr class="tr-margin" style="height: 20px;"></tr>
                    
                    <tr runat="server" id="TrSalon">
                        <td class="col-xs-3 left"><span>Salon</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:DropDownList ID="Salon" runat="server" ClientIDMode="Static" CssClass="select perm-ddl-field"></asp:DropDownList>
                                <%--<asp:RequiredFieldValidator InitialValue="0" 
                                    ID="ValidateSalon" Display="Dynamic" 
                                    ControlToValidate="Salon"
                                    runat="server"  Text="Bạn chưa chọn Salon!" 
                                    ErrorMessage="Vui lòng chọn Salon!"
                                    ForeColor="Red"
                                    CssClass="fb-cover-error">
                                </asp:RequiredFieldValidator>--%>
                            </span>
                        </td>
                    </tr>

                    <tr runat="server" id="TrPermission">
                        <td class="col-xs-3 left"><span>Phân quyền</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:DropDownList ID="PermissionList" runat="server" ClientIDMode="Static" CssClass="select perm-ddl-field"></asp:DropDownList>
                                <%--<asp:RequiredFieldValidator InitialValue="0" 
                                    ID="ValidateSalonTypeStaff" Display="Dynamic" 
                                    ControlToValidate="TypeStaff"
                                    runat="server"  Text="Bạn chưa chọn kiểu nhân viên!" 
                                    ErrorMessage="Vui lòng chọn kiểu nhân viên!"
                                    ForeColor="Red"
                                    CssClass="fb-cover-error">
                                </asp:RequiredFieldValidator>--%>
                            </span>
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="col-xs-3 left"><span>Tên tài khoản</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="FullName" runat="server" ClientIDMode="Static" CssClass="perm-input-field"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="FullNameValidate" ControlToValidate="FullName" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập họ tên!"></asp:RequiredFieldValidator>
                            </span>
                                            
                        </td>
                    </tr>
                    
                    <tr>
                        <td class="col-xs-3 left"><span style="line-height: 20px;">Email (Tên đăng nhập)</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="Email" runat="server" ClientIDMode="Static" placeholder="example@gmail.com" CssClass="perm-input-field"></asp:TextBox>
                            </span>
                        </td>
                    </tr>
                    
                    <tr class="tr-product-category">
                        <td class="col-xs-3 left"><span>Publish</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:CheckBox ID="Publish" Checked="true" runat="server" />
                            </span>
                        </td>
                    </tr>

                    <tr class="tr-send">
                        <td class="col-xs-3 left"></td>
                        <td class="col-xs-9 right no-border">
                            <span class="field-wp">
                                <asp:Button ID="Send" CssClass="btn-send perm-btn-field" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="Update_OBJ"></asp:Button>
                                <span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;">Đóng</span>
                            </span>
                        </td>
                    </tr>
                    <asp:HiddenField runat="server" ID="UrlKeyField"  ClientIDMode="Static"/>
                    <asp:HiddenField runat="server" ID="ImgCoverField"  ClientIDMode="Static"/>
                    <asp:HiddenField ID="HDF_MainImg" runat="server" ClientIDMode="Static"/>
                </tbody>
            </table>
        </div>
    </div>
    <%-- end Add --%>

    <!-- Staff Information -->
    <% if(OBJ != null){ %>
    <div class="wp960 content-wp content-customer-history">
        <div class="container">
            <div class="row">
                <!-- Common Information -->
                <div class="col-xs-12 line-head">
                    <strong class="st-head" style="margin-top: 0px;">
                        <i class="fa fa-file-text"></i>
                        Thông tin cơ bản
                        <span class="view-more-infor">(Xem thông tin đầy đủ)</span>
                    </strong>
                </div>
                <div class="col-xs-12 common-infor">
                    <p>
                        Họ tên&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<%=OBJ.Fullname %>
                    </p>
                    <p>
                        Số ĐT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<%=OBJ.Phone %>
                    </p>
                    <p>
                        Bộ phận&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<%=OBJ.TypeName %> ( <%=OBJ.LevelName %> )
                    </p>
                    <p>
                        Salon&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<%=OBJ.SalonName %>
                    </p>
                </div>
                <!--/ Common Information -->

            </div>
        </div>
    </div>    
    <!--/ Staff Information -->

    <div class="wp960 content-wp">
        <div class="row">
            <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                ClientIDMode="Static" runat="server"></asp:TextBox>

            <strong class="st-head" style="margin-left: 10px;">
                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
            </strong>
            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeTo" placeholder="Đến ngày"
                ClientIDMode="Static" runat="server"></asp:TextBox>

            <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" ClientIDMode="Static" 
                onclick="excPaging(1,false,'all')" runat="server">Xem dữ liệu</asp:Panel>
            <a href="/admin/nhan-vien/<%=OBJ.Id %>.html" class="st-head btn-viewdata">Reset Filter</a>
            <%--<div class="export-wp drop-down-list-wp">
                <asp:Panel ID="fff" CssClass="st-head btn-viewdata"
                    ClientIDMode="Static" runat="server" onclick="EntryExportExcel('all')">Xuất File Excel</asp:Panel>
                <ul class="ul-drop-down-list" style="display:none;">
                    <li onclick="EntryExportExcel('all')">Tất cả dữ liệu</li>
                    <li onclick="EntryExportExcel('current page')">Trang hiện tại</li>
                </ul>                
            </div>--%>
        </div>
        <%--<asp:UpdatePanel ID="UPTotal" runat="server" ClientIDMode="Static">
            <ContentTemplate>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>Báo cáo tổng hợp</strong>
                </div>
                <div class="row">
                    <div class="table-wp">
                        <table class="table-add table-listing" style="width: 50%;">
                            <thead>
                                <tr>
                                    <th>Hạng mục</th>
                                    <th>Số lượng</th>
                                    <th>Doanh thu</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Tổng thu nhập</td>
                                    <td class="be-report-price"><div class="be-report-price">0</div></td>
                                    <td class="be-report-price">0</td>
                                </tr>
                                <tr>
                                    <td>Dịch vụ</td>
                                    <td class="be-report-price">0</td>
                                    <td class="be-report-price">0</td>
                                </tr>                                   
                                <tr>
                                    <td>Sản phẩm</td>
                                    <td class="be-report-price">0</td>
                                    <td class="be-report-price">0</td>
                                </tr>        
                            </tbody>
                        </table>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>--%>
        <div class="row">
            <strong class="st-head"><i class="fa fa-file-text"></i>Báo cáo dịch vụ</strong>
        </div>
        <!-- Row Table Filter -->
        <div class="table-func-panel" style="margin-top: -33px;">
            <div class="table-func-elm">
                <span>Số hàng / Page : </span>
                <div class="table-func-input-wp">
                    <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                    <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                    <ul class="ul-opt-segment">
                        <li data-value="10">10</li>
                        <li data-value="20">20</li>
                        <li data-value="30">30</li>
                        <li data-value="40">40</li>
                        <li data-value="50">50</li>
                        <li data-value="1000000">Tất cả</li>
                    </ul>
                    <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                </div>                
            </div>            
        </div>
        <!-- End Row Table Filter -->
        <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
            <ContentTemplate>
                <div class="row table-wp">
                    <table class="table-add table-listing">
                        <thead>
                            <tr>
                                <%=THeadService %>
                            </tr>
                        </thead>
                        <tbody>
                               <asp:Repeater runat="server" ID="Rpt_DataService">
                                   <HeaderTemplate><tr style="background: #DDDDDD;"><%=StrTotalService%></tr></HeaderTemplate>
                                   <ItemTemplate>
                                       <tr>
                                           <%# Eval("StrDataService") %>
                                       </tr>
                                   </ItemTemplate>
                               </asp:Repeater>                
                        </tbody>
                    </table>
                </div>
                <!-- Paging -->
                <div class="site-paging-wp">
                <% if(PAGING.TotalPage > 1){ %>
                    <asp:Panel CssClass="site-paging" ID="SitePaging1" runat="server" ClientIDMode="Static" data-page="0">
                        <% if (PAGING._Paging.Prev != 0)
                           { %>
                        <a href="javascript://" onclick="excPaging(1, false, 'service')">Đầu</a>
                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>, false, 'service')"><</a>
                        <% } %>
                        <asp:Repeater ID="RptPaging_Service" runat="server">
                            <ItemTemplate>
                                <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>, false, 'service')"
                                    <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>
                                >
                                    <%# Eval("PageNum") %>
                                </a>
                            </ItemTemplate>                    
                        </asp:Repeater>
                        <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                           { %>                
                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>, false, 'service')">></a>
                        <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>, false, 'service')">Cuối</a>
                        <% } %>
                    </asp:Panel>
                <% } %>
                </div>
                <!-- End Paging -->                
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>

        <div class="row">
            <strong class="st-head" style="margin-top: -40px;"><i class="fa fa-file-text"></i>Báo cáo sản phẩm</strong>
        </div>
        <!-- Row Table Filter --><!-- End Row Table Filter -->
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" ClientIDMode="Static">
            <ContentTemplate>
                <div class="row table-wp table-staff-profile">
                    <table class="table-add table-listing">
                        <thead>
                            <tr>
                                <%=THeadProduct %>
                            </tr>
                        </thead>
                        <tbody>                            
                            <asp:Repeater runat="server" ID="Rpt_DataProduct">
                                <HeaderTemplate><tr style="background: #DDDDDD;"><%=StrTotalProduct%></tr></HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <%# Eval("StrDataService") %>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>                 
                        </tbody>
                    </table>
                </div>
                <!-- Paging -->
                <div class="site-paging-wp">
                <% if(PAGING2.TotalPage > 1){ %>
                    <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                        <% if (PAGING2._Paging.Prev != 0)
                           { %>
                        <a href="javascript://" onclick="excPaging(1, false, 'product')">Đầu</a>
                        <a href="javascript://" onclick="excPaging(<%=PAGING2._Paging.Prev %>, false, 'product')"><</a>
                        <% } %>
                        <asp:Repeater ID="RptPaging_Product" runat="server">
                            <ItemTemplate>
                                <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>, false, 'product')"
                                    <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>
                                >
                                    <%# Eval("PageNum") %>
                                </a>
                            </ItemTemplate>                    
                        </asp:Repeater>
                        <% if (PAGING2._Paging.Next != PAGING2.TotalPage + 1)
                           { %>                
                        <a href="javascript://" onclick="excPaging(<%=PAGING2._Paging.Next %>, false, 'product')">></a>
                        <a href="javascript://" onclick="excPaging(<%=PAGING2.TotalPage %>, false, 'product')">Cuối</a>
                        <% } %>
                    </asp:Panel>
                <% } %>
                </div>
		        <!-- End Paging -->                
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>

        <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" style="display:none;" />
        <asp:HiddenField runat="server" ID="HDF_TypeData" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page_Service" />
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page_Product" />

        <!-- Hidden Field-->
        <asp:HiddenField ID="HDF_TypeStaff" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="HDF_Staff" ClientIDMode="Static" runat="server" />
        <!-- END Hidden Field-->
    </div>
    <%-- END Listing --%>
    <% } %>
</div>

<script>
    jQuery(document).ready(function () {
        $("#glbAdminStaff").addClass("active");
        $("#glbProfile").addClass("active");
        $("#subMenu .li-edit").addClass("active");

        // Show form customer information
        $(".view-more-infor").bind("click", function () {
            $("#StaffInfoWrap").show();
        });
        $("#CalcelAddFbcv").bind("click", function () {
            $("#StaffInfoWrap").hide();
        });

        var qs = getQueryStrings();
        showMsgSystem(qs["msg_update_message"], qs["msg_update_status"])
        if (qs["show_form_infor"] == "true") {
            $("#StaffInfoWrap").show();
        }

        //============================
        // Datepicker
        //============================
        $('.txtDateTime').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'vi',
            startDate: '2014/10/10',
            format: 'd/m/Y',
            dateonly: true,
            showHour: false,
            showMinute: false,
            timepicker: false,
            onChangeDateTime: function (dp, $input) { }
        });

        // Price format
        UP_FormatPrice('.be-report-price');

        // Fix avatar thumbnail
        excThumbWidth(120, 120);
    });

    function showDom(selector, status) {
        status = typeof status == "boolean" ? status : false;        
        if (status) {
            $(selector).show();
        }
    }

    function excPaging(page, first, typedata) {
        var first = typeof first == 'boolean' ? first : false;
        if (first) {
            $("#IsPaging").val("false");
        } else {
            $("#IsPaging").val("true");
        }
        $("#HDF_TypeData").val(typedata);
        if (typedata == "service") {
            $("#HDF_Page_Service").val(page);
        } else if (typedata == "product") {
            $("#HDF_Page_Product").val(page);
        } else if(typedata == "all") {
            $("#HDF_Page_Service").val(page);
        }
        
        $("#BtnFakeUP").click();
    }
</script>

<!-- Popup plugin image -->
<script type="text/javascript">
    function popupImageIframe(StoreImgField) {
        var Width = 960;
        var Height = 560;
        var ImgList = [];
        var iframe = $("<div class='iframe-image-upload' style='width:" + Width + "px; height:" + Height + "px; float:left; display:none;'><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + StoreImgField + "&callback=getThumbFromIframe&imgWidth=800&imgHeight=600&multi_img=false&Folder=NhanVien' style='width:100%; height: 100%;'></iframe></div");

        $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
    }

    window.getThumbFromIframe = function (Imgs, StoreImgField) {
        var imgs = "";
        var thumb = "";
        if (Imgs.length > 0) {
            thumb = '<div class="thumb-wp">' +
                        '<img class="thumb" alt="" title="" src="' + Imgs[0] + '"' +
                            'data-img="" />' +
                        '<span class="delete-thumb" onclick="deleteThumbnail($(this), \'' + StoreImgField + '\')"></span>' +
                    '</div>';
            $("." + StoreImgField).find(".listing-img-upload").empty().append(thumb);
            excThumbWidth(120, 120);
            $("#" + StoreImgField).val(Imgs[0]);
        }
        autoCloseEBPopup();
    }

    function deleteThumbnail(This, StoreImgField) {
        var imgs = "";
        var lst = This.parent().parent();
        This.parent().remove();
        lst.find("img.thumb").each(function () {
            imgs += $(this).attr("src") + ",";
        });
        $("#" + StoreImgField).val(imgs);
    }
</script>
<!-- Popup plugin image -->

</asp:Panel>
</asp:Content>
