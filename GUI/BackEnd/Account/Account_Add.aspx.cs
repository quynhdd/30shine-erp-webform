﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;

namespace _30shine.GUI.BackEnd.UIStaff
{
    public partial class Account_Add : System.Web.UI.Page
    {
        public UIHelpers _UIHelpers = new UIHelpers();
        private bool Perm_Access = false;
        private bool Perm_AllPermission = false;

        protected bool HasImages = false;
        protected List<string> ListImagesUrl = new List<string>();
        protected string[] ListImagesName;

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckPermission();

            if (!IsPostBack)
            {
                Bind_Permission();
                Bind_Salon();
            }
        }

        private void Bind_Permission()
        {
            PermissionList.DataTextField = "Permission";
            PermissionList.DataValueField = "Value";
            var key = 0;

            var item = new ListItem("Nhân viên", "0");
            PermissionList.Items.Insert(key++, item);
            if (Perm_Access)
            {
                item = new ListItem("Lễ tân", "1");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Check-in", "7");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Check-out", "8");
                PermissionList.Items.Insert(key++, item);
                // Hide Lễ tân
                PermissionList.Items.FindByValue("1").Enabled = false;
            }
            if (Perm_AllPermission)
            {
                item = new ListItem("Trưởng cửa hàng", "2");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Kế toán", "3");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Mode1", "4");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Admin", "5");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Khảo sát", "6");
                PermissionList.Items.Insert(key++, item);
            }
            PermissionList.SelectedIndex = 0;
        }

        private void Bind_Salon()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Salons = db.Tbl_Salon.Where(w => w.IsDelete != 1).OrderBy(o => o.Id).ToList();
                var Key = 0;
                var SalonId = Convert.ToInt32(Session["SalonId"]);

                Salon.DataTextField = "Salon";
                Salon.DataValueField = "Id";

                if (Perm_AllPermission)
                {
                    ListItem item = new ListItem("Chọn Salon", "0");
                    Salon.Items.Insert(0, item);

                    foreach (var v in _Salons)
                    {
                        Key++;
                        item = new ListItem(v.Name, v.Id.ToString());
                        Salon.Items.Insert(Key, item);
                    }
                    Salon.SelectedIndex = 0;
                }
                else
                {
                    foreach (var v in _Salons)
                    {
                        if (v.Id == SalonId)
                        {
                            ListItem item = new ListItem(v.Name, v.Id.ToString());
                            Salon.Items.Insert(Key, item);
                            Salon.SelectedIndex = SalonId;
                            Salon.Enabled = false;
                        }

                    }
                }
            }
        }

        protected void AddStaff(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var obj = new _30shine.MODEL.ENTITY.EDMX.Staff();
                int integer;

                obj.Fullname = FullName.Text;
                obj.Email = Email.Text;
                obj.Avatar = HDF_MainImg.Value;
                obj.Publish = Publish.Checked;
                obj.CreatedDate = DateTime.Now;
                if (!Perm_AllPermission)
                {
                    obj.SalonId = Convert.ToInt32(Session["SalonId"]);
                }
                else
                {
                    obj.SalonId = Convert.ToInt32(Salon.SelectedValue);
                }
                obj.Password = "a05b49b7dc71773bc7ff4ef877824ffe";
                obj.IsDelete = 0;

                // Check set permission
                int Error = 0;
                int ErrorPerm = 0;
                string[] PermLST = new string[] { "staff", "reception", "salonmanager", "accountant", "mode1", "admin", "servey", "checkin", "checkout" };
                int index = PermissionList.SelectedValue != "" ? Convert.ToInt32(PermissionList.SelectedValue) : 0;
                if (index > PermLST.Length || (!Perm_AllPermission && index > 1))
                {
                    Error++;
                    ErrorPerm++;
                }
                else
                {
                    obj.Permission = PermLST[index];
                }

                if (ErrorPerm > 0)
                {
                    var msg = "Giá trị phân quyền không đúng.";
                    var status = "msg-system warning";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + msg + "','" + status + "')", true);
                }

                // Validate
                // Check trùng email
                if (obj.Email != "")
                {
                    var ExistEmail = db.Staffs.Count(w => w.Email == obj.Email);
                    if (ExistEmail > 0)
                    {
                        var msg = "Email đã tồn tại. Bạn vui lòng nhập email khác.";
                        var status = "msg-system warning";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + msg + "','" + status + "')", true);
                        Error++;
                    }
                }

                if (Error == 0)
                {
                    db.Staffs.Add(obj);
                    db.SaveChanges();

                    var msg = "Cập nhật thành công!";
                    var status = "msg-system success";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + msg + "','" + status + "')", true);
                }
            }
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        { 
            if (Session["User_Id"] == null)
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin"};
                string[] AllowAllPermission = new string[] { "root", "admin" };
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }
                if (Array.IndexOf(AllowAllPermission, Permission) != -1)
                {
                    Perm_AllPermission = true;
                }
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
            if (!Perm_AllPermission)
            {
                TrSalon.Visible = false;
            }
        }
    }
}