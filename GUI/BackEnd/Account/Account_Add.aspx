﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Account_Add.aspx.cs" Inherits="_30shine.GUI.BackEnd.UIStaff.Account_Add" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="StaffAdd" ContentPlaceHolderID="CtMain" runat="server">
<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">
                <li>Quản lý Tài khoản &nbsp;&#187; </li>
                <li class="li-listing"><a href="/admin/account/danh-sach.html">Danh sách</a></li>
                <li class="li-add active"><a href="/admin/account/them-moi.html">Thêm mới</a></li>
                <%--<li class="li-listing li-roll"><a href="/admin/nhan-vien/diem-danh.html">Điểm danh</a></li>--%>
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add">
    <%-- Add --%>                
    <div class="wp960 content-wp">
        <!-- System Message -->
        <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
        <!-- END System Message -->

        <div class="table-wp">
            <table class="table-add">
                <tbody>
                    <tr class="title-head">
                        <td><strong>Thông tin tài khoản</strong></td>
                        <td></td>
                    </tr>
                    <tr class="tr-margin" style="height: 20px;"></tr>
                    <tr runat="server" id="TrSalon">
                        <td class="col-xs-3 left"><span>Salon</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:DropDownList ID="Salon" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <%--<asp:RequiredFieldValidator InitialValue="0" 
                                    ID="ValidateSalon" Display="Dynamic" 
                                    ControlToValidate="Salon"
                                    runat="server"  Text="Bạn chưa chọn Salon!" 
                                    ErrorMessage="Vui lòng chọn Salon!"
                                    ForeColor="Red"
                                    CssClass="fb-cover-error">
                                </asp:RequiredFieldValidator>--%>
                            </span>
                        </td>
                    </tr>

                    <tr>
                        <td class="col-xs-3 left"><span>Phân quyền</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:DropDownList ID="PermissionList" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <%--<asp:RequiredFieldValidator InitialValue="0" 
                                    ID="ValidateSalonTypeStaff" Display="Dynamic" 
                                    ControlToValidate="TypeStaff"
                                    runat="server"  Text="Bạn chưa chọn kiểu nhân viên!" 
                                    ErrorMessage="Vui lòng chọn kiểu nhân viên!"
                                    ForeColor="Red"
                                    CssClass="fb-cover-error">
                                </asp:RequiredFieldValidator>--%>
                            </span>
                        </td>
                    </tr>

                    <tr>
                        <td class="col-xs-3 left"><span>Tên tài khoản</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="FullName" runat="server" ClientIDMode="Static"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="FullNameValidate" ControlToValidate="FullName" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập họ tên!"></asp:RequiredFieldValidator>
                            </span>
                                            
                        </td>
                    </tr>

                    <tr>
                        <td class="col-xs-3 left"><span>Email (Tên đăng nhập)</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="Email" runat="server" ClientIDMode="Static" placeholder="example@gmail.com"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="EmailValidate" ControlToValidate="Email" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập email(hoặc username)!"></asp:RequiredFieldValidator>
                            </span>
                        </td>
                    </tr>

                    <tr class="tr-product-category">
                        <td class="col-xs-3 left"><span>Publish</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:CheckBox ID="Publish" Checked="true" runat="server" />
                            </span>
                        </td>
                    </tr>
                    
                    <tr class="tr-send">
                        <td class="col-xs-3 left"></td>
                        <td class="col-xs-9 right no-border">
                            <span class="field-wp">
                                <asp:Button ID="Send" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="AddStaff"></asp:Button>
                                <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>
                            </span>
                        </td>
                    </tr>
                    <asp:HiddenField runat="server" ID="UrlKeyField"  ClientIDMode="Static"/>
                    <asp:HiddenField runat="server" ID="ImgCoverField"  ClientIDMode="Static"/>
                    <asp:HiddenField ID="HDF_MainImg" runat="server" ClientIDMode="Static"/>
                </tbody>
            </table>
        </div>
    </div>
    <%-- end Add --%>
</div>

<script>
    jQuery(document).ready(function () {
        $("#glbAdminStaff").addClass("active");

        //============================
        // Datepicker
        //============================
        $('.txtDateTime').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'vi',
            startDate: '2014/10/10',
            format: 'd/m/Y',
            dateonly: true,
            showHour: false,
            showMinute: false,
            timepicker: false,
            onChangeDateTime: function (dp, $input) {
                //$input.val($input.val().split(' ')[0]);
            }
        });
    });
</script>

<!-- Popup plugin image -->
<script type="text/javascript">
    function popupImageIframe(StoreImgField) {
        var Width = 960;
        var Height = 560;
        var ImgList = [];
        var iframe = $("<div class='iframe-image-upload' style='width:" + Width + "px; height:" + Height + "px; float:left; display:none;'><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + StoreImgField + "&callback=getThumbFromIframe&imgWidth=800&imgHeight=600&multi_img=false&Folder=NhanVien' style='width:100%; height: 100%;'></iframe></div");

        $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
    }

    window.getThumbFromIframe = function (Imgs, StoreImgField) {
        var imgs = "";
        var thumb = "";
        if (Imgs.length > 0) {
            thumb = '<div class="thumb-wp">' +
                        '<img class="thumb" alt="" title="" src="' + Imgs[0] + '"' +
                            'data-img="" />' +
                        '<span class="delete-thumb" onclick="deleteThumbnail($(this), \'' + StoreImgField + '\')"></span>' +
                    '</div>';
            $("." + StoreImgField).find(".listing-img-upload").empty().append(thumb);
            excThumbWidth(120, 120);
            $("#" + StoreImgField).val(Imgs[0]);
        }
        autoCloseEBPopup();
    }

    function deleteThumbnail(This, StoreImgField) {
        var imgs = "";
        var lst = This.parent().parent();
        This.parent().remove();
        lst.find("img.thumb").each(function () {
            imgs += $(this).attr("src") + ",";
        });
        $("#" + StoreImgField).val(imgs);
    }
</script>
<!-- Popup plugin image -->

</asp:Panel>
</asp:Content>
