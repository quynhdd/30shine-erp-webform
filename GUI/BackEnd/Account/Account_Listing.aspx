﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Account_Listing.aspx.cs" Inherits="_30shine.GUI.BackEnd.UIStaff.Account_Listing" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">
                <li>Quản lý Tài khoản &nbsp;&#187; </li>
                <li class="li-listing li-listing1"><a href="/admin/account/danh-sach.html">Danh sách</a></li>
                <% if(Perm_AllowAddNew){ %>
                    <li class="li-add"><a href="/admin/account/them-moi.html">Thêm mới</a></li>
                <% } %>
                <%--<li class="li-listing li-roll"><a href="/admin/nhan-vien/diem-danh.html">Điểm danh</a></li>--%>
                <%--<li class="li-listing li-payon"><a href="/admin/payon/them-moi.html">Cấu hình tiền công</a></li>--%>
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add customer-listing be-report">
    <%-- Listing --%>                
    <div class="wp960 content-wp">
        <!-- Filter -->        
        <div class="row row-filter">
            <strong class="st-head"><i class="fa fa-filter"></i>Lọc kết quả</strong>
            <div class="filter-item">
                <asp:DropDownList ID="Salon" runat="server" ClientIDMode="Static" style="margin: 12px 0; width: 190px;"></asp:DropDownList>
            </div>
            <div class="filter-item">
                <asp:DropDownList ID="StaffType" runat="server" ClientIDMode="Static" style="margin: 12px 0; width: 190px;"></asp:DropDownList>
            </div>   

            <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" ClientIDMode="Static" 
                onclick="excPaging(1)" runat="server">Xem dữ liệu</asp:Panel>
            <a href="/khach-hang/danh-sach.html" class="st-head btn-viewdata">Reset Filter</a>
        </div>
        <!-- End Filter -->
        <!-- Row Table Filter -->
        <div class="table-func-panel">
            <div class="table-func-elm">
                <span>Số hàng / Page : </span>
                <div class="table-func-input-wp">
                    <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                    <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                    <ul class="ul-opt-segment">
                        <li data-value="10">10</li>
                        <li data-value="20">20</li>
                        <li data-value="30">30</li>
                        <li data-value="40">40</li>
                        <li data-value="50">50</li>
                        <li data-value="1000000">Tất cả</li>
                    </ul>
                    <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                </div>                
            </div>            
        </div>
        <!-- End Row Table Filter -->
        <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
        <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
            <ContentTemplate>
                <div class="table-wp">
                    <table class="table-add table-listing">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Họ và tên</th>
                                <th>Mã Nhân Viên</th>
                                <th>Số điện thoại</th>
                                <th>Email</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="RptStaff" runat="server" OnItemDataBound="Repeater_ItemDataBound">
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                        <td>
                                            <a href="/admin/nhan-vien/<%# Eval("Id") %>.html"><%# Eval("Fullname") %></a>
                                        </td>
                                        <td><%# Eval("OrderCode") %></td>
                                        <td><%# Eval("Phone") %></td>
                                        <td class="map-edit">
                                            <%# Eval("Email") %>
                                            <div class="edit-wp">
                                                <asp:Panel CssClass="edit-action-wp action-edit" runat="server" ID="EAedit" Visible="false">
                                                    <a class="elm edit-btn" href="/admin/nhan-vien/<%# Eval("Id") %>.html" title="Sửa"></a>
                                                </asp:Panel>
                                                <asp:Panel CssClass="edit-action-wp action-delete" runat="server" ID="EAdelete" Visible="false">
                                                    <a class="elm del-btn" onclick="del(this.parentNode.parentNode.parentNode.parentNode,'<%# Eval("Id") %>', '<%# Eval("Fullname") %>')" href="javascript://" title="Xóa"></a>   
                                                </asp:Panel>
                                            </div>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>                    
                        </tbody>
                    </table>
                </div>

                <!-- Paging -->
                <div class="site-paging-wp">
                <% if(PAGING.TotalPage > 1){ %>
                    <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                        <% if (PAGING._Paging.Prev != 0)
                           { %>
                        <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                        <% } %>
                        <asp:Repeater ID="RptPaging" runat="server">
                            <ItemTemplate>
                                <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                    <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>
                                >
                                    <%# Eval("PageNum") %>
                                </a>
                            </ItemTemplate>                    
                        </asp:Repeater>
                        <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                           { %>                
                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                        <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                        <% } %>
                    </asp:Panel>
                <% } %>
                </div>
		        <!-- End Paging -->                
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" style="display:none;" />
        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
        <!-- Hidden Field-->
        <asp:HiddenField ID="HDF_Suggestion_Code" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="HDF_Suggestion_Field" ClientIDMode="Static" runat="server" />
        <!-- END Hidden Field-->
    </div>    
    <%-- END Listing --%>
</div>

<script>
    jQuery(document).ready(function () {
        // Add active menu
        $("#glbAdminStaff").addClass("active");
        $("#subMenu .li-listing1").addClass("active");
    });

    //============================
    // Event delete
    //============================
    function del(This, code, name) {
        var code = code || null,
            name = name || null,
            Row = This;
        if (!code) return false;

        // show EBPopup
        $(".confirm-yn").openEBPopup();
        $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

        $("#EBPopup .yn-yes").bind("click", function () {
            $.ajax({
                type: "POST",
                url: "/GUI/SystemService/Ajax/Del.aspx/Delele_Staff",
                data: '{Code : "' + code + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    var mission = JSON.parse(response.d);
                    if (mission.success) {
                        delSuccess();
                        Row.remove();
                    } else {
                        delFailed();
                    }
                },
                failure: function (response) { alert(response.d); }
            });
        });
        $("#EBPopup .yn-no").bind("click", function () {
            autoCloseEBPopup(0);
        });
    }
</script>

</asp:Panel>
</asp:Content>