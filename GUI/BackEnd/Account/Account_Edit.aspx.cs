﻿using _30shine.Helpers;
using _30shine.MODEL.ENTITY.EDMX;
using LinqKit;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.UIStaff
{
    public partial class Account_Edit : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        protected Paging PAGING2 = new Paging();
        protected StaffProfile OBJ = new StaffProfile();
        protected Staff_TimeKeeping Total_TimeKeeping = new Staff_TimeKeeping();
        private int _TypeStaff;
        private int _StaffId;
        protected bool Staff_All = false;
        protected bool Staff_Hairdresser = false;
        protected bool Staff_HairMassage = false;
        protected bool Staff_Singer = false;
        protected string THeadService = "";
        protected string THeadProduct = "";
        protected string StrTotalService = "<td>Tổng số</td>";
        protected string StrTotalProduct = "<td>Tổng số</td>";
        private DateTime MinTime = new DateTime();
        private DateTime MaxTime = new DateTime();
        private List<Service> LST_Service = new List<Service>();
        private List<Product> LST_Product = new List<Product>();
        private Expression<Func<BillService, bool>> WhereBill = PredicateBuilder.True<BillService>();
        private int Code;

        protected bool HasImages = false;
        protected List<string> ListImagesUrl = new List<string>();
        protected string[] ListImagesName;

        private bool Perm_Access = false;
        private bool Perm_Edit = false;
        private bool Perm_AllPermission = false;
        private bool Perm_HidePermission = false;
        private bool Perm_ActionMenu = false;
        private bool Perm_TypeStaff = false;
        string[] LSTPerm = new string[] { "root", "admin", "salonmanager", "reception", "staff", "checkin", "checkout" };

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckPermission();

            GenWhere();
            Bind_DataHead();
            if (!IsPostBack)
            {
                if (Bind_OBJ())
                {
                    Bind_Permission();
                    Bind_Salon();
                }
            }
        }

        /// <summary>
        /// Bind data for service and product
        /// </summary>
        private void GenWhere()
        {
            Code = Int32.TryParse(Request.QueryString["Code"], out Code) ? Code : 0;
            // Time condition
            CultureInfo culture = new CultureInfo("vi-VN");
            if (TxtDateTimeFrom.Text != "")
            {
                DateTime _FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                DateTime _ToDate;
                if (TxtDateTimeTo.Text != "")
                {
                    _ToDate = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                }
                else
                {
                    _ToDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(1);
                }
                WhereBill = WhereBill.And(w => w.CreatedDate > _FromDate && w.CreatedDate < _ToDate);
            }
            else
            {
                DateTime _FromDate = Convert.ToDateTime("1/8/2015", culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                WhereBill = WhereBill.And(w => w.CreatedDate > _FromDate);
            }
            //WhereBill = WhereBill.And(w => w.Staff_Hairdresser_Id == Code || w.Staff_HairMassage_Id == Code || w.SellerId == Code);
            WhereBill = WhereBill.And(w => w.IsDelete != 1);

            if (IsPostBack)
            {
                if (TxtDateTimeFrom.Text != "")
                    MinTime = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                if (TxtDateTimeTo.Text != "")
                    MaxTime = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
            }
        }

        public string Get_Services()
        {
            using (var db = new Solution_30shineEntities())
            {
                var str = "";
                LST_Service = db.Services.Where(w => w.IsDelete != 1 && w.Publish == 1).OrderByDescending(w => w.Id).ToList();
                if (LST_Service.Count > 0)
                {
                    str += "<th class='th-first'>Dịch vụ</th>";
                    foreach (var v in LST_Service)
                    {
                        str += "<th>" + v.Name + "</th>";
                    }
                }
                return str;
            }
        }

        public string Get_Products()
        {
            using (var db = new Solution_30shineEntities())
            {
                var str = "";
                LST_Product = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1).OrderByDescending(w => w.Id).ToList();
                if (LST_Product.Count > 0)
                {
                    str += "<th class='th-first'>Sản phẩm</th>";
                    foreach (var v in LST_Product)
                    {
                        str += "<th>" + v.Name + "</th>";
                    }
                }
                return str;
            }
        }

        private void Bind_DataHead()
        {
            THeadService = Get_Services();
            THeadProduct = Get_Products();
        }

        public void Bind_Data_Service()
        {
            using (var db = new Solution_30shineEntities())
            {
                var index = -1;
                var Daily_Service_TimeKeeping = new List<Staff_TimeKeeping>();
                var Daily_Service_TimeKeeping2 = new List<Staff_TimeKeeping>();
                var Single_TimeKeeping = new Staff_TimeKeeping();
                var service = new ServiceStatistic();
                var _ServiceTmp = new ServiceStatistic();
                var _TotalTmp = new ServiceStatistic();
                var lstServiceStatistic = new List<ServiceStatistic>();
                var _TotalServiceStatistic = new List<ServiceStatistic>();
                var strservice = "";
                var TimeFrom = MinTime.AddDays(PAGING._Offset);
                var TimeTo = TimeFrom.AddDays(PAGING._Segment);
                var WhereBillService = WhereBill.And(w => w.CreatedDate >= TimeFrom && w.CreatedDate <= TimeTo && (w.Staff_Hairdresser_Id == Code || w.Staff_HairMassage_Id == Code));

                // Print Service
                var Bills = db.BillServices.AsExpandable().Where(WhereBillService).OrderBy(w => w.CreatedDate).ToList();
                var Services = Bills.Join(db.FlowServices,
                                       a => a.Id,
                                       b => b.BillId,
                                       (a, b) => new
                                       {
                                           a.Id,
                                           b.CreatedDate,
                                           b.ServiceId
                                       }
                                   )
                                   .GroupBy(g => String.Format("{0:dd/MM/yyy}", g.CreatedDate))
                                   .Select(s => new
                                   {
                                       Id = s.First().Id,
                                       CreatedDate = s.First().CreatedDate,
                                       ServiceIds = s.Select(ss => new { ss.ServiceId }).ToList()
                                   }).ToList();
                if (Services.Count > 0)
                {
                    foreach (var v in Services)
                    {
                        service = new ServiceStatistic();
                        _ServiceTmp = new ServiceStatistic();
                        lstServiceStatistic = new List<ServiceStatistic>();
                        strservice = "";

                        // Classify service (Phân loại dịch vụ)
                        if (v.ServiceIds.Count > 0)
                        {
                            foreach (var v2 in v.ServiceIds)
                            {
                                service = new ServiceStatistic();
                                _TotalTmp = new ServiceStatistic();
                                index = lstServiceStatistic.FindIndex(fi => fi.ServiceId == v2.ServiceId);
                                if (index == -1)
                                {
                                    service.ServiceId = Convert.ToInt32(v2.ServiceId);
                                    service.ServiceTimes = 1;
                                    lstServiceStatistic.Add(service);
                                    _TotalServiceStatistic.Add(service);
                                }
                                else
                                {
                                    _ServiceTmp = new ServiceStatistic();
                                    service = lstServiceStatistic.Find(f => f.ServiceId == v2.ServiceId);
                                    _ServiceTmp.ServiceId = service.ServiceId;
                                    _ServiceTmp.ServiceTimes = service.ServiceTimes + 1;
                                    lstServiceStatistic[index] = _ServiceTmp;
                                    _TotalServiceStatistic[index] = _ServiceTmp;
                                }
                            }
                            if (LST_Service.Count > 0)
                            {
                                strservice += "<td>" + String.Format("{0:dd/MM/yyyy}", v.CreatedDate) + "</td>";
                                foreach (var v3 in LST_Service)
                                {
                                    index = lstServiceStatistic.FindIndex(fi => fi.ServiceId == v3.Id);
                                    if (index == -1)
                                    {
                                        strservice += "<td>" + "-" + "</td>";
                                    }
                                    else
                                    {
                                        strservice += "<td>" + lstServiceStatistic[index].ServiceTimes + "</td>";
                                    }
                                }
                            }
                            Single_TimeKeeping.Id = v.Id;
                            Single_TimeKeeping._ServiceStatistic = lstServiceStatistic;
                            Single_TimeKeeping.TotalService = v.ServiceIds.Count;
                            Single_TimeKeeping.StrDataService = strservice;
                            Single_TimeKeeping.CreatedDate = Convert.ToDateTime(v.CreatedDate);

                            Daily_Service_TimeKeeping.Add(Single_TimeKeeping);
                        }
                    }
                }

                // Merge staff
                var strDefault = "";
                var loop = 0;
                if (LST_Service.Count > 0)
                {
                    foreach (var v3 in LST_Service)
                    {
                        strDefault += "<td>-</td>";
                    }
                }
                for (var i = 0; i < PAGING._Segment; i++)
                {
                    if (TimeFrom.AddDays(1) > MaxTime)
                    {
                        break;
                    }
                    index = Daily_Service_TimeKeeping.FindIndex(fi => String.Format("{0:dd/MM/yyyy}", fi.CreatedDate) == String.Format("{0:dd/MM/yyyy}", TimeFrom));
                    if (index == -1)
                    {
                        Single_TimeKeeping = new Staff_TimeKeeping();
                        //Single_TimeKeeping.Id = v.Id;
                        Single_TimeKeeping.TotalService = 0;
                        Single_TimeKeeping.StrDataService = "<td>" + String.Format("{0:dd/MM/yyyy}", TimeFrom) + "</td>" + strDefault;
                        Daily_Service_TimeKeeping2.Add(Single_TimeKeeping);
                    }
                    else
                    {
                        Daily_Service_TimeKeeping2.Add(Daily_Service_TimeKeeping[loop++]);
                    }
                    TimeFrom = TimeFrom.AddDays(1);
                }
                Rpt_DataService.DataSource = Daily_Service_TimeKeeping2;
                Rpt_DataService.DataBind();
            }
        }

        public void Bind_AllData_Service()
        {
            using (var db = new Solution_30shineEntities())
            {
                var index = -1;
                var service = new ServiceStatistic();
                var _ServiceTmp = new ServiceStatistic();
                var lstServiceStatistic = new List<ServiceStatistic>();
                var _TotalServiceStatistic = new List<ServiceStatistic>();
                var WhereBillService = WhereBill.And(w => w.Staff_Hairdresser_Id == Code || w.Staff_HairMassage_Id == Code);
                var Bills = db.BillServices.AsExpandable().Where(WhereBillService).ToList();
                // Print Service
                var Services = Bills.Join(db.FlowServices,
                                       a => a.Id,
                                       b => b.BillId,
                                       (a, b) => new
                                       {
                                           a.Id,
                                           b.CreatedDate,
                                           b.ServiceId
                                       }
                                   )
                                   .GroupBy(g => String.Format("{0:dd/MM/yyy}", g.CreatedDate))
                                   .Select(s => new
                                   {
                                       Id = s.First().Id,
                                       CreatedDate = s.First().CreatedDate,
                                       ServiceIds = s.Select(ss => new { ss.ServiceId }).ToList()
                                   }).ToList();
                if (Services.Count > 0)
                {
                    foreach (var v in Services)
                    {
                        service = new ServiceStatistic();
                        _ServiceTmp = new ServiceStatistic();
                        lstServiceStatistic = new List<ServiceStatistic>();

                        // Classify service (Phân loại dịch vụ)
                        if (v.ServiceIds.Count > 0)
                        {
                            foreach (var v2 in v.ServiceIds)
                            {
                                service = new ServiceStatistic();
                                index = _TotalServiceStatistic.FindIndex(fi => fi.ServiceId == v2.ServiceId);
                                if (index == -1)
                                {
                                    service.ServiceId = Convert.ToInt32(v2.ServiceId);
                                    service.ServiceTimes = 1;
                                    _TotalServiceStatistic.Add(service);
                                }
                                else
                                {
                                    _ServiceTmp = new ServiceStatistic();
                                    service = _TotalServiceStatistic.Find(f => f.ServiceId == v2.ServiceId);
                                    _ServiceTmp.ServiceId = service.ServiceId;
                                    _ServiceTmp.ServiceTimes = service.ServiceTimes + 1;
                                    _TotalServiceStatistic[index] = _ServiceTmp;
                                }
                            }
                        }
                    }
                }

                // Merge staff
                if (LST_Service.Count > 0)
                {
                    foreach (var v3 in LST_Service)
                    {
                        index = _TotalServiceStatistic.FindIndex(fi => fi.ServiceId == v3.Id);
                        if (index == -1)
                        {
                            StrTotalService += "<td>" + "-" + "</td>";
                        }
                        else
                        {
                            StrTotalService += "<td>" + _TotalServiceStatistic[index].ServiceTimes + "</td>";
                        }
                    }
                }
            }
        }

        public void Bind_Data_Product()
        {
            using (var db = new Solution_30shineEntities())
            {
                var index = -1;
                var Daily_Product_TimeKeeping = new List<Staff_TimeKeeping>();
                var Daily_Product_TimeKeeping2 = new List<Staff_TimeKeeping>();
                var Single_TimeKeeping = new Staff_TimeKeeping();
                var product = new ProductStatistic();
                var _ProductTmp = new ProductStatistic();
                var lstProductStatistic = new List<ProductStatistic>();
                var _TotalServiceProduct = new List<ProductStatistic>();
                var strproduct = "";
                var TimeFrom = MinTime.AddDays(PAGING2._Offset);
                var TimeTo = TimeFrom.AddDays(PAGING2._Segment);
                var WhereBillProduct = WhereBill.And(w => w.CreatedDate >= TimeFrom && w.CreatedDate <= TimeTo && w.SellerId == Code);

                var Bills = db.BillServices.AsExpandable().Where(WhereBillProduct).OrderBy(w => w.CreatedDate).ToList();
                // Print Service
                var Products = Bills.Join(db.FlowProducts,
                                       a => a.Id,
                                       b => b.BillId,
                                       (a, b) => new
                                       {
                                           a.Id,
                                           b.CreatedDate,
                                           b.ProductId
                                       }
                                   )
                                   .GroupBy(g => String.Format("{0:dd/MM/yyy}", g.CreatedDate))
                                   .Select(s => new
                                   {
                                       Id = s.First().Id,
                                       CreatedDate = s.First().CreatedDate,
                                       ProductIds = s.Select(ss => new { ss.ProductId }).ToList()
                                   }).ToList();
                if (Products.Count > 0)
                {
                    foreach (var v in Products)
                    {
                        product = new ProductStatistic();
                        _ProductTmp = new ProductStatistic();
                        lstProductStatistic = new List<ProductStatistic>();
                        strproduct = "";

                        // Classify service (Phân loại dịch vụ)
                        if (v.ProductIds.Count > 0)
                        {
                            foreach (var v2 in v.ProductIds)
                            {
                                product = new ProductStatistic();
                                index = lstProductStatistic.FindIndex(fi => fi.ProductId == v2.ProductId);
                                if (index == -1)
                                {
                                    product.ProductId = Convert.ToInt32(v2.ProductId);
                                    product.ProductTimes = 1;
                                    lstProductStatistic.Add(product);
                                    _TotalServiceProduct.Add(product);
                                }
                                else
                                {
                                    _ProductTmp = new ProductStatistic();
                                    product = lstProductStatistic.Find(f => f.ProductId == v2.ProductId);
                                    _ProductTmp.ProductId = product.ProductId;
                                    _ProductTmp.ProductTimes = product.ProductTimes + 1;
                                    lstProductStatistic[index] = _ProductTmp;
                                    _TotalServiceProduct[index] = _ProductTmp;
                                }
                            }
                            if (LST_Product.Count > 0)
                            {
                                strproduct += "<td>" + String.Format("{0:dd/MM/yyyy}", v.CreatedDate) + "</td>";
                                foreach (var v3 in LST_Product)
                                {
                                    index = lstProductStatistic.FindIndex(fi => fi.ProductId == v3.Id);
                                    if (index == -1)
                                    {
                                        strproduct += "<td>" + "-" + "</td>";
                                    }
                                    else
                                    {
                                        strproduct += "<td>" + lstProductStatistic[index].ProductTimes + "</td>";
                                    }
                                }
                            }
                            Single_TimeKeeping.Id = v.Id;
                            Single_TimeKeeping._ProductStatistic = lstProductStatistic;
                            Single_TimeKeeping.TotalService = v.ProductIds.Count;
                            Single_TimeKeeping.StrDataService = strproduct;
                            Single_TimeKeeping.CreatedDate = Convert.ToDateTime(v.CreatedDate);

                            Daily_Product_TimeKeeping.Add(Single_TimeKeeping);
                        }
                    }
                }

                // Merge staff
                var strDefault = "";
                if (LST_Product.Count > 0)
                {
                    foreach (var v3 in LST_Product)
                    {
                        strDefault += "<td>-</td>";
                    }
                }
                for (var i = 0; i < PAGING2._Segment; i++)
                {
                    if (TimeFrom.AddDays(1) > MaxTime)
                    {
                        break;
                    }
                    index = Daily_Product_TimeKeeping.FindIndex(fi => String.Format("{0:dd/MM/yyyy}", fi.CreatedDate) == String.Format("{0:dd/MM/yyyy}", TimeFrom));
                    if (index == -1)
                    {
                        Single_TimeKeeping = new Staff_TimeKeeping();
                        //Single_TimeKeeping.Id = Products[loop].Id;
                        Single_TimeKeeping.TotalService = 0;
                        Single_TimeKeeping.StrDataService = "<td>" + String.Format("{0:dd/MM/yyyy}", TimeFrom) + "</td>" + strDefault;
                        Daily_Product_TimeKeeping2.Add(Single_TimeKeeping);
                    }
                    else
                    {
                        Daily_Product_TimeKeeping2.Add(Daily_Product_TimeKeeping[index]);
                    }
                    TimeFrom = TimeFrom.AddDays(1);
                }

                Rpt_DataProduct.DataSource = Daily_Product_TimeKeeping2;
                Rpt_DataProduct.DataBind();
            }
        }

        public void Bind_AllData_Product()
        {
            using (var db = new Solution_30shineEntities())
            {
                var index = -1;
                var product = new ProductStatistic();
                var _ProductTmp = new ProductStatistic();
                var _TotalServiceProduct = new List<ProductStatistic>();
                var WhereBillProduct = WhereBill.And(w => w.SellerId == Code);
                var Bills = db.BillServices.AsExpandable().Where(WhereBillProduct).ToList();
                // Print Service
                var Products = Bills.Join(db.FlowProducts,
                                       a => a.Id,
                                       b => b.BillId,
                                       (a, b) => new
                                       {
                                           a.Id,
                                           b.CreatedDate,
                                           b.ProductId
                                       }
                                   )
                                   .GroupBy(g => String.Format("{0:dd/MM/yyy}", g.CreatedDate))
                                   .Select(s => new
                                   {
                                       Id = s.First().Id,
                                       CreatedDate = s.First().CreatedDate,
                                       ProductIds = s.Select(ss => new { ss.ProductId }).ToList()
                                   }).ToList();
                if (Products.Count > 0)
                {
                    foreach (var v in Products)
                    {
                        product = new ProductStatistic();
                        _ProductTmp = new ProductStatistic();

                        // Classify service (Phân loại dịch vụ)
                        if (v.ProductIds.Count > 0)
                        {
                            foreach (var v2 in v.ProductIds)
                            {
                                product = new ProductStatistic();
                                index = _TotalServiceProduct.FindIndex(fi => fi.ProductId == v2.ProductId);
                                if (index == -1)
                                {
                                    product.ProductId = Convert.ToInt32(v2.ProductId);
                                    product.ProductTimes = 1;
                                    _TotalServiceProduct.Add(product);
                                }
                                else
                                {
                                    _ProductTmp = new ProductStatistic();
                                    product = _TotalServiceProduct.Find(f => f.ProductId == v2.ProductId);
                                    _ProductTmp.ProductId = product.ProductId;
                                    _ProductTmp.ProductTimes = product.ProductTimes + 1;
                                    _TotalServiceProduct[index] = _ProductTmp;
                                }
                            }
                        }
                    }
                }

                // Merge staff
                if (Bills.Count > 0)
                {
                    if (LST_Product.Count > 0)
                    {
                        foreach (var v3 in LST_Product)
                        {
                            index = _TotalServiceProduct.FindIndex(fi => fi.ProductId == v3.Id);
                            if (index == -1)
                            {
                                StrTotalProduct += "<td>" + "-" + "</td>";
                            }
                            else
                            {
                                StrTotalProduct += "<td>" + _TotalServiceProduct[index].ProductTimes + "</td>";
                            }
                        }
                    }
                }
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            if (TxtDateTimeFrom.Text != "")
            {
                Bind_Paging();
                Bind_AllData_Service();
                Bind_AllData_Product();
                Bind_Data_Product();
                Bind_Data_Service();

                ScriptManager.RegisterStartupScript(this, GetType(), "Script", "SetActiveTableListing();ReplaceZezoValue();", true);
            }
        }

        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? (!HDF_Page_Service.Value.Equals("") ? Convert.ToInt32(HDF_Page_Service.Value) : 1) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            PAGING2._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING2._Segment;
            PAGING2._PageNumber = IsPostBack ? (!HDF_Page_Product.Value.Equals("") ? Convert.ToInt32(HDF_Page_Product.Value) : 1) : 1;
            PAGING2._PageNumber = PAGING2._PageNumber > 0 ? PAGING2._PageNumber : 1;
            PAGING2.TotalPage = !PAGING2.TotalPage.Equals(null) ? Get_TotalPage() : PAGING2.TotalPage;
            PAGING2._Offset = (PAGING2._PageNumber - 1) * PAGING2._Segment;
            PAGING2._Paging = PAGING2.Make_Paging();

            RptPaging_Service.DataSource = PAGING._Paging.ListPage;
            RptPaging_Service.DataBind();
            RptPaging_Product.DataSource = PAGING2._Paging.ListPage;
            RptPaging_Product.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                int ReturnTotalPage = (int)(MaxTime - MinTime).TotalDays / PAGING._Segment + 1;
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        private void Bind_Permission()
        {
            PermissionList.DataTextField = "Permission";
            PermissionList.DataValueField = "Value";
            var key = 0;

            var item = new ListItem("Nhân viên", "0");
            PermissionList.Items.Insert(key++, item);
            if (Perm_Access)
            {
                item = new ListItem("Lễ tân", "1");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Check-in", "7");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Check-out", "8");
                PermissionList.Items.Insert(key++, item);
                // Hide Lễ tân
                PermissionList.Items.FindByValue("1").Enabled = false;
            }
            if (Perm_AllPermission)
            {
                item = new ListItem("Trưởng cửa hàng", "2");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Kế toán", "3");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Mode1", "4");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Admin", "5");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Khảo sát", "6");
                PermissionList.Items.Insert(key++, item);
            }

            string[] PermLST = new string[] { "staff", "reception", "salonmanager", "accountant", "mode1", "admin", "servey", "checkin", "checkout" };
            int index = Array.IndexOf(PermLST, OBJ.Permission);
            if (index != -1)
            {
                var ItemSelected = PermissionList.Items.FindByValue(index.ToString());
                if (ItemSelected != null)
                {
                    ItemSelected.Selected = true;
                }
                else
                {
                    PermissionList.SelectedIndex = 0;
                }
            }
        }

        private void Bind_Salon()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Salons = db.Tbl_Salon.Where(w => w.IsDelete != 1).OrderBy(o => o.Id).ToList();
                var Key = 0;
                var SalonId = Convert.ToInt32(Session["SalonId"]);

                Salon.DataTextField = "Salon";
                Salon.DataValueField = "Id";

                if (Perm_AllPermission)
                {
                    ListItem item = new ListItem("Chọn Salon", "0");
                    Salon.Items.Insert(Key++, item);

                    if (_Salons.Count > 0)
                    {
                        foreach (var v in _Salons)
                        {
                            item = new ListItem(v.Name, v.Id.ToString());
                            Salon.Items.Insert(Key++, item);
                        }

                        var ItemSelected = Salon.Items.FindByValue(OBJ.SalonId.ToString());
                        if (ItemSelected != null)
                        {
                            ItemSelected.Selected = true;
                        }
                        else
                        {
                            Salon.SelectedIndex = 0;
                        }
                    }
                }
                else
                {
                    if (_Salons.Count > 0)
                    {
                        foreach (var v in _Salons)
                        {
                            if (v.Id == SalonId)
                            {
                                ListItem item = new ListItem(v.Name, v.Id.ToString());
                                Salon.Items.Insert(Key, item);
                                Salon.SelectedIndex = SalonId;
                                Salon.Enabled = false;
                            }

                        }
                    }
                }
            }
        }


        private bool Bind_OBJ()
        {
            var ExitsOBJ = true;
            using (var db = new Solution_30shineEntities())
            {
                var _OBJ = db.Staffs.FirstOrDefault(w => w.Id == Code);
                if (_OBJ != null)
                {
                    OBJ.Id = _OBJ.Id;
                    OBJ.Fullname = _OBJ.Fullname;
                    OBJ.StaffID = _OBJ.StaffID;
                    OBJ.DateJoin = _OBJ.DateJoin;
                    OBJ.SkillLevel = _OBJ.SkillLevel;
                    OBJ.Phone = _OBJ.Phone;
                    OBJ.Email = _OBJ.Email;
                    OBJ.Code = _OBJ.Code;
                    OBJ.Gender = _OBJ.Gender;
                    OBJ.SN_day = _OBJ.SN_day;
                    OBJ.SN_month = _OBJ.SN_month;
                    OBJ.SN_year = _OBJ.SN_year;
                    OBJ.Address = _OBJ.Address;
                    OBJ.SalonId = _OBJ.SalonId;
                    OBJ.Type = _OBJ.Type;
                    OBJ.CityId = _OBJ.CityId;
                    OBJ.DistrictId = _OBJ.DistrictId;
                    OBJ.Address = _OBJ.Address;
                    OBJ.Permission = _OBJ.Permission;
                    //OBJ.TypeName = _OBJ.Staff_Type != null ? _OBJ.Staff_Type.Name : "";
                    //OBJ.SalonName = _OBJ.Tbl_Salon != null ? _OBJ.Tbl_Salon.Name : "";
                    OBJ.Avatar = _OBJ.Avatar;
                    OBJ.SkinnerIdInGroup = _OBJ.SkinnerIdInGroup;
                    switch (OBJ.SkillLevel)
                    {
                        case 1: OBJ.LevelName = "Bậc 1";
                            break;
                        case 2: OBJ.LevelName = "Bậc 2";
                            break;
                        case 3: OBJ.LevelName = "Thử việc";
                            break;
                        case 4: OBJ.LevelName = "Học việc bậc 1";
                            break;
                        case 5: OBJ.LevelName = "Học việc bậc 2";
                            break;
                        default: OBJ.LevelName = "Đang cập nhật";
                            break;
                    }

                    FullName.Text = OBJ.Fullname;
                    Email.Text = OBJ.Email;
                    if (OBJ.Publish == true)
                    {
                        Publish.Checked = true;
                    }
                    else
                    {
                        Publish.Checked = false;
                    }
                }
                else
                {
                    ExitsOBJ = false;
                    var msg = "Lỗi! Nhân viên không tồn tại.";
                    var status = "msg-system warning";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + msg + "','" + status + "');showDom('#StaffInfoWrap', true);", true);
                }
                return ExitsOBJ;
            }
        }

        protected void Update_OBJ(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Code = Convert.ToInt32(Request.QueryString["Code"]);
                var obj = db.Staffs.Where(w => w.Id == _Code).FirstOrDefault();
                int Error = 0;
                int ErrorPerm = 0;
                int integer;

                if (!obj.Equals(null))
                {
                    obj.Fullname = FullName.Text;
                    obj.Email = Email.Text;

                    obj.ModifiedDate = DateTime.Now;
                    obj.SalonId = Convert.ToInt32(Salon.SelectedValue);

                    if (!Perm_HidePermission)
                    {
                        // Check set permission                        
                        string[] PermLST = new string[] { "staff", "reception", "salonmanager", "accountant", "mode1", "admin", "servey", "checkin", "checkout" };
                        int index = PermissionList.SelectedValue != "" ? Convert.ToInt32(PermissionList.SelectedValue) : 0;
                        if (index > PermLST.Length || (!Perm_AllPermission && index > 1))
                        {
                            Error++;
                            ErrorPerm++;
                        }
                        else
                        {
                            obj.Permission = PermLST[index].ToString();
                        }
                    }

                    // Check trùng email
                    if (obj.Email != "")
                    {
                        var ExistEmail = db.Staffs.Count(w => w.Id != obj.Id && w.Email == obj.Email);
                        if (ExistEmail > 0)
                        {
                            var msg = "Email đã tồn tại. Bạn vui lòng nhập email khác.";
                            var status = "msg-system warning";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + msg + "','" + status + "');showDom('#StaffInfoWrap', true);", true);
                            Error++;
                        }
                    }

                    if (ErrorPerm > 0)
                    {
                        var msg = "Giá trị phân quyền không đúng.";
                        var status = "msg-system warning";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + msg + "','" + status + "');showDom('#StaffInfoWrap', true);", true);
                    }

                    if (Error == 0)
                    {
                        db.Staffs.AddOrUpdate(obj);
                        var exc = db.SaveChanges();
                        if (exc > 0)
                        {
                            var MsgParam = new List<KeyValuePair<string, string>>();
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                            UIHelpers.Redirect("/admin/account/" + obj.Id + ".html", MsgParam);
                        }
                        else
                        {
                            var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                            var status = "msg-system warning";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + msg + "','" + status + "');showDom('#StaffInfoWrap', true);", true);
                        }
                    }
                }
                else
                {
                    MsgSystem.Text = "Lỗi! Nhân viên không tồn tại.";
                    MsgSystem.CssClass = "msg-system warning";
                }
            }
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["User_Id"] == null)
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "mode1", "salonmanager", "reception", "staff", "checkin", "checkout" };
                string[] AllowEdit = new string[] { "root", "admin", "mode1", };
                string[] AllowAllPermission = new string[] { "root", "admin", "mode1" };
                string[] AllowActionMenu = new string[] { "root", "admin", "salonmanager" };
                string[] AllowTypeStaff = new string[] { "root", "admin", "mode1", "salonmanager" };
                var Permission = Session["User_Permission"].ToString().Trim();
                var Code = Request.QueryString["Code"] != "" ? Convert.ToInt32(Request.QueryString["Code"]) : 0;
                int UserId = Convert.ToInt32(Session["User_Id"]);
                var UserSalonId = Convert.ToInt32(Session["SalonId"]);

                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }
                if (Array.IndexOf(AllowEdit, Permission) != -1)
                {
                    Perm_Edit = true;
                }
                if (Array.IndexOf(AllowAllPermission, Permission) != -1)
                {
                    Perm_AllPermission = true;
                }
                if (Array.IndexOf(AllowActionMenu, Permission) != -1)
                {
                    Perm_ActionMenu = true;
                }
                if (Array.IndexOf(AllowTypeStaff, Permission) != -1)
                {
                    Perm_TypeStaff = true;
                }
                if (Code == UserId)
                {
                    Perm_HidePermission = true;
                }

                using (var db = new Solution_30shineEntities())
                {
                    var obj = db.Staffs.FirstOrDefault(w => w.Id == Code);
                    if (obj != null && Array.IndexOf(Allow, obj.Permission) != -1)
                    {
                        /// Check quyền truy cập
                        /// Đối với quyền từ salonmanager trở lên, quyền cao hơn có thể truy cập xem quyền thấp hơn
                        /// Đối với quyền reception, staff, không được truy cập xem các nhân viên khác
                        if (Array.IndexOf(Allow, Session["User_Permission"]) <= Array.IndexOf(Allow, "salonmanager"))
                        {
                            if (Array.IndexOf(Allow, obj.Permission) <= Array.IndexOf(Allow, Session["User_Permission"]))
                            {
                                if (obj.Id != UserId)
                                {
                                    Perm_Access = false;
                                }
                            }
                            // Set quyền edit đối với nhân viên có quyền thấp hơn
                            if (Array.IndexOf(Allow, obj.Permission) >= Array.IndexOf(Allow, Session["User_Permission"]))
                            {
                                Perm_Edit = true;
                            }
                            // Từ chối truy cập xem nhân viên ở salon khác đối với quyền salonmanager
                            if (Array.IndexOf(Allow, Session["User_Permission"]) >= Array.IndexOf(Allow, "salonmanager") && obj.SalonId != UserSalonId)
                            {
                                Perm_Access = false;
                            }
                        }
                        else
                        {
                            if (obj.Id != UserId)
                            {
                                Perm_Access = false;
                            }
                        }
                    }
                }
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
            if (Perm_HidePermission)
            {
                TrPermission.Visible = false;
            }
            if (!Perm_AllPermission)
            {
                TrSalon.Disabled = true;
            }
            if (!Perm_ActionMenu)
            {
                ActionMenu.Visible = false;
            }

            if (!Perm_Edit)
            {
                var Inputs = UIHelpers.GetMarkedControls(this.Controls, "perm-input-field");
                if (Inputs.Count() > 0)
                {
                    foreach (var v in Inputs)
                    {
                        ((TextBox)(v)).Enabled = false;
                    }
                }

                var DropDownLists = UIHelpers.GetMarkedControls(this.Controls, "perm-ddl-field");
                if (DropDownLists.Count() > 0)
                {
                    foreach (var v in DropDownLists)
                    {
                        ((DropDownList)(v)).Enabled = false;
                    }
                }

                var Buttons = UIHelpers.GetMarkedControls(this.Controls, "perm-btn-field");
                if (Buttons.Count() > 0)
                {
                    foreach (var v in Buttons)
                    {
                        ((Button)(v)).Visible = false;
                    }
                }
            }
        }
    }
}