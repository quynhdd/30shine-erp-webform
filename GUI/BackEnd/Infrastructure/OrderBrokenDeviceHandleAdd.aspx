﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMasterVersion1.Master" AutoEventWireup="true" CodeBehind="OrderBrokenDeviceHandleAdd.aspx.cs" Inherits="_30shine.GUI.BackEnd.Infrastructure.OrderBrokenDeviceHandleAdd" %>

<asp:Content ID="ContentMain" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel ID="ContentWrap" runat="server" ClientIDMode="Static">
        <style>
            th, td {
                text-align: center;
            }

            body, html {
                height: 91%;
            }

            .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
                color: #fff;
                background-color: black;
            }

            .shadow-sm {
                margin-bottom: 15px;
                min-height: 680px;
            }

            .form-box-left {
                height: 31px;
                width: 147px;
            }

            .ul-box-left .li-box-left {
                margin-bottom: 8px;
                font-family: Roboto Condensed Regular !important;
                font-size: 14px !important;
            }

            .btn-group {
                margin-top: 21px !important;
            }

            .fa-times {
                font-size: 17px;
                color: red;
            }

                .fa-times:hover {
                    cursor: pointer;
                }

            .btn-outline-secondary {
                color: #000000;
                border-color: #000000;
            }

                .btn-outline-secondary:hover {
                    background: #000000;
                }

            .table td, .table th {
                vertical-align: middle;
            }

            .quantity-left {
                text-align: center;
            }

            .max-box {
                min-height: 550px;
                max-height: 700px;
                overflow-y: scroll;
            }

            .thead-dark {
                font-family: Roboto Condensed Bold;
            }

            .select-list-item {
                width: 100%;
                font-size: 14px;
                height: calc(2.25rem + 2px);
                line-height: 1.5;
                color: #495057;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #ced4da;
                border-radius: .25rem;
                transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
            }

                .select-list-item option {
                    font-size: 14px;
                }

            textarea.broken-note {
                height: 130px;
                width: 100%;
                direction: ltr;
                display: block;
                max-width: 100%;
                line-height: 1.5;
                padding: 15px 15px 30px;
                border-radius: 3px;
                border: 1px solid #F7E98D;
                font: 13px Tahoma, cursive;
                transition: box-shadow 0.5s ease;
                box-shadow: 0 4px 6px rgba(0,0,0,0.1);
                font-smoothing: subpixel-antialiased;
                background: linear-gradient(#F9EFAF, #F7E98D);
                background: -o-linear-gradient(#F9EFAF, #F7E98D);
                background: -ms-linear-gradient(#F9EFAF, #F7E98D);
                background: -moz-linear-gradient(#F9EFAF, #F7E98D);
                background: -webkit-linear-gradient(#F9EFAF, #F7E98D);
                resize: none;
            }

            .input-group {
                width: 70% !important;
                margin: 24px;
            }

            .eb-popup-wp {
                width: 100%;
                height: 100%;
                float: left;
                position: fixed;
                z-index: 100000;
                background: rgba(0,0,0,0.6);
                display: none;
            }

                .eb-popup-wp .eb-popup {
                    width: 100%;
                    float: left;
                    min-height: 20px;
                    background: white;
                    -webkit-box-shadow: 0px 0px 12px 3px rgba(0,0,0,0.7);
                    -moz-box-shadow: 0px 0px 12px 3px rgba(0,0,0,0.7);
                    box-shadow: 0px 0px 12px 3px rgba(0,0,0,0.7);
                }

                .eb-popup-wp .eb-popup-float {
                    margin: 0 auto;
                    position: relative;
                }

                .eb-popup-wp .closeBtn {
                    width: 9px;
                    height: 9px;
                    background: url(../images/icon-collection.png) no-repeat;
                    background-position: left 0 top 0px;
                    background-position-x: 0px;
                    background-position-y: 0;
                    position: absolute !important;
                    top: 6px;
                    right: /*-18px*/ 6px;
                    cursor: pointer;
                    z-index: 2000;
                }

                    .eb-popup-wp .closeBtn .child-wp {
                        width: 100%;
                        position: relative;
                        display: none;
                    }

                        .eb-popup-wp .closeBtn .child-wp .text {
                            padding: 5px 10px;
                            background: rgba(0,0,0,0.4);
                            color: #ffffff;
                            position: absolute;
                            top: 18px;
                            right: -7px;
                            width: 136px;
                            font-family: Roboto Condensed Light;
                        }

                        .eb-popup-wp .closeBtn .child-wp .triangle {
                            width: 0;
                            height: 0;
                            border-style: solid;
                            border-width: 0 5px 6px 5px;
                            border-color: transparent transparent rgba(0,0,0,1) transparent;
                            position: absolute;
                            top: 12px;
                            right: -1px;
                        }

                    .eb-popup-wp .closeBtn:hover .child-wp {
                        display: block;
                    }

            #EBPopupFloat, #EBConfirmYN, #EBPopupWP {
                display: none !important;
            }

            .iframe-image-upload {
                position: relative !important;
                margin: auto !important;
                margin-top: 8% !important;
            }

            .thumb:hover {
                cursor: zoom-in;
            }

            .btn-upload-img:hover {
                cursor: pointer;
            }

            .fa-trash-alt:hover {
                cursor: pointer;
            }

            .select-list-item {
                width: 100%;
                font-size: 14px;
                height: calc(2.25rem + 2px);
                line-height: 1.5;
                color: #495057;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #ced4da;
                border-radius: .25rem;
                transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
            }

                .select-list-item option {
                    font-size: 14px;
                }


                    .select-list-item option:nth-child(2) {
                        background-color: #e74c3c;
                        color: black;
                    }
        </style>
        <section>
            <div class="container-fluid sub-menu">
                <div class="row">
                    <div class="col-md-12 pt-3">
                        <ul>
                            <li><a href="#">THÊM MỚI YC XỬ LÝ CSVC&nbsp;&#187;</a></li>
                            <li><a href="/admin/xu-ly-su-co/thong-ke-su-co.html"><i class="fa fa-list-ul"></i>&nbsp THỐNG KÊ SỰ CỐ</a></li>
                            <li><a href="/admin/xu-ly-su-co/bao-cao-tien-do-hoan-thanh.html"><i class="fa fa-list-ul"></i>&nbsp TĐHT YÊU CẦU XỬ LÝ CSVC</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section style="margin-top: 10px;">
            <form runat="server">
                <div class="container-fluid sub-menu" style="min-width:900px !important;">
                    <div class="col-lg-12 float-left">
                        <div class="card mb-6 shadow-sm">
                            <div class="card-body">
                                <div class="d-flex justify-content-between align-items-center">
                                    <ul class="ul-box-left">
                                        <li class="li-box-left">
                                            <span>
                                                <span>Khu vực <span style="color: red">*</span></span>
                                                <asp:DropDownList runat="server" ID="ddlRegion" CssClass="form-control select form-box-left" ClientIDMode="Static" onchange="GetDropDownData($(this));"></asp:DropDownList>
                                            </span>
                                        </li>
                                        <li class="li-box-left">
                                            <span>
                                                <span>Salon <span style="color: red">*</span></span>
                                                <asp:DropDownList runat="server" ID="ddlSalon" CssClass="form-control select form-box-left" ClientIDMode="Static"></asp:DropDownList>
                                            </span>
                                        </li>
                                        <li class="li-box-left">
                                            <span>
                                                <span>Hạng mục <span style="color: red">*</span></span>
                                                <asp:DropDownList runat="server" ID="ddlCategory" CssClass="form-control select form-box-left" ClientIDMode="Static"></asp:DropDownList>
                                            </span>
                                        </li>
                                        <li class="li-box-left">
                                            <span>
                                                <span>Vị trí <span style="color: red">*</span></span>
                                                <asp:DropDownList runat="server" ID="ddlTeam" CssClass="form-control select form-box-left" ClientIDMode="Static"></asp:DropDownList>
                                            </span>
                                        </li>
                                        <li class="li-box-left">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-sm btn-outline-secondary" onclick="AddItemInList();"><i class="fas fa-plus-circle"></i>&nbsp;Thêm vào danh sách</button>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="row max-box">
                                    <table class="table table-bordered table-striped" style="width:100%;">
                                        <thead class="thead-dark">
                                            <tr>
                                                <th scope="col" width="2.5%">STT</th>
                                                <th scope="col" width="10%">Khu vực</th>
                                                <th scope="col" width="10%">Salon</th>
                                                <th scope="col" width="10%">Hạng mục</th>
                                                <th scope="col" width="10%">Vị trí</th>
                                                <th scope="col" width="10%">Ngày phát sinh</th>
                                                <th scope="col" width="15%">Ảnh sự cố</th>
                                                <th scope="col" width="10%">Diễn giải hỏng hóc</th>
                                                <th scope="col" width="10%">Mức độ ưu tiên</th>
                                                <th scope="col" width="10%">Thời hạn mong muốn</th>
                                                <th scope="col" width="2.5%"></th>
                                            </tr>
                                        </thead>
                                        <tbody id="body-item">
                                            <%--<tr>
                                                <th scope="row" class="td-resize">1</th>
                                                <td data-region="">Hà Nội</td>
                                                <td data-salon="">Khâm Thiên</td>
                                                <td data-category="">Hệ thống điện</td>
                                                <td data-team="">Đội 1</td>
                                                <td data-broken-date="">
                                                    <input type="text" class="datetime-picker form-control" placeholder="Ngày phát sinh..." />
                                                </td>
                                                <td data-broken-image="">
                                                    <div class="col-lg-12">
                                                        <div style="float: left; border: 1px dashed #8c8c8c; background-color: white; width: 102px; height: 82px; margin-right: 7px; margin-bottom: 3px;">
                                                            <i class="fas fa-trash-alt" style="float: right;z-index: 999;position: relative;right:2px;top:2px;color:forestgreen"></i>
                                                            <div class="wrap btn-upload-wp avatar1" style="margin-bottom: 5px; width: auto; padding: 15px 0 0 0;">
                                                                <div class="btn-upload-img" onclick="popupImageIframe($(this),'avatar1')"><i class="fas fa-plus fa-3x" style="color: #737373"></i></div>
                                                                <asp:FileUpload ClientIDMode="Static" Style="display: none;"
                                                                    CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                                                <div class="wrap listing-img-upload">
                                                                    <div class="thumb-wp">
                                                                        <img style="width: 100px; height: 80px; margin-top: -15px;" class="thumb" alt="" title="" src="https://s3.ap-southeast-1.amazonaws.com/30shine/API_UPLOAD_S3/2018_11_28/Img_78219_28_11_2018_20_08_46.jpg" data-img="" onclick="OpenModalImage($(this))" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="float: left; border: 1px dashed #8c8c8c; background-color: white; width: 102px; height: 82px; margin-bottom: 3px;">
                                                            <div class="wrap btn-upload-wp avatar2" style="margin-bottom: 5px; width: auto; padding: 15px 0 0 0;">
                                                                <div class="btn-upload-img" onclick="popupImageIframe($(this),'avatar2')"><i class="fas fa-plus fa-3x" style="color: #737373"></i></div>
                                                                <asp:FileUpload ClientIDMode="Static" Style="display: none;"
                                                                    CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                                                <div class="wrap listing-img-upload">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div style="float: left; border: 1px dashed #8c8c8c; background-color: white; width: 102px; height: 82px; margin-right: 7px; margin-top: 3px">
                                                            <i class="fas fa-trash-alt" style="float: right;z-index: 999;position: relative;right:2px;top:2px;color:forestgreen"></i>
                                                            <div class="wrap btn-upload-wp avatar3" style="margin-bottom: 5px; width: auto; padding: 15px 0 0 0;">
                                                                <div class="btn-upload-img" onclick="popupImageIframe($(this),'avatar1')"><i class="fas fa-plus fa-3x" style="color: #737373"></i></div>
                                                                <asp:FileUpload ClientIDMode="Static" Style="display: none;"
                                                                    CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                                                <div class="wrap listing-img-upload">
                                                                    <div class="thumb-wp">
                                                                        <img style="width: 100px; height: 80px; margin-top: -15px;" class="thumb" alt="" title="" src="https://s3.ap-southeast-1.amazonaws.com/30shine/API_UPLOAD_S3/2018_11_28/Img_78219_28_11_2018_20_08_46.jpg" data-img="" onclick="OpenModalImage($(this))" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="float: left; border: 1px dashed #8c8c8c; background-color: white; width: 102px; height: 82px; margin-top: 3px">
                                                            <div class="wrap btn-upload-wp avatar4" style="margin-bottom: 5px; width: auto; padding: 15px 0 0 0;">
                                                                <div class="btn-upload-img" onclick="popupImageIframe($(this),'avatar4')"><i class="fas fa-plus fa-3x" style="color: #737373"></i></div>
                                                                <asp:FileUpload ClientIDMode="Static" Style="display: none;"
                                                                    CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                                                <div class="wrap listing-img-upload">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td data-broken-note="">
                                                    <textarea class="broken-note" maxlength="300"></textarea>
                                                </td>
                                                <td data-level-priority="">
                                                    <div class="input-group input-group-sm">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">Ví dụ: 2</span>
                                                        </div>
                                                        <input type="text" class="form-control quantity-left" aria-label="Small" maxlength="2" onchange="onlyNumber($(this))" />
                                                    </div>
                                                </td>
                                                <td data-completed="">
                                                    <input type="text" class="datetime-picker form-control" placeholder="Ngày mong muốn..." />
                                                </td>
                                                <td class="td-resize"><i class="fas fa-times" title="Xoá"></i></td>
                                            </tr>--%>
                                        </tbody>
                                    </table>
                                </div>
                                <hr />
                                <div class="row">
                                    <div class="btn-group" style="margin-left: 15px;">
                                        <button type="button" id="completed-list" class="btn btn-sm btn-outline-secondary" onclick="CompleteListingItemSaveToDb();"><i class="fas fa-check" style="color: green"></i>&nbsp;Hoàn tất danh sách</button>
                                    </div>
                                    <div class="btn-group" style="margin-left: 15px;">
                                        <button type="button" id="cancel-list" class="btn btn-sm btn-outline-secondary" onclick="iscancel('table tbody#body-item')"><i class="far fa-times-circle" style="color: red"></i>&nbsp;Huỷ bỏ danh sách</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="myModal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!-- Page loading -->
            <div class="page-loading">
                <p>Vui lòng đợi trong giây lát...</p>
            </div>
        </section>
        <script type="text/javascript">
            var index = 1;
            //var SelectRequest;
            jQuery(document).ready(function () {
                $('#completed-list').hide();
                $('#cancel-list').hide();
                //$.ajax({
                //    type: "POST",
                //    url: "/GUI/BackEnd/Infrastructure/OrderBrokenDeviceHandleAdd.aspx/BindLevelPriority",
                //    contentType: "application/json; charset=utf-8",
                //    dataType: "json",
                //    success: function (data) {
                //        $('.select-list-item').append($("<option />").val('0').text('Chọn yêu cầu'));
                //        $.each(data.d, function () {
                //            $('.select-list-item').append($("<option />").val(this.Value).text(this.Label));
                //        });
                //        SelectRequest = $('.select-list-item');
                //    },
                //    failure: function () {
                //        ShowMessage('', 'Có lỗi xảy ra, vui lòng liên hệ nhà phát triển!', 4);
                //    }
                //});
            });

            function GetDropDownData(This) {
                let $region = This.val();
                if ($region === undefined || $region === null || $region === '') {
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Infrastructure/OrderBrokenDeviceHandleAdd.aspx/BindSalonByRegion",
                    data: '{RegionId: ' + $region + ' }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $("#ddlSalon").html('').val('0');
                        $("#select2-ddlSalon-container").val('0').text('Chọn salon');
                        $("#ddlSalon").append($("<option />").val(0).text('Chọn salon'));
                        $.each(data.d, function () {
                            $("#ddlSalon").append($("<option />").val(this.Id).text(this.Name));
                        });
                    },
                    failure: function () {
                        ShowMessage('', 'Có lỗi xảy ra, vui lòng liên hệ nhà phát triển!', 4);
                    }
                });
            }

            function CheckParameter(param) {
                if (param === undefined || param === null || param === '' || param === '0') {
                    return false
                }
                return true;
            }

            ///only number
            function onlyNumber(This) {
                if (!CheckParameter(This.val())) {
                    return;
                }
                This.val(This.val().replace(/\D/g, ''));
                if (!CheckParameter(This.val())) {
                    ShowMessage('', 'Chỉ được phép nhập số!', 3);
                }
            }

            function AddItemInList() {
                let str = '',
                    $region = $('#ddlRegion :selected'),
                    $salon = $('#ddlSalon :selected'),
                    $category = $('#ddlCategory :selected'),
                    $team = $('#ddlTeam :selected');
                if (!CheckParameter($region.val())) {
                    ShowMessage('', 'Bạn chưa chọn khu vực!', 3);
                    return;
                }
                else if (!CheckParameter($salon.val())) {
                    ShowMessage('', 'Bạn chưa chọn salon!', 3);
                    return;
                }
                else if (!CheckParameter($category.val())) {
                    ShowMessage('', 'Bạn chưa chọn hạng mục', 3);
                    return;
                }
                else if (!CheckParameter($team.val())) {
                    ShowMessage('', 'Bạn chưa chọn vị trí!', 3);
                    return;
                }
                else {
                    str = `<tr>
                                <th scope="row" class="th-index"></th>
                                <td class="td-complete-region" data-region="${$region.val()}">${$region.text()}</td>
                                <td class="td-complete-salon" data-salon="${$salon.val()}">${$salon.text()}</td>
                                <td class="td-complete-category" data-category="${$category.val()}">${$category.text()}</td>
                                <td class="td-complete-team" data-team="${$team.val()}">${$team.text()}</td>
                                <td class="td-complete-date-broken">
                                    <input type="text" class="datetime-picker form-control date-broken" placeholder="Ngày phát sinh..." />
                                </td>
                                <td class="td-complete-image">
                                    <div class="col-lg-12">
                                        <div style="float: left; border: 1px dashed #8c8c8c; background-color: white; width: 102px; height: 82px; margin-right: 7px; margin-bottom: 3px;">
                                            <i class="fas fa-trash-alt" style="float: right;z-index: 999;position: relative;right:2px;top:2px;color:forestgreen;display:none;" onclick="DeleteThumbImg($(this))"></i>
                                            <div class="wrap btn-upload-wp avatar1" style="margin-bottom: 5px; width: auto; padding: 15px 0 0 0;">
                                                <div class="btn-upload-img" onclick="popupImageIframe($(this),'avatar1')"><i class="fas fa-plus fa-3x" style="color: #737373"></i></div>
                                                <asp:FileUpload ClientIDMode="Static" Style="display: none;"
                                                    CssClass="input-file-upload" AllowMultiple="true" />
                                                <div class="wrap listing-img-upload">
                                                </div>
                                            </div>
                                        </div>
                                        <div style="float: left; border: 1px dashed #8c8c8c; background-color: white; width: 102px; height: 82px; margin-bottom: 3px;">
                                            <i class="fas fa-trash-alt" style="float: right;z-index: 999;position: relative;right:2px;top:2px;color:forestgreen;display:none;" onclick="DeleteThumbImg($(this))"></i>
                                            <div class="wrap btn-upload-wp avatar2" style="margin-bottom: 5px; width: auto; padding: 15px 0 0 0;">
                                                <div class="btn-upload-img" onclick="popupImageIframe($(this),'avatar2')"><i class="fas fa-plus fa-3x" style="color: #737373"></i></div>
                                                <asp:FileUpload ClientIDMode="Static" Style="display: none;"
                                                    CssClass="input-file-upload" AllowMultiple="true" />
                                                <div class="wrap listing-img-upload">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div style="float: left; border: 1px dashed #8c8c8c; background-color: white; width: 102px; height: 82px; margin-right: 7px; margin-top: 3px">
                                            <i class="fas fa-trash-alt" style="float: right;z-index: 999;position: relative;right:2px;top:2px;color:forestgreen;display:none;" onclick="DeleteThumbImg($(this))"></i>
                                            <div class="wrap btn-upload-wp avatar3" style="margin-bottom: 5px; width: auto; padding: 15px 0 0 0;">
                                                <div class="btn-upload-img" onclick="popupImageIframe($(this),'avatar3')"><i class="fas fa-plus fa-3x" style="color: #737373"></i></div>
                                                <asp:FileUpload ClientIDMode="Static" Style="display: none;"
                                                    CssClass="input-file-upload" AllowMultiple="true" />
                                                <div class="wrap listing-img-upload">
                                                </div>
                                            </div>
                                        </div>
                                        <div style="float: left; border: 1px dashed #8c8c8c; background-color: white; width: 102px; height: 82px; margin-top: 3px">
                                            <i class="fas fa-trash-alt" style="float: right;z-index: 999;position: relative;right:2px;top:2px;color:forestgreen;display:none;" onclick="DeleteThumbImg($(this))"></i>
                                            <div class="wrap btn-upload-wp avatar4" style="margin-bottom: 5px; width: auto; padding: 15px 0 0 0;">
                                                <div class="btn-upload-img" onclick="popupImageIframe($(this),'avatar4')"><i class="fas fa-plus fa-3x" style="color: #737373"></i></div>
                                                <asp:FileUpload ClientIDMode="Static" Style="display: none;"
                                                    CssClass="input-file-upload" AllowMultiple="true" />
                                                <div class="wrap listing-img-upload">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="td-complete-note-broken">
                                    <textarea class="broken-note" maxlength="300"></textarea>
                                </td>
                                <td class="td-complete-level-priority">
                                    <select class="select-list-item" onchange="OnchangeDropdownlist($(this))">
                                        <option value="0">Chọn yêu cầu</option>
                                        <option value="1">Ưu tiên</option>
                                        <option value="2">Gấp</option>
                                        <option value="3">Bình thường</option>
                                    </select>
                                </td>
                                <td class="td-complete-completed">
                                    <input type="text" class="datetime-picker form-control date-completed" placeholder="Ngày mong muốn..." />
                                </td>
                                <td class="td-resize"><i class="fas fa-times" title="Xoá" onclick="DeleteItem($(this), 'table tbody#body-item')"></i></td>
                            </tr>`;
                    if (str !== '') {
                        $('#body-item').append(str);
                        $('#completed-list').show();
                        $('#cancel-list').show();
                        //$('table tbody#body-item tr td.td-complete-level-priority .select-list-item').remove();
                        //$('table tbody#body-item tr td.td-complete-level-priority').append(SelectRequest);
                        //$('table tbody#body-item tr td.td-complete-level-priority').find('.select-list-item').show();
                        //$('.select-list-item').val('0');
                        SetSelected();
                        UpdateItemOrder('table tbody#body-item');
                        ShowMessage('', 'Thêm dữ liệu vào danh sách thành công!', 2);
                        Datetimepicker();
                    }
                }
            }

            function DeleteItem(This, dom) {
                This.parent().parent().remove();
                if ($("table tbody#body-item tr").length === 0) {
                    $('#completed-list').hide();
                    $('#cancel-list').hide();
                }
                ShowMessage('', 'Xoá thành công!', 2);
                UpdateItemOrder(dom);
            }

            // Update order item
            function UpdateItemOrder(dom) {
                var i = 1;
                $(`${dom}`).find("tr").each(function () {
                    $(this).find("th.th-index").text(i);
                    i++;
                });
            }

            function iscancel(dom) {
                $('#completed-list').hide();
                $('#cancel-list').hide();
                $(`${dom}`).html('');
            }

            function Datetimepicker() {
                $('.datetime-picker').datetimepicker({
                    dayOfWeekStart: 1,
                    startDate: new Date(),
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    scrollMonth: false,
                    scrollInput: false,
                    onChangeDateTime: function (dp, $input) { }
                });
            }

            function CompleteListingItemSaveToDb() {
                let success = true,
                    $img = '',
                    Ids = [],
                    prd = {};
                $("table tbody#body-item tr").each(function () {
                    prd = {};
                    $img = '';
                    let $region = $(this).find("td.td-complete-region").attr('data-region'),
                        $salon = $(this).find("td.td-complete-salon").attr('data-salon'),
                        $category = $(this).find("td.td-complete-category").attr('data-category'),
                        $team = $(this).find("td.td-complete-team").attr('data-team'),
                        $dateBroken = $(this).find("td.td-complete-date-broken").find(".date-broken").val(),
                        $img1 = $(this).find("td.td-complete-image .avatar1 .thumb-wp img.thumb").attr('src'),
                        $img2 = $(this).find("td.td-complete-image .avatar2 .thumb-wp img.thumb").attr('src'),
                        $img3 = $(this).find("td.td-complete-image .avatar3 .thumb-wp img.thumb").attr('src'),
                        $img4 = $(this).find("td.td-complete-image .avatar4 .thumb-wp img.thumb").attr('src'),
                        $noteBroken = $(this).find("td.td-complete-note-broken").find(".broken-note").val(),
                        $priority = $(this).find("td.td-complete-level-priority").find(".select-list-item").val(),
                        $dateCompleted = $(this).find("td.td-complete-completed").find(".date-completed").val();
                    if (!CheckParameter($dateBroken)) {
                        ShowMessage('', 'Bạn chưa nhập ngày phát sinh!', 3);
                        $(this).find("td.td-complete-date-broken").find(".date-broken").focus();
                        success = false;
                        return false;
                    }
                    else if (!CheckParameter($img1) && !CheckParameter($img2) && !CheckParameter($img3) && !CheckParameter($img4)) {
                        ShowMessage('', 'Bạn chưa chọn ảnh sự cố!', 3);
                        $(this).find("td.td-complete-image .avatar1").focus();
                        success = false;
                        return false;
                    }
                    else if (!CheckParameter($noteBroken)) {
                        ShowMessage('', 'Bạn chưa nhập diễn giải hỏng hóc!', 3);
                        $(this).find("td.td-complete-note-broken").find(".broken-note").focus();
                        success = false;
                        return false;
                    }
                    else if (!CheckParameter($priority)) {
                        ShowMessage('', 'Bạn chưa chọn mức độ ưu tiên!', 3);
                        $(this).find("td.td-complete-level-priority").find(".select-list-item").focus();
                        success = false;
                        return false;
                    }
                    //Get today's date 
                    var today = new Date();
                    today = Date.parse(today.getMonth() + '/' + today.getDate() + '/' + today.getFullYear());
                    //Get the selected date 
                    var date = $dateBroken.split("/");
                    var d = new Date(date[1] + "/" + date[0] + "/" + date[2]);
                    var selDate = Date.parse(d.getMonth() + '/' + d.getDate() + '/' + d.getFullYear());
                    if (selDate > today) {
                        //If the selected date was before today, continue to show the datepicker
                        $(this).find("td.td-complete-date-broken").find(".date-broken").val('');
                        ShowMessage('', 'Ngày phát sinh không được lớn hơn ngày hiện tại!', 3);
                        $(this).find("td.td-complete-date-broken").find(".date-broken").focus();
                        success = false;
                        return false;
                    }

                    if (CheckParameter($dateCompleted)) {
                        //Get today's date 
                        var today = new Date();
                        today = Date.parse(today.getMonth() + '/' + today.getDate() + '/' + today.getFullYear());
                        //Get the selected date 
                        var date = $dateCompleted.split("/");
                        var d = new Date(date[1] + "/" + date[0] + "/" + date[2]);
                        var selDate = Date.parse(d.getMonth() + '/' + d.getDate() + '/' + d.getFullYear());
                        if (selDate < today) {
                            //If the selected date was before today, continue to show the datepicker
                            $(this).find("td.td-complete-completed").find(".date-completed").val('');
                            ShowMessage('', 'Ngày mong muốn không được nhỏ hơn ngày hiện tại!', 3);
                            $(this).find("td.td-complete-completed").find(".date-completed").focus();
                            success = false;
                            return false;
                        }
                    }
                    if (CheckParameter($img1)) {
                        $img += $img1 + ','
                    }
                    if (CheckParameter($img2)) {
                        $img += $img2 + ','
                    }
                    if (CheckParameter($img3)) {
                        $img += $img3 + ','
                    }
                    if (CheckParameter($img4)) {
                        $img += $img4
                    }
                    if ($img.endsWith(',')) {
                        $img = $img.slice(0, $img.length - 1);
                    }
                    // check value
                    prd.RegionId = $region.toString().trim() !== "" ? parseInt($region) : 0;
                    prd.SalonId = $salon.toString().trim() !== "" ? parseInt($salon) : 0;
                    prd.CategoryId = $category.toString().trim() !== "" ? parseInt($category) : 0;
                    prd.TeamId = $team.toString().trim() !== "" ? parseInt($team) : 0;
                    prd.DateBrokenSub = $dateBroken.toString().trim();
                    prd.ImagesBroken = $img.toString().trim();
                    prd.DescriptionBroken = $noteBroken.toString().trim();
                    prd.LevelPriority = $priority.toString().trim() !== "" ? parseInt($priority) : 0;
                    prd.DesiredDeadlineSub = $dateCompleted.toString().trim();
                    Ids.push(prd);
                });
                if (success && Ids.length > 0) {
                    startLoading();
                    var data = JSON.stringify({ list: Ids });;
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/Infrastructure/OrderBrokenDeviceHandleAdd.aspx/CompletedList",
                        data: data,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            finishLoading();
                            if (data.d) {
                                $("table tbody#body-item").html('');
                                $('#completed-list').hide();
                                $('#cancel-list').hide();
                                ShowMessage('', 'Hoàn tất thành công!', 2);
                            }
                            else {
                                ShowMessage('', 'Hoàn tất thất bại!', 4);
                            }
                        },
                        failure: function () {
                            ShowMessage('', 'Có lỗi xảy ra, vui lòng liên hệ nhà phát triển!', 4);
                        }
                    });
                }
            }

            function SetSelected() {
                $("table tbody#body-item tr").each(function () {
                    let $category = $(this).find("td.td-complete-category").attr('data-category');
                    if (!CheckParameter($category)) {
                        return;
                    }
                    else {
                        if (parseInt($category) === 1 || parseInt($category) === 2 || parseInt($category) === 13 || parseInt($category) === 14) {
                            $(this).find('td.td-complete-level-priority').find('.select-list-item').val('1');
                            $(this).find('td.td-complete-level-priority').find('.select-list-item').css('background-color', '#e74c3c').css('color', 'black');
                            $(this).find('td.td-complete-level-priority').find('.select-list-item').attr("disabled", true).attr('title', 'Đã khoá');
                        }
                        else {
                            $(this).find('td.td-complete-level-priority').find('.select-list-item').css('background-color', '').css('color', '');
                            $(this).find('td.td-complete-level-priority').find('.select-list-item').removeAttr("disabled").removeAttr("title");
                        }
                    }
                });
            }

            function OnchangeDropdownlist(This) {
                let $value = This.val();
                if ($value === '1') {
                    This.css('background-color', '#e74c3c').css('color', 'black');
                }
                else {
                    This.css('background-color', '').css('color', '');
                }
            }
        </script>

        <!-- Popup plugin image -->
        <style>
            .iframe-wrapper {
                position: fixed;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
                z-index: 1050;
                display: block;
                overflow: hidden;
                outline: 0;
                background-color: rgba(0, 0, 0, 0.5);
            }
        </style>
        <script type="text/javascript">
            var item;
            function popupImageIframe(This, StoreImgField) {
                var ImgList = [];
                var iframe = $("<div class='iframe-wrapper'><div class='iframe-image-upload' style='width:960px; height:590px;'><div style='float:right;position:absolute;right:10px;top:10px;'><i onclick='CloseLibraryImage($(this))' class='fa fa-times fa-2x close-icon-new'></i></div><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + StoreImgField + "&callback=getThumbFromIframe&imgWidth=800&imgHeight=600&multi_img=false&Folder=NhanVien' style='width:100%; height: 100%;'></iframe></div></div>");
                $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
                item = This;
            }

            window.getThumbFromIframe = function (Imgs, StoreImgField) {
                var imgs = "";
                var thumb = "";
                if (Imgs.length > 0) {
                    thumb = `<div class="thumb-wp">
                             <img style="width:100px;height:80px;margin-top: -15px;" class="thumb" alt="" title="" src="${Imgs[0]}"
                             data-img="" onclick="OpenModalImage($(this))" />
                             </div>`;
                    item.parent().find(".listing-img-upload").empty().append(thumb);
                    item.parent().find("." + StoreImgField).val(Imgs[0]);
                    item.parent().parent().find('.fa-trash-alt').css('display', 'block');
                    item.hide();
                }
                CloseLibraryImage();
            }

            function CloseLibraryImage() {
                $(".iframe-image-upload").remove();
                $(".iframe-wrapper").remove();
                $(".close-icon-new").remove();
            }

            function OpenModalImage(This) {
                let item = This.attr('src');
                $("#myModal").modal();
                $('.modal-body').html('');
                $('.modal-body').append('<img src="' + item + '" style="width:100%;height:400px;">');
            }

            function DeleteThumbImg(This) {
                This.css('display', 'none');
                This.parent().find('.btn-upload-img').css('display', 'block');
                This.parent().find('.listing-img-upload').html('');
            }
        </script>
    </asp:Panel>
</asp:Content>
