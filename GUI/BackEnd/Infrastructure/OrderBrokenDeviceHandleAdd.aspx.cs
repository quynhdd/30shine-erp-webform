﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.Infrastructure
{
    public partial class OrderBrokenDeviceHandleAdd : System.Web.UI.Page
    {
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected static bool ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                ViewAllData = Perm_ViewAllData;
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        public int GetIds()
        {
            return int.TryParse(Session["User_Id"].ToString(), out var integer) ? integer : 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                BindCategory();
                BindRegion();
                BindTeam();
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
            }
        }

        /// <summary>
        /// Bind Department to ddl
        /// </summary>
        protected void BindCategory()
        {
            using (var db = new Solution_30shineEntities())
            {
                var configs = db.Tbl_Config.Where(w => w.IsDelete == 0 && w.Status == 1 && w.Key == "order_device_category").OrderBy(o => o.Id).ToList();
                var item = new Tbl_Config();
                item.Value = "0";
                item.Label = "Chọn hạng mục";
                configs.Insert(0, item);
                ddlCategory.DataTextField = "Label";
                ddlCategory.DataValueField = "Value";
                ddlCategory.DataSource = configs;
                ddlCategory.DataBind();
            }
        }

        /// <summary>
        /// Bind Department to ddl
        /// </summary>
        protected void BindTeam()
        {
            using (var db = new Solution_30shineEntities())
            {
                var configs = db.Tbl_Config.Where(w => w.IsDelete == 0 && w.Status == 1 && w.Key == "order_device_team").OrderBy(o => o.Id).ToList();
                var item = new Tbl_Config();
                item.Value = "0";
                item.Label = "Chọn vị trí";
                configs.Insert(0, item);
                ddlTeam.DataTextField = "Label";
                ddlTeam.DataValueField = "Value";
                ddlTeam.DataSource = configs;
                ddlTeam.DataBind();
            }
        }

        /// <summary>
        /// Bind Region to ddl
        /// </summary>
        protected void BindRegion()
        {
            using (var db = new Solution_30shineEntities())
            {
                //chỉ lấy bộ phận stylist, skinner
                var types = db.TinhThanhs.OrderBy(o => o.ID).ToList();
                var item = new TinhThanh();
                item.ID = 0;
                item.TenTinhThanh = "Chọn khu vực";
                types.Insert(0, item);
                ddlRegion.DataTextField = "TenTinhThanh";
                ddlRegion.DataValueField = "Id";
                ddlRegion.DataSource = types;
                ddlRegion.DataBind();
            }
        }

        /// <summary>
        /// Bind Department to ddl
        /// </summary>
        [WebMethod]
        public static List<Tbl_Salon> BindSalonByRegion(int RegionId)
        {
            using (var db = new Solution_30shineEntities())
            {
                //chỉ lấy bộ phận stylist, skinner
                if (RegionId < 0)
                {
                    return new List<Tbl_Salon>();
                }
                var listSalon = new List<Tbl_Salon>();
                var SalonId = Convert.ToInt32(HttpContext.Current.Session["SalonId"]);
                var UserId = Convert.ToInt32(HttpContext.Current.Session["User_Id"]);
                if (ViewAllData)
                {
                    listSalon = db.Tbl_Salon.Where(w => w.IsDelete == 0 && w.Publish == true && ((w.CityId == RegionId) || (RegionId == 0))).OrderByDescending(o => o.CityId).ToList();
                }
                else
                {
                    listSalon = (
                                        from p in db.PermissionSalonAreas.AsNoTracking()
                                        join s in db.Tbl_Salon.AsNoTracking() on p.SalonId equals s.Id
                                        where
                                        p.IsDelete == false && p.StaffId == UserId
                                        && s.Publish == true && s.IsDelete == 0 && ((s.CityId == RegionId) || (RegionId == 0))
                                        select s
                                   ).OrderByDescending(o => o.CityId).ToList();
                    if (listSalon.Count <= 0)
                    {
                        listSalon = db.Tbl_Salon.Where(w => w.Id == SalonId).ToList();
                    }
                }
                return listSalon;
            }
        }

        /// <summary>
        /// Bind Type Request
        /// </summary>
        [WebMethod]
        public static List<Tbl_Config> BindLevelPriority()
        {
            using (var db = new Solution_30shineEntities())
            {
                return db.Tbl_Config.Where(w => w.IsDelete == 0 && w.Status == 1 && w.Key == "order_device_priority").OrderBy(o => o.Id).ToList();
            }
        }

        /// <summary>
        /// insert
        /// </summary>
        [WebMethod]
        public static bool CompletedList(List<OrderBrokenDeviceHandlingSub> list)
        {
            try
            {
                var creatorId = int.TryParse(HttpContext.Current.Session["User_Id"].ToString(), out var integer) ? integer : 0;
                IOrderDeviceBorkenHandlingModel model = new OrderDeviceBorkenHandlingModel();
                if (list.Count <= 0)
                {
                    return false;
                }
                model.Add(list, creatorId);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public class OrderBrokenDeviceHandlingSub : OrderBrokenDeviceHandling
    {
        public string DateBrokenSub { get; set; }
        public string DesiredDeadlineSub { get; set; }
    }
}