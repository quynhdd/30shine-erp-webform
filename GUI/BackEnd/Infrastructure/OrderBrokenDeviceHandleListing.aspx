﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderBrokenDeviceHandleListing.aspx.cs" MasterPageFile="~/TemplateMaster/SiteMaster.Master" Inherits="_30shine.GUI.BackEnd.Infrastructure.OrderBrokenDeviceHandleListing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            table.table-report-department {
            }

            table.table-report-department {
                border-collapse: collapse;
            }

            table.table-report-department, .table-report-department th, .table-report-department td {
                border: 1px solid #bababa;
            }

            .table-report-department th {
                font-weight: normal;
                font-family: Roboto Condensed Bold;
                padding: 5px 10px;
            }

            .table-report-department td {
                padding: 5px 10px;
            }

            .tip {
                /*border-bottom: 1px dashed;*/
                text-decoration: none
            }

                .tip:hover {
                    cursor: pointer;
                    position: relative
                }

                .tip span {
                    display: none
                }

                .tip:hover span {
                    border: #c0c0c0 1px solid;
                    border-radius: 5px;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    padding: 5px 20px 5px 5px;
                    display: block;
                    z-index: 100;
                    background: #c1d9ff repeat-x;
                    left: 0px;
                    margin: 10px;
                    width: auto !important;
                    min-width: 100px;
                    position: absolute;
                    bottom: 0px;
                    text-decoration: none;
                    color: #252734;
                }

            .alert {
                margin-bottom: 0 !important;
            }

            .show-msg {
                line-height: 39px;
                font-style: italic;
            }

            .broken-imgs img, .handling-imgs-td img {
                width: 70px;
                height: 70px;
                margin-bottom: 5px !important;
            }

            .broken-imgs, .handling-imgs-td {
                width: 150px;
                padding: 5px 2px !important;
            }

            .row.update-handle .filter-item {
                padding-top: 20px !important;
                clear:both;
            }

                .row.update-handle .filter-item label {
                    line-height: 33px;
                }
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Báo cáo &nbsp;&#187; </li>
                        <li class="be-report-li"><a href="/admin/xu-ly-su-co/them-moi-yeu-cau.html"><i class="fa fa-th-large"></i>THÊM MỚI YC XỬ LÝ CSVC</a></li>
                        <li class="be-report-li"><a href="#"><i class="fa fa-th-large"></i><b>&nbsp THỐNG KÊ SỰ CỐ</b></a></li>
                        <li class="be-report-li"><a href="/admin/xu-ly-su-co/bao-cao-tien-do-hoan-thanh.html"><i class="fa fa-th-large"></i>&nbsp TĐHT YÊU CẦU XỬ LÝ CSVC</a></li>

                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing be-report be-report-timekeeping">
            <%-- Listing --%>
            <asp:ScriptManager ID="SM02" runat="server"></asp:ScriptManager>
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                    <div class="time-wp">
                        <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="ValidateTime" ControlToValidate="TxtDateTimeFrom" runat="server" CssClass="fb-cover-error" Text="Vui lòng chọn thời gian!"></asp:RequiredFieldValidator>
                    </div>
                    <strong class="st-head" style="margin-left: 10px;">
                        <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                    </strong>
                    <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeTo" placeholder="Đến ngày"
                        ClientIDMode="Static" runat="server"></asp:TextBox>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlDate" CssClass="form-control" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 149px;">
                            <asp:ListItem Text="Ngày tạo yêu cầu" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Ngày phát sinh sự cố" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Ngày deadline mong muốn" Value="3"></asp:ListItem>
                            <asp:ListItem Text="Ngày deadline chuẩn" Value="4"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlRegion" runat="server" CssClass="form-control select" ClientIDMode="Static" Style="width: 149px;"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="Salon" runat="server" CssClass="form-control select" ClientIDMode="Static" Style="width: 149px;"></asp:DropDownList>
                    </div>
                    <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" Style="z-index: 99999 !important" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <asp:Button ID="ExportExcell" CssClass="st-head btn-viewdata" ClientIDMode="Static" runat="server" Text="Xuất Excel" OnClick="Exc_ExportExcel"/>
                    <asp:Panel ID="resetFillter" CssClass="st-head btn-viewdata" ClientIDMode="Static" runat="server">
                        Reset Filter
                    </asp:Panel>

                </div>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-filter"></i>Lọc kết quả</strong>
                    <asp:UpdatePanel runat="server" ID="UPStaff">
                        <ContentTemplate>
                            <div class="filter-item">
                                <asp:DropDownList ID="ddlCategory" CssClass="form-control" runat="server" ClientIDMode="Static" Style="width: 149px;"></asp:DropDownList>
                            </div>
                            <div class="filter-item">
                                <asp:DropDownList ID="ddlPriority" CssClass="form-control" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 149px;">
                                </asp:DropDownList>
                            </div>
                            <div class="filter-item">
                                <asp:DropDownList ID="ddlStatus" CssClass="form-control" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 149px;">
                                </asp:DropDownList>
                            </div>
                            <div class="filter-item">
                                <asp:DropDownList ID="ddlStatusCheck" CssClass="form-control" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px;">
                                </asp:DropDownList>
                            </div>
                            <div class="filter-item">
                                <asp:DropDownList ID="ddlStatusRevoke" CssClass="form-control" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 149px;">
                                </asp:DropDownList>
                            </div>
                            <div class="filter-item">
                                <asp:DropDownList ID="ddlCompleteStatus" CssClass="form-control" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 149px;">
                                <asp:ListItem Value="0">Chọn tiêu chí</asp:ListItem>
                                    <asp:ListItem Value="1">Hoàn thành đúng hạn</asp:ListItem>
                                    <asp:ListItem Value="2">Hoàn thành trễ hạn</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="filter-item">
                                <asp:TextBox runat="server" ClientIDMode="Static" CssClass="form-control" Style="margin: 12px 0; width: 149px;" ID="txtFixed" placeholder="Nhân viên sửa chữa"></asp:TextBox>
                            </div>
                            <%--<div class="filter-item">
                                <span class="show-msg" style="display:none;"></span>
                            </div>--%>
                        </ContentTemplate>
                        <Triggers></Triggers>
                    </asp:UpdatePanel>
                </div>
                <!-- Row Table Filter -->
                <div class="table-func-panel" style="margin-top: -33px;">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <!-- End Row Table Filter -->
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="row table-wp">
                            <table class="table-add table-listing table-listing-salary">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Ngày tạo YC</th>
                                        <th>Ngày phát sinh</th>
                                        <th>Khu vực</th>
                                        <th>Salon</th>
                                        <th>Người tạo</th>
                                        <th>Hạng mục</th>
                                        <th>Vị trí</th>
                                        <th>Ảnh sự cố</th>
                                        <th>Mô tả sự cố</th>
                                        <th>Mức độ ưu tiên</th>
                                        <th>Xử lý trước ngày</th>
                                        <th>Ngày theo quy định</th>
                                        <th>Trạng thái xử lý</th>
                                        <th>Người xử lý</th>
                                        <th>Ngày hoàn thành</th>
                                        <th>Ảnh xử lý</th>
                                        <th>Số lần sửa</th>
                                        <th>Trạng thái nghiệm thu</th>
                                        <th>Ghi chú</th>
                                        <th>Hủy yêu cầu</th>
                                        <th>Hành dộng</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    <asp:Repeater ID="rptSalary" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td><%# Eval("CreatedDate") %></td>
                                                <td><%# Eval("BrokenDate") %></td>
                                                <td><%# Eval("Region") %></td>
                                                <td><%# Eval("Salon") %></td>
                                                <td><%# Eval("Creater") %></td>
                                                <td><%# Eval("Category") %></td>
                                                <td><%# Eval("Team") %></td>
                                                <td class="broken-imgs" style="padding: 5px 2px !important;"><%# Eval("BrokenImgs").ToString() %></td>
                                                <td><%# Eval("DesBroken") %></td>
                                                <td style="background-color: <%# Eval("PriorityColor") %>"><%# Convert.ToInt32(Eval("ReasonRevokeId").ToString()) !=0 ? "Đã hủy" : Eval("Priority") %></td>
                                                <td><%# Eval("DesiredDeadline") %></td>
                                                <td><%# Eval("CorrectDeadline") %></td>
                                                <td class="status-handle-td"><%# Eval("StatusHandle") %></td>
                                                <td class="staff-handling-td"><%# Eval("StaffHandling") %></td>
                                                <td class="handling-date-td"><%# Eval("HandlingDate") %></td>
                                                <td class="handling-imgs-td" style="padding: 5px 2px !important;"><%# Eval("HandlingImgs") %></td>
                                                <td class="handling-count-td"><%# Eval("HandlingCount") %></td>
                                                <td class="status-check-td"><%# Eval("StatusCheck") %></td>
                                                <td class="td-reason-note"><%# Eval("ReasonNote") %></td>
                                                <td class="td-revoke"><%# Eval("ReasonRevoke") %></td>
                                                <td>
                                                    <a title="Cập nhật" href="#" class="btn-submit-change" <%# (Convert.ToInt32(Eval("ReasonRevokeId").ToString()) != 0 ? "style='display:none'" : "") %> onclick="UpdateStatusHandle($(this))"
                                                        data-order-id="<%# Eval("OrderId") %>"
                                                        data-status-handle-id="<%# Eval("StatusHandleId") %>"
                                                        data-status-check-id="<%# Eval("StatusCheckId") %>"
                                                        data-reason-revoke-id="<%# Eval("ReasonRevokeId") %>"
                                                        data-number-handle="<%# Eval("HandlingCount") %>"
                                                        data-handle-imgs="<%# Eval("HandlingImgs") %>"><i class="fa fa-edit"></i></a>
                                                    <%-- <a title='Xác nhận' href="#" style="display:none" class="btn-submit-confirm"><i class="fa fa-check-circle"></i></a>
                                                    <a title='Hủy' href="#" style="display:none" class="btn-submit-cancel"><i class="fa fa-times-circle"></i></a>--%>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>

                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>

                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />

                <!-- Hidden Field-->
                <asp:HiddenField ID="HDF_TypeStaff" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_Staff" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_ExcelType" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_ExportType" ClientIDMode="Static" runat="server" />
                <!-- END Hidden Field-->
            </div>
            <%-- END Listing --%>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="show-image-broken" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h3 style="text-align: center; font-size: 20px;"><b>Ảnh hiện trạng CSVC</b></h3>
                    </div>
                    <div class="modal-body">
                        <div id="myCarousel" class="carousel slide">
                            <!-- Indicators -->
                            <ol class="carousel-indicators"></ol>
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                            </div>

                            <!-- Left and right controls -->
                            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="show-broken-hanle" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 style="text-align: center; font-size: 20px;"><b>CẬP NHẬT HIỆN TRẠNG CSVC</b></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row update-handle">
                            <input type="hidden" id="hf-order-id" />
                            <ul class="nav nav-tabs tabs-update-handle">
                                <li class="active li-status-handle"><a data-toggle="tab" href="#UpdateStatusHandle" onclick="ShowLogStatusHandle()">Cập nhật trạng thái sửa chữa</a></li>
                                <li class="li-status-check"><a data-toggle="tab" href="#UpdateStatusCheck" onclick="ShowLogStatusCheck()">Cập nhật trạng thái nghiệm thu</a></li>
                                <li><a data-toggle="tab" href="#ReasonRevokePanel">Hủy order</a></li>
                            </ul>
                            <div class="tab-content">
                                <asp:Panel ID="UpdateStatusHandle" runat="server" CssClass="tab-pane fade in active" ClientIDMode="Static">
                                    <div class="filter-item">
                                        <div class="col-xs-3">
                                            <label>Trạng thái sửa chữa</label>
                                        </div>
                                        <div class="col-xs-9">
                                            <select class="form-control status-handle" onchange="ShowUploadImgs($(this))">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="filter-item div-handling-note" style="display:none">
                                        <div class="col-xs-3">
                                            <label>Ghi chú sửa chữa</label>
                                        </div>
                                        <div class="col-xs-9">
                                            <textarea rows="5" class="form-control text-handling-note" placeholder="Nhập số lần sửa chữa" ></textarea>
                                        </div>
                                    </div>
                                    <div class="filter-item div-number-handling" style="display: none;">
                                        <div class="col-xs-3">
                                            <label>Số lần sửa</label>
                                        </div>
                                        <div class="col-xs-9">
                                            <input type="text" class="form-control number-handling" placeholder="Nhập số lần sửa chữa" />
                                        </div>

                                    </div>
                                    <div class="filter-item imgs-handling" style="display: none;">
                                        <div class="col-xs-3">
                                            <label>Ảnh sửa chữa</label>
                                        </div>
                                        <div class="col-xs-9">
                                            <input id="files" onchange="fileUpload()" type="file" class="form-control file-upload" multiple />
                                        </div>
                                    </div>
                                    <div class="filter-item">
                                        <div class="col-xs-3">
                                        </div>
                                        <div class="col-xs-9">
                                            <button type="button" class="btn btn-default btn-default pull-left" onclick="UpdateStatusHandling()"><span class="glyphicon glyphicon-confirm"></span>Xác nhận</button>
                                        </div>
                                    </div>
                                    <div class="filter-item">
                                        <table class="table table-listing table-status-handle">
                                            <thead>
                                                <tr>
                                                    <th>Người cập nhật</th>
                                                    <th>Trạng thái cập nhật</th>
                                                    <th>Thời gian cập nhật</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="UpdateStatusCheck" runat="server" CssClass="tab-pane fade" ClientIDMode="Static">
                                    <div class="filter-item">
                                        <div class="col-xs-3">
                                            <label>Nghiệm thu</label>
                                        </div>
                                        <div class="col-xs-9">
                                            <select class="form-control status-check">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="filter-item">
                                        <div class="col-xs-3">
                                        </div>
                                        <div class="col-xs-9">
                                            <button type="button" class="btn btn-default btn-default pull-left" onclick="UpdateStatusCheck()"><span class="glyphicon glyphicon-confirm"></span>Xác nhận</button>
                                        </div>
                                    </div>
                                    <div class="filter-item">
                                        <table class="table table-listing table-status-check">
                                            <thead>
                                                <tr>
                                                    <th>Người cập nhật</th>
                                                    <th>Trạng thái cập nhật</th>
                                                    <th>Thời gian cập nhật</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </asp:Panel>
                                <asp:Panel ID="ReasonRevokePanel" runat="server" CssClass="tab-pane fade" ClientIDMode="Static">
                                    <div class="filter-item">
                                        <div class="col-xs-3">
                                            <label>Hủy yêu cầu</label>
                                        </div>
                                        <div class="col-xs-9">
                                            <select class="form-control reason-revoke" onchange=" ShowHideReasonNote($(this))">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="filter-item">
                                        <div class="col-xs-3">
                                            <label>Ghi chú</label>
                                        </div>
                                        <div class="col-xs-9">
                                            <textarea class="form-control note-revoke" rows="4" placeholder="Nhập lý do hủy" style="display: none"></textarea>
                                        </div>
                                    </div>
                                    <div class="filter-item">
                                        <div class="col-xs-3">
                                        </div>
                                        <div class="col-xs-9">
                                            <button type="button" class="btn btn-default btn-default pull-left" onclick="UpdateReasonRevoke()"><span class="glyphicon glyphicon-confirm"></span>Xác nhận</button>
                                        </div>
                                    </div>

                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">

                        <button type="submit" class="btn btn-default btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span>Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <style>
            .be-report table.table-listing.table-listing-salary td {
                padding: 7px 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                margin-top: 4px !important;
            }

                .select2-container--default .select2-selection--single .select2-selection__arrow {
                    margin-top: 5px;
                }
        </style>
        <link href="/Assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <script src="/Assets/js/jquery.mCustomScrollbar.js"></script>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <style>
            .select2-container {
                width: 190px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/ecmascript">
            function ToJavaScriptDate(value) {
              var pattern = /Date\(([^)]+)\)/;
              var results = pattern.exec(value);
                var dt = new Date(parseFloat(results[1]));
                return (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear() + " " + dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
            }
            function ShowUpdate(This) {
                var tr = This.closest("tr");
                tr.find(".status-handle").empty().append("<in>")
            }

            function ShowImgBroken(This) {
                var tr = This.closest("tr");
                var div = "";
                var img = "";
                $(".carousel-indicators").empty();
                $(".carousel-inner").empty();
                $("#show-image-broken").modal();
                tr.find(".broken-imgs img").each(function (index, val) {
                    var src = $(this).attr("src");
                    img = '<div class="item ' + (index == 0 ? "active" : "") + '">' +
                        '<img src="' + src + '" >' +
                        '</div>';
                    var li = '<li data-target="#myCarousel" data-slide-to="' + index + '" ' + (index == 0 ? 'class="active"' : "") + '></li>';
                    $(".carousel-indicators").append(li);
                    $(".carousel-inner").append(img);
                });
            }

            function ShowHandlingImgs(This) {
                var tr = This.closest("tr");
                var div = "";
                var img = "";
                $(".carousel-indicators").empty();
                $(".carousel-inner").empty();
                $("#show-image-broken").modal();
                tr.find(".handling-imgs-td img").each(function (index, val) {
                    var src = $(this).attr("src");
                    img = '<div class="item ' + (index == 0 ? "active" : "") + '">' +
                        '<img src="' + src + '" >' +
                        '</div>';
                    var li = '<li data-target="#myCarousel" data-slide-to="' + index + '" ' + (index == 0 ? 'class="active"' : "") + '></li>';
                    $(".carousel-indicators").append(li);
                    $(".carousel-inner").append(img);
                });
            }
            function ShowHideReasonNote(This) {
                if (This.val() == 0) {
                    $(".form-control.note-revoke").css("display", "none");
                    return false;
                }
                $(".form-control.note-revoke").removeAttr("style");
            }
            function ShowUploadImgs(This) {
                if (This.val() != 0) {
                    $(".filter-item.div-handling-note").removeAttr("style");
                }
                if (This.val() == 3) {
                    $(".filter-item.imgs-handling").removeAttr("style");
                    $(".filter-item.div-number-handling").removeAttr("style");

                }
            }
            //get region and fill salon by region id
            function GetDropDownData(This) {
                let $region = This.val();
                if ($region === undefined || $region === null || $region === '') {
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Infrastructure/OrderBrokenDeviceHandleListing.aspx/BindSalonByRegion",
                    data: '{RegionId: ' + $region + ' }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $("#Salon").html('').val('0');
                        $("#select2-Salon-container").val('0').text('Chọn salon');
                        $("#Salon").append($("<option />").val(0).text('Chọn salon'));
                        $.each(data.d, function () {
                            $("#Salon").append($("<option />").val(this.Id).text(this.Name));
                        });
                    },
                    failure: function () {
                        ShowMessage('', 'Có lỗi xảy ra, vui lòng liên hệ nhà phát triển!', 4);
                    }
                });
            }
            function ShowLogStatusHandle() {
                type = 1;
                key = "order_device_status_handle";
                orderId = $("#hf-order-id").val();
                $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/Infrastructure/OrderBrokenDeviceHandleListing.aspx/GetLogStatusHandle",
                        contentType: "application/json; charset=utf-8",
                        data: "{orderId:" + orderId + ",type:'" + type + "',key:'" + key + "'}",
                        dataType: "json",
                        success: function (data) {
                            var jsonData = JSON.parse(data.d);
                            console.log(jsonData);
                            $(".table-status-handle tbody").empty();
                            var tr = "";
                            for (var i = 0; i < jsonData.length; i++) {
                                tr += "<tr>";
                                tr += "<td>" + jsonData[i].StaffHandle + " | " + jsonData[i].staffFullname + "</td>";
                                tr += "<td>" + jsonData[i].Value + "</td>";
                                tr += "<td>" + ToJavaScriptDate(jsonData[i].CreatedTime) + "</td>";
                                tr += "</tr>";
                            }
                            
                            $(".table-status-handle tbody").append(tr);
                        }
                    })
            }
            function ShowLogStatusCheck() {
                type = 2;
                key = "order_device_status_check";
                orderId = $("#hf-order-id").val();
                $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/Infrastructure/OrderBrokenDeviceHandleListing.aspx/GetLogStatusCheck",
                        contentType: "application/json; charset=utf-8",
                        data: "{orderId:" + orderId + ",type:'" + type + "',key:'" + key + "'}",
                        dataType: "json",
                        success: function (data) {
                            var jsonData = JSON.parse(data.d);
                            console.log(jsonData);
                            $(".table-status-check tbody").empty();
                            var tr = "";
                            for (var i = 0; i < jsonData.length; i++) {
                                tr += "<tr>";
                                tr += "<td>" + jsonData[i].StaffHandle + " | " + jsonData[i].staffFullname + "</td>";
                                tr += "<td>" + jsonData[i].Value + "</td>";
                                tr += "<td>" + ToJavaScriptDate(jsonData[i].CreatedTime) + "</td>";
                                tr += "</tr>";
                            }
                            
                            $(".table-status-check tbody").append(tr);
                        }
                    })
            }
            function ShowLog(This) {
                if ($("#show-broken-hanle").data('bs.modal')) {
                    debugger;
                    let updateStatusHandle = $(".tabs-update-handle.li-status-handle.active");
                    let updateStatusCheck = $(".tabs-update-handle.li-status-check.active");
                    let type = 0;
                    let key = "";
                    let orderId = 0;
                    if (updateStatusHandle) {
                        ShowLogStatusHandle();
                    }
                    if (updateStatusCheck) {
                        ShowLogStatusCheck();
                    }

                }
            }
            function GetDropdownListData(statusCheckId, statusHandleId, reasonRevokeId) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Infrastructure/OrderBrokenDeviceHandleListing.aspx/GetReasonRevoke",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var jsonData = JSON.parse(data.d);
                        $(".form-control.reason-revoke").empty();
                        var option = "<option value='0'>Chọn lý do hủy</option>";
                        for (var i = 0; i < jsonData.length; i++) {
                            option += "<option value='" + jsonData[i].Value + "' " + (jsonData[i].Value == reasonRevokeId ? "selected='selected'" : "") + ">" + jsonData[i].Label + "</option>";
                        }
                        $(".reason-revoke").append(option);
                    }
                })
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Infrastructure/OrderBrokenDeviceHandleListing.aspx/GetStatusHandle",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var jsonData = JSON.parse(data.d);
                        $(".form-control.status-handle").empty();
                        var option = "";
                        for (var i = 0; i < jsonData.length; i++) {
                            option += "<option value='" + jsonData[i].Value + "' " + (jsonData[i].Value == statusHandleId ? "selected='selected'" : "") + ">" + jsonData[i].Label + "</option>";
                        }
                        $(".status-handle").append(option);
                    }
                })
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Infrastructure/OrderBrokenDeviceHandleListing.aspx/GetStatusCheck",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var jsonData = JSON.parse(data.d);
                        $(".form-control.status-check").empty();
                        var option = "";
                        for (var i = 0; i < jsonData.length; i++) {
                            option += "<option value='" + jsonData[i].Value + "' " + (jsonData[i].Value == statusCheckId ? "selected='selected'" : "") + ">" + jsonData[i].Label + "</option>";
                        }
                        $(".form-control.status-check").append(option);
                    }
                })
            }
            function UpdateStatusHandle(This) {

                let statusHandleId = ~~This.attr("data-status-handle-id");
                let statusCheckId = ~~This.attr("data-status-check-id");
                let reasonRevokeId = ~~This.attr("data-reason-revoke-id");
                $(".number-handling").val(This.attr("data-number-handle"));
                $("#hf-order-id").val(This.attr("data-order-id"));
                $(".form-control.note-revoke").removeAttr("disabled");
                $(".form-control.status-check").removeAttr("disabled");
                $(".form-control.reason-revoke").removeAttr("disabled");
                $(".form-control.status-handle").removeAttr("disabled");
                GetDropdownListData(statusCheckId, statusHandleId, reasonRevokeId);
                if (reasonRevokeId == 0) {
                    $(".form-control.note-revoke").attr("disabled", "disabled");
                }
                //if (statusCheckId != 1) {
                //    $(".form-control.status-check").attr("disabled", "disabled");
                //}
                if (statusHandleId != 1) {
                    $(".form-control.reason-revoke").attr("disabled", "disabled");
                }
                //if (statusHandleId == 3) {
                //    $(".form-control.status-handle").attr("disabled", "disabled");
                //}
                $("#show-broken-hanle").modal();
                ShowLog(This.attr("data-order-id"));
            }
            jQuery(document).ready(function () {
                $('.select').select2();
                // Add active menu
                $("#glbAdminSales").addClass("active");
                $("#glbAdminReportSalary").addClass("active");
                $("li#LuongNhanVien").addClass("active");

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });
            });

            // upload file and encode to base64
            var arrImage = [];
            function fileUpload() {
                let img = "";
                let imgName = "";
                var imgs = $(".form-control.file-upload")[0].files;
                var orderId = $("#hf-order-id").val();
                for (var i = 0; i < imgs.length; i++) {
                    imgName = "HandlingImage_" + 1 + "_OrderId_" + orderId + "_" + Date.now();
                    getBase64(imgs[i], function (base64Data) {
                        let item = {
                            img_name: imgName,
                            img_base64: base64Data
                        };
                        arrImage.push(item);
                    });
                }
            }

            //encode base64
            function getBase64(file, callback) {
                const reader = new FileReader();
                reader.addEventListener('load', () => callback(reader.result));
                reader.readAsDataURL(file);
            }
            function UpdateStatusCheck() {
                var orderId = $("#hf-order-id").val();
                var statusCheck = $(".form-control.status-check").val();
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Infrastructure/OrderBrokenDeviceHandleListing.aspx/UpdateStatusCheckFunc",
                    data: "{orderId:" + orderId + ",statusCheck:" + statusCheck + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        ShowMessage("Thành công", JSON.parse(data.d).message, 1);
                        $(".form-control.status-check").attr("disabled", "disabled");
                        $("#show-broken-hanle").modal("hide");
                    }
                })
            }
            function UpdateReasonRevoke() {
                var orderId = $("#hf-order-id").val();
                var reasonRevoke = $(".form-control.reason-revoke").val();
                var noteRevoke = $(".form-control.note-revoke").val();
                if (reasonRevoke != 0 && (noteRevoke == null || noteRevoke == "")) {
                    ShowMessage("Thông báo", "Vui lòng ghi cụ thể lý do hủy", 4);
                    $(".form-control.note-revoke").focus();
                    $(".form-control.note-revoke").css("border-color", "red");
                    return false;
                }
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Infrastructure/OrderBrokenDeviceHandleListing.aspx/UpdateReasonRevoke",
                    data: "{orderId:" + orderId + ",reasonRevoke:" + reasonRevoke + ",reasonNote:'" + noteRevoke + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        ShowMessage("Thành công", JSON.parse(data.d).message, 1);
                        $(".form-control.status-check").attr("disabled", "disabled");
                        $(".form-control.status-handling").attr("disabled", "disabled");
                        $("#show-broken-hanle").modal("hide");
                    }
                })
            }
            function UpdateStatusHandling() {
                debugger;
                var orderId = $("#hf-order-id").val();
                var numberHandling = $(".form-control.number-handling").val();
                var imgs = $(".form-control.file-upload")[0].files;
                var statusHandle = $(".form-control.status-handle").val();
                var handlingNote = $(".form-control.text-handling-note").val();
                var resultImg = "";
                var arrLinks = [];
                if (statusHandle == 3 && numberHandling == 0) {
                    ShowMessage("Thông báo", "Vui lòng nhập số lần sửa chữa", 4);
                    if (numberHandling == 0) {
                        $(".form-control.number-handling").focus();
                        $(".form-control.number-handling").css("border-color", "red");
                    }
                    return false;
                }
                if (statusHandle == 3 && imgs.length != 0) {
                    let dataImages = {
                        images: arrImage
                    };
                    if (!dataImages.images && $(dataImages.images).size() < 1) {
                        alert("Bạn chưa thêm ảnh sửa chữa (Ít nhất 1 ảnh nhiều nhất 4 ảnh)");
                        return;
                    }
                    resultImg = uploadImagesToS3(dataImages);
                    $.each(resultImg.images, function (i, v) {
                        let link = v.link;
                        if (link) {
                            arrLinks.push(link);
                        }
                    });
                }
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Infrastructure/OrderBrokenDeviceHandleListing.aspx/UpdateStatusHandling",
                    data: "{orderId:" + orderId + ",statusHandle:" + statusHandle + ",numberHandling:" + numberHandling + ",imgsHandling:'" + arrLinks.toString() + "',handlingNote:'" + handlingNote + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        ShowMessage("Thành công", JSON.parse(data.d).message, 1);
                        $(".form-control.status-check").attr("disabled", "disabled");
                        $(".form-control.status-handling").attr("disabled", "disabled");
                        $("#show-broken-hanle").modal("hide");
                    }
                })
            }


            //s3
            function uploadImagesToS3(data) {
                let result = '';
                let url = "https://api-upload-s3.30shine.com/api/s3/multiUpload";
                $.ajax({
                    contentType: "application/json",
                    type: "POST",
                    url: url,
                    dataType: "json",
                    async: false,
                    crossDomain: true,
                    data: JSON.stringify(data),
                    success: function (response, textStatus, xhr) {
                        if (response) {
                            result = response;
                        }
                    },
                    error: function (jqXhr, textStatus, errorMessager) {
                        console.log(errorMessager + "\n" + jqXhr.responseJSON);
                        alert("upload images lên S3 bị lỗi. Bạn hãy thông báo với Admin");
                    }
                });
                console.log(result);
                return result;
            }
        </script>
    </asp:Panel>
</asp:Content>
