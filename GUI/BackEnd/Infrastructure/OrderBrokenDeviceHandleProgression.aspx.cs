﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.Infrastructure
{
    public partial class OrderBrokenDeviceHandleProgression : System.Web.UI.Page
    {
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        protected static bool ViewAllData = false;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                ViewAllData = Perm_ViewAllData;
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                BindRegion();
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
            }
        }

        /// <summary>
        /// Bind Region to ddl
        /// </summary>
        protected void BindRegion()
        {
            using (var db = new Solution_30shineEntities())
            {
                //chỉ lấy bộ phận stylist, skinner
                var types = db.TinhThanhs.OrderBy(o => o.ID).ToList();
                var item = new TinhThanh();
                item.ID = 0;
                item.TenTinhThanh = "Chọn khu vực";
                types.Insert(0, item);
                ddlRegion.DataTextField = "TenTinhThanh";
                ddlRegion.DataValueField = "Id";
                ddlRegion.DataSource = types;
                ddlRegion.DataBind();
            }
        }

        [WebMethod]
        public static List<Tbl_Salon> BindSalonByRegion(int RegionId)
        {
            using (var db = new Solution_30shineEntities())
            {
                //chỉ lấy bộ phận stylist, skinner
                if (RegionId < 0)
                {
                    return new List<Tbl_Salon>();
                }
                var listSalon = new List<Tbl_Salon>();
                var SalonId = Convert.ToInt32(HttpContext.Current.Session["SalonId"]);
                var UserId = Convert.ToInt32(HttpContext.Current.Session["User_Id"]);
                if (ViewAllData)
                {
                    listSalon = db.Tbl_Salon.Where(w => w.IsDelete == 0 && w.Publish == true && ((w.CityId == RegionId) || (RegionId == 0))).OrderByDescending(o => o.CityId).ToList();
                }
                else
                {
                    listSalon = (
                                        from p in db.PermissionSalonAreas.AsNoTracking()
                                        join s in db.Tbl_Salon.AsNoTracking() on p.SalonId equals s.Id
                                        where
                                        p.IsDelete == false && p.StaffId == UserId
                                        && s.Publish == true && s.IsDelete == 0 && ((s.CityId == RegionId) || (RegionId == 0))
                                        select s
                                   ).OrderByDescending(o => o.CityId).ToList();
                    if (listSalon.Count <= 0)
                    {
                        listSalon = db.Tbl_Salon.Where(w => w.Id == SalonId).ToList();
                    }
                }
                return listSalon;
            }
        }

        public List<Tbl_Config> BindConfigForTh()
        {
            using (var db = new Solution_30shineEntities())
            {
                return db.Tbl_Config.Where(w => w.IsDelete == 0 && w.Status == 1 && w.Key == "order_device_category").OrderBy(o => o.Id).ToList();
            }
        }

        [WebMethod]
        public static ResponseData GetData(string timeFrom, string timeTo, List<int> salon)
        {
            using (var db = new Solution_30shineEntities())
            {
                DateTime TimeFrom = new DateTime();
                DateTime TimeTo = new DateTime();
                var response = new ResponseData();
                IOrderDeviceBorkenHandlingModel model = new OrderDeviceBorkenHandlingModel();
                if (String.IsNullOrEmpty(timeFrom) || salon.Count <= 0)
                {
                    response.status = false;
                    response.message = "Data does not exist!";
                    return response;
                }
                TimeFrom = Convert.ToDateTime(timeFrom, new CultureInfo("vi-VN"));
                if (String.IsNullOrEmpty(timeTo))
                {
                    TimeTo = TimeFrom.AddDays(1);
                }
                else
                {
                    TimeTo = Convert.ToDateTime(timeTo, new CultureInfo("vi-VN"));
                    if (TimeTo == TimeFrom)
                    {
                        TimeTo = TimeTo.AddDays(1);
                    }
                    else
                    {
                        TimeTo = TimeTo.AddDays(1);
                    }
                }
                var list = model.GetListProgression(TimeFrom, TimeTo, salon);
                if (list.Count > 0)
                {
                    response.status = true;
                    response.message = "Lấy dữ liệu thành công!";
                    response.data = list;
                }
                else
                {
                    response.status = false;
                    response.message = "Không có dữ liệu!";
                    response.data = new List<ProgressCompleteData>();
                }
                return response;
            }
        }

        [WebMethod]
        public static ResponseData GetProgression(string timeFrom, string timeTo, string region, List<int> salon)
        {
            using (var db = new Solution_30shineEntities())
            {
                DateTime TimeFrom = new DateTime();
                DateTime TimeTo = new DateTime();
                var response = new ResponseData();
                response.progressions = new List<Progression>();
                IOrderDeviceBorkenHandlingModel model = new OrderDeviceBorkenHandlingModel();
                if (String.IsNullOrEmpty(timeFrom) || salon.Count <= 0)
                {
                    response.status = false;
                    response.message = "Data does not exist!";
                    return response;
                }
                TimeFrom = Convert.ToDateTime(timeFrom, new CultureInfo("vi-VN"));
                if (String.IsNullOrEmpty(timeTo))
                {
                    TimeTo = TimeFrom.AddDays(1);
                }
                else
                {
                    TimeTo = Convert.ToDateTime(timeTo, new CultureInfo("vi-VN"));
                    if (TimeTo == TimeFrom)
                    {
                        TimeTo = TimeTo.AddDays(1);
                    }
                    else
                    {
                        TimeTo = TimeTo.AddDays(1);
                    }
                }
                response.status = true;
                response.message = "Lấy dữ liệu thành công";
                response.progressions = model.GetProgression(TimeFrom, TimeTo, region,salon);
                return response;
            }
        }
    }


    public class ResponseData
    {
        public bool status { get; set; }
        public string message { get; set; }
        public List<ProgressCompleteData> data { get; set; }
        public List<Progression> progressions { get; set; }
    }

    public class ProgressCompleteData
    {
        public int SalonId { get; set; }
        public string SalonName { get; set; }
        public string TONGVALUE { get; set; }
        public string HETHONGDIENVALUE { get; set; }
        public string HETHONGNUOCVALUE { get; set; }
        public string HETHONGDENVALUE { get; set; }
        public string GHEGUONGCATVALUE { get; set; }
        public string TRANTUONGSANCUAVALUE { get; set; }
        public string GUONGGOIXAVALUE { get; set; }
        public string SUNGHHVALUE { get; set; }
        public string HUTMUNVALUE { get; set; }
        public string DIEUHOAVALUE { get; set; }
        public string THANGMAYVALUE { get; set; }
        public string WCVALUE { get; set; }
        public string LETANVALUE { get; set; }
        public string BINHNONGLANHVALUE { get; set; }
        public string MAYGIATVALUE { get; set; }
        public string KHUVUCKHACVALUE { get; set; }
        public string AMLYVALUE { get; set; }
        public string TONGLABEL { get; set; }
        public string HETHONGDIENLABEL { get; set; }
        public string HETHONGNUOCLABEL { get; set; }
        public string HETHONGDENLABEL { get; set; }
        public string GHEGUONGCATABEL { get; set; }
        public string TRANTUONGSANCUALABEL { get; set; }
        public string GUONGGOIXALABEL { get; set; }
        public string SUNGHHLABEL { get; set; }
        public string HUTMUNLABEL { get; set; }
        public string DIEUHOALABEL { get; set; }
        public string THANGMAYLABEL { get; set; }
        public string WCLABEL { get; set; }
        public string LETANLABEL { get; set; }
        public string BINHNONGLANHLABEL { get; set; }
        public string MAYGIATLABEL { get; set; }
        public string KHUVUCKHACLABEL { get; set; }
        public string AMLYLABEL { get; set; }
        public int TONGQUANTITY { get; set; }
        public int HETHONGDIENQUANTITY { get; set; }
        public int HETHONGNUOCQUANTITY { get; set; }
        public int HETHONGDENQUANTITY { get; set; }
        public int GHEGUONGCATQUANTITY { get; set; }
        public int TRANTUONGSANCUAQUANTITY { get; set; }
        public int GUONGGOIXAQUANTITY { get; set; }
        public int SUNGHHQUANTITY { get; set; }
        public int HUTMUNQUANTITY { get; set; }
        public int DIEUHOAQUANTITY { get; set; }
        public int THANGMAYQUANTITY { get; set; }
        public int WCQUANTITY { get; set; }
        public int LETANQUANTITY { get; set; }
        public int BINHNONGLANHQUANTITY { get; set; }
        public int MAYGIATQUANTITY { get; set; }
        public int KHUVUCKHACQUANTITY { get; set; }
        public int AMLYQUANTITY { get; set; }
    }

    public class Progression
    {
        public string RegionName { get; set; }
        public int TotalCorrect { get; set; }
        public int DeadlineCorrect { get; set; }
        public double PercentCorrect { get; set; }
    }
}