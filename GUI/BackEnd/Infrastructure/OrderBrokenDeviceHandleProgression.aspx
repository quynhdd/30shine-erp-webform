﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMasterVersion1.Master" AutoEventWireup="true" CodeBehind="OrderBrokenDeviceHandleProgression.aspx.cs" Inherits="_30shine.GUI.BackEnd.Infrastructure.OrderBrokenDeviceHandleProgression" %>

<asp:Content ID="ContentMain" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel ID="ContentWrap" runat="server" ClientIDMode="Static">
        <style>
            th, td {
                text-align: center;
            }

            body, html {
                height: 91%;
            }

            .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
                color: #fff;
                background-color: black;
            }

            .shadow-sm {
                margin-bottom: 15px;
                min-height: 700px;
            }

            .form-box-left {
                height: 31px;
                width: 185px;
            }

            .ul-box-left .li-box-left {
                margin-bottom: 8px;
                font-family: Roboto Condensed Regular !important;
                font-size: 14px !important;
            }

            .btn-group {
                margin-top: 21px !important;
            }

            .fa-times {
                font-size: 17px;
                color: red;
            }

                .fa-times:hover {
                    cursor: pointer;
                }

            .td-resize {
                width: 50px;
                text-align: center;
            }

            .btn-outline-secondary {
                color: #000000;
                border-color: #000000;
            }

                .btn-outline-secondary:hover {
                    background: #000000;
                }

            .table td, .table th {
                vertical-align: middle;
            }

            .quantity-left {
                text-align: center;
            }

            .thead-dark {
                font-family: Roboto Condensed Bold;
            }

            .select-list-item {
                width: 100%;
                font-size: 14px;
                height: calc(2.25rem + 2px);
                line-height: 1.5;
                color: #495057;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #ced4da;
                border-radius: .25rem;
                transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
            }

                .select-list-item option {
                    font-size: 14px;
                }

            #timefrom, #timeto, #selectdate {
                height: 31px;
            }

            .table .thead-dark th {
                color: #fff;
                background-color: #333;
                border-color: white;
            }

            .form-control {
                font-size: 14px;
                padding: 0 0.75rem;
            }

            #tablecontent{
                position:relative;
                z-index:100;
            }
        </style>
        <section>
            <div class="container-fluid sub-menu">
                <div class="row">
                    <div class="col-md-12 pt-3">
                        <ul>
                            <li>BÁO CÁO&nbsp;&#187;</li>
                            <li><a href="/admin/xu-ly-su-co/them-moi-yeu-cau.html">&nbsp<i class="fas fa-plus"></i>&nbsp THÊM MỚI YC XỬ LÝ CSVC</a></li>
                            <li><a href="/admin/xu-ly-su-co/thong-ke-su-co.html">&nbsp<i class="fa fa-list-ul"></i>&nbsp THỐNG KÊ SỰ CỐ</a></li>
                            <li><a href="#"><b>&nbsp<i class="fa fa-list-ul"></i>&nbsp TĐHT YÊU CẦU XỬ LÝ CSVC</b></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section style="margin-top: 10px;">
            <form runat="server">
                <div class="container-fluid sub-menu">
                    <div class="col-lg-12 float-left">
                        <div class="card mb-6 shadow-sm">
                            <div class="card-body">
                                <div class="d-flex justify-content-between align-items-center">
                                    <ul class="ul-box-left">
                                        <li class="li-box-left">
                                            <span>
                                                <span>Từ ngày <span style="color: red">*</span></span>
                                                <input type="text" id="timefrom" placeholder="Vd: 01/11/2018" class="datetime-picker form-control" style="width: 185px" />
                                            </span>
                                        </li>
                                        <li class="li-box-left">
                                            <span>
                                                <span>Đến ngày <span style="color: red">*</span></span>
                                                <input type="text" id="timeto" placeholder="Vd: 30/11/2018" class="datetime-picker form-control" style="width: 185px" />
                                            </span>
                                        </li>
                                        <li class="li-box-left">
                                            <span>
                                                <span>Khu vực <span style="color: red">*</span></span>
                                                <asp:DropDownList runat="server" ID="ddlRegion" CssClass="form-control select form-box-left" ClientIDMode="Static" onchange="GetDropDownData($(this));"></asp:DropDownList>
                                            </span>
                                        </li>
                                        <li class="li-box-left">
                                            <span>
                                                <span>Salon <span style="color: red">*</span></span>
                                                <asp:DropDownList runat="server" ID="ddlSalon" CssClass="form-control select form-box-left" ClientIDMode="Static"></asp:DropDownList>
                                            </span>
                                        </li>
                                        <li class="li-box-left">
                                            <div class="btn-group" style="width: 110px">
                                                <button type="button" class="btn btn-sm btn-outline-secondary" style="width: 100%" onclick="GetCompleteListing()">Xem dữ liệu</button>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div id="clickme" class="btn btn-outline-secondary" style="margin-bottom:10px">
                                    Hiển thị bảng thống kê
                                </div>
                                <table id="tableprogression" class="table table-bordered" style="width: 30%; display: none;">
                                    <thead>
                                        <tr>
                                            <th scope="col"></th>
                                            <th scope="col">Tổng sự cố <br />( Không bị huỷ )</th>
                                            <th scope="col">Việc đúng hạn</th>
                                            <th scope="col">Tỉ lệ đúng (%)</th>
                                        </tr>
                                    </thead>
                                    <tbody id="bodyprogress">
                                        
                                    </tbody>
                                </table>
                                <div class="row max-box">
                                    <table id="tablecontent" class="table table-bordered table-striped">
                                        <thead class="thead-dark" id="thead">
                                            <tr>
                                                <th scope="col" class="td-resize">STT</th>
                                                <th scope="col">Salon</th>
                                                <th scope="col">Tổng</th>
                                                <%foreach (var item in BindConfigForTh())
                                                    {%>
                                                <th scope="col" data-value="<%=item.Value %>"><%=item.Label %></th>
                                                <%} %>
                                            </tr>
                                        </thead>
                                        <tbody id="bodycontent">
                                            <%--<tr>
                                                <th scope="col">STT</th>
                                                <td>Bộ phận</td>
                                                <td>Số lượng</td>
                                                <td>Số lượng đáp ứng tiêu chuẩn</td>
                                                <td>Tỉ lệ đáp ứng tiêu chuẩn thời gian</td>
                                                <td>Thời gian trung bình đáp ứng</td>
                                            </tr>--%>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <!-- Page loading -->
            <div class="page-loading">
                <p>Vui lòng đợi trong giây lát...</p>
            </div>
        </section>
        <script type="text/javascript">
            var SalonIds = [];
            function CheckParameter(param) {
                if (param === undefined || param === null || param === '' || param === '0') {
                    return false
                }
                return true;
            }

            $("#clickme").click(function () {
                if (!$(this).hasClass('active')) {
                    $(this).addClass('active');
                    $(this).text('Ẩn bảng thống kê');
                    $("#tableprogression").slideDown('slow', function () {});
                }
                else {
                    $(this).removeClass('active');
                    $(this).text('Hiển thị bảng thống kê');
                    $("#tableprogression").slideUp('slow', function () { });
                }
            });

            function GetDropDownData(This) {
                let $region = This.val();
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Infrastructure/OrderBrokenDeviceHandleProgression.aspx/BindSalonByRegion",
                    data: '{RegionId: ' + $region + ' }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $("#ddlSalon").html('').val('0');
                        $("#select2-ddlSalon-container").val('0').text('Chọn salon');
                        $("#ddlSalon").append($("<option />").val(0).text('Chọn salon'));
                        $.each(data.d, function () {
                            $("#ddlSalon").append($("<option />").val(this.Id).text(this.Name));
                        });
                    },
                    failure: function () {
                        ShowMessage('', 'Có lỗi xảy ra, vui lòng liên hệ nhà phát triển!', 4);
                    }
                });
            }

            function GetSalonIds() {
                let Ids = [];
                $('#ddlSalon option').each(function () {
                    if (CheckParameter(this.value)) {
                        let salonId = parseInt(this.value);
                        Ids.push(salonId);
                    }
                });
                SalonIds = Ids;
            }

            function GetCompleteListing() {
                let $success = true,
                    $dateFrom = $('#timefrom').val(),
                    $dateTo = $('#timeto').val(),
                    $salon = $('#ddlSalon :selected').val(),
                    $region = $('#ddlRegion :selected'),
                    $regionName = '';
                GetSalonIds();
                if (!CheckParameter($dateFrom)) {
                    ShowMessage('', 'Bạn chưa chọn thời gian bắt đầu!', 3);
                    $success = false;
                }
                if ($salon === '0') {
                    if (SalonIds.length === 0) {
                        ShowMessage('', 'Khu vực này chưa có salon!', 4);
                        return;
                    }
                }
                else {
                    SalonIds = [];
                    SalonIds.push(parseInt($salon));
                }
                if ($region.val() === '0') {
                    $regionName = 'Toàn bộ khu vực';
                }
                else {
                    $regionName = $region.text();
                }
                if ($success && SalonIds.length > 0) {
                    startLoading();
                    var data = JSON.stringify({ timeFrom: $dateFrom, timeTo: $dateTo, region: $regionName,salon: SalonIds });
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/Infrastructure/OrderBrokenDeviceHandleProgression.aspx/GetProgression",
                        data: data,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            if (data.d.status) {
                                $('#bodyprogress').html('');
                                let str = '',
                                    index = 1;
                                for (var i = 0; i < data.d.progressions.length; i++) {
                                    str += `<tr>
                                                <th class="th-name" scope="row">${data.d.progressions[i].RegionName}</th>
                                                <th class="th-total" scope="row">${data.d.progressions[i].TotalCorrect}</th>
                                                <th class="th-correct" scope="row">${data.d.progressions[i].DeadlineCorrect}</th>
                                                <th class="th-percent" scope="row">${data.d.progressions[i].PercentCorrect.toString().substring(0, 4)}</th>
                                            </tr>`;
                                }
                                $('#bodyprogress').append(str);
                            }
                        },
                        failure: function () {
                            ShowMessage('', 'Có lỗi xảy ra, vui lòng liên hệ nhà phát triển!', 4);
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/Infrastructure/OrderBrokenDeviceHandleProgression.aspx/GetData",
                        data: data,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            finishLoading();
                            if (data.d.status) {
                                $('#bodycontent').html('');
                                let str = '',
                                    index = 1;
                                for (var i = 0; i < data.d.data.length; i++) {
                                    str += `<tr>
                                                <th scope="col" class="td-resize">${index++}</th>
                                                <td data-salon='${data.d.data[i].SalonId}'>${data.d.data[i].SalonName}</td>
                                                <td style='background-color: ${data.d.data[i].TONGQUANTITY > 0 ? "#93F9B9" : ""};' data-tongvalue='${data.d.data[i].TONGVALUE}'>${data.d.data[i].TONGQUANTITY}</td>
                                                <td style='background-color: ${data.d.data[i].HETHONGDIENQUANTITY > 0 ? "#F7BB97" : ""};' data-hethongdienvalue='${data.d.data[i].HETHONGDIENVALUE}'>${data.d.data[i].HETHONGDIENQUANTITY}</td>
                                                <td style='background-color: ${data.d.data[i].HETHONGNUOCQUANTITY > 0 ? "#F7BB97" : ""};' data-hethongnuocvalue='${data.d.data[i].HETHONGNUOCVALUE}'>${data.d.data[i].HETHONGNUOCQUANTITY}</td>
                                                <td style='background-color: ${data.d.data[i].HETHONGDENQUANTITY > 0 ? "#F7BB97" : ""};' data-hethongdenvalue='${data.d.data[i].HETHONGDENVALUE}'>${data.d.data[i].HETHONGDENQUANTITY}</td>
                                                <td style='background-color: ${data.d.data[i].GHEGUONGCATQUANTITY > 0 ? "#F7BB97" : ""};' data-gheguongcatvalue='${data.d.data[i].GHEGUONGCATVALUE}'>${data.d.data[i].GHEGUONGCATQUANTITY}</td>
                                                <td style='background-color: ${data.d.data[i].TRANTUONGSANCUAQUANTITY > 0 ? "#F7BB97" : ""};' data-trantuongsancuavalue='${data.d.data[i].TRANTUONGSANCUAVALUE}'>${data.d.data[i].TRANTUONGSANCUAQUANTITY}</td>
                                                <td style='background-color: ${data.d.data[i].GUONGGOIXAQUANTITY > 0 ? "#F7BB97" : ""};' data-guonggoixavalue='${data.d.data[i].GUONGGOIXAVALUE}'>${data.d.data[i].GUONGGOIXAQUANTITY}</td>
                                                <td style='background-color: ${data.d.data[i].SUNGHHQUANTITY > 0 ? "#F7BB97" : ""};' data-sunghhvalue='${data.d.data[i].SUNGHHVALUE}'>${data.d.data[i].SUNGHHQUANTITY}</td>
                                                <td style='background-color: ${data.d.data[i].HUTMUNQUANTITY > 0 ? "#F7BB97" : ""};' data-hutmunvalue='${data.d.data[i].HUTMUNVALUE}'>${data.d.data[i].HUTMUNQUANTITY}</td>
                                                <td style='background-color: ${data.d.data[i].DIEUHOAQUANTITY > 0 ? "#F7BB97" : ""};' data-dieuhoavalue='${data.d.data[i].DIEUHOAVALUE}'>${data.d.data[i].DIEUHOAQUANTITY}</td>
                                                <td style='background-color: ${data.d.data[i].THANGMAYQUANTITY > 0 ? "#F7BB97" : ""};' data-thangmayvalue='${data.d.data[i].THANGMAYVALUE}'>${data.d.data[i].THANGMAYQUANTITY}</td>
                                                <td style='background-color: ${data.d.data[i].WCQUANTITY > 0 ? "#F7BB97" : ""};' data-wcvalue='${data.d.data[i].WCVALUE}'>${data.d.data[i].WCQUANTITY}</td>
                                                <td style='background-color: ${data.d.data[i].LETANQUANTITY > 0 ? "#F7BB97" : ""};' data-letanvalue='${data.d.data[i].LETANVALUE}'>${data.d.data[i].LETANQUANTITY}</td>
                                                <td style='background-color: ${data.d.data[i].BINHNONGLANHQUANTITY > 0 ? "#F7BB97" : ""};' data-binhnonglanhvalue='${data.d.data[i].BINHNONGLANHVALUE}'>${data.d.data[i].BINHNONGLANHQUANTITY}</td>
                                                <td style='background-color: ${data.d.data[i].MAYGIATQUANTITY > 0 ? "#F7BB97" : ""};' data-maygiatvalue='${data.d.data[i].MAYGIATVALUE}'>${data.d.data[i].MAYGIATQUANTITY}</td>
                                                <td style='background-color: ${data.d.data[i].KHUVUCKHACQUANTITY > 0 ? "#F7BB97" : ""};' data-khuvuckhacvalue='${data.d.data[i].KHUVUCKHACVALUE}'>${data.d.data[i].KHUVUCKHACQUANTITY}</td>
                                                <td style='background-color: ${data.d.data[i].AMLYQUANTITY > 0 ? "#F7BB97" : ""};' data-khuvuckhacvalue='${data.d.data[i].AMLYVALUE}'>${data.d.data[i].AMLYQUANTITY}</td>
                                            </tr>`;
                                }
                                $('#bodycontent').append(str);
                                ShowMessage('', data.d.message, 2);
                            }
                            else {
                                ShowMessage('', data.d.message, 4);
                            }
                        },
                        failure: function () {
                            ShowMessage('', 'Có lỗi xảy ra, vui lòng liên hệ nhà phát triển!', 4);
                        }
                    });
                }
            }
        </script>
    </asp:Panel>
</asp:Content>
