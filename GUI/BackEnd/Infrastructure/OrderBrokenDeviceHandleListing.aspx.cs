﻿using _30shine.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Linq.Expressions;
using ExportToExcel;
using System.Data.SqlClient;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using System.Net.Http;
using System.Data;
using _30shine.Helpers.Http;
using System.Web.Services;
using System.Data.Entity.Migrations;

namespace _30shine.GUI.BackEnd.Infrastructure
{
    public partial class OrderBrokenDeviceHandleListing : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        public CultureInfo culture = new CultureInfo("vi-VN");
        public bool Perm_ViewAllData = false;
        public static bool ViewAllData = true;
        protected bool Perm_Add = true;
        protected bool Perm_Edit = true;
        protected bool Perm_Delete = true;
        private bool Perm_Access = false;
        protected bool isRoot = false;
        protected int departmentId;
        public double? salaryTotal = 0;
        protected string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
        protected string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
        private Solution_30shineEntities db = new Solution_30shineEntities();
        private IOrderDeviceBorkenHandlingModel orderDeviceBorken = new OrderDeviceBorkenHandlingModel();


        /// <summary>
        /// set permission
        /// </summary>
        public void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Add = permissionModel.CheckPermisionByAction("Perm_Add", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                ViewAllData = true;
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        /// <summary>
        /// ExecuteByPermission
        /// </summary>
        private void ExecuteByPermission()
        {

            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
                
            }
            if (!Perm_Add)
            {
                UpdateStatusHandle.Visible = false;
            }
            if (!Perm_Edit)
            {
                UpdateStatusCheck.Visible = false;
            }
            if (!Perm_Delete)
            {
                ReasonRevokePanel.Visible = false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = FirstDayOfMonth;
                TxtDateTimeTo.Text = Today;
                Library.Function.bindSalon_NotHoiQuan(new List<DropDownList> { Salon }, true);
                ListCategory();
                ListPriority();
                ListStatus();
                ListStatusCheck();
                ListStatusRevoke();
                ListRegion();
                ddlDate.SelectedValue = "1";
                RemoveLoading();
            }
        }

        protected void ListRegion()
        {
            using (var db = new Solution_30shineEntities())
            {

                var region = db.TinhThanhs.ToList();
                var _region = new TinhThanh();
                _region.ID = 0;
                _region.TenTinhThanh = "Tất cả tỉnh thành";
                region.Insert(0, _region);
                ddlRegion.DataTextField = "TenTinhThanh";
                ddlRegion.DataValueField = "ID";
                ddlRegion.DataSource = region;
                ddlRegion.DataBind();
                ddlRegion.SelectedIndex = 0;
            }
        }
        protected void ListCategory()
        {
            using (var db = new Solution_30shineEntities())
            {

                var status = db.Tbl_Config.Where(w => w.Key == "order_device_category" && w.IsDelete == 0).ToList();
                var first = new Tbl_Config();
                first.Value = 0.ToString();
                first.Label = "Danh mục";
                status.Insert(0, first);
                ddlCategory.DataTextField = "Label";
                ddlCategory.DataValueField = "Value";
                ddlCategory.DataSource = status;
                ddlCategory.DataBind();
                ddlCategory.SelectedIndex = 0;
            }
        }
        /// <summary>
        /// Bind Department to ddl
        /// </summary>
        [WebMethod]
        public static List<Tbl_Salon> BindSalonByRegion(int RegionId)
        {
            using (var db = new Solution_30shineEntities())
            {
                //chỉ lấy bộ phận stylist, skinner
                if (RegionId < 0)
                {
                    return new List<Tbl_Salon>();
                }
                var listSalon = new List<Tbl_Salon>();
                var SalonId = Convert.ToInt32(HttpContext.Current.Session["SalonId"]);
                var UserId = Convert.ToInt32(HttpContext.Current.Session["User_Id"]);
                if (ViewAllData)
                {
                    listSalon = db.Tbl_Salon.Where(w => w.IsDelete == 0 && w.Publish == true && ((w.CityId == RegionId) || (RegionId == 0))).OrderByDescending(o => o.CityId).ToList();
                }
                else
                {
                    listSalon = (
                                        from p in db.PermissionSalonAreas.AsNoTracking()
                                        join s in db.Tbl_Salon.AsNoTracking() on p.SalonId equals s.Id
                                        where
                                        p.IsDelete == false && p.StaffId == UserId
                                        && s.Publish == true && s.IsDelete == 0 && ((s.CityId == RegionId) || (RegionId == 0))
                                        select s
                                   ).OrderByDescending(o => o.CityId).ToList();
                    if (listSalon.Count <= 0)
                    {
                        listSalon = db.Tbl_Salon.Where(w => w.Id == SalonId).ToList();
                    }
                }
                return listSalon;
            }
        }
        protected void ListPriority()
        {
            using (var db = new Solution_30shineEntities())
            {

                var status = db.Tbl_Config.Where(w => w.Key == "order_device_priority" && w.IsDelete == 0).ToList();
                var first = new Tbl_Config();
                first.Value = 0.ToString();
                first.Label = "Độ ưu tiên";
                status.Insert(0, first);
                ddlPriority.DataTextField = "Label";
                ddlPriority.DataValueField = "Value";
                ddlPriority.DataSource = status;
                ddlPriority.DataBind();
                ddlPriority.SelectedIndex = 0;
            }
        }

        protected void ListStatus()
        {
            using (var db = new Solution_30shineEntities())
            {

                var status = db.Tbl_Config.Where(w => w.Key == "order_device_status_handle" && w.IsDelete == 0).ToList();
                var first = new Tbl_Config();
                first.Value = 0.ToString();
                first.Label = "Trạng thái xử lý";
                status.Insert(0, first);
                ddlStatus.DataTextField = "Label";
                ddlStatus.DataValueField = "Value";
                ddlStatus.DataSource = status;
                ddlStatus.DataBind();
                ddlStatus.SelectedIndex = 0;
            }
        }

        protected void ListStatusCheck()
        {
            using (var db = new Solution_30shineEntities())
            {

                var status = db.Tbl_Config.Where(w => w.Key == "order_device_status_check" && w.IsDelete == 0).ToList();
                var first = new Tbl_Config();
                first.Value = 0.ToString();
                first.Label = "Trạng thái nghiệm thu";
                status.Insert(0, first);
                ddlStatusCheck.DataTextField = "Label";
                ddlStatusCheck.DataValueField = "Value";
                ddlStatusCheck.DataSource = status;
                ddlStatusCheck.DataBind();
                ddlStatusCheck.SelectedIndex = 0;
            }
        }
        protected void ListStatusRevoke()
        {
            using (var db = new Solution_30shineEntities())
            {

                var status = db.Tbl_Config.Where(w => w.Key == "order_device_reason_revoke" && w.IsDelete == 0).ToList();
                var first = new Tbl_Config();
                first.Value = 0.ToString();
                first.Label = "Trạng thái hủy";
                status.Insert(0, first);
                ddlStatusRevoke.DataTextField = "Label";
                ddlStatusRevoke.DataValueField = "Value";
                ddlStatusRevoke.DataSource = status;
                ddlStatusRevoke.DataBind();
                ddlStatusRevoke.SelectedIndex = 0;
            }
        }
        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging(GetData().Count());
            rptSalary.DataSource = GetData().Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
            rptSalary.DataBind();
            RemoveLoading();
        }

        private List<HandleObject> GetData()
        {

            var timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
            var timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
            var region = Convert.ToInt32(ddlRegion.SelectedValue);
            var salon = Convert.ToInt32(Salon.SelectedValue);
            var date = Convert.ToInt32(ddlDate.SelectedValue);
            var status = Convert.ToInt32(ddlStatus.SelectedValue);
            var statusCheck = Convert.ToInt32(ddlStatusCheck.SelectedValue);
            var statusRevoke = Convert.ToInt32(ddlStatusRevoke.SelectedValue);
            var category = Convert.ToInt32(ddlCategory.SelectedValue);
            var priority = Convert.ToInt32(ddlPriority.SelectedValue);
            var fixerId = Convert.ToInt32(txtFixed.Text == null || txtFixed.Text == "" ? "0" : txtFixed.Text);
            var broken = new List<OrderBrokenDeviceHandling>();
            var completeStatus = Convert.ToInt32(ddlCompleteStatus.SelectedValue);
            //switch (region)
            //{
            //    #region case region #0
            //    case 0:
            switch (fixerId)
                    {
                        case 0:
                            switch (date)
                            {
                                #region date case #1
                                case 1:
                                    switch (salon)
                                    {
                                        case 0:
                                            switch (status)
                                            {
                                                case 0:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.CategoryId == category).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.CategoryId == category).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.ReasonRevoke == statusRevoke).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.CategoryId == category && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                                default:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.CategoryId == category && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.StatusCheck == statusCheck
                                                                                                                        && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.CategoryId == category && w.StatusCheck == statusCheck
                                                                                                                        && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                            }
                                            break;
                                        default:
                                            switch (status)
                                            {
                                                case 0:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.CategoryId == category && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                            && w.StatusCheck == statusCheck && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.CategoryId == category && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.StatusCheck == statusCheck && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom
                                                                                                                        && w.CreatedDate < timeTo && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                                default:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.CategoryId == category && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.ReasonRevoke == statusRevoke && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.CategoryId == category && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                            }
                                            break;
                                    }
                                    break;
                                #endregion date case #1
                                #region date case #2
                                case 2:
                                    switch (salon)
                                    {
                                        case 0:
                                            switch (status)
                                            {
                                                case 0:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.CategoryId == category).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.CategoryId == category).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.ReasonRevoke == statusRevoke).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.CategoryId == category && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                                default:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.CategoryId == category && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.StatusCheck == statusCheck
                                                                                                                        && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.CategoryId == category && w.StatusCheck == statusCheck
                                                                                                                        && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                            }
                                            break;
                                        default:
                                            switch (status)
                                            {
                                                case 0:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.CategoryId == category && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                            && w.StatusCheck == statusCheck && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.CategoryId == category && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.StatusCheck == statusCheck && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom
                                                                                                                        && w.CreatedDate < timeTo && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                                default:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.CategoryId == category && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.ReasonRevoke == statusRevoke && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.CategoryId == category && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                            }
                                            break;
                                    }
                                    break;
                                #endregion date case #2
                                #region date case #3
                                case 3:
                                    switch (salon)
                                    {
                                        case 0:
                                            switch (status)
                                            {
                                                case 0:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.CategoryId == category).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.ReasonRevoke == statusRevoke).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.CategoryId == category && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                                default:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.CategoryId == category && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.StatusCheck == statusCheck
                                                                                                                        && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.CategoryId == category && w.StatusCheck == statusCheck
                                                                                                                        && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                            }
                                            break;
                                        default:
                                            switch (status)
                                            {
                                                case 0:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.CategoryId == category && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                            && w.StatusCheck == statusCheck && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.CategoryId == category && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.StatusCheck == statusCheck && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom
                                                                                                                        && w.CreatedDate < timeTo && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                                default:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.CategoryId == category && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.ReasonRevoke == statusRevoke && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.CategoryId == category && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                            }
                                            break;
                                    }
                                    break;
                                #endregion date case #3
                                #region default date
                                default:
                                    switch (salon)
                                    {
                                        case 0:
                                            switch (status)
                                            {
                                                case 0:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.CategoryId == category).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.ReasonRevoke == statusRevoke).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.CategoryId == category && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                                default:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.CategoryId == category && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.StatusCheck == statusCheck
                                                                                                                        && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.CategoryId == category && w.StatusCheck == statusCheck
                                                                                                                        && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                            }
                                            break;
                                        default:
                                            switch (status)
                                            {
                                                case 0:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.CategoryId == category && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                            && w.StatusCheck == statusCheck && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.CategoryId == category && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.StatusCheck == statusCheck && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom
                                                                                                                        && w.CreatedDate < timeTo && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                                default:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.CategoryId == category && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.ReasonRevoke == statusRevoke && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.CategoryId == category && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                            }
                                            break;
                                    }
                                    break;
                            }
                            break;
                        #endregion default date
                        default:
                            switch (date)
                            {
                                #region date case #1
                                case 1:
                                    switch (salon)
                                    {
                                        case 0:
                                            switch (status)
                                            {
                                                case 0:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.CategoryId == category && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.ReasonRevoke == statusRevoke && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.CategoryId == category && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                                default:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.CategoryId == category && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.StatusCheck == statusCheck
                                                                                                                        && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.CategoryId == category && w.StatusCheck == statusCheck
                                                                                                                        && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                            }
                                            break;
                                        default:
                                            switch (status)
                                            {
                                                case 0:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.CategoryId == category && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                            && w.StatusCheck == statusCheck && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.CategoryId == category && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.StatusCheck == statusCheck && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom
                                                                                                                        && w.CreatedDate < timeTo && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                                default:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.CategoryId == category && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.ReasonRevoke == statusRevoke && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.CategoryId == category && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom && w.CreatedDate < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                            }
                                            break;
                                    }
                                    break;
                                #endregion date case #1
                                #region date case #2
                                case 2:
                                    switch (salon)
                                    {
                                        case 0:
                                            switch (status)
                                            {
                                                case 0:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.CategoryId == category && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.ReasonRevoke == statusRevoke && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.CategoryId == category && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                                default:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.CategoryId == category && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.StatusCheck == statusCheck
                                                                                                                        && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.CategoryId == category && w.StatusCheck == statusCheck
                                                                                                                        && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                            }
                                            break;
                                        default:
                                            switch (status)
                                            {
                                                case 0:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.CategoryId == category && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                            && w.StatusCheck == statusCheck && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.CategoryId == category && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.StatusCheck == statusCheck && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom
                                                                                                                        && w.CreatedDate < timeTo && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                                default:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.CategoryId == category && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.ReasonRevoke == statusRevoke && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.CategoryId == category && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.BrokenDate >= timeFrom && w.BrokenDate < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                            }
                                            break;
                                    }
                                    break;
                                #endregion date case #2
                                #region date case #3
                                case 3:
                                    switch (salon)
                                    {
                                        case 0:
                                            switch (status)
                                            {
                                                case 0:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.CategoryId == category && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.ReasonRevoke == statusRevoke && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.CategoryId == category && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                                default:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.CategoryId == category && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.StatusCheck == statusCheck
                                                                                                                        && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.CategoryId == category && w.StatusCheck == statusCheck
                                                                                                                        && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                            }
                                            break;
                                        default:
                                            switch (status)
                                            {
                                                case 0:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.CategoryId == category && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                            && w.StatusCheck == statusCheck && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.CategoryId == category && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.StatusCheck == statusCheck && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom
                                                                                                                        && w.CreatedDate < timeTo && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                                default:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.CategoryId == category && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.ReasonRevoke == statusRevoke && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.CategoryId == category && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.DesiredDeadline >= timeFrom && w.DesiredDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                            }
                                            break;
                                    }
                                    break;
                                #endregion date case #3
                                #region default date
                                default:
                                    switch (salon)
                                    {
                                        case 0:
                                            switch (status)
                                            {
                                                case 0:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.CategoryId == category && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.ReasonRevoke == statusRevoke && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.CategoryId == category && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                                default:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.CategoryId == category && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.StatusCheck == statusCheck
                                                                                                                        && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.CategoryId == category && w.StatusCheck == statusCheck
                                                                                                                        && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                            }
                                            break;
                                        default:
                                            switch (status)
                                            {
                                                case 0:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.CategoryId == category && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.CategoryId == category && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                            && w.StatusCheck == statusCheck && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.CategoryId == category && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.StatusCheck == statusCheck && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke
                                                                                                                        && w.StatusCheck == statusCheck && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CreatedDate >= timeFrom
                                                                                                                        && w.CreatedDate < timeTo && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo
                                                                                                                        && w.LevelPriority == priority && w.CategoryId == category
                                                                                                                        && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck
                                                                                                                        && w.SalonId == salon && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                                default:
                                                    switch (statusCheck)
                                                    {
                                                        case 0:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.CategoryId == category && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.ReasonRevoke == statusRevoke && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                        default:
                                                            switch (statusRevoke)
                                                            {
                                                                case 0:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.CategoryId == category && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                                default:
                                                                    switch (category)
                                                                    {
                                                                        case 0:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                        default:
                                                                            switch (priority)
                                                                            {
                                                                                case 0:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                                default:
                                                                                    broken = orderDeviceBorken.GetList(w => w.CorrectDeadline >= timeFrom && w.CorrectDeadline < timeTo && w.LevelPriority == priority && w.CategoryId == category && w.ReasonRevoke == statusRevoke && w.StatusCheck == statusCheck && w.StatusHandling == status && w.StaffIdHandling == fixerId).ToList();
                                                                                    break;
                                                                            }
                                                                            break;
                                                                    }
                                                                    break;
                                                            }
                                                            break;
                                                    }
                                                    break;
                                            }
                                            break;
                                    }
                                    break;
                            }
                            break;
                    }
            List<HandleObject> obj = new List<HandleObject>();
            obj = (from a in broken
                        join s in db.Staffs on a.OrderStaffId equals s.Id
                        join sf in db.Staffs on a.StaffIdHandling equals sf.Id into ps from sf in ps.DefaultIfEmpty()
                        join sl in db.Tbl_Salon on a.SalonId equals sl.Id
                        join t in db.TinhThanhs on a.RegionId equals t.ID
                        select new HandleObject {
                            OrderId = a.Id,
                            CreatedDate = a.CreatedDate,
                            BrokenDate = a.BrokenDate,
                            Region = t.TenTinhThanh,
                            Salon = sl.ShortName,
                            Creater = s.Id + " | " + s.Fullname,
                            Category = GetConfig(a.CategoryId.ToString(), "order_device_category").Label,
                            Team = GetConfig(a.TeamId.ToString(), "order_device_team").Label,
                            BrokenImgs = StringToList(a.ImagesBroken).ToString(),
                            DesBroken = a.DescriptionBroken,
                            Priority = GetConfig(a.LevelPriority.ToString(), "order_device_priority").Label,
                            DesiredDeadline = a.DesiredDeadline,
                            CorrectDeadline = a.CorrectDeadline,
                            StatusHandle = GetConfig(a.StatusHandling.ToString(), "order_device_status_handle").Label,
                            StatusHandleId = a.StatusHandling,
                            StaffHandling = sf == null ? "0" : sf.Id + " | " + sf.Fullname,
                            HandlingDate = a.CompleteDate,
                            HandlingImgs = a.ImageHandling == null || a.ImageHandling == "" ? "" : StringToListHandlingImgs(a.ImageHandling),
                            HandlingCount = a.ProcessHandling,
                            StatusCheck = GetConfig(a.StatusCheck.ToString(), "order_device_status_check").Label,
                            StatusCheckId = a.StatusCheck,
                            ReasonNote = a.NoteRevoke,
                            PriorityColor = a.LevelPriority == 1 ? GetConfig(a.LevelPriority.ToString(), "order_device_priority").Description : "none",
                            ReasonRevoke = a.ReasonRevoke == 0 ? "0" : GetConfig(a.ReasonRevoke.ToString(), "order_device_reason_revoke").Label,
                            ReasonRevokeId = a.ReasonRevoke,
                            Error = a.StatusHandling == 3 && a.StatusCheck == 2 ? DateTime.Compare(a.CorrectDeadline.Value.Date, a.CompleteDate.Value.Date) : 1
                        }).ToList();
            switch (completeStatus)
            {
                case 0:
                    return obj;
                case 1:
                    obj = obj.Where(w=>w.Error >= 0).ToList();
                    break;
                case 2:
                    obj = obj.Where(w=>w.Error < 0).ToList();
                    break;
                default:
                    break;
            }
            return obj;
            //Bind_Paging(data.Count());
            //rptSalary.DataSource = data.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
            //rptSalary.DataBind();
          
        }
        #endregion

        /// <summary>
        /// Export Excell for misa
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Exc_ExportExcel(object sender, EventArgs e)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    if (TxtDateTimeFrom.Text.Trim() != "")
                    {
                        var timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text,culture);
                        var timeTo = Convert.ToDateTime(TxtDateTimeTo.Text,culture);
                       
                        IEnumerable<HandleObject> lstExports = GetData();
                        var _ExcelHeadRow = new List<string>();
                        var RowLST = new List<List<string>>();

                        _ExcelHeadRow.Add("Ngày tạo YC");
                        _ExcelHeadRow.Add("Ngày phát sinh");
                        _ExcelHeadRow.Add("Khu vực");
                        _ExcelHeadRow.Add("Salon");
                        _ExcelHeadRow.Add("Người tạo");
                        _ExcelHeadRow.Add("Hạng mục");
                        _ExcelHeadRow.Add("Vị trí");
                        //_ExcelHeadRow.Add("Ảnh sự cố");
                        _ExcelHeadRow.Add("Mô tả sự cố");
                        _ExcelHeadRow.Add("Mức độ ưu tiên");
                        _ExcelHeadRow.Add("Xử lý trước ngày");
                        _ExcelHeadRow.Add("Ngày theo quy định");
                        _ExcelHeadRow.Add("Trạng thái xử lý");
                        _ExcelHeadRow.Add("Người xử lý");
                        _ExcelHeadRow.Add("Ngày hoàn thành");
                        //_ExcelHeadRow.Add("Ảnh xử lý");
                        _ExcelHeadRow.Add("Số lần sửa");
                        _ExcelHeadRow.Add("Trạng thái nghiệm thu");
                        _ExcelHeadRow.Add("Ghi chú");
                        _ExcelHeadRow.Add("Hủy yêu cầu");

                        if (lstExports.Any())
                        {
                            foreach (var v in lstExports)
                            {
                                var Row = new List<string>();

                                Row.Add(v.CreatedDate.ToString());
                                Row.Add(v.BrokenDate.ToString());
                                Row.Add(v.Region);
                                Row.Add(v.Salon);
                                Row.Add(v.Creater);
                                Row.Add(v.Category.ToString());
                                Row.Add(v.Team.ToString());
                                //Row.Add(v.BrokenImgs);
                                Row.Add(v.DesBroken);
                                Row.Add(v.Priority.ToString());
                                Row.Add(v.DesiredDeadline.ToString());
                                Row.Add(v.CorrectDeadline.ToString());
                                Row.Add(v.StatusHandle);
                                Row.Add(v.StaffHandling);
                                Row.Add(v.HandlingDate.ToString());
                                //Row.Add(v.HandlingImgs.ToString());
                                Row.Add(v.HandlingCount.ToString());
                                Row.Add(v.StatusCheck);
                                Row.Add(v.ReasonNote);
                                Row.Add(v.ReasonRevoke.ToString());

                                RowLST.Add(Row);

                            }
                            var ExcelStorePath = Server.MapPath("~") + "Public/Excel/CSVC/";
                            var FileName = "Bao_cao_CSVC_" + string.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
                            var UrlFileName = "http://" + Request.Url.Host + "/Public/Excel/CSVC/" + FileName;
                            ExportXcel(_ExcelHeadRow, RowLST, ExcelStorePath + FileName);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "');UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing(); alert(\"Xuất excel thành công.\")", true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //new Helpers.PushNotiErrorToSlack().Push("GA1LZN9AM", ex.Message + ", MAIN: " + ex.StackTrace + ", " + Library.Function.JavaScript.Serialize("Input: " + TxtDateTimeFrom.Text + TxtDateTimeTo.Text + ddlRegion.SelectedValue + ddlSalon.SelectedValue),
                //                                       this.ToString() + ".ReportExportSupplies", "dungnm",
                //                                       HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="_ExcelHeadRow"></param>
        /// <param name="List"></param>
        /// <param name="Path"></param>
        private static void ExportXcel(List<string> _ExcelHeadRow, List<List<string>> List, string Path)
        {
            try
            {
                CreateExcelFile.ExcelHeadRow = _ExcelHeadRow;
                CreateExcelFile.CreateExcel(List, Path);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi. Không tạo được file excel.\r\nException: " + ex.Message);
            }
        }
        /// <summary>
        /// split 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private string StringToList(string value)
        {
            var lstObj = new List<object>();
            var count = 0;
            foreach (var item in value.Split(',').ToList())
            {
                lstObj.Add("<img  class='img-broken' src='" + item + "' onclick='ShowImgBroken($(this))' alt='broken image " + count++ + "'/>");
            }
            return String.Join(" ", lstObj.ToArray()); ;
        }
        private string StringToListHandlingImgs(string value)
        {
            var lstObj = new List<object>();
            var count = 0;
            foreach (var item in value.Split(',').ToList())
            {
                lstObj.Add("<img  class='img-broken' src='" + item + "' onclick='ShowHandlingImgs($(this))' alt='handling image" + count++ + "'/>");
            }
            return String.Join(" ", lstObj.ToArray()); ;
        }
        private static Tbl_Config GetConfig(string value, string key)
        {
            var db = new Solution_30shineEntities();
            var config = db.Tbl_Config.Where(w => w.Value == value && w.Key == key).FirstOrDefault();
            return config;
        }
        [WebMethod]
        public static string GetReasonRevoke()
        {
            var db = new Solution_30shineEntities();
            var reason = db.Tbl_Config.Where(w => w.IsDelete == 0 && w.Key == "order_device_reason_revoke").ToList();
            return new JavaScriptSerializer().Serialize(reason);
        }
        [WebMethod]
        public static string GetStatusHandle()
        {
            var db = new Solution_30shineEntities();
            var reason = db.Tbl_Config.Where(w => w.IsDelete == 0 && w.Key == "order_device_status_handle").ToList();
            return new JavaScriptSerializer().Serialize(reason);
        }
        [WebMethod]
        public static string GetStatusCheck()
        {
            var db = new Solution_30shineEntities();
            var reason = db.Tbl_Config.Where(w => w.IsDelete == 0 && w.Key == "order_device_status_check").ToList();
            return new JavaScriptSerializer().Serialize(reason);
        }
        /// <summary>
        /// Paging
        /// </summary>
        /// <param name="TotalRow"></param>
        protected void Bind_Paging(int TotalRow)
        {
            try
            {
                // init Paging value
                int integer = 1;
                PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
                PAGING._PageNumber = !IsPostBack ? 1 : (int.TryParse(HDF_Page.Value, out integer) ? integer : 1);
                PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
                PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
                PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
                PAGING._Paging = PAGING.Make_Paging();

                RptPaging.DataSource = PAGING._Paging.ListPage;
                RptPaging.DataBind();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        [WebMethod(EnableSession = true)]
        public static string UpdateStatusHandling(int statusHandle, int numberHandling, string imgsHandling, int orderId, string handlingNote)
        {
            var db = new Solution_30shineEntities();
            var staffId = Convert.ToInt32(HttpContext.Current.Session["User_Id"].ToString());
            var order = db.OrderBrokenDeviceHandlings.Where(w => w.Id == orderId).FirstOrDefault();
            var staff = db.Staffs.FirstOrDefault(w => w.Id == staffId);
            var log = new OrderBrokenDeviceHandlingLog();
            var dataResult = new object();
            // write log with type 1: log handling broken, 2: log check handling, 3:log
            if (order == null)
            {
                return new JavaScriptSerializer().Serialize("Bản ghi không tồn tại. Vui lòng kiểm tra lại");
            }
            
            
            switch (statusHandle)
            {
                case 1:
                    break;
                case 3:
                    order.StatusHandling = statusHandle;
                    order.StaffIdHandling = staffId;
                    order.ProcessHandling = numberHandling;
                    order.ImageHandling = imgsHandling;
                    order.CompleteDate = DateTime.Now;
                    order.HandlingNote = handlingNote;
                    db.OrderBrokenDeviceHandlings.AddOrUpdate(order);
                    db.SaveChanges();
                    //write log with type = 1 
                    log.OrderBrokenDeviceId = orderId;
                    log.StaffEditId = staffId;
                    log.TypeId = 1;
                    log.Value = statusHandle.ToString();
                    log.CreatedTime = DateTime.Now;
                    db.OrderBrokenDeviceHandlingLogs.Add(log);
                    var rs = db.SaveChanges();
                    if (rs == 0)
                    {
                        dataResult = new
                        {
                            status = false,
                            message = "Có lỗi xảy ra vui lòng liên hệ nhóm phát triển"
                        };
                    }
                    dataResult = new
                    {
                        status = true,
                        message = "Hủy order thành công",
                        data = new
                        {
                            statusHandlingResult = GetConfig(statusHandle.ToString(), "order_device_status_handle").Label,
                            staffHandling = staff.Id + " | " + staff.Fullname,
                            completeTimeResult = DateTime.Now,
                            coutingHandle = numberHandling,
                            imgsHandling = imgsHandling
                        }
                        };
                    break;
                default:
                    order.StatusHandling = statusHandle;
                    order.StaffIdHandling = staffId;
                    order.HandlingNote = handlingNote;
                    //write log with type = 1 
                    log.OrderBrokenDeviceId = orderId;
                    log.StaffEditId = staffId;
                    log.TypeId = 1;
                    log.Value = statusHandle.ToString();
                    log.CreatedTime = DateTime.Now;
                    db.OrderBrokenDeviceHandlingLogs.Add(log);
                    var rs1 = db.SaveChanges();
                    if (rs1 == 0)
                    {
                        dataResult = new
                        {
                            status = false,
                            message = "Có lỗi xảy ra vui lòng liên hệ nhóm phát triển"
                        };
                    }
                        dataResult = new
                        {
                            status = true,
                            message = "Cập nhật trạng thái sửa chữa thành công",
                            data = new
                            {
                                statusHandlingResult = GetConfig(statusHandle.ToString(), "order_device_status_handle").Label,
                                staffHandling = staff.Id + " | " + staff.Fullname,
                            }
                        };
                    break;
            }
            return new JavaScriptSerializer().Serialize(dataResult);
        }

        [WebMethod(EnableSession =true)]
        public static string UpdateStatusCheckFunc(int orderId, int statusCheck)
        {
            var db = new Solution_30shineEntities();
            var staffId = Convert.ToInt32(HttpContext.Current.Session["User_Id"].ToString());
            var order = db.OrderBrokenDeviceHandlings.Where(w => w.Id == orderId).FirstOrDefault();
            var staff = db.Staffs.FirstOrDefault(w => w.Id == staffId);
            var log = new OrderBrokenDeviceHandlingLog();
            var dataResult = new object();
            // write log with type 1: log handling broken, 2: log check handling, 3:log
            if (order == null)
            {
                return new JavaScriptSerializer().Serialize("Bản ghi không tồn tại. Vui lòng kiểm tra lại");
            }
            switch (statusCheck)
            {
                case 1:
                    break;
                default:
                    order.StatusCheck = statusCheck;
                    order.StatusCheckTime = DateTime.Now;
                    db.OrderBrokenDeviceHandlings.AddOrUpdate(order);

                    //write log with type =2 
                    log.OrderBrokenDeviceId = orderId;
                    log.StaffEditId = staffId;
                    log.TypeId = 2;
                    log.Value = statusCheck.ToString();
                    log.CreatedTime = DateTime.Now;
                    db.OrderBrokenDeviceHandlingLogs.Add(log);
                    var rs = db.SaveChanges();
                    if (rs == 0)
                    {
                        dataResult = new
                        {
                            status = false,
                            message = "Có lỗi xảy ra vui lòng liên hệ nhóm phát triển"
                        };
                    }
                    dataResult = new
                    {
                        status = true,
                        message = "Cập nhật trạng thái nghiệm thu thành công",
                        data = new
                        {
                            statusCheckResult = GetConfig(statusCheck.ToString(), "order_device_status_check").Label,
                        }
                    };
                    break;
            }
            return new JavaScriptSerializer().Serialize(dataResult);
        }
        [WebMethod]
        public static string GetLogStatusHandle(int orderId, int type, string key) {
            var db = new Solution_30shineEntities();

            var log = (from l in db.OrderBrokenDeviceHandlingLogs
                           join s in db.Staffs on l.StaffEditId equals s.Id
                           join c in db.Tbl_Config on l.Value equals c.Value
                       where l.OrderBrokenDeviceId == orderId && l.TypeId == 1 && c.Key == "order_device_status_handle"
                       select new
                       {
                           StaffHandle = l.StaffEditId,
                           staffFullname = s.Fullname,
                           CreatedTime = l.CreatedTime,
                           Type = l.TypeId,
                           Value = c.Label,
                       }
                       ).Take(10).OrderByDescending(w=>w.CreatedTime).ToList();


            return new JavaScriptSerializer().Serialize(log);
        }
        [WebMethod]
        public static string GetLogStatusCheck(int orderId, int type, string key)
        {
            var db = new Solution_30shineEntities();
            var log = (from l in db.OrderBrokenDeviceHandlingLogs
                       join s in db.Staffs on l.StaffEditId equals s.Id
                       join c in db.Tbl_Config on l.Value equals c.Value
                       where l.OrderBrokenDeviceId == orderId && l.TypeId == 2 && c.Key == "order_device_status_check"
                       select new
                       {
                           StaffHandle = l.StaffEditId,
                           staffFullname = s.Fullname,
                           CreatedTime = l.CreatedTime,
                           Type = l.TypeId,
                           Value = c.Label,
                       }
                       ).Take(10).OrderByDescending(w => w.CreatedTime).ToList();
            return new JavaScriptSerializer().Serialize(log);
        }
        [WebMethod(EnableSession =true)]
        public static string UpdateReasonRevoke(int reasonRevoke, string reasonNote, int orderId)
        {
            var db = new Solution_30shineEntities();
            var staffId = Convert.ToInt32(HttpContext.Current.Session["User_Id"].ToString());
            var order = db.OrderBrokenDeviceHandlings.Where(w => w.Id == orderId).FirstOrDefault();
            var staff = db.Staffs.FirstOrDefault(w => w.Id == staffId);
            var log = new OrderBrokenDeviceHandlingLog();
            var dataResult = new object();
            // write log with type 1: log handling broken, 2: log check handling, 3:log
            if (order == null)
            {
                return new JavaScriptSerializer().Serialize("Bản ghi không tồn tại. Vui lòng kiểm tra lại");
            }

            switch (reasonRevoke)
            {
                case 0:
                    break;
                default:
                    order.ReasonRevoke = reasonRevoke;
                    order.NoteRevoke = reasonNote;
                    order.ModifiedDate = DateTime.Now;
                    db.OrderBrokenDeviceHandlings.AddOrUpdate(order);
                    var rs = db.SaveChanges();
                    if (rs == 0)
                    {
                        dataResult = new
                        {
                            status = false,
                            message = "Có lỗi xảy ra vui lòng liên hệ nhóm phát triển"
                        };
                    }
                    dataResult = new
                    {
                        status = true,
                        message = "Hủy order thành công",
                        data = new
                        {
                            reasonRevokeResult = GetConfig(reasonRevoke.ToString(), "order_device_reason_revoke").Label,
                            reasonNoteResult = reasonNote,
                        }
                    };
                    break;
            }
            return new JavaScriptSerializer().Serialize(dataResult);
        }

        public class HandleObject
        {
            public int OrderId {get;set;}
            public DateTime? CreatedDate {get;set;}
            public DateTime? BrokenDate {get;set;}
            public string Region {get;set;}
            public string Salon {get;set;}
            public string Creater {get;set;}
            public string Category {get;set;}
            public string Team {get;set;}
            public string BrokenImgs {get;set;}
            public string DesBroken {get;set;}
            public string Priority {get;set;}
            public DateTime? DesiredDeadline {get;set;}
            public DateTime? CorrectDeadline {get;set;}
            public string StatusHandle {get;set;}
            public int? StatusHandleId {get;set;}
            public string StaffHandling {get;set;}
            public DateTime? HandlingDate {get;set;}
            public string HandlingImgs {get;set;}
            public int? HandlingCount {get;set;}
            public string StatusCheck {get;set;}
            public int? StatusCheckId {get;set;}
            public string ReasonNote {get;set;}
            public string PriorityColor {get;set;}
            public string ReasonRevoke {get;set;}
            public int? ReasonRevokeId {get;set;}
            public int? Error { get; set; }
        }
    }
}