﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.GoodsManagerment
{
    public partial class ExportBillService : System.Web.UI.Page
    {
        private string PageID = "DV_XUATVT_TK";
        protected int solanmax = 0;
        protected Paging PAGING = new Paging();
        private bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected bool isRoot = false;
        protected int TotalRow = 0;
        protected List<Item> data = new List<Item>();
        protected List<ItemNumber> ItemHead = new List<ItemNumber>();
        Solution_30shineEntities db = new Solution_30shineEntities();
        //protected List<cls_ExportGood> data = new List<cls_ExportGood>();

        protected void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);               
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                BindProduct();
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                Bind_saff();
            }
        }
        /// <summary>
        /// Bind Product to Dropdowlist
        /// </summary>
        private void BindProduct()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _list = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1 && w.IsCheckVatTu == true).ToList();
                if (_list.Count > 0)
                {
                    ddlProduct.Items.Clear();
                    ddlProduct.Items.Add(new ListItem("--- Chọn sản phẩm ---", "0"));
                    for (int i = 0; i < _list.Count; i++)
                    {
                        ddlProduct.Items.Add(new ListItem(_list[i].Name, _list[i].Id.ToString()));
                    }
                    ddlProduct.DataBind();
                }
            }
        }

        protected void Salon_SelectedIndexChanged(object sender, EventArgs e)
        {
            Bind_saff();
        }

        #region[BindNhanVien]
        private void Bind_saff()
        {
            using (var db = new Solution_30shineEntities())
            {
                var salon = Convert.ToInt32(Salon.SelectedValue);
                var staff = new List<Staff>();
                var whereSalon = "";
                var sql = "";
                if (salon > 0)
                {
                    whereSalon = " and SalonId = " + salon;
                }
                sql = @"select * from Staff 
                        where IsDelete != 1 and Active = 1 and Type in (1,2)
                        and (isAccountLogin != 1 or isAccountLogin is null)" + whereSalon;
                staff = db.Staffs.SqlQuery(sql).ToList();

                var first = new Staff();
                first.Id = 0;
                first.Fullname = "Chọn nhân viên";
                staff.Insert(0, first);

                ddlStaff.DataTextField = "Fullname";
                ddlStaff.DataValueField = "Id";
                ddlStaff.DataSource = staff;
                ddlStaff.DataBind();
            }
        }
        #endregion

        //private void Bind_Product()
        //{
        //    using (var db = new Solution_30shineEntities())
        //    {
        //        var _list = db.Products.Where(w => w.CategoryId != 95).ToList();
        //        ddlProduct.Items.Clear();
        //        ddlProduct.Items.Add(new ListItem("Chọn sản phẩm", "0"));
        //        if (_list.Count > 0)
        //        {
        //            for (int i = 0; i < _list.Count; i++)
        //            {
        //                ddlProduct.Items.Add(new ListItem(_list[i].Name, _list[i].Id.ToString()));
        //            }
        //            ddlProduct.DataBind();
        //        }
        //    }
        //}

        /// <summary>
        /// Bind Data 
        /// </summary>
        int _txtSolan;
        private void Bind_Data()
        {
            try
            {
                int integer;
                var db = new Solution_30shineEntities();
                if (txtSolan.Text.Trim() != "" && Convert.ToInt32(Salon.SelectedValue) != 0 && Convert.ToInt32(ddlProduct.SelectedValue) != 0)
                {
                    int _salonId = Convert.ToInt32(Salon.SelectedValue);
                    int _staffId = int.TryParse(ddlStaff.SelectedValue, out integer) ? integer : 0;
                    int _proId = Convert.ToInt32(ddlProduct.SelectedValue);
                    _txtSolan = Convert.ToInt32(txtSolan.Text);
                    BindHead(_txtSolan);
                    BindListData(_txtSolan, _salonId, _staffId, _proId);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "starScript", "Notification();", true);
                    RemoveLoading();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        /// <summary>
        /// hàm xử lý tìm kiếm
        /// </summary>
        /// <param name="Number"></param>
        /// <param name="Salon_ID"></param>
        /// <param name="Staff_ID"></param>
        /// <param name="Pro_ID"></param>
        public void BindListData(int Number, int Salon_ID, int Staff_ID, int Pro_ID)
        {
            try
            {
                // _Number = Số lần +1 
                int _Number = Number + 1;
                int CountListStaff = 0;
                List<Staff> LstStaff = new List<Staff>();
                List<Statistics_XuatVatTu> Lst_XuatVatTu = new List<Statistics_XuatVatTu>();
                List<Item> result = new List<Item>();
                var whereSalon = "";
                var sql = "";
                if (Salon_ID > 0)
                {
                    whereSalon = " and SalonId = " + Salon_ID;
                }
                if (Staff_ID > 0)
                {
                    whereSalon += " and Id = " + Staff_ID;
                }
                sql = @"select * from Staff 
                        where IsDelete != 1 and Active = 1 and Type in (1,2)
                        and (isAccountLogin != 1 or isAccountLogin is null)" + whereSalon;
                LstStaff = db.Staffs.SqlQuery(sql).ToList();
                CountListStaff = LstStaff.Count;
                if (CountListStaff > 0)
                {
                    for (int i = 0; i < CountListStaff; i++)
                    {
                        var staff = LstStaff[i];
                        //Lst_XuatVatTu = db.Statistics_XuatVatTu.Where(a => a.IsDelete == false
                        //                                                && a.Staff_ID == staff.Id && a.Product_ID == Pro_ID).
                        //                                                OrderByDescending(a => a.Date_XuatVatTu).
                        //                                                OrderByDescending(a => a.CreateTime).Take(Number).ToList();
                        Lst_XuatVatTu = (from a in db.Statistics_XuatVatTu
                                         where a.IsDelete == false && a.Staff_ID == staff.Id && a.Product_ID == Pro_ID
                                         orderby a.Date_XuatVatTu descending, a.CreateTime descending
                                         select a).Take(_Number).ToList();
                        //Loại bỏ đi Phần tử đầu tiên do chưa có khoảng
                        Lst_XuatVatTu.RemoveAt(0);
                        if (Lst_XuatVatTu.Count < Number)
                        {
                            List<Statistics_XuatVatTu> range = new List<Statistics_XuatVatTu>();
                            int div = Number - Lst_XuatVatTu.Count;
                            int j = 0;
                            while (j < div)
                            {
                                range.Add(new Statistics_XuatVatTu());
                                j++;
                            }
                            Lst_XuatVatTu.AddRange(range);
                        }
                        else
                        {
                            //
                        }
                        var item = new Item(staff.Fullname, Lst_XuatVatTu);
                        result.Add(item);
                    }
                    //bind data result;
                    Bind_Paging(result.Count);
                    data = result.Skip(PAGING._Offset).Take(PAGING._Segment).ToList(); ;

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        public void BindListData(int Number, int Salon_ID, int Staff_ID)
        {
            try
            {
                int CountListStaff = 0;
                List<Staff> LstStaff = new List<Staff>();
                List<Statistics_XuatVatTu> Lst_XuatVatTu = new List<Statistics_XuatVatTu>();
                List<Item> result = new List<Item>();
                var whereSalon = "";
                var sql = "";
                if (Salon_ID > 0)
                {
                    whereSalon = " and SalonId = " + Salon_ID;
                }
                if (Staff_ID > 0)
                {
                    whereSalon += " and Id = " + Staff_ID;
                }
                sql = @"select * from Staff 
                        where IsDelete != 1 and Active = 1 and Type in (1,2)
                        and (isAccountLogin != 1 or isAccountLogin is null)" + whereSalon;
                LstStaff = db.Staffs.SqlQuery(sql).ToList();
                CountListStaff = LstStaff.Count;
                if (CountListStaff > 0)
                {
                    for (int i = 0; i < CountListStaff; i++)
                    {
                        var staff = LstStaff[i];
                        Lst_XuatVatTu = db.Statistics_XuatVatTu.Where(a => a.IsDelete == false && a.Staff_ID == staff.Id).OrderByDescending(a => a.Date_XuatVatTu).OrderByDescending(a => a.CreateTime).Take(Number).ToList();
                        if (Lst_XuatVatTu.Count < Number)
                        {
                            List<Statistics_XuatVatTu> range = new List<Statistics_XuatVatTu>();
                            int div = Number - Lst_XuatVatTu.Count;
                            int j = 0;
                            while (j < div)
                            {
                                range.Add(new Statistics_XuatVatTu());
                                j++;
                            }
                            Lst_XuatVatTu.AddRange(range);
                        }
                        else
                        {
                            //
                        }
                        var item = new Item(staff.Fullname, Lst_XuatVatTu);
                        result.Add(item);
                    }
                    //bind data result;
                    Bind_Paging(result.Count);
                    data = result.Skip(PAGING._Offset).Take(PAGING._Segment).ToList(); ;

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        protected void BindHead(int Number)
        {
            try
            {
                List<ItemNumber> LstItemNumber = new List<ItemNumber>();
                ItemNumber ItemNumber = new ItemNumber();
                for (int i = 0; i < Number; i++)
                {
                    ItemNumber = new ItemNumber();
                    ItemNumber.NameNumber = Number - i;
                    LstItemNumber.Add(ItemNumber);
                }
                ItemHead = LstItemNumber;

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        
        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Data();
            RemoveLoading();
        }

        protected void Bind_Paging(int totalRecords)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(totalRecords) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int totalRecords)
        {
            using (var db = new Solution_30shineEntities())
            {
                int TotalRow = totalRecords - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        protected string getValuesToArray(string values, int i)
        {
            try
            {
                return values.Split(',')[i];
            }
            catch
            {
                return "0";
            }
        }        

        //public class cls_ExportGood
        //{
        //    public int STT { get; set; }
        //    public string FullName { get; set; }
        //    public string NameValues { get; set; }
        //}

        public class Item
        {
            public string StaffName { get; set; }
            public List<Statistics_XuatVatTu> list { get; set; }
            public Item(string staffName, List<Statistics_XuatVatTu> list)
            {
                this.StaffName = staffName;
                this.list = list;
            }
        }
        public class ItemNumber
        {
            public int NameNumber { get; set; }
        }


    }
}