﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExportProductToStaff_Listing.aspx.cs" Inherits="_30shine.GUI.BackEnd.GoodsManagerment.ExportProductToStaff_Listing" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<%@ Register Src="~/GUI/BackEnd/Report/ReportMenu.ascx" TagName="ReportMenu" TagPrefix="uc1" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">

    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý xuất vật tư &nbsp;&#187; </li>
                        <li class="li-listing li-listing-1"><a href="/my-pham/xuat-nv/danh-sach.html">Danh sách</a></li>
                        <li class="li-listing li-quantity"><a href="/my-pham/xuat-nv/so-luong.html">Số lượng MP</a></li>
                        <li class="li-add" runat="server" id="subMenu_liAdd"><a href="/my-pham/xuat-nv/xuat-moi.html">Xuất mới</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report" onload="abc()">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row">
                    <div class="filter-item" style="margin-left: 0;">
                        <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="datepicker-wp">
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                                ClientIDMode="Static" runat="server"></asp:TextBox>

                            <strong class="st-head" style="margin-left: 10px; margin-right: 10px;">
                                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                            </strong>
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeTo" placeholder="Đến ngày"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="Salon" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px;" OnSelectedIndexChanged="Salon_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlStaff" CssClass="form-control" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px;"></asp:DropDownList>
                    </div>

                    <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" Text="Xem dữ liệu"
                        ClientIDMode="Static" onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <div class="export-wp drop-down-list-wp" style="display: none;">
                        <asp:Panel ID="fff" CssClass="st-head btn-viewdata"
                            ClientIDMode="Static" runat="server" onclick="EntryExportExcel('all')">
                            Xuất File Excel
                        </asp:Panel>
                        <%--OnClick="Exc_ExportExcel" --%>
                        <ul class="ul-drop-down-list" style="display: none;">
                            <li onclick="EntryExportExcel('all')">Tất cả dữ liệu</li>
                            <li onclick="EntryExportExcel('current page')">Trang hiện tại</li>
                        </ul>
                    </div>
                    <%--<asp:Panel ID="Btn_ExportExcel" CssClass="st-head btn-viewdata"
                ClientIDMode="Static" onclick="excExcel('Export')" runat="server">Xuất File Excel</asp:Panel>--%>
                    <%--<asp:Panel ID="Btn_ImportExcel" CssClass="st-head btn-viewdata"
                ClientIDMode="Static" onclick="excExcel('Import')" runat="server">Nhập File Excel</asp:Panel>--%>
                </div>
                <!-- End Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <div class="row mgt">
                    <strong class="st-head"><i class="fa fa-file-text"></i>Lịch sử xuất vật tư cho nhân viên</strong>
                </div>
                <!-- Row Table Filter -->
                <div class="table-func-panel" style="margin-top: -33px;">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="200">200</li>
                                <%--<li data-value="1000000">Tất cả</li>--%>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="row table-wp">
                            <table class="table-add table-listing report-sales-listing">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>ID</th>
                                        <th>Ngày</th>
                                        <th>Salon</th>
                                        <th>Người nhận</th>
                                        <% if (Perm_ViewAllData)
                                { %>
                                        <th class="has-sub">
                                            <div class="title">Vật tư</div>
                                            <table class="sub-table">
                                                <tbody>
                                                    <tr class="first">
                                                        <td class="col-xs-4">Tên</td>
                                                        <td class="col-xs-4">SL</td>
                                                        <td class="col-xs-4" style="border-right: none;">Giá nhập</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </th>
                                        <th>Tổng giá nhập</th>
                                        <%} else { %>
                                        <th class="has-sub">
                                            <div class="title">Vật tư</div>
                                            <table class="sub-table">
                                                <tbody>
                                                    <tr class="first">
                                                        <td class="col-xs-8">Tên</td>
                                                        <td class="col-xs-4" style="border-right: none;">SL</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </th>
                                        <% } %>
                                        <th style="padding-right: 5px; padding-left: 5px; width: 90px;">Sửa / Xóa</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="RptGoods" runat="server" OnItemDataBound="Repeater_ItemDataBound">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td><%# Eval("Id") %></td>
                                                <td><%# String.Format("{0:dd/MM/yyyy}", Eval("CreatedDate")) %></td>
                                                <td><%# Eval("SalonName") %></td>
                                                <td><%# Eval("RecipientName") %></td>
                                                <% if (Perm_ViewAllData)
                                                    { %>
                                                <td class="has-sub">
                                                    <table class="sub-table">
                                                        <tbody>
                                                            <%# Eval("goodsDetail") %>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td><%# String.Format("{0:#,###}", Eval("totalCost")).Replace(',', '.') %></td>
                                                <% }
                                                else
                                                { %>
                                                <td class="has-sub">
                                                    <table class="sub-table">
                                                        <tbody>
                                                            <%# Eval("goodsDetail") %>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <% } %>
                                                <td class="map-edit" style="padding-right: 27px; padding-left: 5px;">
                                                    <div class="be-report-price" style="padding-left: 7px;"></div>
                                                    <div class="edit-wp">
                                                        <asp:Panel CssClass="edit-action-wp action-edit" runat="server" ID="EAedit" Visible="false">
                                                            <a class="elm edit-btn" href="/my-pham/xuat-nv/xuat-moi.html?id=<%# Eval("Id") %>" title="Sửa" target="_blank"></a>
                                                        </asp:Panel>
                                                        <asp:Panel CssClass="edit-action-wp action-delete" runat="server" ID="EAdelete" Visible="false">
                                                            <a class="elm del-btn" onclick="del(this.parentNode.parentNode.parentNode.parentNode,'<%# Eval("Id") %>', '<%# Eval("Id") %>')" href="javascript://" title="Xóa"></a>
                                                        </asp:Panel>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                            { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <%--<asp:UpdatePanel ID="UPExcel" runat="server" ClientIDMode="Static">
            <ContentTemplate></ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="BtnFakeExcel" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>--%>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <%--<asp:Button ID="BtnFakeExcel" ClientIDMode="Static" runat="server" OnClick="Exc_ExportExcel" style="display:none;" />--%>
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="IsPaging" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_ExcelType" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELPage" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELSegment" />
            </div>
            <%-- END Listing --%>
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
           .select2-container {
            width: 180px !important;
            margin-top: 5px !important;
        }

        .select2-container--default .select2-selection--single {
            height: 32px !important;
            padding-top: 1px !important;
        }
        </style>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                // Add active menu
                $("#glbExportGoods").addClass("active");
                $("#glbExportGoods_Staff").addClass("active");
                $("#subMenu .li-listing-1").addClass("active");

                // Price format
                UP_FormatPrice('.be-report-price');

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { },
                    scrollMonth: false,
                    scrollTime: false,
                    scrollInput: false
                });

                // View data today
                $(".tag-date-today").click();

                // test
                $(window).focus(function () {
                    console.log("here");
                });
            });

            function SubTableFixHeight() {
                $("tbody table.sub-table").each(function () {
                    $(this).height($(this).parent().height() + 1);
                });
            }

            function viewDataByDate(This, time) {
                $(".tag-date.active").removeClass("active");
                This.addClass("active");
                $("#TxtDateTimeTo").val("");
                $("#TxtDateTimeFrom").val(time);
                $("#ViewData").click();
            }

            //============================
            // Event delete
            //============================
            function del(This, code, name) {
                var code = code || null,
                    name = name || null,
                    Row = This;
                if (!code) return false;

                // show EBPopup
                $(".confirm-yn").openEBPopup();
                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

                $("#EBPopup .yn-yes").bind("click", function () {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/SystemService/Ajax/Del.aspx/Delele_ExportGoods",
                        data: '{Code : "' + code + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            var mission = JSON.parse(response.d);
                            if (mission.success) {
                                delSuccess();
                                Row.remove();
                            } else {
                                delFailed();
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    });
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
            }



        </script>

    </asp:Panel>
</asp:Content>
