﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Helpers;
using System.Text.RegularExpressions;
using System.Globalization;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.GoodsManagerment
{
    public partial class ExportProductToSalon : System.Web.UI.Page
    {
        private string PageID = "MP_XUATSL_XM";
        protected bool Perm_ShowSalon = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;

        private List<ProductBasic> ProductList = new List<ProductBasic>();
        private List<ProductBasic> ServiceList = new List<ProductBasic>();
        protected int SalonId = 0;
        private List<int> lstFreeService = new List<int>();
        protected int Id;
        private int integer;
        private CultureInfo culture = new CultureInfo("vi-VN");

        private ExportGood OBJ = new ExportGood();

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (IsAccountant)
            {
                UIHelpers.Redirect("/my-pham/xuat-salon/so-luong.html", null);
            }
            else
            {
                Id = getId();
                if (!IsPostBack)
                {
                    Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ShowSalon);
                    Bind_OBJ();
                    Bind_RptProduct();
                    Bind_RptProductFeatured();
                    bindProductCategory();
                    TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Today);
                }
            }
        }
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);               
                //Perm_ShowSalon = permissionModel.GetActionByActionNameAndPageId("Perm_ShowSalon", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                pageId = permissionModel.GetRegexPageIdSpecial().Replace(pageId, ".html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        /// <summary>
        /// get Id của lô chuyển mỹ phẩm
        /// </summary>
        /// <returns></returns>
        private int getId()
        {
            return int.TryParse(Request.QueryString["Id"], out integer) ? integer : 0;
        }

        /// <summary>
        /// get Id đăng nhập và chuyển mỹ phẩm
        /// </summary>
        /// <returns></returns>
        private int getExportUserId()
        {
            if (Session["User_Id"] != null)
            {
                return int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// get Id người nhận mỹ phẩm
        /// </summary>
        /// <returns></returns>
        private int getRecipientId()
        {
            return int.TryParse(Recipient.Value, out integer) ? integer : 0;
        }

        /// <summary>
        /// Cập nhật lô mỹ phẩm
        /// </summary>
        /// <param name="Id"></param>
        private void Update(int Id)
        {
            if (Id > 0)
            {
                using (var db = new Solution_30shineEntities())
                {
                    var error = 0;
                    var serialize = new JavaScriptSerializer();
                    OBJ = db.ExportGoods.FirstOrDefault(w=>w.Id == Id);
                    if (OBJ != null)
                    {
                        OBJ.SalonId = Convert.ToInt32(Salon.SelectedValue);
                        OBJ.ModifiedDate = DateTime.Now;
                        OBJ.RecipientId = getRecipientId();
                        OBJ.Note = Description.Text;
                        OBJ.GoodsIds = HDF_ProductIds.Value != "[]" ? HDF_ProductIds.Value : "";
                        OBJ.GoodsIds = serialize.Serialize(Library.Function.genListFromJson(OBJ.GoodsIds));
                        db.ExportGoods.AddOrUpdate(OBJ);
                        error += db.SaveChanges() > 0 ? 0 : 1;
                        if (error == 0)
                        {
                            error += UpdateFlow(OBJ.GoodsIds, OBJ.Id);
                        }
                        if (error == 0)
                        {
                            // Thông báo thành công
                            var MsgParam = new List<KeyValuePair<string, string>>();
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                            MsgParam.Add(new KeyValuePair<string, string>("id", Id.ToString()));
                            UIHelpers.Redirect("/my-pham/xuat-salon/xuat-moi.html", MsgParam);
                        }
                        else
                        {
                            MsgSystem.Text = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                            MsgSystem.CssClass = "msg-system warning";
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Xuất lô mỹ phẩm
        /// </summary>
        private void Add()
        {
            using (var db = new Solution_30shineEntities())
            {
                var serialize = new JavaScriptSerializer();
                var obj = new ExportGood();
                var error = 0;
                obj.SalonId = Convert.ToInt32(Salon.SelectedValue);
                obj.ExportId = getExportUserId();
                obj.GoodsIds = HDF_ProductIds.Value != "[]" ? HDF_ProductIds.Value : "";
                obj.GoodsIds = serialize.Serialize(Library.Function.genListFromJson(obj.GoodsIds));
                obj.IsDelete = 0;
                if (TxtDateTimeFrom.Text != "")
                {
                    obj.CreatedDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                }
                else
                {
                    obj.CreatedDate = DateTime.Now;
                }
                obj.RecipientId = getRecipientId();
                obj.Level = 1; // Level = 1 : Xuất mỹ phẩm cho salon, Level = 2 : Xuất mỹ phẩm cho nhân viên
                obj.Note = Description.Text;
                db.ExportGoods.Add(obj);
                error += db.SaveChanges() > 0 ? 0 : 1;
                if (error == 0)
                {
                    error += AddFlow(obj.GoodsIds, obj.Id);
                }

                if (error == 0)
                {
                    // Thông báo thành công
                    var MsgParam = new List<KeyValuePair<string, string>>();
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                    UIHelpers.Redirect("/my-pham/xuat-salon/xuat-moi.html", MsgParam);
                }
                else
                {
                    MsgSystem.Text = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                    MsgSystem.CssClass = "msg-system warning";
                }
            }
        }

        private int AddFlow(string goodsIds, int ExportGoodsId)
        {
            var error = 0;
            if (goodsIds != "")
            {
                using (var db = new Solution_30shineEntities())
                {
                    var obj = new FlowGood();
                    var list = Library.Function.genListFromJson(goodsIds);
                    var goods = new Product();
                    if (list.Count > 0)
                    {
                        foreach (var v in list)
                        {
                            goods = db.Products.FirstOrDefault(w=>w.Id == v.Id);
                            obj = new FlowGood();
                            obj.ExportGoodsId = ExportGoodsId;
                            obj.GoodsId = v.Id;
                            obj.Quantity = v.Quantity;
                            obj.Cost = v.Price;
                            if (goods != null)
                            {
                                obj.Price = Convert.ToInt32(goods.Price);
                            }
                            if (TxtDateTimeFrom.Text != "")
                            {
                                obj.CreatedDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                            }
                            else
                            {
                                obj.CreatedDate = DateTime.Now;
                            }
                            obj.IsDelete = 0;
                            db.FlowGoods.Add(obj);
                            error += db.SaveChanges() > 0 ? 0 : 1;
                        }
                    }                    
                }
            }
            return error;
        }

        private int UpdateFlow(string goodsIds, int ExportGoodsId)
        {
            var error = 0;
            if (goodsIds != "")
            {
                using (var db = new Solution_30shineEntities())
                {
                    var obj = new ProductBasic();
                    var list = Library.Function.genListFromJson(goodsIds);
                    var flow = db.FlowGoods.Where(w => w.ExportGoodsId == ExportGoodsId && w.IsDelete != 1).ToList();
                    var index = -1;
                    var goods = new Product();
                    if (flow.Count > 0)
                    {
                        foreach (var v in flow)
                        {
                            goods = db.Products.FirstOrDefault(w=>w.Id == v.Id);
                            index = list.FindIndex(w => w.Id == v.GoodsId);
                            if (index == -1)
                            {
                                v.IsDelete = 1;
                                db.FlowGoods.AddOrUpdate(v);
                                error += db.SaveChanges() > 0 ? 0 : 1;
                            }
                            else
                            {
                                obj = list[index];
                                v.Quantity = obj.Quantity;
                                v.Cost = obj.Price;
                                v.ModifiedDate = DateTime.Now;
                                if (goods != null)
                                {
                                    v.Price = Convert.ToInt32(goods.Cost);
                                }
                                db.FlowGoods.AddOrUpdate(v);
                                error += db.SaveChanges() > 0 ? 0 : 1;
                            }
                        }
                    }
                }
            }
            return error;
        }

        protected void Action(object sender, EventArgs e)
        {
            if (Id > 0)
            {
                Update(Id);
            }
            else
            {
                Add();
            }
        }

        private bool Bind_OBJ()
        {
            var isset = false;
            using (var db = new Solution_30shineEntities())
            {
                if (Id > 0)
                {
                    OBJ = db.ExportGoods.FirstOrDefault(w => w.Id == Id);
                    if (OBJ != null)
                    {
                        isset = true;
                        // bind salon selected
                        var ItemSelected = Salon.Items.FindByValue(OBJ.SalonId.ToString());
                        if (ItemSelected != null)
                        {
                            ItemSelected.Selected = true;
                        }
                        else
                        {
                            Salon.SelectedIndex = 0;
                        }
                        // bind người nhận mỹ phẩm
                        if (OBJ.RecipientId > 0)
                        {
                            var staff = db.Staffs.FirstOrDefault(w => w.Id == OBJ.RecipientId);
                            if (staff != null)
                            {
                                FVRecipient.InnerText = staff.OrderCode.ToString() + "- " + staff.Fullname;
                                InputRecipient.Text = staff.OrderCode.ToString();
                                Recipient.Value = staff.Id.ToString();
                            }
                        }
                        // bind danh sách mỹ phẩm
                        if (OBJ.GoodsIds != "")
                        {
                            Rpt_Product_Flow.DataSource = Library.Function.genListFromJson(OBJ.GoodsIds);
                            Rpt_Product_Flow.DataBind();
                            ListingProductWp.Style.Add("display","block");
                        }
                        // bind note
                        Description.Text = OBJ.Note;
                    }
                    if (!isset)
                    {
                        MsgSystem.Text = "Lỗi. Dữ liệu không tồn tại.";
                        MsgSystem.CssClass = "msg-system warning";
                    }
                }                
            }            

            return isset;
        }


        private void Bind_RptProduct()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Products = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1).OrderByDescending(o => o.Id).ToList();

                Rpt_Product.DataSource = _Products;
                Rpt_Product.DataBind();
            }
        }

        private void Bind_RptProductFeatured()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1 && w.Status == 1).OrderByDescending(o => o.Id).ToList();

                Rpt_ProductFeatured.DataSource = lst;
                Rpt_ProductFeatured.DataBind();
            }
        }

        private void bindProductCategory()
        {
            using (var db = new Solution_30shineEntities())
            {
                //var list = db.Tbl_Category.Where(w=>w.Pid == 1 && w.IsDelete != 1 && w.Publish == true).ToList();
                ////var c = new Tbl_Category();
                ////c.Id = 0;
                ////c.Name = "Chọn danh mục";
                ////list.Insert(0, c);
                //rptProductCategory.DataSource = list;
                //rptProductCategory.DataBind();
                var list = new List<Tbl_Category>();
                var node = new List<Tbl_Category>();
                var c = new Tbl_Category();
                c.Id = 0;
                c.Name = "Chọn danh mục";
                list.Insert(0, c);
                var cateRoot = db.Tbl_Category.Where(w => w.IsDelete != 1 && w.Pid == 1 && w.Publish == true).ToList();
                if (cateRoot.Count > 0)
                {
                    foreach (var v in cateRoot)
                    {
                        node = db.Tbl_Category.Where(w => w.IsDelete != 1 && w.Pid == v.Id).ToList();
                        list.Add(v);
                        if (node.Count > 0)
                        {
                            foreach (var v2 in node)
                            {
                                v2.Name = "----- " + v2.Name;
                                list.Add(v2);
                            }
                        }
                    }
                }
                rptProductCategory.DataSource = list;
                rptProductCategory.DataBind();
            }
        }

        /// <summary>
        /// Reset Rating Temp Value
        /// </summary>
        private void resetRatingTemp()
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                /// Reset giá trị rating temp
                int accId = Session["User_Id"] != null ? int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0 : 0;
                int salonId = Session["SalonId"] != null ? int.TryParse(Session["SalonId"].ToString(), out integer) ? integer : 0 : 0;
                var ratingTemp = db.Rating_Temp.FirstOrDefault(w => w.SalonId == salonId && w.AccountId == accId);
                if (ratingTemp != null)
                {
                    ratingTemp.RatingValue = 0;
                    db.Rating_Temp.AddOrUpdate(ratingTemp);
                    db.SaveChanges();
                }
            }
        }

        [WebMethod]
        public static string GetCustomer(string CustomerCode)
        {
            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var _Customer = db.Customers.Where(w => w.Customer_Code == CustomerCode).ToList();

                if (_Customer.Count > 0)
                {
                    _Msg.success = true;
                    _Msg.msg = serializer.Serialize(_Customer);
                }
                else
                {
                    _Msg.success = false;
                }
            }
            return serializer.Serialize(_Msg);
        }

        [WebMethod]
        public static string CheckDuplicateBill(string CustomerCode)
        {
            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var today = Convert.ToDateTime(DateTime.Now.ToString("d/M/yyyy"), culture);
                var todayL = today.AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                var todayR = today.AddDays(1);
                var _Bill = db.BillServices.FirstOrDefault(w => w.CreatedDate > todayL && w.CreatedDate < todayR && w.CustomerCode == CustomerCode);

                if (_Bill != null)
                {
                    _Msg.success = true;
                    _Msg.msg = serializer.Serialize(_Bill);
                }
                else
                {
                    _Msg.success = false;
                }
            }
            return serializer.Serialize(_Msg);
        }

        /// <summary>
        /// Lấy toàn bộ danh sách vật tư xuất cho salon
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static object getAllGoods()
        {
            using (var db = new Solution_30shineEntities())
            {
                return db.Products.Where(w => w.IsDelete != 1/* && w.Publish == 1*/ && w.CategoryId == 20).Select(s => new { s.Id, s.Code, s.Name, s.Cost, s.Price, Quantity = 1 }).ToList();
            }
        }

        
    }
}