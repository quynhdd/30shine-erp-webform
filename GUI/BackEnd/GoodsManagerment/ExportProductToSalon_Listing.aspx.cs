﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Script.Serialization;
using ExportToExcel;
using Excel;
using LinqKit;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.GoodsManagerment
{
    public partial class ExportProductToSalon_Listing : System.Web.UI.Page
    {
        private string PageID = "MP_XUATSL_DS";
        protected Paging PAGING = new Paging();
        protected List<List<ProductBasic>> ProductList;
        private Expression<Func<ExportGood, bool>> Where = PredicateBuilder.True<ExportGood>();
        CultureInfo culture = new CultureInfo("vi-VN");
        private string sql = "";
        private string sql_staff_listing = "";

        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;
        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);

        protected string table = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (IsAccountant)
            {
                subMenu_liAdd.Visible = false;
            }

            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                GenWhere();
                bindStaff();
                Bind_Paging();
                Bind_DataByDate();
            }
            else
            {
                GenWhere();
            }
        }
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //Perm_ViewAllDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDate", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ViewAllDate = permissionModel.CheckPermisionByAction("Perm_ViewAllDate", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        private void GenWhere()
        {
            DateTime timeFrom = Convert.ToDateTime("2016/1/1", culture);
            DateTime timeTo = new DateTime();
            string whereTime = "";
            string whereSalon = "";
            int SalonId = Convert.ToInt32(Salon.SelectedValue);
            if (TxtDateTimeFrom.Text != "")
            {
                timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                if (TxtDateTimeTo.Text != "")
                {
                    timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                }
                else
                {
                    timeTo = timeFrom.AddDays(1);
                }
                whereTime = " and a.CreatedDate between '" + string.Format("{0:yyyy/MM/dd}", timeFrom) + "' and '" + string.Format("{0:yyyy/MM/dd}", timeTo) + "'";
            }
            else
            {
                whereTime = " and a.CreatedDate >= '" + string.Format("{0:yyyy/MM/dd}", timeFrom) + "'";
            }
            if (SalonId > 0)
            {
                whereSalon += " and a.SalonId = " + SalonId;
            }
            else
            {
                whereSalon += " and a.SalonId > 0";
            }

            sql = @"select a.*, b.Name as SalonName, c.Fullname as ExportName, d.Fullname as RecipientName
                    from ExportGoods as a
                    left join Tbl_Salon as b
                    on a.SalonId = b.Id
                    left join Staff as c
                    on a.ExportId = c.Id
                    left join Staff as d
                    on a.RecipientId = d.Id
                    where a.IsDelete != 1 and a.Level = 1 " +
                    whereTime + 
                    (SalonId > 0 ? " and a.SalonId = " + SalonId : " and a.SalonId > 0" ) +
                    (ddlStaff.SelectedValue != "" && ddlStaff.SelectedValue != "0" ? " and a.RecipientId = " + ddlStaff.SelectedValue : "" );

            sql_staff_listing = @"select staff.*
                        from
                        (
	                        select exportGoods.RecipientId
	                        from ExportGoods as exportGoods
	                        where exportGoods.IsDelete != 1 " +
	                        ( SalonId > 0 ? " and exportGoods.SalonId = "+ SalonId : " and exportGoods.SalonId > 0 ") +
                            @" and exportGoods.RecipientId != 0 and exportGoods.Level = 1
	                        group by exportGoods.RecipientId
                        ) as a
                        inner join Staff as staff
                        on staff.Id = a.RecipientId 
                        order by staff.Fullname asc";
        }
        

        private void bindStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = db.Staffs.SqlQuery(sql_staff_listing).ToList();
                var staff = new Staff();
                staff.Id = 0;
                staff.Fullname = "Chọn người nhận";
                list.Insert(0, staff);
                ddlStaff.DataValueField = "Id";
                ddlStaff.DataTextField = "Fullname";
                ddlStaff.DataSource = list;
                ddlStaff.DataBind();
            }
        }

        protected void Bind_DataByDate()
        {
            using (var db = new Solution_30shineEntities())
            {
                var listGoods = new List<ProductBasic>();
                var goods = new Library.Class.cls_goods();
                var list = db.Database.SqlQuery<Library.Class.cls_goods>(sql).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                var serialize = new JavaScriptSerializer();
                int loop = 0;
                int totalCost = 0;
                if (list.Count > 0)
                {
                    foreach (var v in list)
                    {
                        totalCost = 0;
                        var goodsDetail = "";
                        listGoods = serialize.Deserialize<List<ProductBasic>>(v.GoodsIds);
                        if (listGoods.Count > 0)
                        {
                            var i = 0;
                            var First = "";
                            foreach (var v2 in listGoods)
                            {
                                // Price lưu ở đây là giá nhập (không phải giá bán)
                                First = i == 0 ? " class='first'" : "";
                                if (Perm_ViewAllData)
                                {
                                    goodsDetail += "<tr" + First + ">" +
                                                    "<td class='col-xs-4'>" +
                                                            "<a href='/admin/san-pham/" + v2.Id + ".html' target='_blank'>" +
                                                            v2.Name +
                                                            "</a>" +
                                                    "</td>" +
                                                    "<td class='col-xs-4'>" + v2.Quantity + "</td>" +
                                                    "<td class='col-xs-4 no-bdr be-report-price'>" + v2.Price + "</td>" +
                                                "</tr>";
                                }
                                else
                                {
                                    goodsDetail += "<tr" + First + ">" +
                                                    "<td class='col-xs-8'>" +
                                                            "<a href='/admin/san-pham/" + v2.Id + ".html' target='_blank'>" +
                                                            v2.Name +
                                                            "</a>" +
                                                    "</td>" +
                                                    "<td class='col-xs-4' style='border-right:none;'>" + v2.Quantity + "</td>" +
                                                "</tr>";
                                }
                                i++;
                                // Tính tổng giá nhập
                                totalCost += v2.Price * v2.Quantity;
                            }
                            goodsDetail = goodsDetail.TrimEnd(',', ' ');
                        }
                        else
                        {
                            goodsDetail += "<tr class='first'><td class='no-bdr'>-</td></tr>";
                        }
                        list[loop].goodsDetail = goodsDetail;
                        list[loop++].totalCost = (double)totalCost;
                    }
                }
                RptGoods.DataSource = list;
                RptGoods.DataBind();
            }            
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
            Bind_DataByDate();
            ScriptManager.RegisterStartupScript(UPTableListing, this.GetType(), "Format Price", "", true);
            RemoveLoading();
        }

        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? (HDF_Page.Value != "" ? Convert.ToInt32(HDF_Page.Value) : 1) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                var Count = db.Database.SqlQuery<Library.Class.cls_goods>(sql).Count();
                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        public string GenTime(int minutes)
        {
            string time = "";
            int hour = minutes / 60;
            int minute = minutes % 60;
            if (hour > 0)
            {
                time += hour + "h";
                time += minute + "p";
            }
            else
            {
                if (minute > 0)
                {
                    time += minute + " p";
                }
                else
                {
                    time += "-";
                }
            }
            return time;
        }
        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }

    }
}