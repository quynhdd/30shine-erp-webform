﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="ExportBillService.aspx.cs" Inherits="_30shine.GUI.BackEnd.GoodsManagerment.ExportBillService" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Thống kê mức dùng vật tư
    </title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .customer-listing table.table-listing tbody tr:hover .edit-wp.display_permission { display: none !important; }
            .display_permission { display: none; }
            .edit-wp { position: absolute; right: 3px; top: 24%; background: #e7e7e7; display: none; }
            .edit-wp .elm { width: 17px; height: 20px; float: left; display: block; background: url(/Assets/images/icon.delete.small.active.png?v=2); margin-right: 30px; }
            .edit-wp .elm:hover { background: url(/Assets/images/icon.delete.small.png?v=2); }
            .wp_time_booking { width: 100%; float: left; padding-left: 55px; margin: 5px 0px 15px; }
            .wp_time_booking .a_time_booking { float: left; height: 26px; line-height: 26px; padding: 0px 15px; background: #dfdfdf; margin-right: 10px; cursor: pointer; position: relative; font-size: 13px; -webkit-border-radius: 15px; -moz-border-radius: 15px; border-radius: 15px; }
            .wp_time_booking .a_time_booking:hover, .wp_time_booking .a_time_booking.active { background: #fcd344; color: #000; }
            .wp_time_booking .a_time_booking .span_time_booking { position: absolute; top: 24px; left: 0; width: 100%; float: left; text-align: center; font-size: 13px; }
            .edit-wp .elm { margin-right: 0; }
        </style>
        <%--<uc1:ReportMenu runat="server" id="ReportMenu" />--%>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Danh sách &nbsp;&#187; </li>
                        <li class="be-report-li"><a href="javascript:void(0);"><i class="fa fa-th-large"></i>Thống kê mức dùng vật tư</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report be-report-timekeeping">
            <%-- Listing --%>
            <div class="wp960 content-wp">

                 <div class="row">
                     <div class="filter-item">
                        <strong class="st-head" style="margin-right: 5px;">Salon : </strong>
                        <asp:DropDownList ID="Salon" runat="server" CssClass="form-control select" ClientIDMode="Static" OnSelectedIndexChanged="Salon_SelectedIndexChanged" AutoPostBack="true"  Style="width: 190px;"></asp:DropDownList>
                    </div>
                     <div class="filter-item">
                        <strong class="st-head" style="margin-right: 5px;">Nhân viên : </strong>
                        <asp:DropDownList ID="ddlStaff" runat="server" CssClass="form-control" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                    </div>
                    <div class="filter-item" >
                        <asp:DropDownList ID="ddlProduct" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px;"></asp:DropDownList>
                    </div>
                      <div class="filter-item">
                        <strong class="st-head" style="margin-right: 5px;">Số lần : </strong>
                          <asp:TextBox ID="txtSolan" CssClass="form-control" runat="server" ClientIDMode="Static"></asp:TextBox>
                    </div>
                    <a id="ViewData" onclick="excPaging(1)" class="st-head btn-viewdata " href="javascript:void(0)">Xem dữ liệu
                    </a>
                </div>
                
                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>Thống kê mức dùng vật tư</strong>
                </div>
                <div class="wp customer-add customer-listing be-report">
                    <%-- Listing --%>
                    <div class="wp960 content-wp">
                        <!-- Filter -->
                        <!-- End Filter -->
                        <!-- Row Table Filter -->
                        <div class="table-func-panel">
                            <div class="table-func-elm">
                                <span>Số hàng / Page : </span>
                                <div class="table-func-input-wp">
                                    <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                                    <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                                    <ul class="ul-opt-segment">
                                        <li data-value="10">10</li>
                                        <li data-value="20">20</li>
                                        <li data-value="30">30</li>
                                        <li data-value="40">40</li>
                                        <li data-value="50">50</li>
                                        <li data-value="1000000">Tất cả</li>
                                    </ul>
                                    <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                                </div>
                            </div>
                        </div>
                        <!-- End Row Table Filter -->
                        <asp:ScriptManager runat="server" ID="ScriptManager1"></asp:ScriptManager>
                        <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                            <ContentTemplate>
                                <div class="table-wp">
                                    <table class="table-add table-listing report-sales-listing" id="example">
                                            <thead>
                                                <tr>
                                                    <th>STT</th>
                                                    <th>Tên nhân viên</th>
                                                    <% for (int j = 0; j < ItemHead.Count; j++)
                                                        {
                                                    %>
                                                    <th>Lần  <%=ItemHead[j].NameNumber %></th>
                                                    <%
                                                        }
                                                    %>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <%
                                                    for (int j = 0; j < data.Count; j++)
                                                    {      %>
                                                <tr>
                                                    <td><%= (j+1) + PAGING._Offset %></td>
                                                    <td>
                                                        <%= (data[j].StaffName.ToString()) %>
                                                    </td>
                                                    <%
                                                        for (int i = 0; i < data[j].list.Count; i++)
                                                        {
                                                    %>
                                                    <td><%= data[j].list[i].TotalBill_XuatVatTu%></td>
                                                    <%  }    %>
                                                </tr>
                                                <%
                                                    }
                                                %>
                                            </tbody>
                                        </table>
                                </div>

                                <!-- Paging -->
                                <div class="site-paging-wp">
                                    <% if (PAGING.TotalPage > 1)
                                        { %>
                                    <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                        <% if (PAGING._Paging.Prev != 0)
                                            { %>
                                        <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                        <% } %>
                                        <asp:Repeater ID="RptPaging" runat="server">
                                            <ItemTemplate>
                                                <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                                    <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                                    <%# Eval("PageNum") %>
                                                </a>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                            { %>
                                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                        <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                        <% } %>
                                    </asp:Panel>
                                    <% } %>
                                </div>
                                <!-- End Paging -->
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                    </div>
                    <%-- END Listing --%>
                </div>

            </div>
            <%-- END Listing --%>
        </div>
     
    </asp:Panel>
       <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
          .select2-container {
                width: 180px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
    <script>
        function Notification()
        {
            alert( "Bạn chưa chọn Salon || Bạn chưa chọn Sản Phẩm || chưa nhập số lần ! " );
        }
    </script>
</asp:Content>