﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Serialization;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Helpers;
using System.Text.RegularExpressions;
using System.Globalization;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.GoodsManagerment
{
    public partial class ExportProductToStaff : System.Web.UI.Page
    {
        private string PageID = "DV_XUATVT_NV";
        private bool Perm_ShowSalon = false;
        private bool Perm_Access = false;
        private bool Perm_AllSalon = false;
        private bool Perm_ViewAllData = false;
        protected bool IsAccountant = false;
        protected string ListProduct;
        private List<ProductBasic> ProductList = new List<ProductBasic>();
        private List<ProductBasic> ServiceList = new List<ProductBasic>();
        public static List<_30shine.MODEL.ENTITY.EDMX.Product> _list = new List<_30shine.MODEL.ENTITY.EDMX.Product>();
        protected int SalonId = 0;
        private List<int> lstFreeService = new List<int>();
        protected int Id;
        private int integer;
        private CultureInfo culture = new CultureInfo("vi-VN");

        private ExportGood OBJ = new ExportGood();

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (IsAccountant)
            {
                UIHelpers.Redirect("/my-pham/xuat-nv/so-luong.html", null);
            }
            else
            {
                Id = getId();
                if (!IsPostBack)
                {
                    Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                    Bind_OBJ();
                    Bind_RptProduct();
                    //Bỏ kiểu checkbox chọn sản phẩm
                    //Bind_RptProductFeatured();
                    bindProductCategory();
                    TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Today);
                }
                ListProduct = GetProduct();
            }
        }
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ShowSalon = permissionModel.GetActionByActionNameAndPageId("Perm_ShowSalon", PageID, permission);
                //Perm_AllSalon = permissionModel.GetActionByActionNameAndPageId("Perm_AllSalon", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                pageId = permissionModel.GetRegexPageIdSpecial().Replace(pageId, ".html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ShowSalon = permissionModel.CheckPermisionByAction("Perm_ShowSalon", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_AllSalon = permissionModel.CheckPermisionByAction("Perm_AllSalon", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
            else if (Perm_ShowSalon)
            {
                TrSalon.Visible = true;
            }
        }
        /// <summary>
        /// get Id của lô chuyển mỹ phẩm
        /// </summary>
        /// <returns></returns>
        private int getId()
        {
            return int.TryParse(Request.QueryString["Id"], out integer) ? integer : 0;
        }

        /// <summary>
        /// get Id đăng nhập và chuyển mỹ phẩm
        /// </summary>
        /// <returns></returns>
        private int getExportUserId()
        {
            if (Session["User_Id"] != null)
            {
                return int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// get Id người nhận mỹ phẩm
        /// </summary>
        /// <returns></returns>
        private int getRecipientId()
        {
            return int.TryParse(Recipient.Value, out integer) ? integer : 0;
        }

        private int getUserSalonId()
        {
            if (Session["SalonId"] != null)
            {
                return int.TryParse(Session["SalonId"].ToString(), out integer) ? integer : 0;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// GetProduct Bind Dropdowlist
        /// </summary>
        private string GetProduct()
        {
            using (var db = new Solution_30shineEntities())
            {
                _list = db.Products.Where(w => (w.IsDelete != 1 && w.Publish == 1 && w.CategoryId == 20)
                                                || (w.Id == 378 || w.Id == 63 || w.Id == 44 || w.Id == 58 || w.Id == 83
                                                || w.Id == 440 || w.Id == 32 || w.Id == 446)).ToList();

                //_list = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1).ToList();
                var seria = new JavaScriptSerializer();
                return seria.Serialize(_list);
            }
        }

        /// <summary>
        /// Xuất lô mỹ phẩm
        /// </summary>
        private void Add()
        {
            using (var db = new Solution_30shineEntities())
            {
                var serialize = new JavaScriptSerializer();
                var obj = new ExportGood();
                var error = 0;
                obj.SalonId = Convert.ToInt32(Salon.SelectedValue);
                obj.ExportId = getExportUserId();
                obj.GoodsIds = HDF_ProductIds.Value != "[]" ? HDF_ProductIds.Value : "";
                obj.GoodsIds = serialize.Serialize(Library.Function.genListFromJson(obj.GoodsIds));
                obj.IsDelete = 0;
                if (TxtDateTimeFrom.Text != "")
                {
                    obj.CreatedDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                }
                else
                {
                    obj.CreatedDate = DateTime.Now;
                }
                obj.RecipientId = getRecipientId();
                obj.Level = 2; // Level = 1 : Xuất mỹ phẩm cho salon, Level = 2 : Xuất mỹ phẩm cho nhân viên
                obj.Note = Description.Text;
                db.ExportGoods.Add(obj);
                error += db.SaveChanges() > 0 ? 0 : 1;

                // Thêm mới Statistics_XuatVatTu
                if (obj.GoodsIds != null)
                    Statistics_XuatVatTu_Add(obj.GoodsIds, obj.Id, obj.SalonId, obj.RecipientId, obj.CreatedDate);

                if (error == 0)
                {
                    error += AddFlow(obj.GoodsIds, obj.Id);
                }

                if (error == 0)
                {
                    // Thông báo thành công
                    var MsgParam = new List<KeyValuePair<string, string>>();
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                    UIHelpers.Redirect("/my-pham/xuat-nv/xuat-moi.html", MsgParam);
                }
                else
                {
                    MsgSystem.Text = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                    MsgSystem.CssClass = "msg-system warning";
                }
            }
        }
        /// <summary>
        /// Cập nhật lô mỹ phẩm
        /// </summary>
        /// <param name="Id"></param>
        private void Update(int Id)
        {
            if (Id > 0)
            {
                using (var db = new Solution_30shineEntities())
                {
                    var serialize = new JavaScriptSerializer();
                    var error = 0;
                    OBJ = db.ExportGoods.FirstOrDefault(w => w.Id == Id);
                    if (OBJ != null)
                    {
                        OBJ.SalonId = Convert.ToInt32(Salon.SelectedValue);
                        OBJ.ModifiedDate = DateTime.Now;
                        OBJ.RecipientId = getRecipientId();
                        OBJ.Note = Description.Text;
                        OBJ.GoodsIds = HDF_ProductIds.Value != "[]" ? HDF_ProductIds.Value : "";
                        OBJ.GoodsIds = serialize.Serialize(Library.Function.genListFromJson(OBJ.GoodsIds));
                        db.ExportGoods.AddOrUpdate(OBJ);
                        error += db.SaveChanges() > 0 ? 0 : 1;

                        // Update Statistics_XuatVatTu
                        if (OBJ.GoodsIds != null)
                            Statistics_XuatVatTu_Update(OBJ.GoodsIds, OBJ.Id, OBJ.SalonId, OBJ.RecipientId, OBJ.CreatedDate);
                        if (error == 0)
                        {
                            error += UpdateFlow(OBJ.GoodsIds, OBJ.Id);
                        }
                        if (error == 0)
                        {
                            // Thông báo thành công
                            var MsgParam = new List<KeyValuePair<string, string>>();
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                            MsgParam.Add(new KeyValuePair<string, string>("id", Id.ToString()));
                            UIHelpers.Redirect("/my-pham/xuat-nv/xuat-moi.html", MsgParam);
                        }
                        else
                        {
                            MsgSystem.Text = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                            MsgSystem.CssClass = "msg-system warning";
                        }
                    }
                }
            }
        }
        /// <summary>
        /// add mức dùng vật tư cho nhân viên
        /// </summary>
        /// <param name="ExportGoods_ID">mã bill xuất vật tư </param>
        /// <param name="Salon_ID">Salon Id</param>
        /// <param name="Staff_ID"> mã nhân viên nhận sản phẩm</param>
        /// <param name="Date_XuatVatTu">ngày xuất vật tư </param>
        /// <param name="Product_ID"> id sản phẩm khi mà xuất vật tư cho nhân viên</param>
        private void Statistics_XuatVatTu_Add(string Goods_ID, int ExportGoods_ID, int? Salon_ID, int Staff_ID, DateTime? Date_XuatVatTu)
        {
            //var error = 0;
            try
            {
                if (ExportGoods_ID > 0)
                {
                    using (var db = new Solution_30shineEntities())
                    {
                        var list = Library.Function.genListFromJson(Goods_ID);
                        foreach (var v in list)
                        {
                            if (v.IsCheckVatTu == true)
                            {
                                var objAdd = db.Statistics_XuatVatTu.FirstOrDefault(a => a.IsDelete == false && a.ExportGoods_ID == ExportGoods_ID && a.Product_ID == v.Id);
                                if (objAdd != null)
                                {
                                    objAdd.ModifiedTime = DateTime.Now;
                                }
                                else
                                {
                                    objAdd = new Statistics_XuatVatTu();
                                    objAdd.ExportGoods_ID = ExportGoods_ID;
                                    objAdd.TotalBill_XuatVatTu = 0;
                                    objAdd.Product_ID = v.Id;
                                    objAdd.IsDelete = false;
                                    objAdd.CreateTime = DateTime.Now;
                                }
                                objAdd.Date_XuatVatTu = Date_XuatVatTu;
                                objAdd.Salon_ID = Salon_ID;
                                objAdd.Staff_ID = Staff_ID;
                                db.Statistics_XuatVatTu.AddOrUpdate(objAdd);
                                db.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Update mức dùng vật tư cho nhân viên
        /// </summary>
        /// <param name="ExportGoods_ID">mã bill xuất vật tư </param>
        /// <param name="Salon_ID">Salon Id</param>
        /// <param name="Staff_ID"> mã nhân viên nhận sản phẩm</param>
        /// <param name="Date_XuatVatTu">ngày xuất vật tư </param>
        /// <param name="Product_ID"> id sản phẩm khi mà xuất vật tư cho nhân viên</param>
        private void Statistics_XuatVatTu_Update(string Goods_ID, int ExportGoods_ID, int? Salon_ID, int Staff_ID, DateTime? Date_XuatVatTu)
        {
            //var error = 0;
            try
            {
                if (ExportGoods_ID > 0)
                {
                    using (var db = new Solution_30shineEntities())
                    {
                        var obj = new ProductBasic();
                        var index = -1;
                        var goods = new Product();
                        var list = Library.Function.genListFromJson(Goods_ID);
                        var Stattis = db.Statistics_XuatVatTu.Where(w => w.ExportGoods_ID == ExportGoods_ID).ToList();

                        if (Stattis.Count > 0 && list.Count > 0)
                        {
                            // kiểm tra xem sản phẩm đã tồn tại chưa, phục vụ trường hợp bị xóa
                            foreach (var v in Stattis)
                            {
                                goods = db.Products.FirstOrDefault(w => w.Id == v.Product_ID);
                                index = list.FindIndex(w => w.Id == v.Product_ID);
                                if (index == -1)
                                {
                                    v.IsDelete = true;
                                }
                                else
                                {
                                    obj = list[index];
                                    v.ModifiedTime = DateTime.Now;
                                    v.IsDelete = false;
                                }
                                db.Statistics_XuatVatTu.AddOrUpdate(v);
                                db.SaveChanges();
                            }
                            // kiểm tra add sản phẩm or update vào bảng Statistics_XuatVatTu
                            foreach (var v in list)
                            {
                                if (v.IsCheckVatTu == true)
                                {
                                    var objAdd = db.Statistics_XuatVatTu.FirstOrDefault(a => a.IsDelete == false && a.ExportGoods_ID == ExportGoods_ID && a.Product_ID == v.Id);
                                    if (objAdd != null)
                                    {
                                        objAdd.ModifiedTime = DateTime.Now;
                                    }
                                    else
                                    {
                                        objAdd = new Statistics_XuatVatTu();
                                        objAdd.ExportGoods_ID = ExportGoods_ID;
                                        objAdd.TotalBill_XuatVatTu = 0;
                                        objAdd.Product_ID = v.Id;
                                        objAdd.IsDelete = false;
                                        objAdd.CreateTime = DateTime.Now;
                                    }
                                    objAdd.Date_XuatVatTu = Date_XuatVatTu;
                                    objAdd.Salon_ID = Salon_ID;
                                    objAdd.Staff_ID = Staff_ID;
                                    db.Statistics_XuatVatTu.AddOrUpdate(objAdd);
                                    db.SaveChanges();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private int AddFlow(string goodsIds, int ExportGoodsId)
        {
            var error = 0;
            if (goodsIds != "")
            {
                using (var db = new Solution_30shineEntities())
                {
                    var obj = new FlowGood();
                    var list = Library.Function.genListFromJson(goodsIds);
                    var goods = new Product();
                    if (list.Count > 0)
                    {
                        foreach (var v in list)
                        {
                            goods = db.Products.FirstOrDefault(w => w.Id == v.Id);
                            obj = new FlowGood();
                            obj.ExportGoodsId = ExportGoodsId;
                            obj.GoodsId = v.Id;
                            obj.Quantity = v.Quantity;
                            obj.Cost = v.Price;
                            if (goods != null)
                            {
                                obj.Price = Convert.ToInt32(goods.Price);
                            }
                            if (TxtDateTimeFrom.Text != "")
                            {
                                obj.CreatedDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                            }
                            else
                            {
                                obj.CreatedDate = DateTime.Now;
                            }
                            obj.IsDelete = 0;
                            db.FlowGoods.Add(obj);
                            error += db.SaveChanges() > 0 ? 0 : 1;
                        }
                    }
                }
            }
            return error;
        }

        private int UpdateFlow(string goodsIds, int ExportGoodsId)
        {
            var error = 0;
            if (goodsIds != "")
            {
                using (var db = new Solution_30shineEntities())
                {
                    var objFlow = new FlowGood();
                    var index = -1;
                    var obj = new ProductBasic();
                    var list = Library.Function.genListFromJson(goodsIds);
                    var flow = db.FlowGoods.Where(w => w.ExportGoodsId == ExportGoodsId).ToList();
                    var goods = new Product();
                    if (flow.Count > 0 && list.Count > 0)
                    {
                        foreach (var v in flow)
                        {
                            goods = db.Products.FirstOrDefault(w => w.Id == v.GoodsId);
                            index = list.FindIndex(w => w.Id == v.GoodsId);
                            if (index == -1)
                            {
                                v.IsDelete = 1;
                            }
                            else
                            {
                                v.Quantity = obj.Quantity;
                                v.Cost = obj.Price;
                                v.ModifiedDate = DateTime.Now;
                                if (goods != null)
                                {
                                    v.Price = Convert.ToInt32(goods.Cost);
                                }
                                v.IsDelete = 0;
                            }
                            db.FlowGoods.AddOrUpdate(v);
                            db.SaveChanges();
                        }
                        foreach (var v in list)
                        {
                            var flowAdd = db.FlowGoods.Where(w => w.ExportGoodsId == ExportGoodsId && w.GoodsId == v.Id).ToList();
                            if (flowAdd.Count == 0)
                            {
                                objFlow = new FlowGood();
                                objFlow.ExportGoodsId = ExportGoodsId;
                                objFlow.GoodsId = v.Id;
                                objFlow.Quantity = v.Quantity;
                                objFlow.Cost = v.Price;
                                if (goods != null)
                                {
                                    objFlow.Price = Convert.ToInt32(goods.Price);
                                }
                                if (TxtDateTimeFrom.Text != "")
                                {
                                    objFlow.CreatedDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                                }
                                else
                                {
                                    objFlow.CreatedDate = DateTime.Now;
                                }
                                objFlow.IsDelete = 0;
                                db.FlowGoods.Add(objFlow);
                                db.SaveChanges();
                            }
                        }

                    }
                }
            }
            return error;
        }

        protected void Action(object sender, EventArgs e)
        {
            if (Id > 0)
            {
                Update(Id);
            }
            else
            {
                Add();
            }
        }

        private bool Bind_OBJ()
        {
            var isset = false;
            using (var db = new Solution_30shineEntities())
            {
                if (Id > 0)
                {
                    OBJ = db.ExportGoods.FirstOrDefault(w => w.Id == Id);
                    if (OBJ != null)
                    {
                        isset = true;
                        // bind salon selected
                        var ItemSelected = Salon.Items.FindByValue(OBJ.SalonId.ToString());
                        if (ItemSelected != null)
                        {
                            ItemSelected.Selected = true;
                        }
                        else
                        {
                            Salon.SelectedIndex = 0;
                        }
                        // bind người nhận mỹ phẩm
                        if (OBJ.RecipientId > 0)
                        {
                            var staff = db.Staffs.FirstOrDefault(w => w.Id == OBJ.RecipientId);
                            if (staff != null)
                            {
                                FVRecipient.InnerText = staff.OrderCode.ToString() + "- " + staff.Fullname;
                                InputRecipient.Text = staff.OrderCode.ToString();
                                Recipient.Value = staff.Id.ToString();
                            }
                        }
                        // bind danh sách mỹ phẩm
                        if (OBJ.GoodsIds != "")
                        {
                            Rpt_Product_Flow.DataSource = Library.Function.genListFromJson(OBJ.GoodsIds);
                            Rpt_Product_Flow.DataBind();
                            ListingProductWp.Style.Add("display", "block");
                        }
                        // bind note
                        Description.Text = OBJ.Note;
                    }
                    if (!isset)
                    {
                        MsgSystem.Text = "Lỗi. Dữ liệu không tồn tại.";
                        MsgSystem.CssClass = "msg-system warning";
                    }
                }
            }

            return isset;
        }
        

        private void Bind_RptProduct()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Products = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1).OrderByDescending(o => o.Id).ToList();

                Rpt_Product.DataSource = _Products;
                Rpt_Product.DataBind();
            }
        }

        //private void Bind_RptProductFeatured()
        //{
        //    using (var db = new Solution_30shineEntities())
        //    {
        //        var lst = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1 && w.Status == 1).OrderByDescending(o => o.Id).ToList();

        //        Rpt_ProductFeatured.DataSource = lst;
        //        Rpt_ProductFeatured.DataBind();
        //    }
        //}

        private void bindProductCategory()
        {
            using (var db = new Solution_30shineEntities())
            {
                //var list = db.Tbl_Category.Where(w=>w.Pid == 1 && w.IsDelete != 1 && w.Publish == true).ToList();
                ////var c = new Tbl_Category();
                ////c.Id = 0;
                ////c.Name = "Chọn danh mục";
                ////list.Insert(0, c);
                //rptProductCategory.DataSource = list;
                //rptProductCategory.DataBind();
                var list = new List<Tbl_Category>();
                var node = new List<Tbl_Category>();
                var c = new Tbl_Category();
                c.Id = 0;
                c.Name = "Chọn danh mục";
                list.Insert(0, c);
                var cateRoot = db.Tbl_Category.Where(w => w.IsDelete != 1 && w.Pid == 1 && w.Publish == true).ToList();
                if (cateRoot.Count > 0)
                {
                    foreach (var v in cateRoot)
                    {
                        node = db.Tbl_Category.Where(w => w.IsDelete != 1 && w.Pid == v.Id).ToList();
                        list.Add(v);
                        if (node.Count > 0)
                        {
                            foreach (var v2 in node)
                            {
                                v2.Name = "----- " + v2.Name;
                                list.Add(v2);
                            }
                        }
                    }
                }
                rptProductCategory.DataSource = list;
                rptProductCategory.DataBind();
            }
        }

        /// <summary>
        /// Reset Rating Temp Value
        /// </summary>
        private void resetRatingTemp()
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                /// Reset giá trị rating temp
                int accId = Session["User_Id"] != null ? int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0 : 0;
                int salonId = Session["SalonId"] != null ? int.TryParse(Session["SalonId"].ToString(), out integer) ? integer : 0 : 0;
                var ratingTemp = db.Rating_Temp.FirstOrDefault(w => w.SalonId == salonId && w.AccountId == accId);
                if (ratingTemp != null)
                {
                    ratingTemp.RatingValue = 0;
                    db.Rating_Temp.AddOrUpdate(ratingTemp);
                    db.SaveChanges();
                }
            }
        }

        [WebMethod]
        public static string GetCustomer(string CustomerCode)
        {
            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                var _Customer = db.Customers.Where(w => w.Customer_Code == CustomerCode).ToList();

                if (_Customer.Count > 0)
                {
                    _Msg.success = true;
                    _Msg.msg = serializer.Serialize(_Customer);
                }
                else
                {
                    _Msg.success = false;
                }
            }
            return serializer.Serialize(_Msg);
        }

        [WebMethod]
        public static string CheckDuplicateBill(string CustomerCode)
        {
            var _Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var today = Convert.ToDateTime(DateTime.Now.ToString("d/M/yyyy"), culture);
                var todayL = today.AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                var todayR = today.AddDays(1);
                var _Bill = db.BillServices.FirstOrDefault(w => w.CreatedDate > todayL && w.CreatedDate < todayR && w.CustomerCode == CustomerCode);

                if (_Bill != null)
                {
                    _Msg.success = true;
                    _Msg.msg = serializer.Serialize(_Bill);
                }
                else
                {
                    _Msg.success = false;
                }
            }
            return serializer.Serialize(_Msg);
        }

        /// <summary>
        /// Lấy toàn bộ danh sách vật tư xuất cho salon
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static object getAllGoods()
        {
            using (var db = new Solution_30shineEntities())
            {
                return db.Products.Where(w => w.IsDelete != 1/* && w.Publish == 1*/ && w.CategoryId == 20).Select(s => new { s.Id, s.Code, s.Name, s.Cost, s.Price, Quantity = 1, IsCheckVatTu = s.IsCheckVatTu == null ? false : s.IsCheckVatTu }).ToList();
            }
        }

    }
}