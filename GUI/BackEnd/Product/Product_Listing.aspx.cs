﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Web.Services;
using System.Data.Entity.Migrations;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using System.Data.SqlClient;
using System.Data;

namespace _30shine.GUI.BackEnd.UIProduct
{
    public partial class Product_Listing : System.Web.UI.Page
    {
        private string PageID = "QL_SP";
        protected Paging PAGING = new Paging();
        private bool Perm_Access = false;
        public int categoryId = 0;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                ////Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {

            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                Bind_ProductCategory();
                Bind_Paging();
                Bind_RptProduct();
                RemoveLoading();
            }
        }
        /// <summary>
        /// Check mức dùng vật tư
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="isChecked"> Checck vat tu theo doi muc dung </param>
        [WebMethod]
        public static object CheckVatTu(int Id, int isChecked)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var data = db.Products.FirstOrDefault(w => w.Id == Id);
                    if (data != null)
                    {
                        data.IsCheckVatTu = Convert.ToBoolean(isChecked);
                        db.Products.AddOrUpdate(data);
                        db.SaveChanges();
                    }
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private string genSql()
        {
            var sql = "";
            int integer;
            categoryId = int.TryParse(ddlCategory.SelectedValue, out integer) ? integer : 0;
            sql = $@"SELECT * FROM Product WHERE IsDelete = 0 and ((CategoryId = @categoryId) OR (@categoryId = 0)) and ('1' = @param1 or Name like N'%'+@param1+'%') ORDER BY Name ASC";
            return sql;
        }

        private void Bind_RptProduct()
        {
            using (var db = new Solution_30shineEntities())
            {
                var productNameTmp = productName.Text;
                var param1 = "1";
                int integer;
                var categoryIdTmp = int.TryParse(ddlCategory.SelectedValue, out integer) ? integer : 0;
                //var param2 = "1";
                if (productNameTmp != null || !productNameTmp.Trim().Equals(""))
                {
                    param1 = productNameTmp;
                }
                var parameters = new[] {
                    new SqlParameter("@categoryId", SqlDbType.Int) { Value = categoryIdTmp },
                    new SqlParameter("@param1", SqlDbType.NVarChar) { Value = param1 }
                };
                //hiển thị d.sách sản phẩm trong order theo số thứ tự
                var productTemp = db.Products.SqlQuery(genSql(), parameters).OrderBy(w => w.Name).ToList();
                var _Product = productTemp.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                RptProduct.DataSource = _Product;
                RptProduct.DataBind();
            }
        }

        private void Bind_ProductCategory()
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = new List<Tbl_Category>();
                var node = new List<Tbl_Category>();
                var cateRoot = db.Tbl_Category.Where(w => w.IsDelete != 1 && w.Pid == 1).ToList();
                if (cateRoot.Count > 0)
                {
                    foreach (var v in cateRoot)
                    {
                        node = db.Tbl_Category.Where(w => w.IsDelete != 1 && w.Pid == v.Id).ToList();
                        list.Add(v);
                        if (node.Count > 0)
                        {
                            foreach (var v2 in node)
                            {
                                v2.Name = "----- " + v2.Name;
                                list.Add(v2);
                            }
                        }
                    }
                }
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "Id";
                ListItem item = new ListItem("Chọn danh mục", "0");
                ddlCategory.DataSource = list;
                ddlCategory.DataBind();
                ddlCategory.Items.Insert(0, item);
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
            Bind_RptProduct();
            RemoveLoading();
        }

        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                var productNameTmp = productName.Text;
                var param1 = "1";
                int integer;
                var categoryIdTmp = int.TryParse(ddlCategory.SelectedValue, out integer) ? integer : 0;
                //var param2 = "1";
                if (productNameTmp != null || !productNameTmp.Trim().Equals(""))
                {
                    param1 = productNameTmp;
                }
                var parameters = new[] {
                    new SqlParameter("@categoryId", SqlDbType.Int) { Value = categoryIdTmp },
                    new SqlParameter("@param1", SqlDbType.VarChar) { Value = param1 }
                };
                var Count = db.Products.SqlQuery(genSql(), parameters).OrderBy(w => w.Order).Count();
                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }


    }
}