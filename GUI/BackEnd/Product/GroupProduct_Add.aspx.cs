﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Data;
namespace _30shine.GUI.BackEnd.UIProduct
{
    public partial class GroupProduct_Add : System.Web.UI.Page
    {
        protected string _Code;
        protected bool _IsUpdate = false;
        protected Tbl_GroupProduct OBJ;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (IsUpdate())
                {
                    Bind_OBJ();
                }
            }
        }
        #region[IsUpdate]
        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }
        #endregion
        #region[Add]
        private void AddGroupProduct()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                   
                    var obj = new Tbl_GroupProduct();
                    obj.Name = Name.Text;
                    obj.CreatedTime = DateTime.Now;
                    var Error = false;
                    if (!Error)
                    {
                        db.Tbl_GroupProduct.Add(obj);
                        var exc = db.SaveChanges();
                        if (exc > 0)
                        {

                            var MsgParam = new List<KeyValuePair<string, string>>();
                            MsgParam.Add(new KeyValuePair<string, string>("msg_add_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_add_message", "Thêm mới thành công!"));
                            UIHelpers.Redirect("/admin/groupproduct/danh-sach.html", MsgParam);
                        }
                        else
                        {
                            var msg = "Thêm mới thất bại! Vui lòng liên hệ nhóm phát triển.";
                            var status = "warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                        }
                    }

                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        #endregion
        #region[Update]
        private void UpdateGroupProduct()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    int Id;
                    if (int.TryParse(_Code, out Id))
                    {
                        OBJ = db.Tbl_GroupProduct.FirstOrDefault(w => w.Id == Id);
                        if (!OBJ.Equals(null))
                        {
                            OBJ.Name = Name.Text;
                            OBJ.ModifiedTime = DateTime.Now;
                            db.Tbl_GroupProduct.AddOrUpdate(OBJ);
                            var exc = db.SaveChanges();
                            if (exc > 0)
                            {
                                var MsgParam = new List<KeyValuePair<string, string>>();
                                MsgParam.Add(new KeyValuePair<string, string>("msg_add_status", "success"));
                                MsgParam.Add(new KeyValuePair<string, string>("msg_add_message", "Cập nhật thành công!"));
                                UIHelpers.Redirect("/admin/groupproduct/danh-sach.html", MsgParam);
                            }
                            else
                            {
                                var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                                var status = "warning";
                                UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                            }
                        }
                        else
                        {
                            var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                            var status = "warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        #endregion
        #region[BindOBJ]
        private bool Bind_OBJ()
        {
            using (var db = new Solution_30shineEntities())
            {
                var ExistOBJ = true;
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Tbl_GroupProduct.Where(w => w.Id == Id).FirstOrDefault();
                    if (!OBJ.Equals(null))
                    {
                        Name.Text = OBJ.Name;
                    }
                }
                else
                {
                    ExistOBJ = false;
                    var msg = "Lỗi! Nhóm sản phẩm không tồn tại";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }
                return ExistOBJ;
            }
        }
        #endregion
        #region[Button]
        protected void ExcAddOrUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
                UpdateGroupProduct();
            else
                AddGroupProduct();
        }
        #endregion
    }
}