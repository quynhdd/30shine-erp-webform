﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
namespace _30shine.GUI.BackEnd.UIProduct
{
    public partial class GroupProduct_Listing : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        private bool Perm_Access = false;
        protected bool Perm_ShowSalon = false;
        protected bool IsAccountant = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckPermission();
            if (!IsPostBack)
            {
                Bind_Data();
            }
        }
        #region[BindRpt]
        private void Bind_Data()
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = db.Tbl_GroupProduct.ToList();
                if (list.Count > 0)
                {
                    Bind_Paging(list.Count);
                    RptProduct.DataSource = list.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                    RptProduct.DataBind();
                }
            }
        }
        #endregion
        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }
        #region[CheckPermission]
        private void CheckPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "salonmanager", "checkin", "checkout" };
                string[] AllowSalon = new string[] { "root", "admin" };
                string[] Accountant = new string[] { "accountant" };
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }
                if (Array.IndexOf(AllowSalon, Permission) != -1)
                {
                    Perm_ShowSalon = true;
                }
                if (Array.IndexOf(Accountant, Permission) != -1)
                {
                    IsAccountant = true;
                }
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        #endregion
    }
}