﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Product_Listing.aspx.cs" Inherits="_30shine.GUI.BackEnd.UIProduct.Product_Listing" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý Sản phẩm &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/admin/san-pham/danh-sach.html">Danh sách</a></li>
                        <li class="li-add"><a href="/admin/san-pham/them-moi.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing be-report">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row row-filter">
                    <strong class="st-head"><i class="fa fa-filter"></i>Lọc kết quả</strong>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlCategory" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px;"></asp:DropDownList>
                        <asp:TextBox ID="productName" runat="server" ClientIDMode="Static" placeholder="Tên sản phẩm" Style="margin-left: 10px!important"></asp:TextBox>
                    </div>
                    <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <a href="/admin/san-pham/danh-sach.html" class="st-head btn-viewdata">Reset Filter</a>
                </div>
                <!-- End Filter -->
                <!-- Row Table Filter -->
                <div class="table-func-panel">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="table-wp">
                            <table class="table-add table-listing">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên sản phẩm</th>
                                        <th>Mã sản phẩm</th>
                                        <th>Mã barcode</th>
                                        <th>Mô tả</th>
                                        <th>Giá nhập</th>
                                        <th>Giá bán</th>
                                        <%if (categoryId == 98)
                                            {%>
                                        <th>Thời hạn sử dụng thẻ</th>
                                        <th>Loại thẻ</th>
                                        <%} %>
                                        <th>Chiết khấu lương (%)</th>
                                        <th>Theo dõi tồn kho</th>
                                        <th>Nổi bật</th>
                                        <th>Publish</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="RptProduct" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td>
                                                    <a href="/admin/san-pham/<%# Eval("Id") %>.html"><%# Eval("Name") %></a>
                                                </td>
                                                <td><%# Eval("Code") %></td>
                                                <td><%# Eval("BarCode") %></td>
                                                <td><%# Eval("Description") %></td>
                                                <td><%# Eval("Cost")%></td>
                                                <td><%# Eval("Price") %></td>
                                                <%if (categoryId == 98)
                                                    {%>
                                                <td><%# Eval("MemberAddDay") %></td>
                                                <td><%# Convert.ToInt32(Eval("MemberTypeAdd")) == 1 ? "Thẻ tháng" : "Thẻ ngày"%></td>
                                                <%} %>
                                                <td><%# Eval("ForSalary") %></td>
                                                <td>
                                                    <input type="checkbox" <%# Convert.ToInt32(Eval("IsCheckVatTu")) == 1 ? "Checked=\"True\"" : "" %> class="chkisDelete" data-id="<%# Eval("Id") %>" id="<%# Eval("Id") %>" />
                                                </td>
                                                <td>
                                                    <input type="checkbox" <%# Convert.ToInt32(Eval("Status")) == 1 ? "Checked=\"True\"" : "" %>
                                                        onclick="checkFeatured($(this), 'Product')" data-id="<%# Eval("Id") %>" />
                                                </td>
                                                <td class="map-edit">
                                                    <input type="checkbox"
                                                        <%# Convert.ToInt32(Eval("Publish")) == 1 ? "Checked=\"True\"" : "" %>
                                                        onclick="checkPublish($(this), 'Product')" data-id="<%# Eval("Id") %>" />
                                                    <div class="edit-wp">
                                                        <a class="elm edit-btn" href="/admin/san-pham/<%# Eval("Id") %>.html" title="Sửa"></a>
                                                        <a class="elm del-btn" onclick="del(this.parentNode.parentNode.parentNode,'<%# Eval("Code") %>', '<%# Eval("Name") %>')" href="javascript://" title="Xóa"></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>

                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
            </div>
            <%-- END Listing --%>
        </div>

        <script>
            jQuery(document).ready(function () {
                $("#glbAdminContent").addClass("active");
                $("#glbAdminProduct").addClass("active");
                $("#subMenu .li-listing").addClass("active");
            });

            //============================
            // Event delete
            //============================
            function del(This, code, name) {
                var code = code || null,
                    name = name || null,
                    Row = This;
                if (!code) return false;

                // show EBPopup
                $(".confirm-yn").openEBPopup();
                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

                $("#EBPopup .yn-yes").bind("click", function () {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/SystemService/Ajax/Del.aspx/Delele_Product",
                        data: '{Code : "' + code + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            var mission = JSON.parse(response.d);
                            if (mission.success) {
                                delSuccess();
                                Row.remove();
                            } else {
                                delFailed();
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    });
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
            }

            //============================
            // Event publish, featured
            //============================
            function checkFeatured(This, table) {
                var Id = This.attr("data-id");
                var isChecked = This.is(":checked");
                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Ajax/Publish_Featured.aspx/Featured",
                    data: '{Id : "' + Id + '", Table : "' + table + '", isChecked : "' + isChecked + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            alert("Chuyển trạng thái thành công.");
                        } else {
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function checkPublish(This, table) {
                var Id = This.attr("data-id");
                var isChecked = This.is(":checked");
                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Ajax/Publish_Featured.aspx/Publish",
                    data: '{Id : "' + Id + '", Table : "' + table + '", isChecked : "' + isChecked + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            alert("Chuyển trạng thái thành công.");
                        } else {
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }
        </script>
        <script>
            //upDate New QuynhDD
            $(document).on("change", ".chkisDelete", function (e) {
                //debugger;
                var Id = $(this).data("id");
                var isChecked;
                if (this.checked)
                    isChecked = 1;
                else
                    isChecked = 0;
                $.ajax({
                    type: "POST",
                    contentType: "application/json;charset=UTF-8",
                    url: "/GUI/BackEnd/Product/Product_Listing.aspx/CheckVatTu",
                    data: '{Id:' + Id + ', isChecked:' + isChecked + '}',
                    dataType: "JSON",
                    success: function (response) {
                        //console.log(response.d);
                        if (response.d != null) {
                            alert("Chuyển trạng thái thành công!");
                        }
                        else {
                            alert("Có lỗi xảy ra . Vui lòng liên hệ với nhóm phát triển !");
                            //$(this).data("id").prop('checked', false);
                            if (isChecked == 1) {
                                $('.chkisDelete[data-id="' + Id + '"]').prop('checked', false);
                            }
                            else {
                                $('.chkisDelete[data-id="' + Id + '"]').prop('checked', true);
                            }

                        }
                    },
                    failure: function (response) { console.log(response.d); }
                });
            });
        </script>

    </asp:Panel>
</asp:Content>
