﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.UIProduct
{
    public partial class Product_Add : System.Web.UI.Page
    {
        public static bool chkCheck;
        private bool Perm_Access = false;
        protected string ListProduct;
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                Bind_ProductCategory();
                bindBrands();
                //Bind_RptProduct();
                BindGroupProduct();

            }
            ListProduct = getProducts();
        }

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                pageId = permissionModel.GetRegexPageIdSpecial().Replace(pageId, ".html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            using (var db = new Solution_30shineEntities())
            {
                Code.Enabled = false;
                Code.Text = GenCode(db.Products.Count());
            }
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        /// <summary>
        /// Check combo
        /// </summary>
        /// <param name="isCheckCombo"></param>
        [WebMethod]
        public static void CheckCombo(bool isCheckCombo)
        {
            try
            {
                var message = new Library.Class.cls_message();
                if (isCheckCombo == true)
                {
                    chkCheck = true;
                }
                else
                {
                    chkCheck = false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Add thêm trường chkCombo, MapIdPro, ProCombo
        /// </summary>
        protected void AddProduct(object sender, EventArgs e)
        {

            using (var db = new Solution_30shineEntities())
            {
                var obj = new MODEL.ENTITY.EDMX.Product();
                int integer;
                float floatVar;
                string mapIdPro = HDF_ProductIds.Value;
                obj.Name = Name.Text;
                obj.Code = GenCode(db.Products.Count());
                obj.Description = Description.Text;
                //obj.Cost = int.TryParse(Cost.Text, out integer) ? integer : 0;
                obj.VoucherPercent = int.TryParse(VoucherPercent.Text, out integer) ? integer : 0;
                obj.ForSalary = float.TryParse(ForSalary.Text, out floatVar) ? floatVar : 0;
                obj.CategoryId = int.TryParse(ddlCategory.SelectedValue, out integer) ? integer : 0;
                obj.BarCode = Barcode.Text.Trim();
                obj.MemberAddDay = int.TryParse(TimeUsedMember.Text.Trim(), out integer) ? integer : 0;
                obj.MemberTypeAdd = int.TryParse(ddlMembershipType.SelectedValue, out integer) ? integer : 0;
                obj.CoefficientOfWaitingDays = int.TryParse(CoefficientOfWaitingDaysTxt.Text.Trim(), out integer) ? integer : 1;
                if (HDF_InventoryType.Value != "[]" && !string.IsNullOrEmpty(HDF_InventoryType.Value))
                {
                    obj.InventoryType = HDF_InventoryType.Value;
                }
                //add tên theo nhóm sản phẩm
                if (ddlGroupProduct.SelectedValue.ToString() == "0")
                {
                    string name = Name.Text;
                    if (name.StartsWith("MN_LK_"))
                    {
                        var matches = (from m in db.Tbl_GroupProduct
                                       where m.Id == 1
                                       select m).FirstOrDefault();
                        obj.GroupProductId = matches.Id;
                    }
                    else
                    {
                        var _obj = new Tbl_GroupProduct();
                        _obj.Name = Name.Text;
                        _obj.CreatedTime = DateTime.Now;
                        db.Tbl_GroupProduct.Add(_obj);
                        db.SaveChanges();
                        var _list = db.Tbl_GroupProduct.OrderByDescending(o => o.Id).FirstOrDefault();
                        obj.GroupProductId = _list.Id;
                    }
                }
                else
                {
                    obj.GroupProductId = Convert.ToInt32(ddlGroupProduct.SelectedValue);
                }
                if (Order.Text == "")
                {
                    obj.Order = 0;
                }
                else
                {
                    obj.Order = Convert.ToInt32(Order.Text);
                }
                obj.IsDelete = 0;
                obj.CreatedDate = DateTime.Now;
                //---------
                obj.BrandId = int.TryParse(Brand.SelectedValue, out integer) ? integer : 0;
                obj.OnWebId = int.TryParse(OnWebId.Text, out integer) ? integer : 0;
                obj.Volume = Volume.Text;
                obj.ModelName = ModelName.Text;

                //---------
                if (chkInventoryHC.Checked == true)
                {
                    obj.CheckInventoryHC = true;
                }
                else
                {
                    obj.CheckInventoryHC = false;
                }
                if (chkVattu.Checked == true)
                {
                    obj.IsCheckVatTu = true;
                }
                else
                {
                    obj.IsCheckVatTu = false;
                }
                if (Publish.Checked == true)
                {
                    obj.Publish = 1;
                }
                else
                {
                    obj.Publish = 0;
                }

                if (Focus.Checked == true)
                {
                    obj.Status = 1;
                }
                else
                {
                    obj.Status = 0;
                }
                if (chkCheck == true)
                {
                    obj.Cost = Convert.ToInt32(HDF_TotalMoney.Value);
                    obj.MapIdProduct = mapIdPro;
                    obj.CheckCombo = true;
                    obj.Price = int.TryParse(Price.Text, out integer) ? integer : 0;
                }
                else
                {
                    obj.Cost = int.TryParse(Cost.Text, out integer) ? integer : 0;
                    obj.Price = int.TryParse(Price.Text, out integer) ? integer : 0;
                    obj.CheckCombo = false;
                }
                //
                if (ChkStopBusiness.Checked == true)
                {
                    obj.StopBusiness = 1;
                }
                else
                {
                    obj.StopBusiness = 0;
                }
                // Validate
                // Check trùng mã sản phẩm
                var check = db.Products.Where(w => w.Code == obj.Code).ToList();
                var Error = false;
                if (check.Count > 0)
                {
                    MsgSystem.Text = "Mã sản phẩm đã tồn tại. Bạn vui lòng nhập mã sản phẩm khác.";
                    MsgSystem.CssClass = "msg-system warning";
                    Error = true;
                }
                //check trùng barcode
                var checkBarCode = db.Products.Where(w => w.BarCode == obj.BarCode && w.IsDelete == 0).ToList();
                if (checkBarCode.Count > 0)
                {
                    MsgSystem.Text = "Mã barcode đã tồn tại. Bạn vui lòng nhập mã barcode khác.";
                    MsgSystem.CssClass = "msg-system warning";
                    Error = true;
                }
                if (!Error)
                {
                    db.Products.Add(obj);
                    db.SaveChanges();

                    // Thông báo thành công
                    var MsgParam = new List<KeyValuePair<string, string>>();
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                    UIHelpers.Redirect("/admin/san-pham/danh-sach.html");
                }
            }
        }

        /// <summary>
        /// Get product
        /// </summary>
        /// <returns></returns>
        private string getProducts()
        {
            using (var db = new Solution_30shineEntities())
            {
                var data = db.Products.Where(w => w.IsDelete == 0 && w.Publish == 1 && w.CategoryId != 95).OrderByDescending(o => o.Id).ToList();
                var serialize = new JavaScriptSerializer();
                return serialize.Serialize(data);
            }
        }

        /// <summary>
        /// Bind group product
        /// </summary>
        private void BindGroupProduct()
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = db.Tbl_GroupProduct.ToList();
                ddlGroupProduct.Items.Add(new ListItem("Chọn tên nhóm sản phẩm", "0"));
                if (list.Count > 0)
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        ddlGroupProduct.Items.Add(new ListItem(list[i].Name, list[i].Id.ToString()));
                    }
                }
                ddlGroupProduct.DataBind();
            }
        }

        /// <summary>
        /// Bind category
        /// </summary>
        private void Bind_ProductCategory()
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = new List<Tbl_Category>();
                var node = new List<Tbl_Category>();
                var cateRoot = db.Tbl_Category.Where(w => w.IsDelete != 1 && w.Pid == 1 && w.Publish == true).ToList();
                if (cateRoot.Count > 0)
                {
                    foreach (var v in cateRoot)
                    {
                        node = db.Tbl_Category.Where(w => w.IsDelete != 1 && w.Pid == v.Id && w.Publish == true).ToList();
                        list.Add(v);
                        if (node.Count > 0)
                        {
                            foreach (var v2 in node)
                            {
                                v2.Name = "----- " + v2.Name;
                                list.Add(v2);
                            }
                        }
                    }
                }
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "Id";
                ListItem item = new ListItem("Chọn danh mục", "0");
                ddlCategory.DataSource = list;
                ddlCategory.DataBind();
                ddlCategory.Items.Insert(0, item);
            }
        }

        private void bindBrands()
        {
            using (var db = new Solution_30shineEntities())
            {
                Brand.DataTextField = "TenThuongHieu";
                Brand.DataValueField = "Id";
                var lst = db.Brands.Where(w => w.Status == true).ToList();
                Brand.DataSource = lst;
                Brand.DataBind();
                var item = new ListItem("Chọn thương hiệu", "0");
                Brand.Items.Insert(0, item);
            }
        }

        public string GenCode(int CodeInt)
        {
            string Code = "";
            int CodeLen = 5;
            try
            {
                Code = (CodeInt + 2).ToString();
                if (Code.Length < CodeLen)
                {
                    int loop = CodeLen - Code.Length;
                    for (int i = 0; i < loop; i++)
                    {
                        Code = "0" + Code;
                    }
                }
                return "SP" + Code;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        ///  Get list product type
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static object GetProductType()
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (var db = new Solution_30shineEntities())
                {
                    var list = (from c in db.Tbl_Config.AsNoTracking()
                                where c.Key == "product_inventory_type" && c.IsDelete == 0
                                select c
                                ).OrderBy(a => a.Value)
                                .ToList();
                    return serializer.Serialize(list);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}