﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Product_Edit.aspx.cs" Inherits="_30shine.GUI.BackEnd.UIProduct.Product_Edit" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="content1" runat="server" ContentPlaceHolderID="head">
    <script>
        var itemProduct;
        var index;
        var ms;
        jQuery(document).ready(function () {             
            ms = $('#ms').magicSuggest({
                maxSelection: 1,
                data:<%=ListProduct %>,
                valueField: 'Id',
                displayField: 'Name'    
            });          
            $(ms).on('selectionchange', function(e,m){
                itemProduct=null;
                var listProduct= this.getSelection();
                console.log(listProduct);
                var listData= ms.getData();
                if(listProduct.length>0)
                {
                    for(var i=0; i<listData.length;i++){
                        if(listProduct[0].Id==listData[i].Id){
                            itemProduct=listProduct[0];
                            break;
                        }
                        else{
                            continue;
                        }
                    }
                }
            });
        });

        function ClickAddCombo(This){
            index=$("#table-item-product tbody tr").length+1;
            if(itemProduct!=null ){
                if(itemProduct.Code!=undefined){                 
                    var Quantity=1;
                    var tr='<tr>'+                          
                           '<td class="td-product-index">'+index+'</td>'+
                           '<td class="td-product-name">' + itemProduct.Name + '</td>'+
                           '<td class="td-product-code" data-id="' + itemProduct.Id + '" data-code="' + itemProduct.Code + '">' + itemProduct.Code + '</td>'+
                           '<td class="td-product-price" data-cost="' + itemProduct.Cost + '">' + itemProduct.Cost + '</td>'+
                            '<td class="td-product-quantity"><input type="text" class="product-quantity" value="' + Quantity + '"></td>'+
                            '<td class="map-edit"><div class="edit-wp"><a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),\'' + itemProduct.Name + '\',\'product\')" href="javascript://" title="Xóa"></a></div></td>'+
                    '</tr>';
                    $("#table-item-product tbody").append($(tr));
                    $("#ListingProductWp").show();
                    TotalMoney();
                    UpdateItemOrder($("#table-item-product").find("tbody"));
                    getProductIds();
                    ResetDropdownProduct();
                }
            }
        }
        
        //TotalMoney
        function TotalMoney() {
            var Money = 0;
            $("table.table-item-product tbody tr").each(function () {
                var money = $(this).find("td.td-product-price").attr("data-cost");
                Money += parseFloat(money);
            });
            console.log(Money);
            $("#TotalMoney").val(FormatPrice(Money));
            $("#HDF_TotalMoney").val(Money);
        }
        // ResetDropdownProduct
        function ResetDropdownProduct() {
            ms.clear();
            $("#inputQuantity").val("");
            $(ms).focus();
            $(".ms-sel-ctn input[type='text']").focus();
        }
        //Update product
        function UpdateItemDisplay(itemName) {
            var dom = $(".table-item-" + itemName);
            var len = dom.find("tbody tr").length;
            if (len == 0) {
                dom.parent().hide();
            } else {
                UpdateItemOrder(dom.find("tbody"));
            }
        }
        // Update STT
        function UpdateItemOrder(dom) {
            var index = 1;
            dom.find("tr").each(function () {
                $(this).find("td.td-product-index").text(index);
                index++;
            });
        }
        //RemoveItem
        function RemoveItem(THIS, name, itemName) {
            var Code = THIS.find(".td-product-code").attr("data-code");
            $("#quick-" + itemName).find("input[data-code='" + Code + "']").removeAttr("checked");
            THIS.remove();
            UpdateItemDisplay(itemName);
            TotalMoney();
            getProductIds();
            getQuantity();
        }
        //GetIDProduct
        function getProductIds() {
            var Ids = '';
            $("table.table-item-product tbody tr").each(function () {
                var Id = $(this).find("td.td-product-code").attr("data-id"),
                Id = Id.toString().trim() != "" ? parseInt(Id) : 0;
                if (Ids != '')
                    Ids += ',';
                Ids += Id;
            });
            $("#HDF_ProductIds").val(Ids);
        }
        function getQuantity() {
            var Quantity = '';
            $("table.table-item-product tbody tr").each(function () {
                var quantity = $(this).find("td.td-product-quantity input:text").attr("value"),
                    quantity = parseInt(quantity);
                if (Quantity != '')
                    Quantity += ',';
                Quantity += quantity;
            });
            $("#HDF_Quantity").val(Quantity);
        }
        function ValidateKeypress(numcheck, e) {
            var keynum, keychar, numcheck;
            if (window.event) {
                keynum = e.keyCode;
            }
            else if (e.which) {
                keynum = e.which;
            }
            if (keynum == 8 || keynum == 127 || keynum == null || keynum == 9 || keynum == 0 || keynum == 13) return true;
            keychar = String.fromCharCode(keynum);
            var result = numcheck.test(keychar);
            return result;
        }
    </script>
    <link href="/Assets/css/style.css" rel="stylesheet" />
    <link href="/Assets/css/bootstrap/bootstrap-toggle.min.css" rel="stylesheet" />
    <script src="/Assets/js/bootstrap/bootstrap-toggle.min.js"></script>
    <script src="/assets/js/jquery.mCustomScrollbar.js"></script>
    <link href="/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
    <link href="/Assets/css/bootstrap/bootstrap-toggle.min.css" rel="stylesheet" />
    <script src="/Assets/js/bootstrap/bootstrap-toggle.min.js"></script>
    <script src="/Assets/libs/magicsuggest-master/magicsuggest.js"></script>
    <link href="/Assets/libs/magicsuggest-master/magicsuggest.css" rel="stylesheet" />
    <style type="text/css">
        .ms-sel-ctn input[type='text'] {
            height: auto !important;
            line-height: 24px !important;
            border: none !important;
        }

        .ms-ctn .ms-sel-item {
            line-height: 22px;
            color: #000000;
            font-size: 13px;
        }

        .ms-helper {
            display: none !important;
        }

        .input-quantity-wrap {
            float: left;
            margin-left: 10px;
        }

        #pem {
            width: 100%;
        }

            #pem span {
                width: 100%;
                line-height: 22px;
                height: 22px;
            }

                #pem span div.btn-group .multiselect, #pem span div.btn-group, .multiselect-container {
                    width: 100% !important;
                }

        #InventoryType {
            width: 100% !important;
        }
    </style>
</asp:Content>
<asp:Content ID="ProductAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý Sản phẩm &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/admin/san-pham/danh-sach.html">Danh sách</a></li>
                        <li class="li-add"><a href="/admin/san-pham/them-moi.html">Thêm mới</a></li>
                        <li class="li-edit"><a href="javascript://">Chỉnh sửa</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add admin-product-add fe-service">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="table-wp">
                    <table class="table-add admin-product-table-add fe-service-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Thông tin sản phẩm</strong></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Loại sản phẩm</span></td> 
                                <td class="col-xs-9 right">
                                    <span class="field-wp" style="width: 490px; margin-right: 10px" id="pem">
                                        <asp:ListBox SelectionMode="Multiple" ID="InventoryType" runat="server" ClientIDMode="Static" CssClass=" perm-ddl-field"></asp:ListBox> 
                                    </span>
                                    <span class="field-wp" style="width: 100px; margin-left: 10px">
                                        <asp:TextBox ID="Code" runat="server" ClientIDMode="Static" placeholder="Ví dụ : SP01"></asp:TextBox>
                                    </span>
                                    <span style="width: 50px; padding: 0 10px;">Combo</span>
                                    <span class="field-wp" style="width: 100px;">
                                        <%if (check == "True")
                                            {%>
                                        <input type="checkbox" id="chkCombo" checked="checked" style="margin-left: 10px; margin-top: 12px" />
                                        <%} %>
                                        <%else
                                            {%><input type="checkbox" id="chkCombo1" style="margin-left: 10px; margin-top: 12px" />
                                        <%} %>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Tên danh mục/Tên nhóm sản phẩm</span></td> 
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <asp:DropDownList onchange="displayMember($(this));" ID="ddlCategory" runat="server"></asp:DropDownList>
                                        </div>
                                        <div class="div_col">
                                            <asp:DropDownList ID="ddlGroupProduct" runat="server" Style="float: right"></asp:DropDownList>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">

                                <td class="col-xs-3 left"><span>Tên sản phẩm/Barcode</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <asp:TextBox ID="Name" runat="server" ClientIDMode="Static" placeholder="Nhập tên sản phẩm"></asp:TextBox>
                                        </div>
                                        <div class="div_col">
                                            <asp:TextBox ID="Barcode" runat="server" ClientIDMode="Static" Style="float: right" placeholder="Nhập barcode"></asp:TextBox>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Thương hiệu/Map 30shine Store</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <asp:DropDownList runat="server" ID="Brand" placeholder="Thương hiệu"></asp:DropDownList>
                                        </div>
                                        <div class="div_col">
                                            <asp:TextBox ID="OnWebId" runat="server" ClientIDMode="Static" placeholder="30shinestore Id" Style="float: right"></asp:TextBox>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Tên model/Thể tích</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <asp:TextBox ID="ModelName" runat="server" ClientIDMode="Static" placeholder="Tên model"></asp:TextBox>
                                        </div>
                                        <div class="div_col">
                                            <asp:TextBox ID="Volume" runat="server" ClientIDMode="Static" placeholder="Thể tích" Style="float: right"></asp:TextBox>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr class="tr-description">
                                <td class="col-xs-2 left"><span>Mô tả</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox TextMode="MultiLine" Rows="5" ID="Description" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>

                            <%-- Thêm mới Combo --%>
                            <tr class="tr-field-ahalf tr-product" id="ListingProduct1">
                                <td class="col-xs-2 left"><span>Sản phẩm</span></td>
                                <td class="col-xs-10 right">
                                    <div class="wp_add_product">
                                        <div class="wp_ddproduct" style="width: 100%">
                                            <div class="suggestion-wrap" style="width: 420px; float: left; margin-bottom: 10px">
                                                <input id="ms" class="form-control" type="text" style="width: 346px;" />
                                            </div>
                                        </div>
                                        <div class="show-product addproduct" id="AddProduct" data-id="0" style="" onclick="ClickAddCombo($(this))">
                                            <i class="fa fa-plus-circle" style="margin: 10px"></i>Thêm sản phẩm
                                        </div>
                                    </div>
                                    <div id="ListingProductWp" runat="server" clientidmode="Static" style="display: block" class="listing-product item-product">

                                        <table class="table table-listing-product table-item-product" id="table-item-product">
                                            <thead>
                                                <tr>
                                                    <th>STT</th>
                                                    <th>Tên sản phẩm</th>
                                                    <th>Mã sản phẩm</th>
                                                    <th>Đơn giá nhập</th>
                                                    <th>Số lượng</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbodyProduct">
                                                <asp:Repeater ID="Rpt_Product_Bill" runat="server">
                                                    <ItemTemplate>
                                                        <tr class="trCategory" data-cate="<%#Eval("CategoryId") %>">
                                                            <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                                            <td class="productID" style="display: none;"><%#Eval("Id") %></td>
                                                            <td class="td-product-name"><%#Eval("Name") %></td>
                                                            <td class="td-product-code" data-id="<%#Eval("Id") %>" data-code="<%#Eval("Code") %>"><%#Eval("Code") %></td>
                                                            <td class="td-product-price" id="box-money" data-cost="<%#Eval("Cost") %>"><%#Eval("Cost") %></td>

                                                            <td class="td-product-quantity">
                                                                <input type="text" class="product-quantity" value="1">
                                                            </td>
                                                            <td class="map-edit">
                                                                <div class="edit-wp">
                                                                    <a class="elm del-btn" onclick="RemoveItem($(this).parent().parent().parent(),' <%#Eval("Name") %>','product')" href="javascript://" title="Xóa"></a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr class="tr-description tr-field-ahalf" id="ListingProduct2">
                                <td class="col-xs-3 left"><span>Tổng số tiền giá nhập</span></td>
                                <td class="col-xs-10 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="TotalMoney" runat="server" ClientIDMode="Static" Text="0" ReadOnly="true"></asp:TextBox>
                                        <span class="unit-money">VNĐ</span>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf" id="gianhap">
                                <td class="col-xs-2 left"><span>Giá nhập</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="Cost" runat="server" ClientIDMode="Static" placeholder="Ví dụ : 1000000" onkeypress="return ValidateKeypress(/\d/,event);"></asp:TextBox>&nbsp;&nbsp;VNĐ
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-field-ahalf" id="gianhap1">
                                <td class="col-xs-2 left"><span>Giá bán</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="Price" runat="server" ClientIDMode="Static" placeholder="Ví dụ : 1000000" onkeypress="return ValidateKeypress(/\d/,event);"></asp:TextBox>&nbsp;&nbsp;VNĐ
                                <asp:RequiredFieldValidator ID="ValidatePrice" ControlToValidate="Price" CssClass="fb-cover-error" Text="Bạn chưa nhập giá!" runat="server"></asp:RequiredFieldValidator>
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-field-ahalf">
                                <td class="col-xs-2 left"><span>Khuyến mại giảm giá</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="VoucherPercent" runat="server" ClientIDMode="Static" placeholder="Ví dụ : 5" onkeypress="return ValidateKeypress(/\d/,event);"></asp:TextBox>&nbsp;&nbsp;( % )
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-field-ahalf">
                                <td class="col-xs-2 left"><span>Chiết khấu lương</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="ForSalary" runat="server" ClientIDMode="Static" placeholder="Ví dụ : 5" onkeypress="return ValidateKeypress(/\d/,event);"></asp:TextBox>&nbsp;&nbsp;( % )
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-product-category" style="display: none;">
                                <td class="col-xs-2 left"><span>Danh mục sản phẩm</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:DropDownList ID="ProductCategory" runat="server"></asp:DropDownList>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf" id="FormOrder">
                                <td class="col-xs-2 left"><span>Order</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="Order" onkeypress="return ValidateKeypress(/\d/,event);" runat="server" ClientIDMode="Static" placeholder="Ví dụ : 1"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                             <tr class="tr-field-ahalf" id="CoefficientOfWaitingDays">
                                <td class="col-xs-2 left"><span>Hệ số ngày chờ hàng</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="CoefficientOfWaitingDaysTxt" onkeypress="return ValidateKeypress(/\d/,event);" runat="server" ClientIDMode="Static" placeholder="Ví dụ : 1"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-field-ahalf membership" <%=CategoryMember%>>
                                <td class="col-xs-2 left"><span>Loại thẻ Membership</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:DropDownList ID="ddlMembershipType" runat="server">
                                            <asp:ListItem Value="0">Chọn loại thẻ</asp:ListItem>
                                            <asp:ListItem Value="1">Thẻ tháng</asp:ListItem>
                                            <asp:ListItem Value="2">Thẻ ngày</asp:ListItem>
                                        </asp:DropDownList>
                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldMember" ControlToValidate="ddlMembershipType" runat="server" CssClass="fb-cover-error" InitialValue="0" Text="Bạn chưa chọn loại thẻ member!"></asp:RequiredFieldValidator>--%>
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-field-ahalf membership" <%=CategoryMember%>>
                                <td class="col-xs-2 left"><span>Thời gian sử dụng Membership</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="TimeUsedMember" runat="server" ClientIDMode="Static" placeholder="Ví dụ : 5" onkeypress="return ValidateKeypress(/\d/,event);"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldTimeUsed" ControlToValidate="TimeUsedMember" CssClass="fb-cover-error" Text="Bạn chưa nhập thời hạn dùng của thẻ" runat="server"></asp:RequiredFieldValidator>--%>
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-product-category">
                                <td class="col-xs-2 left"><span>Nổi bật</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:CheckBox ID="Focus" Checked="true" runat="server" />
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-product-category">
                                <td class="col-xs-2 left"><span>Publish</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:CheckBox ID="Publish" Checked="true" runat="server" />
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-product-category">
                                <td class="col-xs-2 left"><span>Sản phẩm tồn kho (Hóa chất)</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:CheckBox ID="chkInventoryHC" runat="server" />
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-product-category">
                                <td class="col-xs-2 left"><span>Theo dõi mức dùng vật tư</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:CheckBox ID="chkVattu" runat="server" />
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-product-category">
                                <td class="col-xs-2 left"><span>Sản phẩm ngừng kinh doanh</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:CheckBox ID="ChkStopBusiness" Checked="false" runat="server" />
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-send">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <asp:Panel ID="BtnSend" CssClass="btn-send" runat="server" ClientIDMode="Static">Cập nhật</asp:Panel>
                                        <asp:Button ID="Send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="UpdateProduct" Style="display: none;"></asp:Button>
                                        <span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px; display: none;">Hủy</span>
                                    </span>
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
                        </tbody>
                    </table>
                </div>
            </div>
            <%-- end Add --%>
        </div>
        <asp:HiddenField ID="HDF_InventoryType" runat="server" ClientIDMode="Static" /> 
        <asp:HiddenField ID="HDF_TotalMoney" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="HDF_ProductIds" runat="server" ClientIDMode="Static" />
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <script>
            jQuery(document).ready(function () {
                $("#ListingProduct1").hide();
                $("#ListingProduct2").hide();
                $("#glbAdminContent").addClass("active");
                $("#glbAdminProduct").addClass("active");
                $("#subMenu .li-edit").addClass("active");
       
                 var qs = getQueryStrings();
                showMsgSystem(qs["msg_update_message"], qs["msg_update_status"])
                // list select
                $("#InventoryType").multiselect({ 
                    includeSelectAllOption: true
                });
            });
            // get list id inventoryType
            function getInventoryType() { 
                var Ids = [];
                var prd = {};
                $("#InventoryType option:selected").each(function () {
                    prd = {};
                    var inventoryType = $(this).val();
                    if (inventoryType !== null && inventoryType > 0) {
                        inventoryType = inventoryType.toString().trim() !== "" ? parseInt(inventoryType) : 0;
                        prd = inventoryType;
                        Ids.push(prd);
                    }
                });
                $("#HDF_InventoryType").val(Ids);
            }
            // btn Send
            $("#BtnSend").bind("click", function () {
                getInventoryType();
                var kt = true;
                var error = '';
                var name = $('#Name').val();
                var categoryId = $('#CtMain_ddlCategory').val();
                var price = $('#Price').val();
                var ddlMembershipType= $("#CtMain_ddlMembershipType").val();
                var timeUsedMember = $("#TimeUsedMember").val();
                var CoefficientOfWaitingDays = $("#CoefficientOfWaitingDaysTxt").val(); 
                 if (categoryId === "" || categoryId === "0") {
                    kt = false;
                    error = "Bạn phải chọn nhóm sản phẩm";
                    $('#CtMain_ddlCategory').css("border-color", "red");
                }
                else if (name === "") { 
                    kt = false;
                    error = "Bạn phải nhập tên sản phẩm!";
                    $('#Name').css("border-color", "red");
                }
                else if (price === "") { 
                    kt = false;
                    error = "Bạn phải nhập giá sản phẩm!";
                    $('#Price').css("border-color", "red");
                }
                else if (categoryId === "98" && (ddlMembershipType === "" || ddlMembershipType === "0")) { 
                    kt = false;
                    error = "Bạn phải chọn loại thẻ!";
                    $('#CtMain_ddlMembershipType').css("border-color", "red");
                }
                else if (categoryId === "98" && timeUsedMember === "") { 
                    kt = false;
                    error = "Bạn phải chọn thời hạn sử dụng thẻ!";
                    $('#TimeUsedMember').css("border-color", "red");
                }else if (CoefficientOfWaitingDays === "") {
                    kt = false;
                    error = "Bạn phải chọn Hệ số ngày chờ hàng!";
                    $('#CoefficientOfWaitingDaysTxt').css("border-color", "red");
                }
                if (!kt) {
                    ShowMessage('', error, 3);
                }
                else {
                    //call buton addProdduct
                    $("#Send").click();
                }
            });
        </script>
        <script>
            $(document).on('ready', function () {
                $(function () {
                    if ($("#chkCombo").is(":checked")) {
                        $("#gianhap").hide();
                        $("#ListingProduct1").show();
                        $("#ListingProduct2").show();
                    } else {
                        removeLoading();
                        $("#gianhap").show();
                        $("#ListingProduct1").hide();
                        $("#ListingProduct2").hide();
                    }
                    // });
                });
            });
            
            function displayMember(This) {
                let data = This.val();
                if (data != undefined && data != null && data != '' && data == 98) {
                    $('.membership').show();
                }
                else {
                    $('.membership').hide();
                }
            }
        </script>
    </asp:Panel>
</asp:Content>


