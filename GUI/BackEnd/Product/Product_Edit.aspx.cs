﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using System.Text.RegularExpressions;

namespace _30shine.GUI.BackEnd.UIProduct
{
    public partial class Product_Edit : System.Web.UI.Page
    {
        protected Product OBJ;
        public string check = "";
        private bool Perm_Access = false;
        protected string ListProduct;
        protected List<Product> _rpt = new List<Product>();
        public string CategoryMember = "style='display:none'";
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                pageId = permissionModel.GetRegexPageIdSpecial().Replace(pageId, ".html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void ExecuteByPermission()
        {

            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        /// <summary>
        /// Page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                Bind_Product();
                bindBrands();
                BindInvetoryType();
                BindGroupProduct();
                if (Bind_Product())
                {
                    Bind_ProductCategory();
                }

            }
            ListProduct = getProducts();
        }

        /// <summary>
        /// Get product
        /// </summary>
        /// <returns></returns>
        private string getProducts()
        {
            using (var db = new Solution_30shineEntities())
            {
                var data = db.Products.Where(w => w.IsDelete != 1 && w.Publish == 1 && w.CategoryId != 95).OrderByDescending(o => o.Id).ToList();
                var serialize = new JavaScriptSerializer();
                return serialize.Serialize(data);
            }
        }

        /// <summary>
        /// Bind group product 
        /// </summary>
        private void BindGroupProduct()
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = db.Tbl_GroupProduct.ToList();
                ddlGroupProduct.Items.Add(new ListItem("Chọn tên nhóm sản phẩm", "0"));
                if (list.Count > 0)
                {
                    for (int i = 0; i < list.Count; i++)
                    {
                        ddlGroupProduct.Items.Add(new ListItem(list[i].Name, list[i].Id.ToString()));
                    }
                }
                ddlGroupProduct.DataBind();
            }
        }

        /// <summary>
        /// Bind Product
        /// </summary>
        /// <returns></returns>
        private bool Bind_Product()
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                var ExistOBJ = true;
                var _Code = int.TryParse(Request.QueryString["Code"], out integer) ? integer : 0;
                var OBJ = db.Database.SqlQuery<MODEL.ENTITY.EDMX.Product>(" select * from Product where Id=" + _Code + "").FirstOrDefault();
                if (!OBJ.Equals(null))
                {
                    HDF_ProductIds.Value = OBJ.MapIdProduct;
                    check = OBJ.CheckCombo.ToString();
                    ddlCategory.Text = OBJ.CategoryId.ToString();
                    Name.Text = OBJ.Name;
                    Code.Text = OBJ.Code;
                    Barcode.Text = OBJ.BarCode;
                    Description.Text = OBJ.Description;
                    VoucherPercent.Text = OBJ.VoucherPercent.ToString();
                    ForSalary.Text = OBJ.ForSalary.ToString();
                    Order.Text = OBJ.Order.ToString();
                    CoefficientOfWaitingDaysTxt.Text = OBJ.CoefficientOfWaitingDays.ToString();
                    if (OBJ.CategoryId == 98)
                    {
                        CategoryMember = "";
                        TimeUsedMember.Text = OBJ.MemberAddDay.ToString();
                        ddlMembershipType.Text = OBJ.MemberTypeAdd.ToString();
                    }
                    //---------
                    OnWebId.Text = OBJ.OnWebId.ToString();
                    Volume.Text = OBJ.Volume;
                    ModelName.Text = OBJ.ModelName;
                    var itemBrand = Brand.Items.FindByValue(OBJ.BrandId.ToString());
                    var itemGroupProduct = ddlGroupProduct.Items.FindByValue(OBJ.GroupProductId.ToString());
                    if (itemBrand != null)
                    {
                        itemBrand.Selected = true;
                    }
                    if (itemGroupProduct != null)
                    {
                        itemGroupProduct.Selected = true;
                    }
                    if (OBJ.Publish == 1)
                    {
                        Publish.Checked = true;
                    }
                    else
                    {
                        Publish.Checked = false;
                    }

                    if (OBJ.Status == 1)
                    {
                        Focus.Checked = true;
                    }
                    else
                    {
                        Focus.Checked = false;
                    }
                    if (OBJ.CheckInventoryHC == true)
                    {
                        chkInventoryHC.Checked = true;
                    }
                    else
                    {
                        chkInventoryHC.Checked = false;
                    }
                    if (OBJ.IsCheckVatTu == true)
                    {
                        chkVattu.Checked = true;
                    }
                    else
                    {
                        chkVattu.Checked = false;
                    }
                    //
                    if (OBJ.StopBusiness == 1)
                    {
                        ChkStopBusiness.Checked = true;
                    }
                    else
                    {
                        ChkStopBusiness.Checked = false;
                    }
                    //
                    if (OBJ.CheckCombo == true && OBJ.MapIdProduct != null)
                    {
                        try
                        {
                            string mapId = OBJ.MapIdProduct;
                            _rpt = db.Database.SqlQuery<MODEL.ENTITY.EDMX.Product>("select * from Product where Id in (" + mapId + ") and IsDelete=0").ToList();
                            Price.Text = OBJ.Price.ToString();
                            TotalMoney.Text = OBJ.Cost.ToString();
                            Rpt_Product_Bill.DataSource = _rpt;
                            Rpt_Product_Bill.DataBind();
                        }
                        catch (Exception ex) { throw new Exception(ex.Message); }
                    }
                    else
                    {
                        Cost.Text = OBJ.Cost.ToString();
                        Price.Text = OBJ.Price.ToString();
                    }
                }
                else
                {
                    ExistOBJ = false;
                    MsgSystem.Text = "Lỗi! Sản phẩm không tồn tại";
                    MsgSystem.CssClass = "msg-system warning";
                }
                return ExistOBJ;
            }

        }

        /// <summary>
        /// Update thêm trường ChkCombo, MapIdPro, Pricombo
        /// </summary>
        protected void UpdateProduct(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                float floatVar;
                var _Code = int.TryParse(Request.QueryString["Code"], out integer) ? integer : 0;
                var obj = db.Products.Where(w => w.Id == _Code).First();
                if (!obj.Equals(null))
                {
                    string mapIdPro = HDF_ProductIds.Value;
                    check = obj.CheckCombo.ToString();
                    obj.Name = Name.Text;
                    obj.Code = Code.Text;
                    obj.Description = Description.Text;
                    obj.Price = int.TryParse(Price.Text, out integer) ? integer : 0;
                    obj.VoucherPercent = int.TryParse(VoucherPercent.Text, out integer) ? integer : 0;
                    obj.ForSalary = float.TryParse(ForSalary.Text, out floatVar) ? floatVar : 0;
                    obj.CategoryId = int.TryParse(ddlCategory.SelectedValue, out integer) ? integer : 0;
                    obj.BarCode = Barcode.Text.Replace(" ", string.Empty);
                    obj.MemberAddDay = int.TryParse(TimeUsedMember.Text.Trim(), out integer) ? integer : 0;
                    obj.MemberTypeAdd = int.TryParse(ddlMembershipType.SelectedValue, out integer) ? integer : 0;
                    obj.CoefficientOfWaitingDays = int.TryParse(CoefficientOfWaitingDaysTxt.Text.Trim(), out integer) ? integer : 1;
                    if (Order.Text == "")
                    {
                        obj.Order = 0;
                    }
                    else
                    {
                        obj.Order = Convert.ToInt32(Order.Text);
                    }
                    //---------
                    obj.BrandId = int.TryParse(Brand.SelectedValue, out integer) ? integer : 0;
                    obj.OnWebId = int.TryParse(OnWebId.Text, out integer) ? integer : 0;
                    if (HDF_InventoryType.Value != "[]" && !string.IsNullOrEmpty(HDF_InventoryType.Value)) 
                    {
                        obj.InventoryType = HDF_InventoryType.Value;
                    }
                    //add tên theo nhóm sản phẩm
                    if (ddlGroupProduct.SelectedValue.ToString() == "0")
                    {
                        string name = Name.Text;
                        if (name.StartsWith("MN_LK_"))
                        {
                            var matches = (from m in db.Tbl_GroupProduct
                                           where m.Id == 1
                                           select m).FirstOrDefault();
                            obj.GroupProductId = matches.Id;
                        }
                        else
                        {
                            var _obj = new Tbl_GroupProduct();
                            _obj.Name = Name.Text;
                            _obj.CreatedTime = DateTime.Now;
                            db.Tbl_GroupProduct.Add(_obj);
                            db.SaveChanges();
                            var _list = db.Tbl_GroupProduct.OrderByDescending(o => o.Id).FirstOrDefault();
                            obj.GroupProductId = _list.Id;
                        }
                    }
                    else
                    {
                        obj.GroupProductId = Convert.ToInt32(ddlGroupProduct.SelectedValue);
                    }
                    obj.Volume = Volume.Text;
                    obj.ModelName = ModelName.Text;
                    //---------
                    if (Publish.Checked == true)
                    {
                        obj.Publish = 1;
                    }
                    else
                    {
                        obj.Publish = 0;
                    }

                    if (Focus.Checked == true)
                    {
                        obj.Status = 1;
                    }
                    else
                    {
                        obj.Status = 0;
                    }
                    if (chkInventoryHC.Checked == true)
                    {
                        obj.CheckInventoryHC = true;
                    }
                    else
                    {
                        obj.CheckInventoryHC = false;
                    }
                    if (chkVattu.Checked == true)
                    {
                        obj.IsCheckVatTu = true;
                    }
                    else
                    {
                        obj.IsCheckVatTu = false;
                    }
                    obj.ModifiedDate = DateTime.Now;
                    if (check == "True")
                    {
                        obj.Cost = Convert.ToInt32(TotalMoney.Text);
                        obj.MapIdProduct = mapIdPro;
                        obj.CheckCombo = true;
                        obj.Price = int.TryParse(Price.Text, out integer) ? integer : 0;
                    }
                    else
                    {
                        obj.Cost = int.TryParse(Cost.Text, out integer) ? integer : 0;
                    }
                    if (ChkStopBusiness.Checked == true)
                    {
                        obj.StopBusiness = 1;
                    }
                    else
                    {
                        obj.StopBusiness = 0;
                    }
                    // Validate
                    var Error = false;
                    var exc = 0;
                    //check trùng barcode
                    var checkBarCode = db.Products.Where(w => w.BarCode == obj.BarCode && w.Id != _Code && w.IsDelete == 0).ToList();
                    if (checkBarCode.Count > 0)
                    {
                        MsgSystem.Text = "Mã barcode đã tồn tại. Bạn vui lòng nhập mã barcode khác.";
                        MsgSystem.CssClass = "msg-system warning";
                        Error = true;
                    }
                    if (!Error)
                    {
                        db.Products.AddOrUpdate(obj);
                        exc = db.SaveChanges();
                    }
                    if (exc > 0)
                    {
                        // Thông báo thành công
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                        UIHelpers.Redirect("/admin/san-pham/" + obj.Id + ".html", MsgParam);
                    }
                    //else
                    //{
                    //    MsgSystem.Text = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                    //    MsgSystem.CssClass = "msg-system warning";
                    //}
                }
                else
                {
                    MsgSystem.Text = "Lỗi! Không tìm thấy sản phẩm.";
                    MsgSystem.CssClass = "msg-system warning";
                }

                Context.RewritePath("+", "", "");
            }
        }

        /// <summary>
        /// Bind product category
        /// </summary>
        private void Bind_ProductCategory()
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                var _Code = int.TryParse(Request.QueryString["Code"], out integer) ? integer : 0;
                var Edit = db.Products.First(m => m.Id == _Code);
                var list = new List<Tbl_Category>();
                var node = new List<Tbl_Category>();
                var cateRoot = db.Tbl_Category.Where(w => w.IsDelete != 1 && w.Pid == 1 && w.Publish == true).ToList();
                if (cateRoot.Count > 0)
                {
                    foreach (var v in cateRoot)
                    {

                        node = db.Tbl_Category.Where(w => w.IsDelete != 1 && w.Pid == v.Id && w.Publish == true).ToList();
                        list.Add(v);
                        if (node.Count > 0)
                        {
                            foreach (var v2 in node)
                            {
                                v2.Name = "----- " + v2.Name;
                                list.Add(v2);
                            }
                        }
                    }
                }
                ddlCategory.DataTextField = "Name";
                ddlCategory.DataValueField = "Id";
                ListItem item = new ListItem("Chọn danh mục", "0");
                ddlCategory.DataSource = list;
                ddlCategory.DataBind();
                ddlCategory.Items.Insert(0, item);
                if (OBJ != null && OBJ.CategoryId != null)
                {
                    var itemSelected = ddlCategory.Items.FindByValue(OBJ.CategoryId.ToString());
                    if (itemSelected != null)
                    {
                        itemSelected.Selected = true;
                    }
                }
            }
        }

        /// <summary>
        /// Bind brands
        /// </summary>
        private void bindBrands()
        {
            using (var db = new Solution_30shineEntities())
            {
                Brand.DataTextField = "TenThuongHieu";
                Brand.DataValueField = "Id";
                var lst = db.Brands.Where(w => w.Status == true).ToList();
                Brand.DataSource = lst;
                Brand.DataBind();
                var item = new ListItem("Chọn thương hiệu", "0");
                Brand.Items.Insert(0, item);
            }
        }

        /// <summary>
        /// Reload
        /// </summary>
        public void ReloadPage()
        {
            HttpRequest Request = HttpContext.Current.Request;
            string host = "http://" + Request.Url.Host.ToString();
            string rawUrl = Request.RawUrl.ToString();
            string Url = host + rawUrl;
            Response.Redirect(Url);
        }

        /// <summary>
        /// BindInvetoryType
        /// </summary>
        private void BindInvetoryType() 
        {
            var code = Convert.ToInt32(Request.QueryString["Code"]);
            var key = 0;
            using (var db = new Solution_30shineEntities())
            {
                var list = (from c in db.Tbl_Config.AsNoTracking()
                            where c.Key == "product_inventory_type" && c.IsDelete == 0
                            select c
                              ).OrderBy(a => a.Value)
                              .ToList();
                foreach (var v in list)
                {
                    var item = new ListItem(v.Label, v.Value.ToString());
                    InventoryType.Items.Insert(key++, item);
                }
                if (code > 0)
                {
                    var obj = db.Products.FirstOrDefault(w => w.Id == code);
                    if (!string.IsNullOrEmpty(obj.InventoryType))
                    {
                        var listInventory = obj.InventoryType.Split(',').Select(a => int.TryParse(a.Trim(), out var integer) ? integer : 0).ToList();
                        foreach (var v in listInventory)
                        {
                            var ItemSelected = InventoryType.Items.FindByValue(v.ToString());
                            if (ItemSelected != null)
                            {
                                ItemSelected.Selected = true;
                            }
                        }
                    }
                }
            }
        }
    }
}