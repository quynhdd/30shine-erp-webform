﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="GroupProduct_Add.aspx.cs" Inherits="_30shine.GUI.BackEnd.UIProduct.GroupProduct_Add" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ProductAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý nhóm sản phẩm &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/admin/groupproduct/danh-sach.html">Danh sách</a></li>
                        <li class="li-add active"><a href="/admin/groupproduct/them-moi-nhom-san-pham.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add admin-product-add">
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <table class="table-add admin-product-table-add">
                    <tbody>
                        <tr class="title-head">
                            <td><strong>Thông tin nhóm sản phẩm</strong></td>
                            <td></td>
                        </tr>
                        <tr class="tr-margin" style="height: 20px;"></tr>
                        <tr>
                            <td class="col-xs-3 left"><span>Tên nhóm sản phẩm</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:TextBox ID="Name" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="NameValidate" ControlToValidate="Name" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập tên nhóm sản phẩm!"></asp:RequiredFieldValidator>
                                </span>

                            </td>
                        </tr>
                        <tr class="tr-send">
                            <td class="col-xs-3 left"></td>
                            <td class="col-xs-9 right no-border">
                                <span class="field-wp">
                                    <asp:Button ID="Send" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="ExcAddOrUpdate"></asp:Button>
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
