﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMasterVersion1.Master" AutoEventWireup="true" CodeBehind="UpdateUsersProfile.aspx.cs" Inherits="_30shine.GUI.BackEnd.User.UpdateUsersProfile" %>

<asp:Content ID="ContentMain" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel ID="ContentWrap" runat="server" ClientIDMode="Static">
        <style>
            th, td {
                text-align: center;
            }

            body, html {
                height: 91%;
            }

            .nav-pills .nav-link.active, .nav-pills .show > .nav-link {
                color: #fff;
                background-color: black;
            }

            .shadow-sm {
                margin-bottom: 15px;
                width: 70%;
                margin: auto;
            }

            .form-box-left {
                height: 31px;
                width: 147px;
            }

            .ul-box-left .li-box-left {
                margin-bottom: 8px;
                font-family: Roboto Condensed Regular !important;
                font-size: 14px !important;
            }

            .btn-group {
                margin-top: 21px !important;
                float: right;
            }

            .fa-times {
                font-size: 17px;
                color: red;
            }

                .fa-times:hover {
                    cursor: pointer;
                }

            .btn-outline-secondary {
                color: #000000;
                border-color: #000000;
            }

                .btn-outline-secondary:hover {
                    background: #000000;
                }

            .table td, .table th {
                vertical-align: middle;
            }

            .quantity-left {
                text-align: center;
            }

            .thead-dark {
                font-family: Roboto Condensed Bold;
            }

            .select-list-item {
                width: 100%;
                font-size: 14px;
                height: calc(2.25rem + 2px);
                line-height: 1.5;
                color: #495057;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #ced4da;
                border-radius: .25rem;
                transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
            }

                .select-list-item option {
                    font-size: 14px;
                }

            textarea.broken-note {
                height: 130px;
                width: 100%;
                direction: ltr;
                display: block;
                max-width: 100%;
                line-height: 1.5;
                padding: 15px 15px 30px;
                border-radius: 3px;
                border: 1px solid #F7E98D;
                font: 13px Tahoma, cursive;
                transition: box-shadow 0.5s ease;
                box-shadow: 0 4px 6px rgba(0,0,0,0.1);
                font-smoothing: subpixel-antialiased;
                background: linear-gradient(#F9EFAF, #F7E98D);
                background: -o-linear-gradient(#F9EFAF, #F7E98D);
                background: -ms-linear-gradient(#F9EFAF, #F7E98D);
                background: -moz-linear-gradient(#F9EFAF, #F7E98D);
                background: -webkit-linear-gradient(#F9EFAF, #F7E98D);
                resize: none;
            }

            .input-group {
                width: 70% !important;
                margin: 24px;
            }

            .eb-popup-wp {
                width: 100%;
                height: 100%;
                float: left;
                position: fixed;
                z-index: 100000;
                background: rgba(0,0,0,0.6);
                display: none;
            }

                .eb-popup-wp .eb-popup {
                    width: 100%;
                    float: left;
                    min-height: 20px;
                    background: white;
                    -webkit-box-shadow: 0px 0px 12px 3px rgba(0,0,0,0.7);
                    -moz-box-shadow: 0px 0px 12px 3px rgba(0,0,0,0.7);
                    box-shadow: 0px 0px 12px 3px rgba(0,0,0,0.7);
                }

                .eb-popup-wp .eb-popup-float {
                    margin: 0 auto;
                    position: relative;
                }

                .eb-popup-wp .closeBtn {
                    width: 9px;
                    height: 9px;
                    background: url(../images/icon-collection.png) no-repeat;
                    background-position: left 0 top 0px;
                    background-position-x: 0px;
                    background-position-y: 0;
                    position: absolute !important;
                    top: 6px;
                    right: /*-18px*/ 6px;
                    cursor: pointer;
                    z-index: 2000;
                }

                    .eb-popup-wp .closeBtn .child-wp {
                        width: 100%;
                        position: relative;
                        display: none;
                    }

                        .eb-popup-wp .closeBtn .child-wp .text {
                            padding: 5px 10px;
                            background: rgba(0,0,0,0.4);
                            color: #ffffff;
                            position: absolute;
                            top: 18px;
                            right: -7px;
                            width: 136px;
                            font-family: Roboto Condensed Light;
                        }

                        .eb-popup-wp .closeBtn .child-wp .triangle {
                            width: 0;
                            height: 0;
                            border-style: solid;
                            border-width: 0 5px 6px 5px;
                            border-color: transparent transparent rgba(0,0,0,1) transparent;
                            position: absolute;
                            top: 12px;
                            right: -1px;
                        }

                    .eb-popup-wp .closeBtn:hover .child-wp {
                        display: block;
                    }

            #EBPopupFloat, #EBConfirmYN, #EBPopupWP {
                display: none !important;
            }

            .iframe-image-upload {
                position: relative !important;
                margin: auto !important;
                margin-top: 8% !important;
            }

            .thumb:hover {
                cursor: zoom-in;
            }

            .btn-upload-img:hover {
                cursor: pointer;
            }

            .fa-trash-alt:hover {
                cursor: pointer;
            }

            .select-list-item {
                width: 100%;
                font-size: 14px;
                height: calc(2.25rem + 2px);
                line-height: 1.5;
                color: #495057;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #ced4da;
                border-radius: .25rem;
                transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
            }

                .select-list-item option {
                    font-size: 14px;
                }


                    .select-list-item option:nth-child(2) {
                        background-color: #e74c3c;
                        color: black;
                    }



            .checkbox .cr {
                position: relative;
                display: inline-block;
                border: 1px solid #a9a9a9;
                border-radius: .25em;
                width: 1.3em;
                height: 1.3em;
                float: left;
                margin-right: .5em
            }

                .checkbox .cr .cr-icon {
                    position: absolute;
                    font-size: .8em;
                    line-height: 0;
                    top: 50%;
                    left: 15%
                }

            .checkbox label input[type=checkbox] {
                display: none
            }

                .checkbox label input[type=checkbox] + .cr > .cr-icon {
                    opacity: 0
                }

                .checkbox label input[type=checkbox]:checked + .cr > .cr-icon {
                    opacity: 1
                }

                .checkbox label input[type=checkbox]:disabled + .cr {
                    opacity: .5
                }
        </style>
        <section>
            <div class="container-fluid sub-menu">
                <div class="row">
                    <div class="col-md-12 pt-3">
                        <ul>
                            <li>QUẢN LÝ NGƯỜI DÙNG &nbsp;&#187; </li>
                            <li><a href="/admin/quan-ly/danh-sach-nguoi-dung.html"><i class="fa fa-list-ul"></i>&nbsp DANH SÁCH TÀI KHOẢN NGƯỜI DÙNG</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section style="margin-top: 10px;">
            <form runat="server">
                <div class="container-fluid sub-menu">
                    <div class="col-lg-12 float-left">
                        <div class="card mb-6 shadow-sm">
                            <div class="card-body">
                                <div class="row max-box">
                                    <table class="table table-bordered table-striped" style="width: 100%;">
                                        <tbody id="body-item">
                                            <tr>
                                                <td>Tên người dùng: </td>
                                                <td><%=staff.Fullname %></td>
                                            </tr>
                                            <tr>
                                                <td>E-mail: </td>
                                                <td><%=user.Email %></td>
                                            </tr>
                                            <tr>
                                                <td>Số điện thoại: </td>
                                                <td><%=user.Phone %></td>
                                            </tr>
                                            <tr>
                                                <td>Tên tài khoản: </td>
                                                <td><%=user.Username %></td>
                                            </tr>
                                            <tr>
                                                <td>Đăng nhập lần đầu: </td>
                                                <td><%=(user.FirstLogin != null && user.FirstLogin == false) ? "Đã đăng nhập" : "Chưa đăng nhập" %></td>
                                            </tr>
                                            <tr>
                                                <td>Hạn mật khẩu: </td>
                                                <td><%=user.TimeLivePassword %></td>
                                            </tr>
                                            <tr>
                                                <td>Lần cuối thay mật khẩu: </td>
                                                <td><%=String.Format("{0:yyyy/MM/dd}", user.LastChangePassword)%></td>
                                            </tr>
                                            <tr>
                                                <td>Thời gian hết hạn mật khẩu: </td>
                                                <td>
                                                    <asp:TextBox onkeypress="return ValidateKeypress(/\d/,event);" runat="server" Style="text-align: center; width: 30%; margin: auto;" ID="txtTimeLivePassword" CssClass="form-control"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td>Yêu cầu xác thực: </td>
                                                <td>
                                                    <input id="chkIsCall" class="chkIsCall" type="checkbox" <%= user.RequiredValid2FA == true ? "Checked='checked'" : "" %> />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Hoạt động: </td>
                                                <td><%=staff.Active == 1 ? "Đang hoạt động" : "Đã nghỉ"%></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <hr />
                                <div class="btn-group">
                                    <asp:Panel ID="BtnSend" CssClass="btn btn-sm btn-outline-secondary" runat="server" ClientIDMode="Static"><i class="fas fa-check" style="color: green"></i>&nbsp;Hoàn tất</asp:Panel>
                                    <asp:Button ID="BtnFakeSend" OnClick="UpdateUser" runat="server" Text="Hoàn tất" ClientIDMode="Static" Style="display: none;"></asp:Button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:HiddenField runat="server" ID="HiddenCheckbox" ClientIDMode="Static" />
            </form>
            <!-- Page loading -->
            <div class="page-loading">
                <p>Vui lòng đợi trong giây lát...</p>
            </div>
        </section>
        <script type="text/javascript">
            $("#BtnSend").bind("click", function () {
                startLoading();
                $('#BtnFakeSend').click();
            });

            $("#chkIsCall").bind("click", function () {
                let check = $(this).prop("checked");
                $('#HiddenCheckbox').val(check);
            });

            // Validate keypress
            function ValidateKeypress(numcheck, e) {
                var keynum, keychar, numcheck;
                if (window.event) {
                    keynum = e.keyCode;
                }
                else if (e.which) {
                    keynum = e.which;
                }
                if (keynum == 8 || keynum == 127 || keynum == null || keynum == 9 || keynum == 0 || keynum == 13 || keynum == 46) return true;
                keychar = String.fromCharCode(keynum);
                var result = numcheck.test(keychar);
                return result;
            }
        </script>
    </asp:Panel>
</asp:Content>
