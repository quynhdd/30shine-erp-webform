﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="ListingUsersProfile.aspx.cs" Inherits="_30shine.GUI.BackEnd.User.ListingUsersProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            table.table-report-department {
            }

            table.table-report-department {
                border-collapse: collapse;
            }

            table.table-report-department, .table-report-department th, .table-report-department td {
                border: 1px solid #bababa;
            }

            .table-report-department th {
                font-weight: normal;
                font-family: Roboto Condensed Bold;
                padding: 5px 10px;
            }

            .table-report-department td {
                padding: 5px 10px;
            }

            .tip {
                /*border-bottom: 1px dashed;*/
                text-decoration: none
            }

                .tip:hover {
                    cursor: pointer;
                    position: relative
                }

                .tip span {
                    display: none
                }

                .tip:hover span {
                    border: #c0c0c0 1px solid;
                    border-radius: 5px;
                    -webkit-border-radius: 5px;
                    -moz-border-radius: 5px;
                    padding: 5px 20px 5px 5px;
                    display: block;
                    z-index: 100;
                    background: #c1d9ff repeat-x;
                    left: 0px;
                    margin: 10px;
                    width: auto !important;
                    min-width: 100px;
                    position: absolute;
                    bottom: 0px;
                    text-decoration: none;
                    color: #252734;
                }

            .alert {
                margin-bottom: 0 !important;
            }

            .btn-submit-staff {
                width: 100px;
            }
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>QUẢN LÝ NGƯỜI DÙNG &nbsp;&#187; </li>
                        <li class="be-report-li"><a href="#"><i class="fa fa-th-large"></i>DANH SÁCH TÀI KHOẢN NGƯỜI DÙNG</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing be-report be-report-timekeeping">
            <%-- Listing --%>
            <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
            <div class="wp960 content-wp">
                <div class="row">
                    <strong class="st-head"><i class="fa fa-filter"></i>Lọc kết quả</strong>
                    <asp:UpdatePanel runat="server" ID="UPsalon">
                        <ContentTemplate>
                            <div class="filter-item">
                                <asp:DropDownList ID="ddlSalon" runat="server" CssClass="form-control select" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel runat="server" ID="UPDepartment">
                        <ContentTemplate>
                            <div class="filter-item">
                                <asp:DropDownList ID="ddlDepartment" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" Style="z-index: 99999 !important" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                </div>
                <!-- Row Table Filter -->
                <div class="table-func-panel">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <!-- End Row Table Filter -->
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="row table-wp">
                            <table class="table-add table-listing table-listing-salary">
                                <thead>
                                    <tr>
                                        <th width="40px">STT</th>
                                        <th>Tên nhân viên</th>
                                        <th>Salon làm việc</th>
                                        <th>Bộ phận</th>
                                        <th>Tên tài khoản</th>
                                        <th>Thời hạn mật khẩu (ngày)</th>
                                        <th>Đăng nhập lần đầu</th>
                                        <th>Yêu cầu xác thực</th>
                                        <th>Chức năng</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="RptListingUser" runat="server" OnItemDataBound="Repeater_ItemDataBound">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td><%# Eval("StaffName") %></td>
                                                <td><%# Eval("SalonName") %></td>
                                                <td><%# Eval("DepartmentName") %></td>
                                                <td><%# Eval("UserName") %></td>
                                                <td><%# Eval("TimeLivePassword") %></td>
                                                <td><%# Eval("FirstLogin") %></td>
                                                <td><%# Eval("RequiredValid2FA") %></td>
                                                <td class="map-edit">
                                                    <div class="edit-wp">
                                                        <asp:Panel CssClass="edit-action-wp action-edit" runat="server" ID="EAedit" Visible="false">
                                                            <a class="elm edit-btn" href="/admin/quan-ly/cap-nhap-nguoi-dung/<%# Eval("StaffId") %>.html" title="Sửa"></a>
                                                        </asp:Panel>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>

                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>

                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />

                <!-- Hidden Field-->
                <asp:HiddenField ID="HDF_TypeStaff" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_Staff" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_ExcelType" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_ExportType" ClientIDMode="Static" runat="server" />
                <!-- END Hidden Field-->
            </div>
        </div>
        <%-- END Listing --%>
        <style>
            .be-report table.table-listing.table-listing-salary td {
                padding: 7px 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                margin-top: 4px !important;
            }

                .select2-container--default .select2-selection--single .select2-selection__arrow {
                    margin-top: 5px;
                }
        </style>
        <link href="/Assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
        <script src="/Assets/js/jquery.mCustomScrollbar.js"></script>
        <link href="/Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="/Assets/js/select2/select2.min.js"></script>
        <style>
            .select2-container {
                width: 190px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                $('.select').select2();
            });

            $("#BtnFakeUP").click(function () {
                $('.select').select2();
            });
        </script>
    </asp:Panel>
</asp:Content>
