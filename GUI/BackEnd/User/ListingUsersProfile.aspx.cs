﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.User
{
    public partial class ListingUsersProfile : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        private bool Perm_Access = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_Edit = false;
        private IAuthenticationModel model = new AuthenticationModel();
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                Library.Function.bindSalon_NotHoiQuan(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
                BindDepartment();
            }
        }

        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        private void BindDepartment()
        {
            using (var db = new Solution_30shineEntities())
            {
                ListItem item = new ListItem("Chọn bộ phận", "0");
                ddlDepartment.Items.Insert(0, item);

                var lst = db.Staff_Type.Where(w => w.IsDelete != 1).OrderBy(o => o.Id).ToList();
                if (lst.Count > 0)
                {
                    for (int i = 0; i < lst.Count; i++)
                    {
                        ddlDepartment.Items.Add(new ListItem(lst[i].Name, lst[i].Id.ToString()));
                    }
                    ddlDepartment.DataBind();
                }
            }
        }

        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();
            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        public void BindData()
        {
            try
            {
                int salonId = int.TryParse(ddlSalon.SelectedValue, out var integer) ? integer : 0;
                int departmentId = int.TryParse(ddlDepartment.SelectedValue, out integer) ? integer : 0;
                var list = model.GetList(salonId, departmentId);
                if (list.Count > 0)
                {
                    Bind_Paging(list.Count);
                    RptListingUser.DataSource = list.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                    RptListingUser.DataBind();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "SelectDropdownlist();removeLoading();", true);
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            BindData();
            RemoveLoading();
        }

        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
            }
        }
    }

    public class UserListing
    {
        public int StaffId { get; set; }
        public string StaffName { get; set; }
        public string SalonName { get; set; }
        public string DepartmentName { get; set; }
        public string UserName { get; set; }
        public int TimelivePassword { get; set; }
        public string FirstLogin { get; set; }
        public string RequiredValid2FA { get; set; }
    }
}