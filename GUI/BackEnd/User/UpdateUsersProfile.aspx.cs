﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.User
{
    public partial class UpdateUsersProfile : System.Web.UI.Page
    {
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        private IAuthenticationModel model = new AuthenticationModel();
        public MODEL.ENTITY.EDMX.User user = new MODEL.ENTITY.EDMX.User();
        public Staff staff = new Staff();

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        public int GetIds()
        {
            return int.TryParse(Request.QueryString["Code"], out var Code) ? Code : 0;
        }

        public void GetData()
        {
            try
            {
                int id = GetIds();
                if (id > 0)
                {
                    var data = model.GetUserByStaffId(id);
                    if (data != null)
                    {
                        user = data.Item1;
                        staff = data.Item2;
                        txtTimeLivePassword.Text = (user.TimeLivePassword ?? 0).ToString();
                        HiddenCheckbox.Value = (user.RequiredValid2FA ?? true).ToString();
                        if (user.LastChangePassword != null && user.TimeLivePassword != null)
                        {
                            user.TimeLivePassword = Convert.ToInt32((user.LastChangePassword.Value.AddDays(user.TimeLivePassword.Value).Date - DateTime.Now.Date).TotalDays);
                        }
                    }
                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                GetData();
            }
        }

        /// <summary>
        /// Update thông tin người dùng
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UpdateUser(object sender, EventArgs e)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    if (!String.IsNullOrEmpty(HiddenCheckbox.Value))
                    {
                        var check = bool.TryParse(HiddenCheckbox.Value, out var result) ? result : false;
                        var time = int.TryParse(txtTimeLivePassword.Text, out var integer) ? integer : 0;
                        int id = GetIds();
                        if (id > 0)
                        {
                            if (String.IsNullOrEmpty(txtTimeLivePassword.Text))
                            {
                                TriggerJsMsgSystem(this, "Thời hạn mật khẩu không được để trống!", 3, 5000);
                            }
                            else if (time <= 0)
                            {
                                TriggerJsMsgSystem(this, "Thời hạn mật khẩu không được bằng 0!", 3, 5000);
                            }
                            else
                            {
                                var user = db.Users.FirstOrDefault(w => w.StaffId == id);
                                if (user != null)
                                {
                                    user.TimeLivePassword = int.TryParse(txtTimeLivePassword.Text, out var number) ? number : 1;
                                    user.RequiredValid2FA = check;
                                    db.Users.AddOrUpdate(user);
                                    db.SaveChanges();
                                    TriggerJsMsgSystem(this, "Hoàn tất thành công", 2, 5000);
                                }
                                else
                                {
                                    TriggerJsMsgSystem(this, "Hoàn tất thất bại", 4, 5000);
                                }
                            }
                            GetData();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Trả thông báo về client 
        /// </summary>
        /// <param name="_OBJ"></param>
        /// <param name="msg"></param>
        /// <param name="status"></param>        
        public void TriggerJsMsgSystem(Page page, string msg, int status, int timeout = 3000)
        {
            ScriptManager.RegisterStartupScript(page, page.GetType(), "Message System", "ShowMessage('Thông báo','" + msg + "'," + status + ", " + timeout + ");finishLoading();", true);
        }
    }
}