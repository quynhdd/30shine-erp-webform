﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.Helpers;
using _30shine.MODEL.ENTITY.EDMX;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using System.Globalization;

namespace _30shine.GUI.BackEnd.Membership
{
    public partial class MembershipLogListing : System.Web.UI.Page
    {
        private string PageID = "";
        private bool Perm_Access = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_Delete = false;
        protected bool Perm_Edit = false;
        protected Paging PAGING = new Paging();
        protected string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));

        /// <summary>
        /// check permission
        /// </summary>
        /// 
        protected void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = FirstDayOfMonth;
                Library.Function.bindSalon_NotHoiQuan(new List<DropDownList>() { ddlSalon }, Perm_ViewAllData);
                BindPackageMember();
                BindData();
            }
        }

        /// <summary>
        /// Bind PackageMember to ddl
        /// </summary>
        protected void BindPackageMember()
        {
            using (var db = new Solution_30shineEntities())
            {
                List<Product> packageMember = db.Products.Where(w => w.IsDelete == 0 && w.Publish == 1 && w.CategoryId == 98).OrderBy(o => o.Id).ToList();
                var item = new Product();
                item.Name = "Chọn tất cả gói Member";
                item.Id = 0;
                packageMember.Insert(0, item);
                ddlMembership.DataTextField = "Name";
                ddlMembership.DataValueField = "Id";
                ddlMembership.DataSource = packageMember;
                ddlMembership.DataBind();
            }
        }

        /// <summary>
        /// Get data bind to rpt
        /// </summary>
        private void BindData()
        {
            try
            {
                DateTime TimeFrom, TimeTo;
                if (!String.IsNullOrEmpty(TxtDateTimeFrom.Text.Trim()))
                {
                    TimeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, new CultureInfo("vi-VN"));
                    if (!String.IsNullOrEmpty(TxtDateTimeTo.Text.Trim()))
                    {
                        TimeTo = Convert.ToDateTime(TxtDateTimeTo.Text, new CultureInfo("vi-VN"));
                    }
                    else
                    {
                        TimeTo = TimeFrom.AddDays(1);
                    }
                    var memberLogs = GetData(TimeFrom, TimeTo, int.TryParse(ddlSalon.SelectedValue, out var integer) ? integer : 0,
                                              int.TryParse(ddlMembership.SelectedValue, out integer) ? integer : 0);
                    Bind_Paging(memberLogs.Count);
                    RptMemberLog.DataSource = memberLogs.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                    RptMemberLog.DataBind();
                }
            }
            catch (Exception e)
            {
                TriggerJsMsgSystem(this, "Lỗi: " + e.Message, "msg -system warning", 5000);
            }
        }

        private List<MemberLog> GetData(DateTime TimeFrom, DateTime Timeto, int SalonId, int MemberId)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    string sql = $@"SELECT 
	                                    e.ShortName AS SalonName, c.Fullname AS CustomerName, d.Name AS MemberName, 
	                                    CONVERT(VARCHAR(10), a.StartTime, 120) AS StartTime, 
	                                    CONVERT(VARCHAR(10), a.EndTime, 120) AS EndTime,
	                                    d.Price AS MemberPrice 
                                    FROM dbo.MemberLog a
                                    INNER JOIN dbo.BillService b ON a.BillId = b.Id
                                    INNER JOIN dbo.Customer c ON c.Id = a.CustomerId
                                    INNER JOIN dbo.Product d ON d.Id = a.ProductId
                                    INNER JOIN dbo.Tbl_Salon e ON e.Id = b.SalonId
                                    WHERE 
                                    a.isDelete = 0 AND ((b.SalonId = {SalonId}) OR ({SalonId} = 0))
                                    AND b.IsDelete = 0 AND b.Pending = 0
                                    AND ((a.ProductId = {MemberId}) OR ({MemberId} = 0))
                                    AND ((a.StartTime BETWEEN '{TimeFrom}' AND '{Timeto}') 
                                    OR (a.EndTime BETWEEN '{TimeFrom}' AND '{Timeto}'))
                                    ORDER BY MemberName DESC";
                    return db.Database.SqlQuery<MemberLog>(sql).ToList();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _BtnClick(object sender, EventArgs e)
        {
            BindData();
            RemoveLoading();
        }

        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();
            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        protected static void TriggerJsMsgSystem(Page OBJ, string msg, string status, int duration)
        {
            ScriptManager.RegisterStartupScript(OBJ, OBJ.GetType(), "Message System",
                                    "showMsgSystem('" + msg.Replace("'", "") + "','" + status + "'," + duration + ");", true);
        }



        public class MemberLog
        {
            public string SalonName { get; set; }
            public string CustomerName { get; set; }
            public string MemberName { get; set; }
            public string StartTime { get; set; }
            public string EndTime { get; set; }
            public int MemberPrice { get; set; }
        }
    }
}