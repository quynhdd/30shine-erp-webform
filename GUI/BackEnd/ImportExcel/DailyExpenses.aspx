﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="DailyExpenses.aspx.cs" Inherits="_30shine.GUI.BackEnd.ImportExcel.DailyExpenses" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <title>import chi phí hàng ngày cho salon file excel
    </title>
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>--%>
    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.0/jszip.js"></script>--%>
    <script src="/Assets/js/config.js?654873513423"></script>
    <script src="/Assets/js/libs.xlsx/libs-xlsx-0.8.0-jszip.js"></script>
    <script src="/Assets/js/libs.xlsx/libs-xlsx-0.8.0-xlsx.js"></script>
    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.0/xlsx.js"></script>--%>
    <%--<script src=" https://cdnjs.cloudflare.com/ajax/libs/xls/0.7.4-a/xls.js"></script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="Panel1" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .wp_table {
                overflow: scroll;
                width: 100%;
                float: left;
            }

            .cls_table {
                padding: 5px;
                overflow-y: scroll;
            }

            .cls_table, td, th {
                border: 1px solid black;
                border-collapse: collapse;
                padding: 5px;
                min-width: 100px;
            }

                .cls_table, td:first-child, th:first-child {
                    min-width: 30px
                }

            .h2-cls-excel {
                width: 100%;
                float: left;
                text-align: center;
                font-size: 20px;
                padding: 10px;
                text-transform: uppercase;
            }

            .h1-title-timport-file-excel {
                width: 100%;
                float: left;
                font-size: 20px;
                text-transform: uppercase;
                margin-bottom: 10px;
            }

            .wp-page {
                width: 100%;
                float: left;
                padding: 20px 10px 0;
            }
            /*.wp_import-file-excel { width: 100%; float: left; text-align: center; }*/
            #my_file_input {
            }

                #my_file_input::-webkit-file-upload-button {
                    color: red;
                }

                #my_file_input::before {
                    color: red;
                    background: none;
                    border: none;
                }

            .wp-submit {
                width: 100%;
                float: left;
                margin: 15px 0px 20px;
                text-align: right;
            }

            #my_file_input {
                float: left;
            }

            .cls_tempfile {
                float: left;
                text-decoration: underline !important;
                color: #0277bd;
                margin-left: 30px;
            }
        </style>
        <div class="wp-page">
            <div class="wp_import-file-excel">
                <h1 class="h1-title-timport-file-excel">Import thông tin báo cáo.</h1>
                <input type="file" id="my_file_input" />
                <div id='my_file_output'></div>
                <a href="/TemplateFile/ImportExcelBCKD/Huong_Dan_Su_Dung.xlsx" class="cls_tempfile">Tải File hướng dẫn sử dụng</a>
                <a href="/TemplateFile/ImportExcelBCKD/File_Excel_Import_MAU.xlsx" class="cls_tempfile">Tải File Excel Import MẪu</a>
            </div>
            <h2 class="h2-cls-excel">Thông tin Import file excel</h2>
            <div class="wp_table">
                <table class="cls_table">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Ngày</th>
                            <th>ID Salon
                            </th>
                            <th>Tên Salon viết tắt
                            </th>
                            <th>Tiền điện. nước.
                            </th>
                            <th>Thuê cửa hàng
đã bao gồm chi phí thuế
                            </th>
                            <th>Phân bổ 
đầu tư khấu hao, chi phí trả trước
                            </th>
                            <th>CP quảng cáo
                            </th>
                            <th>Chi phí 
vận chuyển TQ
                            </th>
                            <th>Cước điện thoại. internet
                            </th>
                            <th>Tiền BHXH, KPCĐ
                            </th>
                            <th>TNDN
                            </th>
                            <th>Chi phí vận hành 
phát sinh hàng ngày
                            </th>
                            <th>Chi phí thuê văn phòng Thái Hà
+ Phí dịch vụ
                            </th>
                            <th>LƯƠNG CB
                            </th>
                            <th>Lương thưởng khác
                            </th>
                            <th>CHI PHÍ LƯƠNG IT
                            </th>
                            <th>Tiền BHXH
BPC
                            </th>
                            <th>Chi phí vận hành 
phát sinh hàng ngày
                            </th>
                            <th>Giá vốn - dịch vụ - mỹ phẩm, CCDC sử dụng - Khăn
                            </th>
                            <th>Giá vốn bù trừ - MP bán
                            </th>
                            <th>Thu nhập khác
                            </th>
                            <th>Phân bổ ShineMember
                            </th>
                            <th>Lương thưởng doanh số KCS
                            </th>
                            <th>Chi phí SMS
                            </th>
                            <th>THUẾ GTGT
                            </th>
                            <th>Lương Bảo vệ + Checkout 
                            </th>
                        </tr>
                    </thead>
                    <tbody id="body-content">
                    </tbody>
                </table>
            </div>
            <div class="wp-submit">
                <a href="javascript://" onclick="UploadData($(this))" class="btn btn-success">UpLoad File</a>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                var maxheight = $(window).height();
                //console.log('aba', aba);
                $('.wp_table').css('max-height', maxheight);
            });
            var uri = window.apisReportBCKQKD.domain;
            var objListDailyExpense = {};
            var oFileIn;

            $(function () {
                oFileIn = document.getElementById('my_file_input');
                if (oFileIn.addEventListener) {
                    oFileIn.addEventListener('change', filePicked, false);
                }
            });

            // func get data
            function filePicked(oEvent) {
                // Get The File From The Input
                var oFile = oEvent.target.files[0];
                var sFilename = oFile.name;
                // Create A File Reader HTML5
                var reader = new FileReader();

                // Ready The Event For When A File Gets Selected
                reader.onload = function (e) {
                    var data = e.target.result;
                    var cfb = XLSX.read(data, { type: 'binary' });
                    // Here is your object
                    var XL_row_object = XLSX.utils.sheet_to_row_object_array(cfb.Sheets["Sheet1"]);
                    var obj = XLSX.utils.sheet_to_row_object_array()
                    objListDailyExpense = XL_row_object;
                    console.log("XL_row_object", XL_row_object);

                    //var sCSV = XLS.utils.make_csv(cfb.Sheets["Sheet1"]);
                    //var oJS = XLS.utils.sheet_to_json(cfb.Sheets["Sheet1"]);
                    //console.log("oJS", oJS);
                    // append html 
                    if (objListDailyExpense.length > 0) {
                        appendData(objListDailyExpense);
                    }
                    else {
                        $("#body-content").html('');
                        objListDailyExpense = {};
                        alert("Bạn vui lòng check lại file excel");
                    }


                };

                // Tell JS To Start Reading The File.. You could delay this if desired
                reader.readAsBinaryString(oFile);
            }
            // func apeend html
            function appendData(objData) {
                debugger;
                var str = "";
                var Total = 0;
                $("#body-content").html('');
                const Count = objData.length;
                console.log('Count', Count);
                var stt = 0;
                if (Count > 0) {
                    for (var i = 0; i < Count; i++) {
                        Total = Total + parseFloat(objData[i].ElectricityAndWaterBill);
                        stt++
                        str += '<tr> ' +
                            //stt
                            '<td>' +
                            stt +
                            ' </td>' +
                            // date
                            '<td>' +
                            objData[i].ReportDate +
                            ' </td>' +
                            //ID Salon
                            '<td> ' +
                            objData[i].SalonId +
                            '</td>' +
                            //Tên Salon viết tắt
                            '<td> ' +
                            objData[i].salonShortName +
                            '</td>' +
                            //Tiền điện. nước.
                            '<td> ' +
                            objData[i].ElectricityAndWaterBill +
                            '</td>' +
                            //Thuê cửa hàng đã bao gồm chi phí tduế
                            '<td> ' +
                            objData[i].RentWithTax +
                            '</td>' +
                            //Phân bổ đầu tư khấu hao, chi phí trả trước
                            '<td> ' +
                            objData[i].CapitalSpending +
                            ' </td>' +
                            //CP quảng cáo
                            '<td> ' +
                            objData[i].AdvertisementExpend +
                            ' </td>' +
                            //Chi phí vận chuyển TQ
                            '<td> ' +
                            objData[i].ShippingExpend +
                            '</td>' +
                            //Cước điện tdoại. internet
                            '<td> ' +
                            objData[i].InternetAndPhoneBill +
                            ' </td>' +
                            //Tiền BHXH, KPCĐ
                            '<td> ' +
                            objData[i].SocialInsuranceAndFixedCost +
                            '</td>' +
                            //TNDN
                            '<td> ' +
                            objData[i].IncomeTaxes +
                            ' </td>' +
                            //Chi phí vận hành phát sinh hàng ngày
                            '<td> ' +
                            objData[i].SalonUnplannedSpending +
                            ' </td>' +
                            //Chi phí tduê văn phòng Thái Hà + Phí dịch vụ
                            '<td> ' +
                            objData[i].ThaiHaRentAndSeviceCost +
                            ' </td>' +
                            //LƯƠNG CB
                            '<td> ' +
                            objData[i].OfficeStaffSalary +
                            ' </td>' +
                            // LƯƠNG DOANH SỐ/ KPI/ OKR
                            '<td>  ' +
                            objData[i].SalesSalary +
                            ' </td>' +
                            // CHI PHÍ LƯƠNG IT
                            '<td> ' +
                            objData[i].ItSalary +
                            ' </td>' +
                            // Tiền BHXH BPC
                            '<td> ' +
                            objData[i].OfficeStaffSocialInsurance +
                            ' </td>' +
                            // Chi phí vận hành phát sinh hàng ngày
                            '<td> ' +
                            objData[i].UnplannedSpending +
                            ' </td>' +
                            // Giá vốn - dịch vụ - mỹ phẩm, CCDC sử dụng - Khăn
                            '<td> ' +
                            objData[i].DailyCostInventory +
                            ' </td>' +
                            // Gía vốn bù trừ - MP bán
                            '<td> ' +
                            objData[i].Compensation +
                            ' </td>' +
                            // thu nhap khac
                            '<td> ' +
                            objData[i].OtherIncome +
                            '</td>' +
                            // phan bo Shinemember
                            '<td>' +
                            objData[i].SMDistribution +
                            '</td>' +
                            // Luong thuong doanh so KCS
                            '<td>' +
                            objData[i].PayOffKCS +
                            '</td>' +
                            // Chi phi SMS
                            '<td>' +
                            objData[i].SMSExpenses +
                            '</td>' +
                            // Thue GTGT
                            '<td>' +
                            objData[i].Tax +
                            '</td>' +
                            // Luong Bv + checkout
                            '<td>' +
                            objData[i].SecurityCheckoutSalary +
                            ' </td>' +
                            '</tr>';
                    }
                    if (str != '') {
                        $("#body-content").append(str);
                    }
                    console.log("Total", Total);
                }

            }
            // func click luu data
            function UploadData(This) {
                if (objListDailyExpense.length > 0) {
                    startLoading();
                    $.ajax({
                        type: "POST",
                        url: uri + window.apisReportBCKQKD.PostApiSalonDailyCost,
                        //async: false,
                        data: JSON.stringify(objListDailyExpense),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (objData) {
                            alert("Import file thành công !");
                            finishLoading();
                            location.reload();
                            //console.log("DAta", objData);
                        },

                        error: function (jqXHR, textStatus, errorThrown) {
                            var responseText = jqXHR.responseJSON.message;
                            objListDailyExpense = {};
                            alert("Lỗi :  " + responseText + '! Bạn vui lòng check lại file excel import');
                            location.reload();
                        }
                    });
                }
            };
        </script>

        <!-- Page loading -->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
    </asp:Panel>
</asp:Content>
