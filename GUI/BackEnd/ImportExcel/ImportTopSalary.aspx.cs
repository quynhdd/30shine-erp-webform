﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System.Globalization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Services;
using System.Web.UI.WebControls;
using System.Data.Entity;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace _30shine.GUI.BackEnd.ImportExcel
{
    public partial class ImportTopSalary : System.Web.UI.Page
    {
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;
        private Solution_30shineEntities context;
        private DbSet<TopSalary> objEntity;
        public ImportTopSalary()
        {
            context = new Solution_30shineEntities();
            objEntity = context.Set<TopSalary>();
        }
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ViewAllDate = permissionModel.CheckPermisionByAction("Perm_ViewAllDate", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
        }

        [WebMethod(EnableSession = true)]
        [HttpPost]
        public static HttpStatusCode ImportExcelTopSalary(string data)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    //JavaScriptSerializer serializer = new JavaScriptSerializer();
                    var listData = JsonConvert.DeserializeObject<IEnumerable<TopSalary>>(data);
                    if (listData == null)
                    {
                        return HttpStatusCode.NoContent;

                    }

                    var staffIds = listData.Select(a => a.staffId);
                    foreach (var id in staffIds)
                    {
                        if (listData.Where(a => a.staffId == id).Select(a => a.staffId).Distinct().Count() != listData.Where(a => a.staffId == id).Select(a => a.staffId).Count())
                        {
                            return HttpStatusCode.BadRequest;
                        }
                    }

                    var checkdepartment = listData.Where(a => a.departmentId == 0).ToList();
                    if (checkdepartment.Count() > 0)
                    {
                        return HttpStatusCode.BadRequest;
                    }

                    var checkStaffId = listData.Where(a => a.staffId == 0).ToList();
                    if (checkStaffId.Count() > 0)
                    {
                        return HttpStatusCode.BadRequest;
                    }

                    int Month = DateTime.Now.Month - 1;
                    int Year = DateTime.Now.Year;
                    var ListSalaryOld = db.TopSalaries.Where(a => a.isDelete == false && a.month == Month && a.year == Year).ToList();

                    List<TopSalary> ListSalary = listData.Select(a => new TopSalary
                    {
                        staffId = a.staffId,
                        staffName = a.staffName,
                        departmentId = a.departmentId,
                        month = Month,
                        year = Year,
                        salonName = a.salonName,
                        skillLevel = a.skillLevel,
                        salary = a.salary,
                        createTime = DateTime.Now,
                        isDelete = false

                    }).ToList();
                    //delete list ListSalaryOld;
                    if (ListSalaryOld.Count > 0)
                    {
                        foreach (var item in ListSalaryOld)
                        {
                            db.TopSalaries.Remove(item);
                        }
                    }
                    if (ListSalary.Count > 0)
                    {
                        foreach (var item in ListSalary)
                        {
                            db.TopSalaries.Add(item);

                        }
                    }
                    db.SaveChanges();
                    return HttpStatusCode.OK;
                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        //[WebMethod]
        //[System.Web.Script.Services.ScriptMethod(UseHttpGet = true)]
        //public static object GetTopSalaryStaff()
        //{
        //    try
        //    {
        //        using (var db = new Solution_30shineEntities())
        //        {
        //            var ListAll = db.TopSalaries.Where(a => a.isDelete == false).ToList();
        //            var listStylist = ListAll.Where(a => a.departmentId == 1).Select(a => new GetTopSalary
        //            {
        //                id = a.id,
        //                staffId = a.staffId,
        //                staffName = a.staffName,
        //                departmentId = a.departmentId,
        //                departmentName = "Stylist",
        //                month = a.month,
        //                year = a.year,
        //                skillLevel = a.skillLevel,
        //                salonName = a.salonName,
        //                salary = a.salary
        //            }).OrderByDescending(a => a.salary).OrderByDescending(a => a.month).OrderByDescending(a => a.year);
        //            var listSkinner = ListAll.Where(a => a.departmentId == 2).Select(a => new GetTopSalary
        //            {
        //                id = a.id,
        //                staffId = a.staffId,
        //                staffName = a.staffName,
        //                departmentId = a.departmentId,
        //                departmentName = "Skinner",
        //                month = a.month,
        //                year = a.year,
        //                skillLevel = a.skillLevel,
        //                salonName = a.salonName,
        //                salary = a.salary
        //            }).OrderByDescending(a => a.salary).OrderByDescending(a => a.month).OrderByDescending(a => a.year);

        //            var ResulList = listStylist.Union(listSkinner).ToList();

        //            if (!ResulList.Any())
        //            {
        //                return HttpStatusCode.NoContent;
        //            }
        //            return ResulList;
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //        throw e;
        //    }
        //}


    }
    //public class GetTopSalary : TopSalary
    //{
    //    public string departmentName { get; set; }
    //}

}