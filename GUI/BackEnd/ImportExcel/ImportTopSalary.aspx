﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="ImportTopSalary.aspx.cs" Inherits="_30shine.GUI.BackEnd.ImportExcel.ImportTopSalary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <title>import top luong
    </title>
    <script src="/Assets/js/config.js"></script>
    <script src="/Assets/js/libs.xlsx/libs-xlsx-0.8.0-jszip.js"></script>
    <script src="/Assets/js/libs.xlsx/libs-xlsx-0.8.0-xlsx.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="Panel1" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .wp_table {
                /*overflow: scroll;*/
                width: 100%;
                float: left;
            }

            .cls_table {
                padding: 5px;
                /*overflow-y: scroll;*/
                margin: 0 auto;
            }

            .cls_table, td, th {
                border: 1px solid black;
                border-collapse: collapse;
                padding: 5px;
                min-width: 100px;
            }

                .cls_table, td:first-child, th:first-child {
                    min-width: 30px
                }

            .h2-cls-excel {
                width: 100%;
                float: left;
                text-align: center;
                font-size: 20px;
                padding: 10px;
                text-transform: uppercase;
            }

            .h1-title-timport-file-excel {
                width: 100%;
                float: left;
                font-size: 20px;
                text-transform: uppercase;
                margin-bottom: 10px;
            }

            .wp-page {
                width: 100%;
                float: left;
                padding: 20px 10px 0;
            }

            .wp_import-file-excel {
                width: 100%;
                float: left;
                text-align: center;
            }

            #my_file_input {
                margin: 0 auto;
            }

                #my_file_input::-webkit-file-upload-button {
                    color: red;
                }

                #my_file_input::before {
                    color: red;
                    background: none;
                    border: none;
                }

            .wp-submit {
                width: 100%;
                float: left;
                margin: 15px 0px 20px;
                text-align: center;
            }

            .cls_tempfile {
                float: left;
                text-decoration: underline !important;
                color: #0277bd;
                margin-left: 30px;
            }

            #a-hepl {
                margin-left: 15px;
                padding: 5px 10px;
                color: #FFF;
                background: #000;
            }

                #a-hepl:hover {
                    color: #ffe400;
                    cursor:pointer;
                }
        </style>
        <div class="wp-page">
            <div class="wp_import-file-excel">
                <h1 class="h1-title-timport-file-excel">Import top lương nhân viên <a class="a-hepl" id="a-hepl" data-toggle="modal" data-target="#myModal">Hướng dẫn Import dữ liệu</a></h1>
                <input type="file" id="my_file_input" />
                <div id='my_file_output'></div>
                <%-- <a href="/TemplateFile/ImportExcelBCKD/Huong_Dan_Su_Dung.xlsx" class="cls_tempfile">Tải File hướng dẫn sử dụng</a>
                <a href="/TemplateFile/ImportExcelBCKD/File_Excel_Import_MAU.xlsx" class="cls_tempfile">Tải File Excel Import MẪu</a>--%>
            </div>
            <h2 class="h2-cls-excel">Thông tin Import file excel top lương nhân viên</h2>
            <div class="wp_table">
                <table class="cls_table">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Id Nhân viên</th>
                            <th>Tên Nhân Viên
                            </th>
                            <th>Id Bộ Phận</th>
                            <th>Tên Bộ Phận</th>
                            <th>Tên Salon
                            </th>
                            <th>Level
                            </th>
                            <th>Lương
                            </th>
                        </tr>
                    </thead>
                    <tbody id="body-content">
                    </tbody>
                </table>
            </div>
            <div class="wp-submit">
                <a href="javascript://" onclick="UploadData($(this))" class="btn btn-success">UpLoad File</a>
            </div>
            <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Hướng dẫn file import dữ liệu</h4>
      </div>
      <div class="modal-body">
        <p>- Sheet file excel là Sheet đầu tiên, Tên Sheet là : <strong>Sheet1</strong> </p>
          <img style="max-width:100%; margin:10px 0" src="/Assets/images/import-top-salary.PNG" />
          <strong>Trong đó : </strong>
           <p>- <strong>staffId : </strong> Id của nhân viên </p>
          <p>- <strong>staffName : </strong> Tên của nhân viên </p>
          <p>- <strong>departmentId : </strong> Id bộ phận </p>
           <p>  &#160;&#160; + <strong>departmentId = 1  </strong> là Stylist</p>
          <p>   &#160;&#160;  + <strong>departmentId = 2  </strong> là Skinner</p>
          <p>- <strong>salonName : </strong> Tên salon </p>
          <p>- <strong>skillLevel : </strong> Level của nhân viên </p>
          <p>- <strong>salary : </strong> Lương của nhân viên </p>
          <p>- <strong>Lưu ý : </strong> ảnh chính là file import mẫu </p> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
        </div>
        <script>
            $(document).ready(function () {
                //GetData();
                //var maxheight = $(window).height();
                //console.log('aba', aba);
                //$('.wp_table').css('max-height', maxheight);
            });
            var uri = window.apisReportBCKQKD.domain;
            var objListImportTopSalary = {};
            var oFileIn;

            $(function () {
                oFileIn = document.getElementById('my_file_input');
                if (oFileIn.addEventListener) {
                    oFileIn.addEventListener('change', filePicked, false);
                }
            });

            // func get data
            function filePicked(oEvent) {
                // Get The File From The Input
                var oFile = oEvent.target.files[0];
                var sFilename = oFile.name;
                // Create A File Reader HTML5
                var reader = new FileReader();

                // Ready The Event For When A File Gets Selected
                reader.onload = function (e) {
                    var data = e.target.result;
                    var cfb = XLSX.read(data, { type: 'binary' });
                    // Here is your object
                    var XL_row_object = XLSX.utils.sheet_to_row_object_array(cfb.Sheets["Sheet1"]);
                    var obj = XLSX.utils.sheet_to_row_object_array()
                    objListImportTopSalary = XL_row_object;
                    console.log("XL_row_object", XL_row_object);

                    //var sCSV = XLS.utils.make_csv(cfb.Sheets["Sheet1"]);
                    //var oJS = XLS.utils.sheet_to_json(cfb.Sheets["Sheet1"]);
                    //console.log("oJS", oJS);
                    // append html 
                    if (objListImportTopSalary.length > 0) {
                        appendData(objListImportTopSalary);
                    }
                    else {
                        $("#body-content").html('');
                        objListImportTopSalary = {};
                        alert("Bạn vui lòng check lại file excel");
                    }


                };

                // Tell JS To Start Reading The File.. You could delay this if desired
                reader.readAsBinaryString(oFile);
            }
            // func apeend html
            function appendData(objData) {
                var str = "";
                $("#body-content").html('');
                const Count = objData.length;
                var stt = 0;
                if (Count > 0) {
                    for (var i = 0; i < Count; i++) {
                        console.log(objData[i].departmentId);
                        stt++
                        str += '<tr> ' +
                            //stt
                            '<td>' +
                            stt +
                            ' </td>' +
                            // Id nhân viên
                            '<td>' +
                            objData[i].staffId +
                            ' </td>' +
                            //Tên nhân viên
                            '<td> ' +
                            objData[i].staffName +
                            '</td>' +
                            //Id bộ phận
                            '<td> ' +
                            objData[i].departmentId +
                            '</td>' +
                            //Tên bộ phận
                            '<td> ' +
                            (objData[i].departmentId == 1 ? "Stylist" : (objData[i].departmentId == 2 ? "Skinner" : "Không xác định ")) +
                            //(objData[i].departmentId == 1 ? "Stylist" : "Skinner") +
                            '</td>' +
                            // Tên salon
                            '<td> ' +
                            objData[i].salonName +
                            '</td>' +
                            //Level
                            '<td> ' +
                            objData[i].skillLevel +
                            ' </td>' +
                            //Lương
                            '<td> ' +
                            objData[i].salary +
                            ' </td>' +
                            '</tr>';
                    }
                    if (str != '') {
                        $("#body-content").append(str);
                    }
                }
            }
            // func click luu data
            function UploadData(This) {

                if (objListImportTopSalary.length > 0) {
                    startLoading();
                    $.ajax({
                        type: "POST",
                        url: "/import/insert-and-delete-top-salary",

                        data: '{data : \'' + JSON.stringify(objListImportTopSalary) + '\'}',
                        //async: false,
                        //data: JSON.stringify(objListImportTopSalary),
                        //data: objListImportTopSalary,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (objData, textStatus, xhr) {

                            if (objData.d === 400) {
                                alert("Có lỗi xảy ra bạn vui long xem lại file import! ");
                                finishLoading();
                                location.reload();
                            }
                            else {
                                console.log("objData", objData);
                                console.log("textStatus", textStatus);
                                console.log("xhr", xhr);
                                alert("Import file thành công !");
                                location.reload();
                            }
                            //console.log("DAta", objData);
                        },

                        error: function (jqXHR, textStatus, errorThrown) {
                            var responseText = textStatus;
                            objListImportTopSalary = {};
                            alert(responseText);
                            location.reload();
                        }
                    });
                }
                else {
                    alert("Không có dữ liệu! ");
                }
            };
            // tesst get
            //function GetData() {
            //    startLoading();
            //    $.ajax({
            //        type: "GET",
            //        url: "/api/get-top-salary-staff",

            //        //data: '{data : \'' + JSON.stringify(objListImportTopSalary) + '\'}',
            //        //async: false,
            //        //data: JSON.stringify(objListImportTopSalary),
            //        //data: ,
            //        contentType: "application/json; charset=utf-8",
            //        dataType: "json",
            //        success: function (objData, textStatus, xhr) {
            //            console.log("objData", objData);
            //            console.log("textStatus", textStatus);
            //            console.log("xhr", xhr);
            //            alert("Import file thành công !");
            //            finishLoading();
            //            //location.reload();
            //            //console.log("DAta", objData);
            //        },

            //        error: function (jqXHR, textStatus, errorThrown) {
            //            var responseText = textStatus;
            //            objListImportTopSalary = {};
            //            alert(responseText);
            //            finishLoading();
            //            //location.reload();
            //        }
            //    });
            //};
        </script>

        <!-- Page loading -->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
    </asp:Panel>
</asp:Content>
