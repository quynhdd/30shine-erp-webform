﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Marketting_Report.aspx.cs" Inherits="_30shine.GUI.BackEnd.MarKeting.Report.Marketting_Report" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<%--<%@ Register Src="~/GUI/BackEnd/Report/ReportMenu.ascx" TagName="ReportMenu" TagPrefix="uc1" %>--%>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">

    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <%--<uc1:ReportMenu runat="server" id="ReportMenu" />--%>
        <style>
            .table-wp table thead th {
                font-weight: normal;
                font-family: Roboto Condensed Bold;
            }
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Báo cáo &nbsp;&#187; </li>
                        <li class="be-report-li"><a href="javascript:void(0);"><i class="fa fa-th-large"></i>Báo cáo - Vận hành</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report" onload="abc()">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row">
                    <div class="filter-item" style="margin-left: 0;">
                        <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="datepicker-wp">
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                                ClientIDMode="Static" runat="server"></asp:TextBox>

                            <strong class="st-head" style="margin-left: 10px; margin-right: 10px;">
                                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                            </strong>
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeTo" placeholder="Đến ngày"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                        </div>
                        <% if (Perm_ViewAllDate)
                            { %>
                        <br />
                        <% }
                            else
                            { %>
                        <style>
                            .datepicker-wp {
                                display: none;
                            }

                            .customer-listing .tag-wp {
                                padding-left: 0;
                                padding-top: 15px;
                            }
                        </style>
                        <% } %>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="Salon" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                    </div>

                    <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" Text="Xem dữ liệu"
                        ClientIDMode="Static" onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <a href="/marketting/bao-cao" class="st-head btn-viewdata">Reset Filter</a>
                </div>
                <!-- End Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <asp:UpdatePanel ID="UPTotal" runat="server" ClientIDMode="Static" style="margin-top: 15px;">
                    <ContentTemplate>
                        <div class="row">
                            <strong class="st-head"><i class="fa fa-file-text"></i>Báo cáo marketting</strong>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <table class="table-add table-listing">
                                    <thead>
                                        <tr>
                                            <th rowspan="2">STT</th>
                                            <th rowspan="2">Salon</th>
                                            <th rowspan="2">Ngày chi</th>
                                            <th colspan="3">Chi phí marketting</th>
                                        </tr>
                                        <tr>
                                            <th>Tổng chi salon</th>
                                            <th>tổng chi phí chung</th>
                                            <th>Tổng chi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater runat="server" ClientIDMode="Static" ID="rptListReport">
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Eval("STT") %></td>
                                                    <td><%# Eval("Name") %></td>
                                                    <td><%# Eval("NgayChi") %></td>
                                                    <td><%# string.Format("{0:#,0.##}",Eval("trongngay")) %></td>
                                                    <td><%# string.Format("{0:#,0.##}",Eval("phanbo")) %></td>
                                                    <td>
                                                        <%# Eval("TongCong") %>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <asp:Repeater runat="server" ClientIDMode="Static" ID="rptTotal">
                                            <ItemTemplate>
                                                <tr>
                                                    <th>Tổng</th>
                                                    <th colspan="2"><%# Eval("Name") %></th>

                                                    <th><%# string.Format("{0:#,0.##}",Eval("TongCong")) %></th>
                                                    <th><%# string.Format("{0:#,0.##}",Eval("Chiphichung")) %></th>
                                                    <th>
                                                        <%# string.Format("{0:#,0.##}",Eval("Tongchiphi")) %>
                                                    </th>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-xs-6">
                                <table class="table-add table-listing">
                                    <thead>
                                        <tr>
                                            <th>Item</th>
                                            <th>Giá trị</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater runat="server" ClientIDMode="Static" ID="rptItem">
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Eval("Item_Name") %></td>
                                                    <td><%# string.Format("{0:#,0.##}", Eval("GiaTri")) %></td>
                                                    <%--<td><%# string.Format("{0:dd/MM/yyyy}",Eval("NgayChi")) %></td>--%>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <tr>
                                            <th>Tổng</th>
                                            <th><%= string.Format("{0:#,0.##}",total) %></th>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>

                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="IsPaging" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_ExcelType" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELPage" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELSegment" />
            </div>
            <%-- END Listing --%>
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 180px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                // Add active menu
                $("#glbAdminSales").addClass("active");
                $("#glbAdminReportSales").addClass("active");
                $("li.be-report-li").addClass("active");

                // Price format
                UP_FormatPrice('.be-report-price');

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        //$input.val($input.val().split(' ')[0]);
                    }
                });

                // View data today
                //$(".tag-date-today").click();
            });

        </script>

    </asp:Panel>
</asp:Content>
