﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Script.Serialization;
using ExportToExcel;
using Excel;
using LinqKit;
using System.Net.Mail;
using System.Net;
using System.Data.SqlClient;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.MarKeting.Report
{
    public partial class Marketting_Report : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        CultureInfo culture = new CultureInfo("vi-VN");

        private string PageID = "MKT_CP";
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;
        public totalItem totalItem;
        public double total { get; set; }

        private Solution_30shineEntities db;

        public Marketting_Report()
        {
            db = new Solution_30shineEntities();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
            }
            else
            {
                RemoveLoading();
            }
        }
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //Perm_ViewAllDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDate", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ViewAllDate = permissionModel.CheckPermisionByAction("Perm_ViewAllDate", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
    

        public void LoadData()
        {
            bool setDefaultTime = false;
            DateTime timeFrom = new DateTime();
            DateTime timeTo = new DateTime();
            if (TxtDateTimeFrom.Text != "")
            {
                timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
            }
            else
            {
                timeFrom = Convert.ToDateTime("1/1/2016", culture);
                setDefaultTime = true;
            }
            if (TxtDateTimeTo.Text != "")
            {
                timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
            }
            else
            {
                if (setDefaultTime)
                {
                    timeTo = DateTime.Now;
                }
                else
                {
                    timeTo = timeFrom;
                }
            }
            int salonId = Salon.SelectedValue != "" ? Convert.ToInt32(Salon.SelectedValue) : 0;

            rptListReport.DataSource = db.Store_Marketting_BaoCao(timeFrom, timeTo, salonId).ToList();
            rptListReport.DataBind();
            var tongchi = db.Store_Marketting_BaoCao_Total(timeFrom, timeTo, salonId).ToList();
            rptTotal.DataSource = tongchi;
            rptTotal.DataBind();

            var data = db.Store_Marketting_ChiPhiItem(timeFrom, timeTo, salonId).ToList();
            rptItem.DataSource = data;
            rptItem.DataBind();
            total = Convert.ToDouble(data.Sum(p => p.GiaTri));

        }

        protected void _BtnClick(Object sender, EventArgs e)
        {
            LoadData();
        }

        public totalItem getTotalItem(List<Store_Marketting_ChiPhiItem_Result> list)
        {
            var total = new totalItem();
            if (list.Count() > 0)
            {
                foreach (var item in list)
                {
                    total.GiaTri += Convert.ToDouble(item.GiaTri);
                }
            }
            return total;
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        
    }
    public class totalItem
    {
        public double GiaTri { get; set; }
    }
}
