﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="MarKeting_ChienDich.aspx.cs" Inherits="_30shine.GUI.BackEnd.MarKeting.MarKeting_ChienDich" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .edit {
                margin: 8px;
                width: 15px;
            }

            .del {
                margin: 8px;
                width: 15px;
            }

                .del:hover {
                    color: red;
                }

            .edit:hover {
                color: seagreen;
            }
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý chi tiêu chiến dịch &nbsp;&#187; </li>
                        <li class="li-listing" style="cursor: pointer"><a onclick="show()">Thêm chiến dịch</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing be-report woktime-listing">
            <%-- Listing --%>

            <div class="wp960 content-wp">
                <div class="row" id="ql" hidden="hidden">
                    <ul class="form-group">
                        <li class="table table-add">
                            <div class="col-md-5 col-xs-12" style="padding-top: 10px;">
                                <label style="float: left; width: 20%;">Tiêu đề :</label>
                                <input type="text" id="tieude" style="width: 75%;margin-left: 0px !important;" class="form-control" />
                            </div>
                            <div class="col-md-5 col-xs-12" style="padding-top: 10px;">
                                <label style="float: left; width: 22%">Chi Phí :</label>
                                <input type="number" id="chiphi" style="width: 75%;" class="form-control" />
                            </div>
                            <div class="col-md-5 col-xs-12" style="padding-top: 10px;">
                                <label style="float: left; width: 20%;">Ngày bắt đầu :</label>
                                <asp:TextBox CssClass="txtDateTime form-control" ID="txtDateTimeto" placeholder="Chọn ngày" Style="width: 75% !important;margin-left: 0px !important;"
                                    ClientIDMode="Static" runat="server"></asp:TextBox>
                            </div>
                            <div class="col-md-5 col-xs-12" style="padding-top: 10px;">
                                <label style="float: left; width: 20%">Ngày kết thúc :</label>
                                <asp:TextBox CssClass="txtDateTime form-control" ID="TxtDateTimeFrom" placeholder="Chọn ngày" Style="width: 75% !important;"
                                    ClientIDMode="Static" runat="server"></asp:TextBox>
                            </div>
                            <div class="col-md-5 col-xs-12" style="padding-top: 10px;">
                                <label style="float: left; width: 20%">Salon :</label>
                                <asp:DropDownList ID="ddlSalon" runat="server" ClientIDMode="Static" CssClass="form-control select" Width="75%"></asp:DropDownList>
                            </div>

                            <div class="col-md-12 col-xs-12" style="padding-top: 10px;">
                                <a class="btn btn-primary" style="margin-left: 9%; margin-right: 10px; width: 100px;" onclick="save($(this))" data-staff="<%=staff_id %>">Lưu lại</a>
                                <a class="btn btn-default" style="margin-left: 10px; margin-right: 10px; width: 100px;" onclick="huy()">Hủy</a>
                            </div>

                        </li>
                    </ul>
                </div>
            </div>


            <div class="col-md-12 col-xs-12">
                <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Danh sách mục chi chiến dịch</strong>
                <table class="table-add table-listing">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tiêu đề chiến dịch</th>
                            <th>Ngày bắt đầu</th>
                            <th>Ngày kêt thúc</th>
                            <th>Chi phí</th>
                            <th>Salon</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <%if (obj.Count > 0)
                            { %>
                        <%int i = 1; foreach (var v in obj)
                            { %>
                        <tr>
                            <td><%= i++ %></td>
                            <td><%=v.TieuDe %></td>
                            <td><%= string.Format("{0:dd/MM/yyyy}",v.NgayBatDau) %></td>
                            <td><%= string.Format("{0:dd/MM/yyyy}",v.NgayKetThuc) %></td>
                            <td><%= string.Format("{0:#,0.##}",v.ChiPhi) %></td>
                            <td><%= v.Name %></td>
                            <td>
                                <div>
                                    <a onclick="edit($(this) ,<%=v.Id %>)" data-tieude="<%=v.TieuDe %>"
                                        data-ngaybatdau="<%= string.Format("{0:dd/MM/yyyy}", v.NgayBatDau) %>"
                                        data-ngayketthuc="<%=  string.Format("{0:dd/MM/yyyy}",v.NgayKetThuc) %>"
                                        data-chiphi="<%=v.ChiPhi %>" data-salon="<%= v.SalonId %>"
                                        id="sua" class="fa fa-pencil-square-o edit"></a>
                                    <a onclick="deletel($(this),<%=v.Id %>)" id="xoa" class="fa fa-times del"></a>
                                </div>
                            </td>
                        </tr>
                        <%} %>
                        <%} %>
                    </tbody>
                </table>
            </div>
        </div>
    </asp:Panel>
    <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
    <script src="../../../Assets/js/select2/select2.min.js"></script>
    <script>
        $('.select').select2();
    </script>
    <style>
        .select2-container {
            width: 75% !important;
            margin-top: 5px !important;
        }

        .select2-container--default .select2-selection--single {
            height: 32px !important;
            padding-top: 1px !important;
        }
    </style>
    <script>
        var id = 0;
        $('.txtDateTime').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'vi',
            startDate: '2014/10/10',
            format: 'd/m/Y',
            dateonly: true,
            showHour: false,
            showMinute: false,
            timepicker: false,
            onChangeDateTime: function (dp, $input) {
                //$input.val($input.val().split(' ')[0]);
            }
        });

        function save(This) {
            var Id = id;
            //var staff_id = This.attr("data-staff");
            var Tieude = $("#tieude").val();
            if (Tieude == "") {
                alert("Bạn phải nhập tiêu đề chiến dịch");
                return;
            }
            var Ngaybatdau = $("#txtDateTimeto").val();
            if (Ngaybatdau == "") {
                alert("Bạn phải chọn ngày bắt đầu");
                return;
            }
            ngayketthuc = $("#TxtDateTimeFrom").val();

            if (ngayketthuc == "") {
                alert("Bạn phải chọn ngày kết thúc");
                return;
            }
            var D1 = new Date(Ngaybatdau);
            var D2 = new Date(ngayketthuc);
            if (D2.getTime() < D1.getTime()) {
                alert("Ngày bắt đầu phải Nhỏ hơn ngày kết thúc!");
                return;
            }

            var chiphi = $("#chiphi").val();
            if (chiphi == "") {
                alert("Bạn phải chọn ngày chi");
                return;
            }
            if (chiphi.length > 20) {
                alert("Chi phí chiến dịch không vượt quá 20 chữ số !");
                return;
            }

            var salonId = $("#ddlSalon :selected").val();
            if (salonId == 0) {
                alert("Chưa chọn Salon !");
                return;
            }

            $.ajax({
                type: "POST",
                url: "/GUI/BackEnd/MarKeting/MarKeting_ChienDich.aspx/Save",
                data: '{Id:' + Id + ', tieude : "' + Tieude + '", ngaybatdau:"' + Ngaybatdau + '", ngayketthuc:"' + ngayketthuc
                    + '", chiphi:"' + chiphi + '", SalonId:"' + salonId + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    console.log(response.d.data);
                    alert(response.d.message);
                    window.location.reload();
                },
                failure: function (response) { alert(response.d); }
            });

        }
        function edit(This, Id) {
            $("#ql").show();
            id = Id;
            $("#tieude").val(This.attr("data-tieude"));
            $("#txtDateTimeto").val(This.attr("data-ngaybatdau"));
            $("#TxtDateTimeFrom").val(This.attr("data-ngayketthuc"));
            $("#chiphi").val(This.attr("data-chiphi"));
            $("#ddlSalon").val(This.attr("data-salon"));
        }
        function deletel(This, Id) {
            var id = Id;

            console.log(id);
            //return;
            var r = confirm('Bạn có chắc chắn muốn xóa hay không');
            if (r) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/MarKeting/MarKeting_ChienDich.aspx/delete",
                    data: '{Id:' + id + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        console.log(response.d.data);
                        alert(response.d.message);
                        window.location.reload();
                    },
                    failure: function (response) { alert(response.d); }
                });
            }
        }
        function show() {
            $("#ql").show();
        }
        function huy() {
            $("#ql").hide();
        }
    </script>
</asp:Content>
