﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SurveyQuestionAdd.aspx.cs" MasterPageFile="~/TemplateMaster/SiteMaster.Master" Inherits="_30shine.GUI.BackEnd.MarKeting.Survey.SurveyQuestionAdd" %>

<asp:Content ID="ProductAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <link href="/Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="/Assets/js/select2/select2.min.js"></script>
        <style>
            .table-wp {
                background: #f5f5f5;
                float: left;
                width: 100%;
                padding: 15px;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                -webkit-box-shadow: 0px 0px 3px 0px #ddd;
                -moz-box-shadow: 0px 0px 3px 0px #ddd;
                box-shadow: 0px 0px 3px 0px #ddd;
            }

            .tr-skill {
                display: none;
            }

            .tr-show {
                display: table-row;
            }

            .customer-add .table-add tr td.right .fb-cover-error {
                top: 0;
                right: auto;
            }

            #success-alert {
                top: 100px;
                right: 10px;
                position: fixed;
                width: 20% !important;
                z-index: 2000;
            }

            .div_span span {
                margin-left: 2px !important;
            }

            #upload-img {
                width: 80px !important;
                background: #000000;
                color: #ffffff;
                height: 30px;
                line-height: 15px;
                float: left;
                text-align: center;
                cursor: pointer;
                -webkit-border-radius: 2px;
                -moz-border-radius: 2px;
                border-radius: 2px
            }

            #upload-image {
                /*margin: 0 auto;*/
                margin-top: 20px;
                border: 2px #cccccc;
                border-style: dashed;
                width: 49%;
                height: 250px;
                border-radius: 3px;
                background-color: ghostwhite;
                /* text-align: center; */
            }

                #upload-image #result div .close-img span {
                    width: 25px;
                    height: 20px;
                    border: 1px solid #ccc;
                    text-align: center;
                    padding-bottom: 21px;
                    border-radius: 15px;
                    background: #fff;
                    position: absolute;
                    margin-left: 5px;
                    top: 10px;
                    left: 5px;
                    z-index: 1000;
                    line-height: 20px !important;
                }
        </style>
        <%--<div class="alert alert-danger" id="success-alert">
            <strong>Cảnh báo: </strong>
            <span id="msg-alert"></span>
        </div>--%>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Khảo sát &nbsp;&#187; </li>
                        <li class="li-add active"><a href="<%=Code != "" ? "/admin/marketing/tao-survey.html" : "#" %>"">Thêm mới </a></li>
                        <li class="li-listing"><a href="/admin/marketing/survey-listing.html">Danh sách câu hỏi</a></li>
                        
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add admin-product-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="table-wp">
                    <table class="table-add admin-product-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Nhập thông tin khảo sát</strong></td>
                                <td></td>
                            </tr>

                            <tr class="tr-margin" style="height: 20px;">
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Loại câu hỏi</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <select class="form-control" id="question-type" onchange="ShowAnswer($(this))"></select>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Câu hỏi</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <input type="text" class="form-control" id="question" placeholder="Nhập câu hỏi" />
                                        </div>
                                    </div>

                                </td>
                            </tr>

                            <tr id="answers" style="display: none">
                                <td class="col-xs-3 left"><span>Trả lời</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp" style="width: 49%">
                                        <div class="div_col">
                                            <asp:TextBox ID="Answer_1" runat="server" CssClass="form-control answer" ClientIDMode="Static" placeholder="..." Style="margin-top: 10px;"></asp:TextBox>
                                        </div>
                                        <div class="div_col">
                                            <asp:TextBox ID="Answer_2" runat="server" CssClass="form-control answer" ClientIDMode="Static" placeholder="..." Style="margin-top: 10px;"></asp:TextBox>
                                        </div>
                                        <div class="div_col">
                                            <asp:TextBox ID="Answer_3" runat="server" CssClass="form-control answer" ClientIDMode="Static" placeholder="..." Style="margin-top: 10px;"></asp:TextBox>
                                        </div>
                                        <div class="div_col">
                                            <asp:TextBox ID="Answer_4" runat="server" CssClass="form-control answer" ClientIDMode="Static" placeholder="..." Style="margin-top: 10px;"></asp:TextBox>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Noti title</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <input type="text" class="form-control" id="noti-title" placeholder="Nhập tiêu đề" />
                                        </div>
                                    </div>

                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Noti content</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <textarea class="form-control" id="noti-content" placeholder="Nhập nội dung" row="3"></textarea>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Ngày BĐ</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <asp:TextBox ID="StartDate" runat="server" CssClass="form-control txtdatetime" ClientIDMode="Static" placeholder="Ngày bắt đầu khảo sát" Width="49%"></asp:TextBox>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Ngày KT</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <asp:TextBox ID="EndDate" CssClass="form-control txtdatetime" runat="server" ClientIDMode="Static" placeholder="Ngày kết thúc khảo sát" Width="49%"></asp:TextBox>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Đối tượng khảo sát</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <select id="survey-victim" class="form-control">
                                                <option value="0" selected="selected">Chọn đối tượng</option>
                                                <option value="1">Nhân viên</option>
                                                <option value="2">Khách hàng</option>
                                            </select>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Hình ảnh (<u style="color:red">Tỉ lệ 21:9</u>)</span></td>
                                <td class="col-xs-9 right">
                                    <button type="button" class="form-control btn-send" id="upload-img">Upload</button>
                                </td>
                            </tr>
                            <tr class="tr-upload">
                                <td class="col-xs-3 left"></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="col-xs-auto wr-search position-relative">
                                            <div class=" input full-width" id="upload-image">
                                                <div>
                                                    <input type="hidden" id="img-url" />
                                                    <input class="hide-1" style="display: none" id="files" type="file" />
                                                </div>
                                                <output id="result" />
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="tr-send">
                                <td class="col-xs-3 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <button type="button" onclick="Insert()" id="send" class="btn-send" style="border: none; line-height: 25px;"><%=Code == "" ? "Thêm mới" : "Cập nhật" %></button>
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <%-- end Add --%>
           
        </div>
        <script type="text/javascript" src="/Assets/js/parse_date.js"></script>
        <script src="/Assets/js/erp.30shine.com/monitor-staff/config.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.13.5/xlsx.full.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.13.5/jszip.js"></script>
        <script type="text/javascript">
            var domain = '<%=Libraries.AppConstants.URL_API_NOTIFICATION%>';
            var userIds = "";
            var code = "";
            var todayDate = new Date().getDate();
            jQuery(document).ready(function () {
                code = "<%=Code%>";
                //date picker
                $('.txtdatetime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014-10-10',
                    format: 'd-m-Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                    }
                });
                $("#EndDate").datetimepicker({
                    maxDate: new Date(new Date().setDate(todayDate + 30)),
                })
                BindImage();
                BindQuestionType();
                if (code != "") {
                    GetDetailQuestion();
                }
            });
            $("#upload-img").click(function () {
                //$("#files").val("");
                $("#files").click();

            });

            function BindImage() {
                //Check File API support
                if (window.File && window.FileList && window.FileReader) {
                    $('#files').on("change", function (event) {
                        var files = event.target.files; //FileList object
                        $("#result").empty();
                        var output = document.getElementById("result");
                        for (var i = 0; i < files.length; i++) {
                            var file = files[i];
                            //Only pics
                            if (file.type.match('image.*')) {
                                if (this.files[0].size > 2097152) {
                                }
                                // continue;
                                var picReader = new FileReader();
                                picReader.addEventListener("load", function (event) {
                                    var picFile = event.target;
                                    var imageData = picFile.result;
                                    // console.log(imageData);

                                    var div = document.createElement("div");
                                    div.innerHTML = "<img class='thumbnail' width='300px' style='max-height:248px' src='" + imageData + "'" +
                                        "title='preview image'/><div class='close-img'><span class='close-imgs' onclick='CloseImage($(this))'>x</span></div";
                                    output.insertBefore(div, null);
                                });
                                //Read the image
                                $('#result').show();
                                picReader.readAsDataURL(file);

                            } else {
                                alert("You can only upload image file.");
                                $(this).val("");
                            }
                        }

                    });
                } else {
                    console.log("Your browser does not support File API");
                }
            };
            //s3
            function uploadImagesToS3(data) {
                let result = '';
                let url = window.apis.monitorStaffService.domainS3 + "/api/s3/multiUpload/";
                $.ajax({
                    contentType: "application/json",
                    type: "POST",
                    url: url,
                    dataType: "json",
                    async: false,
                    crossDomain: true,
                    data: JSON.stringify(data),
                    success: function (response, textStatus, xhr) {
                        if (response) {
                            result = response;
                        }
                    },
                    error: function (jqXhr, textStatus, errorMessager) {
                        console.log(errorMessager + "\n" + jqXhr.responseJSON);
                        alert("upload images lên S3 bị lỗi. Bạn hãy thông báo với Admin");
                    }
                });
                console.log(result);
                return result;
            };
            function CloseImage(This) {
                var pr = This.parent().parent();
                pr.remove();
                $("#files").val("");

            }

            function BindQuestionType() {

                $.ajax({
                    url: domain + "/api/survey/question-type",
                    type: "GET",
                    dataType: "json",
                    contentType: "application/json;charset:utf-8",
                    success: function (data) {
                        console.log(data);
                        var option = "<option value='0'>Chọn loại câu hỏi</option>";
                        for (var i = 0; i < data.length; i++) {
                            option += "<option value='" + data[i].questionType + "'>" + data[i].questionTypeName + "</option>";
                        }
                        $("#question-type").empty().append(option);
                    }
                })
            }
            function ShowAnswer(This) {
                if (This.val() === "3") {
                    $("#answers").removeAttr("style");
                }
                else {
                    $("#answers").css("display", "none");
                }
            }
            function ValidateForm() {
                var valid = true;
                var msg = "";
                var questionType = $("#question-type").val();
                var question = $("#question").val();
                var startDate = $("#StartDate").val();
                var endDate = $("#EndDate").val();
                var surveyVictim = $("#survey-victim").val();
                var _startDate = moment($("#StartDate").val(), "DD-MM-YYYY").toDate();
                var _endDate = moment($("#EndDate").val(), "DD-MM-YYYY").toDate();
                var answer_1 = $("#Answer_1").val();
                var answer_2 = $("#Answer_2").val();
                var answer_3 = $("#Answer_3").val();
                var answer_4 = $("#Answer_4").val();
                var notiTitle = $("#noti-title").val();
                var notiContent = $("#noti-content").val();
                if (questionType === "0") {
                    valid = false;
                    msg += "Bạn chưa chọn loại câu hỏi khảo sát. ";
                }
                if (question === "") {
                    valid = false;
                    msg += "Nội dung câu hỏi không được để trống. ";
                }
                if (startDate === "") {
                    valid = false;
                    msg += "Ngày bắt đầu không được để trống. ";
                }
                if (endDate === "") {
                    valid = false;
                    msg += "Ngày kết thúc không được để trống. ";
                }
                if (_startDate > _endDate) {
                    valid = false;
                    msg += "Ngày kết thúc không được nhỏ hơn ngày bắt đầu. ";
                }
                if (_endDate > new Date(new Date().setDate(todayDate + 30))) {
                    valid = false;
                    msg += "Ngày kết thúc không vượt quá 30 ngày tính từ ngày hiện tại. ";
                }
                if (surveyVictim === "0") {
                    valid = false;
                    msg += "Bạn chưa chọn đối tượng khảo sát. ";
                }
                if (questionType === "3" && (answer_1 === "" || answer_2 === "" || answer_3 === ""|| answer_4 === "")) {
                    valid = false;
                    msg += "Bạn chưa nhập câu trả lời. ";
                }
                if (notiTitle === "") {
                    valid = false;
                    msg += "Bạn chưa nhập tiêu đề cho tin nhắn. ";
                }
                if (notiTitle.length > 45) {
                    valid = false;
                    msg += "Tiêu đề tin nhắn không được phép lớn hơn 45 kí tự. ";
                }
                if (notiContent=== "") {
                    valid = false;
                    msg += "Bạn chưa nhập nội dung cho tin nhắn. ";
                }
                if (notiContent.length > 75) {
                    valid = false;
                    msg += "Nội dung tin nhắn không được lớn hơn 75 kí tự. ";
                }
                if (!valid) {
                    ShowMessage("Thông báo", msg, 4,5000);
                    return false;
                }
                else {
                    return true;
                }
            }
            function Insert() {
                var _url = "";
                var _type = "";
                var msg = "";
                if (!ValidateForm()) {
                    return;
                }
                var type = $("#question-type").val();
                var question = $("#question").val();
                var startDate = $("#StartDate").val();
                var endDate = $("#EndDate").val();
                var victim = $("#survey-victim").val();
                var notiTitle = $("#noti-title").val();
                var notiContent = $("#noti-content").val();
                var answers = [];
                $(".answer").each(function (i, v) {
                    var item = $(v).val();
                    if (item != "") {
                        answers.push({ "answer": item });
                    }
                });
                let data = {
                    "question": question,
                    "questionType": parseInt(type),
                    "startDate": startDate,
                    "endDate": endDate,
                    "userType": parseInt(victim),
                    "answers": answers,
                    "image": "",
                    "notiTitle": notiTitle,
                    "notiContent": notiContent
                };
                let arrImage = [];
                if ($("#img-url").val() != $('#result img').attr("src")) {
                    $('#result img').each(function (i, v) {
                        let base64 = $(v).attr('src');
                        let item = {
                            img_name: "SurveyImage" + Date.now(),
                            img_base64: base64
                        };
                        arrImage.push(item);
                    });
                    let dataImages = {
                        images: arrImage
                    };
                    //post to s3
                    if (arrImage.length > 0) {
                        let resultUpload = uploadImagesToS3(dataImages);
                        let arrLinks = [];
                        $.each(resultUpload.images, function (i, v) {
                            let link = v.link;
                            if (link) {
                                arrLinks.push(link);
                            }
                        });
                        //set data images
                        data.image = arrLinks.join();
                    }
                }
                else {
                    data.image = $("#img-url").val();
                }
                console.log(data);
                if (code == "") {
                    _url = domain + "/api/survey";
                    _type = "POST";
                    msg = "Thêm mới câu hỏi khảo sát thành công";
                    
                }
                else {
                    _url = domain + "/api/survey/"+code;
                    _type = "PUT";
                    msg = "Cập nhật câu hỏi khảo sát thành công";
                }
                $.ajax({
                    url: _url,
                    type: _type,
                    dataType: "json",
                    contentType: "application/json;charset:utf-8",
                    data: JSON.stringify(data),
                    success: function (data) {
                    },
                    complete: function (data) {
                        //UploadUsers(data.id);
                        ShowMessage("Thông báo", msg, 1,5000);
                        $("#question-type").val(0);
                        $("#question").val("");
                        $("#survey-victim").val(0);
                        $("#noti-title").val("");
                        $("#noti-content").val("");
                        $("#Answer_1").val("");
                        $("#Answer_2").val("");
                        $("#Answer_3").val("");
                        $("#Answer_4").val("");
                        $("#result").empty();
                    }
                })
            }

            function GetDetailQuestion() {
                $.ajax({
                    url: domain + "/api/survey/" + code,
                    type: "GET",
                    contentType: "application/json;charset:utf-8",
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        //debugger;
                        $("#question-type").val(data.type);
                        $("#question").val(data.question);
                        $("#StartDate").val(data.startDate);
                        $("#EndDate").val(data.endDate);
                        $("#survey-victim").val(data.userType);
                        $("#noti-title").val(data.notiTitle != null ? data.notiTitle : "");
                        $("#noti-content").val(data.notiContent != null ? data.notiContent : "");
                        
                        if (data.image != null) {
                            $("#img-url").val(data.image);
                            var div = document.createElement("div");
                            var img = "<img class='thumbnail' width='300px' style='max-height:248px' src='" + data.image + "'" +
                                "title='preview image'/><div class='close-img'><span class='close-imgs' onclick='CloseImage($(this))'>x</span></div";
                            $("#result").empty().append(div);
                            $("#result div").empty().append(img);
                        }
                        if (data.type === 3) {
                            $("#answers").removeAttr("style");
                            var number = 1;
                            for (var i = 0; i < data.answers.length; i++) {
                                $("#Answer_" + number++).val(data.answers[i].answer);
                            }
                        }
                    }
                })
            }
        </script>
    </asp:Panel>
</asp:Content>
