﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.MarKeting.Survey
{
    public partial class SurveyQuestionAdd : System.Web.UI.Page
    {
       
        protected string Code="";
        private bool Perm_Access = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                if (Request.QueryString["Code"] != null)
                {
                    Code = Request.QueryString["Code"].ToString();
                }
                StartDate.Text = string.Format("{0:dd-MM-yyyy}", Convert.ToDateTime(DateTime.Now));
                EndDate.Text = string.Format("{0:dd-MM-yyyy}", Convert.ToDateTime(DateTime.Now.AddDays(7)));
                //BindType();
            }
        }
        /// <summary>
        /// check permission
        /// </summary>
        private void SetPermission()
        {
            if (Session["User_Id"] == null)
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string pageId = "";
                IPermissionModel permissionModel = new PermissionModel();
                if (Request.QueryString["Code"] != null)
                {
                    pageId = Request.RawUrl.Substring(0, Request.RawUrl.Length - 41);
                    pageId = pageId + "edit.html";
                }
                else
                {
                    pageId =permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit");
                }
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }

        }
        /// <summary>
        /// Executeby permission
        /// </summary>
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
    }
}