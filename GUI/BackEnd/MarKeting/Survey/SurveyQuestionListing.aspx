﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SurveyQuestionListing.aspx.cs" MasterPageFile="~/TemplateMaster/SiteMasterVersion1.Master" Inherits="_30shine.GUI.BackEnd.MarKeting.Survey.SurveyQuestionListing" %>

<asp:Content ID="CustomerAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel ID="ContentWrap" runat="server" ClientIDMode="Static">

        <div class="container-fluid sub-menu">
            <div class="row">
                <div class="col-md-12 pt-3 pb-2">
                    <ul>
                        <li>
                            <label style="font-size: 16px">Khảo sát </label>
                            &nbsp;&#187;</li>
                        <li><i class="fas fa-list-alt mr-1 align-middle"></i><a class="align-middle" href="/admin/marketing/tao-survey.html">Thêm mới</a></li>
                        <li><i class="fas fa-list-alt mr-1 align-middle"></i><a class="align-middle" href="#">Danh sách câu hỏi</a></li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <div class="sub-menu-boder"></div>
                </div>
            </div>
        </div>
        <form runat="server">
            <div class="container-fluid pt-2 form-group">
                <div class="row">
                    <div class="form-group col-auto">
                        <strong><i class="fa fa-filter"></i>Lọc kết quả &nbsp;&#187; </strong>
                    </div>

                    <div class="col-auto">
                        <div class="form-group search-content">
                            <input type="text" placeholder="Từ ngày" id="time-from" name="fromDate" class="txtDateTime form-control float-left w-auto" />
                            <i class="fa fa-arrow-circle-right float-left mt-2 mr-2 ml-2" style="color: #D0D6D8;"></i>
                            <input type="text" placeholder="Đến ngày" id="time-to" name="toDate" class="txtDateTime form-control float-left w-auto" />
                        </div>
                    </div>
                    <div class="form-group col-auto">
                        <div id="ViewDataFilter" class="form-group col-auto pl-0" onclick="BindData()">
                            <div class="btn-viewdata">Xem dữ liệu</div>
                        </div>
                    </div>
                    <div class="col-md-12 col-xl-12">
                        <div class="sub-menu-boder-low"></div>
                    </div>
                </div>
            </div>
            <div class="container-fluid form-group">
                <table id="table-listing" class="table table-bordered table-listing table-survey">
                    <thead>
                        <tr>
                            <th class="align-middle text-center">ID Câu hỏi</th>
                            <th class="align-middle text-center">Nội dung KS</th>
                            <th class="align-middle text-center">Loại câu hỏi</th>
                            <th class="align-middle text-center">Ngày bắt đầu</th>
                            <th class="align-middle text-center">Ngày kết thúc</th>
                            <th class="align-middle text-center">Hình ảnh</th>
                            <th class="align-middle text-center">Đối tượng KS</th>
                            <th style="width: 8%; text-align: center">Hành động</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

        </form>
        <%--<div class="add-loading" style="display:none;"><i class="fa fa-spinner fa-5x"></i></div>--%>
        <style>
            .form-control {
                height: 31px !important;
                width: 150px !important;
            }

            .btn-action {
                border: none;
                background: none;
                color: darkgoldenrod;
            }

                .btn-action:hover {
                    color: firebrick;
                }

                .btn-action:active {
                    border: none;
                    color: firebrick;
                }
        </style>
        <script>
            var domain = "<%=Libraries.AppConstants.URL_API_NOTIFICATION%>";
            var todayDate = new Date().getDate();
            $(document).ready(function () {

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    //startDate: '2014-10-10',
                    format: 'd-m-Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,

                    onChangeDateTime: function (dp, $input) { }
                });
                $("#time-from").val("<%=FirstDayOfMonth%>");
                $("#time-to").val("<%=Today%>");
            })
            function BindData() {
                var from = $("#time-from").val();
                var to = $("#time-to").val();
                $(".table-survey").DataTable().clear();
                $(".table-survey").DataTable().destroy();
                AddNewLoading();
                $.ajax({
                    url: domain + "/api/survey/list-question?startDate=" + from + "&endDate=" + to,
                    type: "GET",
                    dataType: "json",
                    contentType: "application/json;charset:utf-8",
                    success: function (data) {
                        if (data != undefined) {
                            var tbody = "";
                            for (var i = 0; i < data.length; i++) {
                                tbody += "<tr>" +
                                    "<td class='align-middle text-center'>" + data[i].id + "</td>" +
                                    "<td class='align-middle text-center'>" + data[i].question + "</td>" +
                                    "<td class='align-middle text-center'>" + data[i].typeName + "</td>" +
                                    "<td class='align-middle text-center'>" + data[i].startDate + "</td>" +
                                    "<td class='align-middle text-center'>" + data[i].endDate + "</td>" +
                                    "<td class='align-middle text-center'>" + (data[i].image != null && data[i].image != "string" ? "<img width='100px' height='100px' src='" + data[i].image + "'>" : "") + "</td>" +
                                    "<td class='align-middle text-center'>" + (data[i].userType === 1 ? 'Nhân viên' : 'Khách hàng') + "</td>" +
                                    "<td style='width:8%' class='align-middle text-center td-action'><a style='color:#007bff !important; margin-right: 10px;' title='Sửa' href='/admin/marketing/edit-survey/" + data[i].id + ".html'><i class='fas fa-edit'></i></a>" +
                                    "<button type='button' data-id='" + data[i].id + "' class='btn-action' title='Xóa' onclick='DeleteRecord($(this))'><i class='fas fa-trash-alt'></i></button>" +
                                    "<a style='color:#801515 !important;;margin-left: 10px;' href='/admin/marketing/survey-add-user/" + data[i].id + ".html' title='Add Users'><i class='fas fa-user-plus'></i></a></td > " +
                                    "</tr>";
                            }
                            $(".table-survey tbody").append(tbody);
                            $('.table-survey').DataTable({
                                "language": {
                                    "lengthMenu": "Hiển thị _MENU_ bản ghi",
                                    "zeroRecords": "Không có dữ liệu",
                                    "info": "Hiển thị trang _PAGE_ trên _PAGES_",
                                    "infoEmpty": "Không có dữ liệu",
                                    //"infoFiltered": "(filtered from _MAX_ total records)"
                                },
                                //serverSide: true,
                                //processing: true,
                                paging: true,
                                searching: false,
                                info: true,
                                ordering: false,

                            });
                        }
                    },
                    complete: function () {
                        RemoveNewLoading();
                    }
                })
            }
            function DeleteRecord(This) {
                var questionId = This.attr("data-id");
                var tr = This.closest("tr");
                var c = confirm("Bạn có muốn xóa bản ghi này??????");
                if (c) {
                    $.ajax({
                        url: domain + "/api/survey/" + questionId,
                        type: "DELETE",
                        dataType: "json",
                        contentType: "application/json;charset:utf-8",
                        success: function (data) {
                        },
                        complete: function (response) {
                            ShowMessage("Thông báo", "Xóa bản ghi thành công", 1);
                            tr.remove();
                        }
                    })
                }
                else {
                    return;
                }

            }
        </script>
    </asp:Panel>
</asp:Content>
