﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SurveyNoti.aspx.cs" MasterPageFile="~/TemplateMaster/SiteMasterVersion1.Master" Inherits="_30shine.GUI.BackEnd.MarKeting.Survey.SurveyNoti" %>

<asp:Content ID="CustomerAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel ID="ContentWrap" runat="server" ClientIDMode="Static">

        <div class="container-fluid sub-menu">
            <div class="row">
                <div class="col-md-12 pt-3 pb-2">
                    <ul>
                        <li>
                            <label style="font-size: 16px">Khảo sát </label>
                            &nbsp;&#187;</li>
                        <li><i class="fas fa-list-alt mr-1 align-middle"></i><a class="align-middle" href="/admin/marketing/tao-survey.html">Thêm mới</a></li>
                        <li><i class="fas fa-list-alt mr-1 align-middle"></i><a class="align-middle" href="/admin/marketing/survey-listing.html">Danh sách câu hỏi</a></li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <div class="sub-menu-boder"></div>
                </div>
            </div>
        </div>
        <form runat="server">
            <div class="container-fluid pt-2 form-group">
                <div class="row">
                    <div class="form-group col-auto">
                        <strong style="line-height: 33px;">DS Users &nbsp;&#187; </strong>
                    </div>

                    <div class="col-auto">
                        <div class="form-group search-content">
                            <input type="file" class="form-control" style="padding: 5px .75rem; font-size: 13px" id="excel-upload" />
                        </div>
                    </div>
                    <div class="form-group col-auto">
                        <a href="/TemplateFile/ImportExcelNotification/huong_dan_su_dung_gui_thong_bao_bang_excel_survey_noti.xlsx" id="download-file" style="text-decoration: underline; line-height: 33px">Tải file mẫu </a>
                    </div>
                    <div class="col-md-12 col-xl-12">
                        <div class="sub-menu-boder-low"></div>
                    </div>
                </div>
            </div>
            <div class="container-fluid form-group">
                <table id="table-listing" class="table table-bordered table-listing table-survey">
                    <thead>
                        <tr>
                            <th class="align-middle text-center">STT</th>
                            <th class="align-middle text-center">NOTI TITLE</th>
                            <th class="align-middle text-center">NOTI CONTENT</th>
                            <th class="align-middle text-center">USER ID</th>
                            <th class="align-middle text-center">USER TYPE</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <div class="row" style="width: 100%">
                    <button type="button" id="put-noti" onclick="UploadUsers()" class="btn-viewdata" style="width: 200px !important;">ADD USERS & PUT NOTI</button>
                </div>
            </div>


        </form>
        <style>
            .btn-action {
                border: none;
                background: none;
                color: darkgoldenrod;
            }

                .btn-action:hover {
                    color: firebrick;
                }

                .btn-action:active {
                    border: none;
                    color: firebrick;
                }

            #put-noti {
                height: 50px !important;
                margin-left: 15px !important;
                font-weight: 600;
            }

                #put-noti:hover {
                    color: #b7b910 !important;
                }
        </style>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.14.1/xlsx.full.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.14.1/jszip.js"></script>
        <script>
            var domain = "<%=Libraries.AppConstants.URL_API_NOTIFICATION%>";
            var code = "<%=code%>";
            let dataNoti = {
                "notiTitle": "",
                "notiContent": "",
                "userType": "",
            }
            $(document).ready(function () {
                GetQuestion();
            })
            function GetQuestion() {

                $.ajax({
                    url: domain + "/api/survey/" + code,
                    type: "GET",
                    dataType: "json",
                    contentType: "application/json;charset:utf-8",
                    success: function (response) {
                        console.log(response);
                        dataNoti.notiTitle = response.notiTitle;
                        dataNoti.notiContent = response.notiContent;
                        dataNoti.userType = response.userType == 1 ? "STAFF" : "CUSTOMER";
                    }
                })
            }

            function ReadExcel() {
                var fileUpload = $("#excel-upload")[0];
                //Validate whether File is valid Excel file.
                var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$/;
                if (regex.test(fileUpload.value.toLowerCase())) {
                    if (typeof (FileReader) != "undefined") {
                        var reader = new FileReader();

                        //For Browsers other than IE.
                        if (reader.readAsBinaryString) {
                            reader.onload = function (e) {
                                ProcessExcel(e.target.result);
                            };
                            reader.readAsBinaryString(fileUpload.files[0]);
                        } else {
                            //For IE Browser.
                            reader.onload = function (e) {
                                var data = "";
                                var bytes = new Uint8Array(e.target.result);
                                for (var i = 0; i < bytes.byteLength; i++) {
                                    data += String.fromCharCode(bytes[i]);
                                }
                                ProcessExcel(data);
                            };
                            reader.readAsArrayBuffer(fileUpload.files[0]);
                        }
                    } else {
                        alert("Trình duyệt của bạn không hỗ trợ HTML5.");
                        location.reload();
                    }
                } else {
                    alert("Định dạng file không chính xác. Vui lòng kiểm tra lại");
                    location.reload();
                }
            };

            function ProcessExcel(data) {
                $('.table-survey').DataTable().destroy();
                //$('.table-survey').DataTable().
                userIds = "";
                //Read the Excel File data.
                var workbook = XLSX.read(data, {
                    type: 'binary'
                });
                //Fetch the name of First Sheet.
                var firstSheet = workbook.SheetNames[0];
                //Read all rows from First Sheet into an JSON array.
                var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[firstSheet]);
                //Add the header row.
                var value = "";
                var tbody = "";
                var number = 1;
                // //Add the data rows from Excel file.
                for (var i = 0; i < excelRows.length; i++) {
                    tbody += "<tr>";
                    tbody += "<td style='text-align:center'>" + number++ + "</td>";
                    tbody += "<td style='text-align:center'>" + dataNoti.notiTitle + "</td>";
                    tbody += "<td style='text-align:center'>" + dataNoti.notiContent + "</td>";
                    tbody += "<td style='text-align:center'>" + excelRows[i].UserId + "</td>";
                    tbody += "<td style='text-align:center'>" + dataNoti.userType + "</td>";
                    tbody += "</tr>";
                    value += excelRows[i].UserId + ",";
                }
                //console.log(value.slice(0, -1));
                userIds = value.slice(0, -1);
                //console.log(value.toString());
                $("#table-listing tbody").empty().append(tbody);
                $('.table-survey').DataTable({
                    "language": {
                        "lengthMenu": "Hiển thị _MENU_ bản ghi",
                        "zeroRecords": "Không có dữ liệu",
                        "info": "Hiển thị trang _PAGE_ trên _PAGES_",
                        "infoEmpty": "Không có dữ liệu",
                        //"infoFiltered": "(filtered from _MAX_ total records)"
                    },
                    paging: true,
                    searching: false,
                    info: true,
                    ordering: false,

                });
            };

            // Add your id of "File Input" 
            $('#excel-upload').change(function () {
                // Do something 
                ReadExcel();
                //console.log($("#users").val());
            });
            function UploadUsers() {
                if ($("#excel-upload").val() == null || $("#excel-upload").val() == "" || $("#excel-upload").val() == undefined) {
                    ShowMessage("Thông báo", "Vui lòng upload file excel", 4, 5000);
                    $("#excel-upload").focus();
                    $("#excel-upload").css("border-color", "pink");
                    return;
                }
                let data = {
                    "questionId": code,
                    "userIds": userIds
                }
                AddNewLoading();
                $.ajax({
                    url: domain + "/api/survey/user",
                    type: "POST",
                    contentType: "application/json;charset:utf-8",
                    dataType: "json",
                    data: JSON.stringify(data),
                    success: function (data) {
                        if (data.length > 0) {
                            CreateExcel(code,data);
                        }
                        console.log(JSON.stringify(data));
                        ShowMessage("Thông báo", "Thêm user thành công", 1, 5000);
                        $("#excel-upload").val("");
                        $("#table-listing tbody").empty();
                        
                        
                    },
                    complete: function (data) {
                        RemoveNewLoading();
                    }
                })
                //console.log(data);
            }
            function CreateExcel(questionId,data = null) {
                var xlsRows = data;
                var createXLSLFormatObj = [];

                /* XLS Head Columns */
                var xlsHeader = [ "KIỂU LỖI","USER ID"];
                createXLSLFormatObj.push(xlsHeader);
                $.each(xlsRows, function (index, value) {
                    var innerRowData = [];
                    $.each(value, function (ind, val) {
                        innerRowData.push(val);
                    });
                    createXLSLFormatObj.push(innerRowData);
                });

                /* File Name */
                var filename = "noti_errors_" + questionId + ".xlsx";

                /* Sheet Name */
                var ws_name = "Error 1";

                if (typeof console !== 'undefined') console.log(new Date());
                var wb = XLSX.utils.book_new(),
                    ws = XLSX.utils.aoa_to_sheet(createXLSLFormatObj);

                /* Add worksheet to workbook */
                XLSX.utils.book_append_sheet(wb, ws, ws_name);

                /* Write workbook and Download */
                if (typeof console !== 'undefined') console.log(new Date());
                XLSX.writeFile(wb, filename);
                if (typeof console !== 'undefined') console.log(new Date());
            }

        </script>
    </asp:Panel>
</asp:Content>
