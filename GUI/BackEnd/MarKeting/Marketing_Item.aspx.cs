﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Script.Serialization;
using ExportToExcel;
using Excel;
using LinqKit;
using System.Web.Services;
using System.Data.Entity.Migrations;
using Project.Model.Structure;

namespace _30shine.GUI.BackEnd.MarKeting
{
    public partial class Marketing_Item : System.Web.UI.Page
    {
        Solution_30shineEntities db = new Solution_30shineEntities();
        protected List<Marketing_ChiphiItem> item;
        protected void Page_Load(object sender, EventArgs e)
        {
            bindata();
        }
        public void bindata()
        {
            var msg = new CommonClass.cls_message();
            item = db.Marketing_ChiphiItem.Where(w => w.IsDelete == false).ToList();
            if (item != null)
            {
                msg.success = true;
                msg.message = "hiển thị thành công";
            }
            else
            {
                msg.success = false;
                msg.message = "lỗi !, Vui lòng liên hệ với nhà phát triển";
            }
        }

        [WebMethod]
        public static object Save(int Id, string TenItem)
        {
            using (var db = new Solution_30shineEntities())
            {
                var msg = new CommonClass.cls_message();
                if (TenItem != null && TenItem != "")
                {
                    if (Id <= 0)
                    {
                        var obj = new Marketing_ChiphiItem();
                        obj.Item_Name = TenItem;
                        obj.IsDelete = false;
                        obj.CreateTime = DateTime.Now;
                        db.Marketing_ChiphiItem.AddOrUpdate(obj);
                        db.SaveChanges();
                        msg.success = true;
                        msg.message = "Thêm thành công";
                        msg.data = obj;
                    }
                    else
                    {
                        var obj = db.Marketing_ChiphiItem.FirstOrDefault(w => w.Id == Id && w.IsDelete == false);
                        obj.Item_Name = TenItem;
                        obj.IsDelete = false;
                        obj.CreateTime = DateTime.Now;
                        db.Marketing_ChiphiItem.AddOrUpdate(obj);
                        db.SaveChanges();
                        msg.success = true;
                        msg.message = "Sửa thành công";
                        msg.data = obj;
                    }

                }
                else
                {
                    msg.success = false;
                    msg.message = "Lỗi!. vui lòng liên hệ với nhà phát triển để được giải đáp";
                }

                return msg;
            }
        }
        [WebMethod]
        public static object edit(int Id)
        {
            using (var db = new Solution_30shineEntities())
            {
                var msg = new CommonClass.cls_message();
                if (Id > 0)
                {
                    var obj = db.Marketing_ChiphiItem.FirstOrDefault(w => w.Id == Id && w.IsDelete == false);
                    msg.success = true;
                    msg.data = obj;
                    msg.message = "Load thành công";

                }
                else
                {
                    msg.success = false;
                    msg.message = "Lỗi!. vui lòng liên hệ với nhà phát triển để được giải đáp";
                }
                return msg;
            }
        }
        [WebMethod]
        public static object delete(int Id)
        {
            using (var db = new Solution_30shineEntities())
            {
                var msg = new CommonClass.cls_message();
                if(Id>0)
                {
                    var obj = db.Marketing_ChiphiItem.FirstOrDefault(w => w.Id == Id && w.IsDelete == false);
                    if(obj!=null)
                    {
                        obj.IsDelete = true;
                        obj.ModifiedTime = DateTime.Now;
                        db.Marketing_ChiphiItem.AddOrUpdate(obj);
                        db.SaveChanges();
                        msg.success = true;
                        msg.data = obj;
                        msg.message = "Xóa thành công";
                    }
                    else
                    {
                        msg.success = false;
                        msg.message = "Lỗi!. vui lòng liên hệ với nhà phát triển để được giải đáp";
                    }
                }
                else
                {
                    msg.success = false;
                    msg.message = "Lỗi!. vui lòng liên hệ với nhà phát triển để được giải đáp";
                }
                return msg;
            }
        }
    }
}
