﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Threading.Tasks;
using _30shine.Helpers.Http;
using System.Web.Script.Serialization;
using System.Net.Http;
using System.Net.Http.Headers;

namespace _30shine.GUI.BackEnd.MarKeting
{
    public partial class MarKeting_Campaign : System.Web.UI.Page
    {
        /*
         - Thêm mới chiến dịch
         - Author: QuynhDD
         - Create: 28/02/2017             
        */
        protected string _Code;
        protected bool _IsUpdate = false;
        private bool Perm_Access = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_Delete = false;
        protected bool Perm_Edit = false;
        protected List<Service> _ListService = new List<Service>();

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        /// <summary>
        /// ExcuteByPermission
        /// </summary>
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }

        /// <summary>
        /// pageLoad
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //SetPermission();
            if (!IsPostBack)
            {
                BindCampaignType();
                BindService();
                if (IsUpdate())
                {
                    Bind_OBJ();
                }
            }
        }

        /// <summary>
        /// Isupdate
        /// </summary>
        /// <returns></returns>
        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }


        /// <summary>
        ///  button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ExcAddOrUpdate(object sender, EventArgs e)
        {

            if (IsUpdate())
            {
                Update();
            }
            else
            {
                Add();
            }
        }

        /// <summary>
        /// Call api Insert MktCampaign
        /// </summary>
        /// <returns></returns>
        public void Add()
        {
            try
            {
                if (HDF_ServiceId.Value == "" || HDF_ServiceId.Value == "[]")
                {
                    TriggerJsMsgSystem_temp(this, "Chuỗi dịch vụ không hợp lệ", "msg-system warning", 15000);
                }
                else
                {
                    int interger = 0;
                    var data = new Request().RunPostAsync(
                         Libraries.AppConstants.URL_API_BILL_SERVICE + "/api/mkt-campaign/add",
                         new
                         {
                             name = Name.Text,
                             label = Label.Text,
                             startDate = !string.IsNullOrEmpty(StartDate.Text) ? Library.Format.getDateTimeFromString(StartDate.Text) : Convert.ToDateTime(null),
                             endDate = !string.IsNullOrEmpty(EndDate.Text) ? Library.Format.getDateTimeFromString(EndDate.Text) : Convert.ToDateTime(null),
                             maxUsage = MaxUse.Text,
                             description = Note.Text,
                             createdTime = DateTime.Now,
                             isActive = chkIsActive.Checked,
                             isDelete = 0,
                             type = int.TryParse(ddlCampaignType.SelectedValue, out interger) ? interger : 0,
                             isBookingPublish = chkIsBookingPublish.Checked,
                             image = HDF_MainImg.Value
                         }
                     ).Result;
                    if (data.IsSuccessStatusCode)
                    {
                        var result = data.Content.ReadAsStringAsync().Result;
                        var responseData = new JavaScriptSerializer().Deserialize<ResponseData>(result);
                        //check data trả về từ api
                        if (responseData.status == 1)
                        {
                            // get id bản ghi mới nhất vừa insert vào
                            var campaignId = GetbyId();
                            // get về chuỗi jSonservice
                            var inputJson = new JavaScriptSerializer().Deserialize<List<Input_Json>>(HDF_ServiceId.Value);
                            // call hàm AddCampaignService
                            var kq = AddCampaignService(campaignId, inputJson);
                            // Check data 
                            if (kq == 1)
                            {
                                var msg = "Thêm mới thành công!!";
                                var status = "msg-system warning";
                                UIHelpers.TriggerJsMsgSystem(Page, msg, status, 2000);
                                var MsgParam = new List<KeyValuePair<string, string>>();
                                MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                                MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Thêm mới thành công"));
                                UIHelpers.Redirect("/admin/marketing/danh-sach-chien-dich.html", MsgParam);
                            }
                            else
                            {
                                TriggerJsMsgSystem_temp(this, "Thêm mới dịch vụ thất bại!", "msg-system warning", 15000);
                            }
                        }
                        else
                        {
                            TriggerJsMsgSystem_temp(this, "Thêm mới chiến dịch thất bại!", "msg-system warning", 15000);
                        }
                    }
                    else
                    {
                        TriggerJsMsgSystem_temp(this, "Thêm mới chiến dịch thất bại!", "msg-system warning", 15000);
                    }
                }
            }
            catch (Exception e)
            {
                TriggerJsMsgSystem_temp(this, "Đã có lỗi xảy ra:" + e.Message.Replace(@"'", @"\'"), "msg-system warning", 15000);
            }
        }

        /// <summary>
        /// Update 
        /// </summary>
        public void Update()
        {
            try
            {
                if (HDF_ServiceId.Value == "" || HDF_ServiceId.Value == "[]")
                {
                    TriggerJsMsgSystem_temp(this, "Chuỗi dịch vụ không hợp lệ", "msg-system warning", 15000);
                }
                else
                {
                    int Id = 0;
                    int interger = 0;
                    if (int.TryParse(_Code, out Id))
                    {
                        var data = new Request().RunPutAsync(
                     Libraries.AppConstants.URL_API_BILL_SERVICE + "/api/mkt-campaign/update",
                     new
                     {
                         id = Id,
                         name = Name.Text,
                         label = Label.Text,
                         startDate = !string.IsNullOrEmpty(StartDate.Text) ? Library.Format.getDateTimeFromString(StartDate.Text) : Convert.ToDateTime(null),
                         endDate = !string.IsNullOrEmpty(EndDate.Text) ? Library.Format.getDateTimeFromString(EndDate.Text) : Convert.ToDateTime(null),
                         maxUsage = MaxUse.Text,
                         description = Note.Text,
                         modifiedTime = DateTime.Now,
                         isActive = chkIsActive.Checked,
                         isDelete = 0,
                         type = int.TryParse(ddlCampaignType.SelectedValue, out interger) ? interger : 0,
                         isBookingPublish = chkIsBookingPublish.Checked,
                         image = HDF_MainImg.Value
                     }).Result;
                        if (data.IsSuccessStatusCode)
                        {
                            var result = data.Content.ReadAsStringAsync().Result;
                            var responseData = new JavaScriptSerializer().Deserialize<ResponseData>(result);
                            //check data trả về từ api
                            if (responseData.status == 1)
                            {
                                // get về chuỗi jSonservice
                                var inputJson = new JavaScriptSerializer().Deserialize<List<Input_Json>>(HDF_ServiceId.Value);
                                // call hàm AddCampaignService
                                var kq = UpdateCampaignService(Id, inputJson);
                                // Check data 
                                if (kq == 1)
                                {
                                    var msg = "Cập nhật thành công!!";
                                    var status = "msg-system warning";
                                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 1000);
                                    var MsgParam = new List<KeyValuePair<string, string>>();
                                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công"));
                                    UIHelpers.Redirect("/admin/marketing/danh-sach-chien-dich.html", MsgParam);
                                }
                                else
                                {
                                    TriggerJsMsgSystem_temp(this, "Cập nhật dịch vụ thất bại", "msg-system warning", 15000);
                                }
                            }
                            else
                            {
                                TriggerJsMsgSystem_temp(this, "Cập nhật thất bại", "msg-system warning", 15000);
                            }
                        }
                        else
                        {
                            TriggerJsMsgSystem_temp(this, "Cập nhật thất bại", "msg-system warning", 15000);
                        }

                    }
                    else
                    {
                        TriggerJsMsgSystem_temp(this, "Không tìm được Id campaign để cập nhật", "msg-system warning", 15000);
                    }
                }
            }
            catch (Exception e)
            {
                TriggerJsMsgSystem_temp(this, "Đã có lỗi xảy ra:" + e.Message.Replace(@"'", @"\'"), "msg-system warning", 15000);
            }
        }

        /// <summary>
        /// add CampaignService
        /// </summary>
        /// <param name="campaignId"></param>
        /// <param name="jsonService"></param>
        public int AddCampaignService(int campaignId, List<Input_Json> jsonService)
        {
            int status = 0;
            try
            {
                // /api/mkt-campaign-service/add
                if (campaignId <= 0 || jsonService == null)
                {
                }
                else
                {
                    var request = new Request();
                    var data = request.RunPostAsync(
                        Libraries.AppConstants.URL_API_BILL_SERVICE + "/api/mkt-campaign-service/add",
                        new
                        {
                            campaignId,
                            jsonService
                        }
                    ).Result;

                    if (data.IsSuccessStatusCode)
                    {
                        var result = data.Content.ReadAsStringAsync().Result;
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        var responseData = serializer.Deserialize<ResponseData>(result);
                        if (responseData.status == 1)
                        {
                            status = responseData.status;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return status;
        }

        /// <summary>
        /// Update CampaignService
        /// </summary>
        /// <param name="campaignId"></param>
        /// <param name="jsonService"></param>
        public int UpdateCampaignService(int campaignId, List<Input_Json> jsonService)
        {
            int status = 0;
            try
            {
                if (campaignId <= 0 || jsonService == null)
                {
                    throw new Exception();
                }
                var request = new Request();
                var data = request.RunPutAsync(
                    Libraries.AppConstants.URL_API_BILL_SERVICE + "/api/mkt-campaign-service/update",
                    new
                    {
                        campaignId,
                        jsonService
                    }
                ).Result;
                //check data trả về từ api
                if (data.IsSuccessStatusCode)
                {
                    var result = data.Content.ReadAsStringAsync().Result;
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    var responseData = serializer.Deserialize<ResponseData>(result);
                    if (responseData.status == 1)
                    {
                        status = responseData.status;
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return status;

        }
        /// <summary>
        /// Bind list các loại campaign
        /// </summary>
        public void BindCampaignType()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var listCampaignType = db.Tbl_Config.Where(r => r.Key == "campaign_type").Select(r => new { Value = r.Value, Name = r.Label }).ToList();
                    var Key = 0;
                    ddlCampaignType.DataTextField = "Name";
                    ddlCampaignType.DataValueField = "Value";
                    ListItem item = new ListItem("Chọn loại code", "0");
                    ddlCampaignType.Items.Insert(0, item);
                    if (listCampaignType.Count > 0)
                    {
                        foreach (var v in listCampaignType)
                        {
                            Key++;
                            item = new ListItem(v.Name, v.Value.ToString());
                            ddlCampaignType.Items.Insert(Key, item);
                        }
                    }
                    ddlCampaignType.DataBind();
                }
            }
            catch (Exception e)
            {
                TriggerJsMsgSystem_temp(this, "Đã có lỗi xảy ra:" + e.Message.Replace(@"'", @"\'"), "msg-system warning", 15000);
            }
        }
        /// <summary>
        /// Bind OBJ
        /// </summary>
        private bool Bind_OBJ()
        {
            var ExistOBJ = true;
            try
            {
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    var request = new Request();
                    var data = request.RunGetAsyncV1(
                        Libraries.AppConstants.URL_API_BILL_SERVICE + "/api/mkt-campaign/get?Id=" + Id).Result;
                    if (data.IsSuccessStatusCode)
                    {
                        var result = data.Content.ReadAsStringAsync().Result;
                        if (result != null)
                        {
                            JavaScriptSerializer serializer = new JavaScriptSerializer();
                            var dataCampaign = serializer.Deserialize<Output_data>(result).data;
                            Name.Text = dataCampaign.name;
                            Label.Text = dataCampaign.label;
                            MaxUse.Text = dataCampaign.maxUsage.ToString();
                            StartDate.Text = String.Format("{0:dd/MM/yyyy}", dataCampaign.startDate);
                            EndDate.Text = String.Format("{0:dd/MM/yyyy}", dataCampaign.endDate);
                            chkIsActive.Checked = dataCampaign.isActive ?? false;
                            Note.Text = dataCampaign.description;
                            HDF_MainImg.Value = dataCampaign.image;
                            // gọi hàm get dataCampaignservice
                            getDataCampaignService(Id);
                            //Bind loại campaign
                            var ItemSelected = ddlCampaignType.Items.FindByValue(dataCampaign.type.ToString());
                            if (ItemSelected != null)
                            {
                                ItemSelected.Selected = true;
                            }
                            else
                            {
                                ddlCampaignType.SelectedIndex = 0;
                            }
                            chkIsBookingPublish.Checked = dataCampaign.isBookingPublish ?? false;
                        }
                    }
                }
                else
                {
                    ExistOBJ = false;
                    var msg = "Lỗi! Bản ghi không tồn tại";
                    TriggerJsMsgSystem_temp(this, msg, "msg-system warning", 15000);

                }
            }
            catch (Exception e)
            {
                TriggerJsMsgSystem_temp(this, "Lỗi:" + e.Message.Replace(@"'", @"\'"), "msg-system warning", 15000);
            }
            return ExistOBJ;
        }
        /// <summary>
        /// Load các dịch vụ được ưu tiên
        /// </summary>
        private void BindService()
        {
            using (var db = new Solution_30shineEntities())
            {
                var data = db.Services.Where(w => w.IsDelete == 0 && w.Publish == 1).OrderByDescending(o => o.Id).ToList();
                if (data.Count > 0)
                {
                    Rpt_Service.DataSource = data;
                    Rpt_Service.DataBind();
                }
            }
        }

        /// <summary>
        /// Get dataCampaignService
        /// </summary>
        /// <param name="campaignId"></param>
        public void getDataCampaignService(int campaignId)
        {
            if (campaignId <= 0)
            {
                throw new Exception();
            }
            try
            {
                var request = new Request();
                var data = request.RunGetAsyncV1(
                    Libraries.AppConstants.URL_API_BILL_SERVICE + "/api/mkt-campaign-service/get-list?campaignId=" + campaignId).Result;

                if (data.IsSuccessStatusCode)
                {
                    var result = data.Content.ReadAsStringAsync().Result;
                    if (result != null)
                    {
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        var responseData = serializer.Deserialize<Output_Json>(result).data.ToList();
                        if (responseData != null)
                        {
                            Rpt_ServiceCampaign.DataSource = responseData;
                            Rpt_ServiceCampaign.DataBind();
                            ListingProductWp.Style.Add("display", "block");
                        }
                    }
                }
            }
            catch (Exception) { }

        }

        /// <summary>
        ///  Get Id record mới insert
        /// </summary>
        /// <returns></returns>
        public int GetbyId()
        {
            int Id = 0;
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    Id = db.MktCampaigns.OrderByDescending(o => o.Id).FirstOrDefault().Id;
                }
            }
            catch { }

            return Id;
        }
        /// <summary>
        /// Trả thông báo về client 
        /// </summary>
        /// <param name="_OBJ"></param>
        /// <param name="msg"></param>
        /// <param name="status"></param>
        /// <param name="duration"></param>
        public void TriggerJsMsgSystem_temp(Page _OBJ, string msg, string status, int duration)
        {
            ScriptManager.RegisterStartupScript(_OBJ, _OBJ.GetType(), "Message System", "showMsgSystem('" + msg + "','" + status + "'," + duration + ");", true);
        }
        /// <summary>
        /// Output data
        /// </summary>
        public class ResponseData
        {
            public int status { get; set; }
            public string message { get; set; }
            public dynamic data { get; set; }
            public ResponseData()
            {
                data = null;
                status = 0;
                message = "";
            }
        }

        /// <summary>
        /// Input MktCampaignService
        /// </summary>
        public class Input_MktCampaignService
        {
            public int campaignId { get; set; }
            public List<Input_Json> jsonService { get; set; }
        }

        /// <summary>
        /// Input Json
        /// </summary>
        public class Input_Json
        {
            public int serviceId { get; set; }
            public double discountPercent { get; set; }
            public double servicePrice { get; set; }
        }

        /// <summary>
        /// 
        /// </summary>
        public class Output_Json
        {
            public List<dataService> data { get; set; }
        }

        /// <summary>
        /// Output json
        /// </summary>
        public class dataService
        {
            public int serviceId { get; set; }
            public double discountPercent { get; set; }
            public double servicePrice { get; set; }
            public string serviceName { get; set; }
        }

        /// <summary>
        /// 
        /// </summary>
        public class Output_data
        {
            public data data { get; set; }
        }
        /// <summary>
        /// class origin
        /// </summary>
        public class data
        {
            public string image { get; set; }
            public int? type { get; set; }
            public bool? isBookingPublish { get; set; }
            public int id { get; set; }
            public string name { get; set; }
            public string label { get; set; }
            public DateTime startDate { get; set; }
            public DateTime endDate { get; set; }
            public int maxUsage { get; set; }
            public String description { get; set; }
            public bool? isActive { get; set; }
        }
    }
}