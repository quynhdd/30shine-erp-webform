﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Script.Serialization;
using ExportToExcel;
using Excel;
using LinqKit;
using System.Web.Services;
using System.Data.Entity.Migrations;
using Project.Model.Structure;
using System.Data.Linq.SqlClient;

namespace _30shine.GUI.BackEnd.MarKeting
{
    public partial class MarKeting_ChienDich : System.Web.UI.Page
    {
        Solution_30shineEntities db = new Solution_30shineEntities();
        //public static int Satff_Id= Convert.ToInt32(Session["SalonId"]);
        protected int staff_id;
        protected List<Store_Marketting_ChienDich_Result> obj;
        UIHelpers helper = new UIHelpers();
        protected bool Perm_ViewAllData = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //binitem();
                loaddata();
                //helper.Bind_Salon(ddlSalon, 0, true);
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
            }

        }

        public void loaddata()
        {
            staff_id = Convert.ToInt32(Session["User_Id"]);
            var msg = new CommonClass.cls_message();
            obj = db.Store_Marketting_ChienDich().ToList();
            if (obj != null)
            {
                msg.success = true;
                msg.message = "hiển thị thành công";
            }
            else
            {
                msg.success = false;
                msg.message = "lỗi !, Vui lòng liên hệ với nhà phát triển";
            }

        }
        //public void binitem()
        //{
        //    //var SalonId = Convert.ToInt32(Session["SalonId"]);
        //    var lst = db.Marketing_ChiphiItem.Where(p => p.IsDelete == false).OrderBy(p => p.Id).ToList();
        //    var item = new Marketing_ChiphiItem();
        //    item.Item_Name = "Chọn mục chi";
        //    item.Id = 0;
        //    lst.Insert(0, item);
        //    ddlItem.DataTextField = "Item_Name";
        //    ddlItem.DataValueField = "Id";
        //    ddlItem.SelectedIndex = 0;

        //    ddlItem.DataSource = lst;
        //    ddlItem.DataBind();
        //}

        /// <summary>
        /// thêm 
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="item_Id"></param>
        /// <param name="giaTri"></param>
        /// <param name="ngayChi"></param>
        /// <returns></returns>
        [WebMethod]
        public static object Save(int Id, string tieude, string ngaybatdau, string ngayketthuc, decimal chiphi, int SalonId)
        {
            //var SalonId = Convert.ToInt32(Session["SalonId"]);
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var msg = new CommonClass.cls_message();
                try
                {
                    if (tieude != null)
                    {
                        if (Id <= 0)
                        {
                            var obj = new Marketing_ChienDich();
                            obj.TieuDe = tieude;
                            obj.NgayBatDau = Convert.ToDateTime(ngaybatdau, culture);
                            obj.NgayKetThuc = Convert.ToDateTime(ngayketthuc, culture);
                            obj.ChiPhi = chiphi;
                            obj.SalonId = SalonId;
                            obj.IsDelete = false;
                            obj.CreateDate = DateTime.Now;
                            db.Marketing_ChienDich.AddOrUpdate(obj);
                            db.SaveChanges();
                            var total = db.Store_Marketing_TotalDate(obj.Id).ToList();
                            foreach (var item in total)
                            {
                                for (int i = 0; i < item.Total; i++)
                                {
                                    Marketing_ChiPhiPhanBo objTong = new Marketing_ChiPhiPhanBo();
                                    objTong.NgayChi = obj.NgayBatDau.Value.AddDays(i);
                                    objTong.TongChi = Convert.ToDecimal(item.trungbinh);
                                    objTong.SalonId = obj.SalonId;
                                    objTong.IsDelete = false;
                                    objTong.IdChienDich = obj.Id;
                                    objTong.ModifileDate = DateTime.Now;
                                    db.Marketing_ChiPhiPhanBo.AddOrUpdate(objTong);
                                }
                                db.SaveChanges();
                            }

                            msg.success = true;
                            msg.message = "Thêm thành công";
                            msg.data = obj;
                        }
                        else
                        {
                            var obj = db.Marketing_ChienDich.FirstOrDefault(w => w.Id == Id && w.IsDelete == false);
                            obj.TieuDe = tieude;
                            obj.NgayBatDau = Convert.ToDateTime(ngaybatdau, culture);
                            obj.NgayKetThuc = Convert.ToDateTime(ngayketthuc, culture);
                            obj.ChiPhi = chiphi;
                            obj.SalonId = SalonId;
                            obj.ModifileDate = DateTime.Now;
                            db.Marketing_ChienDich.AddOrUpdate(obj);
                            db.SaveChanges();

                            var list = db.Marketing_ChiPhiPhanBo.Where(p => p.IdChienDich == obj.Id).ToList();
                            foreach (var itemDelete in list)
                            {
                                var delete = db.Marketing_ChiPhiPhanBo.Single(p => p.Id == itemDelete.Id);
                                db.Marketing_ChiPhiPhanBo.Remove(delete);
                            }
                            db.SaveChanges();

                            var total = db.Store_Marketing_TotalDate(obj.Id).ToList();
                            foreach (var item in total)
                            {
                                for (int i = 0; i < item.Total; i++)
                                {
                                    Marketing_ChiPhiPhanBo objTong = new Marketing_ChiPhiPhanBo();
                                    objTong.NgayChi = obj.NgayBatDau.Value.AddDays(i);
                                    objTong.TongChi = Convert.ToDecimal(item.trungbinh);
                                    objTong.SalonId = obj.SalonId;
                                    objTong.IsDelete = false;
                                    objTong.IdChienDich = Id;
                                    objTong.ModifileDate = DateTime.Now;
                                    db.Marketing_ChiPhiPhanBo.AddOrUpdate(objTong);
                                }
                                db.SaveChanges();
                            }


                            //var tongngay = db.Marketing_TongChiTungNgay.Where(p => p.IsDelete == false && p.NgayChi >= obj.NgayBatDau && p.NgayChi <= obj.NgayKetThuc).ToList();
                            //if (tongngay.Count > 0)
                            //{
                            //    foreach (var v in tongngay)
                            //    {
                            //        var res = db.store_Marketing_Tong_Chi_Tung_Ngay(v.NgayChi).FirstOrDefault();
                            //        if (res != null)
                            //        {
                            //            var tong = db.Marketing_TongChiTungNgay.FirstOrDefault(p => p.NgayChi == v.NgayChi);

                            //            tong.TongChi = res.Tongngaychi;
                            //            tong.ModifileDate = DateTime.Now;
                            //            db.Marketing_TongChiTungNgay.AddOrUpdate(tong);
                            //            db.SaveChanges();

                            //        }
                            //    }
                            //}
                            msg.success = true;
                            msg.message = "Sửa thành công";
                            msg.data = obj;
                        }

                    }
                    else
                    {
                        msg.success = false;
                        msg.message = "Lỗi!. vui lòng liên hệ với nhà phát triển để được giải đáp";
                    }
                }
                catch (Exception e)
                {
                    throw new Exception(e.Message);
                }

                return msg;
            }
        }
        /// <summary>
        /// xóa  
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [WebMethod]
        public static object delete(int Id)
        {
            using (var db = new Solution_30shineEntities())
            {
                var msg = new CommonClass.cls_message();
                if (Id > 0)
                {
                    var obj = db.Marketing_ChienDich.FirstOrDefault(w => w.Id == Id && w.IsDelete == false);
                    if (obj != null)
                    {
                        obj.IsDelete = true;
                        obj.ModifileDate = DateTime.Now;
                        db.Marketing_ChienDich.AddOrUpdate(obj);
                        db.SaveChanges();

                        var tongngay = db.Marketing_ChiPhiPhanBo.Where(p => p.IsDelete == false && p.NgayChi >= obj.NgayBatDau && p.NgayChi <= obj.NgayKetThuc).ToList();
                        if (tongngay.Count > 0)
                        {
                            foreach (var v in tongngay)
                            {
                                var res = db.Store_Marketing_Tong_Chi_Tung_Ngay(v.NgayChi).FirstOrDefault();
                                if (res != null)
                                {
                                    var tong = db.Marketing_ChiPhiPhanBo.FirstOrDefault(p => p.NgayChi == v.NgayChi);

                                    tong.TongChi = Convert.ToDecimal( res.Tongngaychi);
                                    tong.ModifileDate = DateTime.Now;
                                    db.Marketing_ChiPhiPhanBo.AddOrUpdate(tong);
                                    db.SaveChanges();

                                }
                            }
                        }

                        // xoa chi phi phân bo vua lam
                        var cppb = db.Marketing_ChiPhiPhanBo.Where(x => x.IdChienDich == Id).ToList();
                        if(cppb.Count>0)
                        {
                            foreach(var i in cppb)
                            {
                                var deltecppb = db.Marketing_ChiPhiPhanBo.FirstOrDefault(x => x.Id == i.Id);
                                deltecppb.IsDelete = true;
                                deltecppb.ModifileDate = DateTime.Now;
                                db.Marketing_ChiPhiPhanBo.AddOrUpdate(deltecppb);
                                db.SaveChanges();
                                
                            }
                        }
                        else
                        {
                          
                            msg.message = "Lỗi ";
                        }


                        msg.success = true;
                        msg.data = obj;
                        msg.message = "Xóa thành công";
                    }
                    else
                    {
                        msg.success = false;
                        msg.message = "Lỗi!. vui lòng liên hệ với nhà phát triển để được giải đáp";
                    }
                }
                else
                {
                    msg.success = false;
                    msg.message = "Lỗi!. vui lòng liên hệ với nhà phát triển để được giải đáp";
                }
                return msg;
            }
        }
    }
}