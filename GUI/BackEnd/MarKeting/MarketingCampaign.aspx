﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="MarketingCampaign.aspx.cs" Inherits="_30shine.GUI.BackEnd.MarKeting.MarketingCampaign" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/Assets/css/bootstrap/bootstrap-toggle.min.css" rel="stylesheet" />
    <script src="/Assets/js/bootstrap/bootstrap-toggle.min.js"></script>
    <script src="/Assets/js/jquery.mCustomScrollbar.js"></script>
    <link href="/Assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
    <script src="/Assets/js/sweetalert.min.js"></script>
    <link href="/Assets/js/select2/select2.min.css" rel="stylesheet" />
    <script src="/Assets/js/select2/select2.min.js?v=112321"></script>
    <script src="/Assets/js/libs.xlsx/libs-xlsx-0.8.0-jszip.js"></script>
    <script src="/Assets/js/libs.xlsx/libs-xlsx-0.8.0-xlsx.js"></script>
    <style>
        .customer-add .table-add td.left span {
            width: 60% !important;
        }

        .fe-service.tr-product.show-product.addproduct {
            padding: 9px 12px;
            border-radius: 0;
            margin-left: 16px;
        }

        .wp_add_product {
            width: 100%;
            float: left;
            margin-top: 11px;
        }

        .staff-servey-mark td input[type='radio'] {
            top: 9px;
            float: left;
            position: relative
        }

        tr.staff-servey-mark input[type="checkbox"] {
            width: auto;
            vertical-align: text-top;
        }

        tr.staff-servey-mark td.right span {
            width: 15% !important;
            float: none !important;
            padding-left: 10%;
            padding-right: 10px;
            font-weight: bold;
        }

        tr.staff-servey-mark td.right select {
            width: 20% !important;
            float: none !important;
        }

        tr.staff-servey-mark td {
            padding-top: 15px !important;
        }

        #chkIsActive {
            padding-left: 0px !important;
        }

        #MainImg img {
            width: 120px;
            height: 160px;
            left: 0px;
        }

        .text-box-left {
            float: left;
            display: inline-block;
            padding-right: 10px;
        }

        .customer-add .table-add tr.tr-product span {
            width: 100%;
        }

        .title-head {
            padding-left: 50px;
        }

        .customer-add .table-add td span.campaign-note {
            line-height: 87px !important;
        }

        .customer-add .table-add .title-head strong {
            padding-left: 10px;
            text-decoration: underline;
            font-size: 25px;
        }

        .col-lg-12 {
            margin: 0 !important;
        }

        .thumb {
            height: 140px !important;
        }

        .box-main {
            background-color: #f2f2f2 !important;
        }

        #Btn_UploadImg {
            width: 120px;
        }

        .fe-service .tr-product .listing-product {
            display: inline-block;
            width: 98%;
        }

        .select2-container {
            width: 100% !important;
        }

        input.discount[disabled]:hover {
            cursor: no-drop;
        }

        .button-add {
            text-align: center;
            width: 25px !important;
            height: 25px !important;
            padding: 0px;
        }

        #ListingServiceWp {
            overflow-y: scroll;
            max-height: 500px;
        }

        .money-preview, .money-deduc {
            width: 90px !important;
        }

        #CampaignMaxUsed {
            text-align: center;
        }

        .swal-button--No {
            background-color: #cccccc !important;
        }

        .checkbox label:after,
        .radio label:after {
            content: '';
            display: table;
            clear: both;
        }

        .checkbox .cr,
        .radio .cr {
            position: relative;
            display: inline-block;
            border: 1px solid #a9a9a9;
            border-radius: .25em;
            width: 1.3em;
            height: 1.3em;
            float: left;
            margin-right: .5em;
        }

        .radio .cr {
            border-radius: 50%;
        }

            .checkbox .cr .cr-icon,
            .radio .cr .cr-icon {
                position: absolute;
                font-size: .8em;
                line-height: 0;
                top: 50%;
                left: 20%;
            }

            .radio .cr .cr-icon {
                margin-left: 0.04em;
            }

        .checkbox label input[type="checkbox"],
        .radio label input[type="radio"] {
            display: none;
        }

            .checkbox label input[type="checkbox"] + .cr > .cr-icon,
            .radio label input[type="radio"] + .cr > .cr-icon {
                transform: scale(3) rotateZ(-20deg);
                opacity: 0;
                transition: all .3s ease-in;
            }

            .checkbox label input[type="checkbox"]:checked + .cr > .cr-icon,
            .radio label input[type="radio"]:checked + .cr > .cr-icon {
                transform: scale(1) rotateZ(0deg);
                opacity: 1;
            }

            .checkbox label input[type="checkbox"]:disabled + .cr,
            .radio label input[type="radio"]:disabled + .cr {
                opacity: .5;
            }

        .swal-button--Yes {
            background-color: #a3dd82;
        }

        .customer-add .table-add td span.cr {
            height: 19px;
            width: 19px;
            top: 8px;
        }

        div.title-head {
            padding: 20px 0px;
        }

        .listing-img-upload .thumb-wp {
            width: 430px !important;
            height: 200px !important;
            overflow: hidden;
            float: left;
            position: relative;
            border: 1px solid #dfdfdf;
            cursor: pointer;
            margin-left: 5px;
            margin-top: 0px !important;
        }

            .listing-img-upload .thumb-wp img {
                width: 100% !important;
                height: 200px !important;
            }

        @media only screen and (max-width: 1600px) {
            .customer-add .table-add td {
                padding: 20px 0px;
            }
        }

        @media only screen and (max-width: 1200px) {
            .customer-add .table-add td {
                padding: 20px 0px;
            }
        }

        @media only screen and (max-width: 800px) {
            .customer-add .table-add td {
                padding: 15px 0px;
            }
        }
    </style>
    <%if (GetId() > 0)
        {%>
    <style>
        #select2-ddlCampaignType-container:hover {
            cursor: no-drop;
        }

        #select2-ddlServiceType-container:hover {
            cursor: no-drop;
        }
    </style>
    <%} %>
    <!--modal-->
    <style>
        .modal-footer {
            border-top: none;
        }

        .wp_table {
            overflow-y: scroll;
            width: 70%;
            margin: auto;
        }

        .cls_table {
            padding: 5px;
            margin: auto;
            width: 100%;
        }

        .cls_table, td, th {
            border-collapse: collapse;
        }

            .cls_table, td:first-child, th:first-child {
                min-width: 30px
            }

        .h2-cls-excel {
            width: 100%;
            float: left;
            text-align: center;
            font-size: 20px;
            padding: 10px;
            text-transform: uppercase;
            margin-top: 10px;
        }

        .h1-title-timport-file-excel {
            width: 100%;
            float: left;
            font-size: 20px;
            text-transform: uppercase;
            margin-bottom: 10px;
        }

        .wp-page {
            width: 100%;
            float: left;
            padding: 20px 10px 0;
        }

        #my_file_input::-webkit-file-upload-button {
            color: red;
        }

        #my_file_input::before {
            color: red;
            background: none;
            border: none;
        }

        .wp-submit {
            width: 100%;
            float: left;
            margin: 15px 0px 20px;
            text-align: right;
        }

        #my_file_input {
            float: left;
        }

        .cls_tempfile {
            float: left;
            text-decoration: underline !important;
            color: #0277bd;
            margin-left: 30px;
        }

        .wp_import-file-excel {
            float: right;
        }

        tbody tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        .btn-complete {
            background-color: black;
            color: white;
        }

            .btn-complete:hover {
                background-color: black;
                color: red;
            }

        .modal-content {
            width: 1200px;
            margin-left: -310px;
        }
    </style>
    <!--End modal-->
</asp:Content>

<asp:Content ID="ServiceAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý thông tin chiến dịch</li>
                        <li class="li-listing"><a href="/admin/marketing/danh-sach-chien-dich-v2.html">Danh sách</a></li>
                        <li class="li-add"><a href="/admin/marketing/them-moi-chien-dich-v2.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add admin-product-add fe-service">
            <%-- Add --%>
            <div class="wp960 content-wp col-lg-12">
                <div class="row box-main">
                    <div class="table-wp col-lg-12">
                        <table class="table-add admin-product-table-add fe-service-table-add">
                            <tbody>
                                <tr class="title-head">
                                    <td>
                                        <h2 style="font-size: 22px"><strong>Tạo mới chiến dịch</strong></h2>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="col-md-3"></td>
                                    <td>
                                        <span style="width: 98%;">
                                            <span>Tên chiến dịch:</span>
                                            <asp:TextBox ID="CampaignName" runat="server" CssClass="form-control" ClientIDMode="Static" placeholder="tên chiến dịch..."></asp:TextBox>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col-md-3"></td>
                                    <td>
                                        <div class="title-head"></div>
                                        <span style="width: 49%;" class="text-box-left">
                                            <span>Tên hiển thị CHECKOUT:</span>
                                            <asp:TextBox ID="CampaignDisplayCheckout" runat="server" ClientIDMode="Static" placeholder="tên hiển thị trên checkout..." CssClass="text-box-left form-control"></asp:TextBox>
                                        </span>
                                        <span style="width: 49%;">
                                            <span>Tên hiển thị CHECKIN (In Bill):</span>
                                            <asp:TextBox ID="CampaignDisplayCheckin" runat="server" CssClass="form-control" ClientIDMode="Static" placeholder="tên hiển thị trên checkin..."></asp:TextBox>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col-md-3"></td>
                                    <td>
                                        <div class="title-head"></div>
                                        <span style="width: 49%;" class="text-box-left">
                                            <span>Ngày bắt đầu:</span>
                                            <asp:TextBox ID="StartDate" CssClass="txtDateTime text-box-left form-control" runat="server" ClientIDMode="Static" placeholder="ngày bắt đầu..."></asp:TextBox>
                                        </span>
                                        <span style="width: 49%;">
                                            <span>Ngày kết thúc:</span>
                                            <asp:TextBox ID="EndDate" CssClass="txtDateTime form-control" runat="server" ClientIDMode="Static" placeholder="ngày kết thúc..."></asp:TextBox>
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="col-md-3"></td>
                                    <td>
                                        <div class="title-head"></div>
                                        <span style="width: 12%;" class="text-box-left">
                                            <span>Chiến dịch (Max):</span>
                                            <asp:TextBox ID="CampaignMaxUsed" runat="server" MaxLength="8" ClientIDMode="Static" placeholder="Vd: 10" CssClass="text-box-left form-control" onkeypress="return ValidateKeypress(/\d/,event);"></asp:TextBox>
                                        </span>
                                        <span style="width: 12%;" class="text-box-left">
                                            <span>Khách hàng (Max):</span>
                                            <asp:TextBox ID="CampaignCustomerMaxUsed" Style="text-align: center;" runat="server" MaxLength="8" ClientIDMode="Static" placeholder="Vd: 10" CssClass="text-box-left form-control" onkeypress="return ValidateKeypress(/\d/,event);"></asp:TextBox>
                                        </span>
                                        <span style="width: 24%; margin-right: 10px;">
                                            <span>Kiểu chiến dịch:</span>
                                            <asp:DropDownList ID="ddlCampaignType" onchange="onchangeCampaign()" CssClass="text-box-left form-control select" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                        </span>
                                        <span style="width: 24%; margin-right: 7px;">
                                            <span>Kiểu dịch vụ:</span>
                                            <asp:DropDownList ID="ddlServiceType" CssClass="text-box-left form-control select" runat="server" onchange="onchangeServiceType()" ClientIDMode="Static"></asp:DropDownList>
                                        </span>
                                        <span style="width: 24%;">
                                            <span>Kiểu khách hàng:</span>
                                            <asp:DropDownList ID="ddlCustomerType" runat="server" CssClass="form-control select" ClientIDMode="Static"></asp:DropDownList>
                                        </span>
                                    </td>
                                </tr>
                                <%if (GetId() > 0 && SetHidden)
                                    { %>
                                <tr class="tr-field-ahalf tr-product popup-service-mul-item">
                                    <%} %>
                                    <%else
                                        { %>
                                    <tr class="tr-field-ahalf tr-product popup-service-mul-item" style="display: none;">
                                        <%} %>
                                        <td class="col-md-3"></td>
                                        <td>
                                            <div style="margin-bottom: 15px" class="title-head"></div>
                                            <div class="row" id="quick-product">
                                                <div class="show-service show-item btn btn-default" data-item="product"><i class="fa fa-plus-circle"></i>Thêm dịch vụ</div>
                                            </div>
                                            <div class="listing-product item-product" id="ListingServiceWp" runat="server" clientidmode="Static">
                                                <table class="table table-listing-product table-item-product-multi" id="table-item-multi">
                                                    <thead>
                                                        <tr>
                                                            <th width="5%">STT</th>
                                                            <th width="10%">Số lần sử dụng</th>
                                                            <th width="15%">Tên dịch vụ</th>
                                                            <th width="15%">Giá dịch vụ</th>
                                                            <th width="15%">Giảm giá (%)</th>
                                                            <th width="15%">Thu trước</th>
                                                            <th width="15%">Giảm trừ</th>
                                                            <th width="10%">Thêm lần sử dụng</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <asp:Repeater ID="RptServiceMulti" runat="server">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td class="td-product-index td-product-index<%# Eval("ServiceId") %>" data-id='<%# Eval("ServiceId") %>'><%# Container.ItemIndex + 1 %></td>
                                                                    <td style="display: none;" class="td-product-id" data-id='<%# Eval("ServiceId") %>'><%# Eval("ServiceId") %></td>
                                                                    <td class="td-product-nouse" data-time-use="<%# Eval("TimesUsed") %>">Lần <%# Eval("TimesUsed") %></td>
                                                                    <td class="td-product-name" data-id='<%# Eval("ServiceName") %>'><%# Eval("ServiceName") %></td>
                                                                    <td class="td-product-price" data-price='<%# Eval("ServicePrice") %>'><%# Eval("ServicePrice") %></td>
                                                                    <td class="map-edit">
                                                                        <div class="row">
                                                                            <input type="text" maxlength="3" class="service-voucher service-voucher<%# Eval("ServiceId") %>" onchange="ChangeVoucher($(this));" value="<%# Eval("DiscountPercent") %>" style="margin: 0 auto; float: none; text-align: center; width: 50px;" onkeypress="return ValidateKeypress(/\d/,event);" />%
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="row">
                                                                            <input type="text" class="money-preview" maxlength="10" value="<%# String.Format("{0:#,0.#}", Eval("MoneyPrePaid")).Replace(".", ",")%>" data-id="<%# Eval("ServiceId") %>" style="margin: 0 auto; float: none; text-align: center; width: 50px;" onkeyup="onkeyupDisplayButton($(this));reformatText(this);" onkeypress="return ValidateKeypress(/\d/,event);" />
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div class="row">
                                                                            <input type="text" class="money-deduc" maxlength="10" onkeyup="reformatText(this);" value="<%# String.Format("{0:#,0.#}", Eval("MoneyDeductions")).Replace(".", ",")%>" style="margin: 0 auto; float: none; text-align: center; width: 50px;" onkeypress="return ValidateKeypress(/\d/,event);" />
                                                                        </div>
                                                                    </td>
                                                                    <td class="map-edit td-index-<%# Eval("TimesUsed") %>">
                                                                        <div class="btn btn-default button-add button-add<%# Eval("ServiceId") %>"
                                                                            <%# Convert.ToDouble(Eval("MoneyPrePaid")) > 0 ? "style=\"display:none;\"" : "" %>
                                                                            onclick="appendMultiService($(this), 1);" data-id='<%# Eval("ServiceId") %>' data-name="<%# Eval("ServiceName") %>" data-price='<%# Eval("ServicePrice") %>'>
                                                                            +
                                                                        </div>
                                                                        <div class="edit-wp">
                                                                            <a class="elm del-btn" href="javascript://" onclick='RemoveItem($(this).parent().parent().parent(),&#039;<%# Eval("ServiceName") %>&#039;,<%# Eval("ServiceId") %>,&#039;product&#039;)' title="Xóa"></a>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                    <%if (GetId() > 0 && !SetHidden)
                                        { %>
                                    <tr class="tr-field-ahalf tr-product popup-service-little-item">
                                        <%} %>
                                        <%else
                                            { %>
                                        <tr class="tr-field-ahalf tr-product popup-service-little-item" style="display: none;">
                                            <%} %>
                                            <td class="col-md-3"></td>
                                            <td>
                                                <div style="margin-bottom: 15px" class="title-head"></div>
                                                <div class="row" id="quick-service">
                                                    <div class="show-service show-item btn btn-default" data-item="product"><i class="fa fa-plus-circle"></i>Thêm dịch vụ</div>
                                                </div>
                                                <div class="listing-product item-product" id="ListingProductWp" runat="server" clientidmode="Static">
                                                    <table class="table table-listing-product table-item-product-single">
                                                        <thead>
                                                            <tr>
                                                                <th>STT</th>
                                                                <th>Tên dịch vụ</th>
                                                                <th>Giá dịch vụ</th>
                                                                <th>Giảm giá (%)</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <asp:Repeater ID="RptServiceSingle" runat="server">
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td class="td-product-index" data-id='<%# Eval("ServiceId") %>'><%# Container.ItemIndex + 1 %></td>
                                                                        <td style="display: none;" class="td-product-id" data-id='<%# Eval("ServiceId") %>'><%# Eval("ServiceId") %></td>
                                                                        <td class="td-product-name" data-id='<%# Eval("ServiceName") %>'><%# Eval("ServiceName") %></td>
                                                                        <td class="td-product-price" data-price='<%# Eval("ServicePrice") %>'><%# Eval("ServicePrice") %></td>
                                                                        <td class="map-edit">
                                                                            <div class="row">
                                                                                <input type="text" maxlength="3" class="service-voucher service-voucher<%# Eval("ServiceId") %>" onchange="ChangeVoucher($(this));" value="<%# Eval("DiscountPercent") %>" style="margin: 0 auto; float: none; text-align: center; width: 50px;" onkeypress="return ValidateKeypress(/\d/,event);" />%
                                                                            </div>
                                                                            <div class="edit-wp">
                                                                                <a class="elm del-btn" href="javascript://" onclick='RemoveItem($(this).parent().parent().parent(),&#039;<%# Eval("ServiceName") %>&#039;,<%# Eval("ServiceId") %>,&#039;service&#039;)' title="Xóa"></a>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="col-md-3"></td>
                                            <td>
                                                <div class="col-md-12" style="padding: 0; margin-top: 15px;">
                                                    <div class="title-head"></div>
                                                    <div class="col-md-6" style="padding: 0; margin: 0;">
                                                        <table style="margin-top: 10px; width: 100%;">
                                                            <thead>
                                                                <tr class="tr-upload">
                                                                    <td>
                                                                        <div class="wrap btn-upload-wp HDF_MainImg" style="margin-bottom: 5px; width: 100%;">
                                                                            <div id="Btn_UploadImg" class="btn-upload-img btn btn-primary" onclick="popupImageIframe('HDF_MainImg')" style="text-align: center; padding: 0px; background-color: #428bca; width: 75px; height: 35px; float: left">
                                                                                Chọn ảnh
                                                                            </div>
                                                                            <asp:FileUpload ID="FileUpload1" ClientIDMode="Static" Style="display: none;"
                                                                                CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                                                            <div class="wrap listing-img-upload" style="width: auto !important; top: 10px;">
                                                                                <div id="MainImg" class="thumb-wp" style="top: 10px;">
                                                                                    <img class="thumb" src="#" />
                                                                                    <span class="delete-thumb" onclick="deleteThumbnail($(this), 'HDF_MainImg')"></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </thead>
                                                        </table>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="checkbox">
                                                            <label>
                                                                <span style="float: none; display: inline-block; width: 20%">- Kích hoạt</span>
                                                                <input type="checkbox" value="" <%=HDF_IsActive.Value == "True" ? "checked=\"True\"" : "" %> onclick="CkbIsActive($(this));" />
                                                                <span style="float: left;" class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                            </label>
                                                        </div>

                                                        <div class="checkbox">
                                                            <label>
                                                                <span style="float: none; display: inline-block; width: 32%">- Publish Booking</span>
                                                                <input type="checkbox" value="" <%=HDF_IsBooking.Value == "True" ? "checked=\"True\"" : "" %> onclick="CkbIsBooking($(this));" />
                                                                <span style="float: left;" class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="tr-description">
                                            <td class="col-md-3"></td>
                                            <td>
                                                <span class="title-head campaign-note"></span>
                                                <span style="width: 48%; margin-right: 10px;">
                                                    <span>Ghi chú:</span>
                                                    <asp:TextBox TextMode="MultiLine" Rows="3" ID="CampaignNote" CssClass="form-control" runat="server" ClientIDMode="Static"></asp:TextBox>
                                                </span>
                                                <span style="width: 49%;">
                                                    <span>Điều kiện khách hàng:</span>
                                                    <asp:TextBox TextMode="MultiLine" Rows="3" ID="CampaignConditionCustomer" CssClass="form-control" runat="server" ClientIDMode="Static"></asp:TextBox>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr class="tr-send">
                                            <td class="col-md-3"></td>
                                            <td>
                                                <span style="width: 49%;">
                                                    <asp:Panel ID="BtnSend" CssClass="btn-send" runat="server" ClientIDMode="Static">Hoàn Tất</asp:Panel>
                                                    <asp:Button ID="BtnFakeSend" OnClick="ExcAddOrUpdate" runat="server" Enabled="true" Text="Hoàn tất"
                                                        ClientIDMode="Static" Style="display: none;"></asp:Button>
                                                    <button id="trigger" type="button" onclick="OpenModal('phone')" runat="server" class="btn btn-send import import-phone" style="text-align: center; padding: 0px; margin-left: 10px; width: 120px !important;" data-toggle="modal">Import Excel</button>
                                                    <button type="button" onclick="OpenModal('voucher')" runat="server" class="btn btn-send import import-voucher" style="text-align: center; padding: 0px; margin-left: 10px; width: 120px !important;" data-toggle="modal">Import Excel</button>
                                                </span>
                                            </td>
                                        </tr>
                            </tbody>
                        </table>
                        <asp:HiddenField ID="HDF_ServiceSingleId" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="HDF_ServiceMultiId" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="HDF_IsBooking" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="HDF_IsActive" runat="server" ClientIDMode="Static" />
                        <asp:HiddenField ID="HDF_MainImg" runat="server" ClientIDMode="Static" />
                    </div>
                </div>
            </div>
            <!-- Danh mục sản phẩm -->
            <div class="popup-product-wp popup-product-item">
                <div class="wp popup-product-head">
                    <strong>Danh mục sản phẩm</strong>
                </div>
                <div class="wp popup-product-content">
                    <div class="wp listing-product item-product">
                        <table class="table" id="table-product">
                            <thead>
                                <tr>
                                    <th>STT</th>
                                    <th>Tên dịch vụ</th>
                                    <th>Mã dịch vụ</th>
                                    <th>Giá dịch vụ</th>
                                    <th>Giảm giá (%)</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="RptServicePopup" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td class="td-product-index"><%# Container.ItemIndex + 1 %></td>
                                            <td class="td-product-id" data-id='<%# Eval("Id") %>' style="display: none"><%# Eval("Id") %></td>
                                            <td class="td-product-name" data-id='<%# Eval("Id") %>'><%# Eval("Name") %></td>
                                            <td class="td-product-code" data-code='<%# Eval("Code") %>'><%# Eval("Code") %></td>
                                            <td class="td-product-price" data-price='<%# Eval("Price") %>'><%# Eval("Price") %></td>
                                            <td class="map-edit">
                                                <div class="row">
                                                    <input type="text" maxlength="3" class="service-voucher service-voucher<%# Eval("Id") %> discount"  onchange="AddAttr($(this));ChangeVoucher($(this));" value="<%#Eval("VoucherPercent")%>" style="margin: 0 auto; float: none; text-align: center; width: 50px;" onkeypress="return ValidateKeypress(/\d/,event);" data-id='<%# Eval("Id") %>' />%
                                                </div>
                                            </td>
                                            <td class="td-product-checkbox item-product-checkbox">
                                                <input type="checkbox" class="checkbox<%#Eval("Id")%>" value="<%# Eval("Id") %>" data-id='<%# Eval("Id") %>'
                                                    data-code="<%# Eval("Code") %>" data-name="<%# Eval("Name") %>" data-price='<%# Eval("Price") %>' />
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </div>
                    <div class="wp btn-wp">
                        <div class="popup-product-btn btn-complete" data-item="product">Hoàn tất</div>
                        <div class="popup-product-btn btn-esc">Thoát</div>
                    </div>
                </div>
            </div>
            <!-- END Danh mục sản phẩm -->
            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" style="font-size: 20px;">Import Excel</h4>
                        </div>
                        <div class="modal-body">
                            <div>
                                <div class="wp-page">
                                    <div class="col-md-12">
                                        <div class="col-md-7">
                                            <div class="filter-item" style="width: 50%;">
                                                <h1 class="h1-title-timport-file-excel" id="span-title"></h1>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="wp_import-file-excel">
                                                <h1 class="h1-title-timport-file-excel" id="type-campaign"></h1>
                                                <div class="private">
                                                    <input type="file" id="my_file_input2" />
                                                    <a href="/TemplateFile/ImportExcelMarketing/file_import_demo_voucher_code.xlsx" class="cls_tempfile">Tải File Excel Import MẪu</a>
                                                </div>
                                                <div class="phone-number">
                                                    <input type="file" id="my_file_input3" />
                                                    <a href="/TemplateFile/ImportExcelMarketing/file_example_campaign_phone_number.xlsx" class="cls_tempfile">Tải File Excel Import MẪu</a>
                                                </div>
                                                <div class="code-phone">
                                                    <input type="file" id="my_file_input4" />
                                                    <a href="/TemplateFile/ImportExcelMarketing/file_example_campaign_code_phone.xlsx" class="cls_tempfile">Tải File Excel Import MẪu</a>
                                                </div>
                                                <div class="list-code-phone">
                                                    <input type="file" id="my_file_input5" />
                                                    <a href="/TemplateFile/ImportExcelMarketing/file_example_campaign_list_phone.xlsx" class="cls_tempfile">Tải File Excel Import MẪu</a>
                                                </div>
                                                <div class="one-code-phone">
                                                    <input type="file" id="my_file_input6" />
                                                    <a href="/TemplateFile/ImportExcelMarketing/file_example_campaign_one_code_list_phone.xlsx" class="cls_tempfile">Tải File Excel Import MẪu</a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <h2 class="h2-cls-excel">Thông tin Import file excel</h2>
                                    <div class="wp_table">
                                        <table class="cls_table">
                                            <thead>
                                                <tr style="line-height: 3">
                                                    <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px;">STT</th>
                                                    <th id="th-name" class="<%--private phone-number code-phone one-code-phone list-code-phone--%>" style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 150px;"><span>CAMPAIGN NAME</span></th>
                                                    <th id="th-voucher" class="<%--private code-phone one-code-phone list-code-phone--%>" style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 80px;"><span>VOUCHER CODES</span></th>
                                                    <th id="th-phone" class="<%--phone-number code-phone one-code-phone list-code-phone--%>" style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 80px;"><span>PHONE</span></th>
                                                    <th id="th-start-date" class="<%--phone-number code-phone list-code-phone--%>" style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 80px;"><span>START DATE</span></th>
                                                    <th id="th-end-date" class="<%--phone-number code-phone list-code-phone--%>" style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 80px;"><span>END DATE</span></th>
                                                </tr>
                                            </thead>
                                            <tbody id="body-content">
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="wp-submit">
                                        <a href="javascript://" onclick="UploadData();" class="btn btn-complete">UpLoad File</a>
                                        <button type="button" class="btn btn-default" onclick="CloseModal()" data-dismiss="modal">Đóng</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                        </div>
                    </div>
                </div>
            </div>
            <script>
                var campaignId;
                var camType;
                var indexCampaign = 1;
                var codeCampaign;
                jQuery(document).ready(function () {
                    var image = '<%=HDF_MainImg.Value%>';
                    $('#Btn_UploadImg').show();
                    if (image != undefined && image != null && image != '') {
                        $('#Btn_UploadImg').hide();
                    }
                    $('.select').select2();
                    bindImage();
                    let ddlCampaignType = $('#ddlCampaignType').val();
                    $('.import').css('display', 'none');
                    if (ddlCampaignType > '0') {
                        $('.import').css('display', '');
                        $('.import-voucher').css('display', 'none');
                        if (ddlCampaignType === '5') {
                            $('.import').css('display', '');
                            $('.import-phone').text('Import Excel Phone');
                            $('.import-voucher').text('Import Excel Voucher');
                        }
                    }
                    $('.button-add').css('display', 'none');
                    $('td.td-index-1 .button-add').css('display', 'inline-block');
                    $('td.td-index-1').parent().find('td.td-product-nouse').css('background-color', '#bfbfbf');
                    $('.private').hide();
                    $('#th-name').hide();
                    $('.phone-number').hide();
                    $('.code-phone').hide();
                    $('.list-code-phone').hide();
                    $('.one-code-phone').hide();
                    $('#th-voucher').hide();
                    $('#th-phone').hide();
                    $('#th-start-date').hide();
                    $('#th-end-date').hide();
                    //============================
                    // Datepicker
                    //============================
                    $('.txtDateTime').datetimepicker({
                        dayOfWeekStart: 1,
                        lang: 'vi',
                        startDate: '2014/10/10',
                        format: 'd/m/Y',
                        dateonly: true,
                        showHour: false,
                        showMinute: false,
                        timepicker: false,
                        onChangeDateTime: function (dp, $input) {
                            $input.val($input.val().split(' ')[0]);
                        },
                        scrollMonth: false,
                        scrollTime: false,
                        scrollInput: false
                    });
                });

                var checkTimeUsed = <%=GetId() > 0 ? 1 : 0%>;

                $(".show-item").bind("click", function () {
                    let $selectService = $('#ddlServiceType').val();
                    if ($selectService == undefined || $selectService == null || $selectService == '' || parseInt($selectService) == 0) {
                        swal("Vui lòng chọn kiểu dịch vụ.", "", "warning");
                        return;
                    }
                    var item = $(this).attr("data-item");
                    var classItem = ".popup-" + item + "-item";
                    $(classItem).openEBPopup();
                    if (parseInt($selectService) > 0 && $selectService != '') {
                        if ($selectService == '1' || $selectService == '2') {
                            $(".discount").attr("disabled", false);
                            ExcCompletePopupLittle();
                        }
                        else if ($selectService == '3') {
                            $(".discount").attr("disabled", true);
                            ExcCompletePopupMulti();
                        }
                    }
                    $('#EBPopup .listing-product').mCustomScrollbar({
                        theme: "dark-2",
                        scrollInertia: 100
                    });
                    $("#EBPopup .btn-esc").bind("click", function () { autoCloseEBPopup(0); });
                });

                // Update order item
                function UpdateItemOrder(dom) {
                    var index = 1;
                    dom.find("tr").each(function () {
                        $(this).find("td.td-product-index").text(index);
                        index++;
                    });
                }

                // Update order item
                function UpdateItemNumber(dom, Id, Index) {
                    var index = Index;
                    if (Index != undefined) {
                        dom.find("tr").each(function () {
                            $(this).find(`td.td-product-index${Id}`).each(function () {
                                $(this).parent().find('td.td-product-nouse').text('Lần ' + index);
                                $(this).parent().find('td.td-product-nouse').attr('data-time-use', index);
                                $(this).parent().parent().find('tr').css('background-color', '#f2f2f2');
                                index++;
                            })
                        });
                    }
                    else {
                        index = 1;
                        dom.find("tr").each(function () {
                            $(this).find(`td.td-product-index${Id}`).each(function () {
                                $(this).parent().find('td.td-product-nouse').text('Lần ' + index);
                                $(this).parent().parent().find('tr').css('background-color', '#f2f2f2');
                                index++;
                            })
                        });
                    }

                }

                // Check item đã được chọn
                function ExcCheckItemIsChose(Id, itemClass) {
                    var result = false;
                    $(itemClass).find(".td-product-id").each(function () {
                        var _Code = $(this).text().trim();
                        if (_Code == Id)
                            result = true;
                    });
                    return result;
                }

                // Xử lý click hoàn tất chọn item từ popup
                function ExcCompletePopupMulti() {
                    $("#EBPopup .btn-complete").unbind("click").bind("click", function () {
                        var item = $(this).attr("data-item"),
                            tableItem = $("table.table-item-" + item + "-multi tbody"),
                            tr,
                            Code;

                        $("#EBPopup .item-product-checkbox input[type='checkbox']:checked").each(function () {
                            var Id = $(this).attr("data-id"),
                                Name = $(this).attr("data-name").trim(),
                                Price = $(this).attr("data-price");
                            Code = Id;
                            if (!ExcCheckItemIsChose(Id, tableItem)) {
                                tr = `<tr>
                                    <td class="td-product-index" data-id='${Id}'></td>
                                    <td style="display:none;" class="td-product-id" data-id='${Id}'>${Id}</td>
                                    <td class="td-product-nouse" data-time-use="1" style="background-color:#bfbfbf">Lần 1</td>
                                    <td class="td-product-name" data-id='${Id}'>${Name}</td>
                                    <td class="td-product-price" data-price='${Price}'>${Price}</td>
                                    <td class="map-edit">
                                        <div class="row">
                                            <input type="text" maxlength="3" class="service-voucher service-voucher${Id}" onchange="ChangeVoucher($(this));" value="0" style="margin: 0 auto; float: none; text-align: center; width: 50px;" onkeypress="return ValidateKeypress(/\\d/,event);"/>%
                                        </div>
                                    </td>
                                    <td>
                                        <div class="row">
                                            <input type="text" maxlength="10" class="money-preview" data-id="${Id}" value="0" style="margin: 0 auto; float: none; text-align: center; width: 50px;" onkeypress="return ValidateKeypress(/\\d/,event);" onkeyup="onkeyupDisplayButton($(this));reformatText(this);"/>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="row">
                                            <input type="text" maxlength="10" value="0" class="money-deduc" onkeyup="reformatText(this);" style="margin: 0 auto; float: none; text-align: center; width: 50px;" onkeypress="return ValidateKeypress(/\\d/,event);"/>
                                        </div>
                                    </td>
                                    <td class="map-edit">
                                        <div class="btn btn-default button-add button-add${Id}" onclick="appendMultiService($(this), 2);" data-id='${Id}' data-name="${Name}" data-price='${Price}'>+</div>
                                        <div class="edit-wp">
                                            <a class="elm del-btn" href="javascript://" onclick='RemoveItem($(this).parent().parent().parent(),&#039;${Name}&#039;,${Id},&#039;product&#039;)' title="Xóa"></a>
                                        </div>
                                    </td>
                                </tr>`;
                                tableItem.append(tr);
                            }
                        });
                        UpdateItemNumber(tableItem, Code, 1);
                        getServiceMultiIds();
                        UpdateItemOrder(tableItem);
                        autoCloseEBPopup(0);
                    });
                }

                // Xử lý click hoàn tất chọn item từ popup
                function ExcCompletePopupLittle() {
                    $("#EBPopup .btn-complete").unbind("click").bind("click", function () {
                        var item = $(this).attr("data-item"),
                            tableItem = $("table.table-item-" + item + "-single tbody"),
                            tr;
                        $("#EBPopup .item-product-checkbox input[type='checkbox']:checked").each(function () {
                            var Id = $(this).attr("data-id"),
                                Name = $(this).attr("data-name").trim(),
                                Price = $(this).attr("data-price"),
                                VoucherPercent = $(this).attr("data-voucher") != undefined ? parseInt($(this).attr("data-voucher")) : 0;
                            if (VoucherPercent > 100) {
                                VoucherPercent = 100;
                            }
                            if (!ExcCheckItemIsChose(Id, tableItem)) {
                                tr = `<tr> 
                                    <td class="td-product-index" data-id='${Id}' ></td >
                                    <td style="display:none;" class="td-product-id" data-id='${Id}'>${Id}</td>
                                    <td class="td-product-name" data-id='${Id}'>${Name}</td>
                                    <td class="td-product-price" data-price='${Price}'>${Price}</td>
                                    <td class="map-edit">
                                        <div class="row">
                                            <input type="text" maxlength="3" class="service-voucher service-voucher${Id}" onchange="ChangeVoucher($(this));" value="${VoucherPercent}" style="margin: 0 auto; float: none; text-align: center; width: 50px;" onkeypress="return ValidateKeypress(/\\d/,event);"/>%
                                        </div>
                                        <div class="edit-wp">
                                            <a class="elm del-btn" href='javascript://' onclick='RemoveItem($(this).parent().parent().parent(),&#039;${Name}&#039;,${Id},&#039;service&#039;)' title="Xóa"></a>
                                        </div>
                                    </td>
                                </tr >`;
                                tableItem.append(tr);
                            }
                        });
                        getServiceSingleIds();
                        UpdateItemOrder(tableItem);
                        autoCloseEBPopup(0);
                    });
                }

                function getServiceSingleIds() {
                    var Ids = [];
                    var prd = {};
                    $("table.table-item-product-single tbody tr").each(function () {
                        prd = {};
                        var THIS = $(this);
                        var Id = THIS.find("td.td-product-index").attr("data-id"),
                            Name = THIS.find("td.td-product-name").text().trim(),
                            Price = THIS.find(".td-product-price").attr("data-price"),
                            VoucherPercent = THIS.find('input.service-voucher' + Id).val();
                        // check value
                        Id = Id.toString().trim() != "" ? parseInt(Id) : 0;
                        Price = Price.toString().trim() != "" ? parseInt(Price) : 0;
                        VoucherPercent = VoucherPercent.toString().trim() != "" ? parseInt(VoucherPercent) : 0;
                        if (VoucherPercent > 100) {
                            VoucherPercent = 100;
                        }
                        prd.Id = Id;
                        prd.Name = Name;
                        prd.Price = Price;
                        prd.TimeUsed = 1;
                        prd.VoucherPercent = VoucherPercent;
                        Ids.push(prd);
                    });
                    Ids = JSON.stringify(Ids);
                    $("#HDF_ServiceSingleId").val(Ids);
                }

                function getServiceMultiIds() {
                    var Ids = [];
                    var prd = {};
                    $("table.table-item-product-multi tbody tr").each(function () {
                        prd = {};
                        var THIS = $(this);
                        var Id = THIS.find("td.td-product-index").attr("data-id"),
                            Name = THIS.find("td.td-product-name").text().trim(),
                            Price = THIS.find(".td-product-price").attr("data-price"),
                            TimeUsed = THIS.find(".td-product-nouse").attr("data-time-use"),
                            MoneyPreview = THIS.find('input.money-preview').val(),
                            MoneyDeduc = THIS.find('input.money-deduc').val(),
                            VoucherPercent = THIS.find('input.service-voucher' + Id).val();
                        // check value
                        Id = Id.toString().trim() != "" ? parseInt(Id) : 0;
                        Price = Price.toString().trim() != "" ? parseInt(Price) : 0;
                        if (MoneyPreview != undefined && MoneyPreview.toString().trim() != '') {
                            var moneyPre = MoneyPreview.toString().trim().replace(',', '').replace('.', '');
                            MoneyPreview = moneyPre != "" ? parseInt(moneyPre) : 0;
                        }
                        if (MoneyDeduc != undefined && MoneyDeduc.toString().trim() != '') {
                            var moneyDe = MoneyDeduc.toString().trim().replace(',', '').replace('.', '');;
                            MoneyDeduc = moneyDe != "" ? parseInt(moneyDe) : 0;
                        }
                        if (VoucherPercent != undefined) {
                            VoucherPercent = VoucherPercent.toString().trim() != "" ? parseInt(VoucherPercent) : 0;
                        }
                        if (VoucherPercent > 100) {
                            VoucherPercent = 100;
                        }
                        prd.Id = Id;
                        prd.Name = Name;
                        prd.Price = Price;
                        prd.TimeUsed = TimeUsed;
                        prd.MoneyPreview = MoneyPreview;
                        prd.MoneyDeduc = MoneyDeduc;
                        prd.VoucherPercent = VoucherPercent;
                        Ids.push(prd);
                    });
                    Ids = JSON.stringify(Ids);
                    $("#HDF_ServiceMultiId").val(Ids);
                }

                function bindImage() {
                    var mainImg = $("#HDF_MainImg").val();
                    mainImg = typeof mainImg == undefined || mainImg == null ? "" : mainImg;
                    if (mainImg == "") {
                        $("#MainImg").hide();
                    }
                    else {
                        $("#MainImg img").attr("src", mainImg);
                    }
                }

                ///discountPercent
                function AddAttr(This) {
                    var discountPercent = This.val();
                    var code = This.attr('data-id');
                    discountPercent = discountPercent.trim() != "" ? parseInt(discountPercent) : 0;
                    $('.checkbox' + code).attr("data-voucher", discountPercent);
                }


                //Update service
                function UpdateItemDisplayService(itemName) {
                    var dom = $(".table-item-" + itemName);
                    var len = dom.find("tbody tr").length;
                    if (len == 0) {
                        dom.parent().hide();
                    } else {
                        UpdateItemService(dom.find("tbody"));
                    }
                }

                // Update STT
                function UpdateItemService(dom) {
                    var index = 1;
                    dom.find("tr").each(function () {
                        $(this).find("td.td-product-index").text(index);
                        index++;
                    });
                }

                // Remove item đã được chọn
                function RemoveItem(THIS, name, itemName) {
                    // Confirm
                    let table;
                    var index;
                    if (checkTimeUsed) {
                        index = 1;
                    }
                    else {
                        index = 2;
                    }
                    $(".confirm-yn").openEBPopup();
                    $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn hủy [ " + name + " ] ?");
                    $("#EBPopup .yn-yes").bind("click", function () {
                        var Id = THIS.find(".td-product-index").attr("data-id"),
                            text = THIS.find(".td-product-nouse").text();
                        $("#quick-" + itemName).find("input[data-id='" + Id + "']").removeAttr("checked");
                        if (text == 'Lần 1') {
                            $('.td-product-index' + Id).parent().remove();
                        }
                        THIS.remove();
                        autoCloseEBPopup(0);
                        UpdateItemDisplayService(itemName);
                        getServiceSingleIds();
                        getServiceMultiIds();
                        let $selectService = $('#ddlServiceType').val();
                        if (parseInt($selectService) > 0 && $selectService != '') {
                            if ($selectService == '3') {
                                table = $("table.table-item-product-multi tbody");
                                UpdateItemNumber(table, Id, index);
                                UpdateItemOrder(table);
                            }
                            else if ($selectService == '1' || $selectService == '2')
                                table = $("table.table-item-product-single tbody");
                            UpdateItemOrder(table);
                        }
                    });
                    $("#EBPopup .yn-no").bind("click", function () {
                        autoCloseEBPopup(0);
                    });
                }
                //vaidate number
                function ValidateKeypress(numcheck, e) {
                    var keynum, keychar, numcheck;
                    if (window.event) {
                        keynum = e.keyCode;
                    }
                    else if (e.which) {
                        keynum = e.which;
                    }
                    if (keynum == 8 || keynum == 127 || keynum == null || keynum == 9 || keynum == 0 || keynum == 13) return true;
                    keychar = String.fromCharCode(keynum);
                    var result = numcheck.test(keychar);
                    return result;
                }
                function popupImageIframe(StoreImgField) {
                    var Width = 960;
                    var Height = 560;
                    var ImgList = [];
                    var iframe = $("<div class='iframe-image-upload' style='width:" + Width + "px; height:" + Height + "px; float:left; display:none;'><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + StoreImgField + "&callback=getThumbFromIframe&imgWidth=800&imgHeight=600&multi_img=false&Folder=NhanVien' style='width:100%; height: 100%;'></iframe></div");
                    $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
                }

                window.getThumbFromIframe = function (Imgs, StoreImgField) {
                    var imgs = "";
                    var thumb = "";
                    if (Imgs.length > 0) {
                        thumb = '<div class="thumb-wp">' +
                            '<img class="thumb" alt="" title="" src="' + Imgs[0] + '"' +
                            'data-img="" />' +
                            '<span class="delete-thumb" onclick="deleteThumbnail($(this), \'' + StoreImgField + '\')"></span>' +
                            '</div>';
                        $("." + StoreImgField).find(".listing-img-upload").empty().append(thumb);
                        excThumbWidth(120, 120);
                        $('#Btn_UploadImg').hide();
                        $("#" + StoreImgField).val(Imgs[0]);
                    }
                    autoCloseEBPopup();
                }
                function deleteThumbnail(This, StoreImgField) {
                    This.parent().remove();
                    $("#" + StoreImgField).val("");
                    $('#Btn_UploadImg').show();
                }

                //onchange ddlServiceType
                function onchangeServiceType() {
                    let $selectService = $('#ddlServiceType').val();
                    $('.table-item-product-single tbody').empty();
                    $('.table-item-product-multi tbody').empty();
                    if (parseInt($selectService) > 0 && $selectService != '') {
                        if ($selectService == '1' || $selectService == '2') {
                            $('.popup-service-little-item').css('display', '');
                            $('.popup-service-mul-item').css('display', 'none');
                            $('#HDF_ServiceSingleId').val('');
                        }
                        else if ($selectService == '3') {
                            $('.popup-service-little-item').css('display', 'none');
                            $('.popup-service-mul-item').css('display', '');
                            $('#HDF_ServiceMultiId').val('');
                        }
                    }
                }

                function onkeyupDisplayButton(This) {
                    let $data = This.val();
                    let attr = This.attr('data-id');
                    if ($data != undefined && $data != '' && parseInt($data) > 0) {
                        let noUse = This.parent().parent().parent().find('td.td-product-nouse').text().trim();
                        if (noUse != undefined && noUse == 'Lần 1') {
                            $(`td.td-index-1 .button-add${attr}`).css('display', 'inline-block');
                        }
                    }
                }
                function appendMultiService(This, index) {
                    var Id = This.attr("data-id"),
                        Name = This.attr("data-name").trim(),
                        Price = This.attr("data-price");
                    $('#table-item-multi > tbody > tr').eq(This.parent().parent().index()).after(
                        `<tr style="background-color:#f2f2f2">
                        <td class="td-product-index td-product-index${Id}" data-id='${Id}'></td>
                        <td class="td-product-nouse"></td>  
                        <td class="td-product-name" data-id='${Id}'>${Name}</td>
                        <td class="td-product-price" data-price='${Price}'>${Price}</td>
                        <td class="map-edit">
                            <div class="row">
                                <input type="text" maxlength="3" class="service-voucher service-voucher${Id}" onchange="ChangeVoucher($(this));" value="0" style="margin: 0 auto; float: none; text-align: center; width: 50px;" onkeypress="return ValidateKeypress(/\\d/,event);" />%
                            </div>
                        </td>
                        <td>
                            <div class="row">
                                <input type="text" maxlength="10" class="money-preview" data-id="${Id}" value="0" style="margin: 0 auto; float: none; text-align: center; width: 50px;" onkeypress="return ValidateKeypress(/\\d/,event);" onkeyup="onkeyupDisplayButton($(this));reformatText(this);" />
                            </div>
                        </td>
                        <td>
                            <div class="row">
                                <input type="text" maxlength="10" value="0" class="money-deduc" onkeyup="reformatText(this);" style="margin: 0 auto; float: none; text-align: center; width: 50px;" onkeypress="return ValidateKeypress(/\\d/,event);" />
                            </div>
                        </td>
                        <td class="map-edit">
                            <div class="edit-wp">
                                <a class="elm del-btn" href="javascript://" onclick='RemoveItem($(this).parent().parent().parent(),&#039;${Name}&#039;,${Id},&#039;product&#039;)' title="Xóa"></a>
                            </div>
                        </td>
                    </tr>`
                    );
                    let table = $("table.table-item-product-multi tbody");
                    UpdateItemOrder(table);
                    UpdateItemNumber(table, Id, index);
                    $("tr td:contains('Lần 1')").each(function () {
                        $(this).children().children().find('tbody tr td:contains("Lần 1")').css('background-color', '#bfbfbf');
                    });
                }

                String.prototype.reverse = function () {
                    return this.split("").reverse().join("");
                }

                function reformatText(input) {
                    var x = input.value;
                    x = x.replace(/,/g, "");
                    x = x.reverse();
                    x = x.replace(/.../g, function (e) {
                        return e + ",";
                    });
                    x = x.reverse();
                    x = x.replace(/^,/, "");
                    input.value = x;
                }

                // Btn Send
                $("#BtnSend").bind("click", function () {
                    let error = false;
                    let campaignName = $('#CampaignName').val();
                    let campaignCheckoutName = $('#CampaignDisplayCheckout').val();
                    let campaignCheckinName = $('#CampaignDisplayCheckin').val();
                    let startDate = $('#StartDate').val();
                    let endDate = $('#EndDate').val();
                    let maxUsed = $('#CampaignMaxUsed').val();
                    let ddlCampaignType = $('#ddlCampaignType').val();
                    let ddlServiceType = $('#ddlServiceType').val();
                    let ddlCustomerType = $('#ddlCustomerType').val();
                    if (campaignName == undefined || campaignName == '') {
                        swal("Vui lòng nhập tên chiến dịch.", "", "warning");
                        error = true;
                        return;
                    }
                    else if (campaignCheckoutName == undefined || campaignCheckoutName == '') {
                        swal("Vui lòng nhập tên hiển thị trên checkout.", "", "warning");
                        error = true;
                        return;
                    }
                    else if (campaignCheckinName == undefined || campaignCheckinName == '') {
                        swal("Vui lòng nhập tên hiển thị trên checkin.", "", "warning");
                        error = true;
                        return;
                    }
                    else if (startDate == undefined || startDate == '') {
                        swal("Vui lòng nhập thời gian bắt đầu chiến dịch.", "", "warning");
                        error = true;
                        return;
                    }
                    else if (endDate == undefined || endDate == '') {
                        swal("Vui lòng nhập thời gian kết thúc chiến dịch.", "", "warning");
                        error = true;
                        return;
                    }
                    else if (maxUsed == undefined || maxUsed == '') {
                        swal("Vui lòng nhập số lần sử dụng.", "", "warning");
                        error = true;
                        return;
                    }
                    else if (parseInt(maxUsed) == 0) {
                        swal("Số lần sử dụng phải lớn hơn 0.", "", "warning");
                        error = true;
                        return;
                    }
                    else if (ddlCampaignType == undefined || ddlCampaignType == '' || parseInt(ddlCampaignType) == 0) {
                        swal("Vui lòng chọn kiểu chiến dịch.", "", "warning");
                        error = true;
                        return;
                    }
                    else if (ddlServiceType == undefined || ddlServiceType == '' || parseInt(ddlServiceType) == 0) {
                        swal("Vui lòng chọn kiểu dịch vụ.", "", "warning");
                        return;
                    }
                    if (!error) {
                        startLoading();
                        if (ddlServiceType == '1' || ddlServiceType == '2') {
                            getServiceSingleIds();
                        }
                        else if (ddlServiceType == '3') {
                            getServiceMultiIds();
                        }
                        $('#BtnFakeSend').click();
                    }
                });

                function CkbIsActive(This) {
                    let $val = This.prop("checked");
                    $('#HDF_IsActive').val($val);
                }

                function CkbIsBooking(This) {
                    let $val = This.prop("checked");
                    $('#HDF_IsBooking').val($val);
                }
                
                //show popup noti
                function ShowNotification(message, status) {
                    finishLoading();
                    var msg = message.toString().trim();
                    var stt = status.toString().trim();
                    let ddlCampaignType = $('#ddlCampaignType').val();
                    if (stt == 'success') {
                        swal({
                            text: msg,
                            icon: stt,
                            buttons: {
                                No: {
                                    text: "Không",
                                    value: false,
                                },
                                Yes: {
                                    text: "Có",
                                    value: true,
                                }
                            },
                            dangerMode: true,
                        }).then((value) => {
                            switch (value) {
                                case true:
                                    let ddlCampaignType = $('#ddlCampaignType').val();
                                    if (parseInt(ddlCampaignType) > 0) {
                                        campaignId = <%=GetId()%>;
                                            indexCampaign = 2;
                                            OpenModal('phone');
                                        }
                                        break;
                                    case false:
                                        window.location.href = '/admin/marketing/danh-sach-chien-dich-v2.html';
                                        break;
                                    default:
                                        window.location.href = '/admin/marketing/danh-sach-chien-dich-v2.html';
                                        break;
                                }
                            });
                    }
                    else if (stt == 'warning') {
                        swal(msg, '', stt);
                    }
                    else if (stt == 'error') {
                        swal({
                            text: msg,
                            icon: stt,
                            buttons: {
                                Yes: {
                                    text: "Ok",
                                    value: true,
                                }
                            },
                            dangerMode: true,
                        }).then((value) => {
                            switch (value) {
                                case true:
                                    window.location.href = '/admin/marketing/danh-sach-chien-dich-v2.html';
                                    break;
                            }
                        });
                        setTimeout(function () {
                            window.location.href = '/admin/marketing/danh-sach-chien-dich-v2.html';
                        }, 3000);
                    }
                }
            </script>

            <!--Modal-->
            <script>
                var slugKey;
                //onchange campaign
                function onchangeCampaign() {
                    let ddlCampaignType = $('#ddlCampaignType').val();
                    let type = parseInt(ddlCampaignType);
                    campaignId = <%=GetId()%>;
                    if (type > 0 && campaignId != undefined && campaignId != null && campaignId > 0) {
                        $('.import').css('display', '');
                        if (type === 5) {
                            $('.import-phone').show();
                            $('.import-voucher').show();
                            $('.import-phone').text('Import Excel Phone');
                            $('.import-voucher').text('Import Excel Voucher');
                        }
                    }
                    else {
                        $('.import').css('display', 'none');
                    }
                    if (type == 1 || type == 2) {
                        FileIn = document.getElementById('my_file_input2');
                        $('.import-phone').text('Import Excel');
                        $('.import-voucher').hide();
                    }
                    else if (type == 3) {

                        FileIn = document.getElementById('my_file_input3');
                        $('.import-phone').text('Import Excel');
                        $('.import-voucher').hide();
                    }
                    else if (type == 4) {
                        FileIn = document.getElementById('my_file_input4');
                        $('.import-phone').text('Import Excel');
                        $('.import-voucher').hide();
                    }
                    else if (type == 5) {
                        if (slugKey != undefined && slugKey != null && slugKey != '') {
                            if (slugKey === 'phone') {
                                FileIn = document.getElementById('my_file_input5');
                            }
                            else if (slugKey === 'voucher') {
                                FileIn = document.getElementById('my_file_input2');
                            }
                        }
                        else {
                            FileIn = document.getElementById('my_file_input5');
                        }
                    }
                    else if (type == 6) {
                        FileIn = document.getElementById('my_file_input6');
                        $('.import-phone').text('Import Excel');
                        $('.import-voucher').hide();
                    }
                    if (type > 0) {
                        if (FileIn.addEventListener) {
                            FileIn.addEventListener('change', filePicked, false);
                        }
                    }
                }

                function OpenModal(slug_key) {
                    $('#myModal').modal();
                    let ddlCampaignType = $('#ddlCampaignType').val();
                    let campaignType = parseInt(ddlCampaignType);
                    if (campaignType > 0) {
                        if (campaignType == 1 || campaignType == 2) {
                            $('.private').show();
                            $('#th-name').show();
                            $('.phone-number').hide();
                            $('.code-phone').hide();
                            $('.list-code-phone').hide();
                            $('.one-code-phone').hide();
                            $('#th-voucher').show();
                            $('#th-phone').hide();
                            $('#th-start-date').hide();
                            $('#th-end-date').hide();
                        }
                        else if (campaignType == 3) {
                            $('.phone-number').show();
                            $('.private').hide();
                            $('.code-phone').hide();
                            $('.list-code-phone').hide();
                            $('.one-code-phone').hide();
                            $('#th-name').show();
                            $('#th-voucher').hide();
                            $('#th-phone').show();
                            $('#th-start-date').show();
                            $('#th-end-date').show();
                        }
                        else if (campaignType == 4) {
                            $('.code-phone').show();
                            $('.private').hide();
                            $('.phone-number').hide();
                            $('.list-code-phone').hide();
                            $('.one-code-phone').hide();
                            $('#th-name').show();
                            $('#th-voucher').show();
                            $('#th-phone').show();
                            $('#th-start-date').show();
                            $('#th-end-date').show();
                        }
                        else if (campaignType == 5) {
                            slugKey = slug_key;
                            onchangeCampaign();
                            if (slug_key === 'voucher') {
                                $('.private').show();
                                $('#th-name').show();
                                $('.phone-number').hide();
                                $('.code-phone').hide();
                                $('.list-code-phone').hide();
                                $('.one-code-phone').hide();
                                $('#th-voucher').show();
                                $('#th-phone').hide();
                                $('#th-start-date').hide();
                                $('#th-end-date').hide();

                            }
                            else if (slug_key === 'phone') {
                                $('.list-code-phone').show();
                                $('.private').hide();
                                $('.phone-number').hide();
                                $('.code-phone').hide();
                                $('.one-code-phone').hide();
                                $('#th-name').show();
                                $('#th-voucher').hide();
                                $('#th-phone').show();
                                $('#th-start-date').show();
                                $('#th-end-date').show();
                            }
                        }
                        else if (campaignType == 6) {
                            $('.one-code-phone').show();
                            $('.private').hide();
                            $('.phone-number').hide();
                            $('.code-phone').hide();
                            $('.list-code-phone').hide();
                            $('#th-name').show();
                            $('#th-voucher').show();
                            $('#th-phone').show();
                            $('#th-start-date').hide();
                            $('#th-end-date').hide();
                        }
                        $('#span-title').text('Chiến dịch: ' + $('#CampaignName').val());
                        $('#type-campaign').text('Loại: ' + $('#ddlCampaignType').find(':selected').text());
                        camType = <%= CampaignType%>;
                        if (campaignType != camType && indexCampaign == 1) {
                            indexCampaign++;
                            swal("Nếu có thay đổi vui lòng hoàn tất trước khi Import Excel.", "", "warning");
                        }
                    }
                }

                function CloseModal() {
                    $('#modal').modal('toggle');
                    $("#body-content").html('');
                    objListCampaign = {};
                    $("input:file").val('');
                    let id = <%=Id %>;
                    if (id <= 0) {
                        window.location.href = '/admin/marketing/danh-sach-chien-dich-v2.html';
                    }
                }

                $(document).ready(function () {
                    $('.wp_table').css('max-height', "620px");
                });
                var listCampaign = {};
                var FileIn;
                $(function () {
                    let ddlCampaignType = $('#ddlCampaignType').val();
                    let type = parseInt(ddlCampaignType);
                    if (type == 1 || type == 2) {
                        FileIn = document.getElementById('my_file_input2');
                    }
                    else if (type == 3) {
                        FileIn = document.getElementById('my_file_input3');
                    }
                    else if (type == 4) {
                        FileIn = document.getElementById('my_file_input4');
                    }
                    else if (type == 5) {
                        FileIn = document.getElementById('my_file_input5');
                    }
                    else if (type == 6) {
                        FileIn = document.getElementById('my_file_input6');
                    }
                    if (type > 0) {
                        if (FileIn.addEventListener) {
                            FileIn.addEventListener('change', filePicked, false);
                        }
                    }
                });

                // func get data
                function filePicked(oEvent) {
                    campaignId = <%=GetId()%>;
                    if (campaignId >= 0 && campaignId != '' && campaignId != '0') {
                        // Get The File From The Input
                        var oFile = oEvent.target.files[0];
                        var sFilename = oFile.name;
                        // Create A File Reader HTML5
                        var reader = new FileReader();
                        // Ready The Event For When A File Gets Selected
                        reader.onload = function (e) {
                            var data = e.target.result;
                            var cfb = XLSX.read(data, { type: 'binary' });
                            // Here is your object
                            var XL_row_object = XLSX.utils.sheet_to_row_object_array(cfb.Sheets["Sheet1"]);
                            var obj = XLSX.utils.sheet_to_row_object_array()
                            listCampaign = XL_row_object;
                            // append html 
                            if (listCampaign.length > 0) {
                                let ddlCampaignType = $('#ddlCampaignType').val();
                                if (listCampaign.length > 1 && ddlCampaignType === '1') {
                                    swal("Chiến dịch loại Public chỉ được một mã voucher!", "", "warning");
                                    return;
                                }
                                if (listCampaign.length > 10000) {
                                    swal("File excel không thể vượt quá 10.000 dòng!", "", "warning");
                                    return;
                                }
                                else {
                                    appendData(listCampaign);
                                }
                            }
                            else {
                                $("#body-content").html('');
                                listCampaign = {};
                                swal("Vui lòng kiểm tra lại file excel!", "", "warning");
                            }
                        };
                        // Tell JS To Start Reading The File.. You could delay this if desired
                        reader.readAsBinaryString(oFile);
                    }
                    else {
                        swal("Có lỗi xảy ra vui lòng liên hệ nhà phát triển!", "", "warning");
                    }
                }
                // func apeend html
                function appendData(objData) {
                    var str = "";
                    $("#body-content").html('');
                    const Count = objData.length;
                    var stt = 0;
                    if (Count > 0) {
                        let ddlCampaignType = $('#ddlCampaignType').val();
                        if (ddlCampaignType === '1' || ddlCampaignType === '2') {
                            for (var i = 0; i < Count; i++) {
                                stt++
                                str += '<tr style="line-height:2"> ' +
                                    '<td  style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px;">' +
                                    stt +
                                    ' </td>' +
                                    '<td  style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 150px;">' +
                                    $('#CampaignName').val() +
                                    ' </td>' +
                                    '<td  style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 80px;">' +
                                    objData[i].Code +
                                    ' </td>' +
                                    '</tr>';
                            }
                        }
                        else if (ddlCampaignType === '3') {
                            for (var i = 0; i < Count; i++) {
                                stt++
                                str += '<tr style="line-height:2"> ' +
                                    //stt
                                    '<td  style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px;">' +
                                    stt +
                                    ' </td>' +
                                    // date
                                    '<td  style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 150px;">' +
                                    $('#CampaignName').val() +
                                    ' </td>' +
                                    '<td  style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 80px;">' +
                                    objData[i].Phone +
                                    ' </td>' +
                                    '<td  style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 80px;">' +
                                    objData[i].StartDate +
                                    ' </td>' +
                                    '<td  style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 80px;">' +
                                    objData[i].EndDate +
                                    ' </td>' +
                                    '</tr>';
                            }
                        }
                        else if (ddlCampaignType === '4') {
                            for (var i = 0; i < Count; i++) {
                                stt++
                                str += '<tr style="line-height:2"> ' +
                                    //stt
                                    '<td  style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px;">' +
                                    stt +
                                    ' </td>' +
                                    // date
                                    '<td  style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 150px;">' +
                                    $('#CampaignName').val() +
                                    ' </td>' +
                                    '<td  style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 80px;">' +
                                    objData[i].Code +
                                    ' </td>' +
                                    '<td  style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 80px;">' +
                                    objData[i].Phone +
                                    ' </td>' +
                                    '<td  style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 80px;">' +
                                    objData[i].StartDate +
                                    ' </td>' +
                                    '<td  style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 80px;">' +
                                    objData[i].EndDate +
                                    ' </td>' +
                                    '</tr>';
                            }
                        }
                        else if (ddlCampaignType === '5') {
                            if (slugKey === 'phone') {
                                for (var i = 0; i < Count; i++) {
                                    stt++
                                    str += '<tr style="line-height:2"> ' +
                                        //stt
                                        '<td  style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px;">' +
                                        stt +
                                        ' </td>' +
                                        // date
                                        '<td  style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 150px;">' +
                                        $('#CampaignName').val() +
                                        ' </td>' +
                                        '<td  style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 80px;">' +
                                        objData[i].Phone +
                                        ' </td>' +
                                        '<td  style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 80px;">' +
                                        objData[i].StartDate +
                                        ' </td>' +
                                        '<td  style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 80px;">' +
                                        objData[i].EndDate +
                                        ' </td>' +
                                        '</tr>';
                                }
                            }
                            else if (slugKey === 'voucher') {
                                for (var i = 0; i < Count; i++) {
                                    stt++
                                    str += '<tr style="line-height:2"> ' +
                                        '<td  style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px;">' +
                                        stt +
                                        ' </td>' +
                                        '<td  style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 150px;">' +
                                        $('#CampaignName').val() +
                                        ' </td>' +
                                        '<td  style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 80px;">' +
                                        objData[i].Code +
                                        ' </td>' +
                                        '</tr>';
                                }
                            }
                        }
                        else if (ddlCampaignType === '6') {
                            for (var i = 0; i < Count; i++) {
                                stt++;
                                if (stt == 1) {
                                    codeCampaign = objData[i].Code;
                                }
                                str += '<tr style="line-height:2"> ' +
                                    //stt
                                    '<td  style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px;">' +
                                    stt +
                                    ' </td>' +
                                    // date
                                    '<td  style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 150px;">' +
                                    $('#CampaignName').val() +
                                    ' </td>' +
                                    '<td  style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 80px;">' +
                                    codeCampaign +
                                    ' </td>' +
                                    '<td  style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 80px;">' +
                                    objData[i].Phone +
                                    ' </td>' +
                                    '</tr>';
                            }
                        }
                        if (str != '') {
                            $("#body-content").append(str);
                        }
                    }
                }
                // func click luu data
                function UploadData() {
                    var UrlCampaign = '<%=Libraries.AppConstants.URL_API_BILL_SERVICE %>';
                    if (listCampaign.length > 0) {
                        let ddlCampaignType = $('#ddlCampaignType').val();
                        startLoading();
                        var dataJson;
                        if (ddlCampaignType > '0') {
                            var strurl;
                            if (ddlCampaignType == '1' || ddlCampaignType == '2') {
                                strurl = '/api/mkt-voucher';
                                dataJson = {
                                    CampaignId: parseInt(campaignId),
                                    CampaignType: parseInt(ddlCampaignType),
                                    listVoucher: listCampaign
                                };
                            }
                            else if (ddlCampaignType == '3') {
                                strurl = '/api/campaign-customer';
                                dataJson = {
                                    CampaignId: parseInt(campaignId),
                                    listPhone: listCampaign
                                };

                            }
                            else if (ddlCampaignType == '4') {
                                strurl = '/api/campaign-customer/phone-voucher';
                                dataJson = {
                                    Campaign: parseInt(campaignId),
                                    list: listCampaign
                                };
                            }
                            else if (ddlCampaignType == '5') {
                                if (slugKey === 'phone') {

                                    strurl = '/api/campaign-customer';
                                    dataJson = {
                                        CampaignId: parseInt(campaignId),
                                        listPhone: listCampaign
                                    };
                                }
                                else if (slugKey === 'voucher') {
                                    strurl = '/api/mkt-voucher';
                                    dataJson = {
                                        CampaignId: parseInt(campaignId),
										CampaignType: parseInt(ddlCampaignType),
                                        listVoucher: listCampaign
                                    };
                                }
                            }
                            else if (ddlCampaignType == '6') {
                                strurl = '/api/campaign-customer/list-phone-voucher';
                                dataJson = {
                                    campaignId: parseInt(campaignId),
                                    listPhone: listCampaign,
                                    Code: codeCampaign
                                };
                            }
                            $.ajax({
                                type: "POST",
                                url: UrlCampaign + strurl,
                                data: JSON.stringify(dataJson),
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (response) {
                                    finishLoading();
                                    if (response == 'Success') {
                                        swal("Hoàn tất thành công!", "", "success");
                                        $("#body-content").html('');
                                        objListCampaign = {};
                                        $("input:file").val('');
                                    }
                                    else {
                                        swal("Hoàn tất thất bại!", "", "warning");
                                    }
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    var responseText = jqXHR.responseJSON.message;
                                    swal(`Lỗi : ${responseText}`, "", "error");
                                    finishLoading();
                                }
                            });
                        }
                    }
                };

                function ChangeVoucher(This) {
                    let voucher = This.val();
                    if (voucher === undefined || voucher === null || voucher === '') {
                        This.val('0');
                    }
                    else if(parseInt(voucher) > 100) {
                        This.val('100');
                    }
                }
            </script>
            <!--End modal-->
            <!-- Page loading -->
            <div class="page-loading">
                <p>Vui lòng đợi trong giây lát...</p>
            </div>
    </asp:Panel>
</asp:Content>
