﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Marketing_ImportVoucher.aspx.cs" Inherits="_30shine.GUI.BackEnd.MarKeting.Marketing_ImportVoucher" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <title>import chi phí hàng ngày cho salon file excel
    </title>
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>--%>
    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.0/jszip.js"></script>--%>
    <script src="/Assets/js/config.js"></script>
    <script src="/Assets/js/libs.xlsx/libs-xlsx-0.8.0-jszip.js"></script>
    <script src="/Assets/js/libs.xlsx/libs-xlsx-0.8.0-xlsx.js"></script>
    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.0/xlsx.js"></script>--%>
    <%--<script src=" https://cdnjs.cloudflare.com/ajax/libs/xls/0.7.4-a/xls.js"></script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="Panel1" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .wp_table {
                overflow-y: scroll;
                width: 100%;
                float: left;
            }

            .cls_table {
                padding: 5px;
                margin: auto;
                width: 100%;
            }

            .cls_table, td, th {
                border: 1px solid black;
                border-collapse: collapse;
                padding: 5px;
                min-width: 100px;
            }

                .cls_table, td:first-child, th:first-child {
                    min-width: 30px
                }

            .h2-cls-excel {
                width: 100%;
                float: left;
                text-align: center;
                font-size: 20px;
                padding: 10px;
                text-transform: uppercase;
                margin-top: 10px;
            }

            .h1-title-timport-file-excel {
                width: 100%;
                float: left;
                font-size: 20px;
                text-transform: uppercase;
                margin-bottom: 10px;
            }

            .wp-page {
                width: 100%;
                float: left;
                padding: 20px 10px 0;
            }

            #my_file_input::-webkit-file-upload-button {
                color: red;
            }

            #my_file_input::before {
                color: red;
                background: none;
                border: none;
            }

            .wp-submit {
                width: 100%;
                float: left;
                margin: 15px 0px 20px;
                text-align: right;
            }

            #my_file_input {
                float: left;
            }

            .cls_tempfile {
                float: left;
                text-decoration: underline !important;
                color: #0277bd;
                margin-left: 30px;
            }

            .container-sub {
                width: 60%;
                margin: auto;
            }

            th, td {
                text-align: center;
            }

            .wp_import-file-excel {
                float: right;
            }

            .col-md-12, .col-md-6 {
                margin: 0px;
            }

            tbody tr:nth-child(even) {
                background-color: #f2f2f2;
            }

            .select2-container {
                width: 250px !important;
                margin-top: 5px !important;
                text-align: center;
            }

            .select2-container--default .select2-selection--single {
                height: 35px !important;
                padding-top: 1px !important;
            }

            .btn-complete {
                background-color: black;
                color: white;
            }

                .btn-complete:hover {
                    background-color: black;
                    color: red;
                }
        </style>
          <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Thông tin khách hàng theo chiến dịch MKT &nbsp;&#187; </li>
                        <li class="li-listing active"><a href="/admin/marketing/list-customer-campaign.html">Danh sách KH theo chiến dịch</a></li>
                        <li class="li-pending active">
                            <a href="/admin/marketing/import-excel-customer.html">
                                <div class="pending-1"></div>
                                Cập nhật KH từ Excel(theo SDT)</a>
                        </li>
                         <li class="li-listing active"><a href="/admin/marketing/danh-sach-voucher-codes.html">Danh sách Private Code theo chiến dịch</a></li>
                          <li class="li-pending active">
                            <a href="/admin/marketing/import-excel-voucher-codes.html">
                                <div class="pending-1"></div>
                                Cập nhật mã Voucher(theo Campaign)</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div style="height: 800px">
            <div class="wp-page">
                <div class="container-sub">
                    <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-7">
                                <div class="filter-item ">
                                    <h1 class="h1-title-timport-file-excel">Select Campaign.</h1>
                                    <asp:DropDownList ID="ddlCampaign" CssClass="form-control select" onchange="onchangeCampaign();" runat="server" Style="width: 220px; margin-left: 10px;" ClientIDMode="Static"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="wp_import-file-excel">
                                    <h1 class="h1-title-timport-file-excel">Import Voucher Codes.</h1>
                                    <input type="file" id="my_file_input" />
                                    <div id='my_file_output'></div>
                                    <a href="/TemplateFile/ImportExcelMarketing/file_import_demo_voucher_code.xlsx" class="cls_tempfile">Tải File Excel Import MẪu</a>
                                </div>
                            </div>

                        </div>
                        <h2 class="h2-cls-excel">Thông tin Import file excel</h2>
                        <div class="wp_table">
                            <table class="cls_table">
                                <thead>
                                    <tr>
                                        <th style="padding: 10px 27px;">STT</th>
                                        <th style="padding: 10px 228px;"><span>CAMPAIGN NAME</span></th>
                                        <th style="padding: 10px 230px;"><span>VOUCHER CODES</span></th>
                                    </tr>
                                </thead>
                                <tbody id="body-content">
                                </tbody>
                            </table>
                        </div>
                        <div class="wp-submit">
                            <a href="javascript://" onclick="UploadData();" class="btn btn-complete">UpLoad File</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <script>
            $(document).ready(function () {
                //var maxheight = $(window).height();
                //console.log('aba', aba);
                $('.wp_table').css('max-height', "620px");
            });
            var uri = "<%= Libraries.AppConstants.URL_API_BILL_SERVICE %>";
            var objListCampaign = {};
            var oFileIn;

            $(function () {
                oFileIn = document.getElementById('my_file_input');
                if (oFileIn.addEventListener) {
                    oFileIn.addEventListener('change', filePicked, false);
                }
            });

            // func get data
            function filePicked(oEvent) {
                let campaignId = $("#ddlCampaign :selected").val();
                if (campaignId >= 0 && campaignId != '' && campaignId != '0') {
                    // Get The File From The Input
                    var oFile = oEvent.target.files[0];
                    var sFilename = oFile.name;
                    // Create A File Reader HTML5
                    var reader = new FileReader();
                    // Ready The Event For When A File Gets Selected
                    reader.onload = function (e) {
                        var data = e.target.result;
                        var cfb = XLSX.read(data, { type: 'binary' });
                        // Here is your object
                        var XL_row_object = XLSX.utils.sheet_to_row_object_array(cfb.Sheets["Sheet1"]);
                        var obj = XLSX.utils.sheet_to_row_object_array()
                        objListCampaign = XL_row_object;
                        console.log("XL_row_object", XL_row_object);
                        // append html 
                        if (objListCampaign.length > 0) {
                            appendData(objListCampaign);
                        }
                        else {
                            $("#body-content").html('');
                            objListCampaign = {};
                            let msg = 'Vui lòng kiểm tra lại file excel';
                            let status = 'msg-system warning';
                            let duration = 2000;
                            showMsgSystem(msg, status, duration);
                        }
                    };
                    // Tell JS To Start Reading The File.. You could delay this if desired
                    reader.readAsBinaryString(oFile);
                }
                else {
                    let msg = 'Chưa chọn chiến dịch!';
                    let status = 'msg-system warning';
                    let duration = 2000;
                    showMsgSystem(msg, status, duration);
                }
            }
            // func apeend html
            function appendData(objData) {
                var str = "";
                var Total = 0;
                $("#body-content").html('');
                const Count = objData.length;
                console.log('Count', Count);
                var ddlCampaign = $("#ddlCampaign option:selected").text();
                var stt = 0;
                if (Count > 0) {
                    for (var i = 0; i < Count; i++) {
                        Total = Total + parseFloat(objData[i].ElectricityAndWaterBill);
                        stt++
                        str += '<tr> ' +
                            //stt
                            '<td>' +
                            stt +
                            ' </td>' +
                            // date
                            '<td>' +
                            ddlCampaign +
                            ' </td>' +
                            '<td>' +
                            objData[i].Code +
                            ' </td>' +
                            '</tr>';
                    }
                    if (str != '') {
                        $("#body-content").append(str);
                    }
                }
            }
            // func click luu data
            function UploadData() {
                if (objListCampaign.length > 0) {
                    startLoading();
                    let campaignId = $("#ddlCampaign option:selected").val();
                    var dataJson = {
                        CampaignId: parseInt(campaignId),
                        ListCampaign: objListCampaign
                    };
                    $.ajax({
                        type: "POST",
                        url: uri + '/api/mkt-voucher',
                        data: JSON.stringify(dataJson),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            finishLoading();
                            if (response.status == 1) {
                                showMsgSystem('Thêm mới thành công!', 'msg-system success', 3000);
                            }
                            else {
                                showMsgSystem('Thêm mới thất bại!', 'msg-system warning', 3000);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var responseText = jqXHR.responseJSON.message;
                            objListCampaign = {};
                            showMsgSystem('Thêm mới thất bại!', 'msg-system warning', 3000);
                            finishLoading();
                        }
                    });
                }
            };

            //onchange campaign
            function onchangeCampaign() {
                $("#body-content").html('');
                objListCampaign = {};
                $("#my_file_input").val('');
            }
        </script>

        <!-- Page loading -->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
    </asp:Panel>
</asp:Content>
