﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;

namespace _30shine.GUI.BackEnd.MarKeting
{
    public partial class Marketing_ListingVoucher : System.Web.UI.Page
    {
        private bool Perm_Access = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_Delete = false;
        protected bool Perm_Edit = false;
        protected Paging PAGING = new Paging();

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                BindCampaign();
            }
        }

        /// <summary>
        /// check permission
        /// </summary>
        /// 
        protected void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        public void BindCampaign()
        {
            using (var db = new Solution_30shineEntities())
            {
                var listCampaign = new List<MktCampaign>();
                listCampaign = db.MktCampaigns.Where(w => w.IsDelete != true).OrderByDescending(o => o.CreatedTime).ToList();
                var Key = 0;
                var Count = listCampaign.Count;
                ddlCampaign.DataTextField = "Name";
                ddlCampaign.DataValueField = "Id";
                ListItem item = new ListItem("Chọn chiến dịch", "0");
                ddlCampaign.Items.Insert(Key, item);
                Key++;

                if (Count > 0)
                {
                    foreach (var v in listCampaign)
                    {
                        item = new ListItem(v.Name, v.Id.ToString());
                        ddlCampaign.Items.Insert(Key, item);
                        Key++;
                    }
                }
            }
        }

        /// <summary>
        /// Get data bind to rpt
        /// </summary>
        private void GetData()
        {
            try
            {
                int integer;
                int campaignId = int.TryParse(ddlCampaign.SelectedValue, out integer) ? integer : 0;
                using (var client = new HttpClient())
                {
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    client.BaseAddress = new Uri(Libraries.AppConstants.URL_API_BILL_SERVICE);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.GetAsync($"/api/mkt-voucher?campaignId={campaignId}").Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        var data = response.Content.ReadAsAsync<ResponseData>().Result.Data.ToList();
                        if (data != null)
                        {
                            Bind_Paging(data.Count);
                            RptListData.DataSource = data.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                            RptListData.DataBind();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                RptListData.DataSource = "";
                RptListData.DataBind();
            }
        }

        /// <summary>
        /// button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _BtnClick(object sender, EventArgs e)
        {
            GetData();
            RemoveLoading();
        }

        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();
            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Output
        /// </summary>
        public class ResponseData
        {
            public int Status { get; set; }
            public string Message { get; set; }
            public List<OutputVoucherCodes> Data { get; set; }
            public ResponseData()
            {
                Data = null;
                Status = 0;
                Message = "";
            }
        }

        /// <summary>
        /// Output
        /// </summary>
        public class OutputVoucherCodes
        {
            public int Id { get; set; }
            public string Code { get; set; }
            public int CampaignId { get; set; }
            public bool IsDelete { get; set; }
            public bool? IsActive { get; set; }
            public DateTime CreatedTime { get; set; }
            public DateTime? ModifiedTime { get; set; }
            public String CampaignName { get; set; }
        }
    }
}