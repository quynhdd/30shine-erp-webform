﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Listting_Customers_ByCampaign.aspx.cs" Inherits="_30shine.GUI.BackEnd.MarKeting.Listting_Customers_ByCampaign" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Danh sách khách hàng theo chiến dịch
    </title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .customer-listing table.table-listing tbody tr:hover .edit-wp.display_permission {
                display: none !important;
            }

            .display_permission {
                display: none;
            }

            .edit-wp {
                position: absolute;
                right: 3px;
                top: 24%;
                background: #e7e7e7;
                display: none;
            }

                .edit-wp .elm {
                    width: 17px;
                    height: 20px;
                    float: left;
                    display: block;
                    background: url(/Assets/images/icon.delete.small.active.png?v=2);
                    margin-right: 30px;
                }

                    .edit-wp .elm:hover {
                        background: url(/Assets/images/icon.delete.small.png?v=2);
                    }

            .wp_time_booking {
                width: 100%;
                float: left;
                padding-left: 55px;
                margin: 5px 0px 15px;
            }

                .wp_time_booking .a_time_booking {
                    float: left;
                    height: 26px;
                    line-height: 26px;
                    padding: 0px 15px;
                    background: #dfdfdf;
                    margin-right: 10px;
                    cursor: pointer;
                    position: relative;
                    font-size: 13px;
                    -webkit-border-radius: 15px;
                    -moz-border-radius: 15px;
                    border-radius: 15px;
                }

                    .wp_time_booking .a_time_booking:hover, .wp_time_booking .a_time_booking.active {
                        background: #fcd344;
                        color: #000;
                    }

                    .wp_time_booking .a_time_booking .span_time_booking {
                        position: absolute;
                        top: 24px;
                        left: 0;
                        width: 100%;
                        float: left;
                        text-align: center;
                        font-size: 13px;
                    }

            .edit-wp .elm {
                margin-right: 0;
            }

            table.table-total-report-1 {
                border-collapse: collapse;
            }

                table.table-total-report-1,
                table.table-total-report-1 th,
                table.table-total-report-1 td {
                    border: 1px solid black;
                }

                    table.table-total-report-1 td {
                        padding: 5px 15px;
                        text-align: right;
                    }

                        table.table-total-report-1 td:first-child {
                            text-align: left;
                        }
        </style>
        <%--<uc1:ReportMenu runat="server" id="ReportMenu" />--%>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Thông tin khách hàng theo chiến dịch MKT &nbsp;&#187; </li>
                        <li class="li-listing active"><a href="/admin/marketing/list-customer-campaign.html">Danh sách KH theo chiến dịch</a></li>
                        <li class="li-pending active">
                            <a href="/admin/marketing/import-excel-customer.html">
                                <div class="pending-1"></div>
                                Cập nhật KH từ Excel(theo SDT)</a>
                        </li>
                         <li class="li-listing active"><a href="/admin/marketing/danh-sach-voucher-codes.html">Danh sách Private Code theo chiến dịch</a></li>
                          <li class="li-pending active">
                            <a href="/admin/marketing/import-excel-voucher-codes.html">
                                <div class="pending-1"></div>
                                Cập nhật mã Voucher(theo Campaign)</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report be-report-timekeeping">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <div class="row">
                    <div class="filter-item">
                        <strong class="st-head" style="margin-right: 5px;">Chọn chiến dịch</strong>
                        <asp:DropDownList ID="ddlCampaign" runat="server" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                    </div>
                    <a id="ViewData" onclick="excPaging(1)" class="st-head btn-viewdata " href="javascript:void(0)">Xem dữ liệu
                    </a>
                </div>
<%--                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>Danh sách khách hàng</strong>
                </div>--%>
                <div class="wp customer-add customer-listing be-report">
                    <%-- Listing --%>
                    <div class="wp960 content-wp">
                        <!-- Filter -->
                        <!-- End Filter -->
                        <!-- Row Table Filter -->
                        <div class="table-func-panel" style="margin-top: -20px;">
                            <div class="table-func-elm">
                                <span>Số hàng / Page : </span>
                                <div class="table-func-input-wp">
                                    <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                                    <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                                    <ul class="ul-opt-segment">
                                        <li data-value="10">10</li>
                                        <li data-value="20">20</li>
                                        <li data-value="30">30</li>
                                        <li data-value="40">40</li>
                                        <li data-value="50">50</li>
                                        <%--<li data-value="1000000">Tất cả</li>--%>
                                    </ul>
                                    <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                                </div>
                            </div>
                        </div>
                        <!-- End Row Table Filter -->
                        <asp:ScriptManager runat="server" ID="ScriptManager2"></asp:ScriptManager>
                        <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                            <ContentTemplate>
                                <div class="table-wp">
                                    <table class="table-add table-listing">
                                        <thead>
                                            <tr>
                                                <th>STT</th>
                                                <%--<th>Tên chiến dịch</th>--%>
                                                <th>Tên khách hàng</th>
                                                <th>Số điện thoại</th>
                                                <th>Số lần sử dụng dịch vụ</th>
                                                <th>Chức năng</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="RptFluctuation" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                        <%--<td><%# Eval("CampaignName") %></td>--%>
                                                        <td><%# Eval("CustomerName") %></td>
                                                        <td><%# Eval("Phone") %></td>
                                                        <td><%# Eval("Used") %></td>
                                                        <td class="map-edit">
                                                            <div class="edit-wp">
                                                                <%--  <asp:Panel CssClass="edit-action-wp action-delete" runat="server" ID="EAdelete" Visible="false">--%>
                                                                <a class="elm del-btn" onclick="del(this.parentNode.parentNode.parentNode.parentNode,'<%# Eval("ID") %>', '<%# Eval("CustomerName") %>', '<%# Eval("Phone") %>')" href="javascript://" title="Xóa"></a>
                                                                <%--   </asp:Panel>--%>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="site-paging-wp">
                                    <% if (PAGING.TotalPage > 1)
                                        { %>
                                    <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                        <% if (PAGING._Paging.Prev != 0)
                                            { %>
                                        <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                        <% } %>
                                        <asp:Repeater ID="RptPaging" runat="server">
                                            <ItemTemplate>
                                                <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                                    <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                                    <%# Eval("PageNum") %>
                                                </a>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                            { %>
                                        <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                        <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                        <% } %>
                                    </asp:Panel>
                                    <% } %>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                        <!-- Hidden Field-->
                        <asp:HiddenField ID="HDF_Suggestion_Code" ClientIDMode="Static" runat="server" />
                        <asp:HiddenField ID="HDF_Suggestion_Field" ClientIDMode="Static" runat="server" />
                        <!-- END Hidden Field-->
                    </div>
                    <%-- END Listing --%>
                </div>
            </div>
        </div>
    </asp:Panel>
    <script type="text/ecmascript">
        //======================================================
        function viewDataByDate(This, time) {
            $(".a_time_booking.active").removeClass("active");
            This.addClass("active");
            $("#txtStartDate").val(time);
            $("#txtEndDate").val(time);
            $("#ViewData").click();
        }

        var paramClass = function () {
            this.Id = 0;
            this.Description = "";
        }

        var paramObject = new paramClass();
        /*
       * Cập nhật ghi chú
       */
        //============================
        // Event delete
        //============================
        function del(This, code, name) {
            var code = code || null,
                name = name || null,
                Row = This;
            if (!code) return false;

            // show EBPopup
            var URL_API_BILL_SERVICE = "<%= Libraries.AppConstants.URL_API_BILL_SERVICE %>";
           
            $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");
            $(".confirm-yn").openEBPopup();
            $("#EBPopup .yn-yes").bind("click", function () {
                $.ajax({
                    type: "DELETE",
                    url: URL_API_BILL_SERVICE + "/api/mkt-campaign-customer/delete?ID=" + code,
                    data: code,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {

                        if (response.status == 1) {
                          
                            //var msg = "Dữ liệu đã được xóa thành công!";
                            //$("#EBCloseBtn").click();
                            //$("#MsgSuccess").find(".row").text(msg).end().openEBPopup();
                            //autoCloseEBPopup(2000);
                            delSuccess();
                            $("#ViewData").click();
                        }
                        else
                        {
                            delFailed();
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            });
            $("#EBPopup .yn-no").bind("click", function () {
                autoCloseEBPopup(0);
            });
        }

    </script>
</asp:Content>
