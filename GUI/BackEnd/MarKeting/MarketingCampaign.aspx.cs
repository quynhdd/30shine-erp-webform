﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers.Http;
using System.Web.Script.Serialization;
using System.Globalization;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;

namespace _30shine.GUI.BackEnd.MarKeting
{
    public partial class MarketingCampaign : System.Web.UI.Page
    {
        protected bool isUpdate = false;
        private bool Perm_Access = false;
        protected bool Perm_ViewAllData = false;
        protected bool service = false;
        protected bool Perm_Delete = false;
        protected bool Perm_Edit = false;
        public bool CkbIsActive = false;
        public bool CkbIsBooking = false;
        public int Id;
        public int CampaignType;
        public bool SetHidden;
        public MktCampaign mktCampaign;

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        /// <summary>
        /// ExcuteByPermission
        /// </summary>
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }

        public int GetId()
        {
            if (mktCampaign != null)
            {
                return mktCampaign.Id;
            }
            return int.TryParse(Request.QueryString["Id"], out var integer) ? integer : 0;
        }

        /// <summary>
        /// pageLoad
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            Id = int.TryParse(Request.QueryString["Id"], out var integer) ? integer : 0;
            if (!IsPostBack)
            {
                BindCampaignType();
                BindServiceType();
                BindCustomerType();
                BindService();
                if (GetId() > 0)
                {
                    BindObject();
                    ddlCampaignType.Enabled = false;
                    ddlServiceType.Enabled = false;
                }
            }
        }

        /// <summary>
        /// Bind list các loại customer
        /// </summary>
        public void BindCustomerType()
        {
            using (var db = new Solution_30shineEntities())
            {
                var listCustomerType = db.Tbl_Config.Where(r => r.Key == "customer_type").Select(r => new { Value = r.Value, Name = r.Label }).ToList();
                ddlCustomerType.DataTextField = "Name";
                ddlCustomerType.DataValueField = "Value";
                ddlCustomerType.DataSource = listCustomerType;
                ddlCustomerType.DataBind();
            }
        }

        /// <summary>
        /// Bind list các loại campaign
        /// </summary>
        public void BindServiceType()
        {
            using (var db = new Solution_30shineEntities())
            {
                var listServiceType = db.Tbl_Config.Where(r => r.Key == "service_type").ToList();
                var item = new Tbl_Config();
                item.Label = "Chọn loại dịch vụ";
                item.Value = "0";
                listServiceType.Insert(0, item);
                ddlServiceType.DataTextField = "Label";
                ddlServiceType.DataValueField = "Value";
                ddlServiceType.DataSource = listServiceType;
                ddlServiceType.DataBind();
            }
        }

        /// <summary>
        /// Bind list các loại campaign
        /// </summary>
        public void BindCampaignType()
        {
            using (var db = new Solution_30shineEntities())
            {
                var listCampaignType = db.Tbl_Config.Where(r => r.Key == "campaign_type").ToList();
                var item = new Tbl_Config();
                item.Label = "Chọn tất cả chiến dịch";
                item.Value = "0";
                listCampaignType.Insert(0, item);
                ddlCampaignType.DataTextField = "Label";
                ddlCampaignType.DataValueField = "Value";
                ddlCampaignType.DataSource = listCampaignType;
                ddlCampaignType.DataBind();
            }
        }

        /// <summary>
        /// Load các dịch vụ được ưu tiên
        /// </summary>
        private void BindService()
        {
            using (var db = new Solution_30shineEntities())
            {
                var data = db.Services.Where(w => w.IsDelete == 0 && w.Publish == 1).OrderByDescending(o => o.Id).ToList();
                if (data.Count > 0)
                {
                    RptServicePopup.DataSource = data;
                    RptServicePopup.DataBind();
                }
            }
        }

        protected void BindObject()
        {
            try
            {
                if (GetId() > 0)
                {
                    var request = new Request();
                    var data = request.RunGetAsyncV1(Libraries.AppConstants.URL_API_BILL_SERVICE + $"/api/campaign/campaignId/{GetId()}").Result;
                    if (data.IsSuccessStatusCode)
                    {
                        var result = data.Content.ReadAsStringAsync().Result;
                        if (result != null)
                        {
                            JavaScriptSerializer serializer = new JavaScriptSerializer();
                            var record = serializer.Deserialize<ResponseData>(result);
                            if (record.Status)
                            {

                                if (record.data != null)
                                {
                                    var item = record.data.Campaign;
                                    CampaignName.Text = item.Name;
                                    CampaignDisplayCheckout.Text = item.Label;
                                    CampaignDisplayCheckin.Text = item.Description;
                                    StartDate.Text = String.Format("{0:dd/MM/yyyy}", item.StartDate);
                                    EndDate.Text = String.Format("{0:dd/MM/yyyy}", item.EndDate);
                                    CampaignMaxUsed.Text = item.CampaignMaxUsage.ToString();
                                    CampaignCustomerMaxUsed.Text = item.MaxUsage.ToString();
                                    ddlCampaignType.SelectedValue = item.Type.ToString();
                                    ddlServiceType.SelectedValue = item.ServiceType.ToString();
                                    ddlCustomerType.SelectedValue = item.CustomerType.ToString();
                                    CampaignNote.Text = item.Note;
                                    CampaignConditionCustomer.Text = item.CustomerCondition;
                                    HDF_MainImg.Value = item.Image;
                                    HDF_IsActive.Value = item.IsActive.ToString();
                                    HDF_IsBooking.Value = item.IsBookingPublish.ToString();
                                    if ("3".Equals(ddlServiceType.SelectedValue))
                                    {
                                        SetHidden = true;
                                        RptServiceMulti.DataSource = record.data.CampaignService;
                                        RptServiceMulti.DataBind();
                                    }
                                    else if ("1".Equals(ddlServiceType.SelectedValue) || "2".Equals(ddlServiceType.SelectedValue))
                                    {
                                        SetHidden = false;
                                        RptServiceSingle.DataSource = record.data.CampaignService;
                                        RptServiceSingle.DataBind();
                                    }
                                    CampaignType = item.Type;
                                }
                                else
                                {
                                    ShowNotification("Chiến dịch không tồn tại. Vui lòng kiểm tra lại!", "error");
                                }
                            }
                            else
                            {
                                ShowNotification("Vui lòng kiểm tra kết nối hoặc có lỗi trong quá trình lấy dữ liệu!", "warning");
                            }
                        }
                    }
                    else
                    {
                        ShowNotification("Có lỗi xảy ra vui lòng liên hệ nhà phát triển!", "warning");
                    }
                }
                else
                {
                    ShowNotification("Chiến dịch không tồn tại. Vui lòng kiểm tra lại!", "warning");
                }
            }
            catch (Exception ex)
            {
                ShowNotification($"Lỗi: {ex.Message.Replace(@"'", @"\'")}.", "error");
            }
        }

        /// <summary>
        ///  button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ExcAddOrUpdate(object sender, EventArgs e)
        {
            if (GetId() > 0)
            {
                Update();
            }
            else
            {
                Add();
            }
        }

        protected void Update()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var record = new MarketingCampaignSub();
                    record.objCampaign = new MktCampaign();
                    record.ServiceIds = new List<CampaignServiceIds>();
                    var serializer = new JavaScriptSerializer();
                    record.objCampaign.Id = GetId();
                    record.objCampaign.Name = CampaignName.Text;
                    record.objCampaign.Label = CampaignDisplayCheckout.Text;
                    record.objCampaign.Description = CampaignDisplayCheckin.Text;
                    record.objCampaign.StartDate = Convert.ToDateTime(StartDate.Text, new CultureInfo("vi-VN"));
                    record.objCampaign.EndDate = Convert.ToDateTime(EndDate.Text, new CultureInfo("vi-VN"));
                    if (record.objCampaign.StartDate > record.objCampaign.EndDate)
                    {
                        ddlServiceType.SelectedValue = "0";
                        ShowNotification("Ngày bắt đầu phải nhỏ hơn hoặc bằng ngày kết thúc!", "warning");
                        return;
                    }
                    record.objCampaign.MaxUsage = int.TryParse(CampaignCustomerMaxUsed.Text, out var integer) ? integer : 0;
                    record.objCampaign.CampaignMaxUsage = int.TryParse(CampaignMaxUsed.Text, out integer) ? integer : 0;
                    record.objCampaign.Type = int.TryParse(ddlCampaignType.SelectedValue, out integer) ? integer : 0;
                    record.objCampaign.ServiceType = int.TryParse(ddlServiceType.SelectedValue, out integer) ? integer : 0;
                    record.objCampaign.CustomerType = int.TryParse(ddlCustomerType.SelectedValue, out integer) ? integer : 0;
                    record.objCampaign.Note = CampaignNote.Text;
                    record.objCampaign.CustomerCondition = CampaignConditionCustomer.Text;
                    record.objCampaign.Image = HDF_MainImg.Value != "[]" ? HDF_MainImg.Value : "";
                    record.objCampaign.IsActive = HDF_IsActive.Value != "" ? Convert.ToBoolean(HDF_IsActive.Value) : false;
                    record.objCampaign.IsBookingPublish = HDF_IsBooking.Value != "" ? Convert.ToBoolean(HDF_IsBooking.Value) : false;
                    if ("3".Equals(ddlServiceType.SelectedValue) && HDF_ServiceMultiId.Value != "[]")
                    {
                        if ((HDF_ServiceMultiId.Value.Trim().StartsWith("{") && HDF_ServiceMultiId.Value.Trim().EndsWith("}")) ||
                           (HDF_ServiceMultiId.Value.Trim().StartsWith("[") && HDF_ServiceMultiId.Value.Trim().EndsWith("]")))
                        {
                            record.ServiceIds = serializer.Deserialize<List<CampaignServiceIds>>(HDF_ServiceMultiId.Value).ToList();
                        }
                    }
                    else if ("1".Equals(ddlServiceType.SelectedValue) || "2".Equals(ddlServiceType.SelectedValue) && HDF_ServiceSingleId.Value != "[]")
                    {
                        if ((HDF_ServiceSingleId.Value.Trim().StartsWith("{") && HDF_ServiceSingleId.Value.Trim().EndsWith("}")) ||
                           (HDF_ServiceSingleId.Value.Trim().StartsWith("[") && HDF_ServiceSingleId.Value.Trim().EndsWith("]")))
                        {
                            record.ServiceIds = serializer.Deserialize<List<CampaignServiceIds>>(HDF_ServiceSingleId.Value).ToList();
                        }
                    }
                    if (record.ServiceIds == null || record.ServiceIds.Count <= 0)
                    {
                        ddlServiceType.SelectedValue = "0";
                        ShowNotification("Vui lòng chọn dịch vụ trong chiến dịch!", "warning");
                        return;
                    }
                    var result = new HttpClient().PutAsJsonAsync(Libraries.AppConstants.URL_API_BILL_SERVICE + "/api/campaign/update", record).Result;
                    if (result.IsSuccessStatusCode)
                    {
                        BindObject();
                        ShowNotification("Hoàn tất thành công. Bạn có muốn thêm khách hàng hoặc voucher vào chiến dịch không?", "success");
                        return;
                    }
                    else
                    {
                        ShowNotification("Có lỗi xảy ra vui lòng liên hệ nhà phát triển!", "error");
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                ShowNotification($"Lỗi: {ex.Message.Replace(@"'", @"\'")}.", "error");
            }
        }

        protected void Add()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var record = new MarketingCampaignSub();
                    record.objCampaign = new MktCampaign();
                    record.ServiceIds = new List<CampaignServiceIds>();
                    var serializer = new JavaScriptSerializer();
                    record.objCampaign.Name = CampaignName.Text;
                    record.objCampaign.Label = CampaignDisplayCheckout.Text;
                    record.objCampaign.Description = CampaignDisplayCheckin.Text;
                    record.objCampaign.StartDate = Convert.ToDateTime(StartDate.Text, new CultureInfo("vi-VN"));
                    record.objCampaign.EndDate = Convert.ToDateTime(EndDate.Text, new CultureInfo("vi-VN"));
                    if (record.objCampaign.StartDate > record.objCampaign.EndDate)
                    {
                        ddlServiceType.SelectedValue = "0";
                        ShowNotification("Ngày bắt đầu phải nhỏ hơn hoặc bằng ngày kết thúc!", "warning");
                        return;
                    }
                    record.objCampaign.MaxUsage = int.TryParse(CampaignCustomerMaxUsed.Text, out var integer) ? integer : 0;
                    record.objCampaign.CampaignMaxUsage = int.TryParse(CampaignMaxUsed.Text, out integer) ? integer : 0;
                    record.objCampaign.Type = int.TryParse(ddlCampaignType.SelectedValue, out integer) ? integer : 0;
                    record.objCampaign.ServiceType = int.TryParse(ddlServiceType.SelectedValue, out integer) ? integer : 0;
                    record.objCampaign.CustomerType = int.TryParse(ddlCustomerType.SelectedValue, out integer) ? integer : 0;
                    record.objCampaign.Note = CampaignNote.Text;
                    record.objCampaign.CustomerCondition = CampaignConditionCustomer.Text;
                    record.objCampaign.Image = HDF_MainImg.Value != "[]" ? HDF_MainImg.Value : "";
                    record.objCampaign.IsActive = HDF_IsActive.Value != "" ? Convert.ToBoolean(HDF_IsActive.Value) : false;
                    record.objCampaign.IsBookingPublish = HDF_IsBooking.Value != "" ? Convert.ToBoolean(HDF_IsBooking.Value) : false;
                    if ("3".Equals(ddlServiceType.SelectedValue) && HDF_ServiceMultiId.Value != "[]")
                    {
                        if ((HDF_ServiceMultiId.Value.Trim().StartsWith("{") && HDF_ServiceMultiId.Value.Trim().EndsWith("}")) ||
                           (HDF_ServiceMultiId.Value.Trim().StartsWith("[") && HDF_ServiceMultiId.Value.Trim().EndsWith("]")))
                        {
                            record.ServiceIds = serializer.Deserialize<List<CampaignServiceIds>>(HDF_ServiceMultiId.Value).ToList();
                        }
                    }
                    else if ("1".Equals(ddlServiceType.SelectedValue) || "2".Equals(ddlServiceType.SelectedValue) && HDF_ServiceSingleId.Value != "[]")
                    {
                        if ((HDF_ServiceSingleId.Value.Trim().StartsWith("{") && HDF_ServiceSingleId.Value.Trim().EndsWith("}")) ||
                           (HDF_ServiceSingleId.Value.Trim().StartsWith("[") && HDF_ServiceSingleId.Value.Trim().EndsWith("]")))
                        {
                            record.ServiceIds = serializer.Deserialize<List<CampaignServiceIds>>(HDF_ServiceSingleId.Value).ToList();
                        }
                    }
                    if (record.ServiceIds == null || record.ServiceIds.Count <= 0)
                    {
                        ddlServiceType.SelectedValue = "0";
                        ShowNotification("Vui lòng chọn dịch vụ trong chiến dịch!", "warning");
                        return;
                    }
                    var result = new HttpClient().PostAsJsonAsync(Libraries.AppConstants.URL_API_BILL_SERVICE + "/api/campaign/add", record).Result;
                    if (result.IsSuccessStatusCode)
                    {
                        var data = result.Content.ReadAsStringAsync().Result;
                        if (data != null)
                        {
                            mktCampaign = serializer.Deserialize<ResponseCampaign>(data).data;
                            BindObject();
                            ShowNotification("Thêm chiến dịch thành công. Bạn có muốn thêm khách hàng hoặc voucher vào chiến dịch không?", "success");
                            return;
                        }
                    }
                    else
                    {
                        ShowNotification("Có lỗi xảy ra vui lòng liên hệ nhà phát triển!", "error");
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                ShowNotification($"Lỗi: {ex.Message.Replace(@"'", @"\'")}.", "error");
            }
        }

        /// <summary>
        /// show notification
        /// </summary>
        /// <param name="message"></param>
        /// <param name="status"></param>
        public void ShowNotification(string message, string status)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Click", "ShowNotification('" + message + "', '" + status + "');", true);
        }
    }

    public class ResponseData
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public ResCampaignService data { get; set; }
    }

    public class ResponseCampaign
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public MktCampaign data { get; set; }
    }

    public class ResCampaignService
    {
        public MktCampaign Campaign { get; set; }
        public List<CampaignService> CampaignService { get; set; }
    }
    public class CampaignService : MktCampaignService
    {
        public string ServiceName { get; set; }
    }

    public class MarketingCampaignSub
    {
        public MktCampaign objCampaign { get; set; }
        public List<CampaignServiceIds> ServiceIds { get; set; }
    }
    public class CampaignServiceIds
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public double Price { get; set; }
        public int TimeUsed { get; set; }
        public int? MoneyPreview { get; set; }
        public int? MoneyDeduc { get; set; }
        public int VoucherPercent { get; set; }
    }
}