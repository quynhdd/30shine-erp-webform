﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using Library;
using System.Web.Services;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.MODEL.BO;
using _30shine.MODEL.IO;
using System.Text.RegularExpressions;
using System.Reflection;
using _30shine.Helpers;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
namespace _30shine.GUI.BackEnd.MarKeting
{
    public partial class Marketing_Excel : System.Web.UI.Page
    {
        public Solution_30shineEntities db;
        IStaffTypeModel staffTypeModel = new StaffTypeModel();
        private string PageID = "UPDATE_EXCELL";
        private bool Perm_Access = false;


        public static void bindCampaign(List<DropDownList> ddls)
        {
            using (var db = new Solution_30shineEntities())
            {
                var listCampaign = new List<MktCampaign>();
                listCampaign = db.MktCampaigns.Where(w => w.IsDelete != true).OrderByDescending(o => o.CreatedTime).ToList();
                var item = new MktCampaign();
                item.Name = "Chọn chiến dịch";
                item.Id = 0;
                listCampaign.Insert(0, item);
                for (var i = 0; i < ddls.Count; i++)
                {
                    ddls[i].DataTextField = "Name";
                    ddls[i].DataValueField = "Id";
                    ddls[i].DataSource = listCampaign;
                    ddls[i].DataBind();
                }
            }
        }

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //ExecuteByPermission();

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }


        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //SetPermission();
            if (!IsPostBack)
            {
                bindCampaign(new List<DropDownList>() { ddlCampaign });
            }
        }
    }
}
