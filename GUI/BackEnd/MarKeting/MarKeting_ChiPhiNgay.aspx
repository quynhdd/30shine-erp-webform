﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="MarKeting_ChiPhiNgay.aspx.cs" Inherits="_30shine.GUI.BackEnd.MarKeting.MarKeting_ChiPhiNgay" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .edit { margin: 8px; width: 15px; }
            .del { margin: 8px; width: 15px; }
            .del:hover { color: red; }
            .edit:hover { color: seagreen; }
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý chi tiêu hằng ngày &nbsp;&#187; </li>
                        <li class="li-listing" style="cursor: pointer"><a onclick="show()">Thêm mục chi</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing be-report woktime-listing">
            <%-- Listing --%>

            <div class="wp960 content-wp">
                <div class="row" id="ql" hidden="hidden">
                    <ul class="form-group">
                        <li class="list-group-item">
                            <div class="col-md-6 col-xs-12">
                                <label style="float: left; width: 30%;">Chọn mục chi :</label>
                                <asp:DropDownList ID="ddlItem" CssClass="form-control" runat="server" Style="width: 65%" ClientIDMode="Static"></asp:DropDownList>
                            </div>
                            <div class="col-md-6 col-xs-12">
                                <label style="float: left; width: 35%;">Nhâp số tiền chi:</label>
                                <input type="number" id="giatri" style="width: 60%; margin-left:0px !important" class="form-control" />
                            </div>
                            <div class="col-md-6 col-xs-12" style="margin-top: 10px">
                                <label style="float: left; width: 30%">Ngày chi:</label>
                                <asp:TextBox CssClass="txtDateTime form-control" ID="TxtDateTimeFrom" placeholder="Chọn ngày" Style="width: 65% !important;"
                                    ClientIDMode="Static" runat="server"></asp:TextBox>
                            </div>
                            <div class="col-md-6 col-xs-12" style="margin-top: 10px">
                                <label style="float: left; width: 35%">Salon</label>
                                <asp:DropDownList runat="server" ID="ddlSalon" CssClass="form-control select" ClientIDMode="Static" Width="60%"></asp:DropDownList>
                            </div>
                            <a class="btn btn-primary" style="margin-left: 10px; margin-right: 10px; width: 100px; margin-top: 10px" onclick="save($(this))" data-staff="<%=staff_id %>">Lưu lại</a>
                            <a class="btn btn-default" style="margin-left: 10px; margin-right: 10px; width: 100px; margin-top: 10px" onclick="huy()">Hủy</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-md-12 col-xs-12">
                <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Danh sách mục chi hằng ngày</strong>
                <table class="table-add table-listing">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Mục chi</th>
                            <th>Ngày chi</th>
                            <th>Tổng tiền chi</th>
                            <%--<th>Người nhập</th>--%>
                            <th>Salon</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <%if (obj.Count > 0)
                            { %>
                        <%int i = 1; foreach (var v in obj)
                            { %>
                        <tr>
                            <td><%= i++ %></td>
                            <td><%=v.Item_Name %></td>
                            <td><%= string.Format("{0:dd/MM/yyyy}",v.NgayChi) %></td>
                            <td><%= string.Format("{0:#,0.##}",v.GiaTri) %></td>
                           <%-- <td><%= v.Fullname %></td>--%>
                            <td><%= v.Name %></td>
                            <td>
                                <div>
                                    <a onclick="edit($(this) ,<%=v.Id %>)" data-item="<%=v.Item_Id %>"
                                        data-ngay="<%=string.Format("{0:dd/MM/yyyy}", v.NgayChi) %>" 
                                        data-tien="<%=v.GiaTri %>" data-salon="<%=v.SalonId %>" id="sua" class="fa fa-pencil-square-o edit"></a>
                                    <a onclick="deletel($(this),<%=v.Id %>)" id="xoa" class="fa fa-times del"></a>
                                </div>
                            </td>
                        </tr>
                        <%} %>
                        <%} %>
                    </tbody>
                </table>
            </div>
        </div>
    </asp:Panel>
    <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
    <script src="../../../Assets/js/select2/select2.min.js"></script>
    <script>
        $('.select').select2();
    </script>
    <style>
        .select2-container {
            width: 60% !important;
            margin-top: 5px !important;
        }

        .select2-container--default .select2-selection--single {
            height: 32px !important;
            padding-top: 1px !important;
        }
    </style>
    <script>
        var id = 0;
        $('.txtDateTime').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'vi',
            startDate: '2014/10/10',
            format: 'd/m/Y',
            dateonly: true,
            showHour: false,
            showMinute: false,
            timepicker: false,
            onChangeDateTime: function (dp, $input) {
                //$input.val($input.val().split(' ')[0]);
            }
        });

        function save(This) {
            var Id = id;
            var staff_id = This.attr("data-staff");
            var Item_id = $("#ddlItem").val();
            if (Item_id <= 0) {
                alert("Bạn phải chọn mục chi");
                return;
            }
            var Giatri = $("#giatri").val();
            console.log(Giatri);
            if (Giatri == "") {
                alert("Bạn phải nhập số tiền");
                return;
            }
            if (Giatri.length > 20) {
                alert("Số tiền chi không vượt quá 20 chữ số !");
                return;
            }
            console.log(Giatri);
            var NgayChi = $("#TxtDateTimeFrom").val();
            if (NgayChi == "") {
                alert("Bạn phải chọn ngày chi");
                return;
            }
           

            var salonId = $("#ddlSalon :selected").val();
            if (salonId == 0) {
                alert("Chưa chọn Salon !");
                return;
            }

            //return;
            $.ajax({
                type: "POST",
                url: "/GUI/BackEnd/MarKeting/MarKeting_ChiPhiNgay.aspx/Save",
                data: '{Id:' + Id + ',item_Id : "' + Item_id + '",giaTri:"' + Giatri + '",ngayChi:"' + NgayChi
                    + '", SalonId: "' + salonId + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    console.log(response.d.data);
                    alert(response.d.message);
                    window.location.reload();
                },
                failure: function (response) { alert(response.d); }
            });

        }
        function edit(This, Id) {
            $("#ql").show();
            id = Id;
            $("#ddlItem").val(This.attr("data-item"));
            $("#giatri").val(This.attr("data-tien"));
            $("#TxtDateTimeFrom").val(This.attr("data-ngay"));
            $("#ddlSalon").val(This.attr("data-salon"));
        }
        function deletel(This, Id) {
            var id = Id;

            console.log(id);
            //return;
            var r = confirm('Bạn có chắc chắn muốn xóa hay không');
            if (r) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/MarKeting/MarKeting_ChiPhiNgay.aspx/delete",
                    data: '{Id:' + id + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        console.log(response.d.data);
                        alert(response.d.message);
                        window.location.reload();
                    },
                    failure: function (response) { alert(response.d); }
                });
            }
        }
        function show() {
            $("#ql").show();
        }
        function huy() {
            $("#ql").hide();
        }
    </script>
</asp:Content>
