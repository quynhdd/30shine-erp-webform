﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Marketing_ListingVoucher.aspx.cs" Inherits="_30shine.GUI.BackEnd.MarKeting.Marketing_ListingVoucher" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .select2-container {
                width: 220px !important;
                margin-top: 5px !important;
                text-align: center;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }

            .checkbox label:after {
                content: '' !important;
                display: table !important;
                clear: both !important;
            }

            .checkbox .cr {
                position: relative !important;
                display: inline-block !important;
                border: 1px solid #a9a9a9 !important;
                border-radius: .25em !important;
                width: 1.3em !important;
                height: 1.3em !important;
                float: left !important;
                margin-right: .5em !important;
            }

                .checkbox .cr .cr-icon {
                    position: absolute !important;
                    font-size: .8em !important;
                    line-height: 0 !important;
                    top: 50% !important;
                    left: 15% !important;
                }

            .checkbox label input[type=checkbox] {
                display: none !important;
            }

                .checkbox label input[type=checkbox] + .cr > .cr-icon {
                    opacity: 0 !important;
                }

                .checkbox label input[type=checkbox]:checked + .cr > .cr-icon {
                    opacity: 1 !important;
                }

                .checkbox label input[type=checkbox]:disabled + .cr {
                    opacity: .5 !important;
                }
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Thông tin khách hàng theo chiến dịch MKT &nbsp;&#187; </li>
                        <li class="li-listing active"><a href="/admin/marketing/list-customer-campaign.html">Danh sách KH theo chiến dịch</a></li>
                        <li class="li-pending active">
                            <a href="/admin/marketing/import-excel-customer.html">
                                <div class="pending-1"></div>
                                Cập nhật KH từ Excel(theo SDT)</a>
                        </li>
                        <li class="li-listing active"><a href="/admin/marketing/danh-sach-voucher-codes.html">Danh sách Private Code theo chiến dịch</a></li>
                        <li class="li-pending active">
                            <a href="/admin/marketing/import-excel-voucher-codes.html">
                                <div class="pending-1"></div>
                                Cập nhật mã Voucher(theo Campaign)</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing be-report">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <span style="display: none"></span>
                <!-- Filter -->
                <div class="row row-filter">
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlCampaign" CssClass="form-control select" runat="server" Style="width: 220px; margin-left: 10px;" ClientIDMode="Static"></asp:DropDownList>
                    </div>
                    <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                </div>
                <!-- End Filter -->

                <!-- Row Table Filter -->
                <div class="table-func-panel">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="table-wp">
                            <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                            <table class="table-add table-listing">
                                <thead>
                                    <tr>
                                        <th style="width: 4%">STT</th>
                                        <th style="width: 20%">Ngày tạo</th>
                                        <th>Tên chiến dịch</th>
                                        <th>Mã voucher</th>
                                        <th style="width: 6%">Chức năng</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="RptListData" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td><%#String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(Eval("CreatedTime")))%></td>
                                                <td><%# Eval("CampaignName") %></td>
                                                <td><%# Eval("Code") %></td>
                                                <td style="padding: 7px 15px;" class="map-edit">
                                                    <div class="checkbox">
                                                        <label>
                                                            <input class="" type="checkbox" onclick="update($(this),<%# Eval("Id") %>)" <%# Convert.ToBoolean(Eval("IsActive")) == true ? "Checked=\"True\"" : "" %> />
                                                            <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                        </label>
                                                    </div>
                                                    <div class="edit-wp">
                                                        <a class="elm del-btn" onclick="del(this.parentNode.parentNode.parentNode,'<%# Eval("Id") %>')" href="javascript://" title="Xóa"></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>

                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
            </div>
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <script>
            var URL_CAMPAIGN = "<%= Libraries.AppConstants.URL_API_BILL_SERVICE %>";


            //============================
            // Event update
            //============================
            function update(This, code) {
                let IsActive = This.prop("checked");
                var dataJson = {
                    Id: parseInt(code),
                    Active: IsActive
                };
                if (code != null && code > 0) {
                    $.ajax({
                        type: "PUT",
                        data: JSON.stringify(dataJson),
                        url: URL_CAMPAIGN + "/api/mkt-voucher",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            if (response.status == 1) {
                                showMsgSystem('Cập nhật thành công!', 'msg-system success', 3000);
                            }
                            else {
                                showMsgSystem('Cập nhật thất bại!', 'msg-system warning', 3000);
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var responseText = jqXHR.responseJSON.message;
                            showMsgSystem(`Lỗi: ${responseText}`, 'msg-system warning', 3000);
                        }
                    });
                }
                else {
                    showMsgSystem('Cập nhật thất bại!', 'msg-system warning', 3000);
                }
            }

            //============================
            // Event delete
            //============================
            function del(This, code) {
                var code = code || null,
                    Row = This;
                if (!code) return false;
                let campaignName = $("#ddlCampaign option:selected").text();
                // show EBPopup
                $(".confirm-yn").openEBPopup();
                $("#EBPopup .confirm-yn-text").text(`Bạn có chắc chắn muốn xoá mã của chiến dịch: [ ${campaignName}] ?`);
                $("#EBPopup .yn-yes").bind("click", function () {
                    $.ajax({
                        type: "DELETE",
                        url: URL_CAMPAIGN + `/api/mkt-voucher?voucherId=${code}`,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            if (response.status === 1) {
                                delSuccess();
                                Row.remove();
                            } else {
                                delFailed();
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            var responseText = jqXHR.responseJSON.message;
                            $("#EBPopup .confirm-yn-text").text(`Lỗi: ${responseText}`);
                        }
                    });
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
            }
        </script>
    </asp:Panel>
</asp:Content>

