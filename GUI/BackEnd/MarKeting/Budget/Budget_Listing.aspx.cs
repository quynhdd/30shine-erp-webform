﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Script.Serialization;
using ExportToExcel;
using Excel;
using LinqKit;
using System.Web.Services;
using System.Data.Entity.Migrations;
using Project.Model.Structure;
using static Library.Class;

namespace _30shine.GUI.BackEnd.MarKeting.Budget
{
    public partial class Budget_Listing : System.Web.UI.Page
    {
        Solution_30shineEntities db = new Solution_30shineEntities();
        //public static int Satff_Id= Convert.ToInt32(Session["SalonId"]);
        protected int staff_id;
        public List<Marketting_NganSach> objNS = new List<Marketting_NganSach>();
        public decimal ConLai = 0;
        public CultureInfo culture = new CultureInfo("vi-VN");


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
                TxtDateTimeFrom.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
                TimeForm.Text = FirstDayOfMonth;
                TxtDateTimeTo.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
            }
        }

        public void LoadData()
        {
            var timeFrom = Convert.ToDateTime(TimeForm.Text, culture);
            DateTime timeTo = new DateTime();
            if (TxtDateTimeTo.Text != "")
            {
                timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
            }
            else
            {
                timeTo = timeFrom.AddDays(1);
            }

            objNS = db.Marketting_NganSach.Where(p => p.Ngay >= timeFrom && p.Ngay <= timeTo).ToList();
            var list = db.Store_Marketing_NganSachConLai().ToList();
            if (list.Count > 0)
            {
                foreach (var item in list)
                {
                    try
                    {
                        ConLai = Convert.ToDecimal(item.Value);
                    }
                    catch (Exception ex)
                    {
                        //
                    }
                }
            }            
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            LoadData();
        }

        [WebMethod]
        public static object SaveData(int Id, string Ngay, string SoTien, string GhiChu)
        {
            var msg = new cls_message();
            CultureInfo culture = new CultureInfo("vi-VN");
            using (var db = new Solution_30shineEntities())
            {
                if (Id == 0)
                {
                    var objNS = new Marketting_NganSach();
                    objNS.Ngay = Convert.ToDateTime(Ngay, culture);
                    objNS.SoTien = decimal.Parse(SoTien);
                    objNS.GhiChu = GhiChu;
                    objNS.IsDelete = false;
                    objNS.Publish = true;
                    objNS.CreateDate = DateTime.Now;
                    db.Marketting_NganSach.AddOrUpdate(objNS);

                }
                else
                {
                    var budget = db.Marketting_NganSach.Single(p => p.Id == Id);
                    budget.Ngay = Convert.ToDateTime(Ngay, culture);
                    budget.SoTien = decimal.Parse(SoTien);
                    budget.GhiChu = GhiChu;
                    budget.IsDelete = false;
                    budget.Publish = true;
                    budget.CreateDate = DateTime.Now;
                    db.Marketting_NganSach.AddOrUpdate(budget);
                }
                db.SaveChanges();
                msg.status = "success";
                msg.message = "Dữ liệu đã được lưu!";
            }
            return msg;
        }

        [WebMethod]
        public static object DeleteData(int Id)
        {
            using (var db = new Solution_30shineEntities())
            {
                var msg = new cls_message();
                if (Id > 0)
                {
                    var objNS = db.Marketting_NganSach.Single(p => p.Id == Id);
                    objNS.IsDelete = true;
                    db.Marketting_NganSach.AddOrUpdate(objNS);
                    db.SaveChanges();
                    msg.status = "success";
                    msg.message = "Dữ liệu đã được lưu!";
                }

                return msg;
            }
        }
    }
}