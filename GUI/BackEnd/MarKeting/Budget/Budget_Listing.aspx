﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Budget_Listing.aspx.cs" Inherits="_30shine.GUI.BackEnd.MarKeting.Budget.Budget_Listing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .edit { margin: 8px; width: 15px; }
            .del { margin: 8px; width: 15px; }
            .del:hover { color: red; }
            .edit:hover { color: seagreen; }
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý ngân sách &nbsp;&#187; </li>
                        <li class="li-listing" style="cursor: pointer"><a onclick="show()">Thêm ngân sách</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing be-report woktime-listing">
            <%-- Listing --%>

            <div class="wp960 content-wp">
                <div class="row" id="ql" hidden="hidden">
                    <ul class="form-group">
                        <li class="list-group-item style-budget">
                            <div class="col-md-12 col-xs-12" style="margin-top: 10px">
                                <label>Ngày:</label>
                                <asp:TextBox CssClass="txtDateTime form-control" ID="TxtDateTimeFrom" placeholder="Chọn ngày"
                                    ClientIDMode="Static" runat="server"></asp:TextBox>
                            </div>
                            <div class="col-md-12 col-xs-12" style="margin-top: 10px">
                                <label>Nhâp số tiền:</label>
                                <asp:TextBox CssClass="form-control" ID="SoTien" placeholder="Nhập số tiền"
                                    ClientIDMode="Static" runat="server" TextMode="Number"></asp:TextBox>
                            </div>
                            <div class="col-md-12 col-xs-12" style="margin-top: 10px">
                                <label>Ghi chú</label>
                                <asp:TextBox runat="server" CssClass="form-control" ID="txtGhiChu" TextMode="MultiLine" ClientIDMode="Static"></asp:TextBox>
                            </div>
                            <a class="btn btn-primary" style="margin-left: 128px; margin-right: 10px; width: 100px; margin-top: 10px" onclick="SaveBudget($(this))">Lưu lại</a>
                            <a class="btn btn-default" style="margin-left: 10px; margin-right: 10px; width: 100px; margin-top: 10px" onclick="huy()">Hủy</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-md-12 col-xs-12">
                <table class="table-add table-listing" style="width: 20%; margin-bottom:10px">
                    <thead>
                        <tr>
                            <th>Ngân sách còn lại</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                        <tr>
                            <td><%= string.Format("{0:#,0.##}", ConLai) %></td>
                        </tr>
                    </tbody>
                </table>

                <div class="row" style="float:left; width:100%">
                    <div class="filter-item">
                        <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="time-wp">
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TimeForm" placeholder="Từ ngày" Style="margin-left: 10px!important"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ValidateTime" ControlToValidate="TimeForm" runat="server" CssClass="fb-cover-error" Text="Vui lòng chọn thời gian!"></asp:RequiredFieldValidator>
                        </div>
                        <strong class="st-head" style="margin-left: 10px;">
                            <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                        </strong>
                        <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeTo" placeholder="Đến ngày" Style="margin-left: 10px!important"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                        <br />

                    </div>
                    <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <a href="javascript:void(0);" onclick="location.href=location.href" class="st-head btn-viewdata">Reset Filter</a>

                </div>
                <strong class="st-head" style="margin-right: 10px; width: 100%;"><i class="fa fa-clock-o"></i>Danh sách ngân sách</strong>
                
                <table class="table-add table-listing">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Ngày</th>
                            <th>Số tiền</th>
                            <th>Ghi chú</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <%if (objNS.Count > 0)
                            { %>
                        <%int i = 1; foreach (var v in objNS)
                            { %>
                        <tr>
                            <td><%= i++ %></td>
                            <td><%= string.Format("{0:dd/MM/yyyy}",v.Ngay) %></td>
                            <td><%= string.Format("{0:#,0.##}",v.SoTien) %></td>
                            <td><%= v.GhiChu %></td>
                            <td class="map-edit">
                                <div class="edit-wp">
                                    <a onclick="editBudget($(this), <%=v.Id %>)" data-item="<%=v.Id %>"
                                        data-ngay="<%=string.Format("{0:dd/MM/yyyy}", v.Ngay) %>"
                                        data-tien="<%=v.SoTien %>" data-ghichu="<%= v.GhiChu %>"
                                        id="btnEdit" class="elm edit-btn"></a>
                                    <a onclick="deletelBudget($(this),<%=v.Id %>)" id="btnDelete" class="elm del-btn"></a>
                                </div>
                            </td>
                        </tr>
                        <%} %>
                        <%} %>
                    </tbody>
                </table>
            </div>
        </div>
        <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
    </asp:Panel>
    <style>
        .style-budget label { float: left; width: 100px; }
        .style-budget input, textarea { width: 300px !important; float: left; margin-left: 12px; }
    </style>
    <script>
        var id = 0;
        $('.txtDateTime').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'vi',
            startDate: '2014/10/10',
            format: 'd/m/Y',
            dateonly: true,
            showHour: false,
            showMinute: false,
            timepicker: false,
            onChangeDateTime: function (dp, $input) {
                //$input.val($input.val().split(' ')[0]);
            }
        });

        function SaveBudget(This) {
            var Id = parseInt(id);

            var ghichu = $("#txtGhiChu").val();

            var SoTien = $("#SoTien").val();
            if (SoTien == "") {
                alert("Bạn phải nhập số tiền");
                return;
            }
            var Ngay = $("#TxtDateTimeFrom").val();
            if (Ngay == "") {
                alert("Bạn phải chọn ngày chi");
                return;
            }

            console.log(SoTien, Ngay);
            console.log(Id);

            $.ajax({
                type: "POST",
                url: "/GUI/BackEnd/MarKeting/Budget/Budget_Listing.aspx/SaveData",
                data: '{Id:"' + Id + '", Ngay:"' + Ngay + '", SoTien:"' + SoTien + '", GhiChu:"' + ghichu + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    console.log(response.d.data);
                    alert(response.d.message);
                    window.location.reload();
                },
                failure: function (response) { alert(response.d); }
            });

        }

        function editBudget(This, Id) {
            $("#ql").show();
            id = Id;

            $("#SoTien").val(This.attr("data-tien"));
            $("#TxtDateTimeFrom").val(This.attr("data-ngay"));
            $("#txtGhiChu").val(This.attr("data-ghichu"));
            console.log(id);
        }

        function deletelBudget(This, Id) {
            var id = parseInt(Id);

            console.log(id);
            //return;
            var r = confirm('Bạn có chắc chắn muốn xóa hay không');
            if (r) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/MarKeting/Budget/Budget_Listing.aspx/DeleteData",
                    data: '{Id:' + id + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        console.log(response.d.data);
                        alert(response.d.message);
                        window.location.reload();
                    },
                    failure: function (response) { alert(response.d); }
                });
            }
        }

        function show() {
            $("#ql").show();
        }

        function huy() {
            $("#ql").hide();
            id = 0;

            $("#SoTien").val("");
            $("#TxtDateTimeFrom").val("");
            $("#txtGhiChu").val("");
        }
    </script>
</asp:Content>
