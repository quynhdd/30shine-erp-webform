﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Marketing_Excel.aspx.cs" Inherits="_30shine.GUI.BackEnd.MarKeting.Marketing_Excel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
           <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Thông tin khách hàng theo chiến dịch MKT &nbsp;&#187; </li>
                        <li class="li-listing active"><a href="/admin/marketing/list-customer-campaign.html">Danh sách KH theo chiến dịch</a></li>
                        <li class="li-pending active">
                            <a href="/admin/marketing/import-excel-customer.html">
                                <div class="pending-1"></div>
                                Cập nhật KH từ Excel(theo SDT)</a>
                        </li>
                         <li class="li-listing active"><a href="/admin/marketing/danh-sach-voucher-codes.html">Danh sách Private Code theo chiến dịch</a></li>
                          <li class="li-pending active">
                            <a href="/admin/marketing/import-excel-voucher-codes.html">
                                <div class="pending-1"></div>
                                Cập nhật mã Voucher(theo Campaign)</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report">
            <%-- Listing --%>
            <div class="wp960 content-wp">

                <!-- End Filter -->
                <div class="pending-content-wrap" style="width: 1000px; margin: 0 auto;">
                    <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                    <div class="row">
                        <div class="filter-item">
                            <strong class="st-head" style="margin-right: 5px;">Chọn chiến dịch</strong>
                            <asp:DropDownList ID="ddlCampaign" runat="server" Style="width: 190px;" ClientIDMode="Static"></asp:DropDownList>
                        </div>
                        <a style="float:right; color:#4db8ff;text-decoration:underline !important;margin-right:10%;margin-top:10px;" href="/TemplateFile/ImportExcelMarketing/file_mau_import_khach_hang.xlsx" class="cls_tempfile">Tải File Excel Import MẪu</a>
                    </div>
                </div>
                <div class="content-wrapper">
                    <div id="dropZoneExcel">
                        <strong>Click chọn<br />
                            hoặc
                            <br />
                            kéo File Excel vào đây!</strong>
                        <p id="FileName"></p>
                        <input type="file" name="InputExcel" style="display: none" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
                    </div>
                    <div id="updateStaffBtn-wp">
                        <button id="UpdateStaffBtn" class="btn btn-complete" disabled="disabled">Cập nhật khách hàng</button>
                    </div>
                </div>

                <style>
                    .content-wrapper {
                        clear: left;
                    }

                    a#SelectFileBtn {
                    }

                    #dropZoneExcel {
                        border: 2px dashed #ccc;
                        width: 800px;
                        height: 320px;
                        padding: 120px;
                        text-align: center;
                        margin: 20px auto;
                    }

                    #UpdateStaffBtn {
                        display: inline-block;
                        margin-top: 20px
                    }

                    #FileName {
                        margin-top: 6px;
                    }

                    #updateStaffBtn-wp {
                        float: none;
                        cleat: both;
                        text-align: center;
                    }

                    .color-red {
                        color: red;
                    }

                    .text-bold {
                        font-weight: bold;
                    }
                </style>

                <script type="text/javascript">

                    var URL_API_BILL_SERVICE = "<%= Libraries.AppConstants.URL_API_BILL_SERVICE %>";
                    var DataFromExcel = [];
                    var TableOfError = $('#TableOfError');
                    var UpdateBtn = $('#UpdateStaffBtn');
                    jQuery(document).ready(function ($) {
                        $('#dropZoneExcel').proccessExcel({
                            dropAble: true,
                            onDataCollected: function (File, Data) {
                                $('#<%=MsgSystem.ClientID %>').html("");
                                $('#<%=MsgSystem.ClientID %>').removeClass("warning");
                                $('#<%=MsgSystem.ClientID %>').removeClass("success");
                                $('#FileName').empty().text(File.name);
                                $('#UpdateStaffBtn').prop('disabled', false);
                            },
                            ajax: {
                                trigger: "#UpdateStaffBtn.click",
                                url: URL_API_BILL_SERVICE + "/api/mkt-campaign-customer/insert",
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function (response) {

                                    if (response.status == 1) {
                                        $('#<%=MsgSystem.ClientID %>').html("Cập nhật khách hàng thành công !");
                                        $('#<%=MsgSystem.ClientID %>').addClass("success");
                                        $("html, body").animate({ scrollTop: 0 }, "slow");
                                    } else {
                                        $('#<%=MsgSystem.ClientID %>').html("Cập nhật khách hàng thất bại do file excel không đúng định dạng !");
                                        $('#<%=MsgSystem.ClientID %>').addClass("warning");
                                        $("html, body").animate({ scrollTop: 0 }, "slow");
                                        //var RES = "{data: [{CustomerPhone: '', CustomerName: '' }]}";
                                        var RES = JSON.parse('{ "data" : [' +
                                            '{ "CustomerPhone":"" , "CustomerName":"" } ]}');
                                        if (!confirm("File tải lên không đúng mẫu, bạn có thể tải mẫu về và chỉnh lại !")) {
                                            return false;
                                        }
                                        // định dạng mẫu
                                        var dataStructure = function () {
                                            return {
                                                CustomerPhone: { w: 120, v: "", p: 0 },
                                                CustomerName: { w: 120, v: "", p: 1 },
                                            };
                                        }
                                        // nếu sai định dạng cho khách hàng lựa chọn file download về
                                        this.ExportToExcel(this.ToWorkSheetData(RES.data, new dataStructure(), ['FieldGetError']), 'xlsx', function (workBook, workSheet) {
                                            var _this = this;
                                            var cols = new dataStructure();
                                            workSheet['!cols'] = [];
                                            for (var key in RES.data[0]) {
                                                if (typeof cols[key] != "undefined") {
                                                    workSheet['!cols'].push({ wpx: cols[key].w });
                                                }
                                            }
                                            $.each(RES.data, function (r, data) {
                                                var regex = new RegExp('^\\d+$');
                                                if (data.FieldGetError.length > 0) {
                                                    for (var f in data.FieldGetError) {
                                                        var fObj = cols[data.FieldGetError[f]];
                                                        if (typeof fObj != "undefined") {
                                                            var cell = _this.ToCellName(r + 1, fObj.p);
                                                            XLSX.utils.cell_add_comment(workSheet[cell], "Data lỗi!", "Sheet");
                                                        }
                                                    }
                                                }

                                                var c = 0;
                                                for (var i in data) {
                                                    if (regex.test(data[i])) {
                                                        var cell = _this.ToCellName(r + 1, c);
                                                        //XLSX.utils.cell_set_number_format(workSheet[cell], "0");
                                                    }
                                                    c++;
                                                }
                                            });
                                        });
                                    }

                                },
                            },

                        });
                    });

                </script>
                <!-- Hidden Field-->
                <!-- END Hidden Field-->
            </div>
            <%-- END Listing --%>
            <!-- Loading-->
            <div class="page-loading">
                <p>Vui lòng đợi trong giây lát...</p>
            </div>
        </div>
        <script src="../../../Assets/js/sheet/shim.js"></script>
        <script src="../../../Assets/js/sheet/xlsx-0.11.7.min.js"></script>
        <script src="../../../Assets/js/sheet/Blob.js"></script>
        <script src="../../../Assets/js/sheet/FileSaver.js"></script>
        <script src="../../../Assets/js/sheet/swfobject.min.js"></script>
        <script src="../../../Assets/js/sheet/downloadify.min.js"></script>
        <script src="../../../Assets/js/sheet/fn.sheet1.js"></script>
        <%--<script src="../../../Assets/js/sheet/query.js"></script>--%>
    </asp:Panel>
</asp:Content>
