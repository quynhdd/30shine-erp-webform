﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="ToolUpdateLikeStyleMaster.aspx.cs" Inherits="_30shine.GUI.BackEnd.MarKeting.ToolUpdateLikeStyleMaster" %>

<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <link href="/Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="/Assets/js/select2/select2.min.js"></script>
        <style>
            .table-wp {
                background: #f5f5f5;
                float: left;
                width: 100%;
                padding: 15px;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                -webkit-box-shadow: 0px 0px 3px 0px #ddd;
                -moz-box-shadow: 0px 0px 3px 0px #ddd;
                box-shadow: 0px 0px 3px 0px #ddd;
            }

            .tr-skill {
                display: none;
            }

            .tr-show {
                display: table-row;
            }

            .customer-add .table-add tr td.right .fb-cover-error {
                top: 0;
                right: auto;
            }

            .staff-servey-mark td input[type="radio"] {
                top: 4px !important;
                margin-left: 10px;
            }

            .staff-servey-mark span {
                height: 28px !important;
                line-height: 28px !important;
            }

            .staff-servey-mark td label {
                padding-left: 10px;
                float: left;
                font-weight: normal;
                font-family: Roboto Condensed Bold;
            }

            .customer-add .table-add td span.field-wp {
                position: relative;
                margin-top: 5px;
            }

            #success-alert {
                top: 100px;
                right: 10px;
                position: fixed;
                width: 20% !important;
                z-index: 2000;
            }
        </style>
        <div class="alert alert-danger" id="success-alert">
            <strong>Cảnh báo: </strong>
            <span id="msg-alert"></span>
        </div>
        <div class="wp customer-add admin-product-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="table-wp">
                    <table class="table-add admin-product-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Tool auto like StyleMaster</strong></td>
                                <td></td>
                            </tr>
                            <tr class="tr-margin" style="height: 20px;"></tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Like đã tặng /Tổng</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <asp:TextBox ID="TotalLikeNumber" CssClass="form-control" runat="server" ClientIDMode="Static"></asp:TextBox>
                                        </div>
                                        <div class="div_col">
                                            <asp:TextBox ID="TotalLike" CssClass="form-control" class="number-only" runat="server" ClientIDMode="Static" onkeypress="return ValidateKeypress(/\d/,event);" onchange="UpdateTotalLike();"></asp:TextBox>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Tổng số bài viết trong tháng</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="PostNumber" CssClass="form-control" runat="server" ClientIDMode="Static"></asp:TextBox>

                                    </span>
                                </td>
                            </tr>
                            <tr class="staff-servey-mark">
                                <td class="col-xs-3 left"><span>Lựa chọn top bài viết</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <span class="field-wp" id="LstradiobttList">
                                                <asp:RadioButtonList ID="Order" runat="server" ClientIDMode="Static" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="Top đầu" Value="True" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="Top cuối" Value="False"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </span>
                                        </div>
                                        <div class="div_col">
                                            <asp:TextBox ID="TopPost" onkeyup="RemoveColor()" CssClass="form-control" class="number-only" runat="server" ClientIDMode="Static" placeholder="Nhập top bài viết" onkeypress="return ValidateKeypress(/\d/,event);"></asp:TextBox>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Số like tặng</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="NumberLike" onkeyup="RemoveColor()" class="form-control" runat="server" ClientIDMode="Static" onkeypress="return ValidateKeypress(/\d/,event);" placeholder="Nhập tổng số like cho những bài viết trên"></asp:TextBox>

                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-send">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <asp:Panel ID="BtnSend" CssClass="btn-send" runat="server" ClientIDMode="Static">Hoàn Tất</asp:Panel>
                                        <asp:Button ID="BtnFakeSend" runat="server" Text="Hoàn tất" OnClick="Update" ClientIDMode="Static" Style="display: none;"></asp:Button>
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <%-- end Add --%>
        </div>
        <style>
            .field-wp .div_col {
                width: 49%;
                float: left;
            }

                .field-wp .div_col:first-child {
                    margin-right: 10px;
                }

            .customer-add .table-add tr.tr-field-ahalf .div_col input, .customer-add .table-add tr.tr-field-ahalf .div_col textarea, .customer-add .table-add tr.tr-field-ahalf .div_col select {
                width: 100%;
            }

            .select2-container--default .select2-selection--single .select2-selection__rendered {
                line-height: inherit;
            }

            .span_note {
                color: red;
                width: inherit;
            }

            .cls_gender {
                width: 120px;
                float: left;
                height: 36px;
                background: #dfdfdf;
                margin-left: 10px;
                text-align: center;
                line-height: 36px;
                cursor: pointer;
            }

                .cls_gender:first-child {
                    margin-left: 0px;
                }

                .cls_gender.active {
                    background: #fcd72a;
                }

            .cls_hoso {
                width: 120px;
                float: left;
                height: 36px;
                background: #dfdfdf;
                margin-left: 10px;
                text-align: center;
                line-height: 36px;
                cursor: pointer;
            }

                .cls_hoso:first-child {
                    margin-left: 0px;
                }

                .cls_hoso.active {
                    background: #fcd72a;
                }
        </style>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                //hide noti
                $("#success-alert").hide();
                var qs = getQueryStrings();
                showMsgSystem(qs["msg_update_message"], qs["msg_update_status"])
            });

            //check validate
            $("#BtnSend").bind("click", function () {
                var kt = true;
                var totalData = 0;
                var error = 'Vui lòng kiểm tra lại thông tin! ';
                var totallikenumber = $("#TotalLikeNumber").val();
                var totallike = $("#TotalLike").val();
                var toppost = $("#TopPost").val();
                var numberlike = $("#NumberLike").val();
                if (toppost <= 0 || toppost == '') {
                    kt = false;
                    error += "[Bạn phải nhập top bài viết muốn tặng like], ";
                    $('#TopPost').css("border-color", "red");
                }
                if (numberlike <= 0 || numberlike == '') {
                    kt = false;
                    error += "[Bạn phải số like muốn tặng]";
                    $('#NumberLike').css("border-color", "red");
                }
                if (totallikenumber == '') {
                    totallikenumber = 0;
                    totallikenumber = totallikenumber.toString().trim() != "" ? parseInt(totallikenumber) : 0;
                    numberlike = numberlike.toString().trim() != "" ? parseInt(numberlike) : 0;
                    totalData = totallikenumber + numberlike;
                }
                if (totallikenumber != '') {
                    totallikenumber = totallikenumber.toString().trim() != "" ? parseInt(totallikenumber) : 0;
                    numberlike = numberlike.toString().trim() != "" ? parseInt(numberlike) : 0;
                    totalData = totallikenumber + numberlike;
                }
                if (toppost > numberlike) {
                    kt = false;
                    error += "[Top bài viết lớn hơn số like tặng! Vui lòng kiểm tra lại]";
                    $('#TopPost').css("border-color", "red");
                }
                if (totalData > totallike) {
                    kt = false;
                    error += "[Số like tặng đã vượt qua tổng số like trong tháng! Vui lòng kiểm tra lại]";
                    $('#NumberLike').css("border-color", "red");
                }
                if (!kt) {
                    $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                        $("#success-alert").slideUp(2000);
                    });
                    $("#msg-alert").text(error);
                }
                else {
                    //call button
                    $("#BtnFakeSend").click();
                }

                return kt;
            });

            //onchange update totallike in table tbl_Config
            function UpdateTotalLike() {
                var key = 'auto_like';
                var value = $("#TotalLike").val();
                $.ajax({
                    type: "POST",
                    contentType: "application/json;charset:UTF-8",
                    datatype: "JSON",
                    url: "/GUI/BackEnd/MarKeting/ToolUpdateLikeStyleMaster.aspx/UpdateConfig",
                    data: '{value:"' + value + '", key:"' + key + '"}',
                    success: function (response, textStatus, xhr) {
                        var kq = JSON.parse(response.d);
                        if (kq.success) {
                            $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                                $("#success-alert").slideUp(2000);
                            });
                            $("#msg-alert").text("Cập nhật thành công!");
                        }
                    },
                    error: function (jqXhr, textStatus, errorMessager) {
                        console.log(errorMessager + "\n" + jqXhr.responseJSON);
                        alert("Có lỗi xảy ra!Vui lòng liên hệ với nhóm phát triển!");
                    }
                });
            }

            ///remove color
            function RemoveColor() {
                $(".remove-color").css("border-color", "#ddd");
            }

            //Check chỉ được phép nhập số 
            function ValidateKeypress(numcheck, e) {
                var keynum, keychar, numcheck;
                if (window.event) {
                    keynum = e.keyCode;
                }
                else if (e.which) {
                    keynum = e.which;
                }
                if (keynum == 8 || keynum == 127 || keynum == null || keynum == 9 || keynum == 0 || keynum == 13) return true;
                keychar = String.fromCharCode(keynum);
                var result = numcheck.test(keychar);
                return result;
            }
        </script>
    </asp:Panel>
</asp:Content>
