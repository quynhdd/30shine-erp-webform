﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.MarKeting
{
    public partial class MarKeting_ListingCampaign : System.Web.UI.Page
    {
        /// <summary>
        /// Campaign Listting
        /// Author: QuynhDD
        /// Create: 28/02/2018
        /// </summary>

        protected Paging PAGING = new Paging();
        public CultureInfo culture = new CultureInfo("vi-VN");
        private bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected bool isRoot = false;
        protected int departmentId;
        protected string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
        protected string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
        /// <summary>
        /// set permission
        /// </summary>
        public void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        /// <summary>
        /// ExecuteByPermission
        /// </summary>
        private void ExecuteByPermission()
        {

            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = FirstDayOfMonth;
                TxtDateTimeTo.Text = Today;
                GetData();
            }
            RemoveLoading();
        }

        /// <summary>
        /// Get data 
        /// </summary>
        private void GetData()
        {
            try
            {
                var clsSearch = new cls_Search();
                clsSearch.startDate = new DateTime();
                clsSearch.endDate = new DateTime();
                if (TxtDateTimeFrom.Text != "")
                {
                    clsSearch.startDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                    if (TxtDateTimeTo.Text != "")
                    {
                        if (TxtDateTimeFrom.Text == TxtDateTimeTo.Text)
                        {
                            clsSearch.endDate = clsSearch.startDate;
                        }
                        else
                        {
                            clsSearch.endDate = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                        }
                    }
                    else
                    {
                        clsSearch.endDate = clsSearch.startDate;
                    }
                }

                using (var client = new HttpClient())
                {
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    client.BaseAddress = new Uri(Libraries.AppConstants.URL_API_BILL_SERVICE);
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var response = client.PostAsJsonAsync("/api/mkt-campaign/get-list", clsSearch).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var result = response.Content.ReadAsStringAsync().Result;
                        var data = response.Content.ReadAsAsync<ResponseData>().Result.data.ToList();
                        if (data != null)
                        {
                            Bind_Paging(data.Count());
                            Rpt.DataSource = data.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                            Rpt.DataBind();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                TriggerJsMsgSystem_temp(this, "Lỗi:" + e.Message.Replace(@"'", @"\'"), "msg-system warning", 15000);
            }
        }

        /// <summary>
        /// button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _BtnClick(object sender, EventArgs e)
        {
            GetData();
            RemoveLoading();
        }

        /// <summary>
        /// Paging
        /// </summary>
        /// <param name="TotalRow"></param>
        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        /// <summary>
        /// Get_TotalPage
        /// </summary>
        /// <param name="TotalRow"></param>
        /// <returns></returns>
        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }
        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
        /// <summary>
        /// Trả thông báo về client 
        /// </summary>
        /// <param name="_OBJ"></param>
        /// <param name="msg"></param>
        /// <param name="status"></param>
        /// <param name="duration"></param>
        public void TriggerJsMsgSystem_temp(Page _OBJ, string msg, string status, int duration)
        {
            ScriptManager.RegisterStartupScript(_OBJ, _OBJ.GetType(), "Message System", "showMsgSystem('" + msg + "','" + status + "'," + duration + ");", true);
        }
        /// <summary>
        /// author: QuynhDD
        /// </summary>
        public class ResponseData
        {
            public int status { get; set; }
            public string message { get; set; }
            public List<Origin> data { get; set; }
            public ResponseData()
            {
                data = null;
                status = 0;
                message = "";
            }
        }

        /// <summary>
        /// class origin
        /// </summary>
        public class Origin
        {
            public bool isbookingPublish { get; set; }
            public string typeCampaignName { get; set; }
            public int type { get; set; }
            public int id { get; set; }
            public string name { get; set; }
            public string label { get; set; }
            public DateTime startDate { get; set; }
            public DateTime endDate { get; set; }
            public int maxUsage { get; set; }
            public String description { get; set; }
            public bool? isActive { get; set; }
        }

        /// <summary>
        /// cls 
        /// </summary>
        public class cls_Search
        {
            public DateTime startDate { get; set; }
            public DateTime endDate { get; set; }
        }
    }
}