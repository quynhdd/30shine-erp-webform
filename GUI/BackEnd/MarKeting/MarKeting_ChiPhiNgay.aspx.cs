﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Script.Serialization;
using ExportToExcel;
using Excel;
using LinqKit;
using System.Web.Services;
using System.Data.Entity.Migrations;
using Project.Model.Structure;


namespace _30shine.GUI.BackEnd.MarKeting
{
    public partial class MarKeting_ChiPhiNgay : System.Web.UI.Page
    {
        Solution_30shineEntities db = new Solution_30shineEntities();
        //public static int Satff_Id= Convert.ToInt32(Session["SalonId"]);
        protected int staff_id;
        protected List<Store_Marketting_Chiphingay_Result> obj;
        UIHelpers helper = new UIHelpers();
        protected bool Perm_ViewAllData = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //helper.Bind_Salon(ddlSalon, 0, true);
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
                binitem();
                loaddata();
            }

        }

        public void loaddata()
        {
            staff_id = Convert.ToInt32(Session["User_Id"]);
            var msg = new CommonClass.cls_message();
            obj = db.Store_Marketting_Chiphingay().ToList();
            if (obj != null)
            {
                msg.success = true;
                msg.message = "hiển thị thành công";
            }
            else
            {
                msg.success = false;
                msg.message = "lỗi !, Vui lòng liên hệ với nhà phát triển";
            }

        }

        public void binitem()
        {
            //var SalonId = Convert.ToInt32(Session["SalonId"]);
            var lst = db.Marketing_ChiphiItem.Where(p => p.IsDelete == false).OrderBy(p => p.Id).ToList();
            var item = new Marketing_ChiphiItem();
            item.Item_Name = "Chọn mục chi";
            item.Id = 0;
            lst.Insert(0, item);
            ddlItem.DataTextField = "Item_Name";
            ddlItem.DataValueField = "Id";
            ddlItem.SelectedIndex = 0;

            ddlItem.DataSource = lst;
            ddlItem.DataBind();
        }

        /// <summary>
        /// thêm 
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="item_Id"></param>
        /// <param name="giaTri"></param>
        /// <param name="ngayChi"></param>
        /// <returns></returns>
        [WebMethod]
        public static object Save(int Id, int item_Id, decimal giaTri, string ngayChi, int SalonId)
        {
            //var SalonId = Convert.ToInt32(Session["SalonId"]);
            using (var db = new Solution_30shineEntities())
            {              
                CultureInfo culture = new CultureInfo("vi-VN");
                var msg = new CommonClass.cls_message();
                if (item_Id > 0)
                {
                    if (Id <= 0)
                    { 
                        var obj = new Marketing_Chiphingay();
                        obj.Item_Id = item_Id;
                        obj.GiaTri = giaTri;
                        obj.NgayChi = Convert.ToDateTime(ngayChi, culture);
                        obj.SalonId = SalonId;
                        obj.IsDelete = false;
                        obj.CreateDate = DateTime.Now;
                        db.Marketing_Chiphingay.AddOrUpdate(obj);
                        db.SaveChanges();

                        //var res = db.store_Marketing_Tong_Chi_Tung_Ngay(obj.NgayChi).FirstOrDefault();
                        //if (res != null)
                        //{
                        //    var tong = db.Marketing_TongChiTungNgay.FirstOrDefault(p => p.NgayChi == obj.NgayChi);
                        //    if (tong != null)
                        //    {
                        //        tong.TongChi = res.Tongngaychi;
                        //        tong.IsDelete = false;
                        //        db.Marketing_TongChiTungNgay.AddOrUpdate(tong);
                        //        db.SaveChanges();
                        //    }
                        //    else
                        //    {
                        //        var tongchi = new Marketing_TongChiTungNgay();
                        //        tongchi.NgayChi = obj.NgayChi;
                        //        tongchi.TongChi = res.Tongngaychi;
                        //        tongchi.IsDelete = false;
                        //        db.Marketing_TongChiTungNgay.AddOrUpdate(tongchi);
                        //        db.SaveChanges();

                        //    }
                        //}

                        msg.success = true;
                        msg.message = "Thêm thành công";
                        msg.data = obj;


                    }
                    else
                    {
                        var obj = db.Marketing_Chiphingay.FirstOrDefault(w => w.Id == Id && w.IsDelete == false);
                        obj.Item_Id = item_Id;
                        obj.GiaTri = giaTri;
                        obj.NgayChi = Convert.ToDateTime(ngayChi, culture);
                        obj.SalonId = SalonId;
                        obj.IsDelete = false;
                        obj.CreateDate = DateTime.Now;
                        db.Marketing_Chiphingay.AddOrUpdate(obj);
                        db.SaveChanges();
                        //var res = db.store_Marketing_Tong_Chi_Tung_Ngay(obj.NgayChi).FirstOrDefault();
                        //if (res != null)
                        //{
                        //    var tong = db.Marketing_TongChiTungNgay.FirstOrDefault(p => p.NgayChi == obj.NgayChi);
                        //    if (tong != null)
                        //    {
                        //        tong.TongChi = res.Tongngaychi;
                        //        tong.IsDelete = false;
                        //        db.Marketing_TongChiTungNgay.AddOrUpdate(tong);
                        //        db.SaveChanges();
                        //    }
                        //    else
                        //    {
                        //        var tongchi = new Marketing_TongChiTungNgay();
                        //        tongchi.NgayChi = obj.NgayChi;
                        //        tongchi.TongChi = res.Tongngaychi;
                        //        tongchi.IsDelete = false;
                        //        db.Marketing_TongChiTungNgay.AddOrUpdate(tongchi);
                        //        db.SaveChanges();

                        //    }
                        //}
                        msg.success = true;
                        msg.message = "Sửa thành công";
                        msg.data = obj;
                    }

                }
                else
                {
                    msg.success = false;
                    msg.message = "Lỗi!. vui lòng liên hệ với nhà phát triển để được giải đáp";
                }

                return msg;
            }
        }

        /// <summary>
        /// xóa  
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [WebMethod]
        public static object delete(int Id)
        {
            using (var db = new Solution_30shineEntities())
            {
                var msg = new CommonClass.cls_message();
                if (Id > 0)
                {
                    var obj = db.Marketing_Chiphingay.FirstOrDefault(w => w.Id == Id && w.IsDelete == false);
                    if (obj != null)
                    {
                        obj.IsDelete = true;
                        obj.ModifileDate = DateTime.Now;
                        db.Marketing_Chiphingay.AddOrUpdate(obj);
                        db.SaveChanges();
                        var res = db.Store_Marketing_Tong_Chi_Tung_Ngay(obj.NgayChi).FirstOrDefault();
                        if (res != null)
                        {
                            var tong = db.Marketing_ChiPhiPhanBo.FirstOrDefault(p => p.NgayChi == obj.NgayChi);
                            if (tong != null)
                            {
                                tong.TongChi = Convert.ToDecimal( res.Tongngaychi);
                                tong.IsDelete = false;
                                db.Marketing_ChiPhiPhanBo.AddOrUpdate(tong);
                                db.SaveChanges();
                            }
                            else
                            {
                                var tongchi = new Marketing_ChiPhiPhanBo();
                                tongchi.NgayChi = obj.NgayChi;
                                tongchi.TongChi = Convert.ToDecimal(res.Tongngaychi);
                                tongchi.IsDelete = false;
                                db.Marketing_ChiPhiPhanBo.AddOrUpdate(tongchi);
                                db.SaveChanges();

                            }
                        }
                        msg.success = true;
                        msg.data = obj;
                        msg.message = "Xóa thành công";
                    }
                    else
                    {
                        msg.success = false;
                        msg.message = "Lỗi!. vui lòng liên hệ với nhà phát triển để được giải đáp";
                    }
                }
                else
                {
                    msg.success = false;
                    msg.message = "Lỗi!. vui lòng liên hệ với nhà phát triển để được giải đáp";
                }
                return msg;
            }
        }
    }
}