﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Net;

namespace _30shine.GUI.BackEnd.MarKeting
{
    public partial class Listting_Customers_ByCampaign : System.Web.UI.Page
    {
        private string PageID = "";
        protected Paging PAGING = new Paging();
        private bool Perm_Access = false;
        protected bool Perm_Edit = false;
        protected bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            //SetPermission();
            if (!IsPostBack)
            {
                Perm_ViewAllData = true; // Tam set khi chua Set Permission
                bindCampaign(new List<DropDownList>() { ddlCampaign }, Perm_ViewAllData);
                GetListData();
                RemoveLoading();
            }
        }

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //PageID = "CAMPAIGN_CUSTOMER_LISTING";
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                //if (Array.IndexOf(new string[] { "ADMIN", "root" }, permissionModel) != -1)
                //{
                //    Perm_Access = true;
                //    Perm_Delete = true;
                //    Perm_ViewAllData = true;
                //    Perm_Edit = true;
                //}
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();

                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        /// <summary>
        /// Bind danh sách chiến dịch
        /// </summary>
        /// <param name="ddls"></param>
        /// <param name="permViewAll"></param>
        public static void bindCampaign(List<DropDownList> ddls, bool permViewAll)
        {
            using (var db = new Solution_30shineEntities())
            {
                var listCampaign = new List<MktCampaign>();
                var CampaignId = Convert.ToInt32(HttpContext.Current.Session["CampaignId"]);
                if (permViewAll)
                {
                    listCampaign = db.MktCampaigns.Where(w => w.IsDelete != true).OrderBy(o => o.Id).ToList();
                    var item = new MktCampaign();
                    item.Name = "Chọn chiến dịch";
                    item.Id = 0;
                    listCampaign.Insert(0, item);
                }
                else
                {
                    listCampaign = db.MktCampaigns.Where(w => w.Id == CampaignId).ToList();
                }
                for (var i = 0; i < ddls.Count; i++)
                {
                    ddls[i].DataTextField = "Name";
                    ddls[i].DataValueField = "Id";
                    ddls[i].DataSource = listCampaign;
                    ddls[i].DataBind();
                }
            }
        }

        /// <summary>
        /// bind data by method call api
        /// </summary>
        private List<Origin> GetListData()
        {
            var lstOrigin = new List<Origin>();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    int integer;
                    int campaign = int.TryParse(ddlCampaign.SelectedValue, out integer) ? integer : 0;
                    using (var client = new HttpClient())
                    {
                        ServicePointManager.Expect100Continue = true;
                        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                        client.BaseAddress = new Uri(Libraries.AppConstants.URL_API_BILL_SERVICE);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        var response = client.GetAsync("/api/Campaign/get-list-customer-by-campaign/" + campaign).Result;
                        var serializer = new JavaScriptSerializer();
                        var data = response.Content.ReadAsAsync<clsCampaignConfig>().Result.data.ToList();
                        if (data != null)
                        {
                            Bind_Paging(data.Count());
                            RptFluctuation.DataSource = data.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                            RptFluctuation.DataBind();
                        }
                    }
                }
            }
            catch
            {
                return lstOrigin;
            }
            return lstOrigin;
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            GetListData();
            RemoveLoading();
        }

        /// <summary>
        /// Phân trang
        /// </summary>
        /// <param name="TotalRow"></param>
        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();
            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }
    }

    public class clsCampaignConfig
    {
        public int status { get; set; }
        public string message { get; set; }
        public List<Origin> data { get; set; }
    }

    public class Origin
    {
        public string CampaignName { get; set; }
        public string CustomerName { get; set; }
        public string Phone { get; set; }
        public string Used { get; set; }
        public int ID { get; set; }
    }
}
