﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;

namespace _30shine.GUI.BackEnd.MarKeting
{
    public partial class ToolUpdateLikeStyleMaster : System.Web.UI.Page
    {
        private bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        DateTime FirstOfMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
        DateTime Today = DateTime.Now;

        //author: QuynhĐD
        //createdate: 22/05/2018

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                pageId = permissionModel.GetRegexPageIdSpecial().Replace(pageId, ".html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                TotalLikeNumber.Enabled = false;
                PostNumber.Enabled = false;
                if (Perm_ViewAllData == true)
                {
                    TotalLike.Enabled = true;
                }
                else
                {
                    TotalLike.Enabled = false;
                }
                //call
                BindTotalLike();
                CountTotalLike();
                BindTotalPostNumber();
            }
        }

        //bind total like in table tbl_Config
        private void BindTotalLike()
        {
            using (var db = new Solution_30shineEntities())
            {
                var data = (from c in db.Tbl_Config
                            where c.Key == "auto_like"
                            select c.Value
                            ).FirstOrDefault();
                if (data != null)
                {
                    TotalLike.Text = data;
                }
                else
                {
                    TotalLike.Text = "0";
                }
            }
        }

        //bind total post in month
        private void BindTotalPostNumber()
        {
            using (var db = new Solution_30shineEntities())
            {
                var data = (from c in db.StyleMasters
                            where c.IsDelete == false && c.StyleMasterStatusId == 3
                            && c.CreatedTime >= FirstOfMonth && c.CreatedTime <= Today
                            select c).Count();
                if (data > 0)
                {
                    PostNumber.Text = data.ToString();
                }
            }
        }

        //Update Total Like in table Tbl_Config (permission admin)
        [WebMethod]
        public static string UpdateConfig(string value, string key)
        {
            using (var db = new Solution_30shineEntities())
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                var message = new Msg();
                var exc = 0;
                if (value != "" && key != "")
                {
                    var obj = new Tbl_Config();
                    obj = (from c in db.Tbl_Config.AsNoTracking()
                           where c.Key.StartsWith(key)
                           select c).FirstOrDefault();
                    if (obj != null)
                    {
                        obj.Value = Convert.ToString(value);
                        obj.ModifiedDate = DateTime.Now;
                        db.Tbl_Config.AddOrUpdate(obj);
                        exc = db.SaveChanges();
                    }
                    if (exc > 0)
                    {
                        message.success = true;
                    }
                    else
                    {
                        message.success = false;
                    }
                }

                return serializer.Serialize(message);
            }
        }

        /// <summary>
        /// Đếm tổng like đã tặng trong tháng
        /// </summary>
        private void CountTotalLike()
        {
            using (var db = new Solution_30shineEntities())
            {
                var data = (from c in db.StyleMasters.AsNoTracking()
                            where c.StyleMasterStatusId == 3
                            && c.IsDelete == false
                            && c.CreatedTime >= FirstOfMonth && c.CreatedTime <= Today
                            select new
                            {
                                SumToTalLike = c.AutoLikeNumber
                            })
                            .ToList()
                            .Sum(o => o.SumToTalLike);

                TotalLikeNumber.Text = data.ToString();
            }
        }

        //event update
        protected void Update(object sender, EventArgs e)
        {
            UpdateStyleMaster();
        }

        /// <summary>
        /// 1. Select top bài viết
        /// 2. Thực hiện update autolike post 
        /// </summary>
        private void UpdateStyleMaster()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    StyleMaster obj = new StyleMaster();
                    int top = Convert.ToInt32(TopPost.Text);
                    int numberlike = Convert.ToInt32(NumberLike.Text);
                    Random random = new Random();
                    var exc = 0;
                    if (Order.SelectedValue == "True")
                    {
                        var data = (from c in db.StyleMasters.AsNoTracking()
                                    where c.IsDelete == false
                                    && c.StyleMasterStatusId == 3
                                    && c.CreatedTime >= FirstOfMonth && c.CreatedTime <= Today
                                    select c
                            )
                            .OrderByDescending(o => o.TotalLike)
                            .Take(top)
                            .ToList();

                        if (data.Count > 0)
                        {
                            int[] tempArrayDataAutoLike = AutoLikeRandom(data, numberlike);
                            for (int i = 0; i < tempArrayDataAutoLike.Length; i++)
                            {
                                data[i].AutoLikeNumber += tempArrayDataAutoLike[i];
                                data[i].TotalLike += tempArrayDataAutoLike[i];
                                data[i].ModifiedTime = DateTime.Now;
                                db.StyleMasters.AddOrUpdate(data[i]);
                            }
                        }
                    }
                    else
                    {
                        var data = (from c in db.StyleMasters.AsNoTracking()
                                    where c.IsDelete == false
                                    && c.StyleMasterStatusId == 3
                                    && c.CreatedTime >= FirstOfMonth && c.CreatedTime <= Today
                                    select c
                            )
                            .OrderBy(o => o.TotalLike)
                            .Take(top)
                            .ToList();
                        if (data.Count > 0)
                        {
                            int[] tempArrayDataAutoLike = AutoLikeRandom(data, numberlike);
                            for (int i = 0; i < tempArrayDataAutoLike.Length; i++)
                            {
                                data[i].AutoLikeNumber += tempArrayDataAutoLike[i];
                                data[i].TotalLike += tempArrayDataAutoLike[i];
                                data[i].ModifiedTime = DateTime.Now;
                                db.StyleMasters.AddOrUpdate(data[i]);
                            }
                        }
                    }
                    exc = db.SaveChanges();
                    if (exc > 0)
                    {
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                        UIHelpers.Redirect("/admin/marketing/tool-auto-like-stylemaster.html", MsgParam);
                    }
                    else
                    {
                        var msg = "Cập nhật thất bại!";
                        var status = "msg-system warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 1000);
                    }

                }
            }
            catch
            {

                var msg = "Đã xảy ra lỗi!";
                var status = "msg-system warning";
                UIHelpers.TriggerJsMsgSystem(Page, msg, status, 1000);
            }
        }

        //Random auto like
        public int[] AutoLikeRandom(List<StyleMaster> data, int numberlike)
        {
            Random random = new Random();
            int totalTempRandom = 0;
            int[] tempArrayDataAutoLike = new int[data.Count];
            int randomRoot = numberlike;
            for (var item = 0; item < data.Count; item++)
            {
                if (totalTempRandom >= numberlike)
                {
                    break;
                }
                while (totalTempRandom < numberlike)
                {
                    totalTempRandom = 0;
                    for (var i = 0; i < tempArrayDataAutoLike.Length; i++)
                    {
                        totalTempRandom += tempArrayDataAutoLike[i];
                    }

                    int dataRandom = random.Next(1, numberlike - totalTempRandom);
                    tempArrayDataAutoLike[item] = dataRandom;
                    totalTempRandom = 0;
                    for (var i = 0; i < tempArrayDataAutoLike.Length; i++)
                    {
                        totalTempRandom += tempArrayDataAutoLike[i];
                    }
                    if (totalTempRandom > numberlike)
                    {
                        tempArrayDataAutoLike[item] = 0;
                    }
                    else
                    {
                        break;
                    }

                }
            }

            return tempArrayDataAutoLike;
        }
    }
}