﻿using _30shine.Helpers;
using _30shine.Helpers.Http;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using ExportToExcel;
using Libraries;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.MarKeting
{
    public partial class MarketingCampaignListing : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        public CultureInfo culture = new CultureInfo("vi-VN");
        private bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected int departmentId;
        protected string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
        protected string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
        public List<ResCampaign> listOrigin = new List<ResCampaign>();
        public int index = 1;
        public int i = 0;
        private static MarketingCampaignListing instance;
        /// <summary>
        /// set permission
        /// </summary>
        public void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        /// <summary>
        /// ExecuteByPermission
        /// </summary>
        private void ExecuteByPermission()
        {

            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        /// <summary>
        /// Get WorkDay_Listing instance
        /// </summary>
        /// <returns></returns>
        public static MarketingCampaignListing getInstance()
        {
            if (!(MarketingCampaignListing.instance is MarketingCampaignListing))
            {
                return new MarketingCampaignListing();
            }
            else
            {
                return MarketingCampaignListing.instance;
            }
        }

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = FirstDayOfMonth;
                TxtDateTimeTo.Text = Today;
                BindCampaignType();
                BindCustomerType();
                BindServiceType();
                GetData();
            }
        }

        /// <summary>
        /// Bind list các loại customer
        /// </summary>
        public void BindCustomerType()
        {
            using (var db = new Solution_30shineEntities())
            {
                var listCustomerType = db.Tbl_Config.Where(r => r.Key == "customer_type").ToList();
                var item = new Tbl_Config();
                item.Label = "Chọn loại khách hàng";
                item.Id = 0;
                listCustomerType.Insert(0, item);
                ddlCustomerType.DataTextField = "Label";
                ddlCustomerType.DataValueField = "Value";
                ddlCustomerType.DataSource = listCustomerType;
                ddlCustomerType.DataBind();
            }
        }

        /// <summary>
        /// Bind list các loại campaign
        /// </summary>
        public void BindServiceType()
        {
            using (var db = new Solution_30shineEntities())
            {
                var listServiceType = db.Tbl_Config.Where(r => r.Key == "service_type").ToList();
                var item = new Tbl_Config();
                item.Label = "Chọn loại dịch vụ";
                item.Id = 0;
                listServiceType.Insert(0, item);
                ddlServiceType.DataTextField = "Label";
                ddlServiceType.DataValueField = "Value";
                ddlServiceType.DataSource = listServiceType;
                ddlServiceType.DataBind();
            }
        }

        /// <summary>
        /// Bind list các loại campaign
        /// </summary>
        public void BindCampaignType()
        {
            using (var db = new Solution_30shineEntities())
            {
                var listCampaignType = db.Tbl_Config.Where(r => r.Key == "campaign_type").ToList();
                var item = new Tbl_Config();
                item.Label = "Chọn tất cả chiến dịch";
                item.Id = 0;
                listCampaignType.Insert(0, item);
                ddlCampaignType.DataTextField = "Label";
                ddlCampaignType.DataValueField = "Value";
                ddlCampaignType.DataSource = listCampaignType;
                ddlCampaignType.DataBind();
            }
        }

        /// <summary>
        /// Get data 
        /// </summary>
        private void GetData()
        {
            try
            {
                string TimeFrom, TimeTo;
                if (!String.IsNullOrEmpty(TxtDateTimeFrom.Text))
                {
                    TimeFrom = TxtDateTimeFrom.Text;
                    if (!String.IsNullOrEmpty(TxtDateTimeTo.Text))
                    {
                        TimeTo = TxtDateTimeTo.Text;
                    }
                    else
                    {
                        TimeTo = TimeFrom;
                    }
                    int CamType, CusType, SerType, integer;
                    CamType = int.TryParse(ddlCampaignType.SelectedValue, out integer) ? integer : 0;
                    CusType = int.TryParse(ddlCustomerType.SelectedValue, out integer) ? integer : 0;
                    SerType = int.TryParse(ddlServiceType.SelectedValue, out integer) ? integer : 0;
                    var getData = new Request().RunGetAsyncV1(Libraries.AppConstants.URL_API_BILL_SERVICE + $"/api/campaign/list?type={CamType}&fromDate={TimeFrom}&toDate={TimeTo}&customerType={CusType}&serviceType={SerType}").Result;
                    if (getData.IsSuccessStatusCode)
                    {
                        var result = getData.Content.ReadAsStringAsync().Result;
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        var responseData = serializer.Deserialize<List<ResCampaign>>(result);
                        if (responseData != null)
                        {
                            Bind_Paging(responseData.Count);
                            listOrigin = responseData.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                            ResizeTable();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Get Data Customer
        /// </summary>
        private List<CustomerByCampaign> GetDataCustomer(int campaignId)
        {
            try
            {
                var customer = new List<CustomerByCampaign>();
                if (campaignId > 0)
                {
                    var getData = new Request().RunGetAsyncV1(Libraries.AppConstants.URL_API_BILL_SERVICE + $"/api/campaign-customer?campaignId={campaignId}").Result;
                    if (getData.IsSuccessStatusCode)
                    {
                        var result = getData.Content.ReadAsStringAsync().Result;
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        var responseData = serializer.Deserialize<ResponseDataCustomer>(result);
                        if (responseData != null)
                        {
                            customer = responseData.Data;
                        }
                    }
                }
                return customer;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Export to excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ExcExportExcelCustomer(object sender, EventArgs e)
        {
            try
            {
                int campaignId = int.TryParse(HDF_Customer.Value, out var integer) ? integer : 0;
                var data = GetDataCustomer(campaignId);
                if (data.Count > 0)
                {
                    var ExcelHeadRow = new List<string>();
                    var ListCustomerRow = new List<List<string>>();
                    var CustomerRow = new List<string>();
                    ExcelHeadRow.Add("Tên chiến dịch");
                    ExcelHeadRow.Add("Tên khách hàng");
                    ExcelHeadRow.Add("Số điện thoại");
                    ExcelHeadRow.Add("Số lần sử dụng");
                    foreach (var item in data)
                    {
                        CustomerRow = new List<string>();
                        CustomerRow.Add(item.CampaignName);
                        CustomerRow.Add(item.CustomerName);
                        CustomerRow.Add(item.Phone);
                        CustomerRow.Add(item.Used.ToString());
                        ListCustomerRow.Add(CustomerRow);
                    }
                    var ExcelStorePath = Server.MapPath("~") + "Public/Excel/ChienDich/Customer/";
                    var FileName = "danh_sach_khach_hang_" + String.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
                    var UrlFileName = "http://" + HttpContext.Current.Request.Url.Host + "/Public/Excel/ChienDich/Customer/" + FileName;
                    ScriptManager.RegisterStartupScript(BtnFakeExcelCustomer, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "');UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing();ShowNotification('Tải thông tin khách hàng thành công');", true);
                    ExportXcel(ExcelHeadRow, ListCustomerRow, ExcelStorePath + FileName);
                    GetData();
                }
                else
                {
                    ShowNotification("Chiến dịch không có khách hàng!");
                }
            }
            catch (Exception ex)
            {
                ShowNotification("Lỗi: " + ex.Message);
            }
        }

        /// <summary>
        /// Get Data Customer
        /// </summary>
        private List<VoucherByCampaign> GetDataVoucher(int campaignId)
        {
            try
            {
                var voucher = new List<VoucherByCampaign>();
                if (campaignId > 0)
                {
                    var getData = new Request().RunGetAsyncV1(Libraries.AppConstants.URL_API_BILL_SERVICE + $"/api/mkt-voucher?campaignId={campaignId}").Result;
                    if (getData.IsSuccessStatusCode)
                    {
                        var result = getData.Content.ReadAsStringAsync().Result;
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        var responseData = serializer.Deserialize<ResponseDataVoucher>(result);
                        if (responseData != null)
                        {
                            voucher = responseData.Data;
                        }
                    }
                }
                return voucher;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Export to excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ExcExportExcelVoucher(object sender, EventArgs e)
        {
            try
            {
                int campaignId = int.TryParse(HDF_Voucher.Value, out var integer) ? integer : 0;
                var data = GetDataVoucher(campaignId);
                if (data.Count > 0)
                {
                    var ExcelHeadRow = new List<string>();
                    var ListVoucherRow = new List<List<string>>();
                    var VoucherRow = new List<string>();
                    ExcelHeadRow.Add("Tên chiến dịch");
                    ExcelHeadRow.Add("Mã voucher");
                    ExcelHeadRow.Add("Hoạt động");
                    foreach (var item in data)
                    {
                        VoucherRow = new List<string>();
                        VoucherRow.Add(item.CampaignName);
                        VoucherRow.Add(item.Code);
                        VoucherRow.Add(item.IsActive == true ? "Hoạt động" : "Không hoạt động");
                        ListVoucherRow.Add(VoucherRow);
                    }
                    // export
                    var ExcelStorePath = Server.MapPath("~") + "Public/Excel/ChienDich/Voucher/";
                    var FileName = "danh_sach_voucher_" + String.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
                    var UrlFileName = "http://" + HttpContext.Current.Request.Url.Host + "/Public/Excel/ChienDich/Voucher/" + FileName;
                    ScriptManager.RegisterStartupScript(BtnFakeExcelVoucher, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "');UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing();ShowNotification('Tải thông tin voucher thành công!');", true);
                    ExportXcel(ExcelHeadRow, ListVoucherRow, ExcelStorePath + FileName);
                    GetData();
                }
                else
                {
                    ShowNotification("Chiến dịch không có voucher!");
                }
            }
            catch (Exception ex)
            {
                ShowNotification("Lỗi: " + ex.Message);
            }
        }

        /// <summary>
        /// Xuất excel
        /// </summary>
        /// <param name="_ExcelHeadRow"></param>
        /// <param name="List"></param>
        /// <param name="Path"></param>
        private static void ExportXcel(List<string> _ExcelHeadRow, List<List<string>> List, string Path)
        {
            try
            {
                CreateExcelFile.ExcelHeadRow = _ExcelHeadRow;
                CreateExcelFile.CreateExcel(List, Path);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi. Không tạo được file excel.\r\nException: " + ex.Message);
            }
        }

        /// <summary>
        /// button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _BtnClick(object sender, EventArgs e)
        {
            GetData();
            RemoveLoading();
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Resize table
        /// </summary>
        public void ResizeTable()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Resize", "ResizeTable();", true);
        }

        /// <summary>
        /// show notification
        /// </summary>
        /// <param name="message"></param>
        /// <param name="status"></param>
        public void ShowNotification(string message)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Click", "ShowNotification('" + message + "');", true);
        }



        /// <summary>
        /// Paging
        /// </summary>
        /// <param name="TotalRow"></param>
        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();
            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        /// <summary>
        /// Get_TotalPage
        /// </summary>
        /// <param name="TotalRow"></param>
        /// <returns></returns>
        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }
    }

    public class ResCampaign
    {
        public MktCampaignSub MktCampaign { get; set; }
        public List<MktCampaignCustomer> MktCampaignCustomer { get; set; }
        public List<MktCampaignServiceSub> MktCampaignService { get; set; }
        public List<MktVoucher> MktCampaignVoucher { get; set; }
    }

    public class MktCampaignSub : MktCampaign
    {
        public string StrCampaignType { get; set; }
        public string StrCustomerType { get; set; }
        public string StrServiceType { get; set; }
        public int TotalUsedCode { get; set; }
        public int MaxUsedCodeForPhone { get; set; }
    }

    public class MktCampaignServiceSub : MktCampaignService
    {
        public string ServiceName { get; set; }
    }

    public class ResponseDataCustomer
    {
        public string Message { get; set; }
        public int Status { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public List<CustomerByCampaign> Data { get; set; }
    }

    public class CustomerByCampaign
    {
        public string CampaignName { get; set; }
        public string CustomerName { get; set; }
        public string Phone { get; set; }
        public int Used { get; set; }
        public int ID { get; set; }
    }

    public class ResponseDataVoucher
    {
        public string Message { get; set; }
        public int Status { get; set; }
        public List<VoucherByCampaign> Data { get; set; }
    }

    public partial class VoucherByCampaign
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int CampaignId { get; set; }
        public bool IsDelete { get; set; }
        public bool? IsActive { get; set; }
        public DateTime CreatedTime { get; set; }
        public DateTime? ModifiedTime { get; set; }
        public String CampaignName { get; set; }
    }
}