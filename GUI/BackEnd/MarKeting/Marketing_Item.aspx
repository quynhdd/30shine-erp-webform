﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Marketing_Item.aspx.cs" Inherits="_30shine.GUI.BackEnd.MarKeting.Marketing_Item" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">

    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .edit { margin: 8px; width: 15px; }
            .del { margin: 8px; width: 15px; }
            .del:hover { color: red; }
            .edit:hover { color: seagreen; }
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý Item &nbsp;&#187; </li>
                        <li class="li-listing" style="cursor: pointer"><a onclick="show()">Thêm item</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing be-report woktime-listing">
            <%-- Listing --%>

            <div class="wp960 content-wp">
                <div class="row " id="ql" hidden="hidden">
                    <ul class="form-group">
                        <li class="list-group-item">
                            <label style="float: left">Tên item :</label>
                            <input type="text" class="form-control" style="margin-left: 10px; margin-right: 10px; width: 25%" id="name" />
                            <a class="btn btn-primary" style="margin-left: 10px; margin-right: 10px; width: 100px;" onclick="save()">Lưu lại</a>
                            <a class="btn btn-default
                                " style="margin-left: 10px; margin-right: 10px; width: 100px;" onclick="huy()">Hủy</a>
                        </li>
                    </ul>
                </div>
            </div>


            <div class="col-md-12 col-xs-12">
                <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Danh sách item</strong>
                <table class="table-add table-listing">
                    <thead>
                        <tr>
                            <th>STT</th>
                            <th>Tên item</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <%if (item.Count > 0)
                            { %>
                        <%int i = 1; foreach (var v in item)
                            { %>
                        <tr>
                            <td><%= i++ %></td>
                            <td><%=v.Item_Name %>                        
                            </td>
                            <td class="map-edit" style="padding-right: 0px;">
                                <div class="edit-wp">
                                    <a class="elm edit-btn" onclick="edit($(this), <%=v.Id %>)" data-name="<%=v.Item_Name %>" id="sua"></a>
                                    <a class="elm del-btn" onclick="deletel($(this),<%=v.Id %>)" id="xoa" <%--class="fa fa-times del"--%>></a>
                                </div>
                            </td>
                        </tr>
                        <%} %>
                        <%} %>
                    </tbody>
                </table>
            </div>
        </div>
    </asp:Panel>
    <script>
        var id = 0;
        function save() {
            var Id = id;
            console.log(id);

            var tenitem = $("#name").val();
            if (tenitem == "") {
                alert("Bạn phải nhập tên mục chi")
                return;
            }
            //var checkbox = document.getElementsByName('optrcheck');
            //var result = "";
            //for (var i = 0; i < checkbox.length; i++) {
            //    if (checkbox[i].checked === true) {
            //        result = checkbox[i].value;
            //    }
            //    else {
            //        result = 0;
            //    }
            //}
            // console.log(result);
            //return;
            $.ajax({
                type: "POST",
                url: "/GUI/BackEnd/MarKeting/Marketing_Item.aspx/Save",
                data: '{Id:' + Id + ',TenItem : "' + tenitem + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json", success: function (response) {
                    console.log(response.d.data);
                    alert(response.d.message);
                    window.location.reload();
                },
                failure: function (response) { alert(response.d); }
            });
        }
        function edit(This, Id) {
            $("#ql").show();
            id = Id;
            var name = This.attr("data-name");
            $("#name").val(name);
            console.log(id);
            //return;
            //$.ajax({
            //    type: "POST",
            //    url: "/GUI/BackEnd/MarKeting/Marketing_Item.aspx/edit",
            //    data: '{Id:' + id + '}',
            //    contentType: "application/json; charset=utf-8",
            //    dataType: "json", success: function (response) {
            //        console.log(response.d.data);
            //        alert(response.d.message);
            //        $("#name").val = response.d.Item_Name;
            //        if (response.d.data.Active == 1) {
            //            $(THIS).find("input[type=checkbox]").prop("checked", "true");
            //        }
            //        //var mission = JSON.parse(response.d.data);
            //        //if (mission.success) {
            //        //    alert(response.d.message);
            //        //} else {
            //        //}
            //    },
            //    failure: function (response) { alert(response.d); }
            //});
        }
        function show() {
            $("#ql").show();
        }
        function huy() {
            $("#ql").hide();
        }
        function deletel(This, Id) {
            var id = Id;

            console.log(id);
            //return;
            var r = confirm('Bạn có chắc chắn muốn xóa hay không');
            if (r) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/MarKeting/Marketing_Item.aspx/delete",
                    data: '{Id:' + id + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        console.log(response.d.data);
                        alert(response.d.message);
                        window.location.reload();
                    },
                    failure: function (response) { alert(response.d); }
                });
            }
        }
        //them item
        //function save()
        //{
        //    var Tenitem = $("#name").val();
        //    var active = $("#active").val();
        //    console.log(Tenitem, active);
        //    return;
        //}

    </script>
</asp:Content>
