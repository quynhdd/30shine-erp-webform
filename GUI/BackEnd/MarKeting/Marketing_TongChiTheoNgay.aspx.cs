﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Script.Serialization;
using ExportToExcel;
using Excel;
using LinqKit;
using System.Web.Services;
using System.Data.Entity.Migrations;
using Project.Model.Structure;


namespace _30shine.GUI.BackEnd.MarKeting
{
    public partial class Marketing_TongChiTheoNgay : System.Web.UI.Page
    {
        protected List<Marketing_ChiPhiPhanBo> obj;
        Solution_30shineEntities db = new Solution_30shineEntities();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                LoadData();
            }
        }
        public void LoadData()
        {
            try
            {
                DateTime ngay = Convert.ToDateTime(txtDateTimeto.Text);
                obj = db.Marketing_ChiPhiPhanBo.Where(p => p.IsDelete == false).ToList();
            }
            catch(Exception e)
            {
                throw new Exception(e.Message);
            }


        }
    }
}