﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Marketing_TongChiTheoNgay.aspx.cs" Inherits="_30shine.GUI.BackEnd.MarKeting.Marketing_TongChiTheoNgay" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .edit { margin: 8px; width: 15px; }
            .del { margin: 8px; width: 15px; }
            .del:hover { color: red; }
            .edit:hover { color: seagreen; }
        </style>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý tổng chi hàng ngày &nbsp;&#187; </li>
                        <li class="li-listing" style="cursor: pointer"><a onclick="show()">Thêm chiến dịch</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing be-report woktime-listing">
            <%-- Listing --%>

            <div class="wp960 content-wp">
                <div class="row" id="ql" hidden="hidden">
                    <ul class="form-group">
                        <li class="table table-add">            
                            <div class="col-md-5 col-xs-12" style="padding-top: 10px;">
                                <label style="float: left; width: 20%;">Ngày chi :</label>
                                <asp:TextBox CssClass="txtDateTime form-control" ID="txtDateTimeto" placeholder="Chọn ngày" Style="width: 75% !important;"
                                    ClientIDMode="Static" runat="server"></asp:TextBox>
                            </div>
                            <div class="col-md-12 col-xs-12" style="padding-top: 10px;">
                                <a class="btn btn-primary" style="margin-left: 9%; margin-right: 10px; width: 100px;" onclick="save($(this))" data-staff="<%=staff_id %>">Xem dữ liệu</a>
                                <a class="btn btn-default" style="margin-left: 10px; margin-right: 10px; width: 100px;" onclick="huy()">Hủy</a>
                            </div>

                        </li>
                    </ul>
                </div>
            </div>


            <div class="col-md-12 col-xs-12">
                <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Danh sách chiến dịch</strong>
                <table class="table table-bordered text-center ">
                    <thead>
                        <tr>
                            <th style="text-align: center">Stt</th>
                            <th style="text-align: center">Ngày chi</th>
                            <th style="text-align: center">Tổng chi</th>                         
                            <th style="width: 100px;"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <%if (obj.Count > 0)
                            { %>
                        <%int i = 1; foreach (var v in obj)
                            { %>
                        <tr>
                            <td><%= i++ %></td>
                            <td><%=v.TieuDe %></td>
                            <td><%= v.NgayBatDau %></td>
                            <td><%= v.NgayKetThuc %></td>
                            <td><%= v.ChiPhi %></td>
                            <td>
                                <div>
                                    <a onclick="edit($(this) ,<%=v.Id %>)" data-tieude="<%=v.TieuDe %>"
                                        data-ngaybatdau="<%=v.NgayBatDau %>" data-ngayketthuc="<%=v.NgayKetThuc %>" data-chiphi="<%=v.ChiPhi %>" id="sua" class="fa fa-pencil-square-o edit"></a>
                                    <a onclick="deletel($(this),<%=v.Id %>)" id="xoa" class="fa fa-times del"></a>
                                </div>
                            </td>
                        </tr>
                        <%} %>
                        <%} %>
                    </tbody>
                </table>
            </div>
        </div>
    </asp:Panel>
    
</asp:Content>
