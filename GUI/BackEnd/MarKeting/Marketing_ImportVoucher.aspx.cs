﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.MarKeting
{
    public partial class Marketing_ImportVoucher : System.Web.UI.Page
    {
        private bool Perm_Access = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                BindCampaign();
            }
        }


        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        public void BindCampaign()
        {
            using (var db = new Solution_30shineEntities())
            {
                var listCampaign = new List<MktCampaign>();
                listCampaign = db.MktCampaigns.Where(w => w.IsDelete != true).OrderByDescending(o => o.CreatedTime).ToList();
                var Key = 0;
                var Count = listCampaign.Count;
                ddlCampaign.DataTextField = "Name";
                ddlCampaign.DataValueField = "Id";
                ListItem item = new ListItem("Chọn chiến dịch", "0");
                ddlCampaign.Items.Insert(Key, item);
                Key++;

                if (Count > 0)
                {
                    foreach (var v in listCampaign)
                    {
                        item = new ListItem(v.Name, v.Id.ToString());
                        ddlCampaign.Items.Insert(Key, item);
                        Key++;
                    }
                }
            }
        }
    }
}