﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="MarKeting_Campaign.aspx.cs" Inherits="_30shine.GUI.BackEnd.MarKeting.MarKeting_Campaign" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/Assets/css/bootstrap/bootstrap-toggle.min.css" rel="stylesheet" />
    <script src="/Assets/js/bootstrap/bootstrap-toggle.min.js"></script>
    <script src="/assets/js/jquery.mCustomScrollbar.js"></script>
    <link href="/assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
    <link href="/Assets/js/select2/select2.min.css" rel="stylesheet" />
    <script src="/Assets/js/select2/select2.min.js?v=112321"></script>

    <style>
        .fe-service.tr-product.show-product.addproduct {
            padding: 9px 12px;
            border-radius: 0;
            margin-left: 16px;
        }

        .wp_add_product {
            width: 100%;
            float: left;
            margin-top: 11px;
        }

        .staff-servey-mark td input[type='radio'] {
            top: 9px;
            float: left;
            position: relative
        }

        #success-alert {
            top: 100px;
            right: 10px;
            position: fixed;
            width: 20% !important;
            z-index: 2000;
        }

        tr.staff-servey-mark input[type="checkbox"] {
            width: auto;
            vertical-align: text-top;
        }

        tr.staff-servey-mark td.right span {
            width: 15% !important;
            float: none !important;
            padding-left: 10%;
            padding-right: 10px;
            font-weight: bold;
        }

        tr.staff-servey-mark td.right select {
            width: 20% !important;
            float: none !important;
        }

        tr.staff-servey-mark td {
            padding-top: 15px !important;
        }

        #chkIsActive {
            padding-left: 0px !important;
        }

        #MainImg img {
            width: 120px;
            height: 160px;
            left: 0px;
        }
    </style>
</asp:Content>

<asp:Content ID="ServiceAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="alert alert-danger" id="success-alert">
            <span id="msg-alert"></span>
        </div>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý thông tin chiến dịch</li>
                        <li class="li-listing"><a href="/admin/marketing/danh-sach-chien-dich.html">Danh sách</a></li>
                        <li class="li-add"><a href="/admin/marketing/them-moi-chien-dich.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>


        <div class="wp customer-add admin-product-add fe-service">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <!-- System Message -->
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="table-wp">
                    <table class="table-add admin-product-table-add fe-service-table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Tạo mới chiến dịch</strong></td>
                                <td></td>
                            </tr>

                            <tr>
                                <td class="col-xs-2 left"><span>Tên chiến dịch</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="Name" runat="server" ClientIDMode="Static" placeholder="Nhập tên chiến dịch"></asp:TextBox>
                                        <%--  <asp:RequiredFieldValidator ID="NameValidate" ControlToValidate="Name" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập tên chiến dịch!"></asp:RequiredFieldValidator>--%>
                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-2 left"><span>Tên hiển thị</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="Label" runat="server" ClientIDMode="Static" placeholder="Nhập tên hiển thị"></asp:TextBox>
                                        <%--   <asp:RequiredFieldValidator ID="LabelValidate" ControlToValidate="Label" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập tên hiển thị!"></asp:RequiredFieldValidator>--%>
                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-2 left"><span>Số lần sử dụng</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="MaxUse" runat="server" ClientIDMode="Static" placeholder="Nhập số lần sử dụng tối đa" onkeypress="return ValidateKeypress(/\d/,event);"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator ID="MaxUseValidate" ControlToValidate="MaxUse" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập số lần sử dụng dịch vụ!"></asp:RequiredFieldValidator>--%>
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Ngày bắt đầu/kết thúc</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <asp:TextBox ID="StartDate" CssClass="txtDateTime" runat="server" ClientIDMode="Static" placeholder="Chọn ngày bắt đầu" onkeypress="return ValidateKeypress(/\d/,event);"></asp:TextBox>
                                            <%-- <asp:RequiredFieldValidator ID="StartDateValidator" ControlToValidate="StartDate" runat="server" CssClass="fb-cover-error" Text="Bạn chưa chọn ngày bắt đầu!"></asp:RequiredFieldValidator>--%>
                                        </div>
                                        <div class="div_col">
                                            <asp:TextBox ID="EndDate" Style="margin-left: 10px" CssClass="txtDateTime" runat="server" ClientIDMode="Static" placeholder="Chọn ngày kết thúc" onkeypress="return ValidateKeypress(/\d/,event);"></asp:TextBox>
                                            <%--  <asp:RequiredFieldValidator ID="EndDateValidator" ControlToValidate="EndDate" runat="server" CssClass="fb-cover-error" Text="Bạn chưa chọn ngày kết thúc!"></asp:RequiredFieldValidator>--%>
                                        </div>
                                    </div>

                                </td>
                            </tr>

                            <tr class="tr-field-ahalf tr-product">
                                <td class="col-xs-2 left"><span>Dịch vụ</span></td>
                                <td class="col-xs-10 right">
                                    <div class="row" id="quick-product">
                                        <div class="col-xs-12">
                                            <div class="row" style="margin-left: -15px; margin-right: -15px;">
                                                <div class="col-xs-9" style="width: 100%">
                                                    <asp:Repeater ID="Rpt_Service" runat="server">
                                                        <ItemTemplate>
                                                            <div class="checkbox cus-no-infor">
                                                                <label class="lbl-cus-no-infor">
                                                                    <!--thêm Mapid-->
                                                                    <input type="checkbox" data-id="<%# Eval("Id") %>" data-price="<%#Eval("Price") %>" data-name="<%# Eval("Name")%>" onclick="pushQuickServiceData($(this))" />
                                                                    <%# Eval("Name") %>
                                                                </label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div id="ListingProductWp" runat="server" class="listing-product item-product" clientidmode="Static">
                                        <table id="table-item-product" class="table table-listing-product table-item-product">
                                            <thead>
                                                <tr>
                                                    <th>STT</th>
                                                    <th>Tên dịch vụ</th>
                                                    <th>Đơn giá</th>
                                                    <th>Giảm giá (%)</th>
                                                </tr>
                                            </thead>
                                            <tbody id="tbodyProduct">
                                                <asp:Repeater ID="Rpt_ServiceCampaign" runat="server">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td class="td-product-index" data-id='<%# Eval("serviceId") %>'><%# Container.ItemIndex + 1 %></td>
                                                            <td class="td-product-name" data-id='<%# Eval("serviceId") %>'><%# Eval("serviceName") %></td>
                                                            <td class="td-product-price" data-price='<%# Eval("servicePrice") %>'><%# Eval("servicePrice") %></td>
                                                            <td class="map-edit">
                                                                <div class="row">
                                                                    <input type="text" class="service-voucher" value="<%# Eval("discountPercent") %>" style="margin: 0 auto; float: none; text-align: center; width: 50px;" onkeypress="return ValidateKeypress(/\d/,event);" onchange="changeVoucher()" />
                                                                    %
                                                                </div>
                                                                <div class="edit-wp">
                                                                    <a class="elm del-btn" href="javascript://" onclick='RemoveItem($(this).parent().parent().parent(),&#039;<%# Eval("serviceName") %>&#039;,<%# Eval("serviceId") %>,&#039;service&#039;)' title="Xóa"></a>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                    </div>


                                </td>
                            </tr>

                            <tr class="tr-description">
                                <td class="col-xs-2 left"><span>Ghi chú</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox TextMode="MultiLine" Rows="5" ID="Note" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-upload">
                                <td class="col-xs-3 left"><span>Ảnh</span></td>
                                <td class="col-xs-7 right">
                                    <div class="wrap btn-upload-wp HDF_MainImg" style="margin-bottom: 5px; width: auto;">
                                        <div id="Btn_UploadImg" class="btn-upload-img" onclick="popupImageIframe('HDF_MainImg')">Chọn ảnh</div>
                                        <asp:FileUpload ID="UploadFile" ClientIDMode="Static" Style="display: none;"
                                            CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                        <div class="wrap listing-img-upload">
                                            <div id="MainImg" class="thumb-wp">
                                                <img class="thumb" src="#" />
                                                <span class="delete-thumb" onclick="deleteThumbnail($(this), 'HDF_MainImg')"></span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="staff-servey-mark">
                                <td class="col-xs-2 left"><span>Kích hoạt</span></td>
                                <td class="col-xs-2 right">
                                    <asp:CheckBox ID="chkIsActive" runat="server" ClientIDMode="Static" />
                                    <span>IsBookingPublish</span>
                                    <asp:CheckBox ID="chkIsBookingPublish" runat="server" ClientIDMode="Static" />
                                    <span>Chọn loại chiến dịch</span>
                                    <asp:DropDownList ID="ddlCampaignType" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                </td>

                            </tr>
                            <tr class="tr-send">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <asp:Panel ID="BtnSend" CssClass="btn-send" runat="server" ClientIDMode="Static">Hoàn Tất</asp:Panel>
                                        <asp:Button ID="BtnFakeSend" OnClick="ExcAddOrUpdate" runat="server" Enabled="true" Text="Hoàn tất"
                                            ClientIDMode="Static" Style="display: none;"></asp:Button>
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <asp:HiddenField ID="HDF_ServiceId" runat="server" ClientIDMode="Static" />
                    <asp:HiddenField ID="HDF_MainImg" runat="server" ClientIDMode="Static" />
                </div>
            </div>
        </div>

        <script>
            jQuery(document).ready(function () {
                bindImage();
                $("#success-alert").hide();
                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        $input.val($input.val().split(' ')[0]);
                    },
                    scrollMonth: false,
                    scrollTime: false,
                    scrollInput: false
                });
                getServiceId();
                // remove 
                $("#Name").bind("click", function () {
                    $("#Name").css("border-color", "#ddd");
                });
                $("#Label").bind("click", function () {
                    $("#Label").css("border-color", "#ddd");
                });
                $("#MaxUse").bind("click", function () {
                    $("#MaxUse").css("border-color", "#ddd");
                });
                $("#StartDate").bind("click", function () {
                    $("#StartDate").css("border-color", "#ddd");
                });
                $("#EndDate").bind("click", function () {
                    $("#EndDate").css("border-color", "#ddd");
                });
            });

            /// kiểm tra input đầu vào
            $("#BtnSend").bind("click", function () {
                var kt = true;
                var error = 'Vui lòng nhập đầy đủ thông tin!';
                var name = $("#Name").val();
                var label = $("#Label").val();
                var maxusage = $("#MaxUse").val();
                var startdate = $("#StartDate").val();
                var enddate = $("#EndDate").val();
                var startdate1 = new Date($("#StartDate").val());
                var enddate1 = new Date($("#EndDate").val());
                var hdfServie = $("#HDF_ServiceId").val();
                if (name == "") {
                    kt = false;
                    error += "[Tên chiến dịch], ";
                    $("#Name").css("border-color", "red");
                }
                if (label == "") {
                    kt = false;
                    error += "[Tên hiển thị], ";
                    $("#Label").css("border-color", "red");
                }
                if (maxusage == "") {
                    kt = false;
                    error += "[Số lần sử dụng], ";
                    $("#MaxUse").css("border-color", "red");
                }
                if (startdate == "") {
                    kt = false;
                    error += "[Ngày bắt đầu], ";
                    $("#StartDate").css("border-color", "red");
                }
                if (enddate == "") {
                    kt = false;
                    error += "[Ngày kết thúc], ";
                    $("#EndDate").css("border-color", "red");
                }
                if (startdate1 >= enddate1) {
                    kt = false;
                    error += " Ngày kết thúc phải lớn hơn ngày bắt đầu chiến dịch ! ";
                    $("#EndDate").css("border-color", "red");
                }
                if (hdfServie == "[]") {
                    kt = false;
                    error += " [Bạn phải chọn dịch vụ!], ";
                }
                if (!kt) {
                    $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                        $("#success-alert").slideUp(2000);
                    });
                    $("#msg-alert").text(error);
                }
                else {
                    $("#BtnFakeSend").click();
                }
            });

            /// Thêm dịch vụ khi click checkbox
            function pushQuickServiceData(This) {
                var index = 1;
                $('#tbodyProduct').find("tr").each(function () {
                    $('#tbodyProduct tr').find("td.td-product-index").text(index);
                    index++;
                });
                var IdService = This.data('id');
                var ServiceName = This.data('name');
                var ServicePrice = This.data('price');
                var DiscountPercent = 10;

                if (This.is(":checked")) {
                    $('#tbodyProduct').append('<tr><td class="td-product-index"></td><td class="td-product-name" data-id=' + IdService + '>' + ServiceName + '</td><td class="td-product-price" data-price="' + ServicePrice + '">' + ServicePrice + '</td><td class="map-edit" ><div class="row"><input type="text" class="service-voucher" value="' + DiscountPercent + '" style="margin: 0 auto; float: none; text-align: center; width: 50px;" onkeypress="return ValidateKeypress(/\\d/,event);"  onchange="changeVoucher()" /></div><div class="edit-wp"><a class="elm del-btn" href="javascript://"onclick="RemoveItem($(this).parent().parent().parent(),\'' + ServiceName + '\',\'product\')" href="javascript://" title="Xóa"></a></div></td></tr>');
                    $(".item-product").show();

                }
                // bỏ check remove item
                else {
                    $("#table-item-product").find(".td-product-name[data-id='" + IdService + "']").parent().remove();
                }

                getServiceId();
                UpdateItemService($("#table-item-product").find("tbody"));
            }
            function bindImage() {
                var mainImg = $("#HDF_MainImg").val();
                mainImg = typeof mainImg == undefined || mainImg == null ? "" : mainImg;
                if (mainImg == "") {
                    $("#MainImg").hide();
                }
                else {
                    $("#MainImg img").attr("src", mainImg);
                }
            }
            //get ServiceId
            function getServiceId() {
                var Ids = [];
                var prd = {};
                $("table.table-item-product tbody tr").each(function () {
                    var THIS = $(this);
                    prd = {};
                    var serviceId = THIS.find("td.td-product-name").attr("data-id"),
                        servicePrice = THIS.find(".td-product-price").attr("data-price"),
                        discountPercent = THIS.find("input.service-voucher").val();
                    // check 
                    serviceId = serviceId.toString().trim() != "" ? parseInt(serviceId) : 0;
                    servicePrice = servicePrice.toString().trim != "" ? parseInt(servicePrice) : 0;
                    discountPercent = discountPercent.trim() != "" ? parseInt(discountPercent) : 0;

                    prd.serviceId = serviceId;
                    prd.servicePrice = servicePrice;
                    prd.discountPercent = discountPercent;
                    Ids.push(prd);
                });

                Ids = JSON.stringify(Ids);
                $("#HDF_ServiceId").val(Ids);
            }

            ///discountPercent
            function changeVoucher() {
                var discountPercent = $("input.service-voucher").val();
                discountPercent = discountPercent.trim() != "" ? parseInt(discountPercent) : 0;
                //getserviceId
                getServiceId();
            }

            // so sánh Datetime 

            //Update service
            function UpdateItemDisplayService(itemName) {
                var dom = $(".table-item-" + itemName);
                var len = dom.find("tbody tr").length;
                if (len == 0) {
                    dom.parent().hide();
                } else {
                    UpdateItemService(dom.find("tbody"));
                }
            }

            // Update STT
            function UpdateItemService(dom) {
                var index = 1;
                dom.find("tr").each(function () {
                    $(this).find("td.td-product-index").text(index);
                    index++;
                });
            }

            // Remove item đã được chọn
            function RemoveItem(THIS, name, itemName) {
                // Confirm
                $(".confirm-yn").openEBPopup();

                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn hủy [ " + name + " ] ?");
                $("#EBPopup .yn-yes").bind("click", function () {
                    var Id = THIS.find(".td-product-id").attr("data-id");
                    $("#quick-" + itemName).find("input[data-id='" + Id + "']").removeAttr("checked");
                    THIS.remove();
                    autoCloseEBPopup(0)
                    UpdateItemDisplayService(itemName);
                    getServiceId();
                });
            }
            //vaidate number
            function ValidateKeypress(numcheck, e) {
                var keynum, keychar, numcheck;
                if (window.event) {
                    keynum = e.keyCode;
                }
                else if (e.which) {
                    keynum = e.which;
                }
                if (keynum == 8 || keynum == 127 || keynum == null || keynum == 9 || keynum == 0 || keynum == 13) return true;
                keychar = String.fromCharCode(keynum);
                var result = numcheck.test(keychar);
                return result;
            }
            function popupImageIframe(StoreImgField) {
                var Width = 960;
                var Height = 560;
                var ImgList = [];
                var iframe = $("<div class='iframe-image-upload' style='width:" + Width + "px; height:" + Height + "px; float:left; display:none;'><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + StoreImgField + "&callback=getThumbFromIframe&imgWidth=800&imgHeight=600&multi_img=false&Folder=NhanVien' style='width:100%; height: 100%;'></iframe></div");

                $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
            }

            window.getThumbFromIframe = function (Imgs, StoreImgField) {
                var imgs = "";
                var thumb = "";
                if (Imgs.length > 0) {
                    thumb = '<div class="thumb-wp">' +
                        '<img class="thumb" alt="" title="" src="' + Imgs[0] + '"' +
                        'data-img="" />' +
                        '<span class="delete-thumb" onclick="deleteThumbnail($(this), \'' + StoreImgField + '\')"></span>' +
                        '</div>';
                    $("." + StoreImgField).find(".listing-img-upload").empty().append(thumb);
                    excThumbWidth(120, 120);
                    $("#" + StoreImgField).val(Imgs[0]);
                    this.console.log($("#" + StoreImgField).val());
                }
                autoCloseEBPopup();
            }
            function deleteThumbnail(This, StoreImgField) {
                This.parent().remove();
                $("#" + StoreImgField).val("");
            }
        </script>
    </asp:Panel>
</asp:Content>
