﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="MarketingCampaignListing.aspx.cs" Inherits="_30shine.GUI.BackEnd.MarKeting.MarketingCampaignListing" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .select2-container {
                width: 190px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }

            .table-sub-report {
                line-height: 2.2;
                border-style: none;
                width: 100% !important;
            }

                .table-sub-report tr {
                    border-style: none;
                }

                    .table-sub-report tr td {
                        border-right-style: none;
                        border-left-style: none;
                        border-top-style: none;
                    }

            .table-report tr:nth-child(even) {
                background-color: #f2f2f2
            }

            .table-sub-report tr:nth-child(even) {
                background-color: white;
            }

            .btn-filter {
                background: #000000;
                color: #ffffff;
                width: 80px !important;
                height: 35px;
                line-height: 30px;
                width: auto;
                text-align: center;
                cursor: pointer;
                -webkit-border-radius: 2px;
                -moz-border-radius: 2px;
                border-radius: 2px;
                margin: 0 5px;
                padding: 5px;
            }
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý thông tin chiến dịch</li>
                        <li class="li-listing"><a href="/admin/marketing/danh-sach-chien-dich-v2.html">Danh sách</a></li>
                        <li class="li-add"><a href="/admin/marketing/them-moi-chien-dich-v2.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing be-report">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- System Message -->
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <span style="display: none"></span>
                <!-- Filter -->
                <div class="row">
                    <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                    <div class="time-wp">
                        <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="ValidateTime" ControlToValidate="TxtDateTimeFrom" runat="server" CssClass="fb-cover-error" Text="Vui lòng chọn thời gian!"></asp:RequiredFieldValidator>
                    </div>
                    <strong class="st-head" style="margin-left: 10px;">
                        <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                    </strong>
                    <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeTo" placeholder="Đến ngày"
                        ClientIDMode="Static" runat="server"></asp:TextBox>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlCampaignType" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px;"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlServiceType" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px;"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlCustomerType" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 150px;"></asp:DropDownList>
                    </div>
                    <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                </div>
                <!-- End Filter -->

                <!-- Row Table Filter -->
                <div class="table-func-panel">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="table-wp">
                            <table class="table-add table-listing table-report">
                                <tbody>
                                    <tr style="background-color: #ccc">
                                        <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px;" rowspan="2">STT</th>
                                        <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 80px; height: 50px;" rowspan="2">Tên chiến dịch</th>
                                        <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 80px; height: 50px;" rowspan="2">Tên hiển thị (Checkout)</th>
                                        <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 80px; height: 50px;" rowspan="2">Tên hiển thị (Checkin)</th>
                                        <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px;" rowspan="2">Kiểu chiến dịch</th>
                                        <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px;" rowspan="2">Ngày bắt đầu</th>
                                        <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px;" rowspan="2">Ngày kết thúc</th>
                                        <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px;" rowspan="2">Tổng số lượt sử dụng CODE</th>
                                        <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px;" rowspan="2">Lượt giới hạn sử dụng CODE</th>
                                        <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px;" rowspan="2">Lượt giới hạn theo SĐT</th>
                                        <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 60px; height: 50px;" rowspan="2">Ảnh banner</th>
                                        <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px;" rowspan="2">Điều kiện lấy tệp KH</th>
                                        <%--<th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px;" rowspan="2">Danh sách KH được áp dụng</th>
                                        <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px;" rowspan="2">Mã code</th>--%>
                                        <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px;" rowspan="2">Kiểu dịch vụ</th>
                                        <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 160px; max-width: 300px; height: 50px;" colspan="10">Dịch vụ</th>
                                        <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px;" rowspan="2">Trạng thái</th>
                                        <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px;" rowspan="2"></th>
                                    </tr>
                                    <tr style="background-color: #ccc">
                                        <th style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px; display: none;" colspan="2">Mã</th>
                                        <th class="th-name" style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px; width: 7%;" colspan="2">Tên</th>
                                        <th class="th-discout" style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px; width: 2%;" colspan="2">Giảm giá(%)</th>
                                        <th class="th-preview" style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px; width: 4%;" colspan="2">Trả trước($)</th>
                                        <th class="th-deduc" style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px; width: 4%;" colspan="2">Giảm trừ($)</th>
                                        <th class="th-number" style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px; width: 2%;" colspan="2">Số lần</th>
                                    </tr>
                                    <%foreach (var v in listOrigin)
                                        {%>
                                    <tr>
                                        <td><%=index++ %></td>
                                        <td><%=v.MktCampaign.Name %></td>
                                        <td><%=v.MktCampaign.Label %></td>
                                        <td><%=v.MktCampaign.Description %></td>
                                        <td><%=v.MktCampaign.StrCampaignType %></td>
                                        <td><%= String.Format("{0:MM/dd/yyyy}", v.MktCampaign.StartDate )%></td>
                                        <td><%= String.Format("{0:MM/dd/yyyy}", v.MktCampaign.EndDate )%></td>
                                        <td><%=v.MktCampaign.TotalUsedCode %></td>
                                        <td><%=v.MktCampaign.CampaignMaxUsage %></td>
                                        <td><%=v.MktCampaign.MaxUsage %></td>
                                        <td>
                                            <div class="thumb-wp">
                                                <img class="thumb" alt="" title="" src="<%=v.MktCampaign.Image %>" style="width: 120px; height: 160px; left: 0px; top: -20px; z-index: 1;">
                                                <div style="z-index: 2; position: absolute; display: none; padding: 0 15px; margin-top: -95px;" class="downImage">
                                                    <a target="_blank" href="<%=v.MktCampaign.Image %>" class="btn-send perm-btn-field btn-filter" download style="margin-top: 40px; margin-left: 20px;">Xem ảnh</a>
                                                </div>
                                            </div>
                                        </td>
                                        <td><%=v.MktCampaign.CustomerCondition %></td>
                                        <%--<td><a onclick="downListingCustomer(<%= v.MktCampaign.Id %>)" href="javascript:;">Tải về thông tin khách hàng</a></td>
                                        <td><a onclick="downListingVoucher(<%= v.MktCampaign.Id %>)" href="javascript:;">Tải về thông tin mã code</a></td>--%>
                                        <td><%=v.MktCampaign.StrServiceType %></td>
                                        <td style="padding: 0 0 !important;" colspan="10">
                                            <table class="table-sub">
                                                <%foreach (var item in v.MktCampaignService)
                                                    {%>
                                                <%i++; %>
                                                <%if (i > 2)
                                                    { %>
                                                <tr>
                                                    <td style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px; display: none;" colspan="2"><%=item.Id %></td>
                                                    <td class="td-name" style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px;" colspan="2"><%=item.ServiceName %></td>
                                                    <td class="td-discout" style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px;" colspan="2"><%=item.DiscountPercent %></td>
                                                    <td class="td-preview" style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px;" colspan="2"><%=item.MoneyPrePaid %></td>
                                                    <td class="td-deduc" style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px;" colspan="2"><%=item.MoneyDeductions %></td>
                                                    <%if (3.Equals(v.MktCampaign.ServiceType))
                                                        {%>
                                                    <td class="td-number" style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px;" colspan="2">Lần <%=item.TimesUsed %></td>
                                                    <%} %>
                                                    <%else
                                                        {%>
                                                    <td class="td-number" style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px;" colspan="2"></td>
                                                    <%} %>
                                                </tr>
                                                <%} %>
                                                <%else
                                                    { %>
                                                <tr>
                                                    <td style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px; display: none;" colspan="2"><%=item.Id %></td>
                                                    <td class="td-name" style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px;" colspan="2"><%=item.ServiceName %></td>
                                                    <td class="td-discout" style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px;" colspan="2"><%=item.DiscountPercent %></td>
                                                    <td class="td-preview" style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px;" colspan="2"><%=item.MoneyPrePaid %></td>
                                                    <td class="td-deduc" style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px;" colspan="2"><%=item.MoneyDeductions %></td>
                                                    <%if (3.Equals(v.MktCampaign.ServiceType))
                                                        {%>
                                                    <td class="td-number" style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px;" colspan="2">Lần <%=item.TimesUsed %></td>
                                                    <%} %>
                                                    <%else
                                                        {%>
                                                    <td class="td-number" style="border: 1px solid black; border-collapse: collapse; padding: 5px; text-align: center; min-width: 40px; height: 50px;" colspan="2"></td>
                                                    <%} %>
                                                </tr>
                                                <%}  %>
                                                <%} %>
                                            </table>
                                        </td>
                                        <td><%=v.MktCampaign.IsActive == true ? "Hoạt động" : "Không hoạt động" %></td>
                                        <td class="map-edit">
                                            <div class="edit-wp">
                                                <a class="elm edit-btn" href="/admin/marketing/cap-nhat-chien-dich-v2/<%=v.MktCampaign.Id %>.html" title="Sửa"></a>
                                                <a class="elm del-btn" onclick="del(this.parentNode.parentNode.parentNode,'<%=v.MktCampaign.Id %>', '<%=v.MktCampaign.Name %>')" href="javascript://" title="Xóa"></a>
                                            </div>
                                        </td>
                                    </tr>
                                    <%} %>
                                </tbody>
                            </table>
                        </div>

                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:Button ID="BtnFakeExcelCustomer" ClientIDMode="Static" runat="server" OnClick="ExcExportExcelCustomer" Style="display: none;" />
                <asp:Button ID="BtnFakeExcelVoucher" ClientIDMode="Static" runat="server" OnClick="ExcExportExcelVoucher" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Customer" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Voucher" />
            </div>
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <script>
            var URL_CAMPAIGN = '<%=Libraries.AppConstants.URL_API_BILL_SERVICE%>';
            jQuery(document).ready(function () {
                $("td.td-name").css("width", $('.table-report').width() / 100 * 7);
                $("td.td-discout").css("width", $('.table-report').width() / 100 * 2);
                $("td.td-preview").css("width", $('.table-report').width() / 100 * 4);
                $("td.td-deduc").css("width", $('.table-report').width() / 100 * 4);
                $("td.td-number").css("width", $('.table-report').width() / 100 * 2);
                $(window).bind('resize', function () {
                    $("td.td-name").css("width", $('.table-report').width() / 100 * 7);
                    $("td.td-discout").css("width", $('.table-report').width() / 100 * 2);
                    $("td.td-preview").css("width", $('.table-report').width() / 100 * 4);
                    $("td.td-deduc").css("width", $('.table-report').width() / 100 * 4);
                    $("td.td-number").css("width", $('.table-report').width() / 100 * 2);
                });
                $(".thumb-wp").hover(
                    function () {
                        $(this).find(".downImage").css("display", "block");
                    }, function () {
                        $(this).find(".downImage").css("display", "none");
                    }
                );
                $(".downImage").hover(
                    function () {
                        $(this).find(".btn-filter").css("color", "yellow");
                    }, function () {
                        $(this).find(".btn-filter").css("color", "white");
                    }
                );

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });
            });

            function ResizeTable() {
                $("td.td-name").css("width", $('.table-report').width() / 100 * 7);
                $("td.td-discout").css("width", $('.table-report').width() / 100 * 2);
                $("td.td-preview").css("width", $('.table-report').width() / 100 * 4);
                $("td.td-deduc").css("width", $('.table-report').width() / 100 * 4);
                $("td.td-number").css("width", $('.table-report').width() / 100 * 2);
            }

            function del(This, code, name) {
                var code = code || null,
                    name = name || null,
                    Row = This;
                if (!code) return false;

                // show EBPopup
                $(".confirm-yn").openEBPopup();
                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");
                $("#EBPopup .yn-yes").bind("click", function () {
                    $.ajax({
                        type: "DELETE",
                        url: URL_CAMPAIGN + `/api/campaign?campaignId=${code}`,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            if (response.status == 1) {
                                delSuccess();
                                Row.remove();
                            }
                            else {
                                delFailed();
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    });
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
            }



            //$(".table-report").resizable({
            //    resize: function () {
            //        $("td-name").css("Width", $(this).width() / 100 * 7);
            //        $("td-discout").css("Width", $(this).width() / 100 * 2);
            //        $("td-preview").css("Width", $(this).width() / 100 * 4);
            //        $("td-deduc").css("Width", $(this).width() / 100 * 4);
            //        $("td-number").css("Width", $(this).width() / 100 * 2);
            //    }
            //});

            function downListingCustomer(Id) {
                startLoading();
                if (Id != undefined && Id != null && Id != "" && parseInt(Id) > 0) {
                    $("#HDF_Customer").val(Id);
                    $("#BtnFakeExcelCustomer").click();
                }
                else {
                    ShowNotification('Có lỗi xảy ra vui lòng liên hệ nhà phát triển!');
                }
                finishLoading();
            }

            function downListingVoucher(Id) {
                startLoading();
                if (Id != undefined && Id != null && Id != "" && parseInt(Id) > 0) {
                    $("#HDF_Voucher").val(Id);
                    $("#BtnFakeExcelVoucher").click();
                }
                else {
                    ShowNotification('Có lỗi xảy ra vui lòng liên hệ nhà phát triển!');
                }
                finishLoading();
            }

            //show popup noti
            function ShowNotification(message) {
                finishLoading();
                $(".confirm-yn").openEBPopup();
                $("#EBPopup .confirm-yn-text").text(message);
                $("#EBPopup .yn-yes").hide();
                $("#EBPopup .yn-no").hide();
                window.location.href = '/admin/marketing/danh-sach-chien-dich-v2.html';
            }
        </script>
        <!-- Page loading -->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
    </asp:Panel>
</asp:Content>
