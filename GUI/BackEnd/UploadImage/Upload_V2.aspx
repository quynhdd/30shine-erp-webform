﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Upload_V2.aspx.cs" Inherits="_30shine.BackEnd.UploadImage_V2.Upload_V2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="/Assets/css/bootstrap/bootstrap.min.css" rel="stylesheet" />
    <link href="/Assets/css/font.css?v=2" rel="stylesheet" />
    <link href="/Assets/css/font-awesome.css" rel="stylesheet" />
    <link href="/Assets/css/style.css?v=3" rel="stylesheet" />    
    <link href="/Assets/css/jquery.datetimepicker.css?v=2" rel="stylesheet" />        

    <script src="/Assets/js/jquery.v1.11.1.js"></script>
    <script src="/Assets/js/bootstrap/bootstrap.min.js"></script>
    <script src="/Assets/js/jquery.datetimepicker.js"></script>
    <script src="/Assets/js/common.js"></script>

    <script src="/Assets/js/jquery.mCustomScrollbar.js"></script>
    <link href="/Assets/css/jquery.mCustomScrollbar.css" rel="stylesheet" />
    <script src="/Assets/js/dropzone.js"></script>
    <link href="/Assets/css/dropzone.css" rel="stylesheet" />
    <link href="/Assets/css/plugin.upload.images/pui.style.v2.css" rel="stylesheet" />
</head>
<body>
    <style>
        .ListImgUpload img{
            height:84px !important;
        }
    </style>
<form id="form1" runat="server">
    
<!-- Popup Images -->
<div class="plugin-image" style="display:block;">
    <div class="plugin-image-head be-report">
        <!-- Filter -->
        <div class="row">
            <div class="filter-item filter-item-first">
                <p class="st-head"><i class="fa fa-filter"></i>Lọc kết quả :&nbsp;&nbsp;</p>
                <asp:TextBox CssClass="txtDateTime st-head TxtDateTimeFrom" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                    ClientIDMode="Static" runat="server"></asp:TextBox>
            </div>
            <strong class="st-head" style="margin-left: 10px;">
                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
            </strong>
            <div class="filter-item">
                <asp:TextBox CssClass="txtDateTime st-head TxtDateTimeTo" ID="TxtDateTimeTo" placeholder="Đến ngày"
                    ClientIDMode="Static" runat="server"></asp:TextBox>
            </div>
            <%--<div class="filter-item">
                <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion SgCustomerCode" data-field="customer.code" data-value="0"
                    AutoCompleteType="Disabled" ID="SgCustomerCode" ClientIDMode="Static" placeholder="Mã Khách hàng" runat="server"></asp:TextBox>
                <div class="listing-staff-wp eb-select-data ">
                    <ul class="ul-listing-staff ul-listing-suggestion" id="UlCustomerCode"></ul>
                </div>
            </div>
            <div class="filter-item">
                <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                    <ul class="ul-listing-staff ul-listing-suggestion" id="UlCustomerName">
                    </ul>
                </div>
            </div>
            
            <div class="filter-item">
                <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion SgCustomerPhone" data-value="0" data-field="customer.phone"
                        AutoCompleteType="Disabled" ID="SgCustomerPhone" ClientIDMode="Static" placeholder="Số điện thoại" runat="server"></asp:TextBox>
                <div class="listing-staff-wp eb-select-data">
                    <ul class="ul-listing-staff ul-listing-suggestion" id="UlCustomerPhone"></ul>
                </div>
            </div>--%>
            <a href="javascript://" class="st-head btn-viewdata plugin-image-view" onclick="Plugin_Images_Load()">Xem ảnh</a>
            <a href="javascript://" class="st-head btn-viewdata plugin-image-upload" onclick="Plugin_Images_Upload('avatar')">Upload</a>
            <a href="javascript://" class="st-head btn-viewdata plugin-image-push" onclick="Plugin_Images_Push(true)">Chèn ảnh</a>
        </div>
        <!-- End Filter -->
    </div>
    <div class="plugin-image-content">
        <div class="left-bar">
            <div class="left-bar-head"><i class="fa fa-hdd-o"></i>Quản lý thư mục</div>
            <div class="fd-wrap" id="fdWrap">
                <%=DomTree %>
            </div>
        </div>
        <div class="listing-img-upload" id="ListImgUpload">                  
            <div class="wrap"></div>
        </div>
    </div>
</div>
<div id="dZUpload" class="dropzone dz-work-flow" style="display:none;">
    <div class="dz-default dz-message wrap listing-img-upload">
        Drop image here. 
    </div>
</div>
<!-- Hidden Field-->
<asp:HiddenField ID="HDF_Suggestion_Code" ClientIDMode="Static" runat="server" />
<asp:HiddenField ID="HDF_Suggestion_Field" ClientIDMode="Static" runat="server" />
<asp:HiddenField ID="HDF_UserImages" runat="server" ClientIDMode="Static"/>
<asp:HiddenField ID="HDF_UserImagesDelete" runat="server" ClientIDMode="Static"/>
<asp:HiddenField ID="HDF_Page" runat="server" ClientIDMode="Static"/>
<asp:HiddenField ID="HDF_Distance" runat="server" ClientIDMode="Static"/>
<asp:HiddenField ID="HDF_Time" runat="server" ClientIDMode="Static"/>
<!-- END Hidden Field-->
<!-- Suggestion -->

<script type="text/javascript">
    var Imgs = [],
        ImgsDelete = [],
        DbImgs = [],
        InitDropZone = false,
        multi = true,
        qs = [];

    jQuery(document).ready(function ($) {        
        // get array query string
        qs = getQueryStrings();
        multi = qs["multi_img"] == "true" ? true : (qs["multi_img"] == "false" ? false : true);

        $(".btn-upload-wp .listing-img-upload .thumb-wp").each(function () {
            var dataImg = $(this).find("img.thumb").attr("data-img");
            DbImgs.push(dataImg);
        });

        Dropzone.autoDiscover = false;
        if (!InitDropZone) {
            $("#dZUpload").dropzone({
                url: "/GUI/BackEnd/UploadImage/DropZoneUploader_V2.ashx?Code=" + getCode() + "&imgWidth=" + qs["imgWidth"] + "&imgHeight=" + qs["imgHeight"] + "&Folder=" + qs["Folder"],
                maxFiles: 100,
                addRemoveLinks: true,
                success: function (file, response) {
                    //$("#dZUpload").show();
                    var imgName = response;
                    file.previewElement.classList.add("dz-success");
                    $(".plugin-image-view").click();
                },
                error: function (file, response) {
                    file.previewElement.classList.add("dz-error");
                }
            });
            InitDropZone = true;
        }

        // Init load images
        $(".plugin-image-view").click();

        //============================
        // Datepicker
        //============================
        $('.txtDateTime').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'vi',
            startDate: '2014/10/10',
            format: 'd/m/Y',
            dateonly: true,
            showHour: false,
            showMinute: false,
            timepicker: false,
            onChangeDateTime: function (dp, $input) { }
        });

        //Left bar scroll
        $('#fdWrap').mCustomScrollbar({
            theme: "dark-2",
            scrollInertia: 100,
        });

        // execute thumbnail size
        excThumbWidth();
    });

    //--- Dropdown menu Functions
    function menuFdDropdown(This) {
        var root = $(".fd-root"),
            i = This.find("i"),
            ul = This.parent().find(">ul.ul-pi-fd"),
            li = ul.find(">li"),
            open = "fa-folder-open-o",
            close = "fa-folder-o",
            active = "fd-active",
            childLast = isLastChild(This),
            toggleFlag = isActive(This) || !isOpen(This) ? true : false;
        
        if (childLast) {
            if (i.hasClass(open)) {
                findParentNode(This);
            }
        }

        if (i.hasClass(open)) {
            if (!childLast) {
                if (toggleFlag) {
                    i.removeClass(open).addClass(close);
                    if (nextIsLast(This)) {
                        li.find("i").removeClass(open).addClass(close);
                    }
                }
            }
            if (!toggleFlag) {
                $(".fd-active").removeClass(active);
                findParentNode(This);
            } else {
                This.removeClass(active);
            }            
        } else {
            if (childLast) {
                root.find("ul.ul-pi-fd>li").each(function () {
                    if ($(this).find("ul.ul-pi-fd").length == 0) {
                        if (toggleFlag) {
                            $(this).find("i").removeClass(open).addClass(close);
                        }
                        $(this).find(".fd-name").removeClass(active);
                    }
                });
            }
            $(".fd-active").removeClass(active);
            if (toggleFlag) {
                i.removeClass(close).addClass(open);                
            }
            findParentNode(This);
        }

        if (toggleFlag) {
            This.parent().find(">ul.ul-pi-fd>li").show();
            This.parent().find(">ul.ul-pi-fd").slideToggle();
        }

        if (This.hasClass(active) && i.hasClass(open)) {
            // Load data
            setTimeout(function () {
                var Time = This.attr("data-level");
                $("#HDF_Time").val(Time);
                $("#HDF_Page").val(1);
                loadDataImg(Time, "new", 'folder');
            }, 400);
        }

        $('#ListImgUpload').mCustomScrollbar("destroy");
        $('#ListImgUpload').mCustomScrollbar({
            theme: "dark-2",
            scrollInertia: 100,
            callbacks: {
                onScroll: function () {
                    lazyLoadImgs('folder');
                }
            }
        });
    }

    function isFdRoot(This) {
        return This.parent().parent().hasClass("fd-root");
    }

    function isLastChild(This) {
        var ul = This.parent().find(">ul.ul-pi-fd");
        return ul.length > 0 ? false : true;
    }

    function nextIsLast(This){
        var ul = This.parent().find(">ul.ul-pi-fd").find(">li").find(">ul.ul-pi-fd");
        return ul.length > 0 ? false : true;
    }

    function isActive(This) {
        return This.hasClass('fd-active');
    }

    function isOpen(This) {
        return This.find('i').hasClass('fa-folder-open-o');
    }

    function isParentClose(This) {
        if (!isActive(This) && !isOpen(This)) {
            return true;
        } else {
            var fdActive;
            $('.fd-active').each(function () {
                fdActive = $(this);
            });
            if (!isFdRoot(This)) {
                fdActive = fdActive.parent().parent().parent().find(".fd-name");
            }
            return fdActive == This ? true : false;
        }        
    }

    function setActiveFd(This) {
        This.addClass("fd-active");
    }

    function findParentNode(This) {
        var ul = This.parent().parent();
        setActiveFd(This);
        if (ul.hasClass("fd-root")) {
            return "fd-root";
        } else {
            var fdName = ul.parent().find(">.fd-name");            
            setActiveFd(fdName);
            // Recursive (đệ quy)
            findParentNode(fdName);
        }
    }

    function loadDataImg(Time, Type, by) {
        var TimeFrom = $(".TxtDateTimeFrom").val() != undefined ? $(".TxtDateTimeFrom").val() : "",
            TimeTo = $(".TxtDateTimeTo").val() != undefined ? $(".TxtDateTimeTo").val() : "",
            SgCode = $("#HDF_Suggestion_Code").val(),
            SgField = $("#HDF_Suggestion_Field").val(),
            Page = $("#HDF_Page").val() != "" ? $("#HDF_Page").val() : 1,
            Segment = 100;
        var url, data;
        switch (by) {
            case 'folder': url = "/GUI/BackEnd/UploadImage/Upload_V2.aspx/Ajax_LoadImages_ByFolder";
                data = "{'Time' : '" + Time + "', 'Page' : " + Page + ", 'Segment' : " + Segment + "}";
                break;
            case 'filter': url = "/GUI/BackEnd/UploadImage/Upload_V2.aspx/Ajax_LoadImages_ByFilter";
                data = '{TimeFrom : "' + TimeFrom + '", TimeTo : "' + TimeTo + '", SgCode : "' + SgCode + '", SgField : "' + SgField + '", Page : "' + Page + '", Segment : "' + Segment + '"}';
                break;
            default: url = "/GUI/BackEnd/UploadImage/Upload_V2.aspx/Ajax_LoadImages_ByFilter";
                data = '{TimeFrom : "' + TimeFrom + '", TimeTo : "' + TimeTo + '", SgCode : "' + SgCode + '", SgField : "' + SgField + '", Page : "' + Page + '", Segment : "' + Segment + '"}'
                break;
        }
        
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                var mission = JSON.parse(response.d);
                if (mission.success) {
                    var OBJ = JSON.parse(mission.msg);
                    var Thumbs = "";
                    if (OBJ.length > 0) {
                        $.each(OBJ, function (i, v) {
                            var imgname = getImgName(v.Value);
                            Thumbs += '<div class="thumbnail-wrap"><div class="thumb-wp" onclick="Plugin_Images_Chose(event, $(this), '+multi+')">' +
                                        '<img style="width:84px !important;height:84px !important;" class="thumb" alt="" title="" src="' + v.Value + '" data-img="' + v.Value + '"/>' +
                                        //'<span class="delete-thumb" onclick="deleteThum($(this), ' + "'" + v.Value + "'" + ', false)"></span>' +
                                        '<div class="thumb-cover"></div></div>' +
                                        '<p class="img-name">' + imgname + '</p>' +
                                    '</div>';
                        });
                        var page = $("#HDF_Page").val() != "" ? parseInt($("#HDF_Page").val()) + 1 : 1;
                        $("#HDF_Page").val(page);
                    } else {
                        //
                    }
                    if (Type == "new") {
                        $(".listing-img-upload .wrap").empty().append($(Thumbs));
                    } else if(Type == "lazyload") {
                        $(".listing-img-upload .wrap").append($(Thumbs));
                    }
                    // execute thumbnail size
                    excThumbWidth();
                    fixHeightInRow(".thumbnail-wrap", 0, 8);
                }
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }

    function lazyLoadImgs(by) {
        var mScroll = $("#ListImgUpload .mCSB_container");
        var Wrap = $("#ListImgUpload .wrap");
        var h = $("#ListImgUpload").height();
        var h1 = Wrap.height();
        var h2 = Math.abs(parseInt(mScroll.css("top")));
        var DistanceSet = 40;
        if ((h1 - h2 - h) < DistanceSet) {            
            loadDataImg($("#HDF_Time").val(), "lazyload", by);
        }
    }

    //---/ Dropdown menu Functions

    function getCode() {
        var qs = getQueryStrings();
        return qs["Code"];
    }

    function getImgName(path) {
        var imgname = path.split("/");
        return imgname[imgname.length - 1];
    }

    function BindParamToHDF() {
        Bind_UserImagesToHDF();
        // Bind Pid to HDF_Pid
        var LstChecked = "";
        $("#LRInput input:checked").each(function () {
            LstChecked += $(this).val() + ",";
        });
        LstChecked = LstChecked.replace(/\,$/, "");
        $("#HDF_Pid").val(LstChecked);
    }

    function Plugin_Images_Upload(Code) {
        $("#dZUpload").click();
    }

    function Plugin_Images_Chose(event, This, multi) {
        if (!This.hasClass("active")) {
            if (!multi) {
                This.parent().parent().find(".active").removeClass("active");
            }
            This.addClass("active");
        } else {
            This.removeClass("active");
        }   
    }

    function Plugin_Images_Push(multi) {
        var multi = multi != null ? multi : true;
        var Thumb = $('.plugin-image .thumb-wp.active');
        Imgs = [];
        if (multi == true) {
            Thumb.each(function () {
                var dataImg = $(this).find("img.thumb").attr("data-img");
                Imgs.push(dataImg);
            });
        } else {
            var dataImg = Thumb.find("img.thumb").attr("data-img");
            Imgs.push(dataImg);
        }
        // return list images to window parent
        window.parent.ImgList = Imgs;
        window.parent[qs["callback"]](Imgs, qs["StoreImgField"]);
    }

    function Plugin_Images_Load() {
        $("#HDF_Page").val(1);
        var TimeFrom = $(".TxtDateTimeFrom").val() != undefined ? $(".TxtDateTimeFrom").val() : "",
            TimeTo = $(".TxtDateTimeTo").val() != undefined ? $(".TxtDateTimeTo").val() : "",
            SgCode = $("#HDF_Suggestion_Code").val(),
            SgField = $("#HDF_Suggestion_Field").val(),
            Page = $("#HDF_Page").val() != "" ? $("#HDF_Page").val() : 1,
            Segment = 100;
        $.ajax({
            type: "POST",
            url: "/GUI/BackEnd/UploadImage/Upload_V2.aspx/Ajax_LoadImages_ByFilter",
            data: '{TimeFrom : "' + TimeFrom + '", TimeTo : "' + TimeTo + '", SgCode : "' + SgCode + '", SgField : "' + SgField + '", Page : "' + Page + '", Segment : "' + Segment + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                var mission = JSON.parse(response.d);
                if (mission.success) {
                    var OBJ = JSON.parse(mission.msg);
                    var Thumbs = "";
                    if (OBJ.length > 0) {
                        $.each(OBJ, function (i, v) {
                            //var imgname = i == 3 || i == 4 || i == 5 || i == 11 || i == 16 ? 'img_34512312312123123123232323.jpg' : getImgName(v.Value);
                            var imgname = getImgName(v.Value);
                            Thumbs += '<div class="thumbnail-wrap"><div class="thumb-wp" onclick="Plugin_Images_Chose(event, $(this), '+multi+')">' +
                                        '<img class="thumb" style="width:84px !important; height:84px !important;" alt="" title="" src="' + v.Value + '" data-img="' + v.Value + '"/>' +
                                        //'<span class="delete-thumb" onclick="deleteThum($(this), ' + "'" + v.Value + "'" + ', false)"></span>' +
                                        '<div class="thumb-cover"></div></div>' +
                                        '<p class="img-name">'+imgname+'</p>' +
                                    '</div>';
                        });
                        var page = $("#HDF_Page").val() != "" ? parseInt($("#HDF_Page").val()) + 1 : 1;
                        $("#HDF_Page").val(page);
                    } else {
                        //
                    }

                    $(".listing-img-upload .wrap").empty().append($(Thumbs));
                    $('#ListImgUpload').mCustomScrollbar({
                        theme: "dark-2",
                        scrollInertia: 100,
                        callbacks: {
                            onScroll: function () {
                                lazyLoadImgs('filter');
                            }
                        }
                    });
                    // execute thumbnail size
                    excThumbWidth();
                    fixHeightInRow(".thumbnail-wrap", 0, 8);                    
                } else {
                    //
                }
            },
            failure: function (response) { alert(response.d); }
        });
    }

    function deleteThum(THIS, data, oldImg) {
        oldImg = oldImg || false;
        if (!oldImg) {
            var index = Imgs.indexOf(data);
            if (index != -1) {
                Imgs.splice(index, 1);
            }
        } else {
            ImgsDelete.push(data);
        }
        THIS.parent().remove();
    }

    function Bind_UserImagesToHDF() {
        var jsonImages = JSON.stringify(Imgs);
        var jsonImagesDel = JSON.stringify(ImgsDelete);
        $("#HDF_UserImages").val(jsonImages);
        $("#HDF_UserImagesDelete").val(jsonImagesDel);
    }

    //============================
    // Suggestion Functions
    //============================
    function Bind_Suggestion() {
        $(".eb-suggestion").bind("keyup", function (e) {
            if (e.keyCode == 40) {
                UpDownListSuggest($(this));
            } else {
                Call_Suggestion($(this));
            }
        });
        $(".eb-suggestion").bind("focus", function () {
            Call_Suggestion($(this));
        });
        $(".eb-suggestion").bind("blur", function () {
            //Exc_To_Reset_Suggestion($(this));
        });
        $(window).unbind("click").bind("click", function (e) {
            if ((!e.target.className.match("eb-select") && e.target.parentElement.className.match("ul-listing-suggestion")) ||
                (!e.target.className.match("eb-select") && !e.target.parentElement.className.match("ul-listing-suggestion"))) {
                EBSelect_HideBox();
            }
        });
    }

    function UpDownListSuggest(This) {
        var UlSgt = This.parent().find(".ul-listing-suggestion"),
            index = 0,
            LisLen = UlSgt.find(">li").length - 1,
            Value;

        This.blur();
        UlSgt.find(">li.active").removeClass("active");
        UlSgt.find(">li:eq(" + index + ")").addClass("active");

        $(window).unbind("keydown").bind("keydown", function (e) {
            if (e.keyCode == 40) {
                if (index == LisLen) return false;
                UlSgt.find(">li.active").removeClass("active");
                UlSgt.find(">li:eq(" + (++index) + ")").addClass("active");
                return false;
            } else if (e.keyCode == 38) {
                if (index == 0) return false;
                UlSgt.find(">li.active").removeClass("active");
                UlSgt.find(">li:eq(" + (--index) + ")").addClass("active");
                return false;
            } else if (e.keyCode == 13) {
                // Bind data to HDF Field
                var THIS = UlSgt.find(">li.active");
                var Value = THIS.text().trim();
                //var Value = THIS.attr("data-code");
                var dataField = This.attr("data-field");

                BindIdToHDF(THIS, Value, dataField, "#HDF_Suggestion_Code", "#HDF_Suggestion_Field", This);
                EBSelect_HideBox();
            }
        });
    }

    function Exc_To_Reset_Suggestion(This) {
        var value = This.val();
        if (value == "") {
            $(".eb-suggestion").each(function () {
                var THIS = $(this);
                var sgValue = THIS.val();
                if (sgValue != "") {
                    BindIdToHDF(THIS, sgValue, THIS.attr("data-field"), "#HDF_Suggestion_Code", "#HDF_Suggestion_Field", THIS);
                    return false;
                }
            });
        }
    }

    function Call_Suggestion(This) {
        var text = This.val(),
            field = This.attr("data-field");
        Suggestion(This, text, field);
    }

    function Suggestion(This, text, field) {
        var This = This;
        var text = text || "";
        var field = field || "";
        var InputDomId;
        var HDF_Sgt_Code = "#HDF_Suggestion_Code";
        var HDF_Sgt_Field = "#HDF_Suggestion_Field";

        if (text == "") return false;

        switch (field) {
            //case "customer.name": InputDomId = "#CustomerName"; break;
            case "customer.phone": InputDomId = ".SgCustomerPhone"; break;
            case "customer.code": InputDomId = ".SgCustomerCode"; break;
                //case "bill.code": InputDomId = "#BillCode"; break;
        }

        $.ajax({
            type: "POST",
            url: "/GUI/SystemService/Ajax/Suggestion.aspx/Suggestion_Customer",
            data: '{field : "' + field + '", text : "' + text + '"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                var mission = JSON.parse(response.d);
                if (mission.success) {
                    var OBJ = JSON.parse(mission.msg);
                    if (OBJ.length > 0) {
                        var lis = "";
                        $.each(OBJ, function (i, v) {
                            lis += "<li data-code='" + v.Customer_Code + "'" +
                                         "onclick=\"BindIdToHDF($(this),'" + v.Value + "','" + field + "','" + HDF_Sgt_Code +
                                         "','" + HDF_Sgt_Field + "','" + InputDomId + "')\">" +
                                        v.Value +
                                    "</li>";
                        });
                        This.parent().find("ul.ul-listing-suggestion").empty().append(lis).parent().show();
                        This.parent().find(".eb-select-data").show();
                    } else {
                        This.parent().find(".eb-select-data").hide().find("ul.ul-listing-suggestion").empty();
                    }
                } else {
                    This.parent().find("ul.ul-listing-suggestion").empty();
                    var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                    showMsgSystem(msg, "warning");
                }
            },
            failure: function (response) { alert(response.d); }
        });
    }

    function BindIdToHDF(THIS, Code, Field, HDF_Sgt_Code, HDF_Sgt_Field, Input_DomId) {
        var text = THIS.text().trim();
        $("input.eb-suggestion").val("");
        $(HDF_Sgt_Code).val(Code);
        $(HDF_Sgt_Field).val(Field);
        $(Input_DomId).val(text);
        $(Input_DomId).parent().find(".eb-select-data").hide();
    }

    function EBSelect_HideBox() {
        $(".eb-select-data").hide();
        $("ul.ul-listing-suggestion li.active").removeClass("active");
    }

    function excThumbWidth() {
        var ImgStandardWidth = 84,
            ImgStandardHeight = 84;
        var width = ImgStandardWidth,
            height, left, top;
        $(".thumb-wp .thumb").each(function () {
            height = ImgStandardWidth * $(this).height() / $(this).width();
            left = 0;
            top = (ImgStandardHeight - height) / 2;
            $(this).css({ "width": width, "height": height, "left": left});
        });
    }

    //=================
    // Execute element height in row
    //=================
    function fixHeightInRow(selector, index, segment) {
        if (index > ($(selector).length - 1)) {
            return;
        }

        var bound = index + segment;
        var h = 0, h2 = 0;

        for (var i = index; i < bound; i++) {
            h2 = $(selector + ":eq(" + i + ")").height();
            h = h2 > h ? h2 : h;
        }
        for (var i = index; i < bound; i++) {
            setHeight(selector + ":eq(" + i + ")", h);
        }
        // Recursive (đệ quy)
        fixHeightInRow(selector, bound, segment)
    }

    function setHeight(selector, height) {
        $(selector).height(height);
    }

    function getElmQuantity(selector) {
        return $(selector).length;
    }

    function findIndexInRow(index, rowSegment) {
        var odd = index % rowSegment;
        var odd2 = (index + 1) % rowSegment;
        if (odd == 0) {
            odd = odd == odd2 ? 1 : rowSegment;
        }
        return odd - 1;
    }
    
</script>
<!-- END Suggestion -->
<!-- END Popup Images -->

</form>
</body>
</html>