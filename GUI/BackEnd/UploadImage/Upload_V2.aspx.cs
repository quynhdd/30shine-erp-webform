﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Globalization;
using System.Web.Script.Serialization;
using _30shine.Helpers;
using LinqKit;
using System.Threading.Tasks;
using _30shine.Helpers.Http;

namespace _30shine.BackEnd.UploadImage_V2
{
    public partial class Upload_V2 : System.Web.UI.Page
    {
        protected static Upload_V2 instance;
        /// <summary>
        /// get instance, phục vụ trường hợp có hàm statistic, sử dụng instance có thẻ gọi các method không phải static
        /// </summary>
        /// <returns></returns>
        public static Upload_V2 getInstance()
        {
            if (!(Upload_V2.instance is Upload_V2))
            {
                Upload_V2.instance = new Upload_V2();
            }

            return Upload_V2.instance;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            PrintFolderTree();
        }

        public void PrintFolderTree()
        {
            string Folder = getFolder();
            string ImgRootPath = "/Public/Media/Upload/Images/" + Folder + "/";
            string dirFullPath = HttpContext.Current.Server.MapPath("~" + ImgRootPath);
            // Check to create dir
            createDir(dirFullPath);
            TreeNode Tree = getFolderTree(dirFullPath);
            MakeHtmlNode(Tree, 0);
        }


        public ResponseData PushImageToAws(string ImageName, string Base64)
        {
            var request = new Request();
            ImageAws images = new ImageAws();
            images.img_name = ImageName;
            images.img_base64 = "data:image/jpeg;base64," + Base64;
            var response = request.RunPostAsync(Libraries.AppConstants.URL_API_DOMAINS3 + "/api/s3/singleUpload", images).Result;
            var result = response.Content.ReadAsStringAsync().Result;
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var responseData = serializer.Deserialize<ResponseData>(result);
            return responseData;
        }
        private static string getFolder()
        {
            //return HttpContext.Current.Request.QueryString["Folder"] != null ? HttpContext.Current.Request.QueryString["Folder"] : "Default";
            return "Common";
        }

        public void createDir(string pathDir)
        {
            // If the directory doesn't exist, create it.
            if (!Directory.Exists(pathDir))
            {
                Directory.CreateDirectory(pathDir);
            }
        }

        /// <summary>
        /// Make html folder tree 
        /// </summary>
        protected string DomTree = "";
        private List<string> MapLevel = new List<string>();
        public void MakeHtmlNode(TreeNode Tree, int level)
        {
            var child = Tree.GetAllChilds().Values;
            if (level > 0)
            {
                if (MapLevel.Count >= level)
                {
                    MapLevel.RemoveRange(level - 1, MapLevel.Count - (level - 1));
                }
                MapLevel.Add(Tree.ID);
                DomTree += "<li>" +
                                "<p class='fd-name' onclick='menuFdDropdown($(this))' " +
                                        "data-level='" + String.Join("-", MapLevel) + "'>" +
                                    "<i class='fa fa-folder-o'></i>" + Tree.ID +
                                "</p>";
            }

            if (child.Count > 0)
            {
                DomTree += level == 0 ? "<ul class='ul-pi-fd fd-root'>" : "<ul class='ul-pi-fd fd-child'>";
                level++;
            }
            else
            {
                level--;
            }
            foreach (var v in child)
            {
                // Recursive (đệ quy)
                MakeHtmlNode(v, level);
            }
            DomTree += child.Count > 0 ? "</ul>" : "";
            DomTree += level == 0 ? "" : "</li>";
        }

        /// <summary>
        /// Get folder tree
        /// </summary>
        ///// <param name="FolderPath"></param>
        public TreeNode getFolderTree(string FolderPath)
        {
            var FolderName = getFolderName(FolderPath);
            FolderName = FolderName == "" ? "Root" : FolderName;
            TreeNode Tree = new TreeNode(FolderName);
            return renderFolderNode(Tree, Tree, FolderPath);
        }

        public TreeNode renderFolderNode(TreeNode Tree, TreeNode _Tree, string FolderPath)
        {
            List<string> FolderChilds = getFolderNode(FolderPath);
            var FolderName = getFolderName(FolderPath);
            _Tree = FolderName == "" ? Tree :
                        _Tree.GetChild(FolderName) != null ? _Tree.GetChild(FolderName) : _Tree.Parent;
            if (FolderChilds.Count > 0)
            {
                foreach (var v in FolderChilds)
                {
                    AddTreeNode(_Tree, getFolderName(v));
                    // Recursive (đệ quy)                    
                    renderFolderNode(Tree, _Tree, v);
                }
            }
            return Tree;
        }

        public void AddTreeNode(TreeNode _Tree, string FolderName)
        {
            var Child = new TreeNode(FolderName);
            _Tree.Add(Child);
        }

        public List<string> getFolderNode(string FolderPath)
        {
            var fds = new List<int>();
            var fds2 = new List<string>();
            int fd;
            string fd2;
            var dirs = Directory.GetDirectories(FolderPath);

            if (dirs.Length > 0)
            {
                foreach (var v in dirs)
                {
                    fd = Convert.ToInt32(getFolderName(v));
                    fds.Add(fd);
                }
                fds = fds.OrderByDescending(o => o).ToList();
                foreach (var v in fds)
                {
                    fd2 = Path.Combine(FolderPath, v.ToString());
                    fds2.Add(fd2);
                }
            }
            return fds2;
        }

        public string getFolderName(string FolderFullName)
        {
            string name = "";
            char[] delimeter = new char[] { '/', '\\' };
            Regex regex = new Regex("(/)||(\\\\)");
            Match match = regex.Match(FolderFullName);
            if (match.Success)
            {
                string[] parts = FolderFullName.Split(delimeter);
                name = parts.Last();
            }
            else
            {
                name = FolderFullName;
            }

            return name;
        }

        /// <summary>
        /// Load images by date
        /// </summary>
        /// <param name="Time"></param>

        public static List<DateTime> MakeDateTime(string Time)
        {
            string[] ArrTime = new string[] { };
            char[] Seperator = new char[] { ',', '-' };
            var RetDateTime = new List<DateTime>();
            var Culture = new CultureInfo("vi-VN");
            DateTime Date1 = new DateTime();
            DateTime Date2 = new DateTime();
            ArrTime = Time.Split(Seperator);
            switch (ArrTime.Length)
            {
                case 1:
                    Time += "-1";
                    Date1 = Convert.ToDateTime(Time, Culture);
                    Date2 = Date1.AddYears(1);
                    break;
                case 2:
                    Date1 = Convert.ToDateTime(Time, Culture);
                    Date2 = Date1.AddMonths(1);
                    break;
                case 3:
                    Date1 = Convert.ToDateTime(Time, Culture);
                    Date2 = Date1.AddDays(1);
                    break;
            }
            RetDateTime.Add(Date1);
            RetDateTime.Add(Date2);
            return RetDateTime;
        }

        [WebMethod]
        public static string Ajax_LoadImages_ByFolder(string Time, int Page, int Segment)
        {
            using (var db = new Solution_30shineEntities())
            {
                string Folder = getFolder();
                List<DateTime> TimeBound = MakeDateTime(Time);
                DateTime FromDate = Convert.ToDateTime(TimeBound[0]);
                DateTime ToDate = Convert.ToDateTime(TimeBound[1]);
                var LST = db.Tbl_Media.Where(w => w.CreatedDate > FromDate && w.CreatedDate < ToDate && w.IsDelete != 1 && w.Folder == Folder)
                            .Select(s => new { s.Id, s.Key, s.Value }).OrderByDescending(o => o.Id).Skip((Page - 1) * Segment).Take(Segment).ToList();
                var serialize = new JavaScriptSerializer();
                var msg = new Msg();
                msg.success = true;
                msg.msg = serialize.Serialize(LST);
                return serialize.Serialize(msg);
            }
        }

        [WebMethod]
        public static string Ajax_LoadImages_ByFilter(string TimeFrom, string TimeTo, string SgCode, string SgField, int Page, int Segment)
        {
            var Msg = new Msg();
            var serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                string Folder = getFolder();
                CultureInfo culture = new CultureInfo("vi-VN");
                var _Images = new List<Tbl_Media>();
                var Where = PredicateBuilder.True<Tbl_Media>();

                if (TimeFrom != "")
                {
                    DateTime _FromDate = Convert.ToDateTime(TimeFrom, culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                    DateTime _ToDate;
                    if (TimeTo != "")
                    {
                        _ToDate = Convert.ToDateTime(TimeTo, culture).AddDays(1);
                    }
                    else
                    {
                        _ToDate = Convert.ToDateTime(TimeFrom, culture).AddDays(1);
                    }
                    Where = Where.And(w => w.CreatedDate > _FromDate && w.CreatedDate < _ToDate);
                }

                switch (SgField)
                {
                    case "customer.code":
                        Where = Where.And(w => w.Key.Contains(SgCode));
                        break;
                    case "customer.name":
                        Where = Where.And(w => w.Key.Contains(SgCode));
                        break;
                    case "customer.phone":
                        Where = Where.And(w => w.Key.Contains(SgCode));
                        break;
                    case "bill.code": break;
                }

                Where = Where.And(w => w.IsDelete != 1);
                _Images = db.Tbl_Media.AsExpandable().Where(Where).OrderByDescending(o => o.Id).Skip((Page - 1) * Segment).Take(Segment).ToList();

                Msg.success = true;
                Msg.msg = serializer.Serialize(_Images);

                return serializer.Serialize(Msg);
            }
        }

        /// <summary>
        /// log function
        /// </summary>
        /// <param name="str"></param>
        public void log(object str)
        {
            Response.Write(str.ToString() + "<br/>");
        }
    }

    public class TreeNode : IEnumerable<TreeNode>
    {
        private readonly Dictionary<string, TreeNode> _children =
                                            new Dictionary<string, TreeNode>();

        public readonly string ID;
        public TreeNode Parent { get; private set; }

        public TreeNode(string id)
        {
            this.ID = id;
        }

        public TreeNode GetChild(string id)
        {
            TreeNode value;
            if (this._children.TryGetValue(id, out value))
            {
                return this._children[id];
            }
            else
            {
                return null;
            }
        }

        public Dictionary<string, TreeNode> GetAllChilds()
        {
            return this._children;
        }

        public void Add(TreeNode item)
        {
            if (item.Parent != null)
            {
                item.Parent._children.Remove(item.ID);
            }

            item.Parent = this;
            this._children.Add(item.ID, item);
        }

        public IEnumerator<TreeNode> GetEnumerator()
        {
            return this._children.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public int Count
        {
            get { return this._children.Count; }
        }
    }
    public class ImageAws
    {
        public string img_name { get; set; }
        public string img_base64 { get; set; }
    }
    public class ResponseData
    {
        public string img_name { get; set; }
        public string link { get; set; }
        public bool status { get; set; }
        public string message { get; set; }
    }
}