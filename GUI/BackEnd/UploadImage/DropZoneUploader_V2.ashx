﻿<%@ WebHandler Language="C#" Class="DropZoneUploader_V2" %>

using System;
using System.IO;
using System.Drawing;
using System.Web;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using _30shine.MODEL.ENTITY.EDMX;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

public class DropZoneUploader_V2 : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{
    public void ProcessRequest(HttpContext context)
    {
        using (var db = new Solution_30shineEntities())
        {
            context.Response.ContentType = "text/plain";
            string Folder = HttpContext.Current.Request.QueryString["Folder"] != null ? HttpContext.Current.Request.QueryString["Folder"] : "";
            //string ImgRootPath = "/Public/Media/Upload/Images/" + Folder + "/";
            //string fldPath = ImgRootPath + DateTime.Now.Year.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Day.ToString() + "/";
            //string dirFullPath = HttpContext.Current.Server.MapPath("~" + fldPath);
            string[] files;
            int numFiles;
            //List<string> LSTFiles = new List<string>();
            //string str_image = "";
            int integer;
            int DefaultImgWidth = 1334;
            int DefaultImgHeight = 750;
            int imgWidth = int.TryParse(HttpContext.Current.Request.QueryString["imgWidth"], out integer) ? integer : DefaultImgWidth;
            int imgHeight = int.TryParse(HttpContext.Current.Request.QueryString["imgHeight"], out integer) ? integer : DefaultImgHeight;

            // Check to create dir

            //createDir(dirFullPath);

            //files = Directory.GetFiles(dirFullPath);
            //numFiles = files.Length;
            //numFiles = numFiles + 1;

            foreach (string s in context.Request.Files)
            {
                HttpPostedFile file = context.Request.Files[s];
                //string fileName = file.FileName;
                //string fileExtension = file.ContentType;
                string _Code = context.Request.QueryString["Code"];
                Guid guid = Guid.NewGuid();
                Random random = new Random();
                int iRandom1 = random.Next(10000, 99999);
                string ImageName = "Img_" + iRandom1 + "_" + String.Format("{0:dd_MM_yyyy_HH_mm_ss}", DateTime.Now);
                string base64Strings = "";
                //get base64 image
                using (Bitmap bm = new Bitmap(file.InputStream))
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        bm.Save(ms, ImageFormat.Jpeg);
                        base64Strings = Convert.ToBase64String(ms.ToArray());
                    }
                }
                var InsertImageToAws = _30shine.BackEnd.UploadImage_V2.Upload_V2.getInstance().PushImageToAws(ImageName, base64Strings);
                if (!string.IsNullOrEmpty(InsertImageToAws.link))
                {

                    //fileExtension = Path.GetExtension(fileName);
                    //string _filename = fileName.Replace(fileExtension, "");
                    //string UniqueID = GetUniqueKey();
                    //str_image = fldPath + _filename + "-" + UniqueID + fileExtension;
                    //string pathToSave = HttpContext.Current.Server.MapPath("~" + str_image);
                    //Bitmap bmpPostedImage = new Bitmap(file.InputStream);
                    //HandleImageUpload(bmpPostedImage, imgWidth, imgHeight, pathToSave, fileExtension);
                    // Save thumbnail (max-width: 350)
                    //string str_image_thumb = fldPath + _filename + "-" + UniqueID + "x350" + fileExtension;
                    //string pathToSaveThumb = HttpContext.Current.Server.MapPath("~" + str_image_thumb);
                    //bmpPostedImage = new Bitmap(file.InputStream);
                    //HandleImageUpload(bmpPostedImage, 350, 350, pathToSaveThumb, fileExtension);

                    // Insert to database
                    var OBJ = new Tbl_Media();
                    OBJ.Key = _Code;
                    OBJ.Value = new Regex(@"\?([^\?]*)").Replace(InsertImageToAws.link,"");
                    //var strTest = @"https://s3.ap-southeast-1.amazonaws.com/30shine/API_UPLOAD_S3/2018_05_18/Img_52321_18_05_2018_16_43_09.jpg?versionId=636gCKVsV.wNu8HtUfy0sG.5XszMnY9D";
                    //Regex regex = new Regex(@"\?([^\?]*)");
                    //var test = regex.Replace(strTest, "");
                    OBJ.Type = 1;
                    OBJ.Folder = Folder;
                    OBJ.CreatedDate = DateTime.Now;
                    OBJ.IsDelete = 0;
                    db.Tbl_Media.Add(OBJ);
                    db.SaveChanges();
                    //LSTFiles.Add(str_image);
                }
            }
            //context.Response.Write(str_image);
        }
    }

    public void createDir(string pathDir)
    {
        // If the directory doesn't exist, create it.
        if (!Directory.Exists(pathDir))
        {
            Directory.CreateDirectory(pathDir);
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    private System.Drawing.Image RezizeImage(System.Drawing.Image img, int maxWidth, int maxHeight)
    {
        if (img.Height < maxHeight && img.Width < maxWidth) return img;
        using (img)
        {
            Double xRatio = (double)img.Width / maxWidth;
            Double yRatio = (double)img.Height / maxHeight;
            Double ratio = Math.Max(xRatio, yRatio);
            int nnx = (int)Math.Floor(img.Width / ratio);
            int nny = (int)Math.Floor(img.Height / ratio);
            Bitmap cpy = new Bitmap(nnx, nny, PixelFormat.Format32bppArgb);
            using (Graphics gr = Graphics.FromImage(cpy))
            {
                gr.Clear(Color.Transparent);

                // This is said to give best quality when resizing images
                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;

                gr.DrawImage(img,
                    new Rectangle(0, 0, nnx, nny),
                    new Rectangle(0, 0, img.Width, img.Height),
                    GraphicsUnit.Pixel);
            }
            return cpy;
        }

    }

    private MemoryStream BytearrayToStream(byte[] arr)
    {
        return new MemoryStream(arr, 0, arr.Length);
    }

    private void HandleImageUpload(System.Drawing.Bitmap bmpPostedImage, int maxWidth, int maxHeight, string filePath, string fileExtension)
    {
        System.Drawing.Image img = RezizeImage(bmpPostedImage, maxWidth, maxHeight);
        ImageFormat imageFormat = System.Drawing.Imaging.ImageFormat.Jpeg;
        switch (fileExtension)
        {
            case ".png":
                imageFormat = System.Drawing.Imaging.ImageFormat.Png;
                break;
            case ".gif":
                imageFormat = System.Drawing.Imaging.ImageFormat.Gif;
                break;
            case ".icon":
                imageFormat = System.Drawing.Imaging.ImageFormat.Icon;
                break;
            case ".bmp":
                imageFormat = System.Drawing.Imaging.ImageFormat.Bmp;
                break;
        }
        img.Save(filePath, imageFormat);
    }

    private string GetUniqueKey()
    {
        int maxSize = 6;
        int minSize = 6;
        char[] chars = new char[62];
        string a;
        a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        chars = a.ToCharArray();
        int size = maxSize;
        byte[] data = new byte[1];
        System.Security.Cryptography.RNGCryptoServiceProvider crypto = new System.Security.Cryptography.RNGCryptoServiceProvider();
        crypto.GetNonZeroBytes(data);
        size = maxSize;
        data = new byte[size];
        crypto.GetNonZeroBytes(data);
        System.Text.StringBuilder result = new System.Text.StringBuilder(size);
        foreach (byte b in data)
        {
            result.Append(chars[b % (chars.Length - minSize)]);
        }
        return result.ToString();
    }
}