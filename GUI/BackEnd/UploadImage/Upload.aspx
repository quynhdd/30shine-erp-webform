﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Upload.aspx.cs" Inherits="_30shine.GUI.BackEnd.UploadImage.Upload" MasterPageFile="~/TemplateMaster/WorkFlow.Master" %>

<asp:Content ID="MainContent" ContentPlaceHolderID="Main" runat="server">

<script src="/Assets/js/jquery.v1.11.1.js"></script>
<script src="/Assets/js/dropzone.js"></script>
<link href="/Assets/css/dropzone.css" rel="stylesheet" />

<div id="dZUpload" class="dropzone">
    <div class="dz-default dz-message">
        Drop image here. 
    </div>
</div>
<div id="btnUpload" style="width: 100px; height: 100px; background: green;">
</div>
<script type="text/javascript">

    jQuery(document).ready(function ($) {
        Dropzone.autoDiscover = false;
        $("#dZUpload").dropzone({
            url: "DropZoneUploader.ashx",
            maxFiles: 100,
            addRemoveLinks: true,
            success: function (file, response) {
                var imgName = response;
                file.previewElement.classList.add("dz-success");
            },
            error: function (file, response) {
                file.previewElement.classList.add("dz-error");
            }
        });
        $("#btnUpload").bind("click", function () {
            $("#dZUpload").click();
        })        
    });
</script>

</asp:Content>
