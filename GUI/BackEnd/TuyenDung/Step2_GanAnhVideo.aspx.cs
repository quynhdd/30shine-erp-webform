﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Script.Serialization;

namespace _30shine.GUI.BackEnd.TuyenDung
{
    public partial class Step2_GanAnhVideo : System.Web.UI.Page
    {
        protected TuyenDung_UngVien OBJ;
        protected string _Code;
        protected bool _IsUpdate = false;
        public UIHelpers _UIHelpers = new UIHelpers();
        protected cls_media Img = new cls_media();
        protected List<cls_media> listImg = new List<cls_media>();

        private bool Perm_Access = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckPermission();

            if (!IsPostBack)
            {
                if (IsUpdate())
                {
                    Bind_OBJ();
                }
                else
                {
                    //
                }

            }
        }

        protected void ExcAddOrUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
            {
                Update();
            }
            else
            {
                Add();
            }
        }

        private void Add()
        {
            //           
        }

        private void Update()
        {
            using (var db = new Solution_30shineEntities())
            {
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.TuyenDung_UngVien.FirstOrDefault(w => w.Id == Id);

                    if (!OBJ.Equals(null))
                    {
                        //OBJ.ImgSkill = HDF_IMG_SKILL.Value;
                        OBJ.VideoLink = VideoLink.Text;
                        OBJ.Step2Note = Step2Note.Text;

                        if (OBJ.Step2Time == null)
                        {
                            OBJ.Step2Time = DateTime.Now;                            
                        }
                        else
                        {
                            OBJ.Step2ModifiedTime = DateTime.Now;
                        }

                        db.TuyenDung_UngVien.AddOrUpdate(OBJ);
                        var exc = db.SaveChanges();
                        if (exc > 0)
                        {
                            var MsgParam = new List<KeyValuePair<string, string>>();
                            MsgParam.Add(new KeyValuePair<string, string>("msg_add_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_add_message", "Cập nhật thành công!"));
                            UIHelpers.Redirect("/admin/tuyen-dung/step-2/" + OBJ.Id + ".html", MsgParam);
                        }
                        else
                        {
                            var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                            var status = "warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                        }
                    }
                    else
                    {
                        var msg = "Lỗi! Không tìm thấy bản ghi.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);

                    }
                }
                else
                {
                    MsgSystem.Text = "Lỗi! Không tìm thấy bản ghi.";
                    MsgSystem.CssClass = "msg-system warning";
                }

                Context.RewritePath("+", "", "");
            }
        }

        private bool Bind_OBJ()
        {
            using (var db = new Solution_30shineEntities())
            {
                var serialize = new JavaScriptSerializer();
                var ExistOBJ = true;
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.TuyenDung_UngVien.FirstOrDefault(w => w.Id == Id);

                    if (!OBJ.Equals(null))
                    {
                        FullName.Text = OBJ.FullName;
                        Phone.Text = OBJ.Phone;
                        CMT.Text = OBJ.CMT;
                        //HDF_IMG_SKILL.Value = OBJ.ImgSkill;
                        VideoLink.Text = OBJ.VideoLink;
                        Step2Note.Text = OBJ.Step2Note;
                    }
                    else
                    {
                        ExistOBJ = false;
                        var msg = "Lỗi! Bản ghi không tồn tại";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
                else
                {
                    ExistOBJ = false;
                    var msg = "Lỗi! Bản ghi không tồn tại";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }

                return ExistOBJ;
            }
        }

        public void ReloadPage()
        {
            HttpRequest Request = HttpContext.Current.Request;
            string host = "http://" + Request.Url.Host.ToString();
            string rawUrl = Request.RawUrl.ToString();
            string Url = host + rawUrl;
            Response.Redirect(Url);
        }

        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["User_Id"] == null)
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "employer_step2" };
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        public class cls_media
        {
            public string url { get; set; }
            public string title { get; set; }
            public string description { get; set; }
        }
    }
}