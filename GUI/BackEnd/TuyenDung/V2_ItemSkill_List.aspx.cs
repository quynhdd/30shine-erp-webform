﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
namespace _30shine.GUI.BackEnd.TuyenDung
{

    public partial class V2_ItemSkill_List : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        private bool Perm_Access = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckPermission();
            if (!IsPostBack)
            {
                BindList();
                Bind_Paging();
            }
        }
        #region[Bind_Danhsach]
        private void BindList()
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = db.TuyenDung_Skill.Where(w => w.IsDelete == false).OrderBy(o => o.Id).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                Rpt.DataSource = list;
                Rpt.DataBind();
            }
        }
        #endregion
        #region[Paging]
        protected void Bind_Paging()
        {
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }
        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                var Count = db.TuyenDung_Skill.Count(w => w.IsDelete == false);
                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }
        #endregion
        #region[CheckPermis]
        private void CheckPermission()
        {
            if (Session["User_Id"] == null)
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin" };
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        #endregion
    }
}