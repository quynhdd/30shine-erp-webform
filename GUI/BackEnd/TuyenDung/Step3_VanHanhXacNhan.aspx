﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Step3_VanHanhXacNhan.aspx.cs" Inherits="_30shine.GUI.BackEnd.TuyenDung.Step3_VanHanhXacNhan" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ProductAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <link href="/Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="/Assets/js/select2/select2.min.js"></script>
        <link href="/Assets/js/fancybox/jquery.fancybox.min.css" rel="stylesheet" />
        <script src="/Assets/js/fancybox/jquery.fancybox.min.js"></script>
        <style>
            #radioApprove tr { display: inline-table; padding-right: 15px; cursor: pointer; }
            #radioApprove tr td input { margin-left: 15px; cursor: pointer; }
            #radioApprove tr td label { margin-left: 5px; position: relative; top: -1px; cursor: pointer; }

            .listing-img-upload .thumb-wp { border: none; }
            /*.listing-img-upload .thumb-wp img { position: relative; width: auto !important; height: auto !important; max-width: 100%; }*/
            .ddl {
            .table-wp { background: #f5f5f5; float: left; width: 100%; padding: 15px; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; -webkit-box-shadow: 0px 0px 3px 0px #ddd; -moz-box-shadow: 0px 0px 3px 0px #ddd; box-shadow: 0px 0px 3px 0px #ddd; }
            .btn { border-radius: 5px; margin-left: 10px; margin-right: 10px; height: 70px; }
            .btn:hover { background-color: orangered; color: white; }
            .send { box-shadow: 2px 2px #b0b0b0; float: left; }
            .senddd { box-shadow: 2px 2px #b0b0b0; float: left; width: 100px; height: 50px; }
            .senddd:hover { background-color: #2b8a05; color: white; }
            .cancel { border-radius: 5px;}
            .canceldd { float: right; }
            .cancel:hover { background-color: #50b347; color: white; }
            .table-wp { background: #f5f5f5; float: left; width: 100%; padding: 15px; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; -webkit-box-shadow: 0px 0px 3px 0px #ddd; -moz-box-shadow: 0px 0px 3px 0px #ddd; box-shadow: 0px 0px 3px 0px #ddd; }
            .tr-skill { display: none; }
            .tr-show { display: table-row; }
            .customer-add .table-add tr td.right .fb-cover-error { top: 0; right: auto; }
            .gallery { display: inline-block; margin-top: 20px; }
            .thumbnailImage { margin-bottom: 6px; width: 100%; }
            .modal-image { display: none; position: fixed; /* Stay in place */ z-index: 1; /* Sit on top */ /* Location of the box */ left: 0; top: 0; width: 100%; /* Full width */ height: 100%; /* Full height */ overflow: auto; /* Enable scroll if needed */ background-color: rgb(0,0,0); /* Fallback color */ background-color: rgba(0,0,0,0.9); /* Black w/ opacity */ }

            /* Modal Content (image) */
            .modal-content-image {
                margin: auto;
                margin-top: 30px;
                display: block;
                width: 50%;
                max-width: 500px;
                height: auto;
                max-height: 600px;
            }

            /* Caption of Modal Image */
            #caption-image {
                margin: auto;
                display: block;
                width: 60%;
                max-width: 700px;
                text-align: center;
                color: #ccc;
                padding: 10px 0;
                height: 150px;
                margin-top: 10px;
            }

            /* Add Animation */
            .modal-content-image, #caption-image {
                -webkit-animation-name: zoom;
                -webkit-animation-duration: 0.6s;
                animation-name: zoom;
                animation-duration: 0.6s;
            }

            @-webkit-keyframes zoom {
                from {
                    -webkit-transform: scale(0);
                }

                to {
                    -webkit-transform: scale(1);
                }
            }

            @keyframes zoom {
                from {
                    transform: scale(0.1);
                }

                to {
                    transform: scale(1);
                }
            }

            /* The Close Button */
            .close-image { position: absolute; top: 15px; right: 35px; color: #f1f1f1; font-size: 40px; font-weight: bold; transition: 0.3s; }

                .close-image:hover,
            .close-image:focus { color: #bbb; text-decoration: none; cursor: pointer; }
            .btn-send:hover { background: #50b347; color: #ffffff; }
            .btn-send { width: auto !important; height: 30px; line-height: 30px; float: left; margin-right: 20px; margin-top:20px; background: #ddd; text-align: center; cursor: pointer; border-radius: 2px; }
            .one{
                background:#ddd; border-radius:5px; width:auto; padding-top:5px;
                padding-bottom:5px; height:30px;line-height: 30px;
                }
            .one:hover{background: #50b347; color: #ffffff;}
           
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Tuyển dụng &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/admin/tuyen-dung/step-3/danh-sach">Danh sách ứng viên</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add admin-product-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="table-wp">
                    <table class="table-add admin-product-table-add">
                        <tbody>
                            <tr class="title-head ">
                                <td><strong>Phê duyệt ứng viên</strong></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="myModalimage" style="z-index: 2000;" class="modal modal-image">
                                        <img class="modal-content-image" id="img01">
                                        <div id="captionimage"></div>
                                    </div>
                                </td>
                            </tr>
                            <tr>

                                <td class="col-xs-3 left"><span>Họ tên</span></td>
                                <td class="col-xs-3 right">
                                    <asp:TextBox ID="FullName" CssClass="form-control" runat="server" ClientIDMode="Static" Enabled="false" placeholder="Họ tên" Style="width: 220px;"></asp:TextBox>
                                    <% if (OBJ != null && OBJ.MainImg != null && OBJ.MainImg != "")
                                        { %>
                                    <div class="gallery" style="float: left; cursor: pointer; z-index: 200; position: absolute; top: -14px; width: auto; padding-left: 20px;">

                                        <img class="example-image form-control" id="myImg1" alt="" title="" src="<%=OBJ.MainImg %>"
                                            data-img="<%=OBJ.MainImg %>" style="width: 300px; height: auto; max-height: 280px; left: 0px; max-width: 100%;" />
                                    </div>
                                    <% } %> 
                                </td>

                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Salon</span></td>
                                <td class="col-xs-3 right ">
                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlSalon" Style="width: 220px;" ClientIDMode="Static" Enabled="false"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Bộ phận</span></td>
                                <td class="col-xs-3 right ">
                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlDepartment" Style="width: 220px;" ClientIDMode="Static" Enabled="false"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Số điện thoại</span></td>
                                <td class="col-xs-3 right"><span>
                                    <asp:TextBox ID="Phone" runat="server" CssClass="form-control" ClientIDMode="Static" Enabled="false" Style="width: 220px;"></asp:TextBox>
                                </span></td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Số CMT</span></td>
                                <td class="col-xs-3 right">
                                    <span>
                                        <asp:TextBox ID="CMT" CssClass="form-control" runat="server" ClientIDMode="Static" Style="width: 220px;" placeholder="Số CMT" Enabled="false"></asp:TextBox>
                                    </span>

                                </td>
                            </tr>
                            <tr class="image tr-upload">
                                <td class="col-xs-3 left"><span>Ảnh CMT</span></td>
                                <td class="col-xs-9 right" colspan="2">
                                    <div class="wrap btn-upload-wp HDF_IMG_CMT1" style="margin-bottom: 5px">
                                        <div id="Btn_UploadImg" class="btn-upload-img">
                                            Ảnh CMT 1
                                        </div>
                                        <div class="wrap listing-img-upload" style="margin-top: 20px">
                                            <% if (OBJ != null && OBJ.CMTimg1 != null && OBJ.CMTimg1 != "")
                                                { %>
                                            <div class=" gallery">
                                                <img class="example-image form-control" alt="" title="Image 1" src="<%=OBJ.CMTimg1 %>"
                                                    data-img="<%=OBJ.CMTimg1 %>" style="width: 220px; max-height: 200px; height: auto; left: 0px; margin-right: 20px" />
                                            </div>
                                            <% } %><% if (OBJ != null && OBJ.CMTimg2 != null && OBJ.CMTimg2 != "")
                                                       { %>
                                            <div class=" gallery">

                                                <img class="example-image form-control" alt="" title="Image 2" src="<%=OBJ.CMTimg2 %>"
                                                    data-img="<%=OBJ.CMTimg2 %>" style="width: 220px; max-height: 200px; height: auto;" />
                                            </div>
                                            <% } %>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                            <%-- Modal show image --%>

                            <tr class=" tr-upload tr-skill tr-skill-img" <% if (OBJ != null && OBJ.DepartmentId == 2)
                                { %>
                                <%} %>>
                                <td class="col-xs-3 left"><span>Ảnh kỹ năng</span></td>
                                <td class="col-xs-9 right image">
                                    <div class="wrap btn-upload-wp HDF_IMG_SKILL" style="margin-bottom: 5px; width: auto;">
                                        <div class="wrap listing-img-upload image">
                                            <% if (OBJ != null && OBJ.ImgSkill1 != null && OBJ.ImgSkill1 != "")
                                                { %>
                                            <div class="gallery">
                                                <img class="img-rounded myImg form-control" alt="" id="myImg2" title="Image 3" src="<%=OBJ.ImgSkill1 %>"
                                                    data-img="<%=OBJ.ImgSkill1 %>" style="width: 220px; max-height: 200px; height: auto; left: 0px; margin-right: 20px" />
                                            </div>
                                            <% } %>
                                            <% if (OBJ != null && OBJ.ImgSkill2 != null && OBJ.ImgSkill2 != "")
                                                { %>
                                            <div class="gallery">
                                                <img class="img-rounded myImg form-control" alt="" id="myImg3" title="Image 4" src="<%=OBJ.ImgSkill2 %>"
                                                    data-img="<%=OBJ.ImgSkill2 %>" style="width: 220px; height: auto; max-height: 200px" />
                                            </div>
                                            <% } %>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr class="tr-skill tr-skill-video" <% if (OBJ != null && OBJ.DepartmentId == 1)
                                { %>
                                <%} %>>
                                <td class="col-xs-3 left"><span>Link video</span></td>
                                <td class="col-xs-9 right" colspan="2">
                                    <span class="field-wp">
                                        <asp:TextBox ID="VideoLink" runat="server" ClientIDMode="Static" Style="margin-top: 10px" placeholder="Link video"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator ID="TitleValidate" ControlToValidate="Title" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập tên danh mục!"></asp:RequiredFieldValidator>--%>
                                    </span>

                                </td>
                            </tr>
                            <tr class="tr-skill tr-skill-text tr-description">
                                <td class="col-xs-2 left"><span>Kỹ năng sử dụng hóa chất</span></td>
                                <td class="col-xs-9 right" colspan="2">
                                    <span class=" field-wp">
                                        <asp:TextBox CssClass="form-control" TextMode="MultiLine" Rows="3" ID="KyNangSuDungHoaChat" runat="server" Enabled="false" ClientIDMode="Static"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" style="width: 10px"></td>
                            </tr>
                            <tr class="tr-description">
                                <td class="col-xs-2 left"><span>Ghi chú</span></td>
                                <td class="col-xs-9 right " colspan="2">
                                    <span class="field-wp">
                                        <asp:TextBox TextMode="MultiLine" CssClass="form-control" Rows="2" ID="Step2Note" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <td></td>
                                <td class="text-center" colspan="3">
                                     <asp:Button ID="btnDuyet" CssClass="btn-send" Height="30px" Width="100px" runat="server" Text="Duyệt thử việc" ClientIDMode="Static" OnClick="btnDuyet_Click"></asp:Button>
                                    <%--<asp:Button runat="server" CssClass="btn-send tr-skill tr-skill-btn " Text="Duyệt đào tạo" ID="btnUpdateDaoTao" OnClick="btnUpdateDaoTao_Click" />--%>
                                    <asp:Label runat="server" CssClass="btn-send" Height="30px" data-toggle="modal" data-target="#myModalKhongduyet">Không duyệt</asp:Label>
                                </td>
                                <td>
                                    <div class="modal fade" id="myModalKhongduyet" role="dialog">
                                        <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title"><strong>Đánh giá người xét duyệt</strong></h4>
                                                </div>
                                                <div class="modal-body">
                                                    <table class="table table-condensed">
                                                        <tbody>
                                                            <tr class="row">
                                                                <td class="col-xs-12">
                                                                    <asp:TextBox runat="server" TextMode="MultiLine" Rows="5" CssClass="form-control" ClientIDMode="Static" ID="danhGia"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr class="row">
                                                                <td class="col-xs-12">
                                                                    <asp:Button runat="server" ClientIDMode="Static" CssClass="btn-send" ID="btnUpdateKhongDuyet" OnClick="btnUpdateKhongDuyet_Click" Text="Hoàn tất" />
                                                                    <button type="button" class="cancel btn-cancel canceldd" data-dismiss="modal">Đóng</button>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <!-- Modal -->
                                    <div id="myModal" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <!-- Modal content-->
                                            <div class="modal-content" style="width: 750px;">
                                                <div class="modal-body">
                                                    <table class="table-add">
                                                        <tbody>
                                                            <tr class="title-head">
                                                                <td colspan="4" class="col-xs-12"><strong>Thông tin nhân viên</strong>
                                                                </td>

                                                            </tr>
                                                            <tr runat="server" id="TrSalon">
                                                                <td class="col-xs-3 left"><span>Salon</span></td>
                                                                <td class="col-xs-3 right">
                                                                    <span class="field-wp">
                                                                        <asp:DropDownList ID="Salon" CssClass="form-control select ddl" Enabled="false" runat="server" Width="100%" ClientIDMode="Static"></asp:DropDownList>

                                                                    </span>
                                                                </td>
                                                                <td class="col-xs-3 left"><span>Phân quyền</span></td>
                                                                <td class="col-xs-3 right">
                                                                    <span class="field-wp">
                                                                        <asp:DropDownList ID="PermissionList" CssClass="form-control select ddl" Width="100%" runat="server" ClientIDMode="Static"></asp:DropDownList>

                                                                    </span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="col-xs-3 left"><span>Bộ phận</span></td>
                                                                <td class="col-xs-3
                                                                     right">
                                                                    <span class="field-wp">
                                                                        <asp:DropDownList ID="TypeStaff" runat="server" CssClass="ddl" Width="100%" ClientIDMode="Static" Enabled="false"></asp:DropDownList>

                                                                    </span>
                                                                </td>
                                                                <td class="col-xs-3 left"><span>Họ và tên</span></td>
                                                                <td class="col-xs-3 right">
                                                                    <span class="field-wp">
                                                                        <asp:TextBox ID="HoTen" runat="server" ClientIDMode="Static" Enabled="false"></asp:TextBox>
                                                                        <asp:RequiredFieldValidator ID="FullNameValidate" ControlToValidate="FullName" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập họ tên!"></asp:RequiredFieldValidator>
                                                                    </span>

                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="col-xs-3 left"><span>Số CMND</span></td>
                                                                <td class="col-xs-3 right">
                                                                    <span class="field-wp">
                                                                        <asp:TextBox ID="SoCMT" runat="server" ClientIDMode="Static" Enabled="false"></asp:TextBox>
                                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="Phone" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập số điện thoại!"></asp:RequiredFieldValidator>--%>
                                                                    </span>
                                                                </td>
                                                                <td class="col-xs-3 left"><span>Ngày tính thâm niên</span></td>
                                                                <td class="col-xs-3 right">
                                                                    <span class="field-wp">
                                                                        <asp:TextBox ID="DateJoin" CssClass="txtDateTime" runat="server" ClientIDMode="Static"></asp:TextBox>
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="col-xs-3 left"><span>Bậc kỹ năng</span></td>
                                                                <td class="col-xs-3 right">
                                                                    <span class="field-wp">
                                                                        <asp:DropDownList ID="SkillLevel" CssClass="form-control select ddl" Width="100%" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                                                    </span>
                                                                </td>
                                                                <td class="col-xs-3 left"><span>Số điện thoại</span></td>
                                                                <td class="col-xs-3 right">
                                                                    <span class="field-wp">
                                                                        <asp:TextBox ID="txtsodienthoai" runat="server" ClientIDMode="Static" Enabled="false"></asp:TextBox>
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="col-xs-3 left"><span>Account</span></td>
                                                                <td class="col-xs-3 right">
                                                                    <span class="field-wp">
                                                                        <asp:TextBox ID="Email" runat="server" ClientIDMode="Static"></asp:TextBox>
                                                                    </span>
                                                                </td>
                                                                <td class="col-xs-3 left"><span>Giới tính</span></td>
                                                                <td class="col-xs-3 right">
                                                                    <span class="field-wp">
                                                                        <asp:DropDownList ID="Gender" CssClass="form-control select ddl" Width="100%" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="col-xs-3 left"><span>Ngân hàng</span></td>
                                                                <td class="col-xs-9 right" colspan="3">
                                                                    <div class="city" style="padding-bottom: 10px;">
                                                                        <asp:TextBox runat="server" ID="TenNganHang" placeholder="Tên ngân hàng" ClientIDMode="Static"></asp:TextBox>
                                                                    </div>
                                                                    <div class="district" style="padding-bottom: 10px;">
                                                                        <asp:TextBox runat="server" ID="SoTaiKhoan" placeholder="Số tài khoản" ClientIDMode="Static"></asp:TextBox>
                                                                    </div>

                                                                    <asp:TextBox ID="TenChiNhanh" runat="server" placeholder="Tên chi nhánh" ClientIDMode="Static"></asp:TextBox>
                                                                </td>
                                                            </tr>

                                                            <tr class="tr-upload">
                                                                <td class="col-xs-3 left"><span>Ảnh avatar</span></td>
                                                                <td class="col-xs-9 right" colspan="3">
                                                                    <div class="wrap btn-upload-wp HDF_MainImg" style="margin-bottom: 5px;">
                                                                        <%--<div id="Btn_UploadImg2" class="btn-upload-img" onclick="popupImageIframe1('HDF_MainImg')">Chọn ảnh đăng</div>--%>
                                                                        <asp:FileUpload ID="UploadFile" ClientIDMode="Static" Style="display: none;"
                                                                            CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                                                        <div class="wrap listing-img-upload">
                                                                            <% if (OBJ != null && OBJ.MainImg != null && OBJ.MainImg != "")
                                                                                { %>
                                                                            <div class="gallery">
                                                                                <img class="form-control" src="<%=OBJ.MainImg %>" id="myImg4"
                                                                                    data-img="<%=OBJ.MainImg %>" style="width: 300px; height: auto" />
                                                                            </div>
                                                                            <% } %>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr class="tr-birthday">
                                                                <td class="col-xs-3 left"><span>Sinh nhật</span></td>
                                                                <td class="col-xs-9 right" colspan="3">
                                                                    <span class="field-wp">
                                                                        <asp:TextBox ID="Day" runat="server" ClientIDMode="Static" placeholder="Ngày"></asp:TextBox>
                                                                        <asp:TextBox ID="Month" runat="server" ClientIDMode="Static" placeholder="Tháng"></asp:TextBox>
                                                                        <asp:TextBox ID="Year" runat="server" ClientIDMode="Static" placeholder="Năm"></asp:TextBox>
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                            <tr class="staff-servey-mark">
                                                                <td class="col-xs-3 left"><span>Trạng thái</span></td>
                                                                <td class="col-xs-9 right" colspan="3">
                                                                    <span class="field-wp">
                                                                        <asp:RadioButtonList CssClass="checkbuton" ID="Active" runat="server" ClientIDMode="Static">
                                                                            <asp:ListItem Class="one" Text="Đang hoạt động" Value="1"></asp:ListItem>
                                                                            <asp:ListItem Class="one" Text="Đã nghỉ" Value="0"></asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                            <tr class="staff-servey-mark">
                                                                <td class="col-xs-3 left"><span>Loại tài khoản</span></td>
                                                                <td class="col-xs-9 right" colspan="3">
                                                                    <span class="field-wp">
                                                                        <asp:RadioButtonList CssClass="checkbuton" ID="AccountType" runat="server" ClientIDMode="Static">
                                                                            <asp:ListItem Class="one" Text="Tài khoản nhân viên" Value="0"></asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                            <tr class="staff-servey-mark">
                                                                <td class="col-xs-3 left"><span>Tính lương</span></td>
                                                                <td class="col-xs-9 right" colspan="3">
                                                                    <span class="field-wp">
                                                                        <asp:RadioButtonList CssClass="checkbuton" ID="radioSalaryByPerson" runat="server" ClientIDMode="Static">
                                                                            <asp:ListItem Class="one" Text="Theo nhóm (Bộ phận)" Value="False"></asp:ListItem>
                                                                            <asp:ListItem Class="one" Text="Cá nhân" Value="True"></asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                            <tr class="staff-servey-mark">
                                                                <td class="col-xs-3 left"><span>Y/C điểm danh</span></td>
                                                                <td class="col-xs-9 right" colspan="3">
                                                                    <span class="field-wp">
                                                                        <asp:RadioButtonList CssClass="checkbuton" ID="radioEnroll" runat="server" ClientIDMode="Static">
                                                                            <asp:ListItem Class="one" Text="Có" Value="True"></asp:ListItem>
                                                                            <asp:ListItem Class="one" Text="Không" Value="False"></asp:ListItem>
                                                                        </asp:RadioButtonList>
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="col-xs-12" colspan="4"></td>
                                                            </tr>
                                                            <tr class="tr-send">

                                                                <td class="col-xs-6 right" colspan="4">
                                                                    <span class="field-wp">
                                                                        <%--<asp:Button ID="btnSaVe" CssClass="send btn-send" runat="server" Text="Hoàn tất" OnClick="btnSaVe_Click" ClientIDMode="Static"></asp:Button>--%>
                                                                    </span>
                                                                </td>
                                                                <td class="col-xs-6 left">
                                                                    <span>
                                                                        <button class="cancel btn-cancel" data-dismiss="modal" type="button">Close</button>
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="col-xs-12" colspan="4"></td>
                                                            </tr>
                                                            <asp:HiddenField runat="server" ID="HiddenField1" ClientIDMode="Static" />
                                                            <asp:HiddenField runat="server" ID="HiddenField2" ClientIDMode="Static" />
                                                            <asp:HiddenField ID="HDF_MainImg" runat="server" ClientIDMode="Static" />
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="modal-footer">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="tr-send">
                                <td class="col-xs-3 left"></td>
                                <td class="col-xs-9 right no-border"></td>
                            </tr>

                            <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="HiddenFieldUVId" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="HDF_Images" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="HDF_IMG_CMT1" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="HDF_IMG_CMT2" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="HDF_IMG_MAIN" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="HDF_IMG_SKILL1" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="HDF_IMG_SKILL2" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="HDF_IdLevel" ClientIDMode="Static" />
                        </tbody>
                    </table>
                </div>
            </div>
            <%-- end Add --%>
        </div>
        <script type="text/javascript">
            $('select').select2();
        </script>
        <script>
            function choseDepartment(This) {
                var id = parseInt($("#ddlDepartment").val());
                //console.log(id);
                if (!isNaN(id)) {
                    if (id == 1) {
                        $(".tr-skill").removeClass('tr-show');
                        $(".tr-skill-img").addClass("tr-show");
                        $(".tr-skill-btn").addClass("tr-show");
                        $(".tr-skill-text").addClass("tr-show");
                    }
                    else if (id == 2) {
                        $(".tr-skill").removeClass('tr-show');
                        $(".tr-skill-video").addClass("tr-show");
                        $(".tr-skill-text").addClass("tr-show");
                    }
                    else if (id == 0) {
                        $(".tr-skill").removeClass('tr-show');
                    }
                    else {
                        $(".tr-skill").removeClass('tr-show');
                    }
                }
                else {
                    $(".tr-skill").removeClass('tr-show');
                }
            }
            jQuery(document).ready(function () {

                $("#glbAdminContent").addClass("active");
                $("#glbAdminCategory").addClass("active");
                //date picker
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        //$input.val($input.val().split(' ')[0]);
                    }
                });
                //excThumbWidth(120, 120);

                choseDepartment(null);
            });
            //show image
            var modal = document.getElementById('myModalimage');
            // Get the image and insert it inside the modal - use its "alt" text as a caption
            var img = document.getElementById('myImg1');
            var img1 = document.getElementById('myImg2');
            var img2 = document.getElementById('myImg3');
            var img3 = document.getElementById('myImg4');
            var modalImg = document.getElementById("img01");
            var captionText = document.getElementById("captionimage");

            try {
                img.onclick = function () {

                    modal.style.display = "block";
                    modalImg.src = this.src;
                    captionText.innerHTML = this.alt;
                }
                img1.onclick = function () {
                    modal.style.display = "block";
                    modalImg.src = this.src;
                    captionText.innerHTML = this.alt;
                }
                img2.onclick = function () {
                    modal.style.display = "block";
                    modalImg.src = this.src;
                    captionText.innerHTML = this.alt;
                }
                img3.onclick = function () {
                    modal.style.display = "block";
                    modalImg.src = this.src;
                    captionText.innerHTML = this.alt;
                }
                // When the user clicks on <span> (x), close the modal
                modal.onclick = function () {
                    console.log("a");
                    modal.style.display = "none";
                }
            } catch (err) { }


            $('#myModalimage').click(function () {
                $("#myModalimage").css("display", "none")
            });
            $('.modal-content-image').click(function (event) {
                event.stopPropagation();
            });


        </script>
        <!-- Popup plugin image hay là chưa gọi hàm getIdLevel nên nó k chạy  -->
        <script type="text/javascript">
            function popupImageIframe1(StoreImgField) {
                var Width = 960;
                var Height = 560;
                var ImgList = [];
                var iframe = $("<div class='iframe-image-upload' style='width:" + Width + "px; height:" + Height + "px; float:left; display:none;'><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + StoreImgField + "&callback=getThumbFromIframe&imgWidth=800&imgHeight=600&multi_img=false&Folder=Common' style='width:100%; height: 100%;'></iframe></div");

                $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
            }
            function popupImageIframe(StoreImgField) {
                var Width = 960;
                var Height = 560;
                var ImgList = [];
                var iframe = $("<div class='iframe-image-upload' style='width:" + Width + "px; height:" + Height + "px; float:left; display:none;'><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + StoreImgField + "&callback=getThumbFromIframe&imgWidth=800&imgHeight=600&multi_img=false&Folder=Common' style='width:100%; height: 100%;'></iframe></div");

                $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
            }

            window.getThumbFromIframe = function (Imgs, StoreImgField) {
                var imgs = "";
                var thumb = "";
                if (Imgs.length > 0) {
                    thumb = '<div class="thumb-wp">' +
                                '<img class="thumb" alt="" title="" src="' + Imgs[0] + '"' +
                                    'data-img="" />' +
                                '<span class="delete-thumb" onclick="deleteThumbnail($(this), \'' + StoreImgField + '\')"></span>' +
                            '</div>';
                    $("." + StoreImgField).find(".listing-img-upload").empty().append(thumb);
                    //excThumbWidth(120, 120);
                    $("#" + StoreImgField).val(Imgs[0]);
                }
                autoCloseEBPopup();
            }

            function deleteThumbnail(This, StoreImgField) {
                var imgs = "";
                var lst = This.parent().parent();
                This.parent().remove();
                lst.find("img.thumb").each(function () {
                    imgs += $(this).attr("src") + ",";
                });
                $("#" + StoreImgField).val(imgs);
            }

        </script>
    </asp:Panel>
</asp:Content>

