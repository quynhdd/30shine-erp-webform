﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Step2_GanAnhVideo.aspx.cs" Inherits="_30shine.GUI.BackEnd.TuyenDung.Step2_GanAnhVideo" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ProductAdd" ContentPlaceHolderID="CtMain" runat="server">
<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

<style>
    .table-wp { background: #f5f5f5; float: left; width: 100%; padding: 15px; -webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; 
        -webkit-box-shadow: 0px 0px 3px 0px #ddd;
        -moz-box-shadow:    0px 0px 3px 0px #ddd;
        box-shadow:         0px 0px 3px 0px #ddd;}
</style>

<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">
                <li>Tuyển dụng &nbsp;&#187; </li>
                <li class="li-listing"><a href="/admin/tuyen-dung/step-2/danh-sach">Danh sách ứng viên</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add admin-product-add">
    <%-- Add --%>                
    <div class="wp960 content-wp">
        <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
        <div class="table-wp">
            <table class="table-add admin-product-table-add">
                <tbody>
                    <tr class="title-head">
                        <td><strong>Nhập kỹ năng ứng viên</strong></td>
                        <td></td>
                    </tr>
                    <tr class="tr-margin" style="height: 20px;"></tr>
                    <tr>
                        <td class="col-xs-3 left"><span>Tên ứng viên</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="FullName" runat="server" ClientIDMode="Static" style="width: 220px;" placeholder="Họ tên" Enabled="false"></asp:TextBox>
                                <asp:TextBox ID="Phone" runat="server" ClientIDMode="Static" style="width: 220px; margin-left: 10px;" placeholder="Số điện thoại" Enabled="false"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="TitleValidate" ControlToValidate="Title" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập tên danh mục!"></asp:RequiredFieldValidator>--%>
                            </span>
                                            
                        </td>
                    </tr> 
                    
                    <tr>
                        <td class="col-xs-3 left"><span>Số CMT/Thẻ căn cước</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="CMT" runat="server" ClientIDMode="Static" style="width: 220px;" placeholder="Số CMT" Enabled="false"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="TitleValidate" ControlToValidate="Title" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập tên danh mục!"></asp:RequiredFieldValidator>--%>
                            </span>
                                            
                        </td>
                    </tr>                  

                    <%--<tr class="tr-description">
                        <td class="col-xs-3 left"><span>Mô tả</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox TextMode="MultiLine" Rows="5" ID="Description" runat="server" ClientIDMode="Static"></asp:TextBox>
                            </span>
                        </td>
                    </tr>--%>

                    <tr class="tr-upload" <% if (OBJ != null && OBJ.DepartmentId == 2)
                        { %> style="display:none"<%} %> >
                        <td class="col-xs-3 left"><span>Ảnh kỹ năng</span></td>
                        <td class="col-xs-7 right">
                            <div class="wrap btn-upload-wp HDF_IMG_SKILL" style="margin-bottom: 5px; width: auto;">
                                <div id="Btn_UploadImg" class="btn-upload-img" onclick="popupImageIframe('HDF_IMG_SKILL')">Ảnh kỹ năng</div>
                                <asp:FileUpload ID="UploadFile" ClientIDMode="Static" style="display:none;"
                                    CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                <div class="wrap listing-img-upload">
                                    <% if(OBJ != null && OBJ.ImgSkill != null && OBJ.ImgSkill != ""){ %>
                                        <div class="thumb-wp">
                                            <img class="thumb" alt="" title="" src="<%=OBJ.ImgSkill %>" 
                                                data-img="<%=OBJ.ImgSkill %>" />
                                            <span class="delete-thumb" onclick="deleteThumbnail($(this), 'HDF_IMG_SKILL')"></span>
                                        </div>
                                    <% } %>
                                </div>
                            </div>                    
                        </td>
                    </tr>

                    <tr <% if (OBJ != null && OBJ.DepartmentId == 1)
                        { %> style="display:none"<%} %>>
                        <td class="col-xs-3 left"><span>Link video</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="VideoLink" runat="server" ClientIDMode="Static" placeholder="Link video"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="TitleValidate" ControlToValidate="Title" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập tên danh mục!"></asp:RequiredFieldValidator>--%>
                            </span>
                                            
                        </td>
                    </tr>

                    <tr class="tr-description">
                        <td class="col-xs-2 left"><span>Ghi chú</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox TextMode="MultiLine" Rows="3" ID="Step2Note" runat="server" ClientIDMode="Static"></asp:TextBox>
                            </span>
                        </td>
                    </tr>
                    
                    <tr class="tr-send">
                        <td class="col-xs-3 left"></td>
                        <td class="col-xs-9 right no-border">
                            <span class="field-wp">
                                <asp:Button ID="Send" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="ExcAddOrUpdate"></asp:Button>
                                <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>
                            </span>
                        </td>
                    </tr>
                    <asp:HiddenField runat="server" ID="UrlKeyField"  ClientIDMode="Static"/>
                    <asp:HiddenField runat="server" ID="ImgCoverField"  ClientIDMode="Static"/>
                    <asp:HiddenField runat="server" ID="HDF_Images"  ClientIDMode="Static"/>
                    <asp:HiddenField runat="server" ID="HDF_IMG_SKILL"  ClientIDMode="Static"/>                
                </tbody>
            </table>
        </div>
    </div>
    <%-- end Add --%>
</div>

<script>
    jQuery(document).ready(function () {
        $("#glbAdminContent").addClass("active");
        $("#glbAdminCategory").addClass("active");

        excThumbWidth(120, 120);
    });
</script>

<!-- Popup plugin image -->
<script type="text/javascript">
    function popupImageIframe(StoreImgField) {
        var Width = 960;
        var Height = 560;
        var ImgList = [];
        var iframe = $("<div class='iframe-image-upload' style='width:" + Width + "px; height:" + Height + "px; float:left; display:none;'><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + StoreImgField + "&callback=getThumbFromIframe&imgWidth=800&imgHeight=600&multi_img=false&Folder=Common' style='width:100%; height: 100%;'></iframe></div");

        $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
    }

    window.getThumbFromIframe = function (Imgs, StoreImgField) {
        var imgs = "";
        var thumb = "";
        if (Imgs.length > 0) {
            thumb = '<div class="thumb-wp">' +
                        '<img class="thumb" alt="" title="" src="' + Imgs[0] + '"' +
                            'data-img="" />' +
                        '<span class="delete-thumb" onclick="deleteThumbnail($(this), \'' + StoreImgField + '\')"></span>' +
                    '</div>';
            $("." + StoreImgField).find(".listing-img-upload").empty().append(thumb);
            excThumbWidth(120, 120);
            $("#" + StoreImgField).val(Imgs[0]);
        }
        autoCloseEBPopup();
    }

    function deleteThumbnail(This, StoreImgField) {
        var imgs = "";
        var lst = This.parent().parent();
        This.parent().remove();
        lst.find("img.thumb").each(function () {
            imgs += $(this).attr("src") + ",";
        });
        $("#" + StoreImgField).val(imgs);
    }
</script>
<!-- Popup plugin image -->

</asp:Panel>
</asp:Content>

