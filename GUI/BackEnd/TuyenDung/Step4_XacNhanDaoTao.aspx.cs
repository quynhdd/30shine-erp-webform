﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Script.Serialization;
namespace _30shine.GUI.BackEnd.TuyenDung
{
    public partial class Step4_XacNhanDaoTao : System.Web.UI.Page
    {

        protected TuyenDung_UngVien OBJ;
        protected string _Code;
        protected bool _IsUpdate = false;
        public UIHelpers _UIHelpers = new UIHelpers();
        protected cls_media Img = new cls_media();
        protected List<cls_media> listImg = new List<cls_media>();
        protected bool HasImages = false;
        private bool Perm_AllPermission = false;
        private bool Perm_Access = false;
        private bool _boolean;
        private int integer;
        protected List<string> ListImagesUrl = new List<string>();

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckPermission();

            if (!IsPostBack)
            {
                Bind_SkillLevel();
                Bind_TypeStaff();
                Bind_Permission();
                Bind_salon();
                Bind_Gender();
                Bind_Salon();
                //Bind_Staff();
                department();
                if (IsUpdate())
                {
                    Bind_OBJ();
                }
                else
                {
                    //
                }

            }
        }

        

        private void Add()
        {
            //           
        }


        private bool Bind_OBJ()
        {
            using (var db = new Solution_30shineEntities())
            {
                var serialize = new JavaScriptSerializer();
                var ExistOBJ = true;
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.TuyenDung_UngVien.FirstOrDefault(w => w.Id == Id);

                    if (!OBJ.Equals(null))
                    {

                        HoTen.Text = OBJ.FullName;
                        SoCMT.Text = OBJ.CMT;
                        txtsodienthoai.Text = OBJ.Phone;
                        FullName.Text = OBJ.FullName;
                        Phone.Text = OBJ.Phone;
                        CMT.Text = OBJ.CMT;
                        ddlSalon.SelectedValue = OBJ.salonId.ToString();
                        Salon.SelectedValue = OBJ.salonId.ToString();
                        //NgaySinh.Text = string.Format("{0:mm/DD/yyyy}" ,OBJ.NgaySinh);
                        KyNangSuDungHoaChat.Text = OBJ.KyNangHoaChat;
                        TypeStaff.SelectedValue = OBJ.DepartmentId.ToString();
                        ddlDepartment.SelectedValue = OBJ.DepartmentId.ToString();
                        if (OBJ.DepartmentId == 1)
                        {
                            HDF_IMG_SKILL1.Value = OBJ.ImgSkill1;
                            HDF_IMG_SKILL2.Value = OBJ.ImgSkill2;

                        }
                        else if (OBJ.DepartmentId == 2)
                        {
                            //VideoLink.Text = OBJ.VideoLink;
                        }
                        HDF_MainImg.Value = OBJ.MainImg;
                        HiddenFieldUVId.Value = OBJ.Id.ToString();
                        //Step2Note.Text = OBJ.Step2Note;
                    }
                    else
                    {
                        ExistOBJ = false;
                        var msg = "Lỗi! Bản ghi không tồn tại";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
                else
                {
                    ExistOBJ = false;
                    var msg = "Lỗi! Bản ghi không tồn tại";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }

                return ExistOBJ;
            }
        }

        public void ReloadPage()
        {
            HttpRequest Request = HttpContext.Current.Request;
            string host = "http://" + Request.Url.Host.ToString();
            string rawUrl = Request.RawUrl.ToString();
            string Url = host + rawUrl;
            Response.Redirect(Url);
        }

        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["User_Id"] == null)
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "employer_step3" };
                string[] AllowAllData = new string[] { "root", "admin" };

                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }

                if (Array.IndexOf(AllowAllData, Permission) != -1)
                {
                    Perm_AllPermission = true;
                }
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        public class cls_media
        {
            public string url { get; set; }
            public string title { get; set; }
            public string description { get; set; }
        }
        public void Bind_SkillLevel()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Tbl_SkillLevel.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Order).ToList();
                SkillLevel.DataTextField = "Name";
                SkillLevel.DataValueField = "Id";
                SkillLevel.SelectedIndex = 0;
                SkillLevel.DataSource = lst;
                SkillLevel.DataBind();
            }
        }
      

        public void department()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Order).ToList();
                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "Id";
                ddlDepartment.SelectedIndex = 0;
                ddlDepartment.DataSource = LST;
                ddlDepartment.DataBind();
            }
        }
        public void Bind_TypeStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Order).ToList();

                TypeStaff.DataTextField = "Name";
                TypeStaff.DataValueField = "Id";
                TypeStaff.SelectedIndex = 0;
                TypeStaff.DataSource = LST;
                TypeStaff.DataBind();
            }
        }
        public void Bind_salon()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Tbl_Salon.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Order).ToList();

                ddlSalon.DataTextField = "Name";
                ddlSalon.DataValueField = "Id";
                ddlSalon.SelectedIndex = 0;
                ddlSalon.DataSource = LST;
                ddlSalon.DataBind();
            }
        }
        private void Bind_Permission()
        {
            PermissionList.DataTextField = "Permission";
            PermissionList.DataValueField = "Value";
            var key = 0;

            var item = new ListItem("Nhân viên", "0");
            PermissionList.Items.Insert(key++, item);
            if (Perm_Access)
            {
                item = new ListItem("Lễ tân", "1");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Check-in", "7");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Check-out", "8");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("salonmanager", "11");
                PermissionList.Items.Insert(key++, item);
                // Hide Lễ tân
                PermissionList.Items.FindByValue("1").Enabled = false;
            }
            if (Perm_AllPermission)
            {
                item = new ListItem("Trưởng cửa hàng", "2");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Kế toán", "3");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Mode1", "4");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Admin", "5");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Khảo sát", "6");
                PermissionList.Items.Insert(key++, item);
                item = new ListItem("Online store", "9");
                PermissionList.Items.Insert(key++, item);
            }
            PermissionList.SelectedIndex = 0;
        }

        private void Bind_Gender()
        {
            Gender.DataTextField = "Name";
            Gender.DataValueField = "Id";
            ListItem item = new ListItem("Chọn giới tính", "0");
            Gender.Items.Insert(0, item);
            item = new ListItem("Nam", "1");
            Gender.Items.Insert(1, item);
            item = new ListItem("Nữ", "2");
            Gender.Items.Insert(2, item);
            item = new ListItem("Khác", "3");
            Gender.Items.Insert(3, item);
        }

        private void Bind_Salon()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Tbl_Salon.OrderBy(p => p.Name).ToList();

                Salon.DataTextField = "Name";
                Salon.DataValueField = "Id";
                Salon.SelectedIndex = 0;

                Salon.DataSource = lst;
                Salon.DataBind();
            }
        }


        protected void btnSaVe_Click(object sender, EventArgs e)
        {

            using (var db = new Solution_30shineEntities())
            {

                int Id = int.Parse(HiddenFieldUVId.Value);
                OBJ = db.TuyenDung_UngVien.FirstOrDefault(w => w.Id == Id);

                if (!OBJ.Equals(null))
                {
                    OBJ.UngVienStatusId = 4;
                    OBJ.ImgSkillDaoTao1 = HDF_IMG_SKILL_DaoTao_1.Value;
                    OBJ.ImgSkillDaoTao2 = HDF_IMG_SKILL_DaoTao_2.Value;
                    OBJ.StepEndNoteDaoTao = Step2Notesaudaotao.Text;
                    if (OBJ.StepEndTime == null)
                    {
                        OBJ.StepEndTime = DateTime.Now;
                    }
                    else
                    {
                        OBJ.StepEndModifiedTime = DateTime.Now;
                    }

                    db.TuyenDung_UngVien.AddOrUpdate(OBJ);
                    var exc = db.SaveChanges();
                    if (exc > 0)
                    {
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_add_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_add_message", "Cập nhật thành công!"));
                    }
                    else
                    {
                        var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
                else
                {
                    var msg = "Lỗi! Không tìm thấy bản ghi.";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);

                }
                Context.RewritePath("+", "", "");
                //them tai khoan
                var obj = new _30shine.MODEL.ENTITY.EDMX.Staff();
                int integer;
                bool _boolean;
                DateTime datetime;
                obj.Fullname = FullName.Text;
                obj.StaffID = SoCMT.Text;
                obj.DateJoin = DateTime.TryParse(DateJoin.Text, out datetime) ? datetime : Convert.ToDateTime(null);
                obj.SkillLevel = int.TryParse(SkillLevel.SelectedValue, out integer) ? integer : 0;
                obj.Phone = txtsodienthoai.Text;
                obj.Email = Email.Text;
                obj.Avatar = HDF_MainImg.Value;
                obj.isAccountLogin = int.TryParse(AccountType.SelectedValue, out integer) ? integer : 0;
                if (radioSalaryByPerson.SelectedValue == "True")
                {
                    obj.SalaryByPerson = true;
                }
                else
                {
                    obj.SalaryByPerson = false;
                }

                if (radioEnroll.SelectedValue == "True")
                {
                    obj.RequireEnroll = true;
                }
                else
                {
                    obj.RequireEnroll = false;
                }

                if (Day.Text.Length > 0)
                {
                    obj.SN_day = int.TryParse(Day.Text, out integer) ? integer : 0;
                }

                if (Month.Text.Length > 0)
                {
                    obj.SN_month = int.TryParse(Month.Text, out integer) ? integer : 0;
                }
                if (Year.Text.Length > 0)
                {
                    obj.SN_year = int.TryParse(Year.Text, out integer) ? integer : 0;
                }
                obj.CreatedDate = DateTime.Now;
                obj.NgayTinhThamNien = DateTime.TryParse(DateJoin.Text, out datetime) ? datetime : Convert.ToDateTime(null);
                obj.Gender = Convert.ToByte(Gender.SelectedValue);
                obj.Type = Convert.ToInt32(TypeStaff.SelectedValue);
                obj.SalonId = Convert.ToInt32(Salon.SelectedValue);
                obj.Password = "a05b49b7dc71773bc7ff4ef877824ffe";
                obj.IsDelete = 0;
                obj.Active = 1;
                obj.NganHang_Ten = TenNganHang.Text;
                obj.NganHang_SoTK = SoTaiKhoan.Text;
                obj.NganHang_ChiNhanh = TenChiNhanh.Text;
                //obj.NguoiGioiThieuID = Convert.ToInt32(ddlStaff.SelectedValue);
                int Error = 0;
                int ErrorPerm = 0;
                string[] PermLST = new string[] { "staff", "reception", "salonmanager", "accountant", "mode1", "admin", "servey", "checkin", "checkout", "onlinestore" };
                int index = PermissionList.SelectedValue != "" ? Convert.ToInt32(PermissionList.SelectedValue) : 0;
                if (index > PermLST.Length || (!Perm_AllPermission && index > 1))
                {
                    Error++;
                    ErrorPerm++;
                }
                else
                {
                    obj.Permission = PermLST[index];
                }

                if (ErrorPerm > 0)
                {
                    var msg = "Giá trị phân quyền không đúng.";
                    var status = "msg-system warning";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + msg + "','" + status + "');", true);
                }

                // Validate
                // Check trùng số điện thoại
                if (obj.Phone != "")
                {
                    var ExistPhone = db.Staffs.Count(w => w.Phone == obj.Phone);
                    if (ExistPhone > 0)
                    {
                        var msg = "Số điện thoại đã tồn tại. Bạn vui lòng nhập số điện thoại khác.";
                        var status = "msg-system warning";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + msg + "','" + status + "');", true);
                        Error++;
                    }
                }

                // Check trùng email
                if (obj.Email != "")
                {
                    var ExistEmail = db.Staffs.Count(w => w.Email == obj.Email);
                    if (ExistEmail > 0)
                    {
                        var msg = "Account đã tồn tại. Bạn vui lòng nhập Account khác.";
                        var status = "msg-system warning";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + msg + "','" + status + "');", true);
                        Error++;
                    }
                }

                if (Error == 0)
                {
                    db.Staffs.Add(obj);
                    db.SaveChanges();
                    // Update OrderCode
                    db.Database.ExecuteSqlCommand("update Staff set OrderCode = " + obj.Id + " where Id = " + obj.Id);
                    // Log skill level
                    StaffLib.AddLogSkillLevel(obj.Id, obj.SkillLevel.Value, db);
                    // Init flow salary
                    SalaryLib.initSalaryByStaffId(obj.Id, obj.CreatedDate.Value.Date, Convert.ToInt32(obj.SalonId) , db);

                    var MsgParam = new List<KeyValuePair<string, string>>();
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                    UIHelpers.Redirect("/admin/tuyen-dung/step-4/danh-sach-dao-tao/" + obj.Id + ".html", MsgParam);
                }
            }
        }
        protected void btnUpdateKhongDuyet_Click(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                int Id = int.Parse(HiddenFieldUVId.Value);
                OBJ = db.TuyenDung_UngVien.FirstOrDefault(w => w.Id == Id);

                if (!OBJ.Equals(null))
                {
                    OBJ.StepEndNoteStaff = danhGia.Text;
                    OBJ.UngVienStatusId = 2;
                    OBJ.ImgSkillDaoTao1 = HDF_IMG_SKILL_DaoTao_1.Value;
                    OBJ.ImgSkillDaoTao2 = HDF_IMG_SKILL_DaoTao_2.Value;
                    if (OBJ.StepEndTime == null)
                    {
                        OBJ.StepEndTime = DateTime.Now;
                    }
                    else
                    {
                        OBJ.StepEndModifiedTime = DateTime.Now;
                    }

                    db.TuyenDung_UngVien.AddOrUpdate(OBJ);
                    var exc = db.SaveChanges();
                    if (exc > 0)
                    {
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_add_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_add_message", "Cập nhật thành công!"));
                        UIHelpers.Redirect("/admin/tuyen-dung/step-3/danh-sach/");

                    }
                    else
                    {
                        var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
                else
                {
                    var msg = "Lỗi! Không tìm thấy bản ghi.";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);

                }
                Context.RewritePath("+", "", "");
            }
        }
    }
}