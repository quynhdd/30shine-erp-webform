﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PartTime_Add.aspx.cs" Inherits="_30shine.GUI.BackEnd.TimeKeeping.PartTime_Add" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ProductAdd" ContentPlaceHolderID="CtMain" runat="server">
<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">
                <li>Quản lý Part-Time &nbsp;&#187; </li>
                <li class="li-listing"><a href="/admin/cong-nhat/part-time.html">Danh sách</a></li>
                <li class="li-add"><a href="/khao-sat/nhan-vien/them-moi.html">Thêm mới</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add admin-product-add">
    <%-- Add --%>                
    <div class="wp960 content-wp">
        <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
        <div class="table-wp">
            <table class="table-add admin-product-table-add">
                <tbody>
                    <tr class="title-head">
                        <td><strong>Nhập nội dung part-time</strong></td>
                        <td></td>
                    </tr>
                    <tr class="tr-margin" style="height: 20px;"></tr>
                    <tr class="tr-field-ahalf" style="display:none;">
                        <td class="col-xs-2 left"><span>Ngày</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeFrom" placeholder="Chọn ngày"
                        ClientIDMode="Static" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="TimeValidate" ControlToValidate="TxtDateTimeFrom" runat="server" CssClass="fb-cover-error" Text="Bạn chưa chọn ngày!"></asp:RequiredFieldValidator>
                            </span>                                            
                        </td>
                    </tr>

                    <tr runat="server" id="TrSalon" class="tr-field-ahalf">
                        <td class="col-xs-2 left"><span>Salon</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:DropDownList ID="Salon" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <asp:RequiredFieldValidator InitialValue="0" 
                                    ID="ValidateSalon" Display="Dynamic" 
                                    ControlToValidate="Salon"
                                    runat="server"  Text="Bạn chưa chọn Salon!" 
                                    ErrorMessage="Vui lòng chọn Salon!"
                                    ForeColor="Red"
                                    CssClass="fb-cover-error">
                                </asp:RequiredFieldValidator>
                            </span>
                        </td>
                    </tr>

                    <tr class="tr-field-ahalf">
                        <td class="col-xs-2 left"><span>Nhân viên</span></td>
                        <td class="col-xs-9 right">
                                <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion" data-field="staff.name" data-value="0"
                                     AutoCompleteType="Disabled" ID="StaffName" ClientIDMode="Static" placeholder="Nhập tên nhân viên" runat="server"></asp:TextBox>
                                <div class="listing-staff-wp eb-select-data eb-suggestion-listing" style="width: 49%; z-index: 2000;">
                                    <ul class="ul-listing-staff ul-listing-suggestion" id="UlCustomerName">
                                    </ul>
                                </div>                            
                        </td>
                    </tr>

                    <tr class="tr-field-ahalf">
                        <td class="col-xs-2 left"><span>Giờ làm việc</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox CssClass="st-head" ID="WorkHour" placeholder="Nhập số giờ làm việc"
                        ClientIDMode="Static" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="WorkHour" runat="server" 
                                    CssClass="fb-cover-error" Text="Bạn chưa nhập giờ!"></asp:RequiredFieldValidator>
                            </span>                                            
                        </td>
                    </tr>

                    <%--<tr>
                        <td class="col-xs-3 left" style="padding-bottom: 0;"><span style="height: 28px; line-height: 28px;">Điểm đánh giá</span></td>
                        <td class="col-xs-9 right" style="padding-bottom: 0;">
                            <span class="field-wp" style="height: auto; line-height: 28px;">
                                <asp:RadioButtonList ID="Mark" runat="server" CssClass="staff-servey-mark" ClientIDMode="Static">
                                </asp:RadioButtonList>
                            </span>
                        </td>
                    </tr>

                    <tr>
                        <td class="col-xs-3 left" style="padding-top: 0;"><span style="height: 30px; line-height: 30px;">Nguồn đánh giá</span></td>
                        <td class="col-xs-9 right" style="padding-top: 0;">
                            <span class="field-wp" style="height: auto; line-height: 30px; height: 30px;">
                                <asp:RadioButtonList ID="Thread" runat="server" CssClass="staff-servey-mark" ClientIDMode="Static">
                                </asp:RadioButtonList>
                            </span>
                        </td>
                    </tr>--%>

                    <tr class="tr-description">
                        <td class="col-xs-2 left"><span>Ghi chú</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox TextMode="MultiLine" Rows="5" ID="Note" runat="server" ClientIDMode="Static"></asp:TextBox>
                            </span>
                        </td>
                    </tr>

                    <%--<tr>
                        <td class="col-xs-2 left"><span>Bộ phận</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:DropDownList ID="TypeStaff" runat="server" ClientIDMode="Static"></asp:DropDownList>
                            </span>
                        </td>
                    </tr>--%>
                    
                    <tr class="tr-send">
                        <td class="col-xs-2 left"></td>
                        <td class="col-xs-9 right no-border">
                            <span class="field-wp">
                                <asp:Button ID="Send" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="AddServey"></asp:Button>
                                <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>
                            </span>
                        </td>
                    </tr>
                    <asp:HiddenField runat="server" ID="UrlKeyField"  ClientIDMode="Static"/>
                    <asp:HiddenField runat="server" ID="ImgCoverField"  ClientIDMode="Static"/>
                    <asp:HiddenField ID="HDF_StaffId" ClientIDMode="Static" runat="server" />
                    <asp:HiddenField ID="HDF_StylistId" ClientIDMode="Static" runat="server" />
                    <asp:HiddenField ID="HDF_SkinnerId" ClientIDMode="Static" runat="server" />
                </tbody>
            </table>
        </div>
    </div>
    <%-- end Add --%>
</div>

<script>
    jQuery(document).ready(function () {
        $("#glbServey").addClass("active");
        $("#subMenu .li-add").addClass("active");

        //============================
        // Datepicker
        //============================
        $('.txtDateTime').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'vi',
            startDate: '2014/10/10',
            format: 'd/m/Y',
            dateonly: true,
            showHour: false,
            showMinute: false,
            timepicker: false,
            onChangeDateTime: function (dp, $input) {
                //$input.val($input.val().split(' ')[0]);
            }
        });

        // Bind Suggestion
        Bind_Suggestion();

        //============================
        // Show System Message
        //============================ 
        var qs = getQueryStrings();
        showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);
    });

    //============================
    // Suggestion Functions
    //============================
    function Bind_Suggestion() {
        $(".eb-suggestion").bind("keyup", function (e) {
            if (e.keyCode == 40) {
                UpDownListSuggest($(this));
            } else {
                Call_Suggestion($(this));
            }
        });
        $(".eb-suggestion").bind("focus", function () {
            Call_Suggestion($(this));
        });
        $(".eb-suggestion").bind("blur", function () {
            //Exc_To_Reset_Suggestion($(this));
        });
        $(window).bind("click", function (e) {
            if ((!e.target.className.match("eb-select") && e.target.parentElement.className.match("ul-listing-suggestion")) ||
                (!e.target.className.match("eb-select") && !e.target.parentElement.className.match("ul-listing-suggestion"))) {
                EBSelect_HideBox();
            }
        });
    }

    function UpDownListSuggest(This) {
        var UlSgt = This.parent().find(".ul-listing-suggestion"),
            index = 0,
            LisLen = UlSgt.find(">li").length - 1,
            Value;

        This.blur();
        UlSgt.find(">li.active").removeClass("active");
        UlSgt.find(">li:eq(" + index + ")").addClass("active");

        $(window).unbind("keydown").bind("keydown", function (e) {
            if (e.keyCode == 40) {
                if (index == LisLen) return false;
                UlSgt.find(">li.active").removeClass("active");
                UlSgt.find(">li:eq(" + (++index) + ")").addClass("active");
                return false;
            } else if (e.keyCode == 38) {
                if (index == 0) return false;
                UlSgt.find(">li.active").removeClass("active");
                UlSgt.find(">li:eq(" + (--index) + ")").addClass("active");
                return false;
            } else if (e.keyCode == 13) {
                // Bind data to HDF Field
                var THIS = UlSgt.find(">li.active");
                //var Value = THIS.text().trim();
                var Value = THIS.attr("data-code");
                var dataField = This.attr("data-field");
                var HDF_StoreValue;

                switch (dataField)
                {
                    case 'stylist.name': HDF_StoreValue = "#HDF_StylistId"; break;
                    case "skinner.name": HDF_StoreValue = "#HDF_SkinnerId"; break;
                    case "staff.name": HDF_StoreValue = "#HDF_StaffId"; break;
                }
                BindIdToHDF(THIS, Value, dataField, HDF_StoreValue, This);
                EBSelect_HideBox();
            }
        });
    }

    function Exc_To_Reset_Suggestion(This) {
        var value = This.val();
        if (value == "") {
            $(".eb-suggestion").each(function () {
                var THIS = $(this);
                var sgValue = THIS.val();
                if (sgValue != "") {
                    BindIdToHDF(THIS, sgValue, THIS.attr("data-field"), "#HDF_StylistId", "#HDF_SkinnerId", THIS);
                    return false;
                }
            });
        }
    }

    function Call_Suggestion(This) {
        var text = This.val(),
            field = This.attr("data-field");
        Suggestion(This, text, field);
    }

    function Suggestion(This, text, field) {
        var This = This;
        var text = text || "";
        var field = field || "";
        var InputDomId;
        var staffType;

        switch (field) {
            case "stylist.name": InputDomId = "#StylistName"; staffType = 1; HDF_StoreValue = "#HDF_StylistId"; break;
            case "skinner.name": InputDomId = "#SkinnerName"; staffType = 2; HDF_StoreValue = "#HDF_SkinnerId"; break;
            case "staff.name": InputDomId = "#StaffName"; staffType = 0; HDF_StoreValue = "#HDF_StaffId"; break;
        }
        if (text == "") {
            $(HDF_StoreValue).val("");
            return false;
        }

        $.ajax({
            type: "POST",
            url: "/GUI/SystemService/Ajax/Suggestion.aspx/Suggestion_Staff_ByName",
            data: '{field : "' + field + '", name : "' + text + '", staffType : '+ staffType +'}',
            contentType: "application/json; charset=utf-8",
            dataType: "json", success: function (response) {
                var mission = JSON.parse(response.d);
                if (mission.success) {
                    var OBJ = JSON.parse(mission.msg);
                    if (OBJ.length > 0) {
                        var lis = "";
                        $.each(OBJ, function (i, v) {
                            lis += "<li data-code='" + v.Id + "'" +
                                         "onclick=\"BindIdToHDF($(this),'" + v.Id + "','" + field + "','" + HDF_StoreValue +
                                         "','" + InputDomId + "')\">" +
                                        v.Value +
                                    "</li>";
                        });
                        This.parent().find("ul.ul-listing-suggestion").empty().append(lis).parent().show();
                        This.parent().find(".eb-select-data").show();
                    } else {
                        This.parent().find(".eb-select-data").hide().find("ul.ul-listing-suggestion").empty();
                    }
                } else {
                    This.parent().find("ul.ul-listing-suggestion").empty();
                    var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                    showMsgSystem(msg, "warning");
                }
            },
            failure: function (response) { alert(response.d); }
        });
    }

    function BindIdToHDF(THIS, Code, Field, HDF_StoreValue, Input_DomId) {
        var text = THIS.text().trim();
        $(HDF_StoreValue).val(Code);
        $(Input_DomId).val(text);
        $(Input_DomId).parent().find(".eb-select-data").hide();

        // Auto post server
        $("#BtnFakeUP").click();
    }

    function EBSelect_HideBox() {
        $(".eb-select-data").hide();
        $("ul.ul-listing-suggestion li.active").removeClass("active");
    }

    function showMsgSystem(msg, status) {
        $("#MsgSystem").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);
        setTimeout(function () {
            $("#MsgSystem").fadeTo("slow", 0, function () {
                $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 1);
            });
        }, 5000);
    }
</script>

</asp:Panel>
</asp:Content>
