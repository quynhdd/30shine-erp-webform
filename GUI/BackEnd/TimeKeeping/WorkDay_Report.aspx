﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WorkDay_Report.aspx.cs" Inherits="_30shine.GUI.BackEnd.TimeKeeping.WorkDay_Report" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">

    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý ngày công &nbsp;&#187; </li>
                        <li class="li-add"><a href="/admin/cong-nhat/diem-danh.html">Chấm công</a></li>
                        <li class="li-listing"><a href="/admin/cong-nhat/tong-cong.html">Tổng công</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row">
                    <div class="filter-item" style="margin-left: 0;">
                        <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Từ ngày</strong>
                        <div class="datepicker-wp">
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Chọn ngày" Style="width: 150px!important;"
                                ClientIDMode="Static" runat="server"></asp:TextBox>

                            <strong class="st-head" style="margin-left: 10px; margin-right: 10px;">
                                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                            </strong>
                            <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeTo" placeholder="Đến ngày" Style="width: 150px!important;"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                        </div>
                        <%--<br />              
                <div class="tag-wp">
                    <p class="tag tag-date" onclick="viewDataByDate($(this), '<%=Day1 %>')">Hôm kia <span class="txt-date"><%=Day1 %></span></p>
                    <p class="tag tag-date" onclick="viewDataByDate($(this), '<%=Day2 %>')">Hôm qua<span class="txt-date"><%=Day2 %></span></p>
                    <p class="tag tag-date tag-date-today" onclick="viewDataByDate($(this), '<%=Day3 %>')">Hôm nay<span class="txt-date"><%=Day3 %></span></p>
                </div>--%>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="Salon" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 150px;"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="StaffType" CssClass="form-control select" runat="server" ClientIDMode="static" Style="margin: 12px 0; width: 150px;"></asp:DropDownList>
                    </div>
                    <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" Text="Xem dữ liệu"
                        ClientIDMode="Static" onclick="excPaging(1)" runat="server">
                        Xem danh sách
                    </asp:Panel>
                    <a href="/admin/cong-nhat/tong-cong.html" class="st-head btn-viewdata">Reset Filter</a>
                    <div class="export-wp drop-down-list-wp" style="display: none;">
                        <asp:Panel ID="fff" CssClass="st-head btn-viewdata"
                            ClientIDMode="Static" runat="server" onclick="EntryExportExcel('all')">
                            Xuất File Excel
                        </asp:Panel>
                        <%--OnClick="Exc_ExportExcel" --%>
                        <ul class="ul-drop-down-list" style="display: none;">
                            <li onclick="EntryExportExcel('all')">Tất cả dữ liệu</li>
                            <li onclick="EntryExportExcel('current page')">Trang hiện tại</li>
                        </ul>
                    </div>
                    <%--<asp:Panel ID="Btn_ExportExcel" CssClass="st-head btn-viewdata"
                ClientIDMode="Static" onclick="excExcel('Export')" runat="server">Xuất File Excel</asp:Panel>--%>
                    <%--<asp:Panel ID="Btn_ImportExcel" CssClass="st-head btn-viewdata"
                ClientIDMode="Static" onclick="excExcel('Import')" runat="server">Nhập File Excel</asp:Panel>--%>
                </div>
                <!-- End Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>Danh sách nhân viên</strong>
                </div>
                <!-- Row Table Filter -->
                <div class="table-func-panel" style="margin-top: -33px;">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="200">200</li>
                                <%--<li data-value="1000000">Tất cả</li>--%>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="row table-wp">
                            <table class="table-add table-listing report-sales-listing table-enroll-listing" id="tableEnroll">
                                <thead>
                                    <tr>
                                        <th style="width: 10%">STT</th>
                                        <th style="width: 10%;">ID</th>
                                        <th style="width: 30%">Nhân viên</th>
                                        <th style="width: 20%">Loại nhân viên</th>
                                        <th class="th-enroll" style="width: 10%">Tổng công</th>
                                        <th class="th-parttime" style="width: 10%!important">Tổng giờ làm part-time (làm tăng ca)</th>
                                        <th style="width: 10%">Chi tiết</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="Rpt_PartTime" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td><%# Eval("StaffId") %></td>
                                                <td><%# Eval("StaffName") %></td>
                                                <td><%# Eval("DepartmentName") %></td>
                                                <td class="map-edit"><%# Eval("WorkDay") %></td>
                                                <td class="td-parttime map-edit"><%# Eval("WorkHour") %></td>
                                                <td>
                                                    <button type="button" style="width: 70%; margin: 0 auto; background-color: #ffe400; border: 1px solid #c1c1c1; color: #000; height: 30px;" data-toggle="modal" data-target="#myModal" onclick="GetStaffInfo($(this))" data-staffid="<%# Eval("StaffId") %>" data-staffname="<%# Eval("StaffName") %>" data-stafftype="<%# Eval("DepartmentName") %>">
                                                        Chi tiết
                                                    </button>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog" style="width: 90%">

                                <!-- Modal content-->
                                <div class="modal-content" style="border-radius: 0;">
                                    <div class="modal-header" style="position: fixed; top: 0; width: 100%; height: 80px;">
                                        <button type="button" class="close" style="font-size: 40px;" data-dismiss="modal">&times;</button>
                                        <p class="modal_text" id="TenNhanvien"></p>

                                        <p class="modal_text" id="Ngay"></p>
                                    </div>
                                    <div class="modal-body" id="body_modal" style="margin-top: 80px; width: 100%;">
                                        <div>
                                            <table id="Detail_Staff" class="tbl_DetailStaff" style="width: 100%;">
                                                <thead>
                                                    <tr style="background-color: #ddd;">
                                                        <th style="width: 8%; height: 55px;">STT</th>
                                                        <th style="width: 25%; height: 55px;">Ngày chấm công</th>
                                                        <th style="width: 25%; height: 55px;">Salon được chấm công</th>
                                                        <th style="width: 25%; height: 55px;">Ca làm việc</th>
                                                        <th style="width: 17%; height: 55px;">Giờ parttime</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="modal-footer" style="height: 65px; padding: 15px 20px 20px">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <%--<asp:UpdatePanel ID="UPExcel" runat="server" ClientIDMode="Static">
            <ContentTemplate></ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="BtnFakeExcel" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>--%>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <%--<asp:Button ID="BtnFakeExcel" ClientIDMode="Static" runat="server" OnClick="Exc_ExportExcel" style="display:none;" />--%>
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="IsPaging" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_ExcelType" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELPage" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELSegment" />
                <asp:HiddenField ID="HDF_StaffId" ClientIDMode="Static" runat="server" />
            </div>
            <%-- END Listing --%>
        </div>

        <!-- Notification box -->
        <div class="notification-box">Thành công</div>

        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 190px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <!--// Notification box -->

        <script type="text/javascript">
            jQuery(document).ready(function () {
                // Add active menu
                $("#glbTimeKeeping").addClass("active");
                $("#glbPartTime").addClass("active");
                $("#subMenu li.li-listing").addClass("active");

                // Price format
                UP_FormatPrice('.be-report-price');

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        //$input.val($input.val().split(' ')[0]);
                    }
                });

                // View data today
                $(".tag-date-today").click();

                // Bind Suggestion
                Bind_Suggestion();

                //============================
                // Show System Message
                //============================ 
                var qs = getQueryStrings();
                showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);

                /// init status for checkbox checkall
                initCheckall($("#inputCheckAll"));
            });

            function SubTableFixHeight() {
                $("tbody table.sub-table").each(function () {
                    $(this).height($(this).parent().height() + 1);
                });
            }
            function getIdStaffType() {
                var i = $("#StaffType").val();
                alert(i);
            }
            function viewDataByDate(This, time) {
                $(".tag-date.active").removeClass("active");
                This.addClass("active");
                $("#TxtDateTimeTo").val("");
                $("#TxtDateTimeFrom").val(time);
                $("#ViewData").click();
            }
            //$(function () {
            //    $('#tableEnroll button').click(function (e) {
            //        e.preventDefault();
            //        alert($(this).closest('tr').find('td:first').text());
            //    });
            //});
            function GetStaffInfo(This) {
                var s = $("#Salon").find("option:selected").text();
                var s1 = This.data("staffname");
                var s2 = This.data("stafftype");
                var FromDate = document.getElementById("TxtDateTimeFrom").value;
                var ToDate = document.getElementById("TxtDateTimeTo").value;
                var Staff_ID = This.data("staffid");
                document.getElementById("TenNhanvien").innerHTML = "Tên nhân viên  &nbsp;: &nbsp; <span>" + s1 + "</span >  &nbsp;  &nbsp; |&nbsp;  &nbsp;   Bộ phận  &nbsp; : &nbsp; <span>" + s2 + "</span>  &nbsp;  &nbsp; | &nbsp;  &nbsp;  Salon  &nbsp;:&nbsp; <span>" + s + "</span>";
                document.getElementById("Ngay").innerHTML = "Từ ngày : &nbsp; <span>" + FromDate + "</span>&nbsp; đến ngày :&nbsp; <span>" + ToDate + "</span>";
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/TimeKeeping/WorkDay_Report.aspx/BindTimekeepingStaff",
                    data: '{Staff_ID: ' + Staff_ID + ' , Fromdate: "' + FromDate + '" , Todate: "' + ToDate + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        console.log(response);
                        if (response.d.success) {
                            var data = response.d.data;
                            $('#Detail_Staff tbody').html('');
                            for (var i = 0; i < data.length; i++) {

                                var row = '<tr> <td>' + (i + 1) + '</td><td>' + doingay(data[i].WorkDate) + '</td><td>' + data[i].ShortName + '</td><td>' + data[i].w_Name + '</td><td>' + data[i].WorkHour + '</td></tr>';
                                $('#Detail_Staff tbody').append(row);
                            }
                        }
                        else {
                            $('#Detail_Staff tbody').html('');
                        }
                        var _maxheight = $(window).height() - 250;
                        var numb = (_maxheight / 45) - 2;
                        if (data.length > numb) {
                            $('#body_modal').css({
                                'height': _maxheight,
                                'overflow-y': 'scroll'
                            })
                        }
                        else {
                            $('#body_modal').css({
                                'overflow-y': 'hidden'
                            })
                        }

                    }
                })

            }
            //============================
            // Event delete
            //============================
            function doingay(value) {
                if (value === null) return "";

                var pattern = /Date\(([^)]+)\)/;
                var results = pattern.exec(value);
                var dt = new Date(parseFloat(results[1]));
                var day = (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();
                return day.toString();
            }
            function del(This, code, name) {
                var code = code || null,
                    name = name || null,
                    Row = This;
                if (!code) return false;

                // show EBPopup
                $(".confirm-yn").openEBPopup();
                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

                $("#EBPopup .yn-yes").bind("click", function () {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/SystemService/Ajax/Del.aspx/Delele_Bill",
                        data: '{Code : "' + code + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            var mission = JSON.parse(response.d);
                            if (mission.success) {
                                delSuccess();
                                Row.remove();
                            } else {
                                delFailed();
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    });
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });

                // Enter don't submit
                $(window).on('keyup keypress', function (e) {
                    var keyCode = e.keyCode || e.which;
                    if (keyCode === 13) {
                        e.preventDefault();
                        return false;
                    }
                });
            }

            //============================
            // Suggestion Functions
            //============================
            function Bind_Suggestion() {
                $(".eb-suggestion").bind("keyup", function (e) {
                    if (e.keyCode == 40) {
                        UpDownListSuggest($(this));
                    } else {
                        Call_Suggestion($(this));
                    }
                });
                $(".eb-suggestion").bind("focus", function () {
                    Call_Suggestion($(this));
                });

                $(window).bind("click", function (e) {
                    if ((!e.target.className.match("eb-select") && e.target.parentElement.className.match("ul-listing-suggestion")) ||
                        (!e.target.className.match("eb-select") && !e.target.parentElement.className.match("ul-listing-suggestion"))) {
                        EBSelect_HideBox();
                    }
                });
            }

            function UpDownListSuggest(This) {
                var UlSgt = This.parent().find(".ul-listing-suggestion"),
                    index = 0,
                    LisLen = UlSgt.find(">li").length - 1,
                    Value;

                This.blur();
                UlSgt.find(">li.active").removeClass("active");
                UlSgt.find(">li:eq(" + index + ")").addClass("active");

                $(window).unbind("keydown").bind("keydown", function (e) {
                    if (e.keyCode == 40) {
                        if (index == LisLen) return false;
                        UlSgt.find(">li.active").removeClass("active");
                        UlSgt.find(">li:eq(" + (++index) + ")").addClass("active");
                        return false;
                    } else if (e.keyCode == 38) {
                        if (index == 0) return false;
                        UlSgt.find(">li.active").removeClass("active");
                        UlSgt.find(">li:eq(" + (--index) + ")").addClass("active");
                        return false;
                    } else if (e.keyCode == 13) {
                        // Bind data to HDF Field
                        var THIS = UlSgt.find(">li.active");
                        //var Value = THIS.text().trim();
                        var Value = THIS.attr("data-code");
                        var dataField = This.attr("data-field");
                        var HDF_StoreValue;

                        switch (dataField) {
                            case 'staff.name': HDF_StoreValue = "#HDF_StaffId"; break;
                        }
                        BindIdToHDF(THIS, Value, dataField, HDF_StoreValue, This);
                        EBSelect_HideBox();
                    }
                });
            }

            function Call_Suggestion(This) {
                var text = This.val(),
                    field = This.attr("data-field");
                Suggestion(This, text, field);
            }

            function Suggestion(This, text, field) {
                var This = This;
                var text = text || "";
                var field = field || "";
                var InputDomId;
                var staffType;

                switch (field) {
                    case "staff.name": InputDomId = "#StaffName"; staffType = 1; HDF_StoreValue = "#HDF_StaffId"; break;
                }
                if (text == "") {
                    $(HDF_StoreValue).val("");
                    return false;
                }

                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Ajax/Suggestion.aspx/Suggestion_Staff_ByName",
                    data: '{field : "' + field + '", name : "' + text + '", staffType : ' + staffType + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            var OBJ = JSON.parse(mission.msg);
                            if (OBJ.length > 0) {
                                var lis = "";
                                $.each(OBJ, function (i, v) {
                                    lis += "<li data-code='" + v.Id + "'" +
                                        "onclick=\"BindIdToHDF($(this),'" + v.Id + "','" + field + "','" + HDF_StoreValue +
                                        "','" + InputDomId + "')\">" +
                                        v.Value +
                                        "</li>";
                                });
                                This.parent().find("ul.ul-listing-suggestion").empty().append(lis).parent().show();
                                This.parent().find(".eb-select-data").show();
                            } else {
                                This.parent().find(".eb-select-data").hide().find("ul.ul-listing-suggestion").empty();
                            }
                        } else {
                            This.parent().find("ul.ul-listing-suggestion").empty();
                            var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                            showMsgSystem(msg, "warning");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function BindIdToHDF(THIS, Code, Field, HDF_StoreValue, Input_DomId) {
                var text = THIS.text().trim();
                $(HDF_StoreValue).val(Code);
                $(Input_DomId).val(text);
                $(Input_DomId).parent().find(".eb-select-data").hide();

                // Auto post server
                $("#BtnFakeUP").click();
            }

            function EBSelect_HideBox() {
                $(".eb-select-data").hide();
                $("ul.ul-listing-suggestion li.active").removeClass("active");
            }

            function showMsgSystem(msg, status) {
                $("#MsgSystem").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);
                setTimeout(function () {
                    $("#MsgSystem").fadeTo("slow", 0, function () {
                        $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 1);
                    });
                }, 5000);
            }
            //============================//
            //Get Staff Name
            function getStaffName() {
                var bp = $('StaffType').val();
            }
            //==============
            // Check enroll
            //==============
            function checkEnroll(This, staffId, staffName, salonId) {
                var workDate = $("#TxtDateTimeFrom").val();
                var isEnroll = This.parent().find("input.input-enroll").is(":checked");
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/TimeKeeping/WorkDay_Listing.aspx/AddEnroll",
                    data: '{staffId : "' + staffId + '", salonId : "' + salonId + '", workDate : "' + workDate + '", isEnroll : "' + isEnroll + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            //alert("Đã điểm danh cho [ " + staffName + " ]");
                            if (isEnroll)
                                showNotification("Đã điểm danh cho [ " + staffName + " ]");
                            else
                                showNotification("Đã hủy điểm danh cho [ " + staffName + " ]");
                            /// init status for checkbox checkall
                            initCheckall($("#inputCheckAll"));
                        } else {
                            alert("Có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển.");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }
            // Check part-time
            function checkPartTime(This, staffId, staffName, salonId) {
                var workDate = $("#TxtDateTimeFrom").val();
                var workHour = This.parent().find("input.input-parttime").val();
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/TimeKeeping/WorkDay_Listing.aspx/AddPartTime",
                    data: '{staffId : "' + staffId + '", salonId : "' + salonId + '", workDate : "' + workDate + '", workHour : "' + workHour + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            showNotification("Đã nhập part-time cho [ " + staffName + " ]");
                        } else {
                            alert("Có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển.");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }
            // Check enroll list
            function checkEnrollList(listDataJSON, status) {
                var workDate = $("#TxtDateTimeFrom").val();
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/TimeKeeping/WorkDay_Listing.aspx/AddEnrollList",
                    data: "{listData : '" + listDataJSON + "',workDate : '" + workDate + "', isEnroll : '" + status + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            if (status == true)
                                showNotification("Đã điểm danh cho toàn bộ nhân viên trong danh sách.");
                            else
                                showNotification("Đã hủy điểm danh cho toàn bộ nhân viên trong danh sách.");
                        } else {
                            alert("Có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển.");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }
            ///load Staff Name 


            /// show notification
            function showNotification(content) {
                $(".notification-box").text(content).fadeIn(function () {
                    setTimeout(function () {
                        $(".notification-box").fadeOut();
                    }, 2000);
                });
            }

            /// check all staff
            function checkAll(This) {
                var tableEnroll = $("#tableEnroll");
                var inputEnroll = $("#tableEnroll input.input-enroll");
                var listData = [];
                var status;
                inputEnroll.each(function () {
                    var id = parseInt($(this).attr("data-id"));
                    var salonId = parseInt($(this).attr("data-salonid"));
                    if (id != NaN && salonId != NaN) {
                        var obj = { id: id, salonid: salonId };
                        listData.push(obj);
                    }
                });
                if (This.is(":checked")) {
                    inputEnroll.prop("checked", true);
                    status = true;
                } else {
                    inputEnroll.prop("checked", false);
                    status = false;
                }
                // update to db
                checkEnrollList(JSON.stringify(listData), status);
            }

            /// init status for checkbox checkall
            function initCheckall(This) {
                var num = 0;
                var tr = $("#tableEnroll tbody > tr").length;
                $("#tableEnroll input.input-enroll").each(function () {
                    if ($(this).is(":checked")) {
                        num++;
                    }
                });
                if (num == tr) {
                    This.prop("checked", true);
                }
                else {
                    This.prop("checked", false);
                }
            }

        </script>
        <style>
            .modal-header span {
                font-size: 16px;
                line-height: 25px;
                font-weight: bold;
            }

            .tbl_DetailStaff th, .tbl_DetailStaff tbody, .tbl_DetailStaff tr, .tbl_DetailStaff td {
                height: 45px;
                text-align: center;
                border: 1px solid #bababa;
                font-size: 14px;
                line-height: 25px;
            }

            .modal_text {
                font-size: 16px;
                line-height: 25px;
            }
        </style>
    </asp:Panel>
</asp:Content>
