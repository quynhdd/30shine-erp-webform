﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WorkDay_Listing.aspx.cs" Inherits="_30shine.GUI.BackEnd.TimeKeeping.WorkDay_Listing" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">

    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý ngày công &nbsp;&#187; </li>
                        <li class="li-add"><a href="/admin/cong-nhat/diem-danh.html">Chấm công</a></li>
                        <li class="li-listing"><a href="/admin/thong-ke-cham-cong.html">Thống kê</a></li>
                        <li class="li-listing"><a href="/admin/cong-nhat/tong-cong.html">Tổng công</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report woktime-listing">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row">
                    <div class="filter-item" style="margin-left: 0;">
                        <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Ngày chấm công</strong>
                        <div class="datepicker-wp">
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeFrom" placeholder="Chọn ngày" Style="width: 150px!important;"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                            <strong class="st-head" style="margin-left: 10px; margin-right: 10px; display: none;">
                                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                            </strong>
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeTo" placeholder="Đến ngày" Style="width: 150px!important; display: none;"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="Salon" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 150px;" AutoPostBack="true" OnSelectedIndexChanged="Salon_SelectedIndexChanged"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="StaffType" runat="server" ClientIDMode="static" Style="margin: 12px 0; width: 150px;"></asp:DropDownList>
                    </div>
                    <div class="filter-item" style="display: none;">
                        <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion" data-field="staff.name" data-value="0"
                            AutoCompleteType="Disabled" ID="StaffName" ClientIDMode="Static" placeholder="Nhập tên Stylist/Skinner" runat="server"></asp:TextBox>
                        <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                            <ul class="ul-listing-staff ul-listing-suggestion" id="UlCustomerName">
                            </ul>
                        </div>
                    </div>

                    <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" Text="Xem dữ liệu"
                        ClientIDMode="Static" onclick="excPaging(1)" runat="server">
                        Xem danh sách
                    </asp:Panel>
                    <a href="/admin/cong-nhat/diem-danh.html" class="st-head btn-viewdata">Reset Filter</a>
                    <div class="export-wp drop-down-list-wp" style="display: none;">
                        <asp:Panel ID="fff" CssClass="st-head btn-viewdata"
                            ClientIDMode="Static" runat="server" onclick="EntryExportExcel('all')">
                            Xuất File Excel
                        </asp:Panel>
                        <%--OnClick="Exc_ExportExcel" --%>
                        <ul class="ul-drop-down-list" style="display: none;">
                            <li onclick="EntryExportExcel('all')">Tất cả dữ liệu</li>
                            <li onclick="EntryExportExcel('current page')">Trang hiện tại</li>
                        </ul>
                    </div>
                </div>
                <br />
                <div class="row" style="margin-top: -15px;" runat="server" id="PanelEnrollCrossSalon" clientidmode="static" visible="false">
                    <div class="filter-item" style="margin-left: 0;">
                        <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-user-plus" aria-hidden="true" style="margin-right: -3px;"></i>
                            Thêm nhân viên</strong>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="Salonkhac" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 150px;" AutoPostBack="true" OnSelectedIndexChanged="ListStaff_SelectedIndexChanged"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="StaffType2" runat="server" ClientIDMode="static" Style="margin: 12px 0; width: 150px;" AutoPostBack="true" OnSelectedIndexChanged="ListStaff_SelectedIndexChanged"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="Staff" runat="server" ClientIDMode="static" Style="margin: 12px 0; width: 150px;"></asp:DropDownList>
                    </div>
                    <div class="st-head btn-viewdata" onclick="AddcheckEnrollOutSalon()">Thêm nhân viên</div>
                </div>

                <!-- End Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>Danh sách chấm công</strong>
                </div>

                <%--      <asp:UpdatePanel ID="UpdatePanel1" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="row">
                            <div class="col-md-6 col-xs-12 left">                              
                                <table class=" table table-bordered text-center" >
                                    <thead>
                                        <tr>
                                            <%if (ngay1 != string.Format("{0:dd/MM/yyyy}", DateTime.Now))
                                                { %>
                                            <td>Bộ phận</td>
                                            <td colspan="2"><%=ngay1 %></td>
                                            <td colspan="2"><%=ngay2 %></td>
                                            <td colspan="2"><%=ngay3 %></td>
                                            <%} %>
                                            <%else
                                                { %>
                                            <td>Bộ phận</td>
                                            <td colspan="2">Hôm nay (<%=ngay1 %>)</td>
                                            <td colspan="2">Ngày mai (<%=ngay2 %>)</td>
                                            <td colspan="2">Ngày kia (<%=ngay3 %>)</td>

                                            <%} %>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>Stylist</td>
                                            <td>Skinner</td>
                                            <td>Stylist</td>
                                            <td>Skinner</td>
                                            <td>Stylist</td>
                                            <td>Skinner</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%if (OBJ.Count > 0)
                                            { %>
                                         <%foreach (var v in OBJ)
                                             { %>
                                        <tr>                                            
                                            <td><%= v.ShortName %></td>
                                            <td><%= v.todayStylist %></td>
                                            <td><%= v.todaySKinner %></td>
                                            <td><%= v.TomorrowStylist %></td>
                                            <td><%= v.TomorrowSkinner %></td>
                                            <td><%= v.AfterTomorrowStylist %></td>
                                            <td><%= v.AfterTomorrowSkinner %></td>
                                        </tr>
                                        <%} %>
                                        <%} %>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>--%>

                <!-- Row Table Filter -->
                <div class="table-func-panel" style="margin-top: -33px;">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="200">200</li>
                                <%--<li data-value="1000000">Tất cả</li>--%>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>

                </div>
                <div class="wp_worktime">
                    <div class="div_tinhcong">
                        <a href="javascript://" id="lnkCa1" class="st-head btn-viewdata btn-workhour" onclick="loadDataByWorkTimeId(1, $(this))">Ca 1 [8h-17h]</a>
                        <a href="javascript://" id="lnkCa11" class="st-head btn-viewdata btn-workhour" onclick="loadDataByWorkTimeId(5, $(this))">Ca 1.1 [8h-14h30]</a>
                        <a href="javascript://" id="lnkCa2" class="st-head btn-viewdata btn-workhour" onclick="loadDataByWorkTimeId(2, $(this))">Ca 2 [10h-19h]</a>
                        <a href="javascript://" id="lnkCa25" class="st-head btn-viewdata btn-workhour" onclick="loadDataByWorkTimeId(4, $(this))" style="display: none;">Ca 2.5 [11h-20h]</a>
                        <a href="javascript://" id="lnkCa3" class="st-head btn-viewdata btn-workhour" onclick="loadDataByWorkTimeId(3, $(this))">Ca 3 [12h30-21h30]</a>
                        <a href="javascript://" id="lnkCa31" class="st-head btn-viewdata btn-workhour" onclick="loadDataByWorkTimeId(6, $(this))">Ca 3.1 [15h-21h30]</a>
                    </div>
                    <div class="div_ca">
                        <p class="boxca bg_ca1">
                        </p>
                        <p class="p_ca">Ca 1</p>
                        <p class="boxca bg_ca1" style="background: #716c5f;">
                        </p>
                        <p class="p_ca">Ca 1.1</p>
                        <p class="boxca bg_ca2" style="background: #5cb85c;">
                        </p>
                        <p class="p_ca">Ca 2</p>
                        <p class="boxca bg_ca3" style="background: #fcd344;">
                        </p>
                        <p class="p_ca" style="display: none;">Ca 2.5</p>
                        <p class="boxca bg_ca3" style="background: #fcd344; display: none;">
                        </p>
                        <p class="p_ca">Ca 3</p>
                        <p class="boxca bg_ca3" style="background: #bc950b;">
                        </p>
                        <p class="p_ca">Ca 3.1</p>
                    </div>

                </div>
                <!-- End Row Table Filter -->
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <%--thong ke--%>
                        <div class="row table-wp">
                            <table class="table-add table-listing report-sales-listing table-enroll-listing" id="tableEnroll">
                                <thead>
                                    <tr style="background: <%#Eval("Color")%>">
                                        <th style="width: 80px!important;">STT</th>
                                        <th style="width: 100px!important">Ngày</th>
                                        <th style="width: 80px!important;">ID</th>
                                        <th>Nhân viên</th>
                                        <%--  <th style="width:510px;">Khung giờ làm</th>--%>
                                        <th class="th-enroll">
                                            <input type="checkbox" id="inputCheckAll" onclick="checkAll($(this))" style="position: relative; top: 2px; margin-right: 1px; cursor: pointer; display: none;" />
                                            Chấm công
                                        </th>
                                        <th class="th-parttime">Giờ làm part-time</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="Rpt_PartTime" runat="server" OnItemDataBound="Rpt_PartTime_ItemDataBound">
                                        <ItemTemplate>
                                            <tr style="background: <%#Eval("Color")%>">
                                                <%--style="background: <%# Eval("Color")%>;"--%>
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td><%# String.Format("{0:dd/MM/yyyy}", Eval("WorkDate")) %></td>
                                                <td><%# Eval("Id") %></td>
                                                <td><%# Eval("Fullname") %>  &nbsp; <%#Eval("NoteStatus") != null ? Eval("NoteStatus") : Eval("NoteStatusSalonCurrent") == null ? "" : Eval("NoteStatusSalonCurrent") %> </td>
                                                <%--<td style=" "><%# Eval("showHourFrame") %></td>--%>
                                                <td class="map-edit" <%--onclick="$(this).find('input').click();"--%>>
                                                    <%if (checkLimitTime() == true)
                                                        { %>
                                                    <input type="checkbox" class="input-enroll" data-id="<%# Eval("Id") %>" data-salonid="<%# Eval("SalonId") %>" <%# Convert.ToInt32(Eval("IsEnroll")) == 1 ? "Checked=\"True\"" : "" %>
                                                        onclick="checkEnroll($(this),<%# Eval("Id")%>, '<%# Eval("Fullname")%>    ','<%# Eval("SalonName")%> ',<%# Eval("SalonId") %>, <%# Eval("WorkTimeId") != null ? Eval("WorkTimeId") : 0  %>,<%# Eval("SalonIdWorking") %> );" />
                                                    <div class="edit-wp">
                                                        <a class="elm edit-btn" href="javascript://" data-datetime="<%# String.Format("{0:dd/MM/yyyy}", Eval("WorkDate")) %>" title="Sửa giờ làm" onclick="editHourFrame($(this), <%# Eval("Id") %>,  <%# Eval("SalonId") %>,'<%# Eval("Fullname")%>')  ;"></a>
                                                    </div>
                                                    <%} %>
                                                    <%else
                                                        { %>
                                                    <input type="checkbox" class="input-enroll" data-id="<%# Eval("Id") %>" data-salonid="<%# Eval("SalonId") %>" <%# Convert.ToInt32(Eval("IsEnroll")) == 1 ? "Checked=\"True\"" : "" %>
                                                        onclick="checkEnroll($(this),<%# Eval("Id")%>, '<%# Eval("Fullname")%>    ','<%# Eval("SalonName")%> ',<%# Eval("SalonId") %>, <%# Eval("WorkTimeId") != null ? Eval("WorkTimeId") : 0  %> ,<%# Eval("SalonIdWorking") %> );" disabled />
                                                    <div style="display: none !important;" class="edit-wp">
                                                        <a class="elm edit-btn" href="javascript://" data-datetime="<%# String.Format("{0:dd/MM/yyyy}", Eval("WorkDate")) %>" title="Sửa giờ làm" onclick="editHourFrame($(this), <%# Eval("Id") %>,  <%# Eval("SalonId") %>,'<%# Eval("Fullname")%>')  ;"></a>
                                                    </div>
                                                    <%} %>


                                                    <div class="wp_lstbookhour">
                                                        <div class="woktime-listing">
                                                            <div class="ctn_lstbookhour">
                                                                <div class="close_bookHourId_box">X Đóng</div>
                                                                <div class="div_lst_bookhour">
                                                                    <asp:Label ID="lblSalonId" runat="server" Text='<%# Eval("SalonId") %>' Visible="false">  </asp:Label>
                                                                    <%-- <asp:Label ID="lblWorkDate" runat="server" Text='<%#Eval("WorkDate") %>>' Visible="false">  </asp:Label>--%>
                                                                    <asp:Label ID="lblWorkDate" runat="server" Text='<%# Eval("WorkDate") %>' Visible="false">  </asp:Label>
                                                                    <asp:Label ID="lblstaffId" runat="server" Text='<%# Eval("Id") %>' Visible="false">  </asp:Label>
                                                                    <asp:Label ID="lblWorkTimeId" runat="server" Text='<%# Eval("WorkTimeId") %>' Visible="false">  </asp:Label>
                                                                    <asp:Repeater ID="rptBookHour" runat="server">
                                                                        <ItemTemplate>
                                                                            <a class='it_bookhour <%#Eval("Active") %> <%#Eval("PendingHour") %>' data-hourid="<%#Eval("Id") %>" data-hourframe="<%#Eval("HourFrame") %>" onclick="wchooseHour($(this))">
                                                                                <%#Eval("HourName") %>
                                                                            </a>

                                                                        </ItemTemplate>
                                                                    </asp:Repeater>
                                                                    <div class="wp_btn_hourId">
                                                                        <a class="btn_hourId" onclick="EditFTK($(this), <%# Eval("Id") %>, '<%# Eval("Fullname") %> ', <%# Eval("SalonId") %>);">Hoàn tất</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="td-parttime map-edit">
                                                    <%#Eval("WorkHour").ToString() != "0" ? Eval("WorkHour") : Eval("Parttime").ToString() == "0" ? "0" : Eval("Parttime") %>
                                                    <%--<input class="input-parttime" type="number" min="0" max="100" step="1" value="<%# Eval("WorkHour") %>"
                                                        onchange="checkPartTime($(this), <%# Eval("Id") %>, '<%# Eval("Fullname") %>', <%# Eval("SalonId") %>)" />--%>
                                                </td>
                                                <%--<td><%# Eval("salonName") %></td>--%>
                                                <%--<td><%# Eval("Note") %></td>--%>
                                                <%--<td class="map-edit" style="padding-right: 27px; padding-left: 5px;">
                                            <div class="be-report-price" style="padding-left: 7px;"><%# Eval("totalPoint") %></div>
                                            <div class="edit-wp">
                                                <asp:Panel CssClass="edit-action-wp action-edit" runat="server" ID="EAedit" Visible="false">
                                                    <a class="elm edit-btn" href="/dich-vu/<%# Eval("Id") %>.html" title="Sửa" target="_blank"></a>
                                                </asp:Panel>
                                                <asp:Panel CssClass="edit-action-wp action-delete" runat="server" ID="EAdelete" Visible="false">
                                                    <a class="elm del-btn" onclick="del(this.parentNode.parentNode.parentNode.parentNode,'<%# Eval("Id") %>', '<%# Eval("Id") %>')" href="javascript://" title="Xóa"></a></asp:Panel>
                                            </div>
                                        </td>--%>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <%--<asp:UpdatePanel ID="UPExcel" runat="server" ClientIDMode="Static">
            <ContentTemplate></ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="BtnFakeExcel" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>--%>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <%--<asp:Button ID="BtnFakeExcel" ClientIDMode="Static" runat="server" OnClick="Exc_ExportExcel" style="display:none;" />--%>
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="IsPaging" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_ExcelType" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELPage" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELSegment" />
                <asp:HiddenField ID="HDF_StaffId" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_WorkTimeId" ClientIDMode="Static" runat="server" />
            </div>
            <%-- END Listing --%>
        </div>

        <!-- Notification box -->
        <div class="notification-box">Thành công</div>
        <!--// Notification box -->
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 190px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                // Add active menu
                $("#glbTimeKeeping").addClass("active");
                $("#glbPartTime").addClass("active");
                $("#subMenu li.li-add").addClass("active");

                // Price format
                UP_FormatPrice('.be-report-price');

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        //$input.val($input.val().split(' ')[0]);
                    }
                });

                // View data today
                $(".tag-date-today").click();

                // Bind Suggestion
                Bind_Suggestion();

                //============================
                // Show System Message
                //============================ 
                var qs = getQueryStrings();
                showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);

                /// init status for checkbox checkall
                initCheckall($("#inputCheckAll"));
            });

            function SubTableFixHeight() {
                $("tbody table.sub-table").each(function () {
                    $(this).height($(this).parent().height() + 1);
                });
            }

            function viewDataByDate(This, time) {
                $(".tag-date.active").removeClass("active");
                This.addClass("active");
                $("#TxtDateTimeTo").val("");
                $("#TxtDateTimeFrom").val(time);
                $("#ViewData").click();
            }

            //============================
            // Event delete
            //============================
            function del(This, code, name) {
                var code = code || null,
                    name = name || null,
                    Row = This;
                if (!code) return false;

                // show EBPopup
                $(".confirm-yn").openEBPopup();
                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

                $("#EBPopup .yn-yes").bind("click", function () {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/SystemService/Ajax/Del.aspx/Delele_Bill",
                        data: '{Code : "' + code + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            var mission = JSON.parse(response.d);
                            if (mission.success) {
                                delSuccess();
                                Row.remove();
                            } else {
                                delFailed();
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    });
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });

                // Enter don't submit
                $(window).on('keyup keypress', function (e) {
                    var keyCode = e.keyCode || e.which;
                    if (keyCode === 13) {
                        e.preventDefault();
                        return false;
                    }
                });
            }

            //============================
            // Suggestion Functions
            //============================
            function Bind_Suggestion() {
                $(".eb-suggestion").bind("keyup", function (e) {
                    if (e.keyCode == 40) {
                        UpDownListSuggest($(this));
                    } else {
                        Call_Suggestion($(this));
                    }
                });
                $(".eb-suggestion").bind("focus", function () {
                    Call_Suggestion($(this));
                });

                $(window).bind("click", function (e) {
                    if ((!e.target.className.match("eb-select") && e.target.parentElement.className.match("ul-listing-suggestion")) ||
                        (!e.target.className.match("eb-select") && !e.target.parentElement.className.match("ul-listing-suggestion"))) {
                        EBSelect_HideBox();
                    }
                });
            }

            function UpDownListSuggest(This) {
                var UlSgt = This.parent().find(".ul-listing-suggestion"),
                    index = 0,
                    LisLen = UlSgt.find(">li").length - 1,
                    Value;

                This.blur();
                UlSgt.find(">li.active").removeClass("active");
                UlSgt.find(">li:eq(" + index + ")").addClass("active");

                $(window).unbind("keydown").bind("keydown", function (e) {
                    if (e.keyCode == 40) {
                        if (index == LisLen) return false;
                        UlSgt.find(">li.active").removeClass("active");
                        UlSgt.find(">li:eq(" + (++index) + ")").addClass("active");
                        return false;
                    } else if (e.keyCode == 38) {
                        if (index == 0) return false;
                        UlSgt.find(">li.active").removeClass("active");
                        UlSgt.find(">li:eq(" + (--index) + ")").addClass("active");
                        return false;
                    } else if (e.keyCode == 13) {
                        // Bind data to HDF Field
                        var THIS = UlSgt.find(">li.active");
                        //var Value = THIS.text().trim();
                        var Value = THIS.attr("data-code");
                        var dataField = This.attr("data-field");
                        var HDF_StoreValue;

                        switch (dataField) {
                            case 'staff.name': HDF_StoreValue = "#HDF_StaffId"; break;
                        }
                        BindIdToHDF(THIS, Value, dataField, HDF_StoreValue, This);
                        EBSelect_HideBox();
                    }
                });
            }

            function Call_Suggestion(This) {
                var text = This.val(),
                    field = This.attr("data-field");
                Suggestion(This, text, field);
            }

            function Suggestion(This, text, field) {
                var This = This;
                var text = text || "";
                var field = field || "";
                var InputDomId;
                var staffType;

                switch (field) {
                    case "staff.name": InputDomId = "#StaffName"; staffType = 1; HDF_StoreValue = "#HDF_StaffId"; break;
                }
                if (text == "") {
                    $(HDF_StoreValue).val("");
                    return false;
                }

                $.ajax({
                    type: "POST",
                    url: "/GUI/SystemService/Ajax/Suggestion.aspx/Suggestion_Staff_ByName",
                    data: '{field : "' + field + '", name : "' + text + '", staffType : ' + staffType + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            var OBJ = JSON.parse(mission.msg);
                            if (OBJ.length > 0) {
                                var lis = "";
                                $.each(OBJ, function (i, v) {
                                    lis += "<li data-code='" + v.Id + "'" +
                                        "onclick=\"BindIdToHDF($(this),'" + v.Id + "','" + field + "','" + HDF_StoreValue +
                                        "','" + InputDomId + "')\">" +
                                        v.Value +
                                        "</li>";
                                });
                                This.parent().find("ul.ul-listing-suggestion").empty().append(lis).parent().show();
                                This.parent().find(".eb-select-data").show();
                            } else {
                                This.parent().find(".eb-select-data").hide().find("ul.ul-listing-suggestion").empty();
                            }
                        } else {
                            This.parent().find("ul.ul-listing-suggestion").empty();
                            var msg = "Không tìm thấy mã khách hàng trong hệ thống.";
                            showMsgSystem(msg, "warning");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            function BindIdToHDF(THIS, Code, Field, HDF_StoreValue, Input_DomId) {
                var text = THIS.text().trim();
                $(HDF_StoreValue).val(Code);
                $(Input_DomId).val(text);
                $(Input_DomId).parent().find(".eb-select-data").hide();

                // Auto post server
                $("#BtnFakeUP").click();
            }

            function EBSelect_HideBox() {
                $(".eb-select-data").hide();
                $("ul.ul-listing-suggestion li.active").removeClass("active");
            }

            function showMsgSystem(msg, status) {
                $("#MsgSystem").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);
                setTimeout(function () {
                    $("#MsgSystem").fadeTo("slow", 0, function () {
                        $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 1);
                    });
                }, 5000);
            }
            //============================//

            //==============
            // Check enroll : Xử lý ghi nhận chấm công hay hủy chấm công
            //==============
            function checkEnroll(This, staffId, staffName, salonName, salonId, workTimeId, salonIdWorking) {
                var workDate = $("#TxtDateTimeFrom").val()
                var isEnroll = This.parent().find("input.input-enroll").is(":checked");
                var workTime = parseInt($("#HDF_WorkTimeId").val());
                var ddlSalon = $("#Salon").val();
                workTimeId = !isNaN(workTime) ? workTime : workTimeId;
                var error = 0;
                if (salonId != salonIdWorking && salonIdWorking > 0) {
                    var noti = confirm('' + staffName.trim() + ' đang làm việc tại salon: ' + salonName.trim() + ' bạn có muốn tiếp tục?');
                    if (noti == true) {
                        if (isNaN(parseInt($("#HDF_WorkTimeId").val())) && isEnroll == true) {
                            This.parent().find("input.input-enroll").attr('checked', false)
                            alert("Bạn vui lòng chọn ca.");
                            error++;
                        }
                        // set giá trị biến global
                        paramsObj.salonID = salonId;
                        paramsObj.staffID = staffId;
                        paramsObj.workDate = workDate;
                        paramsObj.workTimeID = workTimeId;
                        paramsObj.staffName = staffName;
                        paramsObj.isEnroll = isEnroll;
                        // post API add enroll
                        if (error == 0) {
                            //updateSalonSlotByEnroll();
                            postAPIenroll();
                        }
                        $("#ViewData").click();
                    }
                    else {
                        This.parent().find("input.input-enroll").attr('checked', false)
                    }
                }
                else {
                    if (isNaN(parseInt($("#HDF_WorkTimeId").val())) && isEnroll == true) {
                        This.parent().find("input.input-enroll").attr('checked', false)
                        alert("Bạn vui lòng chọn ca.");
                        error++;
                    }
                    // set giá trị biến global
                    paramsObj.salonID = salonId;
                    paramsObj.staffID = staffId;
                    paramsObj.workDate = workDate;
                    paramsObj.workTimeID = workTimeId;
                    paramsObj.staffName = staffName;
                    paramsObj.isEnroll = isEnroll;
                    // post API add enroll
                    if (error == 0) {
                        //updateSalonSlotByEnroll();
                        postAPIenroll();
                    }
                    $("#ViewData").click();
                }
            }

            // Them Stylist ngoai Salon
            //==============
            function AddcheckEnrollOutSalon() {
                var staffName = $("#Staff option:selected").text();
                var staffId = $("#Staff").val();
                var salonId = $("#Salon").val();
                var workDate = $("#TxtDateTimeFrom").val();
                var isEnroll = true;
                var workTimeId = parseInt($("#HDF_WorkTimeId").val());
                //workTimeId = !isNaN(workTime) ? workTime : workTimeId;
                var error = 0;
                if (isNaN(parseInt($("#HDF_WorkTimeId").val()))) {
                    alert("Bạn vui lòng chọn ca.");
                    error++;
                }
                if (error == 0) {
                    startLoading();
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/TimeKeeping/WorkDay_Listing.aspx/AddEnroll",
                        data: '{staffId : "' + staffId + '", salonId : "' + salonId + '", workDate : "' + workDate + '", isEnroll : "' + isEnroll + '", workTimeId : "' + workTimeId + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            var mission = JSON.parse(response.d);
                            if (mission.success) {
                                //alert("Đã điểm danh cho [ " + staffName + " ]");
                                if (isEnroll) {
                                    showNotification("Đã điểm danh cho [ " + staffName + " ]");
                                    // update panel
                                    $(".wp_worktime .btn-workhour.active").click();
                                }
                                //else{
                                //    showNotification("Đã hủy điểm danh cho [ " + staffName + " ]");
                                //    $(".wp_worktime .btn-workhour.active").click();

                                //}
                                /// init status for checkbox checkall
                                //initCheckall($("#inputCheckAll"));
                            } else {
                                alert("Hệ thống chặn chuyển chấm công do quá thời gian quy định!");
                            }
                            finishLoading();
                        },
                        failure: function (response) { alert(response.d); }
                    });
                }
            }

            // Check part-time
            function checkPartTime(This, staffId, staffName, salonId) {
                var workDate = $("#TxtDateTimeFrom").val();
                var workHour = This.parent().find("input.input-parttime").val();
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/TimeKeeping/WorkDay_Listing.aspx/AddPartTime",
                    data: '{staffId : "' + staffId + '", salonId : "' + salonId + '", workDate : "' + workDate + '", workHour : "' + workHour + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            showNotification("Đã nhập part-time cho [ " + staffName + " ]");
                        } else {
                            alert("Có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển.");
                        }
                        finishLoading();
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            // Check enroll list
            function checkEnrollList(listDataJSON, status) {
                var workDate = $("#TxtDateTimeFrom").val();
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/TimeKeeping/WorkDay_Listing.aspx/AddEnrollList",
                    data: "{listData : '" + listDataJSON + "',workDate : '" + workDate + "', isEnroll : '" + status + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            if (status == true)
                                showNotification("Đã điểm danh cho toàn bộ nhân viên trong danh sách.");
                            else
                                showNotification("Đã hủy điểm danh cho toàn bộ nhân viên trong danh sách.");
                        } else {
                            alert("Có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển.");
                        }
                        finishLoading();
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            /// show notification
            function showNotification(content) {
                $(".notification-box").text(content).fadeIn(function () {
                    setTimeout(function () {
                        $(".notification-box").fadeOut();
                    }, 5000);
                });
            }

            /// check all staff
            function checkAll(This) {
                var tableEnroll = $("#tableEnroll");
                var inputEnroll = $("#tableEnroll input.input-enroll");
                var listData = [];
                var status;
                inputEnroll.each(function () {
                    var id = parseInt($(this).attr("data-id"));
                    var salonId = parseInt($(this).attr("data-salonid"));
                    if (id != NaN && salonId != NaN) {
                        var obj = { id: id, salonid: salonId };
                        listData.push(obj);
                    }
                });
                if (This.is(":checked")) {
                    inputEnroll.prop("checked", true);
                    status = true;
                } else {
                    inputEnroll.prop("checked", false);
                    status = false;
                }
                // update to db
                checkEnrollList(JSON.stringify(listData), status);
            }

            /// init status for checkbox checkall
            function initCheckall(This) {
                var num = 0;
                var tr = $("#tableEnroll tbody > tr").length;
                $("#tableEnroll input.input-enroll").each(function () {
                    if ($(this).is(":checked")) {
                        num++;
                    }
                });
                if (num == tr) {
                    This.prop("checked", true);
                }
                else {
                    This.prop("checked", false);
                }
            }

            function loadDataByWorkTimeId(id, This) {
                This.parent().find(".btn-workhour.active").removeClass("active");
                This.addClass("active");
                $("#HDF_WorkTimeId").val(id);
                // load data
                $("#BtnFakeUP").click();
            };
            //Bind Id FlowTimeKeeping
            function editHourFrame(This, staffId, salonId, NameStylist) {
                //alert(NameStylist);
                if (This.parent().parent().find($('input.input-enroll')).is(':checked')) {
                    $(".wp_lstbookhour").css('display', 'none');
                    This.parent().parent().find(".wp_lstbookhour").css({ 'display': 'block' });
                    $(".close_bookHourId_box").on("click", function () {
                        $(".wp_lstbookhour").css('display', 'none');
                    });
                    var workDate = $("#TxtDateTimeFrom").val();
                }
                else {
                    alert("Bạn chưa chấm công cho Stylist: " + '[ ' + NameStylist + ' ]');
                }
            };

            function wchooseHour(This) {
                if (This.hasClass("active")) {
                    This.removeClass("active");
                }
                else {
                    This.addClass("active");
                }
            }

            //=====================================//
            // Submit EditFTK : Xử lý cập nhật chấm công khi thay đổi khung giờ
            //=====================================//

            function EditFTK(This, staffId, staffName, salonId) {
                var _salonid = $("#Salon").val();
                //them mới
                salonId = $("#Salon").val();
                var listHourId = '';
                var ListHourFrame = '';

                This.parent().parent().find(".it_bookhour").each(function () {

                    if ($(this).hasClass('active')) {

                        var Hourid = parseInt($(this).attr('data-hourid'));
                        listHourId += Hourid + ',';

                        var HourFrame = $(this).attr('data-hourframe');
                        ListHourFrame += HourFrame + ',';
                    }
                });

                if (listHourId.substring(listHourId.length - 1) == ",") {
                    listHourId = listHourId.substring(0, listHourId.length - 1);
                }

                if (ListHourFrame.substring(ListHourFrame.length - 1) == ",") {
                    ListHourFrame = ListHourFrame.substring(0, ListHourFrame.length - 1);
                }

                var workDate = $("#TxtDateTimeFrom").val();

                // set giá trị biến global
                paramsObj.salonID = salonId;
                paramsObj.staffID = staffId;
                paramsObj.staffName = staffName;
                paramsObj.workDate = workDate;
                paramsObj.hourIDs = listHourId;
                paramsObj.hourFrames = ListHourFrame;
                // Post API update
                postAPIUpdateHourIDs();

                // Cập nhật tổng slot trong ngày tại salon
                //updateSalonSlotByHourIDs();                
            }

            /*
            * paramsObj
            */
            var paramsClass = function () {
                this.salonID = 0;
                this.salonName = '';
                this.staffID = 0;
                this.staffName = '';
                this.workDate = '';
                this.hourIDs = '';
                this.hourFrames = '';
                this.isEnroll = false;
                this.workTimeID = 0;
            }

            var paramsObj = new paramsClass();
            function getParamsObj() {
                return this.paramsObj;
            }
            function setParamsObj(params) {
                this.paramsObj = params;
            }

            /*
            * Post API check enroll
            */
            function postAPIenroll() {
                // Post API check enroll
                startLoading();
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/TimeKeeping/WorkDay_Listing.aspx/AddEnroll",
                    data: '{staffId : "' + paramsObj.staffID + '", salonId : "' + paramsObj.salonID + '", workDate : "' + paramsObj.workDate + '", isEnroll : "' + paramsObj.isEnroll + '", workTimeId : "' + paramsObj.workTimeID + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            //alert("Đã điểm danh cho [ " + staffName + " ]");
                            if (paramsObj.isEnroll) {
                                showNotification("Đã điểm danh cho [ " + paramsObj.staffName + " ]");
                                // update panel
                                $(".wp_worktime .btn-workhour.active").click();
                            }
                            else {
                                showNotification("Đã hủy điểm danh cho [ " + paramsObj.staffName + " ]");
                                $(".wp_worktime .btn-workhour.active").click();
                            }
                            /// init status for checkbox checkall
                            initCheckall($("#inputCheckAll"));
                        } else {
                            alert("Có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển.");
                        }
                        finishLoading();
                        // Cập nhật slot cho salon
                        //updateSalonSlotByEnroll(isEnroll);
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            /*
            * Post API update HourIDs
            */
            function postAPIUpdateHourIDs() {
                // Post API update enroll by edit hour frame
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/TimeKeeping/WorkDay_Listing.aspx/EditObjFTK",
                    data: '{staffId : "' + paramsObj.staffID + '", salonId : "' + paramsObj.salonID + '", workDate : "' + paramsObj.workDate + '", HourId :"' + paramsObj.hourIDs + '", ListHourFrame : "' + paramsObj.hourFrames + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            showNotification("Cập nhật giờ làm thành công cho [ " + paramsObj.staffName + " ]");
                            $('.wp_lstbookhour').hide();
                            if ($(".wp_worktime .btn-workhour.active").length > 0) {
                                $(".wp_worktime .btn-workhour.active").click();
                            } else {
                                $("#ViewData").click();
                            }
                        }
                        else {
                            showNotification(`${mission.msg} [ ${paramsObj.staffName} ]`);
                            $("#ViewData").click();
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            /*
            * Cập nhật tổng slot tại salon khi chấm công
            */
            function updateSalonSlotByEnroll(isEnroll) {
                if (isEnroll) {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/TimeKeeping/WorkDay_Listing.aspx/InsertToSlotTable",
                        data: '{staffID : "' + paramsObj.staffID + '", salonID : "' + paramsObj.salonID + '", workDate : "' + paramsObj.workDate + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            postAPIenroll();
                        },
                    });
                }
                else {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/TimeKeeping/WorkDay_Listing.aspx/DeleteToSlotTable",
                        data: '{staffID : "' + paramsObj.staffID + '", salonID : "' + paramsObj.salonID + '", workDate : "' + paramsObj.workDate + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            postAPIenroll();
                        },
                    });
                }
            }

            /*
            * Cập nhật tổng slot tại salon khi set trực tiếp khung giờ
            */
            function updateSalonSlotByHourIDs() {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/TimeKeeping/WorkDay_Listing.aspx/UpdateParttimeToSlotTable",
                    data: '{staffID : "' + paramsObj.staffID + '",listHourId :"' + paramsObj.hourIDs + '", salonID : "' + paramsObj.salonID + '", workDate : "' + paramsObj.workDate + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        //postAPIUpdateHourIDs();
                        console.log("Succeeded.");
                    },
                    failure: function (response) { alert("Có lỗi xảy ra, vui lòng liên hệ nhóm phát triển."); }
                });
            }

            //======================================//
            //Check permission check in and check out
            //=====================================//
            function PermissionCheckinAndcheckout() {
                $(".btn-workhour").css("display", "none");
                $(".map-edit .input-enroll").remove();
                $(".btn_hourId").css("display", "none");
            }

        </script>

    </asp:Panel>
    <style>
        .woktime-listing .wp_worktime {
            padding-right: 0px;
        }

        .wp_worktime .div_ca {
            float: left;
            margin: 0px;
        }

            .wp_worktime .div_ca .p_ca:last-child {
                margin-right: 0px;
            }

        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            padding: 2px;
        }
    </style>
</asp:Content>
