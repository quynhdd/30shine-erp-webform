﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Data.Entity.Migrations;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.TimeKeeping
{
    public partial class PartTime_Add : System.Web.UI.Page
    {
        private string PageID = "PARTTIME_ADD";
        private bool Perm_Access = false;
        private bool Perm_ViewAllData = false;
        public CultureInfo culture = new CultureInfo("vi-VN");
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //////Perm_Add = permissionModel.GetActionByActionNameAndPageId("Perm_Add", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Today);
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                var SalonId = Session["ss_salonId"] != null ? Convert.ToInt32(Session["ss_salonId"]) : 0;
                if (SalonId > 0 && Perm_ViewAllData)
                {
                    var items = Salon.Items.FindByValue(SalonId.ToString());
                    if (items != null)
                    {
                        items.Selected = true;
                    }
                }
            }
        }

        protected void AddServey(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                if (TxtDateTimeFrom.Text != "")
                {
                    var obj = new FlowTimeKeeping();
                    obj.SalonId = int.TryParse(Salon.SelectedValue.ToString(), out integer) ? integer : 0;
                    obj.StaffId = int.TryParse(HDF_StaffId.Value.ToString(), out integer) ? integer : 0;
                    obj.WorkHour = int.TryParse(WorkHour.Text, out integer) ? integer : 0;
                    obj.Note = Note.Text;
                    obj.DoUserId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                    obj.WorkDate = DateTime.Now;
                    obj.CreatedDate = DateTime.Now;
                    obj.IsDelete = 0;
                    obj.Type = 2;

                    if (obj.SalonId > 0)
                    {
                        Session["ss_salonId"] = obj.SalonId;
                    }

                    db.FlowTimeKeepings.AddOrUpdate(obj);
                    var exc = db.SaveChanges();
                    // Thông báo thành công
                    var MsgParam = new List<KeyValuePair<string, string>>();
                    if (exc > 0)
                    {
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                    }
                    else
                    {
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "warning"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thất bại!"));
                    }
                    UIHelpers.Redirect("/admin/cong-nhat/part-time.html", MsgParam);
                }
            }
        }

      
    }
}