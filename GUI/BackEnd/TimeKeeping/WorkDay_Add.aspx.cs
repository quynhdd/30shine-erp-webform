﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Data.Entity.Migrations;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.TimeKeeping
{
    public partial class WorkDay_Add : System.Web.UI.Page
    {
        private string PageID = "";
        private bool Perm_Access = false;
        private bool Perm_ViewAllData = false;
        public CultureInfo culture = new CultureInfo("vi-VN");
        /// <summary>
        /// check permission
        /// </summary>
        protected void checkPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //string perName = Session["User_Permission"].ToString();
                //string url = Request.Url.AbsolutePath.ToString();
                //Solution_30shineEntities db = new Solution_30shineEntities();
                //var pem = (from m in db.Tbl_Permission_Map
                //           join mn in db.Tbl_Permission_Menu on m.mID equals mn.mID
                //           where m.pName == perName && mn.Url_Main.EndsWith(url) || mn.Url_Rewrite.EndsWith(url)
                //           select new { m.aID }
                //           ).FirstOrDefault();
                //if (pem != null)
                //{
                //    var pemArray = pem.aID.Split(',');
                //    if (Array.IndexOf(pemArray, "1") > -1)
                //    {
                //        Perm_Access = true;
                //        if (Array.IndexOf(pemArray, "2") > -1)
                //        {
                //            Perm_ViewAllData = true;
                //        }

                //    }
                //    else
                //    {
                //        ContentWrap.Visible = false;
                //        NotAllowAccess.Visible = true;
                //    }

                //}
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();

            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            checkPermission();
            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = String.Format("{0:dd/MM/yyyy}", DateTime.Today);
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                var SalonId = Session["ss_salonId"] != null ? Convert.ToInt32(Session["ss_salonId"]) : 0;
                if (SalonId > 0)
                {
                    var items = Salon.Items.FindByValue(SalonId.ToString());
                    if (items != null)
                    {
                        items.Selected = true;
                    }
                }
            }
        }

        protected void AddServey(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                if (TxtDateTimeFrom.Text != "")
                {
                    var obj = new FlowTimeKeeping();
                    obj.SalonId = int.TryParse(Salon.SelectedValue.ToString(), out integer) ? integer : 0;
                    obj.StaffId = int.TryParse(HDF_StaffId.Value.ToString(), out integer) ? integer : 0;
                    obj.Note = Note.Text;
                    obj.DoUserId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                    obj.WorkDate = DateTime.Now;
                    obj.CreatedDate = DateTime.Now;
                    obj.IsDelete = 0;
                    obj.Type = 1;

                    if (obj.SalonId > 0)
                    {
                        Session["ss_salonId"] = obj.SalonId;
                    }

                    db.FlowTimeKeepings.AddOrUpdate(obj);
                    var exc = db.SaveChanges();
                    // Thông báo thành công
                    var MsgParam = new List<KeyValuePair<string, string>>();
                    if (exc > 0)
                    {
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                    }
                    else
                    {
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "warning"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thất bại!"));
                    }
                    UIHelpers.Redirect("/admin/cong-nhat/diem-danh.html", MsgParam);
                }
            }
        }

   

        //private void Bind_ratingValue()
        //{
        //    using (var db = new Solution_30shineEntities())
        //    {
        //        var ratingValue = db.Rating_ConfigPoint.Where(w => w.IsDelete != 1 && w.Status == 1 && w.Hint == 2).ToList();
        //        var Key = 0;
        //        var item = new ListItem();
        //        if (ratingValue.Count > 0)
        //        {
        //            foreach (var v in ratingValue)
        //            {
        //                item = new ListItem(v.ConventionName, v.ConventionPoint.ToString());
        //                Mark.Items.Insert(Key, item);
        //                Key++;
        //            }
        //            Mark.SelectedIndex = 0;
        //        }
        //    }
        //}

        //private void Bind_infoThread()
        //{
        //    using (var db = new Solution_30shineEntities())
        //    {
        //        var thread = db.SocialThreads.Where(w => w.IsDelete != 1 && w.Publish == 1 && w.stType == 2).ToList();
        //        var Key = 0;
        //        var item = new ListItem();
        //        if (thread.Count > 0)
        //        {
        //            foreach (var v in thread)
        //            {
        //                item = new ListItem(v.Name, v.Id.ToString());
        //                Thread.Items.Insert(Key, item);
        //                Key++;
        //            }
        //            Thread.SelectedIndex = 0;
        //        }
        //    }
        //}

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "salonmanager" };
                string[] AllowAllData = new string[] { "root", "admin" };
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }
                if (Array.IndexOf(AllowAllData, Permission) != -1)
                {
                    Perm_ViewAllData = true;
                }
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
    }
}