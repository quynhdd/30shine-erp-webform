﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Reckon_WrkDay_Salon.aspx.cs" Inherits="_30shine.GUI.BackEnd.TimeKeeping.Reckon_WrkDay_Salon" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">

    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý ngày công &nbsp;&#187; </li>
                        <li class="li-add"><a href="/admin/cong-nhat/diem-danh.html">Chấm công</a></li>
                        <li class="li-listing"><a href="/admin/thong-ke-cham-cong.html">Thống kê</a></li>
                        <li class="li-listing"><a href="/admin/cong-nhat/tong-cong.html">Tổng công</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report woktime-listing">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row">
                    <div class="filter-item" style="margin-left: 0;">
                        <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Ngày chấm công</strong>
                        <div class="datepicker-wp">
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeFrom" placeholder="Chọn ngày" Style="width: 150px!important;"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                            <strong class="st-head" style="margin-left: 10px; margin-right: 10px; display: none;">
                                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                            </strong>
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeTo" placeholder="Đến ngày" Style="width: 150px!important; display: none;"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                        </div>

                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="Salon" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 150px;"></asp:DropDownList>
                    </div>

                    <div class="filter-item" style="display: none;">
                        <asp:TextBox CssClass="st-head ip-short eb-select eb-suggestion" data-field="staff.name" data-value="0"
                            AutoCompleteType="Disabled" ID="StaffName" ClientIDMode="Static" placeholder="Nhập tên Stylist/Skinner" runat="server"></asp:TextBox>
                        <div class="listing-staff-wp eb-select-data eb-suggestion-listing">
                            <ul class="ul-listing-staff ul-listing-suggestion" id="UlCustomerName">
                            </ul>
                        </div>
                    </div>

                    <asp:Panel ID="ViewData" CssClass="st-head btn-viewdata" Text="Xem dữ liệu"
                        ClientIDMode="Static" onclick="excPaging(1)" runat="server">
                        Xem danh sách
                    </asp:Panel>
                    <a href="/admin/cong-nhat/diem-danh.html" class="st-head btn-viewdata">Reset Filter</a>
                    <div class="export-wp drop-down-list-wp" style="display: none;">
                        <asp:Panel ID="fff" CssClass="st-head btn-viewdata"
                            ClientIDMode="Static" runat="server" onclick="EntryExportExcel('all')">
                            Xuất File Excel
                        </asp:Panel>
                        <%--OnClick="Exc_ExportExcel" --%>
                        <ul class="ul-drop-down-list" style="display: none;">
                            <li onclick="EntryExportExcel('all')">Tất cả dữ liệu</li>
                            <li onclick="EntryExportExcel('current page')">Trang hiện tại</li>
                        </ul>
                    </div>


                </div>
                <br />


                <!-- End Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>Danh sách số lương Stylist/Skinner được chấm công của từng Salon</strong>
                </div>

                <asp:UpdatePanel ID="UpdatePanel1" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="row">
                            <div class="col-md-9 col-xs-12 left">
                                <table class=" table table-bordered text-center">
                                    <thead>
                                        <tr>
                                            <%if (ngay1 != string.Format("{0:dd/MM/yyyy}", DateTime.Now))
                                                { %>
                                            <td>Bộ phận</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td colspan="2"><%=ngay1 %></td>
                                            <td colspan="2"><%=ngay2 %></td>
                                            <td colspan="2"><%=ngay3 %></td>
                                            <%} %>
                                            <%else
                                                { %>
                                            <td>Bộ phận</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td colspan="2">Hôm nay (<%=ngay1 %>)</td>
                                            <td colspan="2">Ngày mai (<%=ngay2 %>)</td>
                                            <td colspan="2">Ngày kia (<%=ngay3 %>)</td>
                                            <%} %>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>DSM</td>
                                            <td>Stylist</td>
                                            <td>Skinner</td>
                                            <td>Checkin</td>
                                            <td>Checkout</td>
                                            <td>Bảo vệ</td>
                                            <td></td>
                                            <td>Stylist</td>
                                            <td>Skinner</td>
                                            <td>Stylist</td>
                                            <td>Skinner</td>
                                            <td>Stylist</td>
                                            <td>Skinner</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%if (OBJ.Count > 0)
                                            { %>
                                        <%foreach (var v in OBJ)
                                            { %>
                                        <tr class="vungmienid<%= v.VungMienID %>">
                                            <td><%= v.ShortName %></td>
                                            <td style="background: <%= v.bgColorDSM %>;"><%= v.DSM %></td>
                                            <td style="background: <%= v.bgColorStylist %>;"><%= v.Stylist %></td>
                                            <td style="background: <%= v.bgColorSkinner %>;"><%= v.Skinner %></td>
                                            <td style="background: <%= v.bgColorCheckin %>;"><%= v.Checkin %></td>
                                            <td style="background: <%= v.bgColorCheckout %>;"><%= v.Checkout %></td>
                                            <td style="background: <%= v.bgColorSecuritys %>;"><%= v.Securitys %></td>
                                            <td></td>
                                            <td><%= v.totalStylistToday%></td>
                                            <td style="background: <%= v.bgColorToday %>;"><%= v.totalSkinerToday %></td>
                                            <td><%= v.totalStylistTomor %></td>
                                            <td style="background: <%= v.bgColorTomorrow %>;"><%= v.totalSkinnerTomor %></td>
                                            <td><%= v.totalStylistAfterTomor %></td>
                                            <td style="background: <%= v.bgColorAfterTomorrow %>;"><%= v.totalSkinnerAfterTomor %></td>
                                        </tr>
                                        <%} %>
                                        <%} %>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <!-- Row Table Filter -->

                <!-- End Row Table Filter -->

                <%--<asp:UpdatePanel ID="UPExcel" runat="server" ClientIDMode="Static">
            <ContentTemplate></ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="BtnFakeExcel" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>--%>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <%--<asp:Button ID="BtnFakeExcel" ClientIDMode="Static" runat="server" OnClick="Exc_ExportExcel" style="display:none;" />--%>
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="IsPaging" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_ExcelType" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELPage" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELSegment" />
                <asp:HiddenField ID="HDF_StaffId" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_WorkTimeId" ClientIDMode="Static" runat="server" />
            </div>
            <%-- END Listing --%>
        </div>

        <!-- Notification box -->
        <div class="notification-box">Thành công</div>
        <!--// Notification box -->
         <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 190px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                // Add active menu
                $("#glbTimeKeeping").addClass("active");
                $("#glbPartTime").addClass("active");
                //$("#subMenu li.li-add").addClass("active");

                // Price format
                UP_FormatPrice('.be-report-price');

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        //$input.val($input.val().split(' ')[0]);
                    }
                });

                // View data today
                $(".tag-date-today").click();

                // Bind Suggestion
                Bind_Suggestion();

                //============================
                // Show System Message
                //============================ 
                var qs = getQueryStrings();
                showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);

                /// init status for checkbox checkall
                initCheckall($("#inputCheckAll"));
            });

            function SubTableFixHeight() {
                $("tbody table.sub-table").each(function () {
                    $(this).height($(this).parent().height() + 1);
                });
            }

            function viewDataByDate(This, time) {
                $(".tag-date.active").removeClass("active");
                This.addClass("active");
                $("#TxtDateTimeTo").val("");
                $("#TxtDateTimeFrom").val(time);
                $("#ViewData").click();
            }

            //============================
            // Event delete
            //============================

            //============================
            // Suggestion Functions
            //============================
            function Bind_Suggestion() {
                $(".eb-suggestion").bind("keyup", function (e) {
                    if (e.keyCode == 40) {
                        UpDownListSuggest($(this));
                    } else {
                        Call_Suggestion($(this));
                    }
                });
                $(".eb-suggestion").bind("focus", function () {
                    Call_Suggestion($(this));
                });

                $(window).bind("click", function (e) {
                    if ((!e.target.className.match("eb-select") && e.target.parentElement.className.match("ul-listing-suggestion")) ||
                        (!e.target.className.match("eb-select") && !e.target.parentElement.className.match("ul-listing-suggestion"))) {
                        EBSelect_HideBox();
                    }
                });
            }

            function UpDownListSuggest(This) {
                var UlSgt = This.parent().find(".ul-listing-suggestion"),
                    index = 0,
                    LisLen = UlSgt.find(">li").length - 1,
                    Value;

                This.blur();
                UlSgt.find(">li.active").removeClass("active");
                UlSgt.find(">li:eq(" + index + ")").addClass("active");

                $(window).unbind("keydown").bind("keydown", function (e) {
                    if (e.keyCode == 40) {
                        if (index == LisLen) return false;
                        UlSgt.find(">li.active").removeClass("active");
                        UlSgt.find(">li:eq(" + (++index) + ")").addClass("active");
                        return false;
                    } else if (e.keyCode == 38) {
                        if (index == 0) return false;
                        UlSgt.find(">li.active").removeClass("active");
                        UlSgt.find(">li:eq(" + (--index) + ")").addClass("active");
                        return false;
                    } else if (e.keyCode == 13) {
                        // Bind data to HDF Field
                        var THIS = UlSgt.find(">li.active");
                        //var Value = THIS.text().trim();
                        var Value = THIS.attr("data-code");
                        var dataField = This.attr("data-field");
                        var HDF_StoreValue;

                        switch (dataField) {
                            case 'staff.name': HDF_StoreValue = "#HDF_StaffId"; break;
                        }
                        BindIdToHDF(THIS, Value, dataField, HDF_StoreValue, This);
                        EBSelect_HideBox();
                    }
                });
            }

            function Call_Suggestion(This) {
                var text = This.val(),
                    field = This.attr("data-field");
                Suggestion(This, text, field);
            }

            function BindIdToHDF(THIS, Code, Field, HDF_StoreValue, Input_DomId) {
                var text = THIS.text().trim();
                $(HDF_StoreValue).val(Code);
                $(Input_DomId).val(text);
                $(Input_DomId).parent().find(".eb-select-data").hide();

                // Auto post server
                $("#BtnFakeUP").click();
            }

            function EBSelect_HideBox() {
                $(".eb-select-data").hide();
                $("ul.ul-listing-suggestion li.active").removeClass("active");
            }

            function showMsgSystem(msg, status) {
                $("#MsgSystem").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);
                setTimeout(function () {
                    $("#MsgSystem").fadeTo("slow", 0, function () {
                        $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 1);
                    });
                }, 5000);
            }
            //============================//

            //==============
            // Check enroll
            //==============

            // Check enroll list

            /// show notification
            function showNotification(content) {
                $(".notification-box").text(content).fadeIn(function () {
                    setTimeout(function () {
                        $(".notification-box").fadeOut();
                    }, 2000);
                });
            }

            /// check all staff
            function checkAll(This) {
                var tableEnroll = $("#tableEnroll");
                var inputEnroll = $("#tableEnroll input.input-enroll");
                var listData = [];
                var status;
                inputEnroll.each(function () {
                    var id = parseInt($(this).attr("data-id"));
                    var salonId = parseInt($(this).attr("data-salonid"));
                    if (id != NaN && salonId != NaN) {
                        var obj = { id: id, salonid: salonId };
                        listData.push(obj);
                    }
                });
                if (This.is(":checked")) {
                    inputEnroll.prop("checked", true);
                    status = true;
                } else {
                    inputEnroll.prop("checked", false);
                    status = false;
                }
                // update to db
                checkEnrollList(JSON.stringify(listData), status);
            }

            /// init status for checkbox checkall
            function initCheckall(This) {
                var num = 0;
                var tr = $("#tableEnroll tbody > tr").length;
                $("#tableEnroll input.input-enroll").each(function () {
                    if ($(this).is(":checked")) {
                        num++;
                    }
                });
                if (num == tr) {
                    This.prop("checked", true);
                }
                else {
                    This.prop("checked", false);
                }
            }

            function loadDataByWorkTimeId(id, This) {
                This.parent().find(".btn-workhour.active").removeClass("active");
                This.addClass("active");
                $("#HDF_WorkTimeId").val(id);
                // load data
                $("#BtnFakeUP").click();
            };
            //Bind Id FlowTimeKeeping
            function editHourFrame(This, staffId, salonId) {
                $(".wp_lstbookhour").css('display', 'none');
                This.parent().parent().find(".wp_lstbookhour").css({ 'display': 'block' });
                $(".close_bookHourId_box").on("click", function () {
                    $(".wp_lstbookhour").css('display', 'none');
                });
                var workDate = $("#TxtDateTimeFrom").val();

            };

            function wchooseHour(This) {
                if (This.hasClass("active")) {
                    This.removeClass("active");
                }
                else {
                    This.addClass("active");
                }
            }

            //=====================================//
            // Submit EditFTK
            //=====================================//

            function EditFTK(This, staffId, staffName, salonId) {
                var listHourId = '';
                var WorkHour = '';
                var Number = This.parent().parent().find('.active').length;
                if (Number > 0) {
                    WorkHour = Math.floor((Number - 16) / 2)
                    if (WorkHour > 0) {
                        WorkHour = WorkHour;
                    }
                    else if (WorkHour < 0) {
                        WorkHour = 0;
                    }

                } else {
                    WorkHour = 0;
                }

                This.parent().parent().find(".it_bookhour").each(function () {

                    if ($(this).hasClass('active')) {

                        var Hourid = parseInt($(this).attr('data-hourid'));
                        listHourId += Hourid + ',';
                    }
                });
                if (listHourId.substring(listHourId.length - 1) == ",")
                    listHourId = listHourId.substring(0, listHourId.length - 1);
                console.log(listHourId);

                var workDate = $("#TxtDateTimeFrom").val();
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/TimeKeeping/WorkDay_Listing.aspx/EditObjFTK",
                    data: '{staffId : "' + staffId + '", salonId : "' + salonId + '", workDate : "' + workDate + '", HourId :"' + listHourId + '", WorkHour : "' + WorkHour + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        console.log(response);
                        var mission = JSON.parse(response.d);
                        if (mission.success) {
                            showNotification("Cập nhật giờ làm thành công cho [ " + staffName + " ]");
                            $('.wp_lstbookhour').hide();
                            if ($(".wp_worktime .btn-workhour.active").length > 0) {
                                $(".wp_worktime .btn-workhour.active").click();
                            } else {
                                $("#ViewData").click();
                            }
                        } else {
                            alert("Có lỗi xảy ra. Bạn vui lòng nhập chấm công !");
                        }
                    },
                    failure: function (response) { alert(response.d); }
                });
            }

            //======================================//
            //Check permission check in and check out
            //=====================================//
            function PermissionCheckinAndcheckout() {
                $(".btn-workhour").css("display", "none");
                $(".map-edit .input-enroll").remove();
                $(".btn_hourId").css("display", "none");
            }


        </script>

    </asp:Panel>
    <style>
        .woktime-listing .wp_worktime {
            padding-right: 0px;
        }

        .wp_worktime .div_ca {
            float: left;
            margin: 0px;
        }
        /*.vungmienid1:last-child { margin-bottom: 25px; }
        .vungmienid2:last-child { margin-bottom: 25px; }*/
        /*.wp_worktime .div_ca .p_ca:last-child { margin-right: 0px; }*/
    </style>
</asp:Content>
