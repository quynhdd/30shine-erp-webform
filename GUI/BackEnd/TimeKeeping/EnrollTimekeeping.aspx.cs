﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.TimeKeeping
{
    public partial class EnrollTimekeeping : System.Web.UI.Page
    {
        public static CultureInfo culture = new CultureInfo("vi-VN");
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        private bool Perm_AllowEnrollCrossSalon = false;
        private static EnrollTimekeeping instance;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ViewAllDate = permissionModel.CheckPermisionByAction("Perm_ViewAllDate", pageId, staffId);
                Perm_AllowEnrollCrossSalon = permissionModel.CheckPermisionByAction("Perm_AllowEnrollCrossSalon", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
            if (Perm_AllowEnrollCrossSalon)
            {
                PanelEnrollCrossSalon.Visible = true;
            }
        }

        public int GetIds()
        {
            return int.TryParse(Session["User_Id"].ToString(), out var integer) ? integer : 0;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtWorkDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                Library.Function.bindSalonSpecial(new List<DropDownList> { ddlSalon, ddlSalonTimekeeping }, Perm_ViewAllData);
                BindSalonOther();
                BindStaffType();
                BindStaffTypeOther();
                BindDepartment();
                GetListStaff();
            }
        }
        
        public bool PermissionSM()
        {
            string permission, timekeeping, admin, root, asm;
            bool check = false;
            permission = HttpContext.Current.Session["User_Permission"].ToString();
            string[] arrPermision = permission.Split(',');
            if (arrPermision.Length == 0)
            {
                arrPermision = new[] { permission };
            }
            timekeeping = Array.Find(arrPermision, s => s.Equals("SỬA CHẤM CÔNG"));
            admin = Array.Find(arrPermision, s => s.Equals("ADMIN"));
            root = Array.Find(arrPermision, s => s.Equals("root"));
            asm = Array.Find(arrPermision, s => s.Equals("ASM"));
            if (!String.IsNullOrEmpty(timekeeping) || !String.IsNullOrEmpty(admin) || !String.IsNullOrEmpty(root) || !String.IsNullOrEmpty(asm))
            {
                check = true;
            }
            return check;
        }

        /// <summary>
        /// author: dungnm
        /// checktime hạn chế chấm công
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static bool CheckLimitTime(string date)
        {
            bool checkLimitTime = false;
            var DateLimited = Convert.ToDateTime(date, culture);
            if (DateLimited != null && DateLimited != DateTime.MinValue)
            {
                try
                {
                    if (DateLimited.Date > DateTime.Now.Date)
                    {
                        checkLimitTime = true;
                    }
                    else if (DateLimited.Date == DateTime.Now.Date && (DateLimited <= DateTime.Now && DateTime.Now <= DateTime.Now.Date.AddDays(1).AddMinutes(-60)))
                    {
                        checkLimitTime = true;
                    }

                    string perName = HttpContext.Current.Session["User_Permission"].ToString();
                    string[] arrayPermision = perName.Split(',');
                    if (arrayPermision.Length == 0)
                    {
                        arrayPermision = new[] { perName };
                    }
                    string checkSuaChamCong = Array.Find(arrayPermision, s => s.Equals("SỬA CHẤM CÔNG"));
                    string checkAdmin = Array.Find(arrayPermision, s => s.Equals("ADMIN"));
                    string checkRoot = Array.Find(arrayPermision, s => s.Equals("root"));
                    if (checkRoot != null || checkAdmin != null || checkSuaChamCong != null)
                    {
                        checkLimitTime = true;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return checkLimitTime;
        }

        /// <summary>
        /// Get WorkDay_Listing instance
        /// </summary>
        /// <returns></returns>
        public static EnrollTimekeeping getInstance()
        {
            if (!(EnrollTimekeeping.instance is EnrollTimekeeping))
            {
                return new EnrollTimekeeping();
            }
            else
            {
                return EnrollTimekeeping.instance;
            }
        }

        /// <summary>
        /// bind salon other <=> salon precent
        /// </summary>
        private void BindSalonOther()
        {
            using (var db = new Solution_30shineEntities())
            {
                var listSalon = new List<Tbl_Salon>();
                int salonId = int.TryParse(Session["SalonId"].ToString(), out var integer) ? integer : 0;
                int currentSalon = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                var UserId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                if (Perm_ViewAllData)
                {
                    listSalon = db.Tbl_Salon.Where(w => w.IsDelete == 0 && w.Publish == true && w.Id != salonId && w.Id != currentSalon).OrderBy(o => o.Order).ToList();
                }
                else
                {
                    listSalon = (
                                    from p in db.PermissionSalonAreas.AsNoTracking()
                                    join s in db.Tbl_Salon.AsNoTracking() on p.SalonId equals s.Id
                                    where
                                    p.IsDelete == false && p.StaffId == UserId
                                    && p.SalonId != currentSalon
                                    && s.Publish == true && s.IsDelete == 0
                                    select s
                                 ).OrderBy(o => o.Order).ToList();
                }
                ddlSalonOther.DataTextField = "ShortName";
                ddlSalonOther.DataValueField = "Id";
                ddlSalonOther.DataSource = listSalon;
                ddlSalonOther.DataBind();
            }
        }


        /// <summary>
        /// Bind Department to ddl
        /// </summary>
        protected void BindStaffType()
        {
            using (var db = new Solution_30shineEntities())
            {
                //chỉ lấy bộ phận stylist, skinner
                var types = db.Staff_Type.Where(w => w.IsDelete == 0 && w.Publish == true).OrderBy(o => o.Id).ToList();
                var item = new Staff_Type();
                item.Id = 0;
                item.Name = "Chọn tất cả bộ phận";
                types.Insert(0, item);
                ddlStaffType.DataTextField = "Name";
                ddlStaffType.DataValueField = "Id";
                ddlStaffType.DataSource = types;
                ddlStaffType.DataBind();
            }
        }

        /// <summary>
        /// Bind Department to ddl
        /// </summary>
        protected void BindDepartment()
        {
            using (var db = new Solution_30shineEntities())
            {
                var types = db.Staff_Type.Where(w => w.IsDelete == 0 && w.Publish == true).OrderBy(o => o.Id).ToList();
                var item = new Staff_Type();
                item.Id = 0;
                item.Name = "Chọn tất cả bộ phận";
                types.Insert(0, item);
                ddlDepartment.DataTextField = "Name";
                ddlDepartment.DataValueField = "Id";
                ddlDepartment.DataSource = types;
                ddlDepartment.DataBind();
            }
        }

        /// <summary>
        /// bind staff type other
        /// </summary>
        private void BindStaffTypeOther()
        {
            using (var db = new Solution_30shineEntities())
            {
                var staffType = db.Staff_Type.Where(w => w.IsDelete == 0 && w.Publish == true).OrderBy(o => o.Id).ToList();
                ddlStaffTypeOther.DataTextField = "Name";
                ddlStaffTypeOther.DataValueField = "Id";
                ddlStaffTypeOther.DataSource = staffType;
                ddlStaffTypeOther.DataBind();
            }
        }

        // get list  Staff where Salon and StaffType
        private void GetListStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                int salonId = int.TryParse(ddlSalonOther.SelectedValue, out var integer) ? integer : 0;
                int staffType = int.TryParse(ddlStaffTypeOther.SelectedValue, out integer) ? integer : 0;
                string sql = $@"select s.Id AS id, CAST((s.Id) AS NVARCHAR(100)) + ' - ' + s.Fullname AS fullName FROM Staff s
                                where s.IsDelete = 0 and s.Active = 1 AND s.SalonId = {salonId} AND s.[Type] = {staffType} AND s.RequireEnroll = 1";
                var staff = db.Database.SqlQuery<_30shine.MODEL.StaffView>(sql).ToList();
                var first = new _30shine.MODEL.StaffView();
                first.id = 0;
                first.fullName = "Chọn nhân viên";
                staff.Insert(0, first);
                ddlStaff.DataTextField = "fullName";
                ddlStaff.DataValueField = "id";
                ddlStaff.DataSource = staff;
                ddlStaff.DataBind();
            }
        }

        protected void ListStaff_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetListStaff();
        }

        protected void Salon_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindSalonOther();
            GetListStaff();
        }
    }
}