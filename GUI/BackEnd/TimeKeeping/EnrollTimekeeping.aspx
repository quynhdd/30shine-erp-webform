﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="EnrollTimekeeping.aspx.cs" Inherits="_30shine.GUI.BackEnd.TimeKeeping.EnrollTimekeeping" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    </
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <link href="/Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="/Assets/js/select2/select2.min.js"></script>
        <link href="/Assets/css/erp.30shine.com/timekeeping/timekeeping.common.css" rel="stylesheet" />
        <script src="/Assets/js/erp.30shine.com/timekeeping/timekeeping.common.js?1313"></script>
        <section>
            <div class="wp sub-menu row">
                <div class="wp960">
                    <div class="wp content-wp">
                        <ul class="ul-sub-menu" id="subMenu">
                            <li>Quản lý ngày công &nbsp;&#187; </li>
                            <li class="li-add"><a href="/admin/cong-nhat/diem-danh.html">Chấm công</a></li>
                            <li class="li-listing"><a href="/admin/thong-ke-cham-cong.html">Thống kê</a></li>
                            <li class="li-listing"><a href="/admin/cong-nhat/tong-cong.html">Tổng công</a></li>
                             <li class="li-listing"><a href="/admin/bao-cao/bieu-do-tong-cong.html">Biểu đồ lượng khách</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <div class="alert" id="success-alert" role="alert">
        </div>
        <section>
            <header id="header">
                <div class="container-fluid box-header">
                    <div class="box-top worktime-listing-header">
                        <span class="span-title">Chuyển nhân sự</span>
                        <div class="customer-add customer-listing be-report timekeeping-box">
                            <div class="row" style="clear: both; border: 1px solid #ccc; padding-bottom: 5px;">
                                <div class="wp960 content-wp date-timekeeping">
                                    <div class="row">
                                        <div class="filter-item" style="margin-left: 0;">
                                            <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Ngày chấm công</strong>
                                            <div class="datepicker-wp">
                                                <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeFrom" onchange="OnchangeDatetime($(this),'step1');" placeholder="Chọn ngày" Style="width: 190px !important;"
                                                    ClientIDMode="Static" autocomplete="off" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="filter-item">
                                            <asp:DropDownList ID="ddlSalon" CssClass="form-control select" onchange="OnchangeSalon('step1');" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 150px;" AutoPostBack="true" OnSelectedIndexChanged="Salon_SelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                        <div class="filter-item">
                                            <asp:DropDownList ID="ddlStaffType" CssClass="form-control select" onchange="OnchangeDepartment('step1');" runat="server" ClientIDMode="static" Style="margin: 12px 0; width: 150px;"></asp:DropDownList>
                                        </div>
                                        <asp:Panel ID="ViewData" CssClass="st-head btn btn-viewdata" Text="Xem dữ liệu"
                                            ClientIDMode="Static" runat="server" onclick="GetList()">
                                            Xem danh sách
                                        </asp:Panel>
                                    </div>
                                </div>
                                <br />
                                <div class="wp960 content-wp">
                                    <div class="row" runat="server" id="PanelEnrollCrossSalon" clientidmode="static" visible="false">
                                        <div class="filter-item" style="margin-left: 0;">
                                            <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-user-plus" aria-hidden="true" style="margin-right: -3px;"></i>
                                                Thêm nhân viên</strong>
                                        </div>
                                        <div class="filter-item">
                                            <asp:DropDownList ID="ddlSalonOther" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 150px;" AutoPostBack="true" OnSelectedIndexChanged="ListStaff_SelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                        <div class="filter-item">
                                            <asp:DropDownList ID="ddlStaffTypeOther" CssClass="form-control select" runat="server" ClientIDMode="static" Style="margin: 12px 0; width: 150px;" AutoPostBack="true" OnSelectedIndexChanged="ListStaff_SelectedIndexChanged"></asp:DropDownList>
                                        </div>
                                        <div class="filter-item">
                                            <asp:DropDownList ID="ddlStaff" CssClass="form-control select" runat="server" ClientIDMode="static" Style="margin: 12px 0; width: 150px;"></asp:DropDownList>
                                        </div>
                                        <div class="st-head btn btn-viewdata" id="TransferOtherStaff" onclick="TransferMoveStaffOtherTimekeeping();">Thêm nhân viên</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <div id="body">
                <div class="container-fluid box-body">
                    <span class="span-title">Chấm công</span>
                    <div class="customer-add customer-listing be-report">
                        <div class="row worktime-listing-body">
                            <div class="row box-do-work">
                                <div class="wp960 content-wp date-timekeeping">
                                    <div class="row">
                                        <div>
                                            <h3>1. Chọn ca</h3>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 10px;">
                                        <div id="work-time" class="col-md-12 list-do-work">
                                            <%--Code demo không xoá
                                                <div class="col-md-2 do-work btn">
                                                <span class="span-do-work">
                                                    <i class="fa fa-check-circle-o" aria-hidden="true"></i>
                                                    <b class="b-do-work">Ca 1</b>
                                                    <span class="time-do-work">8h30 - 17h30</span>
                                                </span>
                                            </div>--%>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row box-department-staff">
                                <div class="wp960 content-wp date-timekeeping">
                                    <div class="row">
                                        <h3>2. Chọn bộ phận, nhân sự</h3>
                                    </div>
                                    <div class="row box-date">
                                        <strong class="st-head st-title"><i class="fa fa-clock-o"></i>Ngày chấm công</strong>
                                        <div class="datepicker-wp">
                                            <asp:TextBox CssClass="txtDateTime st-head" ID="txtWorkDate" onchange="OnchangeDatetime($(this),'step2');" placeholder="Chọn ngày" Style="width: 190px !important;"
                                                ClientIDMode="Static" autocomplete="off" runat="server"></asp:TextBox>
                                        </div>
                                        <div class="filter-item">
                                            <asp:DropDownList ID="ddlSalonTimekeeping" CssClass="form-control select" onchange="OnchangeSalon('step2');" runat="server" ClientIDMode="static" Style="margin: 12px 0; width: 150px;"></asp:DropDownList>
                                        </div>
                                        <div class="filter-item">
                                            <asp:DropDownList ID="ddlDepartment" CssClass="form-control select" runat="server" onchange="OnchangeDepartment('step2');" ClientIDMode="Static" Style="margin: 12px 0; width: 150px;"></asp:DropDownList>
                                        </div>
                                        <div class="do-work div-do-work btn" style="background: #000;" onclick="GetList()">
                                            <span id="span-do-work" class="span-do-work" style="color: white;">Hiển thị nhân sự
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row box-table-add-staff">
                                    <div class="wp960 content-wp">
                                        <div class="col-md-12">
                                            <table id="table-header" class="table table-bordered table-header">
                                                <thead>
                                                    <tr>
                                                        <th id="th-stt" width="100px;">STT</th>
                                                        <th id="th-id" width="200px;">Mã nhân viên</th>
                                                        <th id="th-name">Tên nhân viên</th>
                                                        <th id="th-department">Bộ phận</th>
                                                        <th id="th-timekeeping">
                                                            <div class="checkbox">
                                                                Chọn tất cả 
                                                                <label id="label-for-all">
                                                                    <input class="check-for-all" type="checkbox" />
                                                                    <span class="cr" style="top: 4px; left: 5px;"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                </label>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                </thead>
                                            </table>
                                        </div>
                                        <div class="col-md-12 max-div">
                                            <table id="table-body" class="table table-bordered table-body">
                                                <tbody id="body-content">
                                                    <%--<tr>
                                                        <td class="td-stt">1</td>
                                                        <td class="td-id">234</td>
                                                        <td class="td-name">John</td>
                                                        <td class="td-department">STYLIST</td>
                                                        <td class="td-timekeeping">
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input class="" type="checkbox" />
                                                                    <span class="cr"><i class="cr-icon glyphicon glyphicon-ok"></i></span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                    </tr>--%>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="row box-table-add-staff">
                                    <div class="wp960 content-wp box-button-add-staff">
                                        <div class="row">
                                            <hr />
                                            <div class="col-md-12 list-do-work">
                                                <div style="margin: auto; width: 215PX;">
                                                    <span class="btn btn-default span-cancel" onclick="Cancel();">Huỷ bỏ</span>
                                                    <span class="btn btn-success span-timekeeping" id="span-timekeeping" onclick="InsertStaffTimekeeping();">Chấm công</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wp960 content-wp date-timekeeping">
                                <div class="row">
                                    <h3>3. Danh sách chấm công hiện tại ( Tổng Slot: <span id="span-slot" style="font-size: 20px; text-transform: uppercase; font-weight: 700;"></span>, Tổng giờ: <span id="span-hour" style="font-size: 20px; text-transform: uppercase; font-weight: 700;"></span>)</h3>
                                </div>
                            </div>
                            <div class="row box-department-staff">
                                <div class="row box-table-add-staff">
                                    <div class="col-md-12">
                                        <table id="table-header-timekeeping" class="table table-bordered table-header">
                                            <thead>
                                                <tr>
                                                    <th id="th-hour-timekeeping" style="min-width: 200px;">Ca làm việc</th>
                                                    <th id="th-stt-timekeeping" style="min-width: 70px; width: 100px">STT</th>
                                                    <th id="th-id-timekeeping" style="width: 120px;">Mã nhân viên</th>
                                                    <th id="th-name-timekeeping">Tên nhân viên</th>
                                                    <th id="th-department-timekeeping" style="min-width: 150px; width: 200px;">Bộ phận</th>
                                                    <th id="th-work-hour" style="min-width: 150px; width: 150px">Tổng giờ tăng ca</th>
                                                    <th id="th-flow-timekeeping" style="min-width: 250px;">Hành động</th>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div class="col-md-12">
                                        <table id="table-body-timekeeping" class="table table-bordered table-body">
                                            <tbody id="body-content-timekeeping">
                                                <%--Code demo
                                                    <tr>
                                                        <td class="td-hour-timekeeping" rowspan="3" style="vertical-align: middle !important;">Ca 1</td>
                                                        <td class="td-stt-timekeeping" rowspan="1" style="padding: 8px 0 8px 25px !important;">1</td>
                                                        <td class="td-id-timekeeping" rowspan="1" style="padding: 8px 0 8px 25px !important;">234</td>
                                                        <td class="td-name-timekeeping" rowspan="1" style="padding: 8px 0 8px 25px !important;">John</td>
                                                        <td class="td-department-timekeeping" rowspan="1" style="padding: 8px 0 8px 25px !important;">STYLIST</td>
                                                        <td class="td-work-hour" rowspan="1" style="padding: 8px 0 8px 25px !important;">1</td>
                                                        <td class="td-flow-timekeeping" rowspan="1" style="padding: 8px 0 8px 25px !important;"></td>
                                                    </tr>
                                                    <tr>
                                                        <td class="td-stt-timekeeping" rowspan="1" style="padding: 8px 0 8px 25px !important;">1</td>
                                                        <td class="td-id-timekeeping" rowspan="1" style="padding: 8px 0 8px 25px !important;">234</td>
                                                        <td class="td-name-timekeeping" rowspan="1" style="padding: 8px 0 8px 25px !important;">John</td>
                                                        <td class="td-department-timekeeping" rowspan="1" style="padding: 8px 0 8px 25px !important;">STYLIST</td>
                                                        <td class="td-work-hour" rowspan="1" style="padding: 8px 0 8px 25px !important;">1</td>
                                                        <td class="td-flow-timekeeping" rowspan="1" style="padding: 8px 0 8px 25px !important;"></td>
                                                    </tr>--%>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <!-- Trigger the modal with a button -->
                                <!-- Modal -->
                                <div class="modal fade" id="myModal" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="wp_lstbookhour" style="display: block;">
                                                <div class="worktime-listing">
                                                    <div class="ctn_lstbookhour">
                                                        <div class="close_bookHourId_box" onclick="CloseModal()">X Đóng</div>
                                                        <span id="span-staff-name">Nhân viên: Nguyễn Văn Đồng</span>
                                                        <span id="span-staff-id" style="display: none;"></span>
                                                        <div class="col-md-12 div-color">
                                                            <span class="span-color col-md-3"><i class="fa fa-circle" style="color: #ccc !important"></i>Chưa chấm công</span>
                                                            <span class="span-color col-md-3" style="width: 110px !important;"><i class="fa fa-circle" style="color: #5cb85c !important"></i>Trong ca</span>
                                                            <span class="span-color col-md-3"><i class="fa fa-circle" style="color: #fbb104 !important"></i>Tăng ca</span>
                                                        </div>
                                                        <div class="div_lst_bookhour" id="list-book-hour">
                                                            <%-- mẫu code không xoá
                                                            <a class='it_bookhour' data-hourid="" data-hourframe="" onclick="chooseHour($(this))">8h30</a>
                                                            --%>
                                                        </div>
                                                        <div id="work-time-popup" class="col-md-12 list-do-work word-wrap">
                                                            <%--Code mẫu
                                                                <div class="col-md-2 div-work-time">
                                                                <span class="span-work-time">Ca 1</span><br />
                                                                <span class="span-work-time">8h30 - 17h30</span>
                                                            </div>--%>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="wp_btn_hourId">
                                                    <a class="btn_hourId form-control btn btn-cancel" onclick="CloseModal()">Huỷ bỏ</a>
                                                    <a class="btn_hourId form-control btn btn-complete" onclick="UpdateOvertime()">Hoàn tất</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div>
            <asp:HiddenField ID="HDF_WorkTimeId" ClientIDMode="Static" runat="server" />
        </div>
        <script>
            var url_timekeeping = '<%= Libraries.AppConstants.URL_API_TIMEKEEPING%>';
            var doUserId = '<%=GetIds()%>';
            var permissionSM = '<%=PermissionSM()%>';
        </script>
        <!-- Page loading -->
        <div class="page-loading">
            <p>Vui lòng đợi trong giây lát...</p>
        </div>
    </asp:Panel>
</asp:Content>
