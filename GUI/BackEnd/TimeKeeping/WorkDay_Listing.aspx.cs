﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Globalization;
using System.Web.Script.Serialization;
using ExportToExcel;
using Excel;
using LinqKit;
using System.Web.Services;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using _30shine.Helpers;
using Helpers;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using System.IO;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using _30shine.Helpers.Http;
using System.Net;

namespace _30shine.GUI.BackEnd.TimeKeeping
{
    public partial class WorkDay_Listing : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        protected List<List<ProductBasic>> ProductList;
        private Expression<Func<BillService, bool>> Where = PredicateBuilder.True<BillService>();
        public static CultureInfo culture = new CultureInfo("vi-VN");
        protected string Permission = "";
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        private bool Perm_AllowEnrollCrossSalon = false;
        protected List<Store_Get_Count_Satff_By_WorkDate_Result> OBJ;
        protected bool IsAccountant = false;
        List<_30shine.MODEL.ENTITY.EDMX.Staff> _LstNhanVien;
        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
        protected string ngay1;
        protected string ngay2;
        protected string ngay3;
        protected string table = "";
        private string permission = "";

        private string sql = "";
        private static DateTime timeFrom = new DateTime();
        private DateTime timeTo = new DateTime();
        private int salonId = 0;
        private static WorkDay_Listing instance;
        private Solution_30shineEntities db;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ViewAllDate = permissionModel.CheckPermisionByAction("Perm_ViewAllDate", pageId, staffId);
                Perm_AllowEnrollCrossSalon = permissionModel.CheckPermisionByAction("Perm_AllowEnrollCrossSalon", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
            if (Perm_AllowEnrollCrossSalon)
            {
                PanelEnrollCrossSalon.Visible = true;
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {

            SetPermission();
            if (!IsPostBack)
            {

                PermissionCheckinAndcheckout();
                TxtDateTimeFrom.Text = Day3;
                Library.Function.bindSalonSpecial(new List<DropDownList> { Salon }, Perm_ViewAllData);
                Bind_StaffType();
                //Salon khac
                Bind_SalonKhac();
                Bind_StaffType2();
                GetListStaff();
                //GenWhere();
                Bind_Paging();
                Bind_Data();
                checkLimitTime();
            }
            else
            {
                //getCountSatffbyWorkdate();
                //GenWhere();
            }
        }

        /// <summary>
        /// Get WorkDay_Listing instance
        /// </summary>
        /// <returns></returns>
        public static WorkDay_Listing getInstance()
        {
            if (!(WorkDay_Listing.instance is WorkDay_Listing))
            {
                return new WorkDay_Listing();
            }
            else
            {
                return WorkDay_Listing.instance;
            }
        }

        /// <summary>
        /// author: dungnm
        /// checktime hạn chế chấm công
        /// </summary>
        /// <returns></returns>
        public bool checkLimitTime()
        {
            bool check = false;
            if (timeFrom != null)
            {
                try
                {
                    DateTime workDate = timeFrom;
                    if (workDate.Date > DateTime.Now.Date)
                    {
                        check = true;
                    }
                    else if (workDate.Date == DateTime.Now.Date && (workDate <= DateTime.Now && DateTime.Now <= DateTime.Now.Date.AddDays(1).AddMinutes(-60)))
                    {
                        check = true;
                    }

                    string perName = Session["User_Permission"].ToString();
                    string[] arrayPermision = perName.Split(',');
                    if (arrayPermision.Length == 0)
                    {
                        arrayPermision = new[] { perName };
                    }
                    string checkSuaChamCong = Array.Find(arrayPermision, s => s.Equals("SỬA CHẤM CÔNG"));
                    string checkAdmin = Array.Find(arrayPermision, s => s.Equals("ADMIN"));
                    string checkRoot = Array.Find(arrayPermision, s => s.Equals("root"));
                    if (checkRoot != null || checkAdmin != null || checkSuaChamCong != null)
                    {
                        check = true;
                    }
                }
                catch (Exception ex)
                {
                    //throw ex;
                }
            }
            return check;
        }
        /// <summary>
        /// Trả về số tuần của tháng
        /// </summary>
        /// <param name="Date"></param>
        /// <returns></returns>
        private int GetFirtOfWeek(DateTime Date)
        {
            int mondays = 0;
            int month = Date.Month;
            int year = Date.Year;
            int day = Date.Day;
            int daysThisMonth = DateTime.DaysInMonth(year, month);
            DateTime beginingOfThisMonth = new DateTime(year, month, 1);
            for (int i = 0; i < day; i++)
                if (beginingOfThisMonth.AddDays(i).DayOfWeek == DayOfWeek.Monday)
                    mondays++;
            return mondays;
        }
        /// <summary>
        /// author: dungnm
        /// </summary>
        /// <param name="Staff_ID"></param>   Id nhân viên truyền vào
        /// <param name="sDate"></param>  Ngày chấm công
        /// <param name="SalonId"></param>  Salon được chấm công
        public static string CreateAnUpdateSalary(Input_SalaryIncome objSalaryIncome)
        {
            try
            {
                var response = new ResponseData();
                using (var client = new HttpClient())
                {
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    client.BaseAddress = new Uri(Libraries.AppConstants.URL_API_FINANCIAL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var Result = client.PostAsJsonAsync("salary-income", objSalaryIncome).Result;
                    if (Result.IsSuccessStatusCode)
                    {
                        var result = Result.Content.ReadAsStringAsync().Result;
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        var responseData = serializer.Deserialize<ResponseData>(result);
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                return response.message;
            }
            catch
            {
                throw new Exception();
            }
        }


        /// <summary>
        /// Update lương partTime
        /// </summary>
        /// <param name="Staff_ID"></param>
        /// <param name="sDate"></param>
        /// <param name="_WorkHour"></param>
        public static string UpdateLuongPartTime(Input_SalaryIncome objSalaryIncome)
        {
            try
            {
                var response = new ResponseData();
                using (var client = new HttpClient())
                {
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    client.BaseAddress = new Uri(Libraries.AppConstants.URL_API_FINANCIAL);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var Result = client.PutAsJsonAsync("overtime-salary", objSalaryIncome).Result;
                    if (Result.IsSuccessStatusCode)
                    {
                        var result = Result.Content.ReadAsStringAsync().Result;
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        var responseData = serializer.Deserialize<ResponseData>(result);
                    }
                    else
                    {
                        throw new Exception();
                    }
                }
                return response.message;
            }
            catch
            {
                throw new Exception();
            }
        }

        /// <summary>
        /// Lấy thông tin cấu hình Tăng ca trong bảng ConfigPartTime
        /// </summary>
        /// <param name="staff_id"></param>
        /// <param name="sDate"></param>
        /// <returns></returns>
        public static cls_configPartTime configPartTime(int staff_id, DateTime sDate)
        {
            cls_configPartTime dataConfig = new cls_configPartTime();
            try
            {
                using (Solution_30shineEntities db = new Solution_30shineEntities())
                {
                    var salonPartTime = db.FlowTimeKeepings.Where(r => r.StaffId == staff_id && r.WorkDate == sDate && r.IsEnroll == true).FirstOrDefault();
                    if (salonPartTime != null)
                    {
                        var dataSalonConfig = db.Tbl_Salon.Where(r => r.IsDelete == 0 && r.Id == salonPartTime.SalonId).FirstOrDefault();
                        if (dataSalonConfig != null)
                        {
                            dataConfig.IsPartTimeHour = dataSalonConfig.IsPartTimeHours;
                            dataConfig.IsPartTimeRating = dataSalonConfig.IsPartTimeRating;
                        }
                        else
                        {
                            dataConfig.IsPartTimeHour = true;
                            dataConfig.IsPartTimeRating = false;
                        }

                    }
                    else
                    {
                        dataConfig.IsPartTimeHour = true;
                        dataConfig.IsPartTimeRating = false;
                    }
                }
            }
            catch (Exception ex)
            {
                dataConfig.IsPartTimeHour = true;
                dataConfig.IsPartTimeRating = false;
            }

            return dataConfig;

        }
        /// <summary>
        /// kiểm tra xem id truyền vào có phải là stylist hoặc skinner không
        /// </summary>
        /// <param name="stylist_id">id stylist</param>
        /// <param name="skinner_id">id skinner</param>
        /// <returns></returns>
        public static cls_stylist_skinner checkStylistSkinner(int stylist_id, int skinner_id)
        {
            cls_stylist_skinner objcheck = new cls_stylist_skinner();

            try
            {
                using (Solution_30shineEntities db = new Solution_30shineEntities())
                {
                    var dataST = db.Staffs.Where(r => r.IsDelete == 0 && r.Type == 1 && r.Id == stylist_id).FirstOrDefault();
                    var dataSK = db.Staffs.Where(r => r.IsDelete == 0 && r.Type == 2 && r.Id == skinner_id).FirstOrDefault();
                    objcheck.stylist = dataST == null ? false : true;
                    objcheck.skinner = dataSK == null ? false : true;
                }
            }
            catch (Exception ex)
            {
                objcheck.stylist = true;
                objcheck.skinner = true;
                WriteLog("Có lỗi xảy ra: " + ex.Message, "checkStylistSkinner");
            }
            return objcheck;
        }


        protected void FillLstStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                _LstNhanVien = db.Staffs.ToList();
            }
        }

        private void GenWhere()
        {
            int integer;
            salonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
            int staffType = int.TryParse(StaffType.SelectedValue, out integer) ? integer : 0;
            int staffId = int.TryParse(HDF_StaffId.Value, out integer) ? integer : 0;
            int workTimeId = int.TryParse(HDF_WorkTimeId.Value, out integer) ? integer : 0;
            timeFrom = Convert.ToDateTime("1/8/2015", culture);
            timeTo = DateTime.Now.AddDays(1);
            string whereWorkTime = "";

            if (TxtDateTimeFrom.Text != "")
            {
                timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                if (TxtDateTimeTo.Text != "")
                {
                    timeTo = Convert.ToDateTime(TxtDateTimeTo.Text, culture);
                    if (timeTo.Year >= timeFrom.Year && timeTo.Month >= timeFrom.Month && timeTo.Day >= timeFrom.Day)
                    {
                        timeTo = timeTo.AddDays(1);
                    }
                }
                else
                {
                    timeTo = timeFrom.AddDays(1);
                }
            }
            //if (workTimeId > 0)
            //{
            //    whereWorkTime += " and b.WorkTimeId = " + workTimeId;
            //}
            sql = @"declare @time as DateTime;
                    set @time = '" + String.Format("{0:yyyy/MM/dd}", timeFrom) + @"';
                    select a.*, w.Color as Color , Coalesce(b.WorkDate, @time) as WorkDate, Cast(Coalesce(b.IsEnroll, 0) as bit) as IsEnroll, Coalesce(b.WorkHour, 0) as WorkHour, b.HourIds 
                    from Staff as a
                    left join FlowTimeKeeping as b
                    on a.Id = b.StaffId and b.WorkDate = @time " + whereWorkTime + @" and b.SalonId = a.SalonId
                    left join WorkTime as w
                    on b.WorkTimeId  = w.Id
                    where a.IsDelete != 1 and a.Active = 1 
                    and(a.isAccountLogin != 1 or a.isAccountLogin is null)";
            if (salonId > 0)
            {
                sql += " and a.SalonId = " + salonId /*+ "and b.SalonId = " + salonId*/ ;
            }
            if (staffType > 0)
            {
                sql += " and a.Type = " + staffType;
            }
            sql += " order by IsEnroll desc, w.Id asc , a.Id asc ";

        }

        private void Bind_Data()
        {
            int integer;
            salonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
            int staffType = int.TryParse(StaffType.SelectedValue, out integer) ? integer : 0;
            int staffId = int.TryParse(HDF_StaffId.Value, out integer) ? integer : 0;
            int workTimeId = int.TryParse(HDF_WorkTimeId.Value, out integer) ? integer : 0;
            timeFrom = Convert.ToDateTime("1/8/2015", culture);

            if (TxtDateTimeFrom.Text != "")
            {
                timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
            }
            using (var db = new Solution_30shineEntities())
            {
                var a = sql;
                object[] paras =
                {
                    new SqlParameter("@time", String.Format("{0:yyyy/MM/dd}", timeFrom)),
                    new SqlParameter("@salonId", salonId),
                    new SqlParameter("@staffType", staffType)
                };
                var lst = db.Database.SqlQuery<cls_timekeeping>("store_TimeKeeping_GetListTimeKeeping @time, @salonId, @staffType", paras).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                var hours = db.BookHours.Where(w => w.IsDelete != 1 && w.Publish == true);
                // Bind data
                if (lst.Count > 0)
                {
                    var Count = 0;
                    foreach (var v in lst)
                    {
                        var str = "";
                        if (v.HourIds != null && v.HourIds != "")
                        {
                            var temp = db.BookHours.SqlQuery("select * from BookHour where Id in (" + v.HourIds + ") order by HourFrame asc").ToList();
                            if (temp.Count > 0)
                            {
                                foreach (var v2 in temp)
                                {
                                    str += "<p class='_hourname'>" + v2.Hour + "</p>";
                                }
                            }
                            if (v.IsEnroll == false)
                            {
                                v.Color = "none";
                            }
                        }
                        var timekeeping = (
                                              from t in db.FlowTimeKeepings
                                              join s in db.Tbl_Salon on t.SalonId equals s.Id
                                              join staff in db.Staffs on t.StaffId equals staff.Id
                                              where
                                              t.IsEnroll == true
                                              && t.WorkDate == timeFrom
                                              && t.IsDelete == 0
                                              && t.StaffId == v.Id
                                              && s.IsDelete == 0
                                              && s.Publish == true
                                              && staff.IsDelete == 0
                                              && staff.Active == 1
                                              select new
                                              {
                                                  SalonId = t.SalonId,
                                                  SalonName = s.ShortName,
                                                  Partime = t.WorkHour
                                              }
                                          ).FirstOrDefault();
                        if (timekeeping != null)
                        {
                            v.SalonIdWorking = timekeeping.SalonId;
                            if (salonId != timekeeping.SalonId)
                            {
                                v.NoteStatusSalonCurrent = "(Salon đang làm việc: " + timekeeping.SalonName + ")";
                                v.Parttime = Convert.ToInt32(timekeeping.Partime);
                            }
                        }
                        if (v.SalonId != salonId)
                        {
                            v.NoteStatus = "(Salon hiện tại:  " + v.ShortName + ")";
                        }
                        if ((v.Active == 0 && v.isAccountLogin == 0) || v.IsDelete == 1)
                        {
                            v.NoteStatus = "(Đã nghỉ làm)";
                        }
                        lst[Count++].showHourFrame = str;
                    }
                }

                Rpt_PartTime.DataSource = lst;
                Rpt_PartTime.DataBind();
            }
        }

        //Bind salon con lai
        private void Bind_SalonKhac()
        {
            using (var db = new Solution_30shineEntities())
            {
                var listSalon = new List<Tbl_Salon>();
                int _SalonId = Convert.ToInt32(Session["SalonId"]);
                int _CurrentSalon = Convert.ToInt32(Salon.SelectedValue);
                var UserId = Convert.ToInt32(HttpContext.Current.Session["User_Id"]);
                if (Perm_ViewAllData)
                {
                    listSalon = db.Tbl_Salon.Where(w => w.IsDelete != 1 && w.Publish == true && w.Id != _SalonId && w.Id != _CurrentSalon).OrderByDescending(o => o.CityId).ToList();
                }
                else
                {
                    listSalon = (
                                        from p in db.PermissionSalonAreas.AsNoTracking()
                                        join s in db.Tbl_Salon.AsNoTracking() on p.SalonId equals s.Id
                                        where
                                        p.IsDelete == false && p.StaffId == UserId
                                        && p.SalonId != _CurrentSalon
                                        && s.Publish == true && s.IsDelete == 0
                                        select s
                                   ).OrderBy(o => o.CityId).ToList();
                }
                Salonkhac.DataTextField = "Name";
                Salonkhac.DataValueField = "Id";
                Salonkhac.DataSource = listSalon;
                Salonkhac.DataBind();
            }
        }
        // Bind_StaffType2
        private void Bind_StaffType2()
        {
            using (var db = new Solution_30shineEntities())
            {
                var staffType = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Id).ToList();

                StaffType2.DataTextField = "Name";
                StaffType2.DataValueField = "Id";


                string User_permission = HttpContext.Current.Session["User_Permission"].ToString();
                if (User_permission == "checkin" || User_permission == "checkout")
                {
                    StaffType.SelectedIndex = 1;
                }
                else
                {
                    StaffType.SelectedIndex = 1;
                }
                StaffType2.DataSource = staffType;
                StaffType2.DataBind();
            }
        }

        // get list  Staff where Salon and StaffType
        private void GetListStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                int _SalonId = int.TryParse(Salonkhac.SelectedValue, out integer) ? integer : 0;
                int _StaffType = Convert.ToInt32(StaffType2.SelectedValue);
                var listStaff = db.Staffs.Where(a => a.Active == 1 && a.IsDelete == 0 && (a.isAccountLogin == null || a.isAccountLogin == 0) && (a.isAppLogin == null || a.isAppLogin == 0) && a.SalonId == _SalonId && a.Type == _StaffType).ToList();
                Staff.DataTextField = "FullName";
                Staff.DataValueField = "Id";
                Staff.DataSource = listStaff;
                Staff.DataBind();
            }
        }

        private void Bind_StaffType()
        {
            using (var db = new Solution_30shineEntities())
            {
                var staffType = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Id).ToList();
                var Key = 0;

                StaffType.DataTextField = "Type";
                StaffType.DataValueField = "Id";

                ListItem item = new ListItem("Tất cả", "0");
                StaffType.Items.Insert(0, item);

                foreach (var v in staffType)
                {
                    Key++;
                    item = new ListItem(v.Name, v.Id.ToString());
                    StaffType.Items.Insert(Key, item);
                }
                string User_permission = HttpContext.Current.Session["User_Permission"].ToString();
                if (User_permission == "checkin" || User_permission == "checkout")
                {
                    StaffType.SelectedIndex = 1;
                }
                else
                {
                    StaffType.SelectedIndex = 1;
                }
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {

            Bind_Paging();
            Bind_Data();
            //getCountSatffbyWorkdate();
            ScriptManager.RegisterStartupScript(UPTableListing, this.GetType(), "Format Price", "UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing();", true);

            RemoveLoading();

        }

        protected void Bind_Paging()
        {
            // init Paging value  
            PAGING._Segment = 200000;
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? (HDF_Page.Value != "" ? Convert.ToInt32(HDF_Page.Value) : 1) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                salonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
                int staffType = int.TryParse(StaffType.SelectedValue, out integer) ? integer : 0;
                int staffId = int.TryParse(HDF_StaffId.Value, out integer) ? integer : 0;
                timeFrom = Convert.ToDateTime("1/8/2015", culture);

                if (TxtDateTimeFrom.Text != "")
                {
                    timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
                }
                object[] paras =
                {
                    new SqlParameter("@time", String.Format("{0:yyyy/MM/dd}", timeFrom)),
                    new SqlParameter("@salonId", salonId),
                    new SqlParameter("@staffType", staffType)
                };
                var Count = db.Database.SqlQuery<cls_timekeeping>("store_TimeKeeping_GetListTimeKeeping @time, @salonId, @staffType", paras).Count();
                //var Count = db.Database.SqlQuery<cls_timekeeping>(sql).Count();
                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        protected void Btn_SwitchExcel(object sender, EventArgs e)
        {
            if (HDF_ExcelType.Value == "Export")
            {
                //Exc_ExportExcel();
            }
            else if (HDF_ExcelType.Value == "Import")
            {
                //Exc_ImportExcel();
            }

        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        public string GenTime(int minutes)
        {
            string time = "";
            int hour = minutes / 60;
            int minute = minutes % 60;
            if (hour > 0)
            {
                time += hour + "h";
                time += minute + "p";
            }
            else
            {
                if (minute > 0)
                {
                    time += minute + " p";
                }
                else
                {
                    time += "-";
                }
            }
            return time;
        }

        /// <summary>
        /// Nhập điểm danh hàng ngày
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name="salonId"></param>
        /// <param name="workDate"></param>
        /// <param name="isEnroll"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public static string AddEnroll(int staffId, int salonId, string workDate, bool isEnroll, int workTimeId)
        {
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            var instance = WorkDay_Listing.getInstance();
            if (instance.checkLimitTime())
            {

                using (var db = new Solution_30shineEntities())
                {
                    string User_permission = HttpContext.Current.Session["User_Permission"].ToString();
                    if (User_permission != "checkin" && User_permission != "checkout")
                    {
                        string lstHourId = LstHourid(workTimeId, salonId);
                        int integer;
                        int userId = int.TryParse(HttpContext.Current.Session["User_Id"].ToString(), out integer) ? integer : 0;
                        var wd = Convert.ToDateTime(workDate, culture);
                        //Check stylist co chuyen salon hay khong
                        var objErrorFTKP = db.FlowTimeKeepings.FirstOrDefault(w => w.WorkDate == wd && w.StaffId == staffId && w.SalonId != salonId && w.IsEnroll == true);

                        if (objErrorFTKP != null)
                        {
                            objErrorFTKP.IsDelete = 1;
                            objErrorFTKP.IsEnroll = false;
                            objErrorFTKP.WorkHour = 0;
                            db.FlowTimeKeepings.AddOrUpdate(objErrorFTKP);
                            db.SaveChanges();
                        }

                        var obj = db.FlowTimeKeepings.FirstOrDefault(w => w.WorkDate == wd && w.StaffId == staffId && w.SalonId == salonId);
                        if (obj != null)
                        {
                            obj.ModifiedDate = DateTime.Now;
                        }
                        else
                        {
                            obj = new FlowTimeKeeping();
                            obj.StaffId = staffId;
                            obj.SalonId = salonId;
                            obj.DoUserId = userId;
                            obj.WorkDate = wd;

                            obj.CreatedDate = DateTime.Now;
                            obj.IsDelete = 0;
                        }
                        obj.IsEnroll = isEnroll;
                        if (isEnroll == true)
                        {
                            obj.IsDelete = 0;
                        }

                        obj.WorkTimeId = workTimeId;
                        obj.HourIds = lstHourId;
                        obj.WorkHour = 0;
                        db.FlowTimeKeepings.AddOrUpdate(obj);
                        var exc = db.SaveChanges();

                        if (exc > 0)
                        {
                            Msg.success = true;
                        }
                        else
                        {
                            Msg.success = false;
                        }

                        // InsertOrUpdate vào bảng SM_EnrollTemp
                        InsertOrUpdateEnrollTemp(staffId, salonId, workDate, isEnroll, lstHourId);
                        //Khoi tạo or update bang luong
                        var objInput = new Input_SalaryIncome
                        {
                            salon_id = salonId,
                            staff_id = staffId,
                            work_date = workDate
                        };
                        try
                        {
                            var request = new Request();
                            Task task = Task.Run(async () =>
                            {
                                await request.RunPostAsync(
                                    CreateAnUpdateSalary(objInput),
                                   new
                                   {
                                       data = objInput
                                   });
                            });
                            //task.Wait(200);
                        }
                        catch (Exception ex)
                        {
                            throw new Exception();
                        }
                        // Cập nhật slot statistic [Booking_Statistic_Slot]
                        instance.updateSalonSlot(obj.SalonId, obj.WorkDate.Value);
                    }
                    else
                    {
                        Msg.success = false;
                    }
                }
            }
            return serializer.Serialize(Msg);
        }

        /// <summary>
        /// Nhập giờ làm part-time hàng ngày
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name="salonId"></param>
        /// <param name="workDate"></param>
        /// <param name="isEnroll"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public static string AddPartTime(int staffId, int salonId, string workDate, int workHour)
        {
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                int userId = int.TryParse(HttpContext.Current.Session["User_Id"].ToString(), out integer) ? integer : 0;
                var wd = Convert.ToDateTime(workDate, culture);
                var obj = db.FlowTimeKeepings.FirstOrDefault(w => w.WorkDate == wd && w.StaffId == staffId && w.SalonId == salonId);
                if (obj != null)
                {
                    obj.ModifiedDate = DateTime.Now;
                }
                else
                {
                    obj = new FlowTimeKeeping();
                    obj.StaffId = staffId;
                    obj.SalonId = salonId;
                    obj.DoUserId = userId;
                    obj.CreatedDate = DateTime.Now;
                    obj.WorkDate = wd;
                    obj.IsDelete = 0;
                }
                obj.WorkHour = workHour;
                db.FlowTimeKeepings.AddOrUpdate(obj);
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name="salonId"></param>
        /// <param name="workDate"></param>
        /// <param name="isEnroll"></param>
        /// <param name="lstHourId"></param>
        private static void InsertOrUpdateEnrollTemp(int? staffId, int? salonId, string workDate, bool? isEnroll, string lstHourId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var _workDate = Convert.ToDateTime(workDate, culture);
                #region 
                //Check stylist co chuyen salon hay khong
                var objErrorSM_EnrollFTKP = db.SM_EnrollTemp.FirstOrDefault(w => w.WorkDate == _workDate && w.StaffId == staffId && w.SalonId != salonId && w.IsEnroll == true);

                if (objErrorSM_EnrollFTKP != null)
                {
                    objErrorSM_EnrollFTKP.IsDelete = false;
                    objErrorSM_EnrollFTKP.IsEnroll = false;
                    db.SM_EnrollTemp.AddOrUpdate(objErrorSM_EnrollFTKP);
                    db.SaveChanges();
                }
                #endregion
                // Insert or Update bản ghi vào bảng SM_EnrollTemp
                //var enroll = new SM_EnrollTemp();
                //
                var objSM_EnrollFTKP = db.SM_EnrollTemp.FirstOrDefault(w => w.WorkDate == _workDate && w.StaffId == staffId && w.SalonId == salonId);
                //Update
                if (objSM_EnrollFTKP != null)
                {
                    objSM_EnrollFTKP.ModifiedTime = DateTime.Now;
                    //Delete SM_EnrollTemp_Hour khi cham cong lai
                    //db.Store_update_SM_EnrollTemp_Hour(objSM_EnrollFTKP.Id);
                }
                // Insert
                else
                {
                    objSM_EnrollFTKP = new SM_EnrollTemp();
                    objSM_EnrollFTKP.SalonId = salonId;
                    objSM_EnrollFTKP.StaffId = staffId;
                    objSM_EnrollFTKP.CreatedTime = DateTime.Now;
                }
                objSM_EnrollFTKP.IsEnroll = isEnroll;
                if (isEnroll == true)
                {
                    objSM_EnrollFTKP.IsDelete = false;
                }
                else
                {
                    objSM_EnrollFTKP.IsDelete = true;
                }
                objSM_EnrollFTKP.WorkDate = _workDate;
                db.SM_EnrollTemp.AddOrUpdate(objSM_EnrollFTKP);
                db.SaveChanges();

                // Insert ID khung giờ vào bảng SM_EnrollTemp_Hour
                UpdateSM_EnrollTemp_Hour(staffId, salonId, workDate, lstHourId);
            }
        }

        [WebMethod(EnableSession = true)]
        public static string AddEnrollList(string listData, string workDate, bool isEnroll)
        {
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            if (listData != "[]")
            {
                var list = serializer.Deserialize<List<cls_staffEnroll>>(listData);
                if (list.Count > 0)
                {
                    using (var db = new Solution_30shineEntities())
                    {
                        int integer;
                        int error = 0;
                        int userId = int.TryParse(HttpContext.Current.Session["User_Id"].ToString(), out integer) ? integer : 0;
                        var wd = Convert.ToDateTime(workDate, culture);

                        foreach (var v in list)
                        {
                            var obj = db.FlowTimeKeepings.FirstOrDefault(w => w.WorkDate == wd && w.StaffId == v.id && w.SalonId == v.salonid);
                            if (obj != null)
                            {
                                obj.ModifiedDate = DateTime.Now;
                            }
                            else
                            {
                                obj = new FlowTimeKeeping();
                                obj.StaffId = v.id;
                                obj.SalonId = v.salonid;
                                obj.DoUserId = userId;
                                obj.CreatedDate = DateTime.Now;
                                obj.WorkDate = wd;
                                obj.IsDelete = 0;
                            }
                            obj.IsEnroll = isEnroll;
                            db.FlowTimeKeepings.AddOrUpdate(obj);
                            error += db.SaveChanges() > 0 ? 0 : 1;
                        }
                        if (error == 0)
                        {
                            Msg.success = true;
                        }
                        else
                        {
                            Msg.success = false;
                        }
                    }
                }
            }
            return serializer.Serialize(Msg);
        }





        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }

        [WebMethod]
        public static string LstHourid(int WorkTimeID, int SalonId)
        {
            string str = "";
            using (var db = new Solution_30shineEntities())
            {
                //Xử lý khung giờ theo ca
                if (WorkTimeID > 0)
                {
                    var objWorkTime = db.WorkTimes.FirstOrDefault(a => a.IsDelete == 0 && a.Publish == true && a.Id == WorkTimeID);

                    string strStratWorkHour = Convert.ToString(objWorkTime.StrartTime);
                    string strEndWorkHour = Convert.ToString(objWorkTime.EnadTime);
                    string strLunchhour = Convert.ToString(objWorkTime.Lunchhour);
                    string strLunchhour2 = Convert.ToString(objWorkTime.Lunchhour2);
                    TimeSpan StartWorkHour = TimeSpan.Parse(strStratWorkHour);
                    TimeSpan EndWorkHour = TimeSpan.Parse(strEndWorkHour);
                    TimeSpan Lunchhour = TimeSpan.Parse(strLunchhour);
                    TimeSpan Lunchhour2 = TimeSpan.Parse(strLunchhour2);

                    var lstHour = db.BookHours.Where(a => a.SalonId == SalonId && a.IsDelete == 0 && a.Publish == true && a.HourFrame >= StartWorkHour && a.HourFrame <= EndWorkHour && a.HourFrame != Lunchhour && a.HourFrame != Lunchhour2).OrderBy(a => a.HourFrame).ToList();
                    int _Count = Convert.ToInt32(lstHour.Count);
                    if (_Count > 0)
                    {
                        for (int i = 0; i < _Count; i++)
                        {
                            str += lstHour[i].Id + ",";
                        }
                        if (str.EndsWith(","))
                            str = str.Remove(str.Length - 1, 1);
                    }
                }
            }
            return str;
        }

        protected void Rpt_PartTime_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            int integer;
            Label lblWorkDate = e.Item.FindControl("lblWorkDate") as Label;
            Label lblstaffId = e.Item.FindControl("lblstaffId") as Label;
            Label lblWorkTimeId = e.Item.FindControl("lblWorkTimeId") as Label;
            int salonId = Convert.ToInt32(Salon.SelectedValue);
            DateTime wd = Convert.ToDateTime(lblWorkDate.Text);
            int staffId = int.TryParse(lblstaffId.Text, out integer) ? integer : 0;
            int WorkTimeId = int.TryParse(lblWorkTimeId.Text, out integer) ? integer : 0;

            Repeater rptHour = e.Item.FindControl("rptBookHour") as Repeater;
            using (var db = new Solution_30shineEntities())
            {
                var lstHourId = db.BookHours.Where(a => a.IsDelete == 0 && a.Publish == true && a.SalonId == salonId).Select(s => new TempBookHours { Id = s.Id, HourName = s.Hour, HourFrame = (TimeSpan)s.HourFrame, Active = "", PendingHour = "" }).OrderBy(a => a.HourFrame).ToList();

                var obj = db.FlowTimeKeepings.FirstOrDefault(w => w.WorkDate == wd && w.StaffId == staffId && w.SalonId == salonId);
                if (obj != null)
                {
                    if (obj.HourIds != null && obj.HourIds != "")
                    {
                        string[] _ListHourId = obj.HourIds.Split(',');
                        if (_ListHourId.Length > 0)
                        {
                            foreach (var item in lstHourId)
                            {

                                for (int i = 0; i < _ListHourId.Length; i++)
                                {
                                    if (item.Id == Convert.ToInt32(_ListHourId[i]))
                                    {
                                        item.Active = "active";
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                rptHour.DataSource = lstHourId;
                rptHour.DataBind();
            }
        }

        /// <summary>
        /// Chấm công part-time theo giờ
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name="salonId"></param>
        /// <param name="workDate"></param>
        /// <param name="HourId"></param>
        /// <param name="WorkHour"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public static string EditObjFTK(int staffId, int salonId, string workDate, string HourId, string ListHourFrame)
        {
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                string User_permission = HttpContext.Current.Session["User_Permission"].ToString();
                if (User_permission != "checkin" && User_permission != "checkout")
                {
                    int _WorkHour = 0;
                    int _WorkHourTemp = 0;
                    int integer;
                    int exc = 0;
                    int userId = int.TryParse(HttpContext.Current.Session["User_Id"].ToString(), out integer) ? integer : 0;
                    var wd = Library.Format.getDateTimeFromString(workDate);

                    var obj = db.FlowTimeKeepings.FirstOrDefault(w => w.WorkDate == wd && w.StaffId == staffId && w.SalonId == salonId);
                    if (obj != null && obj.WorkTimeId != 5 && obj.WorkTimeId != 6)
                    {
                        // Tìm ra ca làm việc của nhân viên
                        var objWorkTime = db.WorkTimes.FirstOrDefault(a => a.Id == obj.WorkTimeId);
                        // Tìm list gio lam viec trong ca
                        string[] _ListHourFrame = ListHourFrame.Split(',');
                        if (_ListHourFrame.Length > 0)
                        {
                            for (int i = 0; i < _ListHourFrame.Length; i++)
                            {
                                TimeSpan time = TimeSpan.Parse(_ListHourFrame[i]);
                                var a = _ListHourFrame[i];
                                if (time < objWorkTime.StrartTime || time > objWorkTime.EnadTime)
                                {
                                    _WorkHourTemp = _WorkHourTemp + 1;
                                }
                            }
                        }
                        // Tinh gio partime 
                        //_WorkHour =Convert.ToInt32(Math.Round((double)_WorkHourTemp / 2, 0, MidpointRounding.AwayFromZero));
                        _WorkHour = _WorkHourTemp / 2;


                        if (obj != null)
                        {
                            obj.ModifiedDate = DateTime.Now;
                            obj.HourIds = HourId;
                            obj.DoUserId = userId;
                            obj.WorkHour = _WorkHour;
                            db.FlowTimeKeepings.AddOrUpdate(obj);
                            exc = db.SaveChanges();

                            #region //Edit SM_EnrollTemp_Hour
                            UpdateSM_EnrollTemp_Hour(staffId, salonId, workDate, HourId);
                            #endregion

                            // Cập nhật slot statistic
                            var instance = WorkDay_Listing.getInstance();
                            instance.updateSalonSlot(obj.SalonId, obj.WorkDate.Value);
                        }

                        else
                        {
                            Msg.success = false;
                            Msg.msg = "Vui lòng chấm công trước rồi chấm tăng ca! ";
                        }

                        if (exc > 0)
                        {
                            Msg.success = true;
                            // Update flow salary (part-time salary)
                            //SalaryLib.updateFlowSalaryByTimeKeeping(obj, db);
                            var input = new Input_SalaryIncome
                            {
                                staff_id = staffId,
                                work_date = workDate,
                                work_hour = _WorkHour
                            };
                            try
                            {
                                var request = new Request();
                                Task task = Task.Run(async () =>
                                {
                                    await request.RunPostAsync(
                                     UpdateLuongPartTime(input),
                                       new
                                       {
                                           data = input
                                       });
                                });
                                task.Wait(100);
                            }
                            catch (Exception ex)
                            {
                                throw new Exception();
                            }
                        }
                        else
                        {
                            Msg.success = false;
                            Msg.msg = "Chấm công tăng ca thất bại ";
                        }
                    }
                    else
                    {
                        Msg.success = false;
                        Msg.msg = "Ca gãy không được chấm tăng ca ";
                    }
                }
            }
            return serializer.Serialize(Msg);
        }

        /// <summary>
        /// Cập nhật cho bảng SM_Temp...
        /// 1. Thêm mới khung giờ
        /// 2. Hủy các khung giờ bỏ điểm danh part-time
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name="salonId"></param>
        /// <param name="workDate"></param>
        /// <param name="lstHourId"></param>
        public static void UpdateSM_EnrollTemp_Hour(int? staffId, int? salonId, string workDate, string lstHourId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var _workDate = Convert.ToDateTime(workDate, culture);
                var objSM_EnrollFTKP = db.SM_EnrollTemp.FirstOrDefault(w => w.WorkDate == _workDate && w.StaffId == staffId && w.SalonId == salonId);
                if (objSM_EnrollFTKP != null)
                {
                    List<string> listHourIDStr = lstHourId.Split(',').ToList();
                    List<int> listHourID = new List<int>();
                    var listHourIDTemp = db.SM_EnrollTemp_Hour.Where(w => w.EnrollId == objSM_EnrollFTKP.Id && w.IsDelete != true).ToList();
                    var enrollHour = new SM_EnrollTemp_Hour();
                    var index = -1;
                    int integer;

                    if (listHourIDStr.Count > 0)
                    {
                        foreach (var v in listHourIDStr)
                        {
                            if (int.TryParse(v, out integer))
                            {
                                listHourID.Add(integer);
                            }
                        }
                    }

                    // 1. Them mới khung giờ
                    // Insert ID khung giờ vào bảng SM_EnrollTemp_Hour
                    if (listHourID.Count > 0)
                    {
                        foreach (var v in listHourID)
                        {
                            index = listHourIDTemp.FindIndex(w => w.HourId == v);
                            if (index == -1)
                            {
                                enrollHour = new SM_EnrollTemp_Hour();
                                enrollHour.EnrollId = objSM_EnrollFTKP.Id;
                                enrollHour.HourId = v;
                                enrollHour.CreatedTime = DateTime.Now;
                                enrollHour.IsDelete = false;

                                db.SM_EnrollTemp_Hour.AddOrUpdate(enrollHour);
                                db.SaveChanges();
                            }
                        }
                    }

                    // 2. Hủy các khung giờ bỏ điểm danh part-time
                    if (listHourIDTemp.Count > 0)
                    {
                        foreach (var v in listHourIDTemp)
                        {
                            index = listHourID.FindIndex(w => w == v.HourId);
                            if (index == -1)
                            {
                                v.IsDelete = true;
                                v.ModifiedTime = DateTime.Now;
                                db.SM_EnrollTemp_Hour.AddOrUpdate(v);
                                db.SaveChanges();
                            }
                        }
                    }
                }

            }
        }

        /// <summary>
        /// Lấy tổng số khung giờ làm của Stylist tại salon theo ngày làm việc
        /// </summary>
        /// <param name="salonID"></param>
        /// <param name="workDate"></param>
        /// <returns></returns>
        private int getCountHourIDs(int salonID, DateTime workDate)
        {
            try
            {
                var db = Library.Model.getProjectModel();
                var workDateString = Library.Format.getDateTimeString(workDate.Date);

                return db.Database.SqlQuery<int>(@"
                        declare @workDate date,
		                        @salonID int;
                        set @workDate = '" + workDateString + @"';
                        set @salonID = " + salonID + @";

                        select Count(*) from
                        (
	                        SELECT SalonId, StylistId, WorkDate, WorkTimeId,
		                        LTRIM(RTRIM(m.n.value('.[1]','varchar(1000)'))) AS HourId
	                        FROM
	                        (
		                        SELECT ft.SalonId,ft.StaffId as StylistId,ft.WorkDate,ft.WorkTimeId,CAST('<XMLRoot><RowData>' + REPLACE(ft.HourIds,',','</RowData><RowData>') + '</RowData></XMLRoot>' AS XML) AS x
		                        FROM FlowTimeKeeping as ft
		                        inner join Staff as stylist on stylist.Id = ft.StaffId
		                        where ft.IsDelete = 0 and ft.IsEnroll = 1
			                        and ft.WorkDate = @workDate and ft.SalonId = @salonID
			                        and stylist.[Type] = 1
	                        )t
	                        CROSS APPLY x.nodes('/XMLRoot/RowData')m(n)
                        ) as a").FirstOrDefault();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Lấy giá trị cấu hình tỷ lệ nguồn lực theo salon
        /// </summary>
        /// <param name="salonID"></param>
        /// <returns></returns>
        private float getPercentSlot(int salonID)
        {
            try
            {
                var db = Library.Model.getProjectModel();
                float percent = 0;
                var salonRecord = db.Tbl_Salon.FirstOrDefault();
                if (salonRecord != null && salonRecord.PercentSlot != null)
                {
                    percent = (float)salonRecord.PercentSlot.Value;
                }
                else
                {
                    percent = 0;
                }
                return percent;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Lấy tổng slot có thể phục vụ tại salon theo ngày
        /// </summary>
        /// <param name="salonID"></param>
        /// <param name="workDate"></param>
        /// <returns></returns>
        private int getSlotByFormula(int salonID, DateTime workDate)
        {
            try
            {
                return (int)Math.Round(this.getCountHourIDs(salonID, workDate) * this.getPercentSlot(salonID) / 100);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Cập nhật số slot có thể phục vụ tại salon vào bảng statistic [Booking_Statistic_Slot]
        /// </summary>
        /// <param name="salonID"></param>
        /// <param name="workDate"></param>
        public void updateSalonSlot(int salonID, DateTime workDate)
        {
            try
            {
                this.db = Library.Model.getProjectModel();

                var bookingSlot = this.db.Booking_Statistic_Slot.FirstOrDefault(w => w.SalonID == salonID && w.WorkDate == workDate.Date);
                if (bookingSlot != null)
                {
                    bookingSlot.PercentSlot = this.getPercentSlot(salonID);
                    bookingSlot.Slot = this.getSlotByFormula(salonID, workDate);
                    bookingSlot.ModifiedTime = DateTime.Now;
                    this.db.Booking_Statistic_Slot.AddOrUpdate(bookingSlot);
                    this.db.SaveChanges();
                }
                else
                {
                    this.insertBST(salonID, this.getPercentSlot(salonID), this.getSlotByFormula(salonID, workDate), workDate);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Insert bản ghi [Booking_Statistic_Slot]
        /// </summary>
        /// <param name="salonID"></param>
        /// <param name="percentSlot"></param>
        /// <param name="slot"></param>
        /// <param name="workDate"></param>
        public void insertBST(int salonID, float percentSlot, int slot, DateTime workDate)
        {
            try
            {
                this.db = Library.Model.getProjectModel();
                var record = new Booking_Statistic_Slot();
                record.SalonID = salonID;
                record.PercentSlot = percentSlot;
                record.Slot = slot;
                record.WorkDate = workDate.Date;
                record.IsDelete = false;
                record.CreatedTime = DateTime.Now;
                this.db.Booking_Statistic_Slot.Add(record);
                this.db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Insert bản ghi thống kê booking cho salon vào bảng [Booking_Statistic_Slot]
        /// </summary>
        /// <param name="staffID"></param>
        /// <param name="salonID"></param>
        /// <param name="workDate"></param>
        [WebMethod]
        public static void InsertToSlotTable(int staffID, int salonID, string workDate)
        {
            try
            {
                Solution_30shineEntities db = new Solution_30shineEntities();
                Booking_Statistic_Slot _bss = new Booking_Statistic_Slot();
                //var workDateDT = new DateTime();
                var date = Library.Format.getDateTimeFromString(workDate).Value.Date;

                //get percent slot from tbl_salon

                var percentSlot = db.Tbl_Salon.Where(sl => sl.Id == salonID).FirstOrDefault();
                int _percentSlot = (int)percentSlot.PercentSlot;
                //count slot
                var slot = db.FlowTimeKeepings.Where(tk => tk.StaffId == staffID && tk.SalonId == salonID && tk.WorkDate == date).FirstOrDefault();
                int _slot = (int)slot.HourIds.Length - (int)slot.HourIds.Replace(",", "").Length + 1;

                int totalSlot = _slot * _percentSlot / 100;

                //get data from Booking_Statistic_Slot
                var getData = db.Booking_Statistic_Slot.Where(bss => bss.SalonID == salonID && bss.WorkDate == date).FirstOrDefault();
                if (getData == null)
                {
                    _bss.SalonID = salonID;
                    _bss.WorkDate = date;
                    _bss.Slot = totalSlot;
                    _bss.PercentSlot = _percentSlot;
                    _bss.CreatedTime = DateTime.Now;
                    _bss.IsDelete = false;
                    db.Booking_Statistic_Slot.Add(_bss);
                    db.SaveChanges();
                }
                else
                {
                    getData.Slot = getData.Slot + totalSlot;
                    db.Booking_Statistic_Slot.AddOrUpdate(getData);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Cập nhật hủy số slot chấm công của nhân viên cho salon trong bảng [Booking_Statistic_Slot]
        /// </summary>
        /// <param name="staffID"></param>
        /// <param name="salonID"></param>
        /// <param name="workDate"></param>
        [WebMethod]
        public static void DeleteToSlotTable(int staffID, int salonID, string workDate)
        {
            try
            {
                Solution_30shineEntities db = new Solution_30shineEntities();
                Booking_Statistic_Slot _bss = new Booking_Statistic_Slot();
                var date = Library.Format.getDateTimeFromString(workDate).Value.Date;
                //get percent slot from tbl_salon
                var percentSlot = db.Tbl_Salon.Where(sl => sl.Id == salonID).FirstOrDefault();
                int _percentSlot = (int)percentSlot.PercentSlot;
                //count slot
                var slot = db.FlowTimeKeepings.Where(tk => tk.StaffId == staffID && tk.SalonId == salonID && tk.WorkDate == date).FirstOrDefault();
                int _slot = (int)slot.HourIds.Length - (int)slot.HourIds.Replace(",", "").Length + 1;

                int totalSlot = _slot * _percentSlot / 100;

                //get data from Booking_Statistic_Slot
                var getData = db.Booking_Statistic_Slot.Where(bss => bss.SalonID == salonID && bss.WorkDate == date).FirstOrDefault();

                getData.Slot = getData.Slot - totalSlot;
                db.Booking_Statistic_Slot.AddOrUpdate(getData);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Cập nhật lại số slot cho salon trường hợp update khung giờ, trong bảng [Booking_Statistic_Slot]
        /// </summary>
        /// <param name="staffID"></param>
        /// <param name="salonID"></param>
        /// <param name="listHourId"></param>
        /// <param name="workDate"></param>
        [WebMethod]
        public static void UpdateParttimeToSlotTable(int staffID, int salonID, string listHourId, string workDate)
        {
            try
            {
                var date = Library.Format.getDateTimeFromString(workDate).Value.Date;
                Solution_30shineEntities db = new Solution_30shineEntities();
                Booking_Statistic_Slot _bss = new Booking_Statistic_Slot();

                //get list hour id
                string l = listHourId.ToString();
                string bb = l.Replace(",", "");
                int lenght = l.Length - bb.Length + 1;

                //get percent slot from tbl_salon
                var percentSlot = db.Tbl_Salon.Where(sl => sl.Id == salonID).FirstOrDefault();
                int _percentSlot = (int)percentSlot.PercentSlot;

                //count slot
                var slot = db.FlowTimeKeepings.Where(tk => tk.StaffId == staffID && tk.SalonId == salonID && tk.WorkDate == date).FirstOrDefault();
                var slt = slot.HourIds;
                var slt_replace = slt.Replace(",", "");
                var _slot = slt.Length - slt_replace.Length + 1;

                //get data from Booking_Statistic_Slot
                var getData = db.Booking_Statistic_Slot.Where(bss => bss.SalonID == salonID && bss.WorkDate == date).FirstOrDefault();
                if (lenght >= _slot)
                {
                    getData.Slot = getData.Slot + (lenght - _slot) * _percentSlot / 100;
                }
                else
                {
                    getData.Slot = getData.Slot - (_slot - lenght) * _percentSlot / 100;
                }
                db.Booking_Statistic_Slot.AddOrUpdate(getData);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        protected void PermissionCheckinAndcheckout()
        {
            string User_Permission = HttpContext.Current.Session["User_Permission"].ToString();
            if (User_Permission == "checkin" || User_Permission == "checkout")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script permission", "PermissionCheckinAndcheckout();", true);
            }
        }

        protected void ListStaff_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetListStaff();
        }

        protected void Salon_SelectedIndexChanged(object sender, EventArgs e)
        {
            Bind_SalonKhac();
            GetListStaff();
        }
        public static void WriteLog(string logContent, string methodName)
        {
            //get filename
            string currentDate = DateTime.Now.Day.ToString() + DateTime.Now.Month.ToString() + DateTime.Now.Year.ToString();
            string strFileName = AppDomain.CurrentDomain.BaseDirectory + currentDate + "Log.txt";
            string strLogString;
            strLogString = "\r\n";
            strLogString += "===========  " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString() + "  " + methodName + "  ==============" + "\r\n";
            strLogString += logContent + "\r\n";
            strLogString += "===================================================================";
            try
            {
                System.Text.Encoding charset = System.Text.Encoding.GetEncoding(65001); //"UTF-8"
                if (!File.Exists(strFileName))
                {
                    FileStream oFile;
                    oFile = File.Create(strFileName);
                    StreamWriter oReader = new StreamWriter(oFile, charset);
                    oReader.WriteLine(strLogString);
                    oReader.Close();
                    oFile.Close();
                }
                else
                {
                    // Append text in file when file exitsed
                    FileStream oFile1 = new FileStream(strFileName, FileMode.Open, FileAccess.Read, FileShare.Read);
                    if (oFile1.Length > 1048576)
                    {
                        oFile1.Close();
                        File.Delete(strFileName);
                    }
                    else
                    {
                        oFile1.Close();
                    }
                    StreamWriter oReader = new StreamWriter(strFileName, true, charset);
                    oReader.WriteLine(strLogString);
                    oReader.Close();
                }
            }
            catch (Exception ex)
            {
            }
        }
    }


    public class TempBookHours
    {
        public int Id { get; set; }
        public string HourName { get; set; }
        public TimeSpan HourFrame { get; set; }
        public string Active { get; set; }
        public string PendingHour { get; set; }
    }

    public class cls_timekeeping : Staff
    {
        public DateTime? WorkDate { get; set; }
        public bool? IsEnroll { get; set; }
        public int? WorkHour { get; set; }
        public int? WorkTimeId { get; set; }
        public string Color { get; set; }
        //public string BHourName { get; set; }
        public string HourIds { get; set; }
        public string showHourFrame { get; set; }
        public string NoteStatus { get; set; }
        public string NoteStatusSalonCurrent { get; set; }
        public int Parttime { get; set; }
        public string SalonName { get; set; }
        public string ShortName { get; set; }
        public int SalonIdWorking { get; set; }
    }

    public class cls_staffEnroll
    {
        public int id { get; set; }
        public int salonid { get; set; }
    }
    public class thongke
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int? today { get; set; }
        public int? Tomorrow { get; set; }
        public int? AfterTomorrow { get; set; }
    }
    public class cls_stylist_skinner
    {
        public bool stylist { get; set; }
        public bool skinner { get; set; }
    }
    public class cls_configPartTime
    {
        public bool? IsPartTimeHour { get; set; }
        public bool? IsPartTimeRating { get; set; }
    }

    /// <summary>
    /// author: dungnm
    /// </summary>
    public class ResponseData
    {
        public int status { get; set; }
        public string message { get; set; }
        public dynamic data { get; set; }
        public ResponseData()
        {
            data = null;
            status = 0;
            message = "";
        }
    }
    /// <summary>
    /// Input salary add
    /// author: dungnm
    /// </summary>
    public class Input_SalaryIncome
    {
        public string work_date { get; set; }
        public int? salon_id { get; set; }
        public int? staff_id { get; set; }
        public int? work_hour { get; set; }
    }
}