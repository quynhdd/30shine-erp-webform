﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Script.Serialization;
using ExportToExcel;
using Excel;
using LinqKit;
using System.Web.Services;
using System.Data.Entity.Migrations;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.TimeKeeping
{
    public partial class WorkDay_Report : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        protected List<List<ProductBasic>> ProductList;
        private Expression<Func<BillService, bool>> Where = PredicateBuilder.True<BillService>();
        public static CultureInfo culture = new CultureInfo("vi-VN");
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        protected bool IsAccountant = false;
        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
        protected string table = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
                TxtDateTimeTo.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_ViewAllData);
                Bind_StaffType();
                Bind_Data();
            }
        }

        public void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }

        private void Bind_Data()
        {
            using (var db = new Solution_30shineEntities())
            {
                int salonId = int.TryParse(Salon.SelectedValue, out var integer) ? integer : 0;
                int staffType = int.TryParse(StaffType.SelectedValue, out integer) ? integer : 0;
                DateTime TimeFrom = new DateTime();
                DateTime TimeTo = new DateTime();
                if (TxtDateTimeFrom.Text != "")
                {
                    TimeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, new CultureInfo("vi-VN"));
                    if (TxtDateTimeTo.Text != "")
                    {
                        TimeTo = Convert.ToDateTime(TxtDateTimeTo.Text, new CultureInfo("vi-VN"));
                    }
                    else
                    {
                        TimeTo = TimeFrom;
                    }
                }
                var sql = $@"SELECT 
	                            s.Id AS StaffId, s.Fullname AS StaffName, t.Name AS DepartmentName, 
	                            COALESCE(tk.WorkDay, 0) AS WorkDay, COALESCE(tk.WorkHour, 0) AS WorkHour
                            FROM Staff_Type t, Staff AS s
                            LEFT JOIN
                            (
                                SELECT fl.StaffId, COUNT(fl.Id) AS WorkDay, SUM(COALESCE(fl.WorkHour, 0)) AS WorkHour
                                FROM FlowTimeKeeping fl
                                WHERE 
		                            fl.WorkDate BETWEEN '{TimeFrom}' AND '{TimeTo}' AND fl.IsEnroll = 1 
		                            AND ((fl.SalonId = {salonId}) OR ({salonId} = 0))
	                            GROUP BY fl.StaffId
                            ) AS tk
                            ON s.Id = tk.StaffId
                            WHERE 
	                            s.IsDelete = 0 AND s.Active = 1 
	                            AND s.[Type] = t.Id AND (s.isAccountLogin != 1 OR s.isAccountLogin IS NULL) 
	                            AND ((s.[Type] = {staffType}) OR ({staffType} = 0)) 
	                            AND ((s.SalonId = {salonId}) OR ({salonId} = 0)) 
                            ORDER BY tk.WorkDay DESC, tk.WorkHour DESC";
                var list = db.Database.SqlQuery<TimekeepingReport>(sql).ToList();
                if (list.Count > 0)
                {
                    Bind_Paging(list.Count);
                    Rpt_PartTime.DataSource = list.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                    Rpt_PartTime.DataBind();
                }
            }
        }

        /// <summary>
        /// Paging
        /// </summary>
        /// <param name="TotalRow"></param>
        protected void Bind_Paging(int TotalRow)
        {
            try
            {
                // init Paging value
                int integer = 1;
                PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
                PAGING._PageNumber = !IsPostBack ? 1 : (int.TryParse(HDF_Page.Value, out integer) ? integer : 1);
                PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
                PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
                PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
                PAGING._Paging = PAGING.Make_Paging();
                RptPaging.DataSource = PAGING._Paging.ListPage;
                RptPaging.DataBind();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        private void Bind_StaffType()
        {
            using (var db = new Solution_30shineEntities())
            {
                var staffType = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Id).ToList();
                var Key = 0;
                StaffType.DataTextField = "Type";
                StaffType.DataValueField = "Id";
                ListItem item = new ListItem("Tất cả", "0");
                StaffType.Items.Insert(0, item);
                foreach (var v in staffType)
                {
                    Key++;
                    item = new ListItem(v.Name, v.Id.ToString());
                    StaffType.Items.Insert(Key, item);
                }
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Data();
            RemoveLoading();
        }


        protected void Btn_SwitchExcel(object sender, EventArgs e)
        {
            if (HDF_ExcelType.Value == "Export")
            {
                //Exc_ExportExcel();
            }
            else if (HDF_ExcelType.Value == "Import")
            {
                //Exc_ImportExcel();
            }

        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        public string GenTime(int minutes)
        {
            string time = "";
            int hour = minutes / 60;
            int minute = minutes % 60;
            if (hour > 0)
            {
                time += hour + "h";
                time += minute + "p";
            }
            else
            {
                if (minute > 0)
                {
                    time += minute + " p";
                }
                else
                {
                    time += "-";
                }
            }
            return time;
        }

        /// <summary>
        /// Nhập điểm danh hàng ngày
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name="salonId"></param>
        /// <param name="workDate"></param>
        /// <param name="isEnroll"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public static string AddEnroll(int staffId, int salonId, string workDate, bool isEnroll)
        {
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                int userId = int.TryParse(HttpContext.Current.Session["User_Id"].ToString(), out integer) ? integer : 0;
                var wd = Convert.ToDateTime(workDate, culture);
                var obj = db.FlowTimeKeepings.FirstOrDefault(w => w.WorkDate == wd && w.StaffId == staffId && w.SalonId == salonId);
                if (obj != null)
                {
                    obj.ModifiedDate = DateTime.Now;
                }
                else
                {
                    obj = new FlowTimeKeeping();
                    obj.StaffId = staffId;
                    obj.SalonId = salonId;
                    obj.DoUserId = userId;
                    obj.WorkDate = wd;
                    obj.CreatedDate = DateTime.Now;
                    obj.IsDelete = 0;
                }
                obj.IsEnroll = isEnroll;
                db.FlowTimeKeepings.AddOrUpdate(obj);
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        /// <summary>
        /// Nhập giờ làm part-time hàng ngày
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name="salonId"></param>
        /// <param name="workDate"></param>
        /// <param name="isEnroll"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public static string AddPartTime(int staffId, int salonId, string workDate, int workHour)
        {
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                int userId = int.TryParse(HttpContext.Current.Session["User_Id"].ToString(), out integer) ? integer : 0;
                var wd = Convert.ToDateTime(workDate, culture);
                var obj = db.FlowTimeKeepings.FirstOrDefault(w => w.WorkDate == wd && w.StaffId == staffId && w.SalonId == salonId);
                if (obj != null)
                {
                    obj.ModifiedDate = DateTime.Now;
                }
                else
                {
                    obj = new FlowTimeKeeping();
                    obj.StaffId = staffId;
                    obj.SalonId = salonId;
                    obj.DoUserId = userId;
                    obj.CreatedDate = DateTime.Now;
                    obj.WorkDate = wd;
                    obj.IsDelete = 0;
                }
                obj.WorkHour = workHour;
                db.FlowTimeKeepings.AddOrUpdate(obj);
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }


        [WebMethod(EnableSession = true)]
        public static string AddEnrollList(string listData, string workDate, bool isEnroll)
        {
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            if (listData != "[]")
            {
                var list = serializer.Deserialize<List<cls_staffEnroll>>(listData);
                if (list.Count > 0)
                {
                    using (var db = new Solution_30shineEntities())
                    {
                        int integer;
                        int error = 0;
                        int userId = int.TryParse(HttpContext.Current.Session["User_Id"].ToString(), out integer) ? integer : 0;
                        var wd = Convert.ToDateTime(workDate, culture);

                        foreach (var v in list)
                        {
                            var obj = db.FlowTimeKeepings.FirstOrDefault(w => w.WorkDate == wd && w.StaffId == v.id && w.SalonId == v.salonid);
                            if (obj != null)
                            {
                                obj.ModifiedDate = DateTime.Now;
                            }
                            else
                            {
                                obj = new FlowTimeKeeping();
                                obj.StaffId = v.id;
                                obj.SalonId = v.salonid;
                                obj.DoUserId = userId;
                                obj.CreatedDate = DateTime.Now;
                                obj.WorkDate = wd;
                                obj.IsDelete = 0;
                            }
                            obj.IsEnroll = isEnroll;
                            db.FlowTimeKeepings.AddOrUpdate(obj);
                            error += db.SaveChanges() > 0 ? 0 : 1;
                        }

                        if (error == 0)
                        {
                            Msg.success = true;
                        }
                        else
                        {
                            Msg.success = false;
                        }
                    }
                }
            }
            return serializer.Serialize(Msg);
        }

        [WebMethod]
        public static object BindTimekeepingStaff(int Staff_ID, string Fromdate, string Todate)
        {
            using (var db = new Solution_30shineEntities())
            {
                var msg = new Msg();
                DateTime _FromTo = Convert.ToDateTime(Fromdate, culture);
                DateTime _Todate = Convert.ToDateTime(Todate, culture);

                var Lst = db.Strore_Bind_TimeKeeping_Staff(_FromTo, _Todate, Staff_ID).ToList();
                if (Lst.Count > 0)
                {
                    msg.data = Lst;
                    msg.success = true;
                    msg.msg = "success";
                }
                else
                {
                    msg.success = false;
                    msg.msg = "not data";
                }

                return msg;
            }

        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "salonmanager" };
                string[] AllowAllData = new string[] { "root", "admin" };
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }
                if (Array.IndexOf(AllowAllData, Permission) != -1)
                {
                    Perm_ViewAllData = true;
                }
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }
    }

    public class TimekeepingReport
    {
        public int WorkDay { get; set; }
        public int WorkHour { get; set; }
        public string DepartmentName { get; set; }
        public int StaffId { get; set; }
        public string StaffName { get; set; }
    }
}