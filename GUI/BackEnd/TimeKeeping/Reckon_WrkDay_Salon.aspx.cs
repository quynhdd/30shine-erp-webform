﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Script.Serialization;
using ExportToExcel;
using Excel;
using LinqKit;
using System.Web.Services;
using System.Data.Entity.Migrations;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.TimeKeeping
{
    public partial class Reckon_WrkDay_Salon : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        protected List<List<ProductBasic>> ProductList;
        private Expression<Func<BillService, bool>> Where = PredicateBuilder.True<BillService>();
        public static CultureInfo culture = new CultureInfo("vi-VN");
        protected string Permission = "";

        private string PageID = "CC_SL";
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDate = false;
        private bool Perm_Access = false;
        protected List<clsWorkDaySalon> OBJ;
        protected bool IsAccountant = false;
        List<_30shine.MODEL.ENTITY.EDMX.Staff> _LstNhanVien;
        protected string Day1 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-2));
        protected string Day2 = string.Format("{0:dd/MM/yyyy}", DateTime.Now.AddDays(-1));
        protected string Day3 = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
        protected string ngay1;
        protected string ngay2;
        protected string ngay3;
        protected string table = "";

        private string sql = "";
        private string where = "";
        private DateTime timeFrom = new DateTime();
        private DateTime timeTo = new DateTime();
        private int salonId = 0;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //Perm_ViewAllDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDate", PageID, permission);
                //Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_ViewAllDate = permissionModel.CheckPermisionByAction("Perm_ViewAllDate", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {

            SetPermission();

            if (!IsPostBack)
            {
                PermissionCheckinAndcheckout();
                TxtDateTimeFrom.Text = Day3;
                Library.Function.bindSalon_NotHoiQuan(new List<DropDownList> { Salon }, Perm_ViewAllData);

                //Bind_Paging();

                getCountSatffbyWorkdate();


            }
            else
            {
                getCountSatffbyWorkdate();

            }
        }

        protected void FillLstStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                _LstNhanVien = db.Staffs.ToList();
            }
        }
    
        protected void _BtnClick(object sender, EventArgs e)
        {

            //Bind_Paging();

            getCountSatffbyWorkdate();
            RemoveLoading();

        }
        //thong ke
        private void getCountSatffbyWorkdate()
        {

            int integer;

            int salonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
            var timeFrom = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
            ngay1 = string.Format("{0:dd/MM/yyyy}", timeFrom);
            ngay2 = string.Format("{0:dd/MM/yyyy}", timeFrom.AddDays(+1));
            ngay3 = string.Format("{0:dd/MM/yyyy}", timeFrom.AddDays(+2));

            var db = new Solution_30shineEntities();
            var msg = new Msg();

            if (timeFrom != null)
            {
                try
                {
                    //OBJ = db.Store_Get_Count_Satff_By_WorkDate_V3(timeFrom, salonId).ToList();
                    string sql = @"declare
	                                    @WorkDate DateTime,
	                                    @Salon int
	                                    set @WorkDate =  '" + timeFrom + @"';
	                                    set @Salon = " + salonId + @";
                                    BEGIN

                                    with Stylist as
		                                    (
			                                    select  Count(s.Id) as Stylist,s.Type,s.SalonId
			                                    from Staff as s
			                                    where s.Active = 1 and s.IsDelete = 0 and s.isAccountLogin = 0 and s.SalonId is not null and s.Type = 1 and
			                                    ((s.SalonId = @Salon) or (@Salon = 0))
			                                    group by s.Type,s.SalonId
		                                    )
		                                    ,
		                                    Skinner as
		                                    (
			                                    select  Count(s.Id) as Skinner,s.Type,s.SalonId
			                                    from Staff as s
			                                    where s.Active = 1 and s.IsDelete = 0 and s.isAccountLogin = 0 and s.SalonId is not null and s.Type = 2
			                                    and ((s.SalonId = @Salon) or (@Salon = 0))
			                                    group by s.Type,s.SalonId
		                                    ),
		                                    Securitys as
		                                    (
			                                    select  Count(s.Id) as Securitys,s.Type,s.SalonId
			                                    from Staff as s
			                                    where s.Active = 1 and s.IsDelete = 0 and s.isAccountLogin = 0 and s.SalonId is not null and s.Type = 4
			                                    and ((s.SalonId = @Salon) or (@Salon = 0))
			                                    group by s.Type,s.SalonId
		                                    ),
		                                    Checkin as
		                                    (
			                                    select  Count(s.Id) as Checkin,s.Type,s.SalonId
			                                    from Staff as s
			                                    where s.Active = 1 and s.IsDelete = 0 and s.isAccountLogin = 0 and s.SalonId is not null and s.Type = 5
			                                    and ((s.SalonId = @Salon) or (@Salon = 0))
			                                    group by s.Type,s.SalonId
		                                    ),
		                                    Checkout as
		                                    (
			                                    select  Count(s.Id) as Checkout,s.Type,s.SalonId
			                                    from Staff as s
			                                    where s.Active = 1 and s.IsDelete = 0 and s.isAccountLogin = 0 and s.SalonId is not null and s.Type = 6
			                                    and ((s.SalonId = @Salon) or (@Salon = 0))
			                                    group by s.Type,s.SalonId
		                                    ),
		                                    DSM as
		                                    (
			                                    select  Count(s.Id) as DSM,s.Type,s.SalonId
			                                    from Staff as s
			                                    where s.Active = 1 and s.IsDelete = 0 and s.isAccountLogin = 0 and s.SalonId is not null and s.Type = 16
			                                    and ((s.SalonId = @Salon) or (@Salon = 0))
			                                    group by s.Type,s.SalonId
		                                    ),
	                                       todayStylist as
		                                    (
			                                    select COUNT(st.Id) as totalStylistToday, fw.SalonId as SalonId from SM_EnrollTemp as fw
			                                    inner join Staff as st on st.Id=fw.StaffId
			                                    inner join Staff_Type as s_t on st.[Type]=s_t.Id
			                                    inner join Tbl_Salon as salon on fw.SalonId=salon.Id
			                                    where s_t.Id=1 and fw.IsEnroll=1 and fw.IsDelete=0 and fw.WorkDate=@WorkDate and (fw.SalonId = @Salon or (@Salon = 0 and fw.SalonId > 0)) 
	                                            Group by  fw.SalonId

		                                    )
	                                     --select * from todayStylist	end
	                                      ,
		                                    todaySkiner as
		                                    (
			                                     select COUNT(st.Id) as totalSkinerToday, fw.SalonId as SalonId from SM_EnrollTemp as fw
			                                    inner join Staff as st on st.Id=fw.StaffId
			                                    inner join Staff_Type as s_t on st.[Type]=s_t.Id
			                                    inner join Tbl_Salon as salon on fw.SalonId=salon.Id
			                                    where s_t.Id=2 and fw.IsEnroll=1 and fw.IsDelete=0 and fw.WorkDate=@WorkDate and (fw.SalonId = @Salon or (@Salon = 0 and fw.SalonId > 0)) 
			                                    Group by  fw.SalonId
		                                    )
		                                    --select *  from todaySkiner end
		                                    , 
		                                    --ngay mai
		                                    TomorrowStylist as
		                                    (
			                                    select COUNT(st.Id) as totalStylistTomor, fw.SalonId as SalonId from SM_EnrollTemp as fw
			                                    inner join Staff as st on st.Id=fw.StaffId
			                                    inner join Staff_Type as s_t on st.[Type]=s_t.Id
			                                    inner join Tbl_Salon as salon on fw.SalonId=salon.Id
			                                    where fw.IsEnroll=1 and fw.IsDelete=0 and fw.WorkDate=CONVERT(DATE, DATEADD(DAY, +1, @WorkDate)) and s_t.Id=1   and (fw.SalonId = @Salon or (@Salon = 0 and fw.SalonId > 0)) 
			                                    Group by fw.SalonId
		                                    )
		                                    --select *  from TomorrowStylist end
		                                    ,
		                                    TomorrowSkinner as
		                                    (
			                                    select COUNT(st.Id) as totalSkinnerTomor, fw.SalonId as SalonId from SM_EnrollTemp as fw
			                                    inner join Staff as st on st.Id=fw.StaffId
			                                    inner join Staff_Type as s_t on st.[Type]=s_t.Id
			                                    inner join Tbl_Salon as salon on fw.SalonId=salon.Id
			                                    where fw.IsEnroll=1 and fw.IsDelete=0 and fw.WorkDate=CONVERT(DATE, DATEADD(DAY, +1, @WorkDate))   and s_t.Id=2 and (fw.SalonId = @Salon or (@Salon = 0 and fw.SalonId > 0)) 
			                                    Group by fw.SalonId
		                                    )
		                                    --select *  from TomorrowSkinner end
		                                    ,
		                                    ---ngay kia
		                                    AfterTomorrowStylist as
		                                    (
				                                    select COUNT(st.Id) as totalStylistAfterTomor, fw.SalonId as SalonId from SM_EnrollTemp as fw
				                                    inner join Staff as st on st.Id=fw.StaffId
				                                    inner join Staff_Type as s_t on st.[Type]=s_t.Id
				                                    inner join Tbl_Salon as salon on fw.SalonId=salon.Id
				                                    where fw.IsEnroll=1  and s_t.Id=1 and fw.IsDelete=0 and fw.WorkDate=CONVERT(DATE, DATEADD(DAY, +2, @WorkDate))  and (fw.SalonId = @Salon or (@Salon = 0 and fw.SalonId > 0)) 
				                                    Group by  fw.SalonId
		                                    )
		                                    --	select *  from AfterTomorrowStylist end
		                                    ,
		                                    AfterTomorrowSkinner as
		                                    (
				                                    select COUNT(st.Id) as totalSkinnerAfterTomor, fw.SalonId as SalonId from SM_EnrollTemp as fw
				                                    inner join Staff as st on st.Id=fw.StaffId
				                                    inner join Staff_Type as s_t on st.[Type]=s_t.Id
				                                    inner join Tbl_Salon as salon on fw.SalonId=salon.Id
				                                    where fw.IsEnroll=1  and s_t.Id=2 and fw.IsDelete=0 and fw.WorkDate=CONVERT(DATE, DATEADD(DAY, +2, @WorkDate))  and (fw.SalonId = @Salon or (@Salon = 0 and fw.SalonId > 0)) 
				                                    Group by  fw.SalonId
		                                    )
		                                    --select *  from AfterTomorrowStylist end
	                                    select
		                                       ISNULL(s.Stylist,0) as Stylist,
		                                       ISNULL(s1.Skinner,0) as Skinner,
		                                       ISNULL(s2.Securitys,0) as Securitys,
		                                       ISNULL(s3.Checkin,0) as Checkin, 
		                                       ISNULL(s4.Checkout,0) as Checkout, 
		                                       ISNULL(s5.DSM,0) as DSM,
		                                        bgColorStylist = 
			                                    (
			                                    Case
				                                    When (ISNULL(s.Stylist ,0)) > 16 then '#99ffbb'
				                                    When (ISNULL(s.Stylist ,0)) <= 16 then ''
			                                     end
		                                        ),
			                                    bgColorSkinner = 
			                                    (
			                                    Case
				                                    When cast((ISNULL(s.Stylist ,0)) as float) / cast( ISNULL(s1.Skinner,1) as float) > 2 then '#ffb3b3'
				                                    else ''
			                                     end
		                                        ),
			                                    bgColorSecuritys = 
			                                    (
			                                    Case
				                                    When (ISNULL(s2.Securitys ,0)) > 2 then '#99ffbb'
				                                    When (ISNULL(s2.Securitys ,0)) < 2 then '#ffb3b3'
			                                     end
		                                        ),
			                                    bgColorCheckin = 
			                                    (
			                                    Case
				                                    When (ISNULL(s3.Checkin ,0)) > 2 then '#99ffbb'
				                                    When (ISNULL(s3.Checkin ,0)) < 2 then '#ffb3b3'
				                                    else ''
			                                     end
		                                        ),	  
			                                    bgColorCheckout = 
			                                    (
			                                    Case
				                                    When (ISNULL(s4.Checkout ,0)) > 2 then '#99ffbb'
				                                    When (ISNULL(s4.Checkout ,0)) < 2 then '#ffb3b3'
				                                    else ''
			                                     end
		                                        ),
			                                    bgColorDSM = 
			                                    (
			                                    Case
				                                    When (ISNULL(s5.DSM ,0)) = 0 then '#ffb3b3' 
				                                    else ''
			                                     end
		                                        ), 
	                                    salon_chinh.Id,salon_chinh.ShortName, tt.VungMienID,
	                                    IsNULL(tds.totalStylistToday, 0) as totalStylistToday, ISNULL(tsk.totalSkinerToday,0) as totalSkinerToday, ISNULL(tms.totalStylistTomor,0) as totalStylistTomor,ISNULL( tmk.totalSkinnerTomor,0) as totalSkinnerTomor, ISNULL(afSk.totalSkinnerAfterTomor,0) as totalSkinnerAfterTomor, ISNULL(afSt.totalStylistAfterTomor,0) as totalStylistAfterTomor,
	                                    bgColorToday =
	                                    (
	                                    Case
		                                     When cast((ISNULL(totalStylistToday,0)) as float) / cast( ISNULL(totalSkinerToday,1) as float) < 2 then '#ff6666'
		                                      When cast((ISNULL(totalStylistToday ,0)) as float) / cast( ISNULL(totalSkinerToday,1) as float) = 2 then ''
		                                        When cast((ISNULL(totalStylistToday ,0)) as float) / cast( ISNULL(totalSkinerToday,1) as float) > 2 then '#3a9e46'
	                                     end
	                                     ),
	                                     bgColorTomorrow =
	                                     (
	                                    Case
		                                    When cast((ISNULL(totalStylistTomor ,0)) as float) / cast( ISNULL(totalSkinnerTomor,1) as float) < 2 then '#ff6666'
                                            When cast((ISNULL(totalStylistTomor ,0)) as float) / cast( ISNULL(totalSkinnerTomor,1) as float) = 2 then ''
	                                        When cast((ISNULL(totalStylistTomor ,0)) as float) / cast( ISNULL(totalSkinnerTomor,1) as float) > 2 then  '#3a9e46'
	                                     end
	                                     ),
	                                     bgColorAfterTomorrow = 
	                                     (
	                                    Case
		                                    When cast((ISNULL(totalStylistAfterTomor ,0)) as float) / cast( ISNULL(totalSkinnerAfterTomor,1) as float) < 2 then '#ff6666'
		                                    When cast((ISNULL(totalStylistAfterTomor ,0)) as float) / cast( ISNULL(totalSkinnerAfterTomor,1) as float) = 2 then ''
		                                    When cast((ISNULL(totalStylistAfterTomor ,0)) as float) / cast( ISNULL(totalSkinnerAfterTomor,1) as float) > 2 then '#3a9e46'
	                                     end
	                                     )
                                        from SM_EnrollTemp as fw
	                                    inner join Staff as st on st.Id=fw.StaffId
	                                    inner join Staff_Type as st_type on st.[Type]=st_type.Id
	                                    inner join Tbl_Salon as salon_chinh on fw.SalonId=salon_chinh.Id and salon_chinh.IsSalonHoiQuan != 1  and (fw.SalonId = @Salon or (@Salon = 0 and fw.SalonId > 0)) 
	                                    inner join TinhThanh as tt on salon_chinh.CityId = tt.ID
	                                    left join todayStylist as tds on tds.SalonId = fw.SalonId
	                                    left join todaySkiner as tsk on tsk.SalonId = fw.SalonId
	                                    left join TomorrowStylist as tms on tms.SalonId = fw.SalonId
	                                    left join TomorrowSkinner as tmk on tmk.SalonId = fw.SalonId
	                                    left join AfterTomorrowSkinner as afSk on afSk.SalonId = fw.SalonId
	                                    left join AfterTomorrowStylist as afSt on afSt.SalonId = fw.SalonId
	                                    Left join Stylist as s on s.SalonId = fw.SalonId
	                                    Left join Skinner as s1 on s1.SalonId = fw.SalonId
	                                    Left join Securitys as s2 on s2.SalonId = fw.SalonId
	                                    Left join Checkin as s3 on s3.SalonId = fw.SalonId
	                                    Left join Checkout as s4 on s4.SalonId = fw.SalonId
	                                    Left join DSM as s5 on s5.SalonId = fw.SalonId
	                                    where 
                                        fw.IsDelete=0 and fw.IsEnroll=1 and st_type.Id=1 or st_type.Id=2
	                                    group by salon_chinh.Id,salon_chinh.ShortName,salon_chinh.[Order], tt.VungMienID, tds.totalStylistToday, tsk.totalSkinerToday, tms.totalStylistTomor, tmk.totalSkinnerTomor, afSk.totalSkinnerAfterTomor, afSt.totalStylistAfterTomor, s.Stylist,s1.Skinner,s2.Securitys,s3.Checkin,s4.Checkout,s5.DSM
	                                    order by  tt.VungMienID  ,salon_chinh.[Order]
                                    END";
                    OBJ = db.Database.SqlQuery<clsWorkDaySalon>(sql).ToList();
                    msg.success = true;
                    msg.msg = "Hiển thị thành công";

                }
                catch (Exception e)
                { Console.WriteLine(e); }
            }
            else
            {
                msg.success = false;
                msg.msg = "Lỗi!";
            }

        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                var Count = db.Database.SqlQuery<cls_timekeeping>(sql).Count();
                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        protected void Btn_SwitchExcel(object sender, EventArgs e)
        {
            if (HDF_ExcelType.Value == "Export")
            {
                //Exc_ExportExcel();
            }
            else if (HDF_ExcelType.Value == "Import")
            {
                //Exc_ImportExcel();
            }

        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        public string GenTime(int minutes)
        {
            string time = "";
            int hour = minutes / 60;
            int minute = minutes % 60;
            if (hour > 0)
            {
                time += hour + "h";
                time += minute + "p";
            }
            else
            {
                if (minute > 0)
                {
                    time += minute + " p";
                }
                else
                {
                    time += "-";
                }
            }
            return time;
        }

        /// <summary>
        /// Nhập điểm danh hàng ngày
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name="salonId"></param>
        /// <param name="workDate"></param>
        /// <param name="isEnroll"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public static string AddEnroll(int staffId, int salonId, string workDate, bool isEnroll, int workTimeId)
        {
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                string User_permission = HttpContext.Current.Session["User_Permission"].ToString();
                if (User_permission != "checkin" && User_permission != "checkout")
                {
                    string lstHourId = LstHourid(workTimeId, salonId);
                    int integer;
                    int userId = int.TryParse(HttpContext.Current.Session["User_Id"].ToString(), out integer) ? integer : 0;
                    var wd = Convert.ToDateTime(workDate, culture);
                    //Check stylist co chuyen salon hay khong
                    var objErrorFTKP = db.FlowTimeKeepings.FirstOrDefault(w => w.WorkDate == wd && w.StaffId == staffId && w.SalonId != salonId && w.IsEnroll == true);

                    if (objErrorFTKP != null)
                    {
                        objErrorFTKP.IsDelete = 1;
                        objErrorFTKP.IsEnroll = false;
                        db.FlowTimeKeepings.AddOrUpdate(objErrorFTKP);
                        db.SaveChanges();
                    }

                    var obj = db.FlowTimeKeepings.FirstOrDefault(w => w.WorkDate == wd && w.StaffId == staffId && w.SalonId == salonId);
                    if (obj != null)
                    {
                        obj.ModifiedDate = DateTime.Now;
                    }
                    else
                    {
                        obj = new FlowTimeKeeping();
                        obj.StaffId = staffId;
                        obj.SalonId = salonId;
                        obj.DoUserId = userId;
                        obj.WorkDate = wd;
                        obj.CreatedDate = DateTime.Now;
                        obj.IsDelete = 0;
                    }
                    obj.IsEnroll = isEnroll;
                    obj.WorkTimeId = workTimeId;
                    obj.HourIds = lstHourId;
                    db.FlowTimeKeepings.AddOrUpdate(obj);
                    var exc = db.SaveChanges();

                    if (exc > 0)
                    {
                        Msg.success = true;
                    }
                    else
                    {
                        Msg.success = false;
                    }

                    // InsertOrUpdate vào bảng SM_EnrollTemp
                    InsertOrUpdateEnrollTemp(staffId, salonId, workDate, isEnroll, lstHourId);

                    // Update flow salary (fix salary)
                    SalaryLib.Instance.updateFlowSalaryByTimeKeeping(obj);
                }
            }
            return serializer.Serialize(Msg);
        }

        /// <summary>
        /// Nhập giờ làm part-time hàng ngày
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name="salonId"></param>
        /// <param name="workDate"></param>
        /// <param name="isEnroll"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public static string AddPartTime(int staffId, int salonId, string workDate, int workHour)
        {
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                int userId = int.TryParse(HttpContext.Current.Session["User_Id"].ToString(), out integer) ? integer : 0;
                var wd = Convert.ToDateTime(workDate, culture);
                var obj = db.FlowTimeKeepings.FirstOrDefault(w => w.WorkDate == wd && w.StaffId == staffId && w.SalonId == salonId);
                if (obj != null)
                {
                    obj.ModifiedDate = DateTime.Now;
                }
                else
                {
                    obj = new FlowTimeKeeping();
                    obj.StaffId = staffId;
                    obj.SalonId = salonId;
                    obj.DoUserId = userId;
                    obj.CreatedDate = DateTime.Now;
                    obj.WorkDate = wd;
                    obj.IsDelete = 0;
                }
                obj.WorkHour = workHour;
                db.FlowTimeKeepings.AddOrUpdate(obj);
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    Msg.success = true;
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name="salonId"></param>
        /// <param name="workDate"></param>
        /// <param name="isEnroll"></param>
        /// <param name="lstHourId"></param>
        private static void InsertOrUpdateEnrollTemp(int? staffId, int? salonId, string workDate, bool? isEnroll, string lstHourId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var _workDate = Convert.ToDateTime(workDate, culture);
                #region 
                //Check stylist co chuyen salon hay khong
                var objErrorSM_EnrollFTKP = db.SM_EnrollTemp.FirstOrDefault(w => w.WorkDate == _workDate && w.StaffId == staffId && w.SalonId != salonId && w.IsEnroll == true);

                if (objErrorSM_EnrollFTKP != null)
                {
                    objErrorSM_EnrollFTKP.IsDelete = false;
                    objErrorSM_EnrollFTKP.IsEnroll = false;
                    db.SM_EnrollTemp.AddOrUpdate(objErrorSM_EnrollFTKP);
                    db.SaveChanges();
                }
                #endregion
                // Insert or Update bản ghi vào bảng SM_EnrollTemp
                //var enroll = new SM_EnrollTemp();
                //
                var objSM_EnrollFTKP = db.SM_EnrollTemp.FirstOrDefault(w => w.WorkDate == _workDate && w.StaffId == staffId && w.SalonId == salonId);
                //Update
                if (objSM_EnrollFTKP != null)
                {
                    objSM_EnrollFTKP.ModifiedTime = DateTime.Now;
                    //Delete SM_EnrollTemp_Hour khi cham cong lai
                    //db.Store_update_SM_EnrollTemp_Hour(objSM_EnrollFTKP.Id);
                }
                // Insert
                else
                {
                    objSM_EnrollFTKP = new SM_EnrollTemp();
                    objSM_EnrollFTKP.SalonId = salonId;
                    objSM_EnrollFTKP.StaffId = staffId;
                    objSM_EnrollFTKP.CreatedTime = DateTime.Now;
                }
                objSM_EnrollFTKP.IsEnroll = isEnroll;
                if (isEnroll == true)
                {
                    objSM_EnrollFTKP.IsDelete = false;
                }
                else
                {
                    objSM_EnrollFTKP.IsDelete = true;
                }
                objSM_EnrollFTKP.WorkDate = _workDate;
                db.SM_EnrollTemp.AddOrUpdate(objSM_EnrollFTKP);
                db.SaveChanges();

                // Insert ID khung giờ vào bảng SM_EnrollTemp_Hour
                try
                {
                    var enrollHour = new SM_EnrollTemp_Hour();
                    int integer;
                    List<string> listHourID = lstHourId.Split(',').ToList();
                    if (listHourID.Count > 0)
                    {
                        foreach (var v in listHourID)
                        {
                            enrollHour = new SM_EnrollTemp_Hour();
                            enrollHour.EnrollId = objSM_EnrollFTKP.Id;
                            enrollHour.HourId = int.TryParse(v, out integer) ? integer : 0;
                            enrollHour.CreatedTime = DateTime.Now;
                            enrollHour.IsDelete = false;
                            db.SM_EnrollTemp_Hour.AddOrUpdate(enrollHour);
                            db.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {
                    //
                }
            }
        }

        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                if (Perm_Delete == true)
                    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }

        [WebMethod]
        public static string LstHourid(int WorkTimeID, int SalonId)
        {
            string str = "";
            using (var db = new Solution_30shineEntities())
            {
                //Xử lý khung giờ theo ca
                if (WorkTimeID > 0)
                {
                    var objWorkTime = db.WorkTimes.FirstOrDefault(a => a.IsDelete == 0 && a.Publish == true && a.Id == WorkTimeID);

                    string strStratWorkHour = Convert.ToString(objWorkTime.StrartTime);
                    string strEndWorkHour = Convert.ToString(objWorkTime.EnadTime);
                    string strLunchhour = Convert.ToString(objWorkTime.Lunchhour);
                    string strLunchhour2 = Convert.ToString(objWorkTime.Lunchhour2);
                    TimeSpan StartWorkHour = TimeSpan.Parse(strStratWorkHour);
                    TimeSpan EndWorkHour = TimeSpan.Parse(strEndWorkHour);
                    TimeSpan Lunchhour = TimeSpan.Parse(strLunchhour);
                    TimeSpan Lunchhour2 = TimeSpan.Parse(strLunchhour2);

                    var lstHour = db.BookHours.Where(a => a.SalonId == SalonId && a.IsDelete == 0 && a.Publish == true && a.HourFrame >= StartWorkHour && a.HourFrame <= EndWorkHour && a.HourFrame != Lunchhour && a.HourFrame != Lunchhour2).OrderBy(a => a.HourFrame).ToList();
                    int _Count = Convert.ToInt32(lstHour.Count);
                    if (_Count > 0)
                    {
                        for (int i = 0; i < _Count; i++)
                        {
                            str += lstHour[i].Id + ",";
                        }
                        if (str.EndsWith(","))
                            str = str.Remove(str.Length - 1, 1);
                    }
                }
            }
            return str;
        }



        protected void PermissionCheckinAndcheckout()
        {
            string User_Permission = HttpContext.Current.Session["User_Permission"].ToString();
            if (User_Permission == "checkin" || User_Permission == "checkout")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script permission", "PermissionCheckinAndcheckout();", true);
            }
        }
    }
    public partial class clsWorkDaySalon
    {
        public int Stylist { get; set; }
        public int Skinner { get; set; }
        public int Securitys { get; set; }
        public int Checkin { get; set; }
        public int Checkout { get; set; }
        public int DSM { get; set; }
        public string bgColorStylist { get; set; }
        public string bgColorSkinner { get; set; }
        public string bgColorSecuritys { get; set; }
        public string bgColorCheckin { get; set; }
        public string bgColorCheckout { get; set; }
        public string bgColorDSM { get; set; }
        public int Id { get; set; }
        public string ShortName { get; set; }
        public Nullable<int> VungMienID { get; set; }
        public int totalStylistToday { get; set; }
        public int totalSkinerToday { get; set; }
        public int totalStylistTomor { get; set; }
        public int totalSkinnerTomor { get; set; }
        public int totalSkinnerAfterTomor { get; set; }
        public int totalStylistAfterTomor { get; set; }
        public string bgColorToday { get; set; }
        public string bgColorTomorrow { get; set; }
        public string bgColorAfterTomorrow { get; set; }
    }
}