﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Web.Services;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.UIService
{
    public partial class Service_Add : System.Web.UI.Page
    {
        private string PageID = "DV_ADD";
        private bool Perm_Access = false;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                ////Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                ////Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }



        private void ExecuteByPermission()
        {
            using (var db = new Solution_30shineEntities())
            {
                Code.Enabled = false;
                Code.Text = GenCode(db.Services.Count());
            }
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
        }

        protected void AddService(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var obj = new Service();
                int integer;
                byte abyte;
                obj.Name = Name.Text;
                obj.Code = GenCode(db.Services.Count());
                obj.Description = Description.Text;
                obj.Price = int.TryParse(Price.Text, out integer) ? integer : 0;
                obj.VoucherPercent = int.TryParse(VoucherPercent.Text, out integer) ? integer : 0;
                obj.CoefficientRating = byte.TryParse(CoefficientRating.Text, out abyte) ? abyte : Convert.ToByte(0);
                obj.Images = Array.IndexOf(new string[] { "", "[]" }, HDF_Images.Value) == -1 ? HDF_Images.Value : "";
                obj.Videos = Array.IndexOf(new string[] { "", "[]" }, HDF_Videos.Value) == -1 ? HDF_Videos.Value : "";
                obj.Order = int.TryParse(Order.Text, out integer) ? integer : 0;
                obj.IsPrimary = ServicePrimary.Checked;
                obj.ShowOnApp = ShowOnApp.Checked;
                obj.IsDelete = 0;
                if (Publish.Checked == true)
                {
                    obj.Publish = 1;
                }
                else
                {
                    obj.Publish = 0;
                }

                if (Focus.Checked == true)
                {
                    obj.Status = 1;
                }
                else
                {
                    obj.Status = 0;
                }

                if (IsFreeService.Checked == true)
                {
                    obj.IsFreeService = 1;
                }
                else
                {
                    obj.IsFreeService = 0;
                }

                // Validate
                // Check trùng mã dịch vụ
                var check = db.Services.Where(w => w.Code == obj.Code).ToList();
                var Error = false;
                if (check.Count > 0)
                {
                    MsgSystem.Text = "Mã dịch đã tồn tại. Bạn vui lòng nhập mã dịch vụ khác.";
                    MsgSystem.CssClass = "msg-system warning";
                    Error = true;

                }

                if (!Error)
                {
                    db.Services.Add(obj);
                    db.SaveChanges();

                    var countAll = db.Salon_Service.Where(p => p.ServiceId == obj.Id).ToList();
                    if (countAll.Count() == 0)
                    {
                        var listSalon = db.Tbl_Salon.Where(p => p.IsDelete != 1 && p.Publish == true).ToList();
                        foreach (var item in listSalon)
                        {
                            var checkS = db.Salon_Service.Where(p => p.ServiceId == obj.Id && p.SalonId == item.Id).ToList();
                            if (checkS.Count() == 0)
                            {
                                var salonser = new Salon_Service();
                                salonser.SalonId = item.Id;
                                salonser.ServiceId = obj.Id;
                                salonser.Price = obj.Price;
                                salonser.HeSoHL = obj.CoefficientRating;
                                salonser.IsCheck = false;
                                salonser.IsDelete = false;
                                db.Salon_Service.Add(salonser);
                            }
                        }
                        db.SaveChanges();
                    }

                    // Thông báo thành công
                    var MsgParam = new List<KeyValuePair<string, string>>();
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                    UIHelpers.Redirect("/admin/dich-vu/" + obj.Id + ".html", MsgParam);
                }
            }
        }

        public string GenCode(int CodeInt)
        {
            string Code = "";
            int CodeLen = 5;
            try
            {
                Code = (CodeInt + 1).ToString();
                if (Code.Length < CodeLen)
                {
                    int loop = CodeLen - Code.Length;
                    for (int i = 0; i < loop; i++)
                    {
                        Code = "0" + Code;
                    }
                }
                return "SP" + Code;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        
    }
}