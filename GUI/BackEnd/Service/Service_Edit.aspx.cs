﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Script.Serialization;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.UIService
{
    public partial class Service_Edit : System.Web.UI.Page
    {
        private string PageID = "DV_EDIT";
        protected Service OBJ;
        private bool Perm_Access = false;
        protected List<cls_media> listImg = new List<cls_media>();
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                ////Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                ////Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }



        private void ExecuteByPermission()
        {

            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                Bind_OBJ();
            }
        }

        private bool Bind_OBJ()
        {
            var ExitsOBJ = true;
            int integer;
            var _Code = int.TryParse(Request.QueryString["Code"], out integer) ? integer : 0;
            var serialize = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                OBJ = db.Services.Where(w => w.Id == _Code).FirstOrDefault();
                if (!OBJ.Equals(null))
                {
                    Name.Text = OBJ.Name;
                    Code.Text = OBJ.Code;
                    Description.Text = OBJ.Description;
                    Price.Text = OBJ.Price.ToString();
                    VoucherPercent.Text = OBJ.VoucherPercent.ToString();
                    CoefficientRating.Text = OBJ.CoefficientRating.ToString();
                    if (OBJ.Order != null)
                    {
                        Order.Text = OBJ.Order.ToString();
                    }

                    if (OBJ.Images != null && OBJ.Images != "")
                    {
                        listImg = serialize.Deserialize<List<cls_media>>(OBJ.Images).ToList();
                    }
                    if (OBJ.Videos != null && OBJ.Videos != "")
                    {
                        var videos = serialize.Deserialize<List<cls_media>>(OBJ.Videos).ToList();
                        if (videos.Count > 0)
                        {
                            VideoLink.Text = videos[0].url;
                            VideoTitle.Text = videos[0].title;
                            VideoDescription.Text = videos[0].description;
                        }
                    }

                    if (OBJ.IsPrimary == true)
                    {
                        ServicePrimary.Checked = true;
                    }
                    else
                    {
                        ServicePrimary.Checked = false;
                    }

                    if (OBJ.ShowOnApp == true)
                    {
                        ShowOnApp.Checked = true;
                    }
                    else
                    {
                        ShowOnApp.Checked = false;
                    }

                    if (OBJ.Publish == 1)
                    {
                        Publish.Checked = true;
                    }
                    else
                    {
                        Publish.Checked = false;
                    }

                    if (OBJ.Status == 1)
                    {
                        Focus.Checked = true;
                    }
                    else
                    {
                        Focus.Checked = false;
                    }

                    if (OBJ.IsFreeService == 1)
                    {
                        IsFreeService.Checked = true;
                    }
                    else
                    {
                        IsFreeService.Checked = false;
                    }
                }
                else
                {
                    ExitsOBJ = false;
                    MsgSystem.Text = "Dịch vụ không tồn tại!";
                    MsgSystem.CssClass = "msg-system warning";
                }
                return ExitsOBJ;
            }
        }

        protected void Update_OBJ(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                int integer;
                byte abyte;
                var _Code = int.TryParse(Request.QueryString["Code"], out integer) ? integer : 0;
                var obj = db.Services.Where(w => w.Id == _Code).FirstOrDefault();

                if (!obj.Equals(null))
                {
                    obj.Name = Name.Text;
                    obj.Code = Code.Text;
                    obj.Description = Description.Text;
                    obj.Price = int.TryParse(Price.Text, out integer) ? integer : 0;
                    obj.VoucherPercent = int.TryParse(VoucherPercent.Text, out integer) ? integer : 0;
                    obj.CoefficientRating = byte.TryParse(CoefficientRating.Text, out abyte) ? abyte : Convert.ToByte(0);
                    obj.Images = Array.IndexOf(new string[] { "", "[]" }, HDF_Images.Value) == -1 ? HDF_Images.Value : "";
                    obj.Videos = Array.IndexOf(new string[] { "", "[]" }, HDF_Videos.Value) == -1 ? HDF_Videos.Value : "";
                    obj.Order = int.TryParse(Order.Text, out integer) ? integer : 0;
                    obj.IsPrimary = ServicePrimary.Checked;
                    obj.ShowOnApp = ShowOnApp.Checked;
                    if (Publish.Checked == true)
                    {
                        obj.Publish = 1;
                    }
                    else
                    {
                        obj.Publish = 0;
                    }

                    if (Focus.Checked == true)
                    {
                        obj.Status = 1;
                    }
                    else
                    {
                        obj.Status = 0;
                    }

                    if (IsFreeService.Checked == true)
                    {
                        obj.IsFreeService = 1;
                    }
                    else
                    {
                        obj.IsFreeService = 0;
                    }

                    obj.ModifiedDate = DateTime.Now;

                    db.Services.AddOrUpdate(obj);
                    var exc = db.SaveChanges();
                    if (exc > 0)
                    {
                        var countAll = db.Salon_Service.Where(p => p.ServiceId == obj.Id).ToList();
                        if (countAll.Count() > 0)
                        {
                            var listSalon = db.Tbl_Salon.Where(p => p.IsDelete != 1 && p.Publish == true).ToList();
                            foreach (var item in listSalon)
                            {
                                var salonser = db.Salon_Service.FirstOrDefault(p => p.SalonId == item.Id && p.ServiceId == obj.Id);
                                if (salonser != null)
                                {
                                    salonser.SalonId = item.Id;
                                    salonser.ServiceId = obj.Id;
                                    salonser.Price = obj.Price;
                                    salonser.HeSoHL = obj.CoefficientRating;
                                    if (salonser.IsCheck != null && salonser.IsCheck == false)
                                        salonser.IsCheck = false;
                                    salonser.IsDelete = false;
                                    db.Salon_Service.AddOrUpdate(salonser);
                                }
                            }
                            db.SaveChanges();
                        }

                        // Thông báo thành công
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                        UIHelpers.Redirect("/admin/dich-vu/" + obj.Id + ".html", MsgParam);
                    }
                    else
                    {
                        MsgSystem.Text = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                        MsgSystem.CssClass = "msg-system warning";
                    }
                }
                else
                {
                    MsgSystem.Text = "Lỗi! Không tồn tại dịch vụ.";
                    MsgSystem.CssClass = "msg-system warning";
                }
            }
        }

        

        public class cls_media
        {
            public string url { get; set; }
            public string thumb { get; set; }
            public string title { get; set; }
            public string description { get; set; }
        }
    }
}