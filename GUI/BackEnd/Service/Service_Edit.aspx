﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Service_Edit.aspx.cs" Inherits="_30shine.GUI.BackEnd.UIService.Service_Edit"  MasterPageFile="~/TemplateMaster/SiteMaster.Master"%>

<asp:Content ID="ServiceAdd" ContentPlaceHolderID="CtMain" runat="server">
<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

<style>
    .customer-add .content-wp{padding: 0 15%;}
</style>

<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">
                <li>Quản lý Dịch vụ &nbsp;&#187; </li>
                <li class="li-listing"><a href="/admin/dich-vu/danh-sach.html">Danh sách</a></li>
                <li class="li-add"><a href="/admin/dich-vu/them-moi.html">Thêm mới</a></li>
                <li class="li-edit"><a href="javascript://">Chỉnh sửa</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add admin-product-add admin-service-add">
    <%-- Add --%>                
    <div class="wp960 content-wp">
        <!-- System Message -->
        <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
        <!-- END System Message -->

        <div class="table-wp">
            <table class="table-add admin-product-table-add admin-service-table-add">
                <tbody>
                    <tr class="title-head">
                        <td><strong>Thông tin dịch vụ</strong></td>
                        <td></td>
                    </tr>
                    <tr class="tr-margin" style="height: 20px;"></tr>
                    <tr>
                        <td class="col-xs-2 left"><span>Tên dịch vụ</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="Name" runat="server" ClientIDMode="Static"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="NameValidate" ControlToValidate="Name" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập tên dịch vụ!"></asp:RequiredFieldValidator>
                            </span>
                                            
                        </td>
                    </tr>

                    <tr>
                        <td class="col-xs-2 left"><span>Mã dịch vụ</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="Code" runat="server" ClientIDMode="Static" ReadOnly="true" placeholder="Ví dụ : DV01"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="CodeValidate" ControlToValidate="Code" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập mã dịch vụ!"></asp:RequiredFieldValidator>--%>
                            </span>
                        </td>
                    </tr>

                    <tr class="tr-description">
                        <td class="col-xs-2 left"><span>Mô tả</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox TextMode="MultiLine" Rows="5" ID="Description" runat="server" ClientIDMode="Static"></asp:TextBox>
                            </span>
                        </td>
                    </tr>

                    <tr class="tr-field-ahalf">
                        <td class="col-xs-2 left"><span>Giá</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="Price" runat="server" ClientIDMode="Static" placeholder="Ví dụ : 1000000"></asp:TextBox>&nbsp;&nbsp;VNĐ
                                <asp:RequiredFieldValidator ID="ValidatePrice" ControlToValidate="Price" CssClass="fb-cover-error" Text="Bạn chưa nhập giá!" runat="server"></asp:RequiredFieldValidator>
                            </span>
                        </td>
                    </tr>

                    <tr class="tr-field-ahalf">
                        <td class="col-xs-2 left"><span>Khuyến mại giảm giá</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="VoucherPercent" runat="server" ClientIDMode="Static" placeholder="Ví dụ : 5"></asp:TextBox>&nbsp;&nbsp;( % )
                            </span>
                        </td>
                    </tr>

                    <tr class="tr-field-ahalf">
                        <td class="col-xs-2 left"><span>Hệ số điểm hài lòng</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="CoefficientRating" runat="server" ClientIDMode="Static" placeholder="Ví dụ : 1"></asp:TextBox>
                            </span>
                        </td>
                    </tr>

                    <tr class="tr-product-category">
                        <td class="col-xs-2 left"><span>Nổi bật</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:CheckBox ID="Focus" Checked="true" runat="server" />
                            </span>
                        </td>
                    </tr>

                    <tr class="tr-product-category">
                        <td class="col-xs-2 left"><span>Phụ trợ ( Miễn phí )</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:CheckBox ID="IsFreeService" Checked="true" runat="server" />
                            </span>
                        </td>
                    </tr>

                    <tr class="tr-product-category">
                        <td class="col-xs-2 left"><span>Publish</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:CheckBox ID="Publish" Checked="true" runat="server" />
                            </span>
                        </td>
                    </tr>

                    <tr>
                        <td></td>
                        <td style="text-transform: uppercase;">Cấu hình cho mobile app</td>
                    </tr>

                    <tr class="tr-upload">
                        <td class="col-xs-3 left"><span>Ảnh</span></td>
                        <td class="col-xs-7 right">
                            <div class="wrap btn-upload-wp HDF_Images">
                                <div id="Btn_UploadImg" class="btn-upload-img" onclick="popupImageIframe('HDF_Images')">Thêm ảnh</div>
                                <asp:FileUpload ID="UploadFile" ClientIDMode="Static" Style="display: none;"
                                    CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                <div class="wrap listing-img-upload title-version">
                                    <% if (listImg.Count > 0)
                                        { %>
                                    <% foreach (var v in listImg)
                                        { %>
                                    <div class="thumb-wp">
                                        <div class="left">
                                            <img class="thumb" alt="" title="" src="<%= v.url %>" data-img="" />
                                        </div>
                                        <div class="right">
                                            <input class="thumb-title" type="text" placeholder="Tiêu đề ảnh" value="<%=v.title %>" />
                                            <textarea class="thumb-des" placeholder="Mô tả"><%=v.description %></textarea>
                                            <div class="action">
                                                <div class="action-item action-add" onclick="popupImageIframe('HDF_Images')"><i class="fa fa-plus-square" aria-hidden="true"></i>Thêm ảnh</div>
                                                <div class="action-item action-delete" onclick="deleteThumbnail($(this), 'HDF_Images')"><i class="fa fa-trash-o" aria-hidden="true"></i>Xóa</div>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% } %>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <tr class="tr-field-ahalf">
                        <td class="col-xs-2 left"><span>Link video</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="VideoLink" runat="server" ClientIDMode="Static" placeholder="Youtube link..."></asp:TextBox>
                            </span>
                        </td>
                    </tr>
                    <tr class="tr-field-ahalf">
                        <td class="col-xs-2 left"><span></span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="VideoTitle" runat="server" ClientIDMode="Static" placeholder="Tiêu đề video"></asp:TextBox>
                            </span>
                        </td>
                    </tr>
                    <tr class="tr-field-ahalf">
                        <td class="col-xs-2 left"><span></span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="VideoDescription" runat="server" ClientIDMode="Static" placeholder="Mô tả video"></asp:TextBox>
                            </span>
                        </td>
                    </tr>

                    <tr class="tr-field-ahalf">
                        <td class="col-xs-2 left"><span>Thứ tự sắp xếp</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="Order" runat="server" ClientIDMode="Static" placeholder="Số thứ tự sắp xếp"></asp:TextBox>
                            </span>
                        </td>
                    </tr>

                    <tr class="tr-product-category">
                        <td class="col-xs-2 left"><span>Dịch vụ chính</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:CheckBox ID="ServicePrimary" Checked="true" runat="server" />
                            </span>
                        </td>
                    </tr>

                    <tr class="tr-product-category">
                        <td class="col-xs-2 left"><span>Hiện trên app</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:CheckBox ID="ShowOnApp" Checked="false" runat="server" />
                            </span>
                        </td>
                    </tr>
                    
                    <tr class="tr-send">
                        <td class="col-xs-2 left"></td>
                        <td class="col-xs-9 right no-border">
                            <span class="field-wp">
                                <asp:Button ID="Send" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="Update_OBJ" OnClientClick="clientScript()"></asp:Button>
                                <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>
                            </span>
                        </td>
                    </tr>
                    <asp:HiddenField runat="server" ID="UrlKeyField"  ClientIDMode="Static"/>
                    <asp:HiddenField runat="server" ID="ImgCoverField"  ClientIDMode="Static"/>
                    <asp:HiddenField runat="server" ID="HDF_Images"  ClientIDMode="Static"/>
                    <asp:HiddenField runat="server" ID="HDF_Videos"  ClientIDMode="Static"/>
                </tbody>
            </table>
        </div>
    </div>
    <%-- end Add --%>
</div>

<script>
    jQuery(document).ready(function () {
        $("#glbAdminContent").addClass("active");
        $("#glbAdminService").addClass("active");
        $("#subMenu .li-edit").addClass("active");

        excThumbWidth(120, 120);
        initImgStore("HDF_Images");
    });
</script>

<!-- Popup plugin image -->
<script type="text/javascript">
    function popupImageIframe(StoreImgField) {
        var Width = 960;
        var Height = 560;
        var ImgList = [];
        var iframe = $("<div class='iframe-image-upload' style='width:" + Width + "px; height:" + Height + "px; float:left; display:none;'><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + StoreImgField + "&callback=getThumbFromIframe&imgWidth=1334&imgHeight=750&multi_img=false&Folder=Common' style='width:100%; height: 100%;'></iframe></div");

        $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
    }

    window.getThumbFromIframe = function (Imgs, StoreImgField) {
        var imgs = "";
        var thumb = "";
        if (Imgs.length > 0) {
            for (var i = 0; i < Imgs.length; i++) {
                thumb += '<div class="thumb-wp">' +
                            '<div class="left">' +
                                    '<img class="thumb" alt="" title="" src="' + Imgs[i] + '" data-img=""/>' +
                            '</div>' +
                            '<div class="right">' +
                                '<input class="thumb-title" type="text" placeholder="Tiêu đề ảnh" />' +
                                '<textarea class="thumb-des" placeholder="Mô tả"></textarea>' +
                                '<div class="action">        ' +                                            
                                    '<div class="action-item action-add" onclick="popupImageIframe(\'HDF_Images\')"><i class="fa fa-plus-square" aria-hidden="true"></i>Thêm ảnh</div>' +
                                    '<div class="action-item action-delete" onclick="deleteThumbnail($(this), \'' + StoreImgField + '\')"><i class="fa fa-trash-o" aria-hidden="true"></i>Xóa</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>';
            }
            $("." + StoreImgField).find(".listing-img-upload").append($(thumb));
            excThumbWidth(120, 120);
        }
        autoCloseEBPopup();
    }

    function imgLink(StoreImgField) {
        var imgs = [], src, title, description;
        $("." + StoreImgField).find(".thumb-wp").each(function () {
            src = $(this).find("img.thumb").attr("src");
            title = $(this).find("input.thumb-title").val();
            description = $(this).find("textarea.thumb-des").val();
            console.log(title + " - " + description);
            if (src.trim() != "") {
                imgs.push({ url: src, thumb: executeThumPath(src), title: title, description: description });
            }            
        });
        return imgs;
    }

    function initImgStore(StoreImgField) {
        $("#" + StoreImgField).val(JSON.stringify(imgLink(StoreImgField)))
    }

    function initVideoStore(StoreVideoField) {
        var videos = [];
        var url = $("#VideoLink").val();
        var title = $("#VideoTitle").val();
        var description = $("#VideoDescription").val();
        videos.push({url : url, title : title, description : description});
        $("#" + StoreVideoField).val(JSON.stringify(videos));
    }

    function deleteThumbnail(This, StoreImgField) {
        This.parent().parent().parent().remove();
    }

    function clientScript() {
        initImgStore('HDF_Images');
        initVideoStore("HDF_Videos");
    }

    function executeThumPath(src) {
        var str = "";
        var fileExtension = "";
        var strLeft = "";
        var strPush = "x350";
        if (src != null && src != "") {
            var loop = src.length - 1;
            for (var i = loop; i >= 0; i--) {
                if (src[i] == ".") {
                    str = src.substring(0, i) + strPush + "." + fileExtension;
                    break;
                } else {
                    fileExtension = src[i] + fileExtension;
                }
            }
        }
        return str;
    }
</script>
<!-- Popup plugin image -->

</asp:Panel>
</asp:Content>

