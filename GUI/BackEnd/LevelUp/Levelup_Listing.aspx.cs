﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Linq.Expressions;
using LinqKit;
using System.Web.Services;
using Library;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Web.Script.Serialization;

namespace _30shine.GUI.BackEnd.LevelUp
{
    public partial class Levelup_Listing : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        private Expression<Func<Staff, bool>> Where = PredicateBuilder.True<Staff>();
        private bool Perm_Access = false;
        private bool Perm_AllData = false;
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        private bool Perm_ShowSalon = false;
        protected bool Perm_AllowAddNew = false;
        protected int SalonId;
        protected int Skill;
        protected string name;
        CultureInfo culture = new CultureInfo("vi-VN");
        public static Library.SendEmail sendMail = new Library.SendEmail();
        public int TotalRows;

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckPermission();
            if (!IsPostBack)
            {
                Bind_Skilllevel();
                bindData();
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_AllData);
                RemoveLoading();
            }
            else
            {
                //
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            bindData();
            RemoveLoading();
        }

        private void Bind_Skilllevel()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var lst = db.Store_StaffAutoLevel_BindSkillLevel().ToList();
                    var Key = 0;

                    SkillLevel.DataTextField = "SkillLevel";
                    SkillLevel.DataValueField = "Id";

                    ListItem item = new ListItem("Chọn bậc kỹ năng", "0");
                    SkillLevel.Items.Insert(0, item);

                    foreach (var v in lst)
                    {
                        Key++;
                        item = new ListItem(v.skillName, v.skillId.ToString());
                        SkillLevel.Items.Insert(Key, item);
                    }
                    SkillLevel.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public StaffAutoLevelLog insertLog(StaffAutoLevelup item)
        {
            try
            {
                StaffAutoLevelLog staffLog = new StaffAutoLevelLog();
                using (var db = new Solution_30shineEntities())
                {
                    //Save report
                    staffLog.Staff_Id = item.Staff_Id;
                    staffLog.Salon_Id = item.Salon_Id;
                    staffLog.StaffType_Id = item.StaffType_Id;
                    staffLog.SkillLevel_Id = item.SkillLevel_Id;
                    staffLog.SkillLevelup_Id = item.SkillLevelup_Id;
                    staffLog.sDate = item.sDate;
                    staffLog.Point = item.Point;
                    staffLog.KCS = item.KCS;
                    staffLog.Warning = item.Warning;
                    staffLog.TSL_DanhGia = item.TSL_DanhGia;
                    staffLog.StatusSkillLevel = item.StatusSkillLevel;
                    staffLog.Approval = item.Approval;
                    staffLog.CreatedDate = item.CreatedDate;
                    staffLog.ModifieldTime = item.ModifieldTime;
                    staffLog.IsCondition = item.IsCondition;
                    staffLog.IsDelete = item.IsDelete;
                    staffLog.IsLevelUp = item.IsLevelUp;
                    staffLog.IsApproval = item.IsApproval;
                    db.StaffAutoLevelLogs.AddOrUpdate(staffLog);
                    db.SaveChanges();
                }
                return staffLog;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

        }


        /// <summary>
        /// Hàm check theo ngày, nếu đủ điều kiện thì pass qua Levelup_Listing_V2
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name="Value"></param>
        /// <returns></returns>
        public void CheckStaffLevelup(DateTime endDate, int? StaffId)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    //khai bao PageNumber
                    int PageNumber = 1;
                    int RowspPage = PAGING._Segment;
                    if (HDF_Page.Value != "" && Convert.ToInt32(HDF_Page.Value) != 0)
                    {
                        PageNumber = Convert.ToInt32(HDF_Page.Value);
                    }
                    if (HDF_OPTSegment.Value != "" && Convert.ToInt32(HDF_OPTSegment.Value) != 0)
                    {
                        RowspPage = Convert.ToInt32(HDF_OPTSegment.Value);
                    }
                    //check xem có tồn tại hay không
                    List<StaffAutoLevelup> levelup = (from c in db.StaffAutoLevelups
                                                      where c.Staff_Id == StaffId
                                                      select c).ToList();
                    if (levelup.Count > 0)
                    {
                        foreach (var item in levelup)
                        {
                            var checkStaffUp = db.StaffAutoConditions.FirstOrDefault(f => f.Id == item.Levelup_Id);
                            var inTime = DateTime.Now >= endDate;
                            //Check các điều kiện sau, IsLevelup = null để pass qua 
                            if (checkStaffUp.KCS >= item.KCS && item.Point >= checkStaffUp.Point && checkStaffUp.Warning >= item.Warning && inTime)
                            {
                                item.CreatedDate = DateTime.Now;
                                item.IsLevelUp = null;
                                db.SaveChanges();
                            }
                            else
                            {
                                item.CreatedDate = DateTime.Now;
                                item.IsCondition = false;
                                item.IsLevelUp = false;
                                item.Approval = 1;
                                item.StatusSkillLevel = false;
                                db.SaveChanges();
                                // Init log skill level
                                var IsDate = db.StaffAutoConditions.FirstOrDefault(f => f.Id == item.Levelup_Id);
                                var obj = db.Logs.FirstOrDefault(f => f.OBJId == item.Staff_Id);
                                if (obj == null)
                                {
                                    obj.LogTime = DateTime.MinValue;
                                }
                                DateTime startDate = (DateTime)obj.LogTime;
                                DateTime expiryDate = startDate.AddDays((int)IsDate.sDate);
                                item.sDate = expiryDate;
                                insertLog(item);
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("Lỗi không tồn tại bản ghi");
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        [WebMethod]
        public static object updateDanhGia(int staffId, string Value)
        {
            try
            {
                //Hàm update đánh giá của TSL
                using (var db = new Solution_30shineEntities())
                {
                    var objList = db.StaffAutoLevelups.Where(w => w.Staff_Id == staffId).ToList();
                    if (objList.Count > 0)
                    {
                        //Check các điều kiện IsApproval = true là được chấp thuận, IsCondition = true là đủ điều kiện để xét duyệt
                        foreach (var item in objList)
                        {
                            item.TSL_DanhGia = Value;
                            item.ModifieldTime = DateTime.Now;
                            item.IsDelete = false;
                            item.IsApproval = true;
                            item.IsCondition = true;
                            db.StaffAutoLevelups.AddOrUpdate(item);
                            db.SaveChanges();
                        }
                    }
                    return objList;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        [WebMethod]
        public static object updateTime(int staffId, string Value)
        {
            try
            {
                //Hàm update đánh giá của TSL
                using (var db = new Solution_30shineEntities())
                {
                    var objList = db.StaffAutoLevelups.Where(w => w.Staff_Id == staffId).ToList();
                    if (objList.Count > 0)
                    {
                        //Check các điều kiện IsApproval = true là được chấp thuận, IsCondition = true là đủ điều kiện để xét duyệt
                        foreach (var item in objList)
                        {
                            if (Value == "" || Value.Length < 10 || Value.Length < 8)
                            {
                                item.sDate = item.sDate;
                            }
                            else
                            {
                                CultureInfo culture = new CultureInfo("vi-VN");
                                item.sDate = Convert.ToDateTime(Value, culture);
                                item.IsDelete = false;
                                db.StaffAutoLevelups.AddOrUpdate(item);
                                db.SaveChanges();
                                //update LogTime
                                Log sLog = (from c in db.Logs
                                            where c.OBJId == staffId
                                            select c).OrderByDescending(f => f.Id).FirstOrDefault();
                                if (sLog != null)
                                {
                                    var IsDate = db.StaffAutoConditions.FirstOrDefault(f => f.Id == item.Levelup_Id);
                                    DateTime startDate = (DateTime)item.sDate;
                                    sLog.LogTime = startDate.AddDays(-(int)IsDate.sDate);
                                    db.SaveChanges();
                                }
                            }
                        }
                    }
                    return objList;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// List of staff upgrading
        /// </summary>
        /// <returns></returns>
        protected List<cls_StaffAutoLevelups> getListData()
        {
            var data = new List<cls_StaffAutoLevelups>();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    //khai bao PageNumber
                    int PageNumber = 1;
                    int RowspPage = PAGING._Segment;
                    if (HDF_Page.Value != "" && Convert.ToInt32(HDF_Page.Value) != 0)
                    {
                        PageNumber = Convert.ToInt32(HDF_Page.Value);
                    }
                    if (HDF_OPTSegment.Value != "" && Convert.ToInt32(HDF_OPTSegment.Value) != 0)
                    {
                        RowspPage = Convert.ToInt32(HDF_OPTSegment.Value);
                    }
                    //lấy dữ liệu
                    int integer;
                    this.SalonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
                    this.Skill = int.TryParse(SkillLevel.SelectedValue, out integer) ? integer : 0;
                    //Check điều kiện để nâng bậc, nếu không đủ điều kiện xuất ra phần báo cáo
                    var listStaffOrderBy = db.StaffAutoLevelups.OrderByDescending(o => o.Id).Where(o => o.IsDelete == false).ToList();
                    if (listStaffOrderBy.Count > 0)
                    {
                        foreach (var _item in listStaffOrderBy)
                        {
                            var checkStaffUp = db.StaffAutoConditions.FirstOrDefault(f => f.Id == _item.Levelup_Id);
                            var inTime = DateTime.Now >= _item.sDate;
                            if (checkStaffUp.KCS >= _item.KCS && _item.Point >= checkStaffUp.Point && checkStaffUp.Warning >= _item.Warning && inTime)
                            {
                                _item.CreatedDate = DateTime.Now;
                                _item.IsCondition = true;
                                _item.IsLevelUp = null;
                                db.SaveChanges();
                            }
                            else
                            {
                                // Init log skill level
                                var isDate = db.StaffAutoConditions.FirstOrDefault(f => f.Id == _item.Levelup_Id);
                                var obj = db.StaffAutoLevelLogs.Where(f => f.Staff_Id == _item.Staff_Id).OrderByDescending(f => f.Id).FirstOrDefault();
                                if (obj == null)
                                {
                                    var sLog = db.Logs.Where(f => f.OBJId == _item.Staff_Id).OrderByDescending(f => f.Id).FirstOrDefault();
                                    if (sLog != null)
                                    {
                                        _item.sDate = sLog.LogTime;
                                    }
                                    else
                                    {
                                        var staff = db.Staffs.FirstOrDefault(f => f.Id == _item.Staff_Id);
                                        _item.sDate = staff.CreatedDate;
                                    }
                                }
                                else
                                {
                                    if (DateTime.Now > _item.sDate)
                                    {
                                        DateTime startDate = (DateTime)obj.sDate;
                                        DateTime expiryDate = startDate.AddDays((int)isDate.sDate);
                                        _item.sDate = expiryDate;
                                    }
                                    else
                                    {
                                        _item.sDate = _item.sDate;
                                    }
                                }
                                _item.CreatedDate = DateTime.Now;
                                _item.IsCondition = false;
                                _item.IsLevelUp = false;
                                _item.Approval = 1;
                                _item.StatusSkillLevel = false;
                                db.StaffAutoLevelups.AddOrUpdate(_item);
                                db.SaveChanges();
                                insertLog(_item);
                            }
                        }
                    }
                    //List staff đủ điều kiện nâng bậc
                    var listStaffData = db.Store_StaffAutoLevel_TSLApproval(this.SalonId, this.Skill, PageNumber, RowspPage).ToList();
                    var item = new StaffAutoLevelup();
                    var item2 = new cls_StaffAutoLevelups();
                    if (listStaffData.Count > 0)
                    {
                        foreach (var v in listStaffData)
                        {
                            //Gán dữ liệu hiển thị ra view
                            item2 = new cls_StaffAutoLevelups();
                            item2.staffName = v.staffName;
                            item2.staffId = v.staffId;
                            item2.salonName = v.salonName;
                            item2.typeName = v.typeName;
                            item2.skillName = v.skillName;
                            item2.skillId = v.skillId;
                            item2.skillNameup = v.SkillNameLevelUp;
                            item.Levelup_Id = v.levelUpId;
                            // Init log skill level
                            var isDate = db.StaffAutoConditions.FirstOrDefault(f => f.Id == item.Levelup_Id);
                            var obj = db.Logs.Where(f => f.OBJId == v.staffId).OrderByDescending(f => f.Id).FirstOrDefault();
                            var staff = db.Staffs.FirstOrDefault(f => f.Id == v.staffId);
                            if (obj == null)
                            {
                                obj = new Log();
                                obj.LogTime = staff.CreatedDate;
                            }
                            else
                            {
                                obj.LogTime = obj.LogTime;
                            }
                            DateTime startDate = (DateTime)obj.LogTime;
                            DateTime expiryDate = startDate.AddDays((int)isDate.sDate);
                            if (v.sTypeId == 1)
                            {
                                var TotalStaff = db.Store_StaffAutoLevel_SCSC(startDate, expiryDate, v.salonId, v.staffId).FirstOrDefault();
                                if (TotalStaff == null)
                                {
                                    TotalStaff = new Store_StaffAutoLevel_SCSC_Result();
                                    TotalStaff.ratingPoint = 0;
                                    TotalStaff.Total_SCSC_Error = 0;
                                    TotalStaff.mistPoint = 0;
                                }
                                item2.Point = TotalStaff.ratingPoint;
                                item2.KCS = TotalStaff.Total_SCSC_Error;
                                item2.Warning = TotalStaff.mistPoint;
                            }
                            else
                            {
                                var TotalSkinner = db.Store_StaffAutoLevel_SkinnerBindData(startDate, expiryDate, v.salonId, v.staffId).FirstOrDefault();
                                if (TotalSkinner == null)
                                {
                                    TotalSkinner = new Store_StaffAutoLevel_SkinnerBindData_Result();
                                    TotalSkinner.PointRating = 0;
                                    TotalSkinner.Warning = 0;
                                }
                                item2.Point = TotalSkinner.PointRating;
                                item2.KCS = 0;
                                item2.Warning = TotalSkinner.Warning;
                            }
                            item2.CreatedDate = String.Format("{0:dd/MM/yyyy}", expiryDate);
                            item2.TSL_DanhGia = v.TSL_DanhGia;
                            item2.ModifieldTime = v.ModifieldTime;
                            if (item2 != null)
                            {
                                StaffAutoLevelup levelup = (from c in db.StaffAutoLevelups
                                                            where c.Staff_Id == v.staffId
                                                            select c).FirstOrDefault();
                                //nếu không tồn tại thì add
                                if (levelup == null)
                                {
                                    item.Staff_Id = v.staffId;
                                    item.Salon_Id = v.salonId;
                                    item.StaffType_Id = v.sTypeId;
                                    item.SkillLevel_Id = v.skillId;
                                    item.SkillLevelup_Id = v.SkillLevelup_Id;
                                    item.Levelup_Id = item.Levelup_Id;
                                    item.sDate = expiryDate.Date;
                                    item.Point = item2.Point;
                                    item.KCS = item2.KCS;
                                    item.Warning = item2.Warning;
                                    item.TSL_DanhGia = item2.TSL_DanhGia;
                                    item.CreatedDate = DateTime.Now;
                                    item.ModifieldTime = v.ModifieldTime;
                                    item.IsDelete = false;
                                    db.StaffAutoLevelups.Add(item);
                                    db.SaveChanges();
                                    //hàm check theo ngày
                                    CheckStaffLevelup((DateTime)item.sDate, item.Staff_Id);
                                }
                                //Tồn tại rồi thì update
                                else
                                {
                                    levelup.Staff_Id = v.staffId;
                                    levelup.Salon_Id = v.salonId;
                                    levelup.StaffType_Id = v.sTypeId;
                                    levelup.SkillLevel_Id = v.skillId;
                                    levelup.SkillLevelup_Id = v.SkillLevelup_Id;
                                    levelup.Levelup_Id = v.levelUpId;
                                    levelup.sDate = expiryDate;
                                    levelup.Point = item2.Point;
                                    levelup.KCS = item2.KCS;
                                    levelup.Warning = item2.Warning;
                                    levelup.TSL_DanhGia = item2.TSL_DanhGia;
                                    levelup.ModifieldTime = v.ModifieldTime;
                                    levelup.IsDelete = false;
                                    db.SaveChanges();
                                    //hàm check theo ngày
                                    CheckStaffLevelup((DateTime)levelup.sDate, levelup.Staff_Id);
                                }
                            }
                            data.Add(item2);
                        }
                        try
                        {
                            this.TotalRows = Convert.ToInt32(listStaffData[0].TotalRows);
                        }
                        catch
                        {
                            this.TotalRows = 0;
                        }
                    }
                    else
                    {
                        data = new List<cls_StaffAutoLevelups>();
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return data;
        }

        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();
            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        /// <summary>
        /// Lấy dữ liệu truy vấn từ db qua sql store
        /// </summary>
        /// <returns></returns>
        private void bindData()
        {
            var data = getListData();
            Bind_Paging(this.TotalRows);
            RptLevelup.DataSource = data;
            RptLevelup.DataBind();
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "mode1", "salonmanager", "accountant" };
                string[] AllowAllData = new string[] { "root", "admin", "mode1", "accountant" };
                string[] AllowDelete = new string[] { "root", "admin" };
                string[] AllowSalon = new string[] { "root", "admin" };
                string[] AllowAddNew = new string[] { "root", "admin" };
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }
                if (Array.IndexOf(AllowAllData, Permission) != -1)
                {
                    Perm_AllData = true;
                }
                if (Array.IndexOf(AllowDelete, Permission) != -1)
                {
                    Perm_Delete = true;
                }
                if (Array.IndexOf(AllowSalon, Permission) != -1)
                {
                    Perm_ShowSalon = true;
                }
                if (Array.IndexOf(AllowAddNew, Permission) != -1)
                {
                    Perm_AllowAddNew = true;
                }
                Perm_Edit = true;
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        public class cls_StaffAutoLevelups
        {
            public int staffId { get; set; }

            /// <summary>
            /// Tên nhân viên
            /// </summary>
            public string staffName { get; set; }

            /// <summary>
            /// Tên salon
            /// </summary>
            public string salonName { get; set; }

            /// <summary>
            /// Tên kiểu nhân viên stylist or skiner
            /// </summary>
            public string typeName { get; set; }

            /// <summary>
            /// Tên bậc kỹ năng 1, A, B, C
            /// </summary>
            public string skillName { get; set; }

            /// <summary>
            /// Id bậc hiện tại
            /// </summary>
            public int? skillId { get; set; }

            /// <summary>
            /// Tên bậc nâng lên
            /// </summary>
            public string skillNameup { get; set; }

            public string CreatedDate { get; set; }

            /// <summary>
            /// Hệ số điểm hài lòng
            /// </summary>
            public double? Point { get; set; }

            /// <summary>
            /// % KCS
            /// </summary>
            public double? KCS { get; set; }
            /// <summary>
            /// Số lần vi phạm
            /// </summary>
            public int? Warning { get; set; }
            public DateTime? ModifieldTime { get; set; }
            /// <summary>
            /// Trưởng salon đánh giá
            /// </summary>
            public string TSL_DanhGia { get; set; }

            public Nullable<int> TotalRows { get; set; }
        }
    }
}