﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Linq.Expressions;
using LinqKit;
using System.Web.Services;
using Library;
using System.Data.Entity.Migrations;
using System.Web.Script.Serialization;
using System.Timers;
using System.Web.Caching;

namespace _30shine.GUI.BackEnd.LevelUp
{
    public partial class Levelup_DieuKien : System.Web.UI.Page
    {
        private bool Perm_Access = false;
        private bool Perm_AllData = false;
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        private bool Perm_ShowSalon = false;
        protected bool Perm_AllowAddNew = false;
        public int Interval;
        public System.Timers.Timer _timer;
        public DateTime _scheduleTime;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                RemoveLoading();
                Bind_Data();
            }
            else
            {
            }
        }

        protected List<cls_Levelup> getListData()
        {

            using (var db = new Solution_30shineEntities())
            {
                try
                {

                    var objLevelup = new cls_Levelup();
                    var _list = new List<cls_Levelup>();
                    var sData = db.Store_StaffAutoLevel_BindData().ToList();
                    if (sData.Count > 0)
                    {
                        foreach (var item in sData)
                        {
                            objLevelup = new cls_Levelup();
                            objLevelup.sTypeName = item.sTypeName;
                            objLevelup.sKillName = item.sKillName;
                            objLevelup.fId = item.fId;
                            if (item.fTime == null)
                            {
                                item.fTime = 0;
                            }
                            else
                            {
                                objLevelup.fTime = (int)item.fTime;

                            }
                            if (item.fPoint == null)
                            {
                                item.fPoint = 0;

                            }
                            else
                            {
                                objLevelup.fPoint = (int)item.fPoint;

                            }
                            if (item.fKCS == null)
                            {
                                item.fKCS = 0;
                            }
                            else
                            {
                                objLevelup.fKCS = (int)item.fKCS;

                            }
                            if (item.fId == 4)
                            {
                                objLevelup.disable = "disabled=  " + " disabled " + "  ";
                                objLevelup.sThongBao = " Skiner không có chỉ số này*";
                                objLevelup.fKCS = 0;
                            }
                            else
                            {
                                objLevelup.fKCS = (int)item.fKCS;
                            }
                            if (item.fWarning == null)
                            {
                                item.fWarning = 0;

                            }
                            else
                            {

                                objLevelup.fWarning = (int)item.fWarning;
                            }
                            _list.Add(objLevelup);

                        }

                    }
                    else
                    {
                        throw new Exception("Error!!!");
                    }
                    return _list;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

       


        /// <summary>
        /// Binding data
        /// </summary>
        public void Bind_Data()
        {
            try
            {
                RptLevelup.DataSource = this.getListData();
                RptLevelup.DataBind();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        [WebMethod]
        public static object updatesDate(int Id, int Value)
        {
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                //var obj = db.tbl_Levelup_DieuKien.FirstOrDefault(w => w.Id == Id);
                StaffAutoCondition obj = (from c in db.StaffAutoConditions
                                                      where c.Id == Id
                                             select c).FirstOrDefault();
                if (obj != null)
                {
                    obj.sDate = Value;
                    obj.ModifieldTime = DateTime.Now;
                    //db.tbl_Levelup_DieuKien.Add(obj);
                    var exc = db.SaveChanges();
                    if (exc > 0)
                    {
                        Msg.success = true;
                    }
                    else
                    {
                        Msg.success = false;
                    }
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }


        [WebMethod]
        public static object updatesPoint(int Id, int Value)
        {
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                //var obj = db.tbl_Levelup_DieuKien.FirstOrDefault(w => w.Id == Id);
                StaffAutoCondition obj = (from c in db.StaffAutoConditions
                                            where c.Id == Id
                                            select c).FirstOrDefault();
                if (obj != null)
                {
                    obj.Point = Value;
                    obj.CreatedDate = DateTime.Now;
                    obj.ModifieldTime = DateTime.Now;
                   //db.tbl_Levelup_DieuKien.Add(obj);
                    var exc = db.SaveChanges();
                    if (exc > 0)
                    {
                        Msg.success = true;
                    }
                    else
                    {
                        Msg.success = false;
                    }
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }
        [WebMethod]
        public static object updatesKCS(int Id, int Value)
        {
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                //var obj = db.tbl_Levelup_DieuKien.FirstOrDefault(w => w.Id == Id);
                StaffAutoCondition obj = (from c in db.StaffAutoConditions
                                            where c.Id == Id
                                            select c).FirstOrDefault();
                if (obj != null)
                {
                    obj.KCS = Value;
                    obj.CreatedDate = DateTime.Now;
                    obj.ModifieldTime = DateTime.Now;
                    //db.tbl_Levelup_DieuKien.AddOrUpdate();
                    var exc = db.SaveChanges();
                    if (exc > 0)
                    {
                        Msg.success = true;
                    }
                    else
                    {
                        Msg.success = false;
                    }
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }
        [WebMethod]
        public static object updatesWarning(int Id, int Value)
        {
            var Msg = new Msg();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            using (var db = new Solution_30shineEntities())
            {
                //var obj = db.tbl_Levelup_DieuKien.FirstOrDefault(w => w.Id == Id);
                StaffAutoCondition obj = (from c in db.StaffAutoConditions
                                            where c.Id == Id
                                            select c).FirstOrDefault();
                if (obj != null)
                {
                    obj.Warning = Value;
                    obj.CreatedDate = DateTime.Now;
                    obj.ModifieldTime = DateTime.Now;
                    //db.tbl_Levelup_DieuKien.AddOrUpdate();
                    var exc = db.SaveChanges();
                    if (exc > 0)
                    {
                        Msg.success = true;
                    }
                    else
                    {
                        Msg.success = false;
                    }
                }
                else
                {
                    Msg.success = false;
                }
            }
            return serializer.Serialize(Msg);
        }
        protected void _BtnClick(object sender, EventArgs e)
        {
            RemoveLoading();

        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "mode1", "salonmanager", "accountant" };
                string[] AllowAllData = new string[] { "root", "admin", "mode1", "accountant" };
                string[] AllowDelete = new string[] { "root", "admin" };
                string[] AllowSalon = new string[] { "root", "admin" };
                string[] AllowAddNew = new string[] { "root", "admin" };
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }
                if (Array.IndexOf(AllowAllData, Permission) != -1)
                {
                    Perm_AllData = true;
                }
                if (Array.IndexOf(AllowDelete, Permission) != -1)
                {
                    Perm_Delete = true;
                }
                if (Array.IndexOf(AllowSalon, Permission) != -1)
                {
                    Perm_ShowSalon = true;
                }
                if (Array.IndexOf(AllowAddNew, Permission) != -1)
                {
                    Perm_AllowAddNew = true;
                }
                Perm_Edit = true;
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

    }
    public class cls_Levelup
    {
        public int fId { get; set; }
        public string sTypeName { get; set; }
        public string sKillName { get; set; }
        public int fTime { get; set; }
        public int fPoint { get; set; }
        public int fKCS { get; set; }
        public int fWarning { get; set; }
        public string disable { get; set; }
        public string sThongBao { get; set; }
    }
}