﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Linq.Expressions;
using LinqKit;
using System.Web.Services;
using Library;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Web.Script.Serialization;

namespace _30shine.GUI.BackEnd.LevelUp
{
    public partial class Levelup_BaoCao : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        private Expression<Func<Staff, bool>> Where = PredicateBuilder.True<Staff>();
        private bool Perm_Access = false;
        private bool Perm_AllData = false;
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        private bool Perm_ShowSalon = false;
        protected bool Perm_AllowAddNew = false;
        protected int SalonId;
        protected DateTime TimeFrom;
        protected DateTime TimeTo;
        protected string name;
        CultureInfo culture = new CultureInfo("vi-VN");
        public static Library.SendEmail sendMail = new Library.SendEmail();
        public int dCondition;
        public int skill;

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckPermission();
            if (!IsPostBack)
            {
                bindData();
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_AllData);
                Bind_Skilllevel();
                RemoveLoading();
                txtStartDate.Text = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            }
        }

        private string genSql()
        {
            var db = new Solution_30shineEntities();
            string sql = "";
            string wSalon = "";
            int integer;
            int salonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
            if (salonId > 0)
            {
                wSalon = " and staff.SalonId = " + salonId;
            }
            else
            {
                wSalon = " and staff.SalonId >= 0";
            }
            sql = db.Store_StaffAutoLevel_BindData().ToString();
            return sql;
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            bindData();
            RemoveLoading();

        }

        private void Bind_Skilllevel()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Store_StaffAutoLevel_BindSkillLevel().ToList();
                var Key = 0;

                SkillLevel.DataTextField = "SkillLevel";
                SkillLevel.DataValueField = "Id";

                ListItem item = new ListItem("Chọn bậc kỹ năng", "0");
                SkillLevel.Items.Insert(0, item);

                foreach (var v in lst)
                {
                    Key++;
                    item = new ListItem(v.skillName, v.skillId.ToString());
                    SkillLevel.Items.Insert(Key, item);
                }
                SkillLevel.SelectedIndex = 0;
            }
        }

        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        protected List<cls_StaffAutoLevelups> getListData()
        {
            var data = new List<cls_StaffAutoLevelups>();
            if (txtStartDate.Text != "")
            {
                try
                {
                    using (var db = new Solution_30shineEntities())
                    {
                        TimeFrom = Convert.ToDateTime(txtStartDate.Text, culture);
                        if (txtEndDate.Text != "")
                        {
                            TimeTo = Convert.ToDateTime(txtEndDate.Text, culture).AddDays(1);
                        }
                        else
                        {
                            TimeTo = TimeFrom.AddDays(1);
                        }
                        this.SalonId = Convert.ToInt32(Salon.SelectedValue);
                        this.dCondition = Convert.ToInt32(Condition.SelectedValue);
                        this.skill = Convert.ToInt32(SkillLevel.SelectedValue);
                        var _sData = db.Store_StaffAutoLevel_Report(this.TimeFrom, this.TimeTo, this.SalonId,this.skill, this.dCondition).ToList();
                        var item = new StaffAutoLevelup();
                        var item2 = new cls_StaffAutoLevelups();
                        if (_sData.Count > 0)
                        {
                            foreach (var v in _sData)
                            {
                                item2 = new cls_StaffAutoLevelups();
                                var _tbl_List = db.StaffAutoLevelLogs.FirstOrDefault(f => f.Staff_Id == v.staffId);
                                item2 = new cls_StaffAutoLevelups();
                                item2.staffName = v.staffName;
                                item2.staffId = v.staffId;
                                item2.salonName = v.salonName;
                                item2.typeName = v.typeName;
                                item2.CreatedDate = String.Format("{0:dd/MM/yyyy}", v.sDate);
                                item2.Point = v.ratingPoint;
                                item2.KCS = v.KCS;
                                item2.Warning = v.warning;
                                item2.TSL_DanhGia = v.TSL_DanhGia;
                                item2.ModifieldTime = v.ModifieldTime;
                                //Nếu skill khác null
                                if (v.skillUpId != null)
                                {
                                    //Check điều kiện
                                    if (v.Condition == true)
                                    {
                                        //Check skill lên level
                                        item2.skillName = v.skillName;
                                        item2.skillNameup = v.skillUpName;
                                    }
                                    else
                                    {
                                        item2.skillName = v.skillName;
                                        item2.skillNameup = v.skillName;
                                    }
                                }
                                if (v.Condition == null && v.statusSkill == null)
                                {
                                    item2.isCondition = "Fail";
                                    item2.StatusSkillLevel = "Không nâng bậc";
                                }
                                else
                                {
                                    if (v.Condition == true && v.statusSkill == true && v.Approval == 2)
                                    {
                                        item2.isCondition = "Pass";
                                        item2.StatusSkillLevel = "Đã nâng bậc";
                                    }
                                    else
                                    {
                                        item2.isCondition = "Fail";
                                        item2.StatusSkillLevel = "Không nâng bậc";

                                    }
                                }
                                if (v.Approval == null)
                                {
                                    item2.Approval = "Chưa duyệt";
                                }
                                else
                                {
                                    if (v.Approval == 1)
                                    {
                                        item2.Approval = "Chưa duyệt";
                                    }
                                    else if (v.Approval == 2)
                                    {
                                        item2.Approval = "Đã duyệt";
                                    }
                                    else if(v.Approval == 3)
                                    {
                                        item2.Approval = "Không duyệt";
                                    }
                                }
                                data.Add(item2);
                            }
                        }
                    }
                }

                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
            return data;
        }

        /// <summary>
        /// Lấy dữ liệu truy vấn từ db qua sql store
        /// The conversion of a nvarchar data type to a datetime data type resulted in an out-of-range value.
        /// </summary>
        /// <returns></returns>
        private void bindData()
        {
            var data = getListData();
            Bind_Paging(data.Count);
            RptLevelup.DataSource = data.OrderByDescending(o => o.staffId).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
            RptLevelup.DataBind();
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "mode1", "salonmanager", "accountant" };
                string[] AllowAllData = new string[] { "root", "admin", "mode1", "accountant" };
                string[] AllowDelete = new string[] { "root", "admin" };
                string[] AllowSalon = new string[] { "root", "admin" };
                string[] AllowAddNew = new string[] { "root", "admin" };
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }
                if (Array.IndexOf(AllowAllData, Permission) != -1)
                {
                    Perm_AllData = true;
                }
                if (Array.IndexOf(AllowDelete, Permission) != -1)
                {
                    Perm_Delete = true;
                }
                if (Array.IndexOf(AllowSalon, Permission) != -1)
                {
                    Perm_ShowSalon = true;
                }
                if (Array.IndexOf(AllowAddNew, Permission) != -1)
                {
                    Perm_AllowAddNew = true;
                }
                Perm_Edit = true;
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }


        public class cls_StaffAutoLevelups
        {
            public int staffId { get; set; }
            public string staffName { get; set; }
            public string salonName { get; set; }
            public string typeName { get; set; }
            public string skillName { get; set; }
            public string skillNameup { get; set; }
            public string CreatedDate { get; set; }
            public double? Point { get; set; }
            public double? KCS { get; set; }
            public int? Warning { get; set; }
            public DateTime? ModifieldTime { get; set; }
            public string TSL_DanhGia { get; set; }
            public string Approval { get; set; }
            public string StatusSkillLevel { get; set; }
            public int?  skillId { get; set; }
            public int? _skillIdTmp { get; set; }

            public string isCondition { get; set; }
        }
    }
}