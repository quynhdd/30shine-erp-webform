﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TemplateMaster/SiteMaster.Master" CodeBehind="Levelup_DieuKien.aspx.cs" Inherits="_30shine.GUI.BackEnd.LevelUp.Levelup_DieuKien" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <title>Quản lý nhân sự | Danh sách</title>
    <style>
        .skill-table {
            display: table;
            width: 100%;
        }

            .skill-table .item-row {
                display: table-row;
                border-bottom: 1px solid #ddd;
            }

            .skill-table .item-cell {
                display: table-cell;
                border-bottom: 1px solid #ddd;
                padding-top: 5px;
                padding-bottom: 5px;
            }

                .skill-table .item-cell > span {
                    float: left;
                    width: 100%;
                    text-align: left;
                }

            .skill-table .item-row .item-cell .checkbox {
                float: left;
                padding-top: 0 !important;
                padding-bottom: 0 !important;
                margin-top: 6px !important;
                margin-bottom: 0px !important;
                margin-right: 9px;
                text-align: left;
                background: #ddd;
                padding: 2px 10px !important;
            }

                .skill-table .item-row .item-cell .checkbox input[type='checkbox'] {
                    margin-left: 0 !important;
                    margin-right: 3px !important;
                }

            .skill-table .item-cell-order {
                width: 30px !important;
            }

            .skill-table .item-cell-skill {
                width: 120px !important;
            }

            .skill-table .item-cell-levels {
            }

        .modal {
            z-index: 10000;
        }

        .be-report .row-filter {
            z-index: 0;
        }

        @media(min-width: 768px) {
            .modal-content,
            .modal-dialog {
                width: 750px !important;
                margin: auto;
            }
        }
    </style>
</asp:Content>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Điều kiện nâng bậc &nbsp;&#187; </li>
                        <li class="li-listing li-listing1"><a href="/admin/nhan-vien/danh-sach.html">Danh sách ứng viên</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report">
            <%-- Listing --%>
            <div style="margin-top: 30px" class="wp960 content-wp">
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="table-wp">
                            <table style="text-align: center;" class="table-add table-listing">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th><b>Vị trí</b></th>
                                        <th><b>Cấp bậc hiện tại</b></th>
                                        <th><span>Thời gian</span><br />
                                            (Ngày)</th>
                                        <th><span>ĐHL<span style="color: #535252; font-family: 'Times New Roman'">(Lấy từ tổng ĐHL trong thời gian từ ngày<br />
                                            lên bậc + 30)</span></span><br />
                                            Lớn hơn hoặc bằng là Pass</th>
                                        <th><span>% Lỗi KCS <span style="color: #535252; font-family: 'Times New Roman'">(Lấy từ báo cáo KCS)</span></span><br />
                                            Nhỏ hơn hoặc bằng là Pass</th>
                                        <th><span>Vi phạm <span style="color: #535252; font-family: 'Times New Roman'">(Lấy tổng số theo ID trong<br />
                                            báo cáo vi phạm trường hợp bị >=300k<br />
                                            tính là 1 lần)</span></span><br />
                                            Nhỏ hơn hoặc bằng là Pass</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="RptLevelup" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 %></td>
                                                <td><%#Eval("sTypeName")%></td>
                                                <td><%#Eval("sKillName")%></td>
                                                <td>
                                                    <div class="input-inline-wp">
                                                        <input id="date" type="text" value="<%#Eval("fTime")%>" onkeypress="return ValidateKeypress(/\d/,event);" onchange="updateDate($(this), <%#Eval("fId") %>)" style="margin-left: 0!important;" />
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-inline-wp">
                                                        <input id="point" type="text" value="<%#Eval("fPoint")%>" onkeypress="return ValidateKeypress(/\d/,event);" onchange="updatePoint($(this), <%#Eval("fId") %>)" style="margin-left: 0!important;" />
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-inline-wp">
                                                        <input id="kcs" type="text" <%#Eval("disable") %> value="<%#Eval("fKCS")%><%#Eval("sThongBao") %>" onkeypress="return ValidateKeypress(/\d/,event);" onchange="updateKCS($(this), <%#Eval("fId") %>)" style="margin-left: 0!important;" />
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-inline-wp">
                                                        <input id="warning" type="text" value="<%#Eval("fWarning")%>" onkeypress="return ValidateKeypress(/\d/,event);" onchange="updateWarning($(this), <%#Eval("fId") %>)" style="margin-left: 0!important;" />
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                <!-- Hidden Field-->
                <asp:HiddenField ID="HDF_Suggestion_Code" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_Suggestion_Field" ClientIDMode="Static" runat="server" />
                <!-- END Hidden Field-->
            </div>
            <%-- END Listing --%>
        </div>



        <script type="text/ecmascript">
            jQuery(document).ready(function () {
                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });
            });

            /// Cập nhật giá trị LevelUp
            function updateDate(This, Id) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/LevelUp/Levelup_DieuKien.aspx/updatesDate",
                    data: '{Id : ' + Id + ', Value : ' + This.val() + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                }).success(function (response) {
                    alert("Cập nhật thành công!");
                    removeLoading();
                });
            }
            /// Cập nhật giá trị LevelUp
            function updatePoint(This, Id) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/LevelUp/Levelup_DieuKien.aspx/updatesPoint",
                    data: '{Id : ' + Id + ', Value : ' + This.val() + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                }).success(function (response) {
                    alert("Cập nhật thành công!");
                    removeLoading();
                });
            }
            /// Cập nhật giá trị LevelUp
            function updateKCS(This, Id) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/LevelUp/Levelup_DieuKien.aspx/updatesKCS",
                    data: '{Id : ' + Id + ', Value : ' + This.val() + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                }).success(function (response) {
                    alert("Cập nhật thành công!");
                    removeLoading();
                });
            }
            /// Cập nhật giá trị LevelUp
            function updateWarning(This, Id) {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/LevelUp/Levelup_DieuKien.aspx/updatesWarning",
                    data: "{Id : " + Id + ", Value : " + This.val() + "}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                }).success(function (response) {
                    alert("Cập nhật thành công!");
                    removeLoading();
                });
            }

            /*
      * Validate keypress
      */
            function ValidateKeypress(numcheck, e) {
                var keynum, keychar, numcheck;
                if (window.event) {
                    keynum = e.keyCode;
                }
                else if (e.which) {
                    keynum = e.which;
                }
                if (keynum == 8 || keynum == 127 || keynum == null || keynum == 9 || keynum == 0 || keynum == 13) return true;
                keychar = String.fromCharCode(keynum);
                var result = numcheck.test(keychar);
                return result;
            }
        </script>


    </asp:Panel>
</asp:Content>
