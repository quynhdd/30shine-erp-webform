﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TemplateMaster/SiteMaster.Master" CodeBehind="Levelup_Listing_V2.aspx.cs" Inherits="_30shine.GUI.BackEnd.LevelUp.Levelup_Listing_V2" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <title>Quản lý nhân sự | Danh sách</title>
    <style>
        .skill-table {
            display: table;
            width: 100%;
        }

            .skill-table .item-row {
                display: table-row;
                border-bottom: 1px solid #ddd;
            }

            .skill-table .item-cell {
                display: table-cell;
                border-bottom: 1px solid #ddd;
                padding-top: 5px;
                padding-bottom: 5px;
            }

                .skill-table .item-cell > span {
                    float: left;
                    width: 100%;
                    text-align: left;
                }

            .skill-table .item-row .item-cell .checkbox {
                float: left;
                padding-top: 0 !important;
                padding-bottom: 0 !important;
                margin-top: 6px !important;
                margin-bottom: 0px !important;
                margin-right: 9px;
                text-align: left;
                background: #ddd;
                padding: 2px 10px !important;
            }

                .skill-table .item-row .item-cell .checkbox input[type='checkbox'] {
                    margin-left: 0 !important;
                    margin-right: 3px !important;
                }

            .skill-table .item-cell-order {
                width: 30px !important;
            }

            .skill-table .item-cell-skill {
                width: 120px !important;
            }

            .skill-table .item-cell-levels {
            }

        .modal {
            z-index: 10000;
        }

        .be-report .row-filter {
            z-index: 0;
        }

        @media(min-width: 768px) {
            .modal-content,
            .modal-dialog {
                width: 750px !important;
                margin: auto;
            }
        }
    </style>
</asp:Content>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Danh sách xét nâng bậc &nbsp;&#187; </li>
                        <li class="li-listing li-listing1"><a href="/admin/nhan-vien/danh-sach.html">Danh sách</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row row-filter">
                    <strong class="st-head"><i class="fa fa-filter"></i>Lọc kết quả</strong>
                    <div class="filter-item">
                        <asp:DropDownList ID="Salon" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px;"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="SkillLevel" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px;"></asp:DropDownList>
                    </div>
                    <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <a href="/khach-hang/danh-sach.html" class="st-head btn-viewdata">Reset Filter</a>
                </div>
                <!-- End Filter -->
                <!-- Row Table Filter -->
                <div class="table-func-panel">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="table-wp">
                            <table style="text-align: center;" class="table-add table-listing">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên</th>
                                        <th>ID</th>
                                        <th>Salon</th>
                                        <th>Vị Trí</th>
                                        <th>Bậc hiện tại</th>
                                        <th>Bậc nâng lên</th>
                                        <th>Ngày đủ điều kiện xét duyệt</th>
                                        <th>ĐHL</th>
                                        <th>Lỗi KCS</th>
                                        <th>Vi phạm</th>
                                        <th>TSL đánh giá</th>
                                        <th>Ngày đánh giá</th>
                                        <%if (Perm_AllData)
                                            {  %>
                                        <th>Trạng thái</th>
                                        <th></th>
                                        <% } %>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="RptLevelup" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td><%#Eval("staffName")%></td>
                                                <td><%#Eval("staffId")%></td>
                                                <td><%#Eval("salonName")%></td>
                                                <td><%#Eval("typeName")%></td>
                                                <td><%#Eval("skillName")%></td>
                                                <td><%#Eval("skillNameup")%></td>
                                                 <td>
                                                    <textarea class="txtDateTime" style="text-align: center;" onchange="updatesTime($(this), <%# Eval("staffId") %>)"><%#Eval("CreatedDate")%></textarea>
                                                </td>
                                                <td><%#Eval("Point")%></td>
                                                <td><%#Eval("KCS")%></td>
                                                <td><%#Eval("Warning")%></td>
                                                <td style="text-align: inherit">
                                                    <textarea class="note" id="_input" onchange="updateDanhGia_V2($(this), <%#Eval("staffId") %>)"><%#Eval("TSL_DanhGia")%></textarea>
                                                </td>
                                                <td><%#Eval("ModifieldTime")%></td>
                                                <%if (Perm_AllData)
                                                    {  %>
                                                <td>
                                                    <select class="ddrlAppoval" style="text-align: center; width: 200px; border-radius: 3px;" id="ddlStatus">
                                                        <option value="1">Chưa duyệt</option>
                                                        <option value="2">Đã duyệt</option>
                                                        <option value="3">Không duyệt</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input id="ChkSave" onclick="updateSave($(this),<%#Eval("staffId") %>)" value="<%# Convert.ToBoolean(Eval("StatusSkillLevel")) ? "checked='checked'" : "" %>" type="checkbox" />
                                                </td>
                                                <% } %>
                                            </tr>

                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>

                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                <!-- Hidden Field-->
                <asp:HiddenField ID="HDF_Suggestion_Code" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_Suggestion_Field" ClientIDMode="Static" runat="server" />
                <!-- END Hidden Field-->
            </div>
            <%-- END Listing --%>
        </div>

        <%--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" onclick="showModal_SetSkillLevel(400)">Open Modal</button>--%>


        <script type="text/ecmascript">
            jQuery(document).ready(function () {
                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });
            });
        
            var paramClass = function () {
                this.staffId = 0;
                this.TSL_DanhGia = "";
                this.Approval = 0;
                this.StatusSkillLevel = false;
            }
            var paramObject = new paramClass();
            /*
           * Cập nhật ghi chú
           */
            function updateDanhGia_V2(This, staffId) {
                paramObject.staffId = staffId;
                paramObject.TSL_DanhGia = This.val();
                addLoading();
                //int staffId, string TSL_DanhGia, int Approval, bool StatusSkillLevel
                $.ajax({
                    type: 'POST',
                    url: "/GUI/BackEnd/LevelUp/Levelup_Listing_V2.aspx/updateDanhGia_V2",
                    data: '{staffId : ' + paramObject.staffId + ', TSL_DanhGia : "' + paramObject.TSL_DanhGia + '"}',
                    contentType: 'application/json',
                })
                alert("Cập nhật thành công!");
                removeLoading();
                location.reload();
               
            }
            function updateSave(This, staffId) {
                paramObject.staffId = staffId;
                paramObject.StatusSkillLevel = This.prop("checked");
                paramObject.Approval = This.parent().parent().find("select.ddrlAppoval").val();
                //, Approval : "' + paramObject.Approval + '"+ 
                debugger;
                addLoading();
                $.ajax({
                    type: 'POST',
                    url: "/GUI/BackEnd/LevelUp/Levelup_Listing_V2.aspx/updateDanhGia_V3",
                    data: '{staffId : ' + paramObject.staffId + ', Approval : ' + paramObject.Approval + ' , StatusSkillLevel : ' + paramObject.StatusSkillLevel + '}',
                    contentType: 'application/json',
                }).success(function (response) {
                    alert("Cập nhật thành công!");
                    removeLoading();
                    location.reload();
                });
            }
            function checking() {
                var textBox = $.trim($('input[type=text]').val())
                if (".note" == "") {
                    alert("Vui lòng nhập ngày xét duyệt!!!");
                }
            }
            function updatesTime(This, staffId) {
                paramObject.staffId = staffId;
                paramObject.CreatedDate = This.val();
                addLoading();
                $.ajax({
                    type: 'POST',
                    url: "/GUI/BackEnd/LevelUp/Levelup_Listing.aspx/updateTime",
                    data: '{staffId : ' + paramObject.staffId + ', Value : "' + paramObject.CreatedDate + '"}',
                    contentType: 'application/json',
                }).success(function (response) {
                    removeLoading();
                });
            }

        </script>
    </asp:Panel>
</asp:Content>
