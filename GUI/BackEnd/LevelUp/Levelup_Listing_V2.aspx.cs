﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Linq.Expressions;
using LinqKit;
using System.Web.Services;
using Library;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Web.Script.Serialization;
namespace _30shine.GUI.BackEnd.LevelUp
{
    public partial class Levelup_Listing_V2 : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        private Expression<Func<Staff, bool>> Where = PredicateBuilder.True<Staff>();
        private bool Perm_Access = false;
        public bool Perm_AllData = false;
        private bool AllowSalon = false;
        private bool Perm_ShowSalon = false;
        private bool Perm_Edit = false;
        protected bool Perm_AllowAddNew = false;
        protected int SalonId;
        protected int Skill;
        protected DateTime TimeFrom;
        protected DateTime TimeTo;
        protected string name;
        CultureInfo culture = new CultureInfo("vi-VN");
        public static Library.SendEmail sendMail = new Library.SendEmail();

        protected void Page_Load(object sender, EventArgs e)
        {
            CheckPermission();
            if (!IsPostBack)
            {
                Bind_Skilllevel();
                bindData();
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_AllData);
                RemoveLoading();
            }
            else
            {
                //
            }
        }

        private string genSql()
        {
            var db = new Solution_30shineEntities();
            string sql = "";
            string wSalon = "";
            int integer;
            int salonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
            if (salonId > 0)
            {
                wSalon = " and staff.SalonId = " + salonId;
            }
            else
            {
                wSalon = " and staff.SalonId >= 0";
            }
            sql = db.Store_StaffAutoLevel_BindData().ToString();
            return sql;
        }


        protected void _BtnClick(object sender, EventArgs e)
        {
            bindData();
            RemoveLoading();

        }

        /// <summary>
        /// Cập nhật trưởng salon đánh giá
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name="TSL_DanhGia"></param>
        /// <returns></returns>
        [WebMethod]
        public static object updateDanhGia_V2(int staffId, string TSL_DanhGia)
        {
            using (var db = new Solution_30shineEntities())
            {
                //hàm update đánh giá của tsl
                var objList = db.StaffAutoLevelups.Where(w => w.Staff_Id == staffId).ToList();
                if (objList != null)
                {
                    foreach (var obj in objList)
                    {
                        obj.TSL_DanhGia = TSL_DanhGia;
                        obj.ModifieldTime = DateTime.Now;
                        obj.IsDelete = false;
                        obj.IsCondition = true;
                        obj.IsApproval = null;
                        db.StaffAutoLevelups.AddOrUpdate(obj);
                        db.SaveChanges();
                    }
                }
                return objList;
            }
        }

        /// <summary>
        /// Update đánh giá và xét duyệt
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name="Approval"></param>
        /// <param name="StatusSkillLevel"></param>
        /// <returns></returns>
        [WebMethod]
        public static object updateDanhGia_V3(int staffId, int Approval, bool StatusSkillLevel)
        {
            using (var db = new Solution_30shineEntities())
            {
                //hàm update duyệt hay không duyệt
                StaffAutoLevelLog staffLog = new StaffAutoLevelLog();
                var item = db.StaffAutoLevelups.Where(w => w.Staff_Id == staffId).FirstOrDefault();
                if (item != null)
                {
                    //Nếu trường hợp này đúng thì duyệt
                    if (StatusSkillLevel == true && Approval == 2)
                    {
                        item.StatusSkillLevel = true;
                        item.IsCondition = true;
                        item.IsLevelUp = null;
                        item.IsApproval = null;
                        item.Approval = Approval;
                        if (item.SkillLevelup_Id == 1)
                        {
                            item.IsDelete = true;
                            item.IsLevelUp = false;
                        }
                        //join vào nhân viên
                        //update skill level
                        Staff staff = (from c in db.Staffs
                                       where c.Id == staffId
                                       select c).FirstOrDefault();
                        if (staff != null)
                        {
                            //Update skilllevel staff
                            staff.Id = staffId;
                            staff.SkillLevel = item.SkillLevelup_Id;
                            db.SaveChanges();

                            if (staffId > 0 && item.SkillLevelup_Id > 0)
                            {
                                //Insert bảng Log
                                var objLog = db.Logs.OrderByDescending(o => o.LogTime).FirstOrDefault(w => w.OBJId == staffId && w.Type == "staff_skill_level");
                                if (objLog == null || (objLog != null && objLog.Content != item.SkillLevelup_Id.ToString()))
                                {
                                    objLog = new Log();
                                    objLog.LogTime = item.sDate;
                                    objLog.OBJId = staffId;
                                    objLog.Content = item.SkillLevelup_Id.ToString();
                                    objLog.Type = "staff_skill_level";
                                    objLog.Status = 1;
                                    db.Logs.Add(objLog);
                                    db.SaveChanges();
                                }
                            }
                        }
                    }
                    //fail
                    else
                    {
                        //Check điều kiện
                        var IsDate = db.StaffAutoConditions.FirstOrDefault(f => f.Id == item.Levelup_Id);
                        //Nếu thời gian xét duyệt lớn hơn hiện tại 
                        if (DateTime.Now > item.sDate)
                        {
                            var sItem = db.StaffAutoLevelLogs.Where(f => f.Staff_Id == item.Staff_Id).OrderByDescending(f => f.Id).FirstOrDefault();
                            if (sItem != null)
                            {
                                DateTime startDate = (DateTime)sItem.sDate;
                                DateTime expiryDate = startDate.AddDays((int)IsDate.sDate);
                                item.sDate = expiryDate;
                            }
                        }
                        else
                        {
                            item.sDate = item.sDate;
                        }
                        item.IsLevelUp = null;
                        item.IsCondition = false;
                        item.StatusSkillLevel = false;
                        item.IsApproval = null;
                        item.Approval = Approval;
                        item.ModifieldTime = DateTime.Now;
                        item.IsDelete = false;
                        item.StatusSkillLevel = false;
                    }
                    db.StaffAutoLevelups.AddOrUpdate(item);
                    db.SaveChanges();
                    insertLog(item);
                }
                return item;
            }
        }

        public static StaffAutoLevelLog insertLog(StaffAutoLevelup item)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var staffLog = db.StaffAutoLevelLogs.Where(f => f.Staff_Id == item.Staff_Id).FirstOrDefault();
                    if (staffLog == null)
                    {
                        //Save report
                        staffLog = new StaffAutoLevelLog();
                        staffLog.Staff_Id = item.Staff_Id;
                        staffLog.Salon_Id = item.Salon_Id;
                        staffLog.StaffType_Id = item.StaffType_Id;
                        staffLog.SkillLevel_Id = item.SkillLevel_Id;
                        staffLog.SkillLevelup_Id = item.SkillLevelup_Id;
                        staffLog.Levelup_Id = item.Levelup_Id;
                        staffLog.sDate = item.sDate;
                        staffLog.Point = item.Point;
                        staffLog.KCS = item.KCS;
                        staffLog.Warning = item.Warning;
                        staffLog.TSL_DanhGia = item.TSL_DanhGia;
                        staffLog.StatusSkillLevel = item.StatusSkillLevel;
                        staffLog.Approval = item.Approval;
                        staffLog.CreatedDate = item.CreatedDate;
                        staffLog.ModifieldTime = item.ModifieldTime;
                        staffLog.IsCondition = item.IsCondition;
                        staffLog.IsDelete = item.IsDelete;
                        staffLog.IsLevelUp = item.IsLevelUp;
                        staffLog.IsApproval = item.IsApproval;
                        db.StaffAutoLevelLogs.Add(staffLog);
                        db.SaveChanges();
                        insertLog(item);
                    }
                    else
                    {
                        staffLog.Staff_Id = item.Staff_Id;
                        staffLog.Salon_Id = item.Salon_Id;
                        staffLog.StaffType_Id = item.StaffType_Id;
                        staffLog.SkillLevel_Id = item.SkillLevel_Id;
                        staffLog.SkillLevelup_Id = item.SkillLevelup_Id;
                        staffLog.Levelup_Id = item.Levelup_Id;
                        staffLog.sDate = item.sDate;
                        staffLog.Point = item.Point;
                        staffLog.KCS = item.KCS;
                        staffLog.Warning = item.Warning;
                        staffLog.TSL_DanhGia = item.TSL_DanhGia;
                        staffLog.StatusSkillLevel = item.StatusSkillLevel;
                        staffLog.Approval = item.Approval;
                        staffLog.CreatedDate = item.CreatedDate;
                        staffLog.ModifieldTime = item.ModifieldTime;
                        staffLog.IsCondition = item.IsCondition;
                        staffLog.IsDelete = item.IsDelete;
                        staffLog.IsLevelUp = item.IsLevelUp;
                        staffLog.IsApproval = item.IsApproval;
                        db.StaffAutoLevelLogs.AddOrUpdate(staffLog);
                        db.SaveChanges();
                    }
                  
                    return staffLog;
                }
            }
             catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        private void Bind_Skilllevel()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Store_StaffAutoLevel_BindSkillLevel().ToList();
                var Key = 0;

                SkillLevel.DataTextField = "SkillLevel";
                SkillLevel.DataValueField = "Id";

                ListItem item = new ListItem("Chọn bậc kỹ năng", "0");
                SkillLevel.Items.Insert(0, item);

                foreach (var v in lst)
                {
                    Key++;
                    item = new ListItem(v.skillName, v.skillId.ToString());
                    SkillLevel.Items.Insert(Key, item);
                }
                SkillLevel.SelectedIndex = 0;
            }
        }


        /// <summary>
        /// Lấy thông tin về nhân viên
        /// </summary>
        /// <returns></returns>
        protected List<cls_tbl_Levelup_DanhSach> getListData()
        {
            var data = new List<cls_tbl_Levelup_DanhSach>();
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    int integer;
                    this.SalonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
                    this.Skill = int.TryParse(SkillLevel.SelectedValue, out integer) ? integer : 0;
                    var _sData = db.Store_StaffAutoLevel_AdminApproval(this.SalonId, this.Skill).Where(w => w.TSL_DanhGia.Length > 1).ToList();
                    var item2 = new cls_tbl_Levelup_DanhSach();
                    if (_sData.Count > 0)
                    {
                        foreach (var v in _sData)
                        {
                            item2 = new cls_tbl_Levelup_DanhSach();
                            item2.staffName = v.staffName;
                            item2.staffId = v.staffId;
                            item2.salonName = v.salonName;
                            item2.typeName = v.typeName;
                            item2.skillName = v.skillName;
                            item2.skillId = v.skillId;
                            item2.skillNameup = v.skillupName;
                            item2.CreatedDate = String.Format("{0:dd/MM/yyyy}", v.sDate);
                            item2.Point = v.ratingPoint;
                            item2.KCS = v.KCS;
                            item2.Warning = v.Warning;
                            item2.TSL_DanhGia = v.TSL_DanhGia;
                            item2.ModifieldTime = v.ModifieldTime;
                            if (v.Approval == null)
                            {
                                item2.Approval = 1;
                            }
                            else
                            {
                                item2.Approval = (int)v.Approval;
                            }
                            if (v.StatusSkillLevel == null)
                            {
                                item2.StatusSkillLevel = false;
                            }
                            else
                            {
                                item2.StatusSkillLevel = (bool)v.StatusSkillLevel;
                            }
                            data.Add(item2);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return data;

        }

        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();
            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }
        /// <summary>
        /// Lấy dữ liệu truy vấn từ db qua sql store
        /// </summary>
        /// <returns></returns>
        private void bindData()
        {
            var data = getListData();
            Bind_Paging(data.Count);
            RptLevelup.DataSource = data.OrderByDescending(o => o.staffId).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
            RptLevelup.DataBind();
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "salonmanager" };
                string[] AllowAllData = new string[] { "root", "admin", "mode1", "accountant" };
                string[] AllowSalon = new string[] { "root", "admin", "salonmanager" };
                var Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }
                if (Array.IndexOf(AllowAllData, Permission) != -1)
                {
                    Perm_AllData = true;
                }
                if (Array.IndexOf(AllowSalon, Permission) != -1)
                {
                    Perm_ShowSalon = true;
                }
                Perm_Edit = true;
            }

            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }


        public class cls_tbl_Levelup_DanhSach
        {
            public int staffId { get; set; }
            public string staffName { get; set; }
            public string salonName { get; set; }
            public string typeName { get; set; }
            public string skillName { get; set; }
            public int? skillId { get; set; }
            public string skillNameup { get; set; }
            public string CreatedDate { get; set; }
            public double? Point { get; set; }
            public double? KCS { get; set; }
            public int? Warning { get; set; }
            public DateTime? ModifieldTime { get; set; }
            public string TSL_DanhGia { get; set; }
            public int Approval { get; set; }
            public bool StatusSkillLevel { get; set; }
        }
    }
}