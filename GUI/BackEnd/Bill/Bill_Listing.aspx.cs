﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Globalization;
using System.Web.Script.Serialization;
using ExportToExcel;
using Excel;
using LinqKit;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.UIBill
{
    public partial class Bill_Listing : System.Web.UI.Page
    {
        protected string PageID = "SUPER_HD";
        protected Paging PAGING = new Paging();
        protected SingleReportData _SingleReportData = new SingleReportData();
        protected List<List<ProductBasic>> ProductList;
        protected bool Perm_Access = false;
        protected bool Perm_Add = false;
        protected bool Perm_Edit = false;
        protected bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                Bind_Paging();
                Bind_DataByDate();
            }
        }

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        protected void Bind_DataByDate()
        {
            var LST = Get_DataByDate();
            RptBill.DataSource = LST;
            RptBill.DataBind();
        }

        protected List<BillService> Get_DataByDate()
        {
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                List<BillService> LST = new List<BillService>();
                var _BillService = new List<BillService>();
                var Where = PredicateBuilder.True<BillService>();
                string SgtValue;

                if (TxtDateTimeFrom.Text != "")
                {
                    DateTime _FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                    DateTime _ToDate;
                    if (TxtDateTimeTo.Text != "")
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                    }
                    else
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(1);
                    }
                    Where = Where.And(w => w.CreatedDate > _FromDate && w.CreatedDate < _ToDate);
                }

                switch (HDF_Suggestion_Field.Value)
                {
                    case "customer.code":
                        SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "customer.name":
                        SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "customer.phone":
                        SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                }

                Where = Where.And(w => w.IsDelete != 1);

                // Get LST to execute Total report
                var Count = 0;
                LST = db.BillServices.AsExpandable().Where(Where).OrderBy(o => o.Id)
                            .Skip(PAGING._Offset).Take(PAGING._Segment).ToList();

                if (LST.Count > 0)
                {
                    foreach (var v in LST)
                    {
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        // Print Product
                        if (v.ProductIds != null)
                        {
                            var ProductListThis = serializer.Deserialize<List<ProductBasic>>(v.ProductIds);
                            var _ProductName = "";
                            if (ProductListThis != null)
                            {
                                var i = 0;
                                var First = "";
                                foreach (var v2 in ProductListThis)
                                {
                                    First = i == 0 ? " class='first'" : "";
                                    _ProductName += "<tr" + First + ">" +
                                                        "<td class='col-xs-4'>" +
                                                                "<a href='admin/dich-vu/" + v2.Code + ".html' target='_blank'>" +
                                                                v2.Name +
                                                                "</a>" +
                                                        "</td>" +
                                                        "<td class='col-xs-4'>" + v2.Quantity + "</td>" +
                                                        "<td class='col-xs-4 no-bdr be-report-price'>" + (v2.Quantity * v2.Price) + "</td>" +
                                                    "</tr>";
                                    i++;
                                }
                                _ProductName = _ProductName.TrimEnd(',', ' ');
                            }
                            else
                            {
                                _ProductName += "<tr class='first'><td class='no-bdr'>-</td></tr>";
                            }
                            LST[Count].ProductNames = _ProductName;
                        }
                        else
                        {
                            var _ProductName = "<tr class='first'><td class='no-bdr'>-</td></tr>";
                            LST[Count].ProductNames = _ProductName;
                        }

                        // Print Service
                        if (v.ServiceIds != null)
                        {
                            var ServiceListThis = serializer.Deserialize<List<ProductBasic>>(v.ServiceIds);
                            var _ServiceName = "";
                            if (ServiceListThis != null)
                            {
                                var i = 0;
                                var First = "";
                                foreach (var v2 in ServiceListThis)
                                {
                                    First = i == 0 ? " class='first'" : "";
                                    _ServiceName += "<tr" + First + ">" +
                                                        "<td class='col-xs-4'>" +
                                                                "<a href='admin/dich-vu/" + v2.Code + ".html' target='_blank'>" +
                                                                v2.Name +
                                                                "</a>" +
                                                        "</td>" +
                                                        "<td class='col-xs-4'>" + v2.Quantity + "</td>" +
                                                        "<td class='col-xs-4 no-bdr be-report-price'>" + (v2.Quantity * v2.Price) + "</td>" +
                                                    "</tr>";
                                    i++;
                                }
                                _ServiceName = _ServiceName.TrimEnd(',', ' ');
                            }
                            else
                            {
                                _ServiceName += "<tr class='first'><td class='no-bdr'>-</td></tr>";
                            }
                            LST[Count].ServiceNames = _ServiceName;
                        }
                        else
                        {
                            var _ServiceName = "<tr class='first'><td class='no-bdr'>-</td></tr>";
                            LST[Count].ServiceNames = _ServiceName;
                        }

                        Count++;
                    }
                }
                return LST;
            }
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
            Bind_DataByDate();
            ScriptManager.RegisterStartupScript(UPTableListing, this.GetType(), "Format Price", "UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing();", true);
        }

        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? (HDF_Page.Value != "" ? Convert.ToInt32(HDF_Page.Value) : 1) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                CultureInfo culture = new CultureInfo("vi-VN");
                var Where = PredicateBuilder.True<BillService>();
                string SgtValue;

                if (TxtDateTimeFrom.Text != "")
                {
                    DateTime _FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(-1).AddHours(23).AddMinutes(59).AddSeconds(59).AddMilliseconds(999);
                    DateTime _ToDate;
                    if (TxtDateTimeTo.Text != "")
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
                    }
                    else
                    {
                        _ToDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture).AddDays(1);
                    }
                    Where = Where.And(w => w.CreatedDate > _FromDate && w.CreatedDate < _ToDate);
                }

                switch (HDF_Suggestion_Field.Value)
                {
                    case "customer.code":
                        SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "customer.name":
                        SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                    case "customer.phone":
                        SgtValue = HDF_Suggestion_Code.Value;
                        Where = Where.And(w => w.CustomerCode.Contains(SgtValue));
                        break;
                }

                Where = Where.And(w => w.IsDelete != 1);

                var Count = db.BillServices.AsExpandable().Count(Where);
                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        protected void Btn_SwitchExcel(object sender, EventArgs e)
        {
            if (HDF_ExcelType.Value == "Export")
            {
                //Exc_ExportExcel();
            }
            else if (HDF_ExcelType.Value == "Import")
            {
                //Exc_ImportExcel();
            }

        }

        protected void Exc_ExportExcel(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                Bind_Paging();
                var LST = Get_DataByDate();
                var _ExcelHeadRow = new List<string>();
                var serializer = new JavaScriptSerializer();
                var BillRowLST = new List<List<string>>();
                var BillRow = new List<string>();

                _ExcelHeadRow.Add("Ngày");
                _ExcelHeadRow.Add("Tên KH");
                _ExcelHeadRow.Add("Mã KH");
                _ExcelHeadRow.Add("Số ĐT");

                // Listing service
                var LST_Service = db.Services.OrderByDescending(o => o.Id).ToList();
                if (LST_Service.Count > 0)
                {

                    foreach (var v in LST_Service)
                    {
                        _ExcelHeadRow.Add(v.Name);
                    }
                    _ExcelHeadRow.Add("D.Thu Dịch vụ");
                }

                // Listing product
                var LST_Product = db.Products.OrderByDescending(o => o.Id).ToList();
                if (LST_Product.Count > 0)
                {
                    foreach (var v in LST_Product)
                    {
                        _ExcelHeadRow.Add(v.Name);
                    }
                    _ExcelHeadRow.Add("D.Thu Sản phẩm");
                }
                _ExcelHeadRow.Add("Tổng D.Thu");

                if (LST.Count > 0)
                {
                    var ListService_Quantity = new List<int>();
                    var ListProduct_Quantity = new List<int>();
                    var ServiceListThis = new List<ProductBasic>();
                    var ProductListThis = new List<ProductBasic>();
                    var Service_TotalMoney = 0;
                    var Product_TotalMoney = 0;
                    var TotalMoney = 0;

                    foreach (var v in LST)
                    {
                        Service_TotalMoney = 0;
                        Product_TotalMoney = 0;
                        TotalMoney = 0;
                        BillRow = new List<string>();

                        BillRow.Add(Convert.ToDateTime(v.CreatedDate).ToString("dd/MM/yyyy"));
                        BillRow.Add(v.CustomerName);
                        BillRow.Add(v.CustomerCode);
                        BillRow.Add(v.CustomerPhone);

                        if (v.ServiceIds != null)
                        {
                            ServiceListThis = serializer.Deserialize<List<ProductBasic>>(v.ServiceIds);
                        }

                        if (v.ProductIds != null)
                        {
                            ProductListThis = serializer.Deserialize<List<ProductBasic>>(v.ProductIds);
                        }

                        if (LST_Service.Count > 0)
                        {
                            foreach (var v2 in LST_Service)
                            {
                                if (ServiceListThis != null)
                                {
                                    var ExistId = ServiceListThis.Find(x => x.Id == v2.Id);
                                    if (ExistId.Code != null)
                                    {
                                        Service_TotalMoney += ExistId.Quantity * ExistId.Price;
                                        BillRow.Add(ExistId.Quantity.ToString());
                                    }
                                    else
                                    {
                                        BillRow.Add("0");
                                    }
                                }
                                else
                                {
                                    BillRow.Add("0");
                                }
                            }
                        }

                        BillRow.Add(Service_TotalMoney.ToString());

                        if (LST_Product.Count > 0)
                        {
                            foreach (var v2 in LST_Product)
                            {
                                if (ProductListThis != null)
                                {
                                    var ExistId = ProductListThis.Find(x => x.Id == v2.Id);
                                    if (ExistId.Code != null)
                                    {
                                        Product_TotalMoney += ExistId.Quantity * ExistId.Price;
                                        BillRow.Add(ExistId.Quantity.ToString());
                                    }
                                    else
                                    {
                                        BillRow.Add("0");
                                    }
                                }
                                else
                                {
                                    BillRow.Add("0");
                                }
                            }
                        }

                        TotalMoney = Service_TotalMoney + Product_TotalMoney;

                        BillRow.Add(Product_TotalMoney.ToString());
                        BillRow.Add(TotalMoney.ToString());
                        BillRowLST.Add(BillRow);
                    }
                }

                // export
                var ExcelStorePath = Server.MapPath("~") + "Public/Excel/Hoa.Don/";
                var FileName = "Hoa_Don_" + string.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
                var UrlFileName = "http://" + Request.Url.Host + "/Public/Excel/Hoa.Don/" + FileName;

                ExportXcel(_ExcelHeadRow, BillRowLST, ExcelStorePath + FileName);
                ScriptManager.RegisterStartupScript(UPExcel, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "');", true);
            }
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                //ContentWrap.Visible = false;
                //NotAllowAccess.Visible = true;
            }
        }
        protected void Exc_ImportExcel()
        {
            var _ExcelHeadRow = new List<string>();
            _ExcelHeadRow.Add("STT");
            _ExcelHeadRow.Add("Field1");
            _ExcelHeadRow.Add("Field2");
            _ExcelHeadRow.Add("Field3");

            var LST = new List<Tuple<int, string, string, string>>();

            for (var i = 0; i < 5; i++)
            {
                var v = Tuple.Create(i, "str1-" + i, "str2-" + i, "str3-" + i);
                LST.Add(v);
            }

            // import
            var Path = Server.MapPath("~") + "Public/Excel/Hoa.Don/" + "Hoa_Don_28_07_2015_06_28_13.xlsx";
            Response.Write(Path);
            ImportXcel(Path);
        }

        private void ExportXcel(List<string> _ExcelHeadRow, List<List<string>> List, string Path)
        {
            try
            {
                CreateExcelFile.ExcelHeadRow = _ExcelHeadRow;
                CreateExcelFile.CreateExcel(List, Path);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi. Không tạo được file excel.\r\nException: " + ex.Message);
            }
        }

        private void ImportXcel(string Path)
        {
            var tblDom = "<table><tbody>";
            foreach (var worksheet in Workbook.Worksheets(@Path))
                foreach (var row in worksheet.Rows)
                {
                    tblDom += "<tr>";
                    foreach (var cell in row.Cells)
                    {
                        if (cell != null)
                        {
                            tblDom += "<td>" + cell.Text + "</td>";
                        }
                    }
                    tblDom += "<tr>";
                }
            // if (cell != null) // Do something with the cells
            tblDom += "</tbody></table>";
            Response.Write(tblDom);
        }

    }

    public struct SingleReportData
    {
        public int Total_Bills { get; set; }
        public int Total_Money { get; set; }
        public int Money_Services { get; set; }
        public int Money_Products { get; set; }
        public string ProductNames { get; set; }
    }

    public struct BillStruct
    {
        public string Date { get; set; }
        public string CustomerName { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerPhone { get; set; }
        public List<string> ServiceName { get; set; }
        public List<string> ProductName { get; set; }
        public List<int> ServiceValue { get; set; }
        public List<int> ProductValue { get; set; }
        public int TotalMoneyService { get; set; }
        public int TotalMoneyProduct { get; set; }
        public int TotalMoney { get; set; }
    }
}