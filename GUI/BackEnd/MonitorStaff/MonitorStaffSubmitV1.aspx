﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MonitorStaffSubmitV1.aspx.cs" MasterPageFile="~/TemplateMaster/SiteMasterVersion1.Master" Inherits="_30shine.GUI.BackEnd.MonitorStaff.MonitorStaffSubmitV1" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <title>Giám sát nhân viên - Báo cáo</title>
</asp:Content>

<asp:Content ID="MonitorStaffReport" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel ID="ContentWrap" runat="server" ClientIDMode="Static" Style="background-color: #F7F7F7">
        <link href="/Assets/css/erp.30shine.com/SemanticUI/dist/semantic.min.css" rel="stylesheet" />
        <script src="/Assets/css/erp.30shine.com/SemanticUI/dist/semantic.min.js"></script>
        <!-- calender -->
        <link href="https://cdn.rawgit.com/mdehoog/Semantic-UI-Calendar/76959c6f7d33a527b49be76789e984a0a407350b/dist/calendar.min.css"
            rel="stylesheet" type="text/css" />
        <script src="https://cdn.rawgit.com/mdehoog/Semantic-UI-Calendar/76959c6f7d33a527b49be76789e984a0a407350b/dist/calendar.min.js"></script>

        <!-- my js -->
        <script src="/Assets/js/erp.30shine.com/monitor-staff/common.js?v=435345345"></script>
        <script src="/Assets/js/erp.30shine.com/monitor-staff/config.js?v=313213123"></script>

        <div class="container-fluid sub-menu">
            <div class="row bg-white">
                <div class="col-md-12 pt-3 pb-2">
                    <ul>
                        <li>Quản lý giám sát nhân viên &nbsp;&#187;</li>
                        <li><a href="/bao-cao-giam-sat-nhan-vien-v1.html"><i class="fa fa-list mr-1" aria-hidden="true"></i>Báo cáo giám sát nhân viên</a></li>
                        <li><a href="/bao-cao-thong-ke-giam-sat-nhan-vien.html"><i class="fa fa-list mr-1" aria-hidden="true"></i>Báo cáo thống kê lỗi giám sát nhân viên</a></li>
                        <li><a href="/them-moi-loi-giam-sat-nhan-vien-v1.html"><i class="far fa-file mr-1"></i>Thêm mới lỗi giám sát nhân viên</a></li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <div class="sub-menu-boder"></div>
                </div>
            </div>
        </div>
        <form runat="server">
            <div class="monitor-staff-page container-fluid">
                <div class=" title head">
                    <h4></h4>
                </div>

                <div id="page-monitor-staff">
                    <div class="inverted">
                        <div class="row mt-3">
                            <div class="col-auto form-group">
                                <div class=" field salon-error">
                                    <label class="font-weight-bold">Salon giám sát</label>
                                    <div class="col-xs-2 wr-search position-relative">
                                        <div class=" input full-width">
                                            <select class="ui fluid search dropdown ddlSalon">
                                                <option value='0' selected='selected'>Chọn salon</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-auto form-group">
                                <div class=" field salon-error">
                                    <label class="font-weight-bold">Vị trí</label>
                                    <div class="col-xs-2 wr-search position-relative">
                                        <div class=" input full-width">
                                            <select class="ui fluid search dropdown ddlDepartment">
                                                <option value='0'>Chọn vị trí</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="col-auto form-group">
                                <div class=" field salon-error">
                                    <label class="font-weight-bold">Thời gian mắc lỗi</label>
                                    <div class="col-xs-2 wr-search position-relative">
                                        <div class=" input full-width ui field time-error" style="width: 100% !important">
                                            <div class="ui calendar field date" style="width: 50% !important">
                                                <div class="ui input full-width" style="width: 90% !important;">
                                                    <input class="error-date" readonly="readonly" placeholder="ngày:tháng:năm" style="padding: 6px;" />
                                                </div>
                                            </div>
                                            <div class="ui calendar field time" style="width: 50% !important">
                                                <div class="ui input full-width" style="width: 100% !important">
                                                    <input class="error-time" data-id="" type="text" style="margin-left: 20px; padding: 6px;"
                                                        placeholder="giờ:phút (mặc định là thời điểm submit)" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-auto form-group">
                                <div class=" field salon-error">
                                    <label class="font-weight-bold">Hình thức xử lý</label>
                                    <div class="col-xs-2 wr-search position-relative">
                                        <div class=" input full-width">
                                            <select class="ui fluid search dropdown ddlErrorHandle" onchange="ErrorName()">
                                                <option value='0'>Chọn hình thức</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-auto form-group">
                                <div class=" field salon-error">
                                    <label class="font-weight-bold">Loại lỗi</label>
                                    <div class="col-xs-2 wr-search position-relative">
                                        <div class=" input full-width">
                                            <select class="ui fluid search dropdown ddlTypeErrors" onchange="ErrorName()">
                                                <option value='0'>Chọn kiểu lỗi</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-auto form-group">
                                <div class=" field salon-error">
                                    <label class="font-weight-bold">Tên lỗi</label>
                                    <div class="col-xs-2 wr-search position-relative error-name">
                                        <div class=" input full-width">
                                            <select class="ui fluid search dropdown ddlErrorName">
                                                <option value='0'>Chọn tên lỗi</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-auto form-group" style="width: 100% !important">
                                <div class=" field salon-error">
                                    <label class="font-weight-bold">Ghi chú</label>
                                    <div class="col-xs-2 wr-search position-relative">
                                        <div class=" input full-width">
                                            <textarea rows="4" style="width: 100%; border-radius: 5px; padding-left: 10px; padding-top: 5px;" class="txtNote" placeholder="Nhập ghi chú"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-auto form-group" style="width: 100% !important">
                                <div class=" field salon-error">
                                    <label class="font-weight-bold">Hình ảnh lỗi (<i>Có thể lựa chọn nhiều file ảnh</i>): </label>
                                    <div class="ui secondary button choose-image" style="margin-left: 20px;">Upload</div>
                                    <div class="col-xs-2 wr-search position-relative">
                                        <div class=" input full-width" id="upload-image">
                                            <div>
                                                <input class="hide" style="display: none" id="files" type="file" multiple />
                                            </div>
                                            <output id="result" />
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="col-auto form-group">
                                <div class=" field salon-error">
                                    <label class="font-weight-bold"></label>
                                    <div class="col-xs-2 wr-search position-relative error-name">
                                        <div class=" input full-width">
                                            <input type="button" class="ui secondary button" onclick="OnSubmit()" value="Submit" id="submit-form" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <asp:HiddenField ID="HDF_UserId" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HDF_UserName" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HDF_UserRole" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HDF_SalonId" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HDF_SalonName" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HDF_PermisionViewAllSalon" runat="server" ClientIDMode="Static" />
        </form>

        <style>
            body {
                font-family: Roboto Condensed Regular;
            }

            #upload-image {
                margin: 0 auto;
                margin-top: 20px;
                border: 2px #cccccc;
                border-style: dashed;
                width: 100%;
                height: 250px;
                border-radius: 3px;
                background-color: ghostwhite;
                /*text-align: center;*/
            }

                #upload-image #result div {
                    max-height: 100%;
                    float: left;
                    display: inline-block;
                }

                    #upload-image #result div .close-img span {
                        width: 25px;
                        height: 20px;
                        border: 1px solid #ccc;
                        text-align: center;
                        padding-bottom: 21px;
                        border-radius: 15px;
                        background: #fff;
                        position: absolute;
                        margin-left: 5px;
                        top: 5px;
                        z-index: 99999;
                    }

                #upload-image #result img {
                    max-height: 245px;
                }
            /* Preview */
            .preview {
                width: 100px;
                height: 100px;
                border: 1px solid black;
                margin: 0 auto;
                background: white;
            }

                .preview img {
                    display: none;
                }
            /* Button */
            .button {
                border: 0px;
                background-color: deepskyblue;
                color: white;
                padding: 5px 15px;
                margin-left: 10px;
            }

            .col-auto.form-group {
                width: 33.33%;
                min-width: 20%;
            }

            .font-weight-bold {
                font-family: Roboto Condensed bold;
            }

            .wr-search .result, .dropdown.result {
                overflow-y: scroll;
                max-height: 400px;
                display: none;
            }

            .wr-search .result, .dropdown.result {
                position: absolute;
                background: white;
                z-index: 1000;
                top: 115%;
                width: 100%;
                border: 1px solid #d0d0d0;
            }

                .wr-search .result .item, .dropdown.result .item {
                    padding: 5px 10px;
                }

                .wr-search .result div, .dropdown.result div {
                    color: black;
                }

            .form-control, .btn {
                font-size: inherit !important;
            }

            .menu img.image {
                width: 100px !important;
            }

            .custom.popup {
                display: none;
                width: 1000px;
                position: absolute;
                z-index: 100;
                bottom: 0;
                left: 107%;
            }

                .custom.popup .column .image {
                    width: 100%;
                    box-shadow: 0px 0px 0px 5px rgba(0,0,0,0.1);
                    border-radius: 1px;
                }

                .custom.popup .column {
                    float: left;
                    width: 49%;
                    margin-right: 1%;
                }

                    .custom.popup .column img {
                        width: 100%;
                    }

            .ui.menu:hover .custom.popup {
                display: block !important;
            }

            .buttons-excel.buttons-html5 {
                margin-bottom: 10px;
                float: left;
            }

            #tableListing_length {
                float: right;
            }
        </style>
        <script>
            var rootSocketServer = window.consts.rootSocketServer;
            var url = apis.monitorStaffService.domain;
            var elms = {
                wrSearch: '.wr-search',
                calendarDate: '.calendar.date',
                calendarTime: '.calendar.time',
                inputSearch: '.wr-search input',
                inputSearchCategoryErrorHandle: '.category-error-handle .wr-search input',
                inputSearchCategoryErrorSub: '.category-error-sub .wr-search input',
                inputSearchCategoryErrorRoot: '.category-error-root .wr-search input',
                inputSearchStaffReport: '.staff-report .wr-search input',
                inputSearchSalonError: '.salon-error .wr-search input',
                inputSearchDepartmentError: '.departmenr-error-sub .wr-search input',
                inputTimeError: '.time-error .time input',
                inputDateError: '.time-error .date input',
                resultSearch: '.dropdown.result',
                inputNote: '.note input',
                submit: '.button.submit',
                btnClearImage: '.evidence-error .clear',
                btnChooseImage: '.choose-image',
                inputFile: '#files'
            };
            $(document).ready(function () {
                $(elms.inputDateError).val(window.getDate(new Date()));
                //$(".ddlTypeErrors").attr("disabled", "disabled");
                $(".dropdown").select2();
                GetSalon();
                GetDepartment();
                ErrorHandle();
                TypeError();
                BindImage();
                //ErrorName();
            })
            //choose image
            $(elms.btnChooseImage).click(function () {
                console.log("choose image");
                $(elms.inputFile).click();

            });
            $(elms.calendarTime).calendar(
                {
                    ampm: false,
                    type: 'time'
                });
            function GetSalon() {

                $.ajax({
                    type: "GET",
                    url: url+"/api/monitor-staff/salon-area?staffId=" + $("#HDF_UserId").val() + "&isAllSalon=" + $("#HDF_PermisionViewAllSalon").val(),
                    dataType: "json",
                    contentType: "application/json;charset:utf-8",
                    success: function (data) {
                        console.log(data);
                        $(".dropdown.ddlSalon").empty();
                        var salon = "<option value='0' selected='selected'>Chọn salon</option>";
                        for (var i = 1; i < data.length; i++) {
                            salon += "<option value='" + data[i].id + "'>" + data[i].name + "</option>";
                        }
                        $(".dropdown.ddlSalon").append(salon);
                        //$(".ddlSalon").dropdown("get value");
                    }

                })
            }

            function GetDepartment() {
                $.ajax({
                    type: "GET",
                    url: url+"/api/monitor-staff/staff-department-V1",
                    dataType: "json",
                    contentType: "application/json;charset:utf-8",
                    success: function (data) {
                        console.log(data);
                        $(".dropdown.ddlDepartment").empty();
                        var department = "<option value='0'>Chọn vị trí</option>";
                        for (var i = 0; i < data.length; i++) {
                            department += "<option value='" + data[i].id + "'>" + data[i].name + "</option>";
                        }
                        $(".dropdown.ddlDepartment").append(department);
                        //$(".ddlDepartment").dropdown("get value");
                    }

                })
            }

            function ErrorHandle() {
                $.ajax({
                    type: "GET",
                    url: url+"/api/monitor-staff/error-hanle",
                    dataType: "json",
                    contentType: "application/json;charset:utf-8",
                    success: function (data) {
                        console.log(data);
                        $(".dropdown.ddlErrorHandle").empty();
                        var department = "<option value='0'>Chọn hình thức xử lý</option>";
                        for (var i = 0; i < data.length; i++) {
                            department += "<option value='" + data[i].id + "'>" + data[i].name + "</option>";
                        }
                        $(".dropdown.ddlErrorHandle").append(department);
                        //$(".ddlErrorHandle").dropdown("get value");
                    }

                })
            }

            function TypeError() {
                $.ajax({
                    type: "GET",
                    url: url+"/api/monitor-staff/category-error/root",
                    dataType: "json",
                    contentType: "application/json;charset:utf-8",
                    success: function (data) {
                        console.log(data);
                        $(".dropdown.ddlTypeErrors").empty();
                        var department = "<option value='0'>Chọn loại lỗi</option>";
                        for (var i = 0; i < data.length; i++) {
                            department += "<option value='" + data[i].id + "'>" + data[i].name + "</option>";
                        }
                        $(".dropdown.ddlTypeErrors").append(department);
                        //$(".ddlTypeErrors").dropdown("get value");
                    }

                })
            }
            function ErrorName() {
                $(".error-name").find("span.select2-selection__rendered").text("Chọn tên lỗi");
                $(".dropdown.ddlErrorName").empty();
                var handleErrorId = $(".ddlErrorHandle").val();
                var typeError = $(".ddlTypeErrors").val();
                var department = "";
                if ((handleErrorId == 0 || handleErrorId == null) && (typeError == 0 || typeError == null)) {
                    return false;
                }
                //$(".select2-selection__rendered").empty();
                $.ajax({
                    type: "GET",
                    url: url+"/api/monitor-staff/category-error/" + handleErrorId + "/" + typeError,
                    dataType: "json",
                    contentType: "application/json;charset:utf-8",
                    success: function (data) {
                        console.log(data);

                        department += "<option value='0'>Chọn tên lỗi</option>";
                        for (var i = 0; i < data.length; i++) {
                            department += "<option value='" + data[i].id + "'>" + data[i].name + "</option>";
                        }
                        $(".dropdown.ddlErrorName").append(department);
                        //$(".ddlErrorName").dropdown("get value");
                    }

                })
            }

            function BindImage() {
                //Check File API support
                if (window.File && window.FileList && window.FileReader) {
                    $('#files').on("change", function (event) {
                        var files = event.target.files; //FileList object
                        var output = document.getElementById("result");
                        for (var i = 0; i < files.length; i++) {
                            var file = files[i];
                            //Only pics
                            // if(!file.type.match('image'))
                            console.log(file);
                            if (file.type.match('image.*')) {
                                if (this.files[0].size > 2097152) {
                                    //resize images
                                    // imageData =
                                }
                                // continue;
                                var picReader = new FileReader();
                                picReader.addEventListener("load", function (event) {
                                    var picFile = event.target;
                                    var imageData = picFile.result;
                                    // console.log(imageData);

                                    var div = document.createElement("div");
                                    div.innerHTML = "<img class='thumbnail' width='300px' src='" + imageData + "'" +
                                        "title='preview image'/><div class='close-img'><span class='close-imgs' onclick='CloseImage($(this))'>x</span></div";

                                    console.log(div);
                                    // if (width > maxWidth) {
                                    //     ratio = maxWidth / width;
                                    //     $(that).css('width', maxWidth);
                                    //     $(that).css('height', height * ratio);

                                    // }

                                    // if (height > maxHeight) {
                                    //     ratio = maxHeight / height;
                                    //     $(that).css('height', maxHeight);
                                    //     $(that).css('width', width * ratio);

                                    // }
                                    output.insertBefore(div, null);
                                });
                                //Read the image
                                $('#result').show();
                                picReader.readAsDataURL(file);

                            } else {
                                alert("You can only upload image file.");
                                $(this).val("");
                            }
                        }

                    });
                } else {
                    console.log("Your browser does not support File API");
                }
            };
            //socket io
            function pushNoti(salonId) {
                var pushData = { key_event: 'app_monitor_' + salonId, push_data: "ok" };
                $.ajax({
                    contentType: "application/json",
                    type: "POST",
                    url: rootSocketServer + "/api/pushNotice",
                    dataType: "json",
                    async: true,
                    crossDomain: true,
                    data: JSON.stringify(pushData),
                    beforeSend: function () {
                        //window.waitDialogShow();
                    },
                    complete: function () {
                        //window.waitDialogHide();
                    },
                    success: function (response, textStatus, xhr) {
                        console.log("success");
                    },
                    error: function (jqXhr, textStatus, errorMessager) {
                        console.log(errorMessager + "\n" + jqXhr.responseJSON);
                    }
                });
            }
            //s3
            function uploadImagesToS3(data) {
                // console.log(JSON.stringify(data));
                let result = '';
                let url = window.apis.monitorStaffService.domainS3 + window.apis.monitorStaffService.multiUpload;
                $.ajax({
                    contentType: "application/json",
                    type: "POST",
                    url: url,
                    dataType: "json",
                    async: false,
                    crossDomain: true,
                    data: JSON.stringify(data),
                    success: function (response, textStatus, xhr) {
                        if (response) {
                            result = response;
                        }
                    },
                    error: function (jqXhr, textStatus, errorMessager) {
                        console.log(errorMessager + "\n" + jqXhr.responseJSON);
                        alert("upload images lên S3 bị lỗi. Bạn hãy thông báo với Admin");
                    }
                });
                console.log(result);
                return result;
            }

            //$('#files').on("click", function () {
            //    console.log("click fields");
            //    $('.thumbnail').parent().remove();
            //    $('result').hide();
            //    $(this).val("");
            //});
            function CloseImage(This) {
                var pr = This.parent().parent();
                pr.remove();
            }
            function OnChange() {
                $(".ddlTypeErrors").removeAttr("disabled");
            }

            function OnSubmit() {
                debugger;
                let data = {
                    erpErrorHandleId: $(".ddlErrorHandle").val(),
                    reporterId: $("#HDF_UserId").val(),
                    salonId: $(".ddlSalon").val(),
                    departmentId: $(".ddlDepartment").val(),
                    images: '',
                    categoryErrorRootId: $(".ddlTypeErrors").val(),
                    categoryErrorSubId: $(".ddlErrorName").val(),
                    noteByReporter: $(".txtNote").val(),
                    errorTime: $(".error-date").val() + " " + $(".error-time").val() + ":00"
                }

                if (data.salonId == 0 || data.departmentId == 0 || data.erpErrorHandleId == 0 || data.categoryErrorRootId == 0 || data.categoryErrorSubId == 0) {
                    ShowMessage("Thông báo", "Dữ liệu đầu vào không chính xác. Vui lòng kiểm tra lại để chắc chắn bạn đã nhập đúng dữ liệu", 4);
                    return false;
                }
                let arrImage = [];
                $('#result img').each(function (i, v) {
                    let base64 = $(v).attr('src');
                    //let now = new Date();
                    //let date = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();
                    let item = {
                        img_name: "MonitorStaffError" + data.reporterId.replace(/[^\w\s]/gi, '') + "_" + data.salonId.replace(/[^\w\s]/gi, '') + "_" + Date.now(),
                        img_base64: base64
                    };
                    arrImage.push(item);
                });

                let dataImages = {
                    images: arrImage
                };

                console.log(dataImages);
                //valid images
                if (!dataImages.images) {
                    alert("Bạn hãy thêm ảnh bằng chứng");
                    return;
                }
                //post to s3
                let resultUpload = uploadImagesToS3(dataImages);

                let arrLinks = [];
                $.each(resultUpload.images, function (i, v) {
                    // console.log('image', v);
                    let link = v.link;
                    if (link) {
                        arrLinks.push(link);
                    }
                });
                //set data images
                data.images = arrLinks.join();
                //console.log(data.images);
                //submit data
                submit(data);
            }
            //submit
            function submit(data) {
                console.log('submit', data);

                let url = window.apis.monitorStaffService.domain + window.apis.monitorStaffService.staffError;

                $.ajax({
                    contentType: "application/json",
                    type: "POST",
                    url: url,
                    dataType: "json",
                    async: false,
                    crossDomain: true,
                    data: JSON.stringify(data),
                    beforeSend: function () {
                        //window.waitDialogShow();
                    },
                    complete: function () {
                        //window.waitDialogHide();
                    },
                    success: function (response, textStatus, xhr) {
                        console.log("success");
                        //window.parent.location = "/index.html";
                        alert("submit thành công!");
                        if (data.salonId !== 0) pushNoti(0);
                        pushNoti(data.salonId);

                        location.reload();
                        //clear old input
                        clearOldInput();

                        //window.waitDialogHide();
                    },
                    error: function (jqXhr, textStatus, errorMessager) {
                        console.log(errorMessager + "\n" + jqXhr.responseJSON);
                        //window.waitDialogHide();
                        alert("Submit bị lỗi. Bạn hãy thông báo với Admin");
                    }
                });
            }
        </script>
    </asp:Panel>
</asp:Content>
