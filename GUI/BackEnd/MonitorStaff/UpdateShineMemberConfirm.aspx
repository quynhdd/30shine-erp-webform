﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UpdateShineMemberConfirm.aspx.cs" MasterPageFile="~/TemplateMaster/SiteMaster.Master" Inherits="_30shine.GUI.BackEnd.MonitorStaff.UpdateShineMemberConfirm" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .btn-check-valid {
                width: 20%;
                background: #3a3939;
                color: #ffffff;
            }

            .btn-send {
                background: #3a3939;
                color: #ffffff;
            }

            select.form-control {
                width: 90% !important;
            }

            .confirm, .cancel {
                width: 40%;
            }
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Cập nhật Shine Member &nbsp;&#187; </li>
                        <li class="li-add"><a href="/yeu-cau-cap-nhat-shine-member.html">Thêm mới yêu cầu</a></li>
                        <li class="li-listing active"><a href="#">Danh sách yêu cầu</a></li>

                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row">
                    <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                    <div class="filter-item">
                        <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                            ClientIDMode="Static" runat="server" AutoCompleteType="Disabled" Width="190px"></asp:TextBox>
                    </div>
                    <strong class="st-head" style="margin-left: 10px;">
                        <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                    </strong>
                    <div class="filter-item">
                        <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeTo" placeholder="Đến ngày"
                            ClientIDMode="Static" runat="server" AutoCompleteType="Disabled" Width="190px"></asp:TextBox>
                    </div>

                </div>
                <div class="row row-filter">
                    <strong class="st-head" style="margin-right: 13px"><i class="fa fa-filter"></i>Lọc KQ</strong>

                    <div class="filter-item">
                        <asp:DropDownList ID="ddlConfirm" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px !important;">
                            <asp:ListItem Value="99">Chọn trạng thái</asp:ListItem>
                            <asp:ListItem Value="0">Chờ duyệt</asp:ListItem>
                            <asp:ListItem Value="1">Duyệt</asp:ListItem>
                            <asp:ListItem Value="2">Không duyệt</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlChange" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="margin: 5px 24px !important; width: 190px !important;">
                            <asp:ListItem Value="99">Chọn kết quả</asp:ListItem>
                            <asp:ListItem Value="1">Thay đổi</asp:ListItem>
                            <asp:ListItem Value="2">Hủy</asp:ListItem>
                        </asp:DropDownList>
                    </div>

                    <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <%--<a href="/khach-hang/danh-sach.html" class="st-head btn-viewdata">Reset Filter</a>--%>
                </div>
                <!-- End Filter -->
                <!-- Row Table Filter -->
                <div class="table-func-panel">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="200">200</li>
                                <%--<li data-value="1000000">Tất cả</li>--%>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>

                        <div class="table-wp">
                            <table class="table-add table-listing">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Số ĐT cũ</th>
                                        <th>Tên KH</th>
                                        <th>Bill ID</th>
                                        <th>Số ĐT mới</th>
                                        <th>Người đề nghị</th>
                                        <th>Ghi chú của Telesale</th>
                                        <th>Trạng thái</th>
                                        <th>Lý do</th>

                                        <th>Kết quả</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="RptCustomer" runat="server" OnItemDataBound="Repeater_ItemDataBound">
                                        <ItemTemplate>
                                            <tr>
                                                <td class="index" data-id="<%# Eval("Id") %>"><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td><a href="/khach-hang/<%# Eval("OldCustomerId") %>.html" target="_blank" data-id="<%# Eval("OldCustomerId") %>" data-phone="<%# Eval("OldNumber") %>"><%# Eval("OldNumber") %></a></td>
                                                <td><%# Eval("OldCustomerName") %></td>
                                                <td><a href="/dich-vu/<%# Eval("BillId") %>.html" target="_blank"><%# Eval("BillId") %></a></td>
                                                <td><%# Eval("NewNumber") %></td>
                                                <td><%# Eval("UserRequireId") + " | " +Eval("UserRequireName") %></td>
                                                <td>
                                                    <textarea class="form-control note-by-telesale" rows="4" placeholder="Nhập ghi chú tại đây"><%# Eval("TelesaleNote") == null ? "" : Eval("TelesaleNote").ToString() %></textarea>
                                                </td>
                                                <td>
                                                    <asp:Panel runat="server" ID="ConfirmPanel" ClientIDMode="Static">
                                                        <select class="form-control status" onchange="OnchangeConfirm($(this))">
                                                            <option value="0">Chọn trạng thái</option>
                                                            <option value="1" <%# Convert.ToInt32(Eval("Confirm")) == 1 ? "selected='selected'" :"" %>>Duyệt</option>
                                                            <option value="2" <%# Convert.ToInt32(Eval("Confirm")) == 2 ? "selected='selected'" :"" %>>Không duyệt</option>
                                                        </select>
                                                    </asp:Panel>
                                                </td>
                                                <td>
                                                    <select class="form-control reason-revoke" <%# Convert.ToInt32(Eval("Confirm")) == 2 ? "" : "style='display:none'" %> onchange="ConfirmNotOk($(this))">
                                                        <option value="0">Chọn lý do</option>
                                                        <option value="1" <%# Convert.ToInt32(Eval("ReasonId")) == 1 ? "selected='selected'" : "" %>>Sai khách</option>
                                                        <option value="2" <%# Convert.ToInt32(Eval("ReasonId")) == 2 ? "selected='selected'" : "" %>>Số không có thực</option>
                                                        <option value="3" <%# Convert.ToInt32(Eval("ReasonId")) == 3 ? "selected='selected'" : "" %>>Máy bận</option>
                                                        <option value="4" <%# Convert.ToInt32(Eval("ReasonId")) == 4 ? "selected='selected'" : "" %>>Cần check lại</option>
                                                    </select>
                                                </td>

                                                <td>
                                                    <asp:Panel runat="server" ID="UpdatePanel" ClientIDMode="Static">
                                                        <input type="button" class="form-control btn btn-info confirm" data-id="1" <%#Convert.ToInt32(Eval("Status")) != 0 || Convert.ToInt32(Eval("Confirm")) != 1 ? "disabled='disabled'" : "" %> onclick="UpdateShine($(this))" value="Xác nhận" />
                                                        <input type="button" class="form-control btn btn-warning cancel" data-id="0" <%#Convert.ToInt32(Eval("Status")) != 0 || Convert.ToInt32(Eval("Confirm")) == 0 ? "disabled='disabled'" : "" %> onclick="UpdateShine($(this))" value="Hủy" />
                                                    </asp:Panel>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPagingInPage(1)">Đầu</a>
                                <a href="javascript://" onclick="excPagingInPage(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPagingInPage(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPagingInPage(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPagingInPage(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <%--<asp:Button ID="BtnFakeExcel" ClientIDMode="Static" runat="server" OnClick="Btn_SwitchExcel" Style="display: none;" />--%>
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_ExcelType" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
            </div>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });
            })

            //function GetReasonRevoke_1() {
            //    $.ajax({
            //        url: "/GUI/BackEnd/MonitorStaff/UpdateShineMemberConfirm.aspx/GetReason_1",
            //        //data: "{id:" + id + "}",
            //        type: "POST",
            //        contentType: "application/json;charset:utf-8",
            //        dataType: "json",
            //        success: function (response) {
            //            debugger;
            //            $(".form-control.reason-revoke").empty();
            //            var jsonData = JSON.parse(response.d);
            //            var option = "<option value='0'>Chọn lý do</option>";
            //            for (var i = 0; i < jsonData.length; i++) {

            //                option += "<option value='" + jsonData[i].Value + "' " + (jsonData[i].ReasonId != 0 ? "selected='selected'" : "") + ">" + jsonData[i].ReasonName + "</option>";
            //            }
            //            $(".form-control.reason-revoke").append(option);
            //        }
            //    })
            //}
            //function GetReasonRevoke(This) {

            //    This.closest("tr").find(".form-control.reason-revoke").removeAttr("style");
            //    var id = This.closest("tr").find(".index").attr("data-id");
            //    $.ajax({
            //        url: "/GUI/BackEnd/MonitorStaff/UpdateShineMemberConfirm.aspx/GetReason",
            //        data: "{id:" + id + "}",
            //        type: "POST",
            //        contentType: "application/json;charset:utf-8",
            //        dataType: "json",
            //        success: function (response) {
            //            This.closest("tr").find(".form-control.reason-revoke").empty();
            //            var jsonData = JSON.parse(response.d);
            //            var option = "<option value='0'>Chọn lý do</option>";
            //            for (var i = 0; i < jsonData.length; i++) {
            //                option += "<option value='" + jsonData[i].Value + "' " + (jsonData[i].ReasonId != 0 ? "selected='selected'" : "") + ">" + jsonData[i].ReasonName + "</option>";
            //            }
            //            This.closest("tr").find(".form-control.reason-revoke").append(option);
            //        }
            //    })
            //}
            function OnchangeConfirm(This) {
                if (This.val() == 0) {
                    return;
                }
                if (This.closest("tr").find(".note-by-telesale").val() == "") {
                    ShowMessage("Thông báo", "Vui lòng ghi lại note", 4);
                    This.closest("tr").find(".note-by-telesale").focus();
                    This.closest("tr").find(".note-by-telesale").css("border-color", "pink");
                    This.val(0);
                    return;
                }
                
                if (This.val() == 2) {
                    //GetReasonRevoke(This);
                    This.closest("tr").find(".confirm").attr("disabled", "disabled");
                    This.closest("tr").find(".form-control.reason-revoke").removeAttr("style");
                    return;
                }
                //This.closest("tr").find(".form-control.confirm").removeAttr("disabled");
                This.closest("tr").find(".form-control.cancel").removeAttr("disabled");
                This.closest("tr").find(".form-control.reason-revoke").css("display", "none");
                ConfirmOk(This);
            }
            function ConfirmNotOk(This) {
                //This.closest("tr").find(".form-control.confirm").removeAttr("disabled");
                This.closest("tr").find(".form-control.cancel").removeAttr("disabled");
                var id = This.closest("tr").find(".index").attr("data-id");
                var telesaleNote = This.closest("tr").find(".note-by-telesale").val();
                var reasonId = This.val();
                $.ajax({
                    url: "/GUI/BackEnd/MonitorStaff/UpdateShineMemberConfirm.aspx/UpdateConfirmNotOk",
                    data: "{id:" + id + ",reasonId:" + reasonId + ",telesaleNote:'" + telesaleNote + "'}",
                    type: "POST",
                    contentType: "application/json;charset:utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.d == true) {
                            ShowMessage("Thông báo", "Cập nhật trạng thái thành công", 1);
                            return;
                        }
                        ShowMessage("Thông báo", "Cập nhật trạng thái thất bại", 4);
                    }
                })
            }
            function ConfirmOk(This) {
                var id = This.closest("tr").find(".index").attr("data-id");
                var telesaleNote = This.closest("tr").find(".note-by-telesale").val();
                $.ajax({
                    url: "/GUI/BackEnd/MonitorStaff/UpdateShineMemberConfirm.aspx/UpdateConfirmOk",
                    data: "{id:" + id + ",telesaleNote:'" + telesaleNote + "'}",
                    type: "POST",
                    contentType: "application/json;charset:utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.d == true) {
                            ShowMessage("Thông báo", "Cập nhật trạng thái thành công", 1);
                            return;
                        }
                        ShowMessage("Thông báo", "Cập nhật trạng thái thất bại", 4);
                    }
                })
            }
            function UpdateShine(This) {
                var id = This.closest("tr").find(".index").attr("data-id");
                var status = This.attr("data-id");
                if (status == 1) {
                    var c = confirm("Bạn có chắc chắn muốn thay đổi shinemember cho khách hàng này không?");
                    if (c) {
                        $.ajax({
                            url: "/GUI/BackEnd/MonitorStaff/UpdateShineMemberConfirm.aspx/UpdateShineMember",
                            data: "{id:" + id + ",status:'" + status + "'}",
                            type: "POST",
                            contentType: "application/json;charset:utf-8",
                            dataType: "json",
                            success: function (response) {
                                if (response.d == true) {
                                    ShowMessage("Thông báo", "Cập nhật trạng thái thành công", 1);
                                    return;
                                }
                                ShowMessage("Thông báo", "Cập nhật trạng thái thất bại", 4);
                            }
                        })
                    }
                    else return;
                }
                else {
                    $.ajax({
                        url: "/GUI/BackEnd/MonitorStaff/UpdateShineMemberConfirm.aspx/UpdateShineMember",
                        data: "{id:" + id + ",status:'" + status + "'}",
                        type: "POST",
                        contentType: "application/json;charset:utf-8",
                        dataType: "json",
                        success: function (response) {
                            if (response.d == true) {
                                ShowMessage("Thông báo", "Cập nhật trạng thái thành công", 1);
                                return;
                            }
                            ShowMessage("Thông báo", "Cập nhật trạng thái thất bại", 4);
                        }
                    })
                }

            }
        </script>
    </asp:Panel>
</asp:Content>
