﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using NLog;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.MonitorStaff
{

    public partial class UpdatePhoneShineMember : System.Web.UI.Page
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        protected Paging PAGING = new Paging();
        private int TotalPage;
        private bool Perm_Access = false;
        private bool Perm_ViewAllData = false;
        public static CultureInfo culture = new CultureInfo("vi-VN");
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (String.IsNullOrEmpty(TxtDateTimeFrom.Text))
            {
                TxtDateTimeFrom.Text = DateTime.Now.ToString("dd/MM/yyyy");
            }
            if (String.IsNullOrEmpty(TxtDateTimeTo.Text))
            {
                TxtDateTimeTo.Text = DateTime.Now.ToString("dd/MM/yyyy");
            }
            if (!IsPostBack)
            {
                
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, true);
                Library.Function.bindProduct(new List<DropDownList> { ddlProduct }, true);
                Library.Function.bindService(new List<DropDownList> { ddlService }, true);
            }
        }
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();

            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
            RptCustomer.DataSource = GetData().Skip(PAGING._Offset).Take(PAGING._Segment).ToList(); ;
            RptCustomer.DataBind();
            RemoveLoading();
        }


        protected void ValidControl()
        {
            if (txtPhone.Text.Length < 3)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "showmessage", "ShowMessage('Thông báo','Số điện thoại phải lớn hơn hoặc bằng 3 kí tự',4);", true);
            }

            if (ddlService.SelectedValue == "0" || ddlProduct.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "showmessage", "ShowMessage('Thông báo','Vui lòng chọn dịch vụ hoặc sản phẩm khách hàng đã dùng',4);", true);
            }
        }
        protected List<CustomerInfo> GetData()
        {

            var FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
            var ToDate = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
            var Salon = Convert.ToInt32(ddlSalon.SelectedValue);
            var Product = Convert.ToInt32(ddlProduct.SelectedValue);
            var Service = Convert.ToInt32(ddlService.SelectedValue);
            var Phone = txtPhone.Text;
            var db = new Solution_30shineEntities();
            var data = db.Database.SqlQuery<CustomerInfo>(@"DECLARE
                        @TimeFrom DATETIME = '"+FromDate+ @"',
                        @TimeTo DATETIME = '" + ToDate + @"',
                        @SalonId INT = " + Salon + @",
                        @ProductId INT = " + Product + @",
                        @ServiceId INT = " + Service + @",
                        @PhoneNumber NVARCHAR(50) = '"+Phone+ @"';
                        SELECT c.Id as CustomerId,c.Phone as CustomerPhone,
                        c.MemberType as IsShineMember,
                        b.Id as BillId,c.Fullname as CustomerName 
                        FROM dbo.BillService b 
                        LEFT JOIN dbo.FlowProduct p ON b.Id = p.BillId
                        LEFT JOIN dbo.FlowService s ON b.Id = s.BillId
                        INNER JOIN dbo.Customer c ON b.CustomerId = c.Id
                        WHERE
                        b.CreatedDate >= @TimeFrom AND b.CreatedDate <= @TimeTo
                        AND ((b.SalonId = @SalonId) OR (@SalonId = 0))
                        AND ((s.ServiceId = @ServiceId) OR (@ServiceId = 0))
                        AND ((p.ProductId = @ProductId) OR (@ProductId = 0))
                        AND b.Pending = 0 AND b.IsDelete = 0 AND c.MemberType = 1 AND
                        b.CustomerCode LIKE N'%'+@PhoneNumber+'%'").ToList();
            return data;
        }
        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? (HDF_Page.Value != "" ? Convert.ToInt32(HDF_Page.Value) : 1) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = GetData();
                int Count = lst.Count;
                var adfasdf = PAGING._Segment;
                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((float)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        public void AddLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "addLoading();", true);
        }

        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //if (Perm_Edit == true)
                //    ((Panel)(e.Item.FindControl("EAedit"))).Visible = true;
                //if (Perm_Delete == true)
                //    ((Panel)(e.Item.FindControl("EAdelete"))).Visible = true;
            }
        }
        [WebMethod]
        public static string CheckPhoneNumber(string phoneNumber)
        {
            var db = new Solution_30shineEntities();
            var result = db.Customers.FirstOrDefault(w=>w.Phone==phoneNumber);
            if (result == null)
            {
                return new JavaScriptSerializer().Serialize("false");
            }
            return new JavaScriptSerializer().Serialize(result);
        }
        [WebMethod(EnableSession = true)]
        public static bool InsertRequired(string oldPhone, int oldCustomerId, string newPhone, string newCustomerId, string noteUpdate, int billId)
        {
            var db = new Solution_30shineEntities();
            var userId = HttpContext.Current.Session["User_Id"].ToString();
            var insert = new ShineMemberUpdate();
            insert.BillId = billId;
            insert.CreatedDate = DateTime.Now;
            insert.NoteUpdate = noteUpdate;
            insert.OldCustomerId = oldCustomerId;
            insert.OldCustomerPhone = oldPhone;
            if (newCustomerId == null)
            {
                insert.NewCustomerId = null;
            }
            insert.NewCustomerId = Convert.ToInt32(newCustomerId);
            insert.NewCustomerPhone = newPhone;

            db.ShineMemberUpdates.Add(insert);
            var s = db.SaveChanges();
            if (s == 1)
            {
                
                return true;
                
            }
            else
                return false;
        }
        [WebMethod(EnableSession = true)]
        public static bool SendRequest(int oldCustomerId, string oldCustomerPhone, int newCustomerId, string newCustomerPhone, int billId, string noteUpdate)
        {
            var db = new Solution_30shineEntities();
            var obj = new ShineMemberUpdate();
            obj.CreatedDate = DateTime.Now;
            obj.OldCustomerId = oldCustomerId;
            obj.OldCustomerPhone = oldCustomerPhone;
            if (newCustomerId != 0)
            {
                obj.NewCustomerId = newCustomerId;
            }
            obj.NewCustomerPhone = newCustomerPhone;
            obj.BillId = billId;
            obj.NoteUpdate = noteUpdate;
            obj.UserUpdate = Convert.ToInt32(HttpContext.Current.Session["User_Id"]);
            obj.Confirm = 0;
            obj.ReasonId = 0;
            obj.Status = 0;
            obj.IsDelete = false;
            db.ShineMemberUpdates.Add(obj);
            var rs = db.SaveChanges();
            if (rs == 1)
            {
               
                logger.Info(" UserID: " + HttpContext.Current.Session["User_Id"].ToString() + " da yeu cau thay doi ShineMember tu so " + oldCustomerPhone + " sang " + newCustomerPhone);
                return true;
            }
            return false;
        }
        public class CustomerInfo
        {
            public int CustomerId { get; set; }
            public string CustomerPhone { get; set; }
            public int? IsShineMember { get; set; }
            public int? BillId { get; set; }
            public string CustomerName { get; set; }

        }
    }
}