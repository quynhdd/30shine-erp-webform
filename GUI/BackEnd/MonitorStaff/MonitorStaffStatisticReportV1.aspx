﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MonitorStaffStatisticReportV1.aspx.cs" MasterPageFile="~/TemplateMaster/SiteMasterVersion1.Master" Inherits="_30shine.GUI.BackEnd.MonitorStaff.MonitorStaffStatisticReportV1" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <title>Giám sát nhân viên - Báo cáo</title>
</asp:Content>

<asp:Content ID="MonitorStaffReport" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel ID="ContentWrap" runat="server" ClientIDMode="Static">
        <div class="container-fluid sub-menu">
            <div class="row bg-white">
                <div class="col-md-12 pt-3 pb-2">
                    <ul>
                        <li>Quản lý giám sát nhân viên &nbsp;&#187;</li>
                        <li><a href="/bao-cao-giam-sat-nhan-vien-v1.html"><i class="fa fa-list mr-1" aria-hidden="true"></i>Báo cáo giám sát nhân viên</a></li>
                        <li><a href="/bao-cao-thong-ke-giam-sat-nhan-vien.html"><i class="fa fa-list mr-1" aria-hidden="true"></i>Báo cáo thống kê lỗi giám sát nhân viên</a></li>
                        <li><a href="/them-moi-loi-giam-sat-nhan-vien-v1.html"><i class="far fa-file mr-1"></i>Thêm mới lỗi giám sát nhân viên</a></li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <div class="sub-menu-boder"></div>
                </div>
            </div>
        </div>
        <form runat="server">
            <div class="monitor-staff-page container-fluid">

                <div class=" title head">
                    <h4></h4>
                </div>

                <div id="page-monitor-staff">
                    <div class="inverted">
                        <!-- filter field -->
                        <div class="row mt-3">
                            <!--date-->
                            <div class="col-auto form-group">
                                <div class="calendar field date">
                                    <label class="font-weight-bold">Từ ngày</label>
                                    <div class="input full-width">
                                        <input class="form-control datetime-picker-v1" id="from" type="text"
                                            placeholder="Từ ngày" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-auto form-group">
                                <div class=" calendar field date">
                                    <label class="font-weight-bold">Đến ngày</label>
                                    <div class=" input full-width">
                                        <input class="form-control datetime-picker-v1" id="to" type="text"
                                            placeholder="Đến ngày" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-auto form-group">
                                <div class="group-btn form-group" style="padding-top: 30px">
                                    <!--submit-->
                                    <div class=" btn btn-send mr-2" style="width: 93px;" onclick="GetData()">Xem dữ liệu</div>
                                    <div class=" btn btn-send button" onclick="ExportExcel()">Xuất file excel</div>
                                </div>
                                <!-- table listing -->
                            </div>
                        </div>

                        <table class="selectable celled table table-error table-bordered" id="tblError">
                            <thead>
                                <tr>
                                    <th colspan="25" style="background: #cccccc">THỐNG KÊ BÁO CÁO LỖI</th>
                                </tr>
                                <tr>

                                    <th rowspan="2">STT</th>
                                    <th rowspan="2" style="width: 150px">Salon</th>
                                    <th colspan="4" style="text-align: center">Vệ sinh 3s</th>
                                    <th colspan="5" style="text-align: center">Quy định</th>
                                    <th colspan="5" style="text-align: center">Quy trình</th>
                                    <th colspan="4" style="text-align: center">Văn hóa 5c</th>
                                    <th rowspan="2" style="text-align: center">Tổng nhắc nhở</th>
                                    <th rowspan="2" style="text-align: center">Tổng có lỗi</th>
                                    <th rowspan="2" style="text-align: center">Tổng lỗi nghiêm trọng</th>
                                    <th rowspan="2" style="text-align: center">Tổng không lỗi do khách quan</th>
                                    <th rowspan="2" style="text-align: center">Tổng không lỗi do ghi nhận sai</th>
                                </tr>
                                <tr>
                                    <th style="border-left: 1px solid #d4d4d5">Nhắc nhở</th>
                                    <th>Có lỗi</th>
                                    <th>Không lỗi do khách quan</th>
                                    <th>Không lỗi do ghi nhận sai</th>

                                    <th>Nhắc nhở</th>
                                    <th>Có lỗi</th>
                                    <th>Lỗi nghiêm trọng</th>
                                    <th>Không lỗi do khách quan</th>
                                    <th>Không lỗi do ghi nhận sai</th>

                                    <th>Nhắc nhở</th>
                                    <th>Có lỗi</th>
                                    <th>Lỗi nghiêm trọng</th>
                                    <th>Không lỗi do khách quan</th>
                                    <th>Không lỗi do ghi nhận sai</th>

                                    <th>Nhắc nhở</th>
                                    <th>Có lỗi</th>
                                    <th>Không lỗi do khách quan</th>
                                    <th>Không lỗi do ghi nhận sai</th>
                                </tr>
                            </thead>
                            <tbody id="table-report">
                                <tr>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>0</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </form>

        <style>
            .btn-send {
                background: rgb(0, 0, 0);
                color: #ffffff;
            }

                .btn-send:hover {
                    color: #ffd800;
                }

            .font-weight-bold {
                font-family: Roboto Condensed bold;
            }

            .wr-search .result, .dropdown.result {
                overflow-y: scroll;
                max-height: 400px;
                display: none;
            }

            .wr-search .result, .dropdown.result {
                position: absolute;
                background: white;
                z-index: 1000;
                top: 115%;
                width: 100%;
                border: 1px solid #d0d0d0;
            }

                .wr-search .result .item, .dropdown.result .item {
                    padding: 5px 10px;
                }

                .wr-search .result div, .dropdown.result div {
                    color: black;
                }

            .form-control, .btn {
                font-size: inherit !important;
            }

            .menu img.image {
                width: 100px !important;
            }

            .custom.popup {
                display: none;
                width: 1000px;
                position: absolute;
                z-index: 100;
                bottom: 0;
                left: 107%;
            }

                .custom.popup .column .image {
                    width: 100%;
                    box-shadow: 0px 0px 0px 5px rgba(0,0,0,0.1);
                    border-radius: 1px;
                }

                .custom.popup .column {
                    float: left;
                    width: 49%;
                    margin-right: 1%;
                }

                    .custom.popup .column img {
                        width: 100%;
                    }

            .ui.menu:hover .custom.popup {
                display: block !important;
            }
        </style>
        <script src="/Assets/js/erp.30shine.com/monitor-staff/config.js"></script>
        <script src="/Assets/js/erp.30shine.com/monitor-staff/common.js"></script>
        <script type="text/javascript">
            var btnSubmit = $('#btnSubmit');
            var tblReport = $('#table-report');
            var txtFrom = $('#from');
            var txtTo = $('#to');
            var excelBody = [];
            var elms = {
                pageMonitorStaff: '#page-monitor-staff',
                titleHead: '.title.head h4',
                imgError: '.img-error'
            };

            $(document).ready(function () {
                $.datetimepicker.setLocale('vi');
                $('.datetime-picker-v1').datetimepicker({
                    dayOfWeekStart: 1,
                    startDate: new Date(),
                    format: 'd-m-Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    scrollMonth: false,
                    scrollInput: false,
                    onChangeDateTime: function (dp, $input) { }
                });
            })
            function GetData() {

                if (!txtFrom.val() && txtTo.val()) {
                    ShowMessage("Thông báo", "Vui lòng chọn ngày bắt đầu", 4);
                    txtFrom.focus();
                }
                else if (txtFrom.val() && !txtTo.val()) {
                    ShowMessage("Thông báo", "Vui lòng chọn ngày kết thúc", 4);
                    txtTo.focus();
                }
                else if (!txtFrom.val() && !txtTo.val()) {
                    ShowMessage("Thông báo", "Vui lòng chọn ngày tháng tìm kiếm", 4);
                    txtFrom.focus();
                }
                callAjax(
                    {
                        from: txtFrom.val(),
                        to: txtTo.val()
                    },
                    apis.monitorStaffService.domain + apis.monitorStaffService.statisticAllSalon_v1, "GET", false, completeCallback, errorCallback, successCallback);
            }
            function successCallback(result, status, xhr) {
                console.log(result);
                //$("#tblError tbody").empty();
                result.forEach(function (v, i) {
                    let excelRow = [
                        (i + 1) + "",
                        v.salon,
                        v.vs3s_nhac_nho,
                        v.vs3s_co_loi,
                        v.vs3s_khong_loi_do_kq,
                        v.vs3s_khong_loi_do_ghi_nhan_sai,
                        v.quy_dinh_nhac_nho,
                        v.quy_dinh_co_loi,
                        v.quy_dinh_co_loi_nghiem_trong,
                        v.quy_dinh_khong_loi_do_kq,
                        v.quy_dinh_khong_loi_do_ghi_nhan_sai,
                        v.quy_trinh_nhac_nho,
                        v.quy_trinh_co_loi,
                        v.quy_trinh_co_loi_nghiem_trong,
                        v.quy_trinh_khong_loi_do_kq,
                        v.quy_trinh_khong_loi_do_ghi_nhan_sai,
                        v.vh5c_nhac_nho,
                        v.vh5c_co_loi,
                        v.vh5c_khong_loi_do_kq,
                        v.vh5c_khong_loi_do_ghi_nhan_sai,
                        v.vs3s_nhac_nho + v.quy_dinh_nhac_nho + v.quy_trinh_nhac_nho + v.vh5c_nhac_nho,
                        v.vs3s_co_loi + v.quy_dinh_co_loi + v.quy_trinh_co_loi + v.vh5c_co_loi,
                        v.quy_dinh_co_loi_nghiem_trong + v.quy_trinh_co_loi_nghiem_trong,
                        v.vs3s_khong_loi_do_kq + v.quy_dinh_khong_loi_do_kq + v.quy_trinh_khong_loi_do_kq + v.vh5c_khong_loi_do_kq,
                        v.vs3s_khong_loi_do_ghi_nhan_sai + v.quy_dinh_khong_loi_do_ghi_nhan_sai + v.quy_trinh_khong_loi_do_ghi_nhan_sai + v.vh5c_khong_loi_do_ghi_nhan_sai,
                    ];
                    // ,convertIgnoreNaN(v.statistic.ratioErrorUntreated) + "%"
                    excelBody.push(excelRow);
                    let item = '<tr>';
                    item += '<td>' + excelRow[0] + '</td>';
                    item += '<td  style="width:150px">' + excelRow[1] + '</td>';
                    item += '<td>' + excelRow[2] + '</td>';
                    item += '<td>' + excelRow[3] + '</td>';
                    item += '<td>' + excelRow[4] + '</td>';
                    item += '<td>' + excelRow[5] + '</td>';
                    item += '<td>' + excelRow[6] + '</td>';
                    item += '<td>' + excelRow[7] + '</td>';
                    item += '<td>' + excelRow[8] + '</td>';
                    item += '<td>' + excelRow[9] + '</td>';
                    item += '<td>' + excelRow[10] + '</td>';
                    item += '<td>' + excelRow[11] + '</td>';
                    item += '<td>' + excelRow[12] + '</td>';
                    item += '<td>' + excelRow[13] + '</td>';
                    item += '<td>' + excelRow[14] + '</td>';
                    item += '<td>' + excelRow[15] + '</td>';
                    item += '<td>' + excelRow[16] + '</td>';
                    item += '<td>' + excelRow[17] + '</td>';
                    item += '<td>' + excelRow[18] + '</td>';
                    item += '<td>' + excelRow[19] + '</td>';
                    item += '<td>' + excelRow[20] + '</td>';
                    item += '<td>' + excelRow[21] + '</td>';
                    item += '<td>' + excelRow[22] + '</td>';
                    item += '<td>' + excelRow[23] + '</td>';
                    item += '<td>' + excelRow[24] + '</td>';
                    $("#tblError tbody").append(item + '<tr>');
                });
                ConfigTableListing();
            }
             function completeCallback(xhr, status) {
            // console.log(xhr)
            // console.log(status)
        }
        function errorCallback(xhr, status, error) {
            alert("Đã có lỗi xảy ra trong quá trình lấy dữ liệu. Xin vui lòng thử lại");
            // tblReport.empty();
        }
            function ConfigTableListing() {

                table = $('#tblError').DataTable(
                    {
                        ordering: false,
                        searching: false,
                        info: false,
                        iDisplayLength: 10,
                        language: {
                            "lengthMenu": "Dòng / trang _MENU_",
                        },
                        lengthMenu: [[10, 100, 500, -1], [10, 100, 500, "Tất cả"]]
                    });
            }

        </script>
    </asp:Panel>
</asp:Content>
