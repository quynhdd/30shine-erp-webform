﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.Interface;
using ExportToExcel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.MonitorStaff
{
    public partial class MonitorStaffReportV1 : System.Web.UI.Page
    {
        private bool Perm_Access = false;
        private bool Perm_Edit = false;
        private bool Perm_Delete = false;
        private bool Perm_ViewAllData = false;
        private static MonitorStaffReportV1 instance;
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            //var salonId = Session["SalonId"].ToString();
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var msgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", msgParam);
            }
            var userId = Session["User_Id"].ToString();
            //HDF_SalonId.Value = salonId;
            HDF_UserId.Value = userId;
            HDF_PermisionViewAllSalon.Value = Convert.ToInt16(Perm_ViewAllData).ToString();
            HDF_PermisionCheckError.Value = Helpers.SecurityLib.CheckQlGiamSat() || Helpers.SecurityLib.CheckPermissionAdminOrRoot() ? "1" : "0";

            if (!IsPostBack)
            {

            }
            //return view
        }

        public static MonitorStaffReportV1 getInstance()
        {
            return MonitorStaffReportV1.instance = new MonitorStaffReportV1();
        }
        public static MonitorStaffReportV1 Instance
        {
            get
            {
                return new MonitorStaffReportV1();
            }
        }
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var msgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", msgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }

        [WebMethod]
        public static object ExportExel(string jsonObj)
        {
            try
            {
                var excelHeadRow = new List<string>();
                var serializer = new JavaScriptSerializer();
                var data = new JavaScriptSerializer().Deserialize<List<cls_Excel>>(jsonObj);
                var billRowLST = new List<List<string>>();

                excelHeadRow.Add("Ngày");
                excelHeadRow.Add("Time Xử lý");
                excelHeadRow.Add("Time Nhận tin");
                excelHeadRow.Add("Nhân sự GS");
                excelHeadRow.Add("Salon");
                excelHeadRow.Add("Vị trí");
                excelHeadRow.Add("Time phát sinh");
                excelHeadRow.Add("Loại lỗi");
                excelHeadRow.Add("Tên lỗi");
                excelHeadRow.Add("Ghi chú GS");
                excelHeadRow.Add("Tên nhân viên");
                excelHeadRow.Add("Hình thức xử lý");
                excelHeadRow.Add("Ghi chú QL");
                excelHeadRow.Add("Trạng thái xác nhận lỗi");
                var FileName = "";
                var UrlFileName = "";
                if (data.Count > 0)
                {
                    foreach (var v in data)
                    {
                        var billRow = new List<string>();
                        billRow.Add(v.errorTime.ToString());
                        billRow.Add(v.periodBeforeCommit.ToString());
                        billRow.Add(v.periodBeforeConfirm.ToString());
                        billRow.Add(v.reporterName);
                        billRow.Add(v.salonName);
                        billRow.Add(v.staffTypeName);
                        billRow.Add(string.Format("{0:hh:mm}", v.errorTime));
                        billRow.Add(v.categoryErrorRootName);
                        billRow.Add(v.categoryErrorSubName);
                        billRow.Add(v.noteByReporter);
                        billRow.Add(v.staffName);
                        billRow.Add(v.handleName);
                        billRow.Add(v.statusErrorName);
                        billRowLST.Add(billRow);
                    }

                    // export
                    var ExcelStorePath = getInstance().Server.MapPath("~") + "Public/Excel/Nhan.Vien/";
                    FileName = "GS_" + string.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
                    //var UrlFileName = "http://" + getInstance().Request.Url.Host + "/Public/Excel/Nhan.Vien/" + FileName;
                    UrlFileName = "/Public/Excel/Nhan.Vien/";

                    ExportXcel(excelHeadRow, billRowLST, ExcelStorePath + FileName);
                    //ScriptManager.RegisterStartupScript(getInstance().Page, getInstance().GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "')", true);
                }
                return UrlFileName + FileName;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private static void ExportXcel(List<string> _ExcelHeadRow, List<List<string>> List, string Path)
        {
            try
            {
                CreateExcelFile.ExcelHeadRow = _ExcelHeadRow;
                CreateExcelFile.CreateExcel(List, Path);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class cls_Excel
        {
            public int? categoryErrorRootId { get; set; }
            public string categoryErrorRootName { get; set; }
            public string categoryErrorSubName { get; set; }
            public DateTime? commitTime { get; set; }
            public DateTime? confirmTime { get; set; }
            public DateTime? createDate { get; set; }
            public string errorTime { get; set; }
            public int? handleId { get; set; }
            public string handleName { get; set; }
            public int? id { get; set; }
            public bool? isCommit { get; set; }
            public bool? isDelete { get; set; }
            public bool? isPending { get; set; }
            public DateTime? modifyDate { get; set; }
            public string noteByReporter { get; set; }
            public string noteByReviewer { get; set; }
            public int? periodBeforeCommit { get; set; }
            public int? periodBeforeConfirm { get; set; }
            public int? reporterId { get; set; }
            public string reporterName { get; set; }
            public DateTime? resolveTime { get; set; }
            public string resolverName { get; set; }
            public int? resolverId { get; set; }
            public int? reviewerId { get; set; }
            public string reviewerName { get; set; }
            public int? salonId { get; set; }
            public string salonName { get; set; }
            public int? staffId { get; set; }
            public string staffName { get; set; }
            public int? staffTypeId { get; set; }
            public string staffTypeName { get; set; }
            public int? statusError { get; set; }
            public string statusErrorName { get; set; }
        }
    }
}