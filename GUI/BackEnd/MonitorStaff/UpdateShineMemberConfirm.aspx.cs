﻿using _30shine.Helpers;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.MODEL.Interface;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _30shine.GUI.BackEnd.MonitorStaff
{
    public partial class UpdateShineMemberConfirm : System.Web.UI.Page
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        protected Paging PAGING = new Paging();
        private int TotalPage;
        private bool Perm_Access = false;
        private bool Perm_Edit = false;
        private bool Perm_Update = false;
        private bool Perm_ViewAllData = false;
        public static CultureInfo culture = new CultureInfo("vi-VN");
        
        /// <summary>
        /// Page load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (String.IsNullOrEmpty(TxtDateTimeFrom.Text))
            {
                TxtDateTimeFrom.Text = DateTime.Now.ToString("dd/MM/yyyy");
            }
            if (String.IsNullOrEmpty(TxtDateTimeTo.Text))
            {
                TxtDateTimeTo.Text = DateTime.Now.ToString("dd/MM/yyyy");
            }
            if (!IsPostBack)
            {
               // GetReason();
            }
        }

        /// <summary>
        ///  Set permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                int integer;
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_Update = permissionModel.CheckPermisionByAction("Perm_Update", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();

            }
        }

        /// <summary>
        ///  Execute
        /// </summary>
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        /// <summary>
        /// Bind paging
        /// </summary>
        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? (HDF_Page.Value != "" ? Convert.ToInt32(HDF_Page.Value) : 1) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        /// <summary>
        /// Get data shinemember
        /// </summary>
        /// <returns></returns>
        protected List<ShineMemberUpdate> GetData()
        {

            var FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
            var ToDate = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
            var Status = Convert.ToInt32(ddlConfirm.SelectedValue);
            var Change = Convert.ToInt32(ddlChange.SelectedValue);
            var db = new Solution_30shineEntities();
            var data = new List<ShineMemberUpdate>();
            if (Status == 0 && Change == 0)
            {
                data = db.ShineMemberUpdates.Where(w => w.CreatedDate >= FromDate && w.CreatedDate < ToDate).ToList();
            }
            else if (Status == 0 && Change != 0)
            {
                data = db.ShineMemberUpdates.Where(w => w.CreatedDate >= FromDate && w.CreatedDate < ToDate && w.Status == Change).ToList();
            }
            else
            {
                data = db.ShineMemberUpdates.Where(w => w.CreatedDate >= FromDate && w.CreatedDate < ToDate && w.Confirm == Status).ToList();
            }
            return data;
        }

        /// <summary>
        /// Get total page
        /// </summary>
        /// <returns></returns>
        protected int Get_TotalPage()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = _GetData();
                int Count = lst.Count;
                var adfasdf = PAGING._Segment;
                int TotalRow = Count - PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((float)TotalRow / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        /// <summary>
        /// btn click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
            RptCustomer.DataSource = _GetData().Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
            RptCustomer.DataBind();
            RemoveLoading();
        }

        /// <summary>
        /// add loading
        /// </summary>
        public void AddLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "addLoading();", true);
        }

        /// <summary>
        /// remove loading
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        public void Repeater_ItemDataBound(Object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
              || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                if (Perm_Edit == true)
                    ((Panel)(e.Item.FindControl("ConfirmPanel"))).Visible = true;
                if (Perm_Update == true)
                    ((Panel)(e.Item.FindControl("UpdatePanel"))).Visible = true;
            }
        }

        [WebMethod]
        public static string GetReason(int id)
        {
            var db = new Solution_30shineEntities();
            var rs = db.Database.SqlQuery<Reason>(@"SELECT 
	            CAST(c.Value AS INT) AS Value,
	            ISNULL(u.ReasonId,0) AS ReasonId,
	            c.Label AS ReasonName
            FROM 
	            dbo.Tbl_Config c LEFT JOIN dbo.ShineMemberUpdate u
	            ON CAST(c.Value AS INT) = u.ReasonId AND u.Id = " + id + @"
            WHERE 
	            c.[Key] = 'shinemeber_update_reason' AND c.IsDelete = 0").ToList();
            return new JavaScriptSerializer().Serialize(rs);
        }

        [WebMethod]
        public static string GetReason_1()
        {
            var db = new Solution_30shineEntities();
            var rs = db.Database.SqlQuery<Reason>(@"SELECT 
	            CAST(c.Value AS INT) AS Value,
	            ISNULL(u.ReasonId,0) AS ReasonId,
	            c.Label AS ReasonName
            FROM 
	            dbo.Tbl_Config c LEFT JOIN dbo.ShineMemberUpdate u
	            ON CAST(c.Value AS INT) = u.ReasonId
            WHERE 
	            c.[Key] = 'shinemeber_update_reason' AND c.IsDelete = 0").ToList();
            return new JavaScriptSerializer().Serialize(rs);
        }

        [WebMethod(enableSession: true)]
        public static bool UpdateConfirmOk(int id, string telesaleNote)
        {
            var db = new Solution_30shineEntities();
            var obj = db.ShineMemberUpdates.FirstOrDefault(w => w.Id == id);
            obj.Confirm = 1;
            obj.ReasonId = 0;
            obj.TimeConfirm = DateTime.Now;
            obj.TelesaleNote = telesaleNote;
            obj.UserConfirm = Convert.ToInt32(HttpContext.Current.Session["User_Id"].ToString());
            db.ShineMemberUpdates.AddOrUpdate(obj);
            var rs = db.SaveChanges();
            if (rs == 1)
            {
                logger.Info(" UserID: " + HttpContext.Current.Session["User_Id"].ToString() +
                    " da chap nhan yeu cau doi ShineMember tu SDT " + obj.OldCustomerPhone +
                    " sang " + obj.NewCustomerPhone + ". Ghi chu: " + telesaleNote);
                return true;
            }
            return false;
        }

        [WebMethod(enableSession: true)]
        public static bool UpdateConfirmNotOk(int id, int reasonId, string telesaleNote)
        {
            var db = new Solution_30shineEntities();
            var obj = db.ShineMemberUpdates.FirstOrDefault(w => w.Id == id);
            obj.Confirm = 2;
            obj.ReasonId = reasonId;
            obj.TimeConfirm = DateTime.Now;
            obj.TelesaleNote = telesaleNote;
            obj.UserConfirm = Convert.ToInt32(HttpContext.Current.Session["User_Id"].ToString());
            db.ShineMemberUpdates.AddOrUpdate(obj);
            var rs = db.SaveChanges();
            if (rs == 1)
            {
                logger.Info(" UserID: " + HttpContext.Current.Session["User_Id"].ToString() + 
                    " da huy yeu cau doi ShineMember tu SDT " + obj.OldCustomerPhone + 
                    " sang " + obj.NewCustomerPhone+". Ly do huy: "+ reasonId+ ". Ghi chu: "+ telesaleNote);
                return true;
            }
            return false;
        }

        [WebMethod(EnableSession = true)]
        public static bool UpdateShineMember(int id, int status)
        {
            var result = false;
            var db = new Solution_30shineEntities();
            var obj = db.ShineMemberUpdates.FirstOrDefault(w => w.Id == id);
            obj.Status = status;
            obj.UserUpdateChange = Convert.ToInt32(HttpContext.Current.Session["User_Id"].ToString());
            obj.TimeChange = DateTime.Now;
            db.ShineMemberUpdates.AddOrUpdate(obj);
            if (status == 1)
            {
                if (obj.NewCustomerId != null)
                {
                    var customer = db.Customers.FirstOrDefault(w => w.Id == obj.NewCustomerId);
                    var oldCustomer = db.Customers.FirstOrDefault(w => w.Id == obj.OldCustomerId);
                    customer.MemberType = 1;
                    customer.ModifiedDate = DateTime.Now;
                    customer.MemberStartTime = oldCustomer.MemberStartTime;
                    customer.MemberEndTime = oldCustomer.MemberEndTime;
                    customer.MemberProductId = oldCustomer.MemberProductId;
                    //update old customer
                    oldCustomer.MemberType = 0;
                    oldCustomer.MemberStartTime = null;
                    oldCustomer.MemberEndTime = null;
                    oldCustomer.MemberProductId = null;

                    db.Customers.AddOrUpdate(customer,oldCustomer);
                    logger.Info(" UserID: " + HttpContext.Current.Session["User_Id"].ToString() +
                    " da xac nhan doi ShineMember tu SDT " + obj.OldCustomerPhone +
                    " sang " + obj.NewCustomerPhone + ". SDT moi da ton tai");
                }
                else
                {
                    var update = db.Customers.FirstOrDefault(w => w.Phone == obj.OldCustomerPhone);
                    update.Phone = obj.NewCustomerPhone;
                    update.FingerTemplate = obj.NewCustomerPhone;
                    update.Customer_Code = obj.NewCustomerPhone;
                    db.Customers.AddOrUpdate(update);
                    logger.Info(" UserID: " + HttpContext.Current.Session["User_Id"].ToString() +
                    " da xac nhan doi ShineMember tu SDT " + obj.OldCustomerPhone +
                    " sang " + obj.NewCustomerPhone + ". SDT moi chua ton tai");
                }
            }

            var rs = db.SaveChanges();
            if (rs != 0)
            {
                logger.Info(" UserID: " + HttpContext.Current.Session["User_Id"].ToString() +
                " da tu choi doi ShineMember tu SDT " + obj.OldCustomerPhone +
                " sang " + obj.NewCustomerPhone + ". SDT moi chua ton tai");
                result = true;
            }
            else result = false;

            return result;
        }

        private List<Data> _GetData()
        {
            var FromDate = Convert.ToDateTime(TxtDateTimeFrom.Text, culture);
            var ToDate = Convert.ToDateTime(TxtDateTimeTo.Text, culture).AddDays(1);
            var Status = Convert.ToInt32(ddlConfirm.SelectedValue);
            var Change = Convert.ToInt32(ddlChange.SelectedValue);
            var db = new Solution_30shineEntities();
            var data = new List<Data>();
            if (Status == 99 && Change == 99)
            {
                data = (from a in db.ShineMemberUpdates
                        join s in db.Staffs
                        on a.UserUpdate equals s.Id
                        join c in db.Customers on a.OldCustomerId equals c.Id
                        where a.CreatedDate >= FromDate && a.CreatedDate < ToDate
                        select new Data
                        {
                            Id = a.Id,
                            BillId = a.BillId,
                            Confirm = a.Confirm,
                            ReasonId = a.ReasonId,
                            NewNumber = a.NewCustomerPhone,
                            OldCustomerId = a.OldCustomerId,
                            OldCustomerName = c.Fullname,
                            OldNumber = a.OldCustomerPhone,
                            UserRequireId = a.UserUpdate,
                            Status = a.Status,
                            TelesaleNote = a.TelesaleNote,
                            UserRequireName = s.Fullname
                        }
                        ).ToList();
            }
            else if (Status == 99 && Change != 99)
            {
                data = (from a in db.ShineMemberUpdates
                        join s in db.Staffs
                        on a.UserUpdate equals s.Id
                        join c in db.Customers on a.OldCustomerId equals c.Id
                        where a.CreatedDate >= FromDate && a.CreatedDate < ToDate && a.Status == Change
                        select new Data
                        {
                            Id = a.Id,
                            BillId = a.BillId,
                            Confirm = a.Confirm,
                            ReasonId = a.ReasonId,
                            NewNumber = a.NewCustomerPhone,
                            OldNumber = a.OldCustomerPhone,
                            OldCustomerId = a.OldCustomerId,
                            OldCustomerName = c.Fullname,
                            UserRequireId = a.UserUpdate,
                            Status = a.Status,
                            TelesaleNote = a.TelesaleNote,
                            UserRequireName = s.Fullname
                        }
                        ).ToList();
            }
            else
            {
                data = (from a in db.ShineMemberUpdates
                        join s in db.Staffs
                        on a.UserUpdate equals s.Id
                        join c in db.Customers on a.OldCustomerId equals c.Id
                        where a.CreatedDate >= FromDate && a.CreatedDate < ToDate && a.Confirm == Status
                        select new Data
                        {
                            Id = a.Id,
                            BillId = a.BillId,
                            Confirm = a.Confirm,
                            ReasonId = a.ReasonId,
                            NewNumber = a.NewCustomerPhone,
                            OldNumber = a.OldCustomerPhone,
                            OldCustomerName = c.Fullname,
                            OldCustomerId = a.OldCustomerId,
                            UserRequireId = a.UserUpdate,
                            Status = a.Status,
                            TelesaleNote = a.TelesaleNote,
                            UserRequireName = s.Fullname
                        }
                        ).ToList();
            }
            return data;
        }

        public class Reason
        {
            public int Value { get; set; }
            public int ReasonId { get; set; }
            public string ReasonName { get; set; }
        }

        public class Data
        {
            public int Id { get; set; }
            public string OldNumber { get; set; }
            public int? OldCustomerId { get; set; }
            public string OldCustomerName { get; set; }
            public string NewNumber { get; set; }
            public int? BillId { get; set; }
            public int? UserRequireId { get; set; }
            public string UserRequireName { get; set; }
            public string UserRequire { get; set; }
            public int? Confirm { get; set; }
            public int? ReasonId { get; set; }
            public string TelesaleNote { get; set; }
            public int? Status { get; set; }
        }
    }
}