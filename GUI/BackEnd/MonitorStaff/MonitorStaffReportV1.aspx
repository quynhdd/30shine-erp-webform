﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MonitorStaffReportV1.aspx.cs" MasterPageFile="~/TemplateMaster/SiteMasterVersion1.Master" Inherits="_30shine.GUI.BackEnd.MonitorStaff.MonitorStaffReportV1" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <title>Giám sát nhân viên - Báo cáo</title>
</asp:Content>

<asp:Content ID="MonitorStaffReport" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel ID="ContentWrap" runat="server" ClientIDMode="Static" Style="background-color: #F7F7F7">

        <%--  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.11.8/semantic.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.11.8/semantic.min.js"></script>--%>
        <!-- calender -->
        <%-- <link href="https://cdn.rawgit.com/mdehoog/Semantic-UI-Calendar/76959c6f7d33a527b49be76789e984a0a407350b/dist/calendar.min.css"
            rel="stylesheet" type="text/css" />
        <script src="https://cdn.rawgit.com/mdehoog/Semantic-UI-Calendar/76959c6f7d33a527b49be76789e984a0a407350b/dist/calendar.min.js"></script>--%>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.0/socket.io.js"></script>
        <%--<link rel="stylesheet" type="text/css" href="/Assets/css/erp.30shine.com/monitor-staff/common.css" />--%>
        <script src="/Assets/js/erp.30shine.com/monitor-staff/common.js"></script>
        <script src="/Assets/js/erp.30shine.com/monitor-staff/config.js"></script>

        <div class="container-fluid sub-menu">
            <div class="row bg-white">
                <div class="col-md-12 pt-3 pb-2">
                    <ul>
                        <li>Quản lý giám sát nhân viên &nbsp;&#187;</li>
                        <li><a href="/bao-cao-giam-sat-nhan-vien-v1.html"><i class="fa fa-list mr-1" aria-hidden="true"></i>Báo cáo giám sát nhân viên</a></li>
                        <li><a href="/bao-cao-thong-ke-giam-sat-nhan-vien.html"><i class="fa fa-list mr-1" aria-hidden="true"></i>Báo cáo thống kê lỗi giám sát nhân viên</a></li>
                        <li><a href="/them-moi-loi-giam-sat-nhan-vien-v1.html"><i class="far fa-file mr-1"></i>Thêm mới lỗi giám sát nhân viên</a></li>
                    </ul>
                </div>
                <div class="col-md-12">
                    <div class="sub-menu-boder"></div>
                </div>
            </div>
        </div>

        <form runat="server">
            <div class="monitor-staff-page container-fluid">

                <div class=" title head">
                    <h4></h4>
                </div>

                <div id="page-monitor-staff">
                    <div class="inverted">
                        <!-- filter field -->
                        <div class="row mt-3">
                            <!--date-->
                            <div class="col-auto form-group">
                                <div class="calendar field date">
                                    <label class="font-weight-bold">Từ ngày</label>
                                    <div class="input full-width">
                                        <input class="form-control datetime-picker-v1" id="from" type="text"
                                            placeholder="mặc định là ngày hiện tại" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-auto form-group">
                                <div class=" calendar field date">
                                    <label class="font-weight-bold">Đến ngày</label>
                                    <div class=" input full-width">
                                        <input class="form-control datetime-picker-v1" id="to" type="text"
                                            placeholder="mặc định là ngày hiện tại" />
                                    </div>
                                </div>
                            </div>
                            <!-- resolver/admin/GSM -->
                            <div class="col-auto form-group">
                                <div class=" field hidden staff-manager">
                                    <label class="font-weight-bold">Quản lý giám sát</label>
                                    <div class="col-xs-2 wr-search position-relative">
                                        <div class="input full-width">
                                            <input class="form-control " data-id="-1" type="text" placeholder="chọn quản lý" />
                                        </div>
                                        <div class="dropdown result">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- salon -->
                            <div class="col-auto form-group">
                                <div class=" field salon-error">
                                    <label class="font-weight-bold">Salon giám sát</label>
                                    <br />
                                    <%--  <div class="col-xs-2 wr-search position-relative">
                                        <div class=" input full-width">
                                            <input class="form-control " data-id="" type="text" placeholder="chọn salon" />
                                        </div>
                                        <div class="dropdown result">
                                        </div>
                                    </div>--%>
                                    <select class="select form-control" name="salonId"></select>
                                </div>
                            </div>
                            <!-- status handler -->
                            <!--pending status-->
                            <div class="col-auto form-group">
                                <div class=" field status-handle">
                                    <label class="font-weight-bold">Trạng thái (SM/DSM/GSM quyết định)</label>
                                    <%--<div class="wr-dropdown status position-relative">
                                        <div class=" input full-width">
                                            <input class="form-control " data-id="-1" type="text" placeholder="chọn trạng thái" />
                                        </div>
                                        <div class="dropdown result">
                                            <div class="item full-width border-bottom pointer " role="button" data-id="-1"
                                                data-name="Tất cả trạng thái">
                                                Tất cả trạng thái
                                            </div>
                                            <div class="item full-width border-bottom pointer" data-id="0" data-name="Chưa biết xử lý">
                                                Chưa biết xử lý
                                            </div>
                                            <div class="item full-width border-bottom pointer" data-id="1" data-name="Có lỗi">
                                                Có lỗi
                                            </div>
                                            <div class="item full-width border-bottom pointer" data-id="2" data-name="Không có lỗi">
                                                Không có lỗi
                                            </div>
                                        </div>
                                    </div>--%>
                                    <br />
                                    <select class="select form-control" name="handleId">
                                    </select>
                                </div>
                            </div>
                            <!--pending status-->
                            <div class="col-auto form-group">
                                <div class=" field status-handle">
                                    <label class="font-weight-bold">SM/DSM xử lý</label>
                                    <div class="wr-dropdown is-pending position-relative">
                                        <div class=" input full-width">
                                            <input class="form-control " data-id="-1" type="text"
                                                placeholder="click chọn (isPending)" />
                                        </div>
                                        <div class="dropdown result">
                                            <div class="item full-width border-bottom pointer" data-id="-1" data-name="Không lọc">
                                                Không lọc
                                            </div>
                                            <div class="item full-width border-bottom pointer" data-id="0" data-name="Chưa xử lý">
                                                Chưa xử lý
                                            </div>
                                            <div class="item full-width border-bottom pointer" data-id="1" data-name="Đang xử lý">
                                                Đang xử lý
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--commit status-->
                            <div class="col-auto form-group">
                                <div class=" field status-handle">
                                    <label class="font-weight-bold">SM/DSM xác nhận</label>
                                    <div class="wr-dropdown is-commit position-relative">
                                        <div class=" input full-width">
                                            <input class="form-control " data-id="-1" type="text"
                                                placeholder="click chọn (isCommit)" />
                                        </div>
                                        <div class="dropdown result">
                                            <div class="item full-width border-bottom pointer" data-id="-1" data-name="Không lọc">
                                                Không lọc
                                            </div>
                                            <div class="item full-width border-bottom pointer" data-id="0" data-name="Chưa xác nhận">
                                                Chưa xác nhận
                                            </div>
                                            <div class="item full-width border-bottom pointer" data-id="1" data-name="Đã xác nhận">
                                                Đã xác nhận
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 form-group">
                                <!--submit-->
                                <div class=" btn btn-success button submit mr-2" style="width: 93px;">Xem</div>
                                <%--<div class=" btn btn-primary button" onclick="ExportExcel()">Xuất file excel</div>--%>
                                <!-- table listing -->
                            </div>
                        </div>


                        <table class="selectable celled table table-error table-bordered" id="tableListing">
                            <thead>
                                <tr>
                                    <th class="font-weight-bold">STT</th>
                                    <th class="font-weight-bold" style="min-width: 63px">Ngày</th>
                                    <th class="font-weight-bold" style="min-width: 56px">Time xử lý</th>
                                    <th class="font-weight-bold" style="min-width: 53px">Time nhận tin</th>
                                    <th class="font-weight-bold">Nhân sự GS</th>
                                    <th class="font-weight-bold" style="min-width: 120px">Salon</th>
                                    <th class="font-weight-bold">Vị trí</th>
                                    <th class="font-weight-bold">Time phát sinh</th>
                                    <th class="font-weight-bold">Ảnh đính kèm</th>
                                    <th class="font-weight-bold">Hình thức</th>
                                    <th class="font-weight-bold" style="min-width: 62px;">Loại lỗi</th>
                                    <th class="font-weight-bold" style="min-width: 218px">Tên lỗi</th>
                                    <th class="font-weight-bold">Ghi chú GS</th>
                                    <th class="font-weight-bold">Tên NV</th>
                                    <th class="font-weight-bold">Hình thức XL</th>
                                    <th class="font-weight-bold">Ghi chú QL</th>
                                    <th class="resolve-status-error font-weight-bold">Trạng thái xác nhận lỗi</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            
            <%--main content--%>
            <asp:HiddenField ID="HDF_UserId" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HDF_UserName" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HDF_UserRole" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HDF_SalonId" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HDF_SalonName" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HDF_PermisionViewAllSalon" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="HDF_PermisionCheckError" runat="server" ClientIDMode="Static" />

        </form>
        <style>
            .select2-container--default .select2-selection--single {
                height: 37px;
            }

            .font-weight-bold {
                font-family: Roboto Condensed bold;
            }

            .wr-search .result, .dropdown.result {
                overflow-y: scroll;
                max-height: 400px;
                display: none;
            }

            .wr-search .result, .dropdown.result {
                position: absolute;
                background: white;
                z-index: 1000;
                top: 115%;
                width: 100%;
                border: 1px solid #d0d0d0;
            }

                .wr-search .result .item, .dropdown.result .item {
                    padding: 5px 10px;
                }

                .wr-search .result div, .dropdown.result div {
                    color: black;
                }

            .form-control, .btn {
                font-size: inherit !important;
            }

            .menu img.image {
                width: 100px !important;
            }

            .custom.popup {
                display: none;
                width: 1000px;
                position: absolute;
                z-index: 100;
                bottom: 0;
                left: 107%;
            }

                .custom.popup .column .image {
                    width: 100%;
                    box-shadow: 0px 0px 0px 5px rgba(0,0,0,0.1);
                    border-radius: 1px;
                }

                .custom.popup .column {
                    float: left;
                    width: 49%;
                    margin-right: 1%;
                }

                    .custom.popup .column img {
                        width: 100%;
                    }

            .ui.menu:hover .custom.popup {
                display: block !important;
            }

            .buttons-excel.buttons-html5 {
                margin-bottom: 10px;
                float: left;
            }

            #tableListing_length {
                float: right;
            }
        </style>
        <script type="text/javascript">
            var user = null;
            var userId = null;
            var isBlock = false;
            var permissionAllSalon = false;
            var permisionCheckError = false;
            let listSalon = [];
            var table = null;
            var elms = {
                btnSubmit: '.button.submit',
                resultSearch: '.dropdown.result',
                calendarDate: '.calendar.date',
                inputDateErrorFrom: '#from',
                inputDateErrorTo: '#to',
                tableError: 'table.table-error',
                inputSearch: '.wr-search input',
                inputDropdown: '.wr-dropdown input',
                inputSearchStaffManager: '.staff-manager .wr-search input',
                inputSearchSalonError: '.salon-error .wr-search input',
                inputSearchStatusError: '.status-handle .status input',
                inputIsPendingError: '.status-handle .is-pending input',
                inputIsCommitError: '.status-handle .is-commit input',
                resolveStatusError: '.resolve-status-error',
                pageMonitorStaff: '#page-monitor-staff',
                titleHead: '.title.head h4',
                imgError: '.img-error'
            };

            //on load
            $(document).ready(function () {
                //get user
                userId = ~~window.parent.document.getElementById("HDF_UserId").value;
                permissionAllSalon = ~~window.parent.document.getElementById("HDF_PermisionViewAllSalon").value;
                permisionCheckError = ~~window.parent.document.getElementById("HDF_PermisionCheckError").value;
                if (userId) {
                    user = window.getUserById(userId);
                    //set view by user
                    setViewByUser(user);
                    socketIoHandler(user);
                    //set time
                    $(elms.inputDateErrorFrom).val(window.getDate(new Date()));
                    $(elms.inputDateErrorTo).val(window.getDate(new Date()));

                } else {
                    alert("BẠN ĐANG ĐĂNG NHẬP BẰNG QUYỀN ROOT");
                    window.parent.href = window.consts.login;
                    user = null;
                }

                $.datetimepicker.setLocale('vi');
                $('.datetime-picker-v1').datetimepicker({
                    dayOfWeekStart: 1,
                    startDate: new Date(),
                    format: 'd-m-Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    scrollMonth: false,
                    scrollInput: false,
                    onChangeDateTime: function (dp, $input) { }
                });
                // Bind salon 
                let url = window.apis.monitorStaffService.domain;
                url += window.apis.monitorStaffService.salonsArea + "?staffId=" + userId + "&isAllSalon=" + permissionAllSalon;
                bindResultSearch(url, '', true);
                // Bind Handle 
                BindHandle();


            });

            //socket io
            var rootSocketServer = window.consts.rootSocketServer || "http://192.168.1.28:3001";
            var socket = io(rootSocketServer);
            socket.on('connected', function (data) {
                console.log(data);
            });

            //soket io
            function socketIoHandler(user) {
                console.log("socket handler ", user.salonWorkId);
                let salonID = user.salonWorkId;
                socket.on('app_monitor_' + salonID, btnXemClick);
            }

            //set view by user
            function setViewByUser(user) {
                //set staff review
                let inputSearchStaffManager = $(elms.inputSearchStaffManager);
                inputSearchStaffManager.attr('data-id', user === null ? 0 : user.id);
                inputSearchStaffManager.attr('data-name', user === null ? "root" : user.name);
                inputSearchStaffManager.val(user === null ? "root" : user.name);
            }

            //click input search
            $(elms.inputSearch).click(function () {
                let This = $(this);
                let fieldSearch = This.parent().parent().parent();
                let url = window.apis.monitorStaffService.domain;
                let isSalon = false;

                //check url
                if (fieldSearch.hasClass('category-error-root')) {
                    url += window.apis.monitorStaffService.caregoryErrorRoot;
                } else if (fieldSearch.hasClass('category-error-sub')) {
                    let rootId = $(elms.inputSearchCategoryErrorRoot).attr('data-id');
                    url += window.apis.monitorStaffService.categoryError + rootId;
                } else if (fieldSearch.hasClass('staff-report')) {
                    url += window.apis.monitorStaffService.staffMonitor;
                } else if (fieldSearch.hasClass('salon-error')) {
                    isSalon = true;
                    url += window.apis.monitorStaffService.salonsArea + "?staffId=" + userId + "&isAllSalon=" + permissionAllSalon;
                } else if (fieldSearch.hasClass('staff-manager')) {
                    url += window.apis.monitorStaffService.staffMonitor + "?isManager=true";
                }
                //bind search
                bindResultSearch(url, This, isSalon);

            });

            //staff manager
            $(elms.inputSearchStaffManager).click(function () {
                let This = $(this);
                let resultSearch = This.parent().parent().find('.result');
                resultSearch.show();
                $(resultSearch).find('.item').click(function () {
                    resultSearch.hide();
                    This.attr('data-id', $(this).attr('data-id'));
                    This.attr('data-name', $(this).attr('data-name'));
                    This.val($(this).attr('data-name'));
                });
            });

            //status error
            $(elms.inputDropdown).click(function () {
                let This = $(this);
                let resultSearch = This.parent().parent().find('.result');
                resultSearch.show();
                $(resultSearch).find('.item').click(function () {
                    resultSearch.hide();
                    This.attr('data-id', $(this).attr('data-id'));
                    This.attr('data-name', $(this).attr('data-name'));
                    This.val($(this).attr('data-name'));
                });
            });

            //bind search
            function bindResultSearch(url, elm, isSalon) {
                let input = $(elm);
                //ajax
                $.ajax({
                    contentType: "application/json",
                    type: "GET",
                    url: url,
                    dataType: "json",
                    crossDomain: true,
                    // headers: headers,
                    //data: JSON.stringify(data),
                    async: false,
                    success: function (response, textStatus, xhr) {
                        let resultSearch = input.parent().parent().find('.result');
                        if (response) {
                            let items = '';
                            //listSalon = [];
                            $(response).each(function (i, v) {
                                //bind salon
                                items += '<div class="item full-width border-bottom pointer" data-id="' + v.id + '" data-name="' + v.name + '">' + v.name + '</div>';
                                //if (isSalon) listSalon.push(v.id);
                            });

                            if (isSalon) {
                                var option = '<option value=0 selected="selected"> Chọn tất cả </option>';

                                $(response).each(function (index, value) {
                                    option += '<option value=' + value.id + ">" + value.name + "</option>";
                                });
                                $('select[name=salonId]').append(option);
                                $('.select').select2();
                            }
                            resultSearch.show();
                            resultSearch.empty().append(items);
                            //click item
                            resultSearch.find('.item').click(function () {
                                resultSearch.hide();
                                let id = $(this).attr('data-id');
                                let name = $(this).attr('data-name');
                                input.val($(this).attr('data-name'));
                                input.attr('data-id', id);
                                input.attr('data-name', name);
                            });
                        }
                    },
                    error: function (jqXhr, textStatus, errorMessager) {
                        if (jqXhr.status === 404) {
                            // some error
                            alert("Không tồn tại bản ghi");
                        }
                    }
                });
            }

            //check dbclick
            $(elms.btnSubmit).dblclick(function () {
            });

            function btnXemClick(isExcel = false) {

                //set view by user
                setViewByUser(user);
                //let strSalon = $(elms.inputSearchSalonError).attr('data-id');
                let strSalon = $('select[name=salonId]').val();
                let listSalonRequets = '';
                //let statusError = $(elms.inputSearchStatusError).attr('data-id');
                let statusError = $('select[name=handleId]').val();
                let isPending = $(elms.inputIsPendingError).attr('data-id');
                let isCommit = $(elms.inputIsCommitError).attr('data-id');
                let dateFrom = $(elms.inputDateErrorFrom).val();
                let dateTo = $(elms.inputDateErrorTo).val();

                if (dateFrom === '') {
                    alert("Bạn hãy chọn ngày bắt đầu");
                    return;
                }
                if (dateTo === '') {
                    alert("Bạn hãy chọn ngày kết thúc");
                    return;
                }
                if (strSalon === "" || typeof (strSalon) === 'undefined') {
                    alert("Bạn hãy chọn salon");
                    return;
                }

                else {
                    let salonId = ~~strSalon;
                    //if (salonId === 0) {
                    //    $.each(listSalon, function (index, value) {
                    //        if (value !== 0) {
                    //            listSalonRequets += "lstSalonId=" + value + "&";
                    //        }
                    //    });
                    //}
                    //else {
                    //    listSalonRequets += "lstSalonId=" + salonId + "&";
                    //}
                    if (salonId === 0) {
                        $.each($('select[name=salonId] option'), function (index, value) {
                            if ($(value).val() !== '0') {
                                listSalonRequets += "lstSalonId=" + $(value).val() + "&";
                            }
                        });
                    }
                    else {
                        listSalonRequets += "lstSalonId=" + salonId + "&";
                    }

                }
                if (!dateFrom || dateFrom === '') {
                    dateFrom = window.getDate(new Date());
                }
                if (!dateTo || dateTo === '') {
                    dateTo = window.getDate(new Date());
                }
                if (!statusError || statusError === '-1') {
                    statusError = null;
                } else {
                    statusError = ~~statusError;
                }

                if (isPending === '-1') {
                    isPending = null;
                } else if (isPending === '0' || isPending === '2') {
                    isPending = false;
                } else if (isPending === '1') {
                    isPending = true;
                }

                if (isCommit === '-1') {
                    isCommit = null;
                } else if (isCommit === '0') {
                    isCommit = false;
                } else if (isCommit === '1') {
                    isCommit = true;
                }

                if (isPending === false) {
                    isCommit = false;
                }

                let querytemp = {
                    dateFrom: dateFrom,
                    dateTo: dateTo,
                    isPending: isPending,
                    isCommit: isCommit,
                    statusError: statusError
                };
                var query = listSalonRequets + $.param(querytemp);
                //bind table
                if (table) {
                    $('#tableListing').DataTable().clear();
                    $('#tableListing').DataTable().destroy();
                }

                let url = window.apis.monitorStaffService.domain + window.apis.monitorStaffService.staffErrorV1 + "?" + query;
                startLoading();
                $.ajax({
                    type: "GET",
                    async: true,
                    url: url,
                    contentType: "application/json",
                    dataType: "json",
                    crossDomain: true,
                    // headers: headers,
                    //data: JSON.stringify(data),
                    success: function (response, textStatus, xhr) {
                        if (isExcel === true) {
                            //ExportToExcel(response);
                        }
                        else {
                            bindTable(response);
                        }
                        finishLoading();
                    },
                    error: function (jqXhr, textStatus, errorMessager) {
                        if (jqXhr.status === 404) {
                            // some error
                            alert("Không tồn tại bản ghi");
                            $(elms.tableError).find('tbody').empty();
                            finishLoading();
                        }
                    }
                });
            }

            //submit
            $(elms.btnSubmit).click(btnXemClick);
            //bind statistic table
            function bindTable(response) {
                if (table) {
                    $('#tableListing').DataTable().clear();
                    $('#tableListing').DataTable().destroy();
                }
                let tableError = $(elms.tableError).find('tbody');
                if (response) {
                    result = response;
                    var items = '';
                    var listHandle = GetListHandle();
                    $(response).each(function (i, v) {
                        let dateTime = v.errorTime.split(' ');

                        let periodBeforeCommit = '';
                        let periodBeforeConfirm = '';

                        if (v.periodBeforeConfirm) {
                            if (v.periodBeforeConfirm < 60) {
                                periodBeforeConfirm = v.periodBeforeConfirm + " giây";
                            } else {
                                periodBeforeConfirm = (Math.floor(v.periodBeforeConfirm / 60)) + " phút";
                            }
                        }

                        if (v.periodBeforeCommit) {
                            if (v.periodBeforeCommit < 60) {
                                periodBeforeCommit = v.periodBeforeCommit + " giây";
                            } else {
                                periodBeforeCommit = (Math.floor(v.periodBeforeCommit / 60)) + " phút";
                            }
                        }

                        let images = [];
                        let wrapImages = '';
                        if (v.images) {
                            images = v.images.split(',');
                            let itemImage = '';
                            if ($(images).length === 1) {
                                itemImage += '<div class="column"><div class="image"><img style="width:100%" src="' +
                                    $(images)[0] +
                                    '"></div></div>';
                            } else {
                                $(images).each(function (index, value) {
                                    itemImage += '<div class="column"><div class="image"><img src="' +
                                        value +
                                        '"></div></div>';
                                });
                            }
                            wrapImages = '<div class="ui menu position-relative"><img class="ui small image inline img-error" src="' +
                                (images[0] || '') +
                                '"><div class="ui custom popup equal width grid" style="display:none">' +
                                itemImage +
                                '</div></div></div>';
                        }
                        var strHanlde = '';
                        if (listHandle) {
                            strHanlde = '<div class="dropdown result position-absolute">';
                            var strEnd = '</div>';
                            strHanlde += '<div class="item full-width border-bottom pointer" data-id="0" data-name="Chọn trạng thái">Chọn trạng thái </div>';
                            $(listHandle).each(function (index, value) {
                                if ((v.categoryErrorRootId == 2 || v.categoryErrorRootId == 5) && value.id == 10) {
                                    return value;
                                }
                                strHanlde += '<div class="item full-width border-bottom pointer" style="cursor:pointer" data-id="' + value.id + '" data-name="' + value.name + '">' + value.name + '</div>';
                            });
                            strHanlde += strEnd;
                        }

                        items += '<tr data-id="' + v.id + '">' +
                            '<td>' + (i + 1) + '</td>' +
                            '<td>' + (dateTime[1] || 0) + '</td>' +
                            '<td>' + periodBeforeCommit + '</td>' +
                            '<td>' + periodBeforeConfirm + '</td>' +
                            '<td>' + (v.reporterName || '') + '</td>' +
                            '<td>' + (v.salonName || '') + '</td>' +
                            '<td>' + (v.staffTypeName || '') + '</td>' +
                            '<td>' + (dateTime[0] || '') + '</td>' +
                            '<td>' + wrapImages + '</td>' +
                            '<td>' + v.erpErrorHandleName + '</td>' +
                            '<td>' + (v.categoryErrorRootName || '') + '</td>' +
                            '<td>' + (v.categoryErrorSubName || '') + '</td>' +
                            '<td><p>' + (v.noteByReporter || '') + '</p></td>' +
                            '<td>' + (v.staffName || '') + '</td>' +
                            '<td>' + (v.handleName || '') + '</td>' +
                            '<td><p>' + (v.noteByReviewer || '') + '</p></td>' +
                            '<td class="resolve-status-error ">' +
                            '<div class="d-none excel-text">' + v.statusErrorName + ',</div>' +
                            '<div class="ui field wr-dropdown position-relative">' +
                            '<div class="ui input full-width" > ' +
                            '<input class="pl-1 pr-1" data-id="' + (v.statusError || '') + '" value="' + (v.statusErrorName || "Chọn trạng thái") + '" type="text" readonly="true" data-name="">' +
                            '</div> ' +
                            (permisionCheckError == '1' ? strHanlde : '') +
                            '</div >' +
                            '</td >' +
                            '</tr > ';
                    });
                    tableError.empty().append(items);

                    //click item
                    tableError.find('.resolve-status-error input').click(function () {
                        clickItemStatusError(this, isBlock);
                    });
                    ConfigTableListing();

                } else {
                    tableError.empty();
                }

            }
            function ConfigTableListing() {
                table = $('#tableListing').DataTable(
                    {
                        dom: 'Blfrtip',
                        "buttons": [{
                            "extend": 'excelHtml5',
                            "text": 'Xuất file excel',
                            //"exportOptions": _tableExportOptions,
                            "customize": _customizeExcelOptions
                        }],
                        ordering: false,
                        //searching: false,
                        //info: false,
                        iDisplayLength: 10,
                        pagingType: "full_numbers",
                        language: {
                            lengthMenu: "Dòng / trang _MENU_",
                            paginate: {
                                previous: "<",
                                first: "Đầu",
                                last: "Cuối",
                                next: ">"
                            },
                            info: "Dòng _START_ tới _END_ của _TOTAL_ dòng",
                            search: "Nhập tìm kiếm:"
                        },
                        lengthMenu: [[10, 100, 500, -1], [10, 100, 500, "Tất cả"]]
                    });
            }

            //resolve status error
            function clickItemStatusError(elm, isBlock) {
                if (isBlock) {
                    return;
                }
                let This = $(elm);
                let resultSearch = This.parent().parent().find('.result');
                resultSearch.show();

                $(resultSearch).find('.item').click(function () {
                    resultSearch.hide();
                    let statusError = ~~$(this).attr('data-id');
                    let name = $(this).attr('data-name');
                    This.attr('data-id', statusError);
                    This.attr('data-name', name);
                    This.parent().parent().parent().find('.excel-text').text(name + ",");
                    This.val(name);

                    //putResolveError
                    let id = This.parent().parent().parent().parent().attr('data-id');
                    let staffManagerId = ~~$(elms.inputSearchStaffManager).attr('data-id');
                    if (staffManagerId) {
                        let data = {
                            statusError: statusError,
                            resolverId: staffManagerId
                        };
                        putResolveError(id, data);
                    } else {
                        alert("Hãy chọn nhân viên quản lý giám sát");
                    }
                });
            }

            function putResolveError(id, data) {
                if (id && id > 0) {
                    let url = window.apis.monitorStaffService.domain + window.apis.monitorStaffService.staffError + id + "/statusError";
                    $.ajax({
                        contentType: "application/json",
                        type: "PUT",
                        url: url,
                        dataType: "json",
                        async: false,
                        crossDomain: true,
                        data: JSON.stringify(data),
                        success: function (response, textStatus, xhr) {
                            //window.parent.location = "/index.html";
                            //alert("Đã cập nhật trạng thái giải quyết lỗi.");
                        },
                        error: function (jqXhr, textStatus, errorMessager) {
                            alert("Lỗi khi cập nhật trạng thái. Bạn hãy thông báo với Admin");
                        }
                    });
                } else {
                    console.log("Lỗi. Mã bản ghi không tồn tại");
                }
            }

            //// export excel
            //function ExportExcel() {
            //    let dataExcel = btnXemClick(true);
            //}
            //function ExportToExcel(dataExcel) {
            //    let data = {
            //        jsonObj: JSON.stringify(dataExcel)
            //    };
            //    console.log(dataExcel);

            //    $.ajax({
            //        contentType: "application/json",
            //        type: "POST",
            //        url: "/GUI/BackEnd/MonitorStaff/MonitorStaffReportV1.aspx/ExportExel",
            //        dataType: "json",
            //        async: false,
            //        crossDomain: true,
            //        data: JSON.stringify(data),
            //        beforeSend: function () {
            //            //window.waitDialogShow();
            //        },
            //        complete: function () {
            //            //window.waitDialogHide();
            //        },
            //        success: function (response, textStatus, xhr) {
            //            window.open("http://" + location.host + response.d, "_blank");
            //            console.log("success");
            //        },
            //        error: function (jqXhr, textStatus, errorMessager) {
            //            console.log(errorMessager + "\n" + jqXhr.responseJSON);
            //        }
            //    });
            //}
            function GetListHandle() {
                var result = '';
                var listHandle = null;
                // Get list handle
                $.ajax({
                    contentType: "application/json",
                    type: "GET",
                    url: window.apis.monitorStaffService.domain + window.apis.monitorStaffService.handle,
                    dataType: "json",
                    async: false,
                    crossDomain: true,
                    beforeSend: function () {
                        //window.waitDialogShow();
                    },
                    complete: function () {
                        //window.waitDialogHide();
                    },
                    success: function (response, textStatus, xhr) {
                        result = response;
                        console.log("success");
                    },
                    error: function (jqXhr, textStatus, errorMessager) {
                        console.log(errorMessager + "\n" + jqXhr.responseJSON);
                    }
                });

                return result;
            }
            var _customizeExcelOptions = function (xlsx) {
                var sheet = xlsx.xl.worksheets['sheet1.xml'];
                var numrows = 5;
                var clR = $('row', sheet);
                console.log(sheet);

                //update Row
                clR.each(function (index, value) {
                    var listColumn = $(this).find('c');
                    $(listColumn).each(function (index, value) {
                        var columName = $(value).attr('r');
                        var columnId = columName.substring(1, 2);
                        var pre = columName.substring(0, 1);
                        if (pre === 'Q') {
                            var text = $(value).find('t').text();
                            if (text) {
                                var text1 = text.split(',');
                                $(value).find('t').text(text1[0]);
                            }
                        }
                    });

                });

            }
            function BindHandle() {
                var listHandle = GetListHandle();
                if (listHandle) {
                    var option = '<option value="-1">Chọn trạng thái</option>';
                    $(listHandle).each(function (i, v) {
                        option += '<option value=' + v.id + ">" + v.name + "</option>";
                    });
                    $('select[name=handleId]').append(option);
                    $('.select').select2();
                }
            }

        </script>
    </asp:Panel>
</asp:Content>

