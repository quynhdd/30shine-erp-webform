﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UpdatePhoneShineMember.aspx.cs" MasterPageFile="~/TemplateMaster/SiteMaster.Master" Inherits="_30shine.GUI.BackEnd.MonitorStaff.UpdatePhoneShineMember" %>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .btn-check-valid {
                width: 20%;
                background: #3a3939;
                color: #ffffff;
            }

            .btn-send {
                background: #3a3939;
                color: #ffffff;
            }
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Cập nhật Shine Member &nbsp;&#187; </li>
                        <li class="li-add active"><a href="#">Thêm mới yêu cầu</a></li>
                        <li class="li-listing"><a href="/xac-nhan-cap-nhat.html">Danh sách yêu cầu</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row">
                    <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                    <div class="filter-item">
                        <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeFrom" placeholder="Từ ngày"
                            ClientIDMode="Static" runat="server" AutoCompleteType="Disabled" Width="190px"></asp:TextBox>
                    </div>
                    <strong class="st-head" style="margin-left: 10px;">
                        <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                    </strong>
                    <div class="filter-item">
                        <asp:TextBox CssClass="txtDateTime st-head form-control" ID="TxtDateTimeTo" placeholder="Đến ngày"
                            ClientIDMode="Static" runat="server" AutoCompleteType="Disabled" Width="190px"></asp:TextBox>
                    </div>
                    <div class="filter-item">
                        <asp:TextBox CssClass="st-head form-control" ID="txtPhone" placeholder="Số điện thoại KH"
                            ClientIDMode="Static" runat="server" AutoCompleteType="Disabled" Width="190px"></asp:TextBox>
                    </div>


                </div>
                <div class="row row-filter">
                    <strong class="st-head" style="margin-right: 13px"><i class="fa fa-filter"></i>Lọc KQ</strong>

                    <div class="filter-item" style="margin-top:8px">
                        <asp:DropDownList ID="ddlSalon" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px;"></asp:DropDownList>
                    </div>
                    <div class="filter-item" style="margin-top:8px">
                        <asp:DropDownList ID="ddlService" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px;"></asp:DropDownList>
                    </div>

                    <div class="filter-item" style="margin-top:8px">
                        <asp:DropDownList ID="ddlProduct" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px;"></asp:DropDownList>
                    </div>

                    <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPagingInPage(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <%--<a href="/khach-hang/danh-sach.html" class="st-head btn-viewdata">Reset Filter</a>--%>
                </div>
                <!-- End Filter -->
                <!-- Row Table Filter -->
                <div class="table-func-panel">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="200">200</li>
                                <%--<li data-value="1000000">Tất cả</li>--%>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>

                        <div class="table-wp">
                            <table class="table-add table-listing">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Số ĐT</th>
                                        <th>Tên KH</th>
                                        <th>Shine Member</th>
                                        <th>Bill ID</th>
                                        <th>Số ĐT mới</th>
                                        <th>Tên KH mới</th>
                                        <th>Lý do</th>
                                        <th>Gửi đề xuất</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="RptCustomer" runat="server" OnItemDataBound="Repeater_ItemDataBound">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td class="old-customer">
                                                    <a href="/khach-hang/<%# Eval("CustomerId") %>.html" target="_blank" data-id="<%# Eval("CustomerId") %>" data-phone="<%# Eval("CustomerPhone") %>"><%# Eval("CustomerPhone") %></a>
                                                </td>
                                                <td class="customer-name"><%# Eval("CustomerName") %></td>
                                                <td class="is-shinmember">
                                                    <%# (Eval("IsShineMember") != null && Convert.ToInt32(Eval("IsShineMember")) == 1) ? "Có" : "Không" %>
                                                </td>
                                                <td class="bill-id"><a href="/dich-vu/<%# Eval("BillId") %>.html" target="_blank"><%# Eval("BillId") %></a> </td>
                                                <td style="width: 20%">
                                                    <input type="text" class="form-control new-phone" placeholder="Nhập số điện thoại mới" style="width: 70%; margin-right: 2%;">
                                                    <input type="button" class="form-control btn-check-valid" onclick="CheckPhoneNumber($(this))" value="Kiểm tra">
                                                </td>
                                                <td class="result-name"></td>
                                                <td class="update-note">
                                                    <textarea rows="3" class="form-control note" placeholder="Nhập lý do đổi số"></textarea></td>
                                                <td>
                                                    <input type="button" class="form-control btn-send" value="Gửi đề xuất" onclick="SendRequest($(this))" /></td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPagingInPage(1)">Đầu</a>
                                <a href="javascript://" onclick="excPagingInPage(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPagingInPage(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPagingInPage(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPagingInPage(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server"  OnClick="_BtnClick"  Text="Click" Style="display: none;" />
                <%--<asp:Button ID="BtnFakeExcel" ClientIDMode="Static" runat="server" OnClick="Btn_SwitchExcel" Style="display: none;" />--%>
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_ExcelType" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
            </div>
        </div>

        <script type="text/javascript" src="/Assets/js/jquery.select2.js"></script>    
        <script type="text/javascript">
            $(document).ready(function () {
                $(".select").select2();
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                    }
                });
               
            })
            function excPagingInPage(page) {
                if ($('#TxtDateTimeFrom').val() == "" || $('#TxtDateTimeTo').val() == "") {
                    ShowMessage("Thông báo", "Vui lòng chọn ngày tháng tìm kiếm", 4);
                    return;
                }
                 if ($("#ddlService").val() == 0 && $("#ddlProduct").val() == 0) {
                    ShowMessage("Thông báo", "Vui lòng chọn dịch vụ hoặc sản phẩm khách hàng đã dùng", 4);
                    return;
                }
                if ($("#txtPhone").val().length < 3) {
                    ShowMessage("Thông báo", "Số điện thoại phải lớn hơn hoặc bằng 3 kí tự", 4);
                    $("#txtPhone").focus();
                    return;
                }
                addLoading();
                $("#HDF_Page").val(page);
                $("#BtnFakeUP").click();

            }
            function ValidControl() {
                
               
                //return rs;
                //addLoading();
            }

            function CheckPhoneNumber(This) {
                var resultName = This.closest("tr").find(".result-name");
                var phone = This.closest("tr").find(".new-phone");
                if (phone.val().length != 10) {
                    ShowMessage("Thông báo", "Số điện thoại không hợp lệ. Vui lòng kiểm tra lại", 4);
                    phone.focus();
                    return false;
                }
                $.ajax({
                    url: "/GUI/BackEnd/MonitorStaff/UpdatePhoneShineMember.aspx/CheckPhoneNumber",
                    type: "POST",
                    data: "{phoneNumber:'" + phone.val() + "'}",
                    contentType: "application/json;charset:utf-8",
                    dataType: "json",
                    success: function (response) {
                        resultName.empty();
                        resultName.removeAttr("data-id");
                        var jsonData = JSON.parse(response.d)
                        console.log(jsonData.Fullname);
                        //debugger;
                        if (JSON.parse(response.d) != "false") {
                            resultName.text(jsonData.Fullname);
                            resultName.attr("data-id", jsonData.Id);
                        }
                        else {
                            resultName.attr("data-id", "0");
                            resultName.css("color", "red");
                            resultName.text("Không có dữ liệu");
                        }

                    }
                })
            }

            function SendRequest(This) {
                
                let obj = {
                    oldCustomerId: This.closest("tr").find(".old-customer a").attr("data-id"),
                    oldCustomerPhone: This.closest("tr").find(".old-customer a").attr("data-phone"),
                    newCustomerId: This.closest("tr").find(".result-name").attr("data-id"),
                    newCustomerPhone: This.closest("tr").find(".new-phone").val(),
                    billId: This.closest("tr").find(".bill-id").text(),
                    noteUpdate: This.closest("tr").find(".update-note .note").val()
                }
                if (obj.newCustomerPhone == null || obj.newCustomerPhone == "") {
                    ShowMessage("Thông báo", "Vui lòng nhập số điện thoại mới", 4);
                    This.closest("tr").find(".new-phone").focus();
                    return;
                }
                if (obj.noteUpdate == null || obj.noteUpdate == "") {
                    ShowMessage("Thông báo", "Vui lòng nhập lý do đổi số", 4);
                    This.closest("tr").find(".update-note .note").focus();
                    return;
                }
                var data = JSON.stringify(obj);
                console.log(data);
                $.ajax({
                    url: "/GUI/BackEnd/MonitorStaff/UpdatePhoneShineMember.aspx/SendRequest",
                    data: data,
                    dataType: "json",
                    contentType: "application/json;charset:utf-8",
                    type: "POST",
                    success: function (response) {
                        if (response.d == true) {
                            ShowMessage("Thông báo", "Gửi đề xuất thành công", 1);
                            This.closest("tr").remove();
                            return;
                        }
                        ShowMessage("Thông báo", "Gửi đề xuất thất bại", 4);
                    }
                })
            }
        </script>
    </asp:Panel>
</asp:Content>
