﻿<%@ Page Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="MonitorStaffSubmit.aspx.cs" Inherits="_30shine.GUI.BackEnd.MonitorStaff.MonitorStaffSubmit" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <title>Giám sát nhân viên - Thêm mới lỗi giám sát</title>
    <style>
        html, body {
            overflow: hidden
        }
    </style>
</asp:Content>

<asp:Content ID="MonitorStaffReport" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>

    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý giám sát nhân viên &nbsp;&#187;</li>
                        <li class="li-listing"><a href="/bao-cao-giam-sat-nhan-vien-v1.html">Báo cáo giám sát nhân viên</a></li>
                        <li class="li-listing"><a href="/bao-cao-thong-ke-giam-sat-nhan-vien.html">Báo cáo thống kê lỗi giám sát nhân viên</a></li>
                        <li class="li-add"><a href="/them-moi-loi-giam-sat-nhan-vien.html">Thêm mới lỗi giám sát nhân viên</a></li>
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="monitor-staff-submit-page" style="width: 100%; float: left; height: 100vh">
            <iframe id="monitorStaff" src="/monitor-staff-submit.html" style="width: 100%; float: left; height: 100vh"></iframe>
        </div>

        <%--main content--%>

        <asp:HiddenField ID="HDF_UserId" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="HDF_UserName" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="HDF_UserRole" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="HDF_SalonId" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="HDF_SalonName" runat="server" ClientIDMode="Static" />
        <asp:HiddenField ID="HDF_PermisionViewAllSalon" runat="server" ClientIDMode="Static" />
    </asp:Panel>
</asp:Content>
