﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Staff_Listing.aspx.cs" Inherits="_30shine.GUI.BackEnd.UIStaff.Staff_Listing" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ContentPlaceHolderID="head" runat="server">
    <title>Quản lý nhân sự | Danh sách</title>
    <style>
        .skill-table {
            display: table;
            width: 100%;
        }

            .skill-table .item-row {
                display: table-row;
                border-bottom: 1px solid #ddd;
            }

            .skill-table .item-cell {
                display: table-cell;
                border-bottom: 1px solid #ddd;
                padding-top: 5px;
                padding-bottom: 5px;
            }

                .skill-table .item-cell > span {
                    float: left;
                    width: 100%;
                    text-align: left;
                }

            .skill-table .item-row .item-cell .checkbox {
                float: left;
                padding-top: 0 !important;
                padding-bottom: 0 !important;
                margin-top: 6px !important;
                margin-bottom: 0px !important;
                margin-right: 9px;
                text-align: left;
                background: #ddd;
                padding: 2px 10px !important;
            }

                .skill-table .item-row .item-cell .checkbox input[type='checkbox'] {
                    margin-left: 0 !important;
                    margin-right: 3px !important;
                }

            .skill-table .item-cell-order {
                width: 30px !important;
            }

            .skill-table .item-cell-skill {
                width: 120px !important;
            }

        .modal {
            z-index: 9998;
        }

        .be-report .row-filter {
            z-index: 0;
        }

        @media(min-width: 768px) {
            .modal-content, .modal-dialog {
                width: 750px !important;
                margin: auto;
            }
        }
    </style>
</asp:Content>

<asp:Content ID="ListingContent" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý Nhân sự &nbsp;&#187; </li>
                        <li class="li-listing li-listing1"><a href="/admin/nhan-vien/danh-sach.html">Danh sách</a></li>
                        <% if (Perm_Add)
                            { %>
                        <li class="li-add"><a href="/admin/nhan-vien/them-moi.html">Thêm mới</a></li>
                        <% } %>
                        <li class="li-add"><a href="/admin/nhan-vien/edit-team.html">Set Team</a></li>
                        <li class="li-listing "><a href="/admin/bien-dong-nhan-su.html">Biến động nhân sự</a></li>
                        <li class="li-listing "><a href="/admin/bao-cao-bien-dong.html">Báo cáo biến động</a></li>
                        <%--<li class="li-listing li-roll"><a href="/admin/nhan-vien/diem-danh.html">Điểm danh</a></li>--%>
                        <%--<li class="li-listing li-payon"><a href="/admin/payon/them-moi.html">Cấu hình tiền công</a></li>--%>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row row-filter">
                    <div class="filter-item">
                        <asp:TextBox CssClass="txtDateTime st-head form-control" ID="txtTimeTo" Style="margin: 12px 0; width: 165px;" placeholder="Đến ngày"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlRegion" CssClass="form-control select" data-name="regionId" onchange="ChangeRegion(event)" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px;"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="Salon" CssClass="form-control select" data-name="salonId" onchange="ChangeSalon(event)" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px;"></asp:DropDownList>
                    </div>

                    <div class="filter-item">
                        <asp:DropDownList ID="StaffType" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px;"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="SkillLevel" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 150px;"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="Active" runat="server" CssClass="form-control select" ClientIDMode="Static" Style="margin: 12px 0; width: 150px;">
                            <asp:ListItem Text="Đang làm việc" Value="1" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Đã nghỉ" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Nghỉ tạm thời" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="row row-filter">
                    <div class="filter-item" style="margin-left: 0">
                        <asp:DropDownList ID="SnDay" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 150px;">
                            <%--<asp:ListItem Value="0" Text="--Chọn ngày--"></asp:ListItem>--%>
                        </asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="SnMonth" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 150px;"><%--<asp:ListItem Value="0" Text="--Chọn tháng--"></asp:ListItem>--%></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:TextBox ID="FilterEmail" placeholder="Tìm kiếm tên tài khoản" CssClass="form-control" Style="margin: 12px 0; width: 165px;" ClientIDMode="Static" runat="server"></asp:TextBox>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlPermission" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 150px;"><%--<asp:ListItem Value="0" Text="--Chọn năm--"></asp:ListItem>--%></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlContract" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 150px;"><%--<asp:ListItem Value="0" Text="--Chọn năm--"></asp:ListItem>--%></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlStaffProfile" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 150px;"><%--<asp:ListItem Value="0" Text="--Chọn năm--"></asp:ListItem>--%></asp:DropDownList>
                    </div>
                    <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <%-- <a href="/admin/nhan-vien/danh-sach.html" class="st-head btn-viewdata">Reset Filter</a>--%>
                    <div class="export-wp drop-down-list-wp">
                        <asp:Panel ID="fff" CssClass="st-head btn-viewdata"
                            ClientIDMode="Static" runat="server" onclick="EntryExportExcel('all');">
                            Xuất File Excel
                        </asp:Panel>
                    </div>
                </div>
                <!-- End Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>

                        <!-- Row Table Filter -->
                        <div class="table-func-panel" style="margin-top: 10px; margin-bottom: 10px;">
                            <table class="table table-bordered" style="width: 40%; float: left;">
                                <thead>
                                    <tr>
                                        <th scope="col" style="width: 20%; text-align: center;">Tổng nhân viên</th>
                                        <th scope="col" style="width: 2.5%; text-align: center;color: red;"><%=TotalStaff %></th>
                                        <th scope="col" style="width: 20%; text-align: center;">Đang hoạt động</th>
                                        <th scope="col" style="width: 2.5%; text-align: center;color: red;"><%=IsActive %></th>
                                        <th scope="col" style="width: 20%; text-align: center;">Đã nghỉ tạm thời</th>
                                        <th scope="col" style="width: 2.5%; text-align: center;color: red;"><%=IsTempLeave %></th>
                                        <th scope="col" style="width: 20%; text-align: center;">Đã nghỉ</th>
                                        <th scope="col" style="width: 2.5%; text-align: center;color: red;"><%=IsLeave %></th>
                                    </tr>
                                </thead>
                            </table>
                            <div class="table-func-elm">
                                <span>Số hàng / Page : </span>
                                <div class="table-func-input-wp">
                                    <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                                    <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                                    <ul class="ul-opt-segment">
                                        <li data-value="10">10</li>
                                        <li data-value="20">20</li>
                                        <li data-value="30">30</li>
                                        <li data-value="40">40</li>
                                        <li data-value="50">50</li>
                                        <li data-value="1000000">Tất cả</li>
                                    </ul>
                                    <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                                </div>
                            </div>
                        </div>
                        <!-- End Row Table Filter -->
                        <div class="table-wp">
                            <table class="table-add table-listing">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>ID</th>
                                        <th>Họ và tên</th>
                                        <th>Salon</th>
                                        <th>Bộ phận</th>
                                        <th style="width: 100px">Bậc</th>
                                        <%--<th>Điểm kỹ năng</th>
                                        <th>Tgian cắt TB</th>--%>
                                        <th>Số điện thoại</th>
                                        <th>Tài khoản</th>
                                        <th>Tên hợp đồng</th>
                                        <th>Ngày(Nâng bậc/Thâm niên)</th>
                                        <th style="width: 120px">Chức năng</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="RptStaff" runat="server" OnItemDataBound="Repeater_ItemDataBound">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td class="td-product-id" data-id="<%# Eval("StaffId") %>"><%# Eval("StaffId") %></td>
                                                <td>
                                                    <a href="/admin/nhan-vien/<%# Eval("StaffId") %>.html"><%# Eval("FullName") %></a>
                                                </td>
                                                <td><%# Eval("SalonName") %></td>
                                                <td><%# Eval("DepartmentName") %></td>
                                                <td><%# Eval("LevelName") %></td>
                                                <%-- <td><%# Eval("LevelPoint") %></td>
                                                   <td><%# Eval("CutTimeTB") %></td>--%>
                                                <%--    <td><%# Eval("OrderCode") %></td>--%>
                                                <td><%# Eval("StaffPhone") %></td>
                                                <td><%# Eval("StaffEmail") %></td>
                                                <td style="background-color: <%# Eval("ColorDate") %>"><%#Eval("ContractsName") %></td>
                                                <td><%# Eval("SeniorityDate")   %></td>
                                                <td class="map-edit">
                                                    <div class="edit-wp">
                                                        <asp:Panel CssClass="edit-action-wp action-edit" runat="server" ID="EAedit" Visible="false">
                                                            <a class="elm edit-btn" href="/admin/nhan-vien/<%# Eval("StaffId") %>.html" title="Sửa"></a>
                                                        </asp:Panel>
                                                        <asp:Panel CssClass="edit-action-wp action-delete" runat="server" ID="EAdelete" Visible="false">
                                                            <a class="elm del-btn" onclick="del(this.parentNode.parentNode.parentNode.parentNode,'<%# Eval("StaffId") %>', '<%# Eval("FullName") %>')" href="javascript://" title="Xóa"></a>
                                                        </asp:Panel>
                                                        <button type="button" class="btn btn-info btn-xs details" staffid="<%#Eval("StaffId") %>">Chi tiết</button>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>

                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:Button ID="BtnFakeExcel" ClientIDMode="Static" runat="server" OnClick="Exc_ExportExcel" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                <!-- Hidden Field-->
                <asp:HiddenField ID="HDF_Suggestion_Code" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_Suggestion_Field" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_ExcelType" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_ExportType" ClientIDMode="Static" runat="server" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELPage" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_EXCELSegment" />
                <!-- END Hidden Field-->
            </div>
            <!-- END Hidden Field-->
        </div>
        <%-- END Listing --%>
        </div>

        <%--<button type="button" class="btn btn-info btn-lg" data-toggle="modal" onclick="showModal_SetSkillLevel(400)">Open Modal</button>--%>

        <!-- Modal -->
        <%--<div id="systemModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Thiết lập điểm kỹ năng cho nhân viên</h4>
                    </div>
                    <div class="modal-body">
                        <div class="skill-table">
                            <div class="item-row item-row-head">
                                <div class="item-cell item-cell-order"><span>STT</span></div>
                                <div class="item-cell item-cell-skill"><span>Kỹ năng</span></div>
                                <div class="item-cell item-cell-levels"><span>Trình độ</span></div>
                            </div>
                            <div class="item-row">
                                <div class="item-cell item-cell-order"><span>1</span></div>
                                <div class="item-cell item-cell-skill"><span>Side Part</span></div>
                                <div class="item-cell item-cell-levels">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" />
                                            Can't check this
                                        </label>
                                    </div>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" onclick="setSkillLevel($(this), 400, 11, 1)" />
                                            Can't check this
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                    </div>
                </div>
            </div>
        </div>--%>
        <!-- Modal -->
        <div id="systemModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="filter-item" style="margin-left: 20px;">
                        <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="datepicker-wp" style="margin-top: 10px">
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeFrom" placeholder="Từ ngày" Style="height: 34px; width: 150px!important; border: 1px solid #ddd;"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                            <strong class="st-head" style="margin-left: 10px; margin-right: 10px;">
                                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                            </strong>
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TxtDateTimeTo" placeholder="Đến ngày" Style="height: 34px; width: 150px!important; border: 1px solid #ddd"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                            <button type="button" class="btn btn-default confirm" id="Confirm_Ratting">Tìm kiếm</button>
                        </div>
                    </div>
                    <div class="modal-header">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#home">Thông số tổng quát</a></li>
                            <li><a data-toggle="tab" href="#service">Chi tiết dịch vụ</a></li>
                            <li><a data-toggle="tab" href="#misstake">Tổng lỗi nhân viên</a></li>
                            <li><a data-toggle="tab" href="#hairstyle">Độ đa dạng kiểu tóc</a></li>
                        </ul>
                    </div>
                    <div class="modal-body">
                        <div class=" tab-content">
                            <div id="home" class="tab-pane fade in active">
                                <table class="skill-table display" id="totalResult">
                                </table>
                            </div>
                            <div id="service" class="tab-pane fade">
                                <table class="skill-table display" id="totalService">
                                </table>
                            </div>
                            <div id="misstake" class="tab-pane fade">
                                <table class="skill-table display" id="totalMisstake">
                                </table>
                            </div>
                            <div id="hairstyle" class="tab-pane fade">
                                <table class="skill-table display" id="totalHairStyle">
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="closeModal">Đóng</button>
                    </div>
                </div>
            </div>
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 165px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script>
            jQuery(document).ready(function () {
                // Add active menu
                $("#glbAdminContent").addClass("active");
                $("#glbAdminStaff").addClass("active");
                $("#subMenu .li-listing1").addClass("active");
            });


            /*
            * Hiển thị popup thiết lập điểm kỹ năng
            */
            //function showModal_SetSkillLevel(staffId) {
            //    startLoading();
            //    $.ajax({
            //        type: "POST",
            //        url: "/staff/skill/getlist",
            //        data: '{staffId : ' + staffId + '}',
            //        contentType: "application/json; charset=utf-8",
            //        dataType: "json", success: function (response) {
            //            if (response.d.success) {
            //                var data = response.d.data;
            //                if (data.length > 0) {
            //                    var itemRows = '<div class="skill-table">' +
            //                                        '<div class="item-row item-row-head">' +
            //                                            '<div class="item-cell item-cell-order"><span>STT</span></div>' +
            //                                            '<div class="item-cell item-cell-skill"><span>Kỹ năng</span></div>' +
            //                                            '<div class="item-cell item-cell-levels"><span>Trình độ</span></div>' +
            //                                        '</div>';
            //                    var itemLevels = "";
            //                    for (var i in data) {
            //                        itemLevels = "";
            //                        if (data[i].SkillLevels.length > 0) {
            //                            for (var j in data[i].SkillLevels) {
            //                                itemLevels += '<div class="checkbox">' +
            //                                                '<label>' +
            //                                                    '<input type="checkbox" onclick="setSkillLevel($(this),' + staffId + ','
            //                                                        + data[i].SkillId + ',' + data[i].SkillLevels[j].LevelId + ')" ' + (data[i].SkillLevels[j].IsChecked ? 'checked="checked"' : '') + '/>' + data[i].SkillLevels[j].LevelName +
            //                                                '</label>' +
            //                                            '</div>';
            //                            }
            //                            console.log(itemLevels);
            //                        }
            //                        itemRows += '<div class="item-row">' +
            //                                        '<div class="item-cell item-cell-order"><span>' + (parseInt(i) + 1) + '</span></div>' +
            //                                        '<div class="item-cell item-cell-skill"><span>' + data[i].SkillName + '</span></div>' +
            //                                        '<div class="item-cell item-cell-levels">' +
            //                                             itemLevels +
            //                                        '</div>' +
            //                                    '</div>';
            //                    }

            //                    itemRows += '</div>';

            //                    $("#systemModal .modal-body").html(itemRows);
            //                    $("#systemModal").modal();
            //                }
            //            }
            //            else {
            //                alert("Đã có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển.");
            //            }
            //            finishLoading();
            //        },
            //        failure: function (response) { alert(response.d); }
            //    });

            //    $('#systemModal').on('hidden.bs.modal', function () {
            //        $("#ViewDataFilter").click();
            //    })
            //}

            /*
            * Call ajax thiết lập điểm kỹ năng
            */
            //function setSkillLevel(This, staffId, skillId, levelId) {
            //    startLoading();
            //    $.ajax({
            //        type: "POST",
            //        url: "/staff/skill/setlevel",
            //        data: '{staffId : ' + staffId + ', skillId : ' + skillId + ', levelId : ' + levelId + ', isChecked : ' + (This.prop("checked")) + '}',
            //        contentType: "application/json; charset=utf-8",
            //        dataType: "json", success: function (response) {
            //            if (response.d.success) {
            //                //
            //            }
            //            else {
            //                alert("Đã có lỗi xảy ra. Vui lòng liên hệ nhóm phát triển.");
            //            }
            //            finishLoading();
            //        },
            //        failure: function (response) { alert(response.d); }
            //    });
            //}

            //============================
            // Event delete
            //============================
            function del(This, code, name) {
                var code = code || null,
                    name = name || null,
                    Row = This;
                if (!code) return false;

                // show EBPopup
                $(".confirm-yn").openEBPopup();
                $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

                $("#EBPopup .yn-yes").bind("click", function () {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/SystemService/Ajax/Del.aspx/Delele_Staff",
                        data: '{Code : "' + code + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json", success: function (response) {
                            var mission = JSON.parse(response.d);
                            if (mission.success) {
                                delSuccess();
                                Row.remove();
                            } else {
                                delFailed();
                            }
                        },
                        failure: function (response) { alert(response.d); }
                    });
                });
                $("#EBPopup .yn-no").bind("click", function () {
                    autoCloseEBPopup(0);
                });
            }
        </script>
        <script>
            $(document).ready(function () {
                $(function () {
                    $('.txtDateTime').datetimepicker({
                        dayOfWeekStart: 1,
                        lang: 'vi',
                        startDate: '2014/10/10',
                        format: 'd/m/Y',
                        dateonly: true,
                        showHour: false,
                        showMinute: false,
                        timepicker: false,
                        onChangeDateTime: function (dp, $input) {
                        }
                    });
                });
            });

            $("#subMenu .li-listing1").addClass("active");
            var staffID;
            var timeFrom;
            var timeTo;
            $(document).on("click", "#closeModal", function () {
                $("#EndWork").show();
                $("#Ratting").show();
                $("#systemModal").modal("hide");
                $("table tbody tr").css("background-color", "white");
                $("#TxtDateTimeFrom").val("");
                $("#TxtDateTimeTo").val("");
            })

            $(document).on("click", ".details", function () {
                $("#systemModal").modal();
                $(this).closest("tr").css("background-color", "green");
                staffID = $(this).attr("staffID");
            })

            $(document).on("click", "#Confirm_Ratting", function () {

                timeFrom = $("#TxtDateTimeFrom").val();
                timeTo = $("#TxtDateTimeTo").val();
                //if (timeFrom != "" && timeTo != ""){
                $.ajax({
                    type: "POST",
                    data: '{timeFrom:"' + timeFrom + '", timeTo:"' + timeTo + '", staffID:' + staffID + '}',
                    contentType: "application/json; charset=utf-8",
                    datatype: "JSON",
                    url: "/GUI/BackEnd/Staff/Staff_Listing.aspx/GetTotalResult",
                    success: function (data) {
                        //console.log(data.d);
                        var jsonData = $.parseJSON(data.d);
                        var itemRows = '<thead>' +
                            '<tr class="item-row item-row-head">' +
                            '<th class="item-cell item-cell-skill" style="display:none"><span>Tổng doanh thu</span></th>' +
                            '<th class="item-cell item-cell-skill"><span>Tổng giờ làm</span></th>' +
                            '<th class="item-cell item-cell-skill"><span>Năng suất TB</span></th>' +
                            '<th class="item-cell item-cell-skill"><span>Lỗi KCS</span></th>' +
                            '<th class="item-cell item-cell-skill"><span>Điểm SCSC TB</span></th>' +
                            '<th class="item-cell item-cell-skill"><span>Tốc độ cắt TB</span></th>' +
                            '<th class="item-cell item-cell-skill"><span>Tổng điểm hài lòng</span></th>' +
                            '</tr>' +
                            '</thead>' +
                            '<tbody>';
                        for (var i = 0; i < jsonData.length; i++) {

                            itemRows += '<tr class="item-row">' +
                                '<td class="item-cell item-cell-skill" style="display:none"><span>' + jsonData[i].TotalMoney.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '</span></td>' +
                                '<td class="item-cell item-cell-skill"><span>' + jsonData[i].TotalHours + '</span></td>' +
                                '<td class="item-cell item-cell-skill"><span>' + (Math.round(jsonData[i].TotalMoney / jsonData[i].TotalHours)).toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") + '</span></td>' +
                                '<td class="item-cell item-cell-skill">' + jsonData[i].Total_SCSC_Error + '</td>' +
                                '<td class="item-cell item-cell-skill">' + jsonData[i].PointSCSC_TB + '</td>' +
                                '<td class="item-cell item-cell-skill">' + jsonData[i].AVGTimeCut + '</td>' +
                                '<td class="item-cell item-cell-skill">' + jsonData[i].TotalPoint + '</td>' +
                                '</tr>';
                        }
                        itemRows += '</tbody>'
                        $("#systemModal .modal-body #totalResult").html(itemRows);

                    }
                })
                // Chi tiết dịch vụ
                $.ajax({
                    type: "POST",
                    data: '{timeFrom:"' + timeFrom + '", timeTo:"' + timeTo + '", staffID:' + staffID + '}',
                    contentType: "application/json; charset=utf-8",
                    datatype: "JSON",
                    url: "/GUI/BackEnd/Staff/Staff_Listing.aspx/GetTotalService",
                    success: function (data) {
                        var jsonData = $.parseJSON(data.d);
                        var itemRows = '<thead>' +
                            '<tr class="item-row item-row-head">' +
                            '<th class="item-cell item-cell-skill"><span>Tên dịch vụ</span></th>' +
                            '<th class="item-cell item-cell-skill"><span>Số lượng</span></th>' +
                            '</tr>' +
                            '</thead>' +
                            '<tbody>';
                        for (var i = 0; i < jsonData.length; i++) {

                            itemRows += '<tr class="item-row">' +
                                '<td class="item-cell item-cell-skill"><span>' + jsonData[i].Service_Name + '</span></td>' +
                                '<td class="item-cell item-cell-skill"><span>' + jsonData[i].QUANTITY + '</span></td>' +
                                '</tr>';
                        }
                        itemRows += '</tbody>'
                        $("#systemModal .modal-body #totalService").html(itemRows);
                    }
                })
                // Tổng lỗi nhân viên
                $.ajax({
                    type: "POST",
                    data: '{timeFrom:"' + timeFrom + '", timeTo:"' + timeTo + '", staffID:' + staffID + '}',
                    contentType: "application/json; charset=utf-8",
                    datatype: "JSON",
                    url: "/GUI/BackEnd/Staff/Staff_Listing.aspx/GetTotalMisstake",
                    success: function (data) {
                        var __jsonData = $.parseJSON(data.d);

                        var __itemRows = '<thead>' +
                            '<tr class="item-row item-row-head">' +
                            '<th class="item-cell item-cell-skill"><span>Loại lỗi</span></th>' +
                            '<th class="item-cell item-cell-skill"><span>Số lần phạt</span></th>' +
                            '<th class="item-cell item-cell-skill"><span>Số lần nhắc nhở</span></th>' +

                            '</tr>' +

                            '</thead>' +
                            '<tbody>';
                        for (var i = 0; i < __jsonData.length; i++) {

                            __itemRows += '<tr class="item-row">' +
                                '<td class="item-cell item-cell-skill">' + __jsonData[i].Formality + '</td>' +
                                '<td class="item-cell item-cell-skill"><span>' + __jsonData[i].MISSTAKE_COUNT + '</span></td>' +
                                '<td class="item-cell item-cell-skill">' + __jsonData[i].SoLanNhacNho + '</td>' +
                                '</tr>';
                        }
                        __itemRows += '</tbody>'
                        $("#systemModal .modal-body #totalMisstake").html(__itemRows);

                    }
                })
                // ĐỘ đa dạng kiểu tóc tóc
                $.ajax({
                    type: "POST",
                    data: '{timeFrom:"' + timeFrom + '", timeTo:"' + timeTo + '", staffID:' + staffID + '}',
                    contentType: "application/json; charset=utf-8",
                    datatype: "JSON",
                    url: "/GUI/BackEnd/Staff/Staff_Listing.aspx/GetTotalHairStyle",
                    success: function (data) {
                        var _jsonData = $.parseJSON(data.d);

                        var _itemRows = '<thead>' +
                            '<tr class="item-row item-row-head">' +
                            '<th class="item-cell item-cell-skill"><span>Kiểu tóc</span></th>' +
                            '<th class="item-cell item-cell-skill"><span>Số lượng</span></th>' +

                            '</tr>' +

                            '</thead>' +
                            '<tbody>';
                        for (var i = 0; i < _jsonData.length; i++) {

                            _itemRows += '<tr class="item-row">' +
                                '<td class="item-cell item-cell-skill"><span>' + _jsonData[i].Hair_Name + '</span></td>' +
                                '<td class="item-cell item-cell-skill"><span>' + _jsonData[i].Total_HairStyle + '</span></td>' +
                                '</tr>';
                        }
                        _itemRows += '</tbody>'
                        $("#systemModal .modal-body #totalHairStyle").html(_itemRows);

                    }
                })
                //}
                //else {
                //    alert("Bạn phải chọn ngày tháng!");
                //}
            });
            function ChangeSalon(event) {
                if (event.target.value != "0") {
                    $('select[data-name=regionId]').attr('disabled', 'disabled');
                }
                else {
                    $('select[data-name=regionId]').removeAttr('disabled');
                }
                $('select[data-name=regionId]').val(0);
                $('.select').select2();
            }
            function ChangeRegion(event) {
                if (event.target.value != "0") {
                    $('select[data-name=salonId]').attr('disabled', 'disabled');
                }
                else {
                    $('select[data-name=salonId]').removeAttr('disabled');
                }
                $('select[data-name=salonId]').val(0);
                $('.select').select2();
            }
        </script>
    </asp:Panel>
</asp:Content>
