﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMasterVersion1.Master" AutoEventWireup="true" CodeBehind="ReportGetFileOfSalons.aspx.cs" Inherits="_30shine.GUI.BackEnd.UIStaff.ReportGetFileOfSalons" %>

<asp:Content ID="CustomerAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel ID="ContentWrap" runat="server" ClientIDMode="Static">

        <style>
            .menu-time {
                margin-right: 7px;
                font-size: 13px;
            }

                .menu-time .label {
                    padding: 0px 15px;
                    background: #000000;
                    color: #ffffff;
                    cursor: pointer;
                    border-radius: 15px;
                    line-height: 26px;
                    width: 94px;
                }

            .form-control {
                height: 31px !important;
            }

            .select2.select2-container.select2-container--default {
                width: 180px !important;
            }

            .lable-text {
                color: black;
                font-weight: 600;
                font-size: 15px;
            }

            .dt-buttons.btn-group {
                float: left;
            }

            #ContentWrap .btn-viewdata:hover {
                color: yellow;
            }

            #tableListing tr th {
                text-align: center !important;
                vertical-align: middle !important;
            }

            #tableListing tr td {
                text-align: center !important;
                vertical-align: middle !important;
            }

             #tableListing tbody tr:nth-child(1){
                 color: white;
                 background:#808080;
             }

            .table-listing table thead {
                background-color: white !important;
            }

            #txtEndDate {
                float: right;
                width: auto;
            }

            table.dataTable tbody>tr.selected, table.dataTable tbody>tr>.selected{
                background-color:#0275d8 !important;
            }
        </style>
        <div class="container-fluid sub-menu">
            <div class="row">
                <div class="col-md-12 pt-3 pb-2">
                    <ul>
                        <li>Báo cáo &nbsp;&#187;&nbsp; Thống kê nhân sự</li>
                    </ul>
                </div>
                <div class="col-md-12 form-group">
                    <div class="sub-menu-boder"></div>
                </div>
            </div>
        </div>
        <form runat="server">
            <div class="container-fluid">
                <div class="row">
                    <div class="form-group col-xl-auto col-lg-auto">
                        <strong><i class="fa fa-filter"></i>Lọc kết quả &nbsp;&#187; </strong>
                    </div>
                    <div class="col-xl-auto col-lg-auto col-md-12 col-sm-auto form-group">
                        <label class="mr-2 lable-text">Thời gian</label>
                        <asp:TextBox CssClass="datetime-picker form-control" ID="txtEndDate" placeholder="Trước ngày"
                            ClientIDMode="Static" runat="server"></asp:TextBox>

                    </div>
                    <div class="col-xl-auto col-lg-auto col-md-12 col-sm-auto form-group">
                        <label class="mr-2 lable-text">Khu vực</label>
                        <asp:DropDownList runat="server" ID="ddlRegion" CssClass="form-control select form-box-left" ClientIDMode="Static" onchange="GetDropDownData($(this));"></asp:DropDownList>
                    </div>
                    <div class="col-xl-auto col-lg-auto col-md-12 col-sm-auto form-group">
                        <label class="mr-2 lable-text">Salon</label>
                        <asp:DropDownList runat="server" ID="ddlSalon" CssClass="form-control select form-box-left" ClientIDMode="Static"></asp:DropDownList>
                    </div>
                    <div class="form-group col-xl-auto col-lg-auto col-md-12 col-sm-auto form-group">
                        <div id="ViewDataFilter" class="form-group float-left mr-1" onclick="viewData()">
                            <div class="btn-viewdata">Xem dữ liệu</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid table-listing pt-2">
                <table id="tableListing" class="table table-bordered table-hover thead-light selectable" style="float: left;">
                    <thead>
                        <tr>
                            <th rowspan="2" class="text-center">STT</th>
                            <th rowspan="2" class="text-center">KHU VỰC</th>
                            <th rowspan="2" class="text-center">TÊN SALON<hr />TỔNG SỐ 3 TỶ LỆ</th>
                            <th rowspan="2" class="text-center">TỔNG SỐ NHÂN VIÊN</th>
                            <th colspan="3" class="text-center">TỶ LỆ THU HỒ SƠ</th>
                            <th colspan="3" class="text-center">TỶ LỆ THU HỢP ĐỒNG HỌC NGHỀ</th>
                            <th colspan="3" class="text-center">TỶ LỆ THU HỢP ĐỒNG LAO ĐỘNG</th>
                        </tr>
                        <tr>
                            <th class="text-center" style="background: #ff6b6b !important">SỐ NHÂN VIÊN ĐỦ 22 NGÀY</th>
                            <th class="text-center" style="background: #aaffa9 !important">SỐ NHÂN VIÊN NỘP ĐỦ<br />
                                3 LOẠI GIẤY TỜ CHÍNH</th>
                            <th class="text-center">TỶ LỆ %</th>
                            <th class="text-center" style="background: #ff6b6b !important">SỐ NHÂN VIÊN ĐỦ 20 NGÀY</th>
                            <th class="text-center" style="background: #cbb4d4 !important">SỐ NHÂN VIÊN ĐÃ CÓ HĐHN</th>
                            <th class="text-center">TỶ LỆ %</th>
                            <th class="text-center" style="background: #6dd5ed !important">SỐ HĐLĐ PHÁT ĐI</th>
                            <th class="text-center" style="background: #99f2c8 !important">SỐ HĐLĐ THU VỀ</th>
                            <th class="text-center">TỶ LỆ %</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </form>
        <script>
            var table = null;
            var SalonIds = [];

            ///get salon
            function GetDropDownData(This) {
                let $region = This.val();
                if ($region === undefined || $region === null || $region === '') {
                    return;
                }
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Staff/ReportGetFileOfSalons.aspx/BindSalonByRegion",
                    data: '{RegionId: ' + $region + ' }',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $("#ddlSalon").html('').val('0');
                        $("#select2-ddlSalon-container").val('0').text('Chọn salon');
                        $("#ddlSalon").append($("<option />").val(0).text('Chọn salon'));
                        $.each(data.d, function () {
                            $("#ddlSalon").append($("<option />").val(this.Id).text(this.ShortName));
                        });
                    },
                    failure: function () {
                        ShowMessage('', 'Có lỗi xảy ra, vui lòng liên hệ nhà phát triển!', 4);
                    }
                });
            }

            //get salonids
            function GetSalonIds() {
                let Ids = [];
                $('#ddlSalon option').each(function () {
                    if (CheckParameter(this.value)) {
                        let salonId = parseInt(this.value);
                        Ids.push(salonId);
                    }
                });
                SalonIds = Ids;
            }

            // get data
            function viewData() {
                GetSalonIds();
                let error = false;
                let $salon = $('#ddlSalon :selected').val();
                let $date = $('#txtEndDate').val();
                if (!CheckParameter($salon)) {
                    if (SalonIds.length === 0) {
                        ShowMessage('', 'Khu vực này chưa có salon!', 4);
                        error = true;
                    }
                }
                else {
                    SalonIds = [];
                    SalonIds.push(parseInt($salon));
                }

                if (!CheckParameter($date)) {
                    ShowMessage('', 'Vui lòng chọn ngày!', 4);
                    error = true;
                }

                if (!error) {
                    startLoading();
                    var data = JSON.stringify({ endDate: $date, salonIds: SalonIds });
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/Staff/ReportGetFileOfSalons.aspx/GetReportData",
                        data: data,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            
                            if (!data.d.success) {
                                ShowMessage('', 'Có lỗi xảy ra, vui lòng liên hệ nhà phát triển!', 4);
                            }
                            else {

                                $(data.d.data).each(function (index, value) {
                                    value.TotalPercentFiles = round(value.TotalPercentFiles,2) + '%';
                                    value.TotalPercentContractStudy = round(value.TotalPercentContractStudy,2) + '%';
                                    value.TotalPercentHDLDGoOutRecieved = round(value.TotalPercentHDLDGoOutRecieved,2) + '%';
                                    return value;
                                });

                                if (table) {
                                    $('#tableListing').DataTable().clear();
                                    $('#tableListing').DataTable().destroy();
                                }
                                table = $('#tableListing').DataTable(
                                    {
                                        dom: 'Blfrtip',
                                        buttons: [
                                            'excel'
                                        ],
                                        paging: false,
                                        info: false,
                                        fixedHeader: true,
                                        ordering: false,
                                        scrollY: "70vh",
                                        scrollX: false,
                                        scrollCollapse: true,
                                        searching: false,
                                        data: data.d.data,
                                        columns: [
                                            { "data": "STT" },
                                            { "data": "CityName" },
                                            { "data": "SalonName" },
                                            { "data": "TotalStaff" },
                                            { "data": "TotalStaffHaveExp22" },
                                            { "data": "TotalStaffEnoughProfile" },
                                            { "data": "TotalPercentFiles" },
                                            { "data": "TotalStaffHaveExp20" },
                                            { "data": "TotalStaffContract" },
                                            { "data": "TotalPercentContractStudy" },
                                            { "data": "TotalHDLDGoOut" },
                                            { "data": "TotalHDLDReceived" },
                                            { "data": "TotalPercentHDLDGoOutRecieved" }
                                        ]
                                    });
                                finishLoading();
                                ShowMessage('', 'Lấy dữ liệu thành công!', 2);
                                
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown ) {
                            finishLoading();
                        	ShowMessage('', `Có lỗi xảy ra, ${textStatus}`, 4, 10000);
                            ShowMessage('', 'Có lỗi xảy ra, vui lòng liên hệ nhà phát triển!', 4);
                            
                        }
                    });
                }
            }

            // check parameter
            function CheckParameter(param) {
                if (param === undefined || param === null || param === '' || param === '0') {
                    return false;
                }
                return true;
            }
        </script>
    </asp:Panel>
</asp:Content>
