﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Staff_Edit.aspx.cs" Inherits="_30shine.GUI.BackEnd.UIStaff.Staff_Edit" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content runat="server" ID="headContent" ContentPlaceHolderID="head">
    <script src="/Assets/js/sweetalert.min.js"> </script>
    <link rel="stylesheet" type="text/css" href="/Assets/css/sweetalert.css" />

</asp:Content>
<asp:Content ID="StaffAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            #success-alert {
                top: 100px;
                right: 10px;
                position: fixed;
                width: 20% !important;
                z-index: 2000;
            }

            table.dataTable {
                border-collapse: collapse !important;
            }

            .skill-table {
                display: table;
                width: 100%;
            }

                .skill-table .item-row {
                    display: table-row;
                    border-bottom: 1px solid #ddd;
                }

                .skill-table .item-cell {
                    display: table-cell;
                    border: 1px solid #ddd;
                    padding: 5px;
                }

                    .skill-table .item-cell > span {
                        float: left;
                        width: 100%;
                        text-align: center;
                    }

                .skill-table .item-row .item-cell .checkbox {
                    float: left;
                    padding-top: 0 !important;
                    padding-bottom: 0 !important;
                    margin-top: 6px !important;
                    margin-bottom: 0px !important;
                    margin-right: 9px;
                    text-align: left;
                    background: #ddd;
                    padding: 2px 10px !important;
                }

                    .skill-table .item-row .item-cell .checkbox input[type='checkbox'] {
                        margin-left: 0 !important;
                        margin-right: 3px !important;
                    }

                .skill-table .item-cell-order {
                    width: 30px !important;
                }

            table.table-quick-infor td {
                padding-top: 5px;
                padding-bottom: 5px;
            }

            table.table-quick-infor tr {
                border-bottom: 1px solid #DDDDDD;
            }

                table.table-quick-infor tr.no-border-bottom {
                }

            table.table-log-skill-level {
                margin-top: 5px;
            }

            table.table-log-skill-level {
                border-collapse: collapse;
            }

                table.table-log-skill-level,
                table.table-log-skill-level th,
                table.table-log-skill-level td {
                    border: 1px solid black;
                    padding: 3px 10px;
                }

                    table.table-log-skill-level th {
                        font-weight: normal;
                        font-family: Roboto Condensed Bold;
                        text-align: center;
                    }

            .content-toggle .error {
                border: 1px solid #ff0000;
            }

            .content-toggle .text-warning {
                display: none;
                color: red;
                padding-right: 5px;
                padding-top: 18px;
                padding-left: 7px;
                font-size: 13px;
            }

            .content-toggle .text-success {
                display: none;
                color: green;
                padding-right: 5px;
                padding-top: 20px;
                padding-left: 7px;
                font-size: 13px;
            }

            .f_btn_reset {
                text-decoration: none !important;
                font-style: inherit !important;
                color: #FFF !important;
                width: 101px !important;
                padding: 0px !important;
                padding-left: 4px !important;
                display: block !important;
                float: right !important;
                margin-top: -30px !important;
            }

            .staff-servey-mark span {
                height: 28px !important;
                line-height: 28px !important;
            }

            .staff-servey-mark td input[type="radio"] {
                top: 4px !important;
            }

            .cssradio label {
                margin-left: 2px;
                margin-right: 7px;
                float: left;
            }

            .cssradio input {
                float: left;
                margin-top: 0px;
            }

            #pem {
                width: 100%;
            }

                #pem span {
                    width: 100%;
                    line-height: 22px;
                    height: 22px;
                }

                    #pem span div.btn-group .multiselect, #pem span div.btn-group, .multiselect-container {
                        width: 100% !important;
                    }

            #PermissionList {
                width: 100% !important;
            }

            #ProfileId {
                width: 100% !important;
            }

            .staff-servey-mark td input[type="radio"] {
                top: 4px !important;
            }

            .customer-add .table-add .tr-birthday input[type="text"] {
                width: 80px;
                margin-left: 10px;
                text-align: center;
                margin-bottom: 10px;
            }

            .listing-img-upload .thumb-wp {
                width: 100%;
                height: 150px;
                margin-right: 10px;
                margin-top: 10px;
                overflow: hidden;
                float: left;
                position: relative;
                border: 1px solid #dfdfdf;
                cursor: pointer;
                border-radius: 10px;
            }

            .f_btn_reset {
                text-decoration: none !important;
                font-style: inherit !important;
                background: #000;
                color: #FFF;
                padding: 2px 7px 3px;
                cursor: pointer;
            }

                .f_btn_reset:hover {
                    color: #ffe400;
                }

            /*.customer-add .table-add td .f_btn_reset {
                 height: 36px; 
                 line-height: 36px; 
                 width: 100%; 
                 float: left; 
            }*/

            .listing-img-upload .thumb-wp .thumb {
                width: 100% !important;
                height: 200px !important;
                cursor: pointer;
                border-radius: 10px;
                float: left;
            }

            .nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {
                color: #fff;
                background-color: black !important;
            }

            .nav-pills > li > a:hover {
                color: #fff;
                background-color: black !important;
            }

            .tr_Top {
                margin-top: 10px;
            }

            .test_Skill:hover {
                cursor: pointer !important;
                color: #00ffff;
            }

            .test_Skill {
                color: red;
            }

            .field-wp .div_col a#linkShampoo {
                font-size: 20px;
            }

                .field-wp .div_col a#linkShampoo:hover {
                    color: red;
                }

                .field-wp .div_col a#linkShampoo:before, .field-wp .div_col a#linkShampoo::after, .field-wp .div_col a#linkShampoo:active, .field-wp .div_col a#linkShampoo:focus {
                    color: black;
                }
            /* Modal Content (image) */
            .modal-content-image {
                margin: auto;
                margin-top: 30px;
                display: block;
                width: 50%;
                max-width: 500px;
                height: auto;
                max-height: 600px;
            }

            /* Caption of Modal Image */
            #caption-image {
                margin: auto;
                display: block;
                width: 60%;
                max-width: 700px;
                text-align: center;
                color: #ccc;
                padding: 10px 0;
                height: 150px;
                margin-top: 10px;
            }

            /* Add Animation */
            .modal-content-image, #caption-image {
                -webkit-animation-name: zoom !important;
                -webkit-animation-duration: 0.6s !important;
                animation-name: zoom !important;
                animation-duration: 0.6s !important;
            }

            @media(min-width: 768px) {
                .modal-content, .modal-dialog {
                    width: 800px !important;
                    margin: auto;
                    margin-top: 50px;
                }
            }

            #menu1 .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
                cursor: pointer;
                background-color: #fafafa;
                border: none;
                opacity: 1;
            }

            #menu2 .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
                cursor: pointer;
                background-color: #fafafa;
                border: none;
                opacity: 1;
            }

            .contract {
                height: 45px;
                width: 100%;
                float: left;
            }

            .child {
                width: 20% !important;
                float: left;
                margin-right: 15px;
                /*padding-top: 10px;*/
            }

                .child.text-contract {
                    width: 141px !important;
                }

            .btn.btn-complete.child.btn-contract {
                margin-right: 0px !important;
            }

            .select2-container {
                width: 49% !important;
                margin-top: 0px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 36px !important;
                padding-top: 1px !important;
            }

            #chkLDQL, #chkLDSL {
                margin-top: 12px;
            }
        </style>
        <div class="alert alert-danger" id="success-alert">
            <span id="msg-alert"></span>
        </div>
        <div class="wp sub-menu" runat="server" id="ActionMenu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý Nhân sự &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/admin/nhan-vien/danh-sach.html">Danh sách</a></li>
                        <li class="li-add"><a href="/admin/nhan-vien/them-moi.html">Thêm mới</a></li>
                        <li class="li-add"><a href="/admin/nhan-vien/edit-team.html">Set Team</a></li>
                        <li class="li-edit"><a href="javascript://">Chỉnh sửa</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add">
            <%-- Add --%>
            <div class="wp960 content-wp content-toggle" id="StaffInfoWrap">
                <!-- System Message -->
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <!-- END System Message -->
                <div class="table-wp" style="width: 220px; margin-left: -280px;">
                    <table class="table-add" style="border-right: solid 1px #d9d9d9;">
                        <tbody>
                            <tr class="tr-upload">
                                <td class="col-xs-12 right">
                                    <div id="myModalimage" style="z-index: 2000;" class="modal modal-image">
                                        <img class="modal-content-image" id="img01">
                                        <div id="captionimage"></div>
                                    </div>
                                    <div class="wrap btn-upload-wp HDF_MainImg" style="margin-bottom: 5px; width: 92% !important;">
                                        <div style="width: 100% !important;" id="Btn_UploadImg" class="btn-upload-img" onclick="popupImageIframe('HDF_MainImg')">Chọn ảnh avatar</div>
                                        <asp:FileUpload ID="UploadFile" ClientIDMode="Static" Style="display: none;"
                                            CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                        <div class="wrap listing-img-upload">
                                            <% if (HasImages)
                                                { %>
                                            <% for (var i = 0; i < ListImagesUrl.Count; i++)
                                                { %>
                                            <div class="thumb-wp" style="height: 200px;">
                                                <img style="width: 100% !important; height: 200px !important;" id="myImg1" class="thumb" alt="" title="" src="<%=ListImagesUrl[i] %>"
                                                    data-img="<%=ListImagesUrl[i] %>" />
                                                <span class="delete-thumb" onclick="deleteThumbnail($(this), 'HDF_MainImg')"></span>
                                            </div>
                                            <% } %>
                                            <% } %>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr runat="server">
                                <td class="col-xs-12 right">
                                    <span class="field-wp">
                                        <asp:DropDownList ID="ddlSourceId" Style="height: 36px; line-height: 36px; width: 92% !important; float: left;"
                                            runat="server" ClientIDMode="Static">
                                        </asp:DropDownList>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-upload">
                                <td class="col-xs-12 right">
                                    <div class="wrap btn-upload-wp HDF_MainImgCMT1" style="margin-bottom: 5px; width: 92% !important;">
                                        <div style="width: 100% !important;" id="Btn_UploadImg1" class="btn-upload-img" onclick="popupImageIframe('HDF_MainImgCMT1')">Ảnh CMT mặt trước</div>
                                        <asp:FileUpload ID="FileUpload1" ClientIDMode="Static" Style="display: none;"
                                            CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                        <div class="wrap listing-img-upload">
                                            <% if (HasImages)
                                                { %>
                                            <% for (var i = 0; i < LisImageCMT1.Count; i++)
                                                { %>
                                            <div class="thumb-wp" style="height: 200px;">
                                                <img class="thumb" alt="" style="width: 100% !important; height: 200px;" id="myImg2" title="" src="<%=LisImageCMT1[i] %>"
                                                    data-img="<%=LisImageCMT1[i] %>" />
                                                <span class="delete-thumb" onclick="deleteThumbnail($(this), 'HDF_MainImgCMT1')"></span>
                                            </div>
                                            <% } %>
                                            <% } %>
                                        </div>
                                    </div>
                                    <div class="wrap btn-upload-wp HDF_MainImgCMT2" style="margin-bottom: 5px; width: 92% !important;">
                                        <div style="width: 100% !important;" id="Btn_UploadImg2" class="btn-upload-img" onclick="popupImageIframe('HDF_MainImgCMT2')">Ảnh CMT mặt sau</div>
                                        <asp:FileUpload ID="FileUpload2" ClientIDMode="Static" Style="display: none;"
                                            CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                        <div class="wrap listing-img-upload">
                                            <% if (HasImages)
                                                { %>
                                            <% for (var i = 0; i < ListImageCMT2.Count; i++)
                                                { %>
                                            <div class="thumb-wp" style="height: 200px;">
                                                <img class="thumb" alt="" style="width: 100% !important; height: 200px;" id="myImg3" title="" src="<%=ListImageCMT2[i] %>"
                                                    data-img="<%=ListImageCMT2[i] %>" />
                                                <span class="delete-thumb" onclick="deleteThumbnail($(this), 'HDF_MainImgCMT2')"></span>
                                            </div>
                                            <% } %>
                                            <% } %>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="table-wp">
                    <table class="table-add" style="width: 500px !important; top: -35px; margin-right: 50px;">
                        <tbody>
                            <tr class="title-head">
                                <td><strong class="hihi">THÔNG TIN QUẢN LÝ HÀNH CHÍNH 
                                </strong>
                                </td>
                                <td><%if (Perm_Edit)
                                        { %>
                                    <span class="view-more-infor f_btn_reset" onclick="ResetPass()" style="display: inline; height5">Reset Password </span>
                                    <%} %></td>
                            </tr>
                            <tr id="Tr1" runat="server">
                                <td class="col-xs-3 left"><span>Salon/Bộ phận</span></td>
                                <td class="col-xs-9 right">
                                    <div class="div_col">
                                        <asp:DropDownList ID="Salon" CssClass="select form-control" runat="server" ClientIDMode="Static" Style="width: 49%">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="div_col">
                                        <asp:DropDownList ID="TypeStaff" onchange="RemoveColor()" runat="server" ClientIDMode="Static" Style="width: 49%; margin-left: 6px;"></asp:DropDownList>
                                    </div>
                                </td>
                            </tr>

                            <tr runat="server" id="TrPermission">
                                <td class="col-xs-2 left"><span>Phân quyền</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp" id="pem">
                                        <asp:ListBox SelectionMode="Multiple" ID="PermissionList" runat="server" ClientIDMode="Static" CssClass=" perm-ddl-field"></asp:ListBox>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Tên làm việc</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="FullName" onchange="RemoveColor()" runat="server" ClientIDMode="Static" CssClass="perm-input-field"></asp:TextBox>
                                        <%--           <asp:RequiredFieldValidator ID="FullNameValidate" ControlToValidate="FullName" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập họ tên!"></asp:RequiredFieldValidator>--%>
                                    </span>

                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Tên theo CMT</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="NameCMT" runat="server" ClientIDMode="Static" onchange="RemoveColor()"></asp:TextBox>
                                        <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="NameCMT" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập tên theo CMT!"></asp:RequiredFieldValidator>--%>
                                    </span>

                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Số CMND</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="StaffID" runat="server" ClientIDMode="Static" CssClass="perm-input-field" onkeypress="return ValidateKeypress(/\d/,event);" onchange="RemoveColor()"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="Phone" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập số điện thoại!"></asp:RequiredFieldValidator>--%>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Ngày cấp</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col" style="width: 72%">
                                            <asp:TextBox ID="txtDateID" CssClass="txtDateTime" runat="server" ClientIDMode="Static"></asp:TextBox>
                                        </div>
                                        <div class="div_col">
                                            <span style="padding-left: 10px; padding-right: 7px; width: inherit !important; float: left">Nơi cấp</span>
                                            <asp:TextBox ID="txtAddressID" runat="server" ClientIDMode="Static"></asp:TextBox>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Ngày vào</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="DateJoin" CssClass="txtDateTime" runat="server" ClientIDMode="Static" placeholder="Chọn ngày vào làm việc" Enabled="true"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Ngày tính TN</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="DateTN" CssClass="txtDateTime" runat="server" ClientIDMode="Static" placeholder="Chọn ngày tính thâm niên" onkeypress="return ValidateKeypress(/\d/,event);"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Số BHXH/Thuế</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <asp:TextBox ID="NumberInsurrance" runat="server" ClientIDMode="Static" placeholder="Sổ bảo hiểm xã hội"></asp:TextBox>
                                        </div>
                                        <div class="div_col">
                                            <asp:TextBox ID="MST" Style="margin-left: 6px" runat="server" ClientIDMode="Static" placeholder="Mã số thuế thu nhập cá nhân" onkeypress="return ValidateKeypress(/\d/,event);"></asp:TextBox>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Tên Ngân Hàng</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <asp:TextBox ID="NameBank" runat="server" ClientIDMode="Static" placeholder="Ví dụ: BIDV-CN Hai Bà Trưng "> </asp:TextBox>
                                    </div>

                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Tên TK/Số TK</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <asp:TextBox ID="NameAcc" runat="server" ClientIDMode="Static" placeholder="Ví dụ: NGUYEN VAN A"> </asp:TextBox>
                                        </div>
                                        <div class="div_col">
                                            <asp:TextBox ID="NumberTK" Style="margin-left: 6px" runat="server" ClientIDMode="Static" placeholder="Số tài khoản ngân hàng" onkeypress="return ValidateKeypress(/\d/,event);"></asp:TextBox>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-2 left"><span>Hồ sơ lý lịch </span></td>
                                <td class="col-xs-9 right" style="padding: 20px 0px !important;">
                                    <span class="field-wp" id="pem">
                                        <asp:ListBox SelectionMode="Multiple" ID="ProfileId" runat="server" ClientIDMode="Static" CssClass=" perm-ddl-field"></asp:ListBox>
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Bậc kỹ năng</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:DropDownList ID="SkillLevel" runat="server" ClientIDMode="Static" CssClass="perm-ddl-field"></asp:DropDownList>
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="Phone" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập số điện thoại!"></asp:RequiredFieldValidator>--%>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Điện thoại/STT</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <asp:TextBox ID="Phone" runat="server" ClientIDMode="Static" CssClass="perm-input-field" onchange="RemoveColor()"></asp:TextBox>
                                        </div>
                                        <div class="div_col">
                                            <asp:TextBox ID="OrderCode" Style="margin-left: 6px;" runat="server" ClientIDMode="Static" CssClass="perm-input-field"></asp:TextBox>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span style="line-height: 20px;">Email</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="Email" runat="server" ClientIDMode="Static" placeholder="example@gmail.com" CssClass="perm-input-field"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-2 left"><span>Tên đăng nhập</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="UserAccount" class="remove-color" runat="server" ClientIDMode="Static"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator ID="PhoneValidate" ControlToValidate="Phone" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập số điện thoại!"></asp:RequiredFieldValidator>--%>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-birthday">
                                <td class="col-xs-3 left"><span>Ngày sinh/ Giới tính</span></td>
                                <td class="col-xs-9 right">
                                    <div class="div_col">
                                        <asp:TextBox ID="Day" runat="server" Width="70px" ClientIDMode="Static" placeholder="Ngày"></asp:TextBox>
                                        <asp:TextBox ID="Month" runat="server" Width="70px" ClientIDMode="Static" placeholder="Tháng"></asp:TextBox>
                                        <asp:TextBox ID="Year" runat="server" Width="70px" ClientIDMode="Static" placeholder="Năm"></asp:TextBox>
                                    </div>
                                    <div class="div_col">
                                        <asp:DropDownList ID="Gender" Style="width: 36%; margin-left: 10px;" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Địa chỉ</span></td>
                                <td class="col-xs-9 right">
                                    <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
                                    <asp:UpdatePanel runat="server" ID="UP01">
                                        <ContentTemplate>
                                            <div class="city">
                                                <asp:DropDownList AutoPostBack="true" ID="City" runat="server" CssClass=" perm-ddl-field"
                                                    OnSelectedIndexChanged="Reload_District" ClientIDMode="Static">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="district">
                                                <asp:DropDownList ID="District" runat="server" CssClass=" perm-ddl-field" ClientIDMode="Static"></asp:DropDownList>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <%--<asp:AsyncPostBackTrigger ControlID="OrderCloseBtn" EventName="Click"/>--%>
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <asp:TextBox ID="Address" runat="server" placeholder="Địa chỉ" ClientIDMode="Static" CssClass="perm-input-field"></asp:TextBox>
                                </td>
                            </tr>

                            <tr style="display: none;">
                                <td class="col-xs-2 left"><span>Nhóm DV với Skinner</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:DropDownList ID="SkinnerList" runat="server" ClientIDMode="Static" CssClass=" perm-ddl-field"></asp:DropDownList>
                                    </span>
                                </td>
                            </tr>
                            <tr class="staff-servey-mark">
                                <td class="col-xs-2 left"><span>Trạng thái</span></td>
                                <td class="col-xs-9 right cssradio">
                                    <asp:RadioButtonList ID="LstradiobttList" runat="server" ClientIDMode="Static">
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr class="staff-servey-mark">
                                <td class="col-xs-2 left"><span>Loại tài khoản</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:RadioButtonList ID="AccountType" runat="server" ClientIDMode="Static">
                                            <asp:ListItem Text="Tài khoản nhân viên" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Tài khoản đăng nhập" Value="1"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </span>
                                </td>
                            </tr>

                            <tr class="staff-servey-mark">
                                <td class="col-xs-2 left"><span>Tính lương</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:RadioButtonList ID="radioSalaryByPerson" runat="server" ClientIDMode="Static">
                                            <asp:ListItem Text="Theo nhóm (Bộ phận)" Value="False"></asp:ListItem>
                                            <asp:ListItem Text="Cá nhân" Value="True"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </span>
                                </td>
                            </tr>

                            <tr class="staff-servey-mark">
                                <td class="col-xs-2 left"><span>Yêu cầu điểm danh</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:RadioButtonList ID="radioEnroll" runat="server" ClientIDMode="Static">
                                            <asp:ListItem Text="Có" Value="True"></asp:ListItem>
                                            <asp:ListItem Text="Không" Value="False"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </span>
                                </td>
                            </tr>
                            <tr class="staff-servey-mark">
                                <td class="col-xs-4 left"><span>Nhân viên ảo</span></td>
                                <td class="col-xs-5 right">
                                    <span class="field-wp">
                                        <asp:RadioButtonList ID="radioAddionstaff" runat="server" ClientIDMode="Static">
                                            <asp:ListItem Text="Không" Value="False"></asp:ListItem>
                                            <asp:ListItem Text="Có" Value="True"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-send">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <asp:Panel ID="BtnSend" CssClass="btn-send" runat="server" ClientIDMode="Static">Hoàn Tất</asp:Panel>
                                        <asp:Button ID="BtnFakeSend" OnClick="Update_OBJ" runat="server" Text="Hoàn tất" ClientIDMode="Static" Style="display: none;"></asp:Button>
                                        <span class="btn-send btn-cancel" style="margin-left: 12px;">Đóng</span>
                                    </span>
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
                            <asp:HiddenField ID="HDF_MainImg" runat="server" ClientIDMode="Static" />
                            <asp:HiddenField ID="HDF_MainImgCMT1" runat="server" ClientIDMode="Static" />
                            <asp:HiddenField ID="HDF_MainImgCMT2" runat="server" ClientIDMode="Static" />
                            <asp:HiddenField ID="HDF_radiobttList" runat="server" ClientIDMode="Static" />
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="container">
                <ul class="nav nav-pills" style="height: 70px; margin-top: 10px">
                    <li class="active"><a data-toggle="pill" href="#home">Lịch sử lên bậc</a></li>
                    <li><a data-toggle="pill" href="#menu1">Lịch sử làm việc</a></li>
                    <li><a data-toggle="pill" href="#menu2">Lịch sử đánh giá tuyển dụng</a></li>
                    <li><a data-toggle="pill" href="#menu3">Hợp đồng</a></li>
                </ul>
                <div class="tab-content" style="border-left: 1px solid #e6e6e6; float: left; margin-right: 75px; width: 570px; border: 1px solid #e6e6e6; border-radius: 5px; padding: 10px; margin-top: -22px; height: 100% !important;">
                    <div id="home" class="tab-pane active">
                        <table class="table-add">
                            <tbody>
                                <tr>
                                    <td class="col-md-3">
                                        <table style="margin: 0 auto; width: 50%; text-align: center;" class="table-log-skill-level">
                                            <thead>
                                                <tr>
                                                    <th colspan="2" style="padding: 12px; background: black; color: white; font-size: 17px;">Lịch sử lên bậc</th>
                                                </tr>
                                                <tr>
                                                    <th style="padding: 12px; width: 136px;">Ngày</th>
                                                    <th style="padding: 12px; width: 136px;">Bậc</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater runat="server" ID="rptLogSkillLevel">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="padding: 12px"><%# string.Format("{0:dd/MM/yyyy}",Eval("LogTime")) %></td>
                                                            <td style="padding: 12px"><%# Eval("skillLevelName") %></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                    <div id="menu1" class="tab-pane fade">
                        <table class="table-wp" style="margin-top: 10px;">
                            <tbody>
                                <tr class="tr-field-ahalf">
                                    <td class="col-xs-3 left" style="width: 14% !important;"><span>Thời gian: </span>
                                        <hr style="border-color: white !important" />
                                    </td>
                                    <td class="col-xs-9 right">
                                        <div class="field-wp">
                                            <div class="div_col" style="margin-left: -10px;">
                                                <asp:TextBox Style="width: 30% !important; display: inline; margin-right: 5px;" CssClass="txtDateTime st-head form-control" ID="txtStartDate" placeholder="Từ ngày"
                                                    ClientIDMode="Static" runat="server"></asp:TextBox>
                                                <strong class="st-head" style="margin-left: 3px;">
                                                    <i class="fa fa-arrow-circle-right" style="margin-right: 5px; color: #D0D6D8;"></i>
                                                </strong>
                                                <asp:TextBox Style="width: 30% !important; display: inline; margin-right: 5px;" CssClass="txtDateTime st-head form-control" ID="txtEndDate" placeholder="Đến ngày"
                                                    ClientIDMode="Static" runat="server"></asp:TextBox>
                                                <input type="button" onclick="bindDataWork($(this), <%=OBJ.SalonId%>)" style="background-color: black; color: white; padding: 5px 10px; margin-top: -3px; height: 34px;" value="Xác nhận" class="btn" />
                                            </div>
                                        </div>
                                        <hr />
                                    </td>
                                </tr>
                                <%if (OBJ.Type == 1)
                                    {%>
                                <tr class="tr-field-ahalf">
                                    <td class="col-xs-3 left"><span>Điểm hài lòng: </span>
                                        <hr style="border-color: white !important" />
                                    </td>
                                    <td class="col-xs-9 right">
                                        <div class="field-wp">
                                            <div class="div_col">
                                                <asp:TextBox ID="TotalDHL" CssClass="form-control" Style="text-align: center; display: inline; margin-right: 5px;" runat="server" Width="30%" ClientIDMode="Static"></asp:TextBox>
                                            </div>
                                        </div>
                                        <hr />
                                    </td>
                                </tr>
                                <tr class="tr-field-ahalf">
                                    <td class="col-xs-3 left"><span>Năng suất kinh tế: </span>
                                        <hr style="border-color: white !important" />
                                    </td>
                                    <td class="col-xs-9 right">
                                        <div class="field-wp">
                                            <div class="div_col">
                                                <asp:TextBox ID="StylistNSKT" CssClass="form-control" Style="text-align: center; display: inline; margin-right: 5px;" runat="server" Width="30%" ClientIDMode="Static"></asp:TextBox>
                                            </div>
                                        </div>
                                        <hr />
                                    </td>
                                </tr>
                                <tr class="tr-field-ahalf tr_Top">
                                    <td class="col-xs-3 left" style="width: 17% !important;"><span>Test kỹ năng:</span><hr style="border-color: white !important" />
                                    </td>
                                    <td class="col-xs-9 right">
                                        <div class="field-wp" style="width: 60%;">
                                            <asp:Panel ID="Panel2" CssClass="st-head btn-viewdata details2" Text="Xem dữ liệu"
                                                ClientIDMode="Static" runat="server">
                                                <span id="test_technical" style="cursor: pointer; font-size: 17px; color: #f91010">Xem điểm kỹ thuật</span>
                                            </asp:Panel>
                                        </div>
                                        <hr />
                                    </td>
                                </tr>

                                <tr class="tr-field-ahalf">
                                    <td class="col-xs-3 left" style="width: 17% !important;">
                                        <hr style="border-color: white !important" />
                                        <span>Lý thuyết hóa chất: </span>
                                        <hr style="border-color: white !important" />
                                    </td>
                                    <td class="col-xs-9 right">
                                        <div class="field-wp">
                                            <div class="div_col col-xs-4 left" style="text-align: center">
                                                <span style="margin-right: 5px;">Combo Uốn</span>
                                                <hr style="margin: 5px;" />
                                                <asp:TextBox ID="txtCurling" ClientIDMode="Static" Style="margin-right: 5px; text-align: center;" Width="100%" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>
                                            <div class="div_col col-xs-4" style="text-align: center">
                                                <span style="margin-right: 5px;">Tẩy-Nhuộm</span>
                                                <hr style="margin: 5px;" />
                                                <asp:TextBox ID="txtColorCombo" ClientIDMode="Static" Width="100%" Style="margin-right: 5px; text-align: center;" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>
                                            <div class="div_col col-xs-4" style="text-align: center">
                                                <span style="margin-right: 7px">Combo Cắt</span>
                                                <hr style="margin: 5px;" />
                                                <asp:TextBox ID="txt30shine" ClientIDMode="Static" Width="100%" Style="margin-right: 5px; text-align: center;" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="tr-field-ahalf">
                                    <td class="col-xs-3 left" style="width: 17% !important;">
                                        <hr style="border-color: white !important; margin-top: 20px" />
                                        <span>Kỷ luật: </span>
                                        <hr style="border-color: white !important" />
                                    </td>
                                    <td class="col-xs-9">
                                        <hr />
                                        <div class="field-wp">
                                            <div class="div_col col-xs-4" style="text-align: center; margin-bottom: 30px;">
                                                <span style="margin-right: 5px;">Nhắc nhở: </span>
                                                <hr style="margin: 5px;" />
                                                <asp:TextBox ID="txtDisciplineStylist" ClientIDMode="Static" Style="margin-right: 5px; text-align: center;" Width="100%" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>
                                            <div class="div_col col-xs-4" style="text-align: center">
                                                <span style="margin-right: 5px;">Xử phạt: </span>
                                                <hr style="margin: 5px;" />
                                                <asp:TextBox ID="txtDisciplineStylistWarning" ClientIDMode="Static" Width="100%" Style="margin-right: 5px; text-align: center;" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <%} %>
                                <%else if (OBJ.Type == 2)
                                    {%>
                                <tr class="tr-field-ahalf">
                                    <td class="col-xs-3 left"><span>Điểm hài lòng: </span>
                                        <hr style="border-color: white !important" />
                                    </td>
                                    <td class="col-xs-9 right">
                                        <div class="field-wp">
                                            <div class="div_col">
                                                <span style="margin-right: 5px;">Tổng điểm: </span>
                                                <asp:TextBox ID="txtTotalPointSkinner" CssClass="form-control" Style="text-align: center; display: inline; margin-right: 5px;" runat="server" Width="20%" ClientIDMode="Static"></asp:TextBox>
                                            </div>
                                        </div>
                                        <hr />
                                    </td>
                                </tr>
                                <tr class="tr-field-ahalf">
                                    <td class="col-xs-3 left" style="width: 17% !important;"><span>Kỷ luật: </span>
                                        <hr style="border-color: white !important" />
                                    </td>
                                    <td class="col-xs-9 right">
                                        <div class="field-wp">
                                            <div class="div_col">
                                                <span style="margin-right: 5px;">Nhắc nhở: </span>
                                                <asp:TextBox ID="txtDisciplineSkinner" ClientIDMode="Static" Style="display: inline; margin-right: 5px;" Width="20%" CssClass="form-control" runat="server"></asp:TextBox>
                                                <span style="margin-right: 5px;">Xử phạt: </span>
                                                <asp:TextBox ID="txtDisciplineSkinnerWarning" ClientIDMode="Static" Width="20%" Style="display: inline; margin-right: 5px; text-align: center;" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                        <hr />
                                    </td>
                                </tr>
                                <%} %>
                                <%else if (OBJ.Type == 6)
                                    {%>
                                <tr class="tr-field-ahalf">
                                    <td class="col-xs-3 left" style="width: 17% !important;"><span>Doanh số mỹ phẩm: </span>
                                        <hr style="border-color: white !important" />
                                    </td>
                                    <td class="col-xs-9 right">
                                        <div class="field-wp">
                                            <div class="div_col">
                                                <span style="margin-right: 5px;">Tổng mỹ phẩm: </span>
                                                <asp:TextBox ID="txtTotalSales" ClientIDMode="Static" Style="display: inline; margin-right: 5px;" Width="27%" CssClass="form-control" runat="server"></asp:TextBox>
                                                <span style="margin-right: 5px;">Doanh số: </span>
                                                <asp:TextBox ID="txtSales" ClientIDMode="Static" Width="20%" Style="display: inline; margin-right: 5px;" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                        <hr />
                                    </td>
                                </tr>
                                <%} %>

                                <%else if (OBJ.Type == 5)
                                    {%>
                                <tr class="tr-field-ahalf tr_Top">
                                    <td class="col-xs-3 left" style="width: 17% !important;"><span>Điểm doanh số</span><hr style="border-color: white !important" />
                                    </td>
                                    <td class="col-xs-9 right">
                                        <table>
                                            <tr class="modal-body">
                                                <td>
                                                    <table class="skill-table display" style="text-align: center;" id="totalSales">
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <hr />
                                    </td>
                                </tr>
                                <tr class="tr-field-ahalf">
                                    <td class="col-xs-3 left" style="width: 17% !important; margin-top: 7px">
                                        <span>Kỷ luật: </span>
                                        <hr style="border-color: white !important" />
                                    </td>
                                    <td class="col-xs-9 right" style="margin-left: -18px;">
                                        <div class="field-wp">
                                            <div class="div_col">
                                                <span style="margin-right: 5px; margin-left: 15px;">Nhắc nhở: </span>
                                                <asp:TextBox ID="txtDisciplineCheckin" ClientIDMode="Static" Style="display: inline; margin-right: 5px;" Width="20%" CssClass="form-control" runat="server"></asp:TextBox>
                                                <span style="margin-right: 5px;">Xử phạt: </span>
                                                <asp:TextBox ID="txtDisciplineWarningCheckin" ClientIDMode="Static" Width="20%" Style="display: inline; margin-right: 5px; text-align: center;" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                        <hr />
                                    </td>
                                </tr>
                                <%} %>
                            </tbody>
                        </table>
                    </div>
                    <div id="menu2" class="tab-pane fade in">
                        <table class="table-wp" style="margin-top: 10px;">
                            <tbody>
                                <%if (OBJ.Type == 1)
                                    {%>
                                <tr style="margin-top: 10px;" class="tr-field-ahalf tr_Top">
                                    <td class="col-xs-3 left" style="width: 17% !important;"><span>Test văn hóa:</span><hr style="border-color: white !important" />
                                    </td>
                                    <td class="col-xs-9 right">
                                        <div class="field-wp">
                                            <div class="div_col">
                                                <asp:TextBox ID="txtPointStylist" ClientIDMode="Static" disable="true" Style="text-align: center; display: inline; margin-right: 5px;" Width="35%" CssClass="form-control masterTooltip" title="Điểm văn hóa" runat="server"></asp:TextBox>
                                                <asp:DropDownList ID="ddlTestResultStylist" ClientIDMode="Static" Width="35%" Style="text-align: center; display: inline; margin-right: 5px;" CssClass="form-control select" runat="server">
                                                    <asp:ListItem Value="true">Đạt</asp:ListItem>
                                                    <asp:ListItem Value="fasle">Không đạt</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:CheckBox runat="server" ID="cblTestAgainStylist" ClientIDMode="Static" Style="display: inline; margin-right: 5px;" TextAlign="Right" /><u style="line-height: 32px">Check lần 2</u>
                                            </div>
                                        </div>
                                        <hr />
                                    </td>
                                </tr>
                                <tr class="tr-field-ahalf tr_Top">
                                    <td class="col-xs-3 left" style="width: 17% !important;"><span>Test kỹ năng:</span><hr style="border-color: white !important" />
                                    </td>
                                    <td class="col-xs-9 right">
                                        <div class="field-wp" style="width: 60%;">
                                            <asp:Panel ID="Panel1" Style="width: 125px;" CssClass="st-head btn-viewdata details1" Text="Xem dữ liệu"
                                                ClientIDMode="Static" runat="server">
                                                <span id="test_Skill" style="cursor: pointer; color: red;">Xem điểm test kỹ năng</span>
                                            </asp:Panel>
                                        </div>
                                        <hr />
                                    </td>
                                </tr>
                                <tr class="tr-field-ahalf">
                                    <td class="col-xs-3 left" style="width: 17% !important;"><span>Ảnh test kỹ năng:</span><hr style="border-color: white !important" />
                                    </td>
                                    <td class="col-xs-9 right">
                                        <div class="field-wp">
                                            <div class="thumb-wp col-xs-6 left" style="text-align: center;">
                                                <span style="margin-right: 5px;">Ánh cắt</span>
                                                <hr style="margin: 5px;" />
                                                <img class="thumb" id="myImg4" style="border-radius: 2px; margin-left: 5px; margin-right: 5px; width: 120px; height: 150px;" alt="" title="Ảnh hóa chất" src="<%=clsRecruitment.ImageResult1 %>" />
                                            </div>
                                            <div class="thumb-wp col-xs-6 right" style="text-align: center;">
                                                <span style="margin-right: 5px;">Ảnh hóa chất</span>
                                                <hr style="margin: 5px;" />
                                                <img class="thumb" id="myImg5" style="border-radius: 2px; margin-left: 5px; margin-right: 5px; width: 120px; height: 150px;" alt="" title="" src="<%=clsRecruitment.ImageResult2%>" />
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="tr-field-ahalf">
                                    <td class="col-xs-3 left" style="width: 17% !important;"><span>Lý thuyết cắt: </span>
                                        <hr style="border-color: white !important" />
                                    </td>
                                    <td class="col-xs-9 right">
                                        <hr />
                                        <div class="field-wp">
                                            <div class="div_col">
                                                <asp:TextBox ID="txtStylistLTCUT" ClientIDMode="Static" Style="display: inline; margin-right: 5px;" Width="35%" CssClass="form-control" runat="server"></asp:TextBox>
                                                <asp:DropDownList ID="ddlCutStylist" ClientIDMode="Static" Width="35%" Style="display: inline; margin-right: 5px;" CssClass="form-control select" runat="server">
                                                    <asp:ListItem Value="true">Đạt</asp:ListItem>
                                                    <asp:ListItem Value="fasle">Không đạt</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:CheckBox runat="server" ID="CbxCut" ClientIDMode="Static" Style="display: inline" TextAlign="Right" /><u style="line-height: 32px">Check lần 2</u>
                                            </div>
                                        </div>
                                        <hr />
                                    </td>
                                </tr>
                                <tr class="tr-field-ahalf">
                                    <td class="col-xs-3 left" style="width: 17% !important;"><span>Lý thuyết hóa chất: </span>
                                        <hr style="border-color: white !important" />
                                    </td>
                                    <td class="col-xs-9 right">
                                        <div class="field-wp">
                                            <div class="div_col">
                                                <asp:TextBox ID="txtChemistry" ClientIDMode="Static" Style="display: inline; margin-right: 5px;" Width="35%" CssClass="form-control" runat="server"></asp:TextBox>
                                                <asp:DropDownList ID="ddlChemistry" ClientIDMode="Static" Width="35%" Style="display: inline; margin-right: 5px;" CssClass="form-control select" runat="server">
                                                    <asp:ListItem Value="true">Đạt</asp:ListItem>
                                                    <asp:ListItem Value="fasle">Không đạt</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:CheckBox runat="server" ID="CbxChemistry" ClientIDMode="Static" Style="display: inline" TextAlign="Right" /><u style="line-height: 32px">Check lần 2</u>
                                            </div>
                                        </div>
                                        <hr />
                                    </td>
                                </tr>
                                <%} %>
                                <%else if (OBJ.Type == 2)
                                    { %>
                                <tr style="margin-top: 10px;" class="tr-field-ahalf tr_Top">
                                    <td class="col-xs-3 left" style="width: 17% !important;"><span>Test văn hóa:</span><hr style="border-color: white !important" />
                                    </td>
                                    <td class="col-xs-9 right">
                                        <div class="field-wp">
                                            <div class="div_col">
                                                <asp:TextBox ID="PointSkinner" ClientIDMode="Static" Style="display: inline" Width="20%" CssClass="form-control" runat="server" placeholder="Số Điểm"></asp:TextBox>
                                                <asp:DropDownList ID="TotalSuccess" ClientIDMode="Static" Width="35%" Style="display: inline; margin-right: 5px;" CssClass="form-control select" runat="server">
                                                    <asp:ListItem Value="true">Đạt</asp:ListItem>
                                                    <asp:ListItem Value="fasle">Không đạt</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:CheckBox runat="server" ID="CbxCheck" ClientIDMode="Static" Style="display: inline" TextAlign="Right" /><u style="line-height: 32px">Check lần 2</u>
                                            </div>
                                        </div>
                                        <hr />
                                    </td>
                                </tr>
                                <tr style="margin-top: 10px;" class="tr-field-ahalf tr_Top">
                                    <td class="col-xs-3 left"><span>Link gội: </span>
                                        <hr style="border-color: white !important" />
                                    </td>
                                    <td class="col-xs-9 right">
                                        <div class="field-wp">
                                            <div class="div_col">
                                                <p style="word-break: break-all;" id="linkShampoo"><%=clsRecruitment.linkShampoo %></p>
                                            </div>
                                        </div>
                                        <hr />
                                    </td>
                                </tr>
                                <% }%>
                                <%else if (OBJ.Type == 6 || OBJ.Type == 5)
                                    {%>
                                <tr style="margin-top: 10px;" class="tr-field-ahalf tr_Top">
                                    <td class="col-xs-3 left" style="width: 17% !important;"><span>Test văn hóa:</span><hr style="border-color: white !important" />
                                    </td>
                                    <td class="col-xs-9 right">
                                        <div class="field-wp">
                                            <div class="div_col">
                                                <asp:TextBox ID="txtCultural" ClientIDMode="Static" Style="display: inline; margin-right: 5px;" Width="20%" CssClass="form-control" runat="server" placeholder="Số Điểm"></asp:TextBox>
                                                <asp:DropDownList ID="ddlCultural" ClientIDMode="Static" Width="35%" Style="display: inline; margin-right: 5px;" CssClass="form-control select" runat="server">
                                                    <asp:ListItem Value="true">Đạt</asp:ListItem>
                                                    <asp:ListItem Value="fasle">Không đạt</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:CheckBox runat="server" ID="CbxCultural" ClientIDMode="Static" Style="display: inline" TextAlign="Right" /><u style="line-height: 32px">Check lần 2</u>
                                            </div>
                                        </div>
                                        <hr />
                                    </td>
                                </tr>
                                <%} %>
                                <tr style="margin-top: 10px;" class="tr-field-ahalf tr_Top">
                                    <td class="col-xs-3 left"><span>Ghi chú: </span>
                                        <hr style="border-color: white !important" />
                                    </td>
                                    <td class="col-xs-9 right">
                                        <div class="field-wp">
                                            <div class="div_col">
                                                <asp:TextBox Style="width: 90%; height: 120px; border: 1px solid black; border-radius: 3px; padding: 10px" ReadOnly ID="Note" ClientIDMode="Static" TextMode="multiline" Columns="50" Rows="5" runat="server" />
                                            </div>
                                        </div>
                                        <hr />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="menu3" class="tab-pane fade in">
                        <table class="table-add">
                            <tbody>
                                <tr class="staff-servey-mark">
                                    <td class="col-xs-12 right" style="padding-top: 10px !important;">
                                        <div class="contract">
                                            <p class="child" style="margin-top: 6px">Học nghề (3 tháng)</p>
                                            <asp:TextBox ID="txt3thang" runat="server" placeholder="" ClientIDMode="Static" CssClass="child text-contract txtDateTime"></asp:TextBox>
                                            <asp:Button ID="btn3thang" ClientIDMode="Static" CssClass="btn btn-complete child btn-contract" OnClick="btn3thang_Click" runat="server" Text="In hợp đồng" />
                                        </div>
                                        <div class="contract">
                                            <p class="child" style="margin-top: 6px">Học nghề (6 tháng)</p>
                                            <asp:TextBox ID="txt6thang" runat="server" placeholder="" ClientIDMode="Static" CssClass="child text-contract txtDateTime"></asp:TextBox>
                                            <asp:Button ID="btn6thang" ClientIDMode="Static" CssClass="btn btn-complete child btn-contract" OnClick="btn6thang_Click" runat="server" Text="In hợp đồng" />

                                        </div>
                                        <div class="contract">
                                            <p class="child" style="margin-top: 6px">Thử việc văn phòng</p>
                                            <asp:TextBox ID="txtThuViec" runat="server" placeholder="" ClientIDMode="Static" CssClass="child text-contract txtDateTime"></asp:TextBox>
                                            <asp:Button ID="btnThuViec" ClientIDMode="Static" CssClass="btn btn-complete child btn-contract" OnClick="btnThuViec_Click" runat="server" Text="In hợp đồng" />

                                        </div>
                                        <div class="contract">
                                            <p class="child" style="margin-top: 6px">Lao động (SL/VP)</p>
                                            <asp:TextBox ID="txtHDLD" runat="server" placeholder="" ClientIDMode="Static" CssClass="child text-contract txtDateTime"></asp:TextBox>
                                            <asp:Button ID="btnHDLD" ClientIDMode="Static" CssClass="btn btn-complete child btn-contract" OnClick="btnHDLD_Click" runat="server" Text="In hợp đồng" />
                                            <asp:CheckBox ID="chkLDSL" runat="server" ClientIDMode="Static" onclick="CheckLDSL()" />
                                            <label for="chkLDSL" style="margin-right: 9px; margin-left: 5px; margin-top: 8px;">Nhận về </label>
                                        </div>
                                        <div class="contract">
                                            <p class="child" style="margin-top: 6px">Thử việc quản lý</p>
                                            <asp:TextBox ID="txtHDTVManager" runat="server" placeholder="" ClientIDMode="Static" CssClass="child text-contract txtDateTime"></asp:TextBox>
                                            <asp:Button ID="btnTVQL" ClientIDMode="Static" CssClass="btn btn-complete child btn-contract" OnClick="btnTVQL_Click" runat="server" Text="In hợp đồng" />

                                        </div>
                                        <div class="contract">
                                            <p class="child">Lao động quản lý</p>
                                            <asp:TextBox ID="txtHDLDManager" runat="server" placeholder="" ClientIDMode="Static" CssClass="child text-contract txtDateTime"></asp:TextBox>
                                            <asp:Button ID="btnLDQL" ClientIDMode="Static" CssClass="btn btn-complete child btn-contract" OnClick="btnLDQL_Click" runat="server" Text="In hợp đồng" />
                                            <asp:CheckBox ID="chkLDQL" runat="server" ClientIDMode="Static" onclick="CheckLDQL()" />
                                            <label for="chkLDQL" style="margin-right: 9px; margin-left: 5px; margin-top: 8px;">Nhận về </label>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <%-- end Add --%>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
           
        </style>
        <!-- Staff Information -->
        <% if (OBJ != null)
            { %>
        <!--/ Staff Information -->
        <div class="wp960 content-wp" style="display: none;">
            <!-- Hidden Field-->
            <asp:HiddenField ID="HDF_TypeStaff" ClientIDMode="Static" runat="server" />
            <asp:HiddenField ID="HDF_Staff" ClientIDMode="Static" runat="server" />
            <asp:HiddenField ID="HDF_PermissionID" ClientIDMode="Static" runat="server" />
            <asp:HiddenField ID="HDF_Profile_ID" ClientIDMode="Static" runat="server" />
            <%--<input id="hdf_staffid" value ="<%=Code%>" runat="server" clientidmode="Static" type="hidden" />--%>
            <!-- END Hidden Field-->
        </div>
        <!-- Modal View SCSC theo từng kiểu tóc của từng nhân viên 2  -->
        <div id="systemModal1" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <strong class="st-head"><i class="fa fa-file-text" style="padding: 5px"></i>Điểm test kỹ năng</strong>
                    </div>
                    <div class="modal-body">
                        <div class=" tab-content">
                            <table class="skill-table display">
                                <tbody>
                                    <tr class="tr-field-ahalf">
                                        <td>
                                            <asp:Repeater runat="server" ID="rptSkillName" ClientIDMode="Static" OnItemDataBound="rptSkillName_ItemDataBound">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td class="col-xs-9 right">
                                                            <div class="col-md-4">
                                                                <span style="width: 230px">
                                                                    <label id="lblSkill" class="lbl-cus-no-infor" value="<%#Eval("IdSkill") %>" style="margin-bottom: 8px; cursor: pointer; font-weight: normal; font-size: 13px; padding: 2px 10px; background: #ddd; line-height: 33px; float: left;">
                                                                        <%#Eval("SkillName") %>
                                                                    </label>
                                                                </span>
                                                            </div>
                                                            <div class="col-md-8" style="margin-top: 8px;">
                                                                <span style="width: 354px">
                                                                    <asp:DropDownList runat="server" ID="drlLevelName" Enabled="false"></asp:DropDownList>
                                                                    <hr style="color: #808080" />
                                                                </span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="closeModal1">Đóng</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal View SCSC theo từng kiểu tóc của từng nhân viên 2  -->
        <div id="systemModal2" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="filter-item" style="margin-left: 20px; padding: 10px">
                        <strong class="st-head" style="margin-right: 10px;"><i class="fa fa-clock-o" style="padding: 5px"></i>Thời gian</strong>
                        <div class="datepicker-wp" style="margin-top: 10px">
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TextBox3" placeholder="Từ ngày" Style="height: 34px; width: 150px!important; border: 1px solid #ddd; padding: 10px"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                            <strong class="st-head" style="margin-left: 10px; margin-right: 10px;">
                                <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                            </strong>
                            <asp:TextBox CssClass="txtDateTime st-head" ID="TextBox4" placeholder="Đến ngày" Style="height: 34px; width: 150px!important; border: 1px solid #ddd; padding: 10px"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                            <input type="button" class="btn btn-default confirm" style="background-color: black; color: white; padding: 5px 10px; margin-top: -3px; height: 34px;" value="Tìm kiếm" id="Confirm" onclick="clickConfirm($(this))" />
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="tab-content">
                            <table class="skill-table display" id="totalWork">
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="closeModal2">Đóng</button>
                    </div>
                </div>
            </div>
        </div>
        <%-- END Listing --%>
        <% } %>
        <script type="text/ecmascript">
            jQuery(document).ready(function () {
                $("#success-alert").hide();
                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });

                $("#PermissionList").multiselect({
                    includeSelectAllOption: true
                });

                $("#ProfileId").multiselect({
                    includeSelectAllOption: true
                });
            });

            //validate call btnFake
            //author: QuynhDD
            function CheckLDQL() {
                addLoading();
                var text = window.location.href;
                var listText = text.split("/");
                var textEnd = listText[listText.length - 1].replace(".html", "").trim();
                var staffId = ~~textEnd;
                var isCheck = $('#chkLDQL').prop('checked');
                if (staffId === 0) {
                    staffId = ~~textEnd.replace(/\?[^]*/i, "").trim();
                    //showMsgSystem("Không tìm thấy id nhân viên","msg-system warning",10000);
                    //removeLoading();
                    //return;
                }
                $.ajax({
                    url: '/GUI/BackEnd/Staff/Staff_Edit.aspx/chkLDQL_Click',
                    method: "POST",
                    data: `{staffId:${staffId},isCheck:${isCheck}}`,
                    contentType: "application/json; charset=utf-8",
                    datatype: "JSON",
                    success: function (data, textStatus, jqXHR) {
                        if (data.d.success) {
                            showMsgSystem('Thành công', "msg-system success", 3000);
                        }
                        else {
                            showMsgSystem(data.d.msg, "msg-system warning", 10000);
                        }
                        removeLoading();
                    },
                    error: function () {
                        removeLoading();
                    }
                });
                removeLoading();
            }
            function CheckLDSL() {
                addLoading();
                var text = window.location.href;
                var listText = text.split("/");
                var textEnd = listText[listText.length - 1].replace(".html", "").trim();
                var staffId = ~~textEnd;
                var isCheck = $('#chkLDSL').prop('checked');
                if (staffId === 0) {
                    staffId = ~~textEnd.replace(/\?[^]*/i, "").trim();
                    if (staffId === 0) {
                        showmsgsystem("không tìm thấy id nhân viên", "msg-system warning", 10000);
                        removeloading();
                        return;
                    }

                }
                $.ajax({
                    url: '/GUI/BackEnd/Staff/Staff_Edit.aspx/chkLDSL_Click',
                    method: "POST",
                    data: `{staffId:${staffId},isCheck:${isCheck}}`,
                    contentType: "application/json; charset=utf-8",
                    datatype: "JSON",
                    success: function (data, textStatus, jqXHR) {
                        if (data.d.success) {
                            showMsgSystem('Thành công', "msg-system success", 3000);
                        }
                        else {
                            showMsgSystem(data.d.msg, "msg-system warning", 10000);
                        }
                        removeLoading();
                    },
                    error: function () {
                        removeLoading();
                    }
                });
            }
            $("#BtnSend").bind("click", function () {
                var kt = true;
                var error = 'Vui lòng kiểm tra lại thông tin!';
                var department = $("#TypeStaff").val();
                var fullName = $("#FullName").val();
                var nameCmt = $("#NameCMT").val();
                var numberCmt = $("#StaffID").val();
                var phone = $("#Phone").val();
                var user = $("#UserAccount").val();
                if (department <= 0) {
                    kt = false;
                    error += "[Bạn chưa chọn bộ phận], ";
                    $('#TypeStaff').css("border-color", "red");
                }
                if (fullName == "") {
                    kt = false;
                    error += "[Bạn chưa nhập tên làm việc], ";
                    $('#FullName').css("border-color", "red");
                }
                if (nameCmt == "") {
                    kt = false;
                    error += "[Bạn chưa nhập tên theo chứng minh thư], ";
                    $('#NameCMT').css("border-color", "red");
                }
                if (numberCmt == "") {
                    kt = false;
                    error += "[Bạn chưa nhập số chứng minh nhân dân], ";
                    $('#StaffID').css("border-color", "red");
                }
                if (phone == "") {
                    kt = false;
                    error += "[Bạn chưa nhập số điện thoại], ";
                    $('#Phone').css("border-color", "red");
                }
                if (checkPhone(phone) == false) {
                    kt = false;
                    error += "[Bạn nhập sai định dạng số điện thoại], ";
                    $('#Phone').css("border-color", "red");
                }
                if (user == "") {
                    kt = false;
                    error += "[Bạn chưa nhập tài khoản đăng nhập]";
                    $('#UserAccount').css("border-color", "red");
                }
                if (!kt) {
                    $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                        $("#success-alert").slideUp(2000);
                    });
                    $("#msg-alert").text(error);
                }
                else {
                    //Get list Id permission
                    getPermission();
                    //get list id profile
                    getProfileId();
                    //call buton addStaff
                    $("#BtnFakeSend").click();
                }

                return kt;
            });
            function ShowWarning(message) {
                $('#MsgSystem').text(message);
                $('#MsgSystem').css("color", "red");
            }
            ///Get list Id Permission
            function getPermission() {
                var Ids = [];
                var prd = {};
                $("#PermissionList option:selected").each(function () {
                    prd = {};
                    var permissionId = $(this).val();
                    if (permissionId != null && permissionId > 0) {
                        permissionId = permissionId.toString().trim() != "" ? parseInt(permissionId) : 0;
                        prd.permissionId = permissionId;
                        Ids.push(prd);
                    }
                    //console.log(Ids);
                });
                $("#HDF_PermissionID").val(JSON.stringify(Ids));
            }

            //get list Id Profile
            function getProfileId() {
                var Ids = [];
                var prd = {};
                $("#ProfileId option:selected").each(function () {
                    prd = {};
                    var profileId = $(this).val();
                    if (profileId != null && profileId > 0) {
                        profileId = profileId.toString().trim() != "" ? parseInt(profileId) : 0;
                        prd.profileId = profileId;
                        Ids.push(prd);
                    }
                });
                $("#HDF_Profile_ID").val(JSON.stringify(Ids));
            }

            //check Format Phone
            function checkPhone(phone) {
                phone = String(phone).trim();
                var filter = /^[0-9-+]+$/;
                var x = phone.substring(0, 1);

                if (phone.length < 10 || phone.length > 11 || x != '0' || filter.test(phone) == false)
                    return false;
                else
                    return true;
            }

            ///remove color
            function RemoveColor() {
                $(".remove-color").css("border-color", "#ddd");
            }

        </script>
        <script>
            ///get Id staff
            var Id = <%= Convert.ToInt32(Request.QueryString["Code"]) >0 ? Convert.ToInt32(Request.QueryString["Code"]) : 0%>;
            jQuery(document).ready(function () {
                // Tooltip only Text
                $('.masterTooltip').hover(function () {
                    // Hover over code
                    var title = $(this).attr('title');
                    $(this).data('tipText', title).removeAttr('title');
                    $('<p class="tooltip"></p>')
                        .text(title)
                        .appendTo('body')
                        .fadeIn('slow');
                }, function () {
                    // Hover out code
                    $(this).attr('title', $(this).data('tipText'));
                    $('.tooltip').remove();
                }).mousemove(function (e) {
                    var mousex = e.pageX + 20; //Get X coordinates
                    var mousey = e.pageY + 10; //Get Y coordinates
                    $('.tooltip')
                        .css({ top: mousey, left: mousex })
                });
                //show image
                var modal = document.getElementById('myModalimage');
                // Get the image and insert it inside the modal - use its "alt" text as a caption
                var img = document.getElementById('myImg1');
                var img1 = document.getElementById('myImg2');
                var img2 = document.getElementById('myImg3');
                var img3 = document.getElementById('myImg4');
                var img3 = document.getElementById('myImg5');
                var modalImg = document.getElementById("img01");
                var captionText = document.getElementById("captionimage");

                try {
                    img.onclick = function () {

                        modal.style.display = "block";
                        modalImg.src = this.src;
                        captionText.innerHTML = this.alt;
                    }
                    img1.onclick = function () {
                        modal.style.display = "block";
                        modalImg.src = this.src;
                        captionText.innerHTML = this.alt;
                    }
                    img2.onclick = function () {
                        modal.style.display = "block";
                        modalImg.src = this.src;
                        captionText.innerHTML = this.alt;
                    }
                    img3.onclick = function () {
                        modal.style.display = "block";
                        modalImg.src = this.src;
                        captionText.innerHTML = this.alt;
                    }
                    // When the user clicks on <span> (x), close the modal
                    modal.onclick = function () {
                        console.log("a");
                        modal.style.display = "none";
                    }
                } catch (err) { }

                $('#myModalimage').click(function () {
                    $("#myModalimage").css("display", "none")
                });
                $('.modal-content-image').click(function (event) {
                    event.stopPropagation();
                });
                $(document).on("click", "#closeModal1", function () {
                    $("#systemModal1").modal("hide");
                })
                $(document).on("click", ".details1", function () {
                    $("#systemModal1").modal();
                })
                $(document).on("click", ".details2", function () {
                    $("#systemModal2").modal();
                })
                $(document).on("click", "#closeModal2", function () {
                    $("#systemModal2").modal("hide");
                })

                $("#glbAdminContent").addClass("active");
                $("#glbAdminStaff").addClass("active");
                $("#glbProfile").addClass("active");
                $("#subMenu .li-edit").addClass("active");
                //$("#hdf_staffid").val(Code);

                $(".view-changepass").bind("click", function () {
                    $(".content-toggle").hide();
                    $("#ChangepassWp").show();
                });
                $(".btn-cancel").bind("click", function () {
                    $("#StaffInfoWrap,#ChangepassWp").hide();
                });
                var qs = getQueryStrings();
                if (!$.isEmptyObject(qs)) {
                    showMsgSystem(qs["msg_update_message"], qs["msg_update_status"])
                    if (qs["show_form_infor"] == "true") {
                        $("#StaffInfoWrap").show();
                    }
                }

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { },
                    scrollMonth: false,
                    scrollTime: false,
                    scrollInput: false
                });

                // Price format
                UP_FormatPrice('.be-report-price');

                // Fix avatar thumbnail
                excThumbWidth(120, 120);

                //init thumbnail
                initThumbnail('HDF_MainImg');
                initThumbnail('HDF_MainImgCMT1');
                initThumbnail('HDF_MainImgCMT2');
            });

            ///
            function showDom(selector, status) {
                status = typeof status == "boolean" ? status : false;
                if (status) {
                    $(selector).show();
                }
            }

        </script>

        <!-- Popup plugin image -->
        <script type="text/javascript">
            ///bind lịch sử làm việc
            function bindDataWork(This, SalonId) {
                var timeFrom = $("#txtStartDate").val();
                var timeTo = $("#txtEndDate").val();
                var salon = SalonId;
                var id = <%=Code%>;
                var type = <%=OBJ.Type%>;
                addLoading();
                $.ajax({
                    type: 'POST',
                    //url: "/admin/nhan-vien/<%=Code%>.html/BindDataWork",
                    //url: "/GUI/BackEnd/Staff/Staff_Edit.aspx?Code=$1/BindDataWork",
                    url: "/GUI/BackEnd/Staff/Staff_Edit.aspx/GetDataWork",
                    data: "{timeFrom : '" + timeFrom + "', timeTo : '" + timeTo + "', SalonId: " + salon + ", StaffId: " + id + ", TypeId: " + type + "}",
                    contentType: 'application/json',
                }).success(function (data) {
                    if (type == 1) {
                        $("#TotalDHL").val(data.d.totalPoint);
                        $("#StylistNSKT").val(data.d.TotalMoney);
                        $("#txtCurling").val(data.d.TotalQuantityCurling);
                        $("#txtColorCombo").val(data.d.TotalQuantityColor);
                        $("#txt30shine").val(data.d.TotalQuantityShine);
                        $("#txtDisciplineStylist").val(data.d.Remind);
                        $("#txtDisciplineStylistWarning").val(data.d.mistPoint);
                    }
                    else if (type == 2) {
                        $("#txtTotalPointSkinner").val(data.d.totalPoint);
                        $("#txtDisciplineSkinnerWarning").val(data.d.mistPoint);
                    }
                    else if (type == 5) {
                        $.ajax({
                            type: "POST",
                            url: "/GUI/BackEnd/Staff/Staff_Edit.aspx/bindDataCheckin",
                            data: "{TimeFrom : '" + timeFrom + "', TimeTo : '" + timeTo + "', Id: " + id + "}",
                            contentType: "application/json; charset=utf-8",
                            datatype: "JSON",
                            success: function (data) {
                                var jsonData = $.parseJSON(data.d);
                                var itemRows = '  <thead>' +
                                    '<tr class="item-row item-row-head">' +
                                    '<th class="item-cell item-cell-skill"><span>Khách book chờ (<= 15)</span></th>' +
                                    '<th class="item-cell item-cell-skill"><span>Khách book chờ (15-20)</span></th>' +
                                    '<th class="item-cell item-cell-skill"><span>Khách book chờ ( > 20)</span></th>' +
                                    '<th class="item-cell item-cell-skill"><span>Khách book chờ<br />(Không có dữ liệu)</span></th>' +
                                    '</tr>' +
                                    '</thead>' +
                                    '<tbody>';
                                //for (var i = 0; i < jsonData.lenght; i++) {
                                $.each(jsonData, function (i, v) {
                                    itemRows += '<tr>' +
                                        '<td class="item-cell item-cell-skill">' + v.PointBefore15 + '</td>' +
                                        '<td class="item-cell item-cell-skill">' + v.PointBetween15and20 + '</td>' +
                                        '<td class="item-cell item-cell-skill">' + v.PointAfter20 + '</td>' +
                                        '<td class="item-cell item-cell-skill">' + v.PointNotTime + '</td>' +
                                        '</tr>';
                                })
                                itemRows += '</tbody>';
                                $(".modal-body #totalSales").html(itemRows);
                            }
                        });
                        $("#txtDisciplineWarningCheckin").val(data.d.mistPoint);
                    }
                    else if (type == 6) {
                        $("#txtTotalSales").val(data.d.TotalSales);
                        $("#txtSales").val(data.d.Sales);
                    }
                    removeLoading();
                });
            }

            // set funcition table
            $("#totalBill").DataTable({
                "bSort": false,
                'aoColumns': [
                    { bSearchable: false, bSortable: false }
                ],
                "lengthMenu": false,
                "zeroRecords": false,
                "info": false,
                "paginate": false,
                "bLengthChange": false,
                "bFilter": false,
            });

            ///close modal
            $(document).on("click", "#closeModal2", function () {
                $("#systemModal2").modal("hide");
                $("table tbody tr").css("background-color", "white");
                $("#TextBox3").val("");
                $("#TextBox4").val("");
                $("#systemModal2 .modal-body #totalWork").html('');
            })

            /*BindHairStyle*/
            // click Confirm HairStyle
            function clickConfirm(This) {
                var timeFrom = $("#TextBox3").val();
                var timeTo = $("#TextBox4").val();
                var id = <%=Code%>;
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Staff/Staff_Edit.aspx/bindHairStyle",
                    data: "{TimeFrom : '" + timeFrom + "', TimeTo : '" + timeTo + "', Id: " + id + "}",
                    contentType: "application/json; charset=utf-8",
                    datatype: "JSON",
                    success: function (data) {
                        var jsonData = $.parseJSON(data.d);
                        var itemRows = '  <thead>' +
                            '<tr class="item-row item-row-head">' +
                            '<th class="item-cell item-cell-skill"><span>STT</span></th>' +
                            '<th class="item-cell item-cell-skill"><span>ID</span></th>' +
                            '<th class="item-cell item-cell-skill"><span>Tên nhân viên</span></th>' +
                            '<th class="item-cell item-cell-skill"><span>Kiểu tóc</span></th>' +
                            '<th class="item-cell item-cell-skill"><span>Điểm SCSC TB</span></th>' +
                            '<th class="item-cell item-cell-skill"><span>Tốc độ cắt</span></th>' +
                            '</tr>' +
                            '</thead>' +
                            '<tbody>';
                        //for (var i = 0; i < jsonData.lenght; i++) {
                        $.each(jsonData, function (i, v) {
                            itemRows += '<tr>' +
                                '<td class="item-cell item-cell-skill">' + (i + 1) + '</td>' +
                                '<td class="item-cell item-cell-skill">' + v.StaffId + '</td>' +
                                '<td class="item-cell item-cell-skill">' + v.StaffName + '</td>' +
                                '<td class="item-cell item-cell-skill">' + v.hairName + '</td>' +
                                '<td class="item-cell item-cell-skill">' + v.PointSCSC_TB + '</td>' +
                                '<td class="item-cell item-cell-skill">' + v.TimeCatTB + '</td>' +
                                '</tr>';
                        })
                        itemRows += '</tbody>';
                        $("#systemModal2 .modal-body #totalWork").html(itemRows);
                    }
                });
            }

            //off function table
            $("#totalSales").DataTable({
                "bSort": false,
                'aoColumns': [
                    { bSearchable: false, bSortable: false }
                ],
                "lengthMenu": false,
                "zeroRecords": false,
                "info": false,
                "paginate": false,
                "bLengthChange": false,
                "bFilter": false,
            });

            //reset password
            function ResetPass() {
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Staff/Staff_Edit.aspx/ResetPassWord",
                    data: '{_Id: "' +<%=Code%> +'"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json", success: function (response) {
                        if (response.d == "success") {
                            setTimeout(function () {
                                setTimeout(function () {
                                    swal("Reset Success!", "Clicked the button to close!", "success")
                                }, 2000);
                                $("#btnFakeClick").css("dipslay", "none");

                            })
                        }
                    }
                });
            }

            ///delete image
            function deleteThumbnail(This, StoreImgField) {
                var imgs = "";
                var lst = This.parent().parent();
                This.parent().remove();
                lst.find("img.thumb").each(function () {
                    imgs += $(this).attr("src") + ",";
                });
                $("#" + StoreImgField).val(imgs);
            }

            ///
            function popupImageIframe(StoreImgField) {
                var Width = 960;
                var Height = 560;
                var ImgList = [];
                var iframe = $("<div class='iframe-image-upload' style='width:" + Width + "px; height:" + Height + "px; float:left; display:none;'><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + StoreImgField + "&callback=getThumbFromIframe&imgWidth=800&imgHeight=600&multi_img=false&Folder=NhanVien' style='width:100%; height: 100%;'></iframe></div");

                $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
            }

            ///
            window.getThumbFromIframe = function (Imgs, StoreImgField) {
                var imgs = "";
                var thumb = "";
                if (Imgs.length > 0) {
                    thumb = '<div class="thumb-wp">' +
                        '<img class="thumb" alt="" title="" src="' + Imgs[0] + '"' +
                        'data-img="" />' +
                        '<span class="delete-thumb" onclick="deleteThumbnail($(this), \'' + StoreImgField + '\')"></span>' +
                        '</div>';
                    $("." + StoreImgField).find(".listing-img-upload").empty().append(thumb);
                    excThumbWidth(120, 120);
                    $("#" + StoreImgField).val(Imgs[0]);
                }
                autoCloseEBPopup();
            }

            ///image
            function initThumbnail(StoreImgField) {
                var imgs = "";
                var lst = $("." + StoreImgField).find("img.thumb");
                lst.each(function () {
                    imgs += $(this).attr("src") + ",";
                });
                $("#" + StoreImgField).val(imgs);
            }

            ///validate 
            function ValidateKeypress(numcheck, e) {
                var keynum, keychar, numcheck;
                if (window.event) {
                    keynum = e.keyCode;
                }
                else if (e.which) {
                    keynum = e.which;
                }
                if (keynum == 8 || keynum == 127 || keynum == null || keynum == 9 || keynum == 0 || keynum == 13) return true;
                keychar = String.fromCharCode(keynum);
                var result = numcheck.test(keychar);
                return result;
            }

        </script>
    </asp:Panel>
</asp:Content>
