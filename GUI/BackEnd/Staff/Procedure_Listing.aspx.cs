﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Script.Serialization;

namespace _30shine.GUI.BackEnd.StaffController
{
    public partial class Procedure_Listing : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();
        private Solution_30shineEntities db;
        private int integer;

        public Procedure_Listing()
        {
            db = new Solution_30shineEntities();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind_StaffType();
                Bind_Paging();
                Bind_Rpt();
                RemoveLoading();
            }
        }

        private List<Staff_Procedure> queryStaffProcedure()
        {
            var departmentId = int.TryParse(StaffType.SelectedValue, out integer) ? integer : 0;
            if (departmentId > 0)
            {
                return db.Staff_Procedure.Where(w => w.IsDelete != true && w.Publish == true && w.DepartmentId == departmentId).ToList();
            }
            else
            {
                return db.Staff_Procedure.Where(w => w.IsDelete != true && w.Publish == true).ToList();
            }
        }

        private void Bind_StaffType()
        {
            var list = db.Staff_Type.Where(w => w.IsDelete != 1).OrderBy(o => o.Id).ToList();
            var item = new Staff_Type();
            item.Id = 0;
            item.Name = "Chọn bộ phận";
            list.Insert(0, item);
            StaffType.DataTextField = "Name";
            StaffType.DataValueField = "Id";
            StaffType.DataSource = list;
            StaffType.DataBind();
            StaffType.SelectedIndex = 0;
        }

        private void Bind_Rpt()
        {
            var LST = queryStaffProcedure().OrderBy(o => o.Id).Skip(PAGING._Offset).Take(PAGING._Segment).ToList();

            Rpt.DataSource = LST;
            Rpt.DataBind();
        }

        public void rebindData(object sender, EventArgs e)
        {
            Bind_Rpt();
        }

        protected void _BtnClick(object sender, EventArgs e)
        {
            Bind_Paging();
            Bind_Rpt();
            RemoveLoading();
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        protected void Bind_Paging()
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage() : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage()
        {
            var Count = db.Staff_Procedure.Where(w => w.IsDelete != true && w.Publish == true).Count();
            int TotalRow = Count - PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        public class cls_media
        {
            public string url { get; set; }
            public string thumb { get; set; }
            public string title { get; set; }
            public string description { get; set; }
        }
    }

    public class cls_category : Tbl_Category
    {
        public string Pid_Name { get; set; }
    }
}