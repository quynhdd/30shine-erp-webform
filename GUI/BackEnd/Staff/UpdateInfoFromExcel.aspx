﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UpdateInfoFromExcel.aspx.cs" Inherits="_30shine.GUI.BackEnd.StaffController.UpdateInfoFromExcel" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="StaffUpdateInfo" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý &nbsp;&#187; </li>
                        <li class="li-listing active"><a href="/dich-vu/danh-sach.html">Nhân sự</a></li>
                        <li class="li-pending active">
                            <a href="/dich-vu/pending.html"><div class="pending-1"></div> Cập nhật từ Excel</a>
                        </li>
                        <%--<li class="li-add"><a href="/dich-vu/them-phieu-v3.html">Thêm mới</a></li>--%>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report">
            <%-- Listing --%>
            <div class="wp960 content-wp">

                <!-- End Filter -->
                <div class="pending-content-wrap" style="width: 1000px; margin: 0 auto;">
                    <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                </div>
                
                <div class="content-wrapper">
                    <div id="dropZoneExcel">
                        <strong>Click chọn<br /> hoặc <br />kéo File Excel vào đây!</strong>
                        <p id="FileName"></p>
                        <input type="file" name="InputExcel" style="display:none" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" />
                    </div> 
                    
                    <div id="updateStaffBtn-wp">
                        <button id="UpdateStaffBtn" class="btn btn-complete" disabled="disabled">Cập nhật Nhân sự</button>
                    </div>
                </div>
                
                <style>
                    .content-wrapper{
                        clear:left;
                    }
                    a#SelectFileBtn{

                    }

                    #dropZoneExcel {
                        border: 2px dashed #ccc;
                        width:  800px;
                        height: 320px;
                        padding: 120px;
                        text-align: center;
                        margin: 50px auto;

                    }
                    #UpdateStaffBtn{
                        display: inline-block;
                        margin-top: 20px
                    }
                    #FileName{
                        margin-top: 6px;
                    }

                    #updateStaffBtn-wp {
                        float: none;
                        cleat: both;
                        text-align: center;
                    }

                    .color-red{
                        color: red;
                    }

                    .text-bold{
                        font-weight: bold;
                    }

                </style>

                <script type="text/javascript">
                    var DataFromExcel = [];
                    var TableOfError = $('#TableOfError');
                    var UpdateBtn = $('#UpdateStaffBtn');

                    jQuery(document).ready(function ($) {

                        $('#dropZoneExcel').proccessExcel({
                            dropAble: true,
                            onDataCollected: function (File, Data) {
                                $('#FileName').empty().text(File.name);
                                $('#UpdateStaffBtn').prop('disabled', false);
                            },
                            ajax: {
                                trigger: "#UpdateStaffBtn.click",
                                url: "/GUI/BackEnd/Staff/UpdateInfoFromExcel.aspx/UploadFile",
                                success: function (response, ExcelHelper) {
                                    var RES = JSON.parse(response.d);

                                    if (RES.data.length > 0) {
                                        if (!confirm("Một số bản ghi bị lỗi!\n Bạn có muốn tải file xuống và chỉnh lại?")) {
                                            return false;
                                        }

                                        var dataStructure = function () {
                                            return {
                                                STT: { w: 60, v: "", p: 0 },
                                                Name: { w: 120, v: "", p: 1 },
                                                TenLamViec: { w: 120, v: "", p: 2 },
                                                Salon: { w: 60, v: "", p: 3 },
                                                Code: { w: 90, v: "", p: 4 },
                                                TenNganHang: { w: 150, v: "", p: 5 },
                                                SoTaiKhoan: { w: 120, v: "", p: 6 },
                                                ThamNien: { w: 90, v: "", p: 7 },
                                                NgayBatDauVao: { w: 90, v: "", p: 8 },
                                                ChucVu: { w: 60, v: "", p: 9 },
                                                Level: { w: 60, v: "", p: 10 },
                                                MSTCN: { w: 90, v: "", p: 11 },
                                                HSLD: { w: 60, v: "", p: 12 },
                                                BirthDate: { w: 90, v: "", p: 13 },
                                                FolkCode: { w: 60, v: "", p: 14 },
                                                NationalCode: { w: 90, v: "", p: 15 },
                                                DienThoai: { w: 90, v: "", p: 16 },
                                                CMND_TheCanCuoc: { w: 120, v: "", p: 17 },
                                                CMND_NgayCap: { w: 120, v: "", p: 18 },
                                                CMND_NoiCap: { w: 90, v: "", p: 19 },
                                                DiaChiThuongChu: { w: 180, v: "", p: 20 },
                                                BHXH: { w: 90, v: "", p: 21 },
                                                SexCode: { w: 60, v: "", p: 22 },
                                                FolkName: { w: 90, v: "", p: 23 },
                                                NationalName: { w: 120, v: "", p: 24 }
                                            };
                                        }

                                        this.ExportToExcel(this.ToWorkSheetData(RES.data, new dataStructure(), ['FieldGetError']), 'xlsx', function (workBook, workSheet) {
                                            var _this = this;
                                            var cols = new dataStructure();

                                            workSheet['!cols'] = [];
                                            for (var key in RES.data[0]) {
                                                if (typeof cols[key] != "undefined") {
                                                    workSheet['!cols'].push({ wpx: cols[key].w });
                                                }
                                            }

                                            $.each(RES.data, function (r, data) {
                                                var regex = new RegExp('^\\d+$');
                                                if (data.FieldGetError.length > 0) {
                                                    for (var f in data.FieldGetError) {
                                                        var fObj = cols[data.FieldGetError[f]];
                                                        if (typeof fObj != "undefined") {
                                                            var cell = _this.ToCellName(r + 1, fObj.p);
                                                            XLSX.utils.cell_add_comment(workSheet[cell], "Data lỗi!", "Sheet");
                                                        }
                                                    }
                                                }

                                                var c = 0;
                                                for (var i in data) {
                                                    if (regex.test(data[i])) {
                                                        var cell = _this.ToCellName(r + 1, c);
                                                        //XLSX.utils.cell_set_number_format(workSheet[cell], "0");
                                                    }
                                                    c++;
                                                }
                                            });
                                        });
                                    }
                                }
                            }
                        });


                    });


                </script>
                <!-- Hidden Field-->
                <!-- END Hidden Field-->
            </div>
            <%-- END Listing --%>
            <!-- Loading-->
            <div class="page-loading">
                <p>Vui lòng đợi trong giây lát...</p>
            </div>
        </div>
        <script src="../../../Assets/js/sheet/shim.js"></script>
        <script src="../../../Assets/js/sheet/xlsx-0.11.7.min.js"></script>
        <script src="../../../Assets/js/sheet/Blob.js"></script>
        <script src="../../../Assets/js/sheet/FileSaver.js"></script>
        <script src="../../../Assets/js/sheet/swfobject.min.js"></script>
        <script src="../../../Assets/js/sheet/downloadify.min.js"></script>
        <script src="../../../Assets/js/sheet/fn.sheet.js"></script>
    </asp:Panel>

</asp:Content>