﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TemplateMaster/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Edit_TeamStaff.aspx.cs" Inherits="_30shine.GUI.BackEnd.UIStaff.Edit_TeamStaff" %>

<asp:Content runat="server" ID="headContent" ContentPlaceHolderID="head">
    <title>Ghép nhóm và team cho Stylist</title>
    <script src="/Assets/js/sweetalert.min.js"> </script>
    <link rel="stylesheet" type="text/css" href="/Assets/css/sweetalert.css" />
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="CtMain" runat="server">

    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
        </style>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý Nhân sự &nbsp;&#187; </li>
                        <li class="li-listing li-listing1"><a href="javascript:void(0);">Cấu hình Team - Group</a></li>
                        <%--<li class="li-listing li-roll"><a href="/admin/nhan-vien/diem-danh.html">Điểm danh</a></li>--%>
                        <%--<li class="li-listing li-payon"><a href="/admin/payon/them-moi.html">Cấu hình tiền công</a></li>--%>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing be-report">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <!-- Filter -->
                <div class="row row-filter">
                    <strong class="st-head"><i class="fa fa-filter"></i>Lọc kết quả</strong>
                    <div class="filter-item">
                        <asp:DropDownList ID="Salon" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px;"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="StaffType" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="margin: 12px 0; width: 190px;"></asp:DropDownList>
                    </div>
                    <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                    <a href="/khach-hang/danh-sach.html" class="st-head btn-viewdata">Reset Filter</a>
                </div>
                <!-- End Filter -->
                <!-- Row Table Filter -->
                <div class="table-func-panel">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="table-wp">
                            <table class="table-add table-listing" id="tblTeam">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>ID</th>
                                        <th style="display:none">Salon</th>
                                        <th>Họ và tên</th>
                                        <th>GroupLevel</th>
                                        <th>TeamId</th>
                                        <th>Hành động</th>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="RptStaff" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td><%# Eval("Id") %></td>
                                                <td>
                                                    <a href="/admin/nhan-vien/<%# Eval("Id") %>.html"><%# Eval("Name") %></a>
                                                </td>
                                                <td>
                                                    <input type="text" id="<%#Eval("Id") %>" value="<%# Eval("GroupLevel") %>" onkeypress="return ValidateKeypress(/\d/,event);" maxlength="10"; disabled="disabled"; style="width: 60px; margin-left: 0; text-align: center; margin-left: 0px !important; border: none; border-bottom: 1px solid #000; float: none;" /></td>
                                                <td>
                                                    <input type="text" id="<%#Eval("Id") %>teamId" value="<%# Eval("TeamId") %>" onkeypress="return ValidateKeypress(/\d/,event);" maxlength="10"; disabled="disabled"; style="width: 60px; margin-left: 0; text-align: center; margin-left: 0px !important; border: none; border-bottom: 1px solid #000; float: none;" /></td>
                                                <td class="map-edit" style="max-width: 400px; width: 400px;">
                                                    <div id="<%#Eval("Id") %>btnEdit" class="st-head btn-viewdata" data-id="<%# Eval("Id") %>" onclick="clickEdit($(this))" style="margin-left: 48%;">
                                                        Sửa
                                                    </div>
                                                    <div id="<%#Eval("Id") %>btnCancel" class="st-head btn btn-warning" data-id="<%# Eval("Id") %>" onclick="clickCancel($(this))" style="margin-left: 5px; color: white; display: none; font-size: 13px; padding: 0px 8px;">
                                                        Huỷ bỏ thay đổi
                                                    </div>
                                                    <div id="<%#Eval("Id") %>btnSave" class="st-head btn-viewdata" data-id="<%# Eval("Id") %>" onclick="clickSave($(this))" style="background-color: #1AC0E0 !important; display: none;">
                                                        Lưu thay đổi
                                                    </div>
                                                    <div id="<%#Eval("Id") %>btnClear" class="st-head btn btn-danger" data-id="<%# Eval("Id") %>" onclick="clickClear($(this))" style="margin-left: 8px; color: white; display: none; font-size: 13px; padding: 0px 8px;">
                                                        Xoá bỏ Group&Team
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>

                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                <!-- Hidden Field-->
                <asp:HiddenField ID="HDF_Suggestion_Code" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="HDF_Suggestion_Field" ClientIDMode="Static" runat="server" />
                <!-- END Hidden Field-->
            </div>
            <%-- END Listing --%>
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 190px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script>
            //sửa
            var id;
            var teamId;
            function clickEdit(This) {
                let Id = This.attr("data-id");
                id = $(`#${Id}`).val();
                team = $(`#${Id}teamId`).val();
                $(`#${Id}`).removeAttr('disabled');
                $(`#${Id}teamId`).removeAttr('disabled');
                $(`#${Id}btnSave`).show();
                $(`#${Id}btnCancel`).show();
                $(`#${Id}btnClear`).show();
                $(`#${Id}btnEdit`).hide();
            }

            //huỷ bỏ
            function clickCancel(This) {
                let Id = This.attr("data-id");
                $(`#${Id}`).attr('disabled', true);
                $(`#${Id}teamId`).attr('disabled', true);
                $(`#${Id}btnSave`).hide();
                $(`#${Id}btnCancel`).hide();
                $(`#${Id}btnClear`).hide();
                $(`#${Id}btnEdit`).show();
                $(`#${Id}`).val(id);
                $(`#${Id}teamId`).val(team);
                id = 0;
                team = 0;
            }

            //xoá bỏ
            function clickClear(This) {
                var Id = This.attr("data-id");
                $.ajax({
                    type: "POST",
                    url: "/GUI/BackEnd/Staff/Edit_TeamStaff.aspx/DeleteTeamStaff",
                    data: '{Id: "' + Id + '",group : "' + 0 + '" , team : "' + 0 + '"}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.d == 1) {
                            swal("Xoá bỏ Group&Team thành công...", "", "success");
                            $(`#${Id}btnSave`).hide();
                            $(`#${Id}btnCancel`).hide();
                            $(`#${Id}btnClear`).hide();
                            $(`#${Id}btnEdit`).show();
                            $(`#${Id}`).attr('disabled', true);
                            $(`#${Id}teamId`).attr('disabled', true);
                            $(`#${Id}`).val('0');
                            $(`#${Id}teamId`).val('0');
                        }
                        else {
                            swal("Xoá bỏ Group&Team thất bại...", "", "error");
                        }
                    },
                    failure: function (response) { swal("Có lỗi xảy ra vui lòng liên hệ nhà phát triển...", "", "error") }
                });
            }

            //lưu thay đổi
            function clickSave(This, salon) {
                var Id = This.attr("data-id");
                var group = $(`#${Id}`).val();
                var team = $(`#${Id}teamId`).val();
                let departmentId = $("#StaffType :selected").val();
                let salonId = $("#Salon :selected").val();

                if (group == '' || team == '') {
                    swal("Không để trống Group&Team...", "", "error");
                    $(`#${Id}btnSave`).focus();
                }
                else if (group == '0' || team == '0') {
                    swal("Giá trị Group&Team phải lớn hơn 0...", "", "error");
                    $(`#${Id}btnSave`).focus();
                }
                else {
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/Staff/Edit_TeamStaff.aspx/UpdateTeamStaff",
                        data: '{Id: "' + Id + '", salonId: "' + salonId + '", departmentId: "' + departmentId + '",group : "' + group + '" , team : "' + team + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            if (response.d == 2) {
                                swal("Hoàn tất thành công...", "", "success");
                                $(`#${Id}btnSave`).hide();
                                $(`#${Id}btnCancel`).hide();
                                $(`#${Id}btnClear`).hide();
                                $(`#${Id}btnEdit`).show();
                                $(`#${Id}`).attr('disabled', true);
                                $(`#${Id}teamId`).attr('disabled', true);
                            }
                            else if (response.d == 1) {
                                swal("Team đã đủ người, hãy chọn team khác...", "", "error");
                            }
                            else {
                                swal("Hoàn tất thất bại...", "", "error");
                            }
                        },
                        failure: function (response) { swal("Có lỗi xảy ra vui lòng liên hệ nhà phát triển!", "", "error") }
                    });
                }
            }

            // Validate keypress
            function ValidateKeypress(numcheck, e) {
                var keynum, keychar, numcheck;
                if (window.event) {
                    keynum = e.keyCode;
                }
                else if (e.which) {
                    keynum = e.which;
                }
                if (keynum == 8 || keynum == 127 || keynum == null || keynum == 9 || keynum == 0 || keynum == 13) return true;
                keychar = String.fromCharCode(keynum);
                var result = numcheck.test(keychar);
                return result;
            }
        </script>
    </asp:Panel>
</asp:Content>
