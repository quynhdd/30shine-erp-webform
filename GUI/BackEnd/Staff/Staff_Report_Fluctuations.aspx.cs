﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Linq.Expressions;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.UIStaff
{
    public partial class Staff_Report_Fluctuations : System.Web.UI.Page
    {
        protected string PageID = "BK_DS";
        protected Paging PAGING = new Paging();
        private bool Perm_Access = false;
        private bool Perm_Delete = false;
        protected bool Perm_ViewAllData = false;
        protected bool Perm_ViewAllDataByDate = false;
        protected bool Perm_ViewAllDataBySalon = false;
        protected string style_css = "";
        protected string style1_css = "";
        protected DateTime timeFrom;
        protected DateTime timeTo;
        protected string color = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            if (!IsPostBack)
            {
                txtStartDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now));
                txtEndDate.Text = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(DateTime.Now));
                Library.Function.bindSalon_NotHoiQuan(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
                BindType();
            }
        }
        CultureInfo culture = new CultureInfo("vi-VN");
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //Perm_ViewAllDataByDate = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDataByDate", PageID, permission);
                //Perm_ViewAllDataBySalon = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllDataBySalon", PageID, permission);
                //Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllDataByDate = permissionModel.CheckPermisionByAction("Perm_ViewAllDataByDate", pageId, staffId);
                Perm_ViewAllDataBySalon = permissionModel.CheckPermisionByAction("Perm_ViewAllDataBySalon", pageId, staffId);
                Perm_Delete = permissionModel.CheckPermisionByAction("Perm_Delete", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        protected string checkper()
        {
            string str = "";
            int _UserId = Convert.ToInt32(Session["User_Id"]);
            string a = Session["User_Permission"].ToString();
            if (a == "admin" || a == "root")
            {
                str = "";
            }
            if (_UserId == 119 || _UserId == 79)
            {
                str = "";
            }
            else
            {
                str = "display_permission";
            }

            return str;
        }

        private void BindType()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var sql = "";
                    sql = @"select * from Staff_Type WHERE IsDelete = 0 order by Id asc";
                    var lstType = db.Staff_Type.SqlQuery(sql).ToList();
                    var Key = 0;
                    ddlType.DataTextField = "Name";
                    ddlType.DataValueField = "Id";
                    ListItem item = new ListItem("Chọn bộ phận", "0");
                    ddlType.Items.Insert(0, item);
                    foreach (var v in lstType)
                    {
                        Key++;
                        item = new ListItem(v.Name, v.Id.ToString());
                        ddlType.Items.Insert(Key, item);
                    }
                    ddlType.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
     
        private void Permission()
        {
            int _UserId = Convert.ToInt32(Session["User_Id"]);
            if (_UserId == 119 || _UserId == 79 || _UserId == 0)
            {
                style1_css = "display_permission";
            }
            else
            {
                style_css = "display_permission";
            }
        }

        public List<cls_Staff_Fluctuation_TotalAllSalon> GetListData()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    int integer;
                    int salonId = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                    int typeId = int.TryParse(ddlType.SelectedValue, out integer) ? integer : 0;
                    this.timeFrom = new DateTime();
                    this.timeTo = new DateTime();
                    var lstStaff = new List<cls_Staff_Fluctuation_TotalAllSalon>();
                    if (txtStartDate.Text != "")
                    {
                        timeFrom = Convert.ToDateTime(txtStartDate.Text, culture);
                        if (txtEndDate.Text != "")
                        {
                            if (txtStartDate.Text == txtEndDate.Text)
                            {
                                timeTo = Convert.ToDateTime(txtStartDate.Text, culture).AddDays(1);
                            }
                            else
                            {
                                timeTo = Convert.ToDateTime(txtEndDate.Text, culture).AddDays(1);
                            }
                        }
                        else
                        {
                            timeTo = timeFrom;
                        }
                        var listData = db.Store_Staff_Fluctuation_TotalAllSalon(timeFrom, timeTo, salonId, typeId).ToList();
                        foreach (var item in listData)
                        {
                            var clsStaff = new cls_Staff_Fluctuation_TotalAllSalon();
                            clsStaff.advancedTraining = item.advancedTraining;
                            clsStaff.AgainSource = item.AgainSource;
                            clsStaff.againTraining = item.againTraining;
                            clsStaff.leaveJob = item.leaveJob;
                            clsStaff.numberTransfer = item.numberTransfer + item.NumberReceive;
                            clsStaff.OutSource = item.OutSource;
                            clsStaff.S4mSource = item.S4mSource;
                            clsStaff.SalonId = item.SalonId;
                            clsStaff.SalonName = item.SalonName;
                            clsStaff.tmpLeaveJob = item.tmpLeaveJob;
                            clsStaff.WorkBack = item.WorkBack;
                            if(typeId == 0)
                            {
                                clsStaff.iCount = (clsStaff.OutSource + clsStaff.S4mSource + clsStaff.AgainSource + clsStaff.numberTransfer) - (clsStaff.tmpLeaveJob +
                                    clsStaff.leaveJob  + clsStaff.advancedTraining + clsStaff.againTraining);
                            }
                            else if (typeId == 1)
                            {
                                clsStaff.iCount = (clsStaff.OutSource + clsStaff.S4mSource + clsStaff.numberTransfer + clsStaff.WorkBack + clsStaff.AgainSource) - (clsStaff.tmpLeaveJob + clsStaff.leaveJob + clsStaff.advancedTraining + clsStaff.againTraining);
                            }
                            else
                            {
                                clsStaff.iCount = (clsStaff.OutSource  + clsStaff.numberTransfer + clsStaff.WorkBack + clsStaff.AgainSource) - (clsStaff.tmpLeaveJob + clsStaff.leaveJob);
                            }
                            if (clsStaff.iCount > 0)
                            {
                                clsStaff.Color = "cls_Green";
                            }
                            else if (clsStaff.iCount < 0)
                            {
                                clsStaff.Color = "cls_Red";
                            }
                            else
                            {
                                clsStaff.Color = "";
                            }
                            lstStaff.Add(clsStaff);
                        }
                    }
                    return lstStaff;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Binding data
        /// </summary>
        public void bindData()
        {
            var data = GetListData();
            RptFluctuation.DataSource = data;
            RptFluctuation.DataBind();
        }
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }
        protected void _BtnClick(object sender, EventArgs e)
        {
            bindData();
            RemoveLoading();
        }
    }
    public partial class cls_Staff_Fluctuation_TotalAllSalon
    {
        public int SalonId { get; set; }
        public string SalonName { get; set; }
        public int OutSource { get; set; }
        public int S4mSource { get; set; }
        public int NumberReceive { get; set; }
        public int tmpLeaveJob { get; set; }
        public int leaveJob { get; set; }
        public int numberTransfer { get; set; }
        public int advancedTraining { get; set; }
        public int againTraining { get; set; }
        public int WorkBack { get; set; }
        public int AgainSource { get; set; }
        public Nullable<int> iCount { get; set; }
        public string Color { get; set; }
    }
}