﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Profile.aspx.cs" Inherits="_30shine.GUI.BackEnd.UIStaff.Profile" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="Profile" ContentPlaceHolderID="CtMain" runat="server">
<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

<div class="wp customer-add customer-listing">
    <%-- Add --%>                
    <div class="wp960 content-wp content-customer-detail">
        <div class="table-wp">
            <table class="table-add">
                <tbody>
                    <tr class="title-head">
                        <td><strong>Thông tin cá nhân</strong></td>
                        <td></td>
                    </tr>
                    <tr class="tr-margin " style="height: 20px;"></tr>
                    <tr>
                        <td class="col-xs-2 left"><span>Họ và tên</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="FullName" runat="server" ClientIDMode="Static"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="FullNameValidate" ControlToValidate="FullName" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập họ tên!"></asp:RequiredFieldValidator>
                            </span>
                                            
                        </td>
                    </tr>

                    <tr>
                        <td class="col-xs-2 left"><span>Số điện thoại</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="Phone" runat="server" ClientIDMode="Static"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="PhoneValidate" ControlToValidate="Phone" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập số điện thoại!"></asp:RequiredFieldValidator>
                            </span>
                        </td>
                    </tr>

                    <tr>
                        <td class="col-xs-2 left"><span>Email</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="Email" runat="server" ClientIDMode="Static" placeholder="example@gmail.com"></asp:TextBox>
                            </span>
                        </td>
                    </tr>

                    <tr class="tr-birthday">
                        <td class="col-xs-2 left"><span>Sinh nhật</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:TextBox ID="Day" runat="server" ClientIDMode="Static" placeholder="Ngày"></asp:TextBox>
                                <asp:TextBox ID="Month" runat="server" ClientIDMode="Static" placeholder="Tháng"></asp:TextBox>
                                <asp:TextBox ID="Year" runat="server" ClientIDMode="Static" placeholder="Năm"></asp:TextBox>
                            </span>
                        </td>
                    </tr>

                    <tr>
                        <td class="col-xs-2 left"><span>Giới tính</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:DropDownList ID="Gender" runat="server" ClientIDMode="Static"></asp:DropDownList>
                            </span>
                        </td>
                    </tr>

                    <tr>
                        <td class="col-xs-2 left"><span>Địa chỉ</span></td>
                        <td class="col-xs-9 right">
                            <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
                            <asp:UpdatePanel runat="server" ID="UP01">
                                <ContentTemplate>
                                    <div class="city">
                                        <asp:DropDownList AutoPostBack="true" ID="City" runat="server" CssClass="select"
                                            OnSelectedIndexChanged="Reload_District" ClientIDMode="Static">
                                        </asp:DropDownList>
                                    </div>
                                    <div class="district">
                                        <asp:DropDownList ID="District" runat="server" CssClass="select" ClientIDMode="Static"></asp:DropDownList>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <%--<asp:AsyncPostBackTrigger ControlID="OrderCloseBtn" EventName="Click"/>--%>
                                </Triggers>
                            </asp:UpdatePanel>
                            <asp:TextBox ID="Address" runat="server" placeholder="Địa chỉ" ClientIDMode="Static"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td class="col-xs-2 left"><span>Bộ phận</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:DropDownList ID="TypeStaff" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <%--<asp:RequiredFieldValidator InitialValue="0" 
                                    ID="ValidateSalonTypeStaff" Display="Dynamic" 
                                    ControlToValidate="TypeStaff"
                                    runat="server"  Text="Bạn chưa chọn kiểu nhân viên!" 
                                    ErrorMessage="Vui lòng chọn kiểu nhân viên!"
                                    ForeColor="Red"
                                    CssClass="fb-cover-error">
                                </asp:RequiredFieldValidator>--%>
                            </span>
                        </td>
                    </tr>

                    <tr runat="server" id="TrPermission">
                        <td class="col-xs-2 left"><span>Phân quyền</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:DropDownList ID="PermissionList" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <%--<asp:RequiredFieldValidator InitialValue="0" 
                                    ID="ValidateSalonTypeStaff" Display="Dynamic" 
                                    ControlToValidate="TypeStaff"
                                    runat="server"  Text="Bạn chưa chọn kiểu nhân viên!" 
                                    ErrorMessage="Vui lòng chọn kiểu nhân viên!"
                                    ForeColor="Red"
                                    CssClass="fb-cover-error">
                                </asp:RequiredFieldValidator>--%>
                            </span>
                        </td>
                    </tr>

                    <tr runat="server" id="TrSalon">
                        <td class="col-xs-2 left"><span>Salon</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:DropDownList ID="Salon" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                <%--<asp:RequiredFieldValidator InitialValue="0" 
                                    ID="ValidateSalon" Display="Dynamic" 
                                    ControlToValidate="Salon"
                                    runat="server"  Text="Bạn chưa chọn Salon!" 
                                    ErrorMessage="Vui lòng chọn Salon!"
                                    ForeColor="Red"
                                    CssClass="fb-cover-error">
                                </asp:RequiredFieldValidator>--%>
                            </span>
                        </td>
                    </tr>
                    
                    <tr class="tr-send">
                        <td class="col-xs-2 left"></td>
                        <td class="col-xs-9 right no-border">
                            <span class="field-wp">
                                <asp:Button ID="Send" CssClass="btn-send perm-btn-field" runat="server" Text="Cập nhật" ClientIDMode="Static" OnClick="Update_OBJ"></asp:Button>
                                <span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Đóng</span>
                            </span>
                        </td>
                    </tr>
                    <asp:HiddenField runat="server" ID="UrlKeyField"  ClientIDMode="Static"/>
                    <asp:HiddenField runat="server" ID="ImgCoverField"  ClientIDMode="Static"/>
                </tbody>
            </table>
        </div>
    </div>
    <%-- end Add --%>

    <!-- Customer History -->
    <% if(OBJ != null){ %>
    <div class="wp960 content-wp content-customer-history">
        <!-- System Message -->
        <asp:Label style="width: 50%; margin-left: 30px;" ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
        <!-- END System Message -->
        <div class="container">
            <div class="row">
                <!-- Common Information -->
                <div class="col-xs-12 line-head">
                    <strong class="st-head" style="margin-top: 0px;">
                        <i class="fa fa-file-text"></i>
                        Thông tin khách hàng
                        <span class="view-more-infor">(Xem thông tin đầy đủ)</span>
                    </strong>
                </div>
                <div class="col-xs-12 common-infor">
                    <p>
                        Họ tên&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<%=OBJ.Fullname %>
                    </p>
                    <p>    Tuổi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<%=OBJ.Age %>
                    </p>
                    <p>
                        Số ĐT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<%=OBJ.Phone %>
                    </p>
                    <p>
                        Mã KH&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<%=OBJ.Customer_Code %>
                    </p>
                    <p>
                        Địa chỉ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;<%=OBJ.CityName %>, <%=OBJ.DistrictName %>

                    </p>
                </div>
                <!--/ Common Information -->

                <!-- Statistic Value -->
                <div class="col-xs-12 line-head">
                    <strong class="st-head" style="border-bottom:none; margin-bottom: 3px;">
                        <i class="fa fa-file-text"></i>Thống kê giá trị
                    </strong>
                </div>
                <div class="col-xs-12 customer-value-statistic">
                    <div class="table-wp">
                        <table class="table-add table-listing" style="width: 400px;">
                            <tbody>
                                <tr>
                                    <td>Tổng số lần sử dụng dịch vụ</td>
                                    <td class="be-report-price"><%=Ctm_Statistic.TimesUsed_Service %></td>
                                </tr>
                                <tr>
                                    <td>Tổng chi phí</td>
                                    <td class="be-report-price"><%=Ctm_Statistic.TotalMoney %></td>
                                </tr>   
                                <tr>
                                    <td>Chi phí dịch vụ</td>
                                    <td class="be-report-price"><%=Ctm_Statistic.TotalMoney_Service %></td>
                                </tr>
                                <tr>
                                    <td>Chi phí sản phẩm</td>
                                    <td class="be-report-price"><%=Ctm_Statistic.TotalMoney_Product %></td>
                                </tr>                                
                            </tbody>
                        </table>
                    </div>
                    <div class="customer-detail-service">
                        <p class="_p">- Chi tiết</p>
                        <div class="tbl-wp">
                            <div class="table-wp">
                                <table class="table-add table-listing" style="width: 400px;">
                                    <tbody>
                                        <tr>
                                            <td style="font-family: Roboto Condensed Bold;">Dịch vụ</td>
                                            <td><%=Ctm_Statistic.TimesUsed_Service %></td>
                                            <td class="be-report-price"><%=Ctm_Statistic.TotalMoney_Service %></td>
                                        </tr>
                                        <%=Ctm_Statistic.Service_Statistic %>                            
                                    </tbody>
                                </table>
                            </div>
                            <div class="table-wp">
                                <table class="table-add table-listing" style="width: 400px; margin-top: 15px;">
                                    <tbody>
                                        <tr>
                                            <td style="font-family: Roboto Condensed Bold;">Sản phẩm</td>
                                            <td><%=Ctm_Statistic.TimesUsed_Product %></td>
                                            <td class="be-report-price"><%=Ctm_Statistic.TotalMoney_Product %></td>
                                        </tr>
                                        <%=Ctm_Statistic.Product_Statistic %>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!--/ Statistic Value -->

                <!-- History -->
                <div class="history-timeline">
                    <div class="line-h"></div>
                    <div class="col-xs-12 elm">
                        <div class="row">
                            <div class="col-xs-2 left">
                                <div class="wrap">
                                    <div class="time-value" style="font-family:Roboto Condensed Bold; font-size: 15px;">Lịch sử</div>
                                </div>                                
                            </div>
                            <div class="col-xs-10 right"></div>
                        </div>                        
                    </div>                    
                    
                    <% foreach(var v in Ctm_History){ %>
                        <!-- elm -->
                        <div class="col-xs-12 elm">
                            <div class="row">
                                <div class="col-xs-2 left">
                                    <div class="wrap">
                                        <div class="time-value"><%=v.Date %></div>
                                        <div class="line-v"></div>
                                    </div>                                
                                </div>
                                <div class="col-xs-10 right">
                                    <div class="row">
                                        <div class="col-xs-7">
                                            <%=v.Table %>
                                            <div class="wrap listing-img-upload">
                                                <% if (v.Images != null && v.Images.Count > 0){ %>
                                                    <% foreach(var v2 in v.Images){ %>
                                                        <div class="thumb-wp" onclick="Plugin_Images_Chose(event, $(this))">
                                                            <img class="thumb" alt="" title="" src="<%=v2 %>"/>
                                                            <%--<span class="delete-thumb" onclick="deleteThum($(this), '<%=v2 %>', false)"></span>--%>
                                                            <div class="thumb-cover"></div>
                                                        </div>
                                                    <% } %>
                                                <%} %>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                        
                        </div>
                        <!--/ elm -->  
                        <% } %>                
                </div>
                <!--/ History -->

            </div>
        </div>
    </div>
    <% } %>
    <!--/ Customer History -->

</div>

<script>
    jQuery(document).ready(function () {
        $("#glbCustomer").addClass("active");
        $(".li-edit").addClass("active");

        // Price format
        UP_FormatPrice('.be-report-price');

        // Show form customer information
        $(".view-more-infor").bind("click", function () {
            $(".content-customer-detail").show();
        });
        $("#CalcelAddFbcv").bind("click", function () {
            $(".content-customer-detail").hide();
        });

        // fix thumbnail
        excThumbWidth();

        //============================
        // Show System Message
        //============================ 
        var qs = getQueryStrings();
        showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);
    });

    function excThumbWidth() {
        var ImgStandardWidth = 120,
            ImgStandardHeight = 120;
        var width = ImgStandardWidth,
            height, left, top;
        $(".thumb-wp .thumb").each(function () {
            height = ImgStandardWidth * $(this).height() / $(this).width();
            left = 0;
            top = (ImgStandardHeight - height) / 2;
            $(this).css({ "width": width, "height": height, "left": left, "top": top });
        });
    }

    function showMsgSystem(msg, status) {
        $("#MsgSystem").css("opacity", 0).text(msg).addClass(status).fadeTo(0, 1);
        setTimeout(function () {
            $("#MsgSystem").fadeTo("slow", 0, function () {
                $("#MsgSystem").text("").attr("class", "msg-system").css("opacity", 1);
            });
        }, 5000);
    }
</script>

</asp:Panel>
</asp:Content>
