﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.UIStaff
{
    public partial class Profile : System.Web.UI.Page
    {
        private string PageID = "";
        protected Staff OBJ;
        private bool Perm_Access = false;
        private bool Perm_AllPermission = false;
        private bool Perm_HidePermission = false;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                ////IPermissionModel permissionModel = new PermissionModel();
                ////var permission = Session["User_Permission"].ToString();
                ////Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_HidePermission = permissionModel.GetActionByActionNameAndPageId("Perm_HidePermission", PageID, permission);
                //////Perm_Add = permissionModel.GetActionByActionNameAndPageId("Perm_Add", PageID, permission);
                ////Perm_AllPermission = permissionModel.GetActionByActionNameAndPageId("Perm_AllPermission", PageID, permission);
                ////ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_HidePermission = permissionModel.CheckPermisionByAction("Perm_HidePermission", pageId, staffId);
                Perm_AllPermission = permissionModel.CheckPermisionByAction("Perm_AllPermission", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }


        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
            if (Perm_HidePermission)
            {
                TrPermission.Visible = false;
            }
            if (!Perm_AllPermission)
            {
                TrSalon.Visible = false;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                if (Bind_OBJ())
                {
                    Bind_TypeStaff();
                    Bind_Permission();
                    Bind_Gender();
                    Bind_City();
                    Bind_District();
                    Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_AllPermission);
                }
            }
        }

        public void Bind_TypeStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Order).ToList();
                var Key = 0;
                var Count = LST.Count;
                TypeStaff.DataTextField = "TypeStaff";
                TypeStaff.DataValueField = "Id";
                ListItem item = new ListItem("Chọn bộ phận", "0");
                TypeStaff.Items.Insert(Key, item);
                Key++;

                if (Count > 0)
                {
                    foreach (var v in LST)
                    {
                        item = new ListItem(v.Name, v.Id.ToString());
                        TypeStaff.Items.Insert(Key, item);
                        Key++;
                    }
                }

                var ItemSelected = TypeStaff.Items.FindByValue(OBJ.Type.ToString());
                if (ItemSelected != null)
                    ItemSelected.Selected = true;
            }
        }

        private void Bind_Permission()
        {
            PermissionList.DataTextField = "Permission";
            PermissionList.DataValueField = "Value";

            var item = new ListItem("Nhân viên", "0");
            PermissionList.Items.Insert(0, item);
            if (Perm_Access)
            {
                item = new ListItem("Lễ tân", "1");
                PermissionList.Items.Insert(1, item);
            }
            if (Perm_AllPermission)
            {
                item = new ListItem("Trưởng cửa hàng", "2");
                PermissionList.Items.Insert(2, item);
                item = new ListItem("Admin", "3");
                PermissionList.Items.Insert(3, item);
            }

            string[] PermLST = new string[] { "staff", "reception", "salonmanager", "admin" };
            int index = Array.IndexOf(PermLST, OBJ.Permission);
            if (index != -1)
            {
                var ItemSelected = PermissionList.Items.FindByValue(index.ToString());
                if (ItemSelected != null)
                    ItemSelected.Selected = true;   
            }
        }

        private void Bind_Gender()
        {
            Gender.DataTextField = "TypeStaff";
            Gender.DataValueField = "Id";
            ListItem item = new ListItem("Chọn giới tính", "0");
            Gender.Items.Insert(0, item);
            item = new ListItem("Nam", "1");
            Gender.Items.Insert(1, item);
            item = new ListItem("Nữ", "2");
            Gender.Items.Insert(2, item);
            item = new ListItem("Khác", "3");
            Gender.Items.Insert(3, item);

            var ItemSelected = Gender.Items.FindByValue(OBJ.Gender.ToString());
            if (ItemSelected != null)
                ItemSelected.Selected = true;
        }
        

        protected void Bind_City()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.TinhThanhs.OrderBy(p => p.ThuTu).ToList();

                City.DataTextField = "TenTinhThanh";
                City.DataValueField = "Id";

                City.DataSource = lst;
                City.DataBind();

                var ItemSelected = City.Items.FindByValue(OBJ.CityId.ToString());
                if (ItemSelected != null)
                    ItemSelected.Selected = true;
            }
        }

        protected void Bind_District()
        {
            using (var db = new Solution_30shineEntities())
            {
                int _TinhThanhID = Convert.ToInt32(City.SelectedValue);
                if (_TinhThanhID != 0)
                {
                    var lst = db.QuanHuyens.Where(p => p.TinhThanhID == _TinhThanhID).OrderBy(p => p.ThuTu).ToList();

                    District.DataTextField = "TenQuanHuyen";
                    District.DataValueField = "Id";

                    District.DataSource = lst;
                    District.DataBind();

                    if (!IsPostBack)
                    {
                        var ItemSelected = District.Items.FindByValue(OBJ.DistrictId.ToString());
                        if (ItemSelected != null)
                        {
                            ItemSelected.Selected = true;
                        }
                        else
                        {
                            District.SelectedIndex = 0;
                        }
                    }
                    else 
                    {
                        District.SelectedIndex = 0;    
                    }
                }
            }
        }

        protected void Reload_District(object sender, EventArgs e)
        {
            Bind_District();
        }

        private bool Bind_OBJ()
        {
            var Code = Request.QueryString["Code"] != "" ? Convert.ToInt32(Request.QueryString["Code"]) : 0;
            var ExitsOBJ = true;
            using (var db = new Solution_30shineEntities())
            {
                OBJ = db.Staffs.FirstOrDefault(w => w.Id == Code);
                if (OBJ != null)
                {
                    FullName.Text = OBJ.Fullname;
                    Phone.Text = OBJ.Phone;
                    Email.Text = OBJ.Email;
                    Day.Text = OBJ.SN_day.ToString();
                    Month.Text = OBJ.SN_month.ToString();
                    Year.Text = OBJ.SN_year.ToString();
                    Address.Text = OBJ.Address;
                }
                else
                {
                    ExitsOBJ = false;
                    MsgSystem.Text = "Nhân viên không tồn tại!";
                    MsgSystem.CssClass = "msg-system warning";
                }
                return ExitsOBJ;
            }
        }



        protected void Update_OBJ(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var _Code = Convert.ToInt32(Request.QueryString["Code"]);
                var obj = db.Staffs.Where(w => w.Id == _Code).FirstOrDefault();
                int Error = 0;
                int ErrorPerm = 0;

                if (!obj.Equals(null))
                {
                    obj.Fullname = FullName.Text;
                    obj.Phone = Phone.Text;
                    obj.Email = Email.Text;
                    if (Day.Text.Length > 0)
                    {
                        obj.SN_day = Convert.ToInt32(Day.Text);
                    }

                    if (Month.Text.Length > 0)
                    {
                        obj.SN_month = Convert.ToInt32(Month.Text);
                    }
                    if (Year.Text.Length > 0)
                    {
                        obj.SN_year = Convert.ToInt32(Year.Text);
                    }

                    obj.Address = Address.Text;
                    obj.ModifiedDate = DateTime.Now;
                    obj.CityId = Convert.ToInt32(City.SelectedValue);
                    obj.DistrictId = Convert.ToInt32(District.SelectedValue);
                    obj.Gender = Convert.ToByte(Gender.SelectedValue);
                    obj.Type = Convert.ToInt32(TypeStaff.SelectedValue);

                    if (!Perm_AllPermission)
                    {
                        obj.SalonId = Convert.ToInt32(Salon.SelectedValue);
                    }

                    if (!Perm_HidePermission)
                    {
                        // Check set permission                        
                        string[] PermLST = new string[] { "staff", "reception", "salonmanager", "admin" };
                        int index = PermissionList.SelectedValue != "" ? Convert.ToInt32(PermissionList.SelectedValue) : 0;
                        if (index > PermLST.Length || (!Perm_AllPermission && index > 1))
                        {
                            Error++;
                            ErrorPerm++;
                        }
                        else
                        {
                            obj.Permission = PermLST[index].ToString();
                        }
                    }

                    // Validate
                    // Check trùng số điện thoại
                    var ExistPhone = db.Staffs.Count(w => w.Id != obj.Id && w.Phone == obj.Phone);
                    if (ExistPhone > 0)
                    {
                        var msg = "Số điện thoại đã tồn tại. Bạn vui lòng nhập số điện thoại khác.";
                        var status = "msg-system warning";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + msg + "','" + status + "')", true);
                        Error++;
                    }

                    // Check trùng email
                    if (obj.Email != "")
                    {
                        var ExistEmail = db.Staffs.Count(w => w.Id != obj.Id && w.Email == obj.Email);
                        if (ExistEmail > 0)
                        {
                            var msg = "Email đã tồn tại. Bạn vui lòng nhập email khác.";
                            var status = "msg-system warning";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + msg + "','" + status + "')", true);
                            Error++;
                        }
                    }

                    if (ErrorPerm > 0)
                    {
                        var msg = "Giá trị phân quyền không đúng.";
                        var status = "msg-system warning";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + msg + "','" + status + "')", true);
                    }

                    if (Error == 0)
                    {
                        db.Staffs.AddOrUpdate(obj);
                        var exc = db.SaveChanges();
                        if (exc > 0)
                        {
                            var msg = "Cập nhật thành công!";
                            var status = "msg-system success";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + msg + "','" + status + "')", true);
                        }
                        else
                        {
                            var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                            var status = "msg-system warning";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + msg + "','" + status + "')", true);
                        }   
                    }
                }
                else
                {
                    MsgSystem.Text = "Lỗi! Nhân viên không tồn tại.";
                    MsgSystem.CssClass = "msg-system warning";
                }
            }
        }

        
    }
}