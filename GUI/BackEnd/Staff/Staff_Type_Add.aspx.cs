﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.UIStaff
{
    public partial class Staff_Type_Add : System.Web.UI.Page
    {
        private string PageID = "";
        protected Staff_Type OBJ;
        protected string _Code;
        protected bool _IsUpdate = false;
        private bool Perm_Access = false;

        private List<string> listSkillLevel;
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //if (Request.QueryString["Code"] !=null)
                //{
                //    PageID = "KNV_EDIT";

                //}
                //else
                //{
                //    PageID = "KNV_ADD";

                //}
                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                ////Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //////Perm_Add = permissionModel.GetActionByActionNameAndPageId("Perm_Add", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                if (IsUpdate())
                {
                    if (Bind_OBJ())
                    {
                        //
                    }
                }
                else
                {
                     // 
                }
                bindSkillLevel();
            }            
        }

        /// <summary>
        /// Xử lý update hoặc add
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ExcAddOrUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
            {
                Update();
            }
            else
            {
                Add();
            }
        }

        /// <summary>
        /// Thêm mới bộ phận
        /// </summary>
        private void Add()
        {
            using (var db = new Solution_30shineEntities())
            {
                var obj = new Staff_Type();
                obj.Name = Name.Text;
                obj.Description = Description.Text;                
                obj.Order = _Order.Text != "" ? Convert.ToInt32(_Order.Text) : 0;
                obj.SkillLevelIds = HDF_SkillLevelArray.Value;
                obj.Publish = Publish.Checked;
                obj.IsSpecialBillRating = IsSpecialBillRating.Checked;
                obj.CreatedDate = DateTime.Now;
                obj.IsDelete = 0;

                // Validate
                var Error = false;

                if (!Error)
                {
                    db.Staff_Type.Add(obj);
                    var exc = db.SaveChanges();

                    if (exc > 0)
                    {
                        updatePayon(obj.Id, HDF_SkillLevelArray.Value, db);
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                        UIHelpers.Redirect("/admin/kieu-nhan-vien.html", MsgParam);
                    }
                    else
                    {
                        var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);  
                    }
                }
            }
        }

        /// <summary>
        /// Cập nhật bộ phận
        /// </summary>
        private void Update()
        {
            using (var db = new Solution_30shineEntities())
            {
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Staff_Type.FirstOrDefault(w => w.Id == Id);

                    if (!OBJ.Equals(null))
                    {
                        OBJ.Name = Name.Text;
                        OBJ.Description = Description.Text;
                        OBJ.Order = _Order.Text != "" ? Convert.ToInt32(_Order.Text) : 0;
                        OBJ.SkillLevelIds = HDF_SkillLevelArray.Value;
                        OBJ.Publish = Publish.Checked;
                        OBJ.IsSpecialBillRating = IsSpecialBillRating.Checked;
                        OBJ.ModifiedDate = DateTime.Now;
                        OBJ.IsDelete = 0;

                        db.Staff_Type.AddOrUpdate(OBJ);
                        var exc = db.SaveChanges();
                        if (exc > 0)
                        {
                            updatePayon(OBJ.Id, HDF_SkillLevelArray.Value, db);
                            var MsgParam = new List<KeyValuePair<string, string>>();
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                            UIHelpers.Redirect("/admin/kieu-nhan-vien/" + OBJ.Id + ".html", MsgParam);
                        }
                        else
                        {
                            var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                            var status = "warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                        }
                    }
                    else
                    {
                        var msg = "Lỗi! Không tìm thấy danh mục.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);

                    }
                }
                else
                {
                    MsgSystem.Text = "Lỗi! Không tìm thấy danh mục.";
                    MsgSystem.CssClass = "msg-system warning";
                }

                Context.RewritePath("+", "", "");
            }
        }

        private void updatePayon(int department, string skillLevels, Solution_30shineEntities db)
        {
            var payMethods = new List<KeyValuePair<int, string>>();
            payMethods.Add(new KeyValuePair<int, string>(1, "Lương cứng"));
            payMethods.Add(new KeyValuePair<int, string>(3, "Lương part-time"));
            payMethods.Add(new KeyValuePair<int, string>(4, "Lương phụ cấp"));
            payMethods.Add(new KeyValuePair<int, string>(5, "Hệ số điểm hài lòng"));
            var payon = new Tbl_Payon();

            if (department > 0 && skillLevels != "")
            {
                var payons = db.Tbl_Payon.Where(w => w.TypeStaffId == department && w.Hint == "base_salary").ToList();
                var levels = skillLevels.Split(',').ToList();
                var index = -1;
                int level;
                
                /// Kiểm tra xóa các giá trị cũ
                foreach (var v in payons)
                {
                    index = levels.IndexOf(v.ForeignId.ToString());
                    if (index == -1)
                    {
                        db.Tbl_Payon.Remove(v);
                        db.SaveChanges();
                    }
                }
                /// Kiểm tra thêm các giá trị mới
                foreach (var v in levels)
                {
                    level = Convert.ToInt32(v);
                    index = payons.FindIndex(w => w.ForeignId == level);
                    if (index == -1)
                    {
                        foreach (var v2 in payMethods)
                        {
                            addPayon(department, level, v2, db);
                        }
                    }
                }
            }
            else if (department > 0 && skillLevels == "")
            {
                /// Xóa tất cả payon đã lập
                var payons = db.Tbl_Payon.Where(w => w.TypeStaffId == department && w.Hint == "base_salary").ToList();
                if (payons.Count > 0)
                {
                    foreach (var v in payons)
                    {
                        db.Tbl_Payon.Remove(v);
                        db.SaveChanges();
                    }
                }
                /// Kiểm tra insert mặc định cho skillLevel = 0
                var isset = db.Tbl_Payon.FirstOrDefault(w=>w.TypeStaffId == department && w.ForeignId == 0 && w.Hint == "base_salary");
                if (isset == null)
                {
                    foreach (var v2 in payMethods)
                    {
                        addPayon(department, 0, v2, db);
                    }
                }
            }
        }

        private void addPayon(int department, int skillLevel, KeyValuePair<int, string> payMethod, Solution_30shineEntities db)
        {
            var payon = new Tbl_Payon();
            payon.TypeStaffId = department;
            payon.ForeignId = skillLevel;
            payon.Hint = "base_salary";
            payon.PayByTime = Convert.ToByte(payMethod.Key);
            payon.Description = payMethod.Value;
            payon.Value = 0;
            payon.CreatedDate = DateTime.Now;
            payon.Publish = true;
            payon.IsDelete = 0;
            db.Tbl_Payon.AddOrUpdate(payon);
            db.SaveChanges();
        }

        /// <summary>
        /// Bind dữ liệu bộ phận khi cập nhật
        /// </summary>
        /// <returns></returns>
        private bool Bind_OBJ()
        {
            using (var db = new Solution_30shineEntities())
            {
                var ExistOBJ = true;
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Staff_Type.Where(w => w.Id == Id).FirstOrDefault();

                    if (!OBJ.Equals(null))
                    {
                        Name.Text = OBJ.Name;
                        Description.Text = OBJ.Description;
                        _Order.Text = Convert.ToString(OBJ.Order);

                        if (OBJ.Publish == true)
                        {
                            Publish.Checked = true;
                        }
                        else
                        {
                            Publish.Checked = false;
                        }
                        if (OBJ.IsSpecialBillRating == true)
                        {
                            IsSpecialBillRating.Checked = true;
                        }
                        else
                        {
                            IsSpecialBillRating.Checked = false;
                        }
                    }
                    else
                    {
                        ExistOBJ = false;
                        var msg = "Lỗi! Kiểu nhân viên không tồn tại";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
                else
                {
                    ExistOBJ = false;
                    var msg = "Lỗi! Kiểu nhân viên không tồn tại";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }

                return ExistOBJ;
            }
        }

        /// <summary>
        /// Bind dữ liệu bậc kỹ năng
        /// </summary>
        private void bindSkillLevel()
        {
            using (var db = new Solution_30shineEntities())
            {
                var list = db.Database.SqlQuery<cls_skillLevel>("select * from Tbl_SkillLevel where IsDelete != 1 and Publish = 1 order by [Order] asc").ToList();
                if (list.Count > 0 && OBJ != null && OBJ.SkillLevelIds != null && OBJ.SkillLevelIds != "")
                {
                    var skillLevelIs = db.Tbl_Payon.Where(w=>w.TypeStaffId == OBJ.Id && w.Hint == "base_salary").ToList();
                    var loop = 0;
                    foreach (var v in list)
                    {
                        var index = skillLevelIs.FindIndex(w=>w.ForeignId == v.Id);
                        if (index != -1)
                        {
                            list[loop++].isChecked = true;
                        }
                        else
                        {
                            list[loop++].isChecked = false;
                        }
                    }
                }
                rptSkillLevel.DataSource = list;
                rptSkillLevel.DataBind();
            }
        }

        public void ReloadPage()
        {
            HttpRequest Request = HttpContext.Current.Request;
            string host = "http://" + Request.Url.Host.ToString();
            string rawUrl = Request.RawUrl.ToString();
            string Url = host + rawUrl;
            Response.Redirect(Url);
        }

        /// <summary>
        /// Check update hay không
        /// </summary>
        /// <returns></returns>
        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }

        

        public class cls_skillLevel : Tbl_SkillLevel
        {
            public bool isChecked { get; set; }
        }
    }
}