﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Staff_Report_Fluctuations.aspx.cs" Inherits="_30shine.GUI.BackEnd.UIStaff.Staff_Report_Fluctuations" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Báo cáo biến động
    </title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            .customer-listing table.table-listing tbody tr:hover .edit-wp.display_permission {
                display: none !important;
            }

            .display_permission {
                display: none;
            }

            .edit-wp {
                position: absolute;
                right: 3px;
                top: 24%;
                background: #e7e7e7;
                display: none;
            }

                .edit-wp .elm {
                    width: 17px;
                    height: 20px;
                    float: left;
                    display: block;
                    background: url(/Assets/images/icon.delete.small.active.png?v=2);
                    margin-right: 30px;
                }

                    .edit-wp .elm:hover {
                        background: url(/Assets/images/icon.delete.small.png?v=2);
                    }

            .wp_time_booking {
                width: 100%;
                float: left;
                padding-left: 55px;
                margin: 5px 0px 15px;
            }

                .wp_time_booking .a_time_booking {
                    float: left;
                    height: 26px;
                    line-height: 26px;
                    padding: 0px 15px;
                    background: #dfdfdf;
                    margin-right: 10px;
                    cursor: pointer;
                    position: relative;
                    font-size: 13px;
                    -webkit-border-radius: 15px;
                    -moz-border-radius: 15px;
                    border-radius: 15px;
                }

                    .wp_time_booking .a_time_booking:hover, .wp_time_booking .a_time_booking.active {
                        background: #fcd344;
                        color: #000;
                    }

                    .wp_time_booking .a_time_booking .span_time_booking {
                        position: absolute;
                        top: 24px;
                        left: 0;
                        width: 100%;
                        float: left;
                        text-align: center;
                        font-size: 13px;
                    }

            .edit-wp .elm {
                margin-right: 0;
            }

            table.table-total-report-1 {
                border-collapse: collapse;
            }

                table.table-total-report-1,
                table.table-total-report-1 th,
                table.table-total-report-1 td {
                    border: 1px solid black;
                }

                    table.table-total-report-1 td {
                        padding: 5px 15px;
                        text-align: right;
                    }

                        table.table-total-report-1 td:first-child {
                            text-align: left;
                        }

            .be-report table.table-listing td.cls_Red {
                background-color: red;
            }

            .be-report table.table-listing td.cls_Green {
                background-color: green;
            }
        </style>
        <%--<uc1:ReportMenu runat="server" id="ReportMenu" />--%>

        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Danh sách báo cáo biến động &nbsp;&#187; </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add customer-listing be-report be-report-timekeeping">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                <div class="row">
                    <div class="filter-item">
                        <strong class="st-head"><i class="fa fa-clock-o"></i>Thời gian</strong>
                        <div class="time-wp">
                            <asp:TextBox CssClass="txtDateTime st-head" ID="txtStartDate" placeholder="Từ ngày" Style="margin-left: 10px!important"
                                ClientIDMode="Static" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ValidateTime" ControlToValidate="txtStartDate" runat="server" CssClass="fb-cover-error" Text="Vui lòng chọn thời gian!"></asp:RequiredFieldValidator>
                        </div>
                        <strong class="st-head" style="margin-left: 10px;">
                            <i class="fa fa-arrow-circle-right" style="margin-right: 0; color: #D0D6D8;"></i>
                        </strong>
                        <asp:TextBox CssClass="txtDateTime st-head" ID="txtEndDate" placeholder="Đến ngày" Style="margin-left: 10px!important"
                            ClientIDMode="Static" runat="server"></asp:TextBox>
                        <br />
                    </div>
                    <div class="filter-item">
                        <strong class="st-head" style="margin-right: 5px;">Salon : </strong>
                        <asp:DropDownList ID="ddlSalon" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <strong class="st-head" style="margin-right: 5px;">Bộ phận : </strong>
                        <asp:DropDownList ID="ddlType" runat="server" ClientIDMode="Static" Style="width: 190px;"></asp:DropDownList>
                    </div>
                    <a id="ViewData" onclick="excPaging(1)" class="st-head btn-viewdata " href="javascript:void(0)">Xem dữ liệu
                    </a>
                </div>
                <div class="row">
                    <strong class="st-head"><i class="fa fa-file-text"></i>Danh sách báo cáo</strong>
                </div>
                <div class="wp customer-add customer-listing be-report">
                    <%-- Listing --%>
                    <div class="wp960 content-wp">
                        <asp:ScriptManager runat="server" ID="ScriptManager2"></asp:ScriptManager>
                        <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                            <ContentTemplate>
                                <div class="table-wp">
                                    <table class="table-add table-listing">
                                        <thead>
                                            <tr>
                                                <%if (ddlType.SelectedIndex == 0)
                                                    { %>
                                                <th>STT</th>
                                                <th>Tên Salon</th>
                                                <th>Tuyển ngoài</th>
                                                <th>Tuyển mới S4M</th>
                                                <th>Tuyển lại</th>
                                                <th>Nghỉ tạm thời</th>
                                                <th>Đã nghỉ</th>
                                                <th>Điều chuyển</th>
                                                <th>Đào tạo nâng cao</th>
                                                <th>Đào tạo lại</th>
                                                <th>Tổng biến động</th>
                                                <%} %>
                                                <%if (ddlType.SelectedIndex == 1)
                                                    { %>
                                                <th>STT</th>
                                                <th>Tên Salon</th>
                                                <th>Tuyển ngoài</th>
                                                <th>Tuyển mới S4M</th>
                                                <th>Nghỉ tạm thời</th>
                                                <th>Đã nghỉ</th>
                                                <th>Đào tạo lại</th>
                                                <th>Đào tạo nâng cao</th>
                                                <th>Đi làm lại</th>
                                                <th>Tuyển lại</th>
                                                <th>Điều chuyển</th>
                                                <th>Tổng biến động</th>
                                                <%} %>
                                                <%if (ddlType.SelectedIndex != 1 && ddlType.SelectedIndex != 0)
                                                    { %>
                                                <th>STT</th>
                                                <th>Tên Salon</th>
                                                <th>Tuyển ngoài</th>
                                                <th>Nghỉ tạm thời</th>
                                                <th>Đã nghỉ</th>
                                                <th>Đi làm lại</th>
                                                <th>Tuyển lại</th>
                                                <th>Điều chuyển</th>
                                                <th>Tổng biến động</th>
                                                <%} %>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="RptFluctuation" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <%if (ddlType.SelectedIndex == 0)
                                                            { %>
                                                        <td><%# Container.ItemIndex + 1%></td>
                                                        <td><%#Eval("SalonName") %></td>
                                                        <td><%#Eval("OutSource") %></td>
                                                        <td><%#Eval("S4mSource") %></td>
                                                        <td><%#Eval("AgainSource") %></td>
                                                        <td style="background-color: #f2f2f2"><%#Eval("tmpLeaveJob") %></td>
                                                        <td style="background-color: #f2f2f2"><%#Eval("leaveJob") %></td>
                                                        <td><%#Eval("numberTransfer") %></td>
                                                        <td style="background-color: #f2f2f2"><%#Eval("advancedTraining") %></td>
                                                        <td style="background-color: #f2f2f2"><%#Eval("againTraining") %></td>
                                                        <td class="<%#Eval("Color")%>"><%#Eval("iCount")%></td>
                                                        <%} %>
                                                        <%if (ddlType.SelectedIndex == 1)
                                                            { %>
                                                        <td><%# Container.ItemIndex + 1%></td>
                                                        <td><%#Eval("SalonName") %></td>
                                                        <td><%#Eval("OutSource") %></td>
                                                        <td><%#Eval("S4mSource") %></td>
                                                        <td style="background-color: #f2f2f2"><%#Eval("tmpLeaveJob") %></td>
                                                        <td style="background-color: #f2f2f2"><%#Eval("leaveJob") %></td>
                                                        <td style="background-color: #f2f2f2"><%#Eval("againTraining") %></td>
                                                        <td style="background-color: #f2f2f2"><%#Eval("advancedTraining")%></td>
                                                        <td><%#Eval("WorkBack")%></td>
                                                        <td><%#Eval("AgainSource")%></td>
                                                        <td><%#Eval("numberTransfer")%></td>
                                                        <td class="<%#Eval("Color")%>"><%#Eval("iCount")%></td>
                                                        <%} %>
                                                        <%if (ddlType.SelectedIndex != 1 && ddlType.SelectedIndex != 0)
                                                            { %>
                                                        <td><%# Container.ItemIndex + 1%></td>
                                                        <td><%#Eval("SalonName") %></td>
                                                        <td><%#Eval("OutSource") %></td>
                                                        <td style="background-color: #f2f2f2"><%#Eval("tmpLeaveJob") %></td>
                                                        <td style="background-color: #f2f2f2"><%#Eval("leaveJob") %></td>
                                                        <td><%#Eval("WorkBack")%></td>
                                                        <td><%#Eval("AgainSource")%></td>
                                                        <td><%#Eval("numberTransfer")%></td>
                                                        <td class="<%#Eval("Color")%>"><%#Eval("iCount")%></td>
                                                        <%} %>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                        <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
                    </div>
                    <%-- END Listing --%>
                </div>
            </div>
            <%-- END Listing --%>
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 190px !important;
                margin-top: 5px !important;
            }

            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
        <script type="text/ecmascript">
            jQuery(document).ready(function () {
                // Add active menu
                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) { }
                });
            });
            //======================================================
            function viewDataByDate(This, time) {
                $(".a_time_booking.active").removeClass("active");
                This.addClass("active");
                $("#txtStartDate").val(time);
                $("#txtEndDate").val(time);
                $("#ViewData").click();
            }
        </script>
    </asp:Panel>
</asp:Content>
