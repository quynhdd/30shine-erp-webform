﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LabourContractAdd.aspx.cs" Inherits="_30shine.GUI.BackEnd.UIStaff.LabourContractAdd" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="StaffAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý hợp đồng&nbsp;&#187; </li>
                        <li class="li-listing"><a href="/admin/nhan-vien/hop-dong/danh-sach.html">Danh sách </a></li>
                        <li class="li-add active"><a href="/admin/nhan-vien/hop-dong/them-moi.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="table-wp">
                    <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                    <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                        <ContentTemplate>
                            <table class="table-add admin-product-table-add">
                                <tbody>
                                    <%--<tr class="title-head">
                                        <td><strong>Thêm hợp đồng</strong></td>
                                        <td></td>
                                    </tr>--%>
                                    <tr>
                                        <td class="col-xs-2 left"><span>Nhập tên hợp đồng<i style="color: red">&nbsp*</i></span></td>
                                        <td class="col-xs-9 right">
                                            <span class="field-wp">
                                                <asp:TextBox ClientIDMode="Static" ID="txtNameContract" CssClass="form-control" runat="server"></asp:TextBox>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-xs-3 left"><span>Bộ phận</span></td>
                                        <td class="col-xs-9 right">
                                            <div class="div_col">
                                                <asp:DropDownList ID="ddlTypeStaff" runat="server" CssClass="form-control" ClientIDMode="Static"></asp:DropDownList>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-xs-3 left"><span>Loại hợp đồng</span></td>
                                        <td class="col-xs-9 right">
                                            <div class="div_col">
                                                <asp:DropDownList ID="ddlContractType" runat="server" ClientIDMode="Static" CssClass="form-control"> </asp:DropDownList>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-xs-2 left" style="height: 150px;"><span>Mô tả</span></td>
                                        <td class="col-xs-9 right" style="height: 150px;">
                                            <span class="field-wp" style="height: 100%">
                                                <asp:TextBox Height="100%" ID="txtDescripton" CssClass="form-control" TextMode="MultiLine" runat="server" ClientIDMode="Static"></asp:TextBox>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-xs-2 left"><span>Mẫu hợp đồng</span></td>
                                        <td class="col-xs-9 right">
                                            <span class="field-wp" style="margin-bottom:10px;">
                                                <asp:TextBox ClientIDMode="Static" CssClass="form-control" ID="txtPathTempContract" runat="server"></asp:TextBox>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="col-xs-2 left"><span>Tải file</span></td>
                                        <td class="col-xs-9 right">
                                            <span style="width:55%;">
                                                <input id="getFile" type="file" accept=".docx" onchange="changeUpFile(this);" />
                                            </span>
                                            <span style="width:30%;">
                                                <input type="submit" class ="btn btn-complete" value="Upload File" onclick="UploadFile()" />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="tr-send">
                                        <td class="col-xs-2 left"></td>
                                        <td class="col-xs-9 right no-border">
                                            <span class="field-wp"">
                                                <asp:Button ID="Send" OnClientClick="checkInput()" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="AddContract"></asp:Button>
                                            </span>
                                        </td>
                                    </tr>
                                    <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                                </tbody>
                            </table>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="Send" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
            <script>
                var reader = new FileReader();
                $(document).ready(function () {
                    
                    $("#txtNameContract").bind("click", function () {
                        $('#txtNameContract').css("border-color", "");
                        $('#MsgSystem').text("");
                    });
                    $("#ddlTypeStaff").bind("click", function () {
                        $('#ddlTypeStaff').css("border-color", "");
                        $('#MsgSystem').text("");
                    });
                    $("#ddlContractType").bind("click", function () {
                        $('#ddlContractType').css("border-color", "");
                        $('#MsgSystem').text("");
                    });
                    $("#txtContent").bind("click", function () {
                        $('#txtContent').css("border-color", "");
                        $('#MsgSystem').text("");
                    });
                    $("#txtPathTempContract").bind("click", function () {
                        $('#txtPathTempContract').css("border-color", "");
                        $('#MsgSystem').text("");
                    });
                    $("#txtDescripton").bind("click", function () {
                        $('#txtDescripton').css("border-color", "");
                        $('#MsgSystem').text("");
                    });

                });
                function checkInput() {
                    var error = true;
                    var contractName = $("#txtNameContract").val();
                    var contractType = $("#ddlContractType").val();
                    var content = $("#txtContent").val();
                    var pathContract = $("#txtPathTempContract").val();
                    if (contractName == "" || contractType == "0" || content == "" || pathContract == "" ||
                        contractName == "undefined" || contractType == "undefined") {
                        $('#txtNameContract').css("border-color", "red");
                        $('#ddlContractType').css("border-color", "red");
                        $('#txtContent').css("border-color", "red");
                        $('#txtPathTempContract').css("border-color", "red");
                        ShowWarning("Chưa nhập đủ trường");
                        event.preventDefault();
                        return false;
                    }
                }
                function ShowWarning(message) {
                    showMsgSystem(message, "msg-system success", 15000);
                }
                function UploadFile() {
                    var departmentId = $("#ddlTypeStaff").val();
                    var contractType = $("#ddlContractType").val();
                    var data = {
                        base64: reader.result,
                        department: departmentId,
                        contractType: contractType
                    };
                    $.ajax({
                        type: "POST",
                        url: "/GUI/BackEnd/Staff/LabourContractAdd.aspx/SaveContractToFoldel",
                        async: false,
                        data: JSON.stringify(data),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            if (response.d.success) {
                                ShowWarning(response.d.msg);
                                $("#txtPathTempContract").val(response.d.data);
                            }
                            else {
                                ShowWarning(response.d.msg);
                                $("#getFile").val('');
                                $("#txtPathTempContract").val("");
                            }
                        },
                        error: function (objData, error) {
                            ShowWarning('Có lỗi xảy ra ! Vui lòng liên hệ với nhóm phát triển !');
                            $("#getFile").val('');
                            $("#txtPathTempContract").val("");
                        }
                    });
                }
                function changeUpFile(input) {
                    var contractType = $("#ddlContractType").val();
                    var departmentId = $("#ddlTypeStaff").val();
                    $("#txtPathTempContract").val("");
                    if (contractType != "" && departmentId != "") {
                        if (input.files && input.files[0]) {
                            reader.readAsDataURL(input.files[0]);
                        }
                        else {
                            ShowWarning("Chưa tìm thấy file!");
                        }
                    }
                    else {
                        ShowWarning("Chưa chọn bộ phận hoặc loại hợp đồng");
                        $('#ddlContractType').css("border-color", "red");
                        $("#ddlTypeStaff").css("border-color", "red");
                        $("#getFile").val('');
                    }
                }
            </script>
        </div>
    </asp:Panel>
</asp:Content>
