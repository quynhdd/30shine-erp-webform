﻿using _30shine.Helpers;
using _30shine.MODEL.ENTITY.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Entity.Migrations;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.UIStaff
{
    public partial class Finger_Add : System.Web.UI.Page
    {
        private string PageID = "NV_GVT";
        public UIHelpers _UIHelpers = new UIHelpers();
        private bool Perm_Access = false;
        private bool Perm_AllPermission = false;

        private int FP_CustomerId = 0;
        private string FP_token = "";
        private string FP_template = "";
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                ////Perm_Add = permissionModel.GetActionByActionNameAndPageId("Perm_Add", PageID, permission);
                //Perm_AllPermission = permissionModel.GetActionByActionNameAndPageId("Perm_AllPermission", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_AllPermission = permissionModel.CheckPermisionByAction("Perm_AllPermission", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }



        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
            if (!Perm_AllPermission)
            {
                TrSalon.Visible = false;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { Salon }, Perm_AllPermission);
                Bind_Staffs();

                // Bind customer by finger print
                Bind_Customer_ByFingerPrint();
                Assign_FP_ToCustomer();
            }
        }

        protected void Add_Finger(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                FP_token = Session["FP_Token"] != null ? Session["FP_Token"].ToString() : "";
                FP_template = Session["FP_Template"] != null ? Session["FP_Template"].ToString() : "";
                int integer;
                var StaffId = int.TryParse(HDF_StaffId.Value, out integer) ? integer : 0;
                if (StaffId > 0)
                {
                    var obj = db.Staffs.FirstOrDefault(w => w.Id == StaffId);
                    if (obj != null)
                    {
                        obj.FingerToken = FP_token;
                        obj.FingerTemplate = FP_template;
                        db.Staffs.AddOrUpdate(obj);
                        var exc = db.SaveChanges();

                        if (exc > 0)
                        {
                            var obj2 = new Staff_Roll();
                            obj2.StaffId = obj.Id;
                            obj2.CreatedDate = DateTime.Now;
                            db.Staff_Roll.AddOrUpdate(obj2);
                            db.SaveChanges();
                            UIHelpers.Redirect("/admin/nhan-vien.html");
                        }
                    }
                }
            }
        }

        private void Bind_Staffs()
        {
            using (var db = new Solution_30shineEntities())
            {
                var staffs = new List<_30shine.MODEL.ENTITY.EDMX.Staff>();
                var SalonId = Session["SalonId"] != null ? Convert.ToInt32(Session["SalonId"]) : 0;
                if (SalonId > 0)
                {
                    staffs = db.Staffs.Where(w => w.IsDelete != 1 && w.SalonId == SalonId).OrderBy(o => o.Id).ToList();
                }
                else
                {
                    staffs = db.Staffs.Where(w => w.IsDelete != 1).OrderBy(o => o.Id).ToList();
                }
                Rpt_Staff.DataSource = staffs;
                Rpt_Staff.DataBind();
            }
        }

        private void Bind_Customer_ByFingerPrint()
        {
            int integer;
            FP_CustomerId = int.TryParse(Request.QueryString["ctmId"], out integer) ? integer : 0;
            if (FP_CustomerId > 0)
            {
                using (var db = new Solution_30shineEntities())
                {
                    var staff = db.Staffs.FirstOrDefault(w => w.Id == FP_CustomerId);
                    if (staff != null)
                    {
                        //CustomerName.Text = customer.Fullname;
                        //CustomerCode.Text = customer.Customer_Code;
                        //HDF_CustomerCode.Value = customer.Customer_Code;
                    }
                }
            }
        }

        private void Assign_FP_ToCustomer()
        {
            Session["FP_Token"] = Request.QueryString["fgToken"] != null ? Request.QueryString["fgToken"] : "";
            Session["FP_Template"] = Request.QueryString["fgTemplate"] != null ? Request.QueryString["fgTemplate"] : "";
            if (Session["FP_Token"] == null)
            {
                FP_token = Request.QueryString["fgToken"];
                FP_template = Request.QueryString["fgTemplate"];
                if (FP_token != null && FP_token != "")
                {
                    using (var db = new Solution_30shineEntities())
                    {
                        var staffByFP = db.Staffs.FirstOrDefault(w => w.FingerToken == FP_token);
                        if (staffByFP != null)
                        {
                            Session["FP_Token"] = FP_token;
                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Open Add Customer", "$('.addnew-customer').click();", true);
                        }
                    }
                }
            }
        }

        

       
    }
}