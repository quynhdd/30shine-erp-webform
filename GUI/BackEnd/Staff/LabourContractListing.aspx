﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LabourContractListing.aspx.cs" Inherits="_30shine.GUI.BackEnd.UIStaff.LabourContractListing" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="StaffAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý hợp đồng &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/admin/nhan-vien/hop-dong/danh-sach.html">Danh sách </a></li>
                        <li class="li-add active"><a href="/admin/nhan-vien/hop-dong/them-moi.html">Thêm mới</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="wp customer-add customer-listing be-report">
            <%-- Listing --%>
            <div class="wp960 content-wp">
                 <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <div class="row row-filter">
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlContractType" runat="server" ClientIDMode="Static" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                    <div class="filter-item">
                        <asp:DropDownList ID="ddlTypeStaff" runat="server" CssClass="form-control" ClientIDMode="Static"></asp:DropDownList>
                    </div>
                    <asp:Panel ID="ViewDataFilter" CssClass="st-head btn-viewdata" ClientIDMode="Static"
                        onclick="excPaging(1)" runat="server">
                        Xem dữ liệu
                    </asp:Panel>
                </div>
                <!-- Row Table Filter -->
                <div class="table-func-panel">
                    <div class="table-func-elm">
                        <span>Số hàng / Page : </span>
                        <div class="table-func-input-wp">
                            <span class="table-func-input opt-segment" onclick="ShowUlOptSegment($(this))" id="OPTSegment">10</span>
                            <i class="fa fa-caret-down" onclick="ShowUlOptSegment($(this))"></i>
                            <ul class="ul-opt-segment">
                                <li data-value="10">10</li>
                                <li data-value="20">20</li>
                                <li data-value="30">30</li>
                                <li data-value="40">40</li>
                                <li data-value="50">50</li>
                                <li data-value="1000000">Tất cả</li>
                            </ul>
                            <asp:HiddenField ID="HDF_OPTSegment" ClientIDMode="Static" runat="server" />
                        </div>
                    </div>
                </div>
                <!-- End Row Table Filter -->
                <asp:ScriptManager runat="server" ID="SM01"></asp:ScriptManager>
                <asp:UpdatePanel ID="UPTableListing" runat="server" ClientIDMode="Static">
                    <ContentTemplate>
                        <div class="table-wp">
                            <table class="table-add table-listing">
                                <thead>
                                    <tr>
                                        <th>STT</th>
                                        <th>Tên hợp đồng</th>
                                        <th>Bộ phận</th>
                                        <th>Loại hợp đồng</th>
                                        <th>Đường dẫn</th>
                                        <th>Mô tả</th>
                                        <th>Chức năng</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="Rpt" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 + PAGING._Offset %></td>
                                                <td><%# Eval("Name") %></td>
                                                <td><%# Eval("DepartmentName") %></td>
                                                <td><%# Eval("ContractNameType") %></td>
                                                <td><%# Eval("PathContract") %></td>
                                                <td><%# Eval("DescriptionContract") %></td>
                                                <td class="map-edit">
                                                    <div class="edit-wp">
                                                        <a class="elm edit-btn" href="/admin/nhan-vien/hop-dong/<%# Eval("Id") %>.html" title="Sửa"></a>
                                                        <a class="elm del-btn" onclick="del(this.parentNode.parentNode.parentNode,'<%# Eval("Id") %>', '<%# Eval("Name") %>')" href="javascript://" title="Xóa"></a>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <!-- Paging -->
                        <div class="site-paging-wp">
                            <% if (PAGING.TotalPage > 1)
                                { %>
                            <asp:Panel CssClass="site-paging" ID="SitePaging" runat="server" ClientIDMode="Static" data-page="0">
                                <% if (PAGING._Paging.Prev != 0)
                                    { %>
                                <a href="javascript://" onclick="excPaging(1)">Đầu</a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Prev %>)"><</a>
                                <% } %>
                                <asp:Repeater ID="RptPaging" runat="server">
                                    <ItemTemplate>
                                        <a href="javascript://" onclick="excPaging(<%# Eval("PageNum") %>)"
                                            <%# Convert.ToBoolean(Eval("PageActive")) == true ? "class='active'" :  ""%>>
                                            <%# Eval("PageNum") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (PAGING._Paging.Next != PAGING.TotalPage + 1)
                                    { %>
                                <a href="javascript://" onclick="excPaging(<%=PAGING._Paging.Next %>)">></a>
                                <a href="javascript://" onclick="excPaging(<%=PAGING.TotalPage %>)">Cuối</a>
                                <% } %>
                            </asp:Panel>
                            <% } %>
                        </div>
                        <!-- End Paging -->
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BtnFakeUP" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="BtnFakeUP" ClientIDMode="Static" runat="server" OnClick="_BtnClick" Text="Click" Style="display: none;" />
                <asp:HiddenField runat="server" ClientIDMode="Static" ID="HDF_Page" />
            </div>
            <script>
                $(document).ready(function () {

                })

                function del(This, code, name) {
                    var code = code || null,
                        name = name || null,
                        Row = This;
                    if (!code) return false;
                    // show EBPopup
                    $(".confirm-yn").openEBPopup();
                    $("#EBPopup .confirm-yn-text").text("Bạn có chắc chắn muốn xóa [ " + name + " ] ?");

                    $("#EBPopup .yn-yes").bind("click", function () {
                        var data = { Id: code }
                        $.ajax({
                            type: "POST",
                            url: "/GUI/BackEnd/Staff/LabourContractListing.aspx/DeleteContract",
                            async: false,
                            data: JSON.stringify(data),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (response) {
                                if (response.d.success) {
                                    delSuccess();
                                    Row.remove();
                                    ShowWarning(response.d.msg);
                                }
                                else {
                                    delFailed();
                                    ShowWarning(response.d.msg);
                                }
                            },
                            error: function (objData, error) {
                                ShowWarning('Có lỗi xảy ra ! Vui lòng liên hệ với nhóm phát triển !');
                            }
                        });
                    });

                    $("#EBPopup .yn-no").bind("click", function () {
                        autoCloseEBPopup(0);
                    });
                }
                function ShowWarning(message) {
                    $('#MsgSystem').text(message);
                    $('#MsgSystem').css("color", "red");
                }
            </script>
        </div>
    </asp:Panel>
</asp:Content>