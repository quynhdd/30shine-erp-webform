﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Staff_Type_Add.aspx.cs" Inherits="_30shine.GUI.BackEnd.UIStaff.Staff_Type_Add" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="ProductAdd" ContentPlaceHolderID="CtMain" runat="server">
<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">      
                <li>Quản lý bộ phận &nbsp;&#187; </li>
                <li class="li-listing"><a href="/admin/kieu-nhan-vien.html">Danh sách</a></li>
                <% if(_IsUpdate){ %>
                    <li class="li-add"><a href="/admin/kieu-nhan-vien/them-moi.html">Thêm mới</a></li>
                    <li class="li-edit active"><a href="/admin/kieu-nhan-vien/<%= _Code %>.html">Cập nhật</a></li>
                <% }else{ %>
                    <li class="li-add active"><a href="/admin/kieu-nhan-vien/them-moi.html">Thêm mới</a></li>
                <% } %>
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add">
    <%-- Add --%>                
    <div class="wp960 content-wp">
        <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
            <div class="table-wp">
                <table class="table-add admin-product-table-add">
                    <tbody>
                        <tr class="title-head">
                            <td><strong>Thông tin bộ phận</strong></td>
                            <td></td>
                        </tr>
                        <tr class="tr-margin" style="height: 20px;"></tr>
                        <tr>
                            <td class="col-xs-2 left"><span>Bộ phận</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:TextBox ID="Name" runat="server" ClientIDMode="Static"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="NameValidate" ControlToValidate="Name" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập tên danh mục!"></asp:RequiredFieldValidator>
                                </span>
                                            
                            </td>
                        </tr>                   

                        <tr class="tr-description">
                            <td class="col-xs-2 left"><span>Mô tả</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:TextBox TextMode="MultiLine" Rows="5" ID="Description" runat="server" ClientIDMode="Static"></asp:TextBox>
                                </span>
                            </td>
                        </tr>

                        <tr class="tr-field-ahalf">
                            <td class="col-xs-2 left"><span>Thứ tự sắp xếp</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:TextBox ID="_Order" runat="server" ClientIDMode="Static"
                                        placeholder="Ví dụ : 1"></asp:TextBox>
                                </span>
                            </td>
                        </tr>

                        <tr class="tr-field-ahalf">
                            <td class="col-xs-2 left"><span>Gán bậc kỹ năng</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp" id="skillLevelList">
                                    <asp:Repeater runat="server" ID="rptSkillLevel" ClientIDMode="Static">
                                        <ItemTemplate>
                                            <label class="lbl-cus-no-infor" style="margin-right: 5px; margin-bottom: 8px; cursor: pointer; font-weight: normal; font-size: 13px; padding: 2px 10px; background: #ddd; line-height: 21px; float: left;">
                                                <% if (_IsUpdate)
                                                    { %>
                                                    <input class="ip-skill-level" type="checkbox" value="<%# Eval("Id") %>" <%# Convert.ToBoolean(Eval("isChecked")) == true ? "checked='checked'" : "" %> style="position: relative; top: 2px;" onclick="getSkillLevel()" />
                                                    <%# Eval("Name") %>
                                                <% }
                                                else
                                                { %>
                                                    <input class="ip-skill-level" type="checkbox" value="<%# Eval("Id") %>" <%# Container.ItemIndex == 0 ? "checked='checked'" : "" %> style="position: relative; top: 2px;" onclick="getSkillLevel()" />
                                                    <%# Eval("Name") %>
                                                <% } %>
                                            </label>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </span>
                            </td>
                        </tr>

                        <tr class="tr-product-category">
                            <td class="col-xs-2 left"><span style="line-height: 18px;margin-top: 15px;">Áp dụng bill đặc biệt (tính rating)</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp" style="margin-top: 15px;">
                                    <asp:CheckBox ID="IsSpecialBillRating" Checked="false" runat="server" />
                                </span>
                            </td>
                        </tr>

                        <tr class="tr-product-category">
                            <td class="col-xs-2 left"><span>Publish</span></td>
                            <td class="col-xs-9 right">
                                <span class="field-wp">
                                    <asp:CheckBox ID="Publish" Checked="true" runat="server" />
                                </span>
                            </td>
                        </tr>
                    
                        <tr class="tr-send">
                            <td class="col-xs-2 left"></td>
                            <td class="col-xs-9 right no-border">
                                <span class="field-wp">
                                    <asp:Button ID="Send" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="ExcAddOrUpdate"></asp:Button>
                                    <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>
                                </span>
                            </td>
                        </tr>
                        <asp:HiddenField runat="server" ID="UrlKeyField"  ClientIDMode="Static"/>
                        <asp:HiddenField runat="server" ID="ImgCoverField"  ClientIDMode="Static"/>
                        <asp:HiddenField runat="server" ID="HDF_SkillLevelArray"  ClientIDMode="Static"/>
                    </tbody>
                </table>
            </div>
        </div>
    <%-- end Add --%>
</div>

<script>
    jQuery(document).ready(function () {
        $("#glbAdminContent").addClass("active");
        $("#glbAdminStaffType").addClass("active");
        //============================
        // Show System Message
        //============================ 
        var qs = getQueryStrings();
        showMsgSystem(qs["msg_update_message"], qs["msg_update_status"]);

        getSkillLevel();
    });
        
    function getSkillLevel() {
        var skillLevelArray = [];
        $("#skillLevelList input.ip-skill-level:checked").each(function () {
            skillLevelArray.push($(this).val());
        });
        $("#HDF_SkillLevelArray").val(skillLevelArray);
    }
</script>

</asp:Panel>
</asp:Content>

