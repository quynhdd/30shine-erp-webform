﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Script.Serialization;
using Library;
using System.Web.Services;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.MODEL.BO;
using _30shine.MODEL.IO;
using System.Text.RegularExpressions;
using System.Reflection;
using _30shine.Helpers;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.StaffController
{
    public partial class UpdateInfoFromExcel : System.Web.UI.Page
    {
        public JavaScriptSerializer Serializer;
        public Class.AjaxResponse AjaxResponse;
        public Solution_30shineEntities db;
        IStaffTypeModel staffTypeModel = new StaffTypeModel();
        ISalonModel salonModel = new SalonModel();
        private string PageID = "UPDATE_EXCELL";
        private bool Perm_Access = false;

        public UpdateInfoFromExcel()
        {
            Serializer = new JavaScriptSerializer();
            AjaxResponse = new Class.AjaxResponse();
            db = new Solution_30shineEntities();
        }

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }


        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
        }

        [WebMethod]
        public static string UploadFile(List<DataStaffFromExcel> DataFromExcel)
        {
            var instance = Instance;
            instance.AjaxResponse.Reset();

            try
            {
                DataFromExcel.ForEach(delegate (DataStaffFromExcel Data)
                {
                    var invalidData = false;
                    var staff = StaffModel.Instance.GetByOrderCode(Data.Code);
                    Data.TrimProperties();

                    if (staff == null)
                    {
                        instance.MarkError(Data, "Code");
                        staff = new Staff();
                        invalidData = true;
                    }

                    // Salon
                    var salon = SalonModel.Instance.GetByShortName(Data.Salon);
                    if (Data.Salon.Length > 0 && salon != null)
                    {
                        staff.SalonId = salon.Id;
                    }
                    else
                    {
                        instance.MarkError(Data, "Salon");
                        invalidData = true;
                    }

                    // So TK ngan hang
                    if (Data.SoTaiKhoan.All(Char.IsDigit))
                    {
                        staff.NganHang_SoTK = Data.SoTaiKhoan;
                    }
                    // Ten TK ngan hang
                    if (Data.TenNganHang.Length > 0)
                    {
                        staff.NganHang_TenTK = Data.TenNganHang;
                    }

                    // So dien thoai
                    if (Data.DienThoai.Length > 0)
                    {
                        Data.DienThoai = Regex.Replace(Data.DienThoai, @"[^\d]", "");
                        if (Data.DienThoai.Length > 0)
                        {
                            staff.Phone = Data.DienThoai;
                        }
                        else
                        {
                            instance.MarkError(Data, "DienThoai");
                        }
                    }

                    // Chuc vu nhan vien
                    if (Data.ChucVu.Length > 0)
                    {
                        var staffType = StaffTypeModel.Instance.GetByMeta(Data.ChucVu);
                        if (staffType != null)
                        {
                            staff.Type = staffType.Id;
                        }
                        else
                        {
                            instance.MarkError(Data, "ChucVu");
                        }
                    }

                    // Skill Level
                    if (Data.Level.Length > 0)
                    {
                        var skillLevel = SkillLevelModel.Instance.GetByName(Data.Level);
                        if (skillLevel != null)
                        {
                            staff.SkillLevel = skillLevel.Id;
                        }
                        else
                        {
                            instance.MarkError(Data, "Level");
                        }
                    }

                    // Bao hiem xa hoi
                    if (Data.BHXH.Length > 0)
                    {
                        staff.NumberInsurrance = Data.BHXH;
                    }

                    // Ma so thue ca nhan
                    if (Data.MSTCN.Length > 0)
                    {
                        staff.MST = Regex.Replace(Data.MSTCN, @"[^\d]", "");
                    }

                    // Chung minh nhan dan | The can cuoc
                    if (Data.CMND_TheCanCuoc.Length > 0)
                    {
                        staff.StaffID = Regex.Replace(Data.CMND_TheCanCuoc, @"[^\d]", "");
                    }

                    // CMND_NgayCap
                    if (Data.CMND_NgayCap.Length > 0)
                    {
                        try
                        {
                            var IDProvideDate = Format.getDateTimeFromString(Data.CMND_NgayCap);
                            staff.IDProvidedDate = IDProvideDate;
                        }
                        catch (Exception)
                        {
                            instance.MarkError(Data, "CMND_NgayCap");
                        }
                    }

                    // CMND_NoiCap
                    if (Data.CMND_NoiCap.Length > 0)
                    {
                        staff.IDProviderLocale = Data.CMND_NoiCap;
                    }

                    // DiaChiThuongChu
                    if (Data.DiaChiThuongChu.Length > 0)
                    {
                        staff.IDHomeTown = Data.DiaChiThuongChu;
                        staff.Address = Data.DiaChiThuongChu;
                    }

                    // Ngay sinh
                    if (Data.BirthDate.Length > 0)
                    {
                        try
                        {
                            var BirthDay = Format.getDateTimeFromString(Data.BirthDate);
                            staff.BirthDay = BirthDay;
                        }
                        catch (Exception)
                        {
                            instance.MarkError(Data, "BirthDate");
                        }
                    }

                    if (Data.ThamNien.Length > 0)
                    {
                        try
                        {
                            var ThamNien = Format.getDateTimeFromString(Data.ThamNien);
                            staff.NgayTinhThamNien = ThamNien;
                        }
                        catch (Exception)
                        {
                            instance.MarkError(Data, "ThamNien");
                        }
                    }

                    if (Data.NgayBatDauVao.Length > 0)
                    {
                        try
                        {
                            var ngayBatDauVao = Format.getDateTimeFromString(Data.NgayBatDauVao);
                            staff.DateJoin = ngayBatDauVao;
                        }
                        catch (Exception)
                        {
                            instance.MarkError(Data, "NgayBatDauVao");
                        }
                    }

                    // OrderCode
                    //staff.Code = Data.Code;
                    staff.OrderCode = Data.Code;
                    // Ten lam viec
                    if (Data.TenLamViec.Length > 0)
                    {
                        staff.Fullname = Data.TenLamViec;
                    }

                    // Ten theo CMND|The can cuoc
                    if (Data.Name.Length > 0)
                    {
                        staff.NameCMT = Data.Name;
                    }

                    // Update thông tin Nhan vien
                    if (invalidData != true)
                    {
                        instance.db.Staffs.AddOrUpdate(staff);
                        instance.db.SaveChanges();
                    }
                });

                instance.AjaxResponse.success = true;
            }
            catch (Exception Ex)
            {
                instance.AjaxResponse.success = false;
                instance.AjaxResponse.msg = "Có lỗi xảy ra, vui lòng thử lại! {" + Ex.Message + "}";
            }

            return instance.Serializer.Serialize(instance.AjaxResponse);
        }

        /**
         * Get ServiceRatingModel Instance
         * @return ServiceRatingModel
         **/
        public static UpdateInfoFromExcel Instance
        {
            get
            {
                return new UpdateInfoFromExcel();
            }
        }

        public void MarkError(DataStaffFromExcel Data, string FieldIndex)
        {
            Data.FieldGetError.Add(FieldIndex);
            if (AjaxResponse.data.Where(w => w.Code == Data.Code).ToList().Count() < 1)
            {
                AjaxResponse.data.Add(Data);
            }

        }

        public int IndexToInt(DataStaffFromExcel data, string index)
        {
            int c = 0;
            int indexInt = 0;

            foreach (PropertyInfo i in data.GetType().GetProperties())
            {
                if (index == i.Name)
                {
                    indexInt = c;
                    break;
                }
                c++;
            }

            return indexInt;
        }

        public class DataStaffFromExcel
        {
            public int STT { get; set; }
            public string Name { get; set; }
            public string TenLamViec { get; set; }
            public string Salon { get; set; }
            public string Code { get; set; }
            public string TenNganHang { get; set; }
            public string SoTaiKhoan { get; set; }
            public string NgayBatDauVao { get; set; }
            public string ThamNien { get; set; }
            public string ChucVu { get; set; }
            public string Level { get; set; }
            public string BHXH { get; set; }
            public string MSTCN { get; set; }
            public string HSLD { get; set; }
            public string BirthDate { get; set; }
            public string SexCode { get; set; }
            public string FolkName { get; set; }
            public string FolkCode { get; set; }
            public string NationalName { get; set; }
            public string NationalCode { get; set; }
            public string DienThoai { get; set; }
            public string CMND_TheCanCuoc { get; set; }
            public string CMND_NgayCap { get; set; }
            public string CMND_NoiCap { get; set; }
            public string DiaChiThuongChu { get; set; }
            public List<string> FieldGetError { get; set; }

            public DataStaffFromExcel()
            {
                STT = 0;
                Name = "";
                TenLamViec = "";
                Salon = "";
                Code = "";
                TenNganHang = "";
                SoTaiKhoan = "";
                ThamNien = "";
                NgayBatDauVao = "";
                ChucVu = "";
                Level = "";
                BHXH = "";
                MSTCN = "";
                HSLD = "";
                BirthDate = "";
                SexCode = "";
                FolkCode = "";
                FolkName = "";
                NationalCode = "";
                NationalName = "";
                DienThoai = "";
                CMND_TheCanCuoc = "";
                CMND_NgayCap = "";
                CMND_NoiCap = "";
                DiaChiThuongChu = "";
                FieldGetError = new List<string>();
            }

            public void TrimProperties()
            {
                Name = Name.Trim();
                TenLamViec = TenLamViec.Trim();
                Salon = Salon.Trim();
                Code = Code.Trim();
                TenNganHang = TenNganHang.Trim();
                SoTaiKhoan = SoTaiKhoan.Trim();
                ThamNien = ThamNien.Trim();
                NgayBatDauVao = NgayBatDauVao.Trim();
                ChucVu = ChucVu.Trim();
                Level = Level.Trim();
                BHXH = BHXH.Trim();
                MSTCN = MSTCN.Trim();
                HSLD = HSLD.Trim();
                BirthDate = BirthDate.Trim();
                SexCode = SexCode.Trim();
                FolkCode = FolkCode.Trim();
                FolkName = FolkName.Trim();
                NationalCode = NationalCode.Trim();
                NationalName = NationalName.Trim();
                DienThoai = DienThoai.Trim();
                CMND_TheCanCuoc = CMND_TheCanCuoc.Trim();
                CMND_NgayCap = CMND_NgayCap.Trim();
                CMND_NoiCap = CMND_NoiCap.Trim();
                DiaChiThuongChu = DiaChiThuongChu.Trim();
            }
        }
    }
}