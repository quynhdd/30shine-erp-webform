﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using LinqKit;
using System.Linq.Expressions;
using ExportToExcel;
using System.Data.SqlClient;
using _30shine.MODEL.BO;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.HRM
{
    public partial class Stylist_NangSuatTB : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();

        private int _TypeStaff;
        private int _StaffId;

        protected bool Staff_All = false;
        protected bool Staff_Hairdresser = false;
        protected bool Staff_HairMassage = false;
        protected bool Staff_Singer = false;
        protected string THead = "";
        public CultureInfo culture = new CultureInfo("vi-VN");
        private DateTime d1 = new DateTime();
        private DateTime d2 = new DateTime();
        public DateTime timeFrom;
        public DateTime timeTo;
        private Expression<Func<Staff, bool>> WhereStaff = PredicateBuilder.True<Staff>();
        private List<Service> _Service = new List<Service>();
        private List<Product> _Product = new List<Product>();
        protected string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
        protected string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);
        protected int ThisMon;// = DateTime.Now.Month;
        protected int DayOfMon;// = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
        protected int ConfigWorkDay = 0;
        protected cls_nstb_item TotalReport = new cls_nstb_item();
        protected List<cls_nstb_item> listData = new List<cls_nstb_item>();

        private string PageID = "BC_NS_STYLIST";
        private bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected bool isRoot = false;
        public double? kcs;
        public int totalk;
        public double totalKcs;
        public int totalMist;

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_Delete = permissionModel.GetActionByActionNameAndPageId("Perm_Delete", PageID, permission);
                //Perm_ViewAllData = permissionModel.GetActionByActionNameAndPageId("Perm_ViewAllData", PageID, permission);
                //////Perm_Add = permissionModel.GetActionByActionNameAndPageId("Perm_Add", PageID, permission);
                ////Perm_Edit = permissionModel.GetActionByActionNameAndPageId("Perm_Edit", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }
        /// <summary>
        /// Constructor
        /// </summary>
        public Stylist_NangSuatTB()
        {
            TotalReport.ListItemServices = new List<cls_service_item>();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                TxtDateTimeFrom.Text = FirstDayOfMonth;
                TxtDateTimeTo.Text = Today;
                Library.Function.bindSalonSpecial(new List<DropDownList> { Salon }, Perm_ViewAllData);
                bindStylist();
            }
            RemoveLoading();
        }

        /// <summary>
        /// Truy vấn data từ DB
        /// </summary>
        /// <returns></returns>
        private List<cls_Store_HRM_ThongKeNangSuatStylist> getDataFromStoreSQL()
        {
            try
            {
                int integer;
                int staffId = int.TryParse(ddlStaff.SelectedValue, out integer) ? integer : 0;
                int SalonId = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
                if (SalonId == 0)
                {
                    throw new Exception("Error SalonId = 0.");
                }

                var timeFrom = Library.Format.getDateTimeFromString(TxtDateTimeFrom.Text);
                var timeTo = Library.Format.getDateTimeFromString(TxtDateTimeTo.Text);
                if (timeFrom == null)
                {
                    throw new Exception("Error null.");
                }
                if (timeTo == null)
                {
                    timeTo = timeFrom.Value.AddDays(1);
                }
                else
                {
                    timeTo = timeTo.Value.AddDays(1);
                }

                object[] paras =
                {
                    new SqlParameter("@TimeFrom", timeFrom.Value),
                    new SqlParameter("@TimeTo", timeTo.Value),
                    new SqlParameter("@TimeTo2", timeTo.Value.AddDays(-1)),
                    new SqlParameter("@SalonId", SalonId),
                    new SqlParameter("@StylistId", staffId)
                };
                var list = new List<cls_Store_HRM_ThongKeNangSuatStylist>();
                using (var db = new Solution_30shineEntities())
                {
                    var staffModel = new StaffModel();
                    list = db.Database.SqlQuery<cls_Store_HRM_ThongKeNangSuatStylist>("Store_HRM_ThongKeNangSuatStylist @TimeFrom, @TimeTo, @TimeTo2, @SalonId, @StylistId", paras).ToList();
                    foreach (var item in list)
                    {
                        var sList = list.FirstOrDefault(f => f.StylistId == item.StylistId);
                        if (sList != null)
                        {
                            //var objStylist = db.Store_StylistProductivity(timeFrom, timeTo, SalonId, item.StylistId).FirstOrDefault();
                            //this.kcs = objStylist.Total_SCSC_Error;
                            //this.totalk = objStylist.mistPoint;
                            //sList.KCS = (float)this.kcs;
                            //sList.totalMistake = this.totalk;
                        }
                    }
                }
                return list;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Xử lý dữ liệu
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private List<cls_nstb_item> getDataTotal(List<cls_Store_HRM_ThongKeNangSuatStylist> data)
        {
            try
            {
                var staffModel = new StaffModel();
                var list = new List<cls_nstb_item>();
                var item = new cls_nstb_item();
                var itemService = new cls_service_item();
                var index = -1;
                if (data.Count > 0)
                {
                    foreach (var v in data)
                    {
                        index = list.FindIndex(w => w.StylistId == v.StylistId);
                        if (index == -1)
                        {
                            item = new cls_nstb_item();
                            item.StylistId = v.StylistId;
                            item.StylistName = v.StylistName;
                            item.TotalHour = v.TotalHour;
                            item.TotalBill = v.TotalBill;
                            item.TotalMoney += v.TotalMoney;
                            item._KCS = v.KCS;
                            item._totalMisstake = v.totalMistake;
                            item.TotalQuantity += v.TotalQuantity;
                            item.ListItemServices = new List<cls_service_item>();
                            itemService = new cls_service_item();
                            itemService.ServiceId = v.ServiceId;
                            itemService.ServiceName = v.ServiceName;
                            itemService.Quantity = v.TotalQuantity;
                            itemService.TotalMoney = v.TotalMoney;
                            item.ListItemServices.Add(itemService);
                            list.Add(item);
                        }
                        else
                        {
                            item = list[index];
                            item.TotalMoney += v.TotalMoney;
                            item.TotalQuantity += v.TotalQuantity;
                            item._KCS = v.KCS;
                            item._totalMisstake = v.totalMistake;
                            itemService = new cls_service_item();
                            itemService.ServiceId = v.ServiceId;
                            itemService.ServiceName = v.ServiceName;
                            itemService.Quantity = v.TotalQuantity;
                            itemService.TotalMoney = v.TotalMoney;
                            item.ListItemServices.Add(itemService);
                            list[index] = item;
                        }

                        // Xử lý cho báo cáo tổng
                        TotalReport.TotalQuantity += v.TotalQuantity;
                        TotalReport.TotalMoney += v.TotalMoney;

                        index = TotalReport.ListItemServices.FindIndex(w => w.ServiceId == v.ServiceId);
                        if (index == -1)
                        {
                            itemService = new cls_service_item();
                            itemService.ServiceId = v.ServiceId;
                            itemService.ServiceName = v.ServiceName;
                            itemService.KCS = v.KCS;
                            itemService.totalMistake = v.totalMistake;
                            itemService.Quantity = v.TotalQuantity;
                            itemService.TotalMoney = v.TotalMoney;
                            TotalReport.ListItemServices.Add(itemService);
                        }
                        else
                        {
                            itemService = TotalReport.ListItemServices[index];
                            itemService.TotalMoney += v.TotalMoney;
                            itemService.Quantity += v.TotalQuantity;
                            TotalReport.ListItemServices[index] = itemService;
                            itemService.KCS = v.KCS;
                            itemService.totalMistake = v.totalMistake;
                        }
                        //total kcs and mistake
                        this.totalKcs += v.KCS;
                        this.totalMist += v.totalMistake;
                    }

                    if (list.Count > 0)
                    {
                        foreach (var v in list)
                        {
                            TotalReport.TotalBill += v.TotalBill;
                            TotalReport.TotalHour += v.TotalHour;
                        }
                    }
                }

                return list;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Xử lý lấy data bind
        /// </summary>
        private void getData()
        {
            var data = getDataFromStoreSQL();
            var dataTotal = getDataTotal(data);
            Bind_Paging(dataTotal.Count);
            listData = dataTotal.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
        }

        /// Bind Stylist
        /// </summary>
        private void bindStylist()
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    int integer;
                    var salon = int.TryParse(Salon.SelectedValue, out integer) ? integer : 0;
                    var src = Convert.ToInt32(ddlStylistSrc.SelectedValue);
                    var staff = new List<_30shine.MODEL.StaffView>();
                    var whereSalon = "";
                    var whereSrc = "";
                    var sql = "";
                    if (salon > 0)
                    {
                        whereSalon = " and s.SalonId = " + salon;
                    }
                    if (src == 1)
                    {
                        whereSrc = " and s.S4MClassId is not null";
                    }
                    else if (src == 0)
                    {
                        whereSrc = " and s.S4MClassId is null";
                    }
                    else
                    {
                        whereSrc = " and (s.S4MClassId is not null or s.S4MClassId is null)";
                    }
                    sql = @"select s.Id AS id, CAST((s.Id) AS NVARCHAR(100)) + ' - ' + s.Fullname AS fullName FROM Staff s
                        where s.IsDelete != 1 and s.Active = 1 
                        and (s.isAccountLogin != 1 or s.isAccountLogin is null) and s.[Type] = 1" + whereSalon + whereSrc;
                    staff = db.Database.SqlQuery<_30shine.MODEL.StaffView>(sql).ToList();
                    var first = new _30shine.MODEL.StaffView();
                    first.id = 0;
                    first.fullName = "Chọn Stylist";
                    staff.Insert(0, first);
                    ddlStaff.DataTextField = "fullName";
                    ddlStaff.DataValueField = "id";
                    ddlStaff.DataSource = staff;
                    ddlStaff.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Bind lại danh sách nhân viên khi thay đổi bộ phận
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void bindStylistBySalon(object sender, EventArgs e)
        {
            bindStylist();
            ScriptManager.RegisterStartupScript(this, GetType(), "Script", "SelectFunc();", true);
        }

        /// <summary>
        /// event click to load data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void _BtnClick(object sender, EventArgs e)
        {
            getData();
            ScriptManager.RegisterStartupScript(this, GetType(), "Script", "SetActiveTableListing();ReplaceZezoValue();SelectFunc();", true);
            RemoveLoading();
        }

        /// <summary>
        /// bind paging
        /// </summary>
        protected void Bind_Paging(int TotalRows)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? (!HDF_Page.Value.Equals("") ? Convert.ToInt32(HDF_Page.Value) : 1) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRows) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        /// <summary>
        /// get total page (paging)
        /// </summary>
        /// <returns></returns>
        protected int Get_TotalPage(int TotalRows)
        {
            using (var db = new Solution_30shineEntities())
            {
                TotalRows -= PAGING._TopNewsNum;
                int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRows / PAGING._Segment));
                return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
            }
        }

        /// <summary>
        /// export excel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Exc_ExportExcel(object sender, EventArgs e)
        {
            //using (var db = new Solution_30shineEntities())
            //{
            //    var _ExcelHeadRow = new List<string>();
            //    var serializer = new JavaScriptSerializer();
            //    var BillRowLST = new List<List<string>>();
            //    var BillRow = new List<string>();

            //    _ExcelHeadRow.Add("Tên nhân viên");
            //    _ExcelHeadRow.Add("Bộ phận");
            //    _ExcelHeadRow.Add("Bậc kỹ năng");
            //    _ExcelHeadRow.Add("Tổng công tháng " + ThisMon.ToString());
            //    _ExcelHeadRow.Add("Chấm công");
            //    _ExcelHeadRow.Add("Lương cơ bản");
            //    _ExcelHeadRow.Add("Lương cứng");
            //    _ExcelHeadRow.Add("Lương dịch vụ");
            //    _ExcelHeadRow.Add("Lương sản phẩm");
            //    _ExcelHeadRow.Add("Lương part-time");
            //    _ExcelHeadRow.Add("Phụ cấp");
            //    _ExcelHeadRow.Add("Tổng lương");

            //    var data = getData();
            //    if (data.Count > 0)
            //    {
            //        foreach (var v in data)
            //        {
            //            BillRow = new List<string>();
            //            BillRow.Add(v.Fullname);
            //            BillRow.Add(v.departmentName);
            //            BillRow.Add(v.skillLevelName);
            //            BillRow.Add(v.workDay.ToString());
            //            BillRow.Add(v.baseSalary.ToString());
            //            BillRow.Add(Math.Floor(v.fixSalary).ToString());
            //            BillRow.Add(Math.Floor(v.serviceSalary).ToString());
            //            BillRow.Add(Math.Floor(v.productSalary).ToString());
            //            BillRow.Add(Math.Floor(v.partTimeSalary).ToString());
            //            BillRow.Add(Math.Floor(v.allowanceSalary).ToString());
            //            BillRow.Add((Math.Floor(v.fixSalary) + Math.Floor(v.serviceSalary) + Math.Floor(v.productSalary) + Math.Floor(v.partTimeSalary) + Math.Floor(v.allowanceSalary)).ToString());
            //            BillRowLST.Add(BillRow);
            //        }
            //    }

            //    // export
            //    var ExcelStorePath = Server.MapPath("~") + "Public/Excel/Nhan.Vien/";
            //    var FileName = "Hoa_Don_" + string.Format("{0:dd_MM_yyyy_hh_mm_ss}", DateTime.Now) + ".xlsx";
            //    var UrlFileName = "http://" + Request.Url.Host + "/Public/Excel/Nhan.Vien/" + FileName;

            //    ExportXcel(_ExcelHeadRow, BillRowLST, ExcelStorePath + FileName);
            //    ScriptManager.RegisterStartupScript(BtnFakeExcel, this.GetType(), "Download Excel", "CallBackExportExcel('" + UrlFileName + "');UP_FormatPrice('.be-report-price'); SubTableFixHeight();SetActiveTableListing();", true);
            //}
        }

        private static void ExportXcel(List<string> _ExcelHeadRow, List<List<string>> List, string Path)
        {
            try
            {
                CreateExcelFile.ExcelHeadRow = _ExcelHeadRow;
                CreateExcelFile.CreateExcel(List, Path);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi. Không tạo được file excel.\r\nException: " + ex.Message);
            }
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }



        /// <summary>
        /// Class dữ liệu từ store
        /// </summary>
        public class cls_Store_HRM_ThongKeNangSuatStylist
        {
            public int StylistId { get; set; }
            public string StylistName { get; set; }
            public int ServiceId { get; set; }
            public string ServiceName { get; set; }
            public int TotalQuantity { get; set; }
            public double TotalMoney { get; set; }
            public double TotalHour { get; set; }
            public int TotalBill { get; set; }
            public int billOK { get; set; }
            public float KCS { get; set; }
            public int totalMistake { get; set; }
        }

        /// <summary>
        /// Class report item
        /// </summary>
        public class cls_nstb_item
        {
            /// <summary>
            /// Stylist ID
            /// </summary>
            public int StylistId { get; set; }
            /// <summary>
            /// Stylist Name
            /// </summary>
            public string StylistName { get; set; }
            /// <summary>
            /// Danh sách doanh số dịch vụ
            /// </summary>
            public List<cls_service_item> ListItemServices { get; set; }
            /// <summary>
            /// Doanh số
            /// </summary>
            public int TotalQuantity { get; set; }
            /// <summary>
            /// Doanh thu
            /// </summary>
            public double TotalMoney { get; set; }
            /// <summary>
            /// Tổng giờ làm
            /// </summary>
            public double TotalHour { get; set; }
            /// <summary>
            /// Tổng lượt khách
            /// </summary>
            public int TotalBill { get; set; }
            /// <summary>
            /// Doanh số trung bình / giờ
            /// </summary>
            public double DsTB_H { get; set; }
            /// <summary>
            /// Doanh thu trung bình / giờ
            /// </summary>
            public double DtTB_H { get; set; }
            /// <summary>
            /// KCS Trung bình (Tổng số Bill thành công/ Tổng Bill)
            /// </summary>
            public float _KCS { get; set; }
            /// <summary>
            /// Tổng số lỗi của stylist
            /// </summary>
            public int _totalMisstake { get; set; }

        }

        /// <summary>
        /// Class thể hiện doanh số theo dịch vụ
        /// </summary>
        public class cls_service_item
        {
            public int ServiceId { get; set; }
            public string ServiceName { get; set; }
            public int Quantity { get; set; }
            public float KCS { get; set; }
            public int totalMistake { get; set; }
            public double TotalMoney { get; set; }
        }
        public class cls_Stylist
        {
            public int id { get; set; }
            public int Value { get; set; }
        }
    }
}