﻿<%--﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Staff_Add.aspx.cs" Inherits="_30shine.GUI.BackEnd.UIStaff.Staff_Add" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>--%>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="~/GUI/BackEnd/Staff/Staff_Add.aspx.cs" Inherits="_30shine.GUI.BackEnd.UIStaff.Staff_Add" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="StaffAdd" ContentPlaceHolderID="CtMain" runat="server">
    <asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
    <asp:Panel CssClass="wp" ID="ContentWrap" runat="server">
        <style>
            #success-alert {
                top: 100px;
                right: 10px;
                position: fixed;
                width: 20% !important;
                z-index: 2000;
            }

            .staff-servey-mark span {
                height: 28px !important;
                line-height: 28px !important;
            }

            #pem {
                width: 100%;
            }

                #pem span {
                    width: 100%;
                    line-height: 22px;
                    height: 22px;
                }

                    #pem span div.btn-group .multiselect, #pem span div.btn-group, .multiselect-container {
                        width: 100% !important;
                    }

            #PermissionList {
                width: 100% !important;
            }

            .staff-servey-mark td input[type="radio"] {
                top: 4px !important;
            }
        </style>
        <div class="alert alert-danger" id="success-alert">
            <span id="msg-alert"></span>
        </div>
        <div class="wp sub-menu">
            <div class="wp960">
                <div class="wp content-wp">
                    <ul class="ul-sub-menu" id="subMenu">
                        <li>Quản lý Nhân sự &nbsp;&#187; </li>
                        <li class="li-listing"><a href="/admin/nhan-vien/danh-sach.html">Danh sách</a></li>
                        <li class="li-add active"><a href="/admin/nhan-vien/them-moi.html">Thêm mới</a></li>
                        <li class="li-add"><a href="/admin/nhan-vien/edit-team.html">Set Team</a></li>
                        <%--<li class="li-listing li-roll"><a href="/admin/nhan-vien/diem-danh.html">Điểm danh</a></li>--%>
                    </ul>
                </div>
            </div>
        </div>

        <div class="wp customer-add">
            <%-- Add --%>
            <div class="wp960 content-wp">
                <!-- System Message -->
                <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
                <!-- END System Message -->

                <div class="table-wp">
                    <table class="table-add">
                        <tbody>
                            <tr class="title-head">
                                <td><strong>Thông tin nhân viên</strong></td>
                                <td></td>
                            </tr>
                            <tr id="TrSalon" runat="server">
                                <td class="col-xs-3 left"><span>Salon/Bộ phận</span></td>
                                <td class="col-xs-9 right">
                                    <div class="div_col">

                                        <asp:DropDownList ID="Salon" CssClass="form-control select" runat="server" ClientIDMode="Static" Style="width: 49%">
                                        </asp:DropDownList>
                                        <%--<asp:RequiredFieldValidator InitialValue="0" 
                                    ID="ValidateSalon" Display="Dynamic" 
                                    ControlToValidate="Salon"
                                    runat="server"  Text="Bạn chưa chọn Salon!" 
                                    ErrorMessage="Vui lòng chọn Salon!"
                                    ForeColor="Red"
                                    CssClass="fb-cover-error">
                                </asp:RequiredFieldValidator>--%>
                                    </div>
                                    <div class="div_col">

                                        <asp:DropDownList ID="TypeStaff" class="remove-color" onchange="RemoveColor()" runat="server" ClientIDMode="Static" Style="width: 49%; margin-left: 10px"></asp:DropDownList>
                                        <%--<asp:RequiredFieldValidator InitialValue="0" 
                                    ID="ValidateSalonTypeStaff" Display="Dynamic" 
                                    ControlToValidate="TypeStaff"
                                    runat="server"  Text="Bạn chưa chọn kiểu nhân viên!" 
                                    ErrorMessage="Vui lòng chọn kiểu nhân viên!"
                                    ForeColor="Red"
                                    CssClass="fb-cover-error">
                                </asp:RequiredFieldValidator>--%>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Phân quyền</span></td>
                                <td class="col-xs-9 right" style="padding: 20px 0px !important;">
                                    <span class="field-wp" id="pem">
                                        <%--<asp:DropDownList ID="PermissionList"   multiple="multiple" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                        <asp:RequiredFieldValidator InitialValue="0"
                                            ID="ValidateSalon" Display="Dynamic"
                                            ControlToValidate="Salon"
                                            runat="server" Text="Bạn chưa chọn Salon!"
                                            ErrorMessage="Vui lòng chọn Salon!"
                                            ForeColor="Red"
                                            CssClass="fb-cover-error">
                                        </asp:RequiredFieldValidator>--%>
                                        <select name="ddlPermission" class="ddlPermission" id="ddlPermission" multiple="multiple"></select>
                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-2 left"><span>Tên làm việc</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="FullName" onkeyup="RemoveColor()" class="remove-color" runat="server" ClientIDMode="Static"></asp:TextBox>
                                        <%--       <asp:RequiredFieldValidator ID="FullNameValidate" ControlToValidate="FullName" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập họ tên!"></asp:RequiredFieldValidator>--%>
                                    </span>

                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Tên theo CMT</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="NameCMT" onkeyup="RemoveColor()" class="remove-color" runat="server" ClientIDMode="Static"></asp:TextBox>
                                        <%--    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="NameCMT" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập tên theo CMT!"></asp:RequiredFieldValidator>--%>
                                    </span>

                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-2 left"><span>Số CMND</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="StaffID" onkeyup="RemoveColor()" class="remove-color" runat="server" ClientIDMode="Static" onkeypress="return ValidateKeypress(/\d/,event);"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="Phone" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập số điện thoại!"></asp:RequiredFieldValidator>--%>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Ngày cấp</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col" style="width: 90%">
                                            <asp:TextBox ID="txtDateID" CssClass="txtDateTime" runat="server" ClientIDMode="Static"></asp:TextBox>
                                        </div>
                                        <div class="div_col">
                                            <span style="padding-left: 10px; padding-right: 7px; width: inherit !important; float: left">Nơi cấp</span>
                                            <asp:TextBox ID="txtAddressID" runat="server" ClientIDMode="Static"></asp:TextBox>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Ngày vào/Ngày tính thâm niên</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <asp:TextBox ID="DateJoin" CssClass="txtDateTime" runat="server" ClientIDMode="Static" placeholder="Chọn ngày vào làm việc" onkeypress="return ValidateKeypress(/\d/,event);"></asp:TextBox>
                                        </div>
                                        <div class="div_col">
                                            <asp:TextBox ID="DateTN" Style="margin-left: 9px" CssClass="txtDateTime" runat="server" ClientIDMode="Static" placeholder="Chọn ngày tính thâm niên" onkeypress="return ValidateKeypress(/\d/,event);"></asp:TextBox>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Số BHXH /Mã số thuế</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <asp:TextBox ID="NumberInsurrance" runat="server" ClientIDMode="Static" placeholder="Sổ bảo hiểm xã hội"></asp:TextBox>
                                        </div>
                                        <div class="div_col">
                                            <asp:TextBox ID="MST" Style="margin-left: 9px" runat="server" ClientIDMode="Static" placeholder="Mã số thuế thu nhập cá nhân" onkeypress="return ValidateKeypress(/\d/,event);"></asp:TextBox>
                                        </div>
                                    </div>

                                </td>
                            </tr>
                            <tr>
                                <td class="col-xs-3 left"><span>Tên NH-Chi nhánh</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <asp:TextBox ID="NameBank" runat="server" ClientIDMode="Static" placeholder="Ví dụ: BIDV-CN Hai Bà Trưng "> </asp:TextBox>
                                    </div>
                                </td>
                            </tr>

                            <tr class="tr-field-ahalf">
                                <td class="col-xs-3 left"><span>Tên TK/Số TK</span></td>
                                <td class="col-xs-9 right">
                                    <div class="field-wp">
                                        <div class="div_col">
                                            <asp:TextBox ID="NameAcc" runat="server" ClientIDMode="Static" placeholder="Ví dụ: NGUYEN VAN A "> </asp:TextBox>
                                        </div>
                                        <div class="div_col">
                                            <asp:TextBox ID="NumberTK" Style="margin-left: 9px" runat="server" ClientIDMode="Static" placeholder="Số tài khoản ngân hàng" onkeypress="return ValidateKeypress(/\d/,event);"></asp:TextBox>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-2 left"><span>Hồ sơ lý lịch </span></td>
                                <td class="col-xs-9 right" style="padding: 20px 0px !important;">
                                    <span class="field-wp" id="pem">
                                        <select name="ddlProFile" class="ddlProFile" id="ddlProFile" multiple="multiple"></select>
                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-2 left"><span>Bậc kỹ năng</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:DropDownList ID="SkillLevel" runat="server" ClientIDMode="Static"></asp:DropDownList>
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="Phone" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập số điện thoại!"></asp:RequiredFieldValidator>--%>
                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-2 left"><span>Email</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="Email" runat="server" class="remove-color" ClientIDMode="Static" placeholder="example@gmail.com"></asp:TextBox>
                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-2 left"><span>Số điện thoại</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="Phone" onkeyup="RemoveColor()" class="remove-color" runat="server" ClientIDMode="Static" onkeypress="return isNumber(event);"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator ID="PhoneValidate" ControlToValidate="Phone" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập số điện thoại!"></asp:RequiredFieldValidator>--%>
                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-2 left"><span>Tên đăng nhập</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:TextBox ID="UserAccount" onkeyup="RemoveColor()" class="remove-color" runat="server" ClientIDMode="Static" placeholder="example"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator ID="PhoneValidate" ControlToValidate="Phone" runat="server" CssClass="fb-cover-error" Text="Bạn chưa nhập số điện thoại!"></asp:RequiredFieldValidator>--%>
                                    </span>
                                </td>
                            </tr>

                            <tr class="tr-birthday">
                                <td class="col-xs-3 left"><span>Ngày sinh/ Giới tính</span></td>
                                <td class="col-xs-9 right">
                                    <div class="div_col">
                                        <asp:TextBox ID="Day" runat="server" ClientIDMode="Static" placeholder="Ngày"></asp:TextBox>
                                        <asp:TextBox ID="Month" runat="server" ClientIDMode="Static" placeholder="Tháng"></asp:TextBox>
                                        <asp:TextBox ID="Year" runat="server" ClientIDMode="Static" placeholder="Năm"></asp:TextBox>
                                    </div>
                                    <div class="div_col">
                                        <asp:DropDownList ID="Gender" runat="server" ClientIDMode="Static" Style="margin-left: 48px; width: 49%"></asp:DropDownList>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td class="col-xs-2 left"><span>Địa chỉ</span></td>
                                <td class="col-xs-9 right">
                                    <asp:ScriptManager ID="SM01" runat="server"></asp:ScriptManager>
                                    <asp:UpdatePanel runat="server" ID="UP01">
                                        <ContentTemplate>
                                            <div class="city">
                                                <asp:DropDownList AutoPostBack="true" ID="City" runat="server" CssClass="select"
                                                    OnSelectedIndexChanged="ddlFTinhThanh_SelectedIndexChanged" ClientIDMode="Static">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="district">
                                                <asp:DropDownList ID="District" runat="server" CssClass="select" ClientIDMode="Static"></asp:DropDownList>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <%--<asp:AsyncPostBackTrigger ControlID="OrderCloseBtn" EventName="Click"/>--%>
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <asp:TextBox ID="Address" runat="server" placeholder="Địa chỉ" ClientIDMode="Static"></asp:TextBox>
                                </td>
                            </tr>

                            <tr class="tr-upload">
                                <td class="col-xs-3 left"><span>Ảnh avatar/Ảnh CMT</span></td>
                                <td class="col-xs-7 right">
                                    <div class="wrap btn-upload-wp HDF_MainImg" style="margin-bottom: 5px; width: auto;">
                                        <div id="Btn_UploadImg" class="btn-upload-img" onclick="popupImageIframe('HDF_MainImg')">Chọn ảnh avatar</div>
                                        <asp:FileUpload ID="UploadFile" ClientIDMode="Static" Style="display: none;"
                                            CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                        <div class="wrap listing-img-upload">
                                            <% if (HasImages)
                                                { %>
                                            <% for (var i = 0; i < ListImagesUrl.Count; i++)
                                                { %>
                                            <div class="thumb-wp">
                                                <img class="thumb" alt="" title="" src="<%=ListImagesUrl[i] %>"
                                                    data-img="<%=ListImagesUrl[i] %>" />
                                                <span class="delete-thumb" onclick="deleteThumbnail($(this), 'HDF_MainImg')"></span>
                                            </div>
                                            <% } %>
                                            <% } %>
                                        </div>
                                    </div>
                                    <div class="wrap btn-upload-wp HDF_MainImgCMT1" style="margin-bottom: 5px; margin-left: 10px; width: auto;">
                                        <div id="Btn_UploadImg1" class="btn-upload-img" onclick="popupImageIframe('HDF_MainImgCMT1')">Ảnh CMT mặt trước</div>
                                        <asp:FileUpload ID="FileUpload1" ClientIDMode="Static" Style="display: none;"
                                            CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                        <div class="wrap listing-img-upload">
                                            <% if (HasImages)
                                                { %>
                                            <% for (var i = 0; i < ListImagesUrl.Count; i++)
                                                { %>
                                            <div class="thumb-wp">
                                                <img class="thumb" alt="" title="" src="<%=ListImagesUrl[i] %>"
                                                    data-img="<%=ListImagesUrl[i] %>" />
                                                <span class="delete-thumb" onclick="deleteThumbnail($(this), 'HDF_MainImgCMT1')"></span>
                                            </div>
                                            <% } %>
                                            <% } %>
                                        </div>
                                    </div>
                                    <div class="wrap btn-upload-wp HDF_MainImgCMT2" style="margin-bottom: 5px; width: auto; margin-left: 10px;">
                                        <div id="Btn_UploadImg2" class="btn-upload-img" onclick="popupImageIframe('HDF_MainImgCMT2')">Ảnh CMT mặt sau</div>
                                        <asp:FileUpload ID="FileUpload2" ClientIDMode="Static" Style="display: none;"
                                            CssClass="input-file-upload" runat="server" AllowMultiple="true" />
                                        <div class="wrap listing-img-upload">
                                            <% if (HasImages)
                                                { %>
                                            <% for (var i = 0; i < ListImagesUrl.Count; i++)
                                                { %>
                                            <div class="thumb-wp">
                                                <img class="thumb" alt="" title="" src="<%=ListImagesUrl[i] %>"
                                                    data-img="<%=ListImagesUrl[i] %>" />
                                                <span class="delete-thumb" onclick="deleteThumbnail($(this), 'HDF_MainImgCMT2')"></span>
                                            </div>
                                            <% } %>
                                            <% } %>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="staff-servey-mark">
                                <td class="col-xs-2 left"><span>Trạng thái</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:RadioButtonList ID="Active" runat="server" ClientIDMode="Static">
                                            <asp:ListItem Text="Đang hoạt động" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Đã nghỉ" Value="0"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </span>
                                </td>
                            </tr>

                            <tr class="staff-servey-mark">
                                <td class="col-xs-2 left"><span>Loại tài khoản</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:RadioButtonList ID="AccountType" runat="server" ClientIDMode="Static">
                                            <asp:ListItem Text="Tài khoản nhân viên" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Tài khoản đăng nhập" Value="1"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </span>
                                </td>
                            </tr>

                            <tr class="staff-servey-mark">
                                <td class="col-xs-2 left"><span>Tính lương</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:RadioButtonList ID="radioSalaryByPerson" runat="server" ClientIDMode="Static">
                                            <asp:ListItem Text="Theo nhóm (Bộ phận)" Value="False"></asp:ListItem>
                                            <asp:ListItem Text="Cá nhân" Value="True"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </span>
                                </td>
                            </tr>

                            <tr class="staff-servey-mark">
                                <td class="col-xs-2 left"><span>Yêu cầu điểm danh</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:RadioButtonList ID="radioEnroll" runat="server" ClientIDMode="Static">
                                            <asp:ListItem Text="Có" Value="True"></asp:ListItem>
                                            <asp:ListItem Text="Không" Value="False"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </span>
                                </td>
                            </tr>
                            <tr class="staff-servey-mark">
                                <td class="col-xs-2 left"><span>Nhân viên ảo</span></td>
                                <td class="col-xs-9 right">
                                    <span class="field-wp">
                                        <asp:RadioButtonList ID="radioAddionstaff" runat="server" ClientIDMode="Static">
                                            <asp:ListItem Text="Không" Value="False" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Có" Value="True"></asp:ListItem>
                                        </asp:RadioButtonList>
                                    </span>
                                </td>
                            </tr>
                            <tr class="tr-send">
                                <td class="col-xs-2 left"></td>
                                <td class="col-xs-9 right no-border">
                                    <span class="field-wp">
                                        <asp:Panel ID="BtnSend" CssClass="btn-send" runat="server" ClientIDMode="Static">Hoàn Tất</asp:Panel>
                                        <asp:Button ID="BtnFakeSend" OnClick="AddStaff" runat="server" Text="Hoàn tất" ClientIDMode="Static" Style="display: none;"></asp:Button>
                                        <%--   <asp:Button  ID="Send" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClientClick="getPermission()" OnClick="AddStaff"></asp:Button>--%>
                                        <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</
                                            span>--%>
                                    </span>
                                </td>
                            </tr>
                            <asp:HiddenField runat="server" ID="UrlKeyField" ClientIDMode="Static" />
                            <%--  <asp:HiddenField runat="server" ID="HDF_Permission" ClientIDMode="Static" />--%>
                            <asp:HiddenField runat="server" ID="HDF_Permission_ID" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="HDF_Profile_ID" ClientIDMode="Static" />
                            <asp:HiddenField runat="server" ID="ImgCoverField" ClientIDMode="Static" />
                            <asp:HiddenField ID="HDF_MainImg" runat="server" ClientIDMode="Static" />
                            <asp:HiddenField ID="HDF_MainImgCMT1" runat="server" ClientIDMode="Static" />
                            <asp:HiddenField ID="HDF_MainImgCMT2" runat="server" ClientIDMode="Static" />
                        </tbody>
                    </table>
                </div>
            </div>
            <%-- end Add --%>
        </div>
        <link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 48.7% !important;
            }

            .select2-container--default .select2-selection--single {
                height: 36px !important;
                padding-top: 1px !important;
            }
        </style>
        <script>
            jQuery(document).ready(function () {
                $("#success-alert").hide();
                //call list Permission
                GetListPermission();
                //call list profile
                GetListProfile();
                $("#glbAdminContent").addClass("active");
                $("#glbAdminStaff").addClass("active");

                //============================
                // Datepicker
                //============================
                $('.txtDateTime').datetimepicker({
                    dayOfWeekStart: 1,
                    lang: 'vi',
                    startDate: '2014/10/10',
                    format: 'd/m/Y',
                    dateonly: true,
                    showHour: false,
                    showMinute: false,
                    timepicker: false,
                    onChangeDateTime: function (dp, $input) {
                        //$input.val($input.val().split(' ')[0]);
                    },
                    scrollMonth: false,
                    scrollTime: false,
                    scrollInput: false
                });
            });

            ///get List Permission
            function GetListPermission() {
                $.ajax({
                    url: "/GUI/BackEnd/Staff/Staff_Add.aspx/BindPermission",
                    type: "POST",
                    contentType: "application/JSON; charset:UTF-8",
                    dataType: "JSON",
                    success: function (data) {
                        datajs = JSON.parse(data.d);
                        for (var i = 0; i < datajs.length; i++) {
                            var option = "<option value='" + datajs[i].Id + "'>" + datajs[i].Name + "</option>";

                            $("#ddlPermission").append(option);
                        }
                        $("#ddlPermission").multiselect({
                            includeSelectAllOption: true
                        });
                    },
                });
            }

            ///get list Profile
            function GetListProfile() {
                $.ajax({
                    url: "/GUI/BackEnd/Staff/Staff_Add.aspx/BindListProfile",
                    type: "POST",
                    contentType: "application/JSON; charset:UTF-8",
                    dataType: "JSON",
                    success: function (data) {
                        var jsonData = JSON.parse(data.d);
                        for (i = 0; i < jsonData.length; i++) {
                            var option = "<option value='" + jsonData[i].Value + "'>" + jsonData[i].Name + "</option>";

                            $("#ddlProFile").append(option);
                        }
                        $("#ddlProFile").multiselect({
                            includeSelectAllOption: true
                        });
                    }
                });
            }

            //validate call btnFake
            //author: QuynhDD
            $("#BtnSend").bind("click", function () {
                var kt = true;
                var error = 'Vui lòng kiểm tra lại thông tin!';
                var department = $("#TypeStaff").val();
                var fullName = $("#FullName").val();
                var nameCmt = $("#NameCMT").val();
                var numberCmt = $("#StaffID").val();
                var phone = $("#Phone").val();
                var user = $("#UserAccount").val();
                if (department <= 0) {
                    kt = false;
                    error += "[Bạn chưa chọn bộ phận], ";
                    $('#TypeStaff').css("border-color", "red");
                }
                if (fullName == "") {
                    kt = false;
                    error += "[Bạn chưa nhập tên làm việc], ";
                    $('#FullName').css("border-color", "red");
                }
                if (nameCmt == "") {
                    kt = false;
                    error += "[Bạn chưa nhập tên theo chứng minh thư], ";
                    $('#NameCMT').css("border-color", "red");
                }
                if (numberCmt == "") {
                    kt = false;
                    error += "[Bạn chưa nhập số chứng minh thư], ";
                    $('#StaffID').css("border-color", "red");
                }
                if (phone == "") {
                    kt = false;
                    error += "[Bạn chưa nhập số điện thoại], ";
                    $('#Phone').css("border-color", "red");
                }
                if (checkPhone(phone) == false) {
                    kt = false;
                    error += "[Bạn nhập sai định dạng số điện thoại], ";
                    $('#Phone').css("border-color", "red");
                }
                if (user == "") {
                    kt = false;
                    error += "[Bạn chưa nhập tài khoản đăng nhập]";
                    $('#UserAccount').css("border-color", "red");
                }
                if (!kt) {
                    $("#success-alert").fadeTo(2000, 2000).slideUp(2000, function () {
                        $("#success-alert").slideUp(2000);
                    });
                    $("#msg-alert").text(error);
                }
                else {
                    //Get list Id permission
                    getPermission();
                    //call list Id profile
                    getProfileId();
                    //call buton addStaff
                    $("#BtnFakeSend").click();
                }

                return kt;
            });

            /// get list id permission
            function getPermission() {
                var Ids = [];
                var prd = {};
                $("#ddlPermission option:selected").each(function () {
                    prd = {};
                    var permissionId = $(this).val();
                    if (permissionId != null && permissionId > 0) {
                        permissionId = permissionId.toString().trim() != "" ? parseInt(permissionId) : 0;
                        prd.permissionId = permissionId;
                        Ids.push(prd);
                    }
                    //console.log(Ids);
                });
                $("#HDF_Permission_ID").val(JSON.stringify(Ids));
            }

            //get list Id Profile
            function getProfileId() {
                var Ids = [];
                var prd = {};
                $("#ddlProFile option:selected").each(function () {
                    prd = {};
                    var profileId = $(this).val();
                    if (profileId != null && profileId > 0) {
                        profileId = profileId.toString().trim() != "" ? parseInt(profileId) : 0;
                        prd.profileId = profileId;
                        Ids.push(prd);
                    }
                });
                $("#HDF_Profile_ID").val(JSON.stringify(Ids));
            }

            //check Format Phone
            function checkPhone(phone) {
                phone = String(phone).trim();
                var filter = /^[0-9-+]+$/;
                var x = phone.substring(0, 1);

                if (phone.length < 10 || phone.length > 11 || x != '0' || filter.test(phone) == false)
                    return false;
                else
                    return true;
            }

            ///remove color
            function RemoveColor() {
                $(".remove-color").css("border-color", "#ddd");
            }

            //Check chỉ được phép nhập số 
            function ValidateKeypress(numcheck, e) {
                var keynum, keychar, numcheck;
                if (window.event) {
                    keynum = e.keyCode;
                }
                else if (e.which) {
                    keynum = e.which;
                }
                if (keynum == 8 || keynum == 127 || keynum == null || keynum == 9 || keynum == 0 || keynum == 13) return true;
                keychar = String.fromCharCode(keynum);
                var result = numcheck.test(keychar);
                return result;
            }
        </script>

        <!-- Popup plugin image -->
        <script type="text/javascript">
            function popupImageIframe(StoreImgField) {
                var Width = 960;
                var Height = 560;
                var ImgList = [];
                var iframe = $("<div class='iframe-image-upload' style='width:" + Width + "px; height:" + Height + "px; float:left; display:none;'><iframe src='/GUI/BackEnd/UploadImage/Upload_V2.aspx?Code=img&StoreImgField=" + StoreImgField + "&callback=getThumbFromIframe&imgWidth=800&imgHeight=600&multi_img=false&Folder=NhanVien' style='width:100%; height: 100%;'></iframe></div");

                $("body").find(".iframe-image-upload").remove().end().append(iframe).find(".iframe-image-upload").openEBPopup();
            }

            window.getThumbFromIframe = function (Imgs, StoreImgField) {
                var imgs = "";
                var thumb = "";
                if (Imgs.length > 0) {
                    thumb = '<div class="thumb-wp">' +
                        '<img class="thumb" alt="" title="" src="' + Imgs[0] + '"' +
                        'data-img="" />' +
                        '<span class="delete-thumb" onclick="deleteThumbnail($(this), \'' + StoreImgField + '\')"></span>' +
                        '</div>';
                    $("." + StoreImgField).find(".listing-img-upload").empty().append(thumb);
                    excThumbWidth(120, 120);
                    $("#" + StoreImgField).val(Imgs[0]);
                }
                autoCloseEBPopup();
            }

            //function deleteThumbnail(This, StoreImgField) {
            //    var imgs = "";
            //    var lst = This.parent().parent();
            //    This.parent().remove();
            //    lst.find("img.thumb").each(function () {
            //        imgs += $(this).attr("src") + ",";
            //    });
            //    $("#" + StoreImgField).val(imgs);
            //}
        </script>
    </asp:Panel>
</asp:Content>
