﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Linq.Expressions;
using LinqKit;
using System.Globalization;
using ExportToExcel;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Web.Services;
using System.Security.Cryptography;
using System.Text;
using _30shine.MODEL.BO;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.IO;
using DocumentFormat.OpenXml.Packaging;
using System.Net;
using System.IO;
using NLog;

namespace _30shine.GUI.BackEnd.UIStaff
{
    /// <summary>
    /// 
    /// </summary>
    public partial class Staff_Edit : System.Web.UI.Page
    {

        protected Paging PAGING = new Paging();
        protected Paging PAGING2 = new Paging();
        protected cls_staff OBJ = new cls_staff();
        protected Staff_TimeKeeping Total_TimeKeeping = new Staff_TimeKeeping();
        CultureInfo culture = new CultureInfo("vi-VN");
        //private bool Perm_AllData = false;
        //private bool Perm_Delete = false;
        //private bool Perm_ShowSalon = false;
        protected bool Perm_AllowAddNew = false;
        protected bool Staff_All = false;
        protected bool Staff_Hairdresser = false;
        protected bool Staff_HairMassage = false;
        protected bool Staff_Singer = false;
        protected string THeadService = "";
        protected string THeadProduct = "";
        protected string StrTotalService = "<td>Tổng số</td>";
        protected string StrTotalProduct = "<td>Tổng số</td>";
        private DateTime MinTime = new DateTime();
        private DateTime MaxTime = new DateTime();
        private List<Service> LST_Service = new List<Service>();
        private List<Product> LST_Product = new List<Product>();
        private Expression<Func<BillService, bool>> WhereBill = PredicateBuilder.True<BillService>();
        public int Code;
        protected string _Code;
        protected bool HasImages = false;
        protected List<string> ListImagesUrl = new List<string>();
        protected string[] ListImagesName;
        protected List<string> LisImageCMT1 = new List<string>();
        protected List<string> ListImageCMT2 = new List<string>();
        private bool Perm_Access = false;
        protected bool Perm_Edit = false;
        private bool Perm_AllPermission = false;
        private bool Perm_HidePermission = false;
        private bool Perm_ActionMenu = false;
        private bool Perm_TypeStaff = false;
        string[] LSTPerm = new string[] { "root", "admin", "salonmanager", "reception", "staff", "checkin", "checkout" };
        public StaffModel staffModel = new StaffModel();
        IAuthenticationModel authenticationModel = new AuthenticationModel();
        IStaffContractMap staffContractMap = new StaffContractMapModel();
        IEnrollModel enrollModel = new EnrollModel();
        public cls_Recruitment clsRecruitment = new cls_Recruitment();
        public static int salonId;
        public static int StylistId;
        private static Staff_Edit instance;
        public static cls_Staff_History_Work clsHistory = new cls_Staff_History_Work();
        protected int? TotalQuantityCurling = 0;
        protected int? TotalQuantityColor = 0;
        protected int? TotalQuantityShine = 0;
        protected int? TotalPoint = 0;
        protected int? MistPoint = 0;
        protected double? TotalMoney = 0;
        protected int? Remind = 0;
        private static Logger logger = LogManager.GetCurrentClassLogger();
        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                pageId = permissionModel.GetRegexPageIdSpecial().Replace(pageId, ".html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_Edit = permissionModel.CheckPermisionByAction("Perm_Edit", pageId, staffId);
                Perm_TypeStaff = permissionModel.CheckPermisionByAction("Perm_TypeStaff", pageId, staffId);
                Perm_AllPermission = permissionModel.CheckPermisionByAction("Perm_AllPermission", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();
            //Bind_DataHead();

            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList>() { Salon }, Perm_AllPermission);
                Bind_Source();
                if (Bind_OBJ())
                {
                    OrderCode.Enabled = false;
                    UserAccount.Enabled = false;
                    //Bind_Source();
                    Bind_SkillLevel();
                    Bind_TypeStaff();
                    Bind_Permission();
                    Bind_ProfileId();
                    Bind_Gender();
                    Bind_City();
                    Bind_District();
                    Bind_Skinner();
                    FillRadio();
                    disableControl();
                    BindSkillNameLevel();
                    GetDataRecruitments();
                }
            }
        }


        /// <summary>
        /// Get instance
        /// </summary>
        /// <returns></returns>
        public static Staff_Edit getInstance()
        {
            return Staff_Edit.instance = new Staff_Edit();
        }

        /// <summary>
        /// 
        /// </summary>
        public static Staff_Edit Instance
        {
            get
            {
                return new Staff_Edit();
            }
        }

        /// <summary>
        /// disable Control
        /// </summary>
        public void disableControl()
        {
            txtPointStylist.Enabled = false;
            ddlTestResultStylist.Enabled = false;
            cblTestAgainStylist.Enabled = false;
            txtStylistLTCUT.Enabled = false;
            ddlCutStylist.Enabled = false;
            CbxCut.Enabled = false;
            txtChemistry.Enabled = false;
            ddlChemistry.Enabled = false;
            CbxChemistry.Enabled = false;
            PointSkinner.Enabled = false;
            TotalSuccess.Enabled = false;
            CbxCheck.Enabled = false;
            txtCultural.Enabled = false;
            ddlCultural.Enabled = false;
            CbxCultural.Enabled = false;
            TotalDHL.Enabled = false;
            StylistNSKT.Enabled = false;
            txtCurling.Enabled = false;
            txt30shine.Enabled = false;
            txtColorCombo.Enabled = false;
            txtDisciplineStylist.Enabled = false;
            txtDisciplineStylistWarning.Enabled = false;
            txtTotalPointSkinner.Enabled = false;
            txtDisciplineSkinner.Enabled = false;
            txtDisciplineSkinnerWarning.Enabled = false;
            txtTotalSales.Enabled = false;
            txtSales.Enabled = false;
            txtDisciplineCheckin.Enabled = false;
            txtDisciplineWarningCheckin.Enabled = false;
        }
        /// <summary>
        /// Bind data for service and product
        /// </summary>
        protected void GenWhere()
        {
            Code = Int32.TryParse(Request.QueryString["Code"], out Code) ? Code : 0;
            // Time condition
            CultureInfo culture = new CultureInfo("vi-VN");
        }

        /// <summary>
        /// Bind data to radio button
        /// </summary>
        public void FillRadio()
        {
            try
            {
                ITblStatusModel db = new TblStatusModel();
                //var serilaze = new JavaScriptSerializer();
                var list = db.GetList_Staff();
                LstradiobttList.DataTextField = "Name";
                LstradiobttList.DataValueField = "Value";
                LstradiobttList.DataSource = list;
                LstradiobttList.DataBind();
                //return serilaze.Serialize(list);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Bind nguồn tuyển dụng
        /// </summary>
        public void Bind_Source()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.TuyenDung_Nguon.Where(w => w.IsDelete == false).OrderBy(o => o.Id).ToList();
                var Key = 0;
                ddlSourceId.DataTextField = "Name";
                ddlSourceId.DataValueField = "Id";
                ListItem item = new ListItem("Chọn nguồn lao động", "0");
                ddlSourceId.Items.Insert(Key, item);
                Key++;
                if (lst.Count > 0)
                {
                    foreach (var v in lst)
                    {
                        item = new ListItem(v.Name, v.Id.ToString());
                        ddlSourceId.Items.Insert(Key, item);
                        Key++;
                    }
                    var ItemSelected = ddlSourceId.Items.FindByValue(OBJ.SourceId.ToString());
                    if (ItemSelected != null)
                    {
                        ItemSelected.Selected = true;
                    }
                    else
                    {
                        ddlSourceId.SelectedIndex = 0;
                    }
                }
            }
        }

        /// <summary>
        /// Bind staff information
        /// </summary>
        public void Bind_SkillLevel()
        {
            logger.Info("1");
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Tbl_SkillLevel.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Order).ToList();
                var Key = 0;
                SkillLevel.DataTextField = "SkillLevel";
                SkillLevel.DataValueField = "Value";
                ListItem item = new ListItem("Chọn bậc kỹ năng", "0");
                SkillLevel.Items.Insert(Key, item);
                Key++;

                if (lst.Count > 0)
                {
                    foreach (var v in lst)
                    {
                        item = new ListItem(v.Name, v.Id.ToString());
                        SkillLevel.Items.Insert(Key, item);
                        Key++;
                    }
                    var ItemSelected = SkillLevel.Items.FindByValue(OBJ.SkillLevel.ToString());
                    if (ItemSelected != null)
                    {
                        ItemSelected.Selected = true;
                    }
                    else
                    {
                        SkillLevel.SelectedIndex = 0;
                    }
                }
            }
        }

        /// <summary>
        /// Bind department
        /// </summary>
        public void Bind_TypeStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Order).ToList();
                var Key = 0;
                TypeStaff.DataTextField = "TypeStaff";
                TypeStaff.DataValueField = "Id";

                if (Perm_TypeStaff)
                {
                    ListItem item = new ListItem("Chọn bộ phận", "0");
                    TypeStaff.Items.Insert(0, item);

                    if (LST.Count > 0)
                    {
                        foreach (var v in LST)
                        {
                            Key++;
                            item = new ListItem(v.Name, v.Id.ToString());
                            TypeStaff.Items.Insert(Key, item);
                        }

                        var ItemSelected = TypeStaff.Items.FindByValue(OBJ.Type.ToString());
                        if (ItemSelected != null)
                        {
                            ItemSelected.Selected = true;
                        }
                        else
                        {
                            TypeStaff.SelectedIndex = 0;
                        }
                    }
                }
                else
                {
                    if (LST.Count > 0)
                    {
                        foreach (var v in LST)
                        {
                            if (v.Id == OBJ.Type)
                            {
                                ListItem item = new ListItem(v.Name, v.Id.ToString());
                                TypeStaff.Items.Insert(Key, item);
                                TypeStaff.SelectedIndex = Convert.ToInt32(OBJ.Type);
                                TypeStaff.Enabled = false;
                            }

                        }
                    }
                }
            }
        }

        /// <summary>
        /// Bind Permission
        /// </summary>
        private void Bind_Permission()
        {
            var _Code = Convert.ToInt32(Request.QueryString["Code"]);
            var key = 0;
            using (var db = new Solution_30shineEntities())
            {
                var list = (from c in db.PermissionErps.AsNoTracking()
                            where c.IsDelete == false && c.IsActive == true
                            select c
                               ).ToList();
                foreach (var v in list)
                {
                    var item = new ListItem(v.Name, v.Id.ToString());
                    PermissionList.Items.Insert(key++, item);
                }
                if (_Code > 0)
                {
                    var ListPermissionStaff = (from c in db.PermissionStaffs
                                               where c.StaffId == _Code && c.IsActive == true && c.IsDelete == false
                                               select c).ToList();
                    if (ListPermissionStaff.Count > 0)
                    {
                        foreach (var v in ListPermissionStaff)
                        {
                            var ItemSelected = PermissionList.Items.FindByValue(v.PermissionId.ToString());
                            if (ItemSelected != null)
                            {
                                ItemSelected.Selected = true;
                            }
                        }
                    }
                }
            }

        }

        /// <summary>
        /// Bind Profile id
        /// </summary>
        private void Bind_ProfileId()
        {
            var _Code = Convert.ToInt32(Request.QueryString["Code"]);
            var key = 0;
            using (var db = new Solution_30shineEntities())
            {
                var list = (from c in db.Tbl_Status.AsNoTracking()
                            where c.IsDelete == false && c.Publish == true
                           && c.ParentId == 19
                            select c).ToList();
                foreach (var v in list)
                {
                    var item = new ListItem(v.Name, v.Value.ToString());
                    ProfileId.Items.Insert(key++, item);
                }
                if (_Code > 0)
                {
                    var ListProfileStaff = (from c in db.StaffProfileMaps
                                            where c.StaffId == _Code && c.IsDelete == false
                                            select c).ToList();
                    if (ListProfileStaff.Count > 0)
                    {
                        foreach (var v in ListProfileStaff)
                        {
                            var ItemSelected = ProfileId.Items.FindByValue(v.StatusProfileId.ToString());
                            if (ItemSelected != null)

                            {
                                ItemSelected.Selected = true;
                            }
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Bind sex
        /// </summary>
        private void Bind_Gender()
        {
            Gender.DataTextField = "TypeStaff";
            Gender.DataValueField = "Id";
            ListItem item = new ListItem("Chọn giới tính", "0");
            Gender.Items.Insert(0, item);
            item = new ListItem("Nam", "1");
            Gender.Items.Insert(1, item);
            item = new ListItem("Nữ", "2");
            Gender.Items.Insert(2, item);
            item = new ListItem("Khác", "3");
            Gender.Items.Insert(3, item);

            var ItemSelected = Gender.Items.FindByValue(OBJ.Gender.ToString());
            if (ItemSelected != null)
            {
                ItemSelected.Selected = true;
            }
            else
            {
                Gender.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// Bind thành phố
        /// </summary>
        protected void Bind_City()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.TinhThanhs.OrderBy(p => p.ThuTu).ToList();
                City.DataTextField = "TenTinhThanh";
                City.DataValueField = "Id";
                City.DataSource = lst;
                City.DataBind();

                var ItemSelected = City.Items.FindByValue(OBJ.CityId.ToString());
                if (ItemSelected != null)
                {
                    ItemSelected.Selected = true;
                }
                else
                {
                    City.SelectedIndex = 0;
                }
            }
        }

        /// <summary>
        /// Bind quận huyện
        /// </summary>
        protected void Bind_District()
        {
            using (var db = new Solution_30shineEntities())
            {
                int _TinhThanhID = Convert.ToInt32(City.SelectedValue);
                if (_TinhThanhID != 0)
                {
                    var lst = db.QuanHuyens.Where(p => p.TinhThanhID == _TinhThanhID).OrderBy(p => p.ThuTu).ToList();
                    District.DataTextField = "TenQuanHuyen";
                    District.DataValueField = "Id";
                    District.DataSource = lst;
                    District.DataBind();

                    if (!IsPostBack)
                    {
                        var ItemSelected = District.Items.FindByValue(OBJ.DistrictId.ToString());
                        if (ItemSelected != null)
                        {
                            ItemSelected.Selected = true;
                        }
                        else
                        {
                            District.SelectedIndex = 0;
                        }
                    }
                    else
                    {
                        District.SelectedIndex = 0;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Reload_District(object sender, EventArgs e)
        {
            Bind_District();
        }

        /// <summary>
        /// Bind Skinner
        /// </summary>
        public void Bind_Skinner()
        {
            using (var db = new Solution_30shineEntities())
            {
                var skinner = db.Staffs.Where(w => w.IsDelete != 1 && w.Type == 2).ToList();
                SkinnerList.DataTextField = "Fullname";
                SkinnerList.DataValueField = "Id";
                SkinnerList.DataSource = skinner;
                SkinnerList.DataBind();
                ListItem item = new ListItem("Chọn Skinner cùng nhóm", "0");
                SkinnerList.Items.Insert(0, item);

                var ItemSelected = SkinnerList.Items.FindByValue(OBJ.SkinnerIdInGroup.ToString());
                if (ItemSelected != null)
                {
                    ItemSelected.Selected = true;
                }
                else
                {
                    SkinnerList.SelectedIndex = 0;
                }
            }
        }

        /// <summary>
        /// Lấy về tên các kỹ năng 
        /// </summary>
        private static string chuoi = "";
        private void BindSkillNameLevel()
        {
            using (var db = new Solution_30shineEntities())
            {
                if (OBJ.UngvienID != null)
                {
                    List<cls_skillname> listSkillName = new List<cls_skillname>();
                    var listId = db.TuyenDung_DanhGia.Where(w => w.UngVienID == OBJ.UngvienID).ToList();
                    if (listId.Count > 0)
                    {
                        for (int i = 0; i < listId.Count; i++)
                        {
                            chuoi = listId[i].MapSkillLevelID;
                        }
                        listSkillName = db.Database.SqlQuery<cls_skillname>("select map.Id as IdSkill , skill.SkillName from TuyenDung_SkillLevel_Map as map join TuyenDung_Skill as skill on map.SkillID = skill.Id where map.IsDelete = 0 and map.Id in (" + chuoi + ")").ToList();
                        if (listSkillName.Count > 0)
                        {
                            rptSkillName.DataSource = listSkillName;
                            rptSkillName.DataBind();
                        }
                    }

                }
            }
        }

        /// <summary>
        /// Sự kiện IteamDataBound lấy dữ liệu đổ vào Dropdownlist
        /// </summary>
        protected void rptSkillName_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            try
            {
                var dataItem = (cls_skillname)e.Item.DataItem;
                if (dataItem != null)
                {
                    DropDownList rpt = e.Item.FindControl("drlLevelName") as DropDownList;
                    using (var db = new Solution_30shineEntities())
                    {
                        var listLevel = db.Database.SqlQuery<cls_SkillLevel>("select lev.Id, CONCAT((lev.LevelName), ' --- ', (map.[Description])) as levelName from TuyenDung_SkillLevel_Map as map join TuyenDung_SkillLevel as lev on map.LevelID = lev.Id where map.IsDelete = 0 and map.Id in (" + dataItem.IdSkill + ")").ToList();
                        rpt.Items.Clear();
                        rpt.DataTextField = "levelName";
                        rpt.DataValueField = "Id";
                        rpt.DataSource = listLevel;
                        rpt.DataBind();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public void GetDataRecruitments()
        {
            IStaffModel dbStaff = new StaffModel();
            var modelStaff = dbStaff.GetDataRecruitment(OBJ.Id);
            if (modelStaff != null)
            {
                //Tuyển dụng
                if (OBJ.Type == 1)
                {
                    txtPointStylist.Text = modelStaff.Test_VH;
                    if (modelStaff.Parse_VH == true)
                    {
                        ddlSourceId.SelectedIndex = 0;
                    }
                    else
                    {
                        ddlSourceId.SelectedIndex = 1;
                    }
                    cblTestAgainStylist.Checked = Convert.ToBoolean(modelStaff.TestAgain);
                    clsRecruitment.ImageResult1 = modelStaff.ImgSkill1;
                    clsRecruitment.ImageResult2 = modelStaff.ImgSkill2;
                    txtStylistLTCUT.Text = modelStaff.TestCut;
                    ddlCutStylist.SelectedValue = false.ToString();
                    if (modelStaff.PassCut == true)
                    {
                        ddlCutStylist.SelectedIndex = 0;
                    }
                    else
                    {
                        ddlCutStylist.SelectedIndex = 1;
                    }
                    CbxCut.Checked = Convert.ToBoolean(modelStaff.TestCutAgain);
                    txtChemistry.Text = modelStaff.TestChemistry;
                    if (modelStaff.PassChemistry == true)
                    {
                        ddlChemistry.SelectedIndex = 0;
                    }
                    else
                    {
                        ddlChemistry.SelectedIndex = 1;
                    }
                    CbxChemistry.Checked = Convert.ToBoolean(modelStaff.TestCutAgain);
                }
                else if (OBJ.Type == 2)
                {
                    PointSkinner.Text = modelStaff.Test_VH;
                    if (modelStaff.Parse_VH == true)
                    {
                        TotalSuccess.SelectedIndex = 0;
                    }
                    else
                    {
                        TotalSuccess.SelectedIndex = 1;
                    }
                    CbxCheck.Checked = Convert.ToBoolean(modelStaff.TestAgain);
                    clsRecruitment.linkShampoo = modelStaff.VideoLink;
                }
                else if (OBJ.Type == 5 || OBJ.Type == 6)
                {
                    txtCultural.Text = modelStaff.Test_VH;
                    if (modelStaff.Parse_VH == true)
                    {
                        ddlCultural.SelectedIndex = 0;
                    }
                    else
                    {
                        ddlCultural.SelectedIndex = 1;
                    }
                    CbxCultural.Checked = Convert.ToBoolean(modelStaff.TestAgain);
                }
            }
            else
            {
                ddlChemistry.SelectedIndex = 1;
                ddlCultural.SelectedIndex = 1;
                ddlCutStylist.SelectedIndex = 1;
                ddlTestResultStylist.SelectedIndex = 1;
                TotalSuccess.SelectedIndex = 1;
            }
        }

        /// <summary>
        /// Bind OBJ
        /// </summary>
        /// <returns></returns>
        private bool Bind_OBJ()
        {
            var ExitsOBJ = true;
            try
            {
                Code = Int32.TryParse(Request.QueryString["Code"], out Code) ? Code : 0;
                using (var db = new Solution_30shineEntities())
                {
                    var sql = @"select a.*, t.TenTinhThanh, q.TenQuanHuyen, s.Name as salonName, sk.Name as skillLevelName, stt.Name as Department
                            from Staff as a
                            left join TinhThanh as t
                            on a.CityId = t.ID
                            left join QuanHuyen as q
                            on a.DistrictId = q.ID
                            left join Tbl_Salon as s
                            on a.SalonId = s.Id
                            left join Tbl_SkillLevel as sk
                            on a.SkillLevel = sk.Id
                            left join Staff_Type as stt
                            on a.Type = stt.Id
                            where a.Id = " + Code;
                    OBJ = db.Database.SqlQuery<cls_staff>(sql).FirstOrDefault();

                    if (OBJ != null)
                    {

                        var user = authenticationModel.GetByUserName(OBJ.Email);

                        FullName.Text = OBJ.Fullname;
                        StaffID.Text = OBJ.StaffID;
                        Phone.Text = OBJ.Phone;
                        Email.Text = user != null ? user.Email : "";
                        UserAccount.Text = OBJ.Email;
                        Day.Text = OBJ.SN_day.ToString();
                        Month.Text = OBJ.SN_month.ToString();
                        Year.Text = OBJ.SN_year.ToString();
                        Address.Text = OBJ.Address;
                        Note.Text = OBJ.Note;
                        if (OBJ.DateJoin != Convert.ToDateTime(null))
                        {
                            DateJoin.Text = String.Format("{0:dd/MM/yyyy}", OBJ.DateJoin);
                        }
                        if (OBJ.IDProvidedDate != Convert.ToDateTime(null))
                        {
                            txtDateID.Text = String.Format("{0:dd/MM/yyyy}", OBJ.IDProvidedDate);
                        }
                        txtAddressID.Text = OBJ.IDProviderLocale;
                        //update skill level
                        var autoLevel = db.StaffAutoLevelLogs.Where(w => w.Staff_Id == OBJ.Id).OrderByDescending(f => f.Id).FirstOrDefault();
                        if (autoLevel != null)
                        {
                            StaffAutoLevelLog staff = (from c in db.StaffAutoLevelLogs
                                                       where c.Staff_Id == OBJ.Id
                                                       select c).OrderByDescending(f => f.Id).FirstOrDefault();
                            staff.Staff_Id = OBJ.Id;
                            staff.SkillLevelup_Id = OBJ.SkillLevel;
                            db.SaveChanges();
                        }
                        OrderCode.Text = OBJ.OrderCode != null ? OBJ.OrderCode : "";
                        if (OBJ.Avatar != "" && OBJ.Avatar != null)
                        {
                            ListImagesName = OBJ.Avatar.Split(',');
                            var Len = ListImagesName.Length;
                            for (var i = 0; i < Len; i++)
                            {
                                if (ListImagesName[i] != "")
                                {
                                    ListImagesUrl.Add(ListImagesName[i]);
                                }
                            }
                            if (Len > 0) HasImages = true;
                        }

                        var itemSelected = AccountType.Items.FindByValue(OBJ.isAccountLogin.ToString());
                        if (itemSelected != null)
                        {
                            itemSelected.Selected = true;
                        }

                        itemSelected = radioSalaryByPerson.Items.FindByValue(OBJ.SalaryByPerson.ToString());
                        if (itemSelected != null)
                        {
                            itemSelected.Selected = true;
                        }

                        itemSelected = radioEnroll.Items.FindByValue(OBJ.RequireEnroll.ToString());
                        if (itemSelected != null)
                        {
                            itemSelected.Selected = true;
                        }
                        // add 20190509
                        itemSelected = radioAddionstaff.Items.FindByValue(OBJ.AdditionStaff.ToString());
                        if (itemSelected != null)
                        {
                            itemSelected.Selected = true;
                        }
                        LstradiobttList.SelectedValue = OBJ.Active.ToString();
                        ddlSourceId.SelectedValue = OBJ.SourceId.ToString();
                        Salon.SelectedValue = Convert.ToString(OBJ.SalonId);
                        //Show New
                        //Dũng EDIT Staffautolevel
                        var sDate = db.Logs.Where(f => f.OBJId == OBJ.Id).OrderByDescending(f => f.LogTime).FirstOrDefault();
                        if (OBJ.SkillLevel == 1)
                        {
                            if (OBJ.NgayTinhThamNien != null)
                            {
                                DateTN.Text = String.Format("{0:dd/MM/yyyy}", OBJ.NgayTinhThamNien);
                            }
                            else if (sDate != null)
                            {
                                DateTN.Text = String.Format("{0:dd/MM/yyyy}", sDate.LogTime);
                            }
                        }
                        else
                        {
                            DateTN.Text = String.Format("{0:dd/MM/yyyy}", OBJ.NgayTinhThamNien);
                        }
                        //END edit staffautolevel
                        NameCMT.Text = OBJ.NameCMT;
                        NumberInsurrance.Text = OBJ.NumberInsurrance;
                        MST.Text = OBJ.MST;
                        NameBank.Text = OBJ.NganHang_ChiNhanh;
                        NameAcc.Text = OBJ.NganHang_TenTK;
                        NumberTK.Text = OBJ.NganHang_SoTK;
                        if (OBJ.CMTimg1 != "" && OBJ.CMTimg1 != null)
                        {
                            ListImagesName = OBJ.CMTimg1.Split(',');
                            var Len = ListImagesName.Length;
                            for (var i = 0; i < Len; i++)
                            {
                                if (ListImagesName[i] != "")
                                {
                                    LisImageCMT1.Add(ListImagesName[i]);
                                }
                            }
                            if (Len > 0) HasImages = true;
                        }
                        if (OBJ.CMTimg2 != "" && OBJ.CMTimg2 != null)
                        {
                            ListImagesName = OBJ.CMTimg2.Split(',');
                            var Len = ListImagesName.Length;
                            for (var i = 0; i < Len; i++)
                            {
                                if (ListImagesName[i] != "")
                                {
                                    ListImageCMT2.Add(ListImagesName[i]);
                                }
                            }
                            if (Len > 0) HasImages = true;
                        }
                        // Init log skill level
                        StaffLib.AddLogSkillLevel(OBJ.Id, Convert.ToInt32(OBJ.SkillLevel.Value), db);
                        rptLogSkillLevel.DataSource = StaffLib.getLogSkillLevel(OBJ.Id, db);
                        rptLogSkillLevel.DataBind();
                        // Bind log skill level
                        //Bind contract
                        var date3thang = db.StaffContractMaps.Where(r => r.ContractId == 1 && r.IsDelete == false && r.StaffId == OBJ.Id).FirstOrDefault();
                        txt3thang.Text = date3thang != null ? String.Format("{0:dd/MM/yyyy}", date3thang.ContractDate) : "";
                        var date6thang = db.StaffContractMaps.Where(r => r.ContractId == 2 && r.IsDelete == false && r.StaffId == OBJ.Id).FirstOrDefault();
                        txt6thang.Text = date6thang != null ? String.Format("{0:dd/MM/yyyy}", date6thang.ContractDate) : "";
                        var dateHDLD = db.StaffContractMaps.Where(r => r.ContractId == 3 && r.IsDelete == false && r.StaffId == OBJ.Id).FirstOrDefault();
                        txtHDLD.Text = dateHDLD != null ? String.Format("{0:dd/MM/yyyy}", dateHDLD.ContractDate) : "";
                        var dateHDTV = db.StaffContractMaps.Where(r => r.ContractId == 4 && r.IsDelete == false && r.StaffId == OBJ.Id).FirstOrDefault();
                        txtThuViec.Text = dateHDTV != null ? String.Format("{0:dd/MM/yyyy}", dateHDTV.ContractDate) : "";
                        var dateHDTVQuanLy = db.StaffContractMaps.Where(r => r.ContractId == 5 && r.IsDelete == false && r.StaffId == OBJ.Id).FirstOrDefault();
                        txtHDTVManager.Text = dateHDTVQuanLy != null ? String.Format("{0:dd/MM/yyyy}", dateHDTVQuanLy.ContractDate) : "";
                        var dateHDLDQuanLy = db.StaffContractMaps.Where(r => r.ContractId == 6 && r.IsDelete == false && r.StaffId == OBJ.Id).FirstOrDefault();
                        txtHDLDManager.Text = dateHDLDQuanLy != null ? String.Format("{0:dd/MM/yyyy}", dateHDLDQuanLy.ContractDate) : "";
                        var isCheckLDSL = dateHDLD != null ? dateHDLD.IsReceived : false;
                        chkLDSL.Checked = isCheckLDSL ?? false;
                        var isCheckLSQL = dateHDLDQuanLy != null ? dateHDLDQuanLy.IsReceived : false;
                        chkLDQL.Checked = isCheckLSQL ?? false;
                    }
                    else
                    {
                        ExitsOBJ = false;
                        var msg = "Lỗi! Nhân viên không tồn tại.";
                        var status = "msg-system warning";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + msg + "','" + status + "');showDom('#StaffInfoWrap', true);", true);
                    }
                }
            }
            catch
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + "Đã có lỗi xảy ra. Liên hệ với nhà phát triển" + "','" + "msg-system warning" + "');showDom('#StaffInfoWrap', true);", true);
            }
            return ExitsOBJ;
        }

        /// <summary>
        /// Get Log
        /// </summary>
        /// <param name="type"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private List<Log> GetLog(string type, int id)
        {
            using (var db = new Solution_30shineEntities())
            {
                var sql = @"select * from [Log] where Type='" + type + "' and OBJId=" + id + " order by LogTime desc";
                var list = db.Database.SqlQuery<Log>(sql).ToList();
                return list;
            }
        }

        /// <summary>
        /// Bind lịch sử làm việc
        /// </summary>
        /// <param name="timeFrom"></param>
        /// <param name="timeTo"></param>
        /// <param name="SalonId"></param>
        /// <param name="StaffId"></param>
        /// <param name="TypeId"></param>
        /// <returns></returns>
        [WebMethod]
        public static cls_Staff_History_Work GetDataWork(string timeFrom, string timeTo, int SalonId, int StaffId, int TypeId)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    Staff_Edit instance = Staff_Edit.getInstance();
                    IStaffModel staffModel = new StaffModel();
                    cls_Staff_History_Work clsHis = new cls_Staff_History_Work();
                    CultureInfo culture = new CultureInfo("vi-VN");
                    DateTime _timeFrom = new DateTime();
                    DateTime _timeTo = new DateTime();
                    if (timeFrom != "")
                    {
                        _timeFrom = Convert.ToDateTime(timeFrom, culture);
                        if (timeTo != "")
                        {
                            if (timeFrom == timeTo)
                            {
                                _timeFrom = Convert.ToDateTime(_timeFrom, culture).AddDays(1);
                            }
                            else
                            {
                                _timeTo = Convert.ToDateTime(timeTo, culture).AddDays(1);
                            }
                        }
                        else
                        {
                            _timeTo = _timeFrom;
                        }
                        clsHis.mistPoint = 0;
                        clsHis.totalPoint = 0;
                        clsHis.TotalQuantityColor = 0;
                        clsHis.TotalMoney = "";
                        double? totalMoney = 0;
                        clsHis.TotalHour = 0;
                        clsHis.TotalQuantityCurling = 0;
                        clsHis.TotalQuantityShine = 0;
                        clsHis.remind = 0;
                        var timeTo2 = _timeTo.AddDays(-1);
                        if (TypeId == 1)
                        {
                            var item = db.Store_Staff_History_Work(_timeFrom, _timeTo, timeTo2, StaffId).FirstOrDefault();
                            if (item != null)
                            {
                                clsHis.TotalQuantityCurling = item.TotalQuanTityUon;
                                clsHis.TotalQuantityColor = item.TotalQuanTityTayAndNhuom;
                                clsHis.TotalQuantityShine = item.TotalQuanTityShineComboAndKid;
                                totalMoney = item.TotalMoney / item.TotalHour;
                                clsHis.TotalMoney = String.Format("{0:#,###}", (totalMoney)).Replace(",", ".");
                                clsHis.totalPoint = item.totalPoint;
                                if (item.totalPoint == null)
                                {
                                    clsHis.totalPoint = 0;
                                }
                                clsHis.mistPoint = item.mistPoint;
                            }
                            else
                            {
                                clsHis = new cls_Staff_History_Work();
                            }
                        }
                        else if (TypeId == 2)
                        {
                            var lstSkinner = db.Store_Staff_GetData_Checkin(_timeFrom, _timeTo, StaffId).FirstOrDefault();
                            if (lstSkinner == null)
                            {
                                clsHis.totalPoint = 0;
                                clsHis.mistPoint = 0;
                            }
                            else
                            {
                                clsHis.totalPoint = lstSkinner.totalPoint;
                                clsHis.mistPoint = lstSkinner.Warning;
                            }
                        }
                        else if (TypeId == 5)
                        {
                            var lstCheckin = db.Store_Staff_GetData_Checkin(_timeFrom, _timeTo, StaffId).FirstOrDefault();
                            if (lstCheckin == null)
                            {
                                clsHis.mistPoint = 0;
                            }
                            else
                            {
                                clsHis.mistPoint = lstCheckin.Warning;
                            }
                        }
                        else if (TypeId == 6)
                        {
                            string sql = "";
                            sql = @"select flow.SellerId as CheckoutId, SUM(flow.Quantity) as TotalSales, (SUM(flow.Price) / Count(flow.Id)) as Sales
                                      from FlowProduct as flow
                                      where 
                                      flow.SellerId = '" + StaffId + @"'
                                      and flow.IsDelete = 0
                                      and flow.CreatedDate between '" + _timeFrom + @"' and '" + _timeTo + @"'
                                      group by flow.SellerId";
                            var queryString = db.Database.SqlQuery<clsCheckout>(sql).FirstOrDefault();
                            if (queryString == null)
                            {
                                clsHis.TotalSales = "";
                                clsHis.Sales = "";
                            }
                            else
                            {
                                clsHis.TotalSales = String.Format("{0:#,###}", (queryString.TotalSales)).Replace(",", ".");
                                clsHis.Sales = String.Format("{0:#,###}", (queryString.Sales)).Replace(",", ".");
                            }
                        }
                    }
                    return clsHis;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Bind data checkin
        /// </summary>
        /// <param name="TimeFrom"></param>
        /// <param name="TimeTo"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        [WebMethod]
        public static string bindDataCheckin(string TimeFrom, string TimeTo, int Id)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var salesCheckin = new List<Store_Staff_WaitTime_Result>();
                    if (TimeFrom != "")
                    {
                        var timeFrom = new DateTime();
                        var timeTo = new DateTime();
                        CultureInfo culture = new CultureInfo("vi-VN");
                        timeFrom = Convert.ToDateTime(TimeFrom, culture);
                        if (TimeTo != "")
                        {
                            if (TimeFrom == TimeTo)
                            {
                                timeFrom = Convert.ToDateTime(timeFrom, culture).AddDays(1);
                            }
                            else
                            {
                                timeTo = Convert.ToDateTime(TimeTo, culture).AddDays(1);
                            }
                        }
                        else
                        {
                            timeTo = timeFrom;
                        }
                        salesCheckin = db.Store_Staff_WaitTime(timeFrom, timeTo, Id).ToList();
                        if (salesCheckin == null)
                        {
                            salesCheckin = new List<Store_Staff_WaitTime_Result>();
                        }
                    }
                    return new JavaScriptSerializer().Serialize(salesCheckin);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Bind 
        /// </summary>
        /// <param name="TimeFrom"></param>
        /// <param name="TimeTo"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        [WebMethod]
        public static object bindHairStyle(string TimeFrom, string TimeTo, int Id)
        {
            try
            {
                IStaffModel modelStaff = new StaffModel();
                var model = modelStaff.BindHairStyle(TimeFrom, TimeTo, Id);
                return model;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Update thông tin nhân viên
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Update_OBJ(object sender, EventArgs e)
        {
            int staffId = 0;
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var _Code = Convert.ToInt32(Request.QueryString["Code"]);
                    var obj = db.Staffs.Where(w => w.Id == _Code).FirstOrDefault();
                    int Error = 0;
                    int integer;
                    if (!obj.Equals(null))
                    {
                        staffId = obj.Id;
                        obj.Fullname = FullName.Text;
                        obj.StaffID = StaffID.Text;
                        obj.DateJoin = !string.IsNullOrEmpty(DateJoin.Text) ? Convert.ToDateTime(DateJoin.Text, culture) : Convert.ToDateTime(null);
                        obj.SkillLevel = Convert.ToInt32(SkillLevel.SelectedValue);
                        obj.OrderCode = OrderCode.Text;
                        obj.Phone = Phone.Text;
                        obj.Email = UserAccount.Text;
                        obj.isAccountLogin = int.TryParse(AccountType.SelectedValue, out integer) ? integer : 0;
                        if (radioSalaryByPerson.SelectedValue == "True")
                        {
                            obj.SalaryByPerson = true;
                        }
                        else
                        {
                            obj.SalaryByPerson = false;
                        }

                        if (radioEnroll.SelectedValue == "True")
                        {
                            obj.RequireEnroll = true;
                        }
                        else
                        {
                            obj.RequireEnroll = false;
                        }
                        // add 20190509
                        if (radioAddionstaff.SelectedValue == "True")
                        {
                            obj.AdditionStaff = true;
                        }
                        else
                        {
                            obj.AdditionStaff = false;
                        }
                        if (HDF_MainImg.Value != "")
                        {
                            obj.Avatar = HDF_MainImg.Value;
                        }
                        else
                        {
                            obj.Avatar = "";
                        }
                        if (Day.Text.Length > 0)
                        {
                            obj.SN_day = int.TryParse(Day.Text, out integer) ? integer : 0;
                        }

                        if (Month.Text.Length > 0)
                        {
                            obj.SN_month = int.TryParse(Month.Text, out integer) ? integer : 0;
                        }
                        if (Year.Text.Length > 0)
                        {
                            obj.SN_year = int.TryParse(Year.Text, out integer) ? integer : 0;
                        }
                        obj.SourceId = Convert.ToInt32(ddlSourceId.SelectedValue);
                        obj.Address = Address.Text;
                        obj.ModifiedDate = DateTime.Now;
                        if (City.SelectedValue != "" || City.SelectedValue != null)
                        {
                            obj.CityId = Convert.ToInt32(City.SelectedValue);
                        }
                        if (District.SelectedValue != "" || District.SelectedValue != null)
                        {
                            obj.DistrictId = Convert.ToInt32(District.SelectedValue);
                        }

                        obj.Gender = Convert.ToByte(Gender.SelectedValue);
                        obj.Type = Convert.ToInt32(TypeStaff.SelectedValue);
                        if (Salon.SelectedValue == "" || Salon.SelectedValue == null)
                        {
                            obj.SalonId = 0;
                        }
                        else
                        {
                            obj.SalonId = Convert.ToInt32(Salon.SelectedValue);
                        }
                        obj.SkinnerIdInGroup = Convert.ToInt32(SkinnerList.SelectedValue);
                        if (LstradiobttList.SelectedValue != null)
                        {
                            obj.Active = Convert.ToByte(LstradiobttList.SelectedValue);
                        }
                        else
                        {
                            obj.Active = 1;
                        }
                        if (obj.Active == 1)
                        {
                            if (authenticationModel.CheckUserExistCognito(UserAccount.Text))
                            {
                                authenticationModel.AdminEnableUser(UserAccount.Text);
                            }
                        }
                        else
                        {
                            if (authenticationModel.CheckUserExistCognito(UserAccount.Text))
                            {
                                authenticationModel.AdminDisableUser(UserAccount.Text);
                            }
                        }
                        //Update New
                        obj.MST = MST.Text.Trim();
                        obj.NameCMT = NameCMT.Text.Trim();
                        obj.NgayTinhThamNien = !string.IsNullOrEmpty(DateTN.Text) ? Convert.ToDateTime(DateTN.Text, culture) : Convert.ToDateTime(null);
                        obj.NganHang_ChiNhanh = NameBank.Text.Trim();
                        obj.NganHang_TenTK = NameAcc.Text.Trim();
                        obj.NganHang_SoTK = NumberTK.Text.Trim();
                        obj.NumberInsurrance = NumberInsurrance.Text.Trim();
                        if (HDF_MainImgCMT1.Value != "")
                        {
                            obj.CMTimg1 = HDF_MainImgCMT1.Value;
                        }
                        else
                        {
                            obj.CMTimg1 = "";
                        }
                        if (HDF_MainImgCMT2.Value != "")
                        {
                            obj.CMTimg2 = HDF_MainImgCMT2.Value;
                        }
                        else
                        {
                            obj.CMTimg2 = "";
                        }
                    }
                    obj.NganHang_TenTK = NameAcc.Text;
                    obj.IDProviderLocale = txtAddressID.Text;
                    //Ngày cấp chứng minh thư

                    if (txtDateID.Text != "" && txtDateID.Text != null)
                    {
                        obj.IDProvidedDate = Convert.ToDateTime(txtDateID.Text, new CultureInfo("vi-VN"));
                    }
                    // Cap nhat hop dong 3 thang
                    DateTime? contractDateThuViec = Convert.ToDateTime(txtThuViec.Text == "" ? null : txtThuViec.Text, new CultureInfo("vi-VN"));
                    DateTime? contractDate3Thang = Convert.ToDateTime(txt3thang.Text == "" ? null : txt3thang.Text, new CultureInfo("vi-VN"));
                    DateTime? contractDate6Thang = Convert.ToDateTime(txt6thang.Text == "" ? null : txt6thang.Text, new CultureInfo("vi-VN"));
                    DateTime? contractDateLaodong = Convert.ToDateTime(txtHDLD.Text == "" ? null : txtHDLD.Text, new CultureInfo("vi-VN"));
                    DateTime? contractDateThuViecQuanLy = Convert.ToDateTime(txtHDTVManager.Text == "" ? null : txtHDTVManager.Text, new CultureInfo("vi-VN"));
                    DateTime? contractDateLaodongQuanLy = Convert.ToDateTime(txtHDLDManager.Text == "" ? null : txtHDLDManager.Text, new CultureInfo("vi-VN"));
                    var resultThuViec = staffContractMap.InsertMapContract(txtThuViec.Text == "" ? null : contractDateThuViec, 4, obj.Id);
                    if (!resultThuViec.success)
                    {
                        var msg = resultThuViec.msg;
                        var status = "msg-system warning";
                        TriggerJsMsgSystem_temp(Page, msg, status, 5000);
                        Error++;
                    }
                    var result3thang = staffContractMap.InsertMapContract(txt3thang.Text == "" ? null : contractDate3Thang, 1, obj.Id);
                    if (!result3thang.success)
                    {
                        var msg = result3thang.msg;
                        var status = "msg-system warning";
                        TriggerJsMsgSystem_temp(Page, msg, status, 5000);
                        Error++;
                    }
                    var result6thang = staffContractMap.InsertMapContract(txt6thang.Text == "" ? null : contractDate6Thang, 2, obj.Id);
                    if (!result6thang.success)
                    {
                        var msg = result6thang.msg;
                        var status = "msg-system warning";
                        TriggerJsMsgSystem_temp(Page, msg, status, 5000);
                        Error++;
                    }
                    var resultLaoDong = staffContractMap.InsertMapContract(txtHDLD.Text == "" ? null : contractDateLaodong, 3, obj.Id);
                    if (!resultLaoDong.success)
                    {
                        var msg = resultLaoDong.msg;
                        var status = "msg-system warning";
                        TriggerJsMsgSystem_temp(Page, msg, status, 5000);
                        Error++;
                    }

                    var resultThuviecQuanLy = staffContractMap.InsertMapContract(txtHDTVManager.Text == "" ? null : contractDateThuViecQuanLy, 5, obj.Id);
                    if (!resultThuviecQuanLy.success)
                    {
                        var msg = resultThuviecQuanLy.msg;
                        var status = "msg-system warning";
                        TriggerJsMsgSystem_temp(Page, msg, status, 5000);
                        Error++;
                    }
                    var resultLaodongQuanLy = staffContractMap.InsertMapContract(txtHDLDManager.Text == "" ? null : contractDateLaodongQuanLy, 6, obj.Id);
                    if (!resultLaodongQuanLy.success)
                    {
                        var msg = resultLaodongQuanLy.msg;
                        var status = "msg-system warning";
                        TriggerJsMsgSystem_temp(Page, msg, status, 5000);
                        Error++;
                    }

                    // Check trùng số điện thoại
                    if (obj.Phone != "")
                    {
                        if (staffModel.IssetPhone(obj.Phone, obj))
                        {
                            var msg = "Số điện thoại đã tồn tại. Bạn vui lòng nhập số điện thoại khác.";
                            var status = "msg-system warning";
                            TriggerJsMsgSystem_temp(Page, msg, status, 5000);
                            Error++;
                        }
                    }

                    // Check trùng user name
                    if (obj.Email != "")
                    {
                        if (staffModel.IssetEmail(obj.Email, obj))
                        {
                            var msg = "Tên đăng nhập đã tồn tại. Bạn vui lòng nhập tên khác.";
                            var status = "msg-system warning";
                            TriggerJsMsgSystem_temp(Page, msg, status, 5000);
                            Error++;
                        }
                    }

                    // Check trùng email
                    if (!String.IsNullOrEmpty(Email.Text))
                    {
                        if (authenticationModel.CheckEmail(Email.Text, obj.Id))
                        {
                            var msg = "E-mail đã tồn tại. Bạn vui lòng nhập E-mail khác.";
                            var status = "msg-system warning";
                            TriggerJsMsgSystem_temp(Page, msg, status, 5000);
                            Error++;
                        }
                    }

                    //Insert new 
                    //Check trùng số tài khoản
                    if (obj.NganHang_SoTK != "")
                    {
                        if (staffModel.IssetBankAccount(obj.NganHang_SoTK, obj))
                        {
                            var msg = " Số tài khoản đã tồn tại. Bạn vui lòng nhập số tài khoản khác!";
                            var status = "msg-system warning";
                            TriggerJsMsgSystem_temp(Page, msg, status, 5000);
                            Error++;
                        }
                    }

                    //Check trùng số CMT
                    if (obj.StaffID != "")
                    {
                        if (staffModel.IssetPersonId(obj.StaffID, obj))
                        {
                            var msg = "Số CMT đã tồn tại. Bạn vui lòng nhập số CMT khác!";
                            var status = "msg-system warning";
                            TriggerJsMsgSystem_temp(Page, msg, status, 5000);
                            Error++;
                        }
                    }

                    //Check trùng MST 
                    if (obj.MST != "")
                    {
                        if (staffModel.IssetTaxCode(obj.MST, obj))
                        {
                            var msg = "Mã số thuế đã tồn tại. Bạn vui lòng nhập MST khác!";
                            var status = "msg-system warning";
                            TriggerJsMsgSystem_temp(Page, msg, status, 5000);
                            Error++;
                        }
                    }

                    //Check trùng Số bảo hiểm xã hội
                    if (obj.NumberInsurrance != "")
                    {
                        if (staffModel.IssetInsurranceNumber(obj.NumberInsurrance, obj))
                        {
                            var msg = "Số bảo hiểm xã hội đã tồn tại. Bạn vui lòng nhập số khác!";
                            var status = "msg-system warning";

                            TriggerJsMsgSystem_temp(Page, msg, status, 5000);
                            Error++;
                        }
                    }

                    if (Error == 0)
                    {
                        IStaffFluctuationModel modelStaffFluc = new StaffFluctuationModel();
                        if (obj.Id > 0 && obj.isAccountLogin != 1)
                        {
                            modelStaffFluc.UpdateStaff(obj);
                        }

                        if (obj.isAccountLogin != 1 && obj.Active == 1)
                        {
                            var user = db.Users.FirstOrDefault(f => f.StaffId == obj.Id);
                            if (user != null)
                            {
                                authenticationModel.DeleteOrDisableMapUserStaff(obj);
                                authenticationModel.AddMapUserStaff(user, obj);
                            }
                        }
                        else if (obj.isAccountLogin != 1 && obj.Active != 1)
                        {
                            authenticationModel.DeleteOrDisableMapUserStaff(obj, true, 1);
                        }


                        authenticationModel.UpdateUser(obj.Id, Email.Text ?? "", obj.Phone, obj.Active ?? 0);

                        db.Staffs.AddOrUpdate(obj);
                        db.SaveChanges();

                        //var exc = db.SaveChanges();
                        // if (exc > 0)
                        //{
                        var serializer = new JavaScriptSerializer();
                        var content = serializer.Serialize(obj);
                        UpdateSalaryConfigStaff(obj);
                        AddLog(obj.Id, "staff", content);
                        //Update permissionStaff
                        UpdatePermissionStaff(obj.Id, HDF_PermissionID.Value);
                        //update profileId

                        UpdateProfileStaff(obj.Id, HDF_Profile_ID.Value);
                        // Log skill level
                        StaffLib.AddLogSkillLevel(obj.Id, obj.SkillLevel.Value, db);
                        // Init flow salary
                        if (this.enrollModel.IsEnroll(obj.Id, DateTime.Now.Date))
                        {
                            SalaryLib.Instance.forUpdate_updateSalaryByStaff(obj, obj.CreatedDate.Value.Date, obj.CreatedDate.Value.Date);
                        }
                        var MsgParam = new List<KeyValuePair<string, string>>();
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                        MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thành công!"));
                        UIHelpers.Redirect("/admin/nhan-vien/" + obj.Id + ".html", MsgParam);
                        // }
                    }
                    else
                    {
                        //var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                        //var status = "msg-system warning";
                        //var MsgParam = new List<KeyValuePair<string, string>>();
                        //MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "warning"));
                        //MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!"));
                        //UIHelpers.Redirect("/admin/nhan-vien/" + obj.Id + ".html");
                    }
                }
            }
            catch (Exception ex)
            {
                TriggerJsMsgSystem_temp(Page, "Đã có lỗi xảy ra. vui lòng liên hệ nhà phát triển", "msg-system warning", 5000);
            }
        }

        public void TriggerJsMsgSystem_temp(Page _OBJ, string msg, string status, int duration)
        {
            ScriptManager.RegisterStartupScript(_OBJ, _OBJ.GetType(), "Message System", "showMsgSystem('" + msg + "','" + status + "'," + duration + ");", true);
        }
        /// <summary>
        /// Update PermissionStaff
        /// Author: QuynhDD
        /// </summary>
        public void UpdatePermissionStaff(int staffId, string permissionId)
        {
            try
            {
                if (staffId > 0)
                {
                    List<Input_Permision> list = new List<Input_Permision>();
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    list = serializer.Deserialize<List<Input_Permision>>(permissionId);
                    using (var db = new Solution_30shineEntities())
                    {
                        var data = db.PermissionStaffs.Where(w => w.StaffId == staffId).ToList();
                        var index = -1;
                        var item = new PermissionStaff();
                        // kiểm tra xóa item
                        if (data.Count > 0)
                        {
                            foreach (var v in data)
                            {
                                index = list.FindIndex(w => w.PermissionId == v.PermissionId);
                                if (index == -1)
                                {
                                    v.IsDelete = true;
                                    v.ModifiedTime = DateTime.Now;
                                    db.SaveChanges();
                                }
                                else
                                {
                                    v.ModifiedTime = DateTime.Now;
                                    v.IsDelete = false;
                                    db.SaveChanges();
                                }
                            }
                        }
                        if (list.Count > 0)
                        {
                            foreach (var v in list)
                            {

                                index = data.FindIndex(w => w.PermissionId == v.PermissionId);
                                if (index == -1)
                                {
                                    item = new PermissionStaff();
                                    item.StaffId = staffId;
                                    item.PermissionId = v.PermissionId;
                                    item.CreatedTime = DateTime.Now;
                                    item.IsActive = true;
                                    item.IsDelete = false;
                                    db.PermissionStaffs.Add(item);
                                }
                            }

                            db.SaveChanges();
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Update 
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name="profileId"></param>
        public void UpdateProfileStaff(int staffId, string value)
        {
            try
            {
                if (staffId > 0)
                {
                    List<Input_Profile> list = new List<Input_Profile>();
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    list = serializer.Deserialize<List<Input_Profile>>(value);
                    using (var db = new Solution_30shineEntities())
                    {
                        var data = db.StaffProfileMaps.Where(w => w.StaffId == staffId).ToList();
                        var index = -1;
                        var item = new StaffProfileMap();
                        if (data.Count > 0)
                        {
                            foreach (var v in data)
                            {
                                index = list.FindIndex(w => w.ProfileId == v.StatusProfileId);
                                if (index == -1)
                                {
                                    v.IsDelete = true;
                                    v.ModifiedTime = DateTime.Now;
                                    db.SaveChanges();
                                }
                                else
                                {
                                    v.ModifiedTime = DateTime.Now;
                                    v.IsDelete = false;
                                    db.SaveChanges();
                                }
                            }
                        }
                        if (list.Count > 0)
                        {
                            foreach (var v in list)
                            {
                                index = data.FindIndex(w => w.StatusProfileId == v.ProfileId);
                                if (index == -1)
                                {
                                    item = new StaffProfileMap();
                                    item.StaffId = staffId;
                                    item.StatusProfileId = v.ProfileId;
                                    item.IsDelete = false;
                                    item.CreatedTime = DateTime.Now;
                                    db.StaffProfileMaps.Add(item);
                                }
                            }

                            db.SaveChanges();
                        }
                    }
                }
            }
            catch
            {

                throw;
            }
        }
        /// <summary>
        /// Update salaryConfigStaff
        /// </summary>
        /// <param name="staff"></param>
        private void UpdateSalaryConfigStaff(Staff staff)

        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    if (staff != null && staff.Id > 0)
                    {
                        SalaryConfigStaff record = db.SalaryConfigStaffs.FirstOrDefault(s => s.StaffId == staff.Id && s.IsDeleted == false);
                        if (record != null)
                        {
                            record.StaffId = staff.Id;
                            record.SalonId = staff.SalonId;
                            record.FixSalaryOscillation = 0;
                            SalaryConfig salaryConfig = db.SalaryConfigs.FirstOrDefault(w => w.DepartmentId == staff.Type && w.LevelId == staff.SkillLevel);
                            if (salaryConfig != null)
                            {
                                record.SalaryConfigId = salaryConfig.Id;
                                record.ModifiedTime = DateTime.Now;
                                db.SalaryConfigStaffs.AddOrUpdate(record);
                                db.SaveChanges();
                            }
                        }
                        else
                        {
                            record = new SalaryConfigStaff();
                            record.StaffId = staff.Id;
                            record.SalonId = staff.SalonId;
                            record.FixSalaryOscillation = 0;
                            SalaryConfig salaryConfig = db.SalaryConfigs.FirstOrDefault(w => w.DepartmentId == staff.Type && w.LevelId == staff.SkillLevel);
                            if (salaryConfig != null)
                            {
                                record.SalaryConfigId = salaryConfig.Id;
                                record.CreatedTime = DateTime.Now;
                                record.IsDeleted = false;
                                db.SalaryConfigStaffs.Add(record);
                                db.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch
            { }
        }

        /// <summary>
        /// Add Log
        /// </summary>
        /// <param name="objId"></param>
        /// <param name="type"></param>
        /// <param name="content"></param>
        private void AddLog(int objId, string type, string content)
        {

            using (var db = new Solution_30shineEntities())
            {

                var obj = new Log();
                obj.OBJId = objId;
                obj.Type = type;
                obj.LogTime = DateTime.Now;
                obj.Content = content;
                obj.Status = 1;
                db.Logs.Add(obj);
                db.SaveChanges();

            }

        }

        /// <summary>
        /// Check permission
        /// </summary>
        private void CheckPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "mode1", "salonmanager", "reception", "staff", "checkin", "checkout" };
                string[] AllowEdit = new string[] { "root", "admin", "salonmanager", "accountant" };
                string[] AllowAllPermission = new string[] { "root", "admin", "mode1" };
                string[] AllowActionMenu = new string[] { "root", "admin", "salonmanager" };
                string[] AllowTypeStaff = new string[] { "root", "admin", "mode1", "salonmanager" };
                var Permission = Session["User_Permission"].ToString().Trim();
                var Code = Request.QueryString["Code"] != "" ? Convert.ToInt32(Request.QueryString["Code"]) : 0;
                int UserId = Convert.ToInt32(Session["User_Id"]);
                var UserSalonId = Convert.ToInt32(Session["SalonId"]);

                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }
                if (Array.IndexOf(AllowEdit, Permission) != -1)
                {
                    Perm_Edit = true;
                }
                if (Array.IndexOf(AllowAllPermission, Permission) != -1)
                {
                    Perm_AllPermission = true;
                }
                if (Array.IndexOf(AllowActionMenu, Permission) != -1)
                {
                    Perm_ActionMenu = true;
                }
                if (Array.IndexOf(AllowTypeStaff, Permission) != -1)
                {
                    Perm_TypeStaff = true;
                }
                if (Code == UserId)
                {
                    Perm_HidePermission = true;
                }

                using (var db = new Solution_30shineEntities())
                {
                    var obj = db.Staffs.FirstOrDefault(w => w.Id == Code);
                    if (obj != null && Array.IndexOf(Allow, obj.Permission) != -1)
                    {
                        /// Check quyền truy cập
                        /// Đối với quyền từ salonmanager trở lên, quyền cao hơn có thể truy cập xem quyền thấp hơn
                        /// Đối với quyền reception, staff, không được truy cập xem các nhân viên khác
                        if (Array.IndexOf(Allow, Session["User_Permission"]) <= Array.IndexOf(Allow, "salonmanager"))
                        {
                            if (Array.IndexOf(Allow, obj.Permission) <= Array.IndexOf(Allow, Session["User_Permission"]))
                            {
                                if (obj.Id != UserId)
                                {
                                    Perm_Access = false;
                                }
                            }
                            // Set quyền edit đối với nhân viên có quyền thấp hơn
                            //if (Array.IndexOf(Allow, obj.Permission) >= Array.IndexOf(Allow, Session["User_Permission"]))
                            //{
                            //    Perm_Edit = true;
                            //}
                            // Từ chối truy cập xem nhân viên ở salon khác đối với quyền salonmanager
                            if (Array.IndexOf(Allow, Session["User_Permission"]) >= Array.IndexOf(Allow, "salonmanager") && obj.SalonId != UserSalonId)
                            {
                                Perm_Access = false;
                            }
                        }
                        else
                        {
                            if (obj.Id != UserId)
                            {
                                Perm_Access = false;
                            }
                        }
                    }
                }
            }

            // Call execute function
            ExecuteByPermission();
        }
        /// <summary>
        /// Gửi thông tin cảnh báo về client
        /// </summary>
        /// <param name="message"></param>
        //public void WaringClient(string message)
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Show message", "showMsgSystem('" + message + "','" + "false" + "');showDom('#StaffInfoWrap', true);", true);
        //}
        /// <summary>
        /// GendPassword
        /// </summary>
        /// <param name="md5Hash"></param>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string GenPassword(MD5 md5Hash, string input)
        {
            var scrPush = "uaefsfkoiu&@(^%=";
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input + scrPush));
            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }

        public static MD5 md5Hash;
        /// <summary>
        /// reset password
        /// </summary>
        /// <param name="_Id"></param>
        /// <returns></returns>
        [WebMethod]
        public static string ResetPassWord(int? _Id)
        {
            string str = "";
            using (var db = new Solution_30shineEntities())
            {

                var obj = db.Staffs.Where(x => x.Id == _Id && x.IsDelete == 0).FirstOrDefault();
                using (MD5 md5Hash = MD5.Create())
                {
                    obj.Password = GenPassword(md5Hash, Libraries.AppConstants.PASSWORD_DEFAULT);
                    db.SaveChanges();
                }
                str = "success";

            }
            return str;
        }
        [WebMethod]
        public static object chkLDQL_Click(int staffId, bool isCheck)
        {
            var result = new Msg();
            try
            {

                int typeLDQL = 6;
                using (var db = new Solution_30shineEntities())
                {
                    var staff = db.StaffContractMaps.Where(r => r.StaffId == staffId && r.ContractId == typeLDQL).FirstOrDefault();
                    if (staff != null)
                    {
                        staff.IsReceived = isCheck;
                        db.StaffContractMaps.AddOrUpdate(staff);
                        db.SaveChanges();
                        result.success = true;
                        result.msg = "Cập nhật thành công";
                    }
                    else
                    {
                        result.success = false;
                        result.msg = "Không tìm thấy hợp đồng";
                    }
                }
            }
            catch (Exception ex)
            {
                result.success = false;
                result.msg = "Đã có lỗi xảy ra";
            }
            return result;
        }

        [WebMethod]
        public static object chkLDSL_Click(int staffId, bool isCheck)
        {
            var result = new Msg();
            try
            {
                int typeLDSL = 3;
                using (var db = new Solution_30shineEntities())
                {
                    var staff = db.StaffContractMaps.Where(r => r.StaffId == staffId && r.ContractId == typeLDSL).FirstOrDefault();
                    if (staff != null)
                    {
                        staff.IsReceived = isCheck;
                        db.StaffContractMaps.AddOrUpdate(staff);
                        db.SaveChanges();
                        result.success = true;
                        result.msg = "Cập nhật thành công";
                    }
                    else
                    {
                        result.success = false;
                        result.msg = "Không tìm thấy hợp đồng";
                    }
                }
            }
            catch (Exception ex)
            {
                result.success = false;
                result.msg = "Đã có lỗi xảy ra";
            }
            return result;
        }
        public class StaffProfile : _30shine.MODEL.ENTITY.EDMX.Staff
        {
            public string TypeName { get; set; }
            public string SalonName { get; set; }
        }

        public struct Staff_TimeKeeping
        {
            public int Id { get; set; }
            public string Fullname { get; set; }
            public int Type { get; set; }
            public int IdShowroom { get; set; }
            public List<int> ServiceIds { get; set; }
            public List<ServiceStatistic> _ServiceStatistic { get; set; }
            public List<ProductStatistic> _ProductStatistic { get; set; }
            public int TotalService { get; set; }
            public string StrDataService { get; set; }
            public DateTime CreatedDate { get; set; }
        }

        public struct ServiceStatistic
        {
            public int ServiceId { get; set; }
            public int ServiceName { get; set; }
            public int ServiceTimes { get; set; }
        }

        public struct ProductStatistic
        {
            public int ProductId { get; set; }
            public int ProductName { get; set; }
            public int ProductTimes { get; set; }
        }

        public struct BillServiceJoin
        {
            public int Id { get; set; }
            public string Fullname { get; set; }
            public int Type { get; set; }
            public int IdShowroom { get; set; }
            public int BillIsDelete { get; set; }
            public int BillId { get; set; }
            public DateTime BillCreatedDate { get; set; }
        }

        public class cls_staff : Staff
        {
            public string TenTinhThanh { get; set; }
            public string TenQuanHuyen { get; set; }
            public string salonName { get; set; }
            public string skillLevelName { get; set; }
            public string Department { get; set; }
            public string Log { get; set; }
        }
        public class cls_Recruitment
        {
            /// <summary>
            /// Test Văn hóa
            /// </summary>
            public int pointResult { get; set; }
            public int totalPointResult { get; set; }
            public int testResult { get; set; }
            public bool checkResult { get; set; }

            /// <summary>
            /// Ảnh
            /// </summary>
            public string ImageResult1 { get; set; }
            public string ImageResult2 { get; set; }

            /// <summary>
            /// Test lý thuyết cắt
            /// </summary>
            public int pointResult1 { get; set; }
            public int totalPointResult1 { get; set; }
            public int testResult1 { get; set; }
            public bool checkResult1 { get; set; }

            /// <summary>
            /// Lý thuyết hóa chất
            /// </summary>
            public int pointResult2 { get; set; }
            public int totalPointResult2 { get; set; }
            public int testResult2 { get; set; }
            public bool checkResult2 { get; set; }

            public string linkShampoo { get; set; }
        }

        /// <summary>
        /// Skill name
        /// </summary>
        public class cls_skillname
        {
            public int IdSkill { get; set; }
            public string skillName { get; set; }
        }
        public class cls_SkillLevel
        {
            public int Id { get; set; }
            public string levelName { get; set; }
            public string description { get; set; }
        }
        public partial class cls_Staff_History_Work
        {
            public int StylistId { get; set; }
            public Nullable<int> ServiceId { get; set; }
            public Nullable<int> TotalQuantityCurling { get; set; }
            public Nullable<int> TotalQuantityColor { get; set; }
            public Nullable<int> TotalQuantityShine { get; set; }
            public string TotalMoney { get; set; }
            public Nullable<int> mistPoint { get; set; }
            public string TotalSales { get; set; }
            public string Sales { get; set; }

            public Nullable<int> remind { get; set; }
            public Nullable<double> totalPoint { get; set; }
            public Nullable<double> TotalHour { get; set; }
        }
        public class clsCheckout
        {
            public Nullable<int> CheckoutId { get; set; }
            public Nullable<int> TotalSales { get; set; }
            public Nullable<int> Sales { get; set; }
        }

        /// <summary>
        /// Input
        /// Author: QuynhDD
        /// </summary>
        public class Input_Permision
        {
            public int PermissionId { get; set; }
        }
        public class Input_Profile
        {
            public int ProfileId { get; set; }
        }
        protected void btnThuViec_Click(object sender, EventArgs e)
        {
            try
            {
                Helpers.Msg result = GetObjContract(4);
                if (result.success)
                {
                    PrintContract((contract_cls)result.data);
                }
                else
                {
                    TriggerJsMsgSystem_temp(Page, result.msg, "msg-system warning", 5000);
                    return;
                }
            }
            catch (Exception ex)
            {
                TriggerJsMsgSystem_temp(Page, "Đã có lỗi xảy ra. vui lòng liên hệ nhà phát triển (" + ex.Message.Replace("'", "") + ")", "msg-system warning", 15000);
            }
        }
        protected void btn3thang_Click(object sender, EventArgs e)
        {
            try
            {
                Helpers.Msg result = GetObjContract(1);
                if (result.success)
                {
                    PrintContract((contract_cls)result.data);
                }
                else
                {
                    TriggerJsMsgSystem_temp(Page, result.msg, "msg-system warning", 5000);
                    return;
                }
            }
            catch (Exception ex)
            {
                TriggerJsMsgSystem_temp(Page, "Đã có lỗi xảy ra. vui lòng liên hệ nhà phát triển (" + ex.Message.Replace("'", "") + ")", "msg-system warning", 15000);
            }
        }

        protected void btn6thang_Click(object sender, EventArgs e)
        {
            try
            {
                Helpers.Msg result = GetObjContract(2);
                if (result.success)
                {
                    PrintContract((contract_cls)result.data);
                }
                else
                {
                    TriggerJsMsgSystem_temp(Page, result.msg, "msg-system warning", 5000);
                    return;
                }
            }
            catch (Exception ex)
            {
                TriggerJsMsgSystem_temp(Page, "Đã có lỗi xảy ra. vui lòng liên hệ nhà phát triển (" + ex.Message.Replace("'", "") + ")", "msg-system warning", 15000);
            }
        }

        protected void btnHDLD_Click(object sender, EventArgs e)
        {
            try
            {
                Helpers.Msg result = GetObjContract(3);
                if (result.success)
                {
                    PrintContract((contract_cls)result.data);
                }
                else
                {
                    TriggerJsMsgSystem_temp(Page, result.msg, "msg-system warning", 5000);
                    return;
                }
            }
            catch (Exception ex)
            {
                TriggerJsMsgSystem_temp(Page, "Đã có lỗi xảy ra. vui lòng liên hệ nhà phát triển (" + ex.Message.Replace("'", "") + ")", "msg-system warning", 15000);
            }
        }
        protected void btnLDQL_Click(object sender, EventArgs e)
        {
            try
            {
                Helpers.Msg result = GetObjContract(6);
                if (result.success)
                {
                    PrintContract((contract_cls)result.data);
                }
                else
                {
                    TriggerJsMsgSystem_temp(Page, result.msg, "msg-system warning", 5000);
                    return;
                }

            }
            catch (Exception ex)
            {
                TriggerJsMsgSystem_temp(Page, "Đã có lỗi xảy ra. vui lòng liên hệ nhà phát triển (" + ex.Message.Replace("'", "") + ")", "msg-system warning", 15000);
            }
        }
        protected void btnTVQL_Click(object sender, EventArgs e)
        {
            try
            {
                Helpers.Msg result = GetObjContract(5);
                if (result.success)
                {
                    PrintContract((contract_cls)result.data);
                }
                else
                {
                    TriggerJsMsgSystem_temp(Page, result.msg, "msg-system warning", 5000);
                    return;
                }
            }
            catch (Exception ex)
            {
                TriggerJsMsgSystem_temp(Page, "Đã có lỗi xảy ra. vui lòng liên hệ nhà phát triển (" + ex.Message.Replace("'", "") + ")", "msg-system warning", 15000);
            }
        }
        public void PrintContract(contract_cls objContract)
        {
            try
            {
                if (objContract != null)
                {
                    string domainPhysical = Server.MapPath("~");
                    string pathFoldelPhysical = @"\Public\Word\";
                    string domainUrl = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Host;
                    string pathFoldelUrl = @"/Public/Word/";
                    string fileNameNew = objContract.StaffId + "-" + objContract.DeparmentId + "-" + Guid.NewGuid() + ".docx";
                    string[] pathTemp = objContract.PathContractTemp.Split('/');
                    string TempleFileCurrent = pathTemp.Length > 0 ? pathTemp[pathTemp.Length - 1] : "";
                    //Kiem tra neu khong ton tai thu muc thi tao thu muc moi
                    if (!Directory.Exists(domainPhysical + pathFoldelPhysical))
                    {
                        Directory.CreateDirectory(domainPhysical + pathFoldelPhysical);
                    }
                    if (TempleFileCurrent != "")
                    {
                        var webClient = new WebClient();
                        byte[] docxBytes = webClient.DownloadData(objContract.PathContractTemp);
                        using (MemoryStream ms = new MemoryStream(docxBytes, true))
                        {
                            ms.Write(docxBytes, 0, docxBytes.Length);
                            using (WordprocessingDocument wdDoc = WordprocessingDocument.Open(ms, true))
                            {
                                wdDoc.SaveAs(domainPhysical + pathFoldelPhysical + fileNameNew).Dispose();
                            }
                        }
                        using (WordprocessingDocument wdDoc = WordprocessingDocument.Open(domainPhysical + pathFoldelPhysical + fileNameNew, true))
                        {
                            DocumentFormat.OpenXml.Wordprocessing.Document document = wdDoc.MainDocumentPart.Document;
                            DocumentFormat.OpenXml.Wordprocessing.Body body = wdDoc.MainDocumentPart.Document.Body;
                            // Handle the formatting changes.
                            List<DocumentFormat.OpenXml.Wordprocessing.Text> changes = body.Descendants<DocumentFormat.OpenXml.Wordprocessing.Text>().ToList();
                            foreach (var temp in changes)
                            {
                                switch (temp.InnerText)
                                {
                                    case @"so_hop_dong": temp.Text = objContract.SoHD; break;
                                    case @"ngay_thang_nam_hop_dong": temp.Text = objContract.DateContract; break;
                                    case @"thoi_gian_dao_tao": temp.Text = objContract.UntilDateContract; break;
                                    case @"ho_ten_NLD": temp.Text = objContract.Name.ToUpper(); break;
                                    case @"ho_ten_sign_NLD": temp.Text = objContract.Name.ToUpper(); break;
                                    case @"ngay_sinh_NLD": temp.Text = objContract.BirthDay; break;
                                    case @"dia_chi_NLD": temp.Text = objContract.Address; break;
                                    case @"so_CMT_NLD": temp.Text = objContract.IdentifyCard.ToString(); break;
                                    case @"ngay_cap_CMT_NLD": temp.Text = objContract.DateIdentifyCard; break;
                                    case @"dia_chi_cap_CMT_NLD": temp.Text = objContract.AddressIdentifyCard; break;
                                    case @"so_dien_thoai_NLD": temp.Text = objContract.NumberPhone; break;
                                }
                            }
                        }
                        var portUrl = Request.Url.Port;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Download Word", "CallBackExportExcel('" + domainUrl + ":" + portUrl + pathFoldelUrl + fileNameNew + "');", true);
                    }
                    else
                    {
                        TriggerJsMsgSystem_temp(Page, "Không tìm thấy hợp đồng mẫu", "msg-system warning", 5000);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public Helpers.Msg GetObjContract(int contractType)
        {
            var result = new Helpers.Msg();
            try
            {
                int interger = 0;
                int staffId = Int32.TryParse(Request.QueryString["Code"], out Code) ? Code : 0;
                string salonName = Salon.SelectedItem.Text;
                int day = int.TryParse(Day.Text, out interger) ? interger : 0;
                int month = int.TryParse(Month.Text, out interger) ? interger : 0;
                int year = int.TryParse(Year.Text, out interger) ? interger : 0;
                int DeparmentId = int.TryParse(TypeStaff.SelectedValue, out interger) ? interger : 0;
                var objcontract = new contract_cls();
                objcontract.StaffId = staffId;
                if (contractType == 5 || contractType == 6)
                {
                    DeparmentId = 9;
                }
                objcontract.NumberPhone = Phone.Text;
                objcontract.Address = Address.Text == null ? "" : Address.Text;
                objcontract.AddressIdentifyCard = txtAddressID.Text;
                objcontract.BirthDay = day + "." + month + "." + year;
                objcontract.DateIdentifyCard = txtDateID.Text;
                objcontract.DeparmentId = DeparmentId;
                objcontract.IdentifyCard = StaffID.Text;
                objcontract.Name = FullName.Text;

                DateTime DateContract = new DateTime();
                TextBox objTextContract = new TextBox();
                switch (contractType)
                {
                    case 1:
                        objTextContract = txt3thang;
                        DateContract = Convert.ToDateTime(objTextContract.Text, new CultureInfo("vi-VN"));
                        objcontract.UntilDateContract = "03 tháng kể từ ngày ký";
                        break;
                    case 2:
                        objTextContract = txt6thang;
                        DateContract = Convert.ToDateTime(objTextContract.Text, new CultureInfo("vi-VN"));
                        objcontract.UntilDateContract = "03-06 tháng kể từ ngày ký";
                        break;
                    case 3:
                        objTextContract = txtHDLD;
                        DateContract = Convert.ToDateTime(objTextContract.Text, new CultureInfo("vi-VN"));
                        objcontract.UntilDateContract = "Ngày " + DateContract.Day + " tháng " + DateContract.Month + " năm " + DateContract.Year + " đến " +
                                                        " Ngày " + DateContract.Day + " tháng " + DateContract.Month + " năm " + (DateContract.Year + 1);
                        break;
                    case 4:
                        objTextContract = txtThuViec;
                        DateContract = Convert.ToDateTime(objTextContract.Text, new CultureInfo("vi-VN"));
                        objcontract.UntilDateContract = "02 tháng kể từ ngày " + string.Format("{0:dd/MM/yyyy}", DateContract) + " đến ngày " + string.Format("{0:dd/MM/yyyy}", DateContract.AddMonths(2));
                        break;
                    case 5:
                        objTextContract = txtHDTVManager;
                        DateContract = Convert.ToDateTime(objTextContract.Text, new CultureInfo("vi-VN"));
                        objcontract.UntilDateContract = "02 tháng kể từ ngày " + string.Format("{0:dd/MM/yyyy}", DateContract) + " đến ngày " + string.Format("{0:dd/MM/yyyy}", DateContract.AddMonths(2));
                        break;
                    case 6:
                        objTextContract = txtHDLDManager;
                        DateContract = Convert.ToDateTime(objTextContract.Text, new CultureInfo("vi-VN"));
                        objcontract.UntilDateContract = "Ngày " + DateContract.Day + " tháng " + DateContract.Month + " năm " + DateContract.Year + " đến " +
                                                        " Ngày " + DateContract.Day + " tháng " + DateContract.Month + " năm " + (DateContract.Year + 2);

                        break;
                }
                objcontract.SoHD = staffId + "-" + DateContract.Year;
                objcontract.DateContract = "ngày " + DateContract.Day + " tháng " + DateContract.Month + " năm " + DateContract.Year;
                var resultPath = staffModel.GetPathContractTemp(DeparmentId, contractType);
                if (resultPath.success)
                {
                    objcontract.PathContractTemp = (string)resultPath.data;
                }
                else
                {
                    result.success = false;
                    result.msg = "Không tìm thấy hợp đồng mẫu";
                    return result;
                }
                result.success = true;
                result.msg = "thành công";
                result.data = objcontract;
            }
            catch (Exception ex)
            {
                result.success = false;
                result.msg = "Đã có lỗi xảy ra (" + ex.Message + ")";
            }
            return result;
        }
        public class contract_cls
        {
            public string SoHD { get; set; }
            public string NumberPhone { get; set; }
            public int StaffId { get; set; }
            public string DateContract { get; set; }
            public string Name { get; set; }
            public string BirthDay { get; set; }
            public string Address { get; set; }
            public string IdentifyCard { get; set; }
            public string DateIdentifyCard { get; set; }
            public string AddressIdentifyCard { get; set; }
            public int DeparmentId { get; set; }
            public string PathContractTemp { get; set; }
            //Thời gian đào tạo hợp đồng. 3 hoặc 6 tháng
            public string UntilDateContract { get; set; }
        }

    }
}
