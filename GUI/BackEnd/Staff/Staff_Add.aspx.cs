﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using _30shine.Helpers;
using System.Data.Entity.Migrations;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using _30shine.MODEL.BO;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;
using _30shine.MODEL.IO;

namespace _30shine.GUI.BackEnd.UIStaff
{
    public partial class Staff_Add : System.Web.UI.Page
    {
        private string PageID = "CC_ADD_NV";
        public UIHelpers _UIHelpers = new UIHelpers();
        private bool Perm_Access = false;
        private bool Perm_AllPermission = false;

        protected bool HasImages = false;
        protected List<string> ListImagesUrl = new List<string>();
        protected string[] ListImagesName;
        IStaffModel staffModel = new StaffModel();
        IAuthenticationModel authenticationModel = new AuthenticationModel();

        /// <summary>
        /// check permission
        /// </summary>
        protected void SetPermission()
        {

            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {

                //IPermissionModel permissionModel = new PermissionModel();
                //var permission = Session["User_Permission"].ToString();
                //Perm_Access = permissionModel.GetActionByActionNameAndPageId("Perm_Access", PageID, permission);
                ////Perm_HidePermission = permissionModel.GetActionByActionNameAndPageId("Perm_HidePermission", PageID, permission);
                ////Perm_Add = permissionModel.GetActionByActionNameAndPageId("Perm_Add", PageID, permission);
                //Perm_AllPermission = permissionModel.GetActionByActionNameAndPageId("Perm_AllPermission", PageID, permission);
                //ExecuteByPermission();
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                pageId = permissionModel.GetRegexPageIdSpecial().Replace(pageId, ".html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_AllPermission = permissionModel.CheckPermisionByAction("Perm_AllPermission", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();
            }
        }
        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
            if (!Perm_AllPermission)
            {
                TrSalon.Visible = false;
            }
        }

        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            SetPermission();

            if (!IsPostBack)
            {
                Bind_SkillLevel();
                Bind_TypeStaff();
                //Bind_Permission();
                Bind_Gender();
                Library.Function.bindSalon(new List<DropDownList>() { Salon }, Perm_AllPermission);

                FillTinhThanh();
                FillQuanHuyen();
            }
        }

        /// <summary>
        /// Bind Skill Level
        /// </summary>
        public void Bind_SkillLevel()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Tbl_SkillLevel.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Order).ToList();
                var Key = 0;
                SkillLevel.DataTextField = "SkillLevel";
                SkillLevel.DataValueField = "Value";
                ListItem item = new ListItem("Chọn bậc kỹ năng", "0");
                SkillLevel.Items.Insert(Key, item);
                Key++;

                if (lst.Count > 0)
                {
                    foreach (var v in lst)
                    {
                        item = new ListItem(v.Name, v.Id.ToString());
                        SkillLevel.Items.Insert(Key, item);
                        Key++;
                    }
                }
            }
        }

        /// <summary>
        /// Bind Department
        /// </summary>
        public void Bind_TypeStaff()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Order).ToList();
                var Key = 0;
                var Count = LST.Count;
                TypeStaff.DataTextField = "TypeStaff";
                TypeStaff.DataValueField = "Id";
                ListItem item = new ListItem("Chọn bộ phận", "0");
                TypeStaff.Items.Insert(Key, item);
                Key++;

                if (Count > 0)
                {
                    foreach (var v in LST)
                    {
                        item = new ListItem(v.Name, v.Id.ToString());
                        TypeStaff.Items.Insert(Key, item);
                        Key++;
                    }
                }
            }
        }

        /// <summary>
        /// Bind Permission
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static object BindPermission()
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (var db = new Solution_30shineEntities())
                {
                    var list = (from c in db.PermissionErps.AsNoTracking()
                                where c.IsDelete == false && c.IsActive == true
                                select c
                                ).ToList();

                    return serializer.Serialize(list);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        /// <summary>
        /// Bind profile 
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static object BindListProfile()
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                using (var db = new Solution_30shineEntities())
                {
                    var list = (from c in db.Tbl_Status.AsNoTracking()
                                where c.IsDelete == false && c.Publish == true
                                && c.ParentId == 19
                                select c
                                ).ToList();

                    return serializer.Serialize(list);
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Bind sex
        /// </summary>
        private void Bind_Gender()
        {
            Gender.DataTextField = "TypeStaff";
            Gender.DataValueField = "Id";
            ListItem item = new ListItem("Chọn giới tính", "0");
            Gender.Items.Insert(0, item);
            item = new ListItem("Nam", "1");
            Gender.Items.Insert(1, item);
            item = new ListItem("Nữ", "2");
            Gender.Items.Insert(2, item);
            item = new ListItem("Khác", "3");
            Gender.Items.Insert(3, item);
        }

        /// <summary>
        /// Bind tỉnh thành
        /// </summary>
        protected void FillTinhThanh()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.TinhThanhs.OrderBy(p => p.ThuTu).ToList();

                City.DataTextField = "TenTinhThanh";
                City.DataValueField = "Id";
                City.SelectedIndex = 0;

                City.DataSource = lst;
                City.DataBind();
            }
        }

        /// <summary>
        /// Bind quận huyện
        /// </summary>
        protected void FillQuanHuyen()
        {
            using (var db = new Solution_30shineEntities())
            {
                int _TinhThanhID = Convert.ToInt32(City.SelectedValue);
                if (_TinhThanhID != 0)
                {
                    var lst = db.QuanHuyens.Where(p => p.TinhThanhID == _TinhThanhID).OrderBy(p => p.ThuTu).ToList();

                    District.DataTextField = "TenQuanHuyen";
                    District.DataValueField = "Id";
                    District.SelectedIndex = 0;

                    District.DataSource = lst;
                    District.DataBind();
                }
            }
        }

        /// <summary>
        /// Selected Index
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlFTinhThanh_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillQuanHuyen();
        }

        /// <summary>
        /// Insert Staff
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AddStaff(object sender, EventArgs e)
        {
            using (var db = new Solution_30shineEntities())
            {
                var obj = new _30shine.MODEL.ENTITY.EDMX.Staff();
                int integer;

                obj.Fullname = FullName.Text;
                obj.StaffID = StaffID.Text;
                obj.DateJoin = !string.IsNullOrEmpty(DateJoin.Text) ? Library.Format.getDateTimeFromString(DateJoin.Text) : Convert.ToDateTime(null);
                obj.SkillLevel = int.TryParse(SkillLevel.SelectedValue, out integer) ? integer : 0;
                obj.Phone = Phone.Text;
                obj.Email = UserAccount.Text;
                obj.Avatar = HDF_MainImg.Value;
                obj.isAccountLogin = int.TryParse(AccountType.SelectedValue, out integer) ? integer : 0;
                if (radioSalaryByPerson.SelectedValue == "True")
                {
                    obj.SalaryByPerson = true;
                }
                else
                {
                    obj.SalaryByPerson = false;
                }

                if (radioEnroll.SelectedValue == "True")
                {
                    obj.RequireEnroll = true;
                }
                else
                {
                    obj.RequireEnroll = false;
                }
                // add 20190509
                if (radioAddionstaff.SelectedValue == "True")
                {
                    obj.AdditionStaff = true;
                }
                else
                {
                    obj.AdditionStaff = false;
                }
                if (Day.Text.Length > 0)
                {
                    obj.SN_day = int.TryParse(Day.Text, out integer) ? integer : 0;
                }

                if (Month.Text.Length > 0)
                {
                    obj.SN_month = int.TryParse(Month.Text, out integer) ? integer : 0;
                }
                if (Year.Text.Length > 0)
                {
                    obj.SN_year = int.TryParse(Year.Text, out integer) ? integer : 0;
                }

                obj.Address = Address.Text;
                obj.CreatedDate = DateTime.Now;
                obj.CityId = Convert.ToInt32(City.SelectedValue);
                obj.DistrictId = Convert.ToInt32(District.SelectedValue);
                obj.Gender = Convert.ToByte(Gender.SelectedValue);
                obj.Type = Convert.ToInt32(TypeStaff.SelectedValue);
                obj.MST = MST.Text;
                obj.NameCMT = NameCMT.Text;
                obj.NumberInsurrance = NumberInsurrance.Text;
                obj.CMTimg1 = HDF_MainImgCMT1.Value;
                obj.CMTimg2 = HDF_MainImgCMT2.Value;
                obj.NgayTinhThamNien = !string.IsNullOrEmpty(DateTN.Text) ? Library.Format.getDateTimeFromString(DateTN.Text) : Convert.ToDateTime(null);
                obj.NganHang_ChiNhanh = NameBank.Text;
                obj.NganHang_SoTK = NumberTK.Text;
                obj.NganHang_TenTK = NameAcc.Text;

                obj.IDProviderLocale = txtAddressID.Text;
                if (!Perm_AllPermission)
                {
                    obj.SalonId = Convert.ToInt32(Session["SalonId"]);
                }
                else
                {
                    obj.SalonId = Convert.ToInt32(Salon.SelectedValue);
                }
                obj.Password = "a05b49b7dc71773bc7ff4ef877824ffe";
                obj.IsDelete = 0;
                obj.Active = 1;
                // Check set permission
                int Error = 0;
                //Ngày cấp chứng minh thư
                if (txtDateID.Text != "" && txtDateID.Text != null)
                {
                    try
                    {
                        obj.IDProvidedDate = Convert.ToDateTime(txtDateID.Text, new CultureInfo("vi-VN"));
                    }
                    catch (Exception ex)
                    {
                        var msg = "Ngày chứng minh thư không hợp lệ. vd: 22/11/2018";
                        var status = "msg-system warning";
                        TriggerJsMsgSystem_temp(Page, msg, status, 4000);
                        Error++;
                    }
                }
                // Validate
                // Check trùng số điện thoại
                if (obj.Phone != "")
                {
                    if (staffModel.IssetPhone(obj.Phone))
                    {
                        var msg = "Số điện thoại đã tồn tại. Bạn vui lòng nhập số điện thoại khác.";
                        var status = "msg-system warning";
                        TriggerJsMsgSystem_temp(Page, msg, status, 4000);
                        Error++;
                    }
                }

                //Insert new 
                //Check trùng số tài khoản
                if (obj.NganHang_SoTK != "")
                {
                    if (staffModel.IssetBankAccount(obj.NganHang_SoTK))
                    {
                        var msg = " Số tài khoản đã tồn tại. Bạn vui lòng nhập số tài khoản khác!";
                        var status = "msg-system warning";
                        TriggerJsMsgSystem_temp(Page, msg, status, 4000);
                        Error++;
                    }
                }

                //Check trùng số CMT
                if (obj.StaffID != "")
                {
                    if (staffModel.IssetPersonId(obj.StaffID))
                    {
                        var msg = "Số CMT đã tồn tại. Bạn vui lòng nhập số CMT khác!";
                        var status = "msg-system warning";
                        TriggerJsMsgSystem_temp(Page, msg, status, 4000);
                        Error++;
                    }
                }

                //Check trùng MST 
                if (obj.MST != "")
                {
                    if (staffModel.IssetTaxCode(obj.MST))
                    {
                        var msg = "Mã số thuế đã tồn tại. Bạn vui lòng nhập MST khác!";
                        var status = "msg-system warning";
                        TriggerJsMsgSystem_temp(Page, msg, status, 4000);
                        Error++;
                    }
                }

                //Check trùng Số bảo hiểm xã hội
                if (obj.NumberInsurrance != "")
                {
                    if (staffModel.IssetInsurranceNumber(obj.NumberInsurrance))
                    {
                        var msg = "Số bảo hiểm xã hội đã tồn tại. Bạn vui lòng nhập số khác!";
                        var status = "msg-system warning";

                        TriggerJsMsgSystem_temp(Page, msg, status, 4000);
                        Error++;
                    }
                }

                if (!String.IsNullOrEmpty(UserAccount.Text))
                {
                    var user = authenticationModel.GetByUserName(UserAccount.Text ?? "");
                    if (user != null)
                    {
                        var msg = "Tài khoản nhân viên đã tồn tại. Bạn vui lòng nhập tên tài khoản khác!";
                        var status = "msg-system warning";
                        TriggerJsMsgSystem_temp(Page, msg, status, 4000);
                        Error++;
                    }
                }

                if (!String.IsNullOrEmpty(Email.Text))
                {
                    var user = authenticationModel.GetByEmail(Email.Text ?? "");
                    if (user != null)
                    {
                        var msg = "E-mail đã tồn tại. Bạn vui lòng nhập E-mail khác!";
                        var status = "msg-system warning";
                        TriggerJsMsgSystem_temp(Page, msg, status, 4000);
                        Error++;
                    }
                }

                if (Error == 0)
                {
                    db.Staffs.Add(obj);
                    db.SaveChanges();
                    // Update OrderCode
                    db.Database.ExecuteSqlCommand("update Staff set OrderCode = " + obj.Id + " where Id = " + obj.Id);

                    //create user
                    var user = authenticationModel.AddUser(obj.Id, UserAccount.Text, obj.isAccountLogin ?? 0, Email.Text, Phone.Text);

                    authenticationModel.SignUp(UserAccount.Text, Libraries.AppConstants.PASSWORD_DEFAULT);

                    authenticationModel.AdminConfirmSignUp(UserAccount.Text);

                    authenticationModel.AddMapUserStaff(user, obj);


                    //end

                    ///add salaryConfigStaff
                    AddSalaryConfigStaff(obj);
                    // Log skill level
                    StaffLib.AddLogSkillLevel(obj.Id, obj.SkillLevel.Value, db);
                    // Init flow salary
                    //SalaryLib.Instance.initSalaryByStaffId(obj.Id, obj.CreatedDate.Value.Date, Convert.ToInt32(obj.SalonId));
                    //add PermissionStaff
                    if (HDF_Permission_ID.Value != "[]")
                    {
                        AddPermissionStaff(obj.Id, HDF_Permission_ID.Value);
                    }

                    // add profileId
                    if (HDF_Profile_ID.Value != "[]")
                    {
                        AddStaffProfile(obj.Id, HDF_Profile_ID.Value);
                    }
                    var MsgParam = new List<KeyValuePair<string, string>>();
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_status", "success"));
                    MsgParam.Add(new KeyValuePair<string, string>("msg_update_message", "Thêm mới thành công!"));
                    UIHelpers.Redirect("/admin/nhan-vien/danh-sach.html");
                }
                else
                {
                    var msg = "Thêm mới không thành công. Vui lòng liên hệ với nhóm phát triển!";
                    var status = "msg-system warning";
                    TriggerJsMsgSystem_temp(Page, msg, status, 5000);
                }
            }
        }

        public void TriggerJsMsgSystem_temp(Page _OBJ, string msg, string status, int duration)
        {
            ScriptManager.RegisterStartupScript(_OBJ, _OBJ.GetType(), "Message System", "showMsgSystem('" + msg + "','" + status + "'," + duration + ");", true);
        }

        /// <summary>
        /// Add PermissionStaff
        /// Author: QuynhDD
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name=""></param>
        /// <returns></returns>
        public void AddPermissionStaff(int staffId, string permissionId)
        {
            try
            {
                List<Input_Permision> list = new List<Input_Permision>();
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                list = serializer.Deserialize<List<Input_Permision>>(permissionId);
                if (staffId > 0 && list.Count > 0)
                {
                    using (var db = new Solution_30shineEntities())
                    {
                        foreach (var item in list)
                        {
                            var obj = new PermissionStaff();
                            obj.StaffId = staffId;
                            obj.PermissionId = item.PermissionId;
                            obj.IsActive = true;
                            obj.IsDelete = false;
                            obj.CreatedTime = DateTime.Now;
                            db.PermissionStaffs.Add(obj);
                        }

                        db.SaveChanges();
                    }
                }
            }

            catch { throw; }
        }

        /// <summary>
        /// Add StaffProfile
        /// </summary>
        /// <param name="staffId"></param>
        /// <param name="profileId"></param>
        public void AddStaffProfile(int staffId, string value)
        {
            try
            {
                List<Input_Profile> list = new List<Input_Profile>();
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                list = serializer.Deserialize<List<Input_Profile>>(value);
                if (staffId > 0 && list.Count > 0)
                {
                    using (var db = new Solution_30shineEntities())
                    {
                        foreach (var v in list)
                        {
                            var obj = new StaffProfileMap();
                            obj.StaffId = staffId;
                            obj.StatusProfileId = v.ProfileId;
                            obj.CreatedTime = DateTime.Now;
                            obj.IsDelete = false;
                            db.StaffProfileMaps.Add(obj);
                        }

                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Add salaryConfigStaff
        /// </summary>
        /// <param name="staff"></param>
        private void AddSalaryConfigStaff(Staff staff)
        {
            try
            {
                using (var db = new Solution_30shineEntities())
                {
                    var obj = new SalaryConfigStaff();
                    obj.StaffId = staff.Id;
                    obj.SalonId = staff.SalonId;
                    obj.FixSalaryOscillation = 0;
                    if (obj.StaffId > 0)
                    {
                        SalaryConfig salaryConfig = db.SalaryConfigs.FirstOrDefault(w => w.DepartmentId == staff.Type && w.LevelId == staff.SkillLevel);
                        if (salaryConfig != null)
                        {
                            obj.SalaryConfigId = salaryConfig.Id;
                            var checkStaff = db.SalaryConfigStaffs.FirstOrDefault(f => f.StaffId == obj.StaffId && f.IsDeleted == false);
                            if (checkStaff == null)
                            {
                                obj.CreatedTime = DateTime.Now;
                                obj.IsDeleted = false;
                                db.SalaryConfigStaffs.Add(obj);
                                db.SaveChanges();
                            }
                        }
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// Input
        /// Author: QuynhDD
        /// </summary>
        public class Input_Permision
        {
            public int PermissionId { get; set; }
        }

        /// <summary>
        /// Input
        /// </summary>
        public class Input_Profile
        {
            public int ProfileId { get; set; }
        }
    }
}
