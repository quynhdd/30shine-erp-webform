﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Globalization;
using _30shine.MODEL.Interface;
using _30shine.MODEL.Bussiness;

namespace _30shine.GUI.BackEnd.UIStaff
{
    public partial class Staff_History_Salon : System.Web.UI.Page
    {
        protected Paging PAGING = new Paging();

        protected string THead = "";
        public CultureInfo culture = new CultureInfo("vi-VN");
        protected string FirstDayOfMonth = string.Format("{0:dd/MM/yyyy}", new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
        protected string Today = String.Format("{0:dd/MM/yyyy}", DateTime.Now);

        protected int salonId;
        private int integer;

        private string PageID = "";
        protected bool Perm_ViewAllData = false;
        private bool Perm_Access = false;
        protected bool Perm_AddTeam = false;
        protected string Permission = "";
        /// <summary>
        /// check permission
        /// </summary>
        protected void checkPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                //string perName = Session["User_Permission"].ToString();
                //string url = Request.Url.AbsolutePath.ToString();
                //Solution_30shineEntities db = new Solution_30shineEntities();
                //var pem = (from m in db.Tbl_Permission_Map
                //           join mn in db.Tbl_Permission_Menu on m.mID equals mn.mID
                //           where m.pName == perName && mn.Url_Main.EndsWith(url) || mn.Url_Rewrite.EndsWith(url)
                //           select new { m.aID }
                //           ).FirstOrDefault();
                //if (pem != null)
                //{
                //    var pemArray = pem.aID.Split(',');
                //    if (Array.IndexOf(pemArray, "1") > -1)
                //    {
                //        Perm_Access = true;
                //        if (Array.IndexOf(pemArray, "2") > -1)
                //        {
                //            Perm_ViewAllData = true;
                //        }
                //        if (Array.IndexOf(pemArray, "15") > -1)
                //        {
                //            Perm_AddTeam = true;
                //        }

                //    }
                //    else
                //    {
                //        ContentWrap.Visible = false;
                //        NotAllowAccess.Visible = true;
                //    }

                //}
                IPermissionModel permissionModel = new PermissionModel();
                int integer;
                string pageId = permissionModel.GetRegexPageId().Replace(Request.RawUrl, "/edit.html");
                var staffId = int.TryParse(Session["User_Id"].ToString(), out integer) ? integer : 0;
                Perm_Access = permissionModel.CheckPermisionByAction("Perm_Access", pageId, staffId);
                Perm_ViewAllData = permissionModel.CheckPermisionByAction("Perm_ViewAllData", pageId, staffId);
                Perm_AddTeam = permissionModel.CheckPermisionByAction("Perm_AddTeam", pageId, staffId);
                //Huy kết nối db.
                permissionModel.DisposeConnectString();
                ExecuteByPermission();


            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            checkPermission();
            if (!IsPostBack)
            {
                Library.Function.bindSalon(new List<DropDownList> { ddlSalon }, Perm_ViewAllData);
                bindDepartment();
                bindData();
            }
            else
            {
                //Exc_Filter();
            }
            RemoveLoading();
        }

        private void CheckPermission()
        {
            if (Session["IsValid2FA"] == null || !Convert.ToBoolean(Session["IsValid2FA"]))
            {
                var MsgParam = new List<KeyValuePair<string, string>>();
                UIHelpers.Redirect("/dang-nhap.html", MsgParam);
            }
            else
            {
                string[] Allow = new string[] { "root", "admin", "inventory_manager", "inventory_manager_2" };
                string[] AllowInventory = new string[] { "inventory_manager", "inventory_manager_2" };
                string[] AllowSalon = new string[] { "root", "admin", "inventory_manager", "inventory_manager_2" };
                string[] Accountant = new string[] { "accountant" };
                string[] AllowShowElement = new string[] { "root", "admin" };
                Permission = Session["User_Permission"].ToString().Trim();
                if (Array.IndexOf(Allow, Permission) != -1)
                {
                    Perm_Access = true;
                }

                if (Array.IndexOf(AllowSalon, Permission) != -1)
                {
                    Perm_ViewAllData = true;
                }
            }
            // Call execute function
            ExecuteByPermission();
        }

        private void ExecuteByPermission()
        {
            if (!Perm_Access)
            {
                ContentWrap.Visible = false;
                NotAllowAccess.Visible = true;
            }
        }
        private void bindData()
        {
            using (var db = new Solution_30shineEntities())
            {
                try
                {
                    if (ddlSalon.SelectedValue != "0")
                    {
                        salonId = int.TryParse(ddlSalon.SelectedValue, out integer) ? integer : 0;
                        var departmentId = int.TryParse(ddlBoPhan.SelectedValue, out integer) ? integer : 0;
                        var sql = "";
                        sql = "select A.*, D.Name as SkillLevelName from Staff A left join Tbl_SkillLevel D on D.Id = A.SkillLevel where (A.IsDelete is null or A.IsDelete = 0) and A.Active = 1" +
                            "   and A.SalonId =" + salonId;
                        if (ddlBoPhan.SelectedValue != "0")
                        {
                            sql += "   and A.Type =" + departmentId;
                        }
                        var data = db.Database.SqlQuery<Staff_History>(sql).ToList();
                        Bind_Paging(data.Count);
                        rptHistorySalon.DataSource = data.Skip(PAGING._Offset).Take(PAGING._Segment).ToList();
                        rptHistorySalon.DataBind();
                    }
                }
                catch { }
            }
        }

        protected void Bind_Paging(int TotalRow)
        {
            // init Paging value            
            PAGING._Segment = !HDF_OPTSegment.Value.Equals("") ? Convert.ToInt32(HDF_OPTSegment.Value) : PAGING._Segment;
            PAGING._PageNumber = IsPostBack ? Convert.ToInt32(HDF_Page.Value) : 1;
            PAGING._PageNumber = PAGING._PageNumber > 0 ? PAGING._PageNumber : 1;
            PAGING.TotalPage = !PAGING.TotalPage.Equals(null) ? Get_TotalPage(TotalRow) : PAGING.TotalPage;
            PAGING._Offset = (PAGING._PageNumber - 1) * PAGING._Segment;
            PAGING._Paging = PAGING.Make_Paging();

            RptPaging.DataSource = PAGING._Paging.ListPage;
            RptPaging.DataBind();
        }

        protected int Get_TotalPage(int TotalRow)
        {
            TotalRow -= PAGING._TopNewsNum;
            int ReturnTotalPage = Convert.ToInt32(Math.Ceiling((double)TotalRow / PAGING._Segment));
            return ReturnTotalPage >= 0 ? ReturnTotalPage : 0;
        }

        
        /// <summary>
        /// Bind dữ liệu bộ phận
        /// </summary>
        private void bindDepartment()
        {
            using (var db = new Solution_30shineEntities())
            {
                var _department = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).OrderBy(o => o.Id).ToList();
                var Key = 0;

                ddlBoPhan.DataTextField = "Type";
                ddlBoPhan.DataValueField = "Id";

                ListItem item = new ListItem();
                foreach (var v in _department)
                {
                    item = new ListItem(v.Name, v.Id.ToString());
                    ddlBoPhan.Items.Insert(Key++, item);
                }
                ddlBoPhan.SelectedIndex = 0;

            }
        }
        protected void _BtnClick(object sender, EventArgs e)
        {
            bindData();
            RemoveLoading();
        }

        /// <summary>
        /// Remove Loading Status
        /// </summary>
        public void RemoveLoading()
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "loading", "removeLoading();", true);
        }

        [WebMethod]
        public static List<Tbl_Salon> ReturnAllSalon()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Tbl_Salon.Where(a => a.IsDelete == 0 && a.Publish == true).ToList();
                return lst;
            }
        }

        [WebMethod]
        public static List<Staff_Type> ReturnAllStaff_Type()
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Staff_Type.Where(a => a.IsDelete == 0 && a.Publish == true).ToList();
                return lst;
            }
        }

        [WebMethod]
        public static List<Staff> ReturnAllStaffBySalon(int _SalonId, int _DepartmentId)
        {
            using (var db = new Solution_30shineEntities())
            {

                if (_SalonId != 0 && _DepartmentId != 0)
                {
                    var lst = db.Staffs.Where(a => a.IsDelete == 0 && a.Active == 1 && a.SalonId == _SalonId && a.Type == _DepartmentId).ToList();
                    return lst;
                }
                else if (_SalonId != 0 && _DepartmentId == 0)
                {
                    var lst = db.Staffs.Where(a => a.IsDelete == 0 && a.Active == 1 && a.SalonId == _SalonId).ToList();
                    return lst;
                }
                else
                {
                    var lst = db.Staffs.Where(a => a.IsDelete == 0 && a.Active == 1).ToList();
                    return lst;
                }
            }
        }

        [WebMethod]
        public static List<Staff_HistorySalonId> LoadHistorySalonById(int _StaffId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var sql = "select A.*, B.Fullname as StaffName, C.Name as OlderSalonName  , D.Name as CurrentSalonName" +
                        "   from Staff_Salon_History A left join Staff B on A.StaffId = B.Id" +
                        "   left join Tbl_Salon C on C.Id = A.OlderSalonId  left join Tbl_Salon D on D.Id = A.CurrentSalonId " +
                        "   where (A.IsDelete is null or A.IsDelete = 0) and A.StaffId =" + _StaffId;
                var data = db.Database.SqlQuery<Staff_HistorySalonId>(sql).ToList();
                return data;
            }
        }

        [WebMethod]
        public static List<Staff> getStaffById(int _staffId)
        {
            using (var db = new Solution_30shineEntities())
            {
                var lst = db.Staffs.Where(a => a.IsDelete == 0 && a.Active == 1 && a.Id == _staffId).ToList();
                return lst;
            }
        }

        [WebMethod]
        public static string TransferSalonConfirm(int salonFromId, int salonToId, int staffId)
        {
            var Message = new Library.Class.cls_message();
            var serialize = new JavaScriptSerializer();
            var error = 0;
            var db = new Solution_30shineEntities();
            var obj = new Staff_Salon_History();
            var RECORD = db.Staffs.FirstOrDefault(w => w.Id == staffId);
            try
            {
                if (salonFromId != 0 && salonToId != 0 && staffId != 0)
                {
                    obj.StaffId = staffId;
                    obj.OlderSalonId = salonFromId;
                    obj.CurrentSalonId = salonToId;
                    obj.TransferDate = DateTime.Now;
                    obj.IsDelete = false;
                    db.Staff_Salon_History.Add(obj);
                    error += db.SaveChanges() > 0 ? 0 : 1;
                    if (error == 0)
                    {
                        Message.success = true;
                        Message.message = "Thêm thành công.";
                    }
                    else
                    {
                        Message.success = false;
                        Message.message = "Thêm thất bại.";
                    }

                    if (RECORD != null)
                    {
                        RECORD.SalonId = salonToId;
                        RECORD.ModifiedDate = DateTime.Now;
                        db.Staffs.AddOrUpdate(RECORD);
                        error += db.SaveChanges() > 0 ? 0 : 1;
                        if (error == 0)
                        {
                            Message.success = true;
                            Message.message = "Cập nhật thành công.";
                        }
                        else
                        {
                            Message.success = false;
                            Message.message = "Cập nhật thất bại.";
                        }
                    }
                }
                return serialize.Serialize(Message);
            }
            catch (Exception ex)
            {
                Message.success = false;
                Message.message = ex.Message;
                return serialize.Serialize(Message);
            }

        }
    }
    public class Staff_History : Staff
    {
        public string SkillLevelName { get; set; }
    }
    public class Staff_HistorySalonId : Staff_Salon_History
    {
        public string OlderSalonName { get; set; }
        public string CurrentSalonName { get; set; }
    }
}