﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Finger_Add.aspx.cs" Inherits="_30shine.GUI.BackEnd.UIStaff.Finger_Add" MasterPageFile="~/TemplateMaster/SiteMaster.Master" %>

<asp:Content ID="StaffAdd" ContentPlaceHolderID="CtMain" runat="server">
<asp:Panel CssClass="wp not-allow-access" ID="NotAllowAccess" runat="server" Visible="false"><i class="fa fa-exclamation-circle"></i>Bạn không có quyền truy cập.</asp:Panel>
<asp:Panel CssClass="wp" ID="ContentWrap" runat="server">

<div class="wp sub-menu">
    <div class="wp960">
        <div class="wp content-wp">
            <ul class="ul-sub-menu" id="subMenu">
                <li class="li-listing"><a href="/admin/nhan-vien/danh-sach.html">Danh sách</a></li>
                <li class="li-add"><a href="/admin/nhan-vien/them-moi.html">Thêm mới</a></li>
                <li class="li-finger-add active"><a href="/admin/nhan-vien/gan-van-tay.html">Gán dấu vân tay</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="wp customer-add">
    <%-- Add --%>                
    <div class="wp960 content-wp">
        <!-- System Message -->
        <asp:Label ID="MsgSystem" CssClass="msg-system" runat="server" ClientIDMode="Static"></asp:Label>
        <!-- END System Message -->

        <div class="table-wp">
            <table class="table-add">
                <tbody>
                    <tr class="title-head">
                        <td><strong>Thông tin nhân viên</strong></td>
                        <td></td>
                    </tr>
                    <tr class="tr-margin" style="height: 20px;"></tr>
                    <tr runat="server" id="TrSalon">
                        <td class="col-xs-2 left"><span>Salon</span></td>
                        <td class="col-xs-9 right">
                            <span class="field-wp">
                                <asp:DropDownList ID="Salon" CssClass="form-control select" runat="server" ClientIDMode="Static"></asp:DropDownList>
                            </span>
                        </td>
                    </tr>

                    <tr>
                        <td></td>
                        <td>
                            <asp:Repeater runat="server" ID="Rpt_Staff">
                                <ItemTemplate>
                                    <div class="checkbox cus-no-infor" style="padding-top: 4px;">
                                        <label class="lbl-cus-no-infor">
                                            <input name="staff" type="radio" data-id="<%# Eval("Id") %>" onclick="bindStaffIdToHDF($(this))"/> <%# Eval("Fullname") %>
                                        </label>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                    
                    <tr class="tr-send">
                        <td class="col-xs-2 left"></td>
                        <td class="col-xs-9 right no-border">
                            <span class="field-wp">
                                <asp:Button ID="Send" CssClass="btn-send" runat="server" Text="Hoàn tất" ClientIDMode="Static" OnClick="Add_Finger"></asp:Button>
                                <%--<span class="btn-send btn-cancel" id="CalcelAddFbcv" style="margin-left: 12px;" >Hủy</span>--%>
                            </span>
                        </td>
                    </tr>
                    <asp:HiddenField runat="server" ID="UrlKeyField"  ClientIDMode="Static"/>
                    <asp:HiddenField runat="server" ID="ImgCoverField"  ClientIDMode="Static"/>
                    <asp:HiddenField runat="server" ID="HDF_StaffId"  ClientIDMode="Static"/>
                </tbody>
            </table>
        </div>
    </div>
    <%-- end Add --%>
</div>
<link href="../../../Assets/js/select2/select2.min.css" rel="stylesheet" />
        <script src="../../../Assets/js/select2/select2.min.js"></script>
        <script>
            $('.select').select2();
        </script>
        <style>
            .select2-container {
                width: 190px !important;
                margin-top: 5px !important;
            }
            .select2-container--default .select2-selection--single {
                height: 32px !important;
                padding-top: 1px !important;
            }
        </style>
<script>
    jQuery(document).ready(function () {
        $("#glbAdminStaff").addClass("active");

        //============================
        // Datepicker
        //============================
        $('.txtDateTime').datetimepicker({
            dayOfWeekStart: 1,
            lang: 'vi',
            startDate: '2014/10/10',
            format: 'd/m/Y',
            dateonly: true,
            showHour: false,
            showMinute: false,
            timepicker: false,
            onChangeDateTime: function (dp, $input) {
                //$input.val($input.val().split(' ')[0]);
            }
        });
    });

    function bindStaffIdToHDF(This) {
        $("#HDF_StaffId").val(This.attr("data-id"));
    }
</script>

</asp:Panel>
</asp:Content>

