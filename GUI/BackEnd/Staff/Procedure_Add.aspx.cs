﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using _30shine.MODEL.ENTITY.EDMX;
using System.Data.Entity.Migrations;
using _30shine.Helpers;
using System.Web.Script.Serialization;

namespace _30shine.GUI.BackEnd.StaffController
{
    public partial class Procedure_Add : System.Web.UI.Page
    {
        private Solution_30shineEntities db;
        protected Staff_Procedure OBJ;
        protected string _Code;
        protected bool _IsUpdate = false;
        public UIHelpers _UIHelpers = new UIHelpers();
        protected cls_media Img = new cls_media();
        protected List<cls_media> listImg = new List<cls_media>();

        public Procedure_Add()
        {
            db = new Solution_30shineEntities();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (IsUpdate())
                {
                    if (Bind_OBJ())
                    {
                        Bind_DepartmentId();
                    }
                }
                else
                {
                    Bind_DepartmentId();
                }

            }
        }

        protected void ExcAddOrUpdate(object sender, EventArgs e)
        {
            if (IsUpdate())
            {
                Update();
            }
            else
            {
                Add();
            }
        }

        private void Add()
        {
            var obj = new Staff_Procedure();
            obj.Title = Title.Text;
            obj.Description = Description.Text;
            obj.Images = HDF_Images.Value;
            obj.DepartmentId = Convert.ToInt32(DepartmentId.SelectedValue);
            obj.Order = _Order.Text != "" ? Convert.ToInt32(_Order.Text) : 0;
            obj.Publish = Publish.Checked;
            obj.CreatedTime = DateTime.Now;
            obj.IsDelete = false;

            // Validate
            var Error = false;

            if (!Error)
            {
                db.Staff_Procedure.Add(obj);
                var exc = db.SaveChanges();

                if (exc > 0)
                {
                    var MsgParam = new List<KeyValuePair<string, string>>();
                    MsgParam.Add(new KeyValuePair<string, string>("msg_add_status", "success"));
                    MsgParam.Add(new KeyValuePair<string, string>("msg_add_message", "Cập nhật thành công!"));
                    UIHelpers.Redirect("/admin/quy-trinh/" + obj.Id + ".html", MsgParam);
                }
                else
                {
                    var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển.";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }
            }
        }

        private void Update()
        {
            using (var db = new Solution_30shineEntities())
            {
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Staff_Procedure.FirstOrDefault(w => w.Id == Id);

                    if (!OBJ.Equals(null))
                    {
                        OBJ.Title = Title.Text;
                        OBJ.Description = Description.Text;
                        OBJ.Images = HDF_Images.Value;
                        OBJ.DepartmentId = Convert.ToInt32(DepartmentId.SelectedValue);
                        OBJ.Order = _Order.Text != "" ? Convert.ToInt32(_Order.Text) : 0;
                        OBJ.Publish = Publish.Checked;
                        OBJ.ModifiedTime = DateTime.Now;

                        db.Staff_Procedure.AddOrUpdate(OBJ);
                        var exc = db.SaveChanges();
                        if (exc > 0)
                        {
                            var MsgParam = new List<KeyValuePair<string, string>>();
                            MsgParam.Add(new KeyValuePair<string, string>("msg_add_status", "success"));
                            MsgParam.Add(new KeyValuePair<string, string>("msg_add_message", "Cập nhật thành công!"));
                            UIHelpers.Redirect("/admin/quy-trinh/" + OBJ.Id + ".html", MsgParam);
                        }
                        else
                        {
                            var msg = "Cập nhật thất bại! Vui lòng liên hệ nhóm phát triển!";
                            var status = "warning";
                            UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                        }
                    }
                    else
                    {
                        var msg = "Lỗi! Không tìm thấy danh mục.";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);

                    }
                }
                else
                {
                    MsgSystem.Text = "Lỗi! Không tìm thấy danh mục.";
                    MsgSystem.CssClass = "msg-system warning";
                }

                Context.RewritePath("+", "", "");
            }
        }

        private bool Bind_OBJ()
        {
            using (var db = new Solution_30shineEntities())
            {
                var serialize = new JavaScriptSerializer();
                var ExistOBJ = true;
                int Id;
                if (int.TryParse(_Code, out Id))
                {
                    OBJ = db.Staff_Procedure.Where(w => w.Id == Id).FirstOrDefault();

                    if (!OBJ.Equals(null))
                    {
                        Title.Text = OBJ.Title;
                        Description.Text = OBJ.Description;
                        _Order.Text = Convert.ToString(OBJ.Order);

                        if (OBJ.Images != null && OBJ.Images != "")
                        {
                            listImg = serialize.Deserialize<List<cls_media>>(OBJ.Images).ToList();
                        }

                        if (OBJ.Publish == true)
                        {
                            Publish.Checked = true;
                        }
                        else
                        {
                            Publish.Checked = false;
                        }

                    }
                    else
                    {
                        ExistOBJ = false;
                        var msg = "Lỗi! Danh mục không tồn tại";
                        var status = "warning";
                        UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                    }
                }
                else
                {
                    ExistOBJ = false;
                    var msg = "Lỗi! Danh mục không tồn tại";
                    var status = "warning";
                    UIHelpers.TriggerJsMsgSystem(Page, msg, status, 5000);
                }

                return ExistOBJ;
            }
        }

        public void Bind_DepartmentId()
        {
            using (var db = new Solution_30shineEntities())
            {
                var LST = db.Staff_Type.Where(w => w.IsDelete != 1 && w.Publish == true).ToList();
                var Key = 0;
                var Count = LST.Count;
                DepartmentId.DataTextField = "Name";
                DepartmentId.DataValueField = "Id";
                ListItem item = new ListItem("Chọn bộ phận", "0");
                DepartmentId.Items.Insert(Key, item);
                Key++;

                if (Count > 0)
                {
                    foreach (var v in LST)
                    {
                        item = new ListItem(v.Name, v.Id.ToString());
                        DepartmentId.Items.Insert(Key, item);
                        Key++;
                    }
                }

                if (_IsUpdate)
                {
                    var ItemSelected = DepartmentId.Items.FindByValue(OBJ.DepartmentId.ToString());
                    if (ItemSelected != null)
                        ItemSelected.Selected = true;
                }
                else
                {
                    DepartmentId.SelectedIndex = 0;
                }
            }
        }

        public void ReloadPage()
        {
            HttpRequest Request = HttpContext.Current.Request;
            string host = "http://" + Request.Url.Host.ToString();
            string rawUrl = Request.RawUrl.ToString();
            string Url = host + rawUrl;
            Response.Redirect(Url);
        }

        private bool IsUpdate()
        {
            _Code = Request.QueryString["Code"];
            if (_Code != null)
            {
                _IsUpdate = true;
                return true;
            }
            else
            {
                _IsUpdate = false;
                return false;
            }
        }

        public class cls_media
        {
            public string url { get; set; }
            public string title { get; set; }
            public string description { get; set; }
        }
    }
}